﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using AuthenAPI.Helpers;
using AuthenAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
using LOS.Common.Utils;
using LOS.Common.Extensions;
using Z.EntityFramework.Plus;

namespace AuthenAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;

        public UsersController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
        }

        [HttpGet]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetAllUsers([FromQuery] int page = 1, [FromQuery] int pageSize = 10, [FromQuery] int groupId = -1, [FromQuery] int status = -1, [FromQuery] string name = null, [FromQuery] string sortBy = "UserId", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<UserDetail>>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => x.IsDeleted != true
                && (x.GroupId == groupId || groupId == -1)
                && (x.Status == status || status == -1)
                && (name == null || x.Username.Contains(name))
                && (name == null || x.FullName.Contains(name))
                , null, false, x => x.Company, x => x.Department, x => x.Position, x => x.Group).Select(UserDetail.ProjectionDetail);
                var totalRecords = query.Count();
                List<UserDetail> data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Users/GetAllUsers Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetById(int id)
        {
            var def = new DefaultResponse<Meta, UserDetail>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => x.UserId == id
                , null, false, x => x.Company, x => x.Department, x => x.Position).Select(UserDetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("getuser")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetByUserName(string username)
        {
            var def = new DefaultResponse<Meta, UserDetail>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => x.Username.ToLower() == username.Trim().ToLower()
                , null, false, x => x.Company, x => x.Department, x => x.Position).Select(UserDetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        public ActionResult<DefaultResponse<object>> PostUser([FromBody] UserDTO user)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.BeginTransaction();
                var result = _unitOfWork.UserRepository.Insert(user.Mapping());
                _unitOfWork.Save();
                //insert Group Module
                if (user.ListUserModule != null && user.ListUserModule.Count > 0)
                {
                    var lstUserModule = user.ListUserModule.Where(x => x.ModuleId > 0).ToList();
                    lstUserModule.ForEach(x => x.UserId = result.UserId);
                    _unitOfWork.UserModuleRepository.Insert(lstUserModule);
                }
                _unitOfWork.Save();
                if (result.UserId > 0)
                {
                    _unitOfWork.CommitTransaction();
                    def.data = result.UserId;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    _unitOfWork.RollbackTransaction();
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "Users/CreateUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut]
        [Route("{id}")]
        public ActionResult<DefaultResponse<object>> PutUser(int id, [FromBody] UserDTO user)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != user.UserId)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var lstUserModule = user.ListUserModule;
                var entityOld = _unitOfWork.UserRepository.GetById(user.UserId);
                Ultility.CopyObject(user.Mapping(), ref entityOld, true);
                entityOld.UpdatedTime = DateTime.Now;
                _unitOfWork.BeginTransaction();
                //cập nhật User
                _unitOfWork.UserRepository.Update(entityOld);
                //xóa toàn bộ dữ liệu cũ User-Module
                _unitOfWork.UserModuleRepository.Delete(x => x.UserId == user.UserId);
                //Thêm dữ liệu mới User-Module
                _unitOfWork.UserModuleRepository.Insert(lstUserModule);
                _unitOfWork.Save();
                _unitOfWork.CommitTransaction();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "Users/PutUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpDelete]
        [Route("{id}")]
        public ActionResult<DefaultResponse<object>> DeleteUser(int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.UserRepository.SoftDelete(id);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Users/DeleteUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_staff_hub")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetStaffOfHub(int hubId)
        {
            var def = new DefaultResponse<Meta, List<UserDetail>>();
            try
            {
                var query = _unitOfWork.UserShopRepository.Query(x => x.ShopId == hubId
                , null, false, x => x.User).Where(x => x.User.GroupId == (int)EnumGroupUser.StaffHub).Select(x => new UserDetail
                {
                    UserId = x.User.UserId,
                    FullName = x.User.FullName,
                    Username = x.User.Username,
                    Email = x.User.Email,
                    Phone = x.User.Phone,
                    GroupId = x.User.GroupId,
                    Status = x.User.Status,
                    Ipphone = x.User.Ipphone
                }).ToList();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetStaffOfHub Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_manager_hub")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetManagerOfHub(int hubId)
        {
            var def = new DefaultResponse<Meta, List<UserDetail>>();
            try
            {
                var query = _unitOfWork.UserShopRepository.Query(x => x.ShopId == hubId
                , null, false, x => x.User).Where(x => x.User.GroupId == (int)EnumGroupUser.ManagerHub).Select(x => new UserDetail
                {
                    UserId = x.User.UserId,
                    FullName = x.User.FullName,
                    Username = x.User.Username,
                    Email = x.User.Email,
                    Phone = x.User.Phone,
                    GroupId = x.User.GroupId,
                    Status = x.User.Status,
                    Ipphone = x.User.Ipphone
                }).ToList();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetManagerOfHub Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_user_permission")]
        public ActionResult<DefaultResponse<SummaryMeta, UserDetail>> GetPermissionUser(int userId)
        {
            var def = new DefaultResponse<Meta, UserDetail>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => x.UserId == userId, null, false, x => x.Group).Select(UserDetail.ProjectionPermissionDetail);
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query.FirstOrDefault();
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetPermissionUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_permission")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> UpdatePermissionUser(PermissionUserDTO entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                //xóa dữ liệu cũ
                if (_unitOfWork.UserConditionRepository.Any(x => x.UserId == entity.UserId))
                    _unitOfWork.UserConditionRepository.Delete(x => x.UserId == entity.UserId);
                if (_unitOfWork.UserActionPermissionRepository.Any(x => x.UserId == entity.UserId))
                    _unitOfWork.UserActionPermissionRepository.Delete(x => x.UserId == entity.UserId);
                //insert dữ liệu mới
                if (entity.UserConditions != null && entity.UserConditions.Count > 0)
                {
                    foreach (var item in entity.UserConditions)
                        _unitOfWork.UserConditionRepository.Insert(new LOS.DAL.EntityFramework.UserCondition()
                        {
                            UserId = entity.UserId,
                            PropertyId = item.PropertyId,
                            Operator = item.Operator,
                            CreatetAt = DateTime.Now,
                            CreatedBy = item.CreatedBy,
                            Value = item.Value
                        });
                }

                if (entity.UserActionPermissions != null && entity.UserActionPermissions.Count > 0)
                {
                    foreach (var item in entity.UserActionPermissions)
                        _unitOfWork.UserActionPermissionRepository.Insert(new LOS.DAL.EntityFramework.UserActionPermission()
                        {
                            UserId = entity.UserId,
                            CreatedBy = item.CreatedBy,
                            CreatedAt = DateTime.Now,
                            LoanStatus = item.LoanStatus,
                            Value = item.Value
                        });
                }

                _unitOfWork.Save();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.UserId;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/UpdatePermissionUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

    }
}