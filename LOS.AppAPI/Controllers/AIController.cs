﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Utils;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.AppAPI.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using LOS.Common.Models.Request;
using LOS.Common.Extensions;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Authorization;
using LOS.AppAPI.DTOs.LMS;
using LOS.AppAPI.Models.LMS;
using Microsoft.Extensions.Configuration;
using RestSharp;
using Newtonsoft.Json;
using LOS.AppAPI.Models.App;
using LOS.AppAPI.DTOs.App;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.EntityFrameworkCore;
using LOS.DAL.DTOs;
using LOS.AppAPI.Service.BankLog;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.Service.Notification;
using LOS.AppAPI.Models.Notification;
using LOS.AppAPI.Service;
using LOS.AppAPI.Models.AI;
using LOS.AppAPI.Models;
using LOS.DAL.Dapper;
using System.Text;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class AIController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        private IConfiguration _configuration;

        public AIController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        //Chi tiết đơn vay 
        [HttpGet]
        [Route("get_status")]
        public ActionResult<DefaultResponse<Meta, GetStatus>> GetById([FromQuery] int LoanBriefId = 0)
        {
            var def = new DefaultResponse<Meta, GetStatus>();
            try
            {
                if (LoanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanBrief = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId, null, false).Select(x => new
                {
                    LoanbriefId = x.LoanBriefId,
                    Status = x.Status
                }).FirstOrDefault();
                if (loanBrief != null)
                {
                    var obj = new GetStatus
                    {
                        StatusId = loanBrief.Status.Value,
                        StatusStr = Description.GetDescription((EnumLoanStatus)loanBrief.Status)
                    };
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = obj;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "AI/GetById Exception");
            }
            return Ok(def);
        }
        [HttpGet]
        [AuthorizeToken("APP_API_AI")]
        [Route("get_info_loanbrief")]
        public async Task<ActionResult<DefaultResponse<Meta, List<GetInfoLoanBriefConvertAG>>>> GetInfoLoanBrief(int? loancreditId = null, int? customerId = null, string phone = null, string national_id = null)
        {
            var def = new DefaultResponse<Meta, List<GetInfoLoanBriefConvertAG>>();
            try
            {
                var lstData = new List<GetInfoLoanBriefConvertAG>();
                //Tìm kiếm theo LoanCreditId
                var db = new DapperHelper(_configuration.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("SELECT L.Phone as 'phone', L.NationalCard as 'national_id', L.FullName as 'full_name', ISNULL(LP.BrandId , 0) as 'manufacturerId', ");
                sql.AppendLine("P.Year as 'yearMade', P.FullName as 'vehicle_full_name',LR.Address as 'current_address', ISNULL(LR.ProvinceId, 0) as 'current_address_city_id', ");
                sql.AppendLine("ISNULL(LR.DistrictId, 0) as 'current_address_district_id', ISNULL(LR.WardId, 0) as 'current_address_ward_id', ISNULL(LR.ResidentType, 0) as 'home_ownership_type_id', ");
                sql.AppendLine("L.BankId as 'bank_id', L.BankAccountNumber as 'bank_account_number', L.LoanAmount as 'loan_money', L.LoanTime as 'total_loan_time', ");
                sql.AppendLine("ISNULL(LJ.CompanyProvinceId, 0) as 'company_address_city_id', ISNULL(LJ.CompanyDistrictId, 0) as 'company_address_district_id', ");
                sql.AppendLine("ISNULL(LJ.CompanyWardId, 0) as 'company_address_ward_id', LJ.CompanyAddress as 'company_address', LJ.CompanyName as 'company_name', ");
                sql.AppendLine("L.LoanBriefId as 'loanCreditId', L.CustomerId as 'customerId', L.CreatedTime as 'createDate', L.Status as 'Status', L.LMS_LoanID as 'LoanID', L.ProductId as 'ProductId' ");
                sql.AppendLine("from LoanBrief(NOLOCK) L  left join LoanBriefProperty(NOLOCK) LP ON LP.LoanBriefPropertyId = L.LoanBriefId ");
                sql.AppendLine("left join Product(NOLOCK) P ON LP.ProductId = P.Id ");
                sql.AppendLine("left join LoanBriefResident(NOLOCK) LR ON L.LoanBriefId = LR.LoanBriefResidentId ");
                sql.AppendLine("left join LoanBriefJob(NOLOCK) LJ ON L.LoanBriefId = LJ.LoanBriefJobId ");

                if (loancreditId != null)
                {
                    sql.AppendFormat("WHERE L.LoanBriefId = {0}", loancreditId);
                }
                //Tìm kiếm theo CustomerCredit
                else if (customerId != null)
                {
                    sql.AppendFormat("WHERE L.CustomerId = {0}", customerId);
                }
                //Tìm kiếm theo SĐT
                else if (!string.IsNullOrEmpty(phone))
                {
                    if (!phone.ValidPhone())
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Param tìm kiếm không hợp lệ");
                        return Ok(def);
                    }
                    sql.AppendFormat("WHERE L.Phone = '{0}'", phone);
                }
                //Tìm kiếm theo CMND
                else if (!string.IsNullOrEmpty(national_id))
                {
                    if (national_id.Length != 9 || national_id.Length != 12)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Param tìm kiếm không hợp lệ");
                        return Ok(def);
                    }
                    sql.AppendFormat("WHERE L.NationalCard = '{0}'", national_id);
                }
                //Chưa truyền param
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Param tìm kiếm không hợp lệ");
                    return Ok(def);
                }
                lstData = await db.QueryAsync<GetInfoLoanBriefConvertAG>(sql.ToString());

                //Nếu có data thì add list file vào

                //Nếu có data thì add list file vào
                if (lstData != null && lstData.Count > 0)
                {
                    var loanbriefIds = lstData.Select(x => x.loanCreditId).ToList();
                    var listFiles = await unitOfWork.LoanBriefFileRepository.Query(x => loanbriefIds.Contains(x.LoanBriefId.Value) && x.Status == 1
                        && x.IsDeleted != 1, null, false).Select(ListLoanBriefFile.ProjectionDetail).ToListAsync();
                    var lstLoanBriefRelationship = await unitOfWork.LoanBriefRelationshipRepository.Query(x => loanbriefIds.Contains(x.LoanBriefId.Value), null, false).ToListAsync();
                    Dictionary<int, List<ListLoanBriefFile>> dicFile = new Dictionary<int, List<ListLoanBriefFile>>();
                    Dictionary<int, List<LoanBriefRelationship>> dicRelationship = new Dictionary<int, List<LoanBriefRelationship>>();
                    if (listFiles != null && listFiles.Count > 0)
                    {
                        foreach (var item in listFiles)
                        {
                            if (item.loanbrief_Id > 0)
                            {
                                if (!dicFile.ContainsKey(item.loanbrief_Id))
                                    dicFile[item.loanbrief_Id] = new List<ListLoanBriefFile>();

                                dicFile[item.loanbrief_Id].Add(item);
                            }
                        }
                    }

                    if (lstLoanBriefRelationship != null && lstLoanBriefRelationship.Count > 0)
                    {
                        foreach (var item in lstLoanBriefRelationship)
                        {
                            if (!dicRelationship.ContainsKey(item.LoanBriefId.Value))
                                dicRelationship[item.LoanBriefId.Value] = new List<LoanBriefRelationship>();

                            dicRelationship[item.LoanBriefId.Value].Add(item);
                        }
                    }
                    await Task.Run(() => Parallel.ForEach(lstData, loanbrief =>
                    {
                        if (dicFile.ContainsKey(loanbrief.loanCreditId))
                            loanbrief.loanbrieffiles = dicFile[loanbrief.loanCreditId];
                        if (dicRelationship.ContainsKey(loanbrief.loanCreditId))
                        {
                            loanbrief.reference_phone = dicRelationship[loanbrief.loanCreditId][0].Phone;
                            loanbrief.reference_name = dicRelationship[loanbrief.loanCreditId][0].FullName;
                            loanbrief.reference_relationship_id = dicRelationship[loanbrief.loanCreditId][0].RelationshipType ?? 0;
                        }
                    }));



                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = lstData;
                }

            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return Ok(def);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("create_product")]
        [AuthorizeToken("APP_API_AI")]
        public ActionResult<DefaultResponse<object>> CreateProduct(ProductReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(req.model) || string.IsNullOrEmpty(req.brand) || req.type == 0 || req.year <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //kiểm tra brand
                var brandId = 0;
                var brand = unitOfWork.BrandProductRepository.Query(x => x.Name.ToLower().Trim().Equals(req.brand.ToLower().Trim()), null, false).FirstOrDefault();
                if (brand == null)
                {
                    var objBrand = new BrandProduct()
                    {
                        Name = req.brand.ToLower().Trim(),
                        IsEnable = true
                    };
                    unitOfWork.BrandProductRepository.Insert(objBrand);
                    unitOfWork.Save();
                    brandId = objBrand.Id;
                }
                else
                {
                    brandId = brand.Id;
                }
                var isCreate = false;
                var fullName = string.Format("{0} {1} {2}", req.brand.Trim().ToLower(), req.model.Trim().ToLower(), req.year);
                var product = unitOfWork.ProductRepository.Query(x => x.ShortName.ToLower() == req.model.ToLower().Trim() && x.Year == req.year && x.IsEnable == true, null, false).FirstOrDefault();
                if (product != null)
                {
                    if (fullName != product.FullName.Trim().ToLower())
                        isCreate = true;
                }
                else
                {
                    isCreate = true;
                }
                if (isCreate)
                {
                    var entity = new DAL.EntityFramework.Product()
                    {
                        BrandProductId = brandId,
                        Name = string.Format("{0} {1}", req.model.Trim().ToLower(), req.year),
                        FullName = fullName,
                        ShortName = req.model.Trim().ToLower(),
                        Year = req.year,
                        CreateBy = 1,
                        CreateTime = DateTime.Now,
                        IsEnable = true,
                        ProductTypeId = req.type
                    };
                    unitOfWork.ProductRepository.Insert(entity);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Sản phẩm đã tồn tại trong hệ thống");
                    return Ok(def);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "AI/create_product Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("change_status_transaction_secured")]
        //[AuthorizeToken("APP_API_AI")]
        public async Task<ActionResult<DefaultResponse<Meta>>> ChangeStatusTransactionSecured([FromBody] LogSecuredTransaction req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (string.IsNullOrEmpty(req.NoticeNumber) || req.ConfirmStatus.GetValueOrDefault(0) <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (!Enum.IsDefined(typeof(EnumTransactionState), req.ConfirmStatus))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Trạng thái giao dịch đảm bảo không hợp lệ");
                    return Ok(def);
                }
                var info = await unitOfWork.LogSecuredTransactionRepository.Query(x => x.NoticeNumber == req.NoticeNumber.Trim(), null, false).FirstOrDefaultAsync();
                if (info != null)
                {
                    if (info.ConfirmStatus.GetValueOrDefault(0) != 0)
                    {
                        if (info.ConfirmStatus == (int)EnumTransactionState.DaDuyet)
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Giao dịch đảm bảo đã được duyệt");
                        else if (info.ConfirmStatus == (int)EnumTransactionState.Huy)
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Giao dịch đảm bảo đã bị hủy");
                        return Ok(def);
                    }
                    unitOfWork.LogSecuredTransactionRepository.Update(x => x.Id == info.Id, x => new LogSecuredTransaction()
                    {
                        ConfirmStatus = req.ConfirmStatus
                    });

                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == info.LoanBriefId, x => new LoanBrief()
                    {
                        TransactionState = req.ConfirmStatus
                    });
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "AI/ChangeStatusTransactionSecured Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}