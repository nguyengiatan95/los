﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models.Affiliate;
using LOS.AppAPI.Models.AG;
using LOS.AppAPI.Models.LanDingPage;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.AG;
using LOS.Common.Extensions;
using LOS.Common.Utils;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class AffiliateController : BaseController
    {
        public static ConcurrentDictionary<object, SemaphoreSlim> locks = new ConcurrentDictionary<object, SemaphoreSlim>();
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAgService _agservice;
        private readonly IMemoryCache _memoryCache;
        private readonly IErp _erpService;

        public AffiliateController(IUnitOfWork unitOfWork, IAgService agservice,
            IMemoryCache memoryCache, IErp erpService)
        {
            _memoryCache = memoryCache;
            this._unitOfWork = unitOfWork;
            this._agservice = agservice;
            _erpService = erpService;
        }

        #region AccessTrade
        //Lấy trạng thái đơn vay
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_ACCESSTRADE")]
        [Route("status_loan_accesstrade")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateCPQLItem>>> GetStatusLoanAccessTrade(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateCPQLItem>>();
            try
            {
                var key = Constants.KEY_CACHE_ACCESSTRADE;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateCPQLItem>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);

                        var lstData = new List<LoanBriefAffiliateCPQLItem>();

                        //lấy danh sách trong bảng loanbrief
                        var lstLoanBrief = _unitOfWork.LoanBriefRepository.Query(x => x.FirstProcessingTime >= entity.from_date
                        && x.FirstProcessingTime <= add_to_date && x.Tid == "at-237", null, false)
                            .Select(LoanBriefAffiliateQualify.ProjectionDetail).ToList();
                        if (lstLoanBrief != null && lstLoanBrief.Count > 0)
                        {
                            foreach (var item in lstLoanBrief)
                            {
                                lstData.Add(new LoanBriefAffiliateCPQLItem()
                                {
                                    LoanBriefId = item.loancredit_id,
                                    AffCode = item.aff_code,
                                    AffSid = item.aff_sid,
                                    CreateDate = item.create_date,
                                    UpdateDate = item.update_date,
                                    ReasonCancelName = item.reason_cancel_name,
                                    UploadFile = item.upload_file,
                                    ValueCheckQlf = item.value_check_qlf,
                                    LeadQualify = item.lead_qualify,
                                    Status = item.status,
                                });
                            }
                        }

                        //lấy danh sách trong bảng pushloantopartner
                        var lstPustLoanToPartner = _unitOfWork.PushLoanToPartnerRepository.Query(x => x.CreateDate >= entity.from_date
                        && x.CreateDate <= add_to_date && x.Tid == "at-237", null, false)
                            .Select(PushLoanToPartnerCPS.ProjectionDetail).ToList();
                        if (lstPustLoanToPartner != null && lstPustLoanToPartner.Count > 0)
                        {
                            foreach (var item in lstPustLoanToPartner)
                            {
                                lstData.Add(new LoanBriefAffiliateCPQLItem()
                                {
                                    LoanBriefId = item.Id,
                                    AffCode = item.AffCode,
                                    AffSid = item.AffSid,
                                    CreateDate = item.CreateDate.Value,
                                    UpdateDate = item.PushDate,
                                    ReasonCancelName = item.Status == (int)StatusPushLoanToPartner.Cancel ? "(Tima Note) Không đủ điều kiện vay" : null,
                                    UploadFile = "NO",
                                    ValueCheckQlf = item.Status == (int)StatusPushLoanToPartner.Disbursement ? 248 : 0,
                                    LeadQualify = item.Status == (int)StatusPushLoanToPartner.Disbursement ? "QL không chứng từ" : string.Empty,
                                    Status = item.Status == (int)StatusPushLoanToPartner.Disbursement ? 1 : item.Status == (int)StatusPushLoanToPartner.Cancel ? 2 : 0
                                });
                            }
                        }

                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanAccessTrade Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        //Lấy trạng thái đơn vay theo campain CPS
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_ACCESSTRADE")]
        [Route("status_loan_accesstrade_cps_xe_may")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateCPSItem>>> GetStatusLoanAccessTradeCpsXeMay(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateCPSItem>>();
            try
            {
                var key = Constants.KEY_CACHE_ACCESSTRADE_CPS_XEMAY;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateCPSItem>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);
                        //Lấy danh sách các đơn vay của accesstrade đổ về los theo ngày cập nhập đơn vay
                        var lstData = new List<LoanBriefAffiliateCPSItem>();

                        //lấy danh sách trong bảng loanbrief
                        var lstLoanBrief = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date
                        && x.CreatedTime <= add_to_date && x.Tid == "at-237-cps-xemay", null, false)
                            .Select(LoanBriefAffiliateCPS.ProjectionDetail).ToList();
                        if (lstLoanBrief != null && lstLoanBrief.Count > 0)
                        {
                            foreach (var item in lstLoanBrief)
                            {
                                lstData.Add(new LoanBriefAffiliateCPSItem()
                                {
                                    LoanBriefId = item.loancredit_id,
                                    AffCode = item.aff_code,
                                    AffSid = item.aff_sid,
                                    Status = item.status,
                                    CreateDate = item.create_date,
                                    UpdateDate = item.update_date,
                                    ReasonCancelName = item.reason_cancel_name
                                });
                            }
                        }
                        //lấy danh sách trong bảng pushloantopartner
                        var lstPustLoanToPartner = _unitOfWork.PushLoanToPartnerRepository.Query(x => x.CreateDate >= entity.from_date
                        && x.CreateDate <= add_to_date && x.Tid == "at-237-cps-xemay", null, false)
                            .Select(PushLoanToPartnerCPS.ProjectionDetail).ToList();
                        if (lstPustLoanToPartner != null && lstPustLoanToPartner.Count > 0)
                        {
                            foreach (var item in lstPustLoanToPartner)
                            {
                                lstData.Add(new LoanBriefAffiliateCPSItem()
                                {
                                    LoanBriefId = item.Id,
                                    AffCode = item.AffCode,
                                    AffSid = item.AffSid,
                                    Status = item.Status.Value,
                                    CreateDate = item.CreateDate.Value,
                                    UpdateDate = item.PushDate,
                                    ReasonCancelName = item.Status == (int)StatusPushLoanToPartner.Cancel ? "(Tima Note) Không đủ điều kiện vay" : null
                                });
                            }

                        }

                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanAccessTradeCpsXeMay Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_ACCESSTRADE")]
        [Route("status_loan_accesstrade_cps_o_to")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateCPSItem>>> GetStatusLoanAccessTradeCpsOto(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateCPSItem>>();
            try
            {
                var key = Constants.KEY_CACHE_ACCESSTRADE_CPS_XEMAY;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateCPSItem>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);
                        //Lấy danh sách các đơn vay của accesstrade đổ về los theo ngày cập nhập đơn vay
                        var lstData = new List<LoanBriefAffiliateCPSItem>();

                        //lấy danh sách trong bảng loanbrief
                        var lstLoanBrief = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date
                        && x.CreatedTime <= add_to_date && x.Tid == "at-237-cps-oto", null, false)
                            .Select(LoanBriefAffiliateCPS.ProjectionDetail).ToList();
                        if (lstLoanBrief != null && lstLoanBrief.Count > 0)
                        {
                            foreach (var item in lstLoanBrief)
                            {
                                lstData.Add(new LoanBriefAffiliateCPSItem()
                                {
                                    LoanBriefId = item.loancredit_id,
                                    AffCode = item.aff_code,
                                    AffSid = item.aff_sid,
                                    Status = item.status,
                                    CreateDate = item.create_date,
                                    UpdateDate = item.update_date,
                                    ReasonCancelName = item.reason_cancel_name
                                });
                            }
                        }
                        //lấy danh sách trong bảng pushloantopartner
                        var lstPustLoanToPartner = _unitOfWork.PushLoanToPartnerRepository.Query(x => x.CreateDate >= entity.from_date
                        && x.CreateDate <= add_to_date && x.Tid == "at-237-cps-oto", null, false)
                            .Select(PushLoanToPartnerCPS.ProjectionDetail).ToList();
                        if (lstPustLoanToPartner != null && lstPustLoanToPartner.Count > 0)
                        {
                            foreach (var item in lstPustLoanToPartner)
                            {
                                lstData.Add(new LoanBriefAffiliateCPSItem()
                                {
                                    LoanBriefId = item.Id,
                                    AffCode = item.AffCode,
                                    AffSid = item.AffSid,
                                    Status = item.Status.Value,
                                    CreateDate = item.CreateDate.Value,
                                    UpdateDate = item.PushDate,
                                    ReasonCancelName = item.Status == (int)StatusPushLoanToPartner.Cancel ? "(Tima Note) Không đủ điều kiện vay" : null
                                });
                            }

                        }

                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanAccessTradeCpsOto Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region MasOffer
        //Lấy trạng thái đơn vay
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_MASOFFER")]
        [Route("status_loan_masoffer")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateCPQLItem>>> GetStatusLoanMasOffer(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateCPQLItem>>();
            try
            {
                var key = Constants.KEY_CACHE_MASOFFER;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateCPQLItem>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);

                        var lstData = new List<LoanBriefAffiliateCPQLItem>();

                        //lấy danh sách trong bảng loanbrief
                        var lstLoanBrief = _unitOfWork.LoanBriefRepository.Query(x => x.FirstProcessingTime >= entity.from_date
                        && x.FirstProcessingTime <= add_to_date && x.Tid == "mo-255", null, false)
                            .Select(LoanBriefAffiliateQualify.ProjectionDetail).ToList();
                        if (lstLoanBrief != null && lstLoanBrief.Count > 0)
                        {
                            foreach (var item in lstLoanBrief)
                            {
                                lstData.Add(new LoanBriefAffiliateCPQLItem()
                                {
                                    LoanBriefId = item.loancredit_id,
                                    AffCode = item.aff_code,
                                    AffSid = item.aff_sid,
                                    CreateDate = item.create_date,
                                    UpdateDate = item.update_date,
                                    ReasonCancelName = item.reason_cancel_name,
                                    UploadFile = item.upload_file,
                                    ValueCheckQlf = item.value_check_qlf,
                                    LeadQualify = item.lead_qualify,
                                    Status = item.status,
                                });
                            }
                        }

                        //lấy danh sách trong bảng pushloantopartner
                        var lstPustLoanToPartner = _unitOfWork.PushLoanToPartnerRepository.Query(x => x.CreateDate >= entity.from_date
                        && x.CreateDate <= add_to_date && x.Tid == "mo-255", null, false)
                            .Select(PushLoanToPartnerCPS.ProjectionDetail).ToList();
                        if (lstPustLoanToPartner != null && lstPustLoanToPartner.Count > 0)
                        {
                            foreach (var item in lstPustLoanToPartner)
                            {
                                lstData.Add(new LoanBriefAffiliateCPQLItem()
                                {
                                    LoanBriefId = item.Id,
                                    AffCode = item.AffCode,
                                    AffSid = item.AffSid,
                                    CreateDate = item.CreateDate.Value,
                                    UpdateDate = item.PushDate,
                                    ReasonCancelName = item.Status == (int)StatusPushLoanToPartner.Cancel ? "(Tima Note) Không đủ điều kiện vay" : null,
                                    UploadFile = "NO",
                                    ValueCheckQlf = item.Status == (int)StatusPushLoanToPartner.Disbursement ? 248 : 0,
                                    LeadQualify = item.Status == (int)StatusPushLoanToPartner.Disbursement ? "QL không chứng từ" : string.Empty,
                                    Status = item.Status == (int)StatusPushLoanToPartner.Disbursement ? 1 : item.Status == (int)StatusPushLoanToPartner.Cancel ? 2 : 0
                                });
                            }
                        }

                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanMasOffer Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Lấy trạng thái đơn vay theo campain CPS
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_MASOFFER")]
        [Route("status_loan_masoffer_cps_xe_may")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateCPSNoUpdateTimeItem>>> GetStatusLoanMasofferCpsXeMay(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateCPSNoUpdateTimeItem>>();
            try
            {
                var key = Constants.KEY_CACHE_MASOFFER_CPS_XEMAY;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateCPSNoUpdateTimeItem>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);
                        //Lấy danh sách các đơn vay của accesstrade đổ về los theo ngày cập nhập đơn vay
                        var lstData = new List<LoanBriefAffiliateCPSNoUpdateTimeItem>();

                        //lấy danh sách trong bảng loanbrief
                        var lstLoanBrief = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date
                        && x.CreatedTime <= add_to_date && x.Tid == "mo-255-cps-xemay", null, false)
                            .Select(LoanBriefAffiliateCPSMasOfferXeMay.ProjectionDetail).ToList();
                        if (lstLoanBrief != null && lstLoanBrief.Count > 0)
                        {
                            foreach (var item in lstLoanBrief)
                            {
                                lstData.Add(new LoanBriefAffiliateCPSNoUpdateTimeItem()
                                {
                                    LoanBriefId = item.LoanBriefId,
                                    AffCode = item.AffCode,
                                    AffSid = item.AffSid,
                                    Status = item.Status,
                                    CreateDate = item.CreateDate,
                                    ReasonCancelName = item.ReasonCancelName
                                });
                            }
                        }
                        //lấy danh sách trong bảng pushloantopartner
                        var lstPustLoanToPartner = _unitOfWork.PushLoanToPartnerRepository.Query(x => x.CreateDate >= entity.from_date
                        && x.CreateDate <= add_to_date && x.Tid == "mo-255-cps-xemay", null, false)
                            .Select(PushLoanToPartnerCPS.ProjectionDetail).ToList();
                        if (lstPustLoanToPartner != null && lstPustLoanToPartner.Count > 0)
                        {
                            foreach (var item in lstPustLoanToPartner)
                            {
                                lstData.Add(new LoanBriefAffiliateCPSNoUpdateTimeItem()
                                {
                                    LoanBriefId = item.Id,
                                    AffCode = item.AffCode,
                                    AffSid = item.AffSid,
                                    Status = item.Status.Value,
                                    CreateDate = item.CreateDate.Value,
                                    ReasonCancelName = item.Status == (int)StatusPushLoanToPartner.Cancel ? "(Tima Note) Không đủ điều kiện vay" : null
                                });
                            }

                        }

                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanAccessTradeCpsXeMay Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region LeadGid
        //Lấy trạng thái đơn vay
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_LEADGID")]
        [Route("status_loan_leadgid")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>> GetStatusLoanLeadGid(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>();
            try
            {
                var key = Constants.KEY_CACHE_LEADGID;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);
                        //Lấy danh sách các đơn vay của leadgid đổ về los theo ngày cập nhập đơn vay
                        var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date && x.CreatedTime <= add_to_date && x.Tid == "lg-5057", null, false).Select(LoanBriefAffiliateDisbursementReasonEng.ProjectionDetail).ToList();
                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanLeadGid Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region SalesDouble
        //Lấy trạng thái đơn vay
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_SALESDOUBLE")]
        [Route("status_loan_salesdouble")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>> GetStatusLoanSalesDouble(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>();
            try
            {
                var key = Constants.KEY_CACHE_SALESDOUBLE;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);
                        //Lấy danh sách các đơn vay của salesdouble đổ về los theo ngày cập nhập đơn vay
                        var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date && x.CreatedTime <= add_to_date && x.Tid == "sd-2596", null, false).Select(LoanBriefAffiliateDisbursementReasonEng.ProjectionDetail).ToList();
                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanSalesDouble Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Jeff
        //Lấy trạng thái đơn vay
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_JEFF")]
        [Route("status_loan_jeff")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>> GetStatusLoanJeff(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>();
            try
            {
                var key = Constants.KEY_CACHE_JEFF;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);
                        //Lấy danh sách các đơn vay của jeff đổ về los theo ngày cập nhập đơn vay
                        var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date && x.CreatedTime <= add_to_date && x.Tid == "jeff-666", null, false).Select(LoanBriefAffiliateDisbursementReasonEng.ProjectionDetail).ToList();
                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanJeff Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_JEFF")]
        [Route("create_loan_jeff")]
        public async Task<ActionResult<DefaultResponse<Meta, object>>> CreateLoanBriefJeff(Affiliate.CreateLoanBriefJeff entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                #region Validate Input
                if (entity == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "NO INPUT INFOMATION");
                    return Ok(def);
                }
                else if (string.IsNullOrEmpty(entity.FullName))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "NOT YET INPORT FULLNAME");
                    return Ok(def);
                }
                else if (string.IsNullOrEmpty(entity.Phone))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "NOT YET INPORT PHONE");
                    return Ok(def);
                }
                else if (!entity.Phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "NOT FORMAT PHONE");
                    return Ok(def);
                }
                else if (string.IsNullOrEmpty(entity.NationalCard))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "NOT YET INPORT NATIONAL CARD");
                    return Ok(def);
                }
                else if (entity.NationalCard.Length != (int)NationalCardLength.CMND_QD && entity.NationalCard.Length != (int)NationalCardLength.CMND
                    && entity.NationalCard.Length != (int)NationalCardLength.CCCD)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "NOT FORMAT NATIONAL CARD");
                    return Ok(def);
                }
                else if (string.IsNullOrEmpty(entity.DistrictName))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "NOT YET INPORT DISTRICT NAME");
                    return Ok(def);
                }
                else if (string.IsNullOrEmpty(entity.ProvinceName))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "NOT YET INPORT PROVINCE NAME");
                    return Ok(def);
                }
                else if (!entity.ProductId.HasValue || entity.ProductId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "NOT YET INPORT PRODUCTID");
                    return Ok(def);
                }
                #endregion

                #region Check khu vực có hỗ trợ không
                var province = await _unitOfWork.ProvinceRepository.Query(x => x.Name.Contains(entity.ProvinceName.ToLower()), null, false)
                    .Select(x => new { IsApply = x.IsApply, ProvinceId = x.ProvinceId }).FirstOrDefaultAsync();
                if (province == null || province.ProvinceId == 0 || province.IsApply != 1)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "NO IN THE SUPPORT AREA");
                    return Ok(def);
                }
                var district = await _unitOfWork.DistrictRepository.Query(x => x.Name.Contains(entity.DistrictName.ToLower()), null, false)
                    .Select(x => new { IsApply = x.IsApply, DistrictId = x.DistrictId }).FirstOrDefaultAsync();
                if (district == null || district.DistrictId == 0 || district.IsApply != 1)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "NO IN THE SUPPORT AREA");
                    return Ok(def);
                }
                #endregion

                #region Check Blacklist, Nhân viên Tima, Đơn đang xử lý
                var checkErp = _erpService.CheckEmployeeTima(entity.NationalCard, entity.Phone);
                if (checkErp != null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "CUSTOMERS ARE EMPLOYEES OF TIMA");
                    return Ok(def);
                }

                //var entityBlackList = new RequestCheckBlackListAg
                //{
                //    FullName = entity.FullName,
                //    NumberCard = entity.NationalCard,
                //    Phone = entity.Phone
                //};

                //var checkBlackList = _agservice.CheckBlacklistAg(entityBlackList);
                //if (checkBlackList != null && checkBlackList.Data == true && checkBlackList.Status == 1)
                //{
                //    def.meta = new Meta(ResponseHelper.CUSTOMER_BLACKLIST_CODE, "CUSTOMERS IN BLACK LIST");
                //    return Ok(def);
                //}

                if (await _unitOfWork.LoanBriefRepository.AnyAsync(x => x.Phone == entity.Phone && x.Status != EnumLoanStatus.CANCELED.GetHashCode()
                && x.Status != EnumLoanStatus.FINISH.GetHashCode()))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "CUSTOMERS IS LOAN PROCESSING");
                    return Ok(def);
                }
                #endregion

                //xử lý tên KH
                if (!string.IsNullOrEmpty(entity.FullName))
                {
                    if (!entity.FullName.IsNormalized())
                        entity.FullName = entity.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                    entity.FullName = entity.FullName.ReduceWhitespace();
                    entity.FullName = entity.FullName.TitleCaseString();
                }

                if (!entity.LoanAmount.HasValue)
                {
                    entity.LoanAmount = 10000000;
                }

                var obj = new LoanBrief();
                var objCus = new Customer();

                #region Customer
                //Check xem customer đã tồn tại hay chưa
                var customerInfo = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == entity.Phone);
                if (customerInfo != null && customerInfo.CustomerId > 0)
                    obj.CustomerId = customerInfo.CustomerId;
                else
                {
                    objCus.CreatedAt = DateTime.Now;
                    objCus.FullName = entity.FullName;
                    objCus.Phone = entity.Phone;
                    objCus.ProvinceId = province.ProvinceId;
                    objCus.DistrictId = district.DistrictId;
                    objCus.NationalCard = entity.NationalCard;
                    _unitOfWork.CustomerRepository.Insert(objCus);
                    _unitOfWork.Save();
                    obj.CustomerId = objCus.CustomerId;
                }
                #endregion

                #region LoanBriefResident
                obj.LoanBriefResident = new LoanBriefResident()
                {
                    ProvinceId = province.ProvinceId,
                    DistrictId = district.DistrictId
                };

                #endregion

                #region LoanBrief
                obj.IsHeadOffice = true;
                obj.CreatedTime = DateTime.Now;
                obj.FullName = entity.FullName;
                obj.Phone = entity.Phone;
                obj.NationalCard = entity.NationalCard;
                obj.LoanAmountFirst = entity.LoanAmount;
                obj.LoanAmount = entity.LoanAmount;
                obj.LoanTime = (int)EnumLoanTime.TwentyMonth;
                obj.ProvinceId = province.ProvinceId;
                obj.DistrictId = district.DistrictId;
                obj.ProductId = entity.ProductId;
                obj.UtmSource = "jeff-api";
                obj.AffCode = "jeff-api";
                obj.Tid = "jeff-api";
                obj.PlatformType = EnumPlatformType.JeffApi.GetHashCode();
                obj.TypeLoanBrief = LoanBriefType.Customer.GetHashCode();
                obj.AffSid = entity.AffSId;

                #endregion

                #region LoanBriefNote
                await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = string.Format("Đơn vay được khởi tạo từ api Jeff"),
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                });
                #endregion

                //insert to db
                await _unitOfWork.LoanBriefRepository.InsertAsync(obj);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = obj.LoanBriefId;
                if (obj.LoanBriefId > 0)
                {
                    //Lưu lại log khi tạo đơn
                    var objLogCreate = new LogInfoCreateLoanbrief()
                    {
                        LoanBriefId = obj.LoanBriefId,
                        FullName = entity.FullName,
                        Phone = entity.Phone,
                        LoanAmount = (long)entity.LoanAmount,
                        LoanTime = (int)EnumLoanTime.TwentyMonth,
                        ProvinceId = province.ProvinceId,
                        DistrictId = district.DistrictId,
                        ProductId = entity.ProductId,
                        PlatformType = EnumPlatformType.JeffApi.GetHashCode(),
                        NationalCard = entity.NationalCard,
                        Tid = "jeff-api",
                        AffCode = "jeff-api",
                        UtmSource = "jeff-api",
                        CreateDate = DateTime.Now,
                        AffSid = entity.AffSId,
                    };
                    _unitOfWork.LogInfoCreateLoanbriefRepository.Insert(objLogCreate);
                    _unitOfWork.Save();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/CreateLoanBriefJeff Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_JEFF")]
        [Route("status_loan_jeff_api")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LoanBriefStatusJeffApi>>>> GetStatusLoanJeffApi(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefStatusJeffApi>>();
            try
            {
                var key = Constants.KEY_CACHE_JEFF_API;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefStatusJeffApi>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);
                        //Lấy danh sách các đơn vay của jeff đổ về los theo ngày cập nhập đơn vay
                        var lstData = await _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date
                        && x.CreatedTime <= add_to_date && x.Tid == "jeff-api" && x.PlatformType == (int)EnumPlatformType.JeffApi, null, false)
                            .Select(LoanBriefStatusJeffApi.ProjectionDetail).ToListAsync();

                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanJeffApi Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #endregion

        #region GoodAff
        //Lấy trạng thái đơn vay
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_GOODAFF")]
        [Route("status_loan_goodaff")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>> GetStatusLoanGoodAff(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>();
            try
            {
                var key = Constants.KEY_CACHE_GOODAFF;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);
                        //Lấy danh sách các đơn vay của jeff đổ về los theo ngày cập nhập đơn vay
                        var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date && x.CreatedTime <= add_to_date && x.Tid == "ga-678", null, false).Select(LoanBriefAffiliateDisbursementReasonEng.ProjectionDetail).ToList();
                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanJeff Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region hoptac.vaytima.com
        //Lấy những đơn đã giải ngân từ aff.tima.vn về
        [HttpGet]
        [AuthorizeToken("APP_API_AFFILIATE_TIMA")]
        [Route("loan_tima_disbursement")]
        public ActionResult<DefaultResponse<Meta, List<StatusLoanBriefAffTima>>> GetLoanTimaDisbursement()
        {
            var def = new DefaultResponse<Meta, List<StatusLoanBriefAffTima>>();
            try
            {
                var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.PlatformType == (int)LOS.Common.Extensions.EnumPlatformType.AffTima
                && x.Tid == "lead-aff" && x.SynchorizeFinance == (int)EnumSynchorizeFinance.LoanNotSynchorizeAff, null, false).Select(StatusLoanBriefAffTima.ProjectionDetail).ToList();
                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetLoanTimaDisbursement Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_TIMA")]
        [Route("create_loan")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> CreateLoan(Affiliate.CreateLoanAffTima req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (!req.Phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không đúng định dạng!");
                    return Ok(def);
                }
                var checkErp = _erpService.CheckEmployeeTima("", req.Phone);
                if (checkErp != null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng là nhân viên Tima!");
                    return Ok(def);
                }
                //Check xem khách hàng có đơn vay nào đang xử lý hay không
                if (_unitOfWork.LoanBriefRepository.Any(x => x.Phone == req.Phone
                && x.Status != EnumLoanStatus.CANCELED.GetHashCode() && x.Status != EnumLoanStatus.FINISH.GetHashCode()))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý!");
                    return Ok(def);
                }

                //Kiểm tra đơn cũ xem có phải là đơn CPQL của MMO không
                //nếu phải kiểm tra xem ngày tạo đã quá 1 tháng chưa
                //chưa được 1 tháng không cho tạo đơn nữa
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == req.Phone, null, false).OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                if (loanInfo != null)
                {
                    //Đơn coldlead có SĐT trên hệ thống không cho tạo đơn nữa
                    if (req.UtmSource == "hoptactima_coldlead")
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đã tồn tại trong hệ thống!");
                        return Ok(def);
                    }
                    else
                    {
                        var checkDate = (DateTime.Now - loanInfo.CreatedTime.Value).TotalDays;
                        if (checkDate < 32)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đã tồn tại trong hệ thống!");
                            return Ok(def);
                        }
                    }
                }
                //xử lý tên KH
                if (!string.IsNullOrEmpty(req.FullName))
                {
                    if (!req.FullName.IsNormalized())
                        req.FullName = req.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                    req.FullName = req.FullName.ReduceWhitespace();
                    req.FullName = req.FullName.TitleCaseString();
                }
                //Check xem khách hàng có trong black list không
                var entityBlackList = new RequestCheckBlackListAg
                {
                    FullName = req.FullName,
                    NumberCard = req.NationalCard,
                    Phone = req.Phone
                };
                //var checkBlackList = _agservice.CheckBlacklistAg(entityBlackList);
                //if (checkBlackList != null && checkBlackList.Data == true && checkBlackList.Status == 1)
                //{
                //    def.meta = new Meta(ResponseHelper.CUSTOMER_BLACKLIST_CODE, "Khách hàng nằm trong Black list!");
                //    return Ok(def);
                //}

                var obj = new LoanBrief();
                var objCus = new Customer();

                //Check xem customer đã tồn tại hay chưa
                var customerInfo = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == req.Phone);
                if (customerInfo != null && customerInfo.CustomerId > 0)
                {
                    //cập nhật thông tin customer
                    customerInfo.CreatedAt = DateTime.Now;
                    customerInfo.FullName = req.FullName;
                    customerInfo.Phone = req.Phone;
                    customerInfo.ProvinceId = req.ProvinceId;
                    customerInfo.DistrictId = req.DistrictId;
                    customerInfo.NationalCard = req.NationalCard;
                    _unitOfWork.CustomerRepository.Update(customerInfo);
                    _unitOfWork.Save();
                    obj.CustomerId = customerInfo.CustomerId;
                }
                else
                {
                    objCus.CreatedAt = DateTime.Now;
                    objCus.FullName = req.FullName;
                    objCus.Phone = req.Phone;
                    objCus.ProvinceId = req.ProvinceId;
                    objCus.DistrictId = req.DistrictId;
                    objCus.NationalCard = req.NationalCard;
                    _unitOfWork.CustomerRepository.Insert(objCus);
                    _unitOfWork.Save();
                    obj.CustomerId = objCus.CustomerId;
                }

                if (req.ProvinceId > 0 || req.DistrictId > 0)
                {
                    obj.LoanBriefResident = new LoanBriefResident()
                    {
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId
                    };
                }
                //Check xem có đúng khu vực không
                //Không đúng thì cho đơn ở trạng thái treo
                if (req.ProvinceId == 0 || req.DistrictId == 0)
                {
                    obj.Status = EnumLoanStatus.CANCELED.GetHashCode();
                    obj.PipelineState = -1;
                    obj.ReasonCancel = 660;
                    obj.FirstProcessingTime = DateTime.Now;
                }
                else
                {
                    obj.Status = EnumLoanStatus.INIT.GetHashCode();
                }

                //gói vay xe máy
                if (req.ProductId == 2)
                {
                    //Nếu là đơn import thì gán mặc định về giỏ autocall_coldlead
                    if (req.TypeLoanBrief == 1)
                    {
                        obj.BoundTelesaleId = 55694; //UserId của autocall_coldlead
                        obj.TeamTelesalesId = (int)EnumTeamTelesales.Other;
                    }
                    else
                    {
                        //gán mặc định về giỏ ctv_autocall
                        obj.BoundTelesaleId = 55684; //UserId của ctv_autocall
                        obj.TeamTelesalesId = (int)EnumTeamTelesales.Other;

                    }
                }

                var note = "";
                //Có mã người giới thiệu
                if (!string.IsNullOrEmpty(req.ReferralCode))
                {
                    //kiểm tra xem mã người giới thiệu có trùng với dữ liệu không
                    int userId;
                    bool check = Int32.TryParse(req.ReferralCode, out userId);
                    if (check)
                    {
                        var resultReferral = _unitOfWork.UserRepository.Query(x => x.UserId == userId, null, false).Select(UserDetail.ProjectionDetail).FirstOrDefault();
                        // nếu có thì gán về cho hub
                        if (resultReferral != null)
                        {
                            if (resultReferral.ListShop != null)
                            {
                                //kiểm tra xem đơn đấy có thuộc khu vực mà HUB đấy hỗ trợ không
                                var checkAreaHub = _unitOfWork.ManagerAreaHubRepository.Query(x => x.ShopId == resultReferral.ListShop.First().ShopId
                                                   && x.ProductId == req.ProductId && x.ProvinceId == req.ProvinceId, null, false).ToList();

                                //nếu cùng thì gán về cho HUB đấy
                                if (checkAreaHub != null && checkAreaHub.Count > 0)
                                {
                                    obj.HubId = resultReferral.ListShop.First().ShopId;
                                    obj.HubEmployeeId = resultReferral.UserId;

                                    // Thêm comment tạo về cho CVKD nào
                                    note += " .Đơn chuyển về cho CVKD: " + resultReferral.FullName;
                                }
                                //đẩy sang hub khác có cùng khu vực hỗ trợ
                                obj.TypeRemarketing = (int)EnumTypeRemarketing.IsAff;
                                obj.BoundTelesaleId = null;
                                obj.TeamTelesalesId = null;
                            }
                        }
                    }
                }

                obj.IsHeadOffice = true;
                obj.CreatedTime = DateTime.Now;
                obj.FullName = req.FullName;
                obj.Phone = req.Phone;
                obj.NationalCard = req.NationalCard;
                obj.LoanAmountFirst = req.LoanAmount;
                obj.LoanAmount = req.LoanAmount;
                obj.LoanTime = req.LoanTime;
                obj.ProvinceId = req.ProvinceId;
                obj.DistrictId = req.DistrictId;
                obj.ProductId = req.ProductId;
                obj.UtmSource = req.UtmSource;
                obj.UtmMedium = req.UtmMedium;
                obj.UtmCampaign = req.UtmCampaign;
                obj.UtmContent = req.UtmContent;
                obj.UtmTerm = req.UtmTerm;
                obj.DomainName = req.DomainName;
                obj.PlatformType = EnumPlatformType.AffTima.GetHashCode();
                obj.AffCode = req.Aff_Code;
                obj.AffSid = req.Aff_Sid;
                obj.Tid = req.TId;
                obj.IsVerifyOtpLdp = req.IsVerifyOtpLDP;
                obj.SynchorizeFinance = (int)EnumSynchorizeFinance.LoanNotSynchorizeAff;
                obj.TypeLoanBrief = LoanBriefType.Customer.GetHashCode();
                obj.LoanBriefNote = new List<LoanBriefNote>();
                obj.LoanBriefNote.Add(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = string.Format("Đơn vay chuyển về từ hoptac.vaytima.com" + note),
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                });

                if (obj.Status == EnumLoanStatus.CANCELED.GetHashCode())
                {
                    obj.LoanBriefNote.Add(new LoanBriefNote()
                    {
                        FullName = "Auto System",
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        Note = string.Format("Đơn bị hủy do không thuộc khu vực hỗ trợ"),
                        Status = 1,
                        CreatedTime = DateTime.Now,
                        ShopId = 3703,
                        ShopName = "Tima",
                    });
                }

                //insert to db
                _unitOfWork.LoanBriefRepository.Insert(obj);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = obj.LoanBriefId;
                if (obj.LoanBriefId > 0)
                {
                    //Lưu lại log khi tạo đơn
                    var objLogCreate = new LogInfoCreateLoanbrief()
                    {
                        LoanBriefId = obj.LoanBriefId,
                        FullName = req.FullName,
                        Phone = req.Phone,
                        LoanAmount = req.LoanAmount,
                        LoanTime = req.LoanTime,
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        ProductId = req.ProductId,
                        UtmSource = req.UtmSource,
                        UtmMedium = req.UtmMedium,
                        UtmCampaign = req.UtmCampaign,
                        UtmContent = req.UtmContent,
                        UtmTerm = req.UtmTerm,
                        DomainName = req.DomainName,
                        PlatformType = EnumPlatformType.AffTima.GetHashCode(),
                        NationalCard = req.NationalCard,
                        AffCode = req.Aff_Code,
                        AffSid = req.Aff_Sid,
                        Tid = req.TId,
                        IsVerifyOtpLdp = req.IsVerifyOtpLDP,
                        CreateDate = DateTime.Now,
                    };
                    _unitOfWork.LogInfoCreateLoanbriefRepository.Insert(objLogCreate);

                    if (obj.HubId.HasValue && obj.HubEmployeeId.HasValue)
                    {
                        //thêm log chia đơn cho CVKD
                        var objLogDistributionUser = new LogDistributionUser()
                        {
                            LoanbriefId = obj.LoanBriefId,
                            UserId = obj.HubEmployeeId.Value,
                            TypeDistribution = (int)TypeDistributionLog.CVKD,
                            CreatedAt = DateTime.Now,
                            CreatedBy = 1,
                        };
                        _unitOfWork.LogDistributionUserRepository.Insert(objLogDistributionUser);
                    }
                    _unitOfWork.Save();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/CreateLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_TIMA")]
        [Route("update_synchorize_loanbrief")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> UpdateSynchorizeLoanBrief(int loanBriefId)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.GetById(loanBriefId);
                if (loanBrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không tồn tại!");
                    return Ok(def);
                }

                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    SynchorizeFinance = (int)EnumSynchorizeFinance.LoanDoneSynchorizeAff
                });
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/UpdateSynchorizeLoanBrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);

            }
            return Ok(def);
        }

        #endregion

        #region Lấy ra số ảnh đã upload từ LDP
        [HttpGet]
        [Route("get_list_loanbrief_files_ldp")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefFiles>>> GetListLoanBriefFilesLDP(int loanBriefId)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefFiles>>();
            try
            {
                //Lấy ra danh sách các hình ảnh được upload từ ldp
                var lstData = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanBriefId && x.Status == 1 && x.IsDeleted == 0
                && x.UserId == 0, null, false).ToList();
                def.meta = new Meta(200, "success");
                def.data = lstData;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetListLoanBriefFilesLDP Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Akamuro
        //Lấy trạng thái đơn vay
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_AKAMURO")]
        [Route("status_loan_akamuro")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>> GetStatusLoanAkamuro(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>();
            try
            {
                var key = Constants.KEY_CACHE_AKAMURO;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateDisbursementReasonEng>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);
                        //Lấy danh sách các đơn vay của AKAMURO đổ về los theo ngày cập nhập đơn vay
                        var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date && x.CreatedTime <= add_to_date && x.Tid == "ak-47", null, false).Select(LoanBriefAffiliateDisbursementReasonEng.ProjectionDetail).ToList();
                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanAkamuro Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Tạo đơn vay cho đối tác thứ 3
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_CREATE_LOAN")]
        [Route("create_loan_affiliate")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> CreateLoanAffiliate(Affiliate.CreateLoanAffiliate req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (!req.Phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không đúng định dạng!");
                    return Ok(def);
                }

                //Check xem khách hàng có đơn vay nào đang xử lý hay không
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == req.Phone, null, false).OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                if (loanInfo != null)
                {
                    if (loanInfo.Status != EnumLoanStatus.CANCELED.GetHashCode() && loanInfo.Status != EnumLoanStatus.FINISH.GetHashCode())
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý!");
                        return Ok(def);
                    }
                }
                //xử lý tên KH
                if (!string.IsNullOrEmpty(req.FullName))
                {
                    req.FullName = req.FullName.ReduceWhitespace();
                    req.FullName = req.FullName.TitleCaseString();
                }
                var obj = new LoanBrief();
                var objCus = new Customer();
                //Check xem customer đã tồn tại hay chưa
                var customerInfo = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == req.Phone);
                if (customerInfo != null && customerInfo.CustomerId > 0)
                    obj.CustomerId = customerInfo.CustomerId;
                else
                {
                    objCus.CreatedAt = DateTime.Now;
                    objCus.FullName = req.FullName;
                    objCus.Phone = req.Phone;
                    objCus.ProvinceId = req.ProvinceId;
                    objCus.DistrictId = req.DistrictId;
                    objCus.NationalCard = req.NationalCard;
                    _unitOfWork.CustomerRepository.Insert(objCus);
                    _unitOfWork.Save();
                    obj.CustomerId = objCus.CustomerId;
                }

                if (req.ProvinceId > 0 || req.DistrictId > 0)
                {
                    obj.LoanBriefResident = new LoanBriefResident()
                    {
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId
                    };
                }

                //Check xem có đúng khu vực không
                //Không đúng thì cho đơn ở trạng thái treo
                if (req.ProvinceId == 0 || req.DistrictId == 0 || req.ProductId == 0)
                {
                    obj.Status = EnumLoanStatus.CANCELED.GetHashCode();
                    obj.PipelineState = -1;
                    obj.ReasonCancel = 660;
                }
                else
                {
                    obj.Status = EnumLoanStatus.INIT.GetHashCode();
                }


                obj.IsHeadOffice = true;
                obj.CreatedTime = DateTime.Now;
                obj.FullName = req.FullName;
                obj.Phone = req.Phone;
                obj.NationalCard = req.NationalCard;
                obj.LoanAmountFirst = req.LoanAmount;
                obj.LoanAmount = req.LoanAmount;
                obj.LoanTime = req.LoanTime;
                obj.ProvinceId = req.ProvinceId;
                obj.DistrictId = req.DistrictId;
                obj.ProductId = req.ProductId;
                obj.UtmSource = req.UtmSource;
                obj.UtmMedium = req.UtmMedium;
                obj.UtmCampaign = req.UtmCampaign;
                obj.UtmContent = req.UtmContent;
                obj.UtmTerm = req.UtmTerm;
                obj.DomainName = req.DomainName;
                obj.PlatformType = EnumPlatformType.Affiliate.GetHashCode();
                obj.AffCode = req.Aff_Code;
                obj.AffSid = req.Aff_Sid;
                obj.Tid = req.TId;
                obj.TypeLoanBrief = LoanBriefType.Customer.GetHashCode();
                obj.LoanBriefNote = new List<LoanBriefNote>();
                obj.LoanBriefNote.Add(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = string.Format("Đơn vay được khởi tạo bởi Affiliate"),
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                });

                //insert to db
                _unitOfWork.LoanBriefRepository.Insert(obj);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = obj.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/CreateLoanAffiliate Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region aff.tima.vn
        //Lấy ra tất cả các đơn vay được tạo từ nguồn MMO
        [HttpPost]
        [Route("get_loanbrief_mmo_tima")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAllMMOTimaItem>>> GetLoanBriefMMOTima(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAllMMOTimaItem>>();
            try
            {
                // cộng thêm 1 ngày để select
                DateTime add_to_date = entity.to_date.AddDays(1);
                //Lấy danh sách các đơn vay của MMOTima trong bảng loanbrief
                var lstData = new List<LoanBriefAllMMOTimaItem>();
                var lstLoanBrief = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date
                && x.CreatedTime <= add_to_date && x.PlatformType == (int)EnumPlatformType.MMOTima
                && x.TypeRemarketing != (int)EnumTypeRemarketing.IsTopUp
                && x.UtmSource != "form_rmkt_dpd", null, false)
                    .Select(LoanBriefAllMMOTima.ProjectionDetail).OrderByDescending(x => x.loanbriefId).ToList();
                if (lstLoanBrief != null && lstLoanBrief.Count > 0)
                {
                    foreach (var item in lstLoanBrief)
                    {
                        lstData.Add(new LoanBriefAllMMOTimaItem()
                        {
                            LoanBriefId = item.loanbriefId,
                            FullName = item.full_name,
                            CityName = item.city_name,
                            DistrictName = item.district_name,
                            ProductName = item.product_name,
                            AffCode = item.aff_code,
                            Status = item.status,
                            ReasonCancel = item.reason_cancel,
                            CreateDate = item.create_date,
                            DateDisbursement = item.date_disbursement,
                            LoanAmmount = item.loan_ammount,
                            ProductId = item.productId,
                            CityId = item.cityId,
                            DistrictId = item.districtId,
                        });
                    }

                }
                //lấy danh sách trong bảng pushloantopartner
                var lstPustLoanToPartner = _unitOfWork.PushLoanToPartnerRepository.Query(x => x.CreateDate >= entity.from_date
                && x.CreateDate <= add_to_date && x.Tid == "aff-mmo-tima", null, false)
                    .Select(PushLoanToPartnerDetail.ProjectionDetail).ToList();
                if (lstPustLoanToPartner != null && lstPustLoanToPartner.Count > 0)
                {
                    foreach (var item in lstPustLoanToPartner)
                    {
                        lstData.Add(new LoanBriefAllMMOTimaItem()
                        {
                            LoanBriefId = "HĐ-" + item.Id,
                            FullName = item.CustomerName,
                            CityName = item.ProvinceName,
                            DistrictName = item.DistrictName,
                            ProductName = "Gói vay xe máy",
                            AffCode = item.AffCode,
                            Status = item.Status == (int)StatusPushLoanToPartner.Cancel ? 0 : item.Status == (int)StatusPushLoanToPartner.Processing ? 3 : 2,
                            ReasonCancel = item.Status == (int)StatusPushLoanToPartner.Cancel ? "(Tima Note) Không đủ điều kiện vay" : null,
                            CreateDate = item.CreateDate,
                            DateDisbursement = item.SynchronizedDate,
                            LoanAmmount = 10000000,
                            ProductId = (int)EnumProductCredit.MotorCreditType_CC,
                            CityId = item.ProvinceId != null ? item.ProvinceId.Value : 0,
                            DistrictId = item.DistrictId != null ? item.DistrictId.Value : 0,
                        });
                    }
                }

                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetLoanBriefMMOTima Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Lấy ra tất cả đơn vay được tạo từ nguồn MMO theo AffCode
        [HttpPost]
        [Route("get_loanbrief_mmo_tima_by_affcode")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAllMMOTimaItem>>> GetLoanBriefMMOTimaByAffCode(Affiliate.AffiliateSearchByAffCode entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAllMMOTimaItem>>();
            try
            {
                // cộng thêm 1 ngày để select
                DateTime add_to_date = entity.to_date.AddDays(1);
                //Lấy danh sách các đơn vay của MMOTima
                //Lấy danh sách các đơn vay của MMOTima trong bảng loanbrief
                var lstData = new List<LoanBriefAllMMOTimaItem>();
                var lstLoanBrief = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date
                && x.CreatedTime <= add_to_date && x.PlatformType == (int)EnumPlatformType.MMOTima
                && x.AffCode == entity.aff_code && x.TypeRemarketing != (int)EnumTypeRemarketing.IsTopUp
                && x.UtmSource != "form_rmkt_dpd", null, false)
                    .Select(LoanBriefAllMMOTima.ProjectionDetail).OrderByDescending(x => x.loanbriefId).ToList();
                if (lstLoanBrief != null && lstLoanBrief.Count > 0)
                {
                    foreach (var item in lstLoanBrief)
                    {
                        lstData.Add(new LoanBriefAllMMOTimaItem()
                        {
                            LoanBriefId = item.loanbriefId,
                            FullName = item.full_name,
                            CityName = item.city_name,
                            DistrictName = item.district_name,
                            ProductName = item.product_name,
                            AffCode = item.aff_code,
                            Status = item.status,
                            ReasonCancel = item.reason_cancel,
                            CreateDate = item.create_date,
                            DateDisbursement = item.date_disbursement,
                            LoanAmmount = item.loan_ammount,
                            ProductId = item.productId,
                            CityId = item.cityId,
                            DistrictId = item.districtId,
                        });
                    }

                }
                //lấy danh sách trong bảng pushloantopartner
                var lstPustLoanToPartner = _unitOfWork.PushLoanToPartnerRepository.Query(x => x.CreateDate >= entity.from_date
                && x.CreateDate <= add_to_date && x.Tid == "aff-mmo-tima" && x.AffCode == entity.aff_code, null, false)
                    .Select(PushLoanToPartnerDetail.ProjectionDetail).ToList();
                if (lstPustLoanToPartner != null && lstPustLoanToPartner.Count > 0)
                {
                    foreach (var item in lstPustLoanToPartner)
                    {
                        lstData.Add(new LoanBriefAllMMOTimaItem()
                        {
                            LoanBriefId = "HĐ-" + item.Id,
                            FullName = item.CustomerName,
                            CityName = item.ProvinceName,
                            DistrictName = item.DistrictName,
                            ProductName = "Gói vay xe máy",
                            AffCode = item.AffCode,
                            Status = item.Status == (int)StatusPushLoanToPartner.Cancel ? 0 : item.Status == (int)StatusPushLoanToPartner.Processing ? 3 : 2,
                            ReasonCancel = item.Status == (int)StatusPushLoanToPartner.Cancel ? "(Tima Note) Không đủ điều kiện vay" : null,
                            CreateDate = item.CreateDate,
                            DateDisbursement = item.SynchronizedDate,
                            LoanAmmount = 10000000,
                            ProductId = (int)EnumProductCredit.MotorCreditType_CC,
                            CityId = item.ProvinceId != null ? item.ProvinceId.Value : 0,
                            DistrictId = item.DistrictId != null ? item.DistrictId.Value : 0,
                        });
                    }
                }

                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetLoanBriefMMOTimaByAffCode Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Lấy ra đơn giải ngân mà chưa được đồng bộ về aff
        [HttpGet]
        [Route("get_loanbrief_disbursement_not_synchorize_aff")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefDisbursementNotSynchorizeAff>>> GetLoanBriefDisbursementNotSynchorizeAff()
        {
            var def = new DefaultResponse<Meta, List<LoanBriefDisbursementNotSynchorizeAff>>();
            try
            {
                //Lấy ra các đơn giải ngân của aff mà chưa đồng bộ về aff
                var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.PlatformType == (int)EnumPlatformType.MMOTima
                && x.SynchorizeFinance.GetValueOrDefault(0) == 0
                && (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH), null, false).Select(LoanBriefDisbursementNotSynchorizeAff.ProjectionDetail).ToList();
                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetLoanBriefDisbursementNotSynchorizeAff Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_synchorize_loanbrief_aff")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> UpdateSynchorizeLoanBriefAff(int loanBriefId)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.GetById(loanBriefId);
                if (loanBrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không tồn tại!");
                    return Ok(def);
                }

                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    SynchorizeFinance = (int)EnumSynchorizeFinance.SynchorizeLoanDisbursementAff
                });
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/UpdateSynchorizeLoanBriefAff Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);

            }
            return Ok(def);
        }

        [HttpPost]
        [Route("get_loanbrief_mmo_tima_cpql_by_affcode")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAllMMOTimaCPQL>>> GetLoanBriefMMOTimaCPQLByAffCode(Affiliate.AffiliateSearchByAffCode entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAllMMOTimaCPQL>>();
            try
            {
                // cộng thêm 1 ngày để select
                DateTime add_to_date = entity.to_date.AddDays(1);
                //Lấy danh sách các đơn vay của MMOTima
                var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date
                && x.CreatedTime <= add_to_date && x.PlatformType == (int)EnumPlatformType.MMOTimaCPQL
                && x.AffCode == entity.aff_code, null, false).Select(LoanBriefAllMMOTimaCPQL.ProjectionDetail).OrderByDescending(x => x.loanbriefId).ToList();
                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetLoanBriefMMOTimaCPQLByAffCode Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("get_loanbrief_mmo_tima_cpql")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAllMMOTimaCPQL>>> GetLoanBriefMMOTimaCPQLByAffCode(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAllMMOTimaCPQL>>();
            try
            {
                // cộng thêm 1 ngày để select
                DateTime add_to_date = entity.to_date.AddDays(1);
                //Lấy danh sách các đơn vay của MMOTima
                var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date
                && x.CreatedTime <= add_to_date && x.PlatformType == (int)EnumPlatformType.MMOTimaCPQL, null, false).Select(LoanBriefAllMMOTimaCPQL.ProjectionDetail).OrderByDescending(x => x.loanbriefId).ToList();
                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetLoanBriefMMOTimaCPQLByAffCode Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //lấy ra danh sách những đơn đạt qlf mà chưa được đồng bộ về aff
        [HttpGet]
        [Route("get_loanbrief_qlf_not_synchorize_aff")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefQLFNotSynchorizeAff>>> GetLoanBriefQLFNotSynchorizeAff()
        {
            var def = new DefaultResponse<Meta, List<LoanBriefQLFNotSynchorizeAff>>();
            try
            {
                //Lấy ra các đơn giải ngân của aff mà chưa đồng bộ về aff
                var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.PlatformType == (int)EnumPlatformType.MMOTimaCPQL
                && x.SynchorizeFinance.GetValueOrDefault(0) == 0 && x.ValueCheckQualify == 248
                && (x.Status == (int)EnumLoanStatus.CANCELED || x.Status == (int)EnumLoanStatus.DISBURSED
                || x.Status == (int)EnumLoanStatus.FINISH), null, false).Select(LoanBriefQLFNotSynchorizeAff.ProjectionDetail).ToList();


                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetLoanBriefQLFNotSynchorizeAff Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_synchorize_aff_loanbrief")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> UpdateSynchorizeAffLoanBrief(int loanBriefId, int synchorize)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.GetById(loanBriefId);
                if (loanBrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không tồn tại!");
                    return Ok(def);
                }

                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    SynchorizeFinance = Convert.ToInt16(synchorize)
                });
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/UpdateSynchorizeAffLoanBrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);

            }
            return Ok(def);
        }

        //lấy danh sách đơn vay bán cho đối tác
        [HttpPost]
        [Route("get_push_loan_to_partner")]
        public ActionResult<DefaultResponse<Meta, List<PushLoanToPartnerDetail>>> GetPushLoanToPartner(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<PushLoanToPartnerDetail>>();
            try
            {
                // cộng thêm 1 ngày để select
                DateTime add_to_date = entity.to_date.AddDays(1);

                var lstData = _unitOfWork.PushLoanToPartnerRepository.Query(x => x.CreateDate >= entity.from_date
                && x.CreateDate <= add_to_date, null, false).Select(PushLoanToPartnerDetail.ProjectionDetail).OrderByDescending(x => x.CreateDate).ToList();
                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetPushLoanToPartner Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("get_loan_create_affiliate_api")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LoanBriefCreateAffiliateApi>>>> GetLoanCreateAffiliateApi(Affiliate.RequestGetLoanCreateAffiliateApi entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefCreateAffiliateApi>>();
            try
            {
                var lstData = await _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.FromDate
                && x.CreatedTime <= entity.ToDate.AddDays(1) && x.Tid == entity.TId, null, false)
                    .Select(LoanBriefCreateAffiliateApi.ProjectionDetail)
                    .OrderByDescending(x => x.LoanBriefId).ToListAsync();
                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetLoanCreateAffiliateApi Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #endregion

        #region TrustingSocial
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_TRUSTINGSOCIAL")]
        [Route("check_lead")]
        public ActionResult<CustomResponse.CheckLeadResponseTrustingSocial<object>> CheckLeadTrustingSocial(Affiliate.ResquestCheckLead entity)
        {
            var def = new CustomResponse.CheckLeadResponseTrustingSocial<object>();
            try
            {
                //check request có bị null không
                if (entity == null)
                {
                    def.status = "rejected";
                    def.message = "is null object request";
                    def.date_time = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
                    return Ok(def);
                }
                //check số điện thoại
                if (string.IsNullOrEmpty(entity.phone_number))
                {
                    def.status = "rejected";
                    def.message = "phone number is null";
                    def.date_time = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
                    return Ok(def);
                }

                //check định dạng số điện thoại
                if (!entity.phone_number.ValidPhone())
                {
                    def.status = "rejected";
                    def.message = "phone number not format";
                    def.date_time = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
                    return Ok(def);
                }
                //check số chứng minh thư
                if (!string.IsNullOrEmpty(entity.national_id))
                {
                    //check định dạng số chứng minh thư
                    if (entity.national_id.Length != 9 && entity.national_id.Length != 12)
                    {
                        def.status = "rejected";
                        def.message = "national not format";
                        def.date_time = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
                        return Ok(def);
                    }
                    else
                    {
                        //Check xem khách hàng có trong black list không
                        var entityBlackList = new RequestCheckBlackListAg
                        {
                            FullName = string.Empty,
                            NumberCard = entity.national_id,
                            Phone = entity.phone_number
                        };

                        var checkBlackList = _agservice.CheckBlacklistAg(entityBlackList);
                        if (checkBlackList.Data)
                        {
                            def.status = "rejected";
                            def.message = "customer in blacklist";
                            def.date_time = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
                            return Ok(def);
                        }
                    }
                }
                //Check xem khách hàng có đơn vay nào đang xử lý hay không
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == entity.phone_number, null, false).OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                if (loanInfo != null)
                {
                    def.status = "rejected";
                    def.message = "duplicate records";
                    def.date_time = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
                    return Ok(def);
                }
                def.status = "passed";
                def.message = "qualified customers";
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/CheckLeadTrustingSocial Exception");
                def.status = "rejected";
                def.message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE;
            }
            def.date_time = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_TRUSTINGSOCIAL")]
        [Route("send_lead")]
        public ActionResult<CustomResponse.SendLeadResponseTrustingSocial<object>> SendLeadTrustingSocial(Affiliate.ResquestSendLead entity)
        {
            var def = new CustomResponse.SendLeadResponseTrustingSocial<object>();
            try
            {
                //Check xem khách hàng có trong black list không
                var entityBlackList = new RequestCheckBlackListAg
                {
                    FullName = string.Empty,
                    NumberCard = entity.national_id,
                    Phone = entity.phone_number
                };

                var checkBlackList = _agservice.CheckBlacklistAg(entityBlackList);
                if (checkBlackList.Data)
                {
                    def.status = "rejected";
                    def.message = "customer in blacklist";
                    def.date_time = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
                    return Ok(def);
                }

                //Check xem khách hàng có đơn vay nào đang xử lý hay không
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == entity.phone_number, null, false).OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                if (loanInfo != null)
                {
                    def.status = "dedup";
                    def.message = "duplicate records";
                    def.date_time = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
                    return Ok(def);
                }

                //Check xem có đúng khu vực không
                if (Convert.ToInt32(entity.province) != 1 && Convert.ToInt32(entity.province) != 79 && Convert.ToInt32(entity.province) != 27)
                {
                    def.status = "rejected";
                    def.message = "no support area";
                    def.date_time = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
                    return Ok(def);
                }

                //Check khoản vay
                if (Convert.ToInt32(entity.product_code) != 2 && Convert.ToInt32(entity.product_code) != 8)
                {
                    def.status = "rejected";
                    def.message = "no support product";
                    def.date_time = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
                    return Ok(def);
                }

                //xử lý tên KH
                if (!string.IsNullOrEmpty(entity.full_name))
                {
                    entity.full_name = entity.full_name.ReduceWhitespace();
                    entity.full_name = entity.full_name.TitleCaseString();
                }
                var objLoan = new LoanBrief();
                var objCus = new Customer();
                // Insert vào bảng Customer
                objCus.CreatedAt = DateTime.Now;
                objCus.FullName = entity.full_name;
                objCus.Phone = entity.phone_number;
                objCus.ProvinceId = Convert.ToInt32(entity.province);
                objCus.NationalCard = entity.national_id;
                _unitOfWork.CustomerRepository.Insert(objCus);
                _unitOfWork.Save();
                objLoan.CustomerId = objCus.CustomerId;

                //insert vào bảng LoanBriefResident
                if (!string.IsNullOrEmpty(entity.province))
                {
                    objLoan.LoanBriefResident = new LoanBriefResident()
                    {
                        ProvinceId = Convert.ToInt32(entity.province)
                    };
                }
                //insert vào bảng LoanBrief
                objLoan.IsHeadOffice = true;
                objLoan.CreatedTime = DateTime.Now;
                objLoan.FullName = entity.full_name;
                objLoan.Phone = entity.phone_number;
                objLoan.NationalCard = entity.national_id;
                objLoan.ProvinceId = Convert.ToInt32(entity.province);
                objLoan.ProductId = Convert.ToInt32(entity.product_code);
                objLoan.UtmSource = entity.source;
                objLoan.Status = EnumLoanStatus.INIT.GetHashCode();
                objLoan.PlatformType = EnumPlatformType.TrustingSocial.GetHashCode();
                objLoan.AffCode = "trustingsocial";
                objLoan.AffSid = entity.ts_lead_id.ToString();
                objLoan.Tid = "ts-18";
                objLoan.TypeLoanBrief = LoanBriefType.Customer.GetHashCode();
                objLoan.LoanBriefNote = new List<LoanBriefNote>();
                objLoan.LoanBriefNote.Add(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = "Đơn vay được khởi tạo từ TrustingSocial",
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                });
                //insert to db
                _unitOfWork.LoanBriefRepository.Insert(objLoan);
                _unitOfWork.Save();
                def.status = "success";
                def.message = "create lead success";
                def.data = new Affiliate.ObjectDataTrustingSocial
                {
                    bank_lead_id = objLoan.LoanBriefId.ToString()
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/SendLeadTrustingSocial Exception");
                def.status = "rejected";
                def.message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE;
            }
            def.date_time = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
            return Ok(def);
        }
        #endregion

        #region Rentracks
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_RENTRACKS")]
        [Route("status_loan_rentracks_xe_may")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateRentracksXeMay>>> GetStatusLoanRentracksXeMay(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateRentracksXeMay>>();
            try
            {
                var key = Constants.KEY_CACHE_RENTRACKS_XEMAY;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateRentracksXeMay>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);
                        var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date && x.CreatedTime <= add_to_date && x.Tid == "rt-cps-xemay", null, false).Select(LoanBriefAffiliateRentracksXeMay.ProjectionDetail).ToList();
                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanRentracksXeMay Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_RENTRACKS")]
        [Route("status_loan_rentracks_oto")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateRentracksOto>>> GetStatusLoanRentracksOto(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateRentracksOto>>();
            try
            {
                var key = Constants.KEY_CACHE_RENTRACKS_OTO;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateRentracksOto>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);
                        var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date && x.CreatedTime <= add_to_date && x.Tid == "rt-cps-oto", null, false).Select(LoanBriefAffiliateRentracksOto.ProjectionDetail).ToList();
                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanRentracksOto Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_RENTRACKS")]
        [Route("status_loan_rentracks_cpql")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateCPQLItem>>> GetStatusLoanRentracksCPQL(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateCPQLItem>>();
            try
            {
                var key = Constants.KEY_CACHE_RENTRACKS_CPQL;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateCPQLItem>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);

                        var lstData = new List<LoanBriefAffiliateCPQLItem>();

                        //lấy danh sách trong bảng loanbrief
                        var lstLoanBrief = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date
                        && x.CreatedTime <= add_to_date && x.Tid == "rt-cpql-xemay", null, false)
                            .Select(LoanBriefAffiliateQualify.ProjectionDetail).ToList();
                        if (lstLoanBrief != null && lstLoanBrief.Count > 0)
                        {
                            foreach (var item in lstLoanBrief)
                            {
                                lstData.Add(new LoanBriefAffiliateCPQLItem()
                                {
                                    LoanBriefId = item.loancredit_id,
                                    AffCode = item.aff_code,
                                    AffSid = item.aff_sid,
                                    CreateDate = item.create_date,
                                    UpdateDate = item.update_date,
                                    ReasonCancelName = item.reason_cancel_name,
                                    UploadFile = item.upload_file,
                                    ValueCheckQlf = item.value_check_qlf,
                                    LeadQualify = item.lead_qualify,
                                    Status = item.status,
                                });
                            }
                        }

                        //lấy danh sách trong bảng pushloantopartner
                        var lstPustLoanToPartner = _unitOfWork.PushLoanToPartnerRepository.Query(x => x.CreateDate >= entity.from_date
                        && x.CreateDate <= add_to_date && x.Tid == "rt-cpql-xemay", null, false)
                            .Select(PushLoanToPartnerCPS.ProjectionDetail).ToList();
                        if (lstPustLoanToPartner != null && lstPustLoanToPartner.Count > 0)
                        {
                            foreach (var item in lstPustLoanToPartner)
                            {
                                lstData.Add(new LoanBriefAffiliateCPQLItem()
                                {
                                    LoanBriefId = item.Id,
                                    AffCode = item.AffCode,
                                    AffSid = item.AffSid,
                                    CreateDate = item.CreateDate.Value,
                                    UpdateDate = item.PushDate,
                                    ReasonCancelName = item.Status == (int)StatusPushLoanToPartner.Cancel ? "(Tima Note) Không đủ điều kiện vay" : null,
                                    UploadFile = "NO",
                                    ValueCheckQlf = item.Status == (int)StatusPushLoanToPartner.Disbursement ? 248 : 0,
                                    LeadQualify = item.Status == (int)StatusPushLoanToPartner.Disbursement ? "QL không chứng từ" : string.Empty,
                                    Status = item.Status == (int)StatusPushLoanToPartner.Disbursement ? 1 : item.Status == (int)StatusPushLoanToPartner.Cancel ? 2 : 0
                                });
                            }
                        }

                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanRentracksCPQL Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Dinos
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_DINOS")]
        [Route("status_loan_dinos")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateDinos>>> GetStatusLoanDinos(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateDinos>>();
            try
            {
                var key = Constants.KEY_CACHE_DINOS;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefAffiliateDinos>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        DateTime add_to_date = entity.to_date.AddDays(1);
                        var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date && x.CreatedTime <= add_to_date
                        && (x.Tid == "dinos-cps-oto" || x.Tid == "dinos-cps-xemay"), null, false).Select(LoanBriefAffiliateDinos.ProjectionDetail).ToList();
                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanDinos Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Lưu log Gọi Postback của đối tác
        [HttpPost]
        [Route("insert_log_call_api_postback")]
        public ActionResult<DefaultResponse<Meta, object>> InsertLogCallApiPosbackAffiliate(Affiliate.RequestInsertLogCallApiPosbackAffiliate entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                var objLogCallApi = new LogCallApi()
                {
                    ActionCallApi = entity.ActionCallApi,
                    CreatedAt = entity.CreatedAt,
                    LinkCallApi = entity.LinkCallApi,
                    Input = entity.Input,
                    LoanBriefId = entity.LoanBriefId,
                    Status = entity.Status,
                    TokenCallApi = entity.TokenCallApi
                };
                var result = _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = result.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/InsertLogCallApiPosbackAffiliate Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_log_call_api_postback")]
        public ActionResult<DefaultResponse<Meta, object>> UpdateLogCallApiPosbackAffiliate(Affiliate.ResquestUpdateLogCallApiPosbackAffiliate entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                //Kiểm tra xem có tồn tại log này không
                if (_unitOfWork.LogCallApiRepository.Any(x => x.Id == entity.Id))
                {
                   _unitOfWork.LogCallApiRepository.Update(x => x.Id == entity.Id, x => new LogCallApi()
                    {
                        Output = entity.Output,
                        ModifyAt = entity.ModifyAt,
                        Status = entity.Status,
                    });
                    _unitOfWork.Save();

                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = entity.Id;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tồn tại mã Log này");
                    def.data = null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/UpdateLogCallApiPosbackAffiliate Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Danh sách thành phố và quận/huyện
        [HttpGet]
        [Route("get_province")]
        public async Task<ActionResult<DefaultResponse<Meta, List<ProvinceLendingOnline>>>> GetProvince()
        {
            var def = new DefaultResponse<Meta, List<ProvinceLendingOnline>>();
            try
            {
                var data = await _unitOfWork.ProvinceRepository.Query(x => x.IsApply == 1, null, false).Select(ProvinceLendingOnline.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetProvince Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_district")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DistrictLendingOnline>>>> GetDistrict(int provinceId)
        {
            var def = new DefaultResponse<Meta, List<DistrictLendingOnline>>();
            try
            {
                var data = await _unitOfWork.DistrictRepository.Query(x => x.IsApply == 1 && x.ProvinceId == provinceId, null, false).Select(DistrictLendingOnline.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetDistrict Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_ward")]
        public async Task<ActionResult<DefaultResponse<Meta, List<WardLendingOnline>>>> GetWard(int districtId)
        {
            var def = new DefaultResponse<Meta, List<WardLendingOnline>>();
            try
            {
                var data = await _unitOfWork.WardRepository.Query(x => x.DistrictId == districtId, null, false).Select(WardLendingOnline.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/GetWard Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #endregion

        #region KBFINA
        [HttpPost]
        [AuthorizeToken("API_APP_AFFILIATE_KBFINA")]
        [Route("create_loan_kbfina")]
        public async Task<ActionResult<DefaultResponse<Meta, Affiliate.ResponseCreateLoanKbFina>>> CreateLoanKbFina(Affiliate.ResquetCreateLoanKbFina entity)
        {
            var def = new DefaultResponse<Meta, Affiliate.ResponseCreateLoanKbFina>();
            try
            {
                if (string.IsNullOrEmpty(entity.FullName))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền tên người vay!");
                    return Ok(def);
                }
                //Kiểm tra số đt
                if (string.IsNullOrEmpty(entity.Phone))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền số điện thoại!");
                    return Ok(def);
                }

                if (!entity.Phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không đúng định dạng!");
                    return Ok(def);
                }

                if (!string.IsNullOrEmpty(entity.TransactionId))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền TransactionId!");
                    return Ok(def);
                }

                //kiểm tra có truyền CMND không
                if (!string.IsNullOrEmpty(entity.NationalCard))
                {
                    //Kiểm tra định dạng của CMND
                    if (!ConvertExtensions.IsNumber(entity.NationalCard))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền đúng định dạng chứng minh thư");
                        return Ok(def);
                    }
                    if (entity.NationalCard.Length != (int)NationalCardLength.CMND_QD && entity.NationalCard.Length != (int)NationalCardLength.CMND && entity.NationalCard.Length != (int)NationalCardLength.CCCD)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền đúng định dạng chứng minh thư");
                        return Ok(def);
                    }
                }

                //check xem có đúng thành phố hỗ trợ không
                if (entity.ProvinceId > 0)
                {
                    if (!_unitOfWork.ProvinceRepository.Any(x => x.ProvinceId == entity.ProvinceId && x.IsApply == 1))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Tỉnh/ Thành phố Tima không hỗ trợ");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền Tỉnh/Thành phố!");
                    return Ok(def);
                }
                //check xem có đúng quận/huyện hỗ trợ không
                if (entity.DistrictId > 0)
                {
                    if (!_unitOfWork.DistrictRepository.Any(x => x.ProvinceId == entity.ProvinceId && x.DistrictId == entity.DistrictId && x.IsApply == 1))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Quận/Huyện Tima không hỗ trợ");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền Quận/Huyện!");
                    return Ok(def);
                }
                //Check nhân viên Tima
                var checkErp = _erpService.CheckEmployeeTima(entity.NationalCard, entity.Phone);
                if (checkErp != null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng là nhân viên Tima!");
                    return Ok(def);
                }

                //xử lý tên KH
                if (!string.IsNullOrEmpty(entity.FullName))
                {
                    entity.FullName = entity.FullName.ReduceWhitespace();
                    entity.FullName = entity.FullName.TitleCaseString();
                }

                #region Black list
                //Check xem khách hàng có trong black list không
                var entityBlackList = new RequestCheckBlackListAg
                {
                    FullName = entity.FullName,
                    NumberCard = entity.NationalCard,
                    Phone = entity.Phone
                };

                var checkBlackList = _agservice.CheckBlacklistAg(entityBlackList);

                if (checkBlackList != null && checkBlackList.Data && checkBlackList.Status == 1)
                {
                    def.meta = new Meta(ResponseHelper.CUSTOMER_BLACKLIST_CODE, "Khách hàng nằm trong Black list!");
                    return Ok(def);
                }
                #endregion

                //Kiểm tra xem có đơn vay đang xử lý không
                if (_unitOfWork.LoanBriefRepository.Any(x => (x.Phone == entity.Phone)
                 && x.Status != EnumLoanStatus.CANCELED.GetHashCode() && x.Status != EnumLoanStatus.FINISH.GetHashCode()))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý!");
                    return Ok(def);
                }

                var objLoan = new LoanBrief();
                var objCus = new Customer();

                #region Customer
                var objCustomerInfo = 0;
                //Check xem customer đã tồn tại hay chưa
                if (!string.IsNullOrEmpty(entity.NationalCard))
                    objCustomerInfo = await _unitOfWork.CustomerRepository.Query(x => x.Phone == entity.Phone || x.NationalCard == entity.NationalCard, null, false).Select(x => x.CustomerId).FirstOrDefaultAsync();
                else
                    objCustomerInfo = await _unitOfWork.CustomerRepository.Query(x => x.Phone == entity.Phone, null, false).Select(x => x.CustomerId).FirstOrDefaultAsync();
                if (objCustomerInfo > 0)
                    objLoan.CustomerId = objCustomerInfo;
                else
                {
                    objCus.CreatedAt = DateTime.Now;
                    objCus.FullName = entity.FullName;
                    objCus.Phone = entity.Phone;
                    objCus.ProvinceId = entity.ProvinceId;
                    objCus.DistrictId = entity.DistrictId;
                    objCus.WardId = entity.WardId;
                    objCus.NationalCard = entity.NationalCard;
                    await _unitOfWork.CustomerRepository.InsertAsync(objCus);
                    _unitOfWork.Save();
                    objLoan.CustomerId = objCus.CustomerId;
                }
                #endregion

                #region LoanBriefResident
                objLoan.LoanBriefResident = new LoanBriefResident()
                {
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId
                };
                #endregion

                #region LoanBrief
                objLoan.Status = EnumLoanStatus.INIT.GetHashCode();
                objLoan.IsHeadOffice = true;
                objLoan.CreatedTime = DateTime.Now;
                objLoan.FullName = entity.FullName;
                objLoan.Phone = entity.Phone;
                objLoan.NationalCard = entity.NationalCard;
                objLoan.LoanAmountFirst = entity.LoanAmount ?? 10000000;
                objLoan.LoanAmount = entity.LoanAmount ?? 10000000;
                objLoan.LoanTime = entity.LoanTime ?? 12;
                objLoan.ProvinceId = entity.ProvinceId;
                objLoan.DistrictId = entity.DistrictId;
                objLoan.WardId = entity.WardId;
                objLoan.ProductId = entity.ProductId ?? (int)EnumProductCredit.MotorCreditType_CC;
                objLoan.DomainName = "KBFINA";
                objLoan.PlatformType = EnumPlatformType.KBFINA.GetHashCode();
                objLoan.TypeLoanBrief = LoanBriefType.Customer.GetHashCode();
                objLoan.AffSid = entity.TransactionId;
                objLoan.LoanBriefNote = new List<LoanBriefNote>();
                objLoan.LoanBriefNote.Add(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = "Đơn vay được khởi tạo từ đối tác KBFINA",
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                });
                #endregion

                #region insert to db
                await _unitOfWork.LoanBriefRepository.InsertAsync(objLoan);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                var data = new Affiliate.ResponseCreateLoanKbFina()
                {
                    TransactionId = objLoan.AffSid,
                    CreateDate = objLoan.CreatedTime.Value.DateTime,
                    DistrictName = objLoan.District != null ? objLoan.District.Name : null,
                    FullName = objLoan.FullName,
                    LoanAmount = objLoan.LoanAmount,
                    LoanBriefId = objLoan.LoanBriefId,
                    LoanTime = objLoan.LoanTime,
                    ProductName = Description.GetDescription((EnumProductCredit)objLoan.ProductId),
                    ProvinceName = objLoan.Province != null ? objLoan.Province.Name : null,
                    ReasonCancel = objLoan.ReasonCancelNavigation != null ? objLoan.ReasonCancelNavigation.Reason : null,
                    StatusName = Description.GetDescription((EnumLoanStatus)objLoan.Status),
                    StatusCode = objLoan.Status,
                };
                def.data = data;
                #endregion

                #region LogInfoCreateLoanbrief
                var objLogCreate = new LogInfoCreateLoanbrief()
                {
                    LoanBriefId = objLoan.LoanBriefId,
                    FullName = objLoan.FullName,
                    Phone = objLoan.Phone,
                    NationalCard = objLoan.NationalCard,
                    LoanAmount = (long)objLoan.LoanAmount,
                    LoanTime = objLoan.LoanTime,
                    ProvinceId = objLoan.ProvinceId,
                    DistrictId = objLoan.DistrictId,
                    DomainName = objLoan.DomainName,
                    PlatformType = EnumPlatformType.KBFINA.GetHashCode(),
                    UtmSource = objLoan.UtmSource,
                    UtmMedium = objLoan.UtmMedium,
                    UtmCampaign = objLoan.UtmCampaign,
                    UtmContent = objLoan.UtmContent,
                    UtmTerm = objLoan.UtmTerm,
                    CreateDate = DateTime.Now,
                };
                await _unitOfWork.LogInfoCreateLoanbriefRepository.InsertAsync(objLogCreate);
                _unitOfWork.Save();
                #endregion LogInfoCreateLoanbrief

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/CreateLoanKbFina Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("API_APP_AFFILIATE_KBFINA")]
        [Route("loan_status_kbfina")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefOfKbFinaCPS>>> LoanStatusKbFina(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefOfKbFinaCPS>>();
            try
            {
                var key = Constants.KEY_CACHE_KBFINA;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, List<LoanBriefOfKbFinaCPS>>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        //Lấy danh sách các đơn vay của kbfina đổ về los theo ngày cập nhập đơn vay
                        var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date && x.CreatedTime <= entity.to_date.AddDays(1) && x.PlatformType == (int)EnumPlatformType.KBFINA, null, false).Select(LoanBriefOfKbFinaCPS.ProjectionDetail).ToList();
                        def.meta = new Meta(200, "success");
                        def.data = lstData;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/LoanStatusKbFina Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region GTV
        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_GTV")]
        [Route("status_loan_gtv_cps")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefAffiliateGtv>>> GetStatusLoanGTVCPS(Affiliate.AffiliateSearchDefault entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefAffiliateGtv>>();
            try
            {
                // cộng thêm 1 ngày để select
                DateTime add_to_date = entity.to_date.AddDays(1);
                //Lấy danh sách các đơn vay của gtv đổ về los theo ngày cập nhập đơn vay
                var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime >= entity.from_date && x.CreatedTime <= add_to_date
                && x.Tid == "gtv-9999", null, false).Select(LoanBriefAffiliateGtv.ProjectionDetail).ToList();
                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Affiliate/GetStatusLoanGTVCPS Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion
    }
}