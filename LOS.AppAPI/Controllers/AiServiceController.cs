﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using LOS.AppAPI.Helpers;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using LOS.AppAPI.Service;
using Newtonsoft.Json;
using LOS.AppAPI.Models.AI;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Formats.Jpeg;
using LOS.AppAPI.DTOs.Appraiser;
using LOS.AppAPI.Models;
using LOS.AppAPI.Models.AiServiceDemo;
using LOS.AppAPI.Validators;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class AiServiceController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceAI _aiService;
        private readonly IProductPrice _productPriceService;


        public AiServiceController(IUnitOfWork unitOfWork, IServiceAI serviceAI, IProductPrice productPriceService)
        {
            this._unitOfWork = unitOfWork;
            _aiService = serviceAI;
            _productPriceService = productPriceService;
        }

        #region Đoc thông tin CMT
        [HttpPost]
        [AuthorizeToken("APP_API_TIMA_TECH")]
        [Route("ekyc_national_card")]
        public async Task<ActionResult<DefaultResponse<Meta, EkycNationalCard.Card>>> EkycNationalCard(IFormFile image)
        {
            var def = new DefaultResponse<Meta, EkycNationalCard.Card>();
            try
            {
                if (image == null)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }

                var extension = Path.GetExtension(image.FileName);
                var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                if (allowedExtensions.Contains(extension.ToLower()))
                {
                    var result = _aiService.EkycNationalCard(await image.GetBytes(), image.FileName);
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = result;
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ảnh không đúng định dạng!");
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "AiService/EkycNationalCard Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Ekyc Liveness
        [HttpPost]
        [AuthorizeToken("APP_API_TIMA_TECH")]
        [Route("CheckLivenessBase64")]
        public IActionResult CheckLivenessBase64([FromBody] CheckLivenessBase64Req req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.selfie != null && req.files.Count >= 20)
                {

                    string selfieImage = "";
                    byte[] imageBytesSelfie = Convert.FromBase64String(req.selfie);
                    //resize if needed                                         
                    using (var image = Image.Load(imageBytesSelfie))
                    {
                        if (image.Width >= image.Height) // landscape
                        {
                            if (image.Width > 1280)
                            {
                                image.Mutate(x => x.Resize(1280, image.Height / (image.Width / 1280)));
                                selfieImage = image.ToBase64String(JpegFormat.Instance);
                            }
                            else
                            {
                                selfieImage = req.selfie;
                            }
                        }
                        else // portrait
                        {
                            if (image.Width > 720)
                            {
                                image.Mutate(x => x.Resize(720, image.Height / (image.Width / 720)));
                                selfieImage = image.ToBase64String(JpegFormat.Instance);
                            }
                            else
                            {
                                selfieImage = req.selfie;
                            }
                        }
                    }
                    // action images
                    List<string> actionImages = new List<string>();
                    foreach (var file in req.files)
                    {
                        var fileBase64 = file;
                        if (fileBase64.StartsWith("data:image/jpeg;base64,"))
                            fileBase64 = fileBase64.Replace("data:image/jpeg;base64,", "");
                        byte[] imageBytes = Convert.FromBase64String(fileBase64);
                        //resize if needed                                         
                        using (var image = Image.Load(imageBytes))
                        {
                            if (image.Width >= image.Height) // landscape
                            {
                                if (image.Width > 1280)
                                {
                                    image.Mutate(x => x.Resize(1280, image.Height / (image.Width / 1280)));
                                    actionImages.Add(image.ToBase64String(JpegFormat.Instance));
                                }
                                else
                                {
                                    actionImages.Add(file);
                                }
                            }
                            else // portrait
                            {
                                if (image.Width > 720)
                                {
                                    image.Mutate(x => x.Resize(720, image.Height / (image.Width / 720)));
                                    actionImages.Add(image.ToBase64String(JpegFormat.Instance));
                                }
                                else
                                {
                                    actionImages.Add(file);
                                }
                            }
                        }
                    }
                    if (!String.IsNullOrEmpty(selfieImage) && actionImages.Count >= 20)
                    {
                        try
                        {
                            var result = _aiService.CheckLivenessDemo(selfieImage, actionImages);
                            bool IsSuccess = false;
                            if (result != null && result.StatusCode == 200)
                            {
                                if (!result.Result.IsFake.Value && result.Result.IsLiveAction.Value && result.Result.IsMatchFace.Value)
                                    IsSuccess = true;
                                if (IsSuccess)
                                {
                                    def.data = IsSuccess;
                                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                    return Ok(def);
                                }
                                else
                                {

                                    if (result.Result.IsFake.Value)
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Xác thực thất bại. Vui lòng thử lại");
                                    else if (!result.Result.IsLiveAction.Value)
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Hình ảnh xác thực không liên tiếp nhau. Vui lòng thử lại");
                                    else if (!result.Result.IsMatchFace.Value)
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ảnh selfie và hình ảnh trong video không giống nhau. Vui lòng thử lại");
                                    return Ok(def);

                                }
                            }
                            else
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Có lỗi xảy ra trong quá trình xác thực. Vui lòng thử lại");
                                return Ok(def);
                            }

                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, "AiService/Check Liveness Exception");
                            def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Check Liveness/Check Liveness Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Định giá xe
        [HttpPost]
        [AuthorizeToken("APP_API_TIMA_TECH")]
        [Route("get_price_moto")]
        public ActionResult<DefaultResponse<Meta, ProductPriceRes>> GetPriceMoto([FromBody] ProductDetail req)
        {
            var def = new DefaultResponse<Meta, ProductPriceRes>();
            try
            {
                if (req.IdBrand <= 0 || string.IsNullOrEmpty(req.Name) || req.Year <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }

                //Giá xe từ AI
                var result = _productPriceService.GetPriceCarAIDemo(req);
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = result;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "AiService/GetPriceMoto Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Chấm điểm Credit Scoring
        [HttpPost]
        [AuthorizeToken("APP_API_TIMA_TECH")]
        [Route("credit_scoring_demo")]
        public async Task<ActionResult<DefaultResponse<Meta, CreditScoringDemoAiResponse>>> CreditScoringDemo(CreditScoringDemoRequest entity)
        {
            var def = new DefaultResponse<Meta, CreditScoringDemoAiResponse>();
            try
            {
                var modelState = await new CreditScoringDemoValidator().ValidateAsync(entity);
                if (modelState.IsValid)
                {
                    var product = await _unitOfWork.ProductRepository.Query(x => x.Id == entity.ProductId, null, false).Select(x => new
                    {
                        ShortName = x.ShortName,
                        Year = x.Year
                    }).FirstOrDefaultAsync();
                    var productPrice = _productPriceService.GetPriceCarAIDemo(new ProductDetail()
                    {
                        Id = entity.ProductId,
                        IdBrand = entity.BrandId,
                        ShortName = product?.ShortName,
                        Year = product?.Year
                    });
                    if (productPrice != null && productPrice.predicted_price > 0)
                    {
                        var result = _aiService.CreditScoringDemo(new CreditScoringDemoAiRequest()
                        {
                            LoanAmountExpertiseAI = productPrice.predicted_price * 1000000,
                            JobId = entity.JobId,
                            TypeIncomeName = Enum.IsDefined(typeof(EnumTypeReceivingMoney), entity.TypeIncomeId) ?
                                            Description.GetDescription((EnumTypeReceivingMoney)entity.TypeIncomeId) : "",
                            TotalMoney = entity.TotalMoney,
                            IsLivingTogether = entity.IsLivingTogether,
                            Salary = entity.Salary,
                            Gender = entity.Gender,
                            ProductID = entity.ProductCredit,
                            FullName = entity.FullName,
                            CardNumber = entity.CardNumber,
                            Phone = entity.Phone,
                            IsMarried = entity.IsMarried
                        });
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = result;
                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không lấy được định giá xe");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, modelState.Errors.FirstOrDefault()?.ToString());
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "AiService/CreditScoringDemo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Lấy đữ liệu fraud
        [HttpPost]
        [AuthorizeToken("APP_API_TIMA_TECH")]
        [Route("fraud_detection_demo")]
        public async Task<ActionResult<DefaultResponse<Meta, FraudDetectionDemoAiResponse>>> FraudDetectionDemo([FromForm] FraudDetectionDemoRequest entity)
        {
            var def = new DefaultResponse<Meta, FraudDetectionDemoAiResponse>();
            try
            {
                var modelState = await new FraudDetectionDemoValidator().ValidateAsync(entity);
                if (modelState.IsValid)
                {
                    var result = _aiService.FraudDetectionDemo(entity);
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = result;
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, modelState.Errors.FirstOrDefault()?.ToString());
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "AiService/FraudDetectionDemo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion
    }
}
