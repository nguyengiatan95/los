﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Xml;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using LOS.AppAPI.DTOs.App;
using LOS.AppAPI.DTOs.Appraiser;
using LOS.AppAPI.DTOs.LMS;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Models.App;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.LmsService;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.Common.Utils;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.Services.Services.Loanbrief;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Processing;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AppraiserController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _baseConfig;
        private readonly IWebHostEnvironment environment;
        private readonly IProductPrice _productPriceService;
        private readonly ILmsService _lmsService;
        private readonly IDocumentService _documentService;
        private ILoanbriefV2Service _loanbriefService;

        public AppraiserController(IUnitOfWork unitOfWork, IConfiguration configuration, IWebHostEnvironment environment, 
            IProductPrice productPriceService, ILmsService lmsService, IDocumentService documentService, ILoanbriefV2Service loanbriefService)
        {
            this._unitOfWork = unitOfWork;
            this._baseConfig = configuration;
            this.environment = environment ?? throw new ArgumentNullException(nameof(environment));
            _productPriceService = productPriceService;
            _lmsService = lmsService;
            _documentService = documentService;
            _loanbriefService = loanbriefService;
        }

        [HttpGet]
        [Route("get_product_review")]
        public async Task<ActionResult<DefaultResponse<Meta, List<ProductReviewDetailAPI>>>> GetProductReviewAsync([FromQuery] int loanbriefId, [FromQuery] int productId, [FromQuery] int IdType = 0)
        {
            var def = new DefaultResponse<Meta, List<ProductReviewDetailAPI>>();
            try
            {
                var product = _unitOfWork.ProductRepository.Query(x => x.Id == productId, null, false).Select(ProductDetail.ProjectionDetail).FirstOrDefault();
                if (product != null)
                {

                    var query = _unitOfWork.ProductReviewRepository.Query(x => x.Status > 0, null, false, x => x.ProductReviewResult)
                      .OrderBy(x => x.OrderNo).Include(x => x.ProductReviewResult).Select(x => new ProductReviewDetailAPI()
                      {
                          Id = x.Id,
                          Name = x.Name,
                          ParentId = x.ParentId,
                          Status = x.Status,
                          State = x.State,
                          IsCancel = x.IsCancel,
                          IdType = x.ProductReviewDetail.FirstOrDefault(k => k.ProductTypeId == product.IdType) != null ? x.ProductReviewDetail.FirstOrDefault(k => k.ProductTypeId == product.IdType).ProductTypeId : 0,
                          IsCheck = x.ProductReviewResult.FirstOrDefault(k => k.LoanBriefId == loanbriefId && k.ProductId == productId) != null ? x.ProductReviewResult.FirstOrDefault(k => k.LoanBriefId == loanbriefId && k.ProductId == productId).IsCheck : null,
                          MoneyDiscount = (x.ProductReviewDetail.FirstOrDefault(k => k.ProductTypeId == IdType) != null
                                            && x.ProductReviewDetail.FirstOrDefault(k => k.ProductTypeId == IdType).MoneyDiscount > 0)
                            ? x.ProductReviewDetail.FirstOrDefault(k => k.ProductTypeId == IdType).MoneyDiscount
                            : 0 // (x.ProductReviewDetail.FirstOrDefault(k => k.ProductCreditId == IdType) != null
                                //         && x.ProductReviewDetail.FirstOrDefault(k => k.ProductCreditId == IdType).PecentDiscount > 0
                                //         && x.ProductReviewDetail.FirstOrDefault(k => k.ProductCreditId == IdType).ProductCredit != null
                                //         && x.ProductReviewDetail.FirstOrDefault(k => k.ProductCreditId == IdType).ProductCredit.Price > 0)
                                //     ? (long)x.ProductReviewDetail.FirstOrDefault(k => k.ProductCreditId == IdType).PecentDiscount.Value * x.ProductReviewDetail.FirstOrDefault(k => k.ProductCreditId == IdType).ProductCredit.Price.Value / 100
                                //     : 0, 
                      });

                    def.data = await query.ToListAsync();
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Appraiser/GetProductReview Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("product_review")]
        public async Task<ActionResult<DefaultResponse<Meta>>> UpdateProductReviewAsync(ProductReviewReq model)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (!ModelState.IsValid)
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "");
                    return Ok(def);
                }

                if (model.loanCreditId > 0 && model.ListProductResultReview != null && model.ListProductResultReview.Count > 0)
                {
                    var userId = GetUserId();
                    var user = _unitOfWork.UserRepository.GetById(userId);
                    var loanbrief = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == model.loanCreditId && x.HubEmployeeId == user.UserId, null, false).Select(x => new
                    {

                        ProductCreditId = x.ProductId,
                        LoanBriefId = x.LoanBriefId,
                        ProductId = x.LoanBriefProperty != null ? x.LoanBriefProperty.ProductId : 0,
                        ResidentType = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType : 0,
                        ProductDetailId = x.ProductDetailId,
                        TypeRemarketing = x.TypeRemarketing,
                        ReMarketingLoanBriefId = x.ReMarketingLoanBriefId,
                        IsReborrow = x.IsReborrow,
                        Ltv = x.Ltv,

                    }).FirstOrDefault();
                    if (loanbrief != null && loanbrief.ProductId > 0)
                    {

                        if (loanbrief.ProductCreditId == (int)EnumProductCredit.MotorCreditType_CC
                            || loanbrief.ProductCreditId == (int)EnumProductCredit.MotorCreditType_KCC
                            || loanbrief.ProductCreditId == (int)EnumProductCredit.MotorCreditType_KGT)
                        {
                            //kiểm tra có đúng gói xe máy không
                            if (loanbrief.ResidentType.GetValueOrDefault(0) == 0)
                            {
                                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Đơn vay chưa chọn hình thức sở hữu nhà. Không thể thẩm định được xe.");
                                return Ok(def);
                            }
                            if (loanbrief.ProductDetailId.GetValueOrDefault(0) == 0)
                            {
                                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Chưa có thông tin chi tiết gói vay.");
                                return Ok(def);
                            }

                            var product = _unitOfWork.ProductRepository.Query(x => x.Id == loanbrief.ProductId, null, false).Select(ProductDetail.ProjectionDetail).FirstOrDefault();
                            //tính giá sản phẩm sau thẩm định
                            decimal originalPrice = 0L;
                            decimal priceAI = 0L;
                            if (product != null)
                            {
                                //Giá xe từ AI
                                priceAI = _productPriceService.GetPriceCarAI(product, loanbrief.LoanBriefId);
                                //so sánh giá của AI và AG
                                if (priceAI > 0)
                                    originalPrice = priceAI;

                                if (originalPrice == 0)
                                {
                                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, "Xảy ra lỗi khi lấy định giá xe");
                                    return Ok(def);
                                }
                                _unitOfWork.ProductReviewResultRepository.Delete(x => x.LoanBriefId == model.loanCreditId);
                                foreach (var item in model.ListProductResultReview)
                                {
                                    _unitOfWork.ProductReviewResultRepository.Insert(new DAL.EntityFramework.ProductReviewResult()
                                    {
                                        LoanBriefId = model.loanCreditId,
                                        ProductId = product.Id,
                                        IsCheck = item.IsCheck,
                                        ProductReviewId = item.ProductReviewId,
                                        CreateDate = DateTime.Now
                                    });
                                }
                                long totalPriceDiscount = 0;
                                var listInfoResult = _productPriceService.GetInfoAppraiser(loanbrief.LoanBriefId, product.IdType);
                                if (listInfoResult != null && listInfoResult.Count > 0)
                                {
                                    foreach (var item in listInfoResult)
                                    {
                                        if (item.MoneyDiscount > 0)
                                            totalPriceDiscount += item.MoneyDiscount.Value;
                                        else if (item.PecentDiscount > 0)
                                            totalPriceDiscount += (long)(originalPrice * item.PecentDiscount.Value / 100);
                                    }
                                }
                                originalPrice = (long)originalPrice - totalPriceDiscount;

                                var typeRemarketingLtv = 0;
                                //kiểm tra nếu là đơn topup
                                var totalMoneyDebtCurrent = 0L; // tổng số tiền đang nợ
                                if (loanbrief.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp && loanbrief.ReMarketingLoanBriefId > 0)
                                {

                                    var resultCheckTopup = await _loanbriefService.CheckCanTopup(loanbrief.ReMarketingLoanBriefId.Value);
                                    if (resultCheckTopup.IsCanTopup)
                                    {
                                        totalMoneyDebtCurrent = (long)resultCheckTopup.CurrentDebt;
                                    }
                                    else
                                    {
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không đủ điều kiện topup");
                                        return Ok(def);
                                    }
                                    //var parentLoan = _unitOfWork.LoanBriefRepository.GetById(loanbrief.ReMarketingLoanBriefId);
                                    //if (parentLoan != null && parentLoan.LmsLoanId > 0)
                                    //{
                                    //    var lstPayment = _lmsService.GetPaymentLoanById(parentLoan.LmsLoanId.Value);
                                    //    if (lstPayment != null && lstPayment.Status == 1 && lstPayment.Data != null && lstPayment.Data.Loan != null && lstPayment.Data.Loan.TotalMoneyCurrent > 0)
                                    //        totalMoneyDebtCurrent = lstPayment.Data.Loan.TotalMoneyCurrent;
                                    //}
                                    typeRemarketingLtv = (int)TypeRemarketingLtv.TopUp;
                                }
                                if (loanbrief.IsReborrow.GetValueOrDefault(false))
                                    typeRemarketingLtv = (int)TypeRemarketingLtv.Reborrow;
                                var totalMoney = 0L;
                                if (typeRemarketingLtv == (int)TypeRemarketingLtv.TopUp)
                                {
                                    totalMoney = Common.Utils.ProductPriceUtils.GetMaxPriceTopup(loanbrief.ProductCreditId.Value, originalPrice, totalMoneyDebtCurrent, loanbrief.ResidentType.Value);
                                }
                                else
                                {
                                    var percentLtv = loanbrief.Ltv;
                                    if (percentLtv.GetValueOrDefault(0) == 0)
                                        percentLtv = Common.Utils.ProductPriceUtils.GetPercentLtv(loanbrief.ProductCreditId.Value, typeRemarketingLtv);
                                    totalMoney = Common.Utils.ProductPriceUtils.GetMaxPriceV3(loanbrief.ProductCreditId.Value, originalPrice,
                                            loanbrief.ResidentType.Value, loanbrief.ProductDetailId.Value, 0, totalMoneyDebtCurrent, typeRemarketingLtv, percentLtv.Value);
                                }
                                //Thêm dữ liệu vào bảng ProductReviewResultDetail
                                _unitOfWork.ProductReviewResultDetailRepository.Insert(new DAL.EntityFramework.ProductReviewResultDetail()
                                {
                                    LoanBriefId = model.loanCreditId,
                                    ProductId = product.Id,
                                    UserId = user.UserId,
                                    GroupUserId = user.GroupId,
                                    PriceReview = totalMoney,
                                    CreateDate = DateTime.Now
                                });

                                //cập nhật giá sau thẩm dịnh
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                                {
                                    LoanAmountExpertise = originalPrice,
                                    LoanAmountExpertiseAi = priceAI,
                                    LoanAmountExpertiseLast = totalMoney
                                });

                                // Thêm comment
                                var note = new LoanBriefNote
                                {
                                    LoanBriefId = loanbrief.LoanBriefId,
                                    Note = string.Format("APP: Thẩm định xe máy HĐ-{0} với số tiền: {1}đ", loanbrief.LoanBriefId, totalMoney.ToString("#,##")),
                                    FullName = user.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.VehicleAssessment.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = user.UserId,
                                    //ShopId = model.shopId
                                };
                                if (loanbrief.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                                {
                                    note.Note = string.Format("APP: Thẩm định đơn Topup xe máy HĐ-{0} với số tiền: {1}đ", loanbrief.LoanBriefId, totalMoney.ToString("#,##"));
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(note);
                                _unitOfWork.Save();
                                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            }
                            else
                            {
                                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tìm thấy thông tin xe");
                                return Ok(def);
                            }
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Chỉ thẩm định đối với gói vay xe máy");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        return Ok(def);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Appraiser/UpdateProductReview Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_checklist")]
        public ActionResult<DefaultResponse<Meta, List<GroupName>>> GetCheckList([FromQuery] int loanbriefId)
        {
            var def = new DefaultResponse<Meta, List<GroupName>>();
            try
            {
                var loanbrief = _unitOfWork.LoanBriefRepository.GetById(loanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    //kiểm tra đã có check list trong db chưa
                    var checklist = _unitOfWork.CoordinatorCheckListRepository.Query(x => x.LoanBriefId.Value == loanbrief.LoanBriefId, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                    if (checklist != null && !string.IsNullOrEmpty(checklist.ContentCheckList))
                    {
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GroupName>>(checklist.ContentCheckList);
                        return Ok(def);
                    }
                    else
                    {
                        var data = ReadXMLCheckList(loanbrief);
                        if (data != null)
                        {
                            def.data = data;
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            return Ok(def);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Xảy ra lỗi khi đọc dữ liệu từ file XML");
                            return Ok(def);
                        }

                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Appraiser/GetCheckList Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_checklist")]
        public ActionResult<DefaultResponse<Meta>> AddCheckList(CheckListQuestionReq model)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (!ModelState.IsValid)
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "");
                    return Ok(def);
                }
                if (model.ContentCheckList != null && model.ContentCheckList.Count > 0)
                {
                    var userId = GetUserId();
                    var user = _unitOfWork.UserRepository.GetById(userId);
                    if (user != null)
                    {
                        var loanbrief = _unitOfWork.LoanBriefRepository.GetById(model.LoanBriefId);
                        if (loanbrief != null && loanbrief.LoanBriefId > 0)
                        {
                            var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(model.ContentCheckList);
                            var entity = new CoordinatorCheckList()
                            {
                                UserId = user.UserId,
                                ContentCheckList = jsonInput,
                                LoanBriefId = model.LoanBriefId,
                                CreatedAt = DateTime.Now,
                                GroupId = user.GroupId
                            };
                            _unitOfWork.CoordinatorCheckListRepository.Insert(entity);
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            return Ok(def);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                            return Ok(def);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Appraiser/GetCheckList Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #region thay đổi trạng thái thẩm định
        [HttpPost]
        [Route("update_appraiser")]
        public ActionResult<DefaultResponse<Meta>> UpdateAppraiser(AppraiserReq model)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (!ModelState.IsValid)
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "");
                    return Ok(def);
                }

                var userId = GetUserId();
                if (userId == 0 || !Enum.IsDefined(typeof(EnumFieldSurveyState), model.Status) || !Enum.IsDefined(typeof(EnumFieldSurveyType), model.TypeAppraiser))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //kiểm tra trạng thái thẩm định
                if (model.LoanbriefId > 0)
                {
                    var loanbrief = _unitOfWork.LoanBriefRepository.GetById(model.LoanbriefId);
                    if (loanbrief != null && loanbrief.LoanBriefId > 0)
                    {
                        //kiểm tra nhóm user

                        var user = _unitOfWork.UserRepository.GetById(userId);
                        if (user != null && user.UserId > 0)
                        {
                            if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode() || user.GroupId == EnumGroupUser.FieldSurvey.GetHashCode())
                            {
                                //kiểm tra trạng thái tiếp theo
                                if (model.TypeAppraiser == EnumFieldSurveyType.Home.GetHashCode())
                                {
                                    if ((loanbrief.EvaluationHomeState == (int)EnumFieldSurveyState.DoingAppraised && model.Status == (int)EnumFieldSurveyState.NotAppraised)
                                     || (loanbrief.EvaluationHomeState == (int)EnumFieldSurveyState.CompleteAppraised
                                         && (model.Status == (int)EnumFieldSurveyState.NotAppraised || model.Status == (int)EnumFieldSurveyState.DoingAppraised)))
                                    {
                                        def.meta = new Meta(405, "Vui lòng liên hệ quản trị để trở lại bước trước!");
                                        return Ok(def);
                                    }
                                }
                                else
                                {
                                    if ((loanbrief.EvaluationCompanyState == (int)EnumFieldSurveyState.DoingAppraised && model.Status == (int)EnumFieldSurveyState.NotAppraised)
                                        || (loanbrief.EvaluationCompanyState == (int)EnumFieldSurveyState.CompleteAppraised
                                            && (model.Status == (int)EnumFieldSurveyState.NotAppraised || model.Status == (int)EnumFieldSurveyState.DoingAppraised)))
                                    {
                                        def.meta = new Meta(405, "Vui lòng liên hệ quản trị để trở lại bước trước!");
                                        return Ok(def);
                                    }

                                }

                                // Thêm comment
                                var note = new LoanBriefNote
                                {
                                    LoanBriefId = loanbrief.LoanBriefId,
                                    FullName = user.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = user.UserId,
                                    //ShopId = user.UserShop != null ? user.UserShop.First().ShopId : 0
                                };
                                if (model.Status == (int)EnumFieldSurveyState.DoingAppraised)
                                    note.ActionComment = (int)EnumActionComment.FieldSurveyToDoing;
                                else if (model.Status == (int)EnumFieldSurveyState.CompleteAppraised)
                                    note.ActionComment = (int)EnumActionComment.FieldSurveyCompleteCompanyOrAddress;
                                //kiêm tra đúng trạng thái mới chuyển
                                if (((loanbrief.EvaluationHomeState.GetValueOrDefault() == (int)EnumFieldSurveyState.NotAppraised && model.Status == (int)EnumFieldSurveyState.DoingAppraised && model.TypeAppraiser == EnumFieldSurveyType.Home.GetHashCode()) ||
                                          (loanbrief.EvaluationHomeState.GetValueOrDefault() == (int)EnumFieldSurveyState.DoingAppraised && model.Status == (int)EnumFieldSurveyState.CompleteAppraised && model.TypeAppraiser == EnumFieldSurveyType.Home.GetHashCode()))
                                  ||
                                       ((loanbrief.EvaluationCompanyState.GetValueOrDefault() == (int)EnumFieldSurveyState.NotAppraised && model.Status == (int)EnumFieldSurveyState.DoingAppraised && model.TypeAppraiser == EnumFieldSurveyType.Company.GetHashCode()) ||
                                          (loanbrief.EvaluationCompanyState.GetValueOrDefault() == (int)EnumFieldSurveyState.DoingAppraised && model.Status == (int)EnumFieldSurveyState.CompleteAppraised && model.TypeAppraiser == EnumFieldSurveyType.Company.GetHashCode()))
                                   )
                                {

                                    //cập nhật loanbrief
                                    if (model.TypeAppraiser == EnumFieldSurveyType.Home.GetHashCode())
                                    {
                                        loanbrief.EvaluationHomeState = model.Status;
                                        if (model.Status == (int)EnumFieldSurveyState.CompleteAppraised)
                                        {
                                            loanbrief.EvaluationHomeTime = loanbrief.EvaluationHomeTime ?? DateTime.Now;
                                            loanbrief.LoanBriefComment = "Xác nhận hoàn thành thẩm định nhà";
                                        }
                                        else
                                        {
                                            if (model.Status == (int)EnumFieldSurveyState.DoingAppraised)
                                                loanbrief.EvaluationHomeTime = loanbrief.EvaluationHomeTime ?? DateTime.Now;
                                            loanbrief.LoanBriefComment = model.Status == (int)EnumFieldSurveyState.DoingAppraised ? "Đang thẩm định" : "Chưa thẩm định";
                                        }
                                    }
                                    else
                                    {
                                        loanbrief.EvaluationCompanyState = model.Status;
                                        if (model.Status == (int)EnumFieldSurveyState.CompleteAppraised)
                                        {
                                            loanbrief.EvaluationCompanyTime = loanbrief.EvaluationCompanyTime ?? DateTime.Now;
                                            loanbrief.LoanBriefComment = "Xác nhận hoàn thành thẩm định công ty";
                                        }
                                        else
                                        {
                                            if (model.Status == (int)EnumFieldSurveyState.DoingAppraised)
                                                loanbrief.EvaluationCompanyTime = loanbrief.EvaluationCompanyTime ?? DateTime.Now;
                                            loanbrief.LoanBriefComment = model.Status == (int)EnumFieldSurveyState.DoingAppraised ? "Đang thẩm định" : "Chưa thẩm định";
                                        }
                                    }

                                    //cập nhật thông tin đơn vauy
                                    _unitOfWork.LoanBriefRepository.Update(loanbrief);
                                    //insert vào bảng comment
                                    note.Note = "<b>TĐTĐ</b>: " + loanbrief.LoanBriefComment;
                                    _unitOfWork.LoanBriefNoteRepository.Insert(note);
                                    //insert vào bảng ProductAppraiser
                                    var productAppraiser = new ProductAppraiser()
                                    {
                                        ProductCreditId = loanbrief.ProductId,
                                        LoanBriefId = loanbrief.LoanBriefId,
                                        TypeAppraiser = model.TypeAppraiser,
                                        Status = model.Status,
                                        UserId = user.UserId,
                                        GroupId = user.GroupId,
                                        Latitude = model.Latitude,
                                        Longitude = model.Longitude,
                                        CreatedAt = DateTime.Now
                                    };
                                    if (model.Status == (int)EnumFieldSurveyState.CompleteAppraised)
                                        productAppraiser.IsWork = model.IsWord;
                                    _unitOfWork.ProductAppraiserRepository.Insert(productAppraiser);
                                    _unitOfWork.Save();
                                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                    return Ok(def);
                                }
                                else
                                {
                                    def.meta = new Meta(406, "Trạng thái thẩm định không đúng.!!!");
                                    return Ok(def);
                                }
                            }
                            else
                            {
                                def.meta = new Meta(405, "Nhóm tài khoản của bạn không có quyền");
                                return Ok(def);
                            }
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(210, "Vui lòng truyền sang mã đơn vay!!!");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Appraiser/UpdateAppraiser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #endregion
        private List<GroupName> ReadXMLCheckList(LoanBrief loanBrief)
        {
            try
            {
                var listData = new List<GroupName>();
                if (string.IsNullOrWhiteSpace(environment.WebRootPath))
                {
                    environment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "");
                }
                string filePath = Path.Combine(environment.WebRootPath, "CheckListVerificationXML\\VerificationXML.xml");
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                    filePath = Path.Combine(environment.WebRootPath, "CheckListVerificationXML/VerificationXML.xml");

                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                XmlNode nodequizzes = doc.DocumentElement.SelectSingleNode("/quizzes");
                foreach (XmlNode item in nodequizzes)
                {
                    var groupName = new GroupName();
                    groupName.Name = (item.Attributes["groupname"] != null) ? item.Attributes["groupname"].Value : string.Empty;
                    groupName.ListGroupChild = new List<GroupChildren>();
                    var groupValue = (item.Attributes["groupvalue"] != null) ? item.Attributes["groupvalue"].Value : string.Empty;
                    var _groupvaluehouse = string.Empty;
                    var _groupvaluecompany = string.Empty;
                    var _groupvalueall = string.Empty;

                    if (loanBrief.ProductId == (int)EnumProductCredit.SalaryCreditType
                        || loanBrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                        || loanBrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                        || loanBrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT
                        || loanBrief.ProductId == (int)EnumProductCredit.LoanFastMoto_KCC)
                    {
                        _groupvaluehouse = "house";
                        _groupvaluecompany = "company";
                        _groupvalueall = "all";
                    }
                    else if (loanBrief.ProductId == (int)EnumProductCredit.OtoCreditType_KCC
                        || loanBrief.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                        || loanBrief.ProductId == (int)EnumProductCredit.CamCoHangHoa)
                    {
                        _groupvaluehouse = "house";
                        _groupvaluecompany = "house";
                        _groupvalueall = "all";
                    }
                    if ((groupValue == _groupvaluehouse || groupValue == _groupvaluecompany || groupValue == _groupvalueall))
                    {
                        //duyệt node titlegroup
                        var nodeGroup = item.ChildNodes;
                        foreach (XmlNode nodegroup in nodeGroup)
                        {
                            var groupChild = new GroupChildren();
                            groupChild.GroupChildName = (nodegroup.Attributes["title"] != null) ? nodegroup.Attributes["title"].Value : string.Empty;
                            groupChild.IsRequire = (nodegroup.Attributes["IsRequire"] != null) ? bool.Parse(nodegroup.Attributes["IsRequire"].Value) : true;
                            groupChild.Note = (nodegroup.Attributes["note"] != null) ? nodegroup.Attributes["note"].Value : string.Empty;
                            groupChild.ListQuestion = new List<Question>();

                            var nodequestions = nodegroup.ChildNodes;
                            // Duyet node question
                            foreach (XmlNode nodequestion in nodequestions)
                            {
                                var question = new Question();
                                question.ListGroupAnswer = new List<GroupAnswer>();
                                question.QuestionName = (nodequestion.Attributes["questionname"] != null) ? nodequestion.Attributes["questionname"].Value : string.Empty;
                                question.IsEdit = (nodequestion.Attributes["isEdit"] != null) ? int.Parse(nodequestion.Attributes["isEdit"].Value) : 0;
                                //đuyệt câu hỏi comment trước
                                var isComment = nodequestion.Attributes["isComment"];
                                if (isComment != null && isComment.Value == "1")
                                {
                                    question.IsComment = 1;
                                }
                                else
                                {
                                    question.IsComment = 0;
                                    // Duyệt theo nhóm câu trả lời
                                    var nodegroupanswers = nodequestion.ChildNodes;
                                    foreach (XmlNode nodegroupanswer in nodegroupanswers)
                                    {
                                        var groupAnswer = new GroupAnswer();
                                        groupAnswer.TitleAnswer = (nodegroupanswer.Attributes["title"] != null) ? nodegroupanswer.Attributes["title"].Value : string.Empty;
                                        groupAnswer.TypeInput = (nodegroupanswer.Attributes["typeinput"] != null) ? nodegroupanswer.Attributes["typeinput"].Value : string.Empty;
                                        groupAnswer.ListAnswer = new List<Answer>();
                                        // Duyệt từng câu trả lời
                                        var nodeanswers = nodegroupanswer.ChildNodes;
                                        var listanswer = new List<Answer>();
                                        foreach (XmlNode nodeanswer in nodeanswers)
                                        {
                                            var m_answer = new Answer();
                                            m_answer.AnswerName = nodeanswer.InnerText.ToString();
                                            m_answer.IsCorrect = 0;
                                            m_answer.IsEdit = (nodeanswer.Attributes["IsEdit"] != null) ? int.Parse(nodeanswer.Attributes["IsEdit"].Value) : 0;
                                            groupAnswer.ListAnswer.Add(m_answer);
                                        }
                                        // Add vào nhóm câu trả lời
                                        question.ListGroupAnswer.Add(groupAnswer);
                                    }
                                }
                                groupChild.ListQuestion.Add(question);
                            }
                            groupName.ListGroupChild.Add(groupChild);
                        }

                        listData.Add(groupName);
                    }
                }
                return listData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ReadXMLCheckList Exception");
                return null;
            }
        }

        //Tool Tính lãi phí & Bảo hiểm
        [HttpPost]
        [Route("InterestTool")]
        public ActionResult<DefaultResponse<Meta, DataInterest>> InterestTool([FromBody] InterestToolReq req)
        {
            var def = new DefaultResponse<Meta, DataInterest>();
            try
            {
                if (req.Frequency <= 0 || req.LoanTime <= 0 || req.RateType <= 0 || req.TotalMoney <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }

                var url = _baseConfig["AppSettings:LMS"];
                var client = new RestClient(url + "/api/s2s/GetPaymentSchedulePresume");
                var body = JsonConvert.SerializeObject(req);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                InterestToolDTO lst = JsonConvert.DeserializeObject<InterestToolDTO>(json);
                var data = lst.Data;
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.ListLender.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "App",
                    Status = (int)StatusCallApi.Success,
                    LoanBriefId = 0,
                    Output = json,
                    Input = body
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/Update CoordinatorUserId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //List ảnh chứng từ khách hàng theo type
        [HttpPost]
        [Route("ListImage")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LoanBriefFilesDetail>>>> ListImage([FromBody] ImageReq.ListImage req)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefFilesDetail>>();
            try
            {
                if (req.LoanId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var userId = GetUserId();
                req.UserId = userId;
                var query = await _unitOfWork.LoanBriefFileRepository.Query(x => x.Status == 1 && x.IsDeleted != 1
                    && x.LoanBriefId == req.LoanId && (x.TypeId == req.TypeId || req.TypeId == 0), null, false)
                    .Select(LoanBriefFilesDetail.ProjectionDetail).ToListAsync();
                var totalRecords = query.Count;
                List<LoanBriefFilesDetail> data = query.ToList();
                if (data != null && data.Count > 0)
                {
                    var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                    var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
                    var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
                    await Task.Run(() => Parallel.ForEach(data, item =>
                     {
                         if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                         {
                             item.FilePath = ServiceURL + item.FilePath;
                             if (!string.IsNullOrEmpty(item.FileThumb))
                                 item.FileThumb = ServiceURL + item.FileThumb;
                         }
                         else if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null))
                         {
                             item.FilePath = ServiceURLAG + item.FilePath;
                             if (!string.IsNullOrEmpty(item.FileThumb))
                                 item.FileThumb = ServiceURLAG + item.FileThumb;
                         }
                         else if (item.MecashId > 0 && item.S3status == 1)
                         {
                             item.FilePath = ServiceURLFileTima + item.FilePath;
                             if (!string.IsNullOrEmpty(item.FileThumb))
                                 item.FileThumb = ServiceURLFileTima + item.FileThumb;
                         }
                     }));
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/ListImage Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //List ảnh Chưa được gán cho đơn vay
        [HttpGet]
        [Route("ListImageByUser")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LoanBriefFilesDetail>>>> ListImageByUser()
        {
            var def = new DefaultResponse<Meta, List<LoanBriefFilesDetail>>();
            try
            {
                var userId = GetUserId();
                if (userId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var query = await _unitOfWork.LoanBriefFileRepository.Query(x => x.IsDeleted == 0 && x.UserId == userId
                && (x.TypeId == 0 || x.TypeId == null), null, false).Select(LoanBriefFilesDetail.ProjectionDetail).ToListAsync();
                var totalRecords = query.Count;
                List<LoanBriefFilesDetail>
                data = query.ToList();
                if (data != null && data.Count > 0)
                {
                    var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                    var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
                    var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
                    await Task.Run(() => Parallel.ForEach(data, item =>
                     {
                         if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                         {
                             item.FilePath = ServiceURL + item.FilePath;
                             if (!string.IsNullOrEmpty(item.FileThumb))
                                 item.FileThumb = ServiceURL + item.FileThumb;
                         }
                         else if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null))
                         {
                             item.FilePath = ServiceURLAG + item.FilePath;
                             if (!string.IsNullOrEmpty(item.FileThumb))
                                 item.FileThumb = ServiceURLAG + item.FileThumb;
                         }
                         else if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                         {
                             item.FilePath = ServiceURLFileTima + item.FilePath;
                             if (!string.IsNullOrEmpty(item.FileThumb))
                                 item.FileThumb = ServiceURLFileTima + item.FileThumb;
                         }
                     }));
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/ListImageByUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("ListImageByUserV2")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LoanBriefFilesDetail>>>> ListImageByUserV2(int LoanBriefId)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefFilesDetail>>();
            try
            {

                var userId = GetUserId();
                if (userId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }

                var query = await _unitOfWork.LoanBriefFileRepository.Query(x => x.IsDeleted == 0 && x.UserId == userId && (x.TypeId == 0 || x.TypeId == null)
                && (x.LoanBriefId == LoanBriefId || LoanBriefId == 0), null, false).Select(LoanBriefFilesDetail.ProjectionDetail).ToListAsync();
                var totalRecords = query.Count();
                List<LoanBriefFilesDetail>
                data = query.ToList();
                if (data != null && data.Count > 0)
                {
                    var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                    var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
                    var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
                    await Task.Run(() => Parallel.ForEach(data, item =>
                     {
                         if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                         {
                             item.FilePath = ServiceURL + item.FilePath;
                             if (!string.IsNullOrEmpty(item.FileThumb))
                                 item.FileThumb = ServiceURL + item.FileThumb;
                         }
                         else if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null))
                         {
                             item.FilePath = ServiceURLAG + item.FilePath;
                             if (!string.IsNullOrEmpty(item.FileThumb))
                                 item.FileThumb = ServiceURLAG + item.FileThumb;
                         }
                         else if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                         {
                             item.FilePath = ServiceURLFileTima + item.FilePath;
                             if (!string.IsNullOrEmpty(item.FileThumb))
                                 item.FileThumb = ServiceURLFileTima + item.FileThumb;
                         }
                     }));
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/ListImageByUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Xóa ảnh đã được gán cho khách hàng
        [HttpPost]
        [Route("DeleteImage")]
        public ActionResult<DefaultResponse<Meta>> DeleteImage([FromBody] ImageReq.DeleteImage req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LoanId <= 0 || req.ImgId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }

                var filesInfo = _unitOfWork.LoanBriefFileRepository.GetById(req.ImgId);
                if (filesInfo != null)
                {
                    var userId = GetUserId();
                    var user = _unitOfWork.UserRepository.GetById(userId);
                    req.UserId = user.UserId;
                    if (filesInfo.LoanBriefId == req.LoanId)
                    {
                        filesInfo.IsDeleted = 1;
                        filesInfo.DeletedBy = userId;
                        filesInfo.ModifyAt = DateTime.Now;
                        _unitOfWork.LoanBriefFileRepository.Update(filesInfo);
                        _unitOfWork.Save();
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        return Ok(def);
                    }


                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/DeleteImage Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Xóa ảnh chưa được gán cho khách hàng
        [HttpPost]
        [Route("DeleteImageByUser")]
        public ActionResult<DefaultResponse<Meta>> DeleteImageByUser([FromBody] ImageReq.DeleteImageByUser req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LstImg == "")
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                string[] imgList = req.LstImg.Split(",");

                if (imgList.Length > 0)
                {
                    var filesInfo = new LoanBriefFiles();
                    var userId = GetUserId();
                    var user = _unitOfWork.UserRepository.GetById(userId);
                    req.UserId = user.UserId;
                    for (int i = 0; i < imgList.Length; i++)
                    {
                        filesInfo = _unitOfWork.LoanBriefFileRepository.GetById(Convert.ToInt32(imgList[i]));
                        if (filesInfo != null)
                        {
                            if (filesInfo.UserId == req.UserId)
                            {
                                filesInfo.IsDeleted = 1;
                                filesInfo.DeletedBy = user.UserId;
                                filesInfo.ModifyAt = DateTime.Now;
                            }
                            else
                            {
                                def.meta = new Meta(403, "Bạn không có quyền xóa!");
                                return Ok(def);
                            }
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                            return Ok(def);
                        }

                    }
                    _unitOfWork.LoanBriefFileRepository.Update(filesInfo);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/DeleteImage Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Gán ảnh vào danh mục chứng từ
        [HttpPost]
        [Route("UpdateImageByType")]
        public ActionResult<DefaultResponse<Meta>> UpdateImageByType([FromBody] ImageReq.UpdateImageByType req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LstImg == "" || req.TypeId == 0 || req.LoanId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                string[] imgList = req.LstImg.Split(",");

                //Kiểm tra đơn vay có tồn tại trên hệ thống
                var loanbrief = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.LoanId, null, false).Select(LoanBriefInfo.ProjectionLoanBriefInfo).FirstOrDefault();
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    //Lấy config của chứng từ cần chuyển
                    var typeOwnerShip = Common.Utils.ProductPriceUtils.ConvertEnumTypeOfOwnerShipDocument(loanbrief.CustomerResident.ResidentType.Value);
                    var configDoc = _unitOfWork.ConfigDocumentRepository.Query(x => x.DocumentTypeId == req.TypeId && x.ProductId == loanbrief.ProductId
                    && x.TypeOwnerShip == typeOwnerShip && x.IsEnable == true, null, false).FirstOrDefault();
                    if (configDoc != null)
                    {
                        if (configDoc.AllowFromUpload != "3")
                        {
                            if (imgList.Length > 0)
                            {
                                var filesInfo = new LoanBriefFiles();
                                var userId = GetUserId();
                                var user = _unitOfWork.UserRepository.GetById(userId);
                                req.UserId = user.UserId;
                                for (int i = 0; i < imgList.Length; i++)
                                {
                                    filesInfo = _unitOfWork.LoanBriefFileRepository.GetById(Convert.ToInt32(imgList[i]));
                                    if (filesInfo != null)
                                    {
                                        if (filesInfo.UserId == req.UserId)
                                        {
                                            _unitOfWork.LoanBriefFileRepository.Update(x => x.Id == filesInfo.Id, x => new LoanBriefFiles()
                                            {
                                                TypeId = req.TypeId,
                                                ModifyAt = DateTime.Now
                                            });
                                        }
                                        else
                                        {
                                            def.meta = new Meta(403, "Bạn không có quyền xóa!");
                                            return Ok(def);
                                        }
                                    }
                                    else
                                    {
                                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                                        return Ok(def);
                                    }

                                }
                                _unitOfWork.Save();
                                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            }
                        }
                        else
                        {
                            def.meta = new Meta(403, "Bạn không được chuyển chứng từ vào chuyên mục này!");
                            return Ok(def);
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/UpdateImageByType Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //List danh mục chứng từ theo sản phẩm
        [Route("ListDocument")]
        [HttpGet]
        public ActionResult<DefaultResponse<DocumentTypeDetail>> ListDocument(int ProductId)
        {
            var def = new DefaultResponse<List<DocumentTypeDetail>>();
            var documents = _unitOfWork.DocumentTypeRepository.Query(x => x.ProductId == ProductId && x.IsEnable == 1, null, false).Select(DocumentTypeDetail.ProjectionDetail).ToList();
            def.meta = new Meta(200, "success");
            def.data = documents;
            return Ok(def);
        }

        [Route("ListDocumentV2")]
        [HttpGet]
        public ActionResult<DefaultResponse<LOS.DAL.DTOs.DocumentTypeNewV2>> ListDocumentV2(int LoanBriefId, int ProductId)
        {
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var def = new DefaultResponse<List<LOS.DAL.DTOs.DocumentTypeNewV2>>();
            if (LoanBriefId <= 0 || ProductId <= 0)
            {
                def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                return Ok(def);
            }
            // check if loan action exists
            var loanbrief = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId, null, false).Select(x => new
            {
                JobId = x.LoanBriefJob != null ? x.LoanBriefJob.JobId : 0,
                ImcomeType = x.LoanBriefJob != null ? x.LoanBriefJob.ImcomeType : 0,
                CompanyInsurance = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyInsurance : null,
                BusinessPapers = x.LoanBriefJob != null ? x.LoanBriefJob.BusinessPapers : null,
                ResidentType = x.LoanBriefJob != null ? x.LoanBriefResident.ResidentType : null,
            }).FirstOrDefault();
            if (loanbrief != null)
            {
                var documents = _unitOfWork.DocumentTypeRepository.Query(x => x.ProductId == ProductId && x.IsEnable == 1 && x.Version == 2, null, false)
                    .Select(LOS.DAL.DTOs.DocumentTypeDetail.ProjectionDetail).ToList();
                if (documents != null && documents.Count > 0)
                {
                    var lstFile = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == LoanBriefId && x.Status == 1 && x.IsDeleted != 1, null, false).Select(LOS.DAL.DTOs.LoanBriefFileDetail.ProjectionDetail).ToList();

                    var workRuleType = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(loanbrief.JobId.Value, loanbrief.ImcomeType
                                                , loanbrief.CompanyInsurance, loanbrief.BusinessPapers);
                    var lstDocNew = Helpers.Ultility.GetListDocumentNewV2(documents, loanbrief.ResidentType.Value, workRuleType, lstFile, ServiceURL, ServiceURLAG, ServiceURLFileTima);
                    def.meta = new Meta(200, "success");
                    def.data = lstDocNew;
                }
            }
            else
            {
                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                return Ok(def);
            }
            return Ok(def);
        }

        [Route("ListDocumentV3")]
        [HttpGet]
        public async Task<ActionResult<DefaultResponse<DAL.DTOs.DocumentMapInfo>>> ListDocumentV3(int LoanBriefId, int ProductId)
        {
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var def = new DefaultResponse<List<DAL.DTOs.DocumentMapInfo>>();
            if (LoanBriefId <= 0 || ProductId <= 0)
            {
                def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                return Ok(def);
            }
            // check if loan action exists
            var loanbrief2 = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId, null, false).Select(x => new
            {
                JobId = x.LoanBriefJob != null ? x.LoanBriefJob.JobId : 0,
                ProductId = x.ProductId,
                ImcomeType = x.LoanBriefJob != null ? x.LoanBriefJob.ImcomeType : 0,
                CompanyInsurance = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyInsurance : null,
                BusinessPapers = x.LoanBriefJob != null ? x.LoanBriefJob.BusinessPapers : null,
                ResidentType = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType : null,
            }).FirstOrDefaultAsync();
            if (loanbrief2 != null)
            {
                //lấy danh sách chứng từ
                var listDocument = _documentService.GetDocumentType(loanbrief2.ProductId.Value, Common.Utils.ProductPriceUtils.ConvertEnumTypeOfOwnerShipDocument(loanbrief2.ResidentType.Value));
                if (listDocument != null && listDocument.Count > 0)
                {
                    var groupJobId = 0;
                    if (loanbrief2.JobId.GetValueOrDefault(0) > 0)
                    {
                        groupJobId = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(loanbrief2.JobId.Value, loanbrief2.ImcomeType
                                               , loanbrief2.CompanyInsurance, loanbrief2.BusinessPapers);
                    }

                    var listFiles = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == LoanBriefId && x.Status == 1 && x.IsDeleted != 1, null, false)
                        .Select(LOS.DAL.DTOs.LoanBriefFileDetail.ProjectionDetail).ToList();

                    var resultDocuments = LOS.AppAPI.Helpers.Ultility.ConvertDocumentTree(listDocument, listFiles, 0, groupJobId);
                    def.meta = new Meta(200, "success");
                    def.data = resultDocuments;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không lấy được danh mục chứng từ");
                }
            }
            else
            {
                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                return Ok(def);
            }
            return Ok(def);
        }

        [Route("UploadImage")]
        [HttpPost]
        public async Task<IActionResult> UploadImage(List<IFormFile> files)
        {
            var userId = GetUserId();
            var user = _unitOfWork.UserRepository.GetById(userId);
            var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
            var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var BucketName = _baseConfig["AppSettings:BucketName"];
            var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
            {
                RegionEndpoint = RegionEndpoint.APSoutheast1
            });
            var link = "";
            var folder = BucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
            var lst = new List<LoanBriefFiles>();
            var def = new DefaultResponse<Meta, List<LoanBriefFiles>>();
            if (files == null)
            {
                def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                return Ok(def);
            }
            try
            {
                foreach (var file in files)
                {
                    var extension = Path.GetExtension(file.FileName);
                    var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                    if (allowedExtensions.Contains(extension.ToLower()))
                    {
                        string ImageName = Guid.NewGuid().ToString() + extension;
                        using (var stream = new MemoryStream())
                        {
                            file.CopyTo(stream);
                            PutObjectRequest req = new PutObjectRequest()
                            {
                                InputStream = stream,
                                BucketName = folder,
                                Key = ImageName,
                                CannedACL = S3CannedACL.PublicRead
                            };
                            await client.PutObjectAsync(req);
                            link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                            //insert to db
                            var obj = new LoanBriefFiles
                            {
                                LoanBriefId = null,
                                CreateAt = DateTime.Now,
                                FilePath = link,
                                UserId = user.UserId,
                                Status = 1,
                                TypeId = null,
                                S3status = 1,
                                MecashId = 1000000000,
                                SourceUpload = (int)EnumSourceUpload.Mobile
                            };
                            _unitOfWork.LoanBriefFileRepository.Insert(obj);
                            lst.Add(obj);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(211, "Ảnh không đúng định dạng!");
                        return Ok(def);
                    }

                }
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lst.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/UploadImage Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("UploadImageV2")]
        [HttpPost]
        public async Task<IActionResult> UploadImageV2(List<IFormFile> files, int LoanBriefId = 0, string LatLong = "", DateTime? CreatedTime = null)
        {
            var userId = GetUserId();
            var user = _unitOfWork.UserRepository.GetById(userId);
            var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
            var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
            var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
            var BucketName = _baseConfig["AppSettings:BucketName"];
            var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
            {
                RegionEndpoint = RegionEndpoint.APSoutheast1
            });

            var folder = BucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
            var lst = new List<LoanBriefFiles>();
            var def = new DefaultResponse<Meta, List<LoanBriefFiles>>();
            if (files == null)
            {
                def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                return Ok(def);
            }
            try
            {
                foreach (var file in files)
                {
                    var link = "";
                    var linkThumb = "";
                    var extension = Path.GetExtension(file.FileName);
                    var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                    if (allowedExtensions.Contains(extension.ToLower()))
                    {
                        string ImageName = "";
                        string ImageThumb = Guid.NewGuid().ToString() + "-thumb" + extension;
                        if (!string.IsNullOrEmpty(LatLong))
                        {
                            ImageName = string.Format("{0}-{1}{2}", LatLong.Replace(",", "-"), Guid.NewGuid().ToString(), extension);
                        }
                        else
                        {
                            ImageName = Guid.NewGuid().ToString() + extension;
                        }
                        using (var stream = new MemoryStream())
                        {
                            file.CopyTo(stream);
                            PutObjectRequest req = new PutObjectRequest()
                            {
                                InputStream = stream,
                                BucketName = folder,
                                Key = ImageName,
                                CannedACL = S3CannedACL.PublicRead
                            };
                            await client.PutObjectAsync(req);
                            //Crop image
                            using var image = Image.Load(file.OpenReadStream());
                            if (image.Width > 500)
                            {
                                image.Mutate(x => x.Resize(200, image.Height / (image.Width / 200)));

                                var outputStream = new MemoryStream();
                                image.Save(outputStream, new JpegEncoder());
                                outputStream.Seek(0, 0);
                                //Upload Thumb
                                PutObjectRequest reqThumb = new PutObjectRequest()
                                {
                                    InputStream = outputStream,
                                    BucketName = folder,
                                    Key = ImageThumb,
                                    CannedACL = S3CannedACL.PublicRead
                                };
                                await client.PutObjectAsync(reqThumb);
                                linkThumb = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + ImageThumb;
                            }
                            //End
                            link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                            //insert to db
                            var obj = new LoanBriefFiles
                            {
                                LoanBriefId = LoanBriefId,
                                CreateAt = CreatedTime == null ? DateTime.Now : CreatedTime,
                                FilePath = link,
                                UserId = user.UserId,
                                Status = 1,
                                TypeId = null,
                                S3status = 1,
                                MecashId = 1000000000,
                                SourceUpload = (int)EnumSourceUpload.Mobile,
                                LatLong = LatLong,
                                FileThumb = linkThumb
                            };
                            _unitOfWork.LoanBriefFileRepository.Insert(obj);
                            _unitOfWork.Save();
                            lst.Add(obj);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(211, "Ảnh không đúng định dạng!");
                        return Ok(def);
                    }

                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lst.ToList();
                if (def.data != null && def.data.Count > 0)
                {
                    foreach (var item in def.data)
                    {
                        item.FilePath = ServiceURL + item.FilePath;
                        item.FileThumb = ServiceURL + item.FileThumb;
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/UploadImageV2 Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Update thông tin đơn vay
        [HttpPost]
        [Route("UpdateLoan")]
        public ActionResult<DefaultResponse<Meta>> UpdateLoanBrief([FromBody] Models.App.LoanInfoReq.UpdateLoanApp req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LoanId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = _unitOfWork.LoanBriefRepository.GetById(req.LoanId);
                if (loanInfo != null)
                {
                    var TypeProduct = _unitOfWork.LoanProductRepository.GetById(loanInfo.ProductId).TypeProductId;
                    if (TypeProduct == 1 && (req.RateType == EnumRateType.RateMonthADay.GetHashCode() || req.RateType == EnumRateType.TatToanCuoiKy.GetHashCode()))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Gói sản phẩm nhỏ chỉ áp dụng Dư nợ giảm dần");
                        return Ok(def);
                    }
                    loanInfo.ReceivingMoneyType = req.ReceivingMoneyType;
                    loanInfo.BankAccountNumber = req.BankAccountNumber;
                    loanInfo.BankCardNumber = req.BankCardNumber;
                    if (req.BankId > 0)
                        loanInfo.BankId = req.BankId;
                    loanInfo.BankAccountName = req.BankAccountName;
                    loanInfo.LoanBriefComment = loanInfo.LoanBriefComment + Environment.NewLine + "- " + req.LoanBriefComment;
                    loanInfo.LoanAmount = req.LoanAmount;
                    loanInfo.LoanTime = req.LoanTime;
                    loanInfo.Frequency = req.Frequency;
                    loanInfo.RateTypeId = req.RateType;
                    _unitOfWork.LoanBriefRepository.Update(loanInfo);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/UpdateLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Gửi tin nhắn cho khách hàng
        [HttpPost]
        [Route("SendSMS")]
        public ActionResult<DefaultResponse<Meta>> SendSMS([FromBody] SendSMSReq.param req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LoanId <= 0 || req.Content == "")
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = _unitOfWork.LoanBriefRepository.GetById(req.LoanId);
                if (loanInfo != null)
                {
                    var url = _baseConfig["AppSettings:SMS"] + "/api/SMSBrandName/SendSMSBrandNameFPT";
                    SendSMSReq.Input param = new SendSMSReq.Input();
                    param.Domain = "vay1h.vn";
                    param.Department = "Phòng IT";
                    param.Phone = loanInfo.Phone;
                    param.MessageContent = req.Content;
                    var body = JsonConvert.SerializeObject(param);
                    var client = new RestClient(url);
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Authorization", "8JTLS9eqmSKXdQKdNWrtXCMV5DAhC3k7");
                    request.AddHeader("account", "LOS");
                    request.AddHeader("password", "3cmELn3UsqFkvERbuJdzHtUL7HU6uFuH");
                    request.AddHeader("Content-Type", "application/json");
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    var json = response.Content;
                    SendSMSReq.OutPut lst = JsonConvert.DeserializeObject<SendSMSReq.OutPut>(json);
                    //Add log
                    var log = new LogCallApi
                    {
                        ActionCallApi = ActionCallApi.Sms.GetHashCode(),
                        LinkCallApi = url,
                        TokenCallApi = "App",
                        Status = (int)StatusCallApi.Success,
                        LoanBriefId = req.LoanId,
                        Output = json,
                        Input = body
                    };
                    _unitOfWork.LogCallApiRepository.Insert(log);
                    _unitOfWork.Save();

                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/SendSMS Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Update thông tin khách hàng
        [HttpPost]
        [Route("UpdateCustomer")]
        public ActionResult<DefaultResponse<Meta>> UpdateCustomer([FromBody] Models.App.LoanInfoReq.UpdateCustomerApp req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LoanId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = _unitOfWork.LoanBriefRepository.GetById(req.LoanId);
                if (loanInfo != null)
                {
                    var cusInfo = _unitOfWork.CustomerRepository.GetById(loanInfo.CustomerId);
                    var houseOldInfo = _unitOfWork.LoanBriefHouseholdRepository.Query(x => x.LoanBriefHouseholdId == req.LoanId, null, false).FirstOrDefault();
                    var jobInfo = _unitOfWork.LoanBriefJobRepository.Query(x => x.LoanBriefJobId == req.LoanId, null, false).FirstOrDefault();
                    //Loan
                    if (req.CityId != 0)
                        loanInfo.ProvinceId = req.CityId;
                    if (req.DistrictId != 0)
                        loanInfo.DistrictId = req.DistrictId;
                    if (req.WardId != 0)
                        loanInfo.WardId = req.WardId;
                    _unitOfWork.LoanBriefRepository.Update(loanInfo);
                    //Customer
                    if (cusInfo != null)
                    {
                        cusInfo.UpdatedAt = DateTime.Now;
                        if (req.CompanyName != "")
                            cusInfo.CompanyName = req.CompanyName;
                        if (req.AddressCompany != "")
                            cusInfo.CompanyAddress = req.AddressCompany;
                        if (req.CityId != 0)
                            cusInfo.ProvinceId = req.CityId;
                        if (req.DistrictId != 0)
                            cusInfo.DistrictId = req.DistrictId;
                        if (req.WardId != 0)
                            cusInfo.WardId = req.WardId;
                        if (req.CustomerAddress != "")
                            cusInfo.Address = req.CustomerAddress;
                        _unitOfWork.CustomerRepository.Update(cusInfo);
                    }
                    //HouseOld
                    if (houseOldInfo != null)
                    {
                        if (req.AddressHouseHold != "")
                            houseOldInfo.Address = req.AddressHouseHold;
                        _unitOfWork.LoanBriefHouseholdRepository.Update(houseOldInfo);
                    }
                    //Job
                    if (jobInfo != null)
                    {
                        if (req.AddressCompany != "")
                            jobInfo.CompanyAddress = req.AddressCompany;
                        if (req.CompanyCityId != 0)
                            jobInfo.CompanyProvinceId = req.CompanyCityId;
                        if (req.CompanyDistrictId != 0)
                            jobInfo.CompanyDistrictId = req.CompanyDistrictId;
                        if (req.CompanyWardId != 0)
                            jobInfo.CompanyWardId = req.CompanyWardId;
                        if (req.CompanyName != "")
                            jobInfo.CompanyName = req.CompanyName;
                        _unitOfWork.LoanBriefJobRepository.Update(jobInfo);
                    }
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/UpdateCustomer Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Thẩm định thực địa trả lại đơn
        [HttpPost]
        [Route("ReturnLoan")]
        public async Task<ActionResult<DefaultResponse<object>>> ReturnLoan(Models.LMS.LoanBriefReq.LMSReturnLoan req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var userId = GetUserId();
                if (req.LoanId <= 0 || string.IsNullOrEmpty(req.Note) || userId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var user = await _unitOfWork.UserRepository.Query(x => x.UserId == userId, null, false).Select(x => new
                {
                    UserId = x.UserId,
                    FullName = x.FullName
                }).FirstOrDefaultAsync();
                if (user != null && user.UserId > 0)
                {
                    var loanInfo = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.LoanId, null, false).Select(x => new
                    {
                        LoanbriefId = x.LoanBriefId,
                        Status = x.Status,
                        InProcess = x.InProcess
                    }).FirstOrDefaultAsync();
                    if (loanInfo != null)
                    {
                        if (loanInfo.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        {
                            def.meta = new Meta(211, "Đơn vay đang bị hủy!");
                            return Ok(def);
                        }
                        if (loanInfo.InProcess == EnumInProcess.Process.GetHashCode())
                        {
                            def.meta = new Meta(212, "Đơn vay đang được xử lý!");
                            return Ok(def);
                        }
                        _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanbriefId, x => new LoanBrief()
                        {
                            InProcess = (int)EnumInProcess.Process,
                            ActionState = (int)EnumActionPush.Back,
                            PipelineState = (int)EnumPipelineState.MANUAL_PROCCESSED
                        });
                        _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote
                        {
                            LoanBriefId = req.LoanId,
                            Note = string.Format("Thẩm định viên trả lại đơn: {0}", req.Note),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.APPReturnLoan.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = 0,
                            ShopName = "APP"
                        });
                        _unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tìm thấy thông tin người dùng");
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/ReturnLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Thẩm định thực địa đẩy đơn đơn
        [HttpPost]
        [Route("PushLoan")]
        public async Task<ActionResult<DefaultResponse<object>>> PushLoanAsync(Models.LMS.LoanBriefReq.PushLoan req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var userId = GetUserId();
                if (req.LoanId <= 0 || string.IsNullOrEmpty(req.Note) || userId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var user = await _unitOfWork.UserRepository.Query(x => x.UserId == userId, null, false).Select(x => new
                {
                    UserId = x.UserId,
                    FullName = x.FullName,
                    GroupId = x.GroupId
                }).FirstOrDefaultAsync();
                if (user != null && user.UserId > 0)
                {
                    var loanInfo = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.LoanId, null, false).Select(x => new
                    {
                        LoanBriefId = x.LoanBriefId,
                        Status = x.Status,
                        InProcess = x.InProcess,
                        EvaluationHomeUserId = x.EvaluationHomeUserId,
                        EvaluationHomeState = x.EvaluationHomeState,
                        EvaluationCompanyUserId = x.EvaluationCompanyUserId,
                        EvaluationCompanyState = x.EvaluationCompanyState,
                        ProductId = x.ProductId
                    }).FirstOrDefaultAsync();
                    if (loanInfo != null)
                    {
                        if (loanInfo.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        {
                            def.meta = new Meta(211, "Đơn vay đang bị hủy!");
                            return Ok(def);
                        }
                        if (loanInfo.InProcess == EnumInProcess.Process.GetHashCode())
                        {
                            def.meta = new Meta(212, "Đơn vay đang được xử lý!");
                            return Ok(def);
                        }
                        //kiểm tra có phải đang ở trạng thái chờ thẩm định
                        if (loanInfo.Status != EnumLoanStatus.APPRAISER_REVIEW.GetHashCode() && loanInfo.Status != EnumLoanStatus.WAIT_HO_APPRAISER_REVIEW.GetHashCode()
                            && loanInfo.Status != EnumLoanStatus.WAIT_HUB_EMPLOYEE_PREPARE_LOAN.GetHashCode())
                        {
                            def.meta = new Meta(211, "Đơn đã được đẩy lên đề xuất duyệt!");
                            return Ok(def);
                        }
                        //kiểm tra xem đã thẩm định xong chưa
                        if (loanInfo.Status == EnumLoanStatus.APPRAISER_REVIEW.GetHashCode())
                        {
                            if (loanInfo.EvaluationHomeUserId > 0 && loanInfo.EvaluationHomeState != (int)EnumFieldSurveyState.CompleteAppraised) // Đơn vay phải thẩm định nhà
                            {
                                def.meta = new Meta(212, "Bạn cần hoàn thành thẩm định nhà!");
                                return Ok(def);
                            }

                            if (loanInfo.EvaluationCompanyUserId > 0 && loanInfo.EvaluationCompanyState != (int)EnumFieldSurveyState.CompleteAppraised) // Đơn vay phải thẩm định công ty
                            {
                                def.meta = new Meta(212, "Bạn cần hoàn thành thẩm định công ty!");
                                return Ok(def);
                            }
                        }

                        _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                        {
                            InProcess = (int)EnumInProcess.Process,
                            ActionState = (int)EnumActionPush.Push,
                            PipelineState = (int)EnumPipelineState.MANUAL_PROCCESSED
                        });

                        if (loanInfo.Status == EnumLoanStatus.APPRAISER_REVIEW.GetHashCode())
                        {
                            //Nếu là xe máy thì => thêm request lấy thông tin ảnh TTTB
                            if (loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                || loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                || loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                            {
                                // Thêm yêu cầu lấy dữ liệu TTTB
                                _unitOfWork.LogLoanInfoAiRepository.Insert(new LogLoanInfoAi()
                                {
                                    LoanbriefId = loanInfo.LoanBriefId,
                                    ServiceType = (int)ServiceTypeAI.EkycGetInfoNetwork,
                                    CreatedAt = DateTime.Now,
                                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest,
                                    FromApp = "APP_API"
                                });
                                // Thêm yêu cầu lấy dữ liệu đăng ký xe
                                _unitOfWork.LogLoanInfoAiRepository.Insert(new LogLoanInfoAi()
                                {
                                    LoanbriefId = loanInfo.LoanBriefId,
                                    ServiceType = (int)ServiceTypeAI.EkycMotorbikeRegistrationCertificate,
                                    CreatedAt = DateTime.Now,
                                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest,
                                    FromApp = "APP_API"
                                });
                            }
                            //Add note
                            var note = new LoanBriefNote
                            {
                                LoanBriefId = req.LoanId,
                                Note = user.GroupId == (int)EnumGroupUser.FieldSurvey
                                        ? string.Format("Thẩm định HO thẩm định xong. Đẩy đơn: {0}", req.Note)
                                            : string.Format("Thẩm định viên thẩm định xong. Đề xuất duyệt đơn: {0}", req.Note),
                                FullName = user.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.APPReturnLoan.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = user.UserId,
                                ShopId = 0,
                                ShopName = "APP"
                            };

                            _unitOfWork.LoanBriefNoteRepository.Insert(note);
                        }
                        else if (loanInfo.Status == EnumLoanStatus.WAIT_HUB_EMPLOYEE_PREPARE_LOAN.GetHashCode())
                        {
                            _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote
                            {
                                LoanBriefId = req.LoanId,
                                Note = string.Format("Thẩm định viên đẩy đơn xuống TLS. Commnet: {0}", req.Note),
                                FullName = user.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = user.UserId,
                                ShopId = 0,
                                ShopName = "APP"
                            });
                        }
                        _unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tìm thấy thông tin người dùng");
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/PushLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("AddScheduleTime")]
        public ActionResult<DefaultResponse<Meta>> AddScheduleTime([FromBody] ScheduleTimeHubReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                var userId = GetUserId();
                userId = 50203;
                var user = _unitOfWork.UserRepository.Query(x => x.UserId == userId, null, false).Select(LOS.DAL.DTOs.UserDetail.ProjectionDetail).FirstOrDefault();
                var hubId = user.ListShop[0].ShopId;
                if (req.LoanBriefId <= 0 || req.LoanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = _unitOfWork.LoanBriefRepository.GetById(req.LoanBriefId);
                if (loanInfo != null)
                {
                    _unitOfWork.ScheduleTimeRepository.Insert(new ScheduleTime()
                    {
                        LoanBriefId = req.LoanBriefId,
                        UserId = userId,
                        HubId = hubId,
                        CreatedTime = DateTime.Now,
                        UpdateAt = DateTime.Now,
                        StartTime = DateTime.ParseExact(req.StartTime, "dd/MM/yyyy HH:mm", null),
                        EndTime = DateTime.ParseExact(req.EndTime, "dd/MM/yyyy HH:mm", null),
                        Status = 1,
                        Note = req.Comment,
                        CusName = loanInfo.FullName,
                        UserFullName = user.FullName
                    });
                    //Update vào trường ScheduleTime trong LoanBrief
                    _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief()
                    {
                        ScheduleTime = DateTime.ParseExact(req.StartTime, "dd/MM/yyyy HH:mm", null)
                    });
                    //Add note
                    _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                    {
                        LoanBriefId = loanInfo.LoanBriefId,
                        Note = string.Format("{0} đã đặt lịch hẹn : {1} - {2} với nội dung: {3}", user.FullName, req.StartTime, req.EndTime, req.Comment.ToString()),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.ScheduleTime.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = 0,
                        ShopName = "APP"

                    });
                    _unitOfWork.Save();
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Loanbrief/AddScheduleHub Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("GetScheduleTime")]
        public ActionResult<DefaultResponse<Meta, List<ScheduleTime>>> GetScheduleTimeHub([FromBody] GetListScheduleTimeHubReq req)
        {
            var def = new DefaultResponse<Meta, List<ScheduleTime>>();
            try
            {
                var start = DateTime.ParseExact(req.StartTime, "dd/MM/yyyy HH:mm", null);
                var end = DateTime.ParseExact(req.EndTime, "dd/MM/yyyy HH:mm", null);
                var userId = GetUserId();

                //var start = DateTime.ParseExact(req.StartTime, "dd/MM/yyyy HH:mm", null);
                //var end = DateTime.ParseExact(req.EndTime, "dd/MM/yyyy HH:mm", null);

                var data = _unitOfWork.ScheduleTimeRepository.Query(x => (x.UserId == userId || userId == 0)
                && x.StartTime >= start && x.EndTime <= end, null, false).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/GetScheduleTimeHub Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetScheduleTimeByLoan")]
        public ActionResult<DefaultResponse<Meta, List<ScheduleTime>>> GetScheduleByLoan(int loanBriefId)
        {
            var def = new DefaultResponse<Meta, List<ScheduleTime>>();
            try
            {

                var data = _unitOfWork.ScheduleTimeRepository.Query(x => x.LoanBriefId == loanBriefId, null, false).OrderByDescending(x => x.CreatedTime).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/GetScheduleByLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        private void LogRequest(string name, string json)
        {
            try
            {
                Log.Information(string.Format("{0}: {1}", name, json));
            }
            catch
            {

            }
        }

        [AllowAnonymous]
        [Route("UploadVideo")]
        [HttpPost]
        public async Task<IActionResult> UploadVideo(IFormFile files, int LoanBriefId, int TypeId, string LatLong, DateTime? CreatedTime = null)
        {
            var userId = GetUserId();
            if (userId == 0)
                userId = 1;
            //var user = _unitOfWork.UserRepository.GetById(userId);
            var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
            var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var BucketName = _baseConfig["AppSettings:BucketName"];
            var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
            {
                RegionEndpoint = RegionEndpoint.APSoutheast1
            });
            var link = "";
            var folder = BucketName + "/uploads/LOS/video/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
            var lst = new List<LoanBriefFiles>();
            var def = new DefaultResponse<Meta, List<LoanBriefFiles>>();
            if (files == null || LoanBriefId <= 0 || TypeId <= 0)
            {
                def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                return Ok(def);
            }
            try
            {
                // check if loan action exists
                var loanInfo = _unitOfWork.LoanBriefRepository.GetById(LoanBriefId);
                if (loanInfo != null)
                {
                    var extension = Path.GetExtension(files.FileName);
                    var allowedExtensions = new[] { ".mp4", ".webm", ".ogg", ".mov" };
                    if (allowedExtensions.Contains(extension.ToLower()))
                    {
                        string ImageName = "";
                        if (!string.IsNullOrEmpty(LatLong))
                        {
                            ImageName = string.Format("{0}-{1}{2}", LatLong.Replace(",", "-"), Guid.NewGuid().ToString(), extension);
                        }
                        else
                        {
                            ImageName = Guid.NewGuid().ToString() + extension;
                        }
                        using (var stream = new MemoryStream())
                        {
                            files.CopyTo(stream);
                            PutObjectRequest req = new PutObjectRequest()
                            {
                                InputStream = stream,
                                BucketName = folder,
                                Key = ImageName,
                                CannedACL = S3CannedACL.PublicRead
                            };
                            await client.PutObjectAsync(req);
                            link = "/uploads/LOS/video/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                            //insert to db
                            var obj = new LoanBriefFiles
                            {
                                LoanBriefId = LoanBriefId,
                                CreateAt = CreatedTime.HasValue ? CreatedTime : DateTime.Now,
                                FilePath = link,
                                UserId = userId,
                                Status = 1,
                                TypeId = TypeId,
                                S3status = 1,
                                MecashId = 1000000000,
                                TypeFile = 2,
                                LatLong = LatLong,
                                SourceUpload = (int)EnumSourceUpload.Mobile
                            };
                            _unitOfWork.LoanBriefFileRepository.Insert(obj);
                            lst.Add(obj);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(211, "Định dạng video không được hỗ trợ!");
                        return Ok(def);
                    }

                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = lst.ToList();
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/UploadVideo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("GetStatus")]
        public ActionResult<DefaultResponse<Meta, List<AppAPI.DTOs.LoanStatusDetail.LoanStatusDetail>>> GetStatus(int loanBriefId)
        {
            var def = new DefaultResponse<Meta, List<AppAPI.DTOs.LoanStatusDetail.LoanStatusDetail>>();
            var data = new List<AppAPI.DTOs.LoanStatusDetail.LoanStatusDetail>();
            try
            {
                if (loanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanbrief = _unitOfWork.LoanBriefRepository.GetById(loanBriefId);
                if (loanbrief != null)
                {
                    var lstStatusDetail = _unitOfWork.LoanStatusDetailRepository.Query(x => x.Type == (int)EnumLoanStatusDetail.CVKD && x.IsEnable == true && x.ParentId.GetValueOrDefault() == 0, null, false).ToList();
                    var nextStep = "";
                    var lstConfigStep = _unitOfWork.LoanConfigStepRepository.Query(x => x.IsEnable == true, null, false).ToList();
                    if (lstConfigStep != null && lstConfigStep.Count > 0)
                    {
                        //check LoanStatusDetail nếu không có value set mặc định là Step1
                        if (loanbrief.LoanStatusDetail.GetValueOrDefault(0) == 0)
                            nextStep = "2";
                        else
                            nextStep = _unitOfWork.LoanConfigStepRepository.Query(x => x.LoanStatusDetailId == loanbrief.LoanStatusDetail.Value && x.IsEnable == true, null, false).FirstOrDefault().NextStep;

                        List<int> ListStep = null;
                        if (!string.IsNullOrEmpty(nextStep))
                        {
                            ListStep = nextStep.Split(",").Select<string, int>(int.Parse).ToList();
                        }
                        lstConfigStep = lstConfigStep.Where(x => ListStep.Contains(x.CurrentStep.Value)).ToList();

                        if (lstConfigStep != null && lstConfigStep.Count > 0)
                        {
                            foreach (var item in lstConfigStep)
                            {
                                var lstStatusDetail2 = lstStatusDetail.FirstOrDefault(x => x.Id == item.LoanStatusDetailId);
                                if (lstStatusDetail2 != null)
                                {
                                    var lst = _unitOfWork.LoanStatusDetailRepository.Query(x => x.IsEnable == true && x.ParentId == lstStatusDetail2.Id, null, false).ToList();
                                    data.Add(new AppAPI.DTOs.LoanStatusDetail.LoanStatusDetail
                                    {
                                        Id = lstStatusDetail2.Id,
                                        Name = lstStatusDetail2.Name,
                                        lstStatus = lst
                                    });
                                }
                            }

                        }
                    }
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = data;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "App/GetStatus Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("document_source_upload")]
        [HttpGet]
        public async Task<ActionResult<DefaultResponse<DAL.DTOs.DocumentMapInfo>>> ListDocumentSourceUpload(int LoanBriefId, int sourceUpload)
        {
            var def = new DefaultResponse<List<DAL.DTOs.DocumentMapInfo>>();
            if (LoanBriefId <= 0 || sourceUpload <= 0)
            {
                def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                return Ok(def);
            }
            // check if loan action exists
            var loanbrief2 = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId, null, false).Select(x => new
            {
                JobId = x.LoanBriefJob != null ? x.LoanBriefJob.JobId : 0,
                ProductId = x.ProductId,
                ImcomeType = x.LoanBriefJob != null ? x.LoanBriefJob.ImcomeType : 0,
                CompanyInsurance = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyInsurance : null,
                BusinessPapers = x.LoanBriefJob != null ? x.LoanBriefJob.BusinessPapers : null,
                ResidentType = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType : null,
            }).FirstOrDefaultAsync();
            if (loanbrief2 != null)
            {
                //lấy danh sách chứng từ
                var listDocument = _documentService.GetDocumentType(loanbrief2.ProductId.Value, Common.Utils.ProductPriceUtils.ConvertEnumTypeOfOwnerShipDocument(loanbrief2.ResidentType.Value));
                if (listDocument != null && listDocument.Count > 0)
                {
                    var groupJobId = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(loanbrief2.JobId.Value, loanbrief2.ImcomeType
                                               , loanbrief2.CompanyInsurance, loanbrief2.BusinessPapers);

                    var resultDocuments = LOS.AppAPI.Helpers.Ultility.ConvertDocumentTree(listDocument, new List<DAL.DTOs.LoanBriefFileDetail>(), sourceUpload, groupJobId);
                    def.meta = new Meta(200, "success");
                    def.data = resultDocuments;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không lấy được danh mục chứng từ");
                }
            }
            else
            {
                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                return Ok(def);
            }
            return Ok(def);
        }

        //Cập nhật token push notification
        [HttpPost]
        [Route("update_token_device")]
        public ActionResult<DefaultResponse<object>> UpdateTokenDevice([FromBody] UpdateTokenReq req)
        {
            var def = new DefaultResponse<object>();
            var userId = GetUserId();
            try
            {
                if (userId > 0)
                {
                    if (!string.IsNullOrEmpty(req.Token))
                    {
                        _unitOfWork.UserRepository.Update(x => x.UserId == userId, x => new DAL.EntityFramework.User()
                        {
                            PushTokenAndroid = req.Token
                        });
                        _unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền token của device");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Không lấy được thông tin user");
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Appraiser/UpdateTokenDevice Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        // [AllowAnonymous]
        [Route("upload_contract_scan")]
        [HttpPost]
        public async Task<IActionResult> UploadContractScan(IFormFile fileScan, int LoanBriefId, bool? scanContractStatus)
        {
            var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
            var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
            var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
            var BucketName = _baseConfig["AppSettings:BucketName"];
            var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
            {
                RegionEndpoint = RegionEndpoint.APSoutheast1
            });

            var folder = BucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
            var lst = new List<LoanBriefFiles>();
            var def = new DefaultResponse<Meta, List<LoanBriefFiles>>();
            if (fileScan == null || LoanBriefId <= 0)
            {
                def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                return Ok(def);
            }
            var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId, null, false).Select(x => new
            {
                LoanbriefId = x.LoanBriefId
            }).FirstOrDefault();
            if (loanInfo != null)
            {
                try
                {
                    var link = "";
                    var linkThumb = "";
                    var extension = Path.GetExtension(fileScan.FileName);
                    var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                    if (allowedExtensions.Contains(extension.ToLower()))
                    {
                        string ImageName = Guid.NewGuid().ToString() + extension;
                        string ImageThumb = Guid.NewGuid().ToString() + "-thumb" + extension;

                        using (var stream = new MemoryStream())
                        {
                            fileScan.CopyTo(stream);
                            PutObjectRequest req = new PutObjectRequest()
                            {
                                InputStream = stream,
                                BucketName = folder,
                                Key = ImageName,
                                CannedACL = S3CannedACL.PublicRead
                            };
                            await client.PutObjectAsync(req);
                            //Crop image
                            using var image = Image.Load(fileScan.OpenReadStream());
                            if (image.Width > 500)
                            {
                                image.Mutate(x => x.Resize(200, image.Height / (image.Width / 200)));

                                var outputStream = new MemoryStream();
                                image.Save(outputStream, new JpegEncoder());
                                outputStream.Seek(0, 0);
                                //Upload Thumb
                                PutObjectRequest reqThumb = new PutObjectRequest()
                                {
                                    InputStream = outputStream,
                                    BucketName = folder,
                                    Key = ImageThumb,
                                    CannedACL = S3CannedACL.PublicRead
                                };
                                await client.PutObjectAsync(reqThumb);
                                linkThumb = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + ImageThumb;
                            }
                            //End
                            link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                            //insert to db
                            var obj = new LoanBriefFiles
                            {
                                LoanBriefId = LoanBriefId,
                                CreateAt = DateTime.Now,
                                FilePath = link,
                                UserId = 0,
                                Status = 1,
                                TypeId = (int)EnumDocumentType.ScanContract,
                                S3status = 1,
                                MecashId = 1000000000,
                                SourceUpload = (int)EnumSourceUpload.Mobile,
                                FileThumb = linkThumb
                            };
                            _unitOfWork.LoanBriefFileRepository.Insert(obj);
                            if (scanContractStatus.HasValue)
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                                {
                                    ScanContractStatus = scanContractStatus
                                });
                            }
                            _unitOfWork.Save();
                            lst.Add(obj);
                        }

                        //cập nhật trạng thái scan h
                    }
                    else
                    {
                        def.meta = new Meta(211, "Ảnh không đúng định dạng!");
                        return Ok(def);
                    }

                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = lst.ToList();
                    if (def.data != null && def.data.Count > 0)
                    {
                        foreach (var item in def.data)
                        {
                            item.FilePath = ServiceURL + item.FilePath;
                            item.FileThumb = ServiceURL + item.FileThumb;
                        }

                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Appraiser/UploadContractScan Exception");
                    def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                }

            }
            else
            {
                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                return Ok(def);
            }

            return Ok(def);
        }
    }

}