﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using LOS.AppAPI.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LOS.AppAPI.Controllers
{
    public class BaseController : ControllerBase
    {
        protected int GetUserId()
        {
            var identity = (ClaimsIdentity)User.Identity;
            return int.Parse(identity.Claims.Where(c => c.Type == "Id").Select(c => c.Value).SingleOrDefault());
        }
        protected UserInfoToken GetUserInfo()
        {
            var userInfo = new UserInfoToken();
            var identity = (ClaimsIdentity)User.Identity;
            userInfo.UserId = int.Parse(identity.Claims.Where(c => c.Type == "Id").Select(c => c.Value).SingleOrDefault());
            userInfo.GroupId = int.Parse(identity.Claims.Where(c => c.Type == "GroupId").Select(c => c.Value).SingleOrDefault());
            return userInfo;
        }
    }
}