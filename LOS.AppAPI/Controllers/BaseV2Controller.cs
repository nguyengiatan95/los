﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using LOS.AppAPI.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LOS.AppAPI.Controllers
{
    public class BaseV2Controller : ControllerBase//, IDisposable
    {
        //private IUnitOfWork _unitOfWork;
        //public BaseV2Controller(IUnitOfWork unitOfWork)
        //{
        //    _unitOfWork = unitOfWork;
        //}
        //public void Dispose()
        //{
        //    _unitOfWork.Dispose();
        //}

        protected int GetUserId()
        {
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                return int.Parse(identity.Claims.Where(c => c.Type == "Id").Select(c => c.Value).SingleOrDefault());
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        protected UserInfoToken GetUserInfo()
        {

            try
            {
                var userInfo = new UserInfoToken();
                var identity = (ClaimsIdentity)User.Identity;
                userInfo.UserId = int.Parse(identity.Claims.Where(c => c.Type == "Id").Select(c => c.Value).SingleOrDefault());
                userInfo.GroupId = int.Parse(identity.Claims.Where(c => c.Type == "GroupId").Select(c => c.Value).SingleOrDefault());
                return userInfo;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}