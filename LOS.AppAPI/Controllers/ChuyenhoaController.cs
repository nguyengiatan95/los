﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using LOS.AppAPI.DTOs.Appraiser;
using LOS.AppAPI.DTOs.ChuyenHoaDTO;
using LOS.AppAPI.DTOs.LMS;
using LOS.AppAPI.Helpers;
using LOS.Common.Extensions;
using LOS.DAL.Dapper;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Processing;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [AuthorizeToken("APP_API_CHUYEN_HOA")]
    public class ChuyenhoaController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _baseConfig;

        public ChuyenhoaController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._baseConfig = configuration;
        }

        [HttpGet]
        [Route("get_telesale")]
        public async Task<ActionResult<DefaultResponse<List<TelesaleDetail>>>> GetAllTelesale()
        {
            var users = await _unitOfWork.UserRepository.Query(x => x.GroupId == (int)EnumGroupUser.Telesale && x.Status == 1, null, false)
                .Select(TelesaleDetail.ProjectionDetail).ToListAsync();
            var def = new DefaultResponse<List<TelesaleDetail>>();
            def.meta = new Meta(200, "success");
            def.data = users;
            return Ok(def);
        }

        [HttpGet]
        [Route("all_comment")]
        public async Task<ActionResult<DefaultResponse<Meta, List<CommentDetail>>>> GetComment([FromQuery] int LoanbriefId, [FromQuery] int page = 1, [FromQuery] int pageSize = 20)
        {
            var def = new DefaultResponse<SummaryMeta, List<CommentDetail>>();
            try
            {
                if (LoanbriefId <= 0)
                {
                    def.meta = new SummaryMeta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE, 0, 0, 0);
                    return Ok(def);
                }
                var query = _unitOfWork.LoanBriefNoteRepository.Query(x => x.LoanBriefId == LoanbriefId, null, false).Select(CommentDetail.ProjectionDetail);
                var totalRecords = query.Count();
                var data = await query.OrderByDescending(x => x.LoanBriefNoteId).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Chuyenhoa/GetComment Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_comment")]
        public ActionResult<DefaultResponse<object>> AddNote(CommentReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.LoanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                if (_unitOfWork.LoanBriefRepository.Any(x => x.LoanBriefId == req.LoanbriefId))
                {
                    var note = new LoanBriefNote
                    {
                        LoanBriefId = req.LoanbriefId,
                        Note = req.Note,
                        FullName = req.UserName,
                        Status = 1,
                        ActionComment = EnumActionComment.ChuyenHoaComment.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = req.UserId,
                        ShopId = 0,
                        ShopName = "Chuyển hóa"
                    };
                    _unitOfWork.LoanBriefNoteRepository.Insert(note);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tồn tại đơn vay trong hệ thống");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ChuyenHoa/AddNote Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("list_loanbrief")]
        public async Task<ActionResult<DefaultResponse<Meta, List<ReportTelesaleDetail>>>> GetLoanbriefTelesale([FromQuery] int LoanStatus, [FromQuery] string Search = ""
            , [FromQuery] int telesale = -1, [FromQuery] int cityId = -1, [FromQuery] int districtId = -1, [FromQuery] int hubId = -1, [FromQuery] int page = 1, [FromQuery] int pageSize = 20)
        {
            var def = new DefaultResponse<SummaryMeta, List<ReportTelesaleDetail>>();
            try
            {
                //Telesale đang xử lý: 0 -> 15 ngày  từ ngày tạo
                if (LoanStatus == 1)
                {
                    var query = _unitOfWork.LoanBriefRepository.Query(x => (x.Status == (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN || x.Status == (int)EnumLoanStatus.TELESALE_ADVICE)
                                    && (telesale == -1 || x.BoundTelesaleId == telesale) && (cityId == -1 || x.ProvinceId == cityId)
                                    && (districtId == -1 || x.DistrictId == districtId)
                                    && (hubId == -1 || x.HubId == hubId)
                                    && (string.IsNullOrEmpty(Search) || x.LoanBriefId.ToString() == Search.Trim() || x.FullName.Contains(Search.Trim()))
                                    && x.CreatedTime.Value.AddDays(15).Date >= DateTime.Now.Date
                                     , null, false).Select(ReportTelesaleDetail.ProjectionDetail);
                    var totalRecord = query.Count();
                    var totalLoanAmount = query.Sum(x => x.LoanAmount);
                    var totalWaitCustomerPrepare = query.Count(x => x.Status == (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN);
                    var data = await query.OrderBy(x => x.Status).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                    if (data != null && data.Count > 0)
                    {
                        data[0].TotalWaitCustomerPrepare = totalWaitCustomerPrepare;
                        data[0].TotalLoanAmount = totalLoanAmount;
                    }
                    def.data = data;
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecord);
                    return Ok(def);
                }
                //Telesale quá hạn xử lý => quá 15 ngày
                else if (LoanStatus == 2)
                {
                    var query = _unitOfWork.LoanBriefRepository.Query(x => (x.Status == (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN || x.Status == (int)EnumLoanStatus.TELESALE_ADVICE)
                                    && (telesale == -1 || x.BoundTelesaleId == telesale) && (cityId == -1 || x.ProvinceId == cityId)
                                    && (districtId == -1 || x.DistrictId == districtId)
                                     && (hubId == -1 || x.HubId == hubId)
                                    && (string.IsNullOrEmpty(Search) || x.LoanBriefId.ToString() == Search.Trim() || x.FullName.Contains(Search.Trim()))
                                    && x.CreatedTime.Value.AddDays(15).Date < DateTime.Now.Date
                                     , null, false).Select(ReportTelesaleDetail.ProjectionDetail);
                    var totalRecord = query.Count();
                    var data = await query.OrderByDescending(x => x.LoanCreditID).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                    def.data = data;
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecord);
                    return Ok(def);
                }
                //Telesale hủy đơn
                else if (LoanStatus == 3)
                {
                    var query = _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.CANCELED
                                    && (telesale == -1 || x.BoundTelesaleId == telesale) && (cityId == -1 || x.ProvinceId == cityId)
                                    && (districtId == -1 || x.DistrictId == districtId)
                                     && (hubId == -1 || x.HubId == hubId)
                                    && (string.IsNullOrEmpty(Search) || x.LoanBriefId.ToString() == Search.Trim() || x.FullName.Contains(Search.Trim()))
                                    && x.LoanBriefCancelByNavigation.GroupId == (int)EnumGroupUser.Telesale
                                    && (x.LoanBriefCancelAt.HasValue && x.LoanBriefCancelAt.Value.Date == DateTime.Now.Date)
                                     , null, false).Select(ReportTelesaleDetail.ProjectionDetail);
                    var totalRecord = query.Count();
                    var data = await query.OrderByDescending(x => x.LoanCreditID).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                    def.data = data;
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecord);
                    return Ok(def);
                }
                //Phòng GD xử lý => những đơn đang ở phòng GD + Có telesale xử lý
                else if (LoanStatus == 4)
                {
                    var query = _unitOfWork.LoanBriefRepository.Query(x => (x.Status == (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING || x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW || x.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE)
                                  && x.BoundTelesaleId > 0
                                  && (hubId == -1 || x.HubId == hubId)
                                  && (telesale == -1 || x.BoundTelesaleId == telesale) && (cityId == -1 || x.ProvinceId == cityId)
                                  && (districtId == -1 || x.DistrictId == districtId)
                                  && (string.IsNullOrEmpty(Search) || x.LoanBriefId.ToString() == Search.Trim() || x.FullName.Contains(Search.Trim()))
                                   , null, false).Select(ReportTelesaleDetail.ProjectionDetail);
                    var totalRecord = query.Count();
                    var totalLoanAmount = query.Sum(x => x.LoanAmount);
                    var totalWaitHubDistributing = query.Count(x => x.Status == (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING);
                    var totalWaitAppraiser = query.Count(x => x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW);
                    var totalHubWaitApprove = query.Count(x => x.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE);

                    var data = await query.OrderByDescending(x => x.Status).ThenByDescending(x => x.TelesalesPushAt.HasValue).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

                    var dicPushHub = new Dictionary<int, PipelineHistory>();
                    var listLoanId = data.Select(x => x.LoanCreditID).Distinct().ToList();
                    if (listLoanId != null && listLoanId.Count > 0)
                    {
                        var listHistory = _unitOfWork.PipelineHistoryRepository.Query(x => listLoanId.Contains(x.LoanBriefId.Value)
                        && x.EndStatus == (int)EnumLoanStatus.APPRAISER_REVIEW, null, false).ToList();
                        if (listHistory != null && listHistory.Count > 0)
                        {
                            foreach (var item in listHistory)
                            {
                                if (item.LoanBriefId > 0 && !dicPushHub.ContainsKey(item.LoanBriefId.Value))
                                    dicPushHub[item.LoanBriefId.Value] = item;
                            }
                        }
                    }
                    if (data != null && data.Count > 0)
                    {
                        bool firstRecord = true;
                        foreach (var item in data)
                        {
                            if (firstRecord)
                            {
                                item.TotalWaitHubDistributing = totalWaitHubDistributing;
                                item.TotalWaitAppraiser = totalWaitAppraiser;
                                item.TotalHubWaitApprove = totalHubWaitApprove;
                                item.TotalLoanAmount = totalLoanAmount;
                                firstRecord = false;
                            }
                            if (dicPushHub.ContainsKey(item.LoanCreditID))
                                item.FirstTimeHubReceived = dicPushHub[item.LoanCreditID].EndTime.HasValue
                                    ? dicPushHub[item.LoanCreditID].EndTime.Value.DateTime : dicPushHub[item.LoanCreditID].CreatedDate.Value.DateTime;
                        }
                    }
                    def.data = data;
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecord);
                    return Ok(def);
                }
                //phòng GD hủy đơn
                else if (LoanStatus == 5)
                {
                    var query = _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.CANCELED
                                    && (telesale == -1 || x.BoundTelesaleId == telesale) && (cityId == -1 || x.ProvinceId == cityId)
                                    && (districtId == -1 || x.DistrictId == districtId)
                                    && (hubId == -1 || x.HubId == hubId)
                                    && (string.IsNullOrEmpty(Search) || x.LoanBriefId.ToString() == Search.Trim() || x.FullName.Contains(Search.Trim()))
                                    && (x.LoanBriefCancelByNavigation.GroupId == (int)EnumGroupUser.ManagerHub || x.LoanBriefCancelByNavigation.GroupId == (int)EnumGroupUser.StaffHub)
                                    && (x.LoanBriefCancelAt.HasValue && x.LoanBriefCancelAt.Value.Date == DateTime.Now.Date)
                                    , null, false).Select(ReportTelesaleDetail.ProjectionDetail);
                    var totalRecord = query.Count();
                    var data = await query.OrderByDescending(x => x.LoanCreditID).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

                    var dicPushHub = new Dictionary<int, PipelineHistory>();
                    var listLoanId = data.Select(x => x.LoanCreditID).Distinct().ToList();
                    if (listLoanId != null && listLoanId.Count > 0)
                    {
                        var listHistory = _unitOfWork.PipelineHistoryRepository.Query(x => listLoanId.Contains(x.LoanBriefId.Value)
                            && x.EndStatus == (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING, null, false).ToList();
                        if (listHistory != null && listHistory.Count > 0)
                        {
                            foreach (var item in listHistory)
                            {
                                if (item.LoanBriefId > 0)
                                {
                                    if (!dicPushHub.ContainsKey(item.LoanBriefId.Value))
                                        dicPushHub[item.LoanBriefId.Value] = item;
                                }
                            }
                        }
                    }
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            if (dicPushHub.ContainsKey(item.LoanCreditID))
                                item.FirstTimeHubReceived = dicPushHub[item.LoanCreditID].EndTime.HasValue
                                    ? dicPushHub[item.LoanCreditID].EndTime.Value.DateTime : dicPushHub[item.LoanCreditID].CreatedDate.Value.DateTime;
                        }
                    }
                    def.data = data;
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecord);
                    return Ok(def);
                }
                //Đã GN
                else if (LoanStatus == 6)
                {
                    var query = _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.DISBURSED
                                    && (telesale == -1 || x.BoundTelesaleId == telesale) && (cityId == -1 || x.ProvinceId == cityId)
                                    && (districtId == -1 || x.DistrictId == districtId)
                                    && x.TypeRemarketing.GetValueOrDefault(0) != (int)EnumTypeRemarketing.DebtRevolvingLoan
                                    && (hubId == -1 || x.HubId == hubId)
                                    && (string.IsNullOrEmpty(Search) || x.LoanBriefId.ToString() == Search.Trim() || x.FullName.Contains(Search.Trim()))
                                    && (x.DisbursementAt.HasValue && x.DisbursementAt.Value.Date == DateTime.Now.Date)
                                    , null, false).Select(ReportTelesaleDetail.ProjectionDetail);
                    var totalRecord = query.Count();
                    var totalLoanAmount = query.Sum(x => x.LoanAmount);
                    var data = await query.OrderByDescending(x => x.FirstTimeAccountantDisbursement).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

                    var dicPushHub = new Dictionary<int, PipelineHistory>();
                    var listLoanId = data.Select(x => x.LoanCreditID).Distinct().ToList();
                    if (listLoanId != null && listLoanId.Count > 0)
                    {
                        var listHistory = _unitOfWork.PipelineHistoryRepository.Query(x => listLoanId.Contains(x.LoanBriefId.Value)
                            && x.EndStatus == (int)EnumLoanStatus.WAIT_DISBURSE, null, false).ToList();
                        if (listHistory != null && listHistory.Count > 0)
                        {
                            foreach (var item in listHistory)
                            {
                                if (item.LoanBriefId > 0)
                                {
                                    if (!dicPushHub.ContainsKey(item.LoanBriefId.Value))
                                        dicPushHub[item.LoanBriefId.Value] = item;
                                }
                            }
                        }
                    }
                    if (data != null && data.Count() > 0)
                    {
                        var firstRecord = true;
                        foreach (var item in data)
                        {
                            if (firstRecord)
                            {
                                item.TotalLoanAmount = totalLoanAmount;
                                firstRecord = false;
                            }
                            if (dicPushHub.ContainsKey(item.LoanCreditID))
                                item.FirstTimeAccountantReceived = dicPushHub[item.LoanCreditID].EndTime.HasValue
                                    ? dicPushHub[item.LoanCreditID].EndTime.Value.DateTime : dicPushHub[item.LoanCreditID].CreatedDate.Value.DateTime;
                        }
                    }
                    def.data = data;
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecord);
                    return Ok(def);
                }
                //TĐPD xử lý
                else if (LoanStatus == 7)
                {
                    var query = _unitOfWork.LoanBriefRepository.Query(x => (x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_REVIEW
                                        || x.Status == (int)EnumLoanStatus.WAIT_APPRAISER_CONFIRM_CUSTOMER
                                        || x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_APPROVE_PROPOSION
                                       )// || x.Status == (int)EnumLoanStatus.APPROVAL_LEADER_APPROVE_CANCEL || x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_APPROVE_CANCEL
                                        //&& x.BoundTelesaleId > 0
                                  && x.TypeRemarketing.GetValueOrDefault(0) != (int)EnumTypeRemarketing.DebtRevolvingLoan
                                  && (hubId == -1 || x.HubId == hubId)
                                  && (telesale == -1 || x.BoundTelesaleId == telesale) && (cityId == -1 || x.ProvinceId == cityId)
                                  && (districtId == -1 || x.DistrictId == districtId)
                                  && (string.IsNullOrEmpty(Search) || x.LoanBriefId.ToString() == Search.Trim()
                                  || x.FullName.Contains(Search.Trim()))
                                   , null, false).Select(ReportTelesaleDetail.ProjectionDetail);
                    var totalRecord = query.Count();
                    var totalLoanAmount = query.Sum(x => x.LoanAmount);
                    var data = await query.OrderByDescending(x => x.HubPushAt.HasValue).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

                    var dicPushHub = new Dictionary<int, PipelineHistory>();
                    var listLoanId = data.Select(x => x.LoanCreditID).Distinct().ToList();
                    if (listLoanId != null && listLoanId.Count > 0)
                    {
                        var listHistory = _unitOfWork.PipelineHistoryRepository.Query(x => listLoanId.Contains(x.LoanBriefId.Value)
                        && x.EndStatus == (int)EnumLoanStatus.BRIEF_APPRAISER_REVIEW, null, false).ToList();
                        if (listHistory != null && listHistory.Count > 0)
                        {
                            foreach (var item in listHistory)
                            {
                                if (item.LoanBriefId > 0)
                                {
                                    if (!dicPushHub.ContainsKey(item.LoanBriefId.Value))
                                        dicPushHub[item.LoanBriefId.Value] = item;
                                }
                            }
                        }
                    }
                    if (data != null && data.Count() > 0)
                    {
                        //var firstRecord = true;
                        foreach (var item in data)
                        {
                            //if (firstRecord)
                            //{
                            item.TotalLoanAmount = totalLoanAmount;
                            //    firstRecord = false;
                            //}
                            if (dicPushHub.ContainsKey(item.LoanCreditID))
                                item.FirstTimeTDPDReceived = dicPushHub[item.LoanCreditID].EndTime.HasValue
                                    ? dicPushHub[item.LoanCreditID].EndTime.Value.DateTime : dicPushHub[item.LoanCreditID].CreatedDate.Value.DateTime;
                        }
                    }
                    def.data = data.OrderBy(x => x.FirstTimeTDPDReceived).ToList();
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecord);
                    return Ok(def);
                }
                //thẩm định phê duyệt hủy
                else if (LoanStatus == 8)
                {
                    var query = _unitOfWork.LoanBriefRepository.Query(x => (x.Status == (int)EnumLoanStatus.CANCELED)
                                  && (telesale == -1 || x.BoundTelesaleId == telesale) && (cityId == -1 || x.ProvinceId == cityId)
                                  && (hubId == -1 || x.HubId == hubId)
                                  && (districtId == -1 || x.DistrictId == districtId)
                                  && (x.LoanBriefCancelByNavigation.GroupId == (int)EnumGroupUser.ApproveEmp || x.LoanBriefCancelByNavigation.GroupId == (int)EnumGroupUser.ApproveLeader)
                                  && (x.LoanBriefCancelAt.HasValue && x.LoanBriefCancelAt.Value.Date == DateTime.Now.Date)
                                  && (string.IsNullOrEmpty(Search) || x.LoanBriefId.ToString() == Search.Trim() || x.FullName.Contains(Search.Trim()))
                                   , null, false).Select(ReportTelesaleDetail.ProjectionDetail);
                    var totalRecord = query.Count();
                    var data = await query.OrderByDescending(x => x.LoanCreditID).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

                    var dicPushHub = new Dictionary<int, PipelineHistory>();
                    var listLoanId = data.Select(x => x.LoanCreditID).Distinct().ToList();
                    if (listLoanId != null && listLoanId.Count > 0)
                    {
                        var listHistory = _unitOfWork.PipelineHistoryRepository.Query(x => listLoanId.Contains(x.LoanBriefId.Value)
                        && x.EndStatus == (int)EnumLoanStatus.BRIEF_APPRAISER_REVIEW, null, false).ToList();
                        if (listHistory != null && listHistory.Count > 0)
                        {
                            foreach (var item in listHistory)
                            {
                                if (item.LoanBriefId > 0 && !dicPushHub.ContainsKey(item.LoanBriefId.Value))
                                    dicPushHub[item.LoanBriefId.Value] = item;
                            }
                        }
                    }
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            if (dicPushHub.ContainsKey(item.LoanCreditID))
                                item.FirstTimeTDPDReceived = dicPushHub[item.LoanCreditID].EndTime.HasValue
                                    ? dicPushHub[item.LoanCreditID].EndTime.Value.DateTime : dicPushHub[item.LoanCreditID].CreatedDate.Value.DateTime;
                        }
                    }
                    def.data = data;
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecord);
                    return Ok(def);
                }
                //lender care
                else if (LoanStatus == 9)
                {
                    var query = _unitOfWork.LoanBriefRepository.Query(x => (x.Status == (int)EnumLoanStatus.LENDER_LOAN_DISTRIBUTING
                                        || (x.Status == (int)EnumLoanStatus.WAIT_DISBURSE && x.TypeDisbursement == (int)EnumDisbursement.DISBURSE_LEND))
                                  && (telesale == -1 || x.BoundTelesaleId == telesale) && (cityId == -1 || x.ProvinceId == cityId)
                                  && (hubId == -1 || x.HubId == hubId)
                                  && x.TypeRemarketing.GetValueOrDefault(0) != (int)EnumTypeRemarketing.DebtRevolvingLoan
                                  && (districtId == -1 || x.DistrictId == districtId)
                                  && (string.IsNullOrEmpty(Search) || x.LoanBriefId.ToString() == Search.Trim() || x.FullName.Contains(Search.Trim()))
                                   , null, false).Select(ReportTelesaleDetail.ProjectionDetail);
                    var totalRecord = query.Count();
                    var totalLoanAmount = query.Sum(x => x.LoanAmount);

                    var data = await query.OrderBy(x => x.CoordinatorPushAt.HasValue).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

                    var dicPushHub = new Dictionary<int, PipelineHistory>();
                    var listLoanId = data.Select(x => x.LoanCreditID).Distinct().ToList();
                    if (listLoanId != null && listLoanId.Count > 0)
                    {
                        var listHistory = _unitOfWork.PipelineHistoryRepository.Query(x => listLoanId.Contains(x.LoanBriefId.Value)
                        && x.EndStatus == (int)EnumLoanStatus.LENDER_LOAN_DISTRIBUTING, null, false).ToList();
                        if (listHistory != null && listHistory.Count > 0)
                        {
                            foreach (var item in listHistory)
                            {
                                if (item.LoanBriefId > 0 && !dicPushHub.ContainsKey(item.LoanBriefId.Value))
                                    dicPushHub[item.LoanBriefId.Value] = item;
                            }
                        }
                    }
                    if (data != null && data.Count() > 0)
                    {
                        // var firstRecord = true;
                        foreach (var item in data)
                        {
                            //if (firstRecord)
                            //{
                            item.TotalLoanAmount = totalLoanAmount;
                            //    firstRecord = false;
                            //}
                            if (dicPushHub.ContainsKey(item.LoanCreditID))
                                item.FirstTimeLenderCareReceived = dicPushHub[item.LoanCreditID].EndTime.HasValue
                                    ? dicPushHub[item.LoanCreditID].EndTime.Value.DateTime : dicPushHub[item.LoanCreditID].CreatedDate.Value.DateTime;
                        }
                    }
                    def.data = data.OrderBy(x => x.FirstTimeLenderCareReceived).ToList(); ;
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecord);
                    return Ok(def);
                }
                //Kế toán
                else if (LoanStatus == 10)
                {
                    var query = _unitOfWork.LoanBriefRepository.Query(x => (x.Status == (int)EnumLoanStatus.WAIT_DISBURSE && x.TypeDisbursement != (int)EnumDisbursement.DISBURSE_LEND)
                                 && (telesale == -1 || x.BoundTelesaleId == telesale) && (cityId == -1 || x.ProvinceId == cityId)
                                 && (hubId == -1 || x.HubId == hubId)
                                 && x.TypeRemarketing.GetValueOrDefault(0) != (int)EnumTypeRemarketing.DebtRevolvingLoan
                                 && (districtId == -1 || x.DistrictId == districtId)
                                 && (string.IsNullOrEmpty(Search) || x.LoanBriefId.ToString() == Search.Trim() || x.FullName.Contains(Search.Trim()))
                                  , null, false).Select(ReportTelesaleDetail.ProjectionDetail);
                    var totalRecord = query.Count();
                    var totalLoanAmount = query.Sum(x => x.LoanAmount);

                    var data = await query.OrderBy(x => x.CoordinatorPushAt).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

                    var dicPushHub = new Dictionary<int, PipelineHistory>();
                    var listLoanId = data.Select(x => x.LoanCreditID).Distinct().ToList();
                    if (listLoanId != null && listLoanId.Count > 0)
                    {
                        var listHistory = _unitOfWork.PipelineHistoryRepository.Query(x => listLoanId.Contains(x.LoanBriefId.Value)
                        && x.EndStatus == (int)EnumLoanStatus.WAIT_DISBURSE, null, false).ToList();
                        if (listHistory != null && listHistory.Count > 0)
                        {
                            foreach (var item in listHistory)
                            {
                                if (item.LoanBriefId > 0 && !dicPushHub.ContainsKey(item.LoanBriefId.Value))
                                    dicPushHub[item.LoanBriefId.Value] = item;
                            }
                        }
                    }
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            item.TotalLoanAmount = totalLoanAmount;
                            if (dicPushHub.ContainsKey(item.LoanCreditID))
                                item.FirstTimeAccountantDisbursement = dicPushHub[item.LoanCreditID].EndTime.HasValue
                                    ? dicPushHub[item.LoanCreditID].EndTime.Value.DateTime : dicPushHub[item.LoanCreditID].CreatedDate.Value.DateTime;
                        }
                    }
                    def.data = data.OrderBy(x => x.FirstTimeAccountantDisbursement).ToList();
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecord);
                    return Ok(def);
                }
                else
                {
                    def.meta = new SummaryMeta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE, 0, 0, 0);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Chuyenhoa/GetLoanbriefTelesale Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_latlng")]
        public async Task<ActionResult<DefaultResponse<Meta, LatLngDetail>>> GetLatLngAddress([FromQuery] int LoanbriefId)
        {
            var def = new DefaultResponse<Meta, LatLngDetail>();
            try
            {
                var data = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanbriefId, null, false).Select(LatLngDetail.ProjectionDetail).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Chuyenhoa/GetLatLngAddress Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("upload_files")]
        [HttpPost]
        public async Task<IActionResult> UploadImage(List<IFormFile> files, [FromQuery] int loanBriefId = 0, [FromQuery] int typeId = 0)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefFiles>>();
            if (files == null || loanBriefId <= 0 || typeId <= 0)
            {
                def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                return Ok(def);
            }
            var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
            var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
            var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
            var BucketName = _baseConfig["AppSettings:BucketName"];
            var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
            {
                RegionEndpoint = RegionEndpoint.APSoutheast1
            });
            var folder = BucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
            var lst = new List<LoanBriefFiles>();

            if (_unitOfWork.LoanBriefRepository.Any(x => x.LoanBriefId == loanBriefId))
            {
                try
                {
                    foreach (var file in files)
                    {
                        var link = "";
                        var linkThumb = "";
                        var extension = Path.GetExtension(file.FileName);
                        var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                        if (allowedExtensions.Contains(extension.ToLower()))
                        {
                            string ImageName = Guid.NewGuid().ToString() + extension;
                            string ImageThumb = Guid.NewGuid().ToString() + "-thumb" + extension;
                            using (var stream = new MemoryStream())
                            {
                                file.CopyTo(stream);
                                PutObjectRequest req = new PutObjectRequest()
                                {
                                    InputStream = stream,
                                    BucketName = folder,
                                    Key = ImageName,
                                    CannedACL = S3CannedACL.PublicRead
                                };
                                await client.PutObjectAsync(req);
                                //Crop image
                                using var image = Image.Load(file.OpenReadStream());
                                if (image.Width > 500)
                                {
                                    image.Mutate(x => x.Resize(200, image.Height / (image.Width / 200)));

                                    var outputStream = new MemoryStream();
                                    image.Save(outputStream, new JpegEncoder());
                                    outputStream.Seek(0, 0);
                                    //Upload Thumb
                                    PutObjectRequest reqThumb = new PutObjectRequest()
                                    {
                                        InputStream = outputStream,
                                        BucketName = folder,
                                        Key = ImageThumb,
                                        CannedACL = S3CannedACL.PublicRead
                                    };
                                    await client.PutObjectAsync(reqThumb);
                                    linkThumb = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + ImageThumb;
                                }
                                //End
                                link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                                //insert to db
                                var obj = new LoanBriefFiles
                                {
                                    LoanBriefId = loanBriefId,
                                    CreateAt = DateTime.Now,
                                    FilePath = link,
                                    UserId = 0,
                                    Status = 1,
                                    TypeId = typeId,
                                    S3status = 1,
                                    MecashId = 1000000000,
                                    SourceUpload = (int)EnumSourceUpload.Web,
                                    FileThumb = linkThumb
                                };
                                _unitOfWork.LoanBriefFileRepository.Insert(obj);
                                _unitOfWork.Save();
                                lst.Add(obj);
                            }
                        }
                        else
                        {
                            def.meta = new Meta(211, "Ảnh không đúng định dạng!");
                            return Ok(def);
                        }

                    }
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = lst.ToList();
                    if (def.data != null && def.data.Count > 0)
                    {
                        foreach (var item in def.data)
                        {
                            item.FilePath = ServiceURL + item.FilePath;
                            item.FileThumb = ServiceURL + item.FileThumb;
                        }

                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "ChuyenHoa/UploadImage Exception");
                    def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                }

            }
            else
            {
                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                return Ok(def);
            }

            return Ok(def);
        }

        [HttpGet]
        [Route("get_files")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LoanBriefFilesDetail>>>> GetFiles(int loanBriefId, int typeId)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefFilesDetail>>();
            try
            {
                if (loanBriefId <= 0 || typeId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var data = await _unitOfWork.LoanBriefFileRepository.Query(x => x.IsDeleted == 0 && x.TypeId == typeId && x.LoanBriefId == loanBriefId, null, false).Select(LoanBriefFilesDetail.ProjectionDetail).ToListAsync();
                if (data != null && data.Count > 0)
                {
                    var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                    var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
                    var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
                    await Task.Run(() => Parallel.ForEach(data, item =>
                    {
                        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                        {
                            item.FilePath = ServiceURL + item.FilePath;
                            if (!string.IsNullOrEmpty(item.FileThumb))
                                item.FileThumb = ServiceURL + item.FileThumb;
                        }
                        else if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null))
                        {
                            item.FilePath = ServiceURLAG + item.FilePath;
                            if (!string.IsNullOrEmpty(item.FileThumb))
                                item.FileThumb = ServiceURLAG + item.FileThumb;
                        }
                        else if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        {
                            item.FilePath = ServiceURLFileTima + item.FilePath;
                            if (!string.IsNullOrEmpty(item.FileThumb))
                                item.FileThumb = ServiceURLFileTima + item.FileThumb;
                        }
                    }));
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ChuyenHoa/GetFiles Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_staff_hub")]
        public async Task<ActionResult<DefaultResponse<List<StafHubDetail>>>> GetStaffHub()
        {
            var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
            var sql = new StringBuilder();
            sql.AppendLine("select u.UserId as Id, u.Username, u.FullName, s.Name as HubName, pr.Name as CityName, u.IpPhone ");
            sql.AppendLine("from [User](nolock) u ");
            sql.AppendLine("inner join UserShop(nolock)us on us.UserId = u.UserId ");
            sql.AppendLine("inner join Shop(nolock)s on s.ShopID = us.ShopId ");
            sql.AppendLine("inner join Province(nolock) pr on pr.ProvinceId = s.CityId ");
            sql.AppendLine("where u.GroupId = 45 and u.Status = 1 ");
            sql.AppendLine("order by s.ShopID ");
            var users = await db.QueryAsync<StafHubDetail>(sql.ToString());
            var def = new DefaultResponse<List<StafHubDetail>>();
            def.meta = new Meta(200, "success");
            def.data = users;
            return Ok(def);
        }

        [HttpGet]
        [Route("get_hub_by_asm")]
        public async Task<ActionResult<DefaultResponse<Meta, List<HubByAsm>>>> GetHubByAsm()
        {
            var def = new DefaultResponse<Meta, List<HubByAsm>>();
            try
            {
                //lấy ra danh sách các user quản lý khu vực
                var data = await _unitOfWork.UserRepository.Query(x => x.Status == (int)EnumStatusBase.Active && x.IsAsm == true, null, false).Select(HubByAsm.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Chuyenhoa/GetHubByAsm Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }

    public enum EnumLoanStatusReq
    {
        [Description("Telesale đang xử lý")]
        TelesaleHandle = 1,
    }
}
