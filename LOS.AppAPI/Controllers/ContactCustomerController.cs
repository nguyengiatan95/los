﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.AppAPI.DTOs.ContactCustomerDTO;
using LOS.AppAPI.Helpers;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class ContactCustomerController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration configuration;

        public ContactCustomerController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this.configuration = configuration;
        }

        [HttpPost]
        [Route("create_contact_customer")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> CreateContactCustomer(ContactCustomer req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var userId = GetUserId();
                if (string.IsNullOrEmpty(req.FullName))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Bạn chưa chuyền họ tên");
                    return Ok(def);
                }
                if (string.IsNullOrEmpty(req.Phone))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Bạn chưa chuyền số điện thoại");
                    return Ok(def);
                }
                if (!req.Phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không đúng định dạng!");
                    return Ok(def);
                }
                if (_unitOfWork.ContactCustomerRepository.Any(x => x.Phone == req.Phone && x.Status !=0))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại tạo thông tin liên hệ trước đó!");
                    return Ok(def);
                }
                if (_unitOfWork.LoanBriefRepository.Any(x => x.Phone == req.Phone))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại đã tạo thông tin liên hệ trước đó!");
                    return Ok(def);
                }
                req.CreateDate = DateTime.Now;
                req.CreateBy = userId;
                req.Status = 1;
                //insert to db
                _unitOfWork.ContactCustomerRepository.Insert(req);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = req.ContactId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ContactCustomer/CreateContactCustomer Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_contact_customer")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> UpdateContactCustomer(ContactCustomer req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.ContactId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tồn tại thông tin liên hệ khách hàng này");
                    return Ok(def);
                }
                var objContact = _unitOfWork.ContactCustomerRepository.GetFirstOrDefault(x => x.ContactId == req.ContactId);
                if (objContact == null || objContact.ContactId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tồn tại thông tin liên hệ khách hàng này");
                    return Ok(def);
                }
                objContact.FullName = req.FullName;
                objContact.ProvinceId = req.ProvinceId;
                objContact.DistrictId =req.DistrictId;
                objContact.WardId =req.WardId;
                objContact.JobId =req.JobId;
                objContact.Income = req.Income;
                objContact.HaveCarsOrNot = req.HaveCarsOrNot;
                objContact.BrandProductId = req.BrandProductId;
                objContact.ProductId = req.ProductId;
                objContact.ModifyDate = DateTime.Now;
                objContact.Status = 1;
                //insert to db
                _unitOfWork.ContactCustomerRepository.Update(objContact);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = req.ContactId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ContactCustomer/UpdateContactCustomer Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_contact_customer")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> GetContactCustomer(int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var contact = _unitOfWork.ContactCustomerRepository.Query(x => x.ContactId == id, null, false).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = contact;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ContactCustomer/UpdateContactCustomer Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("data_contact_customer")]
        public ActionResult<DefaultResponse<SummaryMeta, List<ContactCustomerDetailsApi>>> DataAllContactCustomer(ContactCustomerReq req)
        {
            var def = new DefaultResponse<SummaryMeta, List<ContactCustomerDetailsApi>>();
            try
            {
                var userId = GetUserId();
                var query = _unitOfWork.ContactCustomerRepository.Query(x => x.CreateBy == userId && x.Status !=0, null, false);
                if (!string.IsNullOrEmpty(req.name))
                {
                    query = query.Where(x => x.FullName.Contains(req.name));
                }
                if (!string.IsNullOrEmpty(req.phone))
                {
                    query = query.Where(x => x.Phone.Contains(req.phone));
                }
                if (req.province != 0)
                {
                    query = query.Where(x => x.ProvinceId == req.province);
                }
                var totalRecords = query.Count();
                List<ContactCustomerDetailsApi> data = query.OrderBy("CreateDate" + " " + "DESC").Skip((req.page - 1) * req.pageSize).Take(req.pageSize).Select(ContactCustomerDetailsApi.ProjectionDetail).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, req.page, req.pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ContactCustomer/GetAllContactCustomer Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("delete_contact_customer")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> DeleteContactCustomer(ContactCustomer req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.ContactId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tồn tại thông tin liên hệ khách hàng này");
                    return Ok(def);
                }
                var objContact = _unitOfWork.ContactCustomerRepository.GetFirstOrDefault(x => x.ContactId == req.ContactId);
                if (objContact == null || objContact.ContactId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tồn tại thông tin liên hệ khách hàng này");
                    return Ok(def);
                }
                if (_unitOfWork.LoanBriefRepository.Any(x => x.LoanBriefId == objContact.LoanBriefId))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Thông tin liên hệ khách hàng đã được khởi tạo đơn vay. HĐ-" + objContact.LoanBriefId +" nên không thể xóa");
                    return Ok(def);
                }
                objContact.Status = 0;
                //insert to db
                _unitOfWork.ContactCustomerRepository.Update(objContact);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ContactCustomer/UpdateContactCustomer Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}