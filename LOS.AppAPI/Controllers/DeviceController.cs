﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models.Device;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class DeviceController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        public DeviceController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;

        }
        [HttpPost]
        [Route("update_status_device")]
        public ActionResult<DefaultResponse<Meta>> UpdateStatusOfDevice([FromBody] DeviceItem req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                var loanbriefId = 0;
                if (string.IsNullOrEmpty(req.Imei) || !Enum.IsDefined(typeof(StatusOfDeviceID), req.StatusOfDevice) || string.IsNullOrEmpty(req.ContractId))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var arr = req.ContractId.Split('-');
                if (arr != null && arr.Length == 2)
                    Int32.TryParse(arr[1], out loanbriefId);
                if(loanbriefId > 0)
                {
                    var loanbrief = _unitOfWork.LoanBriefRepository.GetById(loanbriefId);
                    if(loanbrief != null && loanbrief.LoanBriefId > 0)
                    {
                        if(loanbrief.ContractGinno == req.ContractId && loanbrief.DeviceId == req.Imei)
                        {
                            if(loanbrief.DeviceStatus != req.StatusOfDevice)
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbriefId, x => new LoanBrief
                                {
                                    DeviceStatus = req.StatusOfDevice
                                });
                                _unitOfWork.Save();
                            }
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            return Ok(def);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Mã hợp đồng hoặc imei không trùng");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tồn tại khoản vay");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Device/UpdateStatusOfDevice Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}