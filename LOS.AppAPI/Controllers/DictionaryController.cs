﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.AppAPI.DTOs.DictionaryDTO;
using LOS.AppAPI.DTOs.LoanProduct;
using LOS.AppAPI.Helpers;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    // [Authorize]
    public class DictionaryController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public DictionaryController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }


        [HttpGet]
        [Route("all_product")]
        public ActionResult<DefaultResponse<Meta, List<ProductDetail>>> GetAllProduct()
        {
            var def = new DefaultResponse<Meta, List<ProductDetail>>();
            try
            {
                var query = _unitOfWork.ProductRepository.All().Select(ProductDetail.ProjectionDetail);
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionary/GetAllProduct Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("all_bank")]
        public ActionResult<DefaultResponse<Meta, List<BankDetail>>> GetAllBank()
        {
            var def = new DefaultResponse<Meta, List<BankDetail>>();
            try
            {
                var query = _unitOfWork.BankRepository.Query(x => x.Type == 3, null
                    , false).Select(BankDetail.ProjectionDetail);
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionary/GetAllBank Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("provinces")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<ProvinceDetail>>> GetProvinces()
        {
            var provinces = _unitOfWork.ProvinceRepository.All().Select(ProvinceDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<ProvinceDetail>>();
            def.meta = new Meta(200, "success");
            def.data = provinces;
            return Ok(def);
        }

        [Route("districts")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<DistrictDetail>>> GetDistricts([FromQuery] int province_id)
        {
            var districts = _unitOfWork.DistrictRepository.Query(x => x.ProvinceId == province_id, null, false).Select(DistrictDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<DistrictDetail>>();
            def.meta = new Meta(200, "success");
            def.data = districts;
            return Ok(def);
        }

        [Route("wards")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<WardDetail>>> GetWards([FromQuery] int district_id)
        {
            var wards = _unitOfWork.WardRepository.Query(x => x.DistrictId == district_id, null, false).Select(WardDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<WardDetail>>();
            def.meta = new Meta(200, "success");
            def.data = wards;
            return Ok(def);
        }

        [Route("rate_type")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<SelectItem>>> GetRateType()
        {
            var def = new DefaultResponse<List<SelectItem>>();
            def.meta = new Meta(200, "success");
            def.data = new List<SelectItem>();
            def.data.Add(new SelectItem() { Id = EnumRateType.DuNoGiamDan.ToString(), Value = "Dư nợ giảm dần" });
            def.data.Add(new SelectItem() { Id = EnumRateType.TatToanCuoiKy.ToString(), Value = "Tất toán cuối kỳ" });
            return Ok(def);
        }

        [Route("frequency")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<SelectItem>>> GetFrequency()
        {
            var data = ExtentionHelper.GetEnumToList(typeof(EnumFrequency));
            var def = new DefaultResponse<List<SelectItem>>();
            def.meta = new Meta(200, "success");
            if (data != null && data.Count > 0)
                def.data = data.Select(x => new SelectItem(x.Value, x.Text)).ToList();
            return Ok(def);
        }

        [Route("loan_time")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<SelectItem>>> GetLoanTime()
        {
            var data = ExtentionHelper.GetEnumToList(typeof(EnumLoanTime));
            var def = new DefaultResponse<List<SelectItem>>();
            def.meta = new Meta(200, "success");
            if (data != null && data.Count > 0)
                def.data = data.Select(x => new SelectItem(x.Value, x.Text)).ToList();
            return Ok(def);
        }
        [Route("type_receive_money")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<SelectItem>>> GetTypeReceivingMoney()
        {
            var data = ExtentionHelper.GetEnumToList(typeof(EnumTypeReceivingMoney));
            var def = new DefaultResponse<List<SelectItem>>();
            def.meta = new Meta(200, "success");
            if (data != null && data.Count > 0)
                def.data = data.Select(x => new SelectItem(x.Value, x.Text)).ToList();
            return Ok(def);
        }

        [Route("reasons")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<ReasonCanceDetail>>> GetReseason()
        {
            var reasons = _unitOfWork.ReasonCancelRepository.Query(x => x.Type == (int)EnumTypeReason.TdtdAddError && x.IsEnable == 1, null, false).Select(ReasonCanceDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<ReasonCanceDetail>>();
            def.meta = new Meta(200, "success");
            def.data = reasons;
            return Ok(def);
        }

        [Route("product_credit")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<LoanProductDetail>>> GetProductCredit()
        {
            var products = _unitOfWork.LoanProductRepository.Query(x => x.Status == 1, null, false).Select(LoanProductDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<LoanProductDetail>>();
            def.meta = new Meta(200, "success");
            def.data = products;
            return Ok(def);
        }
       
    }
}