﻿using LOS.AppAPI.DTOs.DirectSale;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Models.AG;
using LOS.AppAPI.Models.DirectSale;
using LOS.AppAPI.Models.Erp;
using LOS.AppAPI.Models.ServiceAI;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.AG;
using LOS.AppAPI.Service.CareSoft;
using LOS.AppAPI.Service.LmsService;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System.IO;
using LOS.Services.Services.Loanbrief;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class DirectSaleController : BaseV2Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _baseConfig;
        private readonly ICareSoftService _careSoftService;
        private readonly IAgService _agservice;
        private readonly IErp _erpService;
        private readonly ILmsService _lmsService;
        private readonly IProductPrice _productService;
        private readonly IServiceAI _aiService;
        private readonly ITrackDevice _trackDeviceService;
        private readonly IProxyTimaService _proxyTimaService;
        private readonly IDrawImageService _drawImageService;
        private readonly IPipelineService _pipelineService;
        private readonly IMomoService _momoService;
        private readonly ICheckBankService _checkBankService;
        private readonly ILoanbriefV2Service _loanbriefService;

        public DirectSaleController(IUnitOfWork unitOfWork, IConfiguration configuration, ICareSoftService careSoftService, IAgService agservice,
             IErp erpService, ILmsService lmsService, IProductPrice productService, IServiceAI aiService, ITrackDevice trackDeviceService,
             IProxyTimaService proxyTimaService, IDrawImageService drawImageService, IPipelineService pipelineService,
             IMomoService momoService, ICheckBankService checkBankService, ILoanbriefV2Service loanbriefService)// : base(unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this._baseConfig = configuration;
            this._careSoftService = careSoftService;
            this._agservice = agservice;
            this._erpService = erpService;
            this._lmsService = lmsService;
            this._productService = productService;
            this._aiService = aiService;
            this._trackDeviceService = trackDeviceService;
            this._proxyTimaService = proxyTimaService;
            this._drawImageService = drawImageService;
            this._pipelineService = pipelineService;
            this._momoService = momoService;
            this._checkBankService = checkBankService;
            this._loanbriefService = loanbriefService;
        }

        [HttpGet]
        [Route("list_status")]
        public async Task<ActionResult<DefaultResponse<Meta, List<StatusFilterHub>>>> ListStatus()
        {
            var def = new DefaultResponse<Meta, List<StatusFilterHub>>();
            try
            {
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var data = new List<StatusFilterHub>();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        //lấy danh sách đơn vay =>
                        //
                        var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess != EnumInProcess.Process.GetHashCode());
                        if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.HubEmployeeId == userInfo.UserId));
                        else
                        {
                            var userShop = await _unitOfWork.UserShopRepository.Query(x => x.UserId == userInfo.UserId, null, false).FirstOrDefaultAsync();
                            if (userShop != null && userInfo.HubId > 0)
                                filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.HubId == userInfo.HubId));
                        }

                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW
                                                                              || x.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE
                                                                              || x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_REVIEW
                                                                              || x.Status == (int)EnumLoanStatus.WAIT_APPRAISER_CONFIRM_CUSTOMER
                                                                              || x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_APPROVE_PROPOSION
                                                                              || x.Status == (int)EnumLoanStatus.APPROVAL_LEADER_APPROVE_CANCEL
                                                                              || x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_APPROVE_CANCEL
                                                                              || x.Status == (int)EnumLoanStatus.LENDER_LOAN_DISTRIBUTING
                                                                              || x.Status == (int)EnumLoanStatus.WAIT_DISBURSE
                                                                              ));
                        var dataLoanbrief = await _unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            Status = x.Status,
                            DetailStatus = x.LoanStatusDetail,
                            UtmSource = x.UtmSource
                        }).ToListAsync();
                        data.Add(new StatusFilterHub()
                        {
                            Name = ExtentionHelper.GetDescription(FilterLoanbriefForHub.Wait_Support),
                            FilterId = (int)FilterLoanbriefForHub.Wait_Support,
                            TotalLoan = dataLoanbrief.Count(x => x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW && x.UtmSource != "form_remkt_hub")
                        });
                        data.Add(new StatusFilterHub()
                        {
                            Name = ExtentionHelper.GetDescription(FilterLoanbriefForHub.Wait_CHT_Approve),
                            FilterId = (int)FilterLoanbriefForHub.Wait_CHT_Approve,
                            TotalLoan = dataLoanbrief.Count(x => x.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE)
                        });
                        data.Add(new StatusFilterHub()
                        {
                            Name = ExtentionHelper.GetDescription(FilterLoanbriefForHub.Wait_Tdhs_Approve),
                            FilterId = (int)FilterLoanbriefForHub.Wait_Tdhs_Approve,
                            TotalLoan = dataLoanbrief.Count(x => x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_REVIEW
                                                                              || x.Status == (int)EnumLoanStatus.WAIT_APPRAISER_CONFIRM_CUSTOMER
                                                                              || x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_APPROVE_PROPOSION
                                                                              || x.Status == (int)EnumLoanStatus.APPROVAL_LEADER_APPROVE_CANCEL
                                                                              || x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_APPROVE_CANCEL)
                        });
                        if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                        {
                            data.Add(new StatusFilterHub()
                            {
                                Name = ExtentionHelper.GetDescription(FilterLoanbriefForHub.Wait_Push_Lender),
                                FilterId = (int)FilterLoanbriefForHub.Wait_Push_Lender,
                                TotalLoan = dataLoanbrief.Count(x => x.Status == (int)EnumLoanStatus.LENDER_LOAN_DISTRIBUTING)
                            });
                            data.Add(new StatusFilterHub()
                            {
                                Name = ExtentionHelper.GetDescription(FilterLoanbriefForHub.Wait_Push_Disburse),
                                FilterId = (int)FilterLoanbriefForHub.Wait_Push_Disburse,
                                TotalLoan = dataLoanbrief.Count(x => x.Status == (int)EnumLoanStatus.WAIT_DISBURSE)
                            });
                        }
                        var detailStatus = await _unitOfWork.LoanStatusDetailRepository.Query(x => x.Type == (int)EnumLoanStatusDetail.CVKD
                                                && x.IsEnable == true && x.ParentId == 0, null, false).Select(x => new StatusFilterHub()
                                                {
                                                    Name = x.Name,
                                                    FilterId = x.Id
                                                }).ToListAsync();
                        if (detailStatus != null && detailStatus.Count > 0)
                        {
                            foreach (var item in detailStatus)
                            {
                                item.TotalLoan = dataLoanbrief.Count(x => x.DetailStatus == item.FilterId && x.UtmSource != "form_remkt_hub");
                                data.Add(item);
                            }
                        }
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = data;
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/ListStatus Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            finally
            {
                //_unitOfWork.Dispose();
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("list_loanbrief")]
        public async Task<ActionResult<DefaultResponse<SummaryMeta, List<LoanInfoDrsInfo>>>> ListLoanbrief([FromQuery] int? hubId, [FromQuery] int productId = -1, [FromQuery] int filterStatus = -1, [FromQuery] int hubEmployeeId = -1,
            [FromQuery] int page = 1, [FromQuery] int pageSize = 20)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanInfoDrsInfo>>();
            try
            {
                if (hubId.GetValueOrDefault(0) <= 0)
                {
                    def.meta = new SummaryMeta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền thông tin hub", 0, 0, 0);
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var data = new List<LoanInfoDrsForHub>();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var totalRecords = 0;
                        var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess != EnumInProcess.Process.GetHashCode());
                        if (productId > 0)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ProductId == productId));

                        if (!await _unitOfWork.UserShopRepository.AnyAsync(x => x.UserId == userInfo.UserId && x.ShopId == hubId.Value))
                        {
                            def.meta = new SummaryMeta(ResponseHelper.FAIL_CODE, "Tài khoản không thuộc hub", 0, 0, 0);
                            return Ok(def);
                        }
                        if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                        {
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.HubEmployeeId == userInfo.UserId && x.HubId == hubId));
                        }
                        else
                        {
                            if (hubEmployeeId > 0)
                                filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.HubId == hubId && x.HubEmployeeId == hubEmployeeId));
                            else
                                filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.HubId == hubId));
                        }
                        if (filterStatus == (int)FilterLoanbriefForHub.Wait_Support)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW && x.UtmSource != "form_remkt_hub"));
                        else if (filterStatus == (int)FilterLoanbriefForHub.Wait_CHT_Approve)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE));
                        else if (filterStatus == (int)FilterLoanbriefForHub.Wait_Tdhs_Approve)
                        {
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_REVIEW
                                                                                || x.Status == (int)EnumLoanStatus.WAIT_APPRAISER_CONFIRM_CUSTOMER
                                                                                || x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_APPROVE_PROPOSION
                                                                                || x.Status == (int)EnumLoanStatus.APPROVAL_LEADER_APPROVE_CANCEL
                                                                                || x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_APPROVE_CANCEL));
                        }
                        else if (filterStatus == (int)FilterLoanbriefForHub.Wait_Push_Lender)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.LENDER_LOAN_DISTRIBUTING));
                        else if (filterStatus == (int)FilterLoanbriefForHub.Wait_Push_Disburse)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.WAIT_DISBURSE));
                        else
                        {
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW));
                            if (filterStatus > 0)
                                filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LoanStatusDetail.GetValueOrDefault(0) == 0 && x.UtmSource != "form_remkt_hub"));
                        }
                        var dataResult = new List<LoanInfoDrsInfo>();
                        var query = _unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanInfoDrsForHub.ProjectionDetail);
                        totalRecords = query.Count();
                        data = await query.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                        //xử lý time delay
                        if (data != null && data.Count > 0)
                        {
                            var ipphone = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Ipphone > 0, null, false).Select(x => x.Ipphone).FirstOrDefaultAsync();
                            var listLoanbriefId = data.Select(x => x.LoanbriefId).ToList();
                            var logDistributionUsers = await _unitOfWork.LogDistributionUserRepository.Query(x => x.TypeDistribution == (int)TypeDistributionLog.CVKD
                                                && x.LoanbriefId > 0
                                                && listLoanbriefId.Contains(x.LoanbriefId.Value), null, false).ToListAsync();
                            var dicLogHubEmployeeReceived = new Dictionary<string, DateTime>();
                            if (logDistributionUsers != null && logDistributionUsers.Count > 0)
                            {
                                foreach (var item in logDistributionUsers)
                                {
                                    var key = $"{item.LoanbriefId}-{item.UserId}";
                                    if (!dicLogHubEmployeeReceived.ContainsKey(key))
                                        dicLogHubEmployeeReceived[key] = item.CreatedAt.Value;
                                }
                            }
                            //lấy danh sách quyền của user
                            var listPermissionAction = await _unitOfWork.UserActionPermissionRepository.Query(x => x.UserId == userInfo.UserId, null, false).Select(x => new
                            {
                                LoanStatus = x.LoanStatus,
                                PermissionValue = x.Value
                            }).ToListAsync();
                            if (listPermissionAction == null || listPermissionAction.Count == 0)
                            {
                                listPermissionAction = await _unitOfWork.GroupActionPermissionRepository.Query(x => x.GroupId == userInfo.GroupId, null, false).Select(x => new
                                {
                                    LoanStatus = x.LoanStatus,
                                    PermissionValue = x.Value
                                }).ToListAsync();
                            }
                            var dicPermission = new Dictionary<int, int>();
                            if (listPermissionAction != null && listPermissionAction.Count > 0)
                            {
                                foreach (var item in listPermissionAction)
                                {
                                    if (item.LoanStatus > 0 && item.PermissionValue > 0)
                                    {
                                        if (!dicPermission.ContainsKey(item.LoanStatus.Value))
                                            dicPermission[item.LoanStatus.Value] = item.PermissionValue.Value;
                                    }
                                }
                            }

                            var dicUserOfHub = await _unitOfWork.UserRepository.Query(x => x.GroupId > 0 && x.Status == 1
                                    && (x.GroupId == (int)EnumGroupUser.StaffHub || x.GroupId == (int)EnumGroupUser.ManagerHub), null, false)
                                .Select(x => new
                                {
                                    UserId = x.UserId
                                }).ToDictionaryAsync(x => x.UserId, x => x);
                            foreach (var item in data)
                            {
                                var isHubCreated = (item.CreatedBy > 0 && dicUserOfHub.ContainsKey(item.CreatedBy.Value));
                                var itemLoanbriefInfo = new LoanInfoDrsInfo()
                                {
                                    LoanbriefId = item.LoanbriefId,
                                    FullName = item.FullName,
                                    CreatedAt = item.CreatedAt,
                                    LoanTime = item.LoanTime.GetValueOrDefault(0),
                                    Address = item.Address,
                                    ProductCredit = item.ProductCredit,
                                    AddressFieldSurveyStatus = item.AddressFieldSurveyStatus.GetValueOrDefault(0),
                                    CompanyFieldSurveyStatus = item.CompanyFieldSurveyStatus.GetValueOrDefault(0),
                                    LoanStatus = (item.Status > 0 && Enum.IsDefined(typeof(EnumLoanStatus), item.Status))
                                                                    ? Description.GetDescription((EnumLoanStatus)item.Status) : String.Empty,
                                    LoanDetailStatus = (item.LoanDetailStatus > 0 && Enum.IsDefined(typeof(EnumLoanStatusDetailHub), item.LoanDetailStatus))
                                                                    ? Description.GetDescription((EnumLoanStatusDetailHub)item.LoanDetailStatus) : String.Empty,
                                    IsVehicleAppraisal = item.IsVehicleAppraisal,
                                    LinkCall = ipphone > 0 ? _careSoftService.GenarateLinkClick2Call(ipphone.ToString(), item.Phone) : String.Empty,
                                    IsLocate = item.IsLocate,
                                    HubEmployeeName = item.HubEmployeeName,
                                    HubEmployeeCallFirst = item.HubEmployeeCallFirst,
                                    ProductCreditId = item.ProductCreditId.GetValueOrDefault(0),
                                    LoanAmount = item.LoanAmount,
                                    MotobikeId = item.MotobikeId.GetValueOrDefault(0),
                                    Phone = item.Phone
                                };
                                var key = $"{item.LoanbriefId}-{item.HubEmployeeId}";
                                if (dicLogHubEmployeeReceived.ContainsKey(key))
                                    itemLoanbriefInfo.ReceivedAt = dicLogHubEmployeeReceived[key];

                                //Xử lý quyền trên đơn
                                if (item.Status == (int)EnumLoanStatus.APPRAISER_REVIEW)
                                {
                                    itemLoanbriefInfo.IsChangeStatusDetail = true;
                                    itemLoanbriefInfo.IsChangeStaff = true;
                                    itemLoanbriefInfo.IsPushException = true;
                                }
                                itemLoanbriefInfo.IsRegetLocation = item.IsTrackingLocation.GetValueOrDefault(false);

                                if (item.ProductCreditId == (int)EnumProductCredit.OtoCreditType_CC
                                    && (item.Status == (int)EnumLoanStatus.APPRAISER_REVIEW || item.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE)
                                    && (item.ReMarketingLoanBriefId.GetValueOrDefault(0) == 0 || item.ReMarketingLoanBriefId == item.LoanbriefId
                                                                                                || item.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp))
                                {
                                    itemLoanbriefInfo.IsCreatedCarChild = true;
                                }


                                if (item.Status == (int)EnumLoanStatus.APPRAISER_REVIEW || item.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE)
                                {
                                    itemLoanbriefInfo.IsReEsign
                                        = (item.EsignState == (int)EsignState.BORROWER_SIGNED || item.EsignState == (int)EsignState.LENDER_SIGNED);
                                    itemLoanbriefInfo.IsGetInsuranceInfo = true;
                                }

                                itemLoanbriefInfo.IsCheckMomo =
                                    (!string.IsNullOrEmpty(item.FullName) && !string.IsNullOrEmpty(item.Dob.ToString()) && !string.IsNullOrEmpty(item.NationalCard));

                                if (item.Status != EnumLoanStatus.CANCELED.GetHashCode() && dicPermission.ContainsKey(item.Status.Value))
                                {
                                    var permissionValue = dicPermission[item.Status.Value];
                                    if (permissionValue > 0)
                                    {
                                        if ((EnumActionUser.Edit.GetHashCode() & permissionValue) == EnumActionUser.Edit.GetHashCode())
                                            itemLoanbriefInfo.IsEdit = true;
                                        if ((EnumActionUser.PushLoan.GetHashCode() & permissionValue) == EnumActionUser.PushLoan.GetHashCode())
                                            itemLoanbriefInfo.IsPush = true;
                                        if ((EnumActionUser.Cancel.GetHashCode() & permissionValue) == EnumActionUser.Cancel.GetHashCode())
                                        {
                                            if (isHubCreated || item.ProductCreditId == (int)EnumProductCredit.OtoCreditType_CC
                                                    || item.BoundTelesaleId.GetValueOrDefault(0) == 0
                                                    || item.PlatformType == (int)EnumPlatformType.LendingOnlineWeb
                                                    || item.PlatformType == (int)EnumPlatformType.LendingOnlineApp)
                                                itemLoanbriefInfo.IsCancel = true;
                                        }
                                    }
                                }

                                dataResult.Add(itemLoanbriefInfo);
                            }
                        }
                        def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                        def.data = dataResult;
                    }
                    else
                    {
                        def.meta = new SummaryMeta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập", 0, 0, 0);
                    }
                }
                else
                {
                    def.meta = new SummaryMeta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ", 0, 0, 0);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/ListLoanbrief Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("check_customer")]
        public async Task<ActionResult<DefaultResponse<Meta, CheckCusstomer>>> CheckCustomer([FromQuery] string Phone, [FromQuery] string NumberCard)
        {
            var def = new DefaultResponse<Meta, CheckCusstomer>();
            try
            {
                var userInfo = GetUserInfo();
                //Kiểm tra nhân viên tima
                var checkErp = _erpService.CheckEmployeeTima(NumberCard, Phone);
                if (checkErp != null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng là nhân viên Tima!");
                    return Ok(def);
                }
                //Kiểm tra blacklist
                var objBlackList = new RequestCheckBlackListAg
                {
                    FullName = string.Empty,
                    NumberCard = NumberCard,
                    Phone = Phone
                };

                var checkBlackList = _agservice.CheckBlacklistAg(objBlackList);
                if (checkBlackList.Data == true)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng nằm trong Blacklist!");
                    return Ok(def);
                }
                var lstLoan = await _unitOfWork.LoanBriefRepository.Query(x => (x.Phone == Phone || x.NationalCard == NumberCard ||
                (!string.IsNullOrEmpty(x.NationCardPlace) && x.NationCardPlace == NumberCard))
                && x.Status != (int)EnumLoanStatus.CANCELED, null, false).Select(x => new
                {
                    LoanBriefId = x.LoanBriefId,
                    Status = x.Status,
                    Phone = x.Phone,
                    NationalCard = x.NationalCard,
                    ProductId = x.ProductId,
                    LmsLoanId = x.LmsLoanId,
                    FullName = x.FullName,
                    LoanBriefProperty = x.LoanBriefProperty.ProductId,
                    ResidentType = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType.Value : 0
                }).ToListAsync();

                if (lstLoan != null && lstLoan.Count > 0)
                {
                    //Kiểm tra đơn đang xly
                    var loanProcess = lstLoan.Where(x => x.Status != (int)EnumLoanStatus.DISBURSED && x.Status != (int)EnumLoanStatus.FINISH).FirstOrDefault();
                    if (loanProcess != null)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn đang xử lý!");
                        def.data = new CheckCusstomer { LoanBriefId = loanProcess.LoanBriefId };
                        return Ok(def);
                    }
                    //Kiểm tra đơn đang vay
                    var loanDis = lstLoan.Where(x => x.Status == (int)EnumLoanStatus.DISBURSED).FirstOrDefault();
                    if (loanDis != null)
                    {
                        //Kiểm tra Topup
                        //Đối với gói xe máy
                        if (loanDis.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                            || loanDis.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                            || loanDis.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                        {
                            var TotalMoneyCurrent = 0L;
                            var lstPayment = _lmsService.GetPaymentLoanById(loanDis.LmsLoanId.Value);
                            if (lstPayment.Status == 1)
                            {
                                TotalMoneyCurrent = lstPayment.Data.Loan.TotalMoneyCurrent;
                                if (lstPayment.Data.Loan.TopUp != 1)
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng không đủ điều kiện vay topup xe máy!");
                                    return Ok(def);
                                }
                                var maxPrice = GetMaxPriceProductTopupV3((int)loanDis.LoanBriefProperty, (int)loanDis.ProductId, loanDis.LoanBriefId, loanDis.ResidentType, TotalMoneyCurrent);
                                var loanAmount = maxPrice.MaxPriceProduct;
                                //Làm tròn tiền xuống đơn vị triệu đồng
                                //loanAmount = Convert.ToInt64(Math.Round((decimal)loanAmount / 1000000, 0, MidpointRounding.ToEven) * 1000000);
                                loanAmount = Convert.ToInt64(LOS.Common.Utils.MathUtils.RoundMoney(loanAmount));
                                if (loanAmount < 1000000)
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không đủ điều kiện vay Topup, số tiền < 1tr!");
                                    return Ok(def);
                                }

                                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, "Khách hàng đủ điều kiện vay Topup xe máy");
                                def.data = new CheckCusstomer { LoanBriefId = loanDis.LoanBriefId, TypeLoanSupport = (int)EnumTypeLoanSupport.IsCanTopup };
                                return Ok(def);
                            }
                        }
                        //Nếu là gói oto
                        else if (loanDis.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                            || loanDis.ProductId == (int)EnumProductCredit.OtoCreditType_KCC
                            || loanDis.ProductId == (int)EnumProductCredit.CamotoCreditType)
                        {
                            var req = new CheckReLoan.Input()
                            {
                                CustomerName = loanDis.FullName,
                                NumberCard = loanDis.NationalCard
                            };
                            // Query api   	
                            var data = _lmsService.CheckTopupOto(req);
                            if (data.IsAccept == 1)
                            {
                                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, "Khách hàng đủ điều kiện vay Topup ô tô");
                                def.data = new CheckCusstomer { LoanBriefId = loanDis.LoanBriefId };
                                return Ok(def);
                            }
                            else
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng không đủ điều kiện vay topup ô tô!");
                                return Ok(def);
                            }
                        }

                    }
                    //Kiểm tra tái vay
                    var loanFinish = lstLoan.Where(x => x.Status == (int)EnumLoanStatus.FINISH).FirstOrDefault();
                    if (loanFinish != null)
                    {
                        var req = new CheckReLoan.Input()
                        {
                            CustomerName = loanFinish.FullName.Trim(),
                            NumberCard = loanFinish.NationalCard
                        };
                        if (req.CustomerName.IsNormalized())
                            req.CustomerName = req.CustomerName.Normalize();
                        // Query api   	
                        var data = _lmsService.CheckReLoan(req);
                        if (data.Result == 1 && data.IsAccept == 1)
                        {
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, "Khách hàng đủ điều kiện tái vay");
                            def.data = new CheckCusstomer { LoanBriefId = loanFinish.LoanBriefId, TypeLoanSupport = (int)EnumTypeLoanSupport.ReBorrow };
                            return Ok(def);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng không đủ điều kiện tái vay!");
                            return Ok(def);
                        }
                    }

                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, "Khách hàng đủ điều kiện vay đơn thường");
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/CheckCustomer Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        private PriceProductTopup GetMaxPriceProductTopupV3(int productId, int productCredit, int loanBriefId, int typeOfOwnerShip, long totalMoneyCurrent = 0)
        {
            var access_token = GetUserInfo();
            long originalCarPrice = 0;
            var maxPriceProduct = 0L;
            // lấy giá bên AI
            decimal priceCarAI = _productService.GetPriceAI(productId, loanBriefId);
            if (priceCarAI > 0)
            {
                originalCarPrice = (long)priceCarAI;
            }
            //var originalCarPriceLast = originalCarPrice - TotalMoneyCurrent;
            maxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPriceTopup(productCredit, originalCarPrice, totalMoneyCurrent, typeOfOwnerShip);
            var data = new PriceProductTopup
            {
                PriceAi = originalCarPrice,
                MaxPriceProduct = maxPriceProduct
            };
            return data;
        }

        [HttpPost]
        [Route("create_loanbrief")]
        public async Task<ActionResult<DefaultResponse<object>>> CreateLoanBrief(LoanBriefInit entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.HubId.GetValueOrDefault(0) <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền thông tin hub");
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    if (!await _unitOfWork.UserShopRepository.
                        AnyAsync(x => x.UserId == userInfo.UserId && x.ShopId == entity.HubId.Value))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Tài khoản không thuộc hub");
                        return Ok(def);
                    }

                    var messageValid = ValidLoanBriefInit(entity);
                    if (!string.IsNullOrEmpty(messageValid))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, messageValid);
                        return Ok(def);
                    }

                    var note = "";

                    //kiểm tra xem có đơn đang xử lý không
                    //gói vay xe máy
                    if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                            || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                            || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                    {
                        var loanbriefCheck = await _unitOfWork.LoanBriefRepository.Query(x => x.Status != (int)EnumLoanStatus.FINISH && x.Status != (int)EnumLoanStatus.CANCELED
                             && (x.Phone == entity.Phone || (!string.IsNullOrEmpty(entity.NationalCard) && x.NationalCard == entity.NationalCard)), null, false)
                            .Select(x => new { LoanBriefId = x.LoanBriefId }).OrderByDescending(x => x.LoanBriefId).FirstOrDefaultAsync();
                        if (loanbriefCheck != null && loanbriefCheck.LoanBriefId > 0)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý HĐ-" + loanbriefCheck.LoanBriefId);
                            return Ok(def);
                        }
                    }
                    //gói vay ô tô
                    else
                    {
                        var loanbriefCheck = await _unitOfWork.LoanBriefRepository.Query(x => x.Status != (int)EnumLoanStatus.DISBURSED && x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.FINISH
                            && (x.Phone == entity.Phone || (!string.IsNullOrEmpty(entity.NationalCard)
                            && x.NationalCard == entity.NationalCard)), null, false).OrderByDescending(x => x.LoanBriefId)
                            .Select(x => new { LoanBriefId = x.LoanBriefId }).FirstOrDefaultAsync();
                        if (loanbriefCheck != null && loanbriefCheck.LoanBriefId > 0)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý. Vui lòng tạo đơn con từ HĐ-" + loanbriefCheck.LoanBriefId);
                            return Ok(def);
                        }
                        //Kiểm tra có đơn đang vay hay không
                        //Nếu có đơn đang vay => chỉ cho tạo đơn topup đủ điêu kiện
                        var loanbriefDisbursed = await _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.DISBURSED
                            && (x.Phone == entity.Phone || (!string.IsNullOrEmpty(entity.NationalCard)
                            && x.NationalCard == entity.NationalCard)), null, false).OrderByDescending(x => x.LoanBriefId)
                            .Select(x => new { LoanBriefId = x.LoanBriefId, CustomerId = x.CustomerId }).FirstOrDefaultAsync(); ;
                        if (loanbriefDisbursed != null && loanbriefDisbursed.LoanBriefId > 0)
                        {
                            if (!string.IsNullOrEmpty(entity.NationalCard) && !string.IsNullOrEmpty(entity.FullName) && loanbriefDisbursed.CustomerId > 0)
                            {
                                //xem có đơn topup nào đang xử lý không
                                decimal totalMoneyBrrowing = 0;
                                var lstLoanTopup = await _unitOfWork.LoanBriefRepository.Query(x => x.Status != (int)EnumLoanStatus.DISBURSED
                                        && x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.FINISH
                                        && x.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                                        && x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp && x.PlatformType == (int)EnumPlatformType.ReMarketing
                                        && x.CustomerId == loanbriefDisbursed.CustomerId, null, false).ToListAsync();
                                if (lstLoanTopup != null && lstLoanTopup.Count > 0)
                                    totalMoneyBrrowing = lstLoanTopup.Sum(x => x.LoanAmount.Value);

                                //cập nhập đơn thì loại bỏ số tiền của đơn này ra. Đã được sum ở trên
                                totalMoneyBrrowing = totalMoneyBrrowing - entity.LoanAmount.Value;
                                //check xem có đơn vay topup chưa
                                //có rồi thì thông báo không tạo được đơn Topup
                                //chưa có thì check xem đủ điều kiện Topup không
                                var nationalCard = entity.NationalCard;
                                if (!string.IsNullOrEmpty(entity.NationCardPlace))
                                    nationalCard = $"{entity.NationalCard},{entity.NationCardPlace}";
                                var objCheckTopupOto = new CheckReLoan.Input
                                {
                                    NumberCard = nationalCard,
                                    CustomerName = entity.FullName,
                                };
                                //check xem đơn có đủ điều kiện để topup không
                                var checkTopupOto = _lmsService.CheckTopupOto(objCheckTopupOto);
                                if (checkTopupOto.IsAccept == 1)
                                {
                                    //số tiền dư nợ + với số tiền vay hiện tại nhỏ hơn 300tr thì cho cập nhập
                                    if (entity.LoanAmount + checkTopupOto.TotalMoneyCurrent + totalMoneyBrrowing > 300000000)
                                    {
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số tiền vay và số tiền dư nợ phải nhỏ hơn 300tr. Dư nợ hiện tại:" + (checkTopupOto.TotalMoneyCurrent + totalMoneyBrrowing).ToString("###,0") + " VNĐ.");
                                        return Ok(def);
                                    }
                                    //thêm mới
                                    entity.ReMarketingLoanBriefId = loanbriefDisbursed.LoanBriefId;
                                    entity.TypeRemarketing = (int)EnumTypeRemarketing.IsTopUp;
                                    entity.PlatformType = (int)EnumPlatformType.ReMarketing;
                                    note += "Đơn vay Topup được khởi tạo từ HĐ-" + loanbriefDisbursed.LoanBriefId;
                                }
                                else
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, checkTopupOto.Message);
                                    return Ok(def);
                                }
                            }
                        }
                    }

                    //xử lý chuẩn hóa tên KH
                    if (!string.IsNullOrEmpty(entity.FullName))
                    {
                        entity.FullName = entity.FullName.Trim();
                        entity.FullName = entity.FullName.ReduceWhitespace();
                        entity.FullName = entity.FullName.TitleCaseString();
                        if (!entity.FullName.IsNormalized())
                            entity.FullName = entity.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                    }

                    //thông tin của user
                    var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                            .Select(x => new
                            {
                                UserId = x.UserId,
                                FullName = x.FullName,
                                UserName = x.Username,
                            }).FirstOrDefaultAsync();


                    //mặc định mua bảo hiểm đối với gói xe máy
                    //Gói oto mặc định k mua bao hiểm
                    if (entity.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                    {
                        entity.BuyInsurenceCustomer = false;// model.BuyInsurenceCustomer;
                        entity.IsLocate = entity.IsLocateCar;
                    }
                    else
                    {
                        entity.BuyInsurenceCustomer = true;
                    }
                    var hubId = 0;
                    var hubEmployeeId = 0;
                    var isHeadOffice = true;

                    if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        hubId = entity.HubId.Value;
                        isHeadOffice = false;
                    }
                    else if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                    {
                        isHeadOffice = false;
                        hubId = entity.HubId.Value;
                        hubEmployeeId = userInfo.UserId;
                    }
                    if (entity.FromDate == null)
                        entity.FromDate = DateTime.Now;
                    if (entity.LoanTime > 0)
                        entity.ToDate = entity.FromDate.Value.AddMonths((int)entity.LoanTime);
                    entity.IsCheckBank = entity.IsCheckBank;
                    entity.LoanAmountFirst = entity.LoanAmount;
                    entity.Frequency = 1;
                    var feePaymentBeforeLoan = Common.Utils.ProductPriceUtils.GetFeePaymentBeforeLoan(entity.ProductId.Value, entity.LoanTime);

                    #region Customer
                    var customerOld = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == entity.Phone || x.NationalCard == entity.NationalCard);
                    if (customerOld != null && customerOld.CustomerId > 0)
                    {
                        await _unitOfWork.CustomerRepository.UpdateAsync(x => x.CustomerId == customerOld.CustomerId, x => new Customer()
                        {
                            FullName = entity.FullName,
                            JobId = entity.JobId,
                            Phone = entity.Phone,
                            CompanyName = entity.CompanyName,
                            CompanyAddress = entity.CompanyAddress,
                            ProvinceId = entity.ProvinceId,
                            DistrictId = entity.DistrictId,
                            WardId = entity.WardId,
                            Address = entity.ResidentAddress,
                            UpdatedAt = DateTime.Now,
                            InsurenceNumber = entity.InsurenceNumber,
                            InsurenceNote = entity.InsurenceNote,
                            IsMerried = entity.IsMerried,
                            NumberBaby = entity.NumberBaby,
                            Dob = entity.BirthDay,
                            Gender = entity.Gender,
                            NationalCard = !string.IsNullOrEmpty(entity.NationalCard) ? entity.NationalCard : customerOld.NationalCard,
                            NationCardPlace = entity.NationCardPlace,
                            Passport = entity.Passport,
                            FacebookAddress = entity.FacebookAddress,
                        });
                        entity.CustomerId = customerOld.CustomerId;
                    }
                    else
                    {
                        var customer = new Customer()
                        {
                            FullName = entity.FullName,
                            JobId = entity.JobId,
                            CompanyName = entity.CompanyName,
                            CompanyAddress = entity.CompanyAddress,
                            ProvinceId = entity.ProvinceId,
                            DistrictId = entity.DistrictId,
                            WardId = entity.WardId,
                            Address = entity.ResidentAddress,
                            CreatedAt = DateTime.Now,
                            InsurenceNumber = entity.InsurenceNumber,
                            InsurenceNote = entity.InsurenceNote,
                            IsMerried = entity.IsMerried,
                            NumberBaby = entity.NumberBaby,
                            Phone = entity.Phone,
                            Dob = entity.BirthDay,
                            Gender = entity.Gender,
                            NationalCard = entity.NationalCard,
                            NationCardPlace = entity.NationCardPlace,
                            Passport = entity.Passport,
                            FacebookAddress = entity.FacebookAddress,
                        };
                        await _unitOfWork.CustomerRepository.InsertAsync(customer);
                        _unitOfWork.Save();
                        entity.CustomerId = customer.CustomerId;
                    }
                    #endregion

                    #region LoanBrief
                    var loanbrief = new LoanBrief()
                    {
                        LoanBriefId = entity.LoanBriefId,
                        CustomerId = entity.CustomerId,
                        PlatformType = entity.PlatformType.GetValueOrDefault(),
                        TypeLoanBrief = entity.TypeLoanBrief.GetValueOrDefault(),
                        FullName = entity.FullName,
                        Phone = entity.Phone,
                        NationalCard = entity.NationalCard,
                        NationCardPlace = entity.NationCardPlace,
                        IsTrackingLocation = entity.IsTrackingLocation,
                        Dob = entity.BirthDay,
                        Passport = entity.Passport,
                        Gender = entity.Gender,
                        NumberCall = entity.NumberCall,
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId,
                        ProductId = entity.ProductId.GetValueOrDefault(),
                        RateTypeId = entity.RateTypeId.GetValueOrDefault(),
                        LoanAmount = entity.LoanAmount.GetValueOrDefault(),
                        BuyInsurenceCustomer = entity.BuyInsurenceCustomer,
                        LoanTime = entity.LoanTime,
                        Frequency = entity.Frequency,
                        RateMoney = entity.RateMoney,
                        RatePercent = entity.RatePercent,
                        ReceivingMoneyType = entity.ReceivingMoneyType,
                        BankId = entity.BankId,
                        BankAccountNumber = entity.BankAccountNumber,
                        BankCardNumber = entity.BankCardNumber,
                        BankAccountName = entity.BankAccountName,
                        IsLocate = entity.IsLocate,
                        TypeRemarketing = entity.TypeRemarketing,
                        FromDate = entity.FromDate,
                        ToDate = entity.ToDate,
                        CreatedTime = DateTime.Now,
                        IsHeadOffice = isHeadOffice,
                        HubId = hubId,
                        CreateBy = hubEmployeeId,
                        Status = EnumLoanStatus.INIT.GetHashCode(),
                        PipelineState = EnumPipelineState.WAITING_FOR_PIPELINE.GetHashCode(),
                        HubEmployeeId = hubEmployeeId,
                        PresenterCode = entity.PresenterCode,
                        LoanAmountFirst = entity.LoanAmountFirst,
                        LoanAmountExpertise = entity.LoanAmountExpertise,
                        LoanAmountExpertiseLast = entity.LoanAmountExpertiseLast,
                        LoanAmountExpertiseAi = entity.LoanAmountExpertiseAi,
                        PhoneOther = entity.PhoneOther,
                        BankBranch = entity.BankBranch,
                        ProductDetailId = entity.ProductDetailId,
                        IsSim = entity.IsSim,
                        IsReborrow = entity.IsReborrow,
                        LoanPurpose = entity.LoanPurpose,
                        FeePaymentBeforeLoan = feePaymentBeforeLoan,
                        Email = entity.Email,
                        FirstTimeHubFeedBack = DateTime.Now,
                        LoanStatusDetail = (int)EnumLoanStatusDetailHub.CvkdDaLienHe,
                        LoanStatusDetailChild = (int)EnumLoanStatusDetailHub.CvkdChoHoSoTaiCuaHang,
                        Ltv = entity.Ltv,
                        UtmSource = userAction.UserName,
                    };
                    await _unitOfWork.LoanBriefRepository.InsertAsync(loanbrief);
                    _unitOfWork.Save();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                    {
                        await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                        {
                            FirstTimeStaffHubFeedback = DateTime.Now,
                        });
                    }
                    #endregion

                    #region LoanBriefResident
                    await _unitOfWork.LoanBriefResidentRepository.InsertAsync(new LoanBriefResident()
                    {
                        LoanBriefResidentId = loanbrief.LoanBriefId,
                        LivingTime = entity.LivingTime,
                        ResidentType = entity.ResidentType,
                        LivingWith = entity.LivingWith,
                        Address = entity.ResidentAddress,
                        AddressGoogleMap = entity.ResidentAddressGoogleMap,
                        AddressLatLng = entity.ResidentAddressLatLng,
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId,
                        AddressNationalCard = entity.AddressNationalCard,
                        CustomerShareLocation = entity.CustomerShareLocation,
                    });
                    #endregion

                    #region LoanBriefProperty
                    await _unitOfWork.LoanBriefPropertyRepository.InsertAsync(new LoanBriefProperty()
                    {
                        LoanBriefPropertyId = loanbrief.LoanBriefId,
                        BrandId = entity.BrandId,
                        ProductId = entity.PropertyProductId,
                        PlateNumber = entity.PlateNumber,
                        CreatedTime = DateTime.Now,
                        CarManufacturer = entity.CarManufacturer,
                        CarName = entity.CarName,
                        CarColor = entity.CarColor,
                        PlateNumberCar = entity.PlateNumberCar,
                        Chassis = entity.Chassis,
                        Engine = entity.Engine,
                        BuyInsuranceProperty = entity.BuyInsuranceProperty != null ? entity.BuyInsuranceProperty : false,
                        OwnerFullName = entity.OwnerFullName,
                        MotobikeCertificateNumber = entity.MotobikeCertificateNumber,
                        MotobikeCertificateDate = entity.MotobikeCertificateDate,
                        MotobikeCertificateAddress = entity.MotobikeCertificateAddress,
                        PostRegistration = entity.PostRegistration != null ? entity.PostRegistration : false,
                        Description = entity.PropertyDescription,
                    });
                    #endregion

                    #region LoanBriefHousehold
                    await _unitOfWork.LoanBriefHouseholdRepository.InsertAsync(new LoanBriefHousehold()
                    {
                        LoanBriefHouseholdId = loanbrief.LoanBriefId,
                        Address = entity.HouseholdAddress,
                        ProvinceId = entity.HouseholdProvinceId,
                        DistrictId = entity.HouseholdDistrictId,
                        WardId = entity.HouseholdWardId,
                        FullNameHouseOwner = entity.FullNameHouseOwner,
                        RelationshipHouseOwner = entity.RelationshipHouseOwner,
                        BirdayHouseOwner = entity.BirdayHouseOwner,
                    });
                    #endregion

                    #region LoanBriefJob
                    await _unitOfWork.LoanBriefJobRepository.InsertAsync(new LoanBriefJob()
                    {
                        LoanBriefJobId = loanbrief.LoanBriefId,
                        JobId = entity.JobId,
                        CompanyName = entity.CompanyName,
                        CompanyPhone = entity.CompanyPhone,
                        CompanyTaxCode = entity.CompanyTaxCode,
                        CompanyProvinceId = entity.CompanyProvinceId,
                        CompanyDistrictId = entity.CompanyDistrictId,
                        CompanyWardId = entity.CompanyWardId,
                        CompanyAddress = entity.CompanyAddress,
                        TotalIncome = entity.TotalIncome,
                        ImcomeType = entity.ImcomeType,
                        Description = entity.JobDescription,
                        CompanyAddressGoogleMap = entity.CompanyAddressGoogleMap,
                        CompanyAddressLatLng = entity.CompanyAddressLatLng,
                        CompanyInsurance = entity.CompanyInsurance,
                        WorkLocation = entity.WorkLocation,
                        BusinessPapers = entity.BusinessPapers,
                        JobDescriptionId = entity.JobDescriptionId,
                    });
                    #endregion

                    #region LoanBriefCompany
                    if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                    {
                        _unitOfWork.LoanBriefCompanyRepository.Insert(new LoanBriefCompany()
                        {
                            LoanBriefId = loanbrief.LoanBriefId,
                            CompanyName = entity.EnterpriseName,
                            CareerBusiness = entity.CareerBusiness,
                            BusinessCertificationAddress = entity.BusinessCertificationAddress,
                            HeadOffice = entity.HeadOffice,
                            Address = entity.EnterpriseAddress,
                            CompanyShareholder = entity.CompanyShareholder,
                            CardNumberShareholderDate = entity.CardNumberShareholderDate,
                            BusinessCertificationDate = entity.BusinessCertificationDate,
                            BirthdayShareholder = entity.BirthdayShareholder,
                            CardNumberShareholder = entity.CardNumberShareholder,
                            PlaceOfBirthShareholder = entity.PlaceOfBirthShareholder,
                            CreatedDate = DateTime.Now
                        });
                    }
                    #endregion

                    #region LoanBriefRelationship
                    if (entity.LoanBriefRelationshipModels != null && entity.LoanBriefRelationshipModels.Count > 0)
                    {
                        foreach (var relationship in entity.LoanBriefRelationshipModels)
                        {
                            if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                            {
                                await _unitOfWork.LoanBriefRelationshipRepository.InsertAsync(new LoanBriefRelationship()
                                {
                                    LoanBriefId = loanbrief.LoanBriefId,
                                    RelationshipType = relationship.RelationshipType,
                                    FullName = relationship.FullName,
                                    Phone = relationship.Phone,
                                    CreatedDate = DateTime.Now,
                                    Address = relationship.Address
                                });
                            }
                        }
                    }
                    #endregion

                    #region LoanBriefQuestionScript
                    await _unitOfWork.LoanBriefQuestionScriptRepository.InsertAsync(new LoanBriefQuestionScript()
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        WaterSupplier = entity.WaterSupplier,
                        QuestionHouseOwner = entity.QuestionHouseOwner,
                        Appraiser = entity.Appraiser,
                        DocumentBusiness = entity.DocumentBusiness,
                        MaxPrice = entity.MaxPrice,
                        QuestionAddressCoincideAreaSupport = entity.QuestionAddressCoincideAreaSupport,
                    });
                    #endregion

                    //Thêm note khởi tạo đơn vay
                    await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote()
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        Note = !string.IsNullOrEmpty(note) ? string.Format("{0}: Khởi tạo đơn vay", userAction.FullName) + "<br />" + note : string.Format("{0}: Khởi tạo đơn vay", userAction.FullName),
                        FullName = userAction.FullName,
                        UserId = userAction.UserId,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        ShopId = entity.HubId.Value,
                        ShopName = "Tima"
                    });
                    //Log Action
                    await _unitOfWork.LogLoanActionRepository.InsertAsync(new LogLoanAction()
                    {
                        LoanbriefId = entity.LoanBriefId,
                        ActionId = (int)EnumLogLoanAction.CreateLoan,
                        TypeAction = (int)EnumTypeAction.Manual,
                        LoanStatus = EnumLoanStatus.INIT.GetHashCode(),
                        HubId = entity.HubId.Value,
                        HubEmployeeId = userAction.UserId,
                        UserActionId = userAction.UserId,
                        GroupUserActionId = userInfo.GroupId,
                        NewValues = JsonConvert.SerializeObject(entity),
                        CreatedAt = DateTime.Now
                    });

                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = loanbrief.LoanBriefId;
                }
                else
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");

            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "DirectSale/InitLoanBrief POST Exception");
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("logclicktocall")]
        public async Task<ActionResult<DefaultResponse<object>>> LogClickToCall([FromBody] LogClickToCallReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity == null || entity.LoanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var data = new List<StatusFilterHub>();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            Phone = x.Phone,
                            HubEmployeeCallFirst = x.HubEmployeeCallFirst,
                            FirstProcessingTime = x.FirstProcessingTime,
                            CountCall = x.CountCall,
                            CoordinatorUserId = x.CoordinatorUserId,
                            Status = x.Status,
                            LoanStatusChild = x.LoanStatusDetailChild,
                            HubId = x.HubId,
                            BoundTelesaleId = x.BoundTelesaleId,
                            DetailStatusTelesales = x.DetailStatusTelesales,
                            HubEmployeeId = x.HubEmployeeId
                        }).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != userInfo.UserId)
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            else if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                                 && !_unitOfWork.UserShopRepository.Any(x => x.ShopId == loanbrief.HubId && x.UserId == userInfo.UserId))
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            var typeCallService = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId, null, false).Select(x => x.TypeCallService).FirstOrDefaultAsync();
                            _unitOfWork.LogClickToCallRepository.Insert(new DAL.EntityFramework.LogClickToCall()
                            {
                                LoanbriefId = loanbrief.LoanbriefId,
                                UserId = userInfo.UserId,
                                PhoneOfCustomer = loanbrief.Phone,
                                CreatedAt = DateTime.Now,
                                TypeCallService = typeCallService
                            });

                            _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanbriefId, x => new LoanBrief()
                            {
                                CountCall = (loanbrief.CountCall ?? 0) + 1,
                                HubEmployeeCallFirst = loanbrief.HubEmployeeCallFirst ?? DateTime.Now
                            });

                            _unitOfWork.LogLoanActionRepository.Insert(new LogLoanAction
                            {
                                LoanbriefId = loanbrief.LoanbriefId,
                                ActionId = (int)EnumLogLoanAction.Call,
                                TypeAction = (int)EnumTypeAction.Manual,
                                LoanStatus = loanbrief.Status,
                                TlsLoanStatusDetail = loanbrief.DetailStatusTelesales,
                                HubLoanStatusDetail = loanbrief.LoanStatusChild,
                                TelesaleId = loanbrief.BoundTelesaleId,
                                HubId = loanbrief.HubId,
                                HubEmployeeId = loanbrief.HubEmployeeId,
                                CoordinatorUserId = loanbrief.CoordinatorUserId,
                                UserActionId = userInfo.UserId,
                                GroupUserActionId = userInfo.GroupId,
                                CreatedAt = DateTime.Now,
                                NewValues = $"{typeCallService}"
                            });
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }


                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }
            }
            catch (Exception ex)
            {

                Log.Error(ex, "DirectSale/LogClickToCall Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_loanbrief")]
        public async Task<ActionResult<DefaultResponse<object>>> UpdateLoanBrief([FromBody] LoanBriefInit entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (entity.HubId.GetValueOrDefault(0) <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền thông tin hub");
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    if (!await _unitOfWork.UserShopRepository.
                        AnyAsync(x => x.UserId == userInfo.UserId && x.ShopId == entity.HubId.Value))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Tài khoản không thuộc hub");
                        return Ok(def);
                    }

                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            HubEmployeeId = x.HubEmployeeId,
                            HubId = x.HubId,
                            Status = x.Status
                        }).OrderByDescending(x => x.LoanbriefId).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            //kiểm tra xem có đơn đang xử lý không
                            //gói vay xe máy
                            var resultCheckTopup = new DAL.DTOs.Loanbrief.CheckTopupResult();
                            if (entity.TypeRemarketing.GetValueOrDefault(0) != (int)EnumTypeRemarketing.DebtRevolvingLoan)
                            {
                                if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                {
                                    var loanbriefCheck = await _unitOfWork.LoanBriefRepository.Query(x => x.Status != (int)EnumLoanStatus.FINISH && x.Status != (int)EnumLoanStatus.CANCELED
                                         && (x.Phone == entity.Phone || (!string.IsNullOrEmpty(entity.NationalCard) && x.NationalCard == entity.NationalCard)) && x.LoanBriefId != entity.LoanBriefId, null, false)
                                        .Select(x => new { LoanBriefId = x.LoanBriefId, Status = x.Status }).OrderByDescending(x => x.LoanBriefId).FirstOrDefaultAsync();
                                    if (loanbriefCheck != null && loanbriefCheck.LoanBriefId > 0)
                                    {
                                        if (entity.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                                        {
                                            if (loanbriefCheck.Status != (int)EnumLoanStatus.DISBURSED)
                                            {
                                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý HĐ-" + loanbriefCheck.LoanBriefId);
                                                return Ok(def);
                                            }

                                            if (entity.ReMarketingLoanBriefId > 0)
                                            {
                                                if (entity.ProductDetailId.GetValueOrDefault(0) == 0)
                                                {
                                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng chọn gói vay chi tiết");
                                                    return Ok(def);
                                                }
                                                resultCheckTopup = await _loanbriefService.CheckCanTopup(entity.ReMarketingLoanBriefId.Value);
                                                if (resultCheckTopup.IsCanTopup)
                                                {
                                                    //Kiểm tra giới hạn số tiền cho vay
                                                    var limitedLoanAmount = Common.Utils.ProductPriceUtils.LimitedLoanAmount(entity.ProductId.Value, entity.ResidentType.Value);

                                                    if ((entity.LoanAmount + resultCheckTopup.CurrentDebt) > limitedLoanAmount)
                                                    {
                                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, string.Format("Số tiền cho vay không được lớn hơn {0}đ", limitedLoanAmount - resultCheckTopup.CurrentDebt));
                                                        return Ok(def);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý HĐ-" + loanbriefCheck.LoanBriefId);
                                            return Ok(def);
                                        }
                                    }
                                }
                                //gói vay ô tô
                                else
                                {
                                    //Kiểm tra có đơn đang vay hay không
                                    //Nếu có đơn đang vay => chỉ cho tạo đơn topup đủ điêu kiện
                                    var loanbriefDisbursed = await _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.DISBURSED
                                        && (x.Phone == entity.Phone || (!string.IsNullOrEmpty(entity.NationalCard)
                                        && x.NationalCard == entity.NationalCard)), null, false).OrderByDescending(x => x.LoanBriefId)
                                        .Select(x => new { LoanBriefId = x.LoanBriefId, CustomerId = x.CustomerId }).FirstOrDefaultAsync(); ;
                                    if (loanbriefDisbursed != null && loanbriefDisbursed.LoanBriefId > 0)
                                    {
                                        if (!string.IsNullOrEmpty(entity.NationalCard) && !string.IsNullOrEmpty(entity.FullName) && loanbriefDisbursed.CustomerId > 0)
                                        {
                                            //xem có đơn topup nào đang xử lý không
                                            decimal totalMoneyBrrowing = 0;
                                            var lstLoanTopup = await _unitOfWork.LoanBriefRepository.Query(x => x.Status != (int)EnumLoanStatus.DISBURSED
                                                    && x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.FINISH
                                                    && x.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                                                    && x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp && x.PlatformType == (int)EnumPlatformType.ReMarketing
                                                    && x.CustomerId == loanbriefDisbursed.CustomerId, null, false).ToListAsync();
                                            if (lstLoanTopup != null && lstLoanTopup.Count > 0)
                                                totalMoneyBrrowing = lstLoanTopup.Sum(x => x.LoanAmount.Value);

                                            //cập nhập đơn thì loại bỏ số tiền của đơn này ra. Đã được sum ở trên
                                            totalMoneyBrrowing = totalMoneyBrrowing - entity.LoanAmount.Value;
                                            //check xem có đơn vay topup chưa
                                            //có rồi thì thông báo không tạo được đơn Topup
                                            //chưa có thì check xem đủ điều kiện Topup không
                                            var nationalCard = entity.NationalCard;
                                            if (!string.IsNullOrEmpty(entity.NationCardPlace))
                                                nationalCard = $"{entity.NationalCard},{entity.NationCardPlace}";
                                            var objCheckTopupOto = new CheckReLoan.Input
                                            {
                                                NumberCard = nationalCard,
                                                CustomerName = entity.FullName,
                                            };
                                            //check xem đơn có đủ điều kiện để topup không
                                            var checkTopupOto = _lmsService.CheckTopupOto(objCheckTopupOto);
                                            if (checkTopupOto.IsAccept == 1)
                                            {
                                                //số tiền dư nợ + với số tiền vay hiện tại nhỏ hơn 300tr thì cho cập nhập
                                                if (entity.LoanAmount + checkTopupOto.TotalMoneyCurrent + totalMoneyBrrowing > 300000000)
                                                {
                                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số tiền vay và số tiền dư nợ phải nhỏ hơn 300tr. Dư nợ hiện tại:" + (checkTopupOto.TotalMoneyCurrent + totalMoneyBrrowing).ToString("###,0") + " VNĐ.");
                                                    return Ok(def);
                                                }
                                            }
                                            else
                                            {
                                                def.meta = new Meta(ResponseHelper.FAIL_CODE, checkTopupOto.Message);
                                                return Ok(def);
                                            }
                                        }
                                    }
                                }
                            }

                            var checkPermission = CheckPermission(userInfo.GroupId, loanbrief.HubId.Value, loanbrief.HubEmployeeId.Value, loanbrief.Status.Value, userInfo.UserId);
                            if (checkPermission.errorCode != ResponseHelper.SUCCESS_CODE)
                            {
                                def.meta = checkPermission;
                                return Ok(def);
                            }

                            var messageValid = ValidLoanBriefInit(entity);
                            if (!string.IsNullOrEmpty(messageValid))
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, messageValid);
                                return Ok(def);
                            }

                            //xử lý chuẩn hóa tên KH
                            if (!string.IsNullOrEmpty(entity.FullName))
                            {
                                entity.FullName = entity.FullName.Trim();
                                entity.FullName = entity.FullName.ReduceWhitespace();
                                entity.FullName = entity.FullName.TitleCaseString();
                                if (!entity.FullName.IsNormalized())
                                    entity.FullName = entity.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                            }


                            //thông tin của user
                            var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                                    .Select(x => new
                                    {
                                        UserId = x.UserId,
                                        FullName = x.FullName,
                                        UserName = x.Username,
                                    }).FirstOrDefaultAsync();


                            //Lấy thông tin đơn vay cũ
                            var oldLoanBrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false)
                                .Select(LoanBriefInitDetail.ProjectionViewDetail).FirstOrDefaultAsync();
                            if (oldLoanBrief == null)
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay trong hệ thống");
                                return Ok(def);
                            }

                            //check xem khách hàng có tài khoản esign chưa
                            //có rồi thì không cho thay đổi tên và số cmnd nữa
                            if (!string.IsNullOrEmpty(oldLoanBrief.Customer.EsignAgreementId))
                            {
                                entity.FullName = oldLoanBrief.FullName;
                                entity.NationalCard = oldLoanBrief.NationalCard;
                            }

                            //kiểm tra có thay đổi thời gian vay không
                            if (entity.LoanTime != oldLoanBrief.LoanTime && entity.LoanTime > 0 && oldLoanBrief.LoanTime > 0)
                            {
                                await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                {
                                    LoanBriefId = entity.LoanBriefId,
                                    Note = string.Format("{0} thay đổi thời gian vay từ <b>{1}</b> tháng thành <b>{2}</b> tháng", userAction.FullName, oldLoanBrief.LoanTime, entity.LoanTime),
                                    FullName = userAction.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = userAction.UserId,
                                    ShopId = entity.HubId.Value,
                                    ShopName = "Tima"
                                });
                            }
                            //Kiểm tra xem có thay đổi gói vay không
                            bool isChangePipelineProduct = false;
                            bool isChangeProduct = false;
                            bool isChangePipeline = false;
                            if (entity.ProductId > 0 && oldLoanBrief.ProductId > 0 && entity.ProductId != oldLoanBrief.ProductId)
                            {
                                var changePipeline = _pipelineService.ChangePipeline(new ChangePipeline() { LoanBriefId = oldLoanBrief.LoanBriefId, ProductId = entity.ProductId.Value, Status = oldLoanBrief.Status.Value });
                                if (changePipeline == 1)
                                {
                                    isChangePipelineProduct = true;
                                    if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                        || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                        || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                        isChangeProduct = true;
                                    var oldProductName = Enum.IsDefined(typeof(EnumProductCredit), oldLoanBrief.ProductId) ? Description.GetDescription((EnumProductCredit)oldLoanBrief.ProductId) : "";
                                    var newProductName = Enum.IsDefined(typeof(EnumProductCredit), entity.ProductId) ? Description.GetDescription((EnumProductCredit)entity.ProductId) : "";

                                    await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                    {
                                        LoanBriefId = entity.LoanBriefId,
                                        Note = string.Format("{0} thay đổi gói vay từ <b>{1}</b> thành <b>{2}</b>", userAction.FullName, oldProductName, newProductName),
                                        FullName = userAction.FullName,
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = userAction.UserId,
                                        ShopId = entity.HubId.Value,
                                        ShopName = "Tima"
                                    });
                                }
                            }

                            //kiểm tra có thay đổi quận không
                            if (entity.DistrictId > 0 && oldLoanBrief.DistrictId > 0 && entity.DistrictId != oldLoanBrief.DistrictId)
                            {
                                if (userInfo.GroupId == (int)EnumGroupUser.StaffHub || userInfo.GroupId == (int)EnumGroupUser.ManagerHub)
                                {
                                    if (!isChangePipelineProduct)
                                        isChangePipeline = true;
                                }
                            }

                            //kiểm tra có thay đổi thông tin xe hay không hoặc thay đổi gói vay => định giá lại tài sản
                            //Nếu thay đổi công việc => thay đổi giới hạn cho vay
                            var oldProperty = await _unitOfWork.LoanBriefPropertyRepository.Query(x => x.LoanBriefPropertyId == entity.LoanBriefId, null, false).Select(x => new LoanBriefPropertyDTO()
                            {
                                BrandId = x.BrandId,
                                ProductId = x.ProductId
                            }).FirstOrDefaultAsync();
                            if (isChangeProduct || (oldProperty != null && oldProperty.BrandId > 0 && oldProperty.ProductId > 0)
                                || (entity.PropertyProductId > 0 && (oldProperty == null || oldProperty.ProductId == 0 || !oldProperty.ProductId.HasValue)))
                            {
                                if (entity.BrandId > 0 && entity.PropertyProductId > 0
                                    && oldProperty != null && oldProperty.ProductId > 0 && entity.PropertyProductId != oldProperty.ProductId)
                                {
                                    var oldProductName = string.Empty;
                                    if (oldProperty.ProductId.HasValue)
                                    {
                                        var oldProduct = _productService.GetProduct(oldProperty.ProductId.Value);
                                        if (oldProduct != null)
                                            oldProductName = oldProduct.FullName;
                                    }
                                    var newProduct = _productService.GetProduct(entity.PropertyProductId.Value);

                                    await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                    {
                                        LoanBriefId = entity.LoanBriefId,
                                        Note = isChangeProduct ? string.Format("{0} cập nhật xe {1}", userAction.FullName, newProduct.FullName) : string.Format("{0} thay đổi xe máy {1} thành {2}", userAction.FullName, oldProductName, newProduct.FullName),
                                        FullName = userAction.FullName,
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = userAction.UserId,
                                        ShopId = entity.HubId.Value,
                                        ShopName = "Tima"
                                    });
                                    _unitOfWork.Save();

                                    var priceAG = 0l;
                                    decimal priceAI = 0l;
                                    if (entity.ResidentType.HasValue && entity.MaxPrice.HasValue)
                                    {
                                        var typeRemarketingLtv = 0;
                                        var loanAmountExpertiseLast = 0M;
                                        //Nếu là đơn topup => trừ đi dư nợ đang có
                                        //Đối với gói vay xe máy topup
                                        if (entity.TypeRemarketing == EnumTypeRemarketing.IsTopUp.GetHashCode()
                                            && oldLoanBrief.ReMarketingLoanBriefId > 0
                                            && (entity.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                                || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                                || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))
                                        {


                                            //Kiểm tra đơm vay gốc có đủ điều kiện topup không
                                            if (resultCheckTopup == null || resultCheckTopup.CurrentDebt <= 0)
                                                resultCheckTopup = await _loanbriefService.CheckCanTopup(entity.ReMarketingLoanBriefId.Value);
                                            if (resultCheckTopup.IsCanTopup)
                                            {
                                                var totalMoneyDebtCurrent = (long)resultCheckTopup.CurrentDebt;
                                                loanAmountExpertiseLast = GetMaxPriceProductTopup(newProduct.Id, entity.LoanBriefId, entity.ProductId.Value, entity.ResidentType.Value, totalMoneyDebtCurrent, ref priceAI);
                                                if (loanAmountExpertiseLast < 3000000)
                                                {
                                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không đủ điều kiện vay Topup định giá tài sản nhỏ hơn 3tr");
                                                    return Ok(def);
                                                }
                                            }
                                            typeRemarketingLtv = (int)TypeRemarketingLtv.TopUp;
                                        }
                                        if (oldLoanBrief.IsReborrow.GetValueOrDefault(false))
                                            typeRemarketingLtv = (int)TypeRemarketingLtv.Reborrow;
                                        if (loanAmountExpertiseLast == 0)
                                            loanAmountExpertiseLast = GetMaxPriceProductV3((int)newProduct.Id, entity.LoanBriefId, (long)entity.MaxPrice, entity.ProductId.Value, ref priceAG, ref priceAI);

                                        entity.LoanAmountExpertiseLast = loanAmountExpertiseLast;
                                        entity.LoanAmountExpertise = priceAG;
                                        if (priceAI > 0)
                                            entity.LoanAmountExpertiseAi = priceAI;
                                        if (entity.LoanAmountExpertiseLast > 0)
                                        {
                                            await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                            {
                                                LoanBriefId = entity.LoanBriefId,
                                                Note = string.Format("Xe của khách hàng được định giá tối đa {0}đ", entity.LoanAmountExpertiseLast.Value.ToString("##,#")),
                                                FullName = userAction.FullName,
                                                Status = 1,
                                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                                CreatedTime = DateTime.Now,
                                                UserId = userAction.UserId,
                                                ShopId = entity.HubId.Value,
                                                ShopName = "Tima"
                                            });
                                            _unitOfWork.Save();
                                        }
                                    }
                                }
                                else
                                {
                                    if (entity.PropertyProductId > 0)
                                    {
                                        entity.LoanAmountExpertiseLast = oldLoanBrief.LoanAmountExpertiseLast;
                                        entity.LoanAmountExpertise = oldLoanBrief.LoanAmountExpertise;
                                        entity.LoanAmountExpertiseAi = oldLoanBrief.LoanAmountExpertiseAi;
                                        if (entity.LoanAmountExpertiseLast == 0 || !entity.LoanAmountExpertiseLast.HasValue || (entity.LoanAmountExpertiseAi <= 0 && entity.LoanAmountExpertise <= 0) || isChangeProduct)
                                        {
                                            var newProduct = _productService.GetProduct(entity.PropertyProductId.Value);
                                            var priceAG = 0l;
                                            decimal priceAI = 0l;
                                            if (entity.ResidentType.HasValue)
                                            {
                                                var loanAmountExpertiseLast = 0M;
                                                //Nếu là đơn topup => trừ đi dư nợ đang có
                                                //Đối với gói vay xe máy topup
                                                if (entity.TypeRemarketing == EnumTypeRemarketing.IsTopUp.GetHashCode()
                                                    && oldLoanBrief.ReMarketingLoanBriefId > 0
                                                    && (entity.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                                        || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                                        || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))
                                                {
                                                    //Kiểm tra đơm vay gốc có đủ điều kiện topup không
                                                    if (resultCheckTopup == null || resultCheckTopup.CurrentDebt <= 0)
                                                        resultCheckTopup = await _loanbriefService.CheckCanTopup(entity.ReMarketingLoanBriefId.Value);
                                                    if (resultCheckTopup.IsCanTopup)
                                                    {
                                                        var totalMoneyDebtCurrent = (long)resultCheckTopup.CurrentDebt;
                                                        loanAmountExpertiseLast = GetMaxPriceProductTopup(newProduct.Id, entity.LoanBriefId, entity.ProductId.Value, entity.ResidentType.Value, totalMoneyDebtCurrent, ref priceAI);
                                                        if (loanAmountExpertiseLast < 3000000)
                                                        {
                                                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không đủ điều kiện vay Topup định giá tài sản nhỏ hơn 3tr");
                                                            return Ok(def);
                                                        }
                                                    }
                                                }
                                                if (loanAmountExpertiseLast == 0)
                                                    loanAmountExpertiseLast = GetMaxPriceProductV3((int)newProduct.Id, entity.LoanBriefId, (long)entity.MaxPrice.GetValueOrDefault(0), entity.ProductId.Value, ref priceAG, ref priceAI);

                                                entity.LoanAmountExpertiseLast = loanAmountExpertiseLast;
                                                entity.LoanAmountExpertise = priceAG;
                                                if (priceAI > 0)
                                                    entity.LoanAmountExpertiseAi = priceAI;
                                                if (entity.LoanAmountExpertiseLast > 0)
                                                {
                                                    await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                                    {
                                                        LoanBriefId = entity.LoanBriefId,
                                                        Note = string.Format("Xe của khách hàng được định giá tối đa {0}đ", entity.LoanAmountExpertiseLast.Value.ToString("##,#")),
                                                        FullName = userAction.FullName,
                                                        Status = 1,
                                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                                        CreatedTime = DateTime.Now,
                                                        UserId = userAction.UserId,
                                                        ShopId = entity.HubId.Value,
                                                        ShopName = "Tima"
                                                    });
                                                    _unitOfWork.Save();
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            entity.LoanAmountFirst = oldLoanBrief.LoanAmountFirst > 0 ? oldLoanBrief.LoanAmountFirst : entity.LoanAmount;
                            //Call qua Result Ekyc check lấy kết quả
                            if (String.IsNullOrEmpty(entity.AddressNationalCard))
                            {
                                var lstEkyc = await _unitOfWork.ResultEkycRepository.Query(x => x.LoanbriefId == entity.LoanBriefId, null, false).ToListAsync();
                                var ekyc = new ResultEkyc();
                                if (lstEkyc != null && lstEkyc.Count > 0)
                                    ekyc = lstEkyc.OrderByDescending(x => x.Id).FirstOrDefault();

                                if (ekyc != null && !String.IsNullOrEmpty(ekyc.AddressValue))
                                    entity.AddressNationalCard = ekyc.AddressValue;
                            }

                            //kiểm tra có thay đổi LTV không
                            if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                            {
                                if (entity.Ltv.GetValueOrDefault(0) > 0 && entity.Ltv != oldLoanBrief.Ltv.GetValueOrDefault(0))
                                {
                                    await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                    {
                                        LoanBriefId = entity.LoanBriefId,
                                        Note = string.Format("{0} thay đổi LTV từ <b>{1}</b> % thành <b>{2}</b> %", userAction.FullName, oldLoanBrief.Ltv, entity.Ltv),
                                        FullName = userAction.FullName,
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = userAction.UserId,
                                        ShopId = entity.HubId.Value,
                                        ShopName = "Tima"
                                    });
                                }
                            }

                            entity.IsCheckBank = entity.IsCheckBank;
                            entity.Frequency = 1;

                            //kiểm tra xem có thay đổi thông tin quan trọng không
                            //Check xem có thay đổi CMND
                            var noteChange = "";
                            if (!string.IsNullOrEmpty(entity.NationalCard) && !string.IsNullOrEmpty(oldLoanBrief.NationalCard)
                                && entity.NationalCard != oldLoanBrief.NationalCard)
                            {
                                noteChange += string.Format("CMND: <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.NationalCard, entity.NationalCard);
                            }
                            //check xem có thay đổi thành phố không
                            if (entity.ProvinceId != null && oldLoanBrief.ProvinceId != null && entity.ProvinceId != oldLoanBrief.ProvinceId)
                            {
                                var oldProvinceName = await _unitOfWork.ProvinceRepository.Query(x => x.ProvinceId == oldLoanBrief.ProvinceId.Value, null, false).Select(x => new { Name = x.Name }).FirstOrDefaultAsync();
                                var newProvinceName = await _unitOfWork.ProvinceRepository.Query(x => x.ProvinceId == entity.ProvinceId.Value, null, false).Select(x => new { Name = x.Name }).FirstOrDefaultAsync();
                                noteChange += string.Format("Thành phố: <b>{0}</b> thành <b>{1}</b> <br />", oldProvinceName.Name, newProvinceName.Name);
                            }
                            //check xem có thay đổi quận huyện không
                            if (entity.DistrictId != null && oldLoanBrief.DistrictId != null && entity.DistrictId != oldLoanBrief.DistrictId)
                            {
                                var oldDistrictName = await _unitOfWork.DistrictRepository.Query(x => x.DistrictId == oldLoanBrief.DistrictId.Value, null, false).Select(x => new { Name = x.Name }).FirstOrDefaultAsync();
                                var newDistrictName = await _unitOfWork.DistrictRepository.Query(x => x.DistrictId == entity.DistrictId.Value, null, false).Select(x => new { Name = x.Name }).FirstOrDefaultAsync();
                                noteChange += string.Format("Quận/Huyện: <b>{0}</b> thành <b>{1}</b> <br />", oldDistrictName.Name, newDistrictName.Name);
                            }
                            //check xem số tiền cho vay tối đa có thay đổi không
                            if (entity.LoanAmountExpertiseLast != null && oldLoanBrief.LoanAmountExpertiseLast != null &&
                                (entity.LoanAmountExpertiseLast != oldLoanBrief.LoanAmountExpertiseLast))
                            {
                                noteChange += string.Format("Số tiền cho vay tối đa thay đổi <b>{0}</b> thành <b>{1}</b>", oldLoanBrief.LoanAmountExpertiseLast.Value.ToString("###,0"), entity.LoanAmountExpertiseLast.Value.ToString("###,0"));
                            }

                            //Check xem có thay đổi thu nhập không
                            if (entity.TotalIncome != null && oldLoanBrief.LoanBriefJob.TotalIncome != null
                                && entity.TotalIncome != oldLoanBrief.LoanBriefJob.TotalIncome)
                            {
                                noteChange += string.Format("Thu nhập: <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.LoanBriefJob.TotalIncome.Value.ToString("###,0"), entity.TotalIncome.Value.ToString("###,0"));
                            }

                            //check job có thay đổi không
                            //thay đổi thêm vào comment
                            if (oldLoanBrief.LoanBriefJob != null && entity.JobId != null && oldLoanBrief.LoanBriefJob.JobId != null
                                && (entity.JobId != oldLoanBrief.LoanBriefJob.JobId))
                            {
                                var newJobName = await _unitOfWork.JobRepository.Query(x => x.JobId == entity.JobId.Value, null, false).Select(x => new { Name = x.Name }).FirstOrDefaultAsync();
                                var oldJobName = await _unitOfWork.JobRepository.Query(x => x.JobId == oldLoanBrief.LoanBriefJob.JobId.Value, null, false).Select(x => new { Name = x.Name }).FirstOrDefaultAsync();
                                noteChange = string.Format("Nghề nghiệp: <b>{0}</b> thành <b>{1}</b> <br />", oldJobName.Name, newJobName.Name);
                            }
                            //check xem hình thức sở hữu nhà có thanh đổi không
                            if (oldLoanBrief.LoanBriefResident != null && entity.ResidentType != null && oldLoanBrief.LoanBriefResident.ResidentType != null
                                && (entity.ResidentType != oldLoanBrief.LoanBriefResident.ResidentType))
                            {
                                var oldResidentType = Description.GetDescription((EnumTypeofownership)oldLoanBrief.LoanBriefResident.ResidentType);
                                var newResidentType = Description.GetDescription((EnumTypeofownership)entity.ResidentType);
                                noteChange += string.Format("Hình thức sở hữu nhà: <b>{0}</b> thành <b>{1}</b><br />", oldResidentType, newResidentType);
                            }
                            //check xem tọa độ google map nơi ở có thay đổi không
                            if (oldLoanBrief.LoanBriefResident != null && !string.IsNullOrEmpty(entity.ResidentAddressLatLng) && !string.IsNullOrEmpty(oldLoanBrief.LoanBriefResident.AddressLatLng)
                                && entity.ResidentAddressLatLng != oldLoanBrief.LoanBriefResident.AddressLatLng)
                            {
                                noteChange += string.Format("Tọa độ Google Map nơi ở: <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.LoanBriefResident.AddressLatLng, entity.ResidentAddressLatLng);
                            }

                            //Kiểm tra xem có thay đổi từ mua bảo hiểm không
                            if (oldLoanBrief.LoanBriefProperty != null
                                && oldLoanBrief.LoanBriefProperty.BuyInsuranceProperty.GetValueOrDefault(false) != entity.BuyInsuranceProperty.GetValueOrDefault(false))
                            {
                                noteChange += string.Format("Bảo hiểm tài sản thay đổi từ: <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.LoanBriefProperty.BuyInsuranceProperty == true ? "có mua" : "không mua", entity.BuyInsuranceProperty == true ? "có mua" : "không mua");
                            }

                            if (!string.IsNullOrEmpty(noteChange))
                            {
                                await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                {
                                    LoanBriefId = entity.LoanBriefId,
                                    Note = noteChange,
                                    FullName = userAction.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = userAction.UserId,
                                    ShopId = entity.HubId.Value,
                                    ShopName = "Tima"
                                });
                            }

                            //Check rate
                            if (oldLoanBrief.RatePercent > 0 && oldLoanBrief.RateMoney > 0)
                            {
                                if (entity.ProductId == EnumProductCredit.OtoCreditType_CC.GetHashCode())
                                {
                                    var rate = Common.Utils.ProductPriceUtils.GetCalculatorRate(entity.ProductId.Value, entity.LoanAmount, entity.LoanTime, entity.RateTypeId);
                                    entity.RateMoney = rate.Item1;
                                    entity.RatePercent = rate.Item2;
                                }
                                else
                                {
                                    entity.RateMoney = oldLoanBrief.RateMoney;
                                    entity.RatePercent = oldLoanBrief.RatePercent;
                                }
                            }
                            else
                            {
                                if (entity.ProductId > 0)
                                {
                                    var rate = Common.Utils.ProductPriceUtils.GetCalculatorRate(entity.ProductId.Value, entity.LoanAmount, entity.LoanTime, entity.RateTypeId);
                                    entity.RateMoney = rate.Item1;
                                    entity.RatePercent = rate.Item2;
                                }
                            }

                            //Kiểm tra số tiền thay đổi => lưu comment
                            if (entity.LoanAmount != oldLoanBrief.LoanAmount && entity.LoanAmount > 0 && oldLoanBrief.LoanAmount > 0)
                            {
                                await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                {
                                    LoanBriefId = entity.LoanBriefId,
                                    Note = string.Format("Cập nhật: {0} thay đổi số tiền từ {1}đ thành {2}đ", userAction.FullName, oldLoanBrief.LoanAmount.Value.ToString("#,##"), entity.LoanAmount.Value.ToString("#,##")),
                                    FullName = userAction.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = userAction.UserId,
                                    ShopId = entity.HubId.Value,
                                    ShopName = "Tima"
                                });
                            }

                            //kiểm tra có thay đổi xác nhận giao dịch đảm bảo
                            if (entity.IsTransactionsSecured.GetValueOrDefault(false) != oldLoanBrief.IsTransactionsSecured.GetValueOrDefault(false))
                            {
                                var mes1 = oldLoanBrief.IsTransactionsSecured.GetValueOrDefault(false) == true ? "Đã xác nhận" : "Không xác nhận";
                                var mes2 = entity.IsTransactionsSecured.GetValueOrDefault(false) == true ? "Đã xác nhận" : "Không xác nhận";
                                await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                {
                                    LoanBriefId = entity.LoanBriefId,
                                    Note = string.Format("{0} thay đổi xác nhận giao dịch đảm bảo từ <b>{1}</b> thành <b>{2}</b>", userAction.FullName, mes1, mes2),
                                    FullName = userAction.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = userAction.UserId,
                                    ShopId = entity.HubId.Value,
                                    ShopName = "Tima"
                                });
                            }

                            if (entity.FromDate == null)
                                entity.FromDate = DateTime.Now;
                            if (entity.LoanTime > 0)
                                entity.ToDate = entity.FromDate.Value.AddMonths((int)entity.LoanTime);

                            var feePaymentBeforeLoan = Common.Utils.ProductPriceUtils.GetFeePaymentBeforeLoan(entity.ProductId.Value, entity.LoanTime);

                            var firstProcessingTime = DateTime.Now;

                            if (oldLoanBrief.FirstProcessingTime.HasValue)
                                firstProcessingTime = oldLoanBrief.FirstProcessingTime.Value;

                            #region Customer
                            await _unitOfWork.CustomerRepository.UpdateAsync(x => x.CustomerId == entity.CustomerId, x => new Customer()
                            {
                                FullName = entity.FullName,
                                JobId = entity.JobId,
                                CompanyName = entity.CompanyName,
                                CompanyAddress = entity.CompanyAddress,
                                ProvinceId = entity.ProvinceId,
                                DistrictId = entity.DistrictId,
                                WardId = entity.WardId,
                                Address = entity.ResidentAddress,
                                UpdatedAt = DateTime.Now,
                                FacebookAddress = entity.FacebookAddress,
                                InsurenceNumber = entity.InsurenceNumber,
                                InsurenceNote = entity.InsurenceNote,
                                IsMerried = entity.IsMerried,
                                NumberBaby = entity.NumberBaby,
                                Dob = entity.BirthDay,
                                Gender = entity.Gender,
                                NationalCard = entity.NationalCard,
                                NationCardPlace = entity.NationCardPlace,
                                Passport = entity.Passport

                            });
                            #endregion

                            #region Loanbrief
                            await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                            {
                                PlatformType = entity.PlatformType.GetValueOrDefault(),
                                TypeLoanBrief = entity.TypeLoanBrief.GetValueOrDefault(),
                                FullName = entity.FullName,
                                NationalCard = entity.NationalCard,
                                NationCardPlace = entity.NationCardPlace,
                                IsTrackingLocation = entity.IsTrackingLocation,
                                Dob = entity.BirthDay,
                                Passport = entity.Passport,
                                Gender = entity.Gender,
                                NumberCall = entity.NumberCall,
                                ProvinceId = entity.ProvinceId,
                                DistrictId = entity.DistrictId,
                                WardId = entity.WardId,
                                ProductId = entity.ProductId.GetValueOrDefault(),
                                RateTypeId = entity.RateTypeId.GetValueOrDefault(),
                                LoanAmount = entity.LoanAmount.GetValueOrDefault(),
                                BuyInsurenceCustomer = entity.BuyInsurenceCustomer,
                                LoanTime = entity.LoanTime,
                                Frequency = entity.Frequency,
                                RateMoney = entity.RateMoney,
                                RatePercent = entity.RatePercent,
                                ReceivingMoneyType = entity.ReceivingMoneyType,
                                BankId = entity.BankId,
                                BankAccountNumber = entity.BankAccountNumber,
                                BankCardNumber = entity.BankCardNumber,
                                BankAccountName = entity.BankAccountName,
                                IsLocate = entity.IsLocate,
                                FromDate = entity.FromDate,
                                ToDate = entity.ToDate,
                                PresenterCode = entity.PresenterCode,
                                LoanAmountFirst = entity.LoanAmountFirst,
                                LoanAmountExpertise = entity.LoanAmountExpertise,
                                LoanAmountExpertiseLast = entity.LoanAmountExpertiseLast,
                                LoanAmountExpertiseAi = entity.LoanAmountExpertiseAi,
                                FirstProcessingTime = firstProcessingTime,
                                IsCheckBank = entity.IsCheckBank,
                                HomeNetwork = entity.HomeNetwork,
                                PhoneOther = entity.PhoneOther,
                                BankBranch = entity.BankBranch,
                                ProductDetailId = entity.ProductDetailId,
                                IsSim = entity.IsSim,
                                LoanPurpose = entity.LoanPurpose,
                                FeePaymentBeforeLoan = feePaymentBeforeLoan,
                                Email = entity.Email,
                                IsTransactionsSecured = entity.IsTransactionsSecured,
                                Ltv = entity.Ltv,
                            });
                            #endregion

                            #region LoanBriefResident
                            if (await _unitOfWork.LoanBriefResidentRepository.AnyAsync(x => x.LoanBriefResidentId == entity.LoanBriefId))
                            {
                                await _unitOfWork.LoanBriefResidentRepository.UpdateAsync(x => x.LoanBriefResidentId == entity.LoanBriefId, x => new LoanBriefResident()
                                {
                                    LoanBriefResidentId = entity.LoanBriefId,
                                    LivingTime = entity.LivingTime,
                                    ResidentType = entity.ResidentType,
                                    LivingWith = entity.LivingWith,
                                    Address = entity.ResidentAddress,
                                    AddressGoogleMap = entity.ResidentAddressGoogleMap,
                                    AddressLatLng = entity.ResidentAddressLatLng,
                                    AddressNationalCard = entity.AddressNationalCard,
                                    ProvinceId = entity.ProvinceId,
                                    DistrictId = entity.DistrictId,
                                    WardId = entity.WardId,
                                    CustomerShareLocation = entity.CustomerShareLocation,
                                });
                            }
                            else
                            {
                                await _unitOfWork.LoanBriefResidentRepository.InsertAsync(new LoanBriefResident()
                                {
                                    LoanBriefResidentId = entity.LoanBriefId,
                                    LivingTime = entity.LivingTime,
                                    ResidentType = entity.ResidentType,
                                    LivingWith = entity.LivingWith,
                                    Address = entity.ResidentAddress,
                                    ProvinceId = entity.ProvinceId,
                                    DistrictId = entity.DistrictId,
                                    WardId = entity.WardId,
                                    AddressNationalCard = entity.AddressNationalCard,
                                    CustomerShareLocation = entity.CustomerShareLocation,
                                });
                            }
                            #endregion

                            #region LoanBriefProperty
                            if (await _unitOfWork.LoanBriefPropertyRepository.AnyAsync(x => x.LoanBriefPropertyId == entity.LoanBriefId))
                            {
                                await _unitOfWork.LoanBriefPropertyRepository.UpdateAsync(x => x.LoanBriefPropertyId == entity.LoanBriefId, x => new LoanBriefProperty()
                                {
                                    LoanBriefPropertyId = entity.LoanBriefId,
                                    BrandId = entity.BrandId,
                                    ProductId = entity.PropertyProductId,
                                    PlateNumber = entity.PlateNumber,
                                    UpdatedTime = DateTime.Now,
                                    CarManufacturer = entity.CarManufacturer,
                                    CarName = entity.CarName,
                                    CarColor = entity.CarColor,
                                    PlateNumberCar = entity.PlateNumberCar,
                                    Chassis = entity.Chassis,
                                    Engine = entity.Engine,
                                    BuyInsuranceProperty = entity.BuyInsuranceProperty,
                                    OwnerFullName = entity.OwnerFullName,
                                    MotobikeCertificateNumber = entity.MotobikeCertificateNumber,
                                    MotobikeCertificateDate = entity.MotobikeCertificateDate,
                                    MotobikeCertificateAddress = entity.MotobikeCertificateAddress,
                                    PostRegistration = entity.PostRegistration,
                                    Description = entity.PropertyDescription,
                                });
                            }
                            else
                            {
                                await _unitOfWork.LoanBriefPropertyRepository.InsertAsync(new LoanBriefProperty()
                                {
                                    LoanBriefPropertyId = entity.LoanBriefId,
                                    BrandId = entity.BrandId,
                                    ProductId = entity.PropertyProductId,
                                    PlateNumber = entity.PlateNumber,
                                    UpdatedTime = DateTime.Now,
                                    CarManufacturer = entity.CarManufacturer,
                                    CarName = entity.CarName,
                                    CarColor = entity.CarColor,
                                    PlateNumberCar = entity.PlateNumberCar,
                                    Chassis = entity.Chassis,
                                    Engine = entity.Engine,
                                    BuyInsuranceProperty = entity.BuyInsuranceProperty,
                                    OwnerFullName = entity.OwnerFullName,
                                    MotobikeCertificateNumber = entity.MotobikeCertificateNumber,
                                    MotobikeCertificateDate = entity.MotobikeCertificateDate,
                                    MotobikeCertificateAddress = entity.MotobikeCertificateAddress,
                                    PostRegistration = entity.PostRegistration,
                                    Description = entity.PropertyDescription,
                                });
                            }

                            #endregion

                            #region LoanBriefHousehold
                            if (await _unitOfWork.LoanBriefHouseholdRepository.AnyAsync(x => x.LoanBriefHouseholdId == entity.LoanBriefId))
                            {
                                await _unitOfWork.LoanBriefHouseholdRepository.UpdateAsync(x => x.LoanBriefHouseholdId == entity.LoanBriefId, x => new LoanBriefHousehold()
                                {
                                    LoanBriefHouseholdId = entity.LoanBriefId,
                                    Address = entity.HouseholdAddress,
                                    ProvinceId = entity.HouseholdProvinceId,
                                    DistrictId = entity.HouseholdDistrictId,
                                    WardId = entity.HouseholdWardId,
                                    FullNameHouseOwner = entity.FullNameHouseOwner,
                                    RelationshipHouseOwner = entity.RelationshipHouseOwner,
                                    BirdayHouseOwner = entity.BirdayHouseOwner,
                                });
                            }
                            else
                            {
                                await _unitOfWork.LoanBriefHouseholdRepository.InsertAsync(new LoanBriefHousehold()
                                {
                                    LoanBriefHouseholdId = entity.LoanBriefId,
                                    Address = entity.HouseholdAddress,
                                    ProvinceId = entity.HouseholdProvinceId,
                                    DistrictId = entity.HouseholdDistrictId,
                                    WardId = entity.HouseholdWardId,
                                    FullNameHouseOwner = entity.FullNameHouseOwner,
                                    RelationshipHouseOwner = entity.RelationshipHouseOwner,
                                    BirdayHouseOwner = entity.BirdayHouseOwner,
                                });
                            }

                            #endregion

                            #region LoanBriefJob
                            if (await _unitOfWork.LoanBriefJobRepository.AnyAsync(x => x.LoanBriefJobId == entity.LoanBriefId))
                            {
                                await _unitOfWork.LoanBriefJobRepository.UpdateAsync(x => x.LoanBriefJobId == entity.LoanBriefId, x => new LoanBriefJob()
                                {
                                    LoanBriefJobId = entity.LoanBriefId,
                                    JobId = entity.JobId,
                                    CompanyName = entity.CompanyName,
                                    CompanyPhone = entity.CompanyPhone,
                                    CompanyTaxCode = entity.CompanyTaxCode,
                                    CompanyProvinceId = entity.CompanyProvinceId,
                                    CompanyDistrictId = entity.CompanyDistrictId,
                                    CompanyWardId = entity.CompanyWardId,
                                    CompanyAddress = entity.CompanyAddress,
                                    TotalIncome = entity.TotalIncome,
                                    ImcomeType = entity.ImcomeType,
                                    Description = entity.JobDescription,
                                    CompanyAddressGoogleMap = entity.CompanyAddressGoogleMap,
                                    CompanyAddressLatLng = entity.CompanyAddressLatLng,
                                    CompanyInsurance = entity.CompanyInsurance,
                                    WorkLocation = entity.WorkLocation,
                                    BusinessPapers = entity.BusinessPapers,
                                    JobDescriptionId = entity.JobDescriptionId,
                                });
                            }
                            else
                            {
                                await _unitOfWork.LoanBriefJobRepository.InsertAsync(new LoanBriefJob()
                                {
                                    LoanBriefJobId = entity.LoanBriefId,
                                    JobId = entity.JobId,
                                    CompanyName = entity.CompanyName,
                                    CompanyPhone = entity.CompanyPhone,
                                    CompanyTaxCode = entity.CompanyTaxCode,
                                    CompanyProvinceId = entity.CompanyProvinceId,
                                    CompanyDistrictId = entity.CompanyDistrictId,
                                    CompanyWardId = entity.CompanyWardId,
                                    CompanyAddress = entity.CompanyAddress,
                                    TotalIncome = entity.TotalIncome,
                                    ImcomeType = entity.ImcomeType,
                                    Description = entity.JobDescription,
                                    //CompanyAddressGoogleMap = entity.CompanyAddressGoogleMap,
                                    //CompanyAddressLatLng = entity.CompanyAddressLatLng,
                                    CompanyInsurance = entity.CompanyInsurance,
                                    WorkLocation = entity.WorkLocation,
                                    BusinessPapers = entity.BusinessPapers,
                                    JobDescriptionId = entity.JobDescriptionId,
                                });
                            }
                            #endregion

                            #region LoanBriefCompany
                            if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                            {
                                if (await _unitOfWork.LoanBriefCompanyRepository.AnyAsync(x => x.LoanBriefId == entity.LoanBriefId))
                                {
                                    await _unitOfWork.LoanBriefCompanyRepository.UpdateAsync(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBriefCompany()
                                    {
                                        LoanBriefId = entity.LoanBriefId,
                                        CompanyName = entity.EnterpriseName,
                                        CareerBusiness = entity.CareerBusiness,
                                        BusinessCertificationAddress = entity.BusinessCertificationAddress,
                                        HeadOffice = entity.HeadOffice,
                                        Address = entity.EnterpriseAddress,
                                        CompanyShareholder = entity.CompanyShareholder,
                                        CardNumberShareholderDate = entity.CardNumberShareholderDate,
                                        BusinessCertificationDate = entity.BusinessCertificationDate,
                                        BirthdayShareholder = entity.BirthdayShareholder,
                                        CardNumberShareholder = entity.CardNumberShareholder,
                                        PlaceOfBirthShareholder = entity.PlaceOfBirthShareholder,
                                        UpdatedDate = DateTime.Now
                                    });
                                }
                                else
                                {
                                    await _unitOfWork.LoanBriefCompanyRepository.InsertAsync(new LoanBriefCompany()
                                    {
                                        LoanBriefId = entity.LoanBriefId,
                                        CompanyName = entity.EnterpriseName,
                                        CareerBusiness = entity.CareerBusiness,
                                        BusinessCertificationAddress = entity.BusinessCertificationAddress,
                                        HeadOffice = entity.HeadOffice,
                                        Address = entity.EnterpriseAddress,
                                        CompanyShareholder = entity.CompanyShareholder,
                                        CardNumberShareholderDate = entity.CardNumberShareholderDate,
                                        BusinessCertificationDate = entity.BusinessCertificationDate,
                                        BirthdayShareholder = entity.BirthdayShareholder,
                                        CardNumberShareholder = entity.CardNumberShareholder,
                                        PlaceOfBirthShareholder = entity.PlaceOfBirthShareholder,
                                        CreatedDate = DateTime.Now
                                    });
                                }

                            }
                            #endregion

                            #region LoanBriefRelationship
                            if (entity.LoanBriefRelationshipModels != null && entity.LoanBriefRelationshipModels.Count > 0)
                            {
                                if (await _unitOfWork.LoanBriefRelationshipRepository.AnyAsync(x => x.LoanBriefId == entity.LoanBriefId))
                                    await _unitOfWork.LoanBriefRelationshipRepository.DeleteAsync(x => x.LoanBriefId == entity.LoanBriefId);
                                foreach (var relationship in entity.LoanBriefRelationshipModels)
                                {
                                    if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                                    {
                                        await _unitOfWork.LoanBriefRelationshipRepository.InsertAsync(new LoanBriefRelationship()
                                        {
                                            LoanBriefId = entity.LoanBriefId,
                                            RelationshipType = relationship.RelationshipType,
                                            FullName = relationship.FullName,
                                            Phone = relationship.Phone,
                                            CreatedDate = DateTime.Now,
                                            Address = relationship.Address
                                        });
                                    }
                                }
                            }
                            #endregion

                            #region LoanBriefQuestionScript
                            if (await _unitOfWork.LoanBriefQuestionScriptRepository.AnyAsync(x => x.LoanBriefId == entity.LoanBriefId))
                            {
                                await _unitOfWork.LoanBriefQuestionScriptRepository.UpdateAsync(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBriefQuestionScript()
                                {
                                    WaterSupplier = entity.WaterSupplier,
                                    QuestionHouseOwner = entity.QuestionHouseOwner,
                                    Appraiser = entity.Appraiser,
                                    DocumentBusiness = entity.DocumentBusiness,
                                    MaxPrice = entity.MaxPrice,
                                });
                            }
                            else
                            {
                                await _unitOfWork.LoanBriefQuestionScriptRepository.InsertAsync(new LoanBriefQuestionScript()
                                {
                                    LoanBriefId = entity.LoanBriefId,
                                    WaterSupplier = entity.WaterSupplier,
                                    QuestionHouseOwner = entity.QuestionHouseOwner,
                                    Appraiser = entity.Appraiser,
                                    DocumentBusiness = entity.DocumentBusiness,
                                    MaxPrice = entity.MaxPrice,
                                });
                            }
                            #endregion

                            _unitOfWork.Save();

                            //kiểm tra có thay đổi quận không
                            if (isChangePipeline)
                            {
                                _pipelineService.ChangePipeline(new ChangePipeline()
                                { LoanBriefId = oldLoanBrief.LoanBriefId, ProductId = entity.ProductId.Value, Status = oldLoanBrief.Status.Value });
                            }

                            //Lưu log lịch sử chuyển 
                            await _unitOfWork.LoanBriefHistoryRepository.InsertAsync(new LoanBriefHistory()
                            {
                                LoanBriefId = entity.LoanBriefId,
                                CreatedTime = DateTime.Now,
                                UserId = userInfo.UserId,
                                ActorId = EnumActor.UpdateLoanbrief.GetHashCode(),
                                OldValue = JsonConvert.SerializeObject(oldLoanBrief, Formatting.Indented, new JsonSerializerSettings()
                                {
                                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                                }),
                                NewValue = JsonConvert.SerializeObject(entity, Formatting.Indented, new JsonSerializerSettings()
                                {
                                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                                })
                            });

                            //Log Action
                            await _unitOfWork.LogLoanActionRepository.InsertAsync(new LogLoanAction()
                            {
                                LoanbriefId = entity.LoanBriefId,
                                ActionId = (int)EnumLogLoanAction.LoanUpdateScript,
                                TypeAction = (int)EnumTypeAction.Manual,
                                LoanStatus = oldLoanBrief.Status,
                                TlsLoanStatusDetail = oldLoanBrief.DetailStatusTelesales,
                                HubLoanStatusDetail = oldLoanBrief.LoanStatusDetailChild,
                                TelesaleId = oldLoanBrief.BoundTelesaleId,
                                HubId = oldLoanBrief.HubId,
                                HubEmployeeId = oldLoanBrief.HubEmployeeId,
                                CoordinatorUserId = oldLoanBrief.CoordinatorUserId,
                                UserActionId = userInfo.UserId,
                                GroupUserActionId = userInfo.GroupId,
                                OldValues = JsonConvert.SerializeObject(oldLoanBrief),
                                NewValues = JsonConvert.SerializeObject(entity),
                                CreatedAt = DateTime.Now
                            });

                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            def.data = entity.LoanBriefId;
                        }
                        else
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                    }
                }
            }
            catch (Exception ex)
            {
                // _unitOfWork.RollbackTransaction();
                Log.Error(ex, "DirectSale/InitLoanBrief PUT Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("change_hub_employee")]
        public async Task<ActionResult<DefaultResponse<Meta, ChangeHubEmployeeResponse>>> ChangeHubEmployee(int loanbriefid)
        {
            var def = new DefaultResponse<Meta, ChangeHubEmployeeResponse>();
            try
            {
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var data = new ChangeHubEmployeeResponse();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefid, null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            EvaluationHomeUserId = x.EvaluationHomeUserId,
                            EvaluationCompanyUserId = x.EvaluationCompanyUserId,
                            Status = x.Status,
                            HubId = x.HubId,
                            HubEmployeeId = x.HubEmployeeId
                        }).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            if (loanbrief.Status != EnumLoanStatus.APPRAISER_REVIEW.GetHashCode())
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không ở trạng thái đang thẩm định");
                                return Ok(def);
                            }
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != loanbrief.HubEmployeeId)
                            {
                                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                                return Ok(def);
                            }
                            if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                                && !_unitOfWork.UserShopRepository.Any(x => x.UserId == userInfo.UserId && x.ShopId == loanbrief.HubId))
                            {
                                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                                return Ok(def);
                            }

                            if (loanbrief.EvaluationHomeUserId > 0 && loanbrief.EvaluationCompanyUserId > 0)
                            {
                                data.AppraisalId = LOS.Common.Extensions.EnumAppraisal.AppraisalHomeAndCompany.GetHashCode();
                                data.HubEmployeeId = loanbrief.EvaluationHomeUserId.Value;
                            }
                            else if (loanbrief.EvaluationHomeUserId > 0)
                            {
                                data.AppraisalId = LOS.Common.Extensions.EnumAppraisal.AppraisalHome.GetHashCode();
                                data.HubEmployeeId = loanbrief.EvaluationHomeUserId.Value;
                            }
                            else if (loanbrief.EvaluationCompanyUserId > 0)
                            {
                                data.AppraisalId = LOS.Common.Extensions.EnumAppraisal.AppraisalCompany.GetHashCode();
                                data.HubEmployeeId = loanbrief.EvaluationCompanyUserId.Value;
                            }
                            else
                            {
                                data.AppraisalId = LOS.Common.Extensions.EnumAppraisal.NoAppraisalHomeAndCompany.GetHashCode();
                                data.HubEmployeeId = loanbrief.HubEmployeeId.Value;
                            }

                            if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                            {
                                var listUserOfHub = await _unitOfWork.UserRepository.Query(x => x.Status == 1 && x.GroupId == (int)EnumGroupUser.StaffHub
                                                        && x.UserShop.Any(k => k.ShopId == loanbrief.HubId), null, false).Select(x => new UserOfHub()
                                                        {
                                                            UserId = x.UserId,
                                                            UserName = x.Username
                                                        }).ToListAsync();
                                if (listUserOfHub != null && listUserOfHub.Count > 0)
                                    data.ListUser = listUserOfHub;
                            }
                            else
                            {
                                var user = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId, null, false).Select(x => new UserOfHub()
                                {
                                    UserId = x.UserId,
                                    UserName = x.Username
                                }).FirstOrDefaultAsync();
                                if (user != null && user.UserId > 0)
                                    data.ListUser = new List<UserOfHub>() { user };
                            }
                            var listAppraisal = ExtentionHelper.GetEnumToList(typeof(EnumAppraisal));
                            if (listAppraisal != null && listAppraisal.Count > 0)
                                data.ListAppraisal = listAppraisal.Select(x => new AppraisalOfHub() { Id = x.Value.AsInt(), Name = x.Text }).ToList();

                            def.data = data;
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/ChangeHubEmployee Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("list_status_detail")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DAL.EntityFramework.LoanStatusDetail>>>> GetStatusDetail([FromQuery] int loanBriefId)
        {
            var def = new DefaultResponse<Meta, List<DAL.EntityFramework.LoanStatusDetail>>();
            var result = new List<DAL.EntityFramework.LoanStatusDetail>();
            try
            {
                if (loanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanbrief = _unitOfWork.LoanBriefRepository.GetById(loanBriefId);
                if (loanbrief != null)
                {
                    var lstStatusDetail = await _unitOfWork.LoanStatusDetailRepository.Query(x => x.Type == (int)EnumLoanStatusDetail.CVKD && x.IsEnable == true && x.ParentId.GetValueOrDefault() == 0, null, false).ToListAsync();
                    var nextStep = "";
                    var lstConfigStep = await _unitOfWork.LoanConfigStepRepository.Query(x => x.IsEnable == true, null, false).ToListAsync();
                    if (lstConfigStep != null && lstConfigStep.Count > 0)
                    {
                        //check LoanStatusDetail nếu không có value set mặc định là Step1
                        if (loanbrief.LoanStatusDetail.GetValueOrDefault(0) == 0)
                            nextStep = "2";
                        else
                            nextStep = _unitOfWork.LoanConfigStepRepository.Query(x => x.LoanStatusDetailId == loanbrief.LoanStatusDetail.Value && x.IsEnable == true, null, false).FirstOrDefault().NextStep;

                        List<int> ListStep = null;
                        if (!string.IsNullOrEmpty(nextStep))
                        {
                            ListStep = nextStep.Split(",").Select<string, int>(int.Parse).ToList();
                        }
                        lstConfigStep = lstConfigStep.Where(x => ListStep.Contains(x.CurrentStep.Value)).ToList();

                        if (lstConfigStep != null && lstConfigStep.Count > 0)
                        {
                            foreach (var item in lstConfigStep)
                            {
                                var lstStatusDetail2 = lstStatusDetail.Where(x => x.Id == item.LoanStatusDetailId).ToList();
                                result.AddRange(lstStatusDetail2);
                            }

                        }
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = result;
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/GetStatusDetail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("list_status_detail_child")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DAL.EntityFramework.LoanStatusDetail>>>> GetStatusDetailChild([FromQuery] int parentId)
        {
            var def = new DefaultResponse<Meta, List<DAL.EntityFramework.LoanStatusDetail>>();
            try
            {
                if (parentId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var data = await _unitOfWork.LoanStatusDetailRepository.Query(x => x.IsEnable == true && x.ParentId == parentId, null, false).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/GetStatusDetailChild Exception");
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("change_hub_employee")]
        public async Task<ActionResult<DefaultResponse<Meta>>> ChangeHubEmployee(ChangeHubEmployeeRequest entity)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                                        .Select(x => new
                                        {
                                            UserId = x.UserId,
                                            FullName = x.FullName,
                                            UserName = x.Username
                                        }).FirstOrDefaultAsync();
                    if (entity == null || entity.LoanbriefId <= 0 || entity.AppraisalId <= 0 || entity.HubEmployeeId <= 0 ||
                        (entity.AppraisalId > 0 && !Enum.IsDefined(typeof(EnumAppraisal), entity.AppraisalId)))
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                        return Ok(def);
                    }

                    var data = new ChangeHubEmployeeResponse();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var userStaff = await _unitOfWork.UserRepository.Query(x => x.UserId == entity.HubEmployeeId && x.Status == 1
                                          && x.GroupId == (int)EnumGroupUser.StaffHub, null, false).Select(x => new
                                          {
                                              UserId = x.UserId,
                                              FullName = x.FullName,
                                              UserName = x.Username
                                          }).FirstOrDefaultAsync();

                        if (userStaff == null || userStaff.UserId == 0)
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tìm thấy thông tin cvkd");
                            return Ok(def);
                        }
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            EvaluationHomeUserId = x.EvaluationHomeUserId,
                            EvaluationCompanyUserId = x.EvaluationCompanyUserId,
                            Status = x.Status,
                            HubId = x.HubId,
                            HubEmployeeId = x.HubEmployeeId
                        }).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != userInfo.UserId)
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            else if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                                 && !_unitOfWork.UserShopRepository.Any(x => x.ShopId == loanbrief.HubId && x.UserId == userInfo.UserId))
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }

                            if (loanbrief.Status != EnumLoanStatus.APPRAISER_REVIEW.GetHashCode())
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không ở trạng thái đang thẩm định");
                                return Ok(def);
                            }
                            var sNote = string.Empty;
                            //lấy trạng thái thẩm định cũ
                            var oldPlaceOfAppraisal = LOS.Common.Extensions.EnumAppraisal.NoAppraisalHomeAndCompany.GetHashCode();
                            if (loanbrief.EvaluationHomeUserId > 0 && loanbrief.EvaluationCompanyUserId > 0)
                                oldPlaceOfAppraisal = LOS.Common.Extensions.EnumAppraisal.AppraisalHomeAndCompany.GetHashCode();
                            else if (loanbrief.EvaluationHomeUserId > 0)
                                oldPlaceOfAppraisal = LOS.Common.Extensions.EnumAppraisal.AppraisalHome.GetHashCode();
                            else if (loanbrief.EvaluationCompanyUserId > 0)
                                oldPlaceOfAppraisal = LOS.Common.Extensions.EnumAppraisal.AppraisalCompany.GetHashCode();

                            var changeStaff = false;
                            var actionComment = EnumActionComment.CommentLoanBrief.GetHashCode();
                            if (userInfo.GroupId != EnumGroupUser.ManagerHub.GetHashCode() && loanbrief.HubEmployeeId != userStaff.UserId)
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Bạn không có quyền chuyển chuyên viên kinh doanh");
                                return Ok(def);
                            }
                            if (loanbrief.HubEmployeeId != userStaff.UserId)
                            {
                                changeStaff = true;
                                actionComment = EnumActionComment.HubEmployeeChange.GetHashCode();
                                if (userInfo.GroupId != EnumGroupUser.ManagerHub.GetHashCode())
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Bạn không có quyền chuyển chuyên viên kinh doanh");
                                    return Ok(def);
                                }
                            }
                            if (!changeStaff && oldPlaceOfAppraisal == entity.AppraisalId)
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng thay đổi hình thức thẩm định hoặc thay đổi người thẩm định");
                                return Ok(def);
                            }

                            if (entity.AppraisalId == EnumAppraisal.AppraisalHome.GetHashCode())
                            {
                                if (!changeStaff && userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode()
                                           && (oldPlaceOfAppraisal == EnumAppraisal.AppraisalCompany.GetHashCode()
                                           || oldPlaceOfAppraisal == EnumAppraisal.AppraisalHomeAndCompany.GetHashCode()))
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, $"Bạn không có quyền chuyển từ {ExtentionHelper.GetDescription((EnumAppraisal)oldPlaceOfAppraisal)} sang {ExtentionHelper.GetDescription(EnumAppraisal.AppraisalHome)}");
                                    return Ok(def);
                                }
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanbriefId, x => new LoanBrief()
                                {
                                    EvaluationHomeUserId = userStaff.UserId,
                                    EvaluationHomeState = EnumFieldSurveyState.NotAppraised.GetHashCode(),
                                    EvaluationCompanyUserId = null,
                                    EvaluationCompanyState = null,
                                    HubEmployeeId = userStaff.UserId
                                });
                                sNote = string.Format("{0} {1} phải đi thẩm định nhà", changeStaff ? "[APP-Chuyển CVKD]" : "[APP-Cập nhật]", userStaff.FullName);

                            }
                            else if (entity.AppraisalId == EnumAppraisal.AppraisalCompany.GetHashCode())
                            {
                                if (!changeStaff && userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode()
                                            && (oldPlaceOfAppraisal == EnumAppraisal.AppraisalHome.GetHashCode()
                                            || oldPlaceOfAppraisal == EnumAppraisal.AppraisalHomeAndCompany.GetHashCode()))
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, $"Bạn không có quyền chuyển từ {ExtentionHelper.GetDescription((EnumAppraisal)oldPlaceOfAppraisal)} sang {ExtentionHelper.GetDescription(EnumAppraisal.AppraisalCompany)}");
                                    return Ok(def);
                                }
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanbriefId, x => new LoanBrief()
                                {
                                    EvaluationHomeUserId = null,
                                    EvaluationHomeState = null,
                                    EvaluationCompanyUserId = userStaff.UserId,
                                    EvaluationCompanyState = EnumFieldSurveyState.NotAppraised.GetHashCode(),
                                    HubEmployeeId = userStaff.UserId
                                });
                                sNote = string.Format("{0} {1} phải đi thẩm định công ty", changeStaff ? "[APP-Chuyển CVKD]" : "[APP-Cập nhật]", userStaff.FullName);
                            }
                            else if (entity.AppraisalId == EnumAppraisal.AppraisalHomeAndCompany.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanbriefId, x => new LoanBrief()
                                {
                                    EvaluationHomeUserId = userStaff.UserId,
                                    EvaluationHomeState = EnumFieldSurveyState.NotAppraised.GetHashCode(),
                                    EvaluationCompanyUserId = userStaff.UserId,
                                    EvaluationCompanyState = EnumFieldSurveyState.NotAppraised.GetHashCode(),
                                    HubEmployeeId = userStaff.UserId
                                });
                                sNote = string.Format("{0} {1} phải đi thẩm định nhà và công ty", changeStaff ? "[APP-Chuyển CVKD]" : "[APP-Cập nhật]", userStaff.FullName);
                            }
                            else
                            {
                                if (!changeStaff && userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode()
                                           && (oldPlaceOfAppraisal == EnumAppraisal.AppraisalHome.GetHashCode()
                                           || oldPlaceOfAppraisal == EnumAppraisal.AppraisalCompany.GetHashCode()
                                           || oldPlaceOfAppraisal == EnumAppraisal.AppraisalHomeAndCompany.GetHashCode()))
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, $"Bạn không có quyền chuyển từ {ExtentionHelper.GetDescription((EnumAppraisal)oldPlaceOfAppraisal)} sang {ExtentionHelper.GetDescription(EnumAppraisal.NoAppraisalHomeAndCompany)}");
                                    return Ok(def);
                                }
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanbriefId, x => new LoanBrief()
                                {
                                    EvaluationHomeUserId = null,
                                    EvaluationHomeState = null,
                                    EvaluationCompanyUserId = null,
                                    EvaluationCompanyState = null,
                                    HubEmployeeId = userStaff.UserId
                                });
                                sNote = string.Format("{0} {1} phải thẩm định xe. Không phải đi thẩm định nhà và công ty", changeStaff ? "[APP-Chuyển CVKD]" : "[APP-Cập nhật]", userStaff.FullName);
                            }

                            if (changeStaff)
                            {
                                await _unitOfWork.LogDistributionUserRepository.InsertAsync(new LogDistributionUser()
                                {
                                    LoanbriefId = loanbrief.LoanbriefId,
                                    UserId = userStaff.UserId,
                                    HubId = loanbrief.HubId,
                                    TypeDistribution = (int)TypeDistributionLog.CVKD,
                                    CreatedBy = userInfo.UserId,
                                    CreatedAt = DateTime.Now
                                });
                            }

                            if (!string.IsNullOrEmpty(sNote))
                            {
                                var shopName = _unitOfWork.ShopRepository.GetById(loanbrief.HubId)?.Name;

                                await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                {
                                    LoanBriefId = loanbrief.LoanbriefId,
                                    Note = sNote,
                                    FullName = userAction.FullName,
                                    Status = 1,
                                    ActionComment = actionComment,
                                    CreatedTime = DateTime.Now,
                                    UserId = userAction.UserId,
                                    ShopId = loanbrief.HubId,
                                    ShopName = shopName
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/ChangeHubEmployee POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("save_next_step")]
        public async Task<ActionResult<DefaultResponse<object>>> SaveNextStep([FromBody] DAL.Object.LoanStatusDetail.NextStepLoanHub entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.LoanBriefId <= 0 || entity.StatusDetail <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.GetByIdAsync(entity.LoanBriefId);
                        if (loanbrief != null && loanbrief.LoanBriefId > 0)
                        {
                            var checkPermission = CheckPermission(userInfo.GroupId, loanbrief.HubId.Value, loanbrief.HubEmployeeId.Value, loanbrief.Status.Value, userInfo.UserId);
                            if (checkPermission.errorCode != ResponseHelper.SUCCESS_CODE)
                            {
                                def.meta = checkPermission;
                                return Ok(def);
                            }
                            //save status detail
                            _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                            {
                                LoanStatusDetail = entity.StatusDetail,
                                LoanStatusDetailChild = entity.StatusDetailChild
                            });
                            _unitOfWork.Save();

                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                            return Ok(def);
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/SaveNextStep Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #region Exceptions
        [HttpGet]
        [Route("get_list_exception")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DocumentException>>>> GetListException(int parentId)
        {
            var def = new DefaultResponse<Meta, List<DocumentException>>();
            try
            {
                var data = await _unitOfWork.DocumentExceptionRepository.Query(x => x.IsEnable == true
                && x.ParentId == parentId, null, false).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/GetListException Exception");
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_propose_exceptions")]
        public ActionResult<DefaultResponse<object>> AddProposeExceptions([FromBody] ProposeExceptionReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.LoanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var userAction = _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                                        .Select(x => new
                                        {
                                            UserId = x.UserId,
                                            FullName = x.FullName,
                                            UserName = x.Username
                                        }).FirstOrDefault();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = _unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                        if (loanbrief != null)
                        {
                            var checkPermission = CheckPermission(userInfo.GroupId, loanbrief.HubId.Value, loanbrief.HubEmployeeId.Value, loanbrief.Status.Value, userInfo.UserId);
                            if (checkPermission.errorCode != ResponseHelper.SUCCESS_CODE)
                            {
                                def.meta = checkPermission;
                                return Ok(def);
                            }
                            //Update Loan
                            if (entity.Money > 0)
                                loanbrief.LoanAmount = entity.Money;
                            if (loanbrief.HubPushAt != null)
                                loanbrief.ApproverStatusDetail = (int)EnumLoanStatusDetailApprover.BoSungGiayTo;
                            loanbrief.HubPushAt = DateTime.Now;
                            loanbrief.IsExceptions = 1;
                            loanbrief.ActionState = EnumActionPush.Push.GetHashCode();
                            loanbrief.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                            loanbrief.InProcess = EnumInProcess.Process.GetHashCode();
                            loanbrief.ExceptionsId = entity.DocId;

                            _unitOfWork.LoanBriefRepository.Update(loanbrief);

                            //Insert ProposeExceptions
                            _unitOfWork.ProposeExceptionsRepository.Insert(new ProposeExceptions()
                            {
                                LoanBriefId = entity.LoanBriefId,
                                UserId = entity.UserId,
                                DocId = entity.DocId,
                                DocName = entity.DocName,
                                ChildDocId = entity.ChildDocId,
                                ChildDocName = entity.ChildDocName,
                                Money = entity.Money,
                                Note = entity.Note,
                                CreateAt = DateTime.Now
                            });

                            //Insert Note
                            var comment = $"<b>Trình phê duyệt ngoại lệ </b> hợp đồng HĐ-{entity.LoanBriefId} <br />Lý do: {entity.DocName} ";
                            if (!string.IsNullOrEmpty(entity.ChildDocName))
                                comment += $"<br />Lý do chi tiết: {entity.ChildDocName}";
                            if (entity.Money > 0)
                                comment = $"<b>Trình phê duyệt ngoại lệ </b> hợp đồng HĐ-{entity.LoanBriefId} với số tiền là: {entity.Money.ToString("n0")}";

                            if (!string.IsNullOrEmpty(entity.Note))
                                comment += $"<br /> Nội dung: {entity.Note}";
                            var shopName = _unitOfWork.ShopRepository.GetById(loanbrief.HubId)?.Name;
                            _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = comment,
                                FullName = userAction.FullName,
                                Status = 1,
                                ActionComment = (int)EnumActionComment.ProposeExceptions,
                                CreatedTime = DateTime.Now,
                                UserId = userAction.UserId,
                                ShopId = loanbrief.HubId,
                                ShopName = shopName
                            });

                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            def.data = entity.LoanBriefId;
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                            return Ok(def);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/AddProposeExceptions Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        [HttpPost]
        [Route("ekyc")]
        public async Task<ActionResult<DefaultResponse<object>>> CheckEkyc([FromBody] EkycRequest entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity == null || entity.LoanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var data = new List<StatusFilterHub>();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            FullName = x.FullName,
                            Dob = x.Dob,
                            NationalCard = x.NationalCard,
                            HubEmployeeId = x.HubEmployeeId,
                            HubId = x.HubId
                        }).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != userInfo.UserId)
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            else if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                                 && !_unitOfWork.UserShopRepository.Any(x => x.ShopId == loanbrief.HubId && x.UserId == userInfo.UserId))
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }


                            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
                            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];

                            var listFiles = await _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanbrief.LoanbriefId && x.Status == 1
                                        && x.TypeId == (int)EnumDocumentType.CMND_CCCD, null, false).Select(x => new
                                        {
                                            Id = x.Id,
                                            FilePath = x.FilePath,
                                            MecashId = x.MecashId,
                                            S3Status = x.S3status
                                        }).ToListAsync();

                            if (listFiles == null || listFiles.Count < 2)
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Chưa đủ thông tin CMND/CCCD hai mặt");
                                return Ok(def);
                            }
                            var frontId = string.Empty;
                            var backId = string.Empty;
                            var index = 0;
                            foreach (var item in listFiles)
                            {
                                var filePath = string.Empty;
                                if (!item.FilePath.Contains("http") && item.MecashId.GetValueOrDefault(0) == 0)
                                    filePath = ServiceURL + item.FilePath;
                                else if (item.MecashId > 0 && (item.S3Status == 0 || item.S3Status == null) && !item.FilePath.Contains("http"))
                                    filePath = ServiceURLAG + item.FilePath;
                                else if (item.MecashId > 0 && item.S3Status == 1 && !item.FilePath.Contains("http"))
                                    filePath = ServiceURLFileTima + item.FilePath;
                                if (index == 0)
                                    frontId = filePath;
                                else
                                    backId = filePath;
                                index++;
                                if (index > 1)
                                    continue;
                            }

                            var requestEkyc = new EkycNationalReq
                            {
                                FrontId = frontId,
                                BackId = backId,
                                Selfie = "",
                                Fullname = loanbrief.FullName,
                                Birthday = loanbrief.Dob != null ? loanbrief.Dob.Value.ToString("dd-MM-yyyy") : "",
                                NationalId = loanbrief.NationalCard
                            };

                            var lastRequestEkyc = await _unitOfWork.LogLoanInfoAiRepository.Query(x => x.LoanbriefId == loanbrief.LoanbriefId
                                                && x.Status == 1 && x.ServiceType == (int)ServiceTypeAI.Ekyc, null, false).OrderByDescending(s => s.Id).Select(x => new
                                                {
                                                    Id = x.Id,
                                                    Request = x.Request
                                                }).FirstOrDefaultAsync();

                            if (lastRequestEkyc != null)
                            {
                                if (String.Compare(lastRequestEkyc.Request.ToString(), JsonConvert.SerializeObject(requestEkyc).ToString(), true) == 0)
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Thông tin trên đã được check Ekyc!");
                                    return Ok(def);
                                }
                            }
                            var result = _aiService.GetInfoNationalEkyc(requestEkyc, loanbrief.LoanbriefId);
                            if (result.StatusCode == 200)
                            {
                                var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                                      .Select(x => new
                                      {
                                          UserId = x.UserId,
                                          FullName = x.FullName,
                                          UserName = x.Username
                                      }).FirstOrDefaultAsync();
                                await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                {
                                    LoanBriefId = loanbrief.LoanbriefId,
                                    Note = string.Format("[APP] {0} lấy thông tin cmnd/cccd qua ekyc", userAction.FullName),
                                    FullName = userAction.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = userAction.UserId,
                                    ShopId = loanbrief.HubId,
                                    ShopName = "Tima"
                                });
                                _unitOfWork.Save();
                                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            }
                            else
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, result.Message);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }
            }
            catch (Exception ex)
            {

                Log.Error(ex, "DirectSale/CheckEkyc Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("re_location")]
        public async Task<ActionResult<DefaultResponse<object>>> ReGetLocation([FromBody] ReGetLocationRequest entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity == null || entity.LoanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var data = new List<StatusFilterHub>();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            HubEmployeeId = x.HubEmployeeId,
                            HubId = x.HubId
                        }).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != userInfo.UserId)
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            else if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                                 && !_unitOfWork.UserShopRepository.Any(x => x.ShopId == loanbrief.HubId && x.UserId == userInfo.UserId))
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            await _unitOfWork.LogLoanInfoAiRepository.InsertAsync(new LogLoanInfoAi()
                            {
                                LoanbriefId = loanbrief.LoanbriefId,
                                ServiceType = ServiceTypeAI.Location.GetHashCode(),
                                IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest,
                                CreatedAt = DateTime.Now
                            });
                            await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == loanbrief.LoanbriefId, x => new LoanBrief()
                            {
                                ResultLocation = string.Empty
                            });
                            var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                                                           .Select(x => new

                                                           {
                                                               UserId = x.UserId,
                                                               FullName = x.FullName,
                                                               UserName = x.Username
                                                           }).FirstOrDefaultAsync();
                            await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote

                            {
                                LoanBriefId = loanbrief.LoanbriefId,
                                Note = string.Format("[APP] {0} gửi yêu cầu lấy thông tin location Sim.", userAction.FullName),
                                FullName = userAction.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.ReGetLocation.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = userAction.UserId,
                                ShopId = loanbrief.HubId,
                                ShopName = "Tima"
                            });
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }
            }
            catch (Exception ex)
            {

                Log.Error(ex, "DirectSale/ReGetLocation Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("re_esign")]
        public async Task<ActionResult<DefaultResponse<object>>> ReEsign([FromBody] ReEsignRequest entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity == null || entity.LoanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var data = new List<StatusFilterHub>();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            HubEmployeeId = x.HubEmployeeId,
                            Status = x.Status,
                            HubId = x.HubId,
                            EsignState = x.EsignState,
                        }).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            if (loanbrief.Status != (int)EnumLoanStatus.HUB_CHT_APPROVE && loanbrief.Status != (int)EnumLoanStatus.APPRAISER_REVIEW)
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không còn ở hub xử lý");
                                return Ok(def);
                            }
                            else if (loanbrief.EsignState != (int)EsignState.BORROWER_SIGNED && loanbrief.EsignState != (int)EsignState.LENDER_SIGNED)
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "KH chưa ký hợp đồng điện tử");
                                return Ok(def);

                            }
                            if (_unitOfWork.EsignContractRepository.Count(x => x.LoanBriefId == loanbrief.LoanbriefId && x.TypeEsign == (int)TypeEsign.Customer) >= 2)
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đã ký quá số lần quy định");
                                return Ok(def);
                            }
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != userInfo.UserId)
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            else if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                                 && !_unitOfWork.UserShopRepository.Any(x => x.ShopId == loanbrief.HubId && x.UserId == userInfo.UserId))
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == loanbrief.LoanbriefId, x => new LoanBrief()
                            {
                                EsignState = 0,
                                EsignBorrowerContract = null,
                                EsignLenderContract = null,
                                EsignBillCode = null
                            });
                            var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                              .Select(x => new
                              {
                                  UserId = x.UserId,
                                  FullName = x.FullName,
                                  UserName = x.Username
                              }).FirstOrDefaultAsync();
                            await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                            {
                                LoanBriefId = loanbrief.LoanbriefId,
                                Note = "[APP] Hợp đồng đã được mở cho Khách hàng ký lại hợp đồng điện tử",
                                FullName = userAction.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = userAction.UserId,
                                ShopId = loanbrief.HubId,
                                ShopName = "Tima"
                            });
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }
            }
            catch (Exception ex)
            {

                Log.Error(ex, "DirectSale/ReEsign Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("cancel_loanbrief")]
        public async Task<ActionResult<DefaultResponse<object>>> CancelLoanBrief([FromBody] CancelLoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity == null || entity.LoanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        //kiểm tra đơn vay xem có đang xử lý hay đã bị hủy rồi không
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false)
                            .Select(LoanBriefBasicInfo.ProjectionViewDetail).FirstOrDefaultAsync();
                        if (loanbrief == null)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy đơn vay");
                            return Ok(def);
                        }
                        if (loanbrief.Status == (int)EnumLoanStatus.CANCELED)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đã được hủy trước đó");
                            return Ok(def);
                        }
                        else if (loanbrief.InProcess == (int)EnumInProcess.Process)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn đang được xử lý. Vui lòng thử lại sau");
                            return Ok(def);
                        }
                        //kiểm tra thiết bị
                        if (!string.IsNullOrEmpty(loanbrief.DeviceId))
                        {
                            var checkDevice = _erpService.CheckStatusDevice(loanbrief.DeviceId);
                            if (checkDevice != null && checkDevice.serial == loanbrief.DeviceId && checkDevice.status == "Active")
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Phải gỡ thiết bị mới được hủy đơn");
                                return Ok(def);
                            }
                        }

                        var noteCancelLoan = string.Format("<b>Lý do chi tiết: </b> {0} <br /><b>Comment:</b> {1}",
                            await _unitOfWork.ReasonCancelRepository.Query(x => x.Id == entity.ReasonDetailId, null, false)
                            .Select(x => x.Reason).FirstOrDefaultAsync(), entity.CommentCancel);

                        var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                            .Select(x => new
                            {
                                UserId = x.UserId,
                                FullName = x.FullName,
                                UserName = x.Username,
                                ShopId = x.UserShop != null ? x.UserShop.First().Shop.ShopId : EnumShop.Tima.GetHashCode(),
                                ShopName = x.UserShop != null ? x.UserShop.First().Shop.Name : "Tima",
                            }).FirstOrDefaultAsync();

                        //kiểm tra xem lý do hủy có bị thêm vào blacklist không
                        var reason = await _unitOfWork.ReasonCancelRepository.Query(x => x.Id == entity.ReasonDetailId, null, false)
                            .Select(ReasonCancelDetail.ProjectionDetail).FirstOrDefaultAsync();
                        if (reason != null && reason.DayInBlacklist.HasValue && reason.DayInBlacklist > 0)
                        {
                            //gọi api insert black list sang lms
                            _lmsService.AddBlackList(new RequestAddBlacklist()
                            {
                                CustomerCreditId = loanbrief.CustomerId ?? 0,
                                FullName = loanbrief.FullName,
                                NumberPhone = loanbrief.Phone,
                                CardNumber = loanbrief.NationalCard,
                                BirthDay = loanbrief.Dob ?? null,
                                UserIdCreate = userInfo.UserId,
                                UserNameCreate = userAction.UserName,
                                FullNameCreate = userAction.FullName,
                                Note = noteCancelLoan,
                                NumberDay = reason.DayInBlacklist.Value,
                                LoanCreditId = loanbrief.LoanBriefId
                            });
                        }
                        //cập nhập bảng loanbrief
                        _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                        {
                            ReasonCancel = entity.ReasonDetailId,
                            LoanBriefComment = string.Format("Hub {0} hủy đơn: {1}", userAction.FullName, noteCancelLoan),
                            ActionState = EnumActionPush.Cancel.GetHashCode(),
                            PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode(),
                            InProcess = EnumInProcess.Process.GetHashCode(),
                            LoanBriefCancelAt = DateTime.Now,
                            LoanBriefCancelBy = userInfo.UserId,
                        });
                        //thêm note
                        _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = noteCancelLoan,
                            FullName = userAction.FullName,
                            Status = 1,
                            ActionComment = (int)EnumActionComment.HubCancel,
                            CreatedTime = DateTime.Now,
                            UserId = userInfo.UserId,
                            ShopId = userAction.ShopId,
                            ShopName = userAction.ShopName
                        });
                        //log action
                        _unitOfWork.LogLoanActionRepository.Insert(new LogLoanAction()
                        {
                            LoanbriefId = loanbrief.LoanBriefId,
                            ActionId = (int)EnumLogLoanAction.LoanCancel,
                            TypeAction = (int)EnumTypeAction.Manual,
                            LoanStatus = loanbrief.Status,
                            TlsLoanStatusDetail = loanbrief.DetailStatusTelesales,
                            HubLoanStatusDetail = loanbrief.LoanStatusDetailChild,
                            TelesaleId = loanbrief.BoundTelesaleId,
                            HubId = loanbrief.HubId,
                            HubEmployeeId = loanbrief.HubEmployeeId,
                            CoordinatorUserId = loanbrief.CoordinatorUserId,
                            UserActionId = userInfo.UserId,
                            GroupUserActionId = userInfo.GroupId,
                            CreatedAt = DateTime.Now
                        });
                        await _unitOfWork.SaveAsync();
                        //Gọi sang bên AI hủy đơn định vị
                        if (loanbrief.IsLocate == true)
                        {
                            if (_trackDeviceService.CloseContract(string.Format("HD-{0}", loanbrief.LoanBriefId), loanbrief.DeviceId, loanbrief.LoanBriefId))
                            {
                                loanbrief.Status = StatusOfDeviceID.Close.GetHashCode();
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                                {
                                    DeviceStatus = StatusOfDeviceID.Close.GetHashCode(),
                                });
                                //thêm note
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    LoanBriefId = entity.LoanBriefId,
                                    Note = string.Format("Đóng hợp đồng HD-{0} bên ginno", loanbrief.LoanBriefId),
                                    FullName = userAction.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.CloseContractGinno.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = userInfo.UserId,
                                    ShopId = userAction.ShopId,
                                    ShopName = userAction.ShopName
                                });

                                //đóng hợp đồng bên ERP
                                if (_erpService.UpdateDeviceStatus(new PushDeviceReq()
                                {
                                    loanId = loanbrief.LoanBriefId,
                                    serial = loanbrief.DeviceId,
                                    status = (int)StatusOfDeviceID.Close
                                }))
                                {
                                    //thêm note
                                    _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                    {
                                        LoanBriefId = loanbrief.LoanBriefId,
                                        Note = string.Format("Gọi đóng hoạt thiết bị sang ERP"),
                                        FullName = userAction.FullName,
                                        Status = 1,
                                        ActionComment = EnumActionComment.CloseContractERP.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = userInfo.UserId,
                                        ShopId = userAction.ShopId,
                                        ShopName = userAction.ShopName
                                    });
                                }

                                await _unitOfWork.SaveAsync();
                            }
                        }
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                    else
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                }
                else
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/CancelLoanBrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        public Meta CheckPermission(int GroupId, int HubId, int HubEmployeeId, int Status, int UserId)
        {
            var result = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

            if (GroupId == EnumGroupUser.StaffHub.GetHashCode() && HubEmployeeId != UserId)
            {
                result = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                return result;
            }
            else if (GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                 && !_unitOfWork.UserShopRepository.Any(x => x.ShopId == HubId && x.UserId == UserId))
            {
                result = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                return result;
            }
            if (Status != (int)EnumLoanStatus.APPRAISER_REVIEW)
            {
                result = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION IN THIS STATUS");
                return result;
            }
            return result;
        }

        [HttpPost]
        [Route("create_loan_car_child")]
        public async Task<ActionResult<DefaultResponse<Meta, object>>> CreateLoanCarChild(CreateLoanCarChild entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                if (entity.LoanAmount > 0 && entity.LoanBriefId > 0)
                {
                    var userInfo = GetUserInfo();
                    if (userInfo != null && userInfo.UserId > 0)
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false).FirstOrDefaultAsync();
                        if (loanbrief != null && loanbrief.LoanBriefId > 0)
                        {
                            //Kiểm tra user có quyền truy cập vào đơn gốc hay không? cvkd xử lý đơn đó hoặc là tpgd quản lý đơn đó 
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != userInfo.UserId)
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            else if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                                 && !_unitOfWork.UserShopRepository.Any(x => x.ShopId == loanbrief.HubId && x.UserId == userInfo.UserId))
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            //kiểm tra xem có ở HUB không
                            if (loanbrief.Status != (int)EnumLoanStatus.HUB_CHT_APPROVE && loanbrief.Status != (int)EnumLoanStatus.APPRAISER_REVIEW
                                && loanbrief.Status != (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING)
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Đơn không nằm ở HUB không thể tạo đơn con");
                                return Ok(def);
                            }

                            // update lại ReMarketingLoanBriefId
                            await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                            {
                                ReMarketingLoanBriefId = entity.LoanBriefId,
                            });
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Không tìm thấy thông tin đơn vay");
                            return Ok(def);
                        }

                        #region Loanbrief
                        loanbrief.LoanBriefId = 0;
                        loanbrief.CreatedTime = DateTime.Now;
                        loanbrief.LoanAmount = entity.LoanAmount;
                        loanbrief.LoanAmountFirst = entity.LoanAmount;
                        loanbrief.UtmCampaign = null;
                        loanbrief.UtmContent = null;
                        loanbrief.UtmMedium = null;
                        loanbrief.UtmSource = null;
                        loanbrief.UtmTerm = null;
                        loanbrief.Tid = null;
                        loanbrief.PlatformType = (int)EnumPlatformType.LoanCarChild;
                        loanbrief.ReMarketingLoanBriefId = entity.LoanBriefId;
                        loanbrief.CreateBy = null;
                        loanbrief.BoundTelesaleId = null;
                        var loanbriefNew = await _unitOfWork.LoanBriefRepository.InsertAsync(loanbrief);
                        _unitOfWork.Save();
                        #endregion

                        var loanbriefParent = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false)
                            .Select(LoanBriefRelationshipAll.ProjectionDetail).FirstOrDefaultAsync();

                        #region LoanBriefResident
                        if (loanbriefParent.LoanBriefResident != null)
                        {
                            var newLoanBriefResident = loanbriefParent.LoanBriefResident.DeepCopy();
                            newLoanBriefResident.LoanBriefResidentId = loanbrief.LoanBriefId;
                            await _unitOfWork.LoanBriefResidentRepository.InsertAsync(newLoanBriefResident);
                        }
                        #endregion

                        #region LoanBriefProperty
                        if (loanbriefParent.LoanBriefProperty != null)
                        {
                            var newLoanBriefProperty = loanbriefParent.LoanBriefProperty.DeepCopy();
                            newLoanBriefProperty.LoanBriefPropertyId = loanbrief.LoanBriefId;
                            newLoanBriefProperty.CreatedTime = DateTime.Now;
                            await _unitOfWork.LoanBriefPropertyRepository.InsertAsync(newLoanBriefProperty);
                        }
                        #endregion

                        #region LoanBriefHousehold
                        if (loanbriefParent.LoanBriefHousehold != null)
                        {
                            var newLoanBriefHousehold = loanbriefParent.LoanBriefHousehold.DeepCopy();
                            newLoanBriefHousehold.LoanBriefHouseholdId = loanbrief.LoanBriefId;
                            await _unitOfWork.LoanBriefHouseholdRepository.InsertAsync(newLoanBriefHousehold);
                        }
                        #endregion

                        #region LoanBriefJob
                        if (loanbriefParent.LoanBriefJob != null)
                        {
                            var newLoanBriefJob = loanbriefParent.LoanBriefJob.DeepCopy();
                            newLoanBriefJob.LoanBriefJobId = loanbrief.LoanBriefId;
                            await _unitOfWork.LoanBriefJobRepository.InsertAsync(newLoanBriefJob);
                        }
                        #endregion

                        #region LoanBriefRelationship
                        if (loanbriefParent.LoanBriefRelationship != null && loanbriefParent.LoanBriefRelationship.Count > 0)
                        {
                            var newLoanBriefRelationship = loanbriefParent.LoanBriefRelationship.DeepCopy();
                            foreach (var item in newLoanBriefRelationship)
                            {
                                item.Id = 0;
                                item.LoanBriefId = loanbrief.LoanBriefId;
                                item.CreatedDate = DateTime.Now;
                            }
                            await _unitOfWork.LoanBriefRelationshipRepository.InsertAsync(newLoanBriefRelationship.ToList());
                        }

                        #endregion

                        #region LoanBriefImage
                        //lấy ra danh sách file mà chưa bị xóa
                        var lstImage = await _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == entity.LoanBriefId
                        && x.Status == 1 && x.IsDeleted != 1, null, false).ToListAsync();
                        if (lstImage != null && lstImage.Count > 0)
                        {
                            foreach (var item in lstImage)
                            {
                                item.Id = 0;
                                item.LoanBriefId = loanbrief.LoanBriefId;
                                item.CreateAt = DateTime.Now;
                            }
                            await _unitOfWork.LoanBriefFileRepository.InsertAsync(lstImage);
                        }
                        #endregion

                        //thông tin của user
                        var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                                .Select(x => new
                                {
                                    UserId = x.UserId,
                                    FullName = x.FullName,
                                    UserName = x.Username,
                                }).FirstOrDefaultAsync();

                        //thêm note

                        await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote()
                        {
                            LoanBriefId = loanbriefNew.LoanBriefId,
                            Note = "Hợp đồng được tạo ra từ hợp đồng cha HĐ-" + entity.LoanBriefId,
                            FullName = userAction.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = userAction.UserId,
                            ShopId = loanbrief.HubId,
                            ShopName = "Tima"
                        });

                        //kiểm tra xem phải HĐ định vị không và có mã định vị không
                        if (loanbriefNew.IsLocate == true && !string.IsNullOrEmpty(loanbriefNew.DeviceId))
                        {
                            //gọi api mở hợp đồng
                            var resultOpent = _trackDeviceService.OpenContract("HD-" + loanbriefNew.LoanBriefId, loanbriefNew.DeviceId, loanbrief.HubId.ToString(), loanbriefNew.ProductId.Value, loanbriefNew.LoanBriefId, "topup");
                            //StatusCode == 3 => mã hợp đồng đã tồn tại
                            if (resultOpent != null && (resultOpent.StatusCode == 200 || resultOpent.StatusCode == 3))
                            {
                                if (resultOpent.StatusCode == 200)
                                {
                                    await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote()
                                    {
                                        LoanBriefId = loanbriefNew.LoanBriefId,
                                        Note = string.Format("Đơn định vị: Tạo hợp đồng thành công với imei: {0}", loanbriefNew.DeviceId),
                                        FullName = userAction.FullName,
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = userAction.UserId,
                                        ShopId = loanbrief.HubId,
                                        ShopName = "Tima"
                                    });

                                    // update lại DeviceStatus, ContractGinno
                                    await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == loanbriefNew.LoanBriefId, x => new LoanBrief()
                                    {
                                        DeviceStatus = StatusOfDeviceID.OpenContract.GetHashCode(),
                                        ContractGinno = "HD-" + loanbriefNew.LoanBriefId,
                                    });

                                }
                            }
                        }

                        _unitOfWork.Save();

                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = loanbriefNew.LoanBriefId;
                    }
                    else
                        def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }
                else
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng nhập mã HĐ cần tạo đơn con và số tiền và UserId tạo đơn");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Tool/CreateLoanCarChild Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_insurance_info")]
        public async Task<ActionResult<DefaultResponse<Meta, object>>> GetInsuranceInfo(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    //lấy thông tin cơ bản của đơn vay
                    var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false)
                    .Select(LoanBriefBasicInfo.ProjectionViewDetail).FirstOrDefaultAsync();
                    if (loanbrief != null)
                    {
                        if (loanbrief.Dob == null || loanbrief.Dob == DateTime.MinValue)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay chưa có ngày sinh không thể lấy chứng minh thu nhập");
                            return Ok(def);
                        }


                        var insuInfo = _proxyTimaService.GetInsuranceInfo(loanbrief.NationalCard, loanbrief.NationCardPlace ?? "", loanbrief.FullName, loanbrief.Dob.Value.ToString("dd/MM/yyyy"), loanbriefId);
                        if (insuInfo.Result == 1)
                        {
                            if (insuInfo.Data != null && insuInfo.Data.Result.Count > 0)
                            {
                                if (insuInfo.Data.Result.Count > 6)
                                    insuInfo.Data.Result = insuInfo.Data.Result.Take(6).ToList();
                                var lstInsu = new List<Models.DrawImage.InsuranceInfo>();
                                for (var i = 0; i < insuInfo.Data.Result.Count(); i++)
                                {
                                    var obj = new Models.DrawImage.InsuranceInfo();
                                    obj.Smonth = string.Format("- Tháng {0} : {1}", insuInfo.Data.Result[i].Smonth, insuInfo.Data.Result[i].Scompany);
                                    obj.CompanyAddress = string.Format("+ Địa chỉ : {0}", insuInfo.Data.Result[i].Scompanyaddress);

                                    lstInsu.Add(obj);
                                }
                                var objDraw = new Models.DrawImage.DrawImageReq
                                {
                                    CustomerName = loanbrief.FullName,
                                    NationalCard = loanbrief.NationalCard,
                                    Ssiid = insuInfo.Data.Result.First().Ssiid,
                                    InsuranceInfo = lstInsu,
                                    UrlFileTemp = System.IO.Directory.GetCurrentDirectory() + "\\Images\\draw-image.jpg"
                                };
                                var draw = _drawImageService.DrawImage(objDraw);
                                if (draw != null)
                                {
                                    var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                                    var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                                    var BucketName = _baseConfig["AppSettings:BucketName"];
                                    var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
                                    {
                                        RegionEndpoint = RegionEndpoint.APSoutheast1
                                    });
                                    var folder = BucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
                                    var extension = Path.GetExtension("chung-tu-chung-minh-thu-nhap.jpg");
                                    string img = Guid.NewGuid().ToString();
                                    string ImageName = img + extension;
                                    string ImageThumb = img + "-thumb" + extension;
                                    Stream stream = new MemoryStream(draw);
                                    PutObjectRequest req = new PutObjectRequest()
                                    {
                                        InputStream = stream,
                                        BucketName = folder,
                                        Key = ImageName,
                                        CannedACL = S3CannedACL.PublicRead
                                    };
                                    await client.PutObjectAsync(req);

                                    var link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;

                                    //thông tin của user
                                    var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                                            .Select(x => new
                                            {
                                                UserId = x.UserId,
                                                FullName = x.FullName,
                                                UserName = x.Username,
                                            }).FirstOrDefaultAsync();

                                    //insert to db
                                    await _unitOfWork.LoanBriefFileRepository.InsertAsync(new LoanBriefFiles()
                                    {
                                        LoanBriefId = loanbriefId,
                                        CreateAt = DateTime.Now,
                                        Status = 1,
                                        UserId = userAction.UserId,
                                        FilePath = link,
                                        TypeId = 405,
                                        S3status = 1,
                                        MecashId = 1000000000,
                                        SourceUpload = (int)EnumSourceUpload.AutoSystem
                                    });

                                    //Thêm note
                                    await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                    {
                                        LoanBriefId = loanbrief.LoanBriefId,
                                        Note = string.Format("Hợp đồng {0} đã được {1} lấy chứng từ chứng minh thu nhập", loanbriefId, userAction.FullName),
                                        FullName = userAction.FullName,
                                        Status = 1,
                                        ActionComment = (int)EnumActionComment.CommentLoanBrief,
                                        CreatedTime = DateTime.Now,
                                        UserId = userAction.UserId,
                                        ShopId = loanbrief.HubId,
                                        ShopName = "Tima"
                                    });

                                    _unitOfWork.Save();
                                }
                                else
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Có lỗi xảy ra trong quá trình xử lý ảnh chứng từ. Vui lòng liên hệ kỹ thuật");
                                    return Ok(def);
                                }
                            }
                            else
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin bảo hiểm");
                                return Ok(def);
                            }

                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            def.data = true;
                        }
                        else
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, insuInfo.Message);
                    }
                    else
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                }
                else
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Tool/GetInsuranceInfo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("push_loan")]
        public async Task<ActionResult<DefaultResponse<object>>> PushLoan([FromBody] PushLoanRequest entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity == null || entity.LoanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            FullName = x.FullName,
                            ProductId = x.ProductId,
                            IsLocate = x.IsLocate,
                            DeviceId = x.DeviceId,
                            HubEmployeeId = x.HubEmployeeId,
                            HubId = x.HubId,
                            Status = x.Status,
                            HubPushAt = x.HubPushAt,
                            TypeRemarketing = x.TypeRemarketing,
                            EvaluationCompanyUserId = x.EvaluationCompanyUserId,
                            EvaluationCompanyState = x.EvaluationCompanyState,
                            EvaluationHomeUserId = x.EvaluationHomeUserId,
                            EvaluationHomeState = x.EvaluationHomeState,
                            ApproverStatusDetail = x.ApproverStatusDetail,
                            DetailStatusTelesales = x.DetailStatusTelesales,
                            LoanStatusDetailChild = x.LoanStatusDetailChild,
                            BoundTelesaleId = x.BoundTelesaleId,
                            CoordinatorUserId = x.CoordinatorUserId,
                        }).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            var actionComment = EnumActionComment.CommentLoanBrief.GetHashCode();
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != userInfo.UserId)
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            else if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                                 && !_unitOfWork.UserShopRepository.Any(x => x.ShopId == loanbrief.HubId && x.UserId == userInfo.UserId))
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            if (loanbrief.Status != (int)EnumLoanStatus.APPRAISER_REVIEW && loanbrief.Status != (int)EnumLoanStatus.HUB_CHT_APPROVE
                                && loanbrief.Status != (int)EnumLoanStatus.WAIT_HUB_EMPLOYEE_PREPARE_LOAN)
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Bạn không có quyền đẩy đơn lên");
                                return Ok(def);
                            }
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                            {
                                actionComment = EnumActionComment.StaffHubPush.GetHashCode();
                                if ((loanbrief.EvaluationCompanyUserId > 0 && loanbrief.EvaluationCompanyState != EnumAppraisalState.Appraised.GetHashCode())
                                || (loanbrief.EvaluationHomeUserId > 0 && loanbrief.EvaluationHomeState != EnumAppraisalState.Appraised.GetHashCode()))
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng thẩm định trước khi đẩy lên!");
                                    return Ok(def);
                                }
                            }
                            else
                            {
                                if (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                    || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                    || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                {
                                    //Call qua ProductReviewResultDetail lấy kết quả mới nhất
                                    var productReview = await _unitOfWork.ProductReviewResultDetailRepository.Query(x => x.LoanBriefId == loanbrief.LoanbriefId, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();
                                    if (productReview != null && productReview.PriceReview > 0)
                                    {
                                        if (loanbrief.TypeRemarketing != (int)EnumTypeRemarketing.DebtRevolvingLoan &&
                                            productReview.PriceReview.GetValueOrDefault(0) < 3000000)
                                        {
                                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số tiền sau thẩm định xe < 3tr!");
                                            return Ok(def);
                                        }
                                    }
                                    else
                                    {
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay chưa được thẩm định xe!");
                                        return Ok(def);
                                    }
                                }
                                actionComment = EnumActionComment.HubPush.GetHashCode();
                            }

                            if (loanbrief.IsLocate == true && string.IsNullOrEmpty(loanbrief.DeviceId))
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng nhập thông tin imei của định vị");
                                return Ok(def);
                            }
                            var approverStatusDetail = loanbrief.ApproverStatusDetail;
                            var hubPushAt = loanbrief.HubPushAt;
                            if (!hubPushAt.HasValue)
                                hubPushAt = DateTime.Now;
                            else
                                approverStatusDetail = (int)EnumLoanStatusDetailApprover.BoSungGiayTo;
                            _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanbriefId, x => new LoanBrief()
                            {
                                PipelineState = (int)EnumPipelineState.MANUAL_PROCCESSED,
                                ActionState = (int)EnumActionPush.Push,
                                InProcess = (int)EnumInProcess.Process,
                                HubPushAt = hubPushAt,
                                ApproverStatusDetail = approverStatusDetail,
                            });

                            var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                                                          .Select(x => new

                                                          {
                                                              UserId = x.UserId,
                                                              FullName = x.FullName,
                                                              UserName = x.Username,
                                                              GroupId = x.GroupId
                                                          }).FirstOrDefaultAsync();
                            var shopName = _unitOfWork.ShopRepository.GetById(loanbrief.HubId)?.Name;
                            await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                            {
                                LoanBriefId = loanbrief.LoanbriefId,
                                Note = string.Format("[APP] Hợp đồng HĐ-{0} đã được {1} đẩy đi", loanbrief.LoanbriefId, userAction.FullName),
                                FullName = userAction.FullName,
                                Status = 1,
                                ActionComment = actionComment,
                                CreatedTime = DateTime.Now,
                                UserId = userAction.UserId,
                                ShopId = loanbrief.HubId,
                                ShopName = String.IsNullOrEmpty(shopName) ? "Tima" : shopName
                            });
                            await _unitOfWork.LogLoanActionRepository.InsertAsync(new LogLoanAction
                            {
                                LoanbriefId = loanbrief.LoanbriefId,
                                ActionId = (int)EnumLogLoanAction.LoanPush,
                                TypeAction = (int)EnumTypeAction.Manual,
                                LoanStatus = loanbrief.Status,
                                TlsLoanStatusDetail = loanbrief.DetailStatusTelesales,
                                HubLoanStatusDetail = loanbrief.LoanStatusDetailChild,
                                TelesaleId = loanbrief.BoundTelesaleId,
                                HubId = loanbrief.HubId,
                                HubEmployeeId = loanbrief.HubEmployeeId,
                                CoordinatorUserId = loanbrief.CoordinatorUserId,
                                UserActionId = userAction.UserId,
                                GroupUserActionId = userAction.GroupId,
                                CreatedAt = DateTime.Now
                            });
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }
            }
            catch (Exception ex)
            {

                Log.Error(ex, "DirectSale/PushLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpGet]
        [Route("list_staff")]
        public async Task<ActionResult<DefaultResponse<Meta, List<UserOfHub>>>> GetListStaff(int hubId)
        {
            var def = new DefaultResponse<Meta, List<UserOfHub>>();
            try
            {
                if (hubId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền thông tin hub");
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var data = new ChangeHubEmployeeResponse();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        if (_unitOfWork.UserShopRepository.Count(x => x.UserId == userInfo.UserId && x.ShopId == hubId) == 0)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Tài khoản không thuộc hub");
                            return Ok(def);
                        }
                        var listUserOfHub = await _unitOfWork.UserRepository.Query(x => x.Status == 1 && x.GroupId == (int)EnumGroupUser.StaffHub
                                                      && x.UserShop.Any(k => k.ShopId == hubId), null, false).Select(x => new UserOfHub()
                                                      {
                                                          UserId = x.UserId,
                                                          UserName = x.Username,
                                                          FullName = x.FullName
                                                      }).ToListAsync();
                        def.data = listUserOfHub;
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/GetListStaff Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("loan_detail")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanBriefDetailForApi>>> GetLoanDetail(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, LoanBriefDetailForApi>();
            try
            {
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false)
                        .Select(x => new { HubEmployeeId = x.HubEmployeeId, HubId = x.HubId }).FirstOrDefaultAsync();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != loanbrief.HubEmployeeId)
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        return Ok(def);
                    }
                    if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                        && !_unitOfWork.UserShopRepository.Any(x => x.UserId == userInfo.UserId && x.ShopId == loanbrief.HubId))
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        return Ok(def);
                    }

                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var data = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false)
                            .Select(LoanBriefDetailForApi.ProjectionViewDetail).FirstOrDefaultAsync();

                        if (data != null && data.LoanBriefJob != null && data.LoanBriefJob.JobDescriptionId.HasValue)
                        {
                            data.LoanBriefJob.JobDescriptionName = await _unitOfWork.JobRepository.Query(x => x.JobId == data.LoanBriefJob.JobDescriptionId, null, false)
                                .Select(x => x.Name).FirstOrDefaultAsync();

                            data.LoanBriefJob.ImcomeTypeName = data.LoanBriefJob.ImcomeType.HasValue ? Description.GetDescription((EnumImcomeType)data.LoanBriefJob.ImcomeType) : null;
                        }
                        if (data != null && data.LoanBriefResident != null)
                        {
                            data.LoanBriefResident.LivingTimeName = data.LoanBriefResident.LivingTime.HasValue ? Description.GetDescription((EnumLivingTime)data.LoanBriefResident.LivingTime) : null;
                            data.LoanBriefResident.ResidentTypeName = data.LoanBriefResident.ResidentType.HasValue ? Description.GetDescription((TypeOfOwnerShipApi)data.LoanBriefResident.ResidentType) : null;
                            data.LoanBriefResident.LivingWithName = data.LoanBriefResident.LivingWith.HasValue ? Description.GetDescription((EnumLivingWith)data.LoanBriefResident.LivingWith) : null;
                        }
                        if (data != null && data.LoanBriefId > 0)
                        {
                            data.LoanbriefLender = await _unitOfWork.LoanBriefLenderRepository.Query(x => x.LoanbriefId == data.LoanBriefId, null, false).FirstOrDefaultAsync();
                            data.PlatformTypeName = data.PlatformType.HasValue ? Description.GetDescription((EnumPlatformType)data.PlatformType) : null;
                            data.RateTypeName = data.RateTypeId.HasValue ? Description.GetDescription((EnumRateType)data.RateTypeId) : null;
                            data.TypeLoanBriefName = data.TypeLoanBrief.HasValue ? Description.GetDescription((TypeLoanBrief)data.TypeLoanBrief) : null;
                            data.ProductName = data.ProductId.HasValue ? Description.GetDescription((EnumProductCredit)data.ProductId) : null;
                            data.LoanPurposeName = data.LoanPurpose.HasValue ? Description.GetDescription((EnumLoadLoanPurpose)data.LoanPurpose) : null;
                            data.HomeNetworkName = data.HomeNetwork.HasValue ? Description.GetDescription((HomeNetWorkMobile)data.HomeNetwork) : null;

                            if (data.ProductDetailId.HasValue)
                                data.ProductDetailName = await _unitOfWork.InfomationProductDetailRepository.Query(x => x.Id == data.ProductDetailId.Value, null, false)
                                    .Select(x => x.Name).FirstOrDefaultAsync();

                            data.ResultEkyc = await _unitOfWork.ResultEkycRepository.Query(x => x.LoanbriefId == data.LoanBriefId, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();

                            //lấy danh sách quyền của user
                            var listPermissionAction = await _unitOfWork.UserActionPermissionRepository.Query(x => x.UserId == userInfo.UserId, null, false).Select(x => new
                            {
                                LoanStatus = x.LoanStatus,
                                PermissionValue = x.Value
                            }).ToListAsync();
                            if (listPermissionAction == null || listPermissionAction.Count == 0)
                            {
                                listPermissionAction = await _unitOfWork.GroupActionPermissionRepository.Query(x => x.GroupId == userInfo.GroupId, null, false).Select(x => new
                                {
                                    LoanStatus = x.LoanStatus,
                                    PermissionValue = x.Value
                                }).ToListAsync();
                            }
                            var dicPermission = new Dictionary<int, int>();
                            if (listPermissionAction != null && listPermissionAction.Count > 0)
                            {
                                foreach (var item in listPermissionAction)
                                {
                                    if (item.LoanStatus > 0 && item.PermissionValue > 0)
                                    {
                                        if (!dicPermission.ContainsKey(item.LoanStatus.Value))
                                            dicPermission[item.LoanStatus.Value] = item.PermissionValue.Value;
                                    }
                                }
                            }

                            var dicUserOfHub = await _unitOfWork.UserRepository.Query(x => x.GroupId > 0 && x.Status == 1
                                    && (x.GroupId == (int)EnumGroupUser.StaffHub || x.GroupId == (int)EnumGroupUser.ManagerHub), null, false)
                                .Select(x => new
                                {
                                    UserId = x.UserId
                                }).ToDictionaryAsync(x => x.UserId, x => x);

                            //Xử lý quyền trên đơn
                            if (data.Status == (int)EnumLoanStatus.APPRAISER_REVIEW)
                            {
                                data.IsChangeStatusDetail = true;
                                data.IsChangeStaff = true;
                                data.IsPushException = true;
                            }
                            data.IsRegetLocation = data.IsTrackingLocation.GetValueOrDefault(false);

                            if (data.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                                && (data.Status == (int)EnumLoanStatus.APPRAISER_REVIEW || data.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE)
                                && (data.ReMarketingLoanBriefId.GetValueOrDefault(0) == 0 || data.ReMarketingLoanBriefId == data.LoanBriefId
                                                                                            || data.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp))
                            {
                                data.IsCreatedCarChild = true;
                            }


                            if (data.Status == (int)EnumLoanStatus.APPRAISER_REVIEW || data.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE)
                            {
                                data.IsReEsign = (data.EsignState == (int)EsignState.BORROWER_SIGNED
                                    || data.EsignState == (int)EsignState.LENDER_SIGNED);
                                data.IsGetInsuranceInfo = true;
                            }

                            data.IsCheckMomo = (!string.IsNullOrEmpty(data.FullName)
                                && !string.IsNullOrEmpty(data.BirthDay.ToString())
                                && !string.IsNullOrEmpty(data.NationalCard));

                            if (data.Status != EnumLoanStatus.CANCELED.GetHashCode() && dicPermission.ContainsKey(data.Status.Value))
                            {
                                var permissionValue = dicPermission[data.Status.Value];
                                if (permissionValue > 0)
                                {
                                    if ((EnumActionUser.Edit.GetHashCode() & permissionValue) == EnumActionUser.Edit.GetHashCode())
                                        data.IsEdit = true;
                                    if ((EnumActionUser.BackLoan.GetHashCode() & permissionValue) == EnumActionUser.BackLoan.GetHashCode())
                                        data.IsBack = true;
                                    if ((EnumActionUser.PushLoan.GetHashCode() & permissionValue) == EnumActionUser.PushLoan.GetHashCode())
                                        data.IsPush = true;
                                    if ((EnumActionUser.Cancel.GetHashCode() & permissionValue) == EnumActionUser.Cancel.GetHashCode())
                                    {
                                        var isHubCreated = (data.CreateBy > 0 && dicUserOfHub.ContainsKey(data.CreateBy.Value));
                                        if (isHubCreated || data.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                                                || data.BoundTelesaleId.GetValueOrDefault(0) == 0
                                                || data.PlatformType == (int)EnumPlatformType.LendingOnlineWeb
                                                || data.PlatformType == (int)EnumPlatformType.LendingOnlineApp)
                                            data.IsCancel = true;
                                    }
                                }
                            }
                            var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                           .Select(x => new
                           {
                               UserId = x.UserId,
                               IpPhone = x.Ipphone,
                           }).FirstOrDefaultAsync();

                            data.LinkCall = userAction.IpPhone > 0 ? _careSoftService.GenarateLinkClick2Call(userAction.IpPhone.ToString(), data.Phone) : String.Empty;
                            if (data.Relationship != null && data.Relationship.Count > 0)
                            {
                                var lstRelationship = new List<LoanBriefRelationshipDetailForApi>();
                                foreach (var item in data.Relationship)
                                {
                                    var relationship = new LoanBriefRelationshipDetailForApi()
                                    {
                                        Id = item.Id,
                                        RelationshipType = item.RelationshipType,
                                        FullName = item.FullName,
                                        Phone = item.Phone,
                                        Address = item.Address,
                                        RelationshipName = item.RelationshipName,
                                        LinkCall = userAction.IpPhone > 0 ? _careSoftService.GenarateLinkClick2Call(userAction.IpPhone.ToString(), item.Phone) : String.Empty
                                    };
                                    lstRelationship.Add(relationship);
                                }
                                if (lstRelationship != null && lstRelationship.Count > 0)
                                {
                                    data.Relationship.Clear();
                                    data.Relationship = lstRelationship;
                                }
                            }
                        }

                        def.data = data;
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                    else
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                }
                else
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/GetLoanDetail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("check_momo")]
        public async Task<ActionResult<DefaultResponse<Meta, object>>> CheckMoMo(int loanbriefId, int? hubId)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                if (hubId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền thông tin hub");
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                                    .Select(x => new
                                    {
                                        UserId = x.UserId,
                                        FullName = x.FullName,
                                        UserName = x.Username,
                                        ShopId = x.UserShop != null ? x.UserShop.First().Shop.ShopId : EnumShop.Tima.GetHashCode(),
                                        ShopName = x.UserShop != null ? x.UserShop.First().Shop.Name : "Tima",
                                    }).FirstOrDefaultAsync();

                    var data = new ChangeHubEmployeeResponse();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        if (_unitOfWork.UserShopRepository.Count(x => x.UserId == userInfo.UserId && x.ShopId == hubId) == 0)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Tài khoản không thuộc hub");
                            return Ok(def);
                        }

                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false)
                            .Select(x => new
                            {
                                LoanBriefId = x.LoanBriefId,
                                HubId = x.HubId,
                                NationalCard = x.NationalCard,
                                NationCardPlace = x.NationCardPlace,
                                ProductId = x.ProductId,
                                CustomerId = x.CustomerId,
                                FullName = x.FullName,
                                Phone = x.Phone,
                                Dob = x.Dob
                            }).FirstOrDefaultAsync();
                        if (loanbrief != null && loanbrief.LoanBriefId > 0)
                        {
                            if (loanbrief.HubId != hubId)
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Bạn không có quyền check momo của đơn vay này");
                                return Ok(def);
                            }

                            var numBillBad = 0;
                            var isCancel = false;
                            var isBackList = false;
                            var noteStr = "";

                            var result = await _momoService.CheckMomo(loanbrief.LoanBriefId, loanbrief.NationalCard);
                            if (result != null)
                            {
                                if (result != null && result.Data.Bills != null && result.Data.Bills.Count > 0)
                                {
                                    var billsBad = result.Data.Bills.Where(x => x.Status == "BAD").Count();
                                    // Nếu Khách hàng đang có 1 khoản vay là nợ xấu tại các tổ chức khác: Chặn đơn vay sản phẩm XMKCC, XMKGT.
                                    if (billsBad == 1)
                                    {
                                        if (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                            isCancel = true;
                                        else
                                            numBillBad = 1;

                                    }
                                    //Nếu Khách hàng đang có từ 2 khoản vay là nợ xấu tại các tổ chức khác trở lên: Chặn đơn vay và cho vào blacklist.
                                    else if (billsBad >= 2)
                                    {
                                        isCancel = true;
                                        isBackList = true;
                                    }
                                }
                            }
                            else
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Có lỗi xảy ra khi check momo. Vui lòng thử lại sau.");
                                return Ok(def);
                            }
                            if (!isCancel && !string.IsNullOrEmpty(loanbrief.NationCardPlace))
                            {
                                var result2 = await _momoService.CheckMomo(loanbrief.LoanBriefId, loanbrief.NationCardPlace);
                                if (result2 != null)
                                {
                                    if (result2 != null && result2.Data.Bills != null && result2.Data.Bills.Count > 0)
                                    {
                                        var billsBad = result2.Data.Bills.Where(x => x.Status == "BAD").Count();
                                        // Nếu Khách hàng đang có 1 khoản vay là nợ xấu tại các tổ chức khác: Chặn đơn vay sản phẩm XMKCC, XMKGT.
                                        if (billsBad == 1)
                                        {
                                            if (numBillBad > 0)
                                            {
                                                isCancel = true;
                                                isBackList = true;
                                            }
                                            else
                                            {
                                                if (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                                    isCancel = true;
                                            }
                                        }

                                        //Nếu Khách hàng đang có từ 2 khoản vay là nợ xấu tại các tổ chức khác trở lên: Chặn đơn vay và cho vào blacklist.
                                        else if (billsBad >= 2)
                                        {
                                            isCancel = true;
                                            isBackList = true;
                                        }
                                    }
                                }
                            }
                            if (isCancel)
                            {
                                if (isBackList)
                                {
                                    //Add BlackList
                                    var objAddBlacklist = new RequestAddBlacklist()
                                    {
                                        CustomerCreditId = loanbrief.CustomerId ?? 0,
                                        FullName = loanbrief.FullName,
                                        NumberPhone = loanbrief.Phone,
                                        CardNumber = loanbrief.NationalCard,
                                        BirthDay = loanbrief.Dob ?? null,
                                        UserIdCreate = 1,
                                        UserNameCreate = "Auto System",
                                        FullNameCreate = "Auto System",
                                        Note = noteStr,
                                        LoanCreditId = loanbrief.LoanBriefId
                                    };
                                    _lmsService.AddBlackList(objAddBlacklist);
                                }


                                //cập nhập bảng loanbrief
                                await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                                {
                                    ReasonCancel = 759,
                                    ActionState = EnumActionPush.Cancel.GetHashCode(),
                                    PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode(),
                                    InProcess = EnumInProcess.Process.GetHashCode(),
                                    LoanBriefCancelAt = DateTime.Now,
                                    LoanBriefCancelBy = userInfo.UserId,
                                });


                                //thêm note
                                await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote()
                                {
                                    LoanBriefId = loanbrief.LoanBriefId,
                                    Note = isCancel ? "Khách hàng có nhiều khoản vay bên khác, hệ thống tự động hủy đơn" : string.Format("Check momo với số cmnd: {0} không có nợ xấu", loanbrief.NationalCard),
                                    FullName = userAction.FullName,
                                    Status = 1,
                                    ActionComment = isCancel ? (int)EnumActionComment.AutoCancel : (int)EnumActionComment.CommentLoanBrief,
                                    CreatedTime = DateTime.Now,
                                    UserId = userInfo.UserId,
                                    ShopId = userAction.ShopId,
                                    ShopName = userAction.ShopName
                                });

                                _unitOfWork.Save();

                            }
                            if (isCancel)
                                def.data = "Khách hàng có nợ xấu, hệ thống tự động hủy đơn!";
                            else
                                def.data = "Khách hàng không có nợ xấu!";

                            //thêm note
                            await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote()
                            {
                                LoanBriefId = loanbrief.LoanBriefId,
                                Note = def.data.ToString(),
                                FullName = userAction.FullName,
                                Status = 1,
                                ActionComment = isCancel ? (int)EnumActionComment.AutoCancel : (int)EnumActionComment.CommentLoanBrief,
                                CreatedTime = DateTime.Now,
                                UserId = userInfo.UserId,
                                ShopId = userAction.ShopId,
                                ShopName = userAction.ShopName
                            });

                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy đơn vay trên hệ thống");
                    }
                    else
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                }
                else
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/CheckMoMo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("check_bank")]
        public async Task<ActionResult<DefaultResponse<Meta, object>>> CheckBank(int loanbriefId, int? hubId, string bankAccountNumber, string bankCode)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                if (hubId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền thông tin hub");
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var data = new ChangeHubEmployeeResponse();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        if (_unitOfWork.UserShopRepository.Count(x => x.UserId == userInfo.UserId && x.ShopId == hubId) == 0)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Tài khoản không thuộc hub");
                            return Ok(def);
                        }

                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false)
                            .Select(x => new
                            {
                                LoanBriefId = x.LoanBriefId,
                                HubId = x.HubId,
                            }).FirstOrDefaultAsync();
                        if (loanbrief != null && loanbrief.LoanBriefId > 0)
                        {
                            if (loanbrief.HubId != hubId)
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Bạn không có quyền check bank của đơn vay này");
                                return Ok(def);
                            }

                            var result = _checkBankService.CheckBank(loanbriefId, bankAccountNumber, bankCode);
                            if (result != null && result.Status == 0)
                            {
                                def.data = result.Data.AccName;
                                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            }
                            else
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Có lỗi xảy ra khi check bank. Vui lòng thử lại sau");
                        }
                        else
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy đơn vay trên hệ thống");
                    }
                    else
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                }
                else
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/CheckMoMo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("device_info")]
        public async Task<ActionResult<DefaultResponse<Meta, DeviceInfoResult>>> GetDeviceInfo([FromQuery] int loanBriefId)
        {
            var def = new DefaultResponse<Meta, DeviceInfoResult>();
            var result = new DeviceInfoResult();
            try
            {
                if (loanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var data = new List<StatusFilterHub>();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanBriefId, null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            HubEmployeeId = x.HubEmployeeId,
                            HubId = x.HubId,
                            LastSendRequestActive = x.LastSendRequestActive,
                            ActivedDeviceAt = x.ActivedDeviceAt,
                            DeviceId = x.DeviceId,
                            DeviceStatus = x.DeviceStatus
                        }).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != userInfo.UserId)
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            else if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                                 && !_unitOfWork.UserShopRepository.Any(x => x.ShopId == loanbrief.HubId && x.UserId == userInfo.UserId))
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            result = new DeviceInfoResult()
                            {
                                LoanbriefId = loanbrief.LoanbriefId,
                                LastSendRequestActive = loanbrief.LastSendRequestActive,
                                ActivedDeviceAt = loanbrief.ActivedDeviceAt,
                                DeviceId = loanbrief.DeviceId,
                                DeviceStatus = loanbrief.DeviceStatus.GetValueOrDefault(0)
                            };
                            if (Enum.IsDefined(typeof(StatusOfDeviceID), result.DeviceStatus ?? 0))
                            {
                                result.DeviceStatusName = ExtentionHelper.GetDescription((StatusOfDeviceID)(result.DeviceStatus ?? 0));
                            }
                            def.data = result;
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/GetDeviceInfo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_device")]
        public async Task<ActionResult<DefaultResponse<Meta, object>>> AddDevice(AddDeviceReq entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                if (entity.LoanbriefId <= 0 || string.IsNullOrEmpty(entity.DeviceId))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var data = new List<StatusFilterHub>();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            HubEmployeeId = x.HubEmployeeId,
                            HubId = x.HubId,
                            LastSendRequestActive = x.LastSendRequestActive,
                            ActivedDeviceAt = x.ActivedDeviceAt,
                            DeviceId = x.DeviceId,
                            DeviceStatus = x.DeviceStatus,
                            Status = x.Status,
                            TypeRemarketing = x.TypeRemarketing,
                            ProductId = x.ProductId
                        }).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đã bị hủy trước đó");
                                return Ok(def);
                            }
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != userInfo.UserId)
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            else if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                                 && !_unitOfWork.UserShopRepository.Any(x => x.ShopId == loanbrief.HubId && x.UserId == userInfo.UserId))
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            bool isChangeDeviceId = (!string.IsNullOrEmpty(loanbrief.DeviceId) && loanbrief.DeviceId != entity.DeviceId.Trim());
                            var contractType = "thuong";
                            if (loanbrief.TypeRemarketing == EnumTypeRemarketing.IsTopUp.GetHashCode())
                                contractType = "topup";
                            var resultOpent = _trackDeviceService.OpenContract("HD-" + loanbrief.LoanbriefId, entity.DeviceId, loanbrief.HubId.ToString(), loanbrief.ProductId.Value, loanbrief.LoanbriefId, contractType);
                            if (resultOpent != null
                                && (resultOpent.StatusCode == 200 || resultOpent.StatusCode == 3))
                            {
                                if (resultOpent.StatusCode == 200)
                                {
                                    var userAction = await _unitOfWork.UserRepository.Query(x => x.UserId == userInfo.UserId && x.Status == 1, null, false)
                                      .Select(x => new
                                      {
                                          UserId = x.UserId,
                                          FullName = x.FullName,
                                          UserName = x.Username
                                      }).FirstOrDefaultAsync();
                                    var shopName = _unitOfWork.ShopRepository.GetById(loanbrief.HubId)?.Name;
                                    await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                                    {
                                        LoanBriefId = loanbrief.LoanbriefId,
                                        Note = isChangeDeviceId
                                              ? string.Format("Đơn định vị: Cập nhật imei: {0} => {1}", loanbrief.DeviceId, entity.DeviceId)
                                              : string.Format("Đơn định vị: Tạo hợp đồng thành công với imei: {0}", entity.DeviceId),
                                        FullName = userAction.FullName,
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = userAction.UserId,
                                        ShopId = loanbrief.HubId,
                                        ShopName = shopName
                                    });
                                }

                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanbriefId, x => new LoanBrief()
                                {
                                    DeviceStatus = StatusOfDeviceID.OpenContract.GetHashCode(),
                                    DeviceId = entity.DeviceId,
                                    LastSendRequestActive = DateTime.Now,
                                    ContractGinno = "HD-" + loanbrief.LoanbriefId

                                });
                                _unitOfWork.Save();
                                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            }
                            else
                            {
                                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, string.IsNullOrEmpty(resultOpent.Message)
                                        ? "Xảy ra lỗi khi mở hợp đồng. Vui lòng thử lại trong ít phút."
                                        : resultOpent.Message);
                            }
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/AddDevice Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("re_active_device")]
        public async Task<ActionResult<DefaultResponse<Meta, object>>> ReActiveDevice(ReActiveDeviceReq entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                if (entity.LoanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var userInfo = GetUserInfo();
                if (userInfo != null && userInfo.UserId > 0)
                {
                    var data = new List<StatusFilterHub>();
                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            HubEmployeeId = x.HubEmployeeId,
                            HubId = x.HubId,
                            LastSendRequestActive = x.LastSendRequestActive,
                            ActivedDeviceAt = x.ActivedDeviceAt,
                            DeviceId = x.DeviceId,
                            DeviceStatus = x.DeviceStatus,
                            Status = x.Status,
                            TypeRemarketing = x.TypeRemarketing,
                            ProductId = x.ProductId
                        }).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đã bị hủy trước đó");
                                return Ok(def);
                            }
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != userInfo.UserId)
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            else if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                                 && !_unitOfWork.UserShopRepository.Any(x => x.ShopId == loanbrief.HubId && x.UserId == userInfo.UserId))
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            if (loanbrief.DeviceStatus == StatusOfDeviceID.Fail.GetHashCode())
                            {
                                if (_trackDeviceService.ReActiveDevice("HD-" + loanbrief.LoanbriefId, loanbrief.DeviceId, loanbrief.LoanbriefId))
                                {
                                    _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanbriefId, x => new LoanBrief()
                                    {
                                        DeviceStatus = (int)StatusOfDeviceID.OpenContract,
                                        LastSendRequestActive = DateTime.Now
                                    });
                                    _unitOfWork.Save();
                                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                }
                                else
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Xảy ra lỗi khi kích hoạt lại thiết bị");
                                }
                            }
                            else
                            {
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, string.Format("Thiết bị đang ở trạng thái: {0}. Không thể kích hoạt lại.", ExtentionHelper.GetDescription((StatusOfDeviceID)(loanbrief.DeviceStatus ?? 0))));
                            }
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Token không hợp lệ");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DirectSale/ReActiveDevice Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        private string ValidLoanBriefInit(LoanBriefInit entity)
        {
            try
            {
                var strMessage = string.Empty;
                //kiểm tra thông tin đầu vào
                if (string.IsNullOrEmpty(entity.Phone))
                    return "SĐT không được để trống";
                else
                {
                    if (!entity.Phone.ValidPhone())
                        return "Định dạng SĐT không đúng";
                }
                if (string.IsNullOrEmpty(entity.FullName))
                    return "Họ và tên KH không được để trống";
                else
                {
                    entity.FullName = entity.FullName.Trim();
                    entity.FullName = entity.FullName.ReduceWhitespace();
                    entity.FullName = entity.FullName.TitleCaseString();
                    if (!string.IsNullOrEmpty(entity.BankAccountName))
                    {
                        entity.BankAccountName = entity.BankAccountName.ReduceWhitespace();
                        entity.BankAccountName = entity.BankAccountName.TitleCaseString();
                        entity.BankAccountName = ConvertExtensions.ConvertToUnSign(entity.BankAccountName);
                        if (String.Compare(ConvertExtensions.ConvertToUnSign(entity.FullName), entity.BankAccountName, false) != 0)
                            return "Tên khách hàng và tên chủ thẻ không khớp nhau";
                    }
                }
                if (string.IsNullOrEmpty(entity.NationalCard))
                    return "Số CMND không được để trống";

                if (entity.NationalCard.Length != (int)NationalCardLength.CMND_QD && entity.NationalCard.Length != (int)NationalCardLength.CMND
                   && entity.NationalCard.Length != (int)NationalCardLength.CCCD)
                    return "Định dạng CMND không đúng";

                if (!entity.ProvinceId.HasValue)
                    return "Thành phố đang ở không được để trống";

                if (!entity.DistrictId.HasValue)
                    return "Quận/Huyện đang ở không được để trống";

                if (!entity.WardId.HasValue)
                    return "Phường/Xã đang ở không được để trống";

                if (!Enum.IsDefined(typeof(EnumProductCredit), entity.ProductId))
                    return "Gói vay không được để trống";

                if (entity.LoanAmount <= 0)
                    return "Số tiền KH cần vay không được để trống";

                if (!string.IsNullOrEmpty(entity.PhoneOther))
                {
                    if (!entity.PhoneOther.ValidPhone())
                        return "SĐT khác không đúng định dạng";
                }
                if (!string.IsNullOrEmpty(entity.Email))
                {
                    if (!entity.Email.ValidEmail())
                        return "Email không đúng định dạng";
                }

                return strMessage;
            }
            catch (Exception ex)
            {
                return "Xảy ra lỗi. Vui lòng thử lại sau";
            }
        }
        private decimal GetMaxPriceProductTopup(int productId, int loanBriefId, int productCredit, int typeOfOwnerShip, long totalMoneyDebtCurrent, ref decimal priceAi)
        {
            long originalCarPrice = 0;
            // lấy giá bên AI
            decimal priceCarAI = priceAi = _productService.GetPriceAI(productId, loanBriefId);
            if (priceCarAI > 0)
                originalCarPrice = (long)priceCarAI;
            return Common.Utils.ProductPriceUtils.GetMaxPriceTopup(productCredit, originalCarPrice, totalMoneyDebtCurrent, typeOfOwnerShip);

        }
        private decimal GetMaxPriceProductV3(int productId, int loanBriefId, long maxPrice, int productCredit, ref long priceAG, ref decimal priceAi)
        {
            var maxPriceProduct = 0L;
            long originalCarPrice = 0;
            // lấy giá bên AI
            decimal priceCarAI = priceAi = _productService.GetPriceAI(productId, loanBriefId);
            if (priceCarAI > 0)
                originalCarPrice = (long)priceCarAI;
            else
                priceAG = 0;

            var typeRemarketingLtv = 0;
            var loanbrief = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanBriefId, null, false)
                                .Select(LoanBriefBasicInfo.ProjectionViewDetail).FirstOrDefault();
            if (loanbrief.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                typeRemarketingLtv = (int)TypeRemarketingLtv.TopUp;
            if (loanbrief.IsReborrow.GetValueOrDefault(false))
                typeRemarketingLtv = (int)TypeRemarketingLtv.Reborrow;
            maxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPriceV3(productCredit, originalCarPrice, 0, 0, maxPrice, 0, typeRemarketingLtv);
            return maxPriceProduct;
        }
    }
}
