﻿using System;
using System.Collections.Generic;
using System.Linq;
using LOS.DAL.UnitOfWork;
using LOS.AppAPI.Helpers;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.Dapper;
using Microsoft.Extensions.Configuration;
using System.Text;
using LOS.AppAPI.Models.Erp;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.DynamicLinq;
using Microsoft.EntityFrameworkCore;
using LOS.AppAPI.Models;
using LOS.DAL.EntityFramework;
using Microsoft.AspNetCore.Cors;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    // [AuthorizeIPFilter("API_LMS")]
    public class ERPController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IConfiguration _configuration;

        public ERPController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        //Chi tiết đơn vay
        [HttpGet]
        [Route("LoanDetail")]
        [AuthorizeToken("APP_API_ERP")]
        public ActionResult<DefaultResponse<Meta, ERPLoanBriefDetail>> GetById([FromQuery] int LoanId = 0)
        {
            var def = new DefaultResponse<Meta, ERPLoanBriefDetail>();
            try
            {
                if (LoanId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }

                var data = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanId, null, false).Select(ERPLoanBriefDetail.ProjectionLenderDetail).FirstOrDefault();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ERP/Detail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //[HttpPost]
        //[Route("GetList")]
        //[AuthorizeToken("APP_API_ERP")]
        //public ActionResult<DefaultResponse<SummaryMeta, List<ERPLoanBriefGetList>>> Disbursement([FromBody] Models.Erp.Erp.GetList req)
        //{
        //    var def = new DefaultResponse<SummaryMeta, List<ERPLoanBriefGetList>>();
        //    try
        //    {
        //        //var hubId = -1;
        //        //var codeId = -1;
        //        //if (req.HubId > 0)
        //        //    hubId = req.HubId;
        //        //if (req.CodeId > 0)
        //        //    codeId = req.CodeId;
        //        var query = unitOfWork.LoanBriefRepository
        //            .Query(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done
        //            && (x.ReCareLoanbrief == (int)EnumReCareLoanbrief.BorrowCavet)
        //            && (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH)
        //            && (x.HubId == req.HubId || req.HubId == 0) && (x.CodeId == req.CodeId || req.CodeId == 0)
        //            && (x.ToDate >= req.FromDate && x.ToDate <= req.ToDate), null, false).Select(ERPLoanBriefGetList.ProjectionDetail);
        //        var totalRecords = query.Count();
        //        List<ERPLoanBriefGetList> data;
        //        data = query.OrderByDescending(x => x.ToDate).Skip((req.Page - 1) * req.PageSize).Take(req.PageSize).ToList();
        //        def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, req.Page, req.PageSize, totalRecords);
        //        def.data = data;
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex, "Erp/GetList Exception");
        //        def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
        //    }
        //    return Ok(def);
        //}

        [HttpGet]
        [Route("get_fullname_and_statutelesale")]
        public ActionResult<DefaultResponse<Meta, ERPGetFullNameAndStatusTelesales>> GetFullNameAndStatusTelesales(int loanbriefId = 0)
        {
            var def = new DefaultResponse<Meta, ERPGetFullNameAndStatusTelesales>();
            try
            {
                if (loanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var data = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(ERPGetFullNameAndStatusTelesales.ProjectionDetail).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ERP/GetFullNameAndStatusTelesales Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [EnableCors]
        [Route("get_user")]
        public ActionResult<DefaultResponse<Meta, GetUserERP>> GetUser(string userName)
        {
            var def = new DefaultResponse<Meta, GetUserERP>();
            try
            {
                var data = unitOfWork.UserRepository.Query(x => x.Username == userName, null, false).Select(GetUserERP.ProjectionDetail).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ERP/GetUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("list_loanbrif_today")]
        [AuthorizeToken("APP_API_ERP")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LoanInfoForErp>>>> ListLoanbriefToday()
        {
            var def = new DefaultResponse<Meta, List<LoanInfoForErp>>();
            try
            {
                var db = new DapperHelper(_configuration.GetConnectionString("ReportDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select HubEmployeeId, LoanBriefId, Phone ");
                sql.AppendLine("from rp.LogLoanBrief(nolock) ll ");
                sql.AppendLine("where ll.Status in (40, 45)  ");
                sql.AppendLine("and DATEDIFF(day,ll.Fordate, GETDATE()) = 0 ");
                sql.AppendLine("order by HubEmployeeId");
                var data = await db.QueryAsync<LoanInfoForErp>(sql.ToString());
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ERP/ListLoanbriefToday Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        //[HttpGet]
        //[Route("info_loan_property")]
        //[AuthorizeToken("APP_API_ERP")]
        //public async Task<ActionResult<DefaultResponse<Meta, LoanBriefPropertyInfo>>> GetInfoProperty([FromQuery] int codeId, [FromQuery] int userId, [FromQuery] int shopId = 0, [FromQuery] int loanbriefId = 0)
        //{
        //    var def = new DefaultResponse<Meta, LoanBriefPropertyInfo>();
        //    try
        //    {
        //        if (codeId <= 0 || userId <= 0 || (shopId <= 0 && loanbriefId <= 0))
        //        {
        //            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
        //            return Ok(def);
        //        }

        //        var userInfo = await unitOfWork.UserRepository.Query(x => x.UserId == userId && x.Status == 1, null, false).Select(x => new
        //        {
        //            UserId = x.UserId,
        //            GroupId = x.GroupId
        //        }).FirstOrDefaultAsync();

        //        if (userInfo != null && userInfo.UserId > 0)
        //        {
        //            if (userInfo.GroupId == (int)EnumGroupUser.ManagerHub || userInfo.GroupId == (int)EnumGroupUser.StaffHub
        //                || userInfo.GroupId == (int)EnumGroupUser.KSNB || userInfo.GroupId == (int)EnumGroupUser.TP_KSNB)
        //            {
        //                var data = new LoanBriefPropertyInfo();
        //                //nếu là ksnb thì xem all hợp đồng
        //                if (userInfo.GroupId == (int)EnumGroupUser.KSNB || userInfo.GroupId == (int)EnumGroupUser.TP_KSNB)
        //                {
        //                    //Nếu có loanbriefid => lấy theo đơn vay luôn
        //                    if (loanbriefId > 0)
        //                        data = unitOfWork.LoanBriefRepository.Query(x => x.CodeId == codeId && x.LoanBriefId == loanbriefId, null, false)
        //                            .Select(LoanBriefPropertyInfo.Projection).FirstOrDefault();
        //                    else
        //                        data = unitOfWork.LoanBriefRepository.Query(x => x.CodeId == codeId && x.HubId == shopId, null, false)
        //                       .Select(LoanBriefPropertyInfo.Projection).FirstOrDefault();
        //                }
        //                else
        //                {
        //                    //Nếu là nhân viên hub => chỉ xem hd nhân viên đó xử lý
        //                    if (userInfo.GroupId == (int)EnumGroupUser.StaffHub)
        //                    {
        //                        data = unitOfWork.LoanBriefRepository.Query(x => x.CodeId == codeId && x.HubId == shopId && x.HubEmployeeId == userInfo.UserId, null, false)
        //                        .Select(LoanBriefPropertyInfo.Projection).FirstOrDefault();
        //                    }
        //                    else
        //                    {
        //                        //Kiểm tra user có quản lý hub truyền lên k
        //                        if (unitOfWork.UserShopRepository.Any(x => x.UserId == userInfo.UserId && x.ShopId == shopId))
        //                        {
        //                            data = unitOfWork.LoanBriefRepository.Query(x => x.CodeId == codeId && x.HubId == shopId, null, false)
        //                            .Select(LoanBriefPropertyInfo.Projection).FirstOrDefault();
        //                        }
        //                        else
        //                        {
        //                            def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Tài khoản không có quyền quản lý HUB");
        //                            return Ok(def);
        //                        }
        //                    }
        //                }
        //                if (data != null && data.LoanbriefId > 0)
        //                {
        //                    var listDocument = new List<int>()
        //                        {
        //                            (int)EnumDocumentType.Motorbike_Registration_Certificate
        //                        };
        //                    var listFile = await unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == data.LoanbriefId
        //                        && x.TypeId > 0 && listDocument.Contains(x.TypeId.Value)
        //                        && x.Status == 1 && x.IsDeleted.GetValueOrDefault(0) == 0, null, false).Select(x => new
        //                        {
        //                            FileThumb = x.FileThumb,
        //                            FilePath = x.FilePath,
        //                            S3status = x.S3status,
        //                            MecashId = x.MecashId
        //                        }).ToListAsync();
        //                    if (listFile != null && listFile.Count > 0)
        //                    {
        //                        var ServiceURL = _configuration["AppSettings:ServiceURL"];
        //                        var ServiceURLAG = _configuration["AppSettings:ServiceURLAG"];
        //                        var ServiceURLFileTima = _configuration["AppSettings:ServiceURLFileTima"];
        //                        var listFileResult = new List<LoanbriefFileInfo>();
        //                        foreach (var item in listFile)
        //                        {
        //                            var fileItem = new LoanbriefFileInfo();
        //                            if (!string.IsNullOrEmpty(item.FileThumb))
        //                                fileItem.FileThumb = ServiceURLFileTima + item.FileThumb;
        //                            if (!string.IsNullOrEmpty(item.FilePath))
        //                            {
        //                                if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
        //                                    fileItem.FileOrigin = ServiceURL + item.FilePath;
        //                                if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
        //                                    fileItem.FileOrigin = ServiceURLAG + item.FilePath;
        //                                if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
        //                                    fileItem.FileOrigin = ServiceURLFileTima + item.FilePath;
        //                                else fileItem.FileOrigin = item.FilePath;
        //                            }
        //                            if (string.IsNullOrEmpty(fileItem.FileThumb))
        //                                fileItem.FileThumb = fileItem.FileOrigin;

        //                            listFileResult.Add(fileItem);
        //                        }
        //                        data.files = listFileResult;
        //                    }

        //                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
        //                    def.data = data;
        //                    return Ok(def);
        //                }
        //                else
        //                {
        //                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "không tìm thấy thông tin đơn vay");
        //                    return Ok(def);
        //                }
        //            }
        //            else
        //            {
        //                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "nhóm tài khoản không có quyền truy cập");
        //                return Ok(def);
        //            }
        //        }
        //        else
        //        {
        //            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "user not found");
        //            return Ok(def);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex, "ERP/GetInfoProperty Exception");
        //        def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
        //        return Ok(def);
        //    }

        //}

        [HttpGet]
        [Route("info_loan_property")]
        [AuthorizeToken("APP_API_ERP")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanBriefPropertyInfo>>> GetInfoProperty([FromQuery] int userId, [FromQuery] int loanbriefId)
        {
            var def = new DefaultResponse<Meta, LoanBriefPropertyInfo>();
            try
            {
                if (userId <= 0 || loanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }

                var userInfo = await unitOfWork.UserRepository.Query(x => x.UserId == userId && x.Status == 1, null, false).Select(x => new
                {
                    UserId = x.UserId,
                    GroupId = x.GroupId
                }).FirstOrDefaultAsync();

                if (userInfo != null && userInfo.UserId > 0)
                {
                    if (userInfo.GroupId == (int)EnumGroupUser.ManagerHub || userInfo.GroupId == (int)EnumGroupUser.StaffHub
                      || userInfo.GroupId == (int)EnumGroupUser.KSNB || userInfo.GroupId == (int)EnumGroupUser.TP_KSNB)
                    {

                        var loanbrief = await unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(LoanBriefPropertyInfo.Projection).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            //Kiểm tra trạng thái đơn vay
                            if (loanbrief.LoanStatus != (int)EnumLoanStatus.DISBURSED && loanbrief.LoanStatus != (int)EnumLoanStatus.FINISH)
                            {
                                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Đơn vay chưa được GN");
                                return Ok(def);
                            }
                            //nếu là hub thì xem hợp đồng của hub quản lý
                            if (userInfo.GroupId == (int)EnumGroupUser.ManagerHub || userInfo.GroupId == (int)EnumGroupUser.StaffHub)
                            {
                                //Nếu là nhân viên hub => chỉ xem hd nhân viên đó xử lý
                                if (userInfo.GroupId == (int)EnumGroupUser.StaffHub)
                                {
                                    if (loanbrief.HubEmployeeId != userInfo.UserId)
                                    {
                                        def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Tài khoản không có quyền quản lý đơn vay");
                                        return Ok(def);
                                    }
                                }
                                else
                                {
                                    //Kiểm tra user có quản lý hub truyền lên k
                                    if (!unitOfWork.UserShopRepository.Any(x => x.UserId == userInfo.UserId && x.ShopId == loanbrief.HubId))
                                    {
                                        def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Tài khoản không có quyền quản lý HUB");
                                        return Ok(def);
                                    }
                                }
                            }

                            var listDocument = new List<int>()
                                {
                                    (int)EnumDocumentType.Motorbike_Registration_Certificate
                                };
                            var listFile = await unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanbrief.LoanbriefId
                                && x.TypeId > 0 && listDocument.Contains(x.TypeId.Value)
                                && x.Status == 1 && x.IsDeleted.GetValueOrDefault(0) == 0, null, false).Select(x => new
                                {
                                    FileThumb = x.FileThumb,
                                    FilePath = x.FilePath,
                                    S3status = x.S3status,
                                    MecashId = x.MecashId
                                }).ToListAsync();
                            if (listFile != null && listFile.Count > 0)
                            {
                                var ServiceURL = _configuration["AppSettings:ServiceURL"];
                                var ServiceURLAG = _configuration["AppSettings:ServiceURLAG"];
                                var ServiceURLFileTima = _configuration["AppSettings:ServiceURLFileTima"];
                                var listFileResult = new List<LoanbriefFileInfo>();
                                foreach (var item in listFile)
                                {
                                    var fileItem = new LoanbriefFileInfo();
                                    if (!string.IsNullOrEmpty(item.FileThumb))
                                        fileItem.FileThumb = ServiceURLFileTima + item.FileThumb;
                                    if (!string.IsNullOrEmpty(item.FilePath))
                                    {
                                        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                                            fileItem.FileOrigin = ServiceURL + item.FilePath;
                                        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                                            fileItem.FileOrigin = ServiceURLAG + item.FilePath;
                                        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                                            fileItem.FileOrigin = ServiceURLFileTima + item.FilePath;
                                        else fileItem.FileOrigin = item.FilePath;
                                    }
                                    if (string.IsNullOrEmpty(fileItem.FileThumb))
                                        fileItem.FileThumb = fileItem.FileOrigin;

                                    listFileResult.Add(fileItem);
                                }
                                loanbrief.files = listFileResult;
                            }

                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            def.data = loanbrief;
                            return Ok(def);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tìm thấy thông tin đơn vay");
                            return Ok(def);
                        }

                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Nhóm tài khoản không có quyền truy cập");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "user not found");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ERP/GetInfoProperty Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return Ok(def);
            }

        }

        [HttpPost]
        [Route("device_info")]
        [AuthorizeToken("APP_API_ERP")]
        public async Task<ActionResult<DefaultResponse<Meta, List<ERPDeviceInfo>>>> DeviceInfo([FromBody] DeviceInfoReq req)
        {
            var def = new DefaultResponse<Meta, List<ERPDeviceInfo>>();
            try
            {
                if (req == null || req.DeviceIds == null || req.DeviceIds.Count == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var listDeviceId = req.DeviceIds.Distinct();
                var hashDeviceId = new HashSet<string>();
                var data = await unitOfWork.LoanBriefRepository.Query(x => !string.IsNullOrEmpty(x.DeviceId)
                    && x.Status != (int)EnumLoanStatus.FINISH && x.Status != (int)EnumLoanStatus.CANCELED
                    && listDeviceId.Contains(x.DeviceId), null, false)
                    .Select(ERPDeviceInfo.ProjectionDetail).ToListAsync();
                if (data != null && data.Count > 0)
                {
                    var task = Task.Run(() => Parallel.ForEach(data, item =>
                    {
                        if (item.Status > 0 && Enum.IsDefined(typeof(EnumLoanStatus), item.Status))
                            item.LoanStatus = Description.GetDescription((EnumLoanStatus)item.Status);
                    }));
                    Task.WaitAll(task);
                    hashDeviceId = data.Select(x => x.DeviceId).Distinct().ToHashSet();
                }
                else
                    data = new List<ERPDeviceInfo>();
                var taskData = Task.Run(() => Parallel.ForEach(listDeviceId, item =>
                {
                    if (!hashDeviceId.Contains(item))
                        data.Add(new ERPDeviceInfo()
                        {
                            DeviceId = item
                        });
                }));
                Task.WaitAll(taskData);
                def.data = data;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ERP/DeviceInfo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("LoanInfoDevice")]
        [AuthorizeToken("APP_API_ERP")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DTOs.ERP.LoanbriefCavetViewApi>>>> GetLoanInfoDevice(string search)
        {
            var def = new DefaultResponse<Meta, List<DTOs.ERP.LoanbriefCavetViewApi>>();
            try
            {
                var filter = FilterHelper<LoanBrief>.Create(x => (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH));

                if (!string.IsNullOrEmpty(search))
                {
                    var textSearch = search.Trim();
                    if (ConvertExtensions.IsNumber(textSearch)) //Nếu là số
                    {
                        if (textSearch.Length == (int)NationalCardLength.CMND_QD || textSearch.Length == (int)NationalCardLength.CMND || textSearch.Length == (int)NationalCardLength.CCCD)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.NationalCard == textSearch || x.NationCardPlace == textSearch)));
                        else//search phone
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Phone == textSearch || x.PhoneOther == textSearch)));
                    }
                    else//nếu là text =>search tên
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.FullName.Contains(search)));
                }
                var dataResult = new List<DTOs.ERP.LoanbriefCavetViewApi>();
                var data = await unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LOS.AppAPI.DTOs.ERP.LoanbriefCavetDetail.Projection).ToListAsync();
                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        dataResult.Add(new DTOs.ERP.LoanbriefCavetViewApi()
                        {
                            LoanBriefId = item.LoanBriefId,
                            FullName = item.FullName,
                            ProductName = item.ProductName,
                            LoanAmount = item.LoanAmount,
                            CodeId = item.CodeId,
                            LoanTime = $"{item.LoanTime} tháng",
                            DisbursementAt = item.DisbursementAt,
                            IsLocate = item.IsLocate,
                            StatusName = item.Status > 0 && Enum.IsDefined(typeof(EnumLoanStatus), item.Status) ? ExtentionHelper.GetDescription((EnumLoanStatus)item.Status) : string.Empty,
                            DeviceStatus = item.DeviceStatus.HasValue && Enum.IsDefined(typeof(StatusOfDeviceID), item.DeviceStatus) ? ExtentionHelper.GetDescription((StatusOfDeviceID)item.DeviceStatus) : string.Empty,
                        });
                    }
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = dataResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Erp/GetLoanInfoDevice Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [AuthorizeToken("APP_API_ERP")]
        [Route("get_user_by_email")]
        public ActionResult<DefaultResponse<Meta, GetUserERP>> GetUserByEmail(string email)
        {
            var def = new DefaultResponse<Meta, GetUserERP>();
            try
            {
                var data = unitOfWork.UserRepository.Query(x => x.Email == email, null, false).Select(GetUserERP.ProjectionDetail).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ERP/GetUserByEmail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [AuthorizeToken("APP_API_ERP")]
        [Route("get_loan_status")]
        public ActionResult<DefaultResponse<Meta, DTOs.ERP.LoanBriefStatus>> GetLoanStatus(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, DTOs.ERP.LoanBriefStatus>();
            try
            {
                var data = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false)
                    .Select(DTOs.ERP.LoanBriefStatus.ProjectionDetail).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ERP/GetLoanStatus Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}