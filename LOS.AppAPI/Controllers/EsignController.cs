﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.WebSockets;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Xml;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using LOS.AppAPI.DTOs.App;
using LOS.AppAPI.DTOs.Appraiser;
using LOS.AppAPI.DTOs.LMS;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models.App;
using LOS.AppAPI.Models.ESign;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.ESign;
using LOS.AppAPI.Service.ExportPDF;
using LOS.AppAPI.Service.LmsService;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.Common.Utils;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.ColorSpaces;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Processing;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    //[Authorize]
    public class EsignController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _baseConfig;
        private readonly IESignService _esignService;
        private readonly IWebHostEnvironment _environment;
        private readonly ILmsService _lmsService;
        private readonly IExportPdfService _pdfService;
        private readonly IIdentityService _identityService;
        private readonly string TIMA_UUID = "";
        private readonly string TIMA_PASSCODE = "";

        public EsignController(IUnitOfWork unitOfWork, IConfiguration configuration, IESignService eSignService, IWebHostEnvironment environment,
            ILmsService lmsService, IExportPdfService pdfService, IIdentityService identityService)
        {
            this._unitOfWork = unitOfWork;
            this._baseConfig = configuration;
            this._esignService = eSignService;
            this._environment = environment;
            this._lmsService = lmsService;
            this._pdfService = pdfService;
            this._identityService = identityService;
            TIMA_UUID = _baseConfig["AppSettings:esign_tima_uuid"].ToString();
            TIMA_PASSCODE = _baseConfig["AppSettings:esign_tima_passcode"].ToString();
        }

        [HttpPost]
        [Route("CreateAgreementForBorrower")]
        [AuthorizeTokenOtp]
        public async Task<IActionResult> CreateAgreementForBorrower([FromForm] RegisterAgreementReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                // Kiểm tra file cmnd
                bool fileValidated = true;
                if (req.files != null && req.files.Count > 0 && req.files.Count <= 2)
                {
                    foreach (var file in req.files)
                    {
                        var extension = Path.GetExtension(file.FileName);
                        var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                        if (!allowedExtensions.Contains(extension.ToLower()))
                        {
                            fileValidated = false;
                            break;
                        }
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (!fileValidated)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var customer = _unitOfWork.CustomerRepository.Query(x => x.CustomerId == req.customerId, null, false).FirstOrDefault();
                if (customer != null)
                {
                    //Kiểm tra sdt KH và token KH có khớp nhau không
                    var phoneToken = _identityService.GetPhoneIdentity();
                    if (string.IsNullOrEmpty(phoneToken))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                        return Ok(def);
                    }

                    if (customer.Phone != phoneToken)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                        return Ok(def);
                    }

                    // Kiểm tra thông tin có đầy đủ
                    if (String.IsNullOrEmpty(customer.EsignAgreementId))
                    {
                        if ((String.IsNullOrEmpty(customer.NationalCard) && String.IsNullOrEmpty(customer.Passport)) || String.IsNullOrEmpty(customer.FullName) || String.IsNullOrEmpty(customer.Address) || !customer.ProvinceId.HasValue || (customer.ProvinceId.HasValue && customer.ProvinceId <= 0) || String.IsNullOrEmpty(customer.Email) || String.IsNullOrEmpty(customer.Phone))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                            return Ok(def);
                        }
                        string agreementId = Guid.NewGuid().ToString().ToUpper();
                        int idType = !String.IsNullOrEmpty(customer.NationalCard) ? 1 : 2;
                        string idNumber = idType == 1 ? customer.NationalCard : customer.Passport;
                        if (idType == 1 && idNumber.Length == 12)
                            idType = 3;
                        var province = _unitOfWork.ProvinceRepository.GetById(customer.ProvinceId);
                        // file 0 > cmnd mặt trước
                        var front = await req.files[0].GetBytes();
                        var back = await req.files[1].GetBytes();
                        var logPartner = new LogPartner();
                        var result = _esignService.prepareCertificateForSignCloud(agreementId, customer.FullName.ToUpper(), idType, idNumber, customer.Address, province.Name, "VN", front, back, customer.Email, customer.Phone, ref logPartner);
                        if (result != null && result.responseCode == 0)
                        {
                            logPartner.ResponseCode = result.responseCode;
                            logPartner.ResponseContent = JsonConvert.SerializeObject(result);
                            logPartner.CustomerId = customer.CustomerId;
                            _unitOfWork.LogPartnerRepository.Insert(logPartner);
                            // success 
                            customer.EsignAgreementId = agreementId;
                            _unitOfWork.CustomerRepository.Update(customer);
                            _unitOfWork.SaveChanges();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            logPartner.CustomerId = customer.CustomerId;
                            if (result != null)
                            {
                                logPartner.ResponseCode = result.responseCode;
                                logPartner.ResponseContent = JsonConvert.SerializeObject(result);
                            }
                            _unitOfWork.LogPartnerRepository.Insert(logPartner);
                            _unitOfWork.SaveChanges();
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Esign/CreateAgreementForBorrower Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("SignContractForBorrower")]
        [AuthorizeTokenOtp]
        public async Task<IActionResult> SignContractForBorrower([FromBody] ConfirmSignReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanInfo = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.loanBriefId, null, false).Select(DAL.DTOs.CheckValidate.ProjectionDetail).FirstOrDefaultAsync();
                if (loanInfo != null)
                {
                    //Kiểm tra sdt KH và token KH có khớp nhau không
                    var phoneToken = _identityService.GetPhoneIdentity();
                    if (string.IsNullOrEmpty(phoneToken))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                        return Ok(def);
                    }

                    if (loanInfo.Phone != phoneToken)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                        return Ok(def);
                    }
                    //kiểm tra xem đã ký hợp đồng chưa
                    if (loanInfo.EsignState == (int)EsignState.BORROWER_SIGNED && !string.IsNullOrEmpty(loanInfo.EsignBorrowerContract))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Hợp đồng đã được ký thành công trước đó.");
                        return Ok(def);
                    }
                    // Kiểm tra xem có request thành công sang fpt 5p trước không
                    var time = DateTime.Now.AddMinutes(-5);
                    var isRequested = _unitOfWork.LogPartnerRepository.Any(x => x.LoanBriefId == req.loanBriefId && x.Provider == "FPT"
                    && x.TransactionType == "PREPARE_BORROWER_CONTRACT" && x.CreatedTime >= time && x.HttpStatusCode == 200);
                    if (isRequested)
                    {
                        //trả ra thành công để đợi otp hoặc lấy lại otp
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        return Ok(def);
                    }

                    #region validate thông tin trước khi ký
                    //Kiểm tra trạng thái đơn
                    if (loanInfo.Status != (int)EnumLoanStatus.HUB_LOAN_DISTRIBUTING && loanInfo.Status != (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING
                        && loanInfo.Status != (int)EnumLoanStatus.APPRAISER_REVIEW && loanInfo.Status != (int)EnumLoanStatus.HUB_CHT_APPROVE
                        && loanInfo.Status != (int)EnumLoanStatus.WAIT_HUB_EMPLOYEE_PREPARE_LOAN)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Đơn vay đang được phê duyệt, không thể thực hiện ký hợp đồng");
                        return Ok(def);
                    }
                    //Customer
                    if (string.IsNullOrEmpty(loanInfo.FullName))
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin Tên khách hàng");
                        return Ok(def);
                    }
                    if (string.IsNullOrEmpty(loanInfo.Dob.ToString()))
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin ngày sinh khách hàng");
                        return Ok(def);
                    }
                    if (string.IsNullOrEmpty(loanInfo.NationalCard))
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin CMND/CCCD");
                        return Ok(def);
                    }

                    if (loanInfo.LoanBriefResident == null || string.IsNullOrEmpty(loanInfo.LoanBriefResident.Address))
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin địa chỉ hiện tại");
                        return Ok(def);
                    }
                    if (loanInfo.LoanBriefHousehold == null || string.IsNullOrEmpty(loanInfo.LoanBriefHousehold.Address))
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin địa chỉ thường trú");
                        return Ok(def);
                    }
                    //Loan
                    if (loanInfo.LoanAmount.GetValueOrDefault(0) == 0)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có số tiền đăng ký vay");
                        return Ok(def);
                    }
                    if (loanInfo.LoanTime.GetValueOrDefault(0) == 0)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thời gian vay");
                        return Ok(def);
                    }
                    if (loanInfo.LoanPurpose.GetValueOrDefault(0) == 0)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có mục đích vay");
                        return Ok(def);
                    }
                    if (loanInfo.ReceivingMoneyType.GetValueOrDefault(0) == 0)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có hình thức nhận tiền giải ngân");
                        return Ok(def);
                    }
                    if (loanInfo.ReceivingMoneyType == (int)EnumTypeReceivingMoney.NumberAccount)
                    {
                        if (loanInfo.BankId.GetValueOrDefault(0) == 0)
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin ngân hàng");
                            return Ok(def);
                        }
                        if (string.IsNullOrEmpty(loanInfo.BankAccountNumber))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có Số tài khoản ngân hàng");
                            return Ok(def);
                        }
                    }
                    //LoanBriefProperty
                    if (loanInfo.LoanBriefProperty != null)
                    {
                        if (string.IsNullOrEmpty(loanInfo.LoanBriefProperty.MotobikeCertificateNumber))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin số đăng ký xe");
                            return Ok(def);
                        }
                        if (string.IsNullOrEmpty(loanInfo.LoanBriefProperty.PlateNumber))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin biển số xe");
                            return Ok(def);
                        }
                        if (string.IsNullOrEmpty(loanInfo.LoanBriefProperty.OwnerFullName))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin chủ xe");
                            return Ok(def);
                        }
                        if (loanInfo.LoanBriefProperty.BrandId.GetValueOrDefault(0) == 0)
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin hãng xe");
                            return Ok(def);
                        }
                        if (loanInfo.LoanBriefProperty.ProductId.GetValueOrDefault(0) == 0)
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin tên xe");
                            return Ok(def);
                        }
                        if (string.IsNullOrEmpty(loanInfo.LoanBriefProperty.Chassis))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin số khung");
                            return Ok(def);
                        }
                        if (string.IsNullOrEmpty(loanInfo.LoanBriefProperty.Engine))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin số máy");
                            return Ok(def);
                        }

                        if (string.IsNullOrEmpty(loanInfo.LoanBriefProperty.MotobikeCertificateDate))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin ngày cấp đăng ký xe");
                            return Ok(def);
                        }
                        if (string.IsNullOrEmpty(loanInfo.LoanBriefProperty.MotobikeCertificateAddress))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin nơi cấp đăng ký xe");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin tài sản");
                        return Ok(def);
                    }
                    #endregion

                    var customer = _unitOfWork.CustomerRepository.Query(x => x.CustomerId == loanInfo.CustomerId, null, false).FirstOrDefault();
                    if (customer != null)
                    {
                        // Kiểm tra nếu chưa đăng ký thì đăng ký esign
                        if (String.IsNullOrEmpty(customer.EsignAgreementId))
                        {
                            // Kiểm tra xem đã ekyc thành công chua
                            var ekycSuccess = _unitOfWork.LogLoanInfoAiRepository.Any(x => x.ServiceType == (int)ServiceTypeAI.CheckLiveness && x.LoanbriefId == loanInfo.LoanBriefId && x.Status == 1);

                            if (ekycSuccess || loanInfo.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan
                                || loanInfo.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                            {
                                // Đăng ký
                                var files = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanInfo.LoanBriefId && x.TypeId == (int)EnumDocumentType.CMND_CCCD, null, false).OrderByDescending(x => x.CreateAt).ToList();
                                if (files != null)
                                {
                                    // Đủ ảnh để đăng ký ekyc
                                    string agreementId = Guid.NewGuid().ToString().ToUpper();
                                    int idType = !String.IsNullOrEmpty(customer.NationalCard) ? 1 : 2;
                                    string idNumber = (idType == 1) ? customer.NationalCard : customer.Passport;
                                    if (idType == 1 && idNumber.Length == 12)
                                        idType = 3;
                                    var province = _unitOfWork.ProvinceRepository.GetById(customer.ProvinceId);
                                    // file 0 > cmnd mặt trước
                                    if (files.Any(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_FRONT) 
                                        && files.Any(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_BACK))
                                    {
                                        var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
                                        var front = await ReadImageFromUrl(ServiceURL + files.FirstOrDefault(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_FRONT)?.FilePath);
                                        var back = await ReadImageFromUrl(ServiceURL + files.FirstOrDefault(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_BACK)?.FilePath);
                                        var logPartner = new LogPartner();
                                        var resultEsign = _esignService.prepareCertificateForSignCloud(agreementId, customer.FullName.ToUpper(), idType, idNumber, customer.Address, province.Name, "VN", front, back, customer.Email, customer.Phone, ref logPartner);
                                        if (resultEsign != null && resultEsign.responseCode == 0)
                                        {
                                            logPartner.ResponseCode = resultEsign.responseCode;
                                            logPartner.ResponseContent = JsonConvert.SerializeObject(resultEsign);
                                            logPartner.CustomerId = customer.CustomerId;
                                            _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                            // success 
                                            customer.EsignAgreementId = agreementId;
                                            _unitOfWork.CustomerRepository.Update(customer);
                                            _unitOfWork.SaveChanges();
                                        }
                                        else
                                        {
                                            logPartner.CustomerId = customer.CustomerId;
                                            if (resultEsign != null)
                                            {
                                                logPartner.ResponseCode = resultEsign.responseCode;
                                                logPartner.ResponseContent = JsonConvert.SerializeObject(resultEsign);
                                            }
                                            _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                            _unitOfWork.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có đủ hình ảnh hai mặt cmnd/cccd của KH");
                                        return Ok(def);
                                    }
                                }
                            }
                        }
                        if (!String.IsNullOrEmpty(customer.EsignAgreementId))
                        {
                            // recreate agreement 
                            int count_recreate = 0;
                        create_esign:
                            {
                                // Get fire from url
                                //Local
                                //var sData = _pdfService.ContractFinancialAgreementBase64(req.loanBriefId);
                                // Prod
                                var sData = await _esignService.GetContractPdf(req.loanBriefId, true, 0);
                                if (!String.IsNullOrEmpty(sData))
                                {
                                    var data = Convert.FromBase64String(sData);
                                    var fileName = "HD-" + loanInfo.LoanBriefId + "_borrower_" + Guid.NewGuid().ToString() + ".pdf";
                                    // Lấy địa chỉ thường trú và hộ khẩu
                                    var resident = loanInfo.LoanBriefResident;
                                    int offsetX = 35;
                                    int offsetX1 = 210;
                                    int offsetX2 = 0;
                                    if (resident != null && resident.Address != null && resident.Address.Length >= 100)
                                    {
                                        offsetX -= 15;
                                        offsetX1 -= 20;
                                        offsetX2 -= 20;
                                    }
                                    var household = loanInfo.LoanBriefHousehold;
                                    if (household != null && household.Address != null && household.Address.Length >= 100)
                                    {
                                        offsetX -= 15;
                                        offsetX1 -= 20;
                                        offsetX2 -= 20;
                                    }
                                    // Metadata	
                                    var singletonSigning = new Dictionary<string, string>();
                                    singletonSigning["COUNTERSIGNENABLED"] = "True";
                                    singletonSigning["PAGENO"] = "1";
                                    //singletonSigning["POSITIONIDENTIFIER"] = "CUSTOMERSIG001";
                                    //singletonSigning["RECTANGLEOFFSET"] = "-30,-100";
                                    singletonSigning["RECTANGLESIZE"] = "170,80";
                                    singletonSigning["COORDINATE"] = "340,20,510,100";
                                    singletonSigning["VISIBLESIGNATURE"] = "True";
                                    singletonSigning["SHOWSIGNERINFO"] = "True";
                                    singletonSigning["SIGNERINFOPREFIX"] = "Ký bởi:";
                                    singletonSigning["SHOWDATETIME"] = "True";
                                    singletonSigning["DATETIMEPREFIX"] = "Ký ngày:";
                                    singletonSigning["SHOWREASON"] = "True";
                                    singletonSigning["SIGNREASONPREFIX"] = "Lý do:";
                                    singletonSigning["SIGNREASON"] = "Tôi đồng ý";
                                    singletonSigning["SHOWPERSONALID"] = "True"; // show CMND/CCCD (thong tin trong agreementDetails)
                                    singletonSigning["PERSONALIDPREFIX"] = "CMND/CCCD/HC:";

                                    if (loanInfo.ProductId.HasValue && (loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))
                                    {
                                        if (loanInfo.LoanBriefProperty != null && loanInfo.LoanBriefProperty.PostRegistration != null && loanInfo.LoanBriefProperty.PostRegistration == true)
                                        {
                                            singletonSigning["SHADOWSIGNATUREPROPERTIES"] = "PAGENO=2/COORDINATE=330,35,500,115;PAGENO=3/COORDINATE=370," + (offsetX - 10) + ",540," + (offsetX + 70) + ";PAGENO=4/COORDINATE=380," + offsetX1 + ",550,"
                                                + (offsetX1 + 80) + ";PAGENO=5/COORDINATE=380," + offsetX2 + ",550," + (offsetX2 + 80);
                                            // 5 trang
                                            if (loanInfo.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan)
                                            {
                                                singletonSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=6/COORDINATE=330,330,500,410;PAGENO=7/COORDINATE=440,270,10,350;PAGENO=8/COORDINATE=230,250,400,330;";
                                            }
                                            else
                                            {
                                                if (loanInfo.IsLocate.HasValue && loanInfo.IsLocate.Value)
                                                {
                                                    singletonSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=6/COORDINATE=330,10,500,90;";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            singletonSigning["SHADOWSIGNATUREPROPERTIES"] = "PAGENO=2/COORDINATE=330,35,500,115;PAGENO=3/COORDINATE=370," + (offsetX - 10) + ",540," + (offsetX + 70) + ";PAGENO=4/COORDINATE=380," + offsetX1 + ",550,"
                                                + (offsetX1 + 80);
                                            if (loanInfo.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan)
                                            {
                                                singletonSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=5/COORDINATE=330,330,500,410;PAGENO=6/COORDINATE=440,270,610,350;PAGENO=7/COORDINATE=230,250,400,330;";
                                            }
                                            else
                                            {
                                                if (loanInfo.IsLocate.HasValue && loanInfo.IsLocate.Value)
                                                {
                                                    singletonSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=5/COORDINATE=330,10,500,90;";
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (loanInfo.LoanBriefProperty != null && loanInfo.LoanBriefProperty.PostRegistration != null && loanInfo.LoanBriefProperty.PostRegistration == true)
                                        {
                                            singletonSigning["SHADOWSIGNATUREPROPERTIES"] = "PAGENO=2/COORDINATE=330,35,500,115;PAGENO=3/COORDINATE=370," + (offsetX - 10) + ",540," + (offsetX + 70) + ";";
                                            if (loanInfo.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan)
                                            {
                                                singletonSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=4/COORDINATE=330,330,500,410;PAGENO=5/COORDINATE=440,270,610,350;PAGENO=6/COORDINATE=230,250,400,330;";
                                            }
                                            else
                                            {
                                                if (loanInfo.IsLocate.HasValue && loanInfo.IsLocate.Value)
                                                {
                                                    singletonSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=4/COORDINATE=330,10,500,90;";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            singletonSigning["SHADOWSIGNATUREPROPERTIES"] = "PAGENO=2/COORDINATE=330,35,500,115;";
                                            if (loanInfo.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan)
                                            {
                                                singletonSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=3/COORDINATE=330,330,500,410;PAGENO=4/COORDINATE=440,270,610,350;PAGENO=5/COORDINATE=230,250,400,330;";
                                            }
                                            else
                                            {
                                                if (loanInfo.IsLocate.HasValue && loanInfo.IsLocate.Value)
                                                {
                                                    singletonSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=3/COORDINATE=330,10,500,90;";
                                                }
                                            }
                                        }
                                    }

                                    var counterSigning = new Dictionary<string, string>();
                                    counterSigning["PAGENO"] = "1";
                                    //counterSigning["POSITIONIDENTIFIER"] = "TIMASIGN001";
                                    //counterSigning["RECTANGLEOFFSET"] = "-30,-100";
                                    counterSigning["RECTANGLESIZE"] = "170,80";
                                    counterSigning["COORDINATE"] = "40,20,220,100";
                                    counterSigning["VISIBLESIGNATURE"] = "True";
                                    counterSigning["SHOWSIGNERINFO"] = "True";
                                    counterSigning["SIGNERINFOPREFIX"] = "Ký bởi:";
                                    counterSigning["SHOWDATETIME"] = "True";
                                    counterSigning["DATETIMEPREFIX"] = "Ký ngày:";
                                    counterSigning["SHOWREASON"] = "True";
                                    counterSigning["SIGNREASONPREFIX"] = "Lý do:";
                                    counterSigning["SIGNREASON"] = "Tôi đồng ý";
                                    counterSigning["COUNTERAGREEMENTUUID"] = TIMA_UUID;
                                    counterSigning["COUNTERAGREEMENTPASSCODE"] = TIMA_PASSCODE;

                                    //counterSigning["SHADOWSIGNATUREPROPERTIES"] = "PAGENO=3/COORDINATE=70," + offsetX + ",250," + (offsetX + 70);
                                    if (loanInfo.ProductId.HasValue && (loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))
                                    {
                                        counterSigning["SHADOWSIGNATUREPROPERTIES"] = "PAGENO=2/COORDINATE=40,35,210,115;PAGENO=3/COORDINATE=70," + (offsetX - 10) + ",240," + (offsetX + 70);
                                        if (loanInfo.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan)
                                        {
                                            if (loanInfo.LoanBriefProperty != null && loanInfo.LoanBriefProperty.PostRegistration != null && loanInfo.LoanBriefProperty.PostRegistration == true)
                                            {
                                                counterSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=7/COORDINATE=40,270,210,350;PAGENO=8/COORDINATE=40,250,210,330;";
                                            }
                                            else
                                            {
                                                counterSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=6/COORDINATE=40,270,210,350;PAGENO=7/COORDINATE=40,250,210,330;";
                                            }
                                        }
                                        else
                                        {
                                            if (loanInfo.IsLocate.HasValue && loanInfo.IsLocate.Value)
                                            {
                                                counterSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=5/COORDINATE=40,35,210,115;";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        counterSigning["SHADOWSIGNATUREPROPERTIES"] = "PAGENO=2/COORDINATE=40,35,220,115";
                                        if (loanInfo.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan)
                                        {
                                            if (loanInfo.LoanBriefProperty != null && loanInfo.LoanBriefProperty.PostRegistration != null && loanInfo.LoanBriefProperty.PostRegistration == true)
                                            {
                                                counterSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=5/COORDINATE=40,270,210,350;PAGENO=6/COORDINATE=40,250,210,330;";
                                            }
                                            else
                                            {
                                                counterSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=4/COORDINATE=40,270,210,350;PAGENO=5/COORDINATE=40,250,210,330;";
                                            }
                                        }
                                        else
                                        {
                                            if (loanInfo.IsLocate.HasValue && loanInfo.IsLocate.Value)
                                            {
                                                counterSigning["SHADOWSIGNATUREPROPERTIES"] += ";PAGENO=3/COORDINATE=40,35,210,115;";
                                            }
                                        }
                                    }

                                    var signCloudMetaData = new SignCloudMetaData();
                                    signCloudMetaData.singletonSigning = singletonSigning;
                                    signCloudMetaData.counterSigning = counterSigning;
                                    var logPartner = new LogPartner();
                                    var result = _esignService.prepareFileForSignCloud(customer.EsignAgreementId, signCloudMetaData, fileName, data, null, false, ref logPartner);
                                    if (result != null)
                                    {
                                        var shadowResult = result.Clone();
                                        shadowResult.signedFileData = null;
                                        logPartner.ResponseCode = result.responseCode;
                                        logPartner.ResponseContent = JsonConvert.SerializeObject(shadowResult);
                                        logPartner.CustomerId = customer.CustomerId;
                                        logPartner.LoanBriefId = loanInfo.LoanBriefId;
                                        _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                        _unitOfWork.SaveChanges();
                                        if (result.responseCode == 0 || result.responseCode == 1007)
                                        {
                                            if (result.signedFileData != null)
                                            {
                                                using (var stream = new MemoryStream(result.signedFileData))
                                                {
                                                    // save file to s3
                                                    var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                                                    var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                                                    var BucketName = _baseConfig["AppSettings:BucketName"];
                                                    var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
                                                    {
                                                        RegionEndpoint = RegionEndpoint.APSoutheast1
                                                    });
                                                    var folder = BucketName + "/uploads/ESignContract/" + loanInfo.LoanBriefId + "/";
                                                    var signedFileName = fileName;
                                                    PutObjectRequest putReq = new PutObjectRequest()
                                                    {
                                                        InputStream = stream,
                                                        BucketName = folder,
                                                        Key = signedFileName,
                                                        CannedACL = S3CannedACL.PublicRead
                                                    };
                                                    await client.PutObjectAsync(putReq);
                                                    // Tự send OTP
                                                    if (!string.IsNullOrEmpty(result.authorizeCredential))
                                                    {
                                                        //
                                                        string smsTemplate = _baseConfig["AppSettings:esign_sms_template_tima"].ToString();
                                                        smsTemplate = String.Format(smsTemplate, result.authorizeCredential);
                                                        var resultOTP = _lmsService.SendOtpSmsBrandName((int)TypeOtp.VerifyEsignCustomer, customer.Phone, smsTemplate, result.authorizeCredential, DateTime.Now.AddMinutes(5), GetIpAddressRemote());
                                                        if (resultOTP != null && resultOTP.Result == (int)StatusCallApi.Success)
                                                        {

                                                        }
                                                    }
                                                    // save to db
                                                    //loanInfo.EsignBillCode = result.billCode;
                                                    _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                                                    {
                                                        EsignBillCode = result.billCode
                                                    });
                                                    _unitOfWork.SaveChanges();
                                                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                                    def.data = folder + putReq.Key;
                                                }
                                            }
                                            else
                                            {
                                                // Tự send OTP
                                                if (!string.IsNullOrEmpty(result.authorizeCredential))
                                                {
                                                    string smsTemplate = _baseConfig["AppSettings:esign_sms_template_tima"].ToString();
                                                    smsTemplate = String.Format(smsTemplate, result.authorizeCredential);
                                                    var resultOTP = _lmsService.SendOtpSmsBrandName((int)TypeOtp.VerifyEsignCustomer, customer.Phone, smsTemplate, result.authorizeCredential, DateTime.Now.AddMinutes(5), GetIpAddressRemote());
                                                    if (resultOTP != null && resultOTP.Result == (int)StatusCallApi.Success)
                                                    {

                                                    }
                                                }
                                                else
                                                {
                                                    var oblLogSendOtp = new LogSendOtp()
                                                    {
                                                        Phone = customer.Phone,
                                                        CreatedAt = DateTime.Now,
                                                        TypeOtp = (int)TypeOtp.VerifyEsignCustomer,
                                                        TypeSendOtp = (int)TypeSendOtp.FtpSendOtp,
                                                        IpAddress = GetIpAddressRemote(),
                                                        ExpiredVerifyOtp = DateTime.Now.AddMinutes(5)
                                                    };
                                                    _unitOfWork.LogSendOtpRepository.Insert(oblLogSendOtp);
                                                }
                                                Console.WriteLine($"SignContractForBorrower: LoanbriefId: {loanInfo.LoanBriefId} OTP: {result.authorizeCredential}");
                                                // save to db
                                                //loanInfo.EsignBillCode = result.billCode;
                                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                                                {
                                                    EsignBillCode = result.billCode
                                                });
                                                _unitOfWork.SaveChanges();
                                                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                            }
                                        }
                                        else if (result.responseCode == 1014)
                                        {
                                            if (count_recreate == 0)
                                            {
                                                // Đăng ký
                                                var files = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanInfo.LoanBriefId && x.TypeId == (int)EnumDocumentType.CMND_CCCD
                                                               , null, false).OrderByDescending(x => x.CreateAt).ToList();
                                                if (files != null)
                                                {
                                                    // Đủ ảnh để đăng ký ekyc
                                                    string agreementId = Guid.NewGuid().ToString().ToUpper();
                                                    int idType = !String.IsNullOrEmpty(customer.NationalCard) ? 1 : 2;
                                                    string idNumber = (idType == 1) ? customer.NationalCard : customer.Passport;
                                                    if (idType == 1 && idNumber.Length == 12)
                                                        idType = 3;
                                                    var province = _unitOfWork.ProvinceRepository.GetById(customer.ProvinceId);
                                                    // file 0 > cmnd mặt trước
                                                    if (files.Any(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_FRONT)
                                                        && files.Any(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_BACK))
                                                    {
                                                        var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
                                                        var front = await ReadImageFromUrl(ServiceURL + files.FirstOrDefault(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_FRONT)?.FilePath);
                                                        var back = await ReadImageFromUrl(ServiceURL + files.FirstOrDefault(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_BACK)?.FilePath);
                                                        var logPartnerAgreement = new LogPartner();
                                                        var resultEsign = _esignService.prepareCertificateForSignCloud(agreementId, customer.FullName.ToUpper(), idType, idNumber, customer.Address, province.Name, "VN", front, back, customer.Email, customer.Phone, ref logPartnerAgreement);
                                                        if (resultEsign != null && resultEsign.responseCode == 0)
                                                        {
                                                            logPartnerAgreement.ResponseCode = resultEsign.responseCode;
                                                            logPartnerAgreement.ResponseContent = JsonConvert.SerializeObject(resultEsign);
                                                            logPartnerAgreement.CustomerId = customer.CustomerId;
                                                            _unitOfWork.LogPartnerRepository.Insert(logPartnerAgreement);
                                                            // success                                                             
                                                            customer.EsignAgreementId = agreementId;
                                                            _unitOfWork.CustomerRepository.Update(customer);
                                                            _unitOfWork.SaveChanges();
                                                            goto create_esign;
                                                        }
                                                        else
                                                        {
                                                            logPartnerAgreement.CustomerId = customer.CustomerId;
                                                            if (resultEsign != null)
                                                            {
                                                                logPartnerAgreement.ResponseCode = resultEsign.responseCode;
                                                                logPartnerAgreement.ResponseContent = JsonConvert.SerializeObject(resultEsign);
                                                            }
                                                            _unitOfWork.LogPartnerRepository.Insert(logPartnerAgreement);
                                                            _unitOfWork.SaveChanges();
                                                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đăng ký hợp đồng điện tử của bạn thất bại, vui lòng liên hệ hỗ trợ!");
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                // Thông báo lỗi
                                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Thông tin đăng ký hợp đồng điện tử của bạn đã hết hạn, vui lòng liên hệ hỗ trợ!");
                                            }
                                        }
                                        else
                                        {
                                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ký hợp đồng điện tử cho khách hàng thất bại. Vui lòng thử lại");
                                        }
                                    }
                                    else
                                    {
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                                    }
                                }

                                else
                                {
                                    def.meta = new Meta(241, "contract not found");
                                }
                            }
                        }
                        else
                        {
                            def.meta = new Meta(250, "esign agreement not found");
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    }
                }

                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Esign/CreateAgreementForBorrower Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        async Task<byte[]> ReadImageFromUrl(string url)
        {
            using (var client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(url))
            {
                return await response.Content.ReadAsByteArrayAsync();
            }
        }

        [HttpPost]
        [Route("ConfirmSignContractForBorrower")]
        [AuthorizeTokenOtp]
        public async Task<IActionResult> ConfirmSignContractForBorrower([FromBody] ConfirmSignReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (String.IsNullOrEmpty(req.otp) || req.loanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.loanBriefId, null, false).FirstOrDefault();
                if (loanInfo != null)
                {
                    ////Kiểm tra sdt KH và token KH có khớp nhau không
                    var phoneToken = _identityService.GetPhoneIdentity();
                    if (string.IsNullOrEmpty(phoneToken))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                        return Ok(def);
                    }

                    if (loanInfo.Phone != phoneToken)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                        return Ok(def);
                    }

                    var customer = _unitOfWork.CustomerRepository.Query(x => x.CustomerId == loanInfo.CustomerId, null, false).FirstOrDefault();
                    if (customer != null)
                    {
                        if (!String.IsNullOrEmpty(customer.EsignAgreementId))
                        {
                            var logPartner = new LogPartner();
                            var result = _esignService.authorizeCounterSigningForSignCloud(customer.EsignAgreementId, req.otp, loanInfo.EsignBillCode, ref logPartner);
                            if (result != null)
                            {
                                var shadowResult = result.Clone();
                                shadowResult.signedFileData = null;
                                shadowResult.multipleSignedFileData = null;
                                logPartner.ResponseCode = result.responseCode;
                                logPartner.ResponseContent = JsonConvert.SerializeObject(shadowResult);
                                logPartner.CustomerId = customer.CustomerId;
                                logPartner.LoanBriefId = loanInfo.LoanBriefId;
                                _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                _unitOfWork.SaveChanges();
                                if (result.responseCode == 0 || result.responseCode == 1007)
                                {
                                    if (result.responseCode == 0 && result.signedFileData != null)
                                    {
                                        using (var stream = new MemoryStream(result.signedFileData))
                                        {
                                            // save file to s3
                                            var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                                            var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                                            var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
                                            var BucketName = _baseConfig["AppSettings:BucketName"];
                                            var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
                                            {
                                                RegionEndpoint = RegionEndpoint.APSoutheast1
                                            });
                                            var folder = BucketName + "/uploads/ESignContract/" + loanInfo.LoanBriefId;
                                            var signedFileName = result.signedFileName;
                                            PutObjectRequest putReq = new PutObjectRequest()
                                            {
                                                InputStream = stream,
                                                BucketName = folder,
                                                Key = signedFileName,
                                                CannedACL = S3CannedACL.PublicRead
                                            };
                                            await client.PutObjectAsync(putReq);
                                            // save to db
                                            loanInfo.EsignState = (int)EsignState.BORROWER_SIGNED;
                                            loanInfo.EsignBillCode = null;
                                            var link = ServiceURL + "/uploads/ESignContract/" + loanInfo.LoanBriefId + "/" + putReq.Key;
                                            loanInfo.EsignBorrowerContract = link;
                                            _unitOfWork.LoanBriefRepository.Update(loanInfo);
                                            // Deactive các hợp đồng khác
                                            _unitOfWork.EsignContractRepository.Update(x => x.LoanBriefId == req.loanBriefId && x.TypeEsign == 1, x => new EsignContract()
                                            {
                                                Status = 0
                                            });
                                            // tạo record mới
                                            var esignContract = new EsignContract();
                                            esignContract.LoanBriefId = loanInfo.LoanBriefId;
                                            esignContract.Status = 1;
                                            esignContract.TypeEsign = 1; // 1 Người vay
                                            esignContract.Url = link;
                                            esignContract.CreatedAt = DateTime.Now;
                                            esignContract.AgreementId = customer.EsignAgreementId;
                                            esignContract.BillCode = result.billCode;
                                            _unitOfWork.EsignContractRepository.Insert(esignContract);

                                            var note = new LoanBriefNote
                                            {
                                                LoanBriefId = req.loanBriefId,
                                                Note = "Ký hợp đồng điện tử thành công cho Khách hàng",
                                                FullName = "System",
                                                Status = 1,
                                                ActionComment = (int)EnumActionComment.CommentLoanBrief,
                                                CreatedTime = DateTime.Now,
                                                UserId = 0,
                                                ShopId = 0,
                                                ShopName = "System"
                                            };
                                            _unitOfWork.LoanBriefNoteRepository.Insert(note);

                                            _unitOfWork.SaveChanges();
                                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                            def.data = folder + "/" + putReq.Key;
                                        }
                                    }
                                    else
                                    {
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                                    }
                                }
                                else if (result.responseCode == 1004)
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "OTP không đúng vui lòng nhập lại");
                                }
                                else if (result.responseCode == 1005)
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Xác thực bị hạn chế, vui lòng thực liên hệ hỗ trợ");
                                }
                                else if (result.responseCode == 1006)
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "OTP đã hết hạn");
                                }
                                else
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Xác thực thất bại, vui lòng thử lại sau!");
                                }
                            }
                            else
                            {
                                logPartner.CustomerId = customer.CustomerId;
                                _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                _unitOfWork.SaveChanges();
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Xác thực thất bại, vui lòng thử lại sau!");
                            }
                        }
                        else
                        {
                            def.meta = new Meta(250, "esign agreement not found");
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Esign/CreateAgreementForBorrower Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("SignContractForLender")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<IActionResult> SignContractForLender([FromBody] SignContractLenderReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.loanBriefId <= 0 || req.lenderId <= 0 || String.IsNullOrEmpty(req.agreementId) || String.IsNullOrEmpty(req.passcode))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // Kiểm tra nđt đã ký đơn vay này chau
                var time = DateTime.Now.AddMinutes(-60); // Thời gian ký lại hợp đồng mới nếu gọi nhiều lần
                var contract = _unitOfWork.EsignContractRepository.Query(x => x.LoanBriefId == req.loanBriefId && x.LenderId == req.lenderId && x.TypeEsign == 2 && x.CreatedAt >= time, null, false).OrderByDescending(x => x.CreatedAt).FirstOrDefault();
                if (contract != null)
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = contract.Url;
                    return Ok(def);
                }
                var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.loanBriefId, null, false).FirstOrDefault();
                if (loanInfo != null)
                {
                    //if (loanInfo.LenderId != req.lenderId)
                    //{
                    //    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Sai thông tin lender");
                    //    return Ok(def);
                    //}
                    //if (loanInfo.Status != (int)EnumLoanStatus.WAIT_DISBURSE && loanInfo.Status != (int)EnumLoanStatus.DISBURSED)
                    //{
                    //    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Sai thông tin trạng thái khoản vay");
                    //    return Ok(def);
                    //}
                    //if (loanInfo.EsignState != (int)EsignState.BORROWER_SIGNED)
                    //{
                    //    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng chưa ký hợp đồng điện tử, vui lòng kiểm tra lại!");
                    //    return Ok(def);
                    //}
                    // Prod
                    var sData = await _esignService.GetContractPdf(req.loanBriefId, false, req.lenderId);
                    // Localhost
                    //var sData = _pdfService.ContractFinancialAgreementLenderBase64(req.loanBriefId);
                    if (!String.IsNullOrEmpty(sData))
                    {
                        var data = Convert.FromBase64String(sData);
                        var fileName = "HD-" + loanInfo.LoanBriefId + "_lender_" + Guid.NewGuid().ToString() + ".pdf";
                        // Lấy địa chỉ thường trú và hộ khẩu của nhà đt
                        var lenderInfo = _lmsService.GetInfoLender(req.lenderId);
                        if (lenderInfo != null && lenderInfo.Status == 1)
                        {
                            int offsetX = 50;
                            int offsetX1 = 40;
                            if (lenderInfo.Data.Address != null && lenderInfo.Data.Address.Length >= 100)
                                offsetX -= 15;
                            if (lenderInfo.Data.AddressOfResidence != null && lenderInfo.Data.AddressOfResidence.Length >= 100)
                                offsetX -= 15;

                            var singletonSigning = new Dictionary<string, string>();
                            singletonSigning["PAGENO"] = "1";
                            singletonSigning["COUNTERSIGNENABLED"] = "True";
                            //singletonSigning["POSITIONIDENTIFIER"] = "LENDERSIG001";
                            //singletonSigning["RECTANGLEOFFSET"] = "-80,-100";
                            //singletonSigning["RECTANGLESIZE"] = "250,100";
                            singletonSigning["COORDINATE"] = "320," + offsetX + ",500," + (offsetX + 70);
                            singletonSigning["VISIBLESIGNATURE"] = "True";
                            singletonSigning["SHOWSIGNERINFO"] = "True";
                            singletonSigning["SIGNERINFOPREFIX"] = "Ký bởi:";
                            singletonSigning["SHOWDATETIME"] = "True";
                            singletonSigning["DATETIMEPREFIX"] = "Ký ngày:";
                            singletonSigning["SHOWREASON"] = "True";
                            singletonSigning["SIGNREASONPREFIX"] = "Lý do:";
                            singletonSigning["SIGNREASON"] = "Tôi đã đọc và đồng ý";
                            singletonSigning["SHOWPERSONALID"] = "True"; // show CMND/CCCD (thong tin trong agreementDetails)
                            singletonSigning["PERSONALIDPREFIX"] = "CMND/CCCD/HC:";
                            //singletonSigning["FONTSIZE"] = "11"; // font size

                            singletonSigning["SHADOWSIGNATUREPROPERTIES"] = "PAGENO=2/COORDINATE=330," + offsetX1 + ",510," + (offsetX1 + 70);

                            var counterSigning = new Dictionary<string, string>();
                            counterSigning["PAGENO"] = "1";
                            //counterSigning["POSITIONIDENTIFIER"] = "TIMASIGN001";
                            //counterSigning["RECTANGLEOFFSET"] = "-60,-70";
                            //counterSigning["RECTANGLESIZE"] = "170,70";
                            counterSigning["VISIBLESIGNATURE"] = "True";
                            counterSigning["COORDINATE"] = "40," + offsetX + ",220," + (offsetX + 60);
                            counterSigning["SHOWSIGNERINFO"] = "True";
                            counterSigning["SIGNERINFOPREFIX"] = "Ký bởi:";
                            counterSigning["SHOWDATETIME"] = "True";
                            counterSigning["DATETIMEPREFIX"] = "Ký ngày:";
                            counterSigning["SHOWREASON"] = "True";
                            counterSigning["SIGNREASONPREFIX"] = "Lý do:";
                            counterSigning["SIGNREASON"] = "Tôi đồng ý";
                            counterSigning["COUNTERAGREEMENTUUID"] = TIMA_UUID;
                            counterSigning["COUNTERAGREEMENTPASSCODE"] = TIMA_PASSCODE;
                            //counterSigning["FONTSIZE"] = "11"; // font size

                            counterSigning["SHADOWSIGNATUREPROPERTIES"] = "PAGENO=2/COORDINATE=50," + offsetX1 + ",230," + (offsetX1 + 70);

                            var signCloudMetaData = new SignCloudMetaData();
                            signCloudMetaData.singletonSigning = singletonSigning;
                            signCloudMetaData.counterSigning = counterSigning;
                            var logPartner = new LogPartner();
                            var result = _esignService.prepareFileForSignCloud(req.agreementId, signCloudMetaData, fileName, data, req.passcode, true, ref logPartner);
                            if (result != null)
                            {
                                var shadowResult = result.Clone();
                                shadowResult.signedFileData = null;
                                shadowResult.multipleSignedFileData = null;
                                logPartner.ResponseCode = result.responseCode;
                                logPartner.ResponseContent = JsonConvert.SerializeObject(shadowResult);
                                logPartner.LenderId = req.lenderId;
                                logPartner.LoanBriefId = loanInfo.LoanBriefId;
                                _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                _unitOfWork.SaveChanges();
                                if (result.responseCode == 0 || result.responseCode == 1007 || result.responseCode == 1018)
                                {
                                    if (result.signedFileData != null && result.signedFileData.Length > 0)
                                    {
                                        // Ký thành công
                                        using (var stream = new MemoryStream(result.signedFileData))
                                        {
                                            // save file to s3
                                            var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                                            var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                                            var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
                                            var BucketName = _baseConfig["AppSettings:BucketName"];
                                            var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
                                            {
                                                RegionEndpoint = RegionEndpoint.APSoutheast1
                                            });
                                            var folder = BucketName + "/uploads/ESignContract/" + loanInfo.LoanBriefId;
                                            var signedFileName = result.signedFileName;
                                            PutObjectRequest putReq = new PutObjectRequest()
                                            {
                                                InputStream = stream,
                                                BucketName = folder,
                                                Key = signedFileName,
                                                CannedACL = S3CannedACL.PublicRead
                                            };
                                            await client.PutObjectAsync(putReq);
                                            // save to db							
                                            var link = ServiceURL + "/uploads/ESignContract/" + loanInfo.LoanBriefId + "/" + putReq.Key;
                                            loanInfo.EsignLenderContract = link;
                                            loanInfo.EsignState = (int)EsignState.LENDER_SIGNED;
                                            _unitOfWork.LoanBriefRepository.Update(loanInfo);
                                            // Deactive các hợp đồng khác
                                            _unitOfWork.EsignContractRepository.Update(x => x.LoanBriefId == req.loanBriefId && x.TypeEsign == 2, x => new EsignContract()
                                            {
                                                Status = 0
                                            });
                                            // tạo record mới
                                            var esignContract = new EsignContract();
                                            esignContract.LoanBriefId = loanInfo.LoanBriefId;
                                            esignContract.Status = 1;
                                            esignContract.TypeEsign = 2; // 1 Người vay
                                            esignContract.Url = link;
                                            esignContract.CreatedAt = DateTime.Now;
                                            esignContract.AgreementId = req.agreementId;
                                            esignContract.BillCode = result.billCode;
                                            esignContract.LenderId = req.lenderId;
                                            _unitOfWork.EsignContractRepository.Insert(esignContract);
                                            _unitOfWork.SaveChanges();
                                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                            def.data = folder + "/" + putReq.Key;
                                        }
                                    }
                                    else if (result.multipleSignedFileData != null)
                                    {
                                        if (result.multipleSignedFileData.Count > 0)
                                        {
                                            int index = 0;
                                            foreach (var file in result.multipleSignedFileData)
                                            {
                                                if (file.signedFileData != null && file.signedFileData.Length > 0)
                                                {
                                                    using (var stream = new MemoryStream(file.signedFileData))
                                                    {
                                                        // save file to s3
                                                        var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                                                        var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                                                        var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
                                                        var BucketName = _baseConfig["AppSettings:BucketName"];
                                                        var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
                                                        {
                                                            RegionEndpoint = RegionEndpoint.APSoutheast1
                                                        });
                                                        var folder = BucketName + "/uploads/ESignContract/" + loanInfo.LoanBriefId;
                                                        var signedFileName = file.signedFileName;
                                                        PutObjectRequest putReq = new PutObjectRequest()
                                                        {
                                                            InputStream = stream,
                                                            BucketName = folder,
                                                            Key = signedFileName,
                                                            CannedACL = S3CannedACL.PublicRead
                                                        };
                                                        await client.PutObjectAsync(putReq);
                                                        // save to db							
                                                        var link = ServiceURL + "/uploads/ESignContract/" + loanInfo.LoanBriefId + "/" + putReq.Key;
                                                        if (index == 0)
                                                        {
                                                            loanInfo.EsignLenderContract = link;
                                                            loanInfo.EsignState = (int)EsignState.LENDER_SIGNED;
                                                            _unitOfWork.LoanBriefRepository.Update(loanInfo);
                                                            // Deactive các hợp đồng khác													
                                                            _unitOfWork.EsignContractRepository.Update(x => x.LoanBriefId == req.loanBriefId && x.TypeEsign == 2, x => new EsignContract()
                                                            {
                                                                Status = 0
                                                            });
                                                        }
                                                        // tạo record mới
                                                        var esignContract = new EsignContract();
                                                        esignContract.LoanBriefId = loanInfo.LoanBriefId;
                                                        esignContract.Status = 1;
                                                        esignContract.TypeEsign = 2; // 1 Người vay
                                                        esignContract.Url = link;
                                                        esignContract.CreatedAt = DateTime.Now;
                                                        esignContract.AgreementId = req.agreementId;
                                                        esignContract.BillCode = result.billCode;
                                                        esignContract.LenderId = req.lenderId;
                                                        _unitOfWork.EsignContractRepository.Insert(esignContract);
                                                        _unitOfWork.SaveChanges();
                                                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                                        def.data = folder + "/" + putReq.Key;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var note = new LoanBriefNote
                                        {
                                            LoanBriefId = req.loanBriefId,
                                            Note = "Ký hợp đồng điện tử thành công nhưng không có file hợp đồng trả về",
                                            FullName = "System",
                                            Status = 1,
                                            ActionComment = 199,
                                            CreatedTime = DateTime.Now,
                                            UserId = 0,
                                            ShopId = 0,
                                            ShopName = "APP"
                                        };
                                        _unitOfWork.LoanBriefNoteRepository.Insert(note);
                                        _unitOfWork.SaveChanges();
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ký hợp đồng điện tử thành công nhưng không có file hợp đồng trả về");
                                    }
                                }
                                else if (result.responseCode == 1004)
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "OTP không đúng, vui lòng nhập lại");
                                }
                                else if (result.responseCode == 1005)
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Xác thực bị hạn chế, vui lòng thực liên hệ hỗ trợ");
                                }
                                else if (result.responseCode == 1006)
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "OTP đã hết hạn");
                                }
                                else
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ký hợp đồng thất bại, vui lòng liên hệ hỗ trợ");
                                }
                            }
                            else
                            {
                                logPartner.LenderId = req.lenderId;
                                _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                _unitOfWork.SaveChanges();
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ký hợp đồng thất bại, vui lòng liên hệ hỗ trợ");
                            }
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ký hợp đồng thất bại, vui lòng liên hệ hỗ trợ");
                        }
                    }
                    else
                    {
                        def.meta = new Meta(251, "Không tìm thấy thông tin hợp đồng, vui lòng liên hệ hỗ trợ");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Esign/SignContractForLender Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("SendOTP")]
        [AuthorizeTokenOtp]
        public async Task<IActionResult> SendOTP([FromBody] ResendOtpReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.customerId <= 0 && String.IsNullOrEmpty(req.agreementId))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (req.customerId > 0)
                {
                    var customer = await _unitOfWork.CustomerRepository.Query(x => x.CustomerId == req.customerId, null, false).FirstOrDefaultAsync();
                    if (customer != null)
                    {
                        if (!String.IsNullOrEmpty(customer.EsignAgreementId))
                        {
                            var logPartner = new LogPartner();
                            var result = _esignService.regenerateAuthorizationCodeForSignCloud(customer.EsignAgreementId, ref logPartner);
                            if (result != null)
                            {
                                logPartner.ResponseCode = result.responseCode;
                                logPartner.ResponseContent = JsonConvert.SerializeObject(result);
                                logPartner.CustomerId = customer.CustomerId;
                                _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                _unitOfWork.SaveChanges();
                                if (result.responseCode == 0)
                                {
                                    if (!string.IsNullOrEmpty(result.authorizeCredential))
                                    {
                                        // Tự send OTP
                                        string smsTemplate = _baseConfig["AppSettings:esign_sms_template_tima"].ToString();
                                        smsTemplate = String.Format(smsTemplate, result.authorizeCredential);
                                        var resultOTP = _lmsService.SendOtpSmsBrandName((int)TypeOtp.VerifyEsignCustomer, customer.Phone, smsTemplate, result.authorizeCredential, DateTime.Now.AddMinutes(5), GetIpAddressRemote());
                                        if (resultOTP != null && resultOTP.Result == (int)StatusCallApi.Success)
                                        {
                                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                        }
                                        else
                                        {
                                            def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                                        }
                                    }
                                    else
                                    {
                                        var oblLogSendOtp = new LogSendOtp()
                                        {
                                            Phone = customer.Phone,
                                            CreatedAt = DateTime.Now,
                                            TypeOtp = (int)TypeOtp.VerifyEsignCustomer,
                                            TypeSendOtp = (int)TypeSendOtp.FtpSendOtp,
                                            IpAddress = GetIpAddressRemote(),
                                            ExpiredVerifyOtp = DateTime.Now.AddMinutes(5)
                                        };
                                        _unitOfWork.LogSendOtpRepository.Insert(oblLogSendOtp);
                                        _unitOfWork.SaveChanges();
                                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                    }
                                }
                                else
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                                }
                            }
                            else
                            {
                                logPartner.CustomerId = customer.CustomerId;
                                _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                _unitOfWork.SaveChanges();
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                            }
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Esign/CreateAgreementForBorrower Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("LenderEsignTest")]
        [AuthorizeToken("APP_API_LENDER")]
        public async Task<IActionResult> LenderEsignTest([FromBody] SignContractLenderReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (String.IsNullOrEmpty(req.agreementId) || String.IsNullOrEmpty(req.passcode))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
                if (string.IsNullOrWhiteSpace(_environment.WebRootPath))
                {
                    _environment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "");
                }
                string filePath = Path.Combine(_environment.WebRootPath, "uploads\\HD-Test.pdf");
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                    filePath = Path.Combine(_environment.WebRootPath, "uploads/HD-Test.pdf");
                if (!String.IsNullOrEmpty(filePath))
                {
                    var data = System.IO.File.ReadAllBytes(filePath);
                    var fileName = req.lenderId + "_HD_Test_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".pdf";
                    // Metadata					
                    var singletonSigning = new Dictionary<string, string>();
                    singletonSigning["PAGENO"] = "First";
                    singletonSigning["COUNTERSIGNENABLED"] = "True";
                    singletonSigning["POSITIONIDENTIFIER"] = "LENDERSIG001";
                    singletonSigning["RECTANGLEOFFSET"] = "-80,-100";
                    singletonSigning["RECTANGLESIZE"] = "250,100";
                    singletonSigning["VISIBLESIGNATURE"] = "True";
                    singletonSigning["SHOWSIGNERINFO"] = "True";
                    singletonSigning["SIGNERINFOPREFIX"] = "Ký bởi:";
                    singletonSigning["SHOWDATETIME"] = "True";
                    singletonSigning["DATETIMEPREFIX"] = "Ký ngày:";
                    singletonSigning["SHOWREASON"] = "True";
                    singletonSigning["SIGNREASONPREFIX"] = "Lý do:";
                    singletonSigning["SIGNREASON"] = "Tôi đã đọc và đồng ý";
                    singletonSigning["SHOWPERSONALID"] = "True"; // show CMND/CCCD (thong tin trong agreementDetails)
                    singletonSigning["PERSONALIDPREFIX"] = "CMND/CCCD:";
                    singletonSigning["FONTSIZE"] = "11"; // font size

                    var counterSigning = new Dictionary<string, string>();
                    counterSigning["PAGENO"] = "First";
                    counterSigning["POSITIONIDENTIFIER"] = "TIMASIGN001";
                    counterSigning["RECTANGLEOFFSET"] = "-60,-70";
                    counterSigning["RECTANGLESIZE"] = "170,70";
                    counterSigning["VISIBLESIGNATURE"] = "True";
                    counterSigning["SHOWSIGNERINFO"] = "True";
                    counterSigning["SIGNERINFOPREFIX"] = "Ký bởi:";
                    counterSigning["SHOWDATETIME"] = "True";
                    counterSigning["DATETIMEPREFIX"] = "Ký ngày:";
                    counterSigning["SHOWREASON"] = "True";
                    counterSigning["SIGNREASONPREFIX"] = "Lý do:";
                    counterSigning["SIGNREASON"] = "Tôi đồng ý";
                    counterSigning["COUNTERAGREEMENTUUID"] = TIMA_UUID;
                    counterSigning["COUNTERAGREEMENTPASSCODE"] = TIMA_PASSCODE;
                    counterSigning["FONTSIZE"] = "11"; // font size

                    var signCloudMetaData = new SignCloudMetaData();
                    signCloudMetaData.singletonSigning = singletonSigning;
                    signCloudMetaData.counterSigning = counterSigning;
                    var logPartner = new LogPartner();
                    var result = _esignService.prepareFileForSignCloud(req.agreementId, signCloudMetaData, fileName, data, req.passcode, true, ref logPartner);
                    if (result != null)
                    {
                        var shadowResult = result.Clone();
                        shadowResult.signedFileData = null;
                        shadowResult.multipleSignedFileData = null;
                        logPartner.TransactionType = "PREPARE_LENDER_CONTRACT_TEST";
                        logPartner.ResponseCode = result.responseCode;
                        logPartner.ResponseContent = JsonConvert.SerializeObject(shadowResult);
                        logPartner.LenderId = req.lenderId;
                        _unitOfWork.LogPartnerRepository.Insert(logPartner);
                        _unitOfWork.SaveChanges();
                        if (result.responseCode == 0 || result.responseCode == 1007 || result.responseCode == 1018)
                        {
                            if (result.signedFileData != null)
                            {
                                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            }
                            else
                            {
                                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            }
                        }
                        else if (result.responseCode == 1004)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "OTP không đúng vui lòng nhập lại");
                        }
                        else if (result.responseCode == 1005)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Xác thực bị hạn chế, vui lòng thực liên hệ hỗ trợ");
                        }
                        else if (result.responseCode == 1006)
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "OTP đã hết hạn");
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                        }
                    }
                    else
                    {
                        logPartner.TransactionType = "PREPARE_LENDER_CONTRACT_TEST";
                        logPartner.LenderId = req.lenderId;
                        _unitOfWork.LogPartnerRepository.Insert(logPartner);
                        _unitOfWork.SaveChanges();
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ký hợp đồng thất bại, vui lòng liên hệ hỗ trợ");
                    }
                }
                else
                {
                    def.meta = new Meta(251, "contract not found");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Esign/CreateAgreementForBorrower Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("CreateAgreementForBorrowerTest")]
        [HttpPost]
        public async Task<IActionResult> CreateAgreementForBorrowerTest(List<IFormFile> files)
        {
            var def = new DefaultResponse<object>();
            try
            {
                // Kiểm tra file cmnd
                bool fileValidated = true;
                if (files != null && files.Count > 0 && files.Count <= 2)
                {
                    foreach (var file in files)
                    {
                        var extension = Path.GetExtension(file.FileName);
                        var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                        if (!allowedExtensions.Contains(extension.ToLower()))
                        {
                            fileValidated = false;
                            break;
                        }
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (!fileValidated)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var front = await files[0].GetBytes();
                var back = await files[1].GetBytes();
                string agreementId = Guid.NewGuid().ToString().ToUpper();
                var logPartner = new LogPartner();
                var result = _esignService.prepareCertificateForSignCloud(agreementId, "ĐOÀN THANH HÀ", 1, "00000700000", "Nguyễn Chí Thanh", "Hà Nội", "VN", front, back, "hadt@tima.vn", "0915930887", ref logPartner);
                // Save log 
                _unitOfWork.LogCallApiRepository.Insert(new LogCallApi
                {
                    ActionCallApi = ActionCallApi.CreateAgreement.GetHashCode(),
                    LinkCallApi = "EsignCreateAgreement",
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.Success,
                    Input = JsonConvert.SerializeObject(result),
                    CreatedAt = DateTime.Now
                });
                if (result != null && result.responseCode == 0)
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = agreementId;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Esign/CreateAgreementForBorrower Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        private string GetIpAddressRemote()
        {
            try
            {
                StringValues ipRequestHeader;
                var authIpRequest = HttpContext.Request.Headers.TryGetValue("X-Forwarded-For", out ipRequestHeader);
                if (authIpRequest)
                {
                    var remoteIp = GetIpRequest(ipRequestHeader);
                    if (!string.IsNullOrEmpty(remoteIp))
                    {
                        System.Net.IPAddress ipAddress = null;
                        bool isValidIp = System.Net.IPAddress.TryParse(remoteIp, out ipAddress);
                        if (isValidIp)
                            return ipAddress.ToString();

                    }
                }
            }
            catch 
            {
            }
            return string.Empty;
        }

        private string GetIpRequest(string ipRequestHeader)
        {
            if (!string.IsNullOrEmpty(ipRequestHeader))
            {
                if (ipRequestHeader.Contains(","))
                    return ipRequestHeader.Split(',').First().Trim();
                else return ipRequestHeader.Trim();
            }
            return string.Empty;
        }
    }
}
