﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Service;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Processing;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class FileController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _baseConfig;
        private readonly IIdentityService _identityService;
        public FileController(IUnitOfWork unitOfWork, IConfiguration configuration, IIdentityService identityService)
        {
            this._unitOfWork = unitOfWork;
            this._baseConfig = configuration;
            this._identityService = identityService;
        }

        [Route("upload_contract_scan")]
        [AuthorizeToken("APP_API_AI")]
        [HttpPost]
        public async Task<IActionResult> UploadContractScan(IFormFile fileScan, int LoanBriefId, bool? scanContractStatus)
        {
            var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
            var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
            var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
            var BucketName = _baseConfig["AppSettings:BucketName"];
            var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
            {
                RegionEndpoint = RegionEndpoint.APSoutheast1
            });

            var folder = BucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
            var lst = new List<LoanBriefFiles>();
            var def = new DefaultResponse<Meta, List<LoanBriefFiles>>();
            if (fileScan == null || LoanBriefId <= 0)
            {
                def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                return Ok(def);
            }
            var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId, null, false).Select(x => new
            {
                LoanbriefId = x.LoanBriefId
            }).FirstOrDefault();
            if (loanInfo != null)
            {
                try
                {
                    var link = "";
                    var linkThumb = "";
                    var extension = Path.GetExtension(fileScan.FileName);
                    var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                    if (allowedExtensions.Contains(extension.ToLower()))
                    {
                        string ImageName = Guid.NewGuid().ToString() + extension;
                        string ImageThumb = Guid.NewGuid().ToString() + "-thumb" + extension;

                        using (var stream = new MemoryStream())
                        {
                            fileScan.CopyTo(stream);
                            PutObjectRequest req = new PutObjectRequest()
                            {
                                InputStream = stream,
                                BucketName = folder,
                                Key = ImageName,
                                CannedACL = S3CannedACL.PublicRead
                            };
                            await client.PutObjectAsync(req);
                            //Crop image
                            using var image = Image.Load(fileScan.OpenReadStream());
                            if (image.Width > 500)
                            {
                                image.Mutate(x => x.Resize(200, image.Height / (image.Width / 200)));

                                var outputStream = new MemoryStream();
                                image.Save(outputStream, new JpegEncoder());
                                outputStream.Seek(0, 0);
                                //Upload Thumb
                                PutObjectRequest reqThumb = new PutObjectRequest()
                                {
                                    InputStream = outputStream,
                                    BucketName = folder,
                                    Key = ImageThumb,
                                    CannedACL = S3CannedACL.PublicRead
                                };
                                await client.PutObjectAsync(reqThumb);
                                linkThumb = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + ImageThumb;
                                //return Ok();
                            }
                            //End
                            link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                            //insert to db
                            var file = new LoanBriefFiles
                            {
                                LoanBriefId = LoanBriefId,
                                CreateAt = DateTime.Now,
                                FilePath = link,
                                UserId = 0,
                                Status = 1,
                                TypeId = (int)EnumDocumentType.ScanContract,
                                S3status = 1,
                                MecashId = 1000000000,
                                SourceUpload = (int)EnumSourceUpload.Mobile,
                                FileThumb = linkThumb
                            };
                            _unitOfWork.LoanBriefFileRepository.Insert(file);
                            if (scanContractStatus.HasValue)
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                                {
                                    ScanContractStatus = scanContractStatus
                                });
                            }
                            _unitOfWork.Save();
                            //obj.FilePath = ServiceURL + obj.FilePath;
                            lst.Add(file);
                        }

                        //cập nhật trạng thái scan h
                    }
                    else
                    {
                        def.meta = new Meta(211, "Ảnh không đúng định dạng!");
                        return Ok(def);
                    }

                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = lst.ToList();
                    if (def.data != null && def.data.Count > 0)
                    {
                        foreach (var item in def.data)
                        {
                            item.FilePath = ServiceURL + item.FilePath;
                            item.FileThumb = ServiceURL + item.FileThumb;
                        }

                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "File/UploadContractScan Exception");
                    def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                }

            }
            else
            {
                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                return Ok(def);
            }

            return Ok(def);
        }

        [HttpPost]
        [Route("upload_image")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, string>>> UploadImage(IFormFile images, [FromForm] UploadImageReq entity)
        {
            var def = new DefaultResponse<Meta, string>();
            try
            {
                if (images == null || entity.TypeDocumentId == 0 || entity.LoanbriefId == 0 || entity.UploadFrom == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if(!Enum.IsDefined(typeof(EnumSourceUpload), entity.UploadFrom))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Không đúng nguồn upload");
                    return Ok(def);
                }

                if (entity.TypeDocumentId != (int)EnumDocumentType.CMND_CCCD && entity.TypeDocumentId != (int)EnumDocumentType.Selfie)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền đúng loại chứng từ");
                    return Ok(def);
                }

                if (entity.TypeDocumentId == (int)EnumDocumentType.CMND_CCCD)
                {
                    if(entity.SubTypeId.GetValueOrDefault(0) == 0)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền đúng SubTypeId của cmnd/cccd");
                        return Ok(def);

                    }
                   
                }

                //kiểm tra xem có đũng mã loanbrief không
                var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(x => new
                {
                    LoanbriefId = x.LoanBriefId,
                    Status = x.Status,
                    Phone = x.Phone

                }).FirstOrDefaultAsync();

                if (loanbrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                    return Ok(def);
                }

                if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode()
                    || loanbrief.Status == EnumLoanStatus.FINISH.GetHashCode()
                    || loanbrief.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đã đẩy đi không Upload được");
                    return Ok(def);
                }

                var phoneToken = _identityService.GetPhoneIdentity();
                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                    return Ok(def);
                }

                if (loanbrief.Phone != phoneToken)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                    return Ok(def);
                }

                var accessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                var recsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                var serviceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
                var bucketName = _baseConfig["AppSettings:BucketName"];
                var client = new AmazonS3Client(accessKeyS3, recsetAccessKeyS3, new AmazonS3Config()
                {
                    RegionEndpoint = RegionEndpoint.APSoutheast1
                });
                var folder = bucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
                var extension = Path.GetExtension(images.FileName);
                var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                if (allowedExtensions.Contains(extension.ToLower()))
                {
                    string ImageName = Guid.NewGuid().ToString() + extension;
                    using (var stream = new MemoryStream())
                    {
                        images.CopyTo(stream);
                        PutObjectRequest req = new PutObjectRequest()
                        {
                            InputStream = stream,
                            BucketName = folder,
                            Key = ImageName,
                            CannedACL = S3CannedACL.PublicRead
                        };
                        await client.PutObjectAsync(req);
                        var link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                        //insert to db
                        var file = new LoanBriefFiles
                        {
                            LoanBriefId = entity.LoanbriefId,
                            CreateAt = DateTime.Now,
                            FilePath = link,
                            UserId = 0,
                            Status = 1,
                            TypeId = entity.TypeDocumentId,
                            S3status = 1,
                            MecashId = 1000000000,
                            SubTypeId = entity.SubTypeId,
                            SourceUpload = entity.UploadFrom
                        };
                        _unitOfWork.LoanBriefFileRepository.Insert(file);
                        _unitOfWork.Save();

                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = serviceURL + file.FilePath;
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ảnh không đúng định dạng!");
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "File/UploadImage Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
