﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models.AG;
using LOS.AppAPI.Models.Finance;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.AG;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class FinanceController : BaseController
    {
        public static ConcurrentDictionary<object, SemaphoreSlim> locks = new ConcurrentDictionary<object, SemaphoreSlim>();
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAgService _agservice;
        private readonly IMemoryCache _memoryCache;
        private readonly IFinaceService _finaceService;
        private readonly IErp _erpService;
        public FinanceController(IUnitOfWork unitOfWork, IAgService agservice,
            IMemoryCache memoryCache, IFinaceService finaceService, IErp erpService)
        {
            this._unitOfWork = unitOfWork;
            this._agservice = agservice;
            _memoryCache = memoryCache;
            _finaceService = finaceService;
            _erpService = erpService;
        }
        [HttpPost]
        [Route("CreateLoan")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> CreateLoan(Finance.CreateLoan req)
        {
            var obj = new LoanBrief();
            var objCus = new Customer();
            var def = new DefaultResponse<object>();

            try
            {
                var checkErp = _erpService.CheckEmployeeTima(req.NationalCard, req.Phone);
                if (checkErp != null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng là nhân viên Tima!");
                    return Ok(def);
                }
                if (_unitOfWork.LoanBriefRepository.Any(x => x.Phone == req.Phone 
                    && x.Status != (int)EnumLoanStatus.CANCELED
                    && x.Status != (int)EnumLoanStatus.FINISH))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý!");
                    return Ok(def);
                }
                //xử lý tên KH
                if (!string.IsNullOrEmpty(req.FullName))
                {
                    req.FullName = req.FullName.ReduceWhitespace();
                    req.FullName = req.FullName.TitleCaseString();
                }
                //Check xem khách hàng có trong black list không
                var entityBlackList = new RequestCheckBlackListAg
                {
                    FullName = req.FullName,
                    NumberCard = req.NationalCard,
                    Phone = req.Phone
                };
                var checkBlackList = _agservice.CheckBlacklistAg(entityBlackList);
                if (checkBlackList.Data == true && checkBlackList.Status == 1)
                {
                    def.meta = new Meta(ResponseHelper.CUSTOMER_BLACKLIST_CODE, "Khách hàng nằm trong Black list!");
                    return Ok(def);
                }

                //Check xem customer đã tồn tại hay chưa
                var customerInfo = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == req.Phone);
                if (customerInfo != null && customerInfo.CustomerId > 0)
                    obj.CustomerId = customerInfo.CustomerId;
                else
                {
                    objCus.CreatedAt = DateTime.Now;
                    objCus.FullName = req.FullName;
                    objCus.Phone = req.Phone;
                    objCus.ProvinceId = req.ProvinceId;
                    objCus.DistrictId = req.DistrictId;
                    objCus.NationalCard = req.NationalCard;
                    _unitOfWork.CustomerRepository.Insert(objCus);
                    _unitOfWork.Save();
                    obj.CustomerId = objCus.CustomerId;
                }
                if (req.ProvinceId > 0 || req.DistrictId > 0)
                {
                    obj.LoanBriefResident = new LoanBriefResident()
                    {
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId
                    };
                }

                if (req.UtmSource != null && req.UtmSource.Contains("google"))
                {
                    obj.PlatformType = EnumPlatformType.Web.GetHashCode();

                }
                else
                {
                    obj.PlatformType = EnumPlatformType.BuyOfSan.GetHashCode();
                }

                obj.IsHeadOffice = true;
                obj.CreatedTime = DateTime.Now;
                obj.FullName = req.FullName;
                obj.Phone = req.Phone;
                obj.NationalCard = req.NationalCard;
                obj.LoanAmount = req.LoanAmount;
                obj.LoanTime = req.LoanTime;
                obj.Status = EnumLoanStatus.INIT.GetHashCode();
                obj.ProvinceId = req.ProvinceId;
                obj.DistrictId = req.DistrictId;
                obj.ProductId = req.ProductId;
                obj.UtmSource = req.UtmSource;
                obj.UtmMedium = req.UtmMedium;
                obj.UtmCampaign = req.UtmCampaign;
                obj.UtmContent = req.UtmContent;
                obj.UtmTerm = req.UtmTerm;
                obj.DomainName = req.DomainName;
                obj.AffCode = req.Aff_Code;
                obj.AffSid = req.Aff_Sid;
                obj.Tid = req.TId;
                obj.BankAccountName = req.BankAccountName;
                obj.BankCardNumber = req.BankCardNumber;
                obj.BankAccountNumber = req.BankAccountNumber;
                obj.FinanceLoanCreditId = req.FinanceLoanCreditId;
                obj.SynchorizeFinance = req.SynchorizeFinance;
                obj.TypeLoanBrief = LoanBriefType.Customer.GetHashCode();
                obj.LoanBriefNote = new List<LoanBriefNote>();
                obj.LoanBriefNote.Add(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = "Đơn vay được khởi tạo từ Sàn Tima",
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                });

                //insert to db
                _unitOfWork.LoanBriefRepository.Insert(obj);
                _unitOfWork.Save();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = obj.LoanBriefId;
                if (obj.LoanBriefId > 0)
                {
                    //Lưu lại log khi tạo đơn
                    var objLogCreate = new LogInfoCreateLoanbrief()
                    {
                        LoanBriefId = obj.LoanBriefId,
                        FullName = req.FullName,
                        Phone = req.Phone,
                        LoanAmount = req.LoanAmount,
                        LoanTime = req.LoanTime,
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        ProductId = req.ProductId,
                        UtmSource = req.UtmSource,
                        UtmMedium = req.UtmMedium,
                        UtmCampaign = req.UtmCampaign,
                        UtmContent = req.UtmContent,
                        UtmTerm = req.UtmTerm,
                        DomainName = req.DomainName,
                        PlatformType = obj.PlatformType,
                        NationalCard = req.NationalCard,
                        AffCode = req.Aff_Code,
                        AffSid = req.Aff_Sid,
                        Tid = req.TId,
                        CreateDate = DateTime.Now,
                    };
                    _unitOfWork.LogInfoCreateLoanbriefRepository.Insert(objLogCreate);
                    _unitOfWork.Save();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Finance/CreateLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("CreateLoanBacNinh")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> CreateLoanBacNinh(Finance.CreateLoan req)
        {
            var obj = new LoanBrief();
            var objCus = new Customer();
            var def = new DefaultResponse<object>();
            var objLoanBriefScoring = new LogLoanbriefScoring();

            try
            {
                //Check xem khách hàng có đơn vay nào đang xử lý hay không
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == req.Phone, null, false).OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                if (loanInfo != null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý!");
                    return Ok(def);
                }
                //Check xem khách hàng có trong black list không
                var entityBlackList = new RequestCheckBlackListAg
                {
                    FullName = req.FullName,
                    NumberCard = req.NationalCard,
                    Phone = req.Phone
                };
                var checkBlackList = _agservice.CheckBlacklistAg(entityBlackList);
                if (checkBlackList.Data == true && checkBlackList.Status == 1)
                {
                    def.meta = new Meta(ResponseHelper.CUSTOMER_BLACKLIST_CODE, "Khách hàng nằm trong Black list!");
                    return Ok(def);
                }

                //Check xem customer đã tồn tại hay chưa
                var customerInfo = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == req.Phone);
                if (customerInfo != null && customerInfo.CustomerId > 0)
                {
                    //cập nhật thông tin customer
                    customerInfo.CreatedAt = DateTime.Now;
                    customerInfo.FullName = req.FullName.Replace("  ", " ").Trim();
                    customerInfo.Phone = req.Phone;
                    customerInfo.ProvinceId = req.ProvinceId;
                    customerInfo.DistrictId = req.DistrictId;
                    customerInfo.NationalCard = req.NationalCard;
                    if (req.DOB != null && req.DOB != DateTime.MinValue)
                        customerInfo.Dob = req.DOB;
                    _unitOfWork.CustomerRepository.Update(customerInfo);
                    _unitOfWork.Save();
                    obj.CustomerId = customerInfo.CustomerId;
                }
                else
                {
                    objCus.CreatedAt = DateTime.Now;
                    objCus.FullName = req.FullName.Replace("  ", " ").Trim();
                    objCus.Phone = req.Phone;
                    objCus.ProvinceId = req.ProvinceId;
                    objCus.DistrictId = req.DistrictId;
                    objCus.NationalCard = req.NationalCard;
                    _unitOfWork.CustomerRepository.Insert(objCus);
                    _unitOfWork.Save();
                    obj.CustomerId = objCus.CustomerId;
                }
                if (req.ProvinceId > 0 || req.DistrictId > 0)
                {
                    obj.LoanBriefResident = new LoanBriefResident()
                    {
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId
                    };
                }
                obj.IsHeadOffice = true;
                obj.CreatedTime = DateTime.Now;
                obj.FullName = req.FullName.Replace("  ", " ").Trim();
                obj.Phone = req.Phone;
                obj.NationalCard = req.NationalCard;
                obj.LoanAmount = req.LoanAmount;
                obj.LoanTime = req.LoanTime;
                obj.Status = EnumLoanStatus.CANCELED.GetHashCode();
                obj.PipelineState = -1;
                obj.ProvinceId = req.ProvinceId;
                obj.DistrictId = req.DistrictId;
                obj.ProductId = 2;
                obj.UtmSource = req.UtmSource;
                obj.UtmMedium = req.UtmMedium;
                obj.UtmCampaign = req.UtmCampaign;
                obj.UtmContent = req.UtmContent;
                obj.UtmTerm = req.UtmTerm;
                obj.DomainName = req.DomainName;
                obj.PlatformType = EnumPlatformType.ReMarketing.GetHashCode();
                obj.AffCode = req.Aff_Code;
                obj.AffSid = req.Aff_Sid;
                obj.Tid = req.TId;
                obj.FinanceLoanCreditId = req.FinanceLoanCreditId;
                obj.HubId = 35017; //gán về thẳng hub bắc ninh
                obj.TypeLoanBrief = LoanBriefType.Customer.GetHashCode();
                obj.LoanBriefNote = new List<LoanBriefNote>();
                obj.LoanBriefNote.Add(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = "Đơn vay được khởi tạo từ Sàn Tima",
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                });

                //insert to db
                _unitOfWork.LoanBriefRepository.Insert(obj);
                _unitOfWork.Save();

                objLoanBriefScoring.LoanbriefId = obj.LoanBriefId;
                objLoanBriefScoring.LoanbriefCreatedAt = DateTime.Now;
                objLoanBriefScoring.Phone = req.Phone;
                objLoanBriefScoring.NationalCard = req.NationalCard;
                objLoanBriefScoring.FullName = req.FullName;
                objLoanBriefScoring.CreatedAt = DateTime.Now;
                objLoanBriefScoring.HubId = 35017; //gán về thẳng hub bắc ninh
                objLoanBriefScoring.CityId = req.ProvinceId;
                _unitOfWork.LogLoanbriefScoringRepository.Insert(objLoanBriefScoring);
                _unitOfWork.Save();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = obj.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Finance/CreateLoanBacNinh Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("create_loan_tima")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> CreateLoanTimaVn(Finance.CreateLoanTimaVN req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (!req.Phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không đúng định dạng!");
                    return Ok(def);
                }

                var checkErp = _erpService.CheckEmployeeTima(req.NationalCard, req.Phone);
                if (checkErp != null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng là nhân viên Tima!");
                    return Ok(def);
                }

                //Check xem khách hàng có đơn vay nào đang xử lý hay không
                if (_unitOfWork.LoanBriefRepository.Any(x => x.Phone == req.Phone
                && x.Status != EnumLoanStatus.CANCELED.GetHashCode() && x.Status != EnumLoanStatus.FINISH.GetHashCode()))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý!");
                    return Ok(def);
                }

                //Kiểm tra đơn cũ xem có phải là đơn CPQL của MMO không
                //nếu phải kiểm tra xem ngày tạo đã quá 1 tháng chưa
                //chưa được 1 tháng không cho tạo đơn nữa
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == req.Phone, null, false).OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                if (loanInfo != null && loanInfo.PlatformType == (int)EnumPlatformType.MMOTimaCPQL &&
                    (DateTime.Now - loanInfo.CreatedTime.Value).TotalDays < 32)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đã tồn tại trong hệ thống!");
                    return Ok(def);
                }
                //xử lý tên KH
                if (!string.IsNullOrEmpty(req.FullName))
                {
                    if (!req.FullName.IsNormalized())
                        req.FullName = req.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                    req.FullName = req.FullName.ReduceWhitespace();
                    req.FullName = req.FullName.TitleCaseString();
                }
                var obj = new LoanBrief();
                var objCus = new Customer();
                //Check xem customer đã tồn tại hay chưa
                var customerInfo = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == req.Phone);
                if (customerInfo != null && customerInfo.CustomerId > 0)
                    obj.CustomerId = customerInfo.CustomerId;
                else
                {
                    objCus.CreatedAt = DateTime.Now;
                    objCus.FullName = req.FullName;
                    objCus.Phone = req.Phone;
                    objCus.ProvinceId = req.ProvinceId;
                    objCus.DistrictId = req.DistrictId;
                    objCus.WardId = req.WardId;
                    objCus.NationalCard = req.NationalCard;
                    _unitOfWork.CustomerRepository.Insert(objCus);
                    _unitOfWork.Save();
                    obj.CustomerId = objCus.CustomerId;
                }
                if (req.ProvinceId > 0 || req.DistrictId > 0 || req.WardId > 0)
                {
                    obj.LoanBriefResident = new LoanBriefResident()
                    {
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        WardId = req.WardId
                    };
                }

                //Check xem có đúng khu vực không
                //Không đúng thì cho đơn ở trạng thái treo
                if (req.ProvinceId == 0 || req.DistrictId == 0)
                {
                    obj.Status = EnumLoanStatus.CANCELED.GetHashCode();
                    obj.PipelineState = -1;
                    obj.ReasonCancel = 660;
                    obj.FirstProcessingTime = DateTime.Now;
                }
                else
                {
                    obj.Status = EnumLoanStatus.INIT.GetHashCode();
                }
                obj.IsHeadOffice = true;
                obj.CreatedTime = DateTime.Now;
                obj.FullName = req.FullName;
                obj.Phone = req.Phone;
                obj.NationalCard = req.NationalCard;
                obj.LoanAmount = req.LoanAmount;
                obj.LoanTime = req.LoanTime;
                obj.ProvinceId = req.ProvinceId;
                obj.DistrictId = req.DistrictId;
                obj.WardId = req.WardId;
                obj.ProductId = req.ProductId;
                obj.UtmSource = req.UtmSource;
                obj.UtmMedium = req.UtmMedium;
                obj.UtmCampaign = req.UtmCampaign;
                obj.UtmContent = req.UtmContent;
                obj.UtmTerm = req.UtmTerm;
                obj.DomainName = req.DomainName;
                obj.PlatformType = req.PlatformType;
                obj.AffCode = req.Aff_Code;
                obj.Tid = req.Tid;
                obj.TypeLoanBrief = LoanBriefType.Customer.GetHashCode();
                obj.LoanBriefNote = new List<LoanBriefNote>();
                obj.LoanBriefNote.Add(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = "Đơn vay được tạo từ tima.vn",
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                });

                if (obj.Status == EnumLoanStatus.CANCELED.GetHashCode())
                {
                    obj.LoanBriefNote.Add(new LoanBriefNote()
                    {
                        FullName = "Auto System",
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        Note = "Đơn bị hủy do không thuộc khu vực hỗ trợ",
                        Status = 1,
                        CreatedTime = DateTime.Now,
                        ShopId = 3703,
                        ShopName = "Tima",
                    });
                }

                //insert to db
                _unitOfWork.LoanBriefRepository.Insert(obj);
                _unitOfWork.Save();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = obj.LoanBriefId;

                if (obj.LoanBriefId > 0)
                {
                    //Lưu lại log khi tạo đơn
                    var objLogCreate = new LogInfoCreateLoanbrief()
                    {
                        LoanBriefId = obj.LoanBriefId,
                        FullName = req.FullName,
                        Phone = req.Phone,
                        LoanAmount = req.LoanAmount,
                        LoanTime = req.LoanTime,
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        ProductId = req.ProductId,
                        UtmSource = req.UtmSource,
                        UtmMedium = req.UtmMedium,
                        UtmCampaign = req.UtmCampaign,
                        UtmContent = req.UtmContent,
                        UtmTerm = req.UtmTerm,
                        DomainName = req.DomainName,
                        PlatformType = req.PlatformType,
                        NationalCard = req.NationalCard,
                        AffCode = req.Aff_Code,
                        CreateDate = DateTime.Now,
                    };
                    _unitOfWork.LogInfoCreateLoanbriefRepository.Insert(objLogCreate);
                    _unitOfWork.Save();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Finance/CreateLoanTimaVn Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("report_loan_home_tima")]
        public ActionResult<DefaultResponse<Meta, List<ReportLoanHomeTimaModel>>> ReportLoanHomeTima()
        {
            var def = new DefaultResponse<Meta, List<ReportLoanHomeTimaModel>>();
            try
            {
                var lstData = _unitOfWork.LoanBriefRepository.Query(x => x.ProvinceId > 0 && x.DistrictId > 0 && x.LoanAmount > 0, null, false)
                    .OrderByDescending(x => x.LoanBriefId).Select(ReportLoanHomeTimaModel.ProjectionDetail).Take(50).ToList();
                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Finace/ReportLoanHomeTima Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("report_detail_loan_home_tima")]
        public ActionResult<DefaultResponse<Meta, Finance.ReportDetailLoanHomeTimeModel>> ReportDetailLoanHomeTima()
        {
            var def = new DefaultResponse<Meta, Finance.ReportDetailLoanHomeTimeModel>();
            try
            {
                var key = Constants.KEY_CACHE_REPORT_LOAN_HOME_TIMA;
                var getData = _memoryCache.TryGetValue(key, out def);
                if (!getData)// Look for cache key.
                {
                    def = new DefaultResponse<Meta, Finance.ReportDetailLoanHomeTimeModel>();
                    var mylock = locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
                    mylock.Wait();
                    try
                    {
                        // Key not in cache, so get data. 
                        // cộng thêm 1 ngày để select
                        //Lấy danh sách các đơn vay của accesstrade đổ về los theo ngày cập nhập đơn vay
                        var dateNow = DateTime.Now;
                        var result = _finaceService.GetDetailLoanHomeTima();
                        def.meta = new Meta(200, "success");
                        def.data = result;
                        MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
                        {
                            AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(30) - DateTime.Now)
                        };
                        _memoryCache.Set(key, def, cacheOption);
                    }
                    finally
                    {
                        mylock.Release();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Finace/ReportDetailLoanHomeTime Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("report_total_loan_thirdpartner_cpql")]
        public ActionResult<DefaultResponse<Meta, List<Finance.ResponeReportLoanThirdParterCPQL>>> ReportTotalLoanThirdPartnerCPQL(Finance.RequestReportLoanThirdPartner entity)
        {
            var def = new DefaultResponse<Meta, List<Finance.ResponeReportLoanThirdParterCPQL>>();
            try
            {
                var lstData = new List<Finance.ResponeReportLoanThirdParterCPQL>();
                if (!string.IsNullOrEmpty(entity.third_partner_id))
                    lstData = _finaceService.GetTotalLoanThirdPartnerCPQL(entity);
                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Finace/ReportTotalLoanThirdParnerCPQL Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("report_total_loan_thirdpartner_cps")]
        public ActionResult<DefaultResponse<Meta, List<Finance.ResponeReportLoanThirdParterCPS>>> ReportTotalLoanThirdPartnerCPS(Finance.RequestReportLoanThirdPartner entity)
        {
            var def = new DefaultResponse<Meta, List<Finance.ResponeReportLoanThirdParterCPS>>();
            try
            {
                var lstData = new List<Finance.ResponeReportLoanThirdParterCPS>();
                if (!string.IsNullOrEmpty(entity.third_partner_id))
                    lstData = _finaceService.GetTotalLoanThirdPartnerCPS(entity);
                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Finace/ReportTotalLoanThirdPartnerCPS Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("report_detail_loan_thirdpartner")]
        public ActionResult<DefaultResponse<Meta, List<Finance.ResponseReportDetailLoanThirdPartner>>> ReportDetailLoanThirdPartner(Finance.RequestReportLoanThirdPartner entity)
        {
            var def = new DefaultResponse<Meta, List<Finance.ResponseReportDetailLoanThirdPartner>>();
            try
            {
                var lstData = new List<Finance.ResponseReportDetailLoanThirdPartner>();
                if (!string.IsNullOrEmpty(entity.third_partner_id))
                    lstData = _finaceService.GetDetailLoanThirdPartner(entity);
                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Finace/ReportDetailLoanThirdPartner Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("report_detail_loan_no_support_thirdpartner")]
        public ActionResult<DefaultResponse<Meta, List<Finance.ResponseReportDetailLoanThirdPartner>>> ReportDetailLoanNoSupportThirdPartner(Finance.RequestReportLoanThirdPartner entity)
        {
            var def = new DefaultResponse<Meta, List<Finance.ResponseReportDetailLoanThirdPartner>>();
            try
            {
                var lstData = new List<Finance.ResponseReportDetailLoanThirdPartner>();
                if (!string.IsNullOrEmpty(entity.third_partner_id))
                {
                    var lstPustLoanToPartner = _unitOfWork.PushLoanToPartnerRepository.Query(x => x.CreateDate >= entity.from_date
                        && x.CreateDate <= entity.to_date && x.Tid == entity.third_partner_id, null, false)
                            .Select(PushLoanToPartnerCPS.ProjectionDetail).ToList();
                    foreach (var item in lstPustLoanToPartner)
                    {
                        lstData.Add(new Finance.ResponseReportDetailLoanThirdPartner()
                        {
                            loan_brief_id = item.Id,
                            aff_code = item.AffCode,
                            pantner_id = item.AffSid,
                            reason_cancel = item.Status == (int)StatusPushLoanToPartner.Cancel ? "(Tima Note) Không đủ điều kiện vay" : null,
                            qlf = item.Status == (int)StatusPushLoanToPartner.Disbursement ? "QL không chứng từ" : string.Empty,
                            status = item.Status == (int)StatusPushLoanToPartner.Disbursement ? "Giả ngân" : item.Status == (int)StatusPushLoanToPartner.Cancel ? "Hủy" : "Đang xử lý",
                            day = item.CreateDate.Value
                        });
                    }
                }
                def.meta = new Meta(200, "success");
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Finace/ReportDetailLoanNoSupportThirdPartner Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
