﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Utils;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.AppAPI.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using LOS.Common.Models.Request;
using LOS.Common.Extensions;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Authorization;
using LOS.AppAPI.DTOs.LMS;
using LOS.AppAPI.Models.LMS;
using Microsoft.Extensions.Configuration;
using RestSharp;
using Newtonsoft.Json;
using LOS.AppAPI.Models.App;
using LOS.AppAPI.DTOs.App;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.EntityFrameworkCore;
using LOS.DAL.DTOs;
using LOS.AppAPI.Service.BankLog;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.Service.Notification;
using LOS.AppAPI.Models.Notification;
using LOS.AppAPI.Service;
using LOS.DAL.Procedure;
using Microsoft.Data.SqlClient;
using LOS.AppAPI.Models;
using LOS.AppAPI.Service.AG;
using System.Net.Http;
using LOS.AppAPI.Models.ESign;
using System.Text;
using System.Security.Policy;
using LOS.AppAPI.DTOs;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [AuthorizeIPFilter("LMS_IP")]
    public class LMSController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IBankLog _banklogService;
        private readonly IPushNotification _pushNotificationService;
        private readonly ITrackDevice _trackDeviceService;
        private readonly IEventLogAI _eventLogAIService;
        private readonly IAgService _agService;
        public LMSController(IUnitOfWork unitOfWork, IConfiguration configuration, IBankLog banklogService, IPushNotification pushNotificationService, ITrackDevice trackDeviceService, IEventLogAI eventLogAIService, IAgService agService)
        {
            this.unitOfWork = unitOfWork;
            _configuration = configuration;
            _banklogService = banklogService;
            _pushNotificationService = pushNotificationService;
            _trackDeviceService = trackDeviceService;
            _eventLogAIService = eventLogAIService;
            _agService = agService;
        }

        //Đơn vay chờ giải ngân
        [HttpPost]
        [Route("DisbursementWaiting")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LMSGetListDisbursementWaiting>>> DisbursementWaiting([FromBody] Models.LMS.LoanBriefReq.LMSDisbursementWaiting req)
        {
            var def = new DefaultResponse<SummaryMeta, List<LMSGetListDisbursementWaiting>>();
            try
            {
                //Type: 1-> Tất cả, 2 -> Chờ chọn Lender, 3 -> Chờ kế toán giải ngân, 4 -> Chờ lender giải ngân, 5 -> Chờ GN tự động
                //Type = 10 => lấy ra 2 trạng thái (Chờ kế toán giải ngân, Chờ GN tự động)
                var sta = -1;
                var ListType = new List<int>();
                var LoanStatus = EnumLoanStatus.WAIT_DISBURSE.GetHashCode();
                if (req.Type == (int)EnumDisbursementWaitingReq.All)
                    sta = EnumLoanStatus.LENDER_LOAN_DISTRIBUTING.GetHashCode();
                else if (req.Type == (int)EnumDisbursementWaitingReq.LENDER_LOAN_DISTRIBUTING)
                    LoanStatus = EnumLoanStatus.LENDER_LOAN_DISTRIBUTING.GetHashCode();
                else if (req.Type == (int)EnumDisbursementWaitingReq.DISBURSE_ACC)
                    ListType.Add(EnumDisbursement.DISBURSE_ACC.GetHashCode());
                else if (req.Type == (int)EnumDisbursementWaitingReq.DISBURSE_LENDER)
                    ListType.Add(EnumDisbursement.DISBURSE_LEND.GetHashCode());
                else if (req.Type == (int)EnumDisbursementWaitingReq.DISBURSE_AUTO)
                    ListType.Add(EnumDisbursement.DISBURSE_AUTO.GetHashCode());
                else if (req.Type == (int)EnumDisbursementWaitingReq.DISBURSE_ACC_AND_AUTO)
                {
                    ListType.Add(EnumDisbursement.DISBURSE_ACC.GetHashCode());
                    ListType.Add(EnumDisbursement.DISBURSE_AUTO.GetHashCode());
                }
                if (ListType.Count == 0)
                    ListType = null;

                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess != EnumInProcess.Process.GetHashCode()
                && (x.Status == LoanStatus || x.Status == sta));
                if (ListType != null && ListType.Count > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => ListType.Contains(x.TypeDisbursement.Value)));
                if (!string.IsNullOrEmpty(req.Search))
                {
                    var textSearch = req.Search.Trim();
                    if (ConvertExtensions.IsNumber(textSearch)) //Nếu là số
                    {
                        if (textSearch.Length == (int)NationalCardLength.CMND_QD || textSearch.Length == (int)NationalCardLength.CMND || textSearch.Length == (int)NationalCardLength.CCCD)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.NationalCard == textSearch || x.NationCardPlace == textSearch)));
                        else//search phone
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Phone == textSearch || x.PhoneOther == textSearch)));
                    }
                    else//nếu là text =>search tên
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.FullName.Contains(textSearch)));
                }
                var query = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).OrderByDescending(x => x.CoordinatorPushAt).Select(LMSGetListDisbursementWaiting.ProjectionDisbursementWaiting);
                var totalRecords = query.Count();
                List<LMSGetListDisbursementWaiting> data;
                data = query.Skip((req.Page - 1) * req.PageSize).Take(req.PageSize).ToList();
                //chờ chọn lender
                var dicpipelineHistory = new Dictionary<int, List<PipelineHistory>>();
                var listLoanbriefId = data.Select(x => x.LoanBriefId).Distinct().ToList();
                if ((req.Type == 1 || req.Type == 2) && listLoanbriefId != null && listLoanbriefId.Count > 0)
                {
                    var listPipelineHistory = unitOfWork.PipelineHistoryRepository.Query(x => listLoanbriefId.Contains(x.LoanBriefId.Value)
                    && x.EndStatus == EnumLoanStatus.LENDER_LOAN_DISTRIBUTING.GetHashCode(), null, false).OrderBy(x => x.PipelineHistoryId).ToList();
                    if (listPipelineHistory != null && listPipelineHistory.Count > 0)
                    {
                        foreach (var item in listPipelineHistory)
                        {
                            if (!dicpipelineHistory.ContainsKey(item.LoanBriefId.Value))
                                dicpipelineHistory[item.LoanBriefId.Value] = new List<PipelineHistory>();
                            dicpipelineHistory[item.LoanBriefId.Value].Add(item);
                        }
                    }
                }

                var listConfigContract = unitOfWork.ConfigContractRateRepository.Query(x => x.IsEnable == true, null, false).ToList();
                var dicEsignCustomer = new Dictionary<int, EsignContract>();
                if (listLoanbriefId != null && listLoanbriefId.Count > 0)
                {
                    var listEsignCustomer = unitOfWork.EsignContractRepository.Query(x => x.LoanBriefId > 0 && listLoanbriefId.Contains(x.LoanBriefId.Value)
                            && x.TypeEsign == (int)TypeEsign.Customer, null, false).OrderByDescending(x => x.Id).ToList();
                    if (listEsignCustomer != null && listEsignCustomer.Count > 0)
                    {
                        foreach (var item in listEsignCustomer)
                        {
                            if (!dicEsignCustomer.ContainsKey(item.LoanBriefId.Value))
                                dicEsignCustomer[item.LoanBriefId.Value] = item;
                        }
                    }
                }

                foreach (var item in data)
                {
                    //Kiểm tra xem đơn có ký esign không
                    if (item.LenderRate.GetValueOrDefault(0) == 0)
                    {
                        var timeAppliedRate = DateTime.Now;
                        if (item.EsignState.Value && dicEsignCustomer.ContainsKey(item.LoanBriefId))
                        {
                            var lastEsignCustomer = dicEsignCustomer[item.LoanBriefId];
                            if (lastEsignCustomer != null && lastEsignCustomer.CreatedAt.HasValue)
                                timeAppliedRate = lastEsignCustomer.CreatedAt.Value;
                        }
                        //Tính lãi suất lender
                        if (listConfigContract != null && listConfigContract.Count > 0)
                        {
                            var configRate = listConfigContract.Where(x => x.IsEnable == true
                                     && timeAppliedRate >= x.AppliedAt).OrderByDescending(x => x.Id).FirstOrDefault();
                            if (configRate != null && configRate.LenderRate > 0)
                                item.LenderRate = configRate.LenderRate.Value;
                        }
                    }
                    if (item.LenderRate.GetValueOrDefault(0) == 0)
                        item.LenderRate = 19;
                    //chờ chọn lender
                    if ((req.Type == 1 || req.Type == 2) && dicpipelineHistory.ContainsKey(item.LoanBriefId))
                    {
                        item.BackToLenderCare = (dicpipelineHistory[item.LoanBriefId].Count > 1);
                        item.LenderCareReceivedDate = Convert.ToDateTime(dicpipelineHistory[item.LoanBriefId].First().CreatedDate.ToString());
                    }
                    if (item.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode() && item.DisbursementBy == EnumDisbursement.DISBURSE_ACC.GetHashCode())
                        item.Status = EnumLoanStatus.WAIT_DISBURSE_ACC.GetHashCode();
                    else if (item.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode() && item.DisbursementBy == EnumDisbursement.DISBURSE_LEND.GetHashCode())
                        item.Status = EnumLoanStatus.WAIT_DISBURSE_LEND.GetHashCode();
                    else if (item.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode() && item.DisbursementBy == EnumDisbursement.DISBURSE_AUTO.GetHashCode())
                        item.Status = EnumLoanStatus.WAIT_DISBURSE_AUTO.GetHashCode();
                    else if (item.BuyInsurenceCustomer == true && (item.TypeInsurence == 0 || item.TypeInsurence == null))
                        item.TypeInsurence = 2;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, req.Page, req.PageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/DisbursementWaiting Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        //Đơn chờ KT GN
        [HttpPost]
        [Route("LoanDisbursementWaiting")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LMSGetListDisbursementWaiting>>> LoanDisbursementWaiting([FromBody] Models.LMS.LoanBriefReq.LMSDisbursementWaiting req)
        {
            var def = new DefaultResponse<SummaryMeta, List<LMSGetListDisbursementWaiting>>();
            try
            {
                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess != EnumInProcess.Process.GetHashCode()
                        && x.Status == (int)EnumLoanStatus.WAIT_DISBURSE
                        && x.TypeDisbursement == (int)EnumDisbursement.DISBURSE_ACC);
                if (!string.IsNullOrEmpty(req.Search))
                {
                    var textSearch = req.Search.Trim();
                    if (ConvertExtensions.IsNumber(textSearch)) //Nếu là số
                    {
                        if (textSearch.Length == (int)NationalCardLength.CMND_QD || textSearch.Length == (int)NationalCardLength.CMND || textSearch.Length == (int)NationalCardLength.CCCD)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.NationalCard == textSearch || x.NationCardPlace == textSearch)));
                        else//search phone
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Phone == textSearch || x.PhoneOther == textSearch)));
                    }
                    else//nếu là text =>search tên
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.FullName.Contains(textSearch)));
                }
                var query = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).OrderByDescending(x => x.CoordinatorPushAt).Select(LMSGetListDisbursementWaiting.ProjectionDisbursementWaiting);
                var totalRecords = query.Count();
                List<LMSGetListDisbursementWaiting> data;
                data = query.Skip((req.Page - 1) * req.PageSize).Take(req.PageSize).ToList();
                //chờ chọn lender

                var listConfigContract = unitOfWork.ConfigContractRateRepository.Query(x => x.IsEnable == true, null, false).ToList();
                var dicEsignCustomer = new Dictionary<int, EsignContract>();
                var listLoanbriefId = data.Select(x => x.LoanBriefId).Distinct().ToList();
                if (listLoanbriefId != null && listLoanbriefId.Count > 0)
                {
                    var listEsignCustomer = unitOfWork.EsignContractRepository.Query(x => x.LoanBriefId > 0 && listLoanbriefId.Contains(x.LoanBriefId.Value)
                            && x.TypeEsign == (int)TypeEsign.Customer, null, false).OrderByDescending(x => x.Id).ToList();
                    if (listEsignCustomer != null && listEsignCustomer.Count > 0)
                    {
                        foreach (var item in listEsignCustomer)
                        {
                            if (!dicEsignCustomer.ContainsKey(item.LoanBriefId.Value))
                                dicEsignCustomer[item.LoanBriefId.Value] = item;
                        }
                    }
                }

                foreach (var item in data)
                {
                    //Kiểm tra xem đơn có ký esign không
                    if (item.LenderRate.GetValueOrDefault(0) == 0)
                    {
                        var timeAppliedRate = DateTime.Now;
                        if (item.EsignState.Value && dicEsignCustomer.ContainsKey(item.LoanBriefId))
                        {
                            var lastEsignCustomer = dicEsignCustomer[item.LoanBriefId];
                            if (lastEsignCustomer != null && lastEsignCustomer.CreatedAt.HasValue)
                                timeAppliedRate = lastEsignCustomer.CreatedAt.Value;
                        }
                        //Tính lãi suất lender
                        if (listConfigContract != null && listConfigContract.Count > 0)
                        {
                            var configRate = listConfigContract.Where(x => x.IsEnable == true
                                     && timeAppliedRate >= x.AppliedAt).OrderByDescending(x => x.Id).FirstOrDefault();
                            if (configRate != null && configRate.LenderRate > 0)
                                item.LenderRate = configRate.LenderRate.Value;
                        }
                    }
                    if (item.LenderRate.GetValueOrDefault(0) == 0)
                        item.LenderRate = 19;
                    item.Status = EnumLoanStatus.WAIT_DISBURSE_ACC.GetHashCode();
                    if (item.BuyInsurenceCustomer == true && (item.TypeInsurence == 0 || item.TypeInsurence == null))
                        item.TypeInsurence = 2;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, req.Page, req.PageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/LoanDisbursementWaiting Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        //Chi tiết đơn vay
        [HttpGet]
        [Route("LoanDetail")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta, LMSLoanBriefDetail>>> GetById([FromQuery] int LoanId = 0)
        {
            var def = new DefaultResponse<Meta, LMSLoanBriefDetail>();
            try
            {
                if (LoanId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var data = await unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanId, null, false).Select(LMSLoanBriefDetail.ProjectionDetail).FirstOrDefaultAsync();
                if (data.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode() && data.TypeDisbursement == EnumDisbursement.DISBURSE_ACC.GetHashCode())
                    data.Status = EnumLoanStatus.WAIT_DISBURSE_ACC.GetHashCode();
                else if (data.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode() && data.TypeDisbursement == EnumDisbursement.DISBURSE_LEND.GetHashCode())
                    data.Status = EnumLoanStatus.WAIT_DISBURSE_LEND.GetHashCode();
                else if (data.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode() && data.TypeDisbursement == EnumDisbursement.DISBURSE_AUTO.GetHashCode())
                    data.Status = EnumLoanStatus.WAIT_DISBURSE_AUTO.GetHashCode();
                if (data.TypeLoanBrief != null && Enum.IsDefined(typeof(LoanBriefType), data.TypeLoanBrief))
                    data.TypeLoanBriefName = Description.GetDescription((LoanBriefType)data.TypeLoanBrief);
                if (data.LoanPurpose != null && Enum.IsDefined(typeof(EnumLoadLoanPurpose), data.LoanPurpose))
                    data.LoanPurposeName = Description.GetDescription((EnumLoadLoanPurpose)data.LoanPurpose);
                if (data.RateTypeId != null && Enum.IsDefined(typeof(EnumRateType), data.RateTypeId))
                    data.RateTypeName = Description.GetDescription((EnumRateType)data.RateTypeId);
                if (data.LivingWith != null && Enum.IsDefined(typeof(EnumLivingWith), data.LivingWith))
                    data.LivingName = Description.GetDescription((EnumLivingWith)data.LivingWith);
                if (data.ResidentType != null && Enum.IsDefined(typeof(EnumTypeofownership), data.ResidentType))
                    data.ResidentTypeName = Description.GetDescription((EnumTypeofownership)data.ResidentType);
                if (data.ReceivingMoneyType != null && Enum.IsDefined(typeof(EnumTypeReceivingMoney), data.ReceivingMoneyType))
                    data.ReceivingMoneyTypeName = Description.GetDescription((EnumTypeReceivingMoney)data.ReceivingMoneyType);
                if (data.LoanBriefProperty != null &&
                    (data.ProductId == (int)EnumProductCredit.CamotoCreditType
                        || data.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                        || data.ProductId == (int)EnumProductCredit.OtoCreditType_KCC))
                {
                    data.LoanBriefProperty.PlateNumber = data.LoanBriefProperty.PlateNumberCar;
                }
                if (data.LivingTime != null && Enum.IsDefined(typeof(EnumLivingTime), data.LivingTime))
                    data.LivingTimeName = Description.GetDescription((EnumLivingTime)data.LivingTime);
                if (data.BuyInsurenceCustomer == true && (data.TypeInsurence == 0 || data.TypeInsurence == null))
                    data.TypeInsurence = 2;
                if (data.Frequency == null || data.Frequency == 0)
                    data.Frequency = 1;
                if (data.PlatformType != null && Enum.IsDefined(typeof(EnumPlatformType), data.PlatformType))
                    data.PlatformTypeName = Description.GetDescription((EnumPlatformType)data.PlatformType);
                //gán phí tất toán mặc định
                //if (data.FeePaymentBeforeLoan.GetValueOrDefault(0) == 0)
                //    data.FeePaymentBeforeLoan = 0.05M;

                bool resposeParentId = false;
                //Xử lý case tính có phải đơn tách ra từ đơn oto
                if (data.TopUpOfLoanbriefID == 0
                    && data.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                    && data.ReMarketingLoanBriefId > 0)
                {
                    resposeParentId = true;
                    //Kiểm tra xem nếu có đơn cha
                    var loanParent = await unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == data.ReMarketingLoanBriefId
                        && x.ReMarketingLoanBriefId == data.ReMarketingLoanBriefId, null, false)
                        .Select(x => new
                        {
                            LoanStatus = x.Status
                        }).FirstOrDefaultAsync();

                    if (loanParent != null)
                        data.GroupLoanId = data.ReMarketingLoanBriefId;
                }
                //else
                //{
                //    if (data.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                //        || data.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                //        || data.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                //    {
                //        if (data.RateTypeId == (int)EnumRateType.DuNoGiamDan || data.RateTypeId == (int)EnumRateType.RateAmortization30Days)
                //            data.RateTypeId = (int)EnumRateType.DuNoGiamDanTopUp;
                //    }
                //}
                if (data.LenderRate.GetValueOrDefault(0) == 0)
                {

                    var timeAppliedRate = DateTime.Now;
                    if (data.EsignState.Value)
                    {
                        var lastEsignCustomer = unitOfWork.EsignContractRepository.Query(x => x.LoanBriefId == data.LoanBriefId
                        && x.TypeEsign == (int)TypeEsign.Customer, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                        if (lastEsignCustomer != null && lastEsignCustomer.CreatedAt.HasValue)
                            timeAppliedRate = lastEsignCustomer.CreatedAt.Value;
                    }
                    //Tính lãi suất lender
                    var configRate = unitOfWork.ConfigContractRateRepository.Query(x => x.IsEnable == true
                             && timeAppliedRate >= x.AppliedAt, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                    if (configRate != null && configRate.LenderRate > 0)
                        data.LenderRate = configRate.LenderRate.Value;
                }
                if (!resposeParentId)
                    data.ReMarketingLoanBriefId = 0;
                //Lấy thông tin đia chỉ trên cmnd
                if (string.IsNullOrEmpty(data.AddressNationalCard))
                {
                    var ekycInfomation = await unitOfWork.ResultEkycRepository.Query(x => x.LoanbriefId == data.LoanBriefId, null, false).OrderByDescending(x => x.Id).Select(x => new
                    {
                        AddressNationalCard = x.AddressValue
                    }).FirstOrDefaultAsync();
                    if (ekycInfomation != null)
                        data.AddressNationalCard = ekycInfomation.AddressNationalCard;
                }

                if (!string.IsNullOrEmpty(data.FullName) && !data.FullName.IsNormalized())
                    data.FullName = data.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/Detail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Chi tiết đơn vay V2
        [HttpGet]
        [Route("LoanDetailV2")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<Meta, LoanBriefDetailNew>> GetByIdV2([FromQuery] int LoanId = 0)
        {
            var def = new DefaultResponse<Meta, LoanBriefDetailNew>();
            var loanBrief = new LoanBriefDetailNew();
            try
            {
                if (LoanId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                loanBrief = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanId, null, false).Select(LoanBriefDetailNew.ProjectionViewDetail).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                //Lấy thông tin đia chỉ trên cmnd
                if (string.IsNullOrEmpty(loanBrief.AddressNationalCard))
                {
                    var ekyc = unitOfWork.ResultEkycRepository.Query(x => x.LoanbriefId == loanBrief.LoanBriefId, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                    if (ekyc != null)
                        loanBrief.AddressNationalCard = ekyc.AddressValue;
                }
                def.data = loanBrief;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/GetByIdV2 Exception");
            }
            return Ok(def);
        }
        //List Note by LoanId
        [HttpGet]
        [Route("ListComment")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<Meta, List<LMSLoanBriefNoteDetail>>> GetNoteByLoanID([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int LoanId = 0)
        {
            var def = new DefaultResponse<SummaryMeta, List<LMSLoanBriefNoteDetail>>();
            try
            {
                if (LoanId <= 0)
                {
                    def.meta = new SummaryMeta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE, 0, 0, 0);
                    return Ok(def);
                }
                // check if loan action exists
                if (unitOfWork.LoanBriefRepository.Any(x => x.LoanBriefId == LoanId))
                {
                    var query = unitOfWork.LoanBriefNoteRepository.Query(x => x.LoanBriefId == LoanId, null, false).Select(LMSLoanBriefNoteDetail.ProjectionDetail);
                    var totalRecords = query.Count();
                    List<LMSLoanBriefNoteDetail>
                    data = query.OrderByDescending(x => x.LoanBriefNoteId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                    def.data = data;
                }
                else
                {
                    def.meta = new SummaryMeta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE, 0, 0, 0);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/GetNoteByLoanID Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        //Add Comment
        [HttpPost]
        [Route("add_note")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<object>> AddNote(LoanNoteReq.AddNote req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.LoanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.LoanBriefId);
                if (loanInfo != null)
                {
                    var note = new LoanBriefNote
                    {
                        LoanBriefId = req.LoanBriefId,
                        Note = req.Note,
                        FullName = req.UserName,
                        Status = 1,
                        ActionComment = EnumActionComment.LMS.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = req.UserId,
                        ShopId = 0,
                        ShopName = "LMS"
                    };

                    unitOfWork.LoanBriefNoteRepository.Insert(note);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/AddNote Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //[HttpPost]
        //[Route("shop")]
        //public ActionResult<DefaultResponse<object>> AddShop(ShopReq entity)
        //{
        //    var def = new DefaultResponse<object>();
        //    try
        //    {
        //        if (string.IsNullOrEmpty(entity.Name) || entity.ShopId == 0)
        //        {
        //            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
        //            return Ok(def);
        //        }
        //        if (entity.ShopId > 0)
        //        {
        //            var oldEntity = unitOfWork.ShopRepository.GetById(entity.ShopId);
        //            if (oldEntity != null && oldEntity.ShopId > 0)
        //            {
        //                Ultility.CopyObject(entity, ref oldEntity, true);
        //                oldEntity.UpdatedAt = DateTime.Now;
        //                unitOfWork.ShopRepository.Update(oldEntity);
        //                unitOfWork.Save();
        //            }
        //            else
        //            {
        //                var item = new Shop
        //                {
        //                    ShopId = entity.ShopId,
        //                    Name = entity.Name,
        //                    Company = entity.Company,
        //                    Status = 1,
        //                    CreatedAt = DateTime.Now,
        //                    IsReceiveLoan = true
        //                };
        //                unitOfWork.ShopRepository.Insert(item);
        //                unitOfWork.Save();
        //            }
        //        }
        //        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex, "LMSController/AddShop Exception");
        //        def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
        //    }
        //    return Ok(def);
        //}

        //Kế toán trả lại đơn
        [HttpPost]
        [Route("ReturnLoan")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<object>>> ReturnLoan(Models.LMS.LoanBriefReq.LMSReturnLoan req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //lưu log request
                var json = JsonConvert.SerializeObject(req);
                LogRequest("ReturnLoan", json);
                if (req.LoanId <= 0 || req.Note == "" || req.Type <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.LoanId);
                if (loanInfo != null)
                {
                    // Rút đơn không phải chia tự động
                    if (loanInfo.AutoDistributeState == null)
                    {
                        if (loanInfo.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        {
                            def.meta = new Meta(211, "Đơn vay đang bị hủy!");
                            return Ok(def);
                        }
                        if (loanInfo.InProcess == EnumInProcess.Process.GetHashCode())
                        {
                            def.meta = new Meta(212, "Đơn vay đang được xử lý!");
                            return Ok(def);
                        }
                        if (loanInfo.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                        {
                            def.meta = new Meta(215, "Đơn vay đang ở trạng thái đã giải ngân!");
                            return Ok(def);
                        }
                        if (loanInfo.Status == EnumLoanStatus.FINISH.GetHashCode())
                        {
                            def.meta = new Meta(215, "Đơn vay đang ở trạng thái đã đóng HĐ!");
                            return Ok(def);
                        }
                        unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                        {
                            InProcess = (int)EnumInProcess.Process,
                            ActionState = (int)EnumActionPush.Back,
                            PipelineState = (int)EnumPipelineState.MANUAL_PROCCESSED
                        });
                        unitOfWork.Save();
                        //Add note
                        var Action = 0;
                        if (req.Type == 1)
                            Action = EnumActionComment.LMSAccountantReturn.GetHashCode();
                        if (req.Type == 2)
                            Action = EnumActionComment.LMSLenderReturn.GetHashCode();
                        if (req.Type == 3)
                            Action = EnumActionComment.LMSLenderCareRetake.GetHashCode();
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = req.LoanId,
                            Note = req.Note,
                            FullName = string.IsNullOrEmpty(req.UserName) ? "" : req.UserName,
                            Status = 1,
                            ActionComment = Action,
                            CreatedTime = DateTime.Now,
                            UserId = 0,
                            ShopId = 0,
                            ShopName = "LMS"
                        };

                        unitOfWork.LoanBriefNoteRepository.Insert(note);
                        unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                    else // Kế toán trả đơn về, hoặc Lender Care chủ động rút về
                    {
                        if (loanInfo.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        {
                            def.meta = new Meta(211, "Đơn vay đang bị hủy!");
                            return Ok(def);
                        }
                        if (loanInfo.InProcess == EnumInProcess.Process.GetHashCode())
                        {
                            def.meta = new Meta(212, "Đơn vay đang được xử lý!");
                            return Ok(def);
                        }
                        if (loanInfo.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                        {
                            def.meta = new Meta(215, "Đơn vay đang ở trạng thái đã giải ngân!");
                            return Ok(def);
                        }
                        if (loanInfo.Status == EnumLoanStatus.FINISH.GetHashCode())
                        {
                            def.meta = new Meta(215, "Đơn vay đang ở trạng thái đã đóng HĐ!");
                            return Ok(def);
                        }
                        // Kiếm tra nếu đơn đang đc giải ngân thì ko cho rút
                        bool hasPendingTransactions = false;
                        // Kiểm tra xem có giao dịch nghi vấn hay không
                        using (var httpClient = new HttpClient())
                        {
                            // Comment
                            var response = await httpClient.GetAsync(_configuration["AppSettings:PaygateURL"] + "/api/loancredit/pending_transactions?loanBriefId=" + loanInfo.LoanBriefId + "&lenderId=" + loanInfo.LenderId);
                            if (response.IsSuccessStatusCode)
                            {
                                var result = await response.Content.ReadAsStringAsync();
                                var obj = JsonConvert.DeserializeObject<ThirdPartyProccesor.Objects.PaygateDefaultResponse<bool>>(result);
                                if (obj.meta.errCode == 200)
                                {
                                    hasPendingTransactions = obj.data;
                                }
                            }
                            else
                            {
                                // Ko request được >> mặc định ko cho rút về
                                hasPendingTransactions = true;
                            }
                        }
                        unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                        {
                            InProcess = (int)EnumInProcess.Process,
                            ActionState = (int)EnumActionPush.Back,
                            PipelineState = (int)EnumPipelineState.MANUAL_PROCCESSED,
                            AutoDistributeState = null // Bất kể trường hợp thu đơn về thì không chia tự động nữa
                        });
                        unitOfWork.Save();
                        //Add note
                        var Action = 0;
                        if (req.Type == 1)
                            Action = EnumActionComment.LMSAccountantReturn.GetHashCode();
                        if (req.Type == 2)
                            Action = EnumActionComment.LMSLenderReturn.GetHashCode();
                        if (req.Type == 3)
                            Action = EnumActionComment.LMSLenderCareRetake.GetHashCode();
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = req.LoanId,
                            Note = req.Note,
                            FullName = string.IsNullOrEmpty(req.UserName) ? "" : req.UserName,
                            Status = 1,
                            ActionComment = Action,
                            CreatedTime = DateTime.Now,
                            UserId = 0,
                            ShopId = 0,
                            ShopName = "LMS"
                        };

                        if (loanInfo.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING)
                        {
                            // Release tiền
                            var lenderConfig = unitOfWork.LenderConfigRepository.Query(x => x.LenderId == loanInfo.LenderId, null, false).FirstOrDefault();
                            if (lenderConfig != null && lenderConfig.LockedMoney >= loanInfo.LoanAmount)
                            {
                                lenderConfig.LockedMoney -= loanInfo.LoanAmount;
                                lenderConfig.UpdatedTime = DateTime.Now;
                                unitOfWork.LenderConfigRepository.Update(lenderConfig);
                                unitOfWork.Save();
                            }
                        }
                        // Update trạng thái LenderLoanBrief nếu có
                        var lenderLoanBrief = unitOfWork.LenderLoanBriefRepository.Query(x => x.LenderId == loanInfo.LenderId && x.LoanBriefId == loanInfo.LoanBriefId, null, false).FirstOrDefault();
                        if (lenderLoanBrief != null)
                        {
                            lenderLoanBrief.Status = (int)(int)Common.Helpers.Const.AutoDistributeState.LENDER_CARE_KT_RETURNED;
                            lenderLoanBrief.UpdatedTime = DateTime.Now;
                            unitOfWork.LenderLoanBriefRepository.Update(lenderLoanBrief);
                        }

                        unitOfWork.LoanBriefNoteRepository.Insert(note);
                        unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/ReturnLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Giải ngân & Đóng hợp đồng

        [HttpPost]
        [Route("Disbursement")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta>>> Disbursement([FromBody] Models.LMS.LoanBriefReq.LMSUpdateLoan req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                //lưu log request
                var json = JsonConvert.SerializeObject(req);
                LogRequest("Disbursement", json);
                if (req.LoanBriefId <= 0 || req.LoanId <= 0 || req.Type <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.LoanBriefId);
                var task = new List<Task>();
                if (loanInfo != null)
                {
                    if (loanInfo.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode() || loanInfo.Status == EnumLoanStatus.DISBURSED.GetHashCode()
                        || (loanInfo.Status == EnumLoanStatus.LENDER_LOAN_DISTRIBUTING.GetHashCode() && req.IsHandleException == 1))//TH GN lỗi => trạng thái là chờ chọn lender
                    {
                        if (loanInfo.Status == EnumLoanStatus.DISBURSED.GetHashCode() && req.Type == 1)
                        {
                            def.meta = new Meta(215, "Đơn vay đang ở trạng thái đã giải ngân!");
                            return Ok(def);
                        }
                        var action = 0;
                        var mes = "";
                        //Giải ngân đơn vay
                        if (req.Type == 1)
                        {
                            if (loanInfo.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING)
                            {
                                // trừ số tiền đang giữ khi được lock
                                var lenderLoanBrief = await unitOfWork.LenderLoanBriefRepository.Query(x => x.LenderId == loanInfo.LenderId
                                    && x.LoanBriefId == loanInfo.LoanBriefId
                                    && loanInfo.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING, null, false).FirstOrDefaultAsync();
                                if (lenderLoanBrief != null)
                                {
                                    var loanAmount = loanInfo.LoanAmount;
                                    // Chia tự động và đã lock thành công
                                    await unitOfWork.LenderConfigRepository.UpdateAsync(x => x.LenderId == loanInfo.LenderId && x.LockedMoney >= loanAmount, x => new LenderConfig()
                                    {
                                        CurrentMoney = x.CurrentMoney - loanAmount,
                                        LockedMoney = x.LockedMoney - loanAmount,
                                        UpdatedTime = DateTime.Now
                                    });
                                }
                            }
                            action = EnumActionComment.LMSDisbursement.GetHashCode();
                            mes = "Giải ngân HĐ-" + req.LoanBriefId + ", người thao tác: " + req.UserName;
                            if (req.IsHandleException == 1)
                            {
                                mes += "<br /> Đơn vay GN từ trạng thái Chờ chọn Lender";
                            }
                            unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                            {
                                Status = EnumLoanStatus.DISBURSED.GetHashCode(),
                                LmsLoanId = req.LoanId,
                                DisbursementAt = DateTime.Now,
                                FeeInsuranceOfCustomer = req.FeeInsuranceOfCustomer,
                                FeeInsuranceOfProperty = req.FeeInsuranceOfProperty,
                                CodeId = req.CodeId,
                                FromDate = DateTime.Now,
                                ToDate = DateTime.Now.AddMonths((int)loanInfo.LoanTime)
                            });
                            //Thêm log GN đơn vay
                            unitOfWork.LogLoanActionRepository.Insert(new LogLoanAction
                            {
                                LoanbriefId = loanInfo.LoanBriefId,
                                ActionId = (int)EnumLogLoanAction.Disbursement,
                                TypeAction = (int)EnumTypeAction.Auto,
                                LoanStatus = EnumLoanStatus.DISBURSED.GetHashCode(),
                                TelesaleId = loanInfo.BoundTelesaleId,
                                HubId = loanInfo.HubId,
                                HubEmployeeId = loanInfo.HubEmployeeId,
                                CoordinatorUserId = loanInfo.CoordinatorUserId,
                                //UserActionId = 1,
                                //GroupUserActionId = 1,
                                NewValues = JsonConvert.SerializeObject(loanInfo),
                                CreatedAt = DateTime.Now
                            });
                            unitOfWork.Save();
                            //Call api EvetLogAI
                            var logAI = new EventLogAI.DisbursementInput
                            {
                                DisbursementDate = DateTime.Now.ToString("yyyy-MM-dd"),
                                Imei = "",
                                LoanBriefId = req.LoanBriefId,
                                Name = loanInfo.FullName,
                                NationalId = loanInfo.NationalCard,
                                Phone = loanInfo.Phone
                            };
                            _eventLogAIService.LogDisbursement(logAI);


                        }
                        //Đóng hợp đồng
                        else if (req.Type == 2)
                        {
                            action = EnumActionComment.LMSFinish.GetHashCode();
                            mes = "Đóng HĐ-" + req.LoanBriefId + ", người thao tác: " + req.UserName;
                            unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                            {
                                Status = EnumLoanStatus.FINISH.GetHashCode(),
                                PipelineState = -1,
                                FinishAt = DateTime.Now
                            });
                            //nếu là đơn định vị => gọi đóng hợp đồng
                            if (!string.IsNullOrEmpty(loanInfo.DeviceId)
                                && loanInfo.DeviceStatus == (int)StatusOfDeviceID.Actived
                                && _trackDeviceService.CloseContract(string.Format("HD-{0}", loanInfo.LoanBriefId), loanInfo.DeviceId, loanInfo.LoanBriefId))
                            {
                                unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto System",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = "Đóng hợp đồng định vị bên AI",
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = loanInfo.LoanBriefId
                                });
                            }

                            //Call api EvetLogAI
                            _eventLogAIService.LogClosing(req.LoanBriefId);
                        }
                        unitOfWork.LogCallApiRepository.Insert(new LogCallApi
                        {
                            ActionCallApi = ActionCallApi.LMSCallDisLos.GetHashCode(),
                            LinkCallApi = "Disbursement",
                            TokenCallApi = "",
                            Status = (int)StatusCallApi.Success,
                            LoanBriefId = req.LoanBriefId,
                            Input = JsonConvert.SerializeObject(req),
                            CreatedAt = DateTime.Now
                        });

                        unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote
                        {
                            LoanBriefId = req.LoanBriefId,
                            Note = mes,
                            FullName = req.UserName,
                            Status = 1,
                            ActionComment = action,
                            CreatedTime = DateTime.Now,
                            UserId = 0,
                            ShopId = 0,
                            ShopName = "LMS"
                        });
                        unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(214, "Trạng thái đơn vay không hợp lệ!");
                        return Ok(def);
                    }

                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/Disbursement Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Khóa mở đơn vay
        [HttpPost]
        [Route("LoanLock")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<Meta>> LoanLock([FromBody] Models.LMS.LoanBriefReq.LMSLoanLock req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LoanId <= 0 || req.Type <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.LoanId);
                if (loanInfo != null)
                {

                    //Type: 1-> Mở đơn, 2-> Khóa đơn
                    var mess = "";
                    var action = 0;
                    if (req.Type == 1)
                    {
                        loanInfo.IsLock = false;
                        mess = "Mở khóa đơn vay HD-" + req.LoanId + ", người thao tác: " + req.UserName;
                        action = EnumActionComment.LMSLoanOpenLock.GetHashCode();
                    }
                    if (req.Type == 2)
                    {
                        loanInfo.IsLock = true;
                        mess = "Khóa đơn vay HD-" + req.LoanId + " với lý do: " + req.Note + ", người thao tác: " + req.UserName;
                        action = EnumActionComment.LMSLoanLock.GetHashCode();
                    }

                    unitOfWork.LoanBriefRepository.Update(loanInfo);
                    unitOfWork.Save();

                    var note = new LoanBriefNote
                    {
                        LoanBriefId = req.LoanId,
                        Note = mess,
                        FullName = req.UserName,
                        Status = 1,
                        ActionComment = action,
                        CreatedTime = DateTime.Now,
                        UserId = 0,
                        ShopId = 0,
                        ShopName = "LMS"
                    };

                    unitOfWork.LoanBriefNoteRepository.Insert(note);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);

                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/LoanLock Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Đẩy đơn cho Lender
        [HttpPost]
        [Route("PushLoanLender")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta>>> PushLoanLender([FromBody] Models.LMS.LoanBriefReq.LMSPushLender req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                //lưu log request
                var json = JsonConvert.SerializeObject(req);
                LogRequest("PushLoanLender", json);
                if (req.LoanId <= 0 || req.LenderId <= 0 || req.DisbursementBy <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.LoanId);
                if (loanInfo != null)
                {
                    var timeAppliedRate = DateTime.Now;
                    var lenderRate = LOS.Common.Helpers.Const.LENDER_RATE;
                    if (loanInfo.AutoDistributeState == null)
                    {
                        if (loanInfo.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        {
                            def.meta = new Meta(211, "Đơn vay đang bị hủy!");
                            return Ok(def);
                        }
                        else if (loanInfo.InProcess == EnumInProcess.Process.GetHashCode())
                        {
                            def.meta = new Meta(212, "Đơn vay đang được xử lý!");
                            return Ok(def);
                        }

                        else if (loanInfo.IsLock == true)
                        {
                            def.meta = new Meta(213, "Đơn vay đang bị khóa. Vui lòng mở khóa đơn vay!");
                            return Ok(def);
                        }
                        else if (loanInfo.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                        {
                            def.meta = new Meta(215, "Đơn vay đang ở trạng thái đã giải ngân!");
                            return Ok(def);
                        }
                        else if (loanInfo.Status != EnumLoanStatus.LENDER_LOAN_DISTRIBUTING.GetHashCode())
                        {
                            def.meta = new Meta(216, "Đơn vay đang ở trạng thái không hợp lệ!");
                            return Ok(def);
                        }
                        // Ký hợp đồng điện tử nếu có
                        if (loanInfo.EsignState == (int)EsignState.BORROWER_SIGNED || loanInfo.EsignState == (int)EsignState.LENDER_SIGNED) // Ký lại
                        {
                            //Tính lãi suất lender
                            var lastEsignCustomer = unitOfWork.EsignContractRepository.Query(x => x.LoanBriefId == loanInfo.LoanBriefId
                                && x.TypeEsign == (int)TypeEsign.Customer, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                            if (lastEsignCustomer != null && lastEsignCustomer.CreatedAt.HasValue)
                            {
                                timeAppliedRate = lastEsignCustomer.CreatedAt.Value;
                                var configRate = unitOfWork.ConfigContractRateRepository.Query(x => x.IsEnable == true
                                     && timeAppliedRate >= x.AppliedAt, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                                if (configRate != null && configRate.LenderRate > 0)
                                    lenderRate = configRate.LenderRate.Value;
                                unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                                {
                                    LenderRate = lenderRate
                                });
                            }
                            else
                            {
                                def.meta = new Meta(217, "Không lấy được thông tin hợp đồng của khách hàng");
                                return Ok(def);
                            }
                            // Kiểm tra xem nđt có thông tin aggreement id và passcode hay không
                            var shopInfo = await _agService.GetShopInfo(req.LenderId);
                            if (shopInfo != null)
                            {
                                if (shopInfo.Data != null && (string.IsNullOrEmpty(shopInfo.Data.PersonContact) || string.IsNullOrEmpty(shopInfo.Data.PersonCardNumber) ||
                                    shopInfo.Data.PersonBirthDay == null || string.IsNullOrEmpty(shopInfo.Data.Address) || string.IsNullOrEmpty(shopInfo.Data.AddressOfResidence)))
                                {
                                    def.meta = new Meta(217, "Không đủ thông tin nhà đầu tư. Không thể ký hợp đồng");
                                    return Ok(def);
                                }

                                if (shopInfo.Status == 1)
                                {
                                    if (shopInfo.Data != null && !String.IsNullOrEmpty(shopInfo.Data.AgreementId) && !String.IsNullOrEmpty(shopInfo.Data.Passcode))
                                    {
                                        // Đủ thông tin gọi api
                                        using (var httpClient = new HttpClient())
                                        {
                                            var data = new SignContractLenderReq
                                            {
                                                agreementId = shopInfo.Data.AgreementId,
                                                lenderId = req.LenderId,
                                                loanBriefId = loanInfo.LoanBriefId,
                                                passcode = shopInfo.Data.Passcode
                                            };
                                            string token = Request.Headers["Authorization"];
                                            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);
                                            StringContent content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
                                            // Bug Request.Scheme luôn trả ra http
                                            // Nếu domain từ apilos và apitestlos thì https
                                            string host = Request.Host.Value;
                                            string scheme = Request.Scheme;
                                            if (host.Equals("apitestlos.tima.vn") || host.Equals("apilos.tima.vn"))
                                                scheme = "https";
                                            string domainName = scheme + "://" + host;
                                            var response = await httpClient.PostAsync(domainName + "/api/v1.0/esign/SignContractForLender", content);
                                            if (response.IsSuccessStatusCode)
                                            {
                                                var result = await response.Content.ReadAsStringAsync();
                                                var oResult = JsonConvert.DeserializeObject<DefaultResponse<object>>(result);
                                                if (oResult != null)
                                                {
                                                    if (oResult.meta.errorCode != 200)
                                                    {
                                                        def.meta = new Meta(218, oResult.meta.errorMessage);
                                                        return Ok(def);
                                                    }
                                                }
                                                else
                                                {
                                                    def.meta = new Meta(217, "Ký hợp đồng thật bại, vui lòng thử lại sau!");
                                                    return Ok(def);
                                                }
                                            }
                                            else
                                            {
                                                def.meta = new Meta(217, "Ký hợp đồng thật bại, vui lòng thử lại sau!");
                                                return Ok(def);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        def.meta = new Meta(217, "Không lấy được thông tin nhà đầu tư. Không thể ký hợp đồng");
                                        return Ok(def);
                                    }
                                }
                                else
                                {
                                    def.meta = new Meta(217, "Không lấy được thông tin nhà đầu tư. Không thể ký hợp đồng");
                                    return Ok(def);
                                }
                            }
                            else
                            {
                                def.meta = new Meta(217, "Không lấy được thông tin nhà đầu tư. Không thể ký hợp đồng");
                                return Ok(def);
                            }
                        }
                        else
                        {
                            var configRate = unitOfWork.ConfigContractRateRepository.Query(x => x.IsEnable == true
                                    && timeAppliedRate >= x.AppliedAt, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                            if (configRate != null && configRate.LenderRate > 0)
                                lenderRate = configRate.LenderRate.Value;
                        }
                        int minuteWaitDisbursement = 0;
                        var timewait = _configuration["AppSettings:Time_Wait_Disbursement"];
                        if (!string.IsNullOrEmpty(timewait))
                            int.TryParse(timewait, out minuteWaitDisbursement);

                        //DisbursementBy: 1-> Kế toán, 2 -> Lender
                        var nextDate = DateTime.Now.AddMinutes(minuteWaitDisbursement);
                        unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                        {
                            InProcess = (int)EnumInProcess.Process,
                            ActionState = (int)EnumActionPush.Push,
                            PipelineState = (int)EnumPipelineState.MANUAL_PROCCESSED,
                            TypeDisbursement = req.DisbursementBy,
                            LenderId = req.LenderId,
                            LenderReceivedDate = DateTime.Now,
                            NextDate = nextDate,
                            LenderName = req.LenderName,
                            LenderRate = lenderRate
                        });

                        //Check đã được chọn lender chưa, nếu có sẽ update thông tin lender
                        var loanLender = unitOfWork.LoanBriefLenderRepository.Query(x => x.LoanbriefId == req.LoanId, null, false).FirstOrDefault();
                        if (loanLender != null)
                        {
                            //Update lại thông tin lender
                            unitOfWork.LoanBriefLenderRepository.Update(x => x.Id == loanLender.Id, x => new LoanbriefLender()
                            {
                                LenderId = req.LenderId,
                                LenderName = req.LenderName,
                                FullName = req.LenderFullName,
                                NationalCard = req.LenderNationalCard
                            });
                        }
                        else
                        {
                            //Add thông tin vào LoanBriefLender
                            unitOfWork.LoanBriefLenderRepository.Insert(new LoanbriefLender
                            {
                                LoanbriefId = req.LoanId,
                                CreatedAt = DateTime.Now,
                                LenderId = req.LenderId,
                                LenderName = req.LenderName,
                                FullName = req.LenderFullName,
                                NationalCard = req.LenderNationalCard

                            });
                        }

                        unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote
                        {
                            LoanBriefId = req.LoanId,
                            Note = "Đơn vay HD-" + req.LoanId + " được đẩy cho lender:" + req.LenderName,
                            FullName = string.IsNullOrEmpty(req.UserName) ? "" : req.UserName,
                            Status = 1,
                            ActionComment = EnumActionComment.LMSPushLoanLender.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 0,
                            ShopId = 0,
                            ShopName = "LMS"
                        });
                        //send notification to lender
                        var notifi = new NotificationLenderReq();
                        notifi.key = "tima@lender#234";
                        notifi.data = new List<NotificationItem>();
                        notifi.data.Add(new NotificationItem()
                        {
                            loanCreditId = loanInfo.LoanBriefId,
                            lenderId = req.LenderId
                        });
                        _pushNotificationService.SendNotificationToLender(notifi);
                        unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        return Ok(def);
                    }
                    else
                    {
                        if (loanInfo.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        {
                            def.meta = new Meta(211, "Đơn vay đang bị hủy!");
                            return Ok(def);
                        }
                        else if (loanInfo.InProcess == EnumInProcess.Process.GetHashCode())
                        {
                            def.meta = new Meta(212, "Đơn vay đang được xử lý!");
                            return Ok(def);
                        }

                        else if (loanInfo.IsLock == true)
                        {
                            def.meta = new Meta(213, "Đơn vay đang bị khóa. Vui lòng mở khóa đơn vay!");
                            return Ok(def);
                        }
                        else if (loanInfo.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                        {
                            def.meta = new Meta(215, "Đơn vay đang ở trạng thái đã giải ngân!");
                            return Ok(def);
                        }
                        else if (loanInfo.Status != EnumLoanStatus.LENDER_LOAN_DISTRIBUTING.GetHashCode())
                        {
                            def.meta = new Meta(216, "Đơn vay đang ở trạng thái không hợp lệ!");
                            return Ok(def);
                        }

                        // Ký hợp đồng điện tử nếu có
                        if (loanInfo.EsignState == (int)EsignState.BORROWER_SIGNED || loanInfo.EsignState == (int)EsignState.LENDER_SIGNED) // Ký lại
                        {
                            //Tính lãi suất lender
                            var lastEsignCustomer = unitOfWork.EsignContractRepository.Query(x => x.LoanBriefId == loanInfo.LoanBriefId
                                && x.TypeEsign == (int)TypeEsign.Customer, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                            if (lastEsignCustomer != null && lastEsignCustomer.CreatedAt.HasValue)
                            {
                                timeAppliedRate = lastEsignCustomer.CreatedAt.Value;
                                var configRate = unitOfWork.ConfigContractRateRepository.Query(x => x.IsEnable == true
                                     && timeAppliedRate >= x.AppliedAt, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                                if (configRate != null && configRate.LenderRate > 0)
                                    lenderRate = configRate.LenderRate.Value;
                                unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                                {
                                    LenderRate = lenderRate
                                });
                            }
                            else
                            {
                                def.meta = new Meta(217, "Không lấy được thông tin hợp đồng của khách hàng");
                                return Ok(def);
                            }

                            // Kiểm tra xem nđt có thông tin aggreement id và passcode hay không
                            var shopInfo = await _agService.GetShopInfo(req.LenderId);
                            if (shopInfo != null)
                            {
                                if (shopInfo.Data != null && (string.IsNullOrEmpty(shopInfo.Data.PersonContact) || string.IsNullOrEmpty(shopInfo.Data.PersonCardNumber) ||
                                    shopInfo.Data.PersonBirthDay == null || string.IsNullOrEmpty(shopInfo.Data.Address) || string.IsNullOrEmpty(shopInfo.Data.AddressOfResidence)))
                                {
                                    def.meta = new Meta(217, "Không đủ thông tin nhà đầu tư. Không thể ký hợp đồng");
                                    return Ok(def);
                                }

                                if (shopInfo.Status == 1)
                                {
                                    if (shopInfo.Data != null && !String.IsNullOrEmpty(shopInfo.Data.AgreementId) && !String.IsNullOrEmpty(shopInfo.Data.Passcode))
                                    {
                                        // Đủ thông tin gọi api
                                        using (var httpClient = new HttpClient())
                                        {
                                            var data = new SignContractLenderReq
                                            {
                                                agreementId = shopInfo.Data.AgreementId,
                                                lenderId = req.LenderId,
                                                loanBriefId = loanInfo.LoanBriefId,
                                                passcode = shopInfo.Data.Passcode
                                            };
                                            string token = Request.Headers["Authorization"];
                                            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);
                                            StringContent content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
                                            // Bug Request.Scheme luôn trả ra http
                                            // Nếu domain từ apilos và apitestlos thì https
                                            string host = Request.Host.Value;
                                            string scheme = Request.Scheme;
                                            if (host.Equals("apitestlos.tima.vn") || host.Equals("apilos.tima.vn"))
                                                scheme = "https";
                                            string domainName = scheme + "://" + host;
                                            var response = await httpClient.PostAsync(domainName + "/api/v1.0/esign/SignContractForLender", content);
                                            if (response.IsSuccessStatusCode)
                                            {
                                                var result = await response.Content.ReadAsStringAsync();
                                                var oResult = JsonConvert.DeserializeObject<DefaultResponse<object>>(result);
                                                if (oResult != null)
                                                {
                                                    if (oResult.meta.errorCode != 200)
                                                    {
                                                        def.meta = new Meta(218, oResult.meta.errorMessage);
                                                        return Ok(def);
                                                    }
                                                }
                                                else
                                                {
                                                    def.meta = new Meta(217, "Ký hợp đồng thật bại, vui lòng thử lại sau!");
                                                    return Ok(def);
                                                }
                                            }
                                            else
                                            {
                                                def.meta = new Meta(217, "Ký hợp đồng thật bại, vui lòng thử lại sau!");
                                                return Ok(def);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        def.meta = new Meta(217, "Không lấy được thông tin nhà đầu tư. Không thể ký hợp đồng");
                                        return Ok(def);
                                    }
                                }
                                else
                                {
                                    def.meta = new Meta(217, "Không lấy được thông tin nhà đầu tư. Không thể ký hợp đồng");
                                    return Ok(def);
                                }
                            }
                            else
                            {
                                def.meta = new Meta(217, "Không lấy được thông tin nhà đầu tư. Không thể ký hợp đồng");
                                return Ok(def);
                            }


                        }
                        else
                        {
                            var configRate = unitOfWork.ConfigContractRateRepository.Query(x => x.IsEnable == true
                                   && timeAppliedRate >= x.AppliedAt, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                            if (configRate != null && configRate.LenderRate > 0)
                                lenderRate = configRate.LenderRate.Value;
                        }
                        int minuteWaitDisbursement = 0;
                        var timewait = _configuration["AppSettings:Time_Wait_Disbursement"];
                        if (!string.IsNullOrEmpty(timewait))
                            int.TryParse(timewait, out minuteWaitDisbursement);

                        //DisbursementBy: 1-> Kế toán, 2 -> Lender
                        var nextDate = DateTime.Now.AddMinutes(minuteWaitDisbursement);
                        unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                        {
                            InProcess = (int)EnumInProcess.Process,
                            ActionState = (int)EnumActionPush.Push,
                            PipelineState = (int)EnumPipelineState.MANUAL_PROCCESSED,
                            TypeDisbursement = req.DisbursementBy,
                            LenderId = req.LenderId,
                            LenderReceivedDate = DateTime.Now,
                            NextDate = nextDate,
                            LenderName = req.LenderName,
                            LenderRate = lenderRate
                        });

                        //Check đã được chọn lender chưa, nếu có sẽ update thông tin lender
                        var loanLender = unitOfWork.LoanBriefLenderRepository.Query(x => x.LoanbriefId == req.LoanId, null, false).FirstOrDefault();
                        if (loanLender != null)
                        {
                            //Update lại thông tin lender
                            unitOfWork.LoanBriefLenderRepository.Update(x => x.Id == loanLender.Id, x => new LoanbriefLender()
                            {
                                LenderId = req.LenderId,
                                LenderName = req.LenderName,
                                FullName = req.LenderFullName,
                                NationalCard = req.LenderNationalCard
                            });
                        }
                        else
                        {
                            //Add thông tin vào LoanBriefLender
                            unitOfWork.LoanBriefLenderRepository.Insert(new LoanbriefLender
                            {
                                LoanbriefId = req.LoanId,
                                CreatedAt = DateTime.Now,
                                LenderId = req.LenderId,
                                LenderName = req.LenderName,
                                FullName = req.LenderFullName,
                                NationalCard = req.LenderNationalCard

                            });
                        }

                        unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote
                        {
                            LoanBriefId = req.LoanId,
                            Note = "Đơn vay HD-" + req.LoanId + " được đẩy cho lender:" + req.LenderName,
                            FullName = string.IsNullOrEmpty(req.UserName) ? "" : req.UserName,
                            Status = 1,
                            ActionComment = EnumActionComment.LMSPushLoanLender.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 0,
                            ShopId = 0,
                            ShopName = "LMS"
                        });
                        //send notification to lender
                        var notifi = new NotificationLenderReq();
                        notifi.key = "tima@lender#234";
                        notifi.data = new List<NotificationItem>();
                        notifi.data.Add(new NotificationItem()
                        {
                            loanCreditId = loanInfo.LoanBriefId,
                            lenderId = req.LenderId
                        });
                        _pushNotificationService.SendNotificationToLender(notifi);

                        //Add logCallApi
                        //unitOfWork.LogCallApiRepository.Insert(new LogCallApi
                        //{
                        //    ActionCallApi = ActionCallApi.LMSPushLender.GetHashCode(),
                        //    LinkCallApi = "PushLoanLender",
                        //    TokenCallApi = "",
                        //    Status = (int)StatusCallApi.Success,
                        //    LoanBriefId = req.LoanId,
                        //    Input = JsonConvert.SerializeObject(req),
                        //    CreatedAt = DateTime.Now
                        //});
                        unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/PushLoanLender Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        //Gia hạn thời gian giữ đơn
        [Route("extend_time_wait_lender")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<Meta>> ExtendTimeWaitLender([FromBody] Models.LMS.LoanBriefReq.LMSExtendTimeWaitLender req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LoanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.LoanBriefId);
                if (loanInfo != null)
                {
                    DateTime nextDate = DateTime.ParseExact(req.NextTime, "dd/MM/yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
                    loanInfo.NextDate = nextDate;
                    unitOfWork.LoanBriefRepository.Update(loanInfo);
                    unitOfWork.Save();

                    var note = new LoanBriefNote
                    {
                        LoanBriefId = req.LoanBriefId,
                        Note = "Đơn vay HD-" + req.LoanBriefId + " được gia hạn thêm thời gian giải ngân đến :" + req.NextTime,
                        FullName = loanInfo.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 0,
                        ShopId = 0,
                        ShopName = "LMS"
                    };
                    unitOfWork.LoanBriefNoteRepository.Insert(note);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/ExtendTimeWaitLender Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Danh sách chứng từ của đơn vay
        [HttpPost]
        [Route("ListImage")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LMSLoanBriefFilesDetail2>>>> ListImage([FromBody] ImageReq.ListImageLMS req)
        {
            var def = new DefaultResponse<Meta, List<LMSLoanBriefFilesDetail2>>();
            try
            {
                if (req.LoanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var ServiceURL = _configuration["AppSettings:ServiceURL"];
                var ServiceURLAG = _configuration["AppSettings:ServiceURLAG"];
                var ServiceURLFileTima = _configuration["AppSettings:ServiceURLFileTima"];

                var query = await unitOfWork.LoanBriefFileRepository.Query(x => x.IsDeleted != 1 && x.LoanBriefId == req.LoanBriefId
                    && (req.TypeFile == 0 || x.TypeFile == req.TypeFile) && x.Status != 0, null, false)
                    .Select(LMSLoanBriefFilesDetail.ProjectionDetail).ToListAsync();
                var totalRecords = query.Count;
                List<LMSLoanBriefFilesDetail>
                data = query.ToList();
                var list = new List<LMSLoanBriefFilesDetail2>();

                var dicFiles = new Dictionary<int, List<LMSLoanBriefFilesDetail>>();
                foreach (var item in data)
                {
                    if (item.TypeId > 0)
                    {
                        var key = item.TypeId.Value;
                        if (!dicFiles.ContainsKey(key))
                            dicFiles[key] = new List<LMSLoanBriefFilesDetail>();
                        dicFiles[key].Add(item);
                    }
                    else
                    {
                        if (!dicFiles.ContainsKey(0))
                            dicFiles[0] = new List<LMSLoanBriefFilesDetail>();
                        dicFiles[0].Add(item);
                    }
                }

                if (dicFiles.Keys.Count > 0)
                {

                    //lấy thông tin sản phẩm và hình thức sở hữu nhà
                    var allDocument = dicFiles.Keys.Select(x => x).ToList();
                    try
                    {
                        var loanInfo = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.LoanBriefId, null, false).Select(x => new
                        {
                            ProductId = x.ProductId,
                            ResidentType = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType : null
                        }).FirstOrDefault();
                        if (loanInfo != null
                            && (loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                            || loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                            || loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))
                        {
                            var typeOwnerShipId = Common.Utils.ProductPriceUtils.ConvertEnumTypeOfOwnerShipDocument(loanInfo.ResidentType.Value);
                            var dataOrder = unitOfWork.ConfigDocumentRepository.Query(x => x.ProductId == loanInfo.ProductId
                                && x.TypeOwnerShip == typeOwnerShipId && allDocument.Contains(x.DocumentTypeId.Value), null, false)
                                .OrderBy(x => x.Sort.GetValueOrDefault(100)).Select(x => x.DocumentTypeId.Value).ToList();
                            if (dataOrder != null && dataOrder.Count > 0)
                            {
                                foreach (var item in dataOrder)
                                {
                                    if (!allDocument.Contains(item))
                                        allDocument.Add(item);
                                }
                            }
                        }
                    }
                    catch
                    {
                    }
                    foreach (var key in allDocument)
                    {
                        var listFile = dicFiles[key];
                        if (listFile != null && listFile.Count > 0)
                        {

                            var listPath = new List<LstFilePath>();
                            await Task.Run(() => Parallel.ForEach(listFile, item =>
                            {
                                if (!string.IsNullOrEmpty(item.FileThumb))
                                    item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                                if (!string.IsNullOrEmpty(item.FilePath))
                                {
                                    if (!item.FilePath.Contains("http") && (item.MecashId == 0))
                                        item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                                    if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                                        item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                                    if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                                    {
                                        item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                                    }
                                }
                                else if (!string.IsNullOrEmpty(item.LinkImgOfPartner))
                                {
                                    item.FilePath = item.DomainOfPartner + item.LinkImgOfPartner;
                                }

                                if (string.IsNullOrEmpty(item.FileThumb))
                                    item.FileThumb = item.FilePath;

                                if (!string.IsNullOrEmpty(item.FilePath))
                                {
                                    var objFile = new LstFilePath
                                    {
                                        FilePath = item.FilePath,
                                        TypeFile = item.TypeFile,
                                        FileThumb = item.FileThumb
                                    };
                                    listPath.Add(objFile);
                                }

                            }));
                            if (key == 0)
                            {
                                var objItem = new LMSLoanBriefFilesDetail2
                                {
                                    TypeId = 0,
                                    TypeName = "Chứng từ khác",
                                    LstFilePath = listPath
                                };
                                list.Add(objItem);
                            }
                            else
                            {
                                var objItem = new LMSLoanBriefFilesDetail2
                                {
                                    TypeId = listFile[0].TypeId,
                                    TypeName = listFile[0].TypeName,
                                    LstFilePath = listPath
                                };
                                list.Add(objItem);
                            }
                        }
                    }
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = list;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/ListImage Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //đổi hình thức GN
        [HttpPost]
        [Route("change_disbursement")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<Meta>> ChangeDisbusement([FromBody] Models.LMS.LoanBriefReq.LMSChangeDisbursement req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                //lưu log request
                var json = JsonConvert.SerializeObject(req);
                LogRequest("ChangeDisbusement", json);
                if (req.LoanbriefId <= 0 || !Enum.IsDefined(typeof(EnumDisbursement), req.DisbursementBy))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.LoanbriefId);
                if (loanInfo != null)
                {
                    if (loanInfo.InProcess == 1)
                    {
                        def.meta = new Meta(212, "Đơn vay đang được xử lý luồng. Vui lòng thử lại sau!");
                        return Ok(def);
                    }
                    if (loanInfo.Status != EnumLoanStatus.WAIT_DISBURSE.GetHashCode())
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đã chuyển sang bước khác. Vui lòng thử lại sau!");
                        return Ok(def);
                    }

                    //if (loanInfo.IsLock == true)
                    //{
                    //    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đang bị khóa. Vui lòng mở khóa đơn vay");
                    //    return Ok(def);
                    //}

                    if ((loanInfo.TypeDisbursement == EnumDisbursement.DISBURSE_AUTO.GetHashCode() && req.DisbursementBy == EnumDisbursement.DISBURSE_ACC.GetHashCode()) ||
                        (loanInfo.TypeDisbursement == EnumDisbursement.DISBURSE_ACC.GetHashCode() && req.DisbursementBy == EnumDisbursement.DISBURSE_AUTO.GetHashCode()))
                    {
                        unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                        {
                            TypeDisbursement = req.DisbursementBy
                        });
                        unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote
                        {
                            LoanBriefId = req.LoanbriefId,
                            Note = string.Format("LMS chuyển hình thức GN từ {0} sang {1}: {2}", ExtentionHelper.GetDescription((EnumDisbursement)loanInfo.TypeDisbursement), ExtentionHelper.GetDescription((EnumDisbursement)req.DisbursementBy), req.Note),
                            FullName = req.UserName,
                            Status = 1,
                            ActionComment = EnumActionComment.LMSChangeDisbursement.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 0,
                            ShopId = 0,
                            ShopName = "LMS"
                        });
                        unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, string.Format("Không thể chuyển hình thức GN từ {0} sang {1}", ExtentionHelper.GetDescription((EnumDisbursement)loanInfo.TypeDisbursement), ExtentionHelper.GetDescription((EnumDisbursement)req.DisbursementBy)));
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/ChangeDisbusement Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("check_banklog")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<Meta>> CheckBankLog([FromQuery] int LoanbriefId)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                //lưu log request
                if (LoanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(LoanbriefId);
                if (loanInfo != null)
                {
                    if (loanInfo.Status == EnumLoanStatus.CANCELED.GetHashCode())
                    {
                        def.meta = new Meta(211, "Đơn vay đang bị hủy!");
                        return Ok(def);
                    }
                    if (loanInfo.InProcess == EnumInProcess.Process.GetHashCode())
                    {
                        def.meta = new Meta(212, "Đơn vay đang được xử lý!");
                        return Ok(def);
                    }
                    if (loanInfo.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                    {
                        def.meta = new Meta(215, "Đơn vay đã được GN!");
                        return Ok(def);
                    }
                    var listBank = _banklogService.GetListTransacsion(LoanbriefId);
                    if (listBank != null && listBank.Count > 0)
                    {
                        if (listBank.Any(x => !x.HasResult.HasValue && x.CreatedAt.AddMinutes(8) >= DateTime.Now))
                        {
                            def.meta = new Meta(216, "KH đang có giao dịch chưa có kết quả trong khoản thời gian 8p từ khi khởi tạo");
                            return Ok(def);
                        }
                        else if (listBank.Any(x => !x.HasResult.HasValue && (x.VerifyResponseCode == null || (x.VerifyResponseCode == "004" && x.FromBank == "NamA"))))
                        {
                            def.meta = new Meta(217, "Giao dịch xảy ra lỗi. Cân đối soát với bank");
                            return Ok(def);
                        }
                        else if (listBank.Any(x => x.HasResult.HasValue && (x.VerifyResponseCode == "00" || x.VerifyResponseCode == "000")))
                        {
                            def.meta = new Meta(218, "Khách hàng có giao dịch chuyển tiền trên app thành công");
                            return Ok(def);
                        }
                    }

                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);

                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/CheckBankLog Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("all_loan")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LoanDetail>>>> GetAllLoan()
        {
            var def = new DefaultResponse<Meta, List<LoanDetail>>();
            try
            {

                var query = unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.DISBURSED, null, false).Select(LoanDetail.Projection).OrderByDescending(x => x.LoanCredit);
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                var data = await query.ToListAsync();
                if (data != null && data.Count > 0)
                {
                    var listLoanbriefId = query.Select(x => x.LoanCredit).Distinct();
                    var dicCity = unitOfWork.ProvinceRepository.Get(null, null, false).ToDictionary(x => x.ProvinceId, x => x.Name);
                    var dicDistrict = unitOfWork.DistrictRepository.Get(null, null, false).ToDictionary(x => x.DistrictId, x => x.Name);
                    var dicWard = unitOfWork.WardRepository.Get(null, null, false).ToDictionary(x => x.WardId, x => x.Name);

                    var dicResident = unitOfWork.LoanBriefResidentRepository.Get(x => listLoanbriefId.Contains(x.LoanBriefResidentId), null, false).ToDictionary(x => x.LoanBriefResidentId, x => x.Address);
                    var dicHouseHold = unitOfWork.LoanBriefHouseholdRepository.Get(x => listLoanbriefId.Contains(x.LoanBriefHouseholdId), null, false).ToDictionary(x => x.LoanBriefHouseholdId, x => x.Address);

                    await Task.Run(() => Parallel.ForEach(data, item =>
                    {
                        if (dicResident.ContainsKey(item.LoanCredit))
                        {
                            item.Address = dicResident[item.LoanCredit];
                        }
                        if (dicHouseHold.ContainsKey(item.LoanCredit))
                        {
                            item.AddressHouseHold = dicHouseHold[item.LoanCredit];
                        }
                        if (item.HouseHoldCityId > 0 && dicCity.ContainsKey(item.HouseHoldCityId))
                        {
                            item.HouseHoldCity = dicCity[item.HouseHoldCityId];
                        }
                        if (item.HouseHoldDistrictId > 0 && dicDistrict.ContainsKey(item.HouseHoldDistrictId))
                        {
                            item.HouseHoldDistrict = dicDistrict[item.HouseHoldDistrictId];
                        }
                        if (item.HouseHoldWardId > 0 && dicWard.ContainsKey(item.HouseHoldWardId))
                        {
                            item.HouseHoldWard = dicWard[item.HouseHoldWardId];
                        }
                        if (item.CityId > 0 && dicCity.ContainsKey(item.CityId))
                        {
                            item.CityName = dicCity[item.CityId];
                        }
                        if (item.DistrictId > 0 && dicDistrict.ContainsKey(item.DistrictId))
                        {
                            item.DistrictName = dicDistrict[item.DistrictId];
                        }
                        if (item.WardId > 0 && dicWard.ContainsKey(item.WardId))
                        {
                            item.WardName = dicWard[item.WardId];
                        }

                        if (item.CompanyCityId > 0 && dicCity.ContainsKey(item.CompanyCityId))
                        {
                            item.CompanyCity = dicCity[item.CompanyCityId];
                        }
                        if (item.CompanyDistrictId > 0 && dicDistrict.ContainsKey(item.CompanyDistrictId))
                        {
                            item.CompanyDistrict = dicDistrict[item.CompanyDistrictId];
                        }
                        if (item.CompanyWardId > 0 && dicWard.ContainsKey(item.CompanyWardId))
                        {
                            item.CompanyWard = dicWard[item.CompanyWardId];
                        }
                        if (item.ResidentType > 0 && Enum.IsDefined(typeof(EnumTypeofownership), item.ResidentType))
                        {
                            item.ResidentName = ExtentionHelper.GetDescription((EnumTypeofownership)item.ResidentType);
                        }
                    }));

                }
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/GetAllLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        private void LogRequest(string name, string json)
        {
            Log.Information(string.Format("{0}: {1}", name, json));
        }

        [HttpGet]
        [Route("SearchLoanAll")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefSearchDetail>>> SearchLoanAll([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] int loanBriefId = -1, [FromQuery] string search = null)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefSearchDetail>>();
            try
            {
                if (loanBriefId == 0)
                    loanBriefId = -1;
                List<LoanBriefSearchDetail> data;
                if (loanBriefId != -1 || search != null)
                {
                    var checkNumber = false;
                    if (search != null)
                        checkNumber = Common.Extensions.ConvertExtensions.IsNumber(search);

                    var filter = FilterHelper<LoanBrief>.Create(x => loanBriefId == -1 || x.LoanBriefId == loanBriefId);
                    //Kiểm tra là số và độ dài chuỗi là 9 || 12 => CMT
                    if (checkNumber == true && (search.Length == (int)NationalCardLength.CMND_QD || search.Length == (int)NationalCardLength.CCCD || search.Length == (int)NationalCardLength.CMND))
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => search == null || x.NationalCard == search));
                    //Kiểm tra là số và độ dài chuỗi là 10 => SDT
                    if (checkNumber == true && search.Length == 10)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => search == null || x.Phone == search));
                    if (checkNumber == false && search != null)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => search == null || x.FullName.ToLower() == search.ToLower()));

                    var query = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefSearchDetail.ProjectionDetail);
                    var totalRecords = query.Count();
                    if (skip == -1)
                        data = query.OrderByDescending(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    else
                    {
                        data = query.Skip(skip).Take(take).ToList();
                        pageSize = take;
                    }
                    if (data != null)
                    {
                        foreach (var item in data)
                        {

                            item.strStatus = Description.GetDescription((EnumLoanStatus)item.Status);
                        }
                    }
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                    def.data = data;
                }
                else
                {
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, 0, 0, 0);
                    def.data = null;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/SearchLoanAll Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("can_topup")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta>>> CanTopup([FromBody] List<CanTopupItem> reqs)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                unitOfWork.LogCallApiRepository.Insert(new LogCallApi
                {
                    ActionCallApi = ActionCallApi.LMSCallIsCanTopup.GetHashCode(),
                    LinkCallApi = "PushLoanLender",
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.Success,
                    LoanBriefId = 0,
                    Input = JsonConvert.SerializeObject(reqs),
                    CreatedAt = DateTime.Now
                });
                await unitOfWork.SaveAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/CanTopup Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("loan_info")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanInfoItem>>> GetLoanInfo(string param)
        {
            var def = new DefaultResponse<Meta, LoanInfoItem>();
            try
            {
                if (string.IsNullOrEmpty(param))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                    return Ok(def);
                }
                var sLoanbriefId = EncryptUtils.Decrypt(param, false);
                int id = 0;
                if (!string.IsNullOrEmpty(sLoanbriefId))
                {
                    int.TryParse(sLoanbriefId, out id);
                    if (id > 0)
                    {
                        var loanbrief = await unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == id, null, false)
                            .Select(LoanInfoDetail.Projection).FirstOrDefaultAsync();
                        if (loanbrief != null)
                        {
                            if (loanbrief.Status == (int)EnumLoanStatus.CANCELED)
                                loanbrief.StatusOfLoanView = ExtentionHelper.GetDescription(EnumLoanStatus.CANCELED);
                            else if (loanbrief.Status == (int)EnumLoanStatus.DISBURSED)
                                loanbrief.StatusOfLoanView = ExtentionHelper.GetDescription(EnumLoanStatus.DISBURSED);
                            else if (loanbrief.Status == (int)EnumLoanStatus.LENDER_LOAN_DISTRIBUTING || loanbrief.Status == (int)EnumLoanStatus.WAIT_DISBURSE)
                            {
                                loanbrief.StatusOfLoanView = "Chờ giải ngân";
                                loanbrief.StatusOfLoan = 4;
                            }
                            else if (loanbrief.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_REVIEW || loanbrief.Status == (int)EnumLoanStatus.WAIT_APPRAISER_CONFIRM_CUSTOMER
                                || loanbrief.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_APPROVE_PROPOSION || loanbrief.Status == (int)EnumLoanStatus.APPROVAL_LEADER_APPROVE_CANCEL
                                || loanbrief.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_APPROVE_CANCEL || loanbrief.Status == (int)EnumLoanStatus.BOD_REVIEW)
                            {
                                loanbrief.StatusOfLoanView = "Chờ phê duyệt";
                                loanbrief.StatusOfLoan = 3;
                            }
                            else if (loanbrief.Status == (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING || loanbrief.Status == (int)EnumLoanStatus.APPRAISER_REVIEW
                               || loanbrief.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE)
                            {
                                loanbrief.StatusOfLoanView = "Chờ thẩm định";
                                loanbrief.StatusOfLoan = 2;
                            }
                            else
                            {
                                loanbrief.StatusOfLoanView = "Chờ xử lý";
                                loanbrief.StatusOfLoan = 1;
                            }

                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            def.data = loanbrief;
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tồn tại đơn vay trong hệ thống");
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Tham số truyền vào không đúng");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                }

                return Ok(def);

            }
            catch (Exception ex)
            {
                Log.Information("Param: " + param);
                Log.Error(param + ex, "LMS/GetLoanInfo Exception param");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("waiting_money_disbursement_lender")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta, List<WaitingMoneyDisbursementLenderModel>>>> WaitingMoneyDisbursementLender()
        {
            var def = new DefaultResponse<Meta, List<WaitingMoneyDisbursementLenderModel>>();
            try
            {
                var data = await unitOfWork.LoanBriefRepository.Query(x => x.InProcess != EnumInProcess.Process.GetHashCode()
                && x.Status == (int)EnumLoanStatus.WAIT_DISBURSE, null, false)
                    .GroupBy(k => new { k.LenderName, k.LenderId }).Select(x => new WaitingMoneyDisbursementLenderModel()
                    {
                        LenderId = x.Key.LenderId.Value,
                        LenderName = x.Key.LenderName,
                        TotalMoney = x.Sum(n => n.LoanAmount.Value)
                    }).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/WaitingMoneyDisbursementLender Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_va_account")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta>>> UpdateVaAccount([FromBody] VaAccountReq entity)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (entity.LoanbriefId <= 0 || string.IsNullOrEmpty(entity.VaAccountName)
                    || string.IsNullOrEmpty(entity.VaAccountNumner) || string.IsNullOrEmpty(entity.VaBankName))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền đủ các thông tin");
                    return Ok(def);
                }
                //Add logCallApi
                unitOfWork.LogCallApiRepository.Insert(new LogCallApi
                {
                    ActionCallApi = ActionCallApi.UpdateVaAccount.GetHashCode(),
                    LinkCallApi = "UpdateVaAccount",
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.Success,
                    LoanBriefId = 0,
                    Input = JsonConvert.SerializeObject(entity),
                    CreatedAt = DateTime.Now
                });
                 unitOfWork.Save();
                var loanbrief = await unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(x => new
                {
                    LoanBriefId = x.LoanBriefId,
                    CustomerId = x.CustomerId
                }).FirstOrDefaultAsync();
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    unitOfWork.CustomerRepository.Update(x => x.CustomerId == loanbrief.CustomerId, x => new Customer()
                    {
                        VaAccountName = entity.VaAccountName,
                        VaAccountNumber = entity.VaAccountNumner,
                        VaBankCode = entity.VaBankCode,
                        VaBankName = entity.VaBankName
                    });
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tìm thấy thông tin đơn vay");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/UpdateVAAccount Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_phoneother")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> UpdatePhoneOther(UpdatePhoneOther entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.application.ToLower() == "ag" || entity.application.ToLower() == "erp")
                {
                    var loanInfo = unitOfWork.LoanBriefRepository.GetById(entity.loanbriefId);
                    if (loanInfo == null)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tồn tại hợp đồng trên hệ thống");
                        return Ok(def);
                    }
                    if (!entity.phone.ValidPhone())
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không đúng định dạng");
                        return Ok(def);
                    }

                    if (entity.phone == loanInfo.Phone)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại khác không được giống với số chính");
                        return Ok(def);
                    }

                    if (!string.IsNullOrEmpty(loanInfo.PhoneOther) && loanInfo.PhoneOther == entity.phone)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại khác giống trên hệ thống");
                        return Ok(def);
                    }

                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.loanbriefId, x => new LoanBrief()
                    {
                        PhoneOther = entity.phone
                    });
                    //Thêm vào LoanBriefNote
                    var loanbriefnote = new LoanBriefNote()
                    {
                        LoanBriefId = entity.loanbriefId,
                        FullName = entity.application,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        Note = !string.IsNullOrEmpty(loanInfo.PhoneOther) ? entity.application + " Cập nhập số điện thoại khác từ: " + loanInfo.PhoneOther + " thành: " + entity.phone : entity.application + " Thêm mới số điện thoại khác: " + entity.phone,
                        Status = 1,
                        CreatedTime = DateTime.Now,
                    };
                    unitOfWork.LoanBriefNoteRepository.Insert(loanbriefnote);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Bạn không có quyền cập nhập PhoneOther");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/UpdatePhoneOther Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_disbursement_loan_by_date")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<List<DisbursementLoanResponse>>> GetDisbursementLoanByDate([FromQuery] string fromDate, [FromQuery] string toDate, [FromQuery] int type)
        {
            var def = new DefaultResponse<List<DisbursementLoanResponse>>();
            try
            {
                var producer = new ProcedureManager<DisbursementLoanResponse>(unitOfWork.GetContext());
                var dFromDate = DateTime.ParseExact(fromDate, "dd-MM-yyyy", null);
                var dToDate = DateTime.ParseExact(toDate, "dd-MM-yyyy", null);
                dToDate = new DateTime(dToDate.Year, dToDate.Month, dToDate.Day, 23, 59, 59, 999);
                SqlParameter[] parameters = new SqlParameter[3] {
                new SqlParameter(){
                    SqlDbType = System.Data.SqlDbType.DateTime,
                    ParameterName = "@FromDate",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = dFromDate
                },
                new SqlParameter(){
                    SqlDbType = System.Data.SqlDbType.DateTime,
                    ParameterName = "@ToDate",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = dToDate
                },
                new SqlParameter(){
                    SqlDbType = System.Data.SqlDbType.Int,
                    ParameterName = "@Type",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = type
                },
            };
                var result = producer.ExecuteReader("sp_GetDisbursementTime", System.Data.CommandType.StoredProcedure, parameters);
                def.meta = new Meta(200, "success");
                if (result != null && result.Count > 0)
                {
                    foreach (var r in result)
                    {
                        r.ReceiveTime = r.ReceiveTime.AddHours(7);
                        // sai giờ > + 7h!!!!!
                        r.Diff = (int)(r.DisbursementTime - r.ReceiveTime).TotalSeconds;
                    }
                }
                def.data = result;
            }
            catch (Exception ex)
            {
                def.meta = new Meta(500, "internal server error");
                Log.Error(ex, "LMS/GetDisbursementLoanByDate Exception");
            }
            return Ok(def);
        }


        //Đơn vay chờ giải ngân bị khóa
        [HttpGet]
        [Route("disbursement_waiting_block")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<Meta, List<LenderLoanBriefDetail>>> DisbursementWaitingBlock([FromQuery] int LenderId = 0)
        {
            var def = new DefaultResponse<Meta, List<LenderLoanBriefDetail>>();
            try
            {
                var query = unitOfWork.LoanBriefRepository
                    .Query(x => x.InProcess != EnumInProcess.Process.GetHashCode()
                        && (LenderId == 0 || x.LenderId == LenderId)
                        && (x.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode() || x.Status == EnumLoanStatus.LENDER_LOAN_DISTRIBUTING.GetHashCode())
                        && x.IsLock == true, null, false).Select(LenderLoanBriefDetail.ProjectionLenderDetail);
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/DisbursementWaitingBlock Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("EmployeeLoan")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<Meta, List<LoanbriefInfoEmployeeDetail>>> GetEmployeeLoanbrief(List<int> loanIds)
        {
            var def = new DefaultResponse<Meta, List<LoanbriefInfoEmployeeDetail>>();
            try
            {
                var query = unitOfWork.LoanBriefRepository
                    .Query(x => x.Status == EnumLoanStatus.DISBURSED.GetHashCode()
                        && x.LmsLoanId > 0
                        && loanIds.Contains(x.LmsLoanId.Value)
                        , null, false).Select(LoanbriefInfoEmployeeDetail.ProjectionDetail);
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/EmployeeLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("update_gpay_account")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta>>> UpdateGpayAccount([FromBody] GpayAccountReq entity)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (entity.LoanbriefId <= 0 || string.IsNullOrEmpty(entity.GpayAccountName)
                    || string.IsNullOrEmpty(entity.GpayAccountNumner))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền đủ các thông tin");
                    return Ok(def);
                }
                //Add logCallApi
                var logCallApi = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.UpdateGpayAccount.GetHashCode(),
                    LinkCallApi = "UpdateGpayAccount",
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.Success,
                    LoanBriefId = 0,
                    Input = JsonConvert.SerializeObject(entity),
                    CreatedAt = DateTime.Now
                };
                unitOfWork.LogCallApiRepository.Insert(logCallApi);
                unitOfWork.Save();
                var customerId = await unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(x => x.CustomerId).FirstOrDefaultAsync();
                if (customerId > 0)
                {
                    unitOfWork.CustomerRepository.Update(x => x.CustomerId == customerId, x => new Customer()
                    {
                        GpayAccountName = entity.GpayAccountName,
                        GpayAccountNumber = entity.GpayAccountNumner,
                        GpayBankName = entity.GpayBankName
                    });
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tìm thấy thông tin đơn vay");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/UpdateGpayAccount Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        /// <summary>
        /// API đánh dấu mua/ bỏ mua bảo hiểm cho lender
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insurance_lender")]
        [AuthorizeToken("APP_API_LMS")]
        public ActionResult<DefaultResponse<Meta>> BuyInsuranceLender([FromBody] InsuranceLenderModel req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LoanbriefId <= 0 || string.IsNullOrEmpty(req.UserName))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.LoanbriefId);
                if (loanInfo != null)
                {
                    if (loanInfo.Status == EnumLoanStatus.CANCELED.GetHashCode())
                    {
                        def.meta = new Meta(211, "Đơn vay đang bị hủy!");
                        return Ok(def);
                    }
                    if (loanInfo.InProcess == EnumInProcess.Process.GetHashCode())
                    {
                        def.meta = new Meta(212, "Đơn vay đang được xử lý!");
                        return Ok(def);
                    }

                    if (loanInfo.IsLock == true)
                    {
                        def.meta = new Meta(213, "Đơn vay đang bị khóa. Vui lòng mở khóa đơn vay!");
                        return Ok(def);
                    }
                    if (loanInfo.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                    {
                        def.meta = new Meta(215, "Đơn vay đang ở trạng thái đã giải ngân!");
                        return Ok(def);
                    }

                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                    {
                        BuyInsuranceLender = req.BuyInsurance
                    });
                    var note = new LoanBriefNote
                    {
                        LoanBriefId = req.LoanbriefId,
                        Note = string.Format("Đơn vay HĐ-{0} được cập nhật {1} bảo hiểm lender", req.LoanbriefId, req.BuyInsurance ? "<b>mua</b>" : "<b>không mua</b>"),
                        FullName = string.IsNullOrEmpty(req.UserName) ? "" : req.UserName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 0,
                        ShopId = 0,
                        ShopName = "LMS"
                    };
                    unitOfWork.LoanBriefNoteRepository.Insert(note);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);

                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/BuyInsuranceLender Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("relationship")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta>>> AddRelationship([FromBody] AddRelationshipModel entity)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (entity.LoanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanbriefId);
                if (loanbrief != null)
                {
                    if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đang bị hủy!");

                    var relationship = new LoanBriefRelationship
                    {
                        LoanBriefId = entity.LoanbriefId,
                        FullName = entity.FullName,
                        Phone = entity.Phone,
                        RelationshipType = entity.TypeId,
                        CreatedFrom = "THN",
                        CreatedDate = DateTime.Now
                    };
                    unitOfWork.LoanBriefRelationshipRepository.Insert(relationship);

                    var note = new LoanBriefNote
                    {
                        LoanBriefId = entity.LoanbriefId,
                        Note = string.Format("Đơn vay HĐ-{0} được cập nhật người thân {1} từ THN", entity.LoanbriefId, entity.FullName),
                        FullName = string.IsNullOrEmpty(entity.UserName) ? "" : entity.UserName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 0,
                        ShopId = 0,
                        ShopName = "THN"
                    };
                    unitOfWork.LoanBriefNoteRepository.Insert(note);
                    await unitOfWork.SaveAsync();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/AddRelationship Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_relative_family")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta, List<RelativeFamilyDetail>>>> GetRelativeFamily()
        {
            var def = new DefaultResponse<Meta, List<RelativeFamilyDetail>>();
            try
            {
                var lstData = await unitOfWork.RelativeFamilyRepository.Query(x => x.IsEnable == true, null, false).
                    Select(RelativeFamilyDetail.ProjectionDetail).ToListAsync();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/GetRelativeFamily Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("get_property_info")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LMSLoanBriefPropertyDetail>>>> GetPropertyInfo(List<int> listLoanbriefId)
        {
            var def = new DefaultResponse<Meta, List<LMSLoanBriefPropertyDetail>>();
            try
            {
                var lstData = await unitOfWork.LoanBriefPropertyRepository.Query(x => listLoanbriefId.Contains(x.LoanBriefPropertyId), null, false).
                    Select(LMSLoanBriefPropertyDetail.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/GetPropertyInfo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_loan_brief_detail")]
        [AuthorizeToken("APP_API_LMS")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanBriefDetailApi>>> GetLoanBriefDetail(int loanBriefId)
        {
            var def = new DefaultResponse<Meta, LoanBriefDetailApi>();
            try
            {
                if (loanBriefId > 0)
                {
                    var data = await unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanBriefId, null, false)
                        .Select(LoanBriefDetailApi.ProjectionDetail).FirstOrDefaultAsync();
                    if (data != null && data.LoanBriefId > 0)
                    {
                        //lấy Ngành nghề
                        if (data.WorkInformation != null && data.WorkInformation.JobDescriptionId.HasValue)
                            data.WorkInformation.JobDescriptionName = await unitOfWork.JobRepository.Query(x => x.JobId == data.WorkInformation.JobDescriptionId.Value, null, false)
                                .Select(x => x.Name).FirstOrDefaultAsync();

                        //lấy thông tin tiền vào bảo hiểm
                        if (data.LoanAmountInfomation != null)
                        {
                            data.LoanAmountInfomation.LoanAmountReceived = data.LoanAmountInfomation.LoanAmount;
                            if (data.LoanAmountInfomation.BuyInsurenceCustomer == true)
                            {
                                data.LoanAmountInfomation.LoanAmountReceived -= data.LoanAmountInfomation.FeeInsuranceOfCustomer;
                                data.LoanAmountInfomation.TotalFeeInsurence += data.LoanAmountInfomation.FeeInsuranceOfCustomer;
                            }
                            if (data.LoanAmountInfomation.BuyInsuranceProperty == true)
                            {
                                data.LoanAmountInfomation.LoanAmountReceived -= data.LoanAmountInfomation.FeeInsuranceOfProperty;
                                data.LoanAmountInfomation.TotalFeeInsurence += data.LoanAmountInfomation.FeeInsuranceOfProperty;
                            }

                        }

                        //lấy thông tin fraud check
                        var logFraudCheck = await unitOfWork.LogLoanInfoAiRepository.Query(x => x.LoanbriefId == data.LoanBriefId
                        && x.ServiceType == (int)ServiceTypeAI.FraudDetection, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();
                        if (logFraudCheck != null)
                        {
                            if (!string.IsNullOrEmpty(logFraudCheck.Response))
                            {
                                try
                                {
                                    var dataFraudCheck = JsonConvert.DeserializeObject<DAL.Object.ResultFraudCheckModel>(logFraudCheck.Response);
                                    if (dataFraudCheck != null)
                                    {
                                        var fraudCheckModel = new FraudCheckInfomation();
                                        if (dataFraudCheck.tima_result != null && dataFraudCheck.tima_result.unified_result != null)
                                        {
                                            var dataTima = dataFraudCheck.tima_result.unified_result;
                                            if (!string.IsNullOrEmpty(dataTima.channel))
                                                fraudCheckModel.Channel = dataTima.channel;
                                            if (dataTima.details != null && dataTima.details.Count > 0)
                                            {
                                                foreach (var item in dataTima.details)
                                                    if (!string.IsNullOrEmpty(item.message))
                                                        fraudCheckModel.Message.Add(item.message);
                                            }
                                        }

                                        if (dataFraudCheck.hyperverge_result != null && dataFraudCheck.hyperverge_result.unifiedResult != null)
                                        {
                                            var dataHyperveger = dataFraudCheck.hyperverge_result.unifiedResult;
                                            if (!string.IsNullOrEmpty(dataHyperveger.channel))
                                                fraudCheckModel.HvChannel = dataHyperveger.channel;
                                            if (dataHyperveger.details != null && !string.IsNullOrEmpty(dataHyperveger.details.message))
                                            {
                                                fraudCheckModel.HvMessage.Add(dataHyperveger.details.message);
                                            }
                                        }
                                        if (dataFraudCheck.tima_result != null && !string.IsNullOrEmpty(dataFraudCheck.tima_result.report_url))
                                            fraudCheckModel.DetailUrl = dataFraudCheck.tima_result.report_url;

                                        data.FraudCheckInfomation = fraudCheckModel;
                                    }

                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }

                        //lấy thông tin ekyc
                        data.EkycInfomation = await unitOfWork.ResultEkycRepository.Query(x => x.LoanbriefId == loanBriefId, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();

                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = data;
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tìm thấy thông tin đơn vay.");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Vui lòng nhập mã đơn vay.");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/GetLoanBriefDetail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}