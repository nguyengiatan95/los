﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using LOS.AppAPI.DTOs.App;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models.LanDingPage;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.CareSoft;
using LOS.AppAPI.Service.LmsService;
using LOS.Common.Extensions;
using LOS.Common.Utils;
using LOS.DAL.Dapper;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using static LOS.AppAPI.Models.LanDingPage.LandingPage;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    //[Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class LandingPageController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration configuration;
        private readonly IErp _erpService;
        public LandingPageController(IUnitOfWork unitOfWork, IConfiguration configuration, IErp erpService)
        {
            this._unitOfWork = unitOfWork;
            this.configuration = configuration;
            _erpService = erpService;
        }

        [HttpPost]
        [EnableCors("AllowAll")]
        [Route("CreateLoan")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> CreateLoan(CreateLoan req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (!req.Phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không đúng định dạng!");
                    return Ok(def);
                }

                var checkErp = _erpService.CheckEmployeeTima(req.NationalCard, req.Phone);
                if (checkErp != null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng là nhân viên Tima!");
                    return Ok(def);
                }

                //Check xem khách hàng có đơn vay nào đang xử lý hay không
                if (_unitOfWork.LoanBriefRepository.Any(x => x.Phone == req.Phone
                && x.Status != EnumLoanStatus.CANCELED.GetHashCode() && x.Status != EnumLoanStatus.FINISH.GetHashCode()))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý!");
                    return Ok(def);
                }

                //Kiểm tra đơn cũ xem có phải là đơn CPQL của MMO không
                //nếu phải kiểm tra xem ngày tạo đã quá 1 tháng chưa
                //chưa được 1 tháng không cho tạo đơn nữa
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == req.Phone, null, false).OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                if (loanInfo != null && (loanInfo.Tid == "at-237" || loanInfo.Tid == "mo-255" || loanInfo.Tid == "rt-cpql-xemay")
                    && (DateTime.Now - loanInfo.CreatedTime.Value).TotalDays < 32)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đã tồn tại trong hệ thống!");
                    return Ok(def);
                }

                //xử lý tên KH
                if (!string.IsNullOrEmpty(req.FullName))
                {
                    if (!req.FullName.IsNormalized())
                        req.FullName = req.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                    req.FullName = req.FullName.ReduceWhitespace();
                    req.FullName = req.FullName.TitleCaseString();
                }
                var obj = new LoanBrief();
                var objCus = new Customer();
                //Check xem customer đã tồn tại hay chưa
                var customerInfo = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == req.Phone);
                if (customerInfo != null && customerInfo.CustomerId > 0)
                    obj.CustomerId = customerInfo.CustomerId;
                else
                {
                    objCus.CreatedAt = DateTime.Now;
                    objCus.FullName = req.FullName;
                    objCus.Phone = req.Phone;
                    objCus.ProvinceId = req.ProvinceId;
                    objCus.DistrictId = req.DistrictId;
                    objCus.WardId = req.WardId;
                    objCus.NationalCard = req.NationalCard;
                    _unitOfWork.CustomerRepository.Insert(objCus);
                    _unitOfWork.Save();
                    obj.CustomerId = objCus.CustomerId;
                }

                if (req.ProvinceId > 0 || req.DistrictId > 0 || req.WardId > 0)
                {
                    obj.LoanBriefResident = new LoanBriefResident()
                    {
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        WardId = req.WardId
                    };
                }

                //Check xem có đúng khu vực không
                //Không đúng thì cho đơn ở trạng thái treo
                if (req.ProvinceId == 0 || req.DistrictId == 0)
                {
                    obj.Status = EnumLoanStatus.CANCELED.GetHashCode();
                    obj.PipelineState = -1;
                    obj.ReasonCancel = 660;
                    obj.FirstProcessingTime = DateTime.Now;
                }
                else
                {
                    obj.Status = EnumLoanStatus.INIT.GetHashCode();
                }


                obj.IsHeadOffice = true;
                obj.CreatedTime = DateTime.Now;
                obj.FullName = req.FullName;
                obj.Phone = req.Phone;
                obj.NationalCard = req.NationalCard;
                obj.LoanAmountFirst = req.LoanAmount;
                obj.LoanAmount = req.LoanAmount;
                obj.LoanTime = req.LoanTime;
                obj.ProvinceId = req.ProvinceId;
                obj.DistrictId = req.DistrictId;
                obj.WardId = req.WardId;
                obj.ProductId = req.ProductId;
                obj.UtmSource = req.UtmSource;
                obj.UtmMedium = req.UtmMedium;
                obj.UtmCampaign = req.UtmCampaign;
                obj.UtmContent = req.UtmContent;
                obj.UtmTerm = req.UtmTerm;
                obj.DomainName = req.DomainName;
                obj.GoogleClickId = req.GoogleClickId;
                obj.PlatformType = EnumPlatformType.LandingPage.GetHashCode();
                obj.AffCode = req.Aff_Code;
                obj.AffSid = req.Aff_Sid;
                obj.Tid = req.TId;
                obj.IsVerifyOtpLdp = req.IsVerifyOtpLDP;
                obj.TypeLoanBrief = LoanBriefType.Customer.GetHashCode();
                obj.LoanBriefNote = new List<LoanBriefNote>();
                obj.LoanBriefNote.Add(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = string.Format("Đơn vay được khởi tạo từ landingpage"),
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                });

                if (obj.Status == EnumLoanStatus.CANCELED.GetHashCode())
                {
                    obj.LoanBriefNote.Add(new LoanBriefNote()
                    {
                        FullName = "Auto System",
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        Note = string.Format("Đơn bị hủy do không thuộc khu vực hỗ trợ"),
                        Status = 1,
                        CreatedTime = DateTime.Now,
                        ShopId = 3703,
                        ShopName = "Tima",
                    });
                }

                //insert to db
                _unitOfWork.LoanBriefRepository.Insert(obj);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = obj.LoanBriefId;
                if (obj.LoanBriefId > 0)
                {
                    //Lưu lại log khi tạo đơn
                    var objLogCreate = new LogInfoCreateLoanbrief()
                    {
                        LoanBriefId = obj.LoanBriefId,
                        FullName = req.FullName,
                        Phone = req.Phone,
                        LoanAmount = req.LoanAmount,
                        LoanTime = req.LoanTime,
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        ProductId = req.ProductId,
                        UtmSource = req.UtmSource,
                        UtmMedium = req.UtmMedium,
                        UtmCampaign = req.UtmCampaign,
                        UtmContent = req.UtmContent,
                        UtmTerm = req.UtmTerm,
                        DomainName = req.DomainName,
                        PlatformType = EnumPlatformType.LandingPage.GetHashCode(),
                        NationalCard = req.NationalCard,
                        AffCode = req.Aff_Code,
                        AffSid = req.Aff_Sid,
                        Tid = req.TId,
                        GoogleClickId = req.GoogleClickId,
                        IsVerifyOtpLdp = req.IsVerifyOtpLDP,
                        CreateDate = DateTime.Now,
                    };
                    _unitOfWork.LogInfoCreateLoanbriefRepository.Insert(objLogCreate);
                    _unitOfWork.Save();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/CreateLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [EnableCors("AllowAll")]
        [Route("get_districts")]
        public ActionResult<DefaultResponse<Meta, List<District>>> GetDistricts(int provinceId, int isapply)
        {
            var def = new DefaultResponse<Meta, List<District>>();
            try
            {
                //Lấy danh sách quận huyện theo ProvinceId và IsApply
                var lstDistrict = _unitOfWork.DistrictRepository.Query(x => x.ProvinceId == provinceId && x.IsApply == isapply, null, false).OrderBy(x => x.Priority).ToList();
                if (lstDistrict != null && lstDistrict.Count > 0)
                {
                    foreach (var item in lstDistrict)
                    {
                        if (item.Type == 1)
                            item.Name = "Quận " + item.Name;
                        else if (item.Type == 2)
                            item.Name = "Huyện " + item.Name;
                        else if (item.Type == 4)
                            item.Name = "Thành phố " + item.Name;
                        else if (item.Type == 5)
                            item.Name = "Thị xã " + item.Name;
                    }
                }
                def.meta = new Meta(200, "success");
                def.data = lstDistrict;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetDistricts Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //List danh mục chứng từ theo sản phẩm
        [Route("ListDocument")]
        [HttpGet]
        public ActionResult<DefaultResponse<DocumentTypeDetail>> ListDocument(int ProductId)
        {
            var def = new DefaultResponse<List<DocumentTypeDetail>>();
            var documents = _unitOfWork.DocumentTypeRepository.Query(x => x.ProductId == ProductId && x.IsEnable == 1, null, false).Select(DocumentTypeDetail.ProjectionDetail).ToList();
            def.meta = new Meta(200, "success");
            def.data = documents;
            return Ok(def);
        }

        [Route("UploadImage")]
        [HttpPost]
        public async Task<IActionResult> UploadImage(IFormFile files, int LoanBriefId, int TypeId)
        {
            //var userId = GetUserId();
            //var user = _unitOfWork.UserRepository.GetById(userId);
            var AccessKeyS3 = configuration["AppSettings:AccessKeyS3"];
            var RecsetAccessKeyS3 = configuration["AppSettings:RecsetAccessKeyS3"];
            var ServiceURL = configuration["AppSettings:ServiceURLFileTima"];
            var BucketName = configuration["AppSettings:BucketName"];
            var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
            {
                RegionEndpoint = RegionEndpoint.APSoutheast1
            });
            var link = "";
            var folder = BucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
            var lst = new List<LoanBriefFiles>();
            var def = new DefaultResponse<Meta, List<LoanBriefFiles>>();
            if (files == null)
            {
                def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                return Ok(def);
            }
            try
            {
                var extension = Path.GetExtension(files.FileName);
                var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                if (allowedExtensions.Contains(extension.ToLower()))
                {
                    string ImageName = Guid.NewGuid().ToString() + extension;
                    using (var stream = new MemoryStream())
                    {
                        files.CopyTo(stream);
                        PutObjectRequest req = new PutObjectRequest()
                        {
                            InputStream = stream,
                            BucketName = folder,
                            Key = ImageName,
                            CannedACL = S3CannedACL.PublicRead
                        };
                        await client.PutObjectAsync(req);
                        link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                        //insert to db
                        var obj = new LoanBriefFiles
                        {
                            LoanBriefId = LoanBriefId,
                            CreateAt = DateTime.Now,
                            FilePath = link,
                            UserId = 0,
                            Status = 1,
                            TypeId = TypeId,
                            S3status = 1,
                            MecashId = 1000000000
                        };
                        _unitOfWork.LoanBriefFileRepository.Insert(obj);
                        _unitOfWork.Save();
                        lst.Add(obj);
                    }
                }
                else
                {
                    def.meta = new Meta(211, "Ảnh không đúng định dạng!");
                    return Ok(def);
                }


                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lst.ToList();

                if (def.data != null && def.data.Count > 0)
                {
                    foreach (var item in def.data)
                        item.FilePath = ServiceURL + item.FilePath;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Landingpage/UploadImage Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //danh sách log voice otp theo sđt
        [HttpGet]
        [Route("get_log_send_voiceotp")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LogSendVoiceOtp>>>> GetLogSendVoiceOtp(string phone)
        {
            var def = new DefaultResponse<Meta, List<LogSendVoiceOtp>>();
            try
            {
                //lấy danh sách log voice otp theo sđt
                var db = new DapperHelper(configuration.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("SELECT * FROM LogSendVoiceOtp WHERE DATEDIFF(DAY, CreatedDate, GETDATE()) = 0 AND Status = 'OK'");
                sql.AppendFormat(" AND Phone = '{0}'", phone);
                var lstLogVoiceOtp = await db.QueryAsync<LogSendVoiceOtp>(sql.ToString());
                def.meta = new Meta(200, "success");
                def.data = lstLogVoiceOtp;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetLogSendVoiceOtp Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //lưu log send voiceotp
        [HttpPost]
        [Route("create_log_send_voiceotp")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> CreateLogSendVoiceOtp(CreateLogSendVoiceOtp req)
        {
            var def = new DefaultResponse<object>();
            var obj = new LogSendVoiceOtp();
            try
            {
                obj.Phone = req.Phone;
                obj.Otp = req.Otp;
                obj.Domain = req.Domain;
                obj.Token = req.Token;
                obj.Url = req.Url;
                obj.Status = req.Status;
                obj.Response = req.Response;
                obj.CreatedDate = DateTime.Now;
                //insert to db
                _unitOfWork.LogSendVoiceOtpRepository.Insert(obj);
                _unitOfWork.Save();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/CreateLogSendVoiceOtp Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //cập nhập lại trường verify otp
        [HttpPost]
        [Route("update_is_verify_otp")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> UpdateIsVerifyOtp(UpdateIsVerifyOtp req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief()
                {
                    IsVerifyOtpLdp = req.IsVerifyOtp
                });
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/UpdateIsVerifyOtp Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [AuthorizeToken("APP_API_AI")]
        [Route("create_loan_ai")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> CreateLoanAI(CreateLoanAI req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(req.Phone))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không được để trống!");
                    return Ok(def);
                }

                if (!req.Phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không đúng định dạng!");
                    return Ok(def);
                }

                var checkErp = _erpService.CheckEmployeeTima("", req.Phone);
                if (checkErp != null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng là nhân viên Tima!");
                    return Ok(def);
                }

                var obj = new LoanBrief();
                var objCus = new Customer();
                //Check xem khách hàng có đơn vay nào đang xử lý hay không
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == req.Phone, null, false).OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                if (loanInfo != null)
                {
                    if (loanInfo.Status != EnumLoanStatus.CANCELED.GetHashCode() && loanInfo.Status != EnumLoanStatus.FINISH.GetHashCode())
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý!");
                        var data = new ResponseAI()
                        {
                            LoanbriefId = loanInfo.LoanBriefId,
                            Url = string.Format(LOS.Common.Helpers.Const.url_tra_cuu, EncryptUtils.Encrypt(loanInfo.LoanBriefId.ToString(), false))
                        };
                        def.data = data;
                        return Ok(def);
                    }
                }
                if (string.IsNullOrEmpty(req.FullName))
                {
                    req.FullName = "Đơn chatbot";
                }
                else  //xử lý tên KH                
                {
                    if (!req.FullName.IsNormalized())
                        req.FullName = req.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                    req.FullName = req.FullName.ReduceWhitespace();
                    req.FullName = req.FullName.TitleCaseString();
                }

                //Check xem customer đã tồn tại hay chưa
                var customerInfo = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == req.Phone);
                if (customerInfo != null && customerInfo.CustomerId > 0)
                    obj.CustomerId = customerInfo.CustomerId;
                else
                {
                    objCus.CreatedAt = DateTime.Now;
                    objCus.FullName = req.FullName;
                    objCus.Phone = req.Phone;
                    objCus.ProvinceId = req.ProvinceId;
                    objCus.DistrictId = req.DistrictId;
                    _unitOfWork.CustomerRepository.Insert(objCus);
                    _unitOfWork.Save();
                    obj.CustomerId = objCus.CustomerId;
                }
                if (req.ProvinceId > 0 || req.DistrictId > 0 || req.ResidentType > 0)
                {
                    obj.LoanBriefResident = new LoanBriefResident()
                    {
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        ResidentType = req.ResidentType
                    };
                }
                if (req.JobId > 0 || !string.IsNullOrWhiteSpace(req.CompanyName) || !string.IsNullOrWhiteSpace(req.CompanyAddress)
                    || req.ImcomeType > 0)
                {
                    obj.LoanBriefJob = new LoanBriefJob()
                    {
                        JobId = req.JobId,
                        CompanyAddress = req.CompanyAddress,
                        CompanyName = req.CompanyName,
                        ImcomeType = req.ImcomeType
                    };
                }
                //Check xem có đúng khu vực không
                //Không đúng thì cho đơn ở trạng thái treo
                if (req.ProvinceId == 0)
                {
                    obj.Status = EnumLoanStatus.CANCELED.GetHashCode();
                    obj.PipelineState = -1;
                    obj.ReasonCancel = 660;
                    obj.FirstProcessingTime = DateTime.Now;
                }
                else
                {
                    obj.Status = EnumLoanStatus.INIT.GetHashCode();
                }

                obj.IsHeadOffice = true;
                obj.CreatedTime = DateTime.Now;
                obj.FullName = req.FullName;
                obj.Phone = req.Phone;
                obj.LoanAmountFirst = req.LoanAmount;
                obj.LoanAmount = req.LoanAmount;
                obj.LoanTime = req.LoanTime;
                //obj.Status = EnumLoanStatus.INIT.GetHashCode();
                obj.ProvinceId = req.ProvinceId;
                obj.DistrictId = req.DistrictId;
                obj.ProductId = 2;
                obj.UtmSource = req.UtmSource;
                obj.UtmMedium = req.UtmMedium;
                obj.UtmCampaign = req.UtmCampaign;
                obj.UtmContent = req.UtmContent;
                obj.UtmTerm = req.UtmTerm;
                obj.DomainName = req.DomainName;
                obj.LoanPurpose = req.LoanPurpose;
                obj.PlatformType = EnumPlatformType.Ai.GetHashCode();
                obj.TypeLoanBrief = LoanBriefType.Customer.GetHashCode();
                obj.LoanBriefNote = new List<LoanBriefNote>();
                obj.LoanBriefNote.Add(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = string.Format("Đơn vay được khởi tạo từ AI"),
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                });
                //insert to db
                _unitOfWork.LoanBriefRepository.Insert(obj);
                _unitOfWork.Save();
                if (obj.LoanBriefId > 0)
                {
                    //Lưu ảnh
                    if (req.ListImages != null)
                    {
                        foreach (var item in req.ListImages)
                        {
                            var objImage = new LoanBriefFiles()
                            {
                                LoanBriefId = obj.LoanBriefId,
                                UserId = 0,
                                Status = 1,
                                S3status = 0,
                                TypeId = item.TypeId,
                                LinkImgOfPartner = item.LinkImgOfPartner,
                                DomainOfPartner = item.DomainOfPartner,
                                CreateAt = DateTime.Now
                            };
                            _unitOfWork.LoanBriefFileRepository.Insert(objImage);
                            _unitOfWork.Save();
                        }
                    }

                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    var data = new ResponseAI()
                    {
                        LoanbriefId = obj.LoanBriefId,
                        Url = string.Format(LOS.Common.Helpers.Const.url_tra_cuu, EncryptUtils.Encrypt(obj.LoanBriefId.ToString(), false))
                    };
                    def.data = data;
                    //Lưu lại log khi tạo đơn
                    var objLogCreate = new LogInfoCreateLoanbrief()
                    {
                        LoanBriefId = obj.LoanBriefId,
                        FullName = req.FullName,
                        Phone = req.Phone,
                        LoanAmount = req.LoanAmount,
                        LoanTime = req.LoanTime,
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        UtmSource = req.UtmSource,
                        UtmMedium = req.UtmMedium,
                        UtmCampaign = req.UtmCampaign,
                        UtmContent = req.UtmContent,
                        UtmTerm = req.UtmTerm,
                        DomainName = req.DomainName,
                        PlatformType = EnumPlatformType.Ai.GetHashCode(),
                        CreateDate = DateTime.Now,
                    };
                    _unitOfWork.LogInfoCreateLoanbriefRepository.Insert(objLogCreate);
                    _unitOfWork.Save();
                }
                else
                    def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/CreateLoanAI Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("APP_API_AI")]
        [Route("update_loan_ai")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> UpdateLoanAI(UpdateLoanAI req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //Check xem có khoản vay không
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.LoanBriefId, null, false, x => x.Customer, x => x.LoanBriefCompany, x => x.LoanBriefHousehold
              , x => x.LoanBriefJob, x => x.LoanBriefProperty, x => x.LoanBriefResident, x => x.LoanBriefRelationship, x => x.LoanBriefQuestionScript).FirstOrDefault();
                if (loanInfo == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không có mã đơn trên hệ thống!");
                    return Ok(def);
                }
                //check xem có phải đơn chatbox không
                if (loanInfo.PlatformType != (int)EnumPlatformType.Ai)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn của bạn không thể cập nhập!");
                    var dataUrl = new ResponseAI()
                    {
                        LoanbriefId = req.LoanBriefId,
                        Url = string.Format(LOS.Common.Helpers.Const.url_tra_cuu, EncryptUtils.Encrypt(req.LoanBriefId.ToString(), false))
                    };
                    def.data = dataUrl;
                    return Ok(def);
                }

                //check xem có phải đơn ơ phòng telesales thì mới cập nhập
                if (loanInfo.Status != (int)EnumLoanStatus.INIT && loanInfo.Status != (int)EnumLoanStatus.SALEADMIN_LOAN_DISTRIBUTING
                    && loanInfo.Status != (int)EnumLoanStatus.SYSTEM_TELESALE_DISTRIBUTING && loanInfo.Status != (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN
                    && loanInfo.Status != (int)EnumLoanStatus.TELESALE_ADVICE)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đang được xử lý không thể cập nhập!");
                    var dataUrl = new ResponseAI()
                    {
                        LoanbriefId = req.LoanBriefId,
                        Url = string.Format(LOS.Common.Helpers.Const.url_tra_cuu, EncryptUtils.Encrypt(req.LoanBriefId.ToString(), false))
                    };
                    def.data = dataUrl;
                    return Ok(def);
                }


                //check xem đơn đã bị hủy chưa
                if (loanInfo.Status == (int)EnumLoanStatus.CANCELED)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn đã bị hủy không thể cập nhập!");
                    return Ok(def);
                }
                if (string.IsNullOrEmpty(req.FullName))
                {
                    req.FullName = "Đơn chatbot";
                }
                else //xử lý tên KH               
                {
                    req.FullName = req.FullName.ReduceWhitespace();
                    req.FullName = req.FullName.TitleCaseString();
                }
                //Lấy ra customer để update lại
                var customerInfo = _unitOfWork.CustomerRepository.GetById(loanInfo.CustomerId);
                if (customerInfo != null && customerInfo.CustomerId > 0)
                {
                    //cập nhật thông tin customer
                    customerInfo.CreatedAt = DateTime.Now;
                    customerInfo.FullName = req.FullName;
                    customerInfo.ProvinceId = req.ProvinceId;
                    customerInfo.DistrictId = req.DistrictId;
                    customerInfo.Dob = req.DOB;
                    _unitOfWork.CustomerRepository.Update(customerInfo);
                    _unitOfWork.Save();
                }

                //Lưu ảnh
                if (req.ListImages != null)
                {
                    foreach (var item in req.ListImages)
                    {
                        var objImage = new LoanBriefFiles()
                        {
                            LoanBriefId = req.LoanBriefId,
                            UserId = 0,
                            Status = 1,
                            S3status = 0,
                            TypeId = item.TypeId,
                            LinkImgOfPartner = item.LinkImgOfPartner,
                            DomainOfPartner = item.DomainOfPartner,
                            CreateAt = DateTime.Now
                        };
                        _unitOfWork.LoanBriefFileRepository.Insert(objImage);
                        _unitOfWork.Save();
                    }
                }

                //gán lại các chỉ số
                if (req.QuestionBorrow.HasValue)
                {
                    if (loanInfo.LoanBriefQuestionScript == null || loanInfo.LoanBriefQuestionScript.Id == 0)
                    {
                        var objLoanBriefQuestionScript = new LoanBriefQuestionScript()
                        {
                            LoanBriefId = req.LoanBriefId,
                            QuestionBorrow = req.QuestionBorrow,
                            TypeJob = 0,
                            ChooseCareer = 0
                        };
                        _unitOfWork.LoanBriefQuestionScriptRepository.Insert(objLoanBriefQuestionScript);
                    }
                    else
                    {
                        _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBriefQuestionScript()
                        {
                            QuestionBorrow = req.QuestionBorrow
                        });
                    }
                    _unitOfWork.Save();
                }

                if (req.ProvinceId > 0 || req.DistrictId > 0 || req.ResidentType > 0)
                {
                    if (loanInfo.LoanBriefResident == null)
                    {
                        var objLoanBriefResident = new LoanBriefResident()
                        {
                            ProvinceId = req.ProvinceId,
                            DistrictId = req.DistrictId,
                            ResidentType = req.ResidentType,
                            LoanBriefResidentId = req.LoanBriefId
                        };
                        _unitOfWork.LoanBriefResidentRepository.Insert(objLoanBriefResident);
                    }
                    else
                    {
                        _unitOfWork.LoanBriefResidentRepository.Update(x => x.LoanBriefResidentId == req.LoanBriefId, x => new LoanBriefResident()
                        {
                            ProvinceId = req.ProvinceId,
                            DistrictId = req.DistrictId,
                            ResidentType = req.ResidentType
                        });
                    }
                    _unitOfWork.Save();
                }
                if (req.JobId > 0 || !string.IsNullOrWhiteSpace(req.CompanyName) || !string.IsNullOrWhiteSpace(req.CompanyAddress)
                    || req.ImcomeType > 0)
                {
                    if (loanInfo.LoanBriefJob == null)
                    {
                        var objLoanBriefJob = new LoanBriefJob()
                        {
                            JobId = req.JobId,
                            CompanyAddress = req.CompanyAddress,
                            CompanyName = req.CompanyName,
                            ImcomeType = req.ImcomeType,
                            LoanBriefJobId = req.LoanBriefId
                        };
                        _unitOfWork.LoanBriefJobRepository.Insert(objLoanBriefJob);
                    }
                    else
                    {
                        _unitOfWork.LoanBriefJobRepository.Update(x => x.LoanBriefJobId == req.LoanBriefId, x => new LoanBriefJob()
                        {
                            JobId = req.JobId,
                            CompanyAddress = req.CompanyAddress,
                            CompanyName = req.CompanyName,
                            ImcomeType = req.ImcomeType
                        });
                    }
                    _unitOfWork.Save();
                }



                if (req.Status == (int)EnumLoanStatus.CANCELED)
                {


                    _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief()
                    {
                        FullName = req.FullName,
                        LoanAmountFirst = req.LoanAmount,
                        LoanAmount = req.LoanAmount,
                        LoanTime = req.LoanTime,
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        UtmSource = req.UtmSource,
                        UtmMedium = req.UtmMedium,
                        UtmCampaign = req.UtmCampaign,
                        UtmContent = req.UtmContent,
                        UtmTerm = req.UtmTerm,
                        DomainName = req.DomainName,
                        LoanPurpose = req.LoanPurpose,
                        ReasonCancel = req.ReasonCancel,
                        Status = req.Status,
                        PipelineState = -1,
                        LoanBriefCancelAt = DateTime.Now,
                        Dob = req.DOB ?? null,
                        LoanBriefComment = "AI gọi hủy đơn vay"
                    });

                    //Lưu phần hủy đơn vào comment
                    var noteLoanBriefNote = "AI gọi hủy đơn vay";
                    if (req.ReasonCancel != null && req.ReasonCancel > 0)
                    {
                        var reasonCancel = _unitOfWork.ReasonCancelRepository.Query(x => x.Id == req.ReasonCancel, null, false).FirstOrDefault();
                        if (reasonCancel != null)
                            noteLoanBriefNote = reasonCancel.Reason;
                    }
                    var loanBriefNote = new LoanBriefNote()
                    {
                        FullName = "AI",
                        ActionComment = EnumActionComment.AICancelLoan.GetHashCode(),
                        Note = noteLoanBriefNote,
                        Status = 1,
                        CreatedTime = DateTime.Now,
                        ShopId = 3703,
                        ShopName = "Tima",
                        LoanBriefId = req.LoanBriefId
                    };
                    _unitOfWork.LoanBriefNoteRepository.Insert(loanBriefNote);
                    _unitOfWork.Save();
                }
                else
                {
                    _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief()
                    {
                        FullName = req.FullName,
                        LoanAmountFirst = req.LoanAmount,
                        LoanAmount = req.LoanAmount,
                        LoanTime = req.LoanTime,
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        UtmSource = req.UtmSource,
                        UtmMedium = req.UtmMedium,
                        UtmCampaign = req.UtmCampaign,
                        UtmContent = req.UtmContent,
                        UtmTerm = req.UtmTerm,
                        DomainName = req.DomainName,
                        LoanPurpose = req.LoanPurpose,
                        ReasonCancel = req.ReasonCancel,
                        Dob = req.DOB ?? null,
                        Step = req.step ?? null
                    });
                }
                _unitOfWork.Save();
                var data = new ResponseAI()
                {
                    LoanbriefId = req.LoanBriefId,
                    Url = string.Format(LOS.Common.Helpers.Const.url_tra_cuu, EncryptUtils.Encrypt(req.LoanBriefId.ToString(), false))
                };
                def.data = data;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/UpdateLoanAI Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_city")]
        public ActionResult<DefaultResponse<Meta, List<Province>>> GetProvinceIsApply()
        {
            var def = new DefaultResponse<Meta, List<Province>>();
            try
            {
                var lstProvince = _unitOfWork.ProvinceRepository.Query(x => x.IsApply == 1, null, false).OrderBy(x => x.ProvinceId).ToList();
                def.meta = new Meta(200, "success");
                def.data = lstProvince;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetProvinceIsApply Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_ward")]
        public ActionResult<DefaultResponse<Meta, List<Ward>>> GetWard(int districtId)
        {
            var def = new DefaultResponse<Meta, List<Ward>>();
            try
            {
                var lstWard = _unitOfWork.WardRepository.Query(x => x.DistrictId == districtId, null, false).OrderBy(x => x.WardId).ToList();
                def.meta = new Meta(200, "success");
                def.data = lstWard;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetWard Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_loanbrief_email")]
        public ActionResult<DefaultResponse<Meta, object>> UpdateLoanBriefEmail(UpdateLoanBriefEmail entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (loanBrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không tồn tại!");
                    return Ok(def);
                }

                if (!entity.Email.ValidEmail())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Email không đúng định dạng!");
                    return Ok(def);
                }
                else
                {
                    _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                    {
                        Email = entity.Email
                    });
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/UpdateLoanBriefEmail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);

            }
            return Ok(def);
        }

        [HttpGet]
        [EnableCors("AllowAll")]
        [Route("get_city_all")]
        public ActionResult<DefaultResponse<Meta, List<DAL.DTOs.ProvinceDetail>>> GetProvinceAll()
        {
            var def = new DefaultResponse<Meta, List<DAL.DTOs.ProvinceDetail>>();
            try
            {
                var lstProvince = _unitOfWork.ProvinceRepository.All().Select(DAL.DTOs.ProvinceDetail.ProjectionDetail).OrderBy(x => x.Priority).ThenBy(x => x.Name).ToList();
                if (lstProvince != null && lstProvince.Count > 0)
                {
                    foreach (var item in lstProvince)
                    {
                        item.Name = Description.GetDescription((TypeProvince)item.Type) + " " + item.Name;
                    }
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstProvince;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetProvinceAll Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [EnableCors("AllowAll")]
        [Route("get_district_all")]
        public ActionResult<DefaultResponse<Meta, List<DAL.DTOs.DistrictDetail>>> GetDistrictAll(int provinceId)
        {
            var def = new DefaultResponse<Meta, List<DAL.DTOs.DistrictDetail>>();
            try
            {
                var lstDistrict = _unitOfWork.DistrictRepository.Query(x => x.ProvinceId == provinceId, null, false).Select(DAL.DTOs.DistrictDetail.ProjectionDetail).OrderByDescending(x => x.IsApply).ThenBy(x => x.Priority).ToList();
                if (lstDistrict != null && lstDistrict.Count > 0)
                {
                    foreach (var item in lstDistrict)
                    {
                        item.Name = Description.GetDescription((TypeDistrict)item.Type) + " " + item.Name;

                    }
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstDistrict;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetDistrictAll Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [EnableCors("AllowAll")]
        [Route("get_ward_all")]
        public ActionResult<DefaultResponse<Meta, List<DAL.DTOs.WardDetail>>> GetWardAll(int districtId)
        {
            var def = new DefaultResponse<Meta, List<DAL.DTOs.WardDetail>>();
            try
            {
                var lstDistrict = _unitOfWork.WardRepository.Query(x => x.DistrictId == districtId, null, false)
                    .Select(DAL.DTOs.WardDetail.ProjectionDetail).OrderByDescending(x => x.WardId).ToList();
                if (lstDistrict != null && lstDistrict.Count > 0)
                {
                    foreach (var item in lstDistrict)
                    {
                        item.Name = Description.GetDescription((TypeWard)item.Type) + " " + item.Name;
                    }
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstDistrict;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetWardAll Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [EnableCors("AllowAll")]
        [Route("create_loan_push_to_partner")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> CreateLoanPushToPartner(CreateLoanPushToPartner entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (!entity.CustomerPhone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không đúng định dạng!");
                    return Ok(def);
                }
                //kiểm tra số điện thoại đã tồn tại chưa
                //tồn tại rồi không cho tạo nữa
                if (_unitOfWork.PushLoanToPartnerRepository.Any(x => x.CustomerPhone == entity.CustomerPhone))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại đã tồn tại trong hệ thống!");
                    return Ok(def);
                }

                //thêm đơn vào bảng
                var objPushLoanToPartner = new DAL.EntityFramework.PushLoanToPartner()
                {
                    CustomerName = entity.CustomerName,
                    CustomerPhone = entity.CustomerPhone,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    ProvinceName = entity.ProvinceName,
                    DistrictName = entity.DistrictName,
                    UtmSource = entity.UtmSource,
                    UtmMedium = entity.UtmMedium,
                    UtmCampaign = entity.UtmCampaign,
                    UtmContent = entity.UtmContent,
                    UtmTerm = entity.UtmTerm,
                    Domain = entity.Domain,
                    AffCode = entity.AffCode,
                    AffSid = entity.AffSId,
                    Tid = entity.TId,
                    PushAction = (int)ActionPushLoanToPartner.CreateNew,
                    Status = (int)StatusPushLoanToPartner.Processing,
                    CreateDate = DateTime.Now,
                };
                _unitOfWork.PushLoanToPartnerRepository.Insert(objPushLoanToPartner);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = objPushLoanToPartner.Id;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/CreateLoanPushToPartner Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
   [HttpPost]
        [Route("update_push_loan_to_partner_email")]
        public ActionResult<DefaultResponse<Meta, object>> UpdatePushLoanToPartnerEmail(LandingPage.UpdatePushLoanToPartnerEmail entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                if (!_unitOfWork.PushLoanToPartnerRepository.Any(x => x.Id == entity.Id))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không tồn tại!");
                    return Ok(def);
                }

                if (!entity.Email.ValidEmail())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Email không đúng định dạng!");
                    return Ok(def);
                }
                else
                {
                    _unitOfWork.PushLoanToPartnerRepository.Update(x => x.Id == entity.Id, x => new LOS.DAL.EntityFramework.PushLoanToPartner()
                    {
                        Email = entity.Email
                    });
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/UpdatePushLoanToPartnerEmail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);

            }
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("APP_API_AFFILIATE_TIMA")]
        [Route("create_loan_write_log")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> CreateLoanWriteLog(LandingPage.CreateLoanWriteLog req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (!req.Phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không đúng định dạng!");
                    return Ok(def);
                }

                var checkErp = _erpService.CheckEmployeeTima(req.NationalCard, req.Phone);
                if (checkErp != null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng là nhân viên Tima!");
                    return Ok(def);
                }

                //Check xem khách hàng có đơn vay nào đang xử lý hay không
                if (_unitOfWork.LoanBriefRepository.Any(x => x.Phone == req.Phone
                && x.Status != EnumLoanStatus.CANCELED.GetHashCode() && x.Status != EnumLoanStatus.FINISH.GetHashCode()))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay đang xử lý!");
                    return Ok(def);
                }

                //Kiểm tra đơn cũ xem có phải là đơn CPQL của MMO không
                //nếu phải kiểm tra xem ngày tạo đã quá 1 tháng chưa
                //chưa được 1 tháng không cho tạo đơn nữa
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == req.Phone, null, false)
                    .Select(x => new { Tid = x.Tid, LoanBriefId = x.LoanBriefId, CreatedTime = x.CreatedTime })
                    .OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                if (loanInfo != null && (loanInfo.Tid == "at-237" || loanInfo.Tid == "mo-255" || loanInfo.Tid == "rt-cpql-xemay")
                    && (DateTime.Now - loanInfo.CreatedTime.Value).TotalDays < 32)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đã tồn tại trong hệ thống!");
                    return Ok(def);
                }

                //xử lý tên KH
                if (!string.IsNullOrEmpty(req.FullName))
                {
                    if (!req.FullName.IsNormalized())
                        req.FullName = req.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                    req.FullName = req.FullName.ReduceWhitespace();
                    req.FullName = req.FullName.TitleCaseString();
                }
                var obj = new LoanBrief();
                var objCus = new Customer();
                //Check xem customer đã tồn tại hay chưa
                var customerInfo = _unitOfWork.CustomerRepository.Query(x => x.Phone == req.Phone, null, false)
                    .Select(x => new { CustomerId = x.CustomerId }).FirstOrDefault();
                if (customerInfo != null && customerInfo.CustomerId > 0)
                    obj.CustomerId = customerInfo.CustomerId;
                else
                {
                    objCus.CreatedAt = DateTime.Now;
                    objCus.FullName = req.FullName;
                    objCus.Phone = req.Phone;
                    objCus.ProvinceId = req.ProvinceId;
                    objCus.DistrictId = req.DistrictId;
                    objCus.WardId = req.WardId;
                    objCus.NationalCard = req.NationalCard;
                    _unitOfWork.CustomerRepository.Insert(objCus);
                    _unitOfWork.Save();
                    obj.CustomerId = objCus.CustomerId;
                }

                if (req.ProvinceId > 0 || req.DistrictId > 0 || req.WardId > 0)
                {
                    obj.LoanBriefResident = new LoanBriefResident()
                    {
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        WardId = req.WardId
                    };
                }

                //Check xem có đúng khu vực không
                //Không đúng thì cho đơn ở trạng thái treo
                if (req.ProvinceId == 0 || req.DistrictId == 0)
                {
                    obj.Status = EnumLoanStatus.CANCELED.GetHashCode();
                    obj.PipelineState = -1;
                    obj.ReasonCancel = 660;
                    obj.FirstProcessingTime = DateTime.Now;
                }
                else
                {
                    obj.Status = EnumLoanStatus.INIT.GetHashCode();
                }


                obj.IsHeadOffice = true;
                obj.CreatedTime = req.CreateDate;
                obj.FullName = req.FullName;
                obj.Phone = req.Phone;
                obj.NationalCard = req.NationalCard;
                obj.LoanAmountFirst = req.LoanAmount;
                obj.LoanAmount = req.LoanAmount;
                obj.LoanTime = req.LoanTime;
                obj.ProvinceId = req.ProvinceId;
                obj.DistrictId = req.DistrictId;
                obj.WardId = req.WardId;
                obj.ProductId = req.ProductId;
                obj.UtmSource = req.UtmSource;
                obj.UtmMedium = req.UtmMedium;
                obj.UtmCampaign = req.UtmCampaign;
                obj.UtmContent = req.UtmContent;
                obj.UtmTerm = req.UtmTerm;
                obj.DomainName = req.DomainName;
                obj.GoogleClickId = req.GoogleClickId;
                obj.PlatformType = EnumPlatformType.LandingPage.GetHashCode();
                obj.AffCode = req.Aff_Code;
                obj.AffSid = req.Aff_Sid;
                obj.Tid = req.TId;
                obj.IsVerifyOtpLdp = req.IsVerifyOtpLDP;
                obj.TypeLoanBrief = LoanBriefType.Customer.GetHashCode();
                obj.LoanBriefNote = new List<LoanBriefNote>();
                obj.LoanBriefNote.Add(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = string.Format("Đơn vay được khởi tạo từ landingpage"),
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                });

                if (obj.Status == EnumLoanStatus.CANCELED.GetHashCode())
                {
                    obj.LoanBriefNote.Add(new LoanBriefNote()
                    {
                        FullName = "Auto System",
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        Note = string.Format("Đơn bị hủy do không thuộc khu vực hỗ trợ"),
                        Status = 1,
                        CreatedTime = DateTime.Now,
                        ShopId = 3703,
                        ShopName = "Tima",
                    });
                }

                //insert to db
                _unitOfWork.LoanBriefRepository.Insert(obj);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = obj.LoanBriefId;
                if (obj.LoanBriefId > 0)
                {
                    //Lưu lại log khi tạo đơn
                    _unitOfWork.LogInfoCreateLoanbriefRepository.Insert(new LogInfoCreateLoanbrief()
                    {
                        LoanBriefId = obj.LoanBriefId,
                        FullName = req.FullName,
                        Phone = req.Phone,
                        LoanAmount = req.LoanAmount,
                        LoanTime = req.LoanTime,
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        ProductId = req.ProductId,
                        UtmSource = req.UtmSource,
                        UtmMedium = req.UtmMedium,
                        UtmCampaign = req.UtmCampaign,
                        UtmContent = req.UtmContent,
                        UtmTerm = req.UtmTerm,
                        DomainName = req.DomainName,
                        PlatformType = EnumPlatformType.LandingPage.GetHashCode(),
                        NationalCard = req.NationalCard,
                        AffCode = req.Aff_Code,
                        AffSid = req.Aff_Sid,
                        Tid = req.TId,
                        GoogleClickId = req.GoogleClickId,
                        IsVerifyOtpLdp = req.IsVerifyOtpLDP,
                        CreateDate = DateTime.Now,
                    });
                    _unitOfWork.Save();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/CreateLoanWriteLog Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpGet]
        [Route("get_type_of_owner_ship")]
        public ActionResult<DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>> GetTypeOfOwnerShip()
        {
            var def = new DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>();
            try
            {
                var lstData = new List<LandingPage.DefaultReponseEnum>();
                var lstTypeOfOwnerShip = ExtentionHelper.GetEnumToList(typeof(TypeOfOwnerShipApi));
                if (lstTypeOfOwnerShip != null)
                    lstData = lstTypeOfOwnerShip.Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetTypeOfOwnerShip Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_living_time")]
        public ActionResult<DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>> GetLivingTime()
        {
            var def = new DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>();
            try
            {
                var lstData = new List<LandingPage.DefaultReponseEnum>();
                var lstLivingTime = ExtentionHelper.GetEnumToList(typeof(EnumLivingTime));
                if (lstLivingTime != null)
                    lstData = lstLivingTime.Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetLivingTime Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_loan_purpose")]
        public ActionResult<DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>> GetLoanPurpose()
        {
            var def = new DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>();
            try
            {
                var lstData = new List<LandingPage.DefaultReponseEnum>();
                var lstLoadLoanPurpose = ExtentionHelper.GetEnumToList(typeof(EnumLoadLoanPurpose));
                if (lstLoadLoanPurpose != null)
                    lstData = lstLoadLoanPurpose.Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetLoanPurpose Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_job")]
        public ActionResult<DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>> GetJob()
        {
            var def = new DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>();
            try
            {
                var lstData = new List<LandingPage.DefaultReponseEnum>();
                var lstJob = ExtentionHelper.GetEnumToList(typeof(EnumJobTitle));
                if (lstJob != null)
                    lstData = lstJob.Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetJob Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);

        }

        [HttpGet]
        [Route("get_job_description")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DAL.DTOs.JobDetail>>>> GetJobDescription(int jobId)
        {
            var def = new DefaultResponse<Meta, List<DAL.DTOs.JobDetail>>();
            try
            {
                var lstData = await _unitOfWork.JobRepository.Query(x => x.ParentId.GetValueOrDefault(0) == jobId, null, false)
                    .Select(DAL.DTOs.JobDetail.ProjectionDetail)
                    .OrderBy(x => x.Priority).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetJobDescription Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);

        }

        [HttpGet]
        [Route("get_imcome_type")]
        public ActionResult<DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>> GetImcomeType()
        {
            var def = new DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>();
            try
            {
                var lstData = new List<LandingPage.DefaultReponseEnum>();
                var lstImcomeType = ExtentionHelper.GetEnumToList(typeof(EnumImcomeType));
                if (lstImcomeType != null)
                    lstData = lstImcomeType.Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetImcomeType Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);

        }

        [HttpGet]
        [Route("get_relative_family")]
        public ActionResult<DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>> GetRelativeFamily()
        {
            var def = new DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>();
            try
            {
                var lstData = new List<LandingPage.DefaultReponseEnum>();
                var lstRelativeFamily = ExtentionHelper.GetEnumToList(typeof(EnumRelativeFamily));
                if (lstRelativeFamily != null)
                    lstData = lstRelativeFamily.Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetRelativeFamily Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);

        }

        [HttpGet]
        [Route("get_loan_product")]
        public ActionResult<DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>> GetLoanProduct(int? productId)
        {
            var def = new DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>();
            try
            {
                var lstData = new List<LandingPage.DefaultReponseEnum>();
                var lstProductLoanSupport = ExtentionHelper.GetEnumToList(typeof(LoanProductSupport));
                if (lstProductLoanSupport != null)
                {
                    if (productId == null || productId == 0)
                        lstData = lstProductLoanSupport.Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                    else if (productId == (int)LoanProductSupport.MotorCreditType_CC)
                        lstData = lstProductLoanSupport.Where(x => Convert.ToInt32(x.Value) != (int)LoanProductSupport.OtoCreditType_CC)
                            .Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                }

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetLoanProduct Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);

        }

        [HttpGet]
        [Route("get_product_detail")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DAL.DTOs.ProductDetailModel>>>> GetProductDetail(int typeOwnership, int productId, int typeLoanBrief)
        {
            var def = new DefaultResponse<Meta, List<DAL.DTOs.ProductDetailModel>>();
            try
            {
                var lstData = new List<DAL.DTOs.ProductDetailModel>();
                //đơn vay doanh nghiệp
                if (typeLoanBrief == (int)TypeLoanBrief.DOANHNGHIEP)
                    lstData = await _unitOfWork.ConfigProductDetailRepository.Query(x => x.ProductId == productId
                    && x.TypeLoanBrief == typeLoanBrief && x.IsEnable == true, null, false)
                        .Select(DAL.DTOs.ProductDetailModel.ProjectionViewDetail).ToListAsync();
                //đơn vay cá nhân
                else
                    lstData = await _unitOfWork.ConfigProductDetailRepository.Query(x => x.TypeOwnership == typeOwnership
                    && x.ProductId == productId && x.IsEnable == true, null, false)
                        .Select(DAL.DTOs.ProductDetailModel.ProjectionViewDetail).ToListAsync();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetProductDetail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_type_loanbrief")]
        public ActionResult<DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>> GetTypeLoanbrief()
        {
            var def = new DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>();
            try
            {
                var lstData = new List<LandingPage.DefaultReponseEnum>();
                var lstTypeLoanbrief = ExtentionHelper.GetEnumToList(typeof(TypeLoanBrief));
                if (lstTypeLoanbrief != null)
                    lstData = lstTypeLoanbrief.Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetTypeLoanbrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);

        }

        [HttpGet]
        [Route("get_bank")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DAL.DTOs.BankDetail>>>> GetBank()
        {
            var def = new DefaultResponse<Meta, List<DAL.DTOs.BankDetail>>();
            try
            {
                var lstData = await _unitOfWork.BankRepository.All().Select(DAL.DTOs.BankDetail.ProjectionDetail).OrderBy(x => x.BankId).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetBank Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_brand_product")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DAL.DTOs.ProductBasicApi>>>> GetBrandProduct()
        {
            var def = new DefaultResponse<Meta, List<DAL.DTOs.BrandProductDetail>>();
            try
            {
                var lstData = await _unitOfWork.BrandProductRepository.Query(x => x.IsEnable == true, null, false)
                    .Select(DAL.DTOs.BrandProductDetail.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetBrandProduct Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_name_motobike")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DAL.DTOs.ProductBasicApi>>>> GetNameMotobike(int brandId)
        {
            var def = new DefaultResponse<Meta, List<DAL.DTOs.ProductBasicApi>>();
            try
            {
                var lstData = await _unitOfWork.ProductRepository.Query(x => x.BrandProductId == brandId && x.IsEnable == true, null, false)
                    .Select(DAL.DTOs.ProductBasicApi.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetNameMotobike Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_loan_time")]
        public ActionResult<DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>> GetLoanTime(int productId, int productDetailId)
        {
            var def = new DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>();
            try
            {
                var lstData = new List<LandingPage.DefaultReponseEnum>();
                var lstLoanTime = ExtentionHelper.GetEnumToList(typeof(EnumLoanTime));
                //tất cả
                if(productId == 0)
                {
                    lstData = lstLoanTime.Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                }
                //nếu là gói vay ô tô
                else if (productId == (int)EnumProductCredit.OtoCreditType_CC)
                {
                    lstData = lstLoanTime.Where(x => Convert.ToInt32(x.Value) == (int)EnumLoanTime.ThreeMonth
                    || Convert.ToInt32(x.Value) == (int)EnumLoanTime.SixMonth
                    || Convert.ToInt32(x.Value) == (int)EnumLoanTime.NineMonth
                    || Convert.ToInt32(x.Value) == (int)EnumLoanTime.TwentyMonth)
                            .Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                }
                //gói vay xe máy
                else if(productId == (int)EnumProductCredit.MotorCreditType_CC
                    || productId == (int)EnumProductCredit.MotorCreditType_KCC
                    || productId == (int)EnumProductCredit.MotorCreditType_KGT)
                {
                    List<int> listProductDetailFast = new List<int>(){
                         (int)Common.Extensions.InfomationProductDetail.KT1_KSHK_4tr,
                         (int)Common.Extensions.InfomationProductDetail.KT1_KGT_4tr,
                         (int)Common.Extensions.InfomationProductDetail.KT3_KTN_4tr,
                         (int)Common.Extensions.InfomationProductDetail.KT3_KGT_4tr,
                         (int)Common.Extensions.InfomationProductDetail.XMCC_KT1_5tr,
                         (int)Common.Extensions.InfomationProductDetail.XMCC_KT3_5tr
                     };
                    if (listProductDetailFast.Contains(productDetailId))
                    {
                        lstData = lstLoanTime.Where(x => Convert.ToInt32(x.Value) == (int)EnumLoanTime.SixMonth
                            || Convert.ToInt32(x.Value) == (int)EnumLoanTime.TwentyMonth)
                            .Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                    }
                    else
                    {
                        lstData = lstLoanTime.Where(x => Convert.ToInt32(x.Value) == (int)EnumLoanTime.TwentyMonth)
                            .Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                    }
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetLoanTime Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_rate_type")]
        public ActionResult<DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>> GetRateType(int productId, int productDetailId)
        {
            var def = new DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>();
            try
            {
                var lstData = new List<LandingPage.DefaultReponseEnum>();
                var lstRateType = ExtentionHelper.GetEnumToList(typeof(EnumRateType));
                //lấy tất cả
                if(productId == 0)
                {
                    lstData = lstRateType.Where(x => Convert.ToInt32(x.Value) == (int)EnumRateType.DuNoGiamDan
                            || Convert.ToInt32(x.Value) == (int)EnumRateType.TatToanCuoiKy)
                            .Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                }
                else
                {
                    List<int> listProductDetailFast = new List<int>(){
                         (int)Common.Extensions.InfomationProductDetail.KT1_KSHK_4tr,
                         (int)Common.Extensions.InfomationProductDetail.KT1_KGT_4tr,
                         (int)Common.Extensions.InfomationProductDetail.KT3_KTN_4tr,
                         (int)Common.Extensions.InfomationProductDetail.KT3_KGT_4tr,
                         (int)Common.Extensions.InfomationProductDetail.XMCC_KT1_5tr,
                         (int)Common.Extensions.InfomationProductDetail.XMCC_KT3_5tr
                     };
                    if (listProductDetailFast.Contains(productDetailId) || productId == (int)EnumProductCredit.OtoCreditType_CC)
                    {
                        lstData = lstRateType.Where(x => Convert.ToInt32(x.Value) == (int)EnumRateType.DuNoGiamDan
                            || Convert.ToInt32(x.Value) == (int)EnumRateType.TatToanCuoiKy)
                            .Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                    }
                    else
                    {
                        lstData = lstRateType.Where(x => Convert.ToInt32(x.Value) == (int)EnumRateType.DuNoGiamDan)
                            .Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                    }
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetRateType Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_ltv")]
        public ActionResult<DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>> GetLTV(int productId, int typeRemarketing)
        {
            var def = new DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>();
            try
            {
                var lstData = new List<LandingPage.DefaultReponseEnum>();
                if (productId == (int)EnumProductCredit.MotorCreditType_CC)
                {
                    lstData.Add(new LandingPage.DefaultReponseEnum() { Id = 70, Name = "70%" });
                    lstData.Add(new LandingPage.DefaultReponseEnum() { Id = 80, Name = "80%" });
                    if (typeRemarketing == (int)TypeRemarketingLtv.TopUp || typeRemarketing == (int)TypeRemarketingLtv.Reborrow)
                    {
                        lstData.Clear();
                        lstData.Add(new LandingPage.DefaultReponseEnum() { Id = 80, Name = "80%" });
                    }
                }
                else if (productId == (int)EnumProductCredit.MotorCreditType_KCC || productId == (int)EnumProductCredit.MotorCreditType_KGT)
                {
                    lstData.Add(new LandingPage.DefaultReponseEnum() { Id = 60, Name = "60%" });
                    lstData.Add(new LandingPage.DefaultReponseEnum() { Id = 70, Name = "70%" });
                    if (typeRemarketing == (int)TypeRemarketingLtv.TopUp || typeRemarketing == (int)TypeRemarketingLtv.Reborrow)
                    {
                        lstData.Clear();
                        lstData.Add(new LandingPage.DefaultReponseEnum() { Id = 70, Name = "70%" });
                    }
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetLTV Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_platform_type")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DAL.DTOs.PlatformTypeDetail>>>> GetPlatformType()
        {
            var def = new DefaultResponse<Meta, List<DAL.DTOs.PlatformTypeDetail>>();
            try
            {
                var lstData = await _unitOfWork.PlatformTypeRepository.Query(x => x.IsEnable == true, null, false)
                    .Select(DAL.DTOs.PlatformTypeDetail.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetPlatformType Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_home_network_mobile")]
        public ActionResult<DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>> GetHomeNetWorkMobile()
        {
            var def = new DefaultResponse<Meta, List<LandingPage.DefaultReponseEnum>>();
            try
            {
                var lstData = new List<LandingPage.DefaultReponseEnum>();
                var lstHomeNetWorkMobile = ExtentionHelper.GetEnumToList(typeof(HomeNetWorkMobile));
                if (lstHomeNetWorkMobile != null)
                    lstData = lstHomeNetWorkMobile.Select(x => new LandingPage.DefaultReponseEnum() { Id = Convert.ToInt32(x.Value), Name = x.Text }).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetHomeNetWorkMobile Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_reason_cancel_loan")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DAL.DTOs.ReasonCancelDetailApi>>>> GetReasonCancelLoan(int groupId, int productId)
        {
            var def = new DefaultResponse<Meta, List<DAL.DTOs.ReasonCancelDetailApi>>();
            try
            {
                var lstData = new List<DAL.DTOs.ReasonCancelDetailApi>();
                //Telesales
                if (groupId == (int)EnumGroupUser.Telesale || groupId == (int)EnumGroupUser.ManagerTelesales)
                {
                    //lý do hủy ô tô
                    if (productId == (int)EnumProductCredit.OtoCreditType_CC)
                    {
                        lstData = await _unitOfWork.ReasonCancelRepository.Query(x => x.Type == (int)EnumTypeReason.LoanCarTeleSalesCancel
                        && x.IsEnable.GetValueOrDefault(0) == 1, null, false)
                            .Select(DAL.DTOs.ReasonCancelDetailApi.ProjectionDetail).ToListAsync();
                    }
                    else
                    {
                        lstData = await _unitOfWork.ReasonCancelRepository.Query(x => x.Type == (int)EnumTypeReason.TeleSalesCancel
                        && x.IsEnable.GetValueOrDefault(0) == 1, null, false)
                            .Select(DAL.DTOs.ReasonCancelDetailApi.ProjectionDetail).ToListAsync();
                    }
                }
                //Hub
                else if (groupId == (int)EnumGroupUser.StaffHub || groupId == (int)EnumGroupUser.ManagerHub)
                {
                    lstData = await _unitOfWork.ReasonCancelRepository.Query(x => x.Type == (int)EnumTypeReason.HubCancel
                    && x.IsEnable.GetValueOrDefault(0) == 1, null, false)
                       .Select(DAL.DTOs.ReasonCancelDetailApi.ProjectionDetail).ToListAsync();
                }
                //TĐHS
                else if (groupId == (int)EnumGroupUser.ApproveEmp || groupId == (int)EnumGroupUser.ApproveLeader)
                {
                    lstData = await _unitOfWork.ReasonCancelRepository.Query(x => x.Type == (int)EnumTypeReason.Tdhs2Cancel
                    && x.IsEnable.GetValueOrDefault(0) == 1, null, false)
                       .Select(DAL.DTOs.ReasonCancelDetailApi.ProjectionDetail).ToListAsync();
                }

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/GetReasonCancelLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}