﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Utils;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.AppAPI.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using LOS.Common.Models.Request;
using LOS.Common.Extensions;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Authorization;
using LOS.AppAPI.DTOs.LMS;
using LOS.AppAPI.Models.LMS;
using Microsoft.Extensions.Configuration;
using RestSharp;
using Newtonsoft.Json;
using LOS.AppAPI.Models.App;
using LOS.AppAPI.DTOs.App;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.EntityFrameworkCore;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.DTOs.Reason;
using LOS.AppAPI.Service.LmsService;
using static LOS.AppAPI.Models.LMS.CalculatorMoneyInsurance;
using LOS.AppAPI.Models.Lender;
using LOS.DAL.Helpers;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [AuthorizeToken("APP_API_LMS")]
    public class LenderConfigController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ILmsService _lmsService;
        public LenderConfigController(IUnitOfWork unitOfWork, ILmsService lmsService)
        {
            this.unitOfWork = unitOfWork;
            _lmsService = lmsService;
        }

        [HttpGet]
        public ActionResult<DefaultResponse<object>> GetConfigs()
        {
            var def = new DefaultResponse<object>();
            try
            {
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                var data = unitOfWork.LenderConfigRepository.Query(x => x.Status == 1, null, false).ToList();
                foreach (var d in data)
                {
                    if (d.LockedMoney == 0)
                        d.InitialMoney = d.CurrentMoney;
                    else
                    {
                        d.CurrentMoney -= d.LockedMoney;
                    }
                }
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LenderConfig Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("upsert")]
        public ActionResult<DefaultResponse<object>> UpsertConfig([FromBody] LenderConfig lenderConfig)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (lenderConfig.LenderId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check exist               
                var config = unitOfWork.LenderConfigRepository.Query(x => x.LenderId == lenderConfig.LenderId, null, false).FirstOrDefault();
                if (config == null)
                {
                    // insert
                    config = new LenderConfig();
                    if (lenderConfig.LenderName.IndexOf("NA") == 0)
                        config.IsLender = true;
                    else
                        config.IsLender = false;
                    config.LenderId = lenderConfig.LenderId;
                    config.LenderName = lenderConfig.LenderName;
                    config.LockedMoney = 0;
                    config.CurrentMoney = lenderConfig.InitialMoney;
                    config.InitialMoney = lenderConfig.InitialMoney;
                    config.Priority = lenderConfig.Priority;
                    config.Received = 0;
                    config.Accepted = 0;
                    config.Retrieved = 0;
                    config.Returned = 0;
                    config.Status = lenderConfig.InitialMoney > 0 ? 1 : 0;
                    config.UpdatedTime = DateTime.Now;
                    unitOfWork.LenderConfigRepository.Insert(config); 
                    unitOfWork.SaveChanges();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    int status = lenderConfig.InitialMoney > 0 ? 1 : 0;
                    // Update                 
                    unitOfWork.LenderConfigRepository.Update(x => x.Id == config.Id, x => new LenderConfig()
                    {
                        LockedMoney = 0,
                        InitialMoney = lenderConfig.InitialMoney,
                        CurrentMoney = lenderConfig.InitialMoney,
                        Priority = lenderConfig.Priority,
                        UpdatedTime = DateTime.Now,
                        Status = status
                    });
                    unitOfWork.SaveChanges();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LenderConfig Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("upsert_many")]
        public ActionResult<DefaultResponse<object>> UpsertManyConfig([FromBody] List<LenderConfig> lenderConfigs)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (lenderConfigs != null && lenderConfigs.Count > 0)
                {
                    //update tất cả status config là 0
                    unitOfWork.LenderConfigRepository.Update(x => x.Id > 0, x => new LenderConfig()
                    {
                        Status = 0
                    });
                    foreach (var lenderConfig in lenderConfigs)
                    {
                        // check exist
                        var config = unitOfWork.LenderConfigRepository.Query(x => x.LenderId == lenderConfig.LenderId, null, false).FirstOrDefault();
                        if (config == null)
                        {
                            // insert
                            config = new LenderConfig();
                            if (lenderConfig.LenderName.IndexOf("NA") == 0)
                                config.IsLender = true;
                            else
                                config.IsLender = false;
                            config.LenderId = lenderConfig.LenderId;
                            config.LenderName = lenderConfig.LenderName;
                            config.LockedMoney = 0;
                            config.InitialMoney = lenderConfig.InitialMoney;
                            config.CurrentMoney = lenderConfig.InitialMoney;
                            config.Priority = lenderConfig.Priority;
                            config.Received = 0;
                            config.Accepted = 0;
                            config.Retrieved = 0;
                            config.Returned = 0;
                            config.Status = 1;
                            config.UpdatedTime = DateTime.Now;
                            unitOfWork.LenderConfigRepository.Insert(config); 
                            unitOfWork.SaveChanges();
                        }
                        else
                        {
                            // Update                 
                            unitOfWork.LenderConfigRepository.Update(x => x.Id == config.Id, x => new LenderConfig()
                            {
                                LockedMoney = 0,
                                InitialMoney = lenderConfig.InitialMoney,
                                CurrentMoney = lenderConfig.InitialMoney,
                                Priority = lenderConfig.Priority,
                                UpdatedTime = DateTime.Now,
                                Status = 1
                            });
                        }
                    }
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LenderConfig Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_auto_distributing_status")]
        public ActionResult<DefaultResponse<string>> GetAutoStatus()
        {
            var def = new DefaultResponse<string>();
            var currentConfig = unitOfWork.SystemConfigRepository.Query(x => x.Name == ConfigHelper.LENDER_AUTO_DISTRIBUTING_KEY, null, false).FirstOrDefault();
            if (currentConfig != null)
            {
                def.meta = new Meta(200, "success");
                def.data = currentConfig.Value.Equals("true") ? "ON" : "OFF";
            }
            else
            {
                def.meta = new Meta(500, "internal server error");
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("turn_onoff_auto_distributing")]
        public ActionResult<DefaultResponse<string>> TurnOnOffAuto()
        {
            var def = new DefaultResponse<string>();
            try
            {
                var currentConfig = unitOfWork.SystemConfigRepository.Query(x => x.Name == ConfigHelper.LENDER_AUTO_DISTRIBUTING_KEY, null, false).FirstOrDefault();
                if (currentConfig != null)
                {
                    if (currentConfig.Value.Equals("true"))
                    {
                        currentConfig.Value = "false";
                    }
                    else
                    {
                        currentConfig.Value = "true";
                    }
                    unitOfWork.SystemConfigRepository.Update(currentConfig);
                    unitOfWork.SaveChanges();
                    if (currentConfig.Value.Equals("false"))
                    {
                        // Nếu turn off thì rút hết các đơn đâng đc chia về 
                        var loanBriefIds = unitOfWork.LoanBriefRepository.Query(x => x.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED, null, false).Select(x => x.LoanBriefId).ToList();
                        if (loanBriefIds != null && loanBriefIds.Count > 0)
                        {
                            unitOfWork.LenderLoanBriefRepository.Update(x => loanBriefIds.Contains(x.LoanBriefId.Value), x => new LenderLoanBrief()
                            {
                                Status = (int)Common.Helpers.Const.AutoDistributeState.RETRIEVED,
                                UpdatedTime = DateTime.Now
                            });
                        }
                        //unitOfWork.LoanBriefRepository.Update(x => x.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED, x => new LoanBrief()
                        //{
                        //    AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.MANUAL_PUSH
                        //});
                    }
                    else
                    {
                        // Tìm các đơn ko đủ điều kiện
                        var loanBriefs = unitOfWork.LoanBriefRepository.Query(x => x.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.REQUIREMENT_NOT_MET, null, false).ToList();
                        if (loanBriefs != null && loanBriefs.Count > 0)
                        {
                            foreach (var loanBrief in loanBriefs)
                            {
                                unitOfWork.LenderLoanBriefRepository.Delete(x => x.LoanBriefId == loanBrief.LoanBriefId);
                            }
                            unitOfWork.SaveChanges();
                        }
                        unitOfWork.LoanBriefRepository.Update(x => x.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.REQUIREMENT_NOT_MET, x => new LoanBrief()
                        {
                            AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.READY_TO_DISTRIBUTE,
                            PriorityLevel = null
                        });
                    }
                    def.meta = new Meta(200, "success");
                    def.data = currentConfig.Value.Equals("true") ? "ON" : "OFF";
                }
                else
                {
                    def.meta = new Meta(500, "internal server error");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LenderConfig Exception");
                def.meta = new Meta(500, "internal server error");
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_auto_distributing_priority")]
        public ActionResult<DefaultResponse<string>> GetAutoPriority()
        {
            var def = new DefaultResponse<string>();
            var currentConfig = unitOfWork.SystemConfigRepository.Query(x => x.Name == ConfigHelper.LENDER_AUTO_DISTRIBUTING_LENDER_KEY, null, false).FirstOrDefault();
            if (currentConfig != null)
            {
                def.meta = new Meta(200, "success");
                def.data = currentConfig.Value.Equals("NA") ? "NA" : "DT";
            }
            else
            {
                def.meta = new Meta(500, "internal server error");
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("turn_auto_distributing_priority")]
        public ActionResult<DefaultResponse<string>> TurnAutoPriority()
        {
            var def = new DefaultResponse<string>();
            try
            {
                var currentConfig = unitOfWork.SystemConfigRepository.Query(x => x.Name == ConfigHelper.LENDER_AUTO_DISTRIBUTING_LENDER_KEY, null, false).FirstOrDefault();
                if (currentConfig != null)
                {
                    if (currentConfig.Value.Equals("NA"))
                    {
                        currentConfig.Value = "DT";
                    }
                    else
                    {
                        currentConfig.Value = "NA";
                    }
                    unitOfWork.SystemConfigRepository.Update(currentConfig);
                    unitOfWork.SaveChanges();
                    def.meta = new Meta(200, "success");
                    def.data = currentConfig.Value.Equals("NA") ? "NA" : "DT";
                }
                else
                {
                    def.meta = new Meta(500, "internal server error");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LenderConfig Exception");
                def.meta = new Meta(500, "internal server error");
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("reset_distributing")]
        public ActionResult<DefaultResponse<object>> ResetDistributing([FromBody] Models.LMS.LoanBriefReq.LMSLoanLock req)
        {
            var def = new DefaultResponse<object>();
            if (req.LoanId > 0)
            {
                var loanBrief = unitOfWork.LoanBriefRepository.GetById(req.LoanId);
                if (loanBrief != null)
                {
                    unitOfWork.LenderLoanBriefRepository.Delete(x => x.LoanBriefId == loanBrief.LoanBriefId);
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBrief.LoanBriefId, x => new LoanBrief()
                    {
                        AutoDistributeState = 0,
                        PriorityLevel = null
                    });
                    def.meta = new Meta(200, "success");
                    def.data = true;
                }
            }
            def.meta = new Meta(201, "failed");
            return Ok(def);
        }
    }
}
