﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Utils;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.AppAPI.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using LOS.Common.Models.Request;
using LOS.Common.Extensions;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Authorization;
using LOS.AppAPI.DTOs.LMS;
using LOS.AppAPI.Models.LMS;
using Microsoft.Extensions.Configuration;
using RestSharp;
using Newtonsoft.Json;
using LOS.AppAPI.Models.App;
using LOS.AppAPI.DTOs.App;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.EntityFrameworkCore;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.DTOs.Reason;
using LOS.AppAPI.Service.LmsService;
using static LOS.AppAPI.Models.LMS.CalculatorMoneyInsurance;
using LOS.AppAPI.Models.Lender;
using System.Net.Http;
using LOS.AppAPI.Models.ESign;
using System.Text;
using LOS.AppAPI.Service.AG;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [AuthorizeToken("APP_API_LENDER")]
    public class LenderController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ILmsService _lmsService;
        private readonly IAgService _agService;
        
        public LenderController(IUnitOfWork unitOfWork, ILmsService lmsService, IAgService agService)
        {
            this.unitOfWork = unitOfWork;
            _lmsService = lmsService;
            _agService = agService;
        }

        //Đơn vay chờ giải ngân
        [HttpGet]
        [Route("DisbursementWaiting")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LenderListDisbursementWaiting>>> DisbursementWaiting([FromQuery] int LenderId)
        {
            var def = new DefaultResponse<SummaryMeta, List<LenderListDisbursementWaiting>>();
            try
            {

                var query = unitOfWork.LoanBriefRepository
                    .Query(x => x.InProcess != EnumInProcess.Process.GetHashCode() && x.LenderId == LenderId
                        && x.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode()
                        && x.TypeDisbursement == (int)EnumDisbursement.DISBURSE_LEND
                    , null, false).Select(LenderListDisbursementWaiting.ProjectionLenderDisbursementWaiting);
                var totalRecords = query.Count();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, 0, 0, totalRecords);
                def.data = query.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/DisbursementWaiting Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        //Đơn vay cho nhiều ndt chờ giải ngân
        [HttpGet]
        [Route("LenderLoans")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LenderListDisbursementWaiting>>> LenderLoans([FromQuery] int LenderId)
        {
            var def = new DefaultResponse<SummaryMeta, List<LenderListDisbursementWaiting>>();
            try
            {

                var query = (from lb in unitOfWork.LoanBriefRepository.All()
                             join llb in unitOfWork.LenderLoanBriefRepository.All() on lb.LoanBriefId equals llb.LoanBriefId
                             where lb.Status == (int)EnumLoanStatus.LENDER_LOAN_DISTRIBUTING 
                                    && lb.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED 
                                    && llb.Status == (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED && llb.LenderId == LenderId
                             select lb).Select(LenderListDisbursementWaiting.ProjectionLenderDisbursementWaiting).ToList();
                var totalRecords = query.Count;
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, 0, 0, totalRecords);
                def.data = query;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/LenderLoans Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        //Chi tiết đơn vay
        [HttpGet]
        [Route("LoanDetail")]
        public ActionResult<DefaultResponse<Meta, LenderLoanBriefDetail>> GetById([FromQuery] int LoanId = 0, [FromQuery] int lenderId = 0)
        {
            var def = new DefaultResponse<Meta, LenderLoanBriefDetail>();
            try
            {
                if (LoanId <= 0 || lenderId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var data = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanId
                && (lenderId == 0 || x.LenderId == lenderId), null, false).Select(LenderLoanBriefDetail.ProjectionLenderDetail).FirstOrDefault();
                if (data != null)
                {
                    if (data.RateTypeId == EnumRateType.RateMonthADay.GetHashCode() || data.RateTypeId == EnumRateType.TatToanCuoiKy.GetHashCode())
                        data.RateTypeName = "Tất toán cuối kỳ";
                    else if (data.RateTypeId == EnumRateType.RateAmortization30Days.GetHashCode() || data.RateTypeId == EnumRateType.DuNoGiamDan.GetHashCode())
                        data.RateTypeName = "Dư nợ giảm dần";

                    if (data.LenderRate.GetValueOrDefault(0) == 0)
                    {
                        var timeAppliedRate = DateTime.Now;
                        if (data.EsignState == (int)LOS.Common.Extensions.EsignState.BORROWER_SIGNED || data.EsignState == (int)LOS.Common.Extensions.EsignState.LENDER_SIGNED)
                        {
                            var lastEsignCustomer = unitOfWork.EsignContractRepository.Query(x => x.LoanBriefId == data.LoanBriefId
                            && x.TypeEsign == (int)TypeEsign.Customer, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                            if (lastEsignCustomer != null && lastEsignCustomer.CreatedAt.HasValue)
                                timeAppliedRate = lastEsignCustomer.CreatedAt.Value;
                        }
                        //Tính lãi suất lender
                        var configRate = unitOfWork.ConfigContractRateRepository.Query(x => x.IsEnable == true
                                 && timeAppliedRate >= x.AppliedAt, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                        if (configRate != null && configRate.LenderRate > 0)
                            data.LenderRate = configRate.LenderRate.Value;
                    }
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/Detail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Chi tiết đơn vay
        [HttpGet]
        [Route("LoanDetailAuto")]
        public ActionResult<DefaultResponse<Meta, LenderLoanBriefDetail>> GetByIdAuto([FromQuery] int LoanId = 0, [FromQuery] int lenderId = 0)
        {
            var def = new DefaultResponse<Meta, LenderLoanBriefDetail>();
            try
            {
                if (LoanId <= 0 || lenderId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var data = (from lb in unitOfWork.LoanBriefRepository.All()
                            join llb in unitOfWork.LenderLoanBriefRepository.All() on lb.LoanBriefId equals llb.LoanBriefId
                            where (lb.Status == (int)EnumLoanStatus.LENDER_LOAN_DISTRIBUTING
                                    || lb.Status == (int)EnumLoanStatus.WAIT_DISBURSE.GetHashCode()) 
                                    && (llb.Status == (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED 
                                        || llb.Status == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING) 
                                    && llb.LenderId == lenderId && llb.LoanBriefId == LoanId
                            select lb).Select(LenderLoanBriefDetail.ProjectionLenderDetail).FirstOrDefault();
                if (data != null)
                {
                    //zz
                    if (data.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                    {
                        def.meta = new Meta(250, "DISBURSED");
                        return Ok(def);
                    }
                    data.LenderId = lenderId;
                    if (data.RateTypeId == EnumRateType.RateMonthADay.GetHashCode() || data.RateTypeId == EnumRateType.TatToanCuoiKy.GetHashCode())
                        data.RateTypeName = "Tất toán cuối kỳ";
                    else if (data.RateTypeId == EnumRateType.RateAmortization30Days.GetHashCode() || data.RateTypeId == EnumRateType.DuNoGiamDan.GetHashCode())
                        data.RateTypeName = "Dư nợ giảm dần";

                    if (data.LenderRate.GetValueOrDefault(0) == 0)
                    {
                        var timeAppliedRate = DateTime.Now;
                        if (data.EsignState == (int)LOS.Common.Extensions.EsignState.BORROWER_SIGNED || data.EsignState == (int)LOS.Common.Extensions.EsignState.LENDER_SIGNED)
                        {
                            var lastEsignCustomer = unitOfWork.EsignContractRepository.Query(x => x.LoanBriefId == data.LoanBriefId
                            && x.TypeEsign == (int)TypeEsign.Customer, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                            if (lastEsignCustomer != null && lastEsignCustomer.CreatedAt.HasValue)
                                timeAppliedRate = lastEsignCustomer.CreatedAt.Value;
                        }
                        //Tính lãi suất lender
                        var configRate = unitOfWork.ConfigContractRateRepository.Query(x => x.IsEnable == true
                                 && timeAppliedRate >= x.AppliedAt, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                        if (configRate != null && configRate.LenderRate > 0)
                            data.LenderRate = configRate.LenderRate.Value;
                    }
                }
                else
                {
                    def.meta = new Meta(255, "returned");
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/DetailAuto Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Đơn vay chờ giải ngân bị khóa
        [HttpGet]
        [Route("disbursement_waiting_block")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LenderLoanBriefDetail>>> DisbursementWaitingBlock([FromQuery] int LenderId)
        {
            var def = new DefaultResponse<SummaryMeta, List<LenderLoanBriefDetail>>();
            try
            {

                var query = unitOfWork.LoanBriefRepository
                    .Query(x => x.InProcess != EnumInProcess.Process.GetHashCode() && x.LenderId == LenderId
                        && (x.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode() || x.Status == EnumLoanStatus.LENDER_LOAN_DISTRIBUTING.GetHashCode())
                        && x.IsLock == true, null, false).Select(LenderLoanBriefDetail.ProjectionLenderDetail);
                var totalRecords = query.Count();

                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, 0, 0, totalRecords);
                def.data = query.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/DisbursementWaitingBlock Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        //List Note by LoanId
        [HttpGet]
        [Route("ListComment")]
        public ActionResult<DefaultResponse<Meta, List<LMSLoanBriefNoteDetail>>> GetNoteByLoanID([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int LoanId = 0)
        {
            var def = new DefaultResponse<SummaryMeta, List<LMSLoanBriefNoteDetail>>();
            try
            {
                if (LoanId <= 0)
                {
                    def.meta = new SummaryMeta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE, 0, 0, 0);
                    return Ok(def);
                }
                // check if loan action exists
                if (unitOfWork.LoanBriefRepository.Any(x => x.LoanBriefId == LoanId))
                {
                    var query = unitOfWork.LoanBriefNoteRepository.Query(x => x.LoanBriefId == LoanId, null, false).Select(LMSLoanBriefNoteDetail.ProjectionDetail);
                    var totalRecords = query.Count();
                    List<LMSLoanBriefNoteDetail> data = query.OrderByDescending(x => x.LoanBriefNoteId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                    def.data = data;
                }
                else
                {
                    def.meta = new SummaryMeta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE, 0, 0, 0);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/GetNoteByLoanID Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }


        //Trả lại đơn        
        [HttpPost]
        [Route("ReturnLoan")]
        public ActionResult<DefaultResponse<object>> ReturnLoan(Models.Lender.LenderReq.LenderReturnLoan req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.LoanBriefId <= 0 || req.Reason == "")
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.LoanBriefId, null, false).Select(x => new
                {
                    LoanBriefId = x.LoanBriefId,
                    Status = x.Status,
                    LenderId = x.LenderId,
                    InProcess = x.InProcess,
                    PipelineState = x.PipelineState,
                    AutoDistributeState = x.AutoDistributeState,
                    LoanAmount = x.LoanAmount,
                    LenderName = x.LenderName,
                    IsLock = x.IsLock
                }).FirstOrDefault();

                if (loanInfo != null)
                {
                    // kiếm tra trạng thái đơn
                    // Nếu đơn được chia thủ công
                    if (loanInfo.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode() && loanInfo.AutoDistributeState == null)
                    {
                        if (loanInfo.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        {
                            def.meta = new Meta(211, "Đơn vay đang bị hủy!");
                            return Ok(def);
                        }
                        if (loanInfo.InProcess == EnumInProcess.Process.GetHashCode())
                        {
                            def.meta = new Meta(212, "Đơn vay đang được xử lý!");
                            return Ok(def);
                        }
                        if (loanInfo.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                        {
                            def.meta = new Meta(215, "Đơn vay đang ở trạng thái đã giải ngân!");
                            return Ok(def);
                        }
                        if (loanInfo.Status == EnumLoanStatus.FINISH.GetHashCode())
                        {
                            def.meta = new Meta(215, "Đơn vay đang ở trạng thái đã đóng HĐ!");
                            return Ok(def);
                        }
                        if (loanInfo.PipelineState == EnumPipelineState.MANUAL_PROCCESSED.GetHashCode())
                        {
                            def.meta = new Meta(213, "Đơn vay đang được xử lý!");
                            return Ok(def);
                        }
                        unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                        {
                            InProcess = (int)EnumInProcess.Process,
                            ActionState = (int)EnumActionPush.Back,
                            PipelineState = (int)EnumPipelineState.MANUAL_PROCCESSED
                        });
                        unitOfWork.Save();
                        //Add note
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = req.LoanBriefId,
                            Note = string.Format("Hợp đồng HĐ-{0} đã được trả lại với lý do : {1}", req.LoanBriefId, req.Reason),
                            FullName = "",
                            Status = 1,
                            ActionComment = EnumActionComment.LenderComment.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 0,
                            ShopId = 0,
                            ShopName = "Lender"
                        };
                        // 
                        unitOfWork.LoanBriefNoteRepository.Insert(note);
                        unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        return Ok(def);
                    }
                    else if (loanInfo.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode() && loanInfo.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING && loanInfo.LenderId == req.LenderId)
                    {
                        // Nhà đầu tư đã khóa đơn tự động nhưng trả về
                        if (loanInfo.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        {
                            def.meta = new Meta(211, "Đơn vay đang bị hủy!");
                            return Ok(def);
                        }
                        if (loanInfo.InProcess == EnumInProcess.Process.GetHashCode())
                        {
                            def.meta = new Meta(212, "Đơn vay đang được xử lý!");
                            return Ok(def);
                        }
                        if (loanInfo.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                        {
                            def.meta = new Meta(215, "Đơn vay đang ở trạng thái đã giải ngân!");
                            return Ok(def);
                        }
                        if (loanInfo.Status == EnumLoanStatus.FINISH.GetHashCode())
                        {
                            def.meta = new Meta(215, "Đơn vay đang ở trạng thái đã đóng HĐ!");
                            return Ok(def);
                        }
                        if (loanInfo.PipelineState == EnumPipelineState.MANUAL_PROCCESSED.GetHashCode())
                        {
                            def.meta = new Meta(213, "Đơn vay đang được xử lý!");
                            return Ok(def);
                        }
                        // Đơn vay bị khóa, không cho phép trả về
                        if (loanInfo.IsLock.HasValue && loanInfo.IsLock.Value)
                        {
                            def.meta = new Meta(215, "Đơn vay đang bị khóa, vui lòng liên hệ hỗ trợ!");
                            return Ok(def);
                        }
                        // Back lại bước chọn Lender
                        unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                        {
                            InProcess = (int)EnumInProcess.Process,
                            ActionState = (int)EnumActionPush.Back,
                            PipelineState = (int)EnumPipelineState.MANUAL_PROCCESSED
                        });
                        //Add note
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = req.LoanBriefId,
                            Note = string.Format("Hợp đồng HĐ-{0} đã được trả lại với lý do : {1}", req.LoanBriefId, req.Reason),
                            FullName = loanInfo.LenderName,
                            Status = 1,
                            ActionComment = EnumActionComment.ReturnLockDistribute.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 0,
                            ShopId = 0,
                            ShopName = "Lender"
                        };
                        // 
                        unitOfWork.LoanBriefNoteRepository.SingleInsert(note);
                        var lenderLoanBrief = unitOfWork.LenderLoanBriefRepository.Query(x => x.LenderId == req.LenderId && x.LoanBriefId == req.LoanBriefId, null, false).FirstOrDefault();
                        if (lenderLoanBrief != null)
                        {
                            // Đơn có về nđt hay không thì cũng ẩn khỏi danh sách chia tự động					
                            unitOfWork.LenderLoanBriefRepository.Update(x => x.LenderId == req.LenderId && x.LoanBriefId == req.LoanBriefId, x => new LenderLoanBrief()
                            {
                                Status = (int)Common.Helpers.Const.AutoDistributeState.RETURNED,
                                UpdatedTime = DateTime.Now
                            });
                            if (lenderLoanBrief.Status == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING)
                            {
                                // Trả lại tiền đã khóa								
                                var amount = loanInfo.LoanAmount;
                                unitOfWork.LenderConfigRepository.Update(x => x.LenderId == req.LenderId && x.LockedMoney >= amount, x => new LenderConfig()
                                {
                                    LockedMoney = x.LockedMoney - amount,
                                    Returned = x.Returned + 1,
                                    UpdatedTime = DateTime.Now
                                });
                            }
                            else
                            {
                                unitOfWork.LenderConfigRepository.Update(x => x.LenderId == req.LenderId, x => new LenderConfig()
                                {
                                    Returned = x.Returned + 1,
                                    UpdatedTime = DateTime.Now
                                });
                            }
                        }
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        return Ok(def);
                    }
                    else if (loanInfo.Status == EnumLoanStatus.LENDER_LOAN_DISTRIBUTING.GetHashCode() || loanInfo.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode() && loanInfo.AutoDistributeState != null) // Nhà đầu tư được chia đơn tự động
                    {
                        // Trường hợp nđt trả về đơn chia tự động
                        var lenderLoanBrief = unitOfWork.LenderLoanBriefRepository.Query(x => x.LenderId == req.LenderId && x.LoanBriefId == req.LoanBriefId, null, false).FirstOrDefault();
                        if (lenderLoanBrief != null)
                        {
                            // Đơn có về nđt hay không thì cũng ẩn khỏi danh sách chia tự động					
                            unitOfWork.LenderLoanBriefRepository.Update(x => x.LenderId == req.LenderId && x.LoanBriefId == req.LoanBriefId, x => new LenderLoanBrief()
                            {
                                Status = (int)Common.Helpers.Const.AutoDistributeState.RETURNED,
                                UpdatedTime = DateTime.Now
                            });
                            unitOfWork.LenderConfigRepository.Update(x => x.LenderId == req.LenderId, x => new LenderConfig()
                            {
                                Returned = x.Returned + 1,
                                UpdatedTime = DateTime.Now
                            });
                        }
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        return Ok(def);
                    }
                    else
                    {
                        // Khác thì failed ?
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ReturnLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //List Reason
        [HttpGet]
        [Route("list_reason_group")]
        public ActionResult<DefaultResponse<Meta, List<ReasonCancelDetail>>> GetReasonGroup()
        {
            var def = new DefaultResponse<Meta, List<ReasonCancelDetail>>();
            int ReasonType = EnumTypeReasonReturn.Lender.GetHashCode();
            try
            {
                var data = unitOfWork.ReasonCancelRepository.Query(x => x.Type == ReasonType && x.IsEnable == 1 && x.ParentId == 0, null, false).Select(ReasonCancelDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Reason/list_reason_group Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        //Đơn vay quá thời gain chờ giải ngân
        [HttpGet]
        [Route("over_time_disbursement")]
        public ActionResult<DefaultResponse<List<OverTimeDisbursementWaiting>>> LoanbriefOverTimeDisbursement()
        {
            var def = new DefaultResponse<List<OverTimeDisbursementWaiting>>();
            try
            {
                //trừ mã NA00002(28512)
                var query = unitOfWork.LoanBriefRepository
                    .Query(x => x.InProcess != EnumInProcess.Process.GetHashCode() && x.LenderId != 28512
                    && x.IsLock != true && x.Status == EnumLoanStatus.WAIT_DISBURSE.GetHashCode()
                        && x.NextDate <= DateTime.Now, null, false)
                    .Select(OverTimeDisbursementWaiting.ProjectionOverTimeDisbursementWaiting);

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/LoanbriefOverTimeDisbursement Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Giải ngân
        [HttpGet]
        [Route("Disbursement")]
        public ActionResult<DefaultResponse<Meta>> Disbursement([FromQuery] int LoanBriefId = 0, [FromQuery] string SourceBank = "", [FromQuery] string LenderName = "", [FromQuery] decimal FeeInsuranceOfCustomer = 0, [FromQuery] decimal feeInsuranceProperty = 0)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                if (LoanBriefId <= 0 || SourceBank == "")
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                long InsuranceOfCustomer = 0;
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId, null, false).Select(x => new
                {
                    LoanBriefId = x.LoanBriefId,
                    FeeInsuranceOfCustomer = x.FeeInsuranceOfCustomer,
                    FeeInsuranceOfProperty = x.FeeInsuranceOfProperty,
                    LenderId = x.LenderId,
                    FullName = x.FullName,
                    BuyInsuranceProperty = x.LoanBriefProperty != null ? x.LoanBriefProperty.BuyInsuranceProperty : null,
                    AutoDistributeState = x.AutoDistributeState,
                    LoanAmount = x.LoanAmount,
                    Status = x.Status,
                    BoundTelesaleId = x.BoundTelesaleId,
                    HubId = x.HubId,
                    HubEmployeeId = x.HubEmployeeId,
                    CoordinatorUserId = x.CoordinatorUserId

                }).FirstOrDefault();
                if (loanInfo != null)
                {
                    if (loanInfo.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING)
                    {
                        // trừ số tiền đang giữ khi được lock
                        var lenderLoanBrief = unitOfWork.LenderLoanBriefRepository.Query(x => x.LenderId == loanInfo.LenderId && x.LoanBriefId == loanInfo.LoanBriefId && loanInfo.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING, null, false).FirstOrDefault();
                        if (lenderLoanBrief != null)
                        {
                            // Chia tự động và đã lock thành công
                            var loanAmount = loanInfo.LoanAmount;
                            unitOfWork.LenderConfigRepository.Update(x => x.LenderId == loanInfo.LenderId, x => new LenderConfig()
                            {
                                CurrentMoney = x.CurrentMoney - loanAmount,
                                LockedMoney = x.LockedMoney - loanAmount,
                                UpdatedTime = DateTime.Now
                            });
                        }
                    }
                    if (FeeInsuranceOfCustomer > 0)
                    {
                        InsuranceOfCustomer = Convert.ToInt64(FeeInsuranceOfCustomer);
                    }
                    else if (loanInfo.FeeInsuranceOfCustomer != null && loanInfo.FeeInsuranceOfCustomer != 0)
                    {
                        InsuranceOfCustomer = Convert.ToInt64(loanInfo.FeeInsuranceOfCustomer);
                    }
                    int source = 0;
                    if (SourceBank.Trim() == "NamA")
                        source = 1;
                    if (SourceBank.Trim() == "NCB")
                        source = 2;
                    if (SourceBank.Trim() == "VIB")
                        source = 4;
                    var obj = new Disbursement.InPut
                    {
                        LoanCreditId = loanInfo.LoanBriefId,
                        ShopLenderId = Convert.ToInt32(loanInfo.LenderId),
                        MoneyfeeInsuranceOfCustomer = InsuranceOfCustomer,
                        SourceBankDisbursement = source,
                        MoneyfeeInsuranceMaterialCovered = feeInsuranceProperty > 0 && loanInfo.BuyInsuranceProperty == true ? Convert.ToInt64(feeInsuranceProperty) : 0
                    };
                    var data = _lmsService.DisbursementLoanCreditV2(obj);
                    if (data.Status == 1)
                    {
                        if (data.Data != null)
                        {
                            unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                            {
                                LmsLoanId = data.Data.LoanId,
                                CodeId = data.Data.CodeId,
                                Status = EnumLoanStatus.DISBURSED.GetHashCode(),
                                DisbursementAt = DateTime.Now,
                                FeeInsuranceOfCustomer = InsuranceOfCustomer,
                                FeeInsuranceOfProperty = feeInsuranceProperty
                            });
                        }
                        else
                        {
                            unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                            {
                                Status = EnumLoanStatus.DISBURSED.GetHashCode(),
                                DisbursementAt = DateTime.Now,
                                FeeInsuranceOfCustomer = InsuranceOfCustomer,
                                FeeInsuranceOfProperty = feeInsuranceProperty
                            });
                        }

                        //Thêm log GN đơn vay
                        unitOfWork.LogLoanActionRepository.Insert(new LogLoanAction
                        {
                            LoanbriefId = loanInfo.LoanBriefId,
                            ActionId = (int)EnumLogLoanAction.Disbursement,
                            TypeAction = (int)EnumTypeAction.Auto,
                            LoanStatus = EnumLoanStatus.DISBURSED.GetHashCode(),
                            TelesaleId = loanInfo.BoundTelesaleId,
                            HubId = loanInfo.HubId,
                            HubEmployeeId = loanInfo.HubEmployeeId,
                            CoordinatorUserId = loanInfo.CoordinatorUserId,
                            //UserActionId = 1,
                            //GroupUserActionId = 1,
                            NewValues = JsonConvert.SerializeObject(loanInfo),
                            CreatedAt = DateTime.Now
                        });
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = LoanBriefId,
                            Note = "Đơn vay mã HĐ-" + LoanBriefId + " đã được giải ngân bởi Lender : " + LenderName + "(" + loanInfo.LenderId + ")",
                            FullName = loanInfo.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.LenderGN.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 0,
                            ShopId = loanInfo.LenderId,
                            ShopName = "Lender"
                        };
                        unitOfWork.LoanBriefNoteRepository.Insert(note);
                        unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = data.Data;
                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(212, data.Messages[0].ToString());
                        return Ok(def);
                    }

                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/Disbursement Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Thông tin bảo hiểm
        [HttpGet]
        [Route("MoneyInsurance")]
        public ActionResult<DefaultResponse<Meta, DataOutPut>> MoneyInsurance([FromQuery] int LoanBriefId = 0)
        {
            var def = new DefaultResponse<Meta, DataOutPut>();
            try
            {
                if (LoanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                long FeeInsuranceOfCustomer = 0;
                long FeeInsuranceOfLender = 0;
                var data = new DataOutPut();
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(LoanBriefId);
                if (loanInfo != null)
                {
                    if (loanInfo.BuyInsurenceCustomer == true)
                    {
                        //if (loanInfo.FeeInsuranceOfCustomer != null && loanInfo.FeeInsuranceOfCustomer != 0)
                        //{
                        //    FeeInsuranceOfCustomer = Convert.ToInt64(loanInfo.FeeInsuranceOfCustomer);
                        //    FeeInsuranceOfLender = Convert.ToInt64(loanInfo.FeeInsuranceOfLender);
                        //}
                        //else
                        //{
                        var obj = new CalculatorMoneyInsurance.InPut
                        {
                            MoneyDisbursement = Convert.ToInt64(loanInfo.LoanAmount),
                            LenderId = Convert.ToInt64(loanInfo.LenderId),
                            LoanTime = Convert.ToInt32(loanInfo.LoanTime * 30),
                            RateType = Convert.ToInt32(loanInfo.RateTypeId),
                            TotalRate = Convert.ToDecimal(loanInfo.RatePercent != null ? loanInfo.RatePercent : 98.55)
                        };
                        var result = _lmsService.CalculatorMoneyInsurance(obj, LoanBriefId);
                        if (result.Status == 1)
                        {
                            //Update lại số tiền bảo hiểm
                            unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                            {
                                FeeInsuranceOfCustomer = result.Data.FeeInsuranceOfCustomer,
                                FeeInsuranceOfLender = result.Data.FeeInsuranceOfLender
                            });
                            unitOfWork.Save();
                            FeeInsuranceOfCustomer = result.Data.FeeInsuranceOfCustomer;
                            FeeInsuranceOfLender = result.Data.FeeInsuranceOfLender;
                        }
                        else
                        {
                            def.meta = new Meta(212, result.Messages[0].ToString());
                            return Ok(def);
                        }
                        //}
                    }

                    data.FeeInsuranceOfCustomer = FeeInsuranceOfCustomer;
                    data.FeeInsuranceOfLender = FeeInsuranceOfLender;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = data;
                    return Ok(def);


                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/MoneyInsurance Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Khóa mở đơn vay
        [HttpPost]
        [Route("LoanLock")]
        public ActionResult<DefaultResponse<Meta>> LoanLock([FromBody] Models.LMS.LoanBriefReq.LMSLoanLock req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LoanId <= 0 || req.Type <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.LoanId);
                if (loanInfo != null)
                {

                    //Type: 1-> Mở đơn, 2-> Khóa đơn
                    var mess = "";
                    var action = 0;
                    if (req.Type == 1)
                    {
                        loanInfo.IsLock = false;
                        mess = "Lender mở khóa đơn vay HD-" + req.LoanId;
                        action = EnumActionComment.LenderLoanOpenLock.GetHashCode();
                    }
                    if (req.Type == 2)
                    {
                        loanInfo.IsLock = true;
                        mess = "Lender khóa đơn vay HD-" + req.LoanId;
                        action = EnumActionComment.LenderLoanLock.GetHashCode();
                    }

                    unitOfWork.LoanBriefRepository.Update(loanInfo);
                    unitOfWork.Save();

                    var note = new LoanBriefNote
                    {
                        LoanBriefId = req.LoanId,
                        Note = mess,
                        FullName = req.UserName,
                        Status = 1,
                        ActionComment = action,
                        CreatedTime = DateTime.Now,
                        UserId = 0,
                        ShopId = loanInfo.LenderId,
                        ShopName = "Lender"
                    };

                    unitOfWork.LoanBriefNoteRepository.Insert(note);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);

                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/LoanLock Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Khóa mở đơn vay
        [HttpPost]
        [Route("LoanLockDistribute")]
        public async Task<ActionResult<DefaultResponse<Meta>>> LoanLockDistribute([FromBody] Models.LMS.LoanBriefReq.LMSLenderLock req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LoanBriefId <= 0 && req.LenderId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // Kiểm tra có phải nđt trên khóa đơn trước đấy không                
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.LoanBriefId);
                if (loanInfo.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING && loanInfo.LenderId == req.LenderId)
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    // Kiểm tra xem có bị thu hồi không
                    var lenderLoanbrief = unitOfWork.LenderLoanBriefRepository.Query(x => x.LenderId == req.LenderId && x.Status == (int)Common.Helpers.Const.AutoDistributeState.RETRIEVED, null, false).FirstOrDefault();
                    if (lenderLoanbrief != null)
                    {
                        def.meta = new Meta(251, "Đơn bị thu hồi do quá 10 phút giải ngân");
                        return Ok(def);
                    }
                    // Đơn không đang ở trạng thái chia
                    if (loanInfo.AutoDistributeState != (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED)
                    {
                        def.meta = new Meta(251, "Rất tiếc, đơn này đã có người nhận.");
                        return Ok(def);
                    }
                }
                // check xem ndt có đủ tiền				
                // Cập nhật khóa đơn, để nđt khác không nhận đơn
                unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief() { AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING });
                var config = unitOfWork.LenderConfigRepository.Query(x => x.LenderId == req.LenderId, null, false).FirstOrDefault();
                if (loanInfo != null && config != null)
                {
                    //if ((config.CurrentMoney.Value - config.LockedMoney) < loanInfo.LoanAmount)
                    //{
                    //	def.meta = new Meta(251, "Rất tiếc, bạn không đủ tiền để giải ngân");
                    //	return Ok(def);
                    //}	
                    // Ký hợp đồng điện tử nếu có
                    if (loanInfo.EsignState == (int)EsignState.BORROWER_SIGNED || loanInfo.EsignState == (int)EsignState.LENDER_SIGNED) // Ký lại
                    {
                        // Kiểm tra xem nđt có thông tin aggreement id và passcode hay không
                        var shopInfo = await _agService.GetShopInfo(req.LenderId);
                        if (shopInfo != null)
                        {
                            if (shopInfo.Status == 1)
                            {
                                if (shopInfo.Data != null && !String.IsNullOrEmpty(shopInfo.Data.AgreementId) && !String.IsNullOrEmpty(shopInfo.Data.Passcode))
                                {
                                    // Đủ thông tin gọi api
                                    using (var httpClient = new HttpClient())
                                    {
                                        var data = new SignContractLenderReq
                                        {
                                            agreementId = shopInfo.Data.AgreementId,
                                            lenderId = req.LenderId,
                                            loanBriefId = loanInfo.LoanBriefId,
                                            passcode = shopInfo.Data.Passcode
                                        };
                                        string token = unitOfWork.AuthorizationTokenRepository.Query(x => x.ServiceName == "APP_API_LMS", null, false).Select(x => x.Token).FirstOrDefault(); // Lấy token lms
                                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);
                                        StringContent content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
                                        // Bug Request.Scheme luôn trả ra http
                                        // Nếu domain từ apilos và apitestlos thì https
                                        string host = Request.Host.Value;
                                        string scheme = Request.Scheme;
                                        if (host.Equals("apitestlos.tima.vn") || host.Equals("apilos.tima.vn"))
                                            scheme = "https";
                                        string domainName = scheme + "://" + host;
                                        var response = await httpClient.PostAsync(domainName + "/api/v1.0/esign/SignContractForLender", content);
                                        if (response.IsSuccessStatusCode)
                                        {
                                            var result = await response.Content.ReadAsStringAsync();
                                            var oResult = JsonConvert.DeserializeObject<DefaultResponse<object>>(result);
                                            if (oResult != null)
                                            {
                                                if (oResult.meta.errorCode != 200)
                                                {
                                                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief() { AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED });
                                                    def.meta = new Meta(217, oResult.meta.errorMessage);
                                                    return Ok(def);
                                                }
                                            }
                                            else
                                            {
                                                unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief() { AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED });
                                                def.meta = new Meta(217, "Ký hợp đồng thật bại, vui lòng thử lại sau!");
                                                return Ok(def);
                                            }
                                        }
                                        else
                                        {
                                            unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief() { AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED });
                                            def.meta = new Meta(217, "Ký hợp đồng thật bại, vui lòng thử lại sau!");
                                            return Ok(def);
                                        }
                                    }
                                }
                                else
                                {
                                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief() { AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED });
                                    def.meta = new Meta(217, "Ký hợp đồng thật bại, vui lòng thử lại sau!");
                                    return Ok(def);
                                }
                            }
                            else
                            {
                                unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief() { AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED });
                                def.meta = new Meta(217, "Ký hợp đồng thật bại, vui lòng thử lại sau!");
                                return Ok(def);
                            }
                        }
                        else
                        {
                            unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief() { AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED });
                            def.meta = new Meta(217, "Ký hợp đồng thật bại, vui lòng thử lại sau!");
                            return Ok(def);
                        }
                    }
                    var amount = loanInfo.LoanAmount;
                    // Update lender config để lock tiền
                    unitOfWork.LenderConfigRepository.Update(x => x.LenderId == req.LenderId, x => new LenderConfig()
                    {
                        LockedMoney = x.LockedMoney + amount,
                        UpdatedTime = DateTime.Now,
                        Accepted = x.Accepted + 1
                    });
                    // Đẩy đơn lên trạng thái chờ giải ngân
                    var nextDate = DateTime.Now.AddMinutes(10);
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                    {
                        InProcess = (int)EnumInProcess.Process,
                        ActionState = (int)EnumActionPush.Push,
                        PipelineState = (int)EnumPipelineState.MANUAL_PROCCESSED,
                        TypeDisbursement = 2,
                        LenderId = config.LenderId,
                        LenderReceivedDate = DateTime.Now,
                        NextDate = nextDate,
                        LenderName = config.LenderName
                    });
                    // cập nhật bảng lender loan brief
                    unitOfWork.LenderLoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId && x.LenderId == req.LenderId, x => new LenderLoanBrief()
                    {
                        Status = (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING,
                        UpdatedTime = DateTime.Now
                    });
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/LoanLockDistribute Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        /// <summary>
        /// Thêm comment đơn vay
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("add_comment")]
        public ActionResult<DefaultResponse<object>> AddComment(LoanbriefNoteReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.LoanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                if (unitOfWork.LoanBriefRepository.Any(x => x.LoanBriefId == req.LoanBriefId))
                {
                    var note = new LoanBriefNote
                    {
                        LoanBriefId = req.LoanBriefId,
                        Note = req.Comment,
                        FullName = req.UserName,
                        Status = 1,
                        ActionComment = EnumActionComment.LenderComment.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 1,
                        ShopId = 0,
                        ShopName = "Lender"
                    };

                    unitOfWork.LoanBriefNoteRepository.Insert(note);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/AddComment Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("DisbursementV2")]
        public async Task<ActionResult<DefaultResponse<Meta>>> DisbursementV2(DisbursementModel req)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                if (req.LoanBriefId <= 0 || string.IsNullOrEmpty(req.SourceBank))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                long InsuranceOfCustomer = 0;
                // check if loan action exists
                var loanInfo = await unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.LoanBriefId, null, false).Select(x => new
                {
                    LoanBriefId = x.LoanBriefId,
                    FeeInsuranceOfCustomer = x.FeeInsuranceOfCustomer,
                    FeeInsuranceOfProperty = x.FeeInsuranceOfProperty,
                    LenderId = x.LenderId,
                    FullName = x.FullName,
                    BuyInsuranceProperty = x.LoanBriefProperty != null ? x.LoanBriefProperty.BuyInsuranceProperty : null,
                    AutoDistributeState = x.AutoDistributeState,
                    LoanAmount = x.LoanAmount,
                    LoanStatus = x.Status,
                    TypeDisbursement = x.TypeDisbursement,
                    BoundTelesaleId = x.BoundTelesaleId,
                    HubId = x.HubId,
                    HubEmployeeId = x.HubEmployeeId,
                    CoordinatorUserId = x.CoordinatorUserId

                }).FirstOrDefaultAsync();
                if (loanInfo != null)
                {
                    //Kiểm tra trạng thái đơn vay xem có phải đang ở chờ GN hay không
                    if (loanInfo.LoanStatus != (int)EnumLoanStatus.WAIT_DISBURSE)
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Đơn vay không ở trạng thái chờ GN");
                        return Ok(def);
                    }
                    //Kiểm tra xem có đúng là loại GN là Lender hay không
                    if (loanInfo.TypeDisbursement != (int)EnumDisbursement.DISBURSE_LEND)
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Đơn vay chưa được chuyển sang hình thức GN qua APP Lender ");
                        return Ok(def);
                    }

                    if (loanInfo.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING)
                    {
                        // trừ số tiền đang giữ khi được lock
                        var lenderLoanBrief = await unitOfWork.LenderLoanBriefRepository.Query(x => x.LenderId == loanInfo.LenderId
                            && x.LoanBriefId == loanInfo.LoanBriefId
                            && loanInfo.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING, null, false).FirstOrDefaultAsync();
                        if (lenderLoanBrief != null)
                        {
                            var loanAmount = loanInfo.LoanAmount;
                            // Chia tự động và đã lock thành công
                            await unitOfWork.LenderConfigRepository.UpdateAsync(x => x.LenderId == loanInfo.LenderId, x => new LenderConfig()
                            {
                                CurrentMoney = x.CurrentMoney - loanAmount,
                                LockedMoney = x.LockedMoney - loanAmount,
                                UpdatedTime = DateTime.Now
                            });
                        }
                    }
                    if (req.FeeInsuranceOfCustomer > 0)
                        InsuranceOfCustomer = Convert.ToInt64(req.FeeInsuranceOfCustomer);
                    else if (loanInfo.FeeInsuranceOfCustomer != null && loanInfo.FeeInsuranceOfCustomer != 0)
                        InsuranceOfCustomer = Convert.ToInt64(loanInfo.FeeInsuranceOfCustomer);

                    int sourceBank = 0;
                    if (req.SourceBank.Trim() == "NamA")
                        sourceBank = (int)SourceBank.NAMA;
                    if (req.SourceBank.Trim() == "NCB")
                        sourceBank = (int)SourceBank.NCB;
                    if (req.SourceBank.Trim() == "VIB")
                        sourceBank = (int)SourceBank.VIB;

                    if (sourceBank == 0)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "SourceBank không đúng");
                        return Ok(def);
                    }

                    var obj = new Disbursement.InPut
                    {
                        LoanCreditId = loanInfo.LoanBriefId,
                        ShopLenderId = Convert.ToInt32(loanInfo.LenderId),
                        MoneyfeeInsuranceOfCustomer = InsuranceOfCustomer,
                        SourceBankDisbursement = sourceBank,
                        MoneyfeeInsuranceMaterialCovered = req.FeeInsuranceProperty > 0 && loanInfo.BuyInsuranceProperty == true ? Convert.ToInt64(req.FeeInsuranceProperty) : 0
                    };
                    var data = _lmsService.DisbursementLoanCreditV2(obj);
                    if (data.Status == 1)
                    {
                        var listTask = new List<Task>();
                        if (data.Data != null)
                        {
                            listTask.Add(unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                            {
                                LmsLoanId = data.Data.LoanId,
                                CodeId = data.Data.CodeId,
                                Status = EnumLoanStatus.DISBURSED.GetHashCode(),
                                DisbursementAt = DateTime.Now,
                                FeeInsuranceOfCustomer = InsuranceOfCustomer,
                                FeeInsuranceOfProperty = req.FeeInsuranceProperty
                            }));
                        }
                        else
                        {
                            listTask.Add(unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == loanInfo.LoanBriefId, x => new LoanBrief()
                            {
                                Status = EnumLoanStatus.DISBURSED.GetHashCode(),
                                DisbursementAt = DateTime.Now,
                                FeeInsuranceOfCustomer = InsuranceOfCustomer,
                                FeeInsuranceOfProperty = req.FeeInsuranceProperty
                            }));
                        }
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = req.LoanBriefId,
                            Note = $"[APP] Đơn vay mã HĐ-{req.LoanBriefId} đã được giải ngân bởi Lender : {req.LenderName}({loanInfo.LenderId })",
                            FullName = loanInfo.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.LenderGN.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 0,
                            ShopId = loanInfo.LenderId,
                            ShopName = "Lender"
                        };
                        //Thêm log GN đơn vay
                        listTask.Add(unitOfWork.LogLoanActionRepository.InsertAsync(new LogLoanAction
                        {
                            LoanbriefId = loanInfo.LoanBriefId,
                            ActionId = (int)EnumLogLoanAction.Disbursement,
                            TypeAction = (int)EnumTypeAction.Auto,
                            LoanStatus = EnumLoanStatus.DISBURSED.GetHashCode(),
                            TelesaleId = loanInfo.BoundTelesaleId,
                            HubId = loanInfo.HubId,
                            HubEmployeeId = loanInfo.HubEmployeeId,
                            CoordinatorUserId = loanInfo.CoordinatorUserId,
                            //UserActionId = 1,
                            //GroupUserActionId = 1,
                            NewValues = JsonConvert.SerializeObject(loanInfo),
                            CreatedAt = DateTime.Now
                        }));
                        listTask.Add(unitOfWork.LoanBriefNoteRepository.InsertAsync(note));
                        if (listTask.Count > 0)
                            Task.WaitAll(listTask.ToArray());
                        unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = data.Data;
                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(212, data.Messages[0].ToString());
                        return Ok(def);
                    }

                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lender/DisbursementV2 Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
