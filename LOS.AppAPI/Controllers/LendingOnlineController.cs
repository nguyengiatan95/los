﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using LOS.AppAPI.DTOs.LendingOnlineDTO;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Service.LmsService;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using LOS.AppAPI.Service.AG;
using LOS.AppAPI.Models.AG;
using LOS.AppAPI.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json;
using LOS.AppAPI.Models.Webhook;
using LOS.AppAPI.Models.AI;
using static LOS.AppAPI.Models.LendingOnline;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class LendingOnlineController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly ILmsService _lmsService;
        private readonly IAgService _agService;
        private readonly IErp _erpService;
        private readonly INetworkService _networkService;
        private readonly IServiceAI _aiService;
        private readonly IIdentityService _identityService;


        public LendingOnlineController(IUnitOfWork unitOfWork, ILmsService lmsService, IAgService agService, IErp erpService,
            INetworkService networkService, IServiceAI aiService, IIdentityService identityService)
        {
            this._unitOfWork = unitOfWork;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _configuration = builder.Build();
            _lmsService = lmsService;
            _agService = agService;
            _erpService = erpService;
            _aiService = aiService;
            _networkService = networkService;
            _identityService = identityService;
        }

        [HttpGet]
        [Route("get_otp")]
        public async Task<ActionResult<DefaultResponse<Meta, string>>> GetOtp([FromQuery] string phoneNumber)
        {
            var def = new DefaultResponse<Meta, string>();
            try
            {
                var lifeTimeOtp = 15;
                if (string.IsNullOrEmpty(phoneNumber))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (!phoneNumber.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Số điện thoại không đúng định dạng");
                    return Ok(def);
                }
                //Kiểm tra xem sdt của KH có khoản vay không
                var loaninfo = await _unitOfWork.LoanBriefRepository.Query(x => x.Phone == phoneNumber
                    && x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.DISBURSED
                    && x.Status != (int)EnumLoanStatus.FINISH, null, false).Select(x => new { LoanbriefId = x.LoanBriefId }).FirstOrDefaultAsync();
                if (loaninfo == null)
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Số điện thoại không tồn tại đơn vay");
                    return Ok(def);
                }

                //Kiểm tra xem còn có otp có hiệu lực không
                var logSendOtp = await _unitOfWork.LogSendOtpRepository.Query(x => x.Phone == phoneNumber
                    && x.StatusSend == (int)StatusCallApi.Success
                    && x.VerifyStatus.GetValueOrDefault(0) == (int)VerifyOtp.WaitingVerify, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();
                if (logSendOtp != null && logSendOtp.Id > 0)
                {
                    //Nếu thời chờ verify còn quá nửa thời gian => trả luôn otp này
                    if ((logSendOtp.ExpiredVerifyOtp.Value - DateTime.Now).TotalMinutes > 3)
                    {
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = logSendOtp.Otp;
                        return Ok(def);
                    }
                }
                var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                if (environmentName == "Development" || Constants.HashPhoneTestOtp.Contains(phoneNumber))
                {
                    var log = new LogSendOtp
                    {
                        Phone = phoneNumber,
                        Otp = "123456",
                        Request = "",
                        CreatedAt = DateTime.Now,
                        IpAddress = "",
                        Url = "",
                        TypeSendOtp = (int)TypeSendOtp.SmsBrandName,
                        StatusSend = (int)StatusCallApi.Success,
                        ExpiredVerifyOtp = DateTime.Now.AddMinutes(lifeTimeOtp)
                    };
                    _unitOfWork.LogSendOtpRepository.Insert(log);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = "123456";
                    return Ok(def);
                }
                //Send otp mới
                Random random = new Random();
                var otp = random.Next(100001, 999999);
                var mess = $"Tima: Ma xac minh cua ban la {otp}. Ma co hieu luc trong vong {lifeTimeOtp} phut. Khong chia se ma nay voi nguoi khac.";
                var result = _lmsService.SendOtpSmsBrandName((int)TypeOtp.VerifyPhone, phoneNumber, mess, otp.ToString(), DateTime.Now.AddMinutes(lifeTimeOtp), GetIpAddressRemote());
                if (result != null && result.Result == (int)StatusCallApi.Success)
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = otp.ToString();
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, "Xảy ra lỗi khi gửi OTP. Vui lòng thử lại");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/GetOtp Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("verify_otp")]
        public async Task<ActionResult<DefaultResponse<Meta, string>>> VerifyOtpApp(VerifyOtpModel model)
        {
            var def = new DefaultResponse<Meta, string>();
            try
            {
                if (!string.IsNullOrEmpty(model.Phone) && !string.IsNullOrEmpty(model.Otp))
                {
                    //Kiểm tra xem còn có otp có hiệu lực không
                    var logSendOtp = await _unitOfWork.LogSendOtpRepository.Query(x => x.Phone == model.Phone
                        && x.StatusSend == (int)StatusCallApi.Success
                        && x.VerifyStatus.GetValueOrDefault(0) == (int)VerifyOtp.WaitingVerify, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();

                    if (logSendOtp != null && logSendOtp.Id > 0)
                    {
                        if (logSendOtp.Otp == model.Otp)
                        {
                            if (logSendOtp.ExpiredVerifyOtp >= DateTime.Now)
                            {
                                var expiredToken = DateTime.Now.AddDays(1);
                                //tạo token cho user
                                var token = GenerateToken(model.Phone, model.Otp, expiredToken);
                                //chuyển trạng thái đã verify otp 
                                _unitOfWork.LogSendOtpRepository.Update(x => x.Id == logSendOtp.Id, x => new DAL.EntityFramework.LogSendOtp()
                                {
                                    VerifyStatus = (int)VerifyOtp.Verified,
                                    VerifyAt = DateTime.Now,
                                    ExpiredToken = expiredToken,
                                    TokenGennerate = token
                                });
                                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                def.data = token;
                            }
                            else
                                def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, "Quá thời hạn verify OTP");

                        }
                        else
                            def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, "Mã OTP không đúng");
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, "KH chưa có yêu cầu lấy mã OTP");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/VerifyOtpApp Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_bank")]
        // [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, List<BankLendingOnline>>>> GetBank()
        {
            var def = new DefaultResponse<Meta, List<BankLendingOnline>>();
            try
            {
                var data = await _unitOfWork.BankRepository.All().Select(BankLendingOnline.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/GetBank Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_province")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, List<ProvinceLendingOnline>>>> GetProvince()
        {
            var def = new DefaultResponse<Meta, List<ProvinceLendingOnline>>();
            try
            {
                var data = new List<ProvinceLendingOnline>();
                data.Add(new ProvinceLendingOnline()
                {
                    Id = 1,
                    Name = "Hà Nội"
                });
                //var data = await _unitOfWork.ProvinceRepository.Query(x => x.IsApply == 1, null, false).Select(ProvinceLendingOnline.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/GetProvince Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_district")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, List<DistrictLendingOnline>>>> GetDistrict(int provinceId)
        {
            var def = new DefaultResponse<Meta, List<DistrictLendingOnline>>();
            try
            {
                var data = await _unitOfWork.DistrictRepository.Query(x => x.IsApply == 1 && x.ProvinceId == provinceId, null, false).Select(DistrictLendingOnline.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/GetDistrict Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_ward")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, List<WardLendingOnline>>>> GetWard(int districtId)
        {
            var def = new DefaultResponse<Meta, List<WardLendingOnline>>();
            try
            {
                var data = await _unitOfWork.WardRepository.Query(x => x.DistrictId == districtId, null, false).Select(WardLendingOnline.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/GetWard Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("upload_image")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, string>>> UploadImage(IFormFile images, [FromForm] Models.LendingOnline.UploadImageReq entity)
        {
            var def = new DefaultResponse<Meta, string>();
            try
            {
                if (images == null || entity.TypeId == 0 || entity.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (entity.TypeId != (int)EnumDocumentType.CMND_CCCD && entity.TypeId != (int)EnumDocumentType.Selfie
                    && entity.TypeId != (int)EnumDocumentType.Motorbike_Registration_Certificate)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền đúng loại chứng từ");
                    return Ok(def);
                }

                if (entity.TypeId == (int)EnumDocumentType.CMND_CCCD)
                {
                    //kiểm tra xem có truyền ResidentType không
                    if (entity.ResidentType != (int)EnumTypeofownership.KT1 && entity.ResidentType != (int)EnumTypeofownership.KT3)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền đúng loại hình thức cư trú");
                        return Ok(def);
                    }

                    //kiểm tra có truyền CMND không
                    if (string.IsNullOrEmpty(entity.NationalCard))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền chứng minh thư");
                        return Ok(def);
                    }

                    //Kiểm tra định dạng của CMND
                    if (!ConvertExtensions.IsNumber(entity.NationalCard))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền đúng định dạng chứng minh thư");
                        return Ok(def);
                    }
                    if (entity.NationalCard.Length != (int)NationalCardLength.CMND_QD && entity.NationalCard.Length != (int)NationalCardLength.CMND && entity.NationalCard.Length != (int)NationalCardLength.CCCD)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền đúng định dạng chứng minh thư");
                        return Ok(def);
                    }
                }

                //kiểm tra xem có đũng mã loanbrief không
                var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false).Select(LoanBriefLendingOnline.ProjectionViewDetail).OrderByDescending(x => x.LoanBriefId).FirstOrDefaultAsync();
                if (loanbrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                    return Ok(def);
                }

                //kiểm tra xem có phải chứng từ xe máy không
                if (entity.TypeId == (int)EnumDocumentType.Motorbike_Registration_Certificate)
                {
                    //kiểm tra xem có truyền hình thức sở hữu xe không
                    if (entity.OwnershipCar != (int)EnumProductCredit.MotorCreditType_CC
                        && entity.OwnershipCar != (int)EnumProductCredit.MotorCreditType_KCC)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Vui lòng truyền đúng hình thức sở hữu xe");
                        return Ok(def);
                    }
                }

                if (loanbrief.Status != EnumLoanStatus.INIT.GetHashCode()
                    && loanbrief.Status != EnumLoanStatus.APPRAISER_REVIEW.GetHashCode()
                    && loanbrief.Status != EnumLoanStatus.WAIT_HUB_EMPLOYEE_PREPARE_LOAN.GetHashCode()
                    && loanbrief.Status != EnumLoanStatus.HUB_LOAN_DISTRIBUTING.GetHashCode()
                    && loanbrief.Status != EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING.GetHashCode()
                                && loanbrief.Status != EnumLoanStatus.HUB_CHT_APPROVE.GetHashCode())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay đã đẩy đi không Upload được");
                    return Ok(def);
                }

                var phoneToken = _identityService.GetPhoneIdentity();

                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                    return Ok(def);
                }

                if (loanbrief.Phone != phoneToken)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                    return Ok(def);
                }

                var accessKeyS3 = _configuration["AppSettings:AccessKeyS3"];
                var recsetAccessKeyS3 = _configuration["AppSettings:RecsetAccessKeyS3"];
                var serviceURL = _configuration["AppSettings:ServiceURLFileTima"];
                var bucketName = _configuration["AppSettings:BucketName"];
                var client = new AmazonS3Client(accessKeyS3, recsetAccessKeyS3, new AmazonS3Config()
                {
                    RegionEndpoint = RegionEndpoint.APSoutheast1
                });
                var folder = bucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
                var extension = Path.GetExtension(images.FileName);
                var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                if (allowedExtensions.Contains(extension.ToLower()))
                {
                    //Check nhân viên Tima
                    var checkErp = _erpService.CheckEmployeeTima(entity.NationalCard, loanbrief.Phone);
                    if (checkErp != null)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng là nhân viên Tima!");
                        return Ok(def);
                    }

                    //Check xem khách hàng có trong black list không
                    var entityBlackList = new RequestCheckBlackListAg
                    {
                        FullName = loanbrief.FullName,
                        NumberCard = entity.NationalCard,
                        Phone = loanbrief.Phone
                    };
                    var checkBlackList = _agService.CheckBlacklistAg(entityBlackList);
                    if (checkBlackList != null && checkBlackList.Data && checkBlackList.Status == 1)
                    {
                        def.meta = new Meta(ResponseHelper.CUSTOMER_BLACKLIST_CODE, "Khách hàng nằm trong Black list!");
                        return Ok(def);
                    }

                    //Check xem có kỳ thanh tóa cũ quá hạn 60 ngày không
                    var objCheckReBorrow = new CheckReLoan.Input()
                    {
                        CustomerName = loanbrief.FullName,
                        NumberCard = entity.NationalCard,
                    };
                    var resultCheckReBorrow = _lmsService.CheckReBorrow(objCheckReBorrow);
                    if (resultCheckReBorrow != null && resultCheckReBorrow.MaxCountDayLate >= 60)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có kỳ thanh toán cũ quá hạn 60 ngày");
                        return Ok(def);
                    }

                    // update tên với cmnd vào loanbrief và customer

                    //Nếu loại file là cmnnd
                    if (entity.TypeId == (int)EnumDocumentType.CMND_CCCD)
                    {
                        _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                        {
                            NationalCard = entity.NationalCard,
                            Step = 5// upload cmnd chuyển step lên 5
                        });
                        _unitOfWork.CustomerRepository.Update(x => x.CustomerId == loanbrief.CustomerId, x => new Customer()
                        {
                            NationalCard = entity.NationalCard,
                        });

                        //kiểm tra xem có thay đổi hình thức cư trú không  

                        if (entity.ResidentType != loanbrief.ResidentType)
                        {
                            //Lưu lại log thay đổi hình thức cư trú
                            var objLoanBriefNote = new LoanBriefNote()
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = string.Format("[App Lending Online] Hệ thống tự động đổi hình thức cư trú sang {0} dựa vào OCR CMND/CCCD", Description.GetDescription((EnumTypeofownership)entity.ResidentType)),
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 0,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = Description.GetDescription(EnumShop.Tima)
                            };
                            await _unitOfWork.LoanBriefNoteRepository.InsertAsync(objLoanBriefNote);

                            _unitOfWork.LoanBriefResidentRepository.Update(x => x.LoanBriefResidentId == loanbrief.LoanBriefId, x => new LoanBriefResident()
                            {
                                ResidentType = entity.ResidentType,
                            });

                            //Kiểm tra đổi gói vay tương ứng
                            var productDetail = 0;
                            //Nếu chuyển sang KT3
                            if (entity.ResidentType == (int)EnumTypeofownership.KT3)
                            {
                                if (loanbrief.ProductDetailId != (int)Common.Extensions.InfomationProductDetail.LO_KT3_KTN_5tr)
                                    productDetail = (int)Common.Extensions.InfomationProductDetail.LO_KT3_KTN_5tr;
                            }
                            else
                            {
                                //Nếu chuyển sang KT1
                                if (loanbrief.LoanAmount > 5000000)
                                {
                                    if (loanbrief.ProductDetailId != (int)Common.Extensions.InfomationProductDetail.LO_KT1_KSHK_12tr)
                                        productDetail = (int)Common.Extensions.InfomationProductDetail.LO_KT1_KSHK_12tr;
                                }
                                else
                                {
                                    if (loanbrief.ProductDetailId != (int)Common.Extensions.InfomationProductDetail.LO_KT3_KTN_5tr)
                                        productDetail = (int)Common.Extensions.InfomationProductDetail.LO_KT3_KTN_5tr;
                                }
                            }
                            if (productDetail > 0)
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                                {
                                    ProductDetailId = productDetail
                                });
                        }

                    }
                    else if (entity.TypeId == (int)EnumDocumentType.Selfie)
                    {
                        _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                        {
                            Step = 6// upload cmnd chuyển step lên 6
                        });
                    }
                    else if (entity.TypeId == (int)EnumDocumentType.Motorbike_Registration_Certificate)
                    {
                        //kiểm tra xem có phải xe KCC không
                        //nếu phải thì hủy đơn
                        if (entity.OwnershipCar == (int)EnumProductCredit.MotorCreditType_KCC)
                        {
                            await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                            {
                                Status = EnumLoanStatus.CANCELED.GetHashCode(),
                                PipelineState = -1,
                                ReasonCancel = 50, // Xe máy không chính chủ
                                Step = 7, //upload đăng ký chuyển step lên 7
                                ProductId = (int)EnumProductCredit.MotorCreditType_KCC,
                                LoanBriefCancelAt = DateTime.Now,
                            });

                            //Lưu lại log khi hủy đơn
                            var objLoanBriefNote = new LoanBriefNote()
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = "[App Lending Online] Hệ thống tự động hủy đơn do xe máy không chính chủ",
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 0,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = Description.GetDescription(EnumShop.Tima)
                            };
                            await _unitOfWork.LoanBriefNoteRepository.InsertAsync(objLoanBriefNote);
                        }
                        else
                        {
                            _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                            {
                                Step = 7// upload đăng ký chuyển step lên 6
                            });
                        }
                    }
                    _unitOfWork.Save();
                    string ImageName = Guid.NewGuid().ToString() + extension;
                    using (var stream = new MemoryStream())
                    {
                        images.CopyTo(stream);
                        PutObjectRequest req = new PutObjectRequest()
                        {
                            InputStream = stream,
                            BucketName = folder,
                            Key = ImageName,
                            CannedACL = S3CannedACL.PublicRead
                        };
                        await client.PutObjectAsync(req);
                        var link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                        //insert to db
                        var obj = new LoanBriefFiles
                        {
                            LoanBriefId = entity.LoanBriefId,
                            CreateAt = DateTime.Now,
                            FilePath = link,
                            UserId = 0,
                            Status = 1,
                            TypeId = entity.TypeId,
                            S3status = 1,
                            MecashId = 1000000000,
                            SourceUpload = (int)EnumSourceUpload.Mobile
                        };
                        _unitOfWork.LoanBriefFileRepository.Insert(obj);
                        _unitOfWork.Save();

                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = serviceURL + obj.FilePath;
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ảnh không đúng định dạng!");
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/UploadImage Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #region get ip remote
        private string GetIpAddressRemote()
        {
            try
            {
                StringValues ipRequestHeader;
                var authIpRequest = HttpContext.Request.Headers.TryGetValue("X-Forwarded-For", out ipRequestHeader);
                if (authIpRequest)
                {
                    var remoteIp = GetIpRequest(ipRequestHeader);
                    if (!string.IsNullOrEmpty(remoteIp))
                    {
                        System.Net.IPAddress ipAddress = null;
                        bool isValidIp = System.Net.IPAddress.TryParse(remoteIp, out ipAddress);
                        if (isValidIp)
                            return ipAddress.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
            }
            return string.Empty;
        }

        private string GetIpRequest(string ipRequestHeader)
        {
            if (!string.IsNullOrEmpty(ipRequestHeader))
            {
                if (ipRequestHeader.Contains(","))
                    return ipRequestHeader.Split(',').First().Trim();
                else return ipRequestHeader.Trim();
            }
            return string.Empty;
        }
        #endregion

        #region function generate token and validate token
        private string GenerateToken(string phone, string otp, DateTime expired)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                     new Claim(ClaimTypes.NameIdentifier, phone),
                                                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                                    new Claim("phone", phone),
                                                    new Claim("otp", otp)
                }),
                Expires = expired,
                Issuer = _configuration["JWT:Issuer"],
                Audience = _configuration["JWT:Audience"],
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["JWT:SigningKey"])), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        #endregion       

        #region Tạo đơn vay
        [HttpPost]
        [Route("init_loanbrief")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<DTOs.LendingOnlineDTO.LoanBriefDetail>>> InitLoanBrief(LendingOnline.InitLoanBriefReq req)
        {
            var def = new DefaultResponse<DTOs.LendingOnlineDTO.LoanBriefDetail>();
            try
            {
                if (string.IsNullOrEmpty(req.Phone) || req.LoanAmount <= 0 || req.LoanTime <= 0 || req.PlatformType <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (!req.Phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Số điện thoại không đúng định dạng");
                    return Ok(def);
                }
                var phoneToken = _identityService.GetPhoneIdentity();
                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Có lỗi xảy ra khi lấy SDT từ token!");
                    return Ok(def);
                }
                if (string.Compare(req.Phone, phoneToken, false) != 0)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại khách hàng và sdt OTP không khớp!");
                    return Ok(def);
                }
                //Kiểm tra đã có đơn hay chưa
                var loanInfo = await _unitOfWork.LoanBriefRepository.Query(x => x.Phone == req.Phone
                && (x.ProductId == (int)EnumProductCredit.MotorCreditType_CC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                && (x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.FINISH)
                , null, false).Select(DTOs.LendingOnlineDTO.LoanBriefDetail.ProjectionDetail).FirstOrDefaultAsync();
                if (loanInfo != null)
                {
                    if (loanInfo.Status == (int)EnumLoanStatus.DISBURSED)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay gói xe máy chưa tất toán!");
                        return Ok(def);
                    }
                    //Kiểm tra có phải gói vay gói vay Lending hay không
                    //-> nếu đúng trả ra thông tin đơn vay
                    if (loanInfo.PlatformType.GetValueOrDefault(0) == (int)EnumPlatformType.LendingOnlineApp
                        || loanInfo.PlatformType.GetValueOrDefault(0) == (int)EnumPlatformType.LendingOnlineWeb)
                    {
                        def.meta = new Meta(202, "Khách hàng có đơn đang xử lý xử lý!");
                        def.data = loanInfo;
                        //Nếu KH đang ở step 7 => lấy kết quả location
                        if (loanInfo.Step == 7)
                        {
                            var resultLocation = await _unitOfWork.LogLoanInfoAiRepository.Query(x => x.ServiceType == (int)ServiceTypeAI.Location
                              && x.LoanbriefId == loanInfo.LoanBriefId && x.IsCancel != 1 && !string.IsNullOrEmpty(x.ResultFinal)
                              && x.IsExcuted == (int)EnumLogLoanInfoAiExcuted.Excuted, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();
                            if (resultLocation != null)
                                loanInfo.ResultLocation = resultLocation.ResultFinal;
                        }

                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay thuộc gói vay khác đang được xử lý!");
                        return Ok(def);
                    }
                }
                //Xử lý thêm mới đơn vay
                var data = new DTOs.LendingOnlineDTO.LoanBriefDetail();
                #region Customer
                //Kiểm tra khách hàng đã tồn tại hay chưa nếu chưa có thì thêm mới
                var customerId = 0;
                var fullName = "";
                var cusInfo = await _unitOfWork.CustomerRepository.Query(x => x.Phone == req.Phone, null, false).Select(x => new
                {
                    CustomerId = x.CustomerId,
                    FullName = x.FullName
                }).FirstOrDefaultAsync();
                if (cusInfo != null)
                {
                    customerId = cusInfo.CustomerId;
                    fullName = cusInfo.FullName;
                }
                else
                {
                    var cusObj = new Customer()
                    {
                        Phone = req.Phone,
                        FullName = req.Phone,
                        CreatedAt = DateTime.Now
                    };
                    await _unitOfWork.CustomerRepository.InsertAsync(cusObj);
                    _unitOfWork.Save();
                    customerId = cusObj.CustomerId;
                }
                #endregion

                #region LoanBrief
                var loanObj = new LoanBrief()
                {
                    CustomerId = customerId,
                    PlatformType = req.PlatformType,
                    Phone = req.Phone,
                    FullName = !string.IsNullOrEmpty(fullName) ? fullName : req.Phone,
                    LoanAmount = req.LoanAmount,
                    LoanAmountFirst = req.LoanAmount,
                    LoanTime = req.LoanTime,
                    ProductId = (int)EnumProductCredit.MotorCreditType_CC,
                    ProductDetailId = (int)Common.Extensions.InfomationProductDetail.LO_KT1_KSHK_5tr,
                    Step = 0,
                    Status = EnumLoanStatus.INIT.GetHashCode(),
                    FromDate = DateTime.Now,
                    ToDate = DateTime.Now.AddMonths(req.LoanTime),
                    CreatedTime = DateTime.Now,
                    TypeLoanBrief = (int)TypeLoanBrief.CANHAN,
                    BuyInsurenceCustomer = true//mặc định mua bảo hiểm
                };
                if (loanObj.LoanAmount > 5000000)
                    loanObj.ProductDetailId = (int)Common.Extensions.InfomationProductDetail.LO_KT1_KSHK_12tr;

                await _unitOfWork.LoanBriefRepository.InsertAsync(loanObj);
                _unitOfWork.Save();
                #endregion

                #region LoanBriefResident
                await _unitOfWork.LoanBriefResidentRepository.InsertAsync(new LoanBriefResident()
                {
                    LoanBriefResidentId = loanObj.LoanBriefId,
                });
                #endregion
                #region LoanBriefProperty
                await _unitOfWork.LoanBriefPropertyRepository.InsertAsync(new LoanBriefProperty()
                {
                    LoanBriefPropertyId = loanObj.LoanBriefId,
                    CreatedTime = DateTime.Now
                });
                #endregion
                #region LoanBriefHousehold
                await _unitOfWork.LoanBriefHouseholdRepository.InsertAsync(new LoanBriefHousehold()
                {
                    LoanBriefHouseholdId = loanObj.LoanBriefId
                });
                #endregion
                #region LoanBriefJob
                await _unitOfWork.LoanBriefJobRepository.InsertAsync(new LoanBriefJob()
                {
                    LoanBriefJobId = loanObj.LoanBriefId
                });
                #endregion
                //thêm comment đơn vay
                var message = "Đơn vay được khởi tạo từ App Lending Online";
                if (req.PlatformType == (int)EnumPlatformType.LendingOnlineWeb)
                    message = "Đơn vay được khởi tạo từ Web Lending Online";
                await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = message,
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                    LoanBriefId = loanObj.LoanBriefId
                });
                //Thêm log tạo đơn
                await _unitOfWork.LogInfoCreateLoanbriefRepository.InsertAsync(new LogInfoCreateLoanbrief()
                {
                    LoanBriefId = loanObj.LoanBriefId,
                    FullName = loanObj.FullName,
                    Phone = loanObj.Phone,
                    LoanTime = loanObj.LoanTime,
                    ProductId = loanObj.ProductId,
                    PlatformType = loanObj.PlatformType,
                    CreateDate = DateTime.Now,
                });
                _unitOfWork.Save();
                data.LoanBriefId = loanObj.LoanBriefId;
                data.Step = 0;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/InitLoanBrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Danh sách mục đích vay
        [HttpGet]
        [Route("get_loanpurpose")]
        [AuthorizeTokenOtp]
        public ActionResult<DefaultResponse<List<SelectListItem>>> GetLoanPurpose()
        {
            var def = new DefaultResponse<List<SelectListItem>>();
            try
            {
                var data = Helpers.ExtentionHelper.GetEnumToList(typeof(EnumLoadLoanPurpose));

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/GetLoanPurpose Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Ngành nghề
        [HttpGet]
        [Route("get_job")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<List<JobDetail>>>> GetJob()
        {
            var def = new DefaultResponse<List<JobDetail>>();
            try
            {
                var data = await _unitOfWork.JobRepository.Query(x => x.JobId == (int)Common.Extensions.EnumJobTitle.LamHuongLuong
            || x.JobId == (int)Common.Extensions.EnumJobTitle.LamTuDo
            || x.JobId == (int)Common.Extensions.EnumJobTitle.LamTaiXeCongNghe
            || x.JobId == (int)Common.Extensions.EnumJobTitle.TuDoanh, null, false).Select(JobDetail.ProjectionDetail).ToListAsync();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/GetJob Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_job_by_group")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<List<JobDetail>>>> GetJobByGroup(int groupId)
        {
            var def = new DefaultResponse<List<JobDetail>>();
            try
            {
                if (groupId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var data = await _unitOfWork.JobRepository.Query(x => x.ParentId == groupId, null, false).Select(JobDetail.ProjectionDetail).OrderBy(x => x.Name).ToListAsync();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/GetJobByGroup Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Edit
        [HttpPost]
        [Route("edit_loanbrief")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<object>>> EditLoanBrief(LendingOnline.EditLoanBriefReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.LoanBriefId <= 0 || req.Step <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanBrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.LoanBriefId, null, false).Select(x => new
                {
                    CustomerId = x.CustomerId,
                    Phone = x.Phone,
                    Status = x.Status,
                    LoanBriefRelationship = x.LoanBriefRelationship
                }).FirstOrDefaultAsync();
                if (loanBrief == null)
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
                //Kiểm tra trạng thái đơn vay là khởi tạo hoặc chờ cvkd chuẩn bị hồ sơ thì cho update
                if (loanBrief.Status != (int)EnumLoanStatus.INIT
                    && loanBrief.Status != (int)EnumLoanStatus.WAIT_HUB_EMPLOYEE_PREPARE_LOAN
                    && loanBrief.Status != (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING
                    && loanBrief.Status != (int)EnumLoanStatus.HUB_LOAN_DISTRIBUTING)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn đã được chuyển cho cvkd xử lý. Bạn không thể cập nhật thông tin đơn vay");
                    return Ok(def);
                }
                //Kiểm tra phoneToken và phone đơn vay có giống nhau không
                var phoneToken = _identityService.GetPhoneIdentity();
                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Có lỗi xảy ra khi lấy SDT từ token!");
                    return Ok(def);
                }
                if (string.Compare(loanBrief.Phone, phoneToken, false) != 0)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại khách hàng và sdt OTP không khớp!");
                    return Ok(def);
                }

                if (req.Step == 1)
                {
                    if (string.IsNullOrEmpty(req.FullName) || req.Dob == null || req.Gender == null || req.LoanPurpose <= 0 || req.JobId <= 0 || req.TotalIncome <= 0)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                        return Ok(def);
                    }
                    await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief()
                    {
                        FullName = req.FullName,
                        Dob = req.Dob,
                        Gender = req.Gender,
                        LoanPurpose = req.LoanPurpose
                    });
                    await _unitOfWork.CustomerRepository.UpdateAsync(x => x.CustomerId == loanBrief.CustomerId, x => new Customer()
                    {
                        FullName = req.FullName,
                        Dob = req.Dob,
                        Gender = req.Gender
                    });
                    await _unitOfWork.LoanBriefJobRepository.UpdateAsync(x => x.LoanBriefJobId == req.LoanBriefId, x => new LoanBriefJob()
                    {
                        JobId = req.JobId,
                        JobDescriptionId = req.JobDescriptionId,
                        TotalIncome = req.TotalIncome
                    });
                }
                else if (req.Step == 2)
                {
                    if (string.IsNullOrEmpty(req.Address) || req.ProvinceId <= 0 || req.DistrictId <= 0 || req.WardId <= 0)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                        return Ok(def);
                    }
                    //Nếu là đơn khởi tạo => cập nhật lại thông tin luồng
                    if (loanBrief.Status == (int)EnumLoanStatus.INIT)
                    {
                        await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief()
                        {
                            ProvinceId = req.ProvinceId,
                            DistrictId = req.DistrictId,
                            WardId = req.WardId,
                            PipelineState = 0
                        });
                    }
                    else
                    {
                        await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief()
                        {
                            ProvinceId = req.ProvinceId,
                            DistrictId = req.DistrictId,
                            WardId = req.WardId
                        });
                    }

                    await _unitOfWork.LoanBriefResidentRepository.UpdateAsync(x => x.LoanBriefResidentId == req.LoanBriefId, x => new LoanBriefResident()
                    {
                        ProvinceId = req.ProvinceId,
                        DistrictId = req.DistrictId,
                        WardId = req.WardId,
                        Address = req.Address
                    });
                }
                else if (req.Step == 3)
                {
                    if (string.IsNullOrEmpty(req.RelationshipPhone1) || string.IsNullOrEmpty(req.RelationshipPhone2))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Phải nhập đủ thông tin tham chiếu");
                        return Ok(def);
                    }
                    //Kiểm tra xem đã tồn tại thông tin tham chiếu hay chưa -> nếu có -> Xóa
                    if (loanBrief.LoanBriefRelationship != null && loanBrief.LoanBriefRelationship.Count > 0)
                        await _unitOfWork.LoanBriefRelationshipRepository.DeleteAsync(x => x.LoanBriefId == req.LoanBriefId);
                    //Insert
                    await _unitOfWork.LoanBriefRelationshipRepository.InsertAsync(new LoanBriefRelationship()
                    {
                        LoanBriefId = req.LoanBriefId,
                        Phone = req.RelationshipPhone1,
                        CreatedDate = DateTime.Now
                    });
                    await _unitOfWork.LoanBriefRelationshipRepository.InsertAsync(new LoanBriefRelationship()
                    {
                        LoanBriefId = req.LoanBriefId,
                        Phone = req.RelationshipPhone2,
                        CreatedDate = DateTime.Now
                    });
                }
                else if (req.Step == 4)
                {
                    await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief()
                    {
                        BankId = req.BankId,
                        BankAccountNumber = req.BankAccountNumber,
                        ReceivingMoneyType = (int)LOS.Common.Extensions.EnumTypeReceivingMoney.NumberAccount //nhận tiền bằng số tài khoản
                    });
                }

                await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief()
                {
                    Step = req.Step
                });
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/EditLoanBrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Location
        [HttpPost]
        [Route("send_request_location")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, dynamic>>> SendRequestLocation(LendingOnline.RequestSendRequestLocation entity)
        {
            var def = new DefaultResponse<Meta, dynamic>();
            try
            {
                //kiểm tra xem có đũng mã loanbrief không
                var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false).Select(LoanBriefLendingOnline.ProjectionViewDetail).OrderByDescending(x => x.LoanBriefId).FirstOrDefaultAsync();
                if (loanbrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                    def.data = false;
                    return Ok(def);
                }

                var phoneToken = _identityService.GetPhoneIdentity();

                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                    def.data = false;
                    return Ok(def);
                }

                if (loanbrief.Phone != phoneToken)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                    def.data = false;
                    return Ok(def);
                }

                //Kiểm tra SĐT khách hàng xem có thuộc mạng Viettel hay Mobi không
                if (_networkService.CheckHomeNetwok(loanbrief.Phone) != (int)HomeNetWorkMobile.Viettel
                     && _networkService.CheckHomeNetwok(loanbrief.Phone) != (int)HomeNetWorkMobile.Mobi)
                {

                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Lấy location chỉ hỗ trợ cho mạng Viettel hoặc Mobi");
                    def.data = false;
                    return Ok(def);

                }

                //Kiểm tra xem có request chưa
                var lstLogLoanInfo = await _unitOfWork.LogLoanInfoAiRepository.Query(x => x.ServiceType == (int)ServiceTypeAI.Location && x.LoanbriefId == entity.LoanBriefId, null, false).ToListAsync();
                if (lstLogLoanInfo != null && lstLogLoanInfo.Count > 0 && lstLogLoanInfo.Count(x => x.ResultFinal == "YES" || x.ResultFinal == "NO") >= 2)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Quá số lần lấy Location");
                    def.data = false;
                    return Ok(def);
                }
                //Kiểm tra xem có yêu cầu đang được xử lý không
                var logLoanInfo = lstLogLoanInfo.OrderByDescending(x => x.Id).FirstOrDefault();
                if (logLoanInfo != null && !string.IsNullOrEmpty(logLoanInfo.Request)
                    && !string.IsNullOrEmpty(logLoanInfo.RefCode)
                    && logLoanInfo.IsCancel != 1
                    && logLoanInfo.IsExcuted != (int)EnumLogLoanInfoAiExcuted.HasResult
                    && logLoanInfo.IsExcuted != (int)EnumLogLoanInfoAiExcuted.Excuted)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đang có yêu cầu location khác đang được xử lý. Vui lòng chờ kết quả");
                    def.data = false;
                    return Ok(def);
                }

                var objLogLoanInfoAi = new LogLoanInfoAi()
                {
                    LoanbriefId = entity.LoanBriefId,
                    ServiceType = (int)ServiceTypeAI.Location,
                    CreatedAt = DateTime.Now,
                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest,
                    FromApp = "LendingAPP"
                };
                await _unitOfWork.LogLoanInfoAiRepository.InsertAsync(objLogLoanInfoAi);

                //Lưu lại log lấy location 
                var objLoanBriefNote = new LoanBriefNote()
                {
                    LoanBriefId = entity.LoanBriefId,
                    Note = "System: Hệ thống gửi yêu cầu lấy dữ liệu AI thành công",
                    FullName = "Auto System",
                    Status = 1,
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    UserId = 0,
                    ShopId = EnumShop.Tima.GetHashCode(),
                    ShopName = Description.GetDescription(EnumShop.Tima)
                };
                await _unitOfWork.LoanBriefNoteRepository.InsertAsync(objLoanBriefNote);
                _unitOfWork.Save();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/SendRequestLocation Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                def.data = false;
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("check_location")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, LendingOnline.ResponceLocation>>> CheckLocation(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, ResponceLocation>();
            var data = new ResponceLocation();
            try
            {
                //kiểm tra xem có đũng mã loanbrief không
                var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(LoanBriefLendingOnline.ProjectionViewDetail).OrderByDescending(x => x.LoanBriefId).FirstOrDefaultAsync();
                if (loanbrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                    return Ok(def);
                }

                var phoneToken = _identityService.GetPhoneIdentity();

                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                    return Ok(def);
                }

                if (loanbrief.Phone != phoneToken)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                    return Ok(def);
                }

                //Kiểm tra log location
                var logLoanInfo = await _unitOfWork.LogLoanInfoAiRepository.Query(x => x.ServiceType == (int)ServiceTypeAI.Location
                && x.LoanbriefId == loanbriefId && x.IsCancel != 1, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();
                if (logLoanInfo != null && logLoanInfo.Id > 0)
                {
                    //Kiểm tra xem đã xử lý xong chưa
                    if (logLoanInfo.IsExcuted == (int)EnumLogLoanInfoAiExcuted.Excuted)
                    {
                        if (!string.IsNullOrEmpty(logLoanInfo.Response))
                        {
                            //kiểm tra xem là viettel hay mobile
                            if (logLoanInfo.HomeNetwok == "viettel")
                            {
                                var dataLocation = JsonConvert.DeserializeObject<LocationViettelReq>(logLoanInfo.Response);
                                if (dataLocation != null && dataLocation.data != null)
                                {
                                    data.home = dataLocation.data.home != null ? dataLocation.data.home.result : null;
                                    data.office = dataLocation.data.office != null ? dataLocation.data.office.result : null;
                                    data.other = dataLocation.data.other != null ? dataLocation.data.other.result : null;
                                }
                                else
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Error. Convert string to object {viettel}");
                                    return Ok(def);
                                }
                            }
                            else if (logLoanInfo.HomeNetwok == "mobifone")
                            {
                                var dataLocation = JsonConvert.DeserializeObject<LocationMobiReq>(logLoanInfo.Response);
                                if (dataLocation != null && dataLocation.data != null)
                                {
                                    data.home = dataLocation.data.home != null ? dataLocation.data.home.result : null;
                                    data.office = dataLocation.data.office != null ? dataLocation.data.office.result : null;
                                    data.other = dataLocation.data.other != null ? dataLocation.data.other.result : null;
                                }
                                else
                                {
                                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Error. Convert string to object {mobifone}");
                                    return Ok(def);
                                }
                            }

                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            def.data = data;
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Request bị lỗi hoặc quá thời gian lấy kết quả");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Request được xử lý vui lòng chờ trong giây lát");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Chưa lấy location");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/CheckLocation Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region đoc thông tin đăng ký xe
        [HttpPost]
        [Route("computer_vision_vr")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, MotorbikeRegistrationInfo>>> EkycMotorbikeRegistrationCertificate(IFormFile image, int loanbriefId = 0)
        {
            var def = new DefaultResponse<Meta, MotorbikeRegistrationInfo>();
            try
            {
                if (image == null || loanbriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //kiểm tra xem có đũng mã loanbrief không
                var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(LoanBriefLendingOnline.ProjectionViewDetail).OrderByDescending(x => x.LoanBriefId).FirstOrDefaultAsync();
                if (loanbrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                    return Ok(def);
                }
                var phoneToken = _identityService.GetPhoneIdentity();
                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                    return Ok(def);
                }

                if (loanbrief.Phone != phoneToken)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                    return Ok(def);
                }
                var extension = Path.GetExtension(image.FileName);
                var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                if (allowedExtensions.Contains(extension.ToLower()))
                {
                    var result = _aiService.EkycMotorbikeRegistrationCertificate(await image.GetBytes(), image.FileName);
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = result;
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ảnh không đúng định dạng!");
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/EkycMotorbikeRegistrationCertificate Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region đoc thông tin CMT
        [HttpPost]
        [Route("ekyc_national_card")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, EkycNationalCard.Card>>> EkycNationalCard(IFormFile image, int loanbriefId = 0)
        {
            var def = new DefaultResponse<Meta, EkycNationalCard.Card>();
            try
            {
                if (image == null || loanbriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //kiểm tra xem có đũng mã loanbrief không
                var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(LoanBriefLendingOnline.ProjectionViewDetail).OrderByDescending(x => x.LoanBriefId).FirstOrDefaultAsync();
                if (loanbrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                    return Ok(def);
                }
                var phoneToken = _identityService.GetPhoneIdentity();
                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                    return Ok(def);
                }

                if (string.Compare(loanbrief.Phone, phoneToken, false) != 0)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                    return Ok(def);
                }
                var extension = Path.GetExtension(image.FileName);
                var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                if (allowedExtensions.Contains(extension.ToLower()))
                {
                    var result = _aiService.EkycNationalCard(await image.GetBytes(), image.FileName);
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = result;
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ảnh không đúng định dạng!");
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/EkycNationalCard Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Thông tin fraud check
        [HttpPost]
        [Route("send_request_fraud_detection")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, dynamic>>> SendRequestFraudDetection(FraudDetectionReq entity)
        {
            var def = new DefaultResponse<Meta, dynamic>();
            try
            {
                //kiểm tra xem có đũng mã loanbrief không
                var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(LoanBriefLendingOnline.ProjectionViewDetail).OrderByDescending(x => x.LoanBriefId).FirstOrDefaultAsync();
                if (loanbrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                    def.data = false;
                    return Ok(def);
                }
                var phoneToken = _identityService.GetPhoneIdentity();

                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                    def.data = false;
                    return Ok(def);
                }

                if (loanbrief.Phone != phoneToken)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                    def.data = false;
                    return Ok(def);
                }
                //Kiểm tra xem có request chưa
                var lstLogLoanInfo = await _unitOfWork.LogLoanInfoAiRepository.Query(x => x.ServiceType == (int)ServiceTypeAI.FraudDetection
                && x.LoanbriefId == entity.LoanbriefId, null, false).ToListAsync();
                //Kiểm tra xem có yêu cầu đang được xử lý không
                var logLoanInfo = lstLogLoanInfo.OrderByDescending(x => x.Id).FirstOrDefault();
                if (logLoanInfo != null && !string.IsNullOrEmpty(logLoanInfo.Request)
                    && !string.IsNullOrEmpty(logLoanInfo.RefCode)
                    && logLoanInfo.IsCancel != 1
                    && logLoanInfo.IsExcuted != (int)EnumLogLoanInfoAiExcuted.HasResult
                    && logLoanInfo.IsExcuted != (int)EnumLogLoanInfoAiExcuted.Excuted)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đang có yêu cầu fraud check khác đang được xử lý. Vui lòng chờ kết quả");
                    def.data = false;
                    return Ok(def);
                }

                var objLogLoanInfoAi = new LogLoanInfoAi()
                {
                    LoanbriefId = entity.LoanbriefId,
                    ServiceType = (int)ServiceTypeAI.FraudDetection,
                    CreatedAt = DateTime.Now,
                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest,
                    FromApp = "LendingAPP"
                };
                await _unitOfWork.LogLoanInfoAiRepository.InsertAsync(objLogLoanInfoAi);

                //Lưu lại log lấy location
                var objLoanBriefNote = new LoanBriefNote()
                {
                    LoanBriefId = entity.LoanbriefId,
                    Note = "System: Hệ thống gửi yêu cầu lấy dữ liệu Fraud Check thành công",
                    FullName = "Auto System",
                    Status = 1,
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    UserId = 0,
                    ShopId = EnumShop.Tima.GetHashCode(),
                    ShopName = Description.GetDescription(EnumShop.Tima)
                };
                await _unitOfWork.LoanBriefNoteRepository.InsertAsync(objLoanBriefNote);
                _unitOfWork.Save();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/SendRequestFraudDetection Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                def.data = false;
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("result_fraud_detection")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, FraudCheckResultRes>>> ResultFraudDetection(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, FraudCheckResultRes>();
            var data = new FraudCheckResultRes();
            try
            {
                //kiểm tra xem có đũng mã loanbrief không
                var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(LoanBriefLendingOnline.ProjectionViewDetail).OrderByDescending(x => x.LoanBriefId).FirstOrDefaultAsync();
                if (loanbrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                    return Ok(def);
                }

                var phoneToken = _identityService.GetPhoneIdentity();

                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                    return Ok(def);
                }

                if (loanbrief.Phone != phoneToken)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                    return Ok(def);
                }

                //Kiểm tra log  fraud check
                var logLoanInfo = await _unitOfWork.LogLoanInfoAiRepository.Query(x => x.ServiceType == (int)ServiceTypeAI.FraudDetection
                && x.LoanbriefId == loanbriefId && x.IsCancel != 1, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();
                if (logLoanInfo != null && logLoanInfo.Id > 0)
                {
                    //Kiểm tra xem đã xử lý xong chưa
                    if (logLoanInfo.IsExcuted == (int)EnumLogLoanInfoAiExcuted.Excuted)
                    {
                        if (!string.IsNullOrEmpty(logLoanInfo.Response))
                        {
                            var dataFraudCheck = JsonConvert.DeserializeObject<DAL.Object.ResultFraudCheckModel>(logLoanInfo.Response);
                            if (dataFraudCheck != null)
                            {
                                if (dataFraudCheck.tima_result != null && dataFraudCheck.tima_result.unified_result != null)
                                {
                                    var dataTima = dataFraudCheck.tima_result.unified_result;
                                    if (!string.IsNullOrEmpty(dataTima.channel))
                                        data.Channel = dataTima.channel;
                                    if (dataTima.details != null && dataTima.details.Count > 0)
                                    {
                                        foreach (var item in dataTima.details)
                                            if (!string.IsNullOrEmpty(item.message))
                                                data.Message.Add(item.message);
                                    }
                                }

                                if (dataFraudCheck.hyperverge_result != null && dataFraudCheck.hyperverge_result.unifiedResult != null)
                                {
                                    var dataHyperveger = dataFraudCheck.hyperverge_result.unifiedResult;
                                    if (!string.IsNullOrEmpty(dataHyperveger.channel))
                                        data.HvChannel = dataHyperveger.channel;
                                    if (dataHyperveger.details != null && !string.IsNullOrEmpty(dataHyperveger.details.message))
                                    {
                                        data.HvMessage.Add(dataHyperveger.details.message);
                                    }
                                }
                                if (dataFraudCheck.tima_result != null && !string.IsNullOrEmpty(dataFraudCheck.tima_result.report_url))
                                    data.DetailUrl = dataFraudCheck.tima_result.report_url;
                            }
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            def.data = data;
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Request bị lỗi hoặc quá thời gian lấy kết quả");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Request được xử lý vui lòng chờ trong giây lát");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Chưa lấy location");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/ResultFraudDetection Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #endregion

        [HttpGet]
        [Route("loan_info")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<DTOs.LendingOnlineDTO.LoanBriefDetail>>> GetLoanInfo(string phone)
        {
            var def = new DefaultResponse<DTOs.LendingOnlineDTO.LoanBriefDetail>();
            try
            {
                if (string.IsNullOrEmpty(phone))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (!phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Số điện thoại không đúng định dạng");
                    return Ok(def);
                }

                var phoneToken = _identityService.GetPhoneIdentity();
                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Có lỗi xảy ra khi lấy SDT từ token!");
                    return Ok(def);
                }
                if (string.Compare(phone, phoneToken, false) != 0)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại khách hàng và sdt OTP không khớp!");
                    return Ok(def);
                }
                //Kiểm tra đã có đơn hay chưa
                var loanInfo = await _unitOfWork.LoanBriefRepository.Query(x => x.Phone == phone
                && (x.ProductId == (int)EnumProductCredit.MotorCreditType_CC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                && (x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.FINISH)
                , null, false).Select(DTOs.LendingOnlineDTO.LoanBriefDetail.ProjectionDetail).FirstOrDefaultAsync();
                if (loanInfo != null)
                {
                    if (loanInfo.Status == (int)EnumLoanStatus.DISBURSED)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay gói xe máy chưa tất toán!");
                        return Ok(def);
                    }
                    //Lấy thông tin mục đích vay
                    if (loanInfo.LoanPurpose > 0 && Enum.IsDefined(typeof(EnumLoadLoanPurpose), loanInfo.LoanPurpose))
                        loanInfo.LoanPurposeName = Description.GetDescription((EnumLoadLoanPurpose)loanInfo.LoanPurpose);

                    //Kiểm tra có phải gói vay gói vay Lending hay không
                    //-> nếu đúng trả ra thông tin đơn vay
                    if (loanInfo.PlatformType.GetValueOrDefault(0) == (int)EnumPlatformType.LendingOnlineApp
                        || loanInfo.PlatformType.GetValueOrDefault(0) == (int)EnumPlatformType.LendingOnlineWeb)
                    {

                        //Nếu KH đang ở step 7 => lấy kết quả location
                        if (loanInfo.Step == 7)
                        {
                            var resultLocation = await _unitOfWork.LogLoanInfoAiRepository.Query(x => x.ServiceType == (int)ServiceTypeAI.Location
                              && x.LoanbriefId == loanInfo.LoanBriefId && x.IsCancel != 1 && !string.IsNullOrEmpty(x.ResultFinal)
                              && x.IsExcuted == (int)EnumLogLoanInfoAiExcuted.Excuted, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();
                            if (resultLocation != null)
                                loanInfo.ResultLocation = resultLocation.ResultFinal;
                        }
                        //Lây thông tin chứng từ
                        if (loanInfo.Step >= 5)
                        {
                            var listFile = await _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanInfo.LoanBriefId && x.Status == 1
                            && (x.TypeId == (int)EnumDocumentType.CMND_CCCD || x.TypeId == (int)EnumDocumentType.Selfie
                            || x.TypeId == (int)EnumDocumentType.Motorbike_Registration_Certificate), null, false).OrderByDescending(x => x.Id).ToListAsync();
                            if (listFile != null && listFile.Count > 0)
                            {
                                var ServiceURL = _configuration["AppSettings:ServiceURL"];
                                var ServiceURLAG = _configuration["AppSettings:ServiceURLAG"];
                                var ServiceURLFileTima = _configuration["AppSettings:ServiceURLFileTima"];
                                foreach (var item in listFile)
                                {
                                    if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                                    {
                                        item.FilePath = ServiceURL + item.FilePath;
                                        if (!string.IsNullOrEmpty(item.FileThumb))
                                            item.FileThumb = ServiceURL + item.FileThumb;
                                    }
                                    else if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null))
                                    {
                                        item.FilePath = ServiceURLAG + item.FilePath;
                                        if (!string.IsNullOrEmpty(item.FileThumb))
                                            item.FileThumb = ServiceURLAG + item.FileThumb;
                                    }
                                    else if (item.MecashId > 0 && item.S3status == 1)
                                    {
                                        item.FilePath = ServiceURLFileTima + item.FilePath;
                                        if (!string.IsNullOrEmpty(item.FileThumb))
                                            item.FileThumb = ServiceURLFileTima + item.FileThumb;
                                    }
                                    if (string.IsNullOrEmpty(loanInfo.FileNational) && item.TypeId == (int)EnumDocumentType.CMND_CCCD)
                                    {
                                        if (!string.IsNullOrEmpty(item.FileThumb))
                                            loanInfo.FileNational = item.FileThumb;
                                        else
                                            loanInfo.FileNational = item.FilePath;
                                    }
                                    else if (string.IsNullOrEmpty(loanInfo.FileSelfie) && item.TypeId == (int)EnumDocumentType.Selfie)
                                    {
                                        if (!string.IsNullOrEmpty(item.FileThumb))
                                            loanInfo.FileSelfie = item.FileThumb;
                                        else
                                            loanInfo.FileSelfie = item.FilePath;
                                    }
                                    else if (string.IsNullOrEmpty(loanInfo.FileMotobikeCerfiticate) && item.TypeId == (int)EnumDocumentType.Motorbike_Registration_Certificate)
                                    {
                                        if (!string.IsNullOrEmpty(item.FileThumb))
                                            loanInfo.FileMotobikeCerfiticate = item.FileThumb;
                                        else
                                            loanInfo.FileMotobikeCerfiticate = item.FilePath;
                                    }
                                }
                            }
                        }
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = loanInfo;
                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng có đơn vay thuộc gói vay khác đang được xử lý!");
                        return Ok(def);
                    }
                }
                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/GetLoanInfo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("schedule_payment")]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<Meta, ReponseGetLoan>>> SchedulePayment(string phone)
        {
            var def = new DefaultResponse<Meta, ReponseGetLoan>();
            var data = new ReponseGetLoan();
            try
            {
                if (!phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không đúng định dạng!");
                    return Ok(def);
                }

                //kiểm tra xem có đũng mã loanbrief không
                var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.Phone == phone, null, false).Select(LoanBriefLendingOnline.ProjectionViewDetail).OrderByDescending(x => x.LoanBriefId).FirstOrDefaultAsync();
                if (loanbrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                    return Ok(def);
                }

                var phoneToken = _identityService.GetPhoneIdentity();

                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                    return Ok(def);
                }

                if (loanbrief.Phone != phoneToken)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                    return Ok(def);
                }

                if (loanbrief.Status == (int)EnumLoanStatus.DISBURSED)
                {
                    data.CodeId = loanbrief.CodeId;

                    //gọi api sang lms lấy lịch thanh toán của đơn vay
                    var dataLms = _lmsService.GetPaymentLoanById(loanbrief.LmsLoanId.Value);
                    if (dataLms != null && dataLms.Data != null && dataLms.Data.PaymentSchedule != null && dataLms.Data.PaymentSchedule.Count > 0)
                    {
                        //Add lịch thanh toán của LMS
                        var lstPaymentSchedule = new List<PaymentSchedule>();

                        foreach (var item in dataLms.Data.PaymentSchedule)
                        {

                            var objPaymentSchedule = new PaymentSchedule();
                            objPaymentSchedule.PayDate = item.DaysPayable;
                            objPaymentSchedule.PayMoney = item.PayMoney;
                            objPaymentSchedule.Status = item.Done;
                            lstPaymentSchedule.Add(objPaymentSchedule);

                            //kiểm tra xem ngày phải đóng có quá ngày hiện tại không
                            if (!item.Done)
                            {
                                //Nếu ngày phải thanh toán của kỳ <= Ngày hiện tại => kỳ này bị chậm
                                if (item.PayDate <= DateTime.Now)
                                    data.PayMoney += item.PayMoney;
                                else if (data.NextPayDate != null)  //kiểm tra xem có trong cùng 1 tháng không
                                {
                                    data.NextPayDate = item.PayDate;
                                    data.PayMoney += item.PayMoney;
                                }

                            }
                        }
                        data.PaymentSchedule = lstPaymentSchedule;
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không gọi được API sang LMS");
                        return Ok(def);
                    }
                }
                data.LoanAmount = loanbrief.LoanAmount;
                data.LoanTime = loanbrief.LoanTime;
                data.StatusName = Description.GetDescription((EnumLoanStatus)loanbrief.Status);
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LendingOnline/GetLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
