﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.CareSoft;
using LOS.AppAPI.Service.LmsService;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class LoanBriefController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICareSoftService _careSoftService;
        private readonly ILmsService _lmsService;
        private readonly ICiscoService _ciscoService;
        public LoanBriefController(IUnitOfWork unitOfWork, ICareSoftService careSoftService, ILmsService lmsService,
            ICiscoService ciscoService)
        {
            this._unitOfWork = unitOfWork;
            _careSoftService = careSoftService;
            _lmsService = lmsService;
            _ciscoService = ciscoService;
        }

        [HttpGet]
        [Route("wait_appraiser")]
        public async Task<ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefWaitAppraiser>>>> ListWaitAppraiser([FromQuery] int productId = -1, [FromQuery] int page = 1, [FromQuery] int pageSize = 20,
             [FromQuery] string customerName = null, [FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefWaitAppraiser>>();
            try
            {
                int userId = GetUserId();
                var user = _unitOfWork.UserRepository.GetById(userId);
                var data = new List<LoanBriefWaitAppraiser>();
                var totalRecords = 0;
                if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                {
                    var query = _unitOfWork.LoanBriefRepository.Query(x => (x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW
                                || x.Status == (int)EnumLoanStatus.WAIT_HUB_EMPLOYEE_PREPARE_LOAN) && x.InProcess != (int)EnumInProcess.Process
                             && (x.ProductId == productId || productId == -1)
                             && (x.HubEmployeeId == userId)
                             && (customerName == null || customerName == "" || x.FullName.Contains(customerName))
                                     && ((fromDate == null || !fromDate.HasValue || x.CreatedTime.Value > fromDate.Value.AddDays(-1)) && (toDate == null || !toDate.HasValue || x.CreatedTime.Value < toDate.Value.AddDays(1)))
                                     , null, false)
                                 .Select(LoanBriefWaitAppraiser.ProjectionDetail);
                    totalRecords = query.Count();
                    data = await query.OrderBy(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                }
                else if (user.GroupId == EnumGroupUser.FieldSurvey.GetHashCode())
                {
                    var query = _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.WAIT_HO_APPRAISER_REVIEW && x.InProcess != (int)EnumInProcess.Process
                         && (x.ProductId == productId || productId == -1)
                         && (x.FieldHo == userId || x.EvaluationCompanyUserId == userId || x.EvaluationHomeUserId == userId)
                         && (customerName == null || customerName == "" || x.FullName.Contains(customerName))
                                 && ((fromDate == null || !fromDate.HasValue || x.CreatedTime.Value > fromDate.Value.AddDays(-1)) && (toDate == null || !toDate.HasValue || x.CreatedTime.Value < toDate.Value.AddDays(1)))
                                 , null, false)
                             .Select(LoanBriefWaitAppraiser.ProjectionDetail);
                    totalRecords = query.Count();
                    data = await query.OrderBy(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                }
                //xử lý time delay
                if (data != null && data.Count > 0)
                {
                    await Task.Run(() => Parallel.ForEach(data, item =>
                    {
                        int TimeDelay = 0;
                        string TimeDelayValue = string.Empty;
                        item.BeginStartTime = (item.BeginStartTime == null) ? DateTime.Now : item.BeginStartTime;
                        CommonHelper.CaculateTimeGroup(Convert.ToDateTime(item.BeginStartTime), (int)EnumGroupUser.StaffHub, ref TimeDelay, ref TimeDelayValue);
                        item.TimeDelay = TimeDelay;
                        item.TimeDelayValue = TimeDelayValue;
                        if (user.Ipphone > 0)
                            item.LinkCall = _careSoftService.GenarateLinkClick2Call(user.Ipphone.ToString(), item.Phone);

                        if (item.ProductId == (int)EnumProductCredit.MotorCreditType_CC || item.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                            || item.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                            item.IsThamDinhXe = 1;
                    }));
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ListWaitAppraiser Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }
       
        [HttpGet]
        [Route("get_detail")]
        public ActionResult<DefaultResponse<Meta, LoanBriefViewDetail>> GetDetail([FromQuery] int LoanBriefId)
        {
            var userId = 0;
            var def = new DefaultResponse<Meta, LoanBriefViewDetail>();
            try
            {
                userId = GetUserId();
                if (userId == 0)
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Không có thông tin user đăng nhập");
                    return Ok(def);
                }
                var user = _unitOfWork.UserRepository.GetById(userId);
                if (user == null || user.UserId == 0)
                {
                    def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "Không tồn tại thông tin tài khoản trong hệ thống");
                    return Ok(def);
                }
                var data = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId &&
                x.HubEmployeeId == user.UserId
                && x.InProcess != (int)EnumInProcess.Process, null, false).Select(LoanBriefViewDetail.ProjectionDetail).FirstOrDefault();

                if (data != null && data.LoanBriefId > 0)
                {
                    if (data.TypeOfOwnershipId > 0)
                    {
                        data.TypeOfOwnershipName = (Enum.IsDefined(typeof(EnumTypeofownership), data.TypeOfOwnershipId))
                            ? ExtentionHelper.GetDescription((EnumTypeofownership)data.TypeOfOwnershipId) : "";
                    }
                    if (user.Ipphone > 0)
                    {
                        if (!string.IsNullOrEmpty(data.Phone))
                            data.LinkCall = _careSoftService.GenarateLinkClick2Call(user.Ipphone.ToString(), data.Phone);
                        if (data.CustomerRelationship != null && data.CustomerRelationship.Count > 0)
                        {
                            foreach (var item in data.CustomerRelationship)
                            {
                                if (!string.IsNullOrEmpty(data.Phone))
                                    item.LinkCall = _careSoftService.GenarateLinkClick2Call(user.Ipphone.ToString(), item.Phone);
                            }
                        }
                    }
                    if (data.ProductId == (int)EnumProductCredit.MotorCreditType_CC || data.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                        || data.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                        data.IsThamDinhXe = 1;
                    //tính số tiền bảo hiểm: MoneyFeeInsuranceOfCustomer

                    //Tính số tiền lãi
                    //Tổng phí tư vấn: TotalMoneyConsulant
                    //Lãi và phí trả hàng kỳ: TotalMoneyPaymentOnePeriod
                    //Tổng phí dịch vụ: TotalMoneyService
                    //Tiền tất toán: TotalMoneyAccounting
                    //Tổng tiền lãi: TotalMoneyInterest
                    var objReq = new Models.LMS.InterestAndInsurenceInput()
                    {
                        TotalMoneyDisbursement = data.LoanAmount.HasValue ? (long)data.LoanAmount.Value : 0,
                        LoanTime = data.LoanTime.HasValue ? data.LoanTime.Value : 0,
                        RateType = data.RateTypeId.HasValue ? data.RateTypeId.Value : 0,
                        Frequency = data.Frequency.HasValue ? data.Frequency.Value : 0,
                        LoanBriefId = data.LoanBriefId
                    };
                    var InterestAndInsurence = _lmsService.InterestAndInsurence(objReq);
                    if (InterestAndInsurence != null && InterestAndInsurence.Data != null)
                    {
                        data.MoneyFeeInsuranceOfCustomer = InterestAndInsurence.Data.FeeInsurrance;
                        if (InterestAndInsurence.Data.LstPaymentSchedule != null && InterestAndInsurence.Data.LstPaymentSchedule.Any())
                        {
                            data.TotalMoneyConsulant = InterestAndInsurence.Data.LstPaymentSchedule.Sum(x => x.MoneyConsultant);
                            var rowFirst = InterestAndInsurence.Data.LstPaymentSchedule.First();
                            data.TotalMoneyPaymentOnePeriod = rowFirst.TotalMoneyNeedPayment;
                            data.TotalMoneyService = InterestAndInsurence.Data.LstPaymentSchedule.Sum(x => x.MoneyService);
                            data.TotalMoneyInterest = InterestAndInsurence.Data.LstPaymentSchedule.Sum(x => x.MoneyInterest);
                            data.TotalMoneyAccounting = rowFirst.TotalMoneyNeedClosePayment;
                        }
                    }
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Information($"APPAPI LoanBrief/GetDetail Exception loanbriefId; {LoanBriefId} UserId: {userId}");
                Log.Error(ex, "APPAPI LoanBrief/GetDetail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("all_comment")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefNoteDetail>>> GetAllComment([FromQuery] int loanbriefId)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefNoteDetail>>();
            try
            {
                var query = _unitOfWork.LoanBriefNoteRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).OrderByDescending(x => x.CreatedTime).Select(LoanBriefNoteDetail.ProjectionDetail);
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetAllComment Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("disbursed")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefWaitAppraiser>>> ListDisbursed([FromQuery] int page = 1, [FromQuery] int pageSize = 20,
             [FromQuery] string customerName = null, [FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefWaitAppraiser>>();
            try
            {
                int userId = GetUserId();
                var query = _unitOfWork.LoanBriefRepository.Query(x => x.Status == EnumLoanStatus.DISBURSED.GetHashCode()
                && (x.HubEmployeeId == userId || x.EvaluationHomeUserId == userId || x.EvaluationCompanyUserId == userId) && ((x.FullName.Contains(customerName) || customerName == null))
                        && ((!fromDate.HasValue || x.CreatedTime.Value > fromDate.Value.AddDays(-1)) && (!toDate.HasValue || x.CreatedTime.Value < toDate.Value.AddDays(1))), null, false)
                    .Select(LoanBriefWaitAppraiser.ProjectionDetail);
                var totalRecords = query.Count();
                var data = query.OrderByDescending(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ListDisbursed Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("wait_coordinator")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefWaitAppraiser>>> ListWaitCoordinator([FromQuery] int page = 1, [FromQuery] int pageSize = 20,
             [FromQuery] string customerName = null, [FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefWaitAppraiser>>();
            try
            {
                int userId = GetUserId();
                var query = _unitOfWork.LoanBriefRepository.Query(x => x.InProcess != (int)EnumInProcess.Process && (x.Status == EnumLoanStatus.BRIEF_APPRAISER_REVIEW.GetHashCode()
                || x.Status == EnumLoanStatus.BRIEF_APPRAISER_APPROVE_PROPOSION.GetHashCode() || x.Status == EnumLoanStatus.BRIEF_APPRAISER_APPROVE_CANCEL.GetHashCode())
                && (x.EvaluationHomeUserId == userId || x.EvaluationCompanyUserId == userId || x.HubEmployeeId == userId) && ((x.FullName.Contains(customerName) || customerName == null))
                        && ((!fromDate.HasValue || x.CreatedTime.Value > fromDate.Value.AddDays(-1)) && (!toDate.HasValue || x.CreatedTime.Value <= toDate.Value.AddDays(1))), null, false)
                    .Select(LoanBriefWaitAppraiser.ProjectionDetail);
                var totalRecords = query.Count();
                var data = query.OrderByDescending(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ListWaitCoordinator Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_comment")]
        public async Task<ActionResult<DefaultResponse<object>>> AddComment(AddCommentReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var userId = GetUserId();
                if (string.IsNullOrEmpty(req.comment) || userId == 0 || req.loanbriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var user = await _unitOfWork.UserRepository.Query(x => x.UserId == userId, null, false).Select(x => new
                {
                    UserId = x.UserId,
                    GroupId = x.GroupId,
                    FullName = x.FullName
                }).FirstOrDefaultAsync();
                if (user != null && user.UserId > 0)
                {
                    var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.loanbriefId, null, false).Select(x => new
                                                                            {
                                                                                LoanBriefId = x.LoanBriefId,
                                                                                FirstTimeHubFeedBack = x.FirstTimeHubFeedBack,
                                                                                FirstTimeStaffHubFeedback = x.FirstTimeStaffHubFeedback,
                                                                                Status = x.Status,
                                                                                DetailStatusTelesales = x.DetailStatusTelesales,
                                                                                LoanStatusChild = x.LoanStatusDetailChild,
                                                                                BoundTelesaleId = x.BoundTelesaleId,
                                                                                HubId = x.HubId,
                                                                                HubEmployeeId = x.HubEmployeeId,
                                                                                CoordinatorUserId = x.CoordinatorUserId
                                                                            }).FirstOrDefaultAsync();
                    if (loanbrief != null)
                    {
                        if (user.GroupId == (int)EnumGroupUser.ManagerHub || user.GroupId == (int)EnumGroupUser.StaffHub)
                        {
                            if (!loanbrief.FirstTimeHubFeedBack.HasValue)
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                                {
                                    FirstTimeHubFeedBack = DateTime.Now
                                });
                            }
                            if (!loanbrief.FirstTimeStaffHubFeedback.HasValue && user.GroupId == (int)EnumGroupUser.StaffHub)
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                                {
                                    FirstTimeStaffHubFeedback = DateTime.Now
                                });
                            }
                        }
                        await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote
                        {
                            LoanBriefId = req.loanbriefId,
                            Note = $"[APP] {req.comment}",
                            FullName = user.FullName,
                            UserId = user.UserId,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                        });
                        await _unitOfWork.LogLoanActionRepository.InsertAsync(new LogLoanAction
                        {
                            LoanbriefId = loanbrief.LoanBriefId,
                            ActionId = (int)EnumLogLoanAction.Comment,
                            TypeAction = (int)EnumTypeAction.Manual,
                            LoanStatus = loanbrief.Status,
                            TlsLoanStatusDetail = loanbrief.DetailStatusTelesales,
                            HubLoanStatusDetail = loanbrief.LoanStatusChild,
                            NewValues = req.comment,
                            TelesaleId = loanbrief.BoundTelesaleId,
                            HubId = loanbrief.HubId,
                            HubEmployeeId = loanbrief.HubEmployeeId,
                            CoordinatorUserId = loanbrief.CoordinatorUserId,
                            UserActionId = user.UserId,
                            GroupUserActionId = user.GroupId,
                            CreatedAt = DateTime.Now
                        });
                        _unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tồn tại thông tin đơn vay");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddComment Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("generate_callid")]
        public async Task<ActionResult<DefaultResponse<object>>> GenerateCallid(GenerateCallIdRequest req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var userId = GetUserId();
                if (req.LoanbriefId <= 0 || string.IsNullOrEmpty(req.Phone))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var callId = _ciscoService.GenerateCallId();
                if (!string.IsNullOrEmpty(callId))
                {
                    var userInfo = await _unitOfWork.UserRepository.Query(x => x.UserId == userId, null, false).Select(x => new
                    {
                        UserId = x.UserId,
                        UserName = x.Username,
                        GroupId = x.GroupId
                    }).FirstOrDefaultAsync();

                    if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() || userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.LoanbriefId, null, false).Select(x => new
                        {
                            LoanbriefId = x.LoanBriefId,
                            Phone = x.Phone,
                            HubEmployeeCallFirst = x.HubEmployeeCallFirst,
                            FirstProcessingTime = x.FirstProcessingTime,
                            CountCall = x.CountCall,
                            CoordinatorUserId = x.CoordinatorUserId,
                            Status = x.Status,
                            LoanStatusChild = x.LoanStatusDetailChild,
                            HubId = x.HubId,
                            BoundTelesaleId = x.BoundTelesaleId,
                            DetailStatusTelesales = x.DetailStatusTelesales,
                            HubEmployeeId = x.HubEmployeeId
                        }).FirstOrDefaultAsync();

                        if (loanbrief != null && loanbrief.LoanbriefId > 0)
                        {
                            if (userInfo.GroupId == EnumGroupUser.StaffHub.GetHashCode() && loanbrief.HubEmployeeId != userInfo.UserId)
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }
                            else if (userInfo.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                                 && !_unitOfWork.UserShopRepository.Any(x => x.ShopId == loanbrief.HubId && x.UserId == userInfo.UserId))
                            {
                                def.meta = new Meta(ResponseHelper.UNAUTHORIZED_CODE, "NO PERMISSION");
                                return Ok(def);
                            }

                            _unitOfWork.LogClickToCallRepository.Insert(new LogClickToCall()
                            {
                                LoanbriefId = req.LoanbriefId,
                                UserId = userId,
                                PhoneOfCustomer = req.Phone,
                                CreatedAt = DateTime.Now,
                                TypeCallService = (int)EnumTypeCallService.Metech,
                                GetRecording = 0,
                                CallId = callId
                            });

                            _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanbriefId, x => new LoanBrief()
                            {
                                CountCall = (loanbrief.CountCall ?? 0) + 1,
                                HubEmployeeCallFirst = loanbrief.HubEmployeeCallFirst ?? DateTime.Now
                            });

                            _unitOfWork.LogLoanActionRepository.Insert(new LogLoanAction
                            {
                                LoanbriefId = loanbrief.LoanbriefId,
                                ActionId = (int)EnumLogLoanAction.Call,
                                TypeAction = (int)EnumTypeAction.Manual,
                                LoanStatus = loanbrief.Status,
                                TlsLoanStatusDetail = loanbrief.DetailStatusTelesales,
                                HubLoanStatusDetail = loanbrief.LoanStatusChild,
                                TelesaleId = loanbrief.BoundTelesaleId,
                                HubId = loanbrief.HubId,
                                HubEmployeeId = loanbrief.HubEmployeeId,
                                CoordinatorUserId = loanbrief.CoordinatorUserId,
                                UserActionId = userInfo.UserId,
                                GroupUserActionId = userInfo.GroupId,
                                CreatedAt = DateTime.Now,
                                NewValues = $"{(int)EnumTypeCallService.Metech}"
                            });
                            _unitOfWork.Save();
                            def.data = callId;
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Nhóm người dùng không có quyền truy cập");
                    }                 
                   
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Xảy ra lỗi khi tạo callId");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GenerateCallid Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

    }
}