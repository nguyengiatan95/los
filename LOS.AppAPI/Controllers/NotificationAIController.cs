﻿using System;
using LOS.DAL.UnitOfWork;
using LOS.AppAPI.Helpers;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using LOS.AppAPI.Service.SlackService;
using LOS.DAL.EntityFramework;
using LOS.Common.Extensions;
using Newtonsoft.Json;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    // [AuthorizeIPFilter("API_LMS")]
    public class NotificationAIController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ISlackService _slackService;
        public NotificationAIController(IUnitOfWork unitOfWork, ISlackService slackService)
        {
            this.unitOfWork = unitOfWork;
            _slackService = slackService;
        }

        //Send Notification
        [HttpPost]
        [Route("send")]
        public ActionResult<DefaultResponse<object>> ReturnLoan(Models.NotificationAI.SendReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.LoanBriefId <= 0 || req.DeviceId == "" || req.Message == "")
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.LoanBriefId);
                if (loanInfo != null)
                {
                    if (loanInfo.HubId == null || loanInfo.HubId == 0)
                    {
                        def.meta = new Meta(211, "Đơn vay chưa có thông tin Hub!");
                        return Ok(def);
                    }
                    //Get info Shop
                    var shop = unitOfWork.ShopRepository.GetById(loanInfo.HubId.Value);
                    if (shop != null && !string.IsNullOrEmpty(shop.WebhookSlack))
                    {
                        var result = _slackService.SendNotifyOfStack("Hệ thống", "Blue",
                             "HĐ-" + loanInfo.LoanBriefId
                             , string.Format("RePush: HĐ-{0} {1} " + req.Message, loanInfo.LoanBriefId, loanInfo.FullName)
                             , shop.WebhookSlack, loanInfo.LoanBriefId, "hub");

                        if (result == 1)
                        {
                            //Add comment
                            unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote
                            {
                                LoanBriefId = req.LoanBriefId,
                                Note = req.Message,
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                ShopId = 3703,
                                ShopName = "Tima"
                            });
                            unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(213, "SendNotificationToSlackHub err!");
                        }
                    }

                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
                //Add LogCall
                unitOfWork.LogCallApiRepository.Insert(new LogCallApi
                {
                    ActionCallApi = ActionCallApi.PushNotiGino.GetHashCode(),
                    LinkCallApi = "send",
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.Success,
                    LoanBriefId = req.LoanBriefId,
                    Input = JsonConvert.SerializeObject(req),
                    CreatedAt = DateTime.Now
                });
            }
            catch (Exception ex)
            {
                Log.Error(ex, "NotificationAI/send Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

    }
}