﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [AuthorizeToken("APP_API_LENDER")]
    //[Authorize]
    public class NotificationController : ControllerBase
    {

        private readonly IUnitOfWork _unitOfWork;
        public NotificationController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        [HttpPost]
        [Route("add_notify")]
        public ActionResult<DefaultResponse<object>> AddNotify(Notification req)
        {
            var def = new DefaultResponse<object>();
            try
            {

                if (req.LoanbriefId == 0 || string.IsNullOrEmpty(req.Action))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanbrief = _unitOfWork.LoanBriefRepository.GetById(req.LoanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    req.CreatedAt = DateTime.Now;
                    _unitOfWork.NotificationRepository.Insert(req);
                    _unitOfWork.Save();
                    def.data = req.Id;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Notification/AddNotify Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_status")]
        public ActionResult<DefaultResponse<object>> UpdateStatusOfNotify(NotificationUpdateReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {

                if (req.Id == 0 || req.Status == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var notify = _unitOfWork.NotificationRepository.GetById(req.Id);
                if (notify != null && notify.Id > 0)
                {
                    notify.UpdatedAt = DateTime.Now;
                    notify.Status = req.Status;
                    if (!string.IsNullOrEmpty(req.PushTime))
                    {
                        DateTime pushTime = DateTime.ParseExact(req.PushTime, "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
                        notify.PushTime = pushTime;
                    }
                    _unitOfWork.NotificationRepository.Update(notify);
                    _unitOfWork.Save();
                    def.data = req.Id;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Notification/UpdateStatusOfNotify Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("all_notify")]
        public ActionResult<DefaultResponse<Meta, List<NotificationDetail>>> GetAllNotify(string fromDate)
        {
            var def = new DefaultResponse<Meta, List<NotificationDetail>>();
            try
            {
                DateTime nextDate = DateTime.ParseExact(fromDate, "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var query = _unitOfWork.NotificationRepository.Query(x => x.Status == 0 && x.CreatedAt >= nextDate, null, false).OrderByDescending(x => x.CreatedAt).Select(NotificationDetail.ProjectionDetail);
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Notification/GetAllNotify Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}