﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using LOS.AppAPI.DTOs.App;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models.LanDingPage;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.CareSoft;
using LOS.AppAPI.Service.LmsService;
using LOS.Common.Extensions;
using LOS.Common.Utils;
using LOS.DAL.Dapper;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
using static LOS.AppAPI.Models.LanDingPage.LandingPage;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    //[Authorize]
    public class PartnerController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITikiService _tikiService;
        public PartnerController(IUnitOfWork unitOfWork, ITikiService tikiService)
        {
            this._unitOfWork = unitOfWork;
            _tikiService = tikiService;
        }

        [HttpPost]
        [EnableCors("AllowAll")]
        [Route("push_loan")]
        public async Task<ActionResult<DefaultResponse<Meta, object>>> CreateLoanPushToPartner(LandingPage.PushLoanToPartner entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                int tikiId = Int32.Parse(entity.tikiCustomerId);
                if (!entity.partnerProcess)
                {
                    var log = _unitOfWork.LogPushToPartnerRepository.Query(x => x.LoanBriefId == entity.loanBriefId && x.Partner == entity.partner, null, false).FirstOrDefault();
                    if (log == null || (log.Status == 1 && log.PushStatus == 0))
                    {

                        var loanBrief = _unitOfWork.LoanBriefRepository.GetById(entity.loanBriefId);
                        if (loanBrief != null)
                        {
                            if (log == null)
                            {
                                log = new LogPushToPartner();
                                log.Partner = entity.partner;
                                log.PushStatus = 0;
                                log.Status = 1;
                                log.LoanBriefId = entity.loanBriefId;
                                log.CreatedTime = DateTime.Now;
                                log.RetryCount = 0;
                                log.PartnerCustomerId = entity.tikiCustomerId;
                                log.PartnerProcess = entity.partnerProcess;
                                _unitOfWork.LogPushToPartnerRepository.Insert(log);
                                _unitOfWork.SaveChanges();
                            }
                            if (entity.partner.Equals("TIKI"))
                            {
                                // push to partner
                                string address = "";
                                if (loanBrief.DistrictId > 0)
                                    address += _unitOfWork.DistrictRepository.GetById(loanBrief.DistrictId).Name;
                                if (loanBrief.ProvinceId > 0)
                                    address += ", " + _unitOfWork.ProvinceRepository.GetById(loanBrief.ProvinceId).Name;
                                var result = await _tikiService.CreateLead(tikiId.ToString(), loanBrief.FullName, loanBrief.Phone, address);
                                if (result != null && result.data != null && result.data.id != null)
                                {
                                    log.PartnerId = result.data.id;
                                    log.PushStatus = 1;
                                    log.UpdatedTime = DateTime.Now;
                                    _unitOfWork.LogPushToPartnerRepository.Update(log);
                                    _unitOfWork.SaveChanges();
                                    def.meta = new Meta(200, "success");
                                    return Ok(def);
                                }
                                log.RetryCount += 1;
                                log.UpdatedTime = DateTime.Now;
                                _unitOfWork.LogPushToPartnerRepository.Update(log);
                                _unitOfWork.SaveChanges();
                            }
                            def.meta = new Meta(211, "push failed, retry later!");
                        }
                        else
                        {
                            def.meta = new Meta(404, "not found");
                        }
                    }
                    else
                    {
                        // đã đẩy sang partner
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                }
                else
                {
                    // Tìm trong bảng push partner
                    var log = _unitOfWork.LogPushToPartnerRepository.Query(x => x.LoanBriefId == entity.loanBriefId && x.PartnerProcess == true && x.Partner == entity.partner, null, false).FirstOrDefault();
                    if (log == null || (log.Status == 1 && log.PushStatus == 0))
                    {
                        var loanBrief = _unitOfWork.PushLoanToPartnerRepository.GetById(entity.loanBriefId);
                        if (loanBrief != null)
                        {
                            if (log == null)
                            {
                                log = new LogPushToPartner();
                                log.Partner = entity.partner;
                                log.PushStatus = 0;
                                log.Status = 2;
                                log.LoanBriefId = entity.loanBriefId;
                                log.CreatedTime = DateTime.Now;
                                log.RetryCount = 0;
                                log.PartnerCustomerId = entity.tikiCustomerId;
                                log.PartnerProcess = entity.partnerProcess;
                                _unitOfWork.LogPushToPartnerRepository.Insert(log);
                                _unitOfWork.SaveChanges();
                            }
                            if (entity.partner.Equals("TIKI"))
                            {
                                // push to partner
                                string address = loanBrief.DistrictName + ", " + loanBrief.ProvinceName;
                                var result = await _tikiService.CreateLead(tikiId.ToString(), loanBrief.CustomerName, loanBrief.CustomerPhone, address);
                                if (result != null && result.data != null && result.data.id != null)
                                {
                                    log.PartnerId = result.data.id;
                                    log.PushStatus = 1;
                                    log.UpdatedTime = DateTime.Now;
                                    _unitOfWork.LogPushToPartnerRepository.Update(log);
                                    _unitOfWork.SaveChanges();
                                    def.meta = new Meta(200, "success");
                                    return Ok(def);
                                }
                                log.RetryCount += 1;
                                log.UpdatedTime = DateTime.Now;
                                _unitOfWork.LogPushToPartnerRepository.Update(log);
                                _unitOfWork.SaveChanges();
                            }
                            def.meta = new Meta(211, "push failed, retry later!");
                        }
                        else
                        {
                            def.meta = new Meta(404, "not found");
                        }
                    }
                    else
                    {
                        // đã đẩy sang partner
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LandingPage/CreateLoanPushToPartner Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}