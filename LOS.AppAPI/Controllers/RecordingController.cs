﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOS.AppAPI.Helpers;
using LOS.DAL.Dapper;
using LOS.DAL.Object;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class RecordingController : BaseController
    {
        private readonly IConfiguration _baseConfig;
        public RecordingController(IConfiguration configuration)
        {
            _baseConfig = configuration;
        }

        [HttpGet]
        [Route("get_recording")]
        public async Task<ActionResult<DefaultResponse<Meta, List<RecordingDetail>>>> GetRecording(string phoneNumber, DateTime? fromDate = null, DateTime? toDate = null)
        {
            var def = new DefaultResponse<Meta, List<RecordingDetail>>();
            try
            {
                if (string.IsNullOrEmpty(phoneNumber))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                try
                {
                    var db = new DapperHelper(_baseConfig.GetConnectionString("ReportDatabase"));
                    var sql = new StringBuilder();
                    sql.AppendLine("select PhoneNumber,StartTime, CallDuration, Line, AgentUserName, RecordingUrl, PlayUrl, ContactType ");
                    sql.AppendLine("from LogRecording(nolock) ");
                    sql.AppendLine($"where PhoneNumber = '{phoneNumber}' ");
                    if (fromDate != null && fromDate.HasValue)
                        sql.AppendLine($"and DATEDIFF(DAY, '{fromDate.Value.ToString("yyyy-MM-dd")}', StartTime) >= 0 ");
                    if (toDate != null && toDate.HasValue)
                        sql.AppendLine($"and DATEDIFF(day, startTime,  '{toDate.Value.ToString("yyyy-MM-dd")}') >= 0 ");
                    var data = await db.QueryAsync<RecordingDetail>(sql.ToString());
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = data;
                    return Ok(def);
                }
                catch (Exception ex)
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Recording/GetRecording Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_recording_by_user")]
        public async Task<ActionResult<DefaultResponse<Meta, List<RecordingDetail>>>> GetRecordingByUser(string line, DateTime? fromDate = null, DateTime? toDate = null)
        {
            var def = new DefaultResponse<Meta, List<RecordingDetail>>();
            try
            {
                if (string.IsNullOrEmpty(line))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var db = new DapperHelper(_baseConfig.GetConnectionString("ReportDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select PhoneNumber,StartTime, CallDuration, Line, AgentUserName, RecordingUrl, PlayUrl ");
                sql.AppendLine("from LogRecording(nolock) ");
                sql.AppendLine($"where Line = '{line}' ");
                if (fromDate.HasValue)
                    sql.AppendLine($"and DATEDIFF(DAY, '{fromDate.Value.ToString("yyyy-MM-dd")}', StartTime) >= 0 ");
                if (toDate.HasValue)
                    sql.AppendLine($"and DATEDIFF(day, startTime,  '{toDate.Value.ToString("yyyy-MM-dd")}') >= 0 ");
                var data = await db.QueryAsync<RecordingDetail>(sql.ToString());
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Recording/GetRecordingByUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("get_recording_phones")]
        public async Task<ActionResult<DefaultResponse<Meta, List<RecordingDetail>>>> GetRecordingByPhones([FromBody] List<string> phoneNumbers, DateTime? fromDate = null, DateTime? toDate = null)
        {
            var def = new DefaultResponse<Meta, List<RecordingDetail>>();
            try
            {
                if (phoneNumbers == null || phoneNumbers.Count == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var strPhone = string.Join(",", phoneNumbers.Select(x => $"'{x}'").ToArray());
                var db = new DapperHelper(_baseConfig.GetConnectionString("ReportDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select PhoneNumber,StartTime, CallDuration, Line, AgentUserName, RecordingUrl, PlayUrl ");
                sql.AppendLine("from LogRecording(nolock) ");
                sql.AppendLine($"where PhoneNumber in ({strPhone})");
                if (fromDate.HasValue)
                    sql.AppendLine($"and DATEDIFF(DAY, '{fromDate.Value.ToString("yyyy-MM-dd")}', StartTime) >= 0 ");
                if (toDate.HasValue)
                    sql.AppendLine($"and DATEDIFF(day, startTime,  '{toDate.Value.ToString("yyyy-MM-dd")}') >= 0 ");
                var data = await db.QueryAsync<RecordingDetail>(sql.ToString());
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Recording/GetRecording Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
