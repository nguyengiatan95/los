﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Models.ReportLOS;
using LOS.Common.Extensions;
using LOS.DAL.Dapper;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ReportLosController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IConfiguration _configuration;

        public ReportLosController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        [HttpPost]
        [Route("update_loanbrief_duplicate")]
        [AuthorizeToken("APP_API_CHUYEN_HOA")]
        public async Task<ActionResult<DefaultResponse<Meta>>> UpdateLoanbriefDuplicate([FromBody] UpdateDuplicateReq entity)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (entity.loanbriefId <= 0 || entity.duplicateId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền đủ các thông tin");
                    return Ok(def);
                }
                var loanbrief = await unitOfWork.LoanBriefRepository.Query(x=>x.LoanBriefId == entity.loanbriefId, null, false).Select(x=> new
                {
                    LoanBriefId = x.LoanBriefId,
                    Status = x.Status
                }).FirstOrDefaultAsync();
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (loanbrief.Status == (int)EnumLoanStatus.SYSTEM_TELESALE_DISTRIBUTING
                        || loanbrief.Status == (int)EnumLoanStatus.TELESALE_ADVICE)
                    {
                        //tls_duplicate : 55711
                        unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new DAL.EntityFramework.LoanBrief()
                        {
                            BoundTelesaleId = 55711
                        });
                        unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote
                        {
                            LoanBriefId = loanbrief.LoanBriefId,
                            Note = string.Format("Đơn lọc trùng với đơn HĐ-{0}", entity.duplicateId),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            ShopName = "APP"
                        });
                        unitOfWork.Save();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không được chuyển giỏ");
                        return Ok(def);

                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tồn tại thông tin đơn vay trong hệ thống");
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "ReportLos/UpdateLoanbriefDuplicate Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpGet]
        [Route("f2l_telesale")]
        public async Task<ActionResult<DefaultResponse<Meta, F2LModel>>> GetF2L(int telesaleId)
        {
            var def = new DefaultResponse<Meta, F2LModel>();
            try
            {
                if (telesaleId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền đủ các thông tin");
                    return Ok(def);
                }
                var db = new DapperHelper(_configuration.GetConnectionString("RpDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select a.supportLastId as TelesaleId ,Form,	Lead  ");
                sql.AppendLine("from (  ");
                sql.AppendLine("SELECT RD.supportLastId ,COUNT(1) Form	");
                sql.AppendLine("FROM [rp].[ReportDataUpdating](NOLOCK) RD ");
                sql.AppendLine("WHERE datediff(d,  CAST(RD.createDate as DATETIME) , getdate ())  =0 ");
                sql.AppendLine("AND RD.productId in (2,5,28) ");
                sql.AppendLine($"AND RD.supportLastId = {telesaleId} ");
                sql.AppendLine("group by  RD.supportLastId ");
                sql.AppendLine(") a  left join  (  ");
                sql.AppendLine("SELECT RD.supportLastId, COUNT(1) Lead ");
                sql.AppendLine("FROM [rp].[ReportDataUpdating](NOLOCK) RD  ");
                sql.AppendLine("where  datediff( d,   CAST(RD.telesaleFistDateLogPushToHub as DATE), getdate ())  =0  ");
                sql.AppendLine($"and  RD.supportLastId = {telesaleId} -- HO ");
                sql.AppendLine("AND  RD.productId in (2,5,28) ");
                sql.AppendLine("group by  RD.supportLastId  ");
                sql.AppendLine(") b on    a.  supportLastId = b . supportLastId  ");
                var data = await db.QueryFirstOrDefaultAsync<F2LModel>(sql.ToString());
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "ReportLos/GetF2L Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("loan_cancel")]
        [AuthorizeToken("APP_API_REPORT")]
        public async Task<ActionResult<DefaultResponse<Meta>>> AddLoanCancel([FromBody] LoanCancelReq entity)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (entity.Data == null || entity.Data.Count == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền đủ các thông tin");
                    return Ok(def);
                }

                foreach (var item in entity.Data)
                {
                    if (item.LoanbriefId > 0)
                    {
                        await unitOfWork.LogCancelLoanbriefRepository.InsertAsync(new LogCancelLoanbrief()
                        {
                            LoanbriefId = item.LoanbriefId,
                            ReasonCancel = item.ReasonCancel,
                            ReasonComment = item.ReasonComment,
                            CreatedAt = DateTime.Now,
                            IsExcuted = 0
                        });
                    }
                }
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "ReportLos/AddLoanCancel Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("log_distribution_telelse")]
        [AuthorizeToken("APP_API_REPORT")]
        public async Task<ActionResult<DefaultResponse<Meta>>> CiscoLogDistributionTelesale([FromBody] CiscoLogDistributionTelesaleReq entity)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (entity.Data == null || entity.Data.Count == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền đủ các thông tin");
                    return Ok(def);
                }

                foreach (var item in entity.Data)
                {
                    if (item.LoanBriefId > 0)
                    {
                        await unitOfWork.LogCiscoPushToTelesaleRepository.InsertAsync(new LogCiscoPushToTelesale()
                        {
                            LoanBriefId = item.LoanBriefId,
                            IsPushToTelesale = item.IsPushToTelesale,
                            CiscoStatus = item.CiscoStatus,
                            CreateTime = DateTime.Now
                        });
                    }
                }
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "ReportLos/LogDistributionTelesale Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("recreate_loan_cancel")]
        [AuthorizeToken("APP_API_REPORT")]
        public async Task<ActionResult<DefaultResponse<Meta>>> RecreatedLoanCancel([FromBody] ReCreateLoanCancel data)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (data.LoanbriefIds == null || data.LoanbriefIds.Count == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền đủ các thông tin");
                    return Ok(def);
                }
                if (data.TelesaleId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền thông tin giỏ nhận đơn");
                    return Ok(def);
                }

                var lastTeamTelesaleId = await unitOfWork.LogReLoanbriefRepository.Query(x => x.TypeReLoanbrief == (int)TypeReLoanbrief.CancelLoanbrief
                    && x.TeamTelesaleId > 0 && x.UtmSource == "autocall_form_cancel_rmkt", null, false)
                    .OrderByDescending(x => x.Id).Select(x => x.TeamTelesaleId).FirstOrDefaultAsync();
                foreach (var entity in data.LoanbriefIds)
                {
                    var loanbriefRemarketingItem = new LogReLoanbrief()
                    {
                        LoanbriefId = entity,
                        CreatedAt = DateTime.Now,
                        CreatedBy = 1,
                        TypeRemarketing = EnumTypeRemarketing.IsRemarketing.GetHashCode(),
                        UtmSource = "autocall_form_cancel_rmkt",
                        IsExcuted = (int)LogReLoanbriefExcuted.NoExcuted,
                        TypeReLoanbrief = TypeReLoanbrief.CancelLoanbrief.GetHashCode(),
                        TelesaleId = data.TelesaleId,
                    };
                    if (data.TelesaleId == (int)EnumUser.follow_team || data.TelesaleId == (int)EnumUser.system_team
                        || data.TelesaleId == (int)EnumUser.recare_team)
                    {

                        //
                        if (lastTeamTelesaleId == (int)EnumTeamTelesales.HN1)
                            data.TeamTelesaleId = (int)EnumTeamTelesales.HN2;
                        else if (lastTeamTelesaleId == (int)EnumTeamTelesales.HN2)
                            data.TeamTelesaleId = (int)EnumTeamTelesales.HN3;
                        else
                            data.TeamTelesaleId = (int)EnumTeamTelesales.HN1;
                        lastTeamTelesaleId = data.TeamTelesaleId;
                        loanbriefRemarketingItem.TeamTelesaleId = data.TeamTelesaleId;
                    }
                    unitOfWork.LogReLoanbriefRepository.Insert(loanbriefRemarketingItem);
                }
                await unitOfWork.SaveAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "ReportLos/RecreatedLoanCancel Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("change_telesale")]
        [AuthorizeToken("APP_API_REPORT")]
        public async Task<ActionResult<DefaultResponse<Meta>>> ChangeTelesale([FromBody] ReCreateLoanCancel data)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (data.LoanbriefIds == null || data.LoanbriefIds.Count == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền đủ các thông tin");
                    return Ok(def);
                }
                if (data.TelesaleId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền thông tin giỏ nhận đơn");
                    return Ok(def);
                }
                var telesale = unitOfWork.UserRepository.Query(x => x.UserId == data.TelesaleId
                && x.Status == 1 && x.GroupId == (int)EnumGroupUser.Telesale, null, false).FirstOrDefault();
                if (telesale == null)
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tìm thấy thông tin giỏ nhận đơn");
                    return Ok(def);
                }
                if (telesale.TeamTelesalesId == (int)EnumTeamTelesales.SystemTeam)
                {
                    if (data.TeamTelesaleId.GetValueOrDefault(0) == 0)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Vui lòng truyền thông tin team");
                        return Ok(def);
                    }
                }
                foreach (var entity in data.LoanbriefIds)
                {
                    unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote
                    {
                        LoanBriefId = entity,
                        Note = string.Format("Hợp đồng HĐ-{0} đã được hệ thống chuyển tự động cho {1} ", entity, telesale.Username),
                        FullName = "AutoCall",
                        Status = 1,
                        ActionComment = EnumActionComment.SaleAdminSplitLoan.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 1,
                        ShopId = 1,
                        ShopName = "Tima"
                    });
                    unitOfWork.LogDistributionUserRepository.Insert(new LogDistributionUser()
                    {
                        LoanbriefId = entity,
                        UserId = telesale.UserId,
                        TypeDistribution = (int)TypeDistributionLog.TELESALE,
                        CreatedAt = DateTime.Now
                    });
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity, x => new LoanBrief()
                    {
                        BoundTelesaleId = telesale.UserId,
                        TeamTelesalesId = data.TeamTelesaleId > 0 ? data.TeamTelesaleId : telesale.TeamTelesalesId
                    });
                }
                await unitOfWork.SaveAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "ReportLos/ChangeTelesale Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("put_kpi_telesale")]
        //[AuthorizeToken("APP_API_REPORT")]
        public ActionResult<DefaultResponse<Meta>> PutKpiTelesale([FromForm] string month, [FromForm] string data)
        {
            var def = new DefaultResponse<Meta>();
            try
            {                
                if (!String.IsNullOrEmpty(data) && !String.IsNullOrEmpty(month))
				{
                    // Remove [ ] Đầu và cuối
                    data = data.Substring(0, data.Length).Substring(0, data.Length - 1);
                    // split theo dòng
                    var lines = data.Split("],[");
                    var insertedTime = DateTime.Now;
                    if (lines.Length >= 3)
					{
                        List<KpiTelesale> kpis = new List<KpiTelesale>();
                        List<KpiTelesaleSummary> kpiSummaries = new List<KpiTelesaleSummary>();
                        Dictionary<int, KpiTelesaleSummary> updateKpiSummaries = new Dictionary<int, KpiTelesaleSummary>();
                        // split từng cột
                        for (int i = 2; i < lines.Length; i++)
                        {
                            var cols = lines[i].Split(",");
                            var kpiTelesale = new KpiTelesale();
                            var kpiSummary = unitOfWork.KpiTelesaleSummaryRepository.Query(x => x.UserId == Int32.Parse(cols[0]) && x.MonthKpi == month, null, false).FirstOrDefault();
                            if (kpiSummary == null)
                            {
                                kpiSummary = new KpiTelesaleSummary();
                            }
                            kpiSummary.MonthKpi = kpiTelesale.MonthKpi = month;
                            kpiTelesale.InstertedTime = insertedTime;
                            kpiSummary.UserId = kpiTelesale.UserId = Int32.Parse(cols[0]);
                            kpiSummary.FullName = kpiTelesale.FullName = cols[1].Replace("\"", "");
                            kpiSummary.Form = kpiTelesale.Form = IsNumeric(cols[2]) ? Int32.Parse(cols[2]) : 0;
                            kpiSummary.Lead = kpiTelesale.Lead = IsNumeric(cols[3]) ? Int32.Parse(cols[3]) : 0;
                            kpiSummary.Loan = kpiTelesale.Loan = IsNumeric(cols[4]) ? Int32.Parse(cols[4]) : 0;
                            kpiSummary.LoanAmountProductMotor = kpiTelesale.LoanAmountProductMotor = IsNumeric(cols[5]) ? Int64.Parse(cols[5]) : 0;
                            kpiSummary.LoanAmountProductCar = kpiTelesale.LoanAmountProductCar = IsNumeric(cols[6]) ? Int64.Parse(cols[6]) : 0;
                            kpiSummary.TotalAmount = kpiTelesale.TotalAmount = IsNumeric(cols[7]) ? Int64.Parse(cols[7]) : 0;
                            kpiSummary.F2l = kpiTelesale.F2l = IsNumeric(cols[8]) ? Double.Parse(cols[8]) : 0;
                            kpiSummary.F2s = kpiTelesale.F2s = IsNumeric(cols[9]) ? Double.Parse(cols[9]) : 0;
                            kpiSummary.TotalKpi = kpiTelesale.TotalKpi = IsNumeric(cols[10]) ? Double.Parse(cols[10]) : 0;
                            kpiSummary.KpiMotor = kpiTelesale.KpiMotor = IsNumeric(cols[11]) ? Double.Parse(cols[11]) : 0;
                            kpiSummary.F2lpercent = kpiTelesale.F2lpercent = IsNumeric(cols[12]) ? Double.Parse(cols[12]) : 0;
                            kpiSummary.F2spercent = kpiTelesale.F2spercent = IsNumeric(cols[13]) ? Double.Parse(cols[13]) : 0;
                            kpiSummary.CompletePercent = kpiTelesale.CompletePercent = IsNumeric(cols[14]) ? Double.Parse(cols[14]) : 0;
                            kpiSummary.QuintilePerformance = kpiTelesale.QuintilePerformance = IsNumeric(cols[15]) ? Double.Parse(cols[15]) : 0;
                            kpiSummary.NewIdMoney = kpiTelesale.NewIdMoney = IsNumeric(cols[16]) ? Int64.Parse(cols[16]) : 0;
                            kpiSummary.CarMoney = kpiTelesale.CarMoney = IsNumeric(cols[17]) ? Int64.Parse(cols[17]) : 0;
                            kpiSummary.Incentive = kpiTelesale.Incentive = IsNumeric(cols[18]) ? Int64.Parse(cols[18]) : 0;
                            kpiSummary.Bonus = kpiTelesale.Bonus = IsNumeric(cols[19]) ? Double.Parse(cols[19]) : 0;
                            kpiSummary.BonusMoney = kpiTelesale.BonusMoney = IsNumeric(cols[20]) ? Int64.Parse(cols[20]) : 0;
                            kpiSummary.Behavior = kpiTelesale.Behavior = IsNumeric(cols[21]) ? Double.Parse(cols[20]) : 0;
                            kpiSummary.DiscountBehavior = kpiTelesale.DiscountBehavior = IsNumeric(cols[22]) ? Int64.Parse(cols[20]) : 0;
                            kpiSummary.Discount = kpiTelesale.Discount = IsNumeric(cols[23]) ? Int64.Parse(cols[20]) : 0;
                            kpiSummary.Salary = kpiTelesale.Salary = IsNumeric(cols[24]) ? Int64.Parse(cols[24]) : 0;
                            kpiSummary.ContestMotor = kpiTelesale.ContestMotor = IsNumeric(cols[25]) ? Int64.Parse(cols[25]) : 0;
                            kpiSummary.ContestCar = kpiTelesale.ContestCar = IsNumeric(cols[26]) ? Int64.Parse(cols[26]) : 0;
                            kpiSummary.ContestVanto = kpiTelesale.ContestVanto = IsNumeric(cols[27]) ? Int64.Parse(cols[27]) : 0;
                            kpiSummary.SalaryAndContest = kpiTelesale.SalaryAndContest = IsNumeric(cols[28]) ? Int64.Parse(cols[28]) : 0;
                            kpis.Add(kpiTelesale);
                            // Summary                            
                            if (kpiSummary.Id == 0)
                            {
                                kpiSummaries.Add(kpiSummary);                               
                            } 
                            else
							{
                                if (!updateKpiSummaries.ContainsKey(kpiSummary.UserId.Value))
                                    updateKpiSummaries.Add(kpiSummary.UserId.Value, kpiSummary);
                            }
                        }
                        unitOfWork.KpiTelesaleRepository.Insert(kpis);
                        unitOfWork.KpiTelesaleSummaryRepository.Insert(kpiSummaries);
                        foreach (var update in updateKpiSummaries.Values)
						{
                            unitOfWork.KpiTelesaleSummaryRepository.Update(update);
						}
                        unitOfWork.SaveChanges();
                        def.meta = new Meta(200, "success");
					}   
                    else
					{
                        def.meta = new Meta(400, "bad request");
                    }                        
				}   
                else
				{
                    def.meta = new Meta(400, "bad request");                    
				}                    
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Report/Update KPI Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        public bool IsNumeric(string value)
        {
            return value.All(char.IsNumber);
        }
    }
}
