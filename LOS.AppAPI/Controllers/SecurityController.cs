﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using LOS.AppAPI.DTOs.Webhook;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Models.Sercurity;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.LmsService;
using LOS.AppAPI.Service.SSOService;
using LOS.Common.Extensions;
using LOS.Common.Utils;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    //[Authorize]
    public class SecurityController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration configuration;
        private ISSOService _ssoService;
        private ILmsService _lmsService;
        protected IConfiguration _configuration;
        protected IVoiceOtpService _voiceOtpService;

        public SecurityController(IUnitOfWork unitOfWork, IConfiguration configuration, ISSOService ssoService, ILmsService lmsService, IVoiceOtpService voiceOtpService)
        {
            this._unitOfWork = unitOfWork;
            this.configuration = configuration;
            _ssoService = ssoService;
            _lmsService = lmsService;
            _voiceOtpService = voiceOtpService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _configuration = builder.Build();
        }

        [HttpPost]
        [EnableCors("AllowAll")]
        [Route("login")]
        [AllowAnonymous]
        public ActionResult<DefaultResponse<UserInfo>> Login([FromBody] LoginReq req)
        {
            var def = new DefaultResponse<UserInfo>();
            try
            {
                if (string.IsNullOrEmpty(req.username) || string.IsNullOrEmpty(req.password))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //Login SSO
                var obj = new Models.SSO.SSO.LoginInput
                {
                    Username = req.username,
                    Password = req.password
                };
                var login = _ssoService.LoginSSO(obj);
                if (login.Meta.ErrorCode == 200)
                {
                    var data = _unitOfWork.UserRepository.Query(x => x.UserIdSso == login.Data.Id, null, false).Select(UserInfo.ProjectionDetail).FirstOrDefault();
                    if (data != null)
                    {
                        #region generate token
                        var claims = new[]{
                           new Claim(ClaimTypes.NameIdentifier, data.UserId.ToString()),
                           new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                           new Claim("Id", data.UserId.ToString()),
                           new Claim("GroupId", data.GroupId.ToString())
                    };
                        var Expiration = DateTime.UtcNow.AddDays(30);
                        if (data.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                            Expiration = DateTime.UtcNow.AddDays(1);
                        var token = new JwtSecurityToken
                        (
                            issuer: configuration["JWT:Issuer"],
                            audience: configuration["JWT:Audience"],
                            claims: claims,
                            expires: Expiration,
                            notBefore: DateTime.UtcNow,
                            signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:SigningKey"])),
                                    SecurityAlgorithms.HmacSha256)
                        );

                        data.Token = new JwtSecurityTokenHandler().WriteToken(token);
                        data.Expiration = Expiration;
                        #endregion

                        //lấy thông tin call mobile
                        var userMobile = _unitOfWork.UserMobileCiscoRepository.Query(x => x.UserId == data.UserId, null, false).FirstOrDefault();
                        if (userMobile != null)
                        {
                            if (!string.IsNullOrEmpty(userMobile.MbciscoPassword) && !string.IsNullOrEmpty(userMobile.MbCiscoExtension))
                            {
                                data.MobileExtension = userMobile.MbCiscoExtension;
                                data.MobilePassword = userMobile.MbciscoPassword;
                            }
                        }
                        if (data.GroupId == EnumGroupUser.FieldSurvey.GetHashCode())
                            data.GroupId = (int)EnumGroupUser.StaffHub;
                        def.data = data;
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data.Password = string.Empty;
                        return Ok(def);
                    }
                    else
                    {
                        if (_unitOfWork.UserRepository.Any(x => x.Username == req.username))
                        {
                            def.meta = new Meta(ResponseHelper.USER_WRONG_PASSWORD_CODE, ResponseHelper.USER_WRONG_PASSWORD_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta((int)login.Meta.ErrorCode, login.Meta.ErrorMessage);
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "API_APP Security/Login exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return Ok(def);
            }
        }

        [HttpPost]
        [EnableCors("AllowAll")]
        [Route("authen")]
        [AllowAnonymous]
        public ActionResult<DefaultResponse<AuthenRes>> Authentication([FromBody] LoginReq req)
        {
            var def = new DefaultResponse<AuthenRes>();
            try
            {
                if (string.IsNullOrEmpty(req.username) || string.IsNullOrEmpty(req.password))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);

                }
                var obj = new Models.SSO.SSO.LoginInput
                {
                    Username = req.username,
                    Password = req.password
                };
                var resultSso = _ssoService.LoginSSO(obj);
                if (resultSso != null && resultSso.Meta.ErrorCode == 200)
                {
                    var data = _unitOfWork.UserRepository.Query(x => x.UserIdSso == resultSso.Data.Id, null, false).Select(AuthenRes.ProjectionDetail).FirstOrDefault();
                    if (data != null)
                    {
                        #region generate token
                        var claims = new[]{
                           new Claim(ClaimTypes.NameIdentifier, data.UserId.ToString()),
                           new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                           new Claim("Id", data.UserId.ToString())
                        };
                        var Expiration = DateTime.UtcNow.AddDays(1);
                        var token = new JwtSecurityToken
                        (
                            issuer: configuration["JWT:Issuer"],
                            audience: configuration["JWT:Audience"],
                            claims: claims,
                            expires: Expiration,
                            notBefore: DateTime.UtcNow,
                            signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:SigningKey"])),
                                    SecurityAlgorithms.HmacSha256)
                        );

                        data.Token = new JwtSecurityTokenHandler().WriteToken(token);
                        data.Expiration = Expiration.ToString("dd/MM/yyyy hh:mm:ss");
                        #endregion
                        def.data = data;
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                    else
                    {
                        if (_unitOfWork.UserRepository.Any(x => x.Username == req.username))
                            def.meta = new Meta(ResponseHelper.USER_WRONG_PASSWORD_CODE, ResponseHelper.USER_WRONG_PASSWORD_MESSAGE);
                        else
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    }
                }
                else
                {
                    def.meta = new Meta((int)resultSso.Meta.ErrorCode, resultSso.Meta.ErrorMessage);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "API_APP Security/Login exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return Ok(def);
            }
        }

        [HttpPost]
        [EnableCors("AllowAll")]
        [Route("check_authen")]
        [AllowAnonymous]
        public ActionResult<DefaultResponse<AuthenRes>> CheckAuthentication([FromBody] LoginReq req)
        {
            var def = new DefaultResponse<AuthenRes>();
            try
            {
                if (string.IsNullOrEmpty(req.username) || string.IsNullOrEmpty(req.password))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //req.password = Security.GetMD5Hash(req.password);
                var obj = new Models.SSO.SSO.LoginInput
                {
                    Username = req.username,
                    Password = req.password
                };
                var login = _ssoService.LoginSSO(obj);
                if (login.Meta.ErrorCode == 200)
                {
                    var data = _unitOfWork.UserRepository.Query(x => x.UserIdSso == login.Data.Id, null, false).Select(AuthenRes.ProjectionDetail).FirstOrDefault();
                    if (data != null)
                    {
                        def.data = data;
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                    else
                    {
                        if (_unitOfWork.UserRepository.Any(x => x.Username == req.username))
                        {
                            def.meta = new Meta(ResponseHelper.USER_WRONG_PASSWORD_CODE, ResponseHelper.USER_WRONG_PASSWORD_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                        }
                    }
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(login.Meta.ErrorCode, login.Meta.ErrorMessage);
                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "API_APP Security/CheckAuthentication exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("get_otp")]
        public async Task<ActionResult<DefaultResponse<Meta, string>>> GetOtp([FromQuery] string phone)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var def = new DefaultResponse<Meta, string>();
            try
            {
                var lifeTimeOtp = 15;
                if (string.IsNullOrEmpty(phone))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (!phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Số điện thoại không đúng định dạng");
                    return Ok(def);
                }

                //Kiểm tra xem sdt của KH có khoản vay không
                var loaninfo = await _unitOfWork.LoanBriefRepository.Query(x => x.Phone == phone
                    && x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.DISBURSED
                    && x.Status != (int)EnumLoanStatus.FINISH, null, false).Select(x => new { LoanbriefId = x.LoanBriefId }).FirstOrDefaultAsync();
                if (loaninfo == null)
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Số điện thoại không tồn tại đơn vay");
                    return Ok(def);
                }
                //Kiểm tra xem còn có otp có hiệu lực không
                var logSendOtp = await _unitOfWork.LogSendOtpRepository.Query(x => x.Phone == phone
                    && x.StatusSend == (int)StatusCallApi.Success
                    && x.VerifyStatus.GetValueOrDefault(0) == (int)VerifyOtp.WaitingVerify, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();
                if (logSendOtp != null && logSendOtp.Id > 0)
                {
                    //Nếu thời chờ verify còn quá nửa thời gian => trả luôn otp này
                    if ((logSendOtp.ExpiredVerifyOtp.Value - DateTime.Now).TotalMinutes > (lifeTimeOtp / 2))
                    {
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = logSendOtp.Otp;
                        return Ok(def);
                    }
                }

                if (environmentName == "Development")
                {
                    var log = new LogSendOtp
                    {
                        Phone = phone,
                        Otp = "123456",
                        Request = "",
                        CreatedAt = DateTime.Now,
                        IpAddress = "",
                        Url = "",
                        TypeSendOtp = (int)TypeSendOtp.SmsBrandName,
                        StatusSend = (int)StatusCallApi.Success,
                        ExpiredVerifyOtp = DateTime.Now.AddMinutes(lifeTimeOtp)
                    };
                    _unitOfWork.LogSendOtpRepository.Insert(log);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = "123456";
                    return Ok(def);
                }
                //Send otp mới
                Random random = new Random();
                var otp = random.Next(100001, 999999);
                var isSuccess = false;
                if (_configuration["AppSettings:UsingVoiceOtp"] == "1")
                {
                    var result = _voiceOtpService.SendOtpVoiceOtp((int)TypeOtp.VerifyPhone, phone, otp.ToString(), DateTime.Now.AddMinutes(lifeTimeOtp), GetIpAddressRemote());
                    if (!string.IsNullOrEmpty(result.status) && result.status.AsToLower() == "ok")
                        isSuccess = true;
                }
                else
                {
                    var mess = $"Tima: Ma xac minh cua ban la {otp}. Ma co hieu luc trong vong {lifeTimeOtp} phut. Khong chia se ma nay voi nguoi khac.";
                    var result = _lmsService.SendOtpSmsBrandName((int)TypeOtp.VerifyPhone, phone, mess, otp.ToString(), DateTime.Now.AddMinutes(lifeTimeOtp), GetIpAddressRemote());
                    isSuccess = (result != null && result.Result == (int)StatusCallApi.Success);
                }
                //var mess = $"Tima: Ma xac minh cua ban la {otp}. Ma co hieu luc trong vong {lifeTimeOtp} phut. Khong chia se ma nay voi nguoi khac.";
                //var result = _lmsService.SendOtpSmsBrandName((int)TypeOtp.VerifyPhone, phone, mess, otp.ToString(), DateTime.Now.AddMinutes(lifeTimeOtp), GetIpAddressRemote());
                if (isSuccess)
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = otp.ToString();
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, "Xảy ra lỗi khi gửi OTP. Vui lòng thử lại");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Security/GetOtp Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("reget_otp")]
        public async Task<ActionResult<DefaultResponse<Meta, string>>> ReGetOtp([FromQuery] string phone)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var def = new DefaultResponse<Meta, string>();
            try
            {
                var lifeTimeOtp = 15;
                if (string.IsNullOrEmpty(phone))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (!phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Số điện thoại không đúng định dạng");
                    return Ok(def);
                }

                //Kiểm tra xem sdt của KH có khoản vay không
                var loaninfo = await _unitOfWork.LoanBriefRepository.Query(x => x.Phone == phone
                    && x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.DISBURSED
                    && x.Status != (int)EnumLoanStatus.FINISH, null, false).Select(x => new { LoanbriefId = x.LoanBriefId }).FirstOrDefaultAsync();
                if (loaninfo == null)
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Số điện thoại không tồn tại đơn vay");
                    return Ok(def);
                }
                //Kiểm tra xem còn có otp có hiệu lực không
                var logSendOtp = await _unitOfWork.LogSendOtpRepository.Query(x => x.Phone == phone
                    && x.StatusSend == (int)StatusCallApi.Success
                    && x.VerifyStatus.GetValueOrDefault(0) == (int)VerifyOtp.WaitingVerify, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();
                if (logSendOtp != null && logSendOtp.Id > 0)
                {
                    //Nếu thời chờ verify còn quá nửa thời gian => trả luôn otp này
                    if ((logSendOtp.ExpiredVerifyOtp.Value - DateTime.Now).TotalMinutes > (lifeTimeOtp / 2))
                    {
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = logSendOtp.Otp;
                        return Ok(def);
                    }
                }

                if (environmentName == "Development")
                {
                    var log = new LogSendOtp
                    {
                        Phone = phone,
                        Otp = "123456",
                        Request = "",
                        CreatedAt = DateTime.Now,
                        IpAddress = "",
                        Url = "",
                        TypeSendOtp = (int)TypeSendOtp.SmsBrandName,
                        StatusSend = (int)StatusCallApi.Success,
                        ExpiredVerifyOtp = DateTime.Now.AddMinutes(lifeTimeOtp)
                    };
                    _unitOfWork.LogSendOtpRepository.Insert(log);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = "123456";
                    return Ok(def);
                }
                //Send otp mới
                Random random = new Random();
                var otp = random.Next(100001, 999999);

                var mess = $"Tima: Ma xac minh cua ban la {otp}. Ma co hieu luc trong vong {lifeTimeOtp} phut. Khong chia se ma nay voi nguoi khac.";
                var result = _lmsService.SendOtpSmsBrandName((int)TypeOtp.VerifyPhone, phone, mess, otp.ToString(), DateTime.Now.AddMinutes(lifeTimeOtp), GetIpAddressRemote());
                var isSuccess =  (result != null && result.Result == (int)StatusCallApi.Success);
                if (isSuccess)
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = otp.ToString();
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, "Xảy ra lỗi khi gửi OTP. Vui lòng thử lại");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Security/ReGetOtp Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("verify_otp")]
        public async Task<ActionResult<DefaultResponse<Meta, string>>> VerifyOtpApp(VerifyOtpModel model)
        {
            var def = new DefaultResponse<Meta, string>();
            try
            {
                if (!string.IsNullOrEmpty(model.Phone) && !string.IsNullOrEmpty(model.Otp))
                {
                    //Kiểm tra xem còn có otp có hiệu lực không
                    var logSendOtp = await _unitOfWork.LogSendOtpRepository.Query(x => x.Phone == model.Phone
                        && x.StatusSend == (int)StatusCallApi.Success
                        && x.VerifyStatus.GetValueOrDefault(0) == (int)VerifyOtp.WaitingVerify, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();

                    if (logSendOtp != null && logSendOtp.Id > 0)
                    {
                        if (logSendOtp.Otp == model.Otp)
                        {
                            if (logSendOtp.ExpiredVerifyOtp >= DateTime.Now)
                            {
                                var expiredToken = DateTime.Now.AddDays(1);
                                //tạo token cho user
                                var token = GenerateToken(model.Phone, model.Otp, expiredToken);
                                //chuyển trạng thái đã verify otp 
                                _unitOfWork.LogSendOtpRepository.Update(x => x.Id == logSendOtp.Id, x => new DAL.EntityFramework.LogSendOtp()
                                {
                                    VerifyStatus = (int)VerifyOtp.Verified,
                                    VerifyAt = DateTime.Now,
                                    ExpiredToken = expiredToken,
                                    TokenGennerate = token
                                });
                                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                def.data = token;
                            }
                            else
                                def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, "Quá thời hạn verify OTP");

                        }
                        else
                            def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, "Mã OTP không đúng");
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, "KH chưa có yêu cầu lấy mã OTP");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Security/VerifyOtpApp Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        #region function generate token and validate token
        private string GenerateToken(string phone, string otp, DateTime expired)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                     new Claim(ClaimTypes.NameIdentifier, phone),
                                                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                                    new Claim("phone", phone),
                                                    new Claim("otp", otp)
                }),
                Expires = expired,
                Issuer = _configuration["JWT:Issuer"],
                Audience = _configuration["JWT:Audience"],
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["JWT:SigningKey"])), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        #endregion
        #region get ip remote
        private string GetIpAddressRemote()
        {
            try
            {
                StringValues ipRequestHeader;
                var authIpRequest = HttpContext.Request.Headers.TryGetValue("X-Forwarded-For", out ipRequestHeader);
                if (authIpRequest)
                {
                    var remoteIp = GetIpRequest(ipRequestHeader);
                    if (!string.IsNullOrEmpty(remoteIp))
                    {
                        System.Net.IPAddress ipAddress = null;
                        bool isValidIp = System.Net.IPAddress.TryParse(remoteIp, out ipAddress);
                        if (isValidIp)
                            return ipAddress.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
            }
            return string.Empty;
        }

        private string GetIpRequest(string ipRequestHeader)
        {
            if (!string.IsNullOrEmpty(ipRequestHeader))
            {
                if (ipRequestHeader.Contains(","))
                    return ipRequestHeader.Split(',').First().Trim();
                else return ipRequestHeader.Trim();
            }
            return string.Empty;
        }
        #endregion

    }
}