﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.Common.Extensions;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class SupportController : BaseController
    {

        private IUnitOfWork _unitOfWork;
        private IConfiguration configuration;

        public SupportController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this.configuration = configuration;
        }

        //[HttpPost]
        //[Route("support_loan_unknown")]
        //public ActionResult<DefaultResponse<object>> SupportUnknownLoan(SupportLoanReq req)
        //{
        //    var def = new DefaultResponse<object>();
        //    try
        //    {
        //        var userId = GetUserId();
        //        if (req.LoanbriefId <= 0)
        //        {
        //            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
        //            return Ok(def);
        //        }
        //        var loanbrief = _unitOfWork.LoanBriefRepository.GetById(req.LoanbriefId);
        //        if (loanbrief != null && loanbrief.LoanBriefId > 0)
        //        {
        //            if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
        //            {
        //                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Đơn đang được luồng xử lý");
        //                return Ok(def);
        //            }
        //            if (loanbrief.Status == -99)
        //            {
        //                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new DAL.EntityFramework.LoanBrief()
        //                {
        //                    Status = 1,
        //                    CurrentPipelineId = null,
        //                    PipelineState = 0,
        //                    AddedToQueue = false,
        //                    BoundTelesaleId = req.TelesaleId 
        //                });
        //                _unitOfWork.Save();
        //                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
        //            }
        //            else
        //            {
        //                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Chỉ xử lý đơn ở trạng thái -99");
        //            }
                    
        //        }
        //        else
        //        {
        //            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
        //        }
        //        return Ok(def);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex, "Support/SupportUnknownLoan Exception");
        //        def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
        //    }
        //    return Ok(def);
        //}

    }
}
