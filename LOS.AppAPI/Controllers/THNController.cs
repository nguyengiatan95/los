﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models.THN;
using LOS.AppAPI.Service;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;
using static LOS.AppAPI.Models.THN.THN;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class THNController : BaseController
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration configuration;
        private ITrackDevice _trackDeviceService;
        public THNController(IUnitOfWork unitOfWork, IConfiguration configuration, ITrackDevice trackDeviceService)
        {
            this._unitOfWork = unitOfWork;
            this.configuration = configuration;
            this._trackDeviceService = trackDeviceService;
        }
        #region API lấy ra link facebook và API cập nhập
        [HttpGet]
        [AuthorizeToken("APP_API_THN")]
        [Route("get_link_facebook")]
        public async Task<ActionResult<DefaultResponse<Meta, object>>> GetLinkFacebook(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                //Lấy ra mã khách hàng dựa vào mã đơn vay
                var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).FirstOrDefaultAsync();
                if(loanbrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không tồn tại!");
                    return Ok(def);
                }
                var customer = await _unitOfWork.CustomerRepository.Query(x => x.CustomerId == loanbrief.CustomerId, null, false).FirstOrDefaultAsync();
                if (customer == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Khách hàng không tồn tại!");
                    return Ok(def);
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = customer.FacebookAddress;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "THN/GetStatusLoanAccessTrade Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("APP_API_THN")]
        [Route("update_link_facebook")]
        public async Task<ActionResult<DefaultResponse<Meta, object>>> UpdateLinkFacebook(THN.ResquestUpdateFacebook entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                //Check xem link facebook có phải là link không
                if (string.IsNullOrEmpty(entity.FacebookAddress))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Url không được để trống!");
                    return Ok(def);
                }
                if (!entity.FacebookAddress.IsValidUri())
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không đúng định dạng url!");
                    return Ok(def);
                }
                if (!entity.FacebookAddress.Contains("facebook"))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải url Facebook!");
                    return Ok(def);
                }
                //Lấy ra mã khách hàng dựa vào mã đơn vay
                var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false).FirstOrDefaultAsync();
                if (loanbrief == null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đơn vay không tồn tại!");
                    return Ok(def);
                }
                var customer = await _unitOfWork.CustomerRepository.Query(x => x.CustomerId == loanbrief.CustomerId, null, false).FirstOrDefaultAsync();
                //Lưu lại url facebook
                _unitOfWork.CustomerRepository.Update(x => x.CustomerId == loanbrief.CustomerId, x => new Customer()
                {
                    FacebookAddress = entity.FacebookAddress
                });

                var objLoanBriefNote = new LoanBriefNote()
                {
                    FullName = "THN",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = string.Format("Đổi link facebook " + customer.FacebookAddress + " thành " + entity.FacebookAddress),
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                    LoanBriefId = entity.LoanBriefId
                };
                _unitOfWork.LoanBriefNoteRepository.Insert(objLoanBriefNote);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "THN/UpdateLinkFacebook Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #endregion

        #region API tạo đơn vay đảo nợ
        [HttpPost]
        [AuthorizeToken("APP_API_THN")]
        [Route("init_debtRevolving_loan")]
        public async Task<ActionResult<DefaultResponse<object>>> InitDebtRevolvingLoan(InitDebtRevolvingLoanReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.LoanBriefId <= 0 || req.LoanAmount <= 0 || req.RateTypeId <= 0 || req.LoanTime <= 0 || string.IsNullOrEmpty(req.Username))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.LoanBriefId, null, false).Select(LoanBriefInitDetail.ProjectionViewDetail).FirstOrDefaultAsync();
                if (loanInfo == null || loanInfo.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }    
                //Kiểm tra đã tạo đơn đảo nợ nào chưa
                var check = await _unitOfWork.LoanBriefRepository.Query(x => x.ReMarketingLoanBriefId == req.LoanBriefId && x.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan
                && x.Status != (int)EnumLoanStatus.CANCELED, null, false).FirstOrDefaultAsync();
                if(check != null)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đã có đơn tái cấu trúc khoản vay trên HĐ này!");
                    return Ok(def);
                }    
                var files = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == req.LoanBriefId && x.Status == 1 && x.IsDeleted != 1, null, false).ToList();
                if(loanInfo.FeePaymentBeforeLoan.GetValueOrDefault(0)== 0)
                    loanInfo.FeePaymentBeforeLoan = Common.Utils.ProductPriceUtils.GetFeePaymentBeforeLoan(loanInfo.ProductId.Value, req.LoanTime);
                #region Loanbrief
                var loanbrief = new LoanBrief()
                {
                    LoanBriefId = 0,
                    CustomerId = loanInfo.CustomerId,
                    PlatformType = loanInfo.PlatformType.GetValueOrDefault(),
                    TypeLoanBrief = loanInfo.TypeLoanBrief.GetValueOrDefault(),
                    FullName = loanInfo.FullName,
                    Phone = loanInfo.Phone,
                    NationalCard = loanInfo.NationalCard,
                    NationCardPlace = loanInfo.NationCardPlace,
                    IsTrackingLocation = loanInfo.IsTrackingLocation,
                    Dob = loanInfo.Dob,
                    Passport = loanInfo.Passport,
                    Gender = loanInfo.Gender,
                    NumberCall = loanInfo.NumberCall,
                    ProvinceId = loanInfo.ProvinceId,
                    DistrictId = loanInfo.DistrictId,
                    WardId = loanInfo.WardId,
                    ProductId = loanInfo.ProductId.GetValueOrDefault(),
                    RateTypeId = req.RateTypeId,
                    LoanAmount = req.LoanAmount,
                    BuyInsurenceCustomer = false,
                    LoanTime = req.LoanTime,
                    Frequency = loanInfo.Frequency,
                    RateMoney = loanInfo.RateMoney,
                    RatePercent = loanInfo.RatePercent,
                    ReceivingMoneyType = (int)EnumReceivingMoneyType.TienMat,
                    BankId = loanInfo.BankId,
                    BankAccountNumber = loanInfo.BankAccountNumber,
                    BankCardNumber = loanInfo.BankCardNumber,
                    BankAccountName = loanInfo.BankAccountName,
                    IsLocate = loanInfo.IsLocate,
                    DeviceId = loanInfo.DeviceId,
                    TypeRemarketing = (int)EnumTypeRemarketing.DebtRevolvingLoan,
                    FromDate = DateTime.Now,
                    ToDate = DateTime.Now.AddMonths((int)req.LoanTime),
                    CreatedTime = DateTime.Now,
                    IsHeadOffice = loanInfo.IsHeadOffice,
                    //Status = EnumLoanStatus.INIT.GetHashCode(),
                    Status = -1,
                    PipelineState = -1,

                    PresenterCode = loanInfo.PresenterCode,
                    LoanAmountFirst = req.LoanAmount,
                    LoanAmountExpertise = loanInfo.LoanAmountExpertise,
                    LoanAmountExpertiseLast = loanInfo.LoanAmountExpertiseLast,
                    LoanAmountExpertiseAi = loanInfo.LoanAmountExpertiseAi,
                    PhoneOther = loanInfo.PhoneOther,
                    BankBranch = loanInfo.BankBranch,

                    ProductDetailId = loanInfo.ProductDetailId,
                    IsSim = loanInfo.IsSim,
                    IsReborrow = loanInfo.IsReborrow,
                    ReMarketingLoanBriefId = req.LoanBriefId,
                    LoanPurpose = loanInfo.LoanPurpose,
                    FeePaymentBeforeLoan = loanInfo.FeePaymentBeforeLoan??0.08M
                };
                _unitOfWork.LoanBriefRepository.Insert(loanbrief);
                _unitOfWork.Save();
                #endregion

                #region LoanBriefResident
                _unitOfWork.LoanBriefResidentRepository.Insert(new LoanBriefResident()
                {
                    LoanBriefResidentId = loanbrief.LoanBriefId,
                    LivingTime = loanInfo.LoanBriefResident != null ? loanInfo.LoanBriefResident.LivingTime : 0,
                    ResidentType = loanInfo.LoanBriefResident != null ? loanInfo.LoanBriefResident.ResidentType : 0,
                    LivingWith = loanInfo.LoanBriefResident != null ? loanInfo.LoanBriefResident.LivingWith : 0,
                    Address = loanInfo.LoanBriefResident != null ? loanInfo.LoanBriefResident.Address : "",
                    AddressGoogleMap = loanInfo.LoanBriefResident != null ? loanInfo.LoanBriefResident.AddressGoogleMap : "",
                    AddressLatLng = loanInfo.LoanBriefResident != null ? loanInfo.LoanBriefResident.AddressLatLng : "",
                    ProvinceId = loanInfo.ProvinceId,
                    DistrictId = loanInfo.DistrictId,
                    WardId = loanInfo.WardId,
                    BillElectricityId = loanInfo.LoanBriefResident != null ? loanInfo.LoanBriefResident.BillElectricityId : "",
                    BillWaterId = loanInfo.LoanBriefResident != null ? loanInfo.LoanBriefResident.BillWaterId:"",
                    AddressNationalCard = loanInfo.LoanBriefResident != null ? loanInfo.LoanBriefResident.AddressNationalCard : "",
                    CustomerShareLocation = loanInfo.LoanBriefResident != null ? loanInfo.LoanBriefResident.CustomerShareLocation : "",
                });
                #endregion

                #region LoanBriefProperty
                _unitOfWork.LoanBriefPropertyRepository.Insert(new LoanBriefProperty()
                {
                    LoanBriefPropertyId = loanbrief.LoanBriefId,
                    BrandId = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.BrandId : 0,
                    ProductId = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.ProductId : 0,
                    PlateNumber = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.PlateNumber : "",
                    CreatedTime = DateTime.Now,
                    CarManufacturer = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.CarManufacturer : "",
                    CarName = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.CarName : "",
                    CarColor = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.CarColor : "",
                    PlateNumberCar = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.PlateNumberCar : "",
                    Chassis = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.Chassis : "",
                    Engine = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.Engine : "",
                    BuyInsuranceProperty = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.BuyInsuranceProperty : false,
                    OwnerFullName = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.OwnerFullName : "",
                    MotobikeCertificateNumber = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.MotobikeCertificateNumber : "",
                    MotobikeCertificateDate = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.MotobikeCertificateDate : "",
                    MotobikeCertificateAddress = loanInfo.LoanBriefProperty != null ? loanInfo.LoanBriefProperty.MotobikeCertificateAddress : "",
                });
                #endregion

                #region LoanBriefHousehold
                _unitOfWork.LoanBriefHouseholdRepository.Insert(new LoanBriefHousehold()
                {
                    LoanBriefHouseholdId = loanbrief.LoanBriefId,
                    Address = loanInfo.LoanBriefHousehold != null ? loanInfo.LoanBriefHousehold.Address : "",
                    ProvinceId = loanInfo.LoanBriefHousehold != null ? loanInfo.LoanBriefHousehold.ProvinceId : 0,
                    DistrictId = loanInfo.LoanBriefHousehold != null ? loanInfo.LoanBriefHousehold.DistrictId : 0,
                    WardId = loanInfo.LoanBriefHousehold != null ? loanInfo.LoanBriefHousehold.WardId : 0,
                    FullNameHouseOwner = loanInfo.LoanBriefHousehold != null ? loanInfo.LoanBriefHousehold.FullNameHouseOwner : null,
                    RelationshipHouseOwner = loanInfo.LoanBriefHousehold != null ? loanInfo.LoanBriefHousehold.RelationshipHouseOwner : null,
                    BirdayHouseOwner = loanInfo.LoanBriefHousehold != null ? loanInfo.LoanBriefHousehold.BirdayHouseOwner : null,
                });
                #endregion

                #region LoanBriefJob
                _unitOfWork.LoanBriefJobRepository.Insert(new LoanBriefJob()
                {
                    LoanBriefJobId = loanbrief.LoanBriefId,
                    JobId = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.JobId : 0,
                    CompanyName = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.CompanyName : "",
                    CompanyPhone = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.CompanyPhone : "",
                    CompanyTaxCode = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.CompanyTaxCode : "",
                    CompanyProvinceId = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.CompanyProvinceId : 0,
                    CompanyDistrictId = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.CompanyDistrictId : 0,
                    CompanyWardId = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.CompanyWardId : 0,
                    CompanyAddress = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.CompanyAddress : "",
                    TotalIncome = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.TotalIncome : 0,
                    ImcomeType = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.ImcomeType : 0,
                    Description = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.Description : "",
                    CompanyAddressGoogleMap = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.CompanyAddressGoogleMap : "",
                    CompanyAddressLatLng = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.CompanyAddressLatLng : "",
                    CompanyInsurance = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.CompanyInsurance : null,
                    WorkLocation = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.WorkLocation : null,
                    BusinessPapers = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.BusinessPapers : null,
                    JobDescriptionId = loanInfo.LoanBriefJob != null ? loanInfo.LoanBriefJob.JobDescriptionId : null,
                });
                #endregion

                #region LoanBriefCompany
                if (loanInfo.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    _unitOfWork.LoanBriefCompanyRepository.Insert(new LoanBriefCompany()
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        CompanyName = loanInfo.LoanBriefCompany != null ? loanInfo.LoanBriefCompany.CompanyName : "",
                        CareerBusiness = loanInfo.LoanBriefCompany != null ? loanInfo.LoanBriefCompany.CareerBusiness : "",
                        BusinessCertificationAddress = loanInfo.LoanBriefCompany != null ? loanInfo.LoanBriefCompany.BusinessCertificationAddress : "",
                        HeadOffice = loanInfo.LoanBriefCompany != null ? loanInfo.LoanBriefCompany.HeadOffice : "",
                        Address = loanInfo.LoanBriefCompany != null ? loanInfo.LoanBriefCompany.Address : "",
                        CompanyShareholder = loanInfo.LoanBriefCompany != null ? loanInfo.LoanBriefCompany.CompanyShareholder : "",
                        CardNumberShareholderDate = loanInfo.LoanBriefCompany != null && loanInfo.LoanBriefCompany.CardNumberShareholderDate.HasValue
                        ? loanInfo.LoanBriefCompany.CardNumberShareholderDate : null,
                        BusinessCertificationDate = loanInfo.LoanBriefCompany != null && loanInfo.LoanBriefCompany.BusinessCertificationDate.HasValue
                        ? loanInfo.LoanBriefCompany.BusinessCertificationDate : null,
                        BirthdayShareholder = loanInfo.LoanBriefCompany != null && loanInfo.LoanBriefCompany.BirthdayShareholder.HasValue
                        ? loanInfo.LoanBriefCompany.BirthdayShareholder : null,
                        CardNumberShareholder = loanInfo.LoanBriefCompany != null ? loanInfo.LoanBriefCompany.CardNumberShareholder : "",
                        PlaceOfBirthShareholder = loanInfo.LoanBriefCompany != null ? loanInfo.LoanBriefCompany.PlaceOfBirthShareholder : "",
                        CreatedDate = DateTime.Now
                    });
                }
                #endregion

                #region LoanBriefRelationship
                if (loanInfo.LoanBriefRelationship != null && loanInfo.LoanBriefRelationship.Count > 0)
                {
                    foreach (var relationship in loanInfo.LoanBriefRelationship)
                    {
                        if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                        {
                            _unitOfWork.LoanBriefRelationshipRepository.Insert(new LoanBriefRelationship()
                            {
                                LoanBriefId = loanbrief.LoanBriefId,
                                RelationshipType = relationship.RelationshipType,
                                FullName = relationship.FullName,
                                Phone = relationship.Phone,
                                CreatedDate = DateTime.Now,
                                Address = relationship.Address
                            });
                        }
                    }
                }
                #endregion

                #region LoanBriefFiles
                if (files != null && files.Count > 0)
                {
                    foreach (var item in files)
                    {
                        _unitOfWork.LoanBriefFileRepository.Insert(new LoanBriefFiles()
                        {
                            LoanBriefId = loanbrief.LoanBriefId,
                            FilePath = item.FilePath,
                            FileThumb = item.FileThumb,
                            TypeId = item.TypeId,
                            UserId = item.UserId,
                            CreateAt = item.CreateAt,
                            MecashId = item.MecashId,
                            S3status = item.S3status,
                            SourceUpload = item.SourceUpload,
                            Status = item.Status,
                            SubTypeId = item.SubTypeId,
                            TypeFile = item.TypeFile
                        });
                    }
                }
                #endregion

                //Thêm log cấu trúc lại khoản vay
                _unitOfWork.LogActionRestructRepository.Insert(new LogActionRestruct
                {
                    LoanbriefId = loanbrief.LoanBriefId,
                    LoanbriefParentId = req.LoanBriefId,
                    TypeAction = ActionLogRestruct.CauTrucLaiKhoanVay.GetHashCode(),
                    LoanAmount = req.LoanAmount,
                    CreatedAt = DateTime.Now,
                    LoanTime = req.LoanTime,
                    Status = 1
                });
                _unitOfWork.Save();

                //Call ChangePipeline
                ChangePipeline(loanbrief.LoanBriefId,loanbrief.ProductId.Value, (int)EnumLoanStatus.HUB_LOAN_DISTRIBUTING);
                //Add note
                await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote()
                {
                    FullName = req.Username,
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = string.Format("<b>{0}</b> tạo đơn cấu trúc lại khoản vay từ App THN của HĐ-{1} với số tiền {2}đ", req.Username, req.LoanBriefId, req.LoanAmount.ToString("#,##")),
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = EnumShop.Tima.GetHashCode(),
                    ShopName = "Tima",
                    LoanBriefId = loanbrief.LoanBriefId
                });
                if (!string.IsNullOrEmpty(req.Comment))
                {
                    await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote()
                    {
                        FullName = req.Username,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        Note = req.Comment,
                        Status = 1,
                        CreatedTime = DateTime.Now,
                        ShopId = EnumShop.Tima.GetHashCode(),
                        ShopName = "Tima",
                        LoanBriefId = loanbrief.LoanBriefId
                    });
                }
                _unitOfWork.Save();

                //Kiểm tra xem có phải đơn định vị không
                if(loanbrief.IsLocate.GetValueOrDefault(false) && !string.IsNullOrEmpty(loanbrief.DeviceId))
                {
                    var resultOpent = _trackDeviceService.OpenContract("HD-" + loanbrief.LoanBriefId, loanbrief.DeviceId, loanInfo.HubId.ToString(), loanbrief.ProductId.Value, loanbrief.LoanBriefId, "topup");
                    //StatusCode == 3 => mã hợp đồng đã tồn tại
                    if (resultOpent != null && (resultOpent.StatusCode == 200 || resultOpent.StatusCode == 3))
                    {
                        if (resultOpent.StatusCode == 200)
                        {
                            await _unitOfWork.LoanBriefNoteRepository.InsertAsync(new LoanBriefNote()
                            {
                                FullName = req.Username,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                Note = string.Format("Đơn định vị: Tạo hợp đồng thành công với imei: {0}", loanbrief.DeviceId),
                                Status = 1,
                                CreatedTime = DateTime.Now,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = "Tima",
                                LoanBriefId = loanbrief.LoanBriefId
                            });

                            _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                            {
                                DeviceStatus = StatusOfDeviceID.OpenContract.GetHashCode(),
                                ContractGinno = "HD-" + loanbrief.LoanBriefId
                            });
                            _unitOfWork.Save();
                        }
                    }
                }

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = loanbrief.LoanBriefId;
                //_unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanbriefV3/InitDebtRevolvingLoan POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("change_pipeline")]
        private bool ChangePipeline(int loanBriefId, int productId, int status)
        {
            var re = false;
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.GetById(loanBriefId);
                var result = SimulatePipeline(loanBrief, productId, status);
                if (result == 1)
                    re = true;              
                else
                    re = false;
                return re;
            }
            catch (Exception ex)
            {
                return re;
            }
        }

        int SimulatePipeline(LoanBrief loanBrief, int newProductId, int newStatus)
        {
            try
            {
                var pipelines = getPipeline();
                bool IsOk = false;
                if (pipelines != null && pipelines.Count > 0)
                {
                    DatabaseHelper<LoanBrief> helper = new DatabaseHelper<LoanBrief>();
                    // gán lại productId
                    loanBrief.ProductId = newProductId;
                    //if (loanBrief.CurrentPipelineId != null)
                    //{
                    PipelineDTO newPipeline = null;
                    foreach (var pipeline in pipelines)
                    {
                        var sections = pipeline.Sections;
                        if (sections != null && sections.Count() > 0)
                        {
                            SectionDetailDTO lastAutomaticSuccess = null;
                            int indexApproveSucess = -1;
                            foreach (var section in sections)
                            {
                                // for each detail	
                                int detailSucess = 0;
                                int detailCount = section.Details != null ? section.Details.Count() : 0;
                                if (detailCount == 0)
                                {
                                    goto gotonextPipeline;
                                }
                                foreach (var detail in section.Details)
                                {
                                    if (detail.Mode == "AUTOMATIC")
                                    {
                                        if (detail.Approves != null && detail.Approves.Count() > 0)
                                        {
                                            bool IsOneSuccess = false;
                                            int index = 0;
                                            foreach (var approve in detail.Approves)
                                            {
                                                IsOneSuccess = false;
                                                int conditionSuccess = 0;
                                                int conditionCount = approve.Conditions != null ? approve.Conditions.Count() : 0;
                                                if (conditionCount <= 0)
                                                {
                                                    goto gotonextPipeline;
                                                }
                                                foreach (var condition in approve.Conditions)
                                                {
                                                    if (helper.CheckFieldOperator(loanBrief, condition.Property.FieldName, condition.Operator, condition.Value, condition.Property.Type))
                                                    {
                                                        indexApproveSucess = index;
                                                        Console.WriteLine("Condition successed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                        conditionSuccess++;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("Condition failed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                    }
                                                }
                                                // nếu điều kiện thành công hết thì chuyển đến aprrove section
                                                if (conditionSuccess == conditionCount)
                                                {
                                                    // detail success
                                                    detailSucess++;
                                                    lastAutomaticSuccess = detail;
                                                    IsOneSuccess = true;
                                                    break;
                                                }
                                                index++;
                                            }
                                            if (!IsOneSuccess)
                                            {
                                                goto gotonextPipeline;
                                            }
                                            else
                                            {
                                                IsOk = true;
                                                newPipeline = pipeline;
                                                goto successed;
                                            }
                                        }
                                        else
                                        {
                                            return 0;
                                        }
                                    }
                                    else
                                    {
                                        // kiêm tra xem có đúng section sau khi kiểm tra không
                                        if (lastAutomaticSuccess != null)
                                        {
                                            if (indexApproveSucess >= 0)
                                            {
                                                if (lastAutomaticSuccess.Approves.ToList()[indexApproveSucess].ApproveId == detail.SectionDetailId)
                                                {
                                                    IsOk = true;
                                                    newPipeline = pipeline;
                                                    goto successed;
                                                }
                                                else
                                                {
                                                    goto gotonextPipeline;
                                                }
                                            }
                                            else
                                            {
                                                goto gotonextPipeline;
                                            }
                                        }
                                        else
                                        {
                                            goto gotonextPipeline;
                                        }
                                    }
                                }
                            }
                        }
                    gotonextPipeline:
                        {
                            Console.WriteLine("Go to next pipeline, Current pipeline : " + pipeline.Name);
                        }
                    }
                successed:
                    {
                        if (IsOk)
                        {
                            if (newPipeline != null)
                            {
                                // tìm xem action có status tương ứng
                                var sectionAction = _unitOfWork.SectionActionRepository.Query(x => x.PropertyId == 4 && x.Value == newStatus.ToString() && x.SectionDetail.Status != 99 && x.SectionDetail.Section.PipelineId == newPipeline.PipelineId, null, false).FirstOrDefault();
                                if (sectionAction != null)
                                {
                                    var sectionDetail = _unitOfWork.SectionDetailRepository.GetById(sectionAction.SectionDetailId);
                                    // gán lại pipeline đơn vay
                                    if (sectionDetail != null)
                                    {
                                        //save log
                                        var currentHistory = new PipelineHistory();
                                        currentHistory.ActionState = null;
                                        currentHistory.BeginSectionDetailId = loanBrief.CurrentSectionDetailId;
                                        currentHistory.BeginSectionId = loanBrief.CurrentSectionId;
                                        currentHistory.BeginStatus = loanBrief.Status;
                                        currentHistory.BeginTime = DateTimeOffset.Now;
                                        currentHistory.CreatedDate = DateTimeOffset.Now;
                                        //currentHistory.Data = JsonConvert.SerializeObject(loanBrief);
                                        currentHistory.LoanBriefId = loanBrief.LoanBriefId;
                                        currentHistory.PipelineId = newPipeline.PipelineId;
                                        currentHistory.EndSectionDetailId = sectionDetail.SectionDetailId;
                                        currentHistory.EndSectionId = sectionDetail.SectionId;
                                        if (sectionDetail.Mode.Equals("MANUAL"))
                                            currentHistory.EndPipelineState = 20;
                                        else
                                            currentHistory.EndPipelineState = 30;
                                        currentHistory.EndStatus = newStatus;
                                        currentHistory.ExecuteTime = 0;
                                        _unitOfWork.PipelineHistoryRepository.SingleInsert(currentHistory);
                                        _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBrief.LoanBriefId, x => new LoanBrief()
                                        {
                                            ProductId = newProductId,
                                            CurrentPipelineId = newPipeline.PipelineId,
                                            CurrentSectionId = sectionDetail.SectionId,
                                            CurrentSectionDetailId = sectionDetail.SectionDetailId,
                                            Status = newStatus,
                                            PipelineState = sectionDetail.Mode.Equals("MANUAL") ? 20 : 30,
                                            AddedToQueue = false,
                                            InProcess = 0,
                                            ActionState = null,
                                        });
                                        return 1;
                                    }
                                }
                                else
                                {
                                    return -1;
                                }
                                Console.WriteLine("Pipeline Successed:" + loanBrief.LoanBriefId);
                            }
                        }
                        else
                        {
                            return -2;
                        }
                    }
                    //}
                    //else
                    //{
                    //	return 0;
                    //}
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Pipeline Exception:" + ex.Message);
                Console.WriteLine("Pipeline Exception:" + ex.StackTrace);
            }
            return 0;
        }
        private List<PipelineDTO> getPipeline()
        {
            try
            {
                return _unitOfWork.PipelineRepository.Query(x => x.Status != 99 && x.Status != 0, x => x.OrderBy(x1 => x1.Priority), false)
                    .Select(x => new PipelineDTO()
                    {
                        CreatedAt = x.CreatedAt,
                        CreatorId = x.CreatorId,
                        ModifiedAt = x.ModifiedAt,
                        Name = x.Name,
                        PipelineId = x.PipelineId,
                        Priority = x.Priority,
                        Status = x.Status,
                        Sections = x.Section.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Order).Select(x1 => new SectionDTO()
                        {
                            Name = x1.Name,
                            Order = x1.Order,
                            SectionGroupId = x1.SectionGroupId,
                            SectionId = x1.SectionId,
                            IsShow = x1.IsShow,
                            PipelineId = x1.PipelineId,
                            Details = x1.SectionDetail.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Step).Select(x2 => new SectionDetailDTO()
                            {
                                Mode = x2.Mode,
                                Name = x2.Name,
                                SectionDetailId = x2.SectionDetailId,
                                SectionId = x2.SectionId,
                                Step = x2.Step,
                                IsShow = x2.IsShow,
                                Approves = x2.SectionApproveSectionDetail.Select(x3 => new SectionApproveDTO()
                                {
                                    Name = x3.Name,
                                    Approve = x3.Approve != null ? new SectionDetailDTO()
                                    {
                                        Name = x3.Approve.Name,
                                        SectionDetailId = x3.Approve.SectionDetailId,
                                        SectionId = x3.Approve.SectionId,
                                        Step = x3.Approve.Step
                                    } : null,
                                    SectionApproveId = x3.SectionApproveId,
                                    SectionDetailId = x3.SectionDetailId,
                                    ApproveId = x3.ApproveId,
                                    Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
                                    {
                                        Operator = x4.Operator,
                                        PropertyId = x4.PropertyId,
                                        SectionConditionId = x4.SectionConditionId,
                                        SectionApproveId = x4.SectionApproveId,
                                        Value = x4.Value,
                                        Property = x4.Property != null ? new PropertyDTO()
                                        {
                                            Object = x4.Property.Object,
                                            FieldName = x4.Property.FieldName,
                                            Priority = x4.Property.Priority,
                                            Regex = x4.Property.Regex,
                                            Type = x4.Property.Type,
                                            PropertyId = x4.Property.PropertyId,
                                            Name = x4.Property.Name,
                                            LinkObject = x4.Property.LinkObject
                                        } : null
                                    })
                                }),
                                DisapproveId = x2.DisapproveId,
                                Disapprove = x2.Disapprove != null ? new SectionDetailDTO()
                                {
                                    Name = x2.Disapprove.Name,
                                    SectionDetailId = x2.Disapprove.SectionDetailId,
                                    SectionId = x2.Disapprove.SectionId,
                                    Step = x2.Disapprove.Step
                                } : null,
                                ReturnId = x2.ReturnId,
                                Return = x2.Return != null ? new SectionDetailDTO()
                                {
                                    Name = x2.Return.Name,
                                    SectionDetailId = x2.Return.SectionDetailId,
                                    SectionId = x2.Return.SectionId,
                                    Step = x2.Return.Step
                                } : null,
                                Actions = x2.SectionAction.Select(x3 => new SectionActionDTO()
                                {
                                    PropertyId = x3.PropertyId,
                                    SectionActionId = x3.SectionActionId,
                                    SectionDetailId = x3.SectionDetailId,
                                    Value = x3.Value,
                                    Type = x3.Type,
                                    Property = x3.Property != null ? new PropertyDTO()
                                    {
                                        Object = x3.Property.Object,
                                        FieldName = x3.Property.FieldName,
                                        Priority = x3.Property.Priority,
                                        Regex = x3.Property.Regex,
                                        Type = x3.Property.Type,
                                        PropertyId = x3.Property.PropertyId,
                                        Name = x3.Property.Name,
                                        LinkObject = x3.Property.LinkObject
                                    } : null
                                })
                            })
                        })
                    }).ToList();
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        #endregion
    }
}