﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.WebSockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using LOS.AppAPI.DTOs.App;
using LOS.AppAPI.DTOs.Appraiser;
using LOS.AppAPI.DTOs.LMS;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models.AI;
using LOS.AppAPI.Models.App;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.ESign;
using LOS.AppAPI.Service.ExportPDF;
using LOS.AppAPI.Service.LmsService;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.Common.Utils;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Memory;
using SixLabors.ImageSharp.Processing;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    //[Authorize]
    public class TimaCareController : BaseController
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _baseConfig;
        protected IServiceAI _aiService;
        private IExportPdfService _exportPdfService;
        private readonly IIdentityService _identityService;
        private IESignService _esignService;
        private ILmsService _lmsService;

        public TimaCareController(IUnitOfWork unitOfWork, IConfiguration configuration, IServiceAI serviceAI, IExportPdfService exportPdfService,
            IIdentityService identityService, IESignService esignService, ILmsService lmsService)
        {
            this._unitOfWork = unitOfWork;
            this._baseConfig = configuration;
            this._aiService = serviceAI;
            this._exportPdfService = exportPdfService;
            this._identityService = identityService;
            this._esignService = esignService;
            this._lmsService = lmsService;
        }


        [Route("UploadImage")]
        [HttpPost]
        public async Task<IActionResult> UploadImage(List<IFormFile> files, int LoanBriefId = 0, int TypeId = 0, string LatLong = "", DateTime? CreatedTime = null)
        {
            //var userId = GetUserId();
            //var user = _unitOfWork.UserRepository.GetById(userId);
            var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
            var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
            var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
            var BucketName = _baseConfig["AppSettings:BucketName"];
            var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
            {
                RegionEndpoint = RegionEndpoint.APSoutheast1
            });

            var folder = BucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
            var lst = new List<LoanBriefFiles>();
            var def = new DefaultResponse<Meta, List<LoanBriefFiles>>();
            if (files == null || LoanBriefId <= 0 || TypeId <= 0 || string.IsNullOrEmpty(LatLong))
            {
                def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                return Ok(def);
            }
            var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId, null, false).Select(LoanBriefInfo.ProjectionLoanBriefInfo).FirstOrDefault();
            if (loanInfo != null)
            {
                try
                {
                    foreach (var file in files)
                    {
                        var link = "";
                        var linkThumb = "";
                        var extension = Path.GetExtension(file.FileName);
                        var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                        if (allowedExtensions.Contains(extension.ToLower()))
                        {
                            string ImageName = "";
                            string ImageThumb = Guid.NewGuid().ToString() + "-thumb" + extension;
                            if (!string.IsNullOrEmpty(LatLong))
                            {
                                ImageName = string.Format("{0}-{1}{2}", LatLong.Replace(",", "-"), Guid.NewGuid().ToString(), extension);
                            }
                            else
                            {
                                ImageName = Guid.NewGuid().ToString() + extension;
                            }
                            using (var stream = new MemoryStream())
                            {
                                file.CopyTo(stream);
                                PutObjectRequest req = new PutObjectRequest()
                                {
                                    InputStream = stream,
                                    BucketName = folder,
                                    Key = ImageName,
                                    CannedACL = S3CannedACL.PublicRead
                                };
                                await client.PutObjectAsync(req);
                                //Crop image
                                using var image = Image.Load(file.OpenReadStream());
                                if (image.Width > 500)
                                {
                                    image.Mutate(x => x.Resize(200, image.Height / (image.Width / 200)));

                                    var outputStream = new MemoryStream();
                                    image.Save(outputStream, new JpegEncoder());
                                    outputStream.Seek(0, 0);
                                    //Upload Thumb
                                    PutObjectRequest reqThumb = new PutObjectRequest()
                                    {
                                        InputStream = outputStream,
                                        BucketName = folder,
                                        Key = ImageThumb,
                                        CannedACL = S3CannedACL.PublicRead
                                    };
                                    await client.PutObjectAsync(reqThumb);
                                    linkThumb = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + ImageThumb;
                                    //image.Save(file.FileName);
                                    //return Ok();
                                }
                                //End
                                link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                                //insert to db
                                var obj = new LoanBriefFiles
                                {
                                    LoanBriefId = LoanBriefId,
                                    CreateAt = CreatedTime == null ? DateTime.Now : CreatedTime,
                                    FilePath = link,
                                    UserId = 0,
                                    Status = 1,
                                    TypeId = TypeId,
                                    S3status = 1,
                                    MecashId = 1000000000,
                                    SourceUpload = (int)EnumSourceUpload.TimaCare,
                                    LatLong = LatLong,
                                    FileThumb = linkThumb
                                };
                                _unitOfWork.LoanBriefFileRepository.Insert(obj);
                                _unitOfWork.Save();
                                //obj.FilePath = ServiceURL + obj.FilePath;
                                lst.Add(obj);
                            }
                        }
                        else
                        {
                            def.meta = new Meta(211, "Ảnh không đúng định dạng!");
                            return Ok(def);
                        }

                    }
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = lst.ToList();
                    if (def.data != null && def.data.Count > 0)
                    {
                        foreach (var item in def.data)
                        {
                            item.FilePath = ServiceURL + item.FilePath;
                            item.FileThumb = ServiceURL + item.FileThumb;
                        }

                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "TimaCare/UploadImage Exception");
                    def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                }

            }
            else
            {
                def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                return Ok(def);
            }

            return Ok(def);
        }

        [Route("ListDocumentByType")]
        [HttpGet]
        public ActionResult<DefaultResponse<DocumentTypeDetail>> ListDocumentByType(int TypeId)
        {
            var def = new DefaultResponse<List<DocumentTypeDetail>>();
            if (TypeId <= 0)
            {
                def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                return Ok(def);
            }
            try
            {
                var docId = 0;
                //Thẩm định nhà TypeId = 1
                if (TypeId == 1) docId = 431;
                //Thẩm định nơi làm việc TypeId = 2
                if (TypeId == 2) docId = 435;

                var documents = _unitOfWork.DocumentTypeRepository.Query(x => x.ParentId == docId && x.IsEnable == 1, null, false).Select(DocumentTypeDetail.ProjectionDetail).ToList();
                if (documents != null)
                {
                    if (TypeId == 1)
                        documents.Add(new DocumentTypeDetail() { Id = 446, Name = "Ảnh bên trong nơi ở cho CVKD" });
                    if (TypeId == 2)
                        documents.Add(new DocumentTypeDetail() { Id = 449, Name = "Ảnh bên trong nơi làm việc cho CVKD" });
                }

                ////var documents = _unitOfWork.DocumentTypeRepository.Query(x => x.ParentId == docId && x.IsEnable == 1, null, false).Select(DocumentTypeDetail.ProjectionDetail).ToList();
                //var documents = new List<DocumentTypeDetail>();
                //if (TypeId == 1)
                //{
                //    documents.Add(new DocumentTypeDetail() { Id = 446, Name = "Ảnh bên trong nơi ở (CVKD)" });
                //    documents.Add(new DocumentTypeDetail() { Id = 447, Name = "Ảnh check out nơi ở CVKD" });
                //}    

                //if (TypeId == 2)
                //{
                //    documents.Add(new DocumentTypeDetail() { Id = 449, Name = "Ảnh bên trong nơi làm việc (CVKD)" });
                //    documents.Add(new DocumentTypeDetail() { Id = 450, Name = "Ảnh check out nơi làm việc CVKD" });
                //}    

                def.meta = new Meta(200, "success");
                def.data = documents;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "TimaCare/ListDocumentByType Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }

            return Ok(def);
        }


        [HttpGet]
        [Route("get_loan_by_phone")]
        [AuthorizeTokenOtp]
        public ActionResult<DefaultResponse<Meta, List<DAL.DTOs.LoanBriefTimaCareDTO>>> GetLoanByPhone(string phone)
        {
            var def = new DefaultResponse<List<DAL.DTOs.LoanBriefTimaCareDTO>>();
            try
            {
                if (string.IsNullOrEmpty(phone))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (!phone.ValidPhone())
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Số điện thoại không đúng định dạng");
                    return Ok(def);
                }

                var phoneToken = _identityService.GetPhoneIdentity();
                if (string.IsNullOrEmpty(phoneToken))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                    return Ok(def);
                }
                if (phoneToken != phone)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                    return Ok(def);
                }

                var data = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == phone
                && (x.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                    || x.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                    || x.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                && x.Status != (int)EnumLoanStatus.CANCELED
                && x.Status != (int)EnumLoanStatus.DISBURSED
                && x.Status != (int)EnumLoanStatus.FINISH
                , null, false).Select(DAL.DTOs.LoanBriefTimaCareDTO.ProjectionDetail).ToList();
                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        item.Phone = item.Phone.Remove(4, 4).Insert(4, "****");
                    }
                }

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "TimaCare/GetLoanByPhone Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("CheckLiveness")]
        [AuthorizeTokenOtp]
        public async Task<IActionResult> CheckLiveness([FromForm] CheckLivenessReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.selfie != null && req.files.Count >= 1 && req.loanBriefId > 0)
                {
                    //kiểm tra xem có đũng mã loanbrief không
                    var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.loanBriefId, null, false).FirstOrDefaultAsync();
                    if (loanbrief == null)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                        return Ok(def);
                    }

                    var phoneToken = _identityService.GetPhoneIdentity();
                    if (string.IsNullOrEmpty(phoneToken))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                        return Ok(def);
                    }

                    if (loanbrief.Phone != phoneToken)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                        return Ok(def);
                    }

                    // selfie picture > 0                    
                    var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                    var extension = Path.GetExtension(req.selfie.FileName);
                    var selfieImage = "";
                    if (allowedExtensions.Contains(extension.ToLower()) && req.selfie.Length > 0)
                    {
                        //resize if needed                                         
                        using (var image = Image.Load(req.selfie.OpenReadStream()))
                        {
                            if (image.Width > 1280)
                            {
                                // resize
                                image.Mutate(x => x.Resize(1280, image.Height / (image.Width / 1280)));
                                selfieImage = image.ToBase64String(JpegFormat.Instance);
                            }
                            else if (image.Height > 720)
                            {
                                // resize
                                image.Mutate(x => x.Resize(image.Width / (image.Height / 720), 720));
                                selfieImage = image.ToBase64String(JpegFormat.Instance);
                            }
                            else
                            {
                                selfieImage = image.ToBase64String(JpegFormat.Instance);
                            }
                        }
                    }
                    else
                    {
                        def.meta = new Meta(211, "Ảnh không đúng định dạng!");
                        return Ok(def);
                    }
                    // action images
                    List<string> actionImages = new List<string>();
                    foreach (var file in req.files)
                    {
                        extension = Path.GetExtension(file.FileName);
                        if (allowedExtensions.Contains(extension.ToLower()) && file.Length > 0)
                        {
                            //resize if needed                                         
                            using (var image = Image.Load(file.OpenReadStream()))
                            {
                                if (image.Width > 1280)
                                {
                                    // resize
                                    image.Mutate(x => x.Resize(1280, image.Height / (image.Width / 1280)));
                                    actionImages.Add(image.ToBase64String(JpegFormat.Instance));
                                }
                                else if (image.Height > 720)
                                {
                                    // resize
                                    image.Mutate(x => x.Resize(image.Width / (image.Height / 720), 720));
                                    actionImages.Add(image.ToBase64String(JpegFormat.Instance));
                                }
                                else
                                {
                                    actionImages.Add(image.ToBase64String(JpegFormat.Instance));
                                }
                            }
                        }
                    }
                    if (!String.IsNullOrEmpty(selfieImage) && actionImages.Count >= 20)
                    {
                        try
                        {
                            var logData = new LogLoanInfoAi();
                            var result = _aiService.CheckLivenessV2(selfieImage, actionImages, ref logData);
                            bool IsSuccess = false;
                            if (result != null && result.StatusCode == 200)
                            {
                                if (!result.Result.IsFake.Value && result.Result.IsLiveAction.Value && result.Result.IsMatchFace.Value)
                                    IsSuccess = true;
                                if (IsSuccess)
                                {
                                    //lưu log call liveness
                                    logData.LoanbriefId = req.loanBriefId;
                                    _unitOfWork.LogLoanInfoAiRepository.Insert(logData);
                                    //Add note
                                    var note = new LoanBriefNote
                                    {
                                        LoanBriefId = req.loanBriefId,
                                        Note = "Đã xác minh khách hàng thông qua ekyc",
                                        FullName = "System",
                                        Status = 1,
                                        ActionComment = EnumActionComment.CheckLiveness.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = 0,
                                        ShopId = 0,
                                        ShopName = "APP"
                                    };
                                    _unitOfWork.LoanBriefNoteRepository.Insert(note);
                                    _unitOfWork.SaveChanges();
                                    // Save file to amz, Lấy 5 ảnh random trong list
                                    try
                                    {
                                        Random rnd = new Random();
                                        var textToSave = selfieImage + "," + String.Join(",", actionImages.OrderBy(x => rnd.Next()).Take(5).ToArray());
                                        var imagesToSave = new List<string>();
                                        imagesToSave.Add(selfieImage);
                                        imagesToSave.AddRange(actionImages.OrderBy(x => rnd.Next()).Take(5));
                                        var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                                        var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                                        var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
                                        var BucketName = _baseConfig["AppSettings:BucketName"];
                                        var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
                                        {
                                            RegionEndpoint = RegionEndpoint.APSoutheast1
                                        });

                                        var folder = BucketName + "/uploads/LOS/ekyc/" + req.loanBriefId;
                                        int i = 1;
                                        foreach (var img in imagesToSave)
                                        {
                                            var trimImage = img.Replace("data:image/jpeg;base64,", "");
                                            var fileName = "ekyc_" + i + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".jpg";
                                            using (var stream = new MemoryStream(GenerateBytesFromString(trimImage)))
                                            {

                                                PutObjectRequest putReq = new PutObjectRequest()
                                                {
                                                    InputStream = stream,
                                                    BucketName = folder,
                                                    Key = fileName,
                                                    CannedACL = S3CannedACL.PublicRead
                                                };
                                                await client.PutObjectAsync(putReq);
                                                // Save files
                                                var link = "/uploads/LOS/ekyc/" + req.loanBriefId + "/" + putReq.Key;
                                                var obj = new LoanBriefFiles
                                                {
                                                    LoanBriefId = req.loanBriefId,
                                                    CreateAt = DateTime.Now,
                                                    FilePath = link,
                                                    UserId = 0,
                                                    Status = 1,
                                                    TypeId = 455,
                                                    S3status = 1,
                                                    MecashId = 1000000000,
                                                    SourceUpload = (int)EnumSourceUpload.WebTimaCare,
                                                    TypeFile = (int)TypeFile.Image
                                                };
                                                _unitOfWork.LoanBriefFileRepository.Insert(obj);
                                                _unitOfWork.Save();
                                            }
                                            i++;
                                        }

                                        var customer = _unitOfWork.CustomerRepository.GetById(loanbrief.CustomerId);
                                        if (customer != null && String.IsNullOrEmpty(customer.EsignAgreementId))
                                        {
                                            // upload thành công thì thực hiện đăng ký ekyc luôn
                                            var files = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanbrief.LoanBriefId && x.TypeId == (int)EnumDocumentType.CMND_CCCD
                                               , null, false).OrderByDescending(x => x.CreateAt).ToList();
                                            if (files != null)
                                            {
                                                // Đủ ảnh để đăng ký ekyc
                                                string agreementId = Guid.NewGuid().ToString().ToUpper();
                                                int idType = !String.IsNullOrEmpty(customer.NationalCard) ? 1 : 2;
                                                string idNumber = idType == 1 ? customer.NationalCard : customer.Passport;
                                                if (idType == 1 && idNumber.Length == 12)
                                                    idType = 3;
                                                var province = _unitOfWork.ProvinceRepository.GetById(customer.ProvinceId);
                                                // file 0 > cmnd mặt trước
                                                if (files.Where(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_FRONT).Any() && files.Where(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_BACK).Any())
                                                {
                                                    var front = await ReadImageFromUrl(ServiceURL + files.Where(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_FRONT).First().FilePath);
                                                    var back = await ReadImageFromUrl(ServiceURL + files.Where(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_BACK).First().FilePath);
                                                    var logPartner = new LogPartner();
                                                    var resultEsign = _esignService.prepareCertificateForSignCloud(agreementId, customer.FullName.ToUpper(), idType, idNumber, customer.Address, province.Name, "VN", front, back, customer.Email, customer.Phone, ref logPartner);
                                                    if (resultEsign != null && resultEsign.responseCode == 0)
                                                    {
                                                        logPartner.ResponseCode = resultEsign.responseCode;
                                                        logPartner.ResponseContent = JsonConvert.SerializeObject(resultEsign);
                                                        logPartner.CustomerId = customer.CustomerId;
                                                        _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                                        loanbrief.EsignState = (int)EsignState.READY_TO_SIGN;
                                                        _unitOfWork.LoanBriefRepository.Update(loanbrief);
                                                        _unitOfWork.SaveChanges();
                                                        // success 
                                                        customer.EsignAgreementId = agreementId;
                                                        _unitOfWork.CustomerRepository.Update(customer);
                                                        _unitOfWork.SaveChanges();
                                                    }
                                                    else
                                                    {
                                                        logPartner.CustomerId = customer.CustomerId;
                                                        if (resultEsign != null)
                                                        {
                                                            logPartner.ResponseCode = resultEsign.responseCode;
                                                            logPartner.ResponseContent = JsonConvert.SerializeObject(resultEsign);
                                                        }
                                                        _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                                        _unitOfWork.SaveChanges();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error(ex, "TimaCare/Check Liveness Exception");
                                    }
                                }
                                else
                                {
                                    //lưu log call liveness
                                    logData.LoanbriefId = req.loanBriefId;
                                    _unitOfWork.LogLoanInfoAiRepository.Insert(logData);
                                    _unitOfWork.SaveChanges();
                                }
                            }
                            def.data = IsSuccess;
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            return Ok(def);
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, "TimaCare/Check Liveness Exception");
                            def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "TimaCare/Check Liveness Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("CheckLivenessBase64")]
        [AuthorizeTokenOtp]
        public async Task<IActionResult> CheckLivenessBase64([FromBody] CheckLivenessBase64Req req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.selfie != null && req.files.Count >= 20 && req.loanBriefId > 0)
                {
                    //kiểm tra xem có đũng mã loanbrief không
                    var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == req.loanBriefId, null, false).FirstOrDefaultAsync();

                    if (loanbrief == null)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
                        return Ok(def);
                    }

                    var phoneToken = _identityService.GetPhoneIdentity();
                    if (string.IsNullOrEmpty(phoneToken))
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Token không chính xác");
                        return Ok(def);
                    }

                    if (loanbrief.Phone != phoneToken)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không phải đơn vay của bạn");
                        return Ok(def);
                    }

                    string selfieImage = "";
                    byte[] imageBytesSelfie = Convert.FromBase64String(req.selfie);
                    //resize if needed                                         
                    using (var image = Image.Load(imageBytesSelfie))
                    {
                        if (image.Width >= image.Height) // landscape
                        {
                            if (image.Width > 1280)
                            {
                                image.Mutate(x => x.Resize(1280, image.Height / (image.Width / 1280)));
                                selfieImage = image.ToBase64String(JpegFormat.Instance);
                            }
                            else
                            {
                                selfieImage = req.selfie;
                            }
                        }
                        else // portrait
                        {
                            if (image.Width > 720)
                            {
                                image.Mutate(x => x.Resize(720, image.Height / (image.Width / 720)));
                                selfieImage = image.ToBase64String(JpegFormat.Instance);
                            }
                            else
                            {
                                selfieImage = req.selfie;
                            }
                        }
                    }
                    // action images
                    List<string> actionImages = new List<string>();
                    foreach (var file in req.files)
                    {
                        var fileBase64 = file;
                        if (fileBase64.StartsWith("data:image/jpeg;base64,"))
                            fileBase64 = fileBase64.Replace("data:image/jpeg;base64,", "");
                        byte[] imageBytes = Convert.FromBase64String(fileBase64);
                        //resize if needed                                         
                        using (var image = Image.Load(imageBytes))
                        {
                            if (image.Width >= image.Height) // landscape
                            {
                                if (image.Width > 1280)
                                {
                                    image.Mutate(x => x.Resize(1280, image.Height / (image.Width / 1280)));
                                    actionImages.Add(image.ToBase64String(JpegFormat.Instance));
                                }
                                else
                                {
                                    actionImages.Add(file);
                                }
                            }
                            else // portrait
                            {
                                if (image.Width > 720)
                                {
                                    image.Mutate(x => x.Resize(720, image.Height / (image.Width / 720)));
                                    actionImages.Add(image.ToBase64String(JpegFormat.Instance));
                                }
                                else
                                {
                                    actionImages.Add(file);
                                }
                            }
                        }
                    }
                    if (!String.IsNullOrEmpty(selfieImage) && actionImages.Count >= 20)
                    {
                        try
                        {
                            var logData = new LogLoanInfoAi();
                            var result = _aiService.CheckLivenessV2(selfieImage, actionImages, ref logData);
                            bool IsSuccess = false;
                            if (result != null && result.StatusCode == 200)
                            {
                                if (!result.Result.IsFake.Value && result.Result.IsLiveAction.Value && result.Result.IsMatchFace.Value)
                                    IsSuccess = true;
                                if (IsSuccess)
                                {
                                    //lưu log call liveness
                                    //bỏ body reques
                                    logData.ResultFinal = "SUCCESS";
                                    logData.LoanbriefId = req.loanBriefId;
                                    _unitOfWork.LogLoanInfoAiRepository.Insert(logData);
                                    //Add note
                                    var note = new LoanBriefNote
                                    {
                                        LoanBriefId = req.loanBriefId,
                                        Note = "Đã xác minh khách hàng thông qua ekyc",
                                        FullName = "System",
                                        Status = 1,
                                        ActionComment = EnumActionComment.CheckLiveness.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = 0,
                                        ShopId = 0,
                                        ShopName = "APP"
                                    };
                                    _unitOfWork.LoanBriefNoteRepository.Insert(note);
                                    _unitOfWork.SaveChanges();
                                    // Save file to amz, Lấy 5 ảnh random trong list
                                    try
                                    {
                                        Random rnd = new Random();
                                        //var textToSave = selfieImage + "," + String.Join(",", actionImages.OrderBy(x => rnd.Next()).Take(5).ToArray());
                                        var imagesToSave = new List<string>();
                                        imagesToSave.Add(selfieImage);
                                        imagesToSave.AddRange(actionImages.OrderBy(x => rnd.Next()).Take(5));
                                        var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                                        var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                                        var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
                                        var BucketName = _baseConfig["AppSettings:BucketName"];
                                        var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
                                        {
                                            RegionEndpoint = RegionEndpoint.APSoutheast1
                                        });

                                        var folder = BucketName + "/uploads/LOS/ekyc/" + req.loanBriefId;
                                        int i = 1;
                                        foreach (var img in imagesToSave)
                                        {
                                            var typeId = (int)EnumDocumentType.EkycImage;
                                            if (i == 1)
                                                typeId = (int)EnumDocumentType.Selfie;
                                            var trimImage = img.Replace("data:image/jpeg;base64,", "");
                                            var fileName = "ekyc_" + i + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".jpg";
                                            using (var stream = new MemoryStream(GenerateBytesFromString(trimImage)))
                                            {

                                                PutObjectRequest putReq = new PutObjectRequest()
                                                {
                                                    InputStream = stream,
                                                    BucketName = folder,
                                                    Key = fileName,
                                                    CannedACL = S3CannedACL.PublicRead
                                                };
                                                await client.PutObjectAsync(putReq);
                                                // Save files
                                                var link = "/uploads/LOS/ekyc/" + req.loanBriefId + "/" + putReq.Key;
                                                var obj = new LoanBriefFiles
                                                {
                                                    LoanBriefId = req.loanBriefId,
                                                    CreateAt = DateTime.Now,
                                                    FilePath = link,
                                                    UserId = 0,
                                                    Status = 1,
                                                    TypeId = typeId,
                                                    S3status = 1,
                                                    MecashId = 1000000000,
                                                    SourceUpload = (int)EnumSourceUpload.WebTimaCare,
                                                    TypeFile = (int)TypeFile.Image
                                                };
                                                _unitOfWork.LoanBriefFileRepository.Insert(obj);
                                                _unitOfWork.Save();
                                            }
                                            i++;
                                        }

                                        var customer = _unitOfWork.CustomerRepository.GetById(loanbrief.CustomerId);
                                        if (customer != null && String.IsNullOrEmpty(customer.EsignAgreementId))
                                        {
                                            // upload thành công thì thực hiện đăng ký ekyc luôn
                                            var files = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanbrief.LoanBriefId && x.TypeId == (int)EnumDocumentType.CMND_CCCD
                                               , null, false).OrderByDescending(x => x.CreateAt).ToList();
                                            if (files != null)
                                            {
                                                // Đủ ảnh để đăng ký ekyc
                                                string agreementId = Guid.NewGuid().ToString().ToUpper();
                                                int idType = !String.IsNullOrEmpty(customer.NationalCard) ? 1 : 2;
                                                string idNumber = (idType == 1) ? customer.NationalCard : customer.Passport;
                                                if (idType == 1 && idNumber.Length == 12)
                                                    idType = 3;
                                                var province = _unitOfWork.ProvinceRepository.GetById(customer.ProvinceId);
                                                // file 0 > cmnd mặt trước
                                                if (files.Where(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_FRONT).Any() && files.Where(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_BACK).Any())
                                                {
                                                    var front = await ReadImageFromUrl(ServiceURL + files.Where(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_FRONT).First().FilePath);
                                                    var back = await ReadImageFromUrl(ServiceURL + files.Where(x => x.SubTypeId == (int)TypeUploadWebTimaCare.CMND_BACK).First().FilePath);
                                                    var logPartner = new LogPartner();
                                                    var resultEsign = _esignService.prepareCertificateForSignCloud(agreementId, customer.FullName.ToUpper(), idType, idNumber, customer.Address, province.Name, "VN", front, back, customer.Email, customer.Phone, ref logPartner);
                                                    if (resultEsign != null && resultEsign.responseCode == 0)
                                                    {
                                                        logPartner.ResponseCode = resultEsign.responseCode;
                                                        logPartner.ResponseContent = JsonConvert.SerializeObject(resultEsign);
                                                        logPartner.CustomerId = customer.CustomerId;
                                                        _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                                        loanbrief.EsignState = (int)EsignState.READY_TO_SIGN;
                                                        _unitOfWork.LoanBriefRepository.Update(loanbrief);
                                                        _unitOfWork.SaveChanges();
                                                        // success 
                                                        customer.EsignAgreementId = agreementId;
                                                        _unitOfWork.CustomerRepository.Update(customer);
                                                        _unitOfWork.SaveChanges();
                                                    }
                                                    else
                                                    {
                                                        logPartner.CustomerId = customer.CustomerId;
                                                        if (resultEsign != null)
                                                        {
                                                            logPartner.ResponseCode = resultEsign.responseCode;
                                                            logPartner.ResponseContent = JsonConvert.SerializeObject(result);
                                                        }
                                                        _unitOfWork.LogPartnerRepository.Insert(logPartner);
                                                        _unitOfWork.SaveChanges();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error(ex, "TimaCare/Check Liveness Exception");
                                    }
                                }
                                else
                                {
                                    var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                                    var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                                    var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
                                    var BucketName = _baseConfig["AppSettings:BucketName"];
                                    var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
                                    {
                                        RegionEndpoint = RegionEndpoint.APSoutheast1
                                    });
                                    actionImages.Insert(0, selfieImage);
                                    var images = new Dictionary<int, string>();
                                    var order = 0;
                                    foreach (var image in actionImages)
                                    {
                                        images.Add(order, image);
                                        order++;
                                    }
                                    var folder = BucketName + "/uploads/LOS/ekyc/" + req.loanBriefId;
                                    var attempt = _unitOfWork.EkycImagesRepository.Query(x => x.Attempt > 0, null, false).OrderByDescending(x => x.Attempt).Select(x => x.Attempt).FirstOrDefault();
                                    List<EkycImages> ekycImages = new List<EkycImages>();
                                    var now = DateTime.Now;
                                    var task = images.AsyncParallelForEach(async image =>
                                    {
                                        var type = 2;
                                        if (image.Key == 0)
                                            type = 1;
                                        var trimImage = image.Value.Replace("data:image/jpeg;base64,", "");
                                        var fileName = "ekyc_" + image.Key + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".jpg";
                                        using (var stream = new MemoryStream(GenerateBytesFromString(trimImage)))
                                        {

                                            PutObjectRequest putReq = new PutObjectRequest()
                                            {
                                                InputStream = stream,
                                                BucketName = folder,
                                                Key = fileName,
                                                CannedACL = S3CannedACL.PublicRead
                                            };
                                            await client.PutObjectAsync(putReq);
                                            // Save files
                                            var link = "/uploads/LOS/ekyc/" + req.loanBriefId + "/" + putReq.Key;
                                            var obj = new EkycImages
                                            {
                                                LoanBriefId = req.loanBriefId,
                                                FilePath = link,
                                                Attempt = attempt + 1,
                                                CreatedAt = now,
                                                FileName = fileName,
                                                Order = type == 2 ? image.Key : 1,
                                                Type = type
                                            };
                                            ekycImages.Add(obj);
                                        }
                                    }, 10);
                                    task.Wait();
                                    // Insert to db
                                    _unitOfWork.EkycImagesRepository.Insert(ekycImages);
                                    //lưu log call liveness
                                    logData.LoanbriefId = req.loanBriefId;
                                    logData.Status = 0;
                                    logData.ResultFinal = "ERROR";
                                    _unitOfWork.LogLoanInfoAiRepository.Insert(logData);
                                    _unitOfWork.SaveChanges();
                                    if (result.Result.IsFake.Value)
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Xác thực thất bại. Vui lòng thử lại");
                                    else if (!result.Result.IsLiveAction.Value)
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Hình ảnh xác thực không liên tiếp nhau. Vui lòng thử lại");
                                    else if (!result.Result.IsMatchFace.Value)
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ảnh selfie và hình ảnh trong video không giống nhau. Vui lòng thử lại");
                                    return Ok(def);

                                }
                            }
                            else
                            {
                                var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                                var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                                var ServiceURL = _baseConfig["AppSettings:ServiceURLFileTima"];
                                var BucketName = _baseConfig["AppSettings:BucketName"];
                                var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
                                {
                                    RegionEndpoint = RegionEndpoint.APSoutheast1
                                });
                                actionImages.Insert(0, selfieImage);
                                var images = new Dictionary<int, string>();
                                var order = 0;
                                foreach (var image in actionImages)
                                {
                                    images.Add(order, image);
                                    order++;
                                }
                                var folder = BucketName + "/uploads/LOS/ekyc/" + req.loanBriefId;
                                var attempt = _unitOfWork.EkycImagesRepository.Query(x => x.Attempt > 0, null, false).OrderByDescending(x => x.Attempt).Select(x => x.Attempt).FirstOrDefault();
                                List<EkycImages> ekycImages = new List<EkycImages>();
                                var now = DateTime.Now;
                                var task = images.AsyncParallelForEach(async image =>
                                {
                                    var type = 2;
                                    if (image.Key == 0)
                                        type = 1;
                                    var trimImage = image.Value.Replace("data:image/jpeg;base64,", "");
                                    var fileName = "ekyc_" + image.Key + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".jpg";
                                    using (var stream = new MemoryStream(GenerateBytesFromString(trimImage)))
                                    {

                                        PutObjectRequest putReq = new PutObjectRequest()
                                        {
                                            InputStream = stream,
                                            BucketName = folder,
                                            Key = fileName,
                                            CannedACL = S3CannedACL.PublicRead
                                        };
                                        await client.PutObjectAsync(putReq);
                                        // Save files
                                        var link = "/uploads/LOS/ekyc/" + req.loanBriefId + "/" + putReq.Key;
                                        var obj = new EkycImages
                                        {
                                            LoanBriefId = req.loanBriefId,
                                            FilePath = link,
                                            Attempt = attempt + 1,
                                            CreatedAt = now,
                                            FileName = fileName,
                                            Order = type == 2 ? image.Key : 1,
                                            Type = type
                                        };
                                        ekycImages.Add(obj);
                                    }
                                }, 10);
                                task.Wait();
                                // Insert to db
                                _unitOfWork.EkycImagesRepository.Insert(ekycImages);
                                //lưu log call liveness
                                logData.LoanbriefId = req.loanBriefId;
                                logData.Status = 0;
                                logData.ResultFinal = "ERROR";
                                _unitOfWork.LogLoanInfoAiRepository.Insert(logData);
                                _unitOfWork.SaveChanges();
                            }
                            def.data = IsSuccess;
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                            return Ok(def);
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, "TimaCare/Check Liveness Exception");
                            def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "TimaCare/Check Liveness Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("CheckLivenessVideo")]
        public async Task<IActionResult> CheckLivenessVideo([FromForm] CheckLivenessVideoReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var request = HttpContext.Request;
                if (req.video != null && req.loanBriefId > 0)
                {
                    var extension = Path.GetExtension(req.video.FileName);
                    var allowedExtensions = new[] { ".mp4", ".webm", ".ogg", ".mov" };
                    if (allowedExtensions.Contains(extension.ToLower()))
                    {

                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "TimaCare/Check Liveness Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        public static MemoryStream GenerateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(value ?? ""));
        }

        public static byte[] GenerateBytesFromString(string value)
        {
            return Convert.FromBase64String(value);
        }

        async Task<byte[]> ReadImageFromUrl(string url)
        {
            using (var client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(url))
            {
                return await response.Content.ReadAsByteArrayAsync();
            }
        }
        [Route("check_ekyc_ai")]
        [HttpGet]
        public ActionResult<DefaultResponse<object>> CheckEkycAI(int loanBriefId)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (loanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (_unitOfWork.LogLoanInfoAiRepository.Any(x => x.LoanbriefId == loanBriefId && x.ServiceType == (int)ServiceTypeAI.CheckLiveness && x.Status == 1))
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "TimaCare/CheckEkycAI Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("check_verify_image")]
        [HttpGet]
        public async Task<ActionResult<DefaultResponse<object>>> CheckVerifyImage(string phone)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(phone))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var lstLoan = await _unitOfWork.LoanBriefRepository.Query(x => x.Phone == phone
                && x.Status != (int)EnumLoanStatus.CANCELED
                && x.Status != (int)EnumLoanStatus.DISBURSED
                && x.Status != (int)EnumLoanStatus.FINISH, null, false).Select(x => new
                {
                    LoanBriefId = x.LoanBriefId
                }).ToListAsync();
                var check = false;
                if (lstLoan != null && lstLoan.Count > 0)
                {
                    foreach (var item in lstLoan)
                    {
                        var files = await _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == item.LoanBriefId
                        && x.TypeId == (int)EnumDocumentType.CMND_CCCD
                        && (x.SourceUpload == (int)EnumSourceUpload.TimaCare || x.SourceUpload == (int)EnumSourceUpload.WebTimaCare)
                        && x.SubTypeId.GetValueOrDefault(0) > 0
                        && x.Status == 1 && x.IsDeleted != 1, null, false).Select(x => new
                        {
                            ID = x.Id
                        }).ToListAsync();
                        if (files != null && files.Count >= 2)
                        {
                            check = true;
                            break;
                        }
                    }
                    if (check)
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "TimaCare/CheckVerifyImage Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("get_contract_financial_agreement_base64")]
        [HttpGet]
        public ActionResult<DefaultResponse<Meta, string>> GetContractFinancialAgreementBase64(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, string>();
            try
            {
                var data = _exportPdfService.ContractFinancialAgreementBase64(loanbriefId);
                if (data != null)
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = data;
                }
                else
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Lỗi hệ thống. Vui lòng thử lại sau.");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "TimaCare/GetContractFinancialAgreementBase64 Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("check_validate_esign")]
        [HttpGet]
        public async Task<ActionResult<DefaultResponse<object>>> CheckValidateEsign(int loanBriefId)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (loanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var data = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanBriefId, null, false).Select(DAL.DTOs.CheckValidate.ProjectionDetail).FirstOrDefaultAsync();
                if (data != null)
                {
                    //Kiểm tra trạng thái đơn
                    if (data.Status != (int)EnumLoanStatus.HUB_LOAN_DISTRIBUTING && data.Status != (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING
                        && data.Status != (int)EnumLoanStatus.APPRAISER_REVIEW && data.Status != (int)EnumLoanStatus.HUB_CHT_APPROVE
                        && data.Status != (int)EnumLoanStatus.WAIT_HUB_EMPLOYEE_PREPARE_LOAN)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Đơn vay đang được phê duyệt, không thể thực hiện ký hợp đồng");
                        return Ok(def);
                    }
                    //Customer
                    if (string.IsNullOrEmpty(data.FullName))
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin Tên khách hàng");
                        return Ok(def);
                    }
                    if (string.IsNullOrEmpty(data.Dob.ToString()))
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin ngày sinh khách hàng");
                        return Ok(def);
                    }
                    if (string.IsNullOrEmpty(data.NationalCard))
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin CMND/CCCD");
                        return Ok(def);
                    }

                    if (data.LoanBriefResident == null || string.IsNullOrEmpty(data.LoanBriefResident.Address))
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin địa chỉ hiện tại");
                        return Ok(def);
                    }
                    if (data.LoanBriefHousehold == null || string.IsNullOrEmpty(data.LoanBriefHousehold.Address))
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin địa chỉ thường trú");
                        return Ok(def);
                    }
                    //Loan
                    if (data.LoanAmount.GetValueOrDefault(0) == 0)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có số tiền đăng ký vay");
                        return Ok(def);
                    }
                    if (data.LoanTime.GetValueOrDefault(0) == 0)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thời gian vay");
                        return Ok(def);
                    }
                    if (data.LoanPurpose.GetValueOrDefault(0) == 0)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có mục đích vay");
                        return Ok(def);
                    }
                    if (data.ReceivingMoneyType.GetValueOrDefault(0) == 0)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có hình thức nhận tiền giải ngân");
                        return Ok(def);
                    }
                    if (data.ReceivingMoneyType == (int)EnumTypeReceivingMoney.NumberAccount)
                    {
                        if (data.BankId.GetValueOrDefault(0) == 0)
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin ngân hàng");
                            return Ok(def);
                        }
                        if (string.IsNullOrEmpty(data.BankAccountNumber))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có Số tài khoản ngân hàng");
                            return Ok(def);
                        }
                    }
                    //LoanBriefProperty
                    if (data.LoanBriefProperty != null)
                    {
                        if (string.IsNullOrEmpty(data.LoanBriefProperty.MotobikeCertificateNumber))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin số đăng ký xe");
                            return Ok(def);
                        }
                        if (string.IsNullOrEmpty(data.LoanBriefProperty.PlateNumber))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin biển số xe");
                            return Ok(def);
                        }
                        if (string.IsNullOrEmpty(data.LoanBriefProperty.OwnerFullName))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin chủ xe");
                            return Ok(def);
                        }
                        if (data.LoanBriefProperty.BrandId.GetValueOrDefault(0) == 0)
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin hãng xe");
                            return Ok(def);
                        }
                        if (data.LoanBriefProperty.ProductId.GetValueOrDefault(0) == 0)
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin tên xe");
                            return Ok(def);
                        }
                        if (string.IsNullOrEmpty(data.LoanBriefProperty.Chassis))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin số khung");
                            return Ok(def);
                        }
                        if (string.IsNullOrEmpty(data.LoanBriefProperty.Engine))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin số máy");
                            return Ok(def);
                        }

                        if (string.IsNullOrEmpty(data.LoanBriefProperty.MotobikeCertificateDate))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin ngày cấp đăng ký xe");
                            return Ok(def);
                        }
                        if (string.IsNullOrEmpty(data.LoanBriefProperty.MotobikeCertificateAddress))
                        {
                            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin nơi cấp đăng ký xe");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, "Chưa có thông tin tài sản");
                        return Ok(def);
                    }

                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "TimaCare/CheckValidateEsign Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("get_contract_financial_agreement_lender_base64")]
        [HttpGet]
        public ActionResult<DefaultResponse<Meta, string>> GetContractFinancialAgreementLenderBase64(int loanbriefId, int lenderId)
        {
            var def = new DefaultResponse<Meta, string>();
            try
            {
                var data = _exportPdfService.ContractFinancialAgreementLenderBase64(loanbriefId, lenderId);
                if (data != null)
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = data;
                }
                else
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Lỗi hệ thống. Vui lòng thử lại sau.");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "TimaCare/GetContractFinancialAgreementBase64 Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("recove_otp_sign_contract")]
        [HttpGet]
        [AuthorizeTokenOtp]
        public async Task<ActionResult<DefaultResponse<object>>> RecoveOTPSignContract(int customerId)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //lấy ra thông tin khách hàng
                var customer = await _unitOfWork.CustomerRepository.Query(x => x.CustomerId == customerId, null, false).Select(x => new
                {
                    CustomerId = x.CustomerId,
                    Phone = x.Phone,
                    EsignAgreementId = x.EsignAgreementId
                }).FirstOrDefaultAsync();
                if (customer != null && customer.CustomerId > 0 && !string.IsNullOrEmpty(customer.EsignAgreementId))
                {
                    //kiểm tra xem otp cũ đã quá 3 phút chưa
                    if (_unitOfWork.LogSendOtpRepository.Any(x => x.Phone == customer.Phone
                    && x.TypeOtp == (int)TypeOtp.VerifyEsignCustomer
                    && Convert.ToInt32((DateTime.Now - x.CreatedAt.Value).TotalMinutes) > 3))
                    {
                        //gửi otp mới
                        var logPartner = new LogPartner();
                        var result = _esignService.regenerateAuthorizationCodeForSignCloud(customer.EsignAgreementId, ref logPartner);
                        if (result != null)
                        {
                            logPartner.ResponseCode = result.responseCode;
                            logPartner.ResponseContent = JsonConvert.SerializeObject(result);
                            logPartner.CustomerId = customer.CustomerId;
                            _unitOfWork.LogPartnerRepository.Insert(logPartner);
                            _unitOfWork.SaveChanges();
                            if (result.responseCode == 0)
                            {
                                if (!string.IsNullOrEmpty(result.authorizeCredential))
                                {
                                    // Tự send OTP
                                    string smsTemplate = _baseConfig["AppSettings:esign_sms_template"].ToString();
                                    smsTemplate = String.Format(smsTemplate, result.authorizeCredential);
                                    var resultOTP = _lmsService.SendOtpSmsBrandName((int)TypeOtp.VerifyEsignCustomer, customer.Phone, smsTemplate, result.authorizeCredential, DateTime.Now.AddMinutes(3), GetIpAddressRemote());
                                    if (resultOTP != null && resultOTP.Result == (int)StatusCallApi.Success)
                                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                    else
                                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Lỗi khi gửi OTP. Vui lòng thử lại sau");
                                }
                                else
                                {
                                    var oblLogSendOtp = new LogSendOtp()
                                    {
                                        Phone = customer.Phone,
                                        CreatedAt = DateTime.Now,
                                        TypeOtp = (int)TypeOtp.VerifyEsignCustomer,
                                        TypeSendOtp = (int)TypeSendOtp.FtpSendOtp,
                                        IpAddress = GetIpAddressRemote(),
                                        ExpiredVerifyOtp = DateTime.Now.AddMinutes(3)
                                    };
                                    _unitOfWork.LogSendOtpRepository.Insert(oblLogSendOtp);
                                    _unitOfWork.SaveChanges();
                                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                                }
                            }
                            else
                                def.meta = new Meta(ResponseHelper.FAIL_CODE, "Lỗi khi gửi OTP. Vui lòng thử lại sau");
                        }
                        else
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Lỗi khi gửi OTP. Vui lòng thử lại sau");
                    }
                    else
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "OTP cũ chưa hết hạn. Vui lòng dung OTP cũ mà khách hàng đã nhận được");
                }
                else
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin khách hàng, hoặc khách hàng chưa đăng ký chữ ký. Vui lòng thử lại sau");

            }
            catch (Exception ex)
            {
                Log.Error(ex, "TimaCare/RecoveOTPSignContract Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        private string GetIpAddressRemote()
        {
            try
            {
                StringValues ipRequestHeader;
                var authIpRequest = HttpContext.Request.Headers.TryGetValue("X-Forwarded-For", out ipRequestHeader);
                if (authIpRequest)
                {
                    var remoteIp = GetIpRequest(ipRequestHeader);
                    if (!string.IsNullOrEmpty(remoteIp))
                    {
                        System.Net.IPAddress ipAddress = null;
                        bool isValidIp = System.Net.IPAddress.TryParse(remoteIp, out ipAddress);
                        if (isValidIp)
                            return ipAddress.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
            }
            return string.Empty;
        }

        private string GetIpRequest(string ipRequestHeader)
        {
            if (!string.IsNullOrEmpty(ipRequestHeader))
            {
                if (ipRequestHeader.Contains(","))
                    return ipRequestHeader.Split(',').First().Trim();
                else return ipRequestHeader.Trim();
            }
            return string.Empty;
        }
    }
}
