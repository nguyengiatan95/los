﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LOS.AppAPI.DTOs.LoanBriefDTO;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Models.LMS;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.LmsService;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ToolController : BaseController
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;
        private ILmsService _lmsService;
        private IProductPrice _productPrice;
       
        public ToolController(IUnitOfWork unitOfWork, IConfiguration configuration, ILmsService lmsService, IProductPrice productPrice)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
            this._lmsService = lmsService;
            this._productPrice = productPrice;         
        }

        [HttpPost]
        [Route("calculation_interest_often")]
        public async Task<ActionResult<DefaultResponse<Meta, ResponceCalculationInterestOften>>> CalculationInterestOften(RequestCalculationInterestOften entity)
        {
            var def = new DefaultResponse<Meta, ResponceCalculationInterestOften>();
            try
            {
                var data = new ResponceCalculationInterestOften();
                var request = new InterestAndInsurenceInput()
                {
                    TotalMoneyDisbursement = entity.TotalMoney,
                    LoanTime = entity.LoanTime * 30,
                    RateType = entity.RateType,
                    Frequency = entity.Frequency,
                    Rate = (entity.Rate * 365 / 1000000) * 100,
                    NotSchedule = 0,
                };
                var result = _lmsService.InterestAndInsurence(request);
                if (result != null && result.Data != null && result.Data.LstPaymentSchedule != null)
                {
                    data.FeeInsurrance = result.Data.FeeInsurrance;
                    data.FeeInsuranceMaterialCovered = result.Data.FeeInsuranceMaterialCovered;
                    data.lstPaymentSchedule = result.Data.LstPaymentSchedule.ToList();
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Tool/CalculationInterestOften Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("calculation_interest_topup")]
        public async Task<ActionResult<DefaultResponse<Meta, ResponceCalculationInterestTopup>>> CalculationInterestTopup(RequestCalculationInterestTopup entity)
        {
            var def = new DefaultResponse<Meta, ResponceCalculationInterestTopup>();
            try
            {
                var data = new ResponceCalculationInterestTopup();
                var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.CodeId == entity.CodeId, null, false)
                    .Select(x => new { LoanBriefId = x.LoanBriefId, LmsLoanId = x.LmsLoanId }).FirstOrDefaultAsync();
                if (loanbrief != null && loanbrief.LoanBriefId > 0 && loanbrief.LmsLoanId.HasValue)
                {
                    var objRequestCalendarPaymentTopup = new RequestCalendarPaymentTopup()
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        LoanId = loanbrief.LmsLoanId.Value,
                        LoanTime = entity.LoanTime * 30,
                        TotalMoneyDisbursement = entity.TotalMoney
                    };
                    var result = _lmsService.GenCalendarPaymentTopup(objRequestCalendarPaymentTopup);
                    if (result != null && result.DataTopUp != null && result.DataTopUp.Keys.Count > 0)
                    {
                        data.MoneyFeeInsuranceOfCustomer = result.MoneyFeeInsuranceOfCustomer;
                        data.MoneyInsuranceOfMaterialCovered = result.MoneyInsuranceOfMaterialCovered;
                        data.DataTopUp = new List<ListPaymentScheduleTopup>();
                        foreach (var itemKey in result.DataTopUp.Keys)
                        {
                            var dataDic = result.DataTopUp[itemKey];
                            var dataTopUp = new ListPaymentScheduleTopup();
                            dataTopUp.DataPaymentScheduleTopup = new List<PaymentScheduleBasic>();
                            for (var i = 0; i < dataDic.Count; i++)
                            {
                                dataTopUp.DataPaymentScheduleTopup.Add(dataDic[i]);
                            }
                            data.DataTopUp.Add(dataTopUp);
                        }
                    }
                    else
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Call API LMS lỗi");

                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = data;
                }
                else
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Tool/CalculationInterestTopup Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("price_motobike")]
        public async Task<ActionResult<DefaultResponse<Meta, ResponcePriceMotobike>>> PriceMotobike(RequestPriceMotobike entity)
        {
            var def = new DefaultResponse<Meta, ResponcePriceMotobike>();
            try
            {
                var data = new ResponcePriceMotobike();
                var maxPriceProduct = 0L;
                decimal priceCarAI = 0;
                var maxPriceLtv = 0L;
                // lấy giá bên AI
                var priceCurrent = 0L;
                priceCarAI = _productPrice.GetPriceAI(entity.MotobikeId, 0);
                if (priceCarAI > 0)
                    priceCurrent = (long)priceCarAI;
                if (priceCurrent > 0)
                {
                    maxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPriceV3(entity.ProductId, priceCurrent, entity.TypeOwnership, entity.ProductDetailId, 0, 0, 0, entity.Ltv);
                    maxPriceLtv = Common.Utils.ProductPriceUtils.GetMaxPriceLtv(entity.ProductId, priceCurrent, entity.Ltv);
                    data.CurrentPrice = priceCurrent;
                    data.MaxPrice = maxPriceProduct;
                    data.MaxPriceLtv = maxPriceLtv;

                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Tool/PriceMotobike Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("premature_interest")]
        public async Task<ActionResult<DefaultResponse<Meta, ResponcePrematureInterest>>> PrematureInterest(RequestPrematureInterest entity)
        {
            var def = new DefaultResponse<Meta, ResponcePrematureInterest>();
            try
            {
                if (string.IsNullOrEmpty(entity.Date))
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Phải truyền ngày tất toán");
                var date = DateTimeUtility.ConvertStringToDate(entity.Date, DateTimeUtility.DATE_FORMAT);
                if (date < DateTime.Now.AddDays(-1))
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Ngày tất toán phải lớn hơn ngày hiện tại");

                List<int> lstLoanId = new List<int>();
                var data = new ResponcePrematureInterest();
                data.InfomationCutomer = new List<LoanBriefPrematureInterest>();
                data.ItemPrematureInterestMoneyCustomer = new List<ItemPrematureInterestMoneyCustomer>();
                data.PrematureInterestPaymentSchedules = new List<PrematureInterestPaymentSchedules>();
                if (entity.LoanBriefId == 0 && string.IsNullOrEmpty(entity.TextSearch))
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không được bỏ trống SĐT, CMND hoặc Mã HĐ");
                else if (entity.LoanBriefId > 0)
                {
                    //Mã HĐ
                    //lấy thông tin cơ bản của đơn vay
                    var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false)
                        .Select(LoanBriefBasicInfo.ProjectionViewDetail).FirstOrDefaultAsync();
                    if (loanbrief != null)
                    {
                        if (loanbrief.Status != (int)EnumLoanStatus.FINISH && loanbrief.Status != (int)EnumLoanStatus.DISBURSED)
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Hợp đồng chưa được giải ngân. Vui lòng kiểm tra lại");

                        //add thông tin của khách hàng
                        var loanBriefPrematureInterest = new LoanBriefPrematureInterest()
                        {
                            FullName = loanbrief.FullName,
                            IsLocate = loanbrief.IsLocate,
                            Status = loanbrief.Status,
                            LmsLoanId = loanbrief.LmsLoanId,
                            LoanBriefId = loanbrief.LoanBriefId,
                            CodeId = loanbrief.CodeId,
                        };
                        data.InfomationCutomer.Add(loanBriefPrematureInterest);
                        lstLoanId.Add(loanbrief.LmsLoanId.Value);
                    }
                    else
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy đơn vay. Vui lòng kiểm tra lại");
                }
                else if (!string.IsNullOrEmpty(entity.TextSearch))
                {
                    // SĐT, CMND
                    if (entity.TextSearch.Length == 10)
                    {
                        if (!entity.TextSearch.ValidPhone())
                            def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại không đúng định dạng. Vui lòng kiểm tra lại");
                    }
                    //lấy danh sách đơn vay dựa vào SĐT, CMND
                    var lstLoanBriefPrematureInterest = new List<LoanBriefPrematureInterest>();
                    //tìm theo sđt
                    if (entity.TextSearch.Length == 10)
                    {
                        lstLoanBriefPrematureInterest = await _unitOfWork.LoanBriefRepository.Query(x => x.Phone == entity.TextSearch
                        && (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH), null, false)
                            .OrderByDescending(x => x.LoanBriefId).Select(LoanBriefPrematureInterest.ProjectionDetail).ToListAsync();
                    }
                    //tìm theo số cmnd
                    else
                    {
                        lstLoanBriefPrematureInterest = await _unitOfWork.LoanBriefRepository.Query(x => x.NationalCard == entity.TextSearch
                        && (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH), null, false)
                            .OrderByDescending(x => x.LoanBriefId).Select(LoanBriefPrematureInterest.ProjectionDetail).ToListAsync();
                    }
                    if (lstLoanBriefPrematureInterest != null && lstLoanBriefPrematureInterest.Count > 0)
                    {
                        foreach (var item in lstLoanBriefPrematureInterest)
                        {
                            data.InfomationCutomer.Add(item);
                            lstLoanId.Add(item.LmsLoanId.Value);
                        }
                    }
                    else
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy đơn vay. Vui lòng kiểm tra lại");
                }

                if (lstLoanId != null && lstLoanId.Count > 0)
                {
                    var lstPrematureInterestLMS = new List<ResponcePrematureInterestLMS>();
                    foreach (var item in lstLoanId)
                    {
                        var objRequestPrematureInterestLMS = new RequestPrematureInterestLMS()
                        {
                            LoanID = item,
                            CloseDate = entity.Date
                        };
                        var dataLms = _lmsService.GetPayAmountForCustomerByLoanID(objRequestPrematureInterestLMS, entity.LoanBriefId);
                        if (dataLms != null && dataLms.Data != null && dataLms.Data.LstLoanInfos != null && dataLms.Data.LstLoanInfos.Count > 0)
                            lstPrematureInterestLMS.Add(dataLms);
                    }
                    if (lstPrematureInterestLMS != null && lstPrematureInterestLMS.Count > 0)
                    {
                        foreach (var item in lstPrematureInterestLMS)
                        {
                            foreach (var loanInfo in item.Data.LstLoanInfos)
                            {
                                data.ItemPrematureInterestMoneyCustomer.Add(new ItemPrematureInterestMoneyCustomer()
                                {
                                    CodeId = loanInfo.LoanCodeID,
                                    TotalMoneyCurrent = loanInfo.TotalMoneyCurrent,
                                    MoneyFineOriginal = loanInfo.MoneyFineOriginal,
                                    MoneyInterest = loanInfo.MoneyInterest,
                                    OtherMoney = loanInfo.OtherMoney,
                                    MoneyCloseLoan = loanInfo.MoneyCloseLoan,
                                    TotalPaymentCustomer = item.Data.CustomerMoney > 0 ? (loanInfo.MoneyCloseLoan - item.Data.CustomerMoney) : loanInfo.MoneyCloseLoan,
                                });

                                data.PrematureInterestPaymentSchedules.Add(new PrematureInterestPaymentSchedules()
                                {
                                    CodeId = loanInfo.LoanCodeID,
                                    LstPaymentSchedules = loanInfo.LstPaymentSchedules,
                                });
                            }
                        }

                        data.PrematureInterestMoneyCustomer = new PrematureInterestMoneyCustomer()
                        {
                            FullName = data.InfomationCutomer[0].FullName,
                            DateSettlement = entity.Date,
                            CustomerMoney = lstPrematureInterestLMS[0].Data.CustomerMoney.ToString("###,0")
                        };
                    }
                }
                else
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy thông tin đơn vay. Vui lòng kiểm tra lại SĐT, CMND");

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Tool/PrematureInterest Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

    }
}