﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Models.Sercurity;
using LOS.Common.Extensions;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    // [Authorize]
    public class UserController : BaseController
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration configuration;

        public UserController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this.configuration = configuration;
        }

        [HttpPost]
        [Route("update_fcm")]
        [Authorize]
        public ActionResult<DefaultResponse<object>> UpdateFCM(UpdateFcmReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var userId = GetUserId();
                if (string.IsNullOrEmpty(req.TokenDevice) || userId == 0 || !Enum.IsDefined(typeof(EnumDeviceOS), req.DeviceId))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var user = _unitOfWork.UserRepository.GetById(userId);
                if (user != null && user.UserId > 0)
                {
                    if (req.DeviceId == EnumDeviceOS.IOS.GetHashCode())
                        user.PushTokenIos = req.TokenDevice;
                    else
                        user.PushTokenAndroid = req.TokenDevice;
                    _unitOfWork.UserRepository.Update(user);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/UpdateFCM Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("user_info")]
        [Authorize]
        public ActionResult<DefaultResponse<UserInfo>> GetUserInfo()
        {
            var def = new DefaultResponse<UserInfo>();
            try
            {
                var userId = GetUserId();
                if (userId > 0)
                {
                    var data = _unitOfWork.UserRepository.Query(x => x.UserId == userId, null, false).Select(UserInfo.ProjectionDetail).FirstOrDefault();
                    if (data != null)
                    {
                        def.data = data;
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "API_APP User/GetUserInfo exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return Ok(def);
            }
        }
    }
}