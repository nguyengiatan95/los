﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Models.AG;
using LOS.AppAPI.Models.Webhook;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.AG;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.TagHelpers.Cache;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace LOS.AppAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class WebhookController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration configuration;
        private IErp _erpService;
        private IAgService _agService;

        public WebhookController(IUnitOfWork unitOfWork, IConfiguration configuration, IErp erpService, IAgService agService)
        {
            this._unitOfWork = unitOfWork;
            this.configuration = configuration;
            this._erpService = erpService;
            this._agService = agService;
        }

        [HttpPost]
        [Route("rule_cac")]
        [Authorize]
        public ActionResult<DefaultResponse<object>> UpdateRuleCAC(RuleCacReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(req.ref_code) || req.response == null)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var logLoanInfoAi = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.RefCode == req.ref_code && x.IsCancel != 1, null, false).FirstOrDefault();
                if (logLoanInfoAi != null && logLoanInfoAi.Id > 0)
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(req.response);
                    logLoanInfoAi.Response = json;
                    logLoanInfoAi.UpdatedAt = DateTime.Now;
                    logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.HasResult.GetHashCode();//có kết quả
                    _unitOfWork.LogLoanInfoAiRepository.Update(logLoanInfoAi);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/UpdateRuleCAC Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("location_viettel")]
        // [Authorize]
        public ActionResult<DefaultResponse<object>> UpdateLocationViettel(LocationViettelReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(req.refCode))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var logLoanInfoAi = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.RefCode == req.refCode && x.HomeNetwok == "viettel"
                                                            && x.ServiceType == (int)ServiceTypeAI.Location && x.IsCancel != 1, null, false).FirstOrDefault();
                if (logLoanInfoAi != null && logLoanInfoAi.Id > 0)
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                    logLoanInfoAi.Response = json;
                    logLoanInfoAi.UpdatedAt = DateTime.Now;
                    logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.HasResult.GetHashCode();//có kết quả
                    _unitOfWork.LogLoanInfoAiRepository.Update(logLoanInfoAi);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/UpdateLocationViettel Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("location_mobi")]
        // [Authorize]
        public ActionResult<DefaultResponse<object>> UpdateLocationMobi(LocationMobiReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(req.refCode))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var logLoanInfoAi = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.RefCode == req.refCode && x.HomeNetwok == "mobifone"
                                                    && x.ServiceType == (int)ServiceTypeAI.Location && x.IsCancel != 1, null, false).FirstOrDefault();
                if (logLoanInfoAi != null && logLoanInfoAi.Id > 0)
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                    logLoanInfoAi.Response = json;
                    logLoanInfoAi.UpdatedAt = DateTime.Now;
                    logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.HasResult.GetHashCode();//có kết quả
                    _unitOfWork.LogLoanInfoAiRepository.Update(logLoanInfoAi);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/UpdateLocationMobi Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("ref_phone_viettel")]
        public ActionResult<DefaultResponse<object>> UpdateRefPhone(RefphoneReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(req.refCode))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var logLoanInfoAi = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.RefCode == req.refCode && x.ServiceType == (int)ServiceTypeAI.RefPhone && x.IsCancel != 1, null, false).FirstOrDefault();
                if (logLoanInfoAi != null && logLoanInfoAi.Id > 0)
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                    logLoanInfoAi.Response = json;
                    logLoanInfoAi.UpdatedAt = DateTime.Now;
                    logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.HasResult.GetHashCode();//có kết quả
                    _unitOfWork.LogLoanInfoAiRepository.Update(logLoanInfoAi);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/UpdateRefPhone Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("ref_phone_mobifone")]
        public ActionResult<DefaultResponse<object>> UpdateRefPhoneMobi(RefphoneMobiReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(req.refCode))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var logLoanInfoAi = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.RefCode == req.refCode && x.ServiceType == (int)ServiceTypeAI.RefPhone && x.IsCancel != 1, null, false).FirstOrDefault();
                if (logLoanInfoAi != null && logLoanInfoAi.Id > 0)
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                    logLoanInfoAi.Response = json;
                    logLoanInfoAi.UpdatedAt = DateTime.Now;
                    logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.HasResult.GetHashCode();//có kết quả
                    _unitOfWork.LogLoanInfoAiRepository.Update(logLoanInfoAi);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/UpdateRefPhone Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("credit_scoring")]
        [Authorize]
        public ActionResult<DefaultResponse<object>> UpdateCreditScoring(CreditScoringReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(req.RequestID) || req.RequestID == null)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var logLoanInfoAi = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.RefCode == req.RequestID && x.IsCancel != 1, null, false).FirstOrDefault();
                if (logLoanInfoAi != null && logLoanInfoAi.Id > 0)
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                    logLoanInfoAi.Response = json;
                    logLoanInfoAi.UpdatedAt = DateTime.Now;
                    logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.HasResult.GetHashCode();//có kết quả
                    _unitOfWork.LogLoanInfoAiRepository.Update(logLoanInfoAi);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tồn tại RequestID");
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/UpdateCreditScoring Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("actived_device")]
        //Status: 2 thành công, -1 Thất bại
        public ActionResult<DefaultResponse<object>> ActiveDevice(ActiveDeviceReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(req.Imei) || !Enum.IsDefined(typeof(StatusOfDeviceID), req.StatusOfDevice) || string.IsNullOrEmpty(req.ContractId))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanbriefId = 0;
                var arr = req.ContractId.Split('-');
                if (arr != null && arr.Length == 2)
                    Int32.TryParse(arr[1], out loanbriefId);

                var loanbrief = _unitOfWork.LoanBriefRepository.GetById(loanbriefId);
                if (loanbrief != null)
                {
                    if (!string.IsNullOrEmpty(loanbrief.DeviceId) && loanbrief.DeviceId.Trim() == req.Imei.Trim())
                    {
                        if (loanbrief.DeviceStatus != req.StatusOfDevice)
                        {

                            _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                            {
                                DeviceStatus = req.StatusOfDevice,
                                ActivedDeviceAt = DateTime.Now
                            });
                            _unitOfWork.SaveChanges();
                            //nếu là kích hoạt thiết bị thì gọi sang erp
                            if (req.StatusOfDevice == StatusOfDeviceID.Actived.GetHashCode())
                            {
                                CallToErp(new Models.Erp.PushDeviceReq()
                                {
                                    loanId = loanbrief.LoanBriefId,
                                    serial = req.Imei,
                                    status = (int)StatusOfDeviceID.Actived
                                });
                            }
                        }
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Imei không trùng với mã hợp đồng");
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tồn tại thông tin đơn vay");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/ActiveDevice Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        //Cập nhật kết quả step của autocall
        //result:0 - Không đồng ý, 1 - đồng ý , 3 - không thao tác
        [HttpPost]
        [Route("result_autocall")]
        public ActionResult<DefaultResponse<object>> ResultAutoCall(ResultAutocallReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var json = JsonConvert.SerializeObject(req);
                LogRequest("ResultAutoCall", json);

                if (string.IsNullOrEmpty(req.phone) || string.IsNullOrEmpty(req.step))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (!req.loanbriefId.HasValue)
                {
                    var loanbrief = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == req.phone
                             && (x.Status == (int)EnumLoanStatus.INIT || x.Status == (int)EnumLoanStatus.SALEADMIN_LOAN_DISTRIBUTING
                             || x.Status == (int)EnumLoanStatus.SYSTEM_TELESALE_DISTRIBUTING || x.Status == (int)EnumLoanStatus.TELESALE_ADVICE), null, false)
                                 .OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                    if (loanbrief != null && loanbrief.LoanBriefId > 0)
                        req.loanbriefId = loanbrief.LoanBriefId;
                }

                if (req.loanbriefId > 0)
                {
                    var logResult = _unitOfWork.LogResultAutoCallRepository.Query(x => x.LoanbriefId == req.loanbriefId, null, false).FirstOrDefault();
                    if (logResult != null)
                    {
                        var step = 0;
                        if (logResult.MotobikeCertificate == 1)
                            step++;
                        if (logResult.AreaSupport == 1)
                            step++;
                        if (logResult.BorrowMotobike == 1)
                            step++;

                        if (req.step == "motobike")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                BorrowMotobike = req.value
                            });
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = step + 1
                                });
                            }

                            //Thêm vào bảng kịch bản telesale
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }

                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có sử dụng xe máy hay không: {0}", req.value == EnumAutoCallValue.NoAccept.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "area")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                AreaSupport = req.value
                            });
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = step + 1
                                });
                            }
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode() || req.value == EnumAutoCallValue.NoAccept.GetHashCode())
                            {
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có ở khu vực HN hoặc HCM hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "connect_time")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                ConnectNextTime = req.value
                            });
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "result_call")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                ResultCall = req.value
                            });
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "motobike_certificate")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                MotobikeCertificate = req.value
                            });
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = step + 1
                                });
                            }
                            //Thêm vào bảng kịch bản telesale
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có đăng ký xe máy bản gốc hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Step không tồn tại");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        var objLog = new LogResultAutoCall();
                        objLog.LoanbriefId = req.loanbriefId;
                        objLog.CreatedAt = DateTime.Now;
                        objLog.CampaignId = req.campaignId;
                        objLog.PhoneAgent = req.phone;
                        if (req.step == "motobike" && req.value.HasValue)
                        {
                            objLog.BorrowMotobike = req.value.Value;
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = 1
                                });
                            }
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có sử dụng xe máy hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                        }
                        else if (req.step == "area" && req.value.HasValue)
                        {
                            objLog.AreaSupport = req.value.Value;
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = 1
                                });
                            }
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode() || req.value == EnumAutoCallValue.NoAccept.GetHashCode())
                            {
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có ở khu vực HN hoặc HCM hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                        }
                        else if (req.step == "connect_time" && req.value.HasValue)
                        {
                            objLog.ConnectNextTime = req.value.Value;
                        }
                        else if (req.step == "result_call" && req.value.HasValue)
                        {
                            objLog.ResultCall = req.value.Value;
                        }
                        else if (req.step == "motobike_certificate" && req.value.HasValue)
                        {
                            objLog.MotobikeCertificate = req.value.Value;
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = 1
                                });
                            }
                            //Thêm vào bảng kịch bản telesale
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có đăng ký xe máy bản gốc hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                        }
                        _unitOfWork.LogResultAutoCallRepository.Insert(objLog);
                        _unitOfWork.Save();

                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tồn tại thông tin đơn vay");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/ResultAutoCall Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Cập nhật kết quả step của autocall đơn đã hủy
        //result:0 - Không đồng ý, 1 - đồng ý , 3 - không thao tác
        [HttpPost]
        [Route("result_autocall_cancel")]
        public ActionResult<DefaultResponse<object>> ResultAutoCallCancel(ResultAutocallReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var json = JsonConvert.SerializeObject(req);
                // LogRequest("ResultAutoCallCancel", json);

                if (string.IsNullOrEmpty(req.phone) || string.IsNullOrEmpty(req.step))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (!req.loanbriefId.HasValue)
                {
                    var loanbrief = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == req.phone
                             && x.Status == (int)EnumLoanStatus.CANCELED, null, false)
                                 .OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                    if (loanbrief != null && loanbrief.LoanBriefId > 0)
                        req.loanbriefId = loanbrief.LoanBriefId;
                }

                if (req.loanbriefId > 0)
                {
                    var logResult = _unitOfWork.LogResultAutoCallRepository.Query(x => x.LoanbriefId == req.loanbriefId, null, false).FirstOrDefault();
                    if (logResult != null)
                    {
                        var step = 0;
                        if (logResult.MotobikeCertificate == 1)
                            step++;
                        if (logResult.AreaSupport == 1)
                            step++;
                        if (logResult.BorrowMotobike == 1)
                            step++;

                        if (req.step == "motobike")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                BorrowMotobike = req.value
                            });
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = step + 1
                                });
                            }

                            //Thêm vào bảng kịch bản telesale
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }

                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có sử dụng xe máy hay không: {0}", req.value == EnumAutoCallValue.NoAccept.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "area")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                AreaSupport = req.value
                            });
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = step + 1
                                });
                            }
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode() || req.value == EnumAutoCallValue.NoAccept.GetHashCode())
                            {
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có ở khu vực HN hoặc HCM hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "connect_time")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                ConnectNextTime = req.value
                            });
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "result_call")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                ResultCall = req.value
                            });
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "motobike_certificate")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                MotobikeCertificate = req.value
                            });
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = step + 1
                                });
                            }
                            //Thêm vào bảng kịch bản telesale
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có đăng ký xe máy bản gốc hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Step không tồn tại");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        var objLog = new LogResultAutoCall();
                        objLog.LoanbriefId = req.loanbriefId;
                        objLog.CreatedAt = DateTime.Now;
                        objLog.CampaignId = req.campaignId;
                        if (req.step == "motobike" && req.value.HasValue)
                        {
                            objLog.BorrowMotobike = req.value.Value;
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = 1
                                });
                            }
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có sử dụng xe máy hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                        }
                        else if (req.step == "area" && req.value.HasValue)
                        {
                            objLog.AreaSupport = req.value.Value;
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = 1
                                });
                            }
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode() || req.value == EnumAutoCallValue.NoAccept.GetHashCode())
                            {
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có ở khu vực HN hoặc HCM hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                        }
                        else if (req.step == "connect_time" && req.value.HasValue)
                        {
                            objLog.ConnectNextTime = req.value.Value;
                        }
                        else if (req.step == "result_call" && req.value.HasValue)
                        {
                            objLog.ResultCall = req.value.Value;
                        }
                        else if (req.step == "motobike_certificate" && req.value.HasValue)
                        {
                            objLog.MotobikeCertificate = req.value.Value;
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = 1
                                });
                            }
                            //Thêm vào bảng kịch bản telesale
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có đăng ký xe máy bản gốc hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                        }
                        _unitOfWork.LogResultAutoCallRepository.Insert(objLog);
                        _unitOfWork.Save();

                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tồn tại thông tin đơn vay");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/ResultAutoCallCancel Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        //Cập nhật kết quả step của KH gọi lên tổng đài
        //result:0 - Không đồng ý, 1 - đồng ý , 3 - không thao tác
        [HttpPost]
        [Route("result_autocall_callcenter")]
        public ActionResult<DefaultResponse<object>> ResultAutoCallCallCenter(ResultAutocallReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //LogRequest("ResultAutoCallCallCenter", JsonConvert.SerializeObject(req));

                if (string.IsNullOrEmpty(req.phone) || string.IsNullOrEmpty(req.step))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (!req.loanbriefId.HasValue)
                {
                    var loanbrief = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == req.phone
                             && (x.Status == (int)EnumLoanStatus.INIT || x.Status == (int)EnumLoanStatus.SALEADMIN_LOAN_DISTRIBUTING
                             || x.Status == (int)EnumLoanStatus.SYSTEM_TELESALE_DISTRIBUTING || x.Status == (int)EnumLoanStatus.TELESALE_ADVICE), null, false)
                                 .OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                    if (loanbrief != null && loanbrief.LoanBriefId > 0)
                        req.loanbriefId = loanbrief.LoanBriefId;
                }

                if (req.loanbriefId > 0)
                {
                    var logResult = _unitOfWork.LogResultAutoCallRepository.Query(x => x.LoanbriefId == req.loanbriefId, null, false).FirstOrDefault();
                    if (logResult != null)
                    {
                        var step = 0;
                        if (logResult.MotobikeCertificate == 1)
                            step++;
                        if (logResult.AreaSupport == 1)
                            step++;
                        if (logResult.BorrowMotobike == 1)
                            step++;

                        if (req.step == "motobike")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                BorrowMotobike = req.value
                            });
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = step + 1
                                });
                            }

                            //Thêm vào bảng kịch bản telesale
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }

                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "CallCenter",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có sử dụng xe máy hay không: {0}", req.value == EnumAutoCallValue.NoAccept.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "area")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                AreaSupport = req.value
                            });
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = step + 1
                                });
                            }
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode() || req.value == EnumAutoCallValue.NoAccept.GetHashCode())
                            {
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "CallCenter",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có ở khu vực HN hoặc HCM hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "connect_time")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                ConnectNextTime = req.value
                            });
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "result_call")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                ResultCall = req.value
                            });
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "motobike_certificate")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                MotobikeCertificate = req.value
                            });
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = step + 1
                                });
                            }
                            //Thêm vào bảng kịch bản telesale
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "CallCenter",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có đăng ký xe máy bản gốc hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Step không tồn tại");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        var objLog = new LogResultAutoCall();
                        objLog.LoanbriefId = req.loanbriefId;
                        objLog.CreatedAt = DateTime.Now;
                        objLog.CampaignId = req.campaignId;
                        if (req.step == "motobike" && req.value.HasValue)
                        {
                            objLog.BorrowMotobike = req.value.Value;
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = 1
                                });
                            }
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "CallCenter",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có sử dụng xe máy hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                        }
                        else if (req.step == "area" && req.value.HasValue)
                        {
                            objLog.AreaSupport = req.value.Value;
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = 1
                                });
                            }
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode() || req.value == EnumAutoCallValue.NoAccept.GetHashCode())
                            {
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "CallCenter",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có ở khu vực HN hoặc HCM hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                        }
                        else if (req.step == "connect_time" && req.value.HasValue)
                        {
                            objLog.ConnectNextTime = req.value.Value;
                        }
                        else if (req.step == "result_call" && req.value.HasValue)
                        {
                            objLog.ResultCall = req.value.Value;
                        }
                        else if (req.step == "motobike_certificate" && req.value.HasValue)
                        {
                            objLog.MotobikeCertificate = req.value.Value;
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = 1
                                });
                            }
                            //Thêm vào bảng kịch bản telesale
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "CallCenter",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có đăng ký xe máy bản gốc hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                        }
                        _unitOfWork.LogResultAutoCallRepository.Insert(objLog);
                        _unitOfWork.Save();

                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tồn tại thông tin đơn vay");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/ResultAutoCallCallCenter Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("ref_ekyc_motobike_registriation_certificate")]
        public ActionResult<DefaultResponse<object>> UpdateRefMotobikeRegistriationCertificate(RefMotobikeRegistriationCertificateReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(req.refcode))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var logLoanInfoAi = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.RefCode == req.refcode
                && x.ServiceType == (int)ServiceTypeAI.EkycMotorbikeRegistrationCertificate && x.IsCancel != 1, null, false).FirstOrDefault();
                if (logLoanInfoAi != null && logLoanInfoAi.Id > 0)
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                    logLoanInfoAi.Response = json;
                    logLoanInfoAi.UpdatedAt = DateTime.Now;
                    if (req.response_code == 200)
                        logLoanInfoAi.ResultFinal = "SUCCESS";
                    else
                        logLoanInfoAi.ResultFinal = "ERROR";
                    logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode();//có kết quả + không cần xử lý
                    _unitOfWork.LogLoanInfoAiRepository.Update(logLoanInfoAi);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/UpdateRefMotobikeRegistriationCertificate Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("proxy_pay_amount_ag")]
        public ActionResult<string> ProxyCiscoAG(ResultAutocallReq req)
        {
            try
            {
                if (!string.IsNullOrEmpty(req.phone) && !string.IsNullOrEmpty(req.step))
                {
                    var model = new RequestPayAmount()
                    {
                        Phone = req.phone,
                        NumberCard = req.step
                    };
                    var result = _agService.GetPayAmountForCustomerByCisco(model);
                    return Ok(result);
                }
                else
                {
                    return Ok("Vui lòng truyền thông tin sđt và cmnd");
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/ProxyCiscoAG Exception");
                return Ok("Xảy ra lỗi");
            }

        }


        [HttpPost]
        [Route("result_fraud_check")]
        public ActionResult<DefaultResponse<object>> UpdateResultFraudCheck(ResultFraudCheckModel req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (req.loan_brief_id == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var logLoanInfoAi = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.LoanbriefId == req.loan_brief_id && x.Status == 1
                                                            && x.ServiceType == (int)ServiceTypeAI.FraudDetection && x.IsCancel != 1, null, false).FirstOrDefault();
                if (logLoanInfoAi != null && logLoanInfoAi.Id > 0)
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                    logLoanInfoAi.Response = json;
                    logLoanInfoAi.UpdatedAt = DateTime.Now;
                    logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode();//có kết quả và không cần xử lý
                    _unitOfWork.LogLoanInfoAiRepository.Update(logLoanInfoAi);
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/UpdateResultFraudCheck Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        /// <summary>
        /// api nhận kết quả cuộc gọi forword phone về sdt cá nhân
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("result_autocall_forword")]
        public ActionResult<DefaultResponse<object>> ResultAutoCallForwordPhone(ResultAutocallReq req)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var json = JsonConvert.SerializeObject(req);
                LogRequest("ResultAutoCallForwordPhone ", json);

                if (string.IsNullOrEmpty(req.phone) || string.IsNullOrEmpty(req.step))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                
                if (req.loanbriefId > 0)
                {
                    var logResult = _unitOfWork.LogResultAutoCallRepository.Query(x => x.LoanbriefId == req.loanbriefId, null, false).FirstOrDefault();
                    if (logResult != null)
                    {
                        var step = 0;
                        if (logResult.MotobikeCertificate == 1)
                            step++;
                        if (logResult.AreaSupport == 1)
                            step++;
                        if (logResult.BorrowMotobike == 1)
                            step++;

                        if (req.step == "motobike")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                BorrowMotobike = req.value,
                                PhoneAgent = req.phone
                            });
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = step + 1
                                });
                            }

                            //Thêm vào bảng kịch bản telesale
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }

                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có sử dụng xe máy hay không: {0}", req.value == EnumAutoCallValue.NoAccept.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "area")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                AreaSupport = req.value,
                                PhoneAgent = req.phone
                            });
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = step + 1
                                });
                            }
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode() || req.value == EnumAutoCallValue.NoAccept.GetHashCode())
                            {
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có ở khu vực HN hoặc HCM hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "connect_time")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                ConnectNextTime = req.value,
                                PhoneAgent = req.phone
                            });
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "result_call")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                ResultCall = req.value,
                                PhoneAgent = req.phone
                            });
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else if (req.step == "motobike_certificate")
                        {
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == logResult.Id, x => new LogResultAutoCall()
                            {
                                UpdatedAt = DateTime.Now,
                                MotobikeCertificate = req.value,
                                PhoneAgent = req.phone
                            });
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = step + 1
                                });
                            }
                            //Thêm vào bảng kịch bản telesale
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có đăng ký xe máy bản gốc hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                            _unitOfWork.Save();
                            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Step không tồn tại");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        var objLog = new LogResultAutoCall();
                        objLog.LoanbriefId = req.loanbriefId;
                        objLog.CreatedAt = DateTime.Now;
                        objLog.CampaignId = req.campaignId;
                        objLog.PhoneAgent = req.phone;
                        if (req.step == "motobike" && req.value.HasValue)
                        {
                            objLog.BorrowMotobike = req.value.Value;
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = 1
                                });
                            }
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionUseMotobikeGo = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có sử dụng xe máy hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                        }
                        else if (req.step == "area" && req.value.HasValue)
                        {
                            objLog.AreaSupport = req.value.Value;
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = 1
                                });
                            }
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode() || req.value == EnumAutoCallValue.NoAccept.GetHashCode())
                            {
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có ở khu vực HN hoặc HCM hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                        }
                        else if (req.step == "connect_time" && req.value.HasValue)
                        {
                            objLog.ConnectNextTime = req.value.Value;
                        }
                        else if (req.step == "result_call" && req.value.HasValue)
                        {
                            objLog.ResultCall = req.value.Value;
                        }
                        else if (req.step == "motobike_certificate" && req.value.HasValue)
                        {
                            objLog.MotobikeCertificate = req.value.Value;
                            if (req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBrief()
                                {
                                    Step = 1
                                });
                            }
                            //Thêm vào bảng kịch bản telesale
                            if (req.value == EnumAutoCallValue.NoAccept.GetHashCode() || req.value == EnumAutoCallValue.Accepted.GetHashCode())
                            {
                                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == req.loanbriefId))
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == req.loanbriefId, x => new LoanBriefQuestionScript()
                                    {
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted
                                    });
                                }
                                else
                                {
                                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                                    {
                                        LoanBriefId = req.loanbriefId,
                                        QuestionMotobikeCertificate = req.value == (int)EnumAutoCallValue.Accepted,
                                        TypeJob = 0,
                                        ChooseCareer = 0
                                    });
                                }
                                _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                                {
                                    FullName = "Auto Call",
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    Note = string.Format("Khách hàng có đăng ký xe máy bản gốc hay không: {0}", req.value == EnumAutoCallValue.Accepted.GetHashCode() ? "Có" : "Không"),
                                    Status = 1,
                                    CreatedTime = DateTime.Now,
                                    ShopId = 3703,
                                    ShopName = "Tima",
                                    LoanBriefId = req.loanbriefId
                                });
                            }
                        }
                        _unitOfWork.LogResultAutoCallRepository.Insert(objLog);
                        _unitOfWork.Save();

                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tồn tại thông tin đơn vay");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/ResultAutoCallForwordPhone Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        private void CallToErp(Models.Erp.PushDeviceReq req)
        {
            try
            {
                if (_erpService.UpdateDeviceStatus(req))
                {
                    //Thêm comment
                    _unitOfWork.LoanBriefNoteRepository.Insert(new LoanBriefNote()
                    {
                        FullName = "Auto System",
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        Note = string.Format("Gọi kích hoạt thiết bị sang ERP"),
                        Status = 1,
                        CreatedTime = DateTime.Now,
                        ShopId = 3703,
                        ShopName = "Tima",
                        LoanBriefId = req.loanId
                    });
                    _unitOfWork.Save();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Webhook/CallToErp Exception");
            }
        }
        private void LogRequest(string name, string json)
        {
            try
            {
                Log.Information(string.Format("{0}: {1}", name, json));
            }
            catch
            {

            }
        }
    }
}