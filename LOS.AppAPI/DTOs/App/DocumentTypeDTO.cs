﻿using LOS.DAL.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.AppAPI.DTOs.App
{

    public class DocumentTypeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte? IsEnable { get; set; }
        public int? ProductId { get; set; }
        public int? MobileUpload { get; set; }
    }

    public class DocumentTypeDetail : DocumentTypeDTO
    {
        public static Expression<Func<DocumentType, DocumentTypeDetail>> ProjectionDetail
        {
            get
            {
                return x => new DocumentTypeDetail()
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsEnable = x.IsEnable,
                    ProductId = x.ProductId,
                    MobileUpload = x.MobileUpload.GetValueOrDefault(0)
                };
            }
        }
    }


}
