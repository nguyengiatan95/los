﻿using LOS.DAL.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.AppAPI.DTOs.App
{

    public class InterestToolDTO
    {
        [JsonProperty("Result")]
        public long Result { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("Data")]
        public DataInterest Data { get; set; }
    }

    public partial class DataInterest
    {
        [JsonProperty("LstPaymentSchedule")]
        public LstPaymentSchedule[] LstPaymentSchedule { get; set; }

        [JsonProperty("FeeInsurrance")]
        public long FeeInsurrance { get; set; }
    }

    public partial class LstPaymentSchedule
    {
        [JsonProperty("FromDate")]
        public DateTimeOffset FromDate { get; set; }

        [JsonProperty("ToDate")]
        public DateTimeOffset ToDate { get; set; }

        [JsonProperty("PayDate")]
        public DateTimeOffset PayDate { get; set; }

        [JsonProperty("MoneyOriginal")]
        public long MoneyOriginal { get; set; }

        [JsonProperty("MoneyInterest")]
        public long MoneyInterest { get; set; }

        [JsonProperty("MoneyService")]
        public long MoneyService { get; set; }

        [JsonProperty("MoneyConsultant")]
        public long MoneyConsultant { get; set; }

        [JsonProperty("MoneyFinePayEarly")]
        public long MoneyFinePayEarly { get; set; }

        [JsonProperty("OutStandingBalance")]
        public long OutStandingBalance { get; set; }

        [JsonProperty("TotalMoneyNeedPayment")]
        public long TotalMoneyNeedPayment { get; set; }

        [JsonProperty("TotalMoneyNeedClosePayment")]
        public long TotalMoneyNeedClosePayment { get; set; }
    }

}
