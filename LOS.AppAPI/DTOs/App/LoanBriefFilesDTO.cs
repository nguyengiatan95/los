﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.AppAPI.DTOs.LMS
{
    public class LoanBriefFilesDTO
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public DateTime? CreateAt { get; set; }
        public string FilePath { get; set; }
        public int? UserId { get; set; }
        public int? Status { get; set; }
        public int? TypeId { get; set; }
        public int? S3status { get; set; }
        public string LinkImgOfPartner { get; set; }
        public int? TypeFile { get; set; }
        public int? Synchronize { get; set; }
        public string DomainOfPartner { get; set; }
        public int? DeleteStatus { get; set; }
        public int? MecashId { get; set; }

        public string TypeName { get; set; }
        public string FileThumb { get; set; }
    }


    public class LoanBriefFilesDetail : LoanBriefFilesDTO
    {
        public static Expression<Func<LoanBriefFiles, LoanBriefFilesDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefFilesDetail()
                {
                    Id = x.Id,
                    LoanBriefId = x.LoanBriefId,
                    CreateAt = x.CreateAt,
                    FilePath = x.FilePath,
                    UserId = x.UserId,
                    Status = x.Status,
                    TypeId = x.TypeId,
                    TypeFile = x.TypeFile,
                    MecashId = x.MecashId,
                    S3status = x.S3status,
                    FileThumb = x.FileThumb,
                    TypeName = x.Type != null ? x.Type.Name : ""
                };
            }
        }
    }

    public class LMSLoanBriefFilesDTO
    {
        public int? TypeId { get; set; }
        public string TypeName { get; set; }
        public string FilePath { get; set; }
        public string FileThumb { get; set; }
        public int MecashId { get; set; }
        public int? S3status { get; set; }
        public string LinkImgOfPartner { get; set; }
        public string DomainOfPartner { get; set; }
        public int? TypeFile { get; set; }

    }

    public class LMSLoanBriefFilesDetail : LMSLoanBriefFilesDTO
    {
        public static Expression<Func<LoanBriefFiles, LMSLoanBriefFilesDetail>> ProjectionDetail
        {
            get
            {
                return x => new LMSLoanBriefFilesDetail()
                {
                    TypeId = x.TypeId,
                    TypeName = x.Type != null ? x.Type.Name : "",
                    MecashId = x.MecashId.GetValueOrDefault(),
                    S3status = x.S3status,
                    FilePath = x.FilePath,
                    LinkImgOfPartner = x.LinkImgOfPartner,
                    DomainOfPartner = x.DomainOfPartner,
                    TypeFile = x.TypeFile,
                    FileThumb = x.FileThumb
                    
                };
            }
        }
    }

    public class LMSLoanBriefFilesDetail2
    {
        public int? TypeId { get; set; }
        public string TypeName { get; set; }

        public List<LstFilePath> LstFilePath { get; set; }
    }

    public partial class LstFilePath
    {
        public string FileThumb { get; set; }
        public string FilePath { get; set; }
        public int? TypeFile { get; set; }
    }
}
