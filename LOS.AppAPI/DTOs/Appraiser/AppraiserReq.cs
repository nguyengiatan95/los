﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.Appraiser
{
    public class AppraiserReq
    {
        [Required]
        public int LoanbriefId { get; set; }
        //[Required]
        //public int ProductCreditId { get; set; }
        //[Required]
        public int TypeAppraiser { get; set; }
        [Required]
        public int Status { get; set; }
        [Required]
        public int IsWord { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
    }
}
