﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.Appraiser
{
    public class GroupName
    {
        public string Name { get; set; }
        public List<GroupChildren> ListGroupChild { get; set; }

    }
    public class GroupChildren
    {
        public string Note { get; set; }
        public string GroupChildName { get; set; }
        public bool IsRequire { get; set; }
        public List<Question> ListQuestion { get; set; }
    }
    public class Question
    {
        public string QuestionName { get; set; }
        public List<GroupAnswer> ListGroupAnswer { get; set; }
        public int IsComment { get; set; }
        public int IsEdit { get; set; }
        public string Comment { get; set; }
    }
    public class GroupAnswer
    {
        public string TitleAnswer { get; set; }
        public string TypeInput { get; set; }
        public List<Answer> ListAnswer { get; set; }
    }
    public class Answer
    {
        public string AnswerName { get; set; }
        public int IsCorrect { get; set; }
        public int IsEdit { get; set; }
    }
}
