﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.Appraiser
{
    public class CheckListQuestionReq
    {
        [Required]
        public int LoanBriefId { get; set; }
        [Required]
        public List<GroupName> ContentCheckList { get; set; }
    }
}
