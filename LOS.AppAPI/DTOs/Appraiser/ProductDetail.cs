﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.Appraiser
{
    public class ProductDTO
    {
        public int Id { get; set; }
        public int IdBrand { get; set; }
        public int IdType { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public long? Price { get; set; }
        public int? Year { get; set; }
        public long? PriceCurrent { get; set; }
        public int? BrakeType { get; set; }
        public int? RimType { get; set; }
    }

    public class ProductDetail : ProductDTO
    {
        public static Expression<Func<Product, ProductDetail>> ProjectionDetail
        {
            get
            {
                return x => new ProductDetail()
                {
                    Id = x.Id,
                    IdBrand = x.BrandProductId,
                    IdType = x.ProductTypeId,
                    Name = x.Name,
                    FullName = x.FullName,
                    ShortName = x.ShortName,
                    BrakeType = x.BrakeType,
                    RimType = x.RimType,
                    Price = x.Price,
                    Year = x.Year
                };
            }
        }
    }

    public class ProductPercentReductionDTO
    {
        public int Id { get; set; }
        public int IdBrand { get; set; }
        public int IdType { get; set; }
        public int? CountYear { get; set; }
        public int? PercentReduction { get; set; }
    }

    public class ProductPercentReductionDetailAPI : ProductPercentReductionDTO
    {
        public static Expression<Func<ProductPercentReduction, ProductPercentReductionDetailAPI>> ProjectionDetail
        {
            get
            {
                return x => new ProductPercentReductionDetailAPI()
                {
                    Id = x.Id,
                    IdBrand = x.BrandProductId,
                    IdType = x.ProductTypeId,
                    CountYear = x.CountYear,
                    PercentReduction = x.PercentReduction
                };
            }
        }
    }
}
