﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.Appraiser
{
    public class ProductReviewDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int? Status { get; set; }       
        public bool? State { get; set; }
        public bool? IsCancel { get; set; }
        public int? IdType { get; set; }
        public bool? IsCheck { get; set; }
        public long? MoneyDiscount { get; set; }
        public decimal? PecentDiscount { get; set; }
    }
    public class ProductReviewDetailAPI: ProductReviewDTO
    {
        public static Expression<Func<ProductReview, ProductReviewDetailAPI>> ProjectionDetail
        {
            get
            {
                return x => new ProductReviewDetailAPI()
                {
                    Id = x.Id,
                    Name = x.Name,
                    ParentId = x.ParentId,
                    Status = x.Status,
                    State = x.State,
                    IsCancel = x.IsCancel,
                    IdType = (x.ProductReviewDetail != null && x.ProductReviewDetail.Count > 0) ? x.ProductReviewDetail.First().ProductTypeId : null,
                    IsCheck = (x.ProductReviewResult != null && x.ProductReviewResult.Count > 0) ? x.ProductReviewResult.First().IsCheck : null,
                    MoneyDiscount =0,
                };
            }
        }

    }

   

}
