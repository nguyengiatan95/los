﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.Appraiser
{
    public class ProductReviewReq
    {
        //[Required]
        //public string fullName { get; set; }
        //[Required]
        //public int groupId { get; set; }
        [Required]
        public int loanCreditId { get; set; }
        [Required]
        //public int productId { get; set; }
        //[Required]
        //public int userId { get; set; }
        //[Required]
        //public int shopId { get; set; }
        public List<ProductReviewResultReq> ListProductResultReview { get; set; }
    }

    public class ProductReviewResultReq
    {
        public bool IsCancel { get; set; }
        public bool IsCheck { get; set; }
        public int ProductReviewId { get; set; }
    }
}
