﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.ChuyenHoaDTO
{
    public class CommentDTO
    {
        public int LoanBriefNoteId { get; set; }
        public int? LoanBriefId { get; set; }
        public int? Type { get; set; }

        public int? UserId { get; set; }
        public string Note { get; set; }
        public int? Status { get; set; }

        public string FullName { get; set; }
        public string ShopName { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
    }

    public class CommentDetail : CommentDTO
    {
        public static Expression<Func<LoanBriefNote, CommentDetail>> ProjectionDetail
        {
            get
            {
                return x => new CommentDetail()
                {
                    LoanBriefNoteId = x.LoanBriefNoteId,
                    LoanBriefId = x.LoanBriefId,
                    Type = x.Type,
                    Note = x.Note,
                    Status = x.Status,
                    UserId = x.UserId,
                    FullName = x.FullName,
                    ShopName = x.ShopName,
                    CreatedTime = x.CreatedTime
                };
            }
        }
    }

    public class CommentReq
    {
        public int LoanbriefId { get; set; }
        public string Note { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
    }
}
