﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.ChuyenHoaDTO
{
    public class LatLngDTO
    {
        public string LatlngAddress { get; set; }
        public string GoogleMapAddress { get; set; }
        public string LatlngCompany { get; set; }
        public string GoogleMapCompanyAddress { get; set; }
    }
    public class LatLngDetail : LatLngDTO
    {
        public static Expression<Func<LoanBrief, LatLngDetail>> ProjectionDetail
        {
            get
            {
                return x => new LatLngDetail()
                {
                    LatlngAddress = x.LoanBriefResident != null ? x.LoanBriefResident.AddressLatLng : "",
                    GoogleMapAddress = x.LoanBriefResident != null ? x.LoanBriefResident.AddressGoogleMap : "",
                    LatlngCompany = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyAddressLatLng : "",
                    GoogleMapCompanyAddress = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyAddressGoogleMap : ""                    
                };
            }
        }
    }
}
