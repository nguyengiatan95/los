﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.ChuyenHoaDTO
{

    public class ReportTelesale
    {
        public int LoanCreditID { get; set; }
        public string CustomerName { get; set; }
        public DateTime CreateDate { get; set; }
        public decimal TotalMoney { get; set; }
        public string Address { get; set; }
        public int SupportLastID { get; set; }
        public int Status { get; set; }
        public int CityID { get; set; }
        public int DistrictID { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string ShopName { get; set; }
        public string ProductName { get; set; }
        public string SupportName { get; set; }
        public string SupportUserName { get; set; }
        public DateTime? CancelDateLoanCredit { get; set; }
        public string CancelUserName { get; set; }
        public int? UserCancelId { get; set; }
        public DateTime? CreateLoan { get; set; }
        public DateTime? FirstTimeHubReceived { get; set; }
        public DateTime? FirstTimeLenderCareReceived { get; set; }
        public DateTime? FirstTimeAccountantReceived { get; set; }
        public DateTime? FirstTimeAccountantDisbursement { get; set; }
        public DateTime? FirstTimeTDPDReceived { get; set; }
        public DateTime? TelesalesPushAt { get; set; }
        public DateTime? HubPushAt { get; set; }
        public DateTime? CoordinatorPushAt { get; set; }
        public int? ReasonID { get; set; }
        public int? TotalWaitCustomerPrepare { get; set; } = 0;
        public int? TotalWaitHubDistributing { get; set; } = 0;
        public int? TotalWaitAppraiser { get; set; } = 0;
        public int? TotalHubWaitApprove { get; set; } = 0;
        public decimal? TotalLoanAmount { get; set; } = 0;
        public decimal LoanAmount { get; set; } = 0;
    }

    public class ReportTelesaleDetail : ReportTelesale
    {
        public static Expression<Func<LoanBrief, ReportTelesaleDetail>> ProjectionDetail
        {
            get
            {
                return x => new ReportTelesaleDetail()
                {
                    LoanCreditID = x.LoanBriefId,
                    CustomerName = x.FullName,
                    CreateDate = x.CreatedTime.Value.DateTime,
                    TotalMoney = x.LoanAmount.GetValueOrDefault(),
                    Address = x.LoanBriefResident != null ? x.LoanBriefResident.Address : "",
                    SupportLastID = x.BoundTelesaleId.GetValueOrDefault(),
                    SupportName = x.BoundTelesale != null ? x.BoundTelesale.FullName : "",
                    SupportUserName = x.BoundTelesale != null ? x.BoundTelesale.Username : "",
                    Status = x.Status.GetValueOrDefault(),
                    CityID = x.ProvinceId.GetValueOrDefault(),
                    DistrictID = x.DistrictId.GetValueOrDefault(),
                    CityName = x.Province != null ? x.Province.Name : "",
                    DistrictName = x.District != null ? x.District.Name : "",
                    ShopName = x.Hub != null ? x.Hub.Name : "",
                    ProductName = x.Product != null ? x.Product.Name : "",
                    CancelDateLoanCredit = x.LoanBriefCancelAt,
                    ReasonID = x.ReasonCancel,
                    UserCancelId = x.LoanBriefCancelBy,
                    CreateLoan = x.DisbursementAt,
                    CancelUserName = x.LoanBriefCancelByNavigation != null? x.LoanBriefCancelByNavigation.Username : "",
                    FirstTimeAccountantDisbursement = x.DisbursementAt,
                    TelesalesPushAt = x.TelesalesPushAt,
                    HubPushAt = x.HubPushAt,
                    CoordinatorPushAt = x.CoordinatorPushAt,
                    LoanAmount = x.LoanAmount??0
                };
           }
        }
    }
}
