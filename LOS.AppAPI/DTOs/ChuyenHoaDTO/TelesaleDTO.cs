﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.ChuyenHoaDTO
{
    public class TelesaleDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public int? IpPhone { get; set; }
    }
    public class TelesaleDetail : TelesaleDTO
    {
        public static Expression<Func<User, TelesaleDetail>> ProjectionDetail
        {
            get
            {
                return x => new TelesaleDetail()
                {
                    Id = x.UserId,
                    UserName = x.Username,
                    FullName = x.FullName,
                    IpPhone = x.Ipphone
                };
            }
        }
    }

    public class StafHubDetail : TelesaleDTO
    {
        public string CityName { get; set; }
        public string HubName { get; set; }

    }

    public class HubByAsm
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public List<HubByAsmItem> LstHub { get; set; }

        public static Expression<Func<User, HubByAsm>> ProjectionDetail
        {
            get
            {
                return x => new HubByAsm()
                {
                    UserId = x.UserId,
                    UserName = x.Username,
                    FullName = x.FullName,
                    LstHub = x.UserShop != null ? x.UserShop
                    .Select(r => new HubByAsmItem()
                    {
                        ShopId = r.Shop.ShopId,
                        Name = r.Shop.Name
                    }).ToList() : null,
                };
            }
        }
    }

    public class HubByAsmItem
    {
        public int ShopId { get; set; }
        public string Name { get; set; }
    }
}
