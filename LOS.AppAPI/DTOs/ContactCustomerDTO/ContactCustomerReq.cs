﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.ContactCustomerDTO
{
    public class ContactCustomerReq
    {
        public int page { get; set; }
        public int pageSize { get; set; }
        public string phone { get; set; }
        public int province { get; set; }
        public string name { get; set; }
    }
}
