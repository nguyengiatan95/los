﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.DictionaryDTO
{
    public class BankDTO
    {
        public int BankId { get; set; }
        public string Name { get; set; }
        public string BankCode { get; set; }
        public string NapasCode { get; set; }
        public int? Type { get; set; }
        public int? BankValue { get; set; }
    }

    public class BankDetail: BankDTO
    {
        public static Expression<Func<Bank, BankDetail>> ProjectionDetail
        {
            get
            {
                return x => new BankDetail()
                {
                    BankId = x.BankId,
                    Name = x.Name,
                    BankCode = x.BankCode,
                    NapasCode = x.NapasCode,
                    BankValue = x.BankValue
                };
            }
        }

    }

}
