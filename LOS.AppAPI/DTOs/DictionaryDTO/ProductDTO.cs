﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.DictionaryDTO
{
    public class ProductDTO
    {
        public int Id { get; set; }
        public int IdBrand { get; set; }
        public int IdType { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public long? Price { get; set; }
        public int? Year { get; set; }
    }

    public class ProductDetail : ProductDTO
    {
      
        public static Expression<Func<Product, ProductDetail>> ProjectionDetail
        {
            get
            {
                return x => new ProductDetail()
                {
                    Id = x.Id,
                    IdBrand = x.BrandProductId,
                    IdType = x.ProductTypeId,
                    Name = x.Name,
                    FullName = x.FullName,
                    Price = x.Price,
                    Year = x.Year
                };
            }
        }


    }
}
