﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.DictionaryDTO
{
    public class ProvinceDTO
    {
        public int ProvinceId { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? Priority { get; set; }
    }

    public class ProvinceDetail : ProvinceDTO
    {
        public static Expression<Func<Province, ProvinceDetail>> ProjectionDetail
        {
            get
            {
                return x => new ProvinceDetail()
                {
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    Name = x.Name,
                    Priority = x.Priority,
                    ProvinceId = x.ProvinceId,
                    Type = x.Type
                };
            }
        }
    }

    public class DistrictDTO
    {
        public int DistrictId { get; set; }
        public int? ProvinceId { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? Priority { get; set; }
    }

    public class DistrictDetail : DistrictDTO
    {
        public static Expression<Func<District, DistrictDetail>> ProjectionDetail
        {
            get
            {
                return x => new DistrictDetail()
                {
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    Name = x.Name,
                    Priority = x.Priority,
                    ProvinceId = x.ProvinceId,
                    Type = x.Type,
                    DistrictId = x.DistrictId
                };
            }
        }
    }

    public class WardDTO
    {
        public int WardId { get; set; }
        public int? DistrictId { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? Priority { get; set; }
    }

    public class WardDetail : WardDTO
    {
        public static Expression<Func<Ward, WardDetail>> ProjectionDetail
        {
            get
            {
                return x => new WardDetail()
                {
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    Name = x.Name,
                    Priority = x.Priority,
                    WardId = x.WardId,
                    Type = x.Type,
                    DistrictId = x.DistrictId
                };
            }
        }
    }
}
