﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.DictionaryDTO
{
    public class SelectItem
    {
        public SelectItem()
        {

        }
        public SelectItem(string _id, string _value)
        {
            Id = _id;
            Value = _value;
        }
        public string Id { get; set; }
        public string Value { get; set; }
    }
}
