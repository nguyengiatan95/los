﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using System;
using System.Linq.Expressions;

namespace LOS.AppAPI.DTOs.DirectSale
{
    public class LoanInfoDrsInfo
    {
        public int LoanbriefId { get; set; }
        public string FullName { get; set; }
        public decimal? LoanAmount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ReceivedAt { get; set; }
        public int LoanTime { get; set; }
        public string Address { get; set; }
        public int ProductCreditId { get; set; }
        public string ProductCredit { get; set; }
        public int? AddressFieldSurveyStatus { get; set; }
        public int? CompanyFieldSurveyStatus { get; set; }
        public string LoanStatus { get; set; }
        public string LoanDetailStatus { get; set; }
        public bool IsVehicleAppraisal { get; set; }
        public string LinkCall { get; set; }
        public bool IsEdit { get; set; } = false;
        public bool IsPush { get; set; } = false;
        public bool IsBack { get; set; } = false;
        public bool IsCancel { get; set; } = false;
        public bool IsChangeStatusDetail { get; set; } = false;
        public bool IsChangeStaff { get; set; } = false;
        public bool IsPushException { get; set; } = false;
        public bool IsRegetLocation { get; set; } = false;
        public bool IsCreatedCarChild { get; set; } = false;
        public bool IsReEsign { get; set; } = false;
        public bool IsGetInsuranceInfo { get; set; } = false;
        public bool IsCheckMomo { get; set; } = false;
        public bool IsLocate { get; set; } 
        public DateTime? HubEmployeeCallFirst { get; set; } 
        public string HubEmployeeName { get; set; }
        public int MotobikeId { get; set; }
        public string Phone { get; set; }
    }

    public class LoanInfoDrsDto
    {
        public int LoanbriefId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public decimal? LoanAmount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ReceivedAt { get; set; }
        public int? LoanTime { get; set; }
        public string Address { get; set; }
        public int? ProductCreditId { get; set; }
        public string ProductCredit { get; set; }
        public int? AddressFieldSurveyStatus { get; set; }
        public int? CompanyFieldSurveyStatus { get; set; }
        public int? Status { get; set; }
        public int? LoanDetailStatus { get; set; }
        public int? HubEmployeeId { get; set; }
        public int? CreatedBy { get; set; }
        public int? BoundTelesaleId { get; set; }
        public int? PlatformType { get; set; }
        public int? ReMarketingLoanBriefId { get; set; }
        public int? TypeRemarketing { get; set; }
        public int? EsignState { get; set; }
        public bool? IsTrackingLocation { get; set; }
        public bool IsVehicleAppraisal { get; set; }
        public bool IsLocate { get; set; }
        public string NationalCard { get; set; }
        public DateTime? Dob { get; set; }
        public DateTime? HubEmployeeCallFirst { get; set; }
        public string HubEmployeeName { get; set; }
        public int? MotobikeId { get; set; }

    }
    public class LoanInfoDrsForHub : LoanInfoDrsDto
    {

        public static Expression<Func<LoanBrief, LoanInfoDrsForHub>> ProjectionDetail
        {
            get
            {
                return x => new LoanInfoDrsForHub()
                {
                    LoanbriefId = x.LoanBriefId,
                    LoanAmount = x.LoanAmount,
                    Phone = x.Phone,
                    FullName = x.FullName,
                    LoanTime = x.LoanTime,
                    CreatedAt = x.CreatedTime.Value.DateTime,
                    Address = x.LoanBriefResident != null ? x.LoanBriefResident.Address : string.Empty,
                    ProductCreditId = x.ProductId,
                    ProductCredit = x.Product != null ? x.Product.Name : string.Empty,
                    Status = x.Status,
                    AddressFieldSurveyStatus = x.EvaluationHomeState,
                    CompanyFieldSurveyStatus = x.EvaluationCompanyState,
                    LoanDetailStatus = x.LoanStatusDetail,
                    IsVehicleAppraisal = (x.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                    || x.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                    || x.ProductId == (int)EnumProductCredit.MotorCreditType_KGT),
                    HubEmployeeId = x.HubEmployeeId,
                    BoundTelesaleId = x.BoundTelesaleId,
                    PlatformType = x.PlatformType,
                    IsLocate = x.IsLocate.GetValueOrDefault(false),
                    IsTrackingLocation = x.IsTrackingLocation,
                    ReMarketingLoanBriefId = x.ReMarketingLoanBriefId,
                    TypeRemarketing = x.TypeRemarketing,
                    EsignState = x.EsignState,
                    NationalCard = x.NationalCard,
                    Dob = x.Dob,
                    CreatedBy = x.CreateBy,
                    HubEmployeeCallFirst = x.HubEmployeeCallFirst,
                    HubEmployeeName = x.HubEmployee != null ? x.HubEmployee.FullName : string.Empty,
                    MotobikeId = x.LoanBriefProperty != null ? x.LoanBriefProperty.ProductId : null
                };
            }
        }
    }
}
