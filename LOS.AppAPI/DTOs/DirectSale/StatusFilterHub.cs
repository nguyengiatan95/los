﻿namespace LOS.AppAPI.DTOs.DirectSale
{
    public class StatusFilterHub
    {
        public string Name { get; set; }
        public int FilterId { get; set; }
        public int TotalLoan { get; set; }
    }

    public class CheckCusstomer
    {
        public int LoanBriefId { get; set; }
        public string ProductName { get; set; }
        public int TypeLoanSupport { get; set; }
    }
}
