﻿namespace LOS.AppAPI.DTOs
{
    public class UserInfoToken
    {
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public int HubId { get; set; }
    }
}
