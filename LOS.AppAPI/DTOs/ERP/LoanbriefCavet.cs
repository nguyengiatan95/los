﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using System;
using System.Linq.Expressions;

namespace LOS.AppAPI.DTOs.ERP
{
    public class LoanbriefCavet
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public int? CodeId { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public int? Status { get; set; }
        public DateTime? DisbursementAt { get; set; }
        public bool? IsLocate { get; set; }
        public int? DeviceStatus { get; set; }
        public string ProductName { get; set; }
    }

    public class LoanbriefCavetDetail : LoanbriefCavet
    {
        public static Expression<Func<LoanBrief, LoanbriefCavetDetail>> Projection
        {
            get
            {
                return x => new LoanbriefCavetDetail
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    CodeId = x.CodeId,
                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    Status = x.Status,
                    DisbursementAt = x.DisbursementAt,
                    IsLocate = x.IsLocate,
                    DeviceStatus = x.DeviceStatus,
                    ProductName = x.Product != null && x.Product.Name != null ? x.Product.Name : "",

                };
            }
        }
    }

    public class LoanbriefCavetViewApi
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public int? CodeId { get; set; }
        public decimal? LoanAmount { get; set; }
        public string LoanTime { get; set; }
        public string StatusName { get; set; }
        public DateTime? DisbursementAt { get; set; }
        public bool? IsLocate { get; set; }
        public string DeviceStatus { get; set; }
        public string ProductName { get; set; }

    }

    public class LoanBriefStatus
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public DateTimeOffset? CreateTime { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public string StatusName { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefStatus>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefStatus
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    ProvinceName = x.Province != null ? x.Province.Name : null,
                    DistrictName = x.District != null ? x.District.Name : null,
                    CreateTime = x.CreatedTime,
                    StatusName = x.Status.HasValue ? Description.GetDescription((EnumLoanStatus)x.Status) : null,
                };
            }
        }
    }
}
