﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Linq;
using LOS.Common.Extensions;

namespace LOS.AppAPI.DTOs.LMS
{
    public class LMSLoanBriefDTO
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public int? ProductId { get; set; }
        public int? RateTypeId { get; set; }
        public string Phone { get; set; }
        public int? Gender { get; set; }
        public DateTime? DOB { get; set; }
        public string NationalCard { get; set; }
        public DateTime? NationalCardDate { get; set; }
        public string NationCardPlace { get; set; }
        public decimal? LoanAmount { get; set; }
        public decimal? LoanAmountFinal { get; set; }
        public int? LoanTime { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public double? ConsultantRate { get; set; }
        public double? ServiceRate { get; set; }
        public double? LoanRate { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? WardId { get; set; }
        public string WardName { get; set; }
        public string UtmSource { get; set; }
        public string UtmMedium { get; set; }
        public string UtmCampaign { get; set; }
        public string UtmTerm { get; set; }
        public string UtmContent { get; set; }
        public int? ScoreCredit { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTimeOffset? UpdatedTime { get; set; }
        public int? Status { get; set; }
        public int? AffCode { get; set; }
        public int? BoundTelesaleId { get; set; }
        public int? AffStatus { get; set; }
        public string LoanBriefComment { get; set; }
        public DateTime? ScheduleTime { get; set; }
        public DateTime? LoanBriefCancelAt { get; set; }
        public int? LoanBriefCancelBy { get; set; }
        public DateTime? TelesalesPushAt { get; set; }
        public int? PipelineState { get; set; }
        public int? ActionState { get; set; }
        public int? LenderId { get; set; }
        public int? CoordinatorUserId { get; set; }
        public int? HubEmployeeId { get; set; }
        //public bool? IsLocate { get; set; } = false;
        public string DeviceId { get; set; }
        public int? Frequency { get; set; }

        public int? CustomerId { get; set; }
        public int? BankId { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }

        public bool? BuyInsurenceCustomer { get; set; }
        public string ProductName { get; set; }

        public int ShopId { get; set; }
        public string ShopName { get; set; }
        public string Address { get; set; }

        public int ApproveId { get; set; }
        public string ApproveName { get; set; }
        public string ApprovePhone { get; set; }
        public int? TypeInsurence { get; set; }

        public string HouseOldCityName { get; set; }
        public string HouseOldDistrictName { get; set; }
        public string HouseOldWardName { get; set; }
        public string HouseOldAddress { get; set; }
        public int? DisbursementBy { get; set; }

        public bool? IsLock { get; set; }
        public bool? BackToLenderCare { get; set; }
        public DateTime? LenderReceivedDate { get; set; }
        public string LenderName { get; set; }
        public bool BuyInsuranceLender { get; set; }

        public int? AutoDistributeState { get; set; }

    }
    public class LMSDisbursementWaitingDTO
    {
        public int LoanBriefId { get; set; }
        public int? Status { get; set; }
        public decimal? LoanAmount { get; set; }
        public decimal? LoanAmountFinal { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? RateTypeId { get; set; }
        public string RateTypeName { get; set; }
        public int? PlatformType { get; set; }
        public string PlatformTypeName { get; set; }
        public int? LoanPurpose { get; set; }
        public string LoanPurposeName { get; set; }
        public bool? IsCheckCic { get; set; }
        public int? ScoreCredit { get; set; }
        public bool? IsTrackingLocation { get; set; }
        public int? CountCall { get; set; }
        public string Email { get; set; }
        public int? LoanTime { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        //Thông tin doanh nghiệp
        public int? TypeLoanBrief { get; set; }
        public string TypeLoanBriefName { get; set; }
        public string BusinessCompanyName { get; set; }
        //Ngành nghề kinh doanh
        public string CareerBusiness { get; set; }
        //Ngày cấp giấy CNĐKD/CNĐKND
        public DateTime? BusinessCertificationDate { get; set; }
        //Nơi cấp giấy CNĐKD/CNĐKND
        public string BusinessCertificationAddress { get; set; }
        //Trụ sở đăng ký kinh doanh
        public string HeadOffice { get; set; }
        //Địa chỉ kinh doanh hiện tại
        public string BusinessAddress { get; set; }
        //Cổ đông góp vốn nhiều nhất
        public string CompanyShareholder { get; set; }
        //Ngày sinh
        public DateTime? BirthdayShareholder { get; set; }
        //CMT
        public string CardNumberShareholder { get; set; }
        //Ngày Cấp
        public DateTime? CardNumberShareholderDate { get; set; }
        //Nơi sinh
        public string PlaceOfBirthShareholder { get; set; }

        //Khách hàng
        public int? CustomerId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public int? Gender { get; set; }
        public DateTime? DOB { get; set; }
        public string NationalCard { get; set; }
        public DateTime? NationalCardDate { get; set; }
        public string NationCardPlace { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? WardId { get; set; }
        public string WardName { get; set; }
        public string CustomerAddress { get; set; }
        public string FacebookAddress { get; set; }

        public int? LenderId { get; set; }
        public int? Frequency { get; set; }
        //ĐÃ có gia đình hay chưa
        public int? IsMerried { get; set; }
        //Có mấy cháu
        public int? NumberBaby { get; set; }
        //Đang sống cùng ai
        public int? LivingWith { get; set; }
        public string LivingName { get; set; }
        //Thời gian cư trú
        public int? LivingTime { get; set; }

        public string LivingTimeName { get; set; }
        //Hình thức cư trú
        public int? ResidentType { get; set; }
        public string ResidentTypeName { get; set; }

        public double? RatePercent { get; set; }

        public string InsurenceNumber { get; set; }
        public string InsurenceNote { get; set; }

        //Thông tin Bank
        public int? ReceivingMoneyType { get; set; }
        public string ReceivingMoneyTypeName { get; set; }
        public int? BankId { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }
        //Thông tin công việc
        public int? JobId { get; set; }
        public string JobTitle { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyTaxCode { get; set; }
        public string CompanyProvinceName { get; set; }
        public string CompanyDistrictName { get; set; }
        public string CompanyWardName { get; set; }
        public decimal? TotalIncome { get; set; }
        public string DescriptionJob { get; set; }
        public int? ImcomeType { get; set; }

        public bool? BuyInsurenceCustomer { get; set; }

        //Hub
        public int ShopId { get; set; }
        public string ShopName { get; set; }
        //Thông tin thẩm định
        public int ApproveId { get; set; }
        public string ApproveName { get; set; }
        public string ApprovePhone { get; set; }
        public int? TypeInsurence { get; set; }

        public int? HouseOldCityId { get; set; }
        public string HouseOldCityName { get; set; }
        public int? HouseOldDistrictId { get; set; }
        public string HouseOldDistrictName { get; set; }
        public int? HouseOldWardId { get; set; }
        public string HouseOldWardName { get; set; }
        public string HouseOldAddress { get; set; }
        public int? TypeDisbursement { get; set; }

        public bool? IsLock { get; set; }
        public DateTime? LenderReceivedDate { get; set; }


        //Thông tin tài sản
        public bool? IsLocate { get; set; }
        public int? BrandId { get; set; }
        public string BrandName { get; set; }

        //Thông tin thiết bị định vị
        public string DeviceId { get; set; }
        public int? DeviceStatus { get; set; }

        public string CompanyAddressGoogleMap { get; set; }
        public string AddressGoogleMap { get; set; }
        public string Passport { get; set; }
        public bool? IsReborrow { get; set; }
        public bool BuyInsuranceLender { get; set; }
        public bool? BuyInsuranceProperty { get; set; }


    }

    public class LMSLoanBriefPropertyDTO
    {
        public int LoanBriefId { get; set; }
        public string Brand { get; set; }
        public string Product { get; set; }
        public int? YearMade { get; set; }
        public string PlateNumber { get; set; }
        public string PlateNumberCar { get; set; }
        public string Chassis { get; set; }
        public string Engine { get; set; }

    }
    public class LMSLoanBriefRelationshipDTO
    {
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string RelationshipName { get; set; }
    }

    public class LMSLoanBriefDetail : LMSDisbursementWaitingDTO
    {
        public List<LMSLoanBriefRelationshipDTO> Relationship { get; set; }
        public LMSLoanBriefPropertyDTO LoanBriefProperty { get; set; }
        public int Inprocess { get; set; }
        public int TopUpOfLoanbriefID { get; set; }
        public string AddressNationalCard { get; set; }
        public int IsLivingInProvince { get; set; }
        public string BankName { get; set; }
        public int? AutoDistributeState { get; set; }
        public decimal? LoanAmountExpertise { get; set; }
        public decimal? LoanAmountExpertiseLast { get; set; }

        public int? ReMarketingLoanBriefId { get; set; }
        public int? GroupLoanId { get; set; }
        public bool? EsignState { get; set; }
        public decimal? FeePaymentBeforeLoan { get; set; }
        public bool DebtRevolvingLoan { get; set; }
        public int? DebtRevolvingLoanbriefId { get; set; }
        public string EsignBorrowerContract { get; set; }
        public string EsignLenderContract { get; set; }
        public int? HubEmployeeId { get; set; }
        public string HubEmployeeName { get; set; }
        public double? LenderRate { get; set; }
        public static Expression<Func<LoanBrief, LMSLoanBriefDetail>> ProjectionDetail
        {
            get
            {
                return x => new LMSLoanBriefDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    Status = x.Status,
                    LoanAmount = x.LoanAmountFirst,
                    LoanAmountFinal = x.LoanAmount,
                    LoanAmountExpertise = x.LoanAmountExpertiseAi > 0 ? x.LoanAmountExpertiseAi : x.LoanAmountExpertise,
                    LoanAmountExpertiseLast = x.LoanAmountExpertiseLast,
                    ProductId = x.ProductId,
                    ProductName = x.Product.Name,
                    RateTypeId = x.RateTypeId,
                    PlatformType = x.PlatformType,
                    LoanPurpose = x.LoanPurpose,
                    IsCheckCic = x.IsCheckCic,
                    IsTrackingLocation = x.IsTrackingLocation,
                    CountCall = x.CountCall,
                    Email = x.Email,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Frequency = x.Frequency,
                    ShopId = x.Hub.ShopId,
                    ShopName = x.Hub.Name,
                    LenderId = x.LenderId,
                    TypeLoanBrief = x.TypeLoanBrief,
                    BusinessCompanyName = x.LoanBriefCompany.CompanyName,
                    CareerBusiness = x.LoanBriefCompany.CareerBusiness,
                    BusinessCertificationDate = x.LoanBriefCompany.BusinessCertificationDate,
                    BusinessCertificationAddress = x.LoanBriefCompany.BusinessCertificationAddress,
                    HeadOffice = x.LoanBriefCompany.HeadOffice,
                    BusinessAddress = x.LoanBriefCompany.Address,
                    CompanyShareholder = x.LoanBriefCompany.CompanyShareholder,
                    CardNumberShareholder = x.LoanBriefCompany.CardNumberShareholder,
                    CardNumberShareholderDate = x.LoanBriefCompany.CardNumberShareholderDate,
                    CustomerId = x.CustomerId,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    Gender = x.Gender,
                    DOB = x.Dob,
                    NationalCard = x.NationalCard,
                    Passport = x.Passport,
                    ProvinceId = x.ProvinceId,
                    ProvinceName = x.Province.Name,
                    DistrictId = x.DistrictId,
                    DistrictName = x.District.Name,
                    WardId = x.WardId,
                    WardName = x.Ward.Name,
                    CustomerAddress = x.LoanBriefResident.Address,
                    AddressGoogleMap = x.LoanBriefResident.AddressLatLng,
                    IsMerried = x.Customer.IsMerried,
                    NumberBaby = x.Customer.NumberBaby,
                    LivingWith = x.LoanBriefResident.LivingWith,
                    ReceivingMoneyType = x.ReceivingMoneyType,
                    BankId = x.BankId,
                    BankName = x.Bank.Name,
                    BankAccountName = x.BankAccountName,
                    BankAccountNumber = x.BankAccountNumber,
                    BankCardNumber = x.BankCardNumber,
                    TypeInsurence = x.TypeInsurence,
                    JobId = x.LoanBriefJob.JobId,
                    JobTitle = x.LoanBriefJob.Job.Name,
                    CompanyName = x.LoanBriefJob.CompanyName,
                    CompanyPhone = x.LoanBriefJob.CompanyPhone,
                    CompanyAddress = x.LoanBriefJob.CompanyAddress,
                    CompanyAddressGoogleMap = x.LoanBriefJob.CompanyAddressLatLng,
                    CompanyTaxCode = x.LoanBriefJob.CompanyTaxCode,
                    CompanyProvinceName = x.LoanBriefJob.CompanyProvince.Name,
                    CompanyDistrictName = x.LoanBriefJob.CompanyDistrict.Name,
                    CompanyWardName = x.LoanBriefJob.CompanyWard.Name,
                    TotalIncome = x.LoanBriefJob.TotalIncome,
                    DescriptionJob = x.LoanBriefJob.Description,
                    BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                    ImcomeType = x.LoanBriefJob.ImcomeType,
                    ApproveName = x.CoordinatorUser.FullName,
                    ApproveId = x.CoordinatorUser.UserId,
                    ApprovePhone = x.CoordinatorUser.Phone,
                    HouseOldCityId = x.LoanBriefHousehold.Province.ProvinceId,
                    HouseOldCityName = x.LoanBriefHousehold.Province.Name,
                    HouseOldDistrictId = x.LoanBriefHousehold.District.DistrictId,
                    HouseOldDistrictName = x.LoanBriefHousehold.District.Name,
                    HouseOldWardId = x.LoanBriefHousehold.Ward.WardId,
                    HouseOldWardName = x.LoanBriefHousehold.Ward.Name,
                    HouseOldAddress = x.LoanBriefHousehold.Address,
                    TypeDisbursement = x.TypeDisbursement,
                    IsLock = x.IsLock,
                    LenderReceivedDate = x.LenderReceivedDate,
                    LivingTime = x.LoanBriefResident.LivingTime,
                    RatePercent = x.RatePercent,
                    InsurenceNumber = x.Customer.InsurenceNumber,
                    InsurenceNote = x.Customer.InsurenceNote,
                    FacebookAddress = x.Customer.FacebookAddress,
                    IsLocate = x.IsLocate,
                    DeviceId = x.DeviceId,
                    DeviceStatus = x.DeviceStatus,
                    CreatedTime = x.CreatedTime,
                    ReMarketingLoanBriefId = x.ReMarketingLoanBriefId,
                    Relationship = (x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0) ? x.LoanBriefRelationship.Select(x => new LMSLoanBriefRelationshipDTO()
                    {
                        FullName = x.FullName,
                        Address = x.Address,
                        Phone = x.Phone,
                        RelationshipName = x.RelationshipTypeNavigation != null ? x.RelationshipTypeNavigation.Name : ""
                    }).ToList() : null,
                    Inprocess = x.InProcess.GetValueOrDefault(),
                    LoanBriefProperty = x.LoanBriefProperty != null ? new LMSLoanBriefPropertyDTO()
                    {
                        LoanBriefId = x.LoanBriefProperty.LoanBriefPropertyId,
                        PlateNumber = x.LoanBriefProperty.PlateNumber,
                        PlateNumberCar = x.LoanBriefProperty.PlateNumberCar,
                        Brand = x.ProductId == (int)EnumProductCredit.OtoCreditType_CC ? x.LoanBriefProperty.CarManufacturer :
                                                x.LoanBriefProperty.Brand != null ? x.LoanBriefProperty.Brand.Name : "",
                        Product = x.ProductId == (int)EnumProductCredit.OtoCreditType_CC ? x.LoanBriefProperty.CarName :
                                                x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.FullName : "",
                        YearMade = x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.Year : 0,
                        Chassis = x.LoanBriefProperty.Chassis,
                        Engine = x.LoanBriefProperty.Engine
                    } : null,

                    TopUpOfLoanbriefID = x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp ? x.ReMarketingLoanBriefId.Value : 0,
                    DebtRevolvingLoan = x.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan,
                    DebtRevolvingLoanbriefId = x.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan ? x.ReMarketingLoanBriefId.Value : 0,
                    IsReborrow = x.IsReborrow,
                    AddressNationalCard = x.LoanBriefResident != null && !string.IsNullOrEmpty(x.LoanBriefResident.AddressNationalCard) ? x.LoanBriefResident.AddressNationalCard : "",
                    ResidentType = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType : 0,
                    IsLivingInProvince = x.ProvinceId == x.LoanBriefHousehold.ProvinceId ? 1 : 0,
                    BuyInsuranceLender = x.BuyInsuranceLender.GetValueOrDefault(true),
                    AutoDistributeState = x.AutoDistributeState,
                    BuyInsuranceProperty = x.LoanBriefProperty != null ? x.LoanBriefProperty.BuyInsuranceProperty : null,
                    FeePaymentBeforeLoan = x.FeePaymentBeforeLoan,
                    EsignState = x.EsignState == (int)LOS.Common.Extensions.EsignState.BORROWER_SIGNED || x.EsignState == (int)LOS.Common.Extensions.EsignState.LENDER_SIGNED,
                    EsignBorrowerContract = x.EsignBorrowerContract,
                    EsignLenderContract = x.EsignLenderContract,
                    HubEmployeeId = x.HubEmployeeId,
                    HubEmployeeName = x.HubEmployee != null ? x.HubEmployee.FullName : string.Empty,
                    LenderRate = x.LenderRate
                };
            }
        }


    }
    public class LMSGetListDisbursementWaiting : LMSLoanBriefDTO
    {
        public DateTime LenderCareReceivedDate { get; set; }
        public DateTime? NextDate { get; set; }
        public bool? EsignState { get; set; }
        public bool DebtRevolvingLoan { get; set; }
        public double? LenderRate { get; set; }
        public static Expression<Func<LoanBrief, LMSGetListDisbursementWaiting>> ProjectionDisbursementWaiting
        {
            get
            {
                return x => new LMSGetListDisbursementWaiting()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    LoanAmount = x.LoanAmountFirst,
                    LoanAmountFinal = x.LoanAmount,
                    CreatedTime = x.CreatedTime,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Status = x.Status,
                    LenderId = x.LenderId,
                    ProductId = x.ProductId,
                    ProductName = x.Product.Name,
                    ProvinceId = x.ProvinceId,
                    ProvinceName = x.Province.Name,
                    DistrictId = x.DistrictId,
                    DistrictName = x.District.Name,
                    WardId = x.WardId,
                    WardName = x.Ward.Name,
                    ShopId = x.Hub.ShopId,
                    ShopName = x.Hub.Name,
                    DisbursementBy = x.TypeDisbursement,
                    IsLock = x.IsLock,
                    NextDate = x.NextDate,
                    TypeInsurence = x.TypeInsurence,
                    BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                    LenderName = x.LenderName,
                    BuyInsuranceLender = x.BuyInsuranceLender.GetValueOrDefault(true),
                    AutoDistributeState = x.AutoDistributeState,
                    EsignState = x.EsignState == (int)LOS.Common.Extensions.EsignState.BORROWER_SIGNED || x.EsignState == (int)LOS.Common.Extensions.EsignState.LENDER_SIGNED,
                    DebtRevolvingLoan = x.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan,
                    LenderRate = x.LenderRate
                };
            }
        }


    }

    public class LMSLoanBriefWaitAppraiser : LMSLoanBriefDTO
    {
        public string CounselorPhone { get; set; }
        public string CounselorName { get; set; }
        public int CounselorID { get; set; }
        public int IsThamDinhNha { get; set; }
        public int IsThamDinhCongTy { get; set; }
        public static Expression<Func<LoanBrief, LMSLoanBriefWaitAppraiser>> ProjectionDetail
        {
            get
            {
                return x => new LMSLoanBriefWaitAppraiser()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.Customer.FullName,
                    ShopId = x.HubId.Value,
                    ShopName = x.Hub.Name,
                    ProductId = x.ProductId,
                    ProductName = x.Product.Name,
                    ProvinceId = x.ProvinceId,
                    ProvinceName = x.Province.Name,
                    DistrictId = x.DistrictId,
                    DistrictName = x.District.Name,
                    WardId = x.WardId,
                    WardName = x.Ward.Name,
                    LoanTime = x.LoanTime,
                    RateTypeId = x.RateTypeId,
                    Frequency = x.Frequency,
                    LenderId = x.LenderId,
                    CustomerId = x.Customer.CustomerId,
                    NationalCard = x.Customer.NationalCard,
                    DOB = x.Customer.Dob,
                    Gender = x.Gender,
                    Address = x.Customer.Address,
                    BankId = x.BankId,
                    BankAccountName = x.BankAccountName,
                    BankAccountNumber = x.BankAccountNumber,
                    BankCardNumber = x.BankCardNumber,
                    NationalCardDate = x.NationalCardDate,
                    NationCardPlace = x.NationCardPlace,
                    LoanAmount = x.LoanAmountFirst,
                    LoanAmountFinal = x.LoanAmount,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    ConsultantRate = x.ConsultantRate,
                    ServiceRate = x.ServiceRate,
                    LoanRate = x.LoanRate,
                    CreatedTime = x.CreatedTime,
                    LoanBriefComment = x.LoanBriefComment,
                    TypeInsurence = x.TypeInsurence,
                    ApproveName = x.CoordinatorUser.FullName,
                    ApproveId = x.CoordinatorUser.UserId,
                    ApprovePhone = x.CoordinatorUser.Phone,
                    CounselorID = x.EvaluationHomeUserId.GetValueOrDefault(),
                    CounselorName = x.EvaluationHomeUser != null ? x.EvaluationHomeUser.Username : string.Empty,
                    IsThamDinhNha = x.EvaluationHomeUserId.GetValueOrDefault() > 0 ? 1 : 0,
                    IsThamDinhCongTy = x.EvaluationCompanyUserId.GetValueOrDefault() > 0 ? 1 : 0,
                };
            }
        }
    }

    public class LMSLoanBriefViewDetail : LMSLoanBriefDTO
    {
        public Customer Customer { get; set; }
        public LoanBriefHouseholdDTO CustomerHousehold { get; set; }
        public LoanBriefJobDTO CustomerJob { get; set; }
        public LoanBriefResidentDTO CustomerResident { get; set; }
        public List<LoanBriefRelationship> CustomerRelationship { get; set; }
        public List<LoanBriefRelationship> Relationship { get; set; }
        public int IsThamDinhNha { get; set; }
        public int IsThamDinhCongTy { get; set; }
        public static Expression<Func<LoanBrief, LMSLoanBriefViewDetail>> ProjectionDetail
        {
            get
            {
                return x => new LMSLoanBriefViewDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    ProductId = x.ProductId,
                    ProductName = x.Product.Name,
                    ProvinceId = x.ProvinceId,
                    ProvinceName = x.Province.Name,
                    DistrictId = x.DistrictId,
                    DistrictName = x.District.Name,
                    WardId = x.WardId,
                    WardName = x.Ward.Name,
                    LoanTime = x.LoanTime,
                    RateTypeId = x.RateTypeId,
                    Frequency = x.Frequency,
                    ShopId = x.Hub.ShopId,
                    ShopName = x.Hub.Name,
                    LenderId = x.LenderId,
                    CustomerId = x.Customer.CustomerId,
                    FullName = x.Customer.FullName,
                    NationalCard = x.Customer.NationalCard,
                    DOB = x.Customer.Dob,
                    Gender = x.Gender,
                    Address = x.Customer.Address,
                    NationalCardDate = x.NationalCardDate,
                    NationCardPlace = x.NationCardPlace,
                    LoanAmount = x.LoanAmountFirst,
                    LoanAmountFinal = x.LoanAmount,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    ConsultantRate = x.ConsultantRate,
                    ServiceRate = x.ServiceRate,
                    LoanRate = x.LoanRate,
                    CreatedTime = x.CreatedTime,
                    TypeInsurence = x.TypeInsurence,
                    ApproveName = x.CoordinatorUser.FullName,
                    ApproveId = x.CoordinatorUser.UserId,
                    ApprovePhone = x.CoordinatorUser.Phone,
                    Customer = x.Customer,
                    CustomerJob = x.LoanBriefJob != null ? new LoanBriefJobDTO()
                    {
                        CompanyDistrictId = x.LoanBriefJob.CompanyDistrictId,
                        CompanyDistrictName = x.LoanBriefJob.CompanyDistrict != null ? x.LoanBriefJob.CompanyDistrict.Name : "",
                        CompanyAddress = x.LoanBriefJob.CompanyAddress,
                        CompanyWardId = x.LoanBriefJob.CompanyWardId,
                        CompanyWardName = x.LoanBriefJob.CompanyWard != null ? x.LoanBriefJob.CompanyWard.Name : "",
                        CompanyName = x.LoanBriefJob.CompanyName,
                        JobId = x.LoanBriefJob.JobId,
                        JobName = x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.Name : string.Empty

                    } : null,
                    CustomerResident = x.LoanBriefResident != null ? new LoanBriefResidentDTO()
                    {
                        Address = x.LoanBriefResident.Address,
                        LivingTime = x.LoanBriefResident.LivingTime,
                        LivingWith = x.LoanBriefResident.LivingWith,
                        DistrictId = x.LoanBriefResident.DistrictId,
                        DistrictName = x.LoanBriefResident.District != null ? x.LoanBriefResident.District.Name : string.Empty,
                        WardId = x.LoanBriefResident.WardId,
                        WardName = x.LoanBriefResident.Ward != null ? x.LoanBriefResident.Ward.Name : string.Empty,

                    } : null,
                    CustomerHousehold = x.LoanBriefHousehold != null ? new LoanBriefHouseholdDTO()
                    {
                        Address = x.LoanBriefHousehold.Address,
                        DistrictId = x.LoanBriefHousehold.DistrictId,
                        DistrictName = x.LoanBriefHousehold.District != null ? x.LoanBriefHousehold.District.Name : string.Empty,
                        WardId = x.LoanBriefHousehold.WardId,
                        WardName = x.LoanBriefHousehold.Ward != null ? x.LoanBriefHousehold.Ward.Name : string.Empty,
                    } : null,
                    CustomerRelationship = (x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0) ? x.LoanBriefRelationship.ToList() : null,
                    IsThamDinhNha = x.EvaluationHomeUserId.GetValueOrDefault() > 0 ? 1 : 0,
                    IsThamDinhCongTy = x.EvaluationCompanyUserId.GetValueOrDefault() > 0 ? 1 : 0,

                };
            }
        }


    }
}
