﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.AppAPI.DTOs.LMS
{
	public class LMSLoanBriefNoteDTO
	{
		public int LoanBriefNoteId { get; set; }
		public int? LoanBriefId { get; set; }
		public int? Type { get; set; }

        public int? UserId { get; set; }
        public string Note { get; set; }
		public int? Status { get; set; }

        public string FullName { get; set; }
        public string ShopName { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
	}

    public class LMSLoanBriefNoteDetail : LMSLoanBriefNoteDTO
    {
        public static Expression<Func<LoanBriefNote, LMSLoanBriefNoteDetail>> ProjectionDetail
        {
            get
            {
                return x => new LMSLoanBriefNoteDetail()
                {
                    LoanBriefNoteId = x.LoanBriefNoteId,
                    LoanBriefId = x.LoanBriefId,
                    Type = x.Type,
                    Note = x.Note,
                    Status = x.Status,
                    UserId = x.UserId,
                    FullName = x.FullName,
                    ShopName = x.ShopName,
                    CreatedTime = x.CreatedTime
                };
            }
        }
    }
}
