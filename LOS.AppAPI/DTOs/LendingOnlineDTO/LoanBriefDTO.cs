﻿using LOS.AppAPI.Helpers;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.LendingOnlineDTO
{
    public class LoanBriefDTO
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public DateTime? DOB { get; set; }
        public int? Gender { get; set; }
        public int? LoanPurpose { get; set; }
        public string LoanPurposeName { get; set; }
        public string Phone { get; set; }
        public string CardNumber { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public int? Status { get; set; }
        public int? BankId { get; set; }
        public string BankName { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }
        public string BankAccountName { get; set; }
        public int? Step { get; set; }

    }

    public class LoanbriefJobForLending
    {
        public int? JobId { get; set; }
        public string JobName { get; set; }
        public decimal? TotalIncome { get; set; }
    }

    public class LoanBriefResidentForLending
    {
        public string Address { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? WardId { get; set; }
        public string WardName { get; set; }
    }

    public class LoanBriefRelationshipForLending
    {
        public string Phone { get; set; }
    }


    public class LoanBriefDetail : LoanBriefDTO
    {
        public int? PlatformType { get; set; }
        public int? ProductDetailId { get; set; }
        public LoanbriefJobForLending Job { get; set; }
        public LoanBriefResidentForLending LoanBriefResident { get; set; }
        public List<LoanBriefRelationshipForLending> LoanBriefRelationship { get; set; }
        public string ResultLocation { get; set; }
        public string FileNational { get; set; }
        public string FileSelfie { get; set; }
        public string FileMotobikeCerfiticate { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.Customer.FullName,
                    Phone = x.Phone,
                    DOB = x.Customer.Dob,
                    Gender = x.Gender,
                    LoanPurpose = x.LoanPurpose,
                    CardNumber = x.Customer.NationalCard,
                    Status = x.Status,
                    LoanTime = x.LoanTime,
                    LoanAmount = x.LoanAmountFirst,
                    BankId = x.BankId,
                    BankName = x.Bank != null ? x.Bank.Name : "",
                    BankAccountName = x.BankAccountName,
                    BankAccountNumber = x.BankAccountNumber,
                    BankCardNumber = x.BankCardNumber,
                    CreatedTime = x.CreatedTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Step = x.Step,
                    PlatformType = x.PlatformType,
                    ProductDetailId = x.ProductDetailId,
                   
                    Job = x.LoanBriefJob != null ? new LoanbriefJobForLending()
                    {
                        JobId = x.LoanBriefJob.JobId,
                        JobName = x.LoanBriefJob != null && x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.Name : string.Empty,
                        TotalIncome = x.LoanBriefJob != null ? x.LoanBriefJob.TotalIncome: 0

                    } : null,
                    LoanBriefResident = x.LoanBriefResident != null ? new LoanBriefResidentForLending()
                    {
                        ProvinceId = x.LoanBriefResident != null ? x.LoanBriefResident.ProvinceId : 0,
                        ProvinceName = x.LoanBriefResident != null && x.LoanBriefResident.Province != null? x.LoanBriefResident.Province.Name : "",
                        DistrictId = x.LoanBriefResident != null ? x.LoanBriefResident.DistrictId : 0,
                        DistrictName = x.LoanBriefResident != null && x.LoanBriefResident.District != null ? x.LoanBriefResident.District.Name : "",
                        WardId = x.LoanBriefResident != null ? x.LoanBriefResident.WardId : 0,
                        WardName = x.LoanBriefResident != null && x.LoanBriefResident.Ward != null ? x.LoanBriefResident.Ward.Name : "",
                        Address = x.LoanBriefResident.Address
                    } : null,
                    LoanBriefRelationship = (x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0) ? x.LoanBriefRelationship.Select(x=> new LoanBriefRelationshipForLending() { Phone = x.Phone}).ToList() : null

                };
            }
        }
    }

}
