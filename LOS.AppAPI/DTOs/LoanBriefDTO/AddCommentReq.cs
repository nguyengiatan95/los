﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.LoanBriefDTO
{
    public class AddCommentReq
    {
        public int loanbriefId { get; set; }

        public string comment { get; set; }

        public string sDevice { get; set; }

        public string sLat { get; set; }

        public string sLong { get; set; }

        public string sAddress { get; set; }

        public string sCity { get; set; }
    }
}
