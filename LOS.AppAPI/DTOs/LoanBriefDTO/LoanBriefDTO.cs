﻿using LOS.AppAPI.Helpers;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.LoanBriefDTO
{
    public class LoanBriefDTO
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public int? ProductId { get; set; }
        public int? RateTypeId { get; set; }
        public string Phone { get; set; }
        public int? Gender { get; set; }
        public DateTime? DOB { get; set; }
        public string CardNumber { get; set; }
        public DateTime? NationalCardDate { get; set; }
        public string NationCardPlace { get; set; }
        public decimal? LoanAmount { get; set; }
        public decimal? LoanAmountFinal { get; set; }
        public int? LoanTime { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public double? ConsultantRate { get; set; }
        public double? ServiceRate { get; set; }
        public double? LoanRate { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? WardId { get; set; }
        public string WardName { get; set; }
        public string UtmSource { get; set; }
        public string UtmMedium { get; set; }
        public string UtmCampaign { get; set; }
        public string UtmTerm { get; set; }
        public string UtmContent { get; set; }
        public int? ScoreCredit { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTimeOffset? UpdatedTime { get; set; }
        public int? Status { get; set; }

        public int? AffCode { get; set; }
        public int? BoundTelesaleId { get; set; }
        public int? AffStatus { get; set; }
        public string Comment { get; set; }
        public DateTime? ScheduleTime { get; set; }
        public DateTime? LoanBriefCancelAt { get; set; }
        public int? LoanBriefCancelBy { get; set; }
        public DateTime? TelesalesPushAt { get; set; }
        public int? PipelineState { get; set; }
        public int? ActionState { get; set; }
        public int? LenderId { get; set; }
        public int? CoordinatorUserId { get; set; }
        public int? HubEmployeeId { get; set; }
        //public bool? IsLocate { get; set; } = false;
        public string DeviceId { get; set; }
        public int? Frequency { get; set; }

        public int? CustomerId { get; set; }
        public int? ReceivingMoneyType { get; set; }
        public int? BankId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }
        public string BankAccountName { get; set; }

        public bool? BuyInsurenceCustomer { get; set; }
        public string ProductName { get; set; }

        public int ShopId { get; set; }
        public string HubName { get; set; }
        public string Address { get; set; }

        public int ApproveId { get; set; }
        public string CoordinatorPhone { get; set; }
        public string CoordinatorName { get; set; }
        public int? TypeInsurence { get; set; }

        public string HouseholdCityName { get; set; }
        public string HouseholdDistrictName { get; set; }
        public string HouseholdWardName { get; set; }
        public string HouseholdAddress { get; set; }
        public int? DisbursementBy { get; set; }

        public bool? IsLock { get; set; }
        public string LinkCall { get; set; }
        public string FieldSurveyName { get; set; }
        public string FieldSurveyPhone { get; set; }
        public int? AddressFieldSurveyStatus { get; set; }
        public int? FieldSurveyUserID { get; set; }
        public int? CompanyFieldSurveyUserID { get; set; }
        public int? CompanyFieldSurveyStatus { get; set; }
        public string EmployeesNameCompany { get; set; }
        public string EmployeesPhoneCompany { get; set; }

        public int? PlatformType { get; set; }
        public int? TypeOfOwnershipId { get; set; }
        public int? ReceiveYourIncome { get; set; }
        public string ReceiveYourIncomeName
        {
            get
            {
                var result = string.Empty;
                if (ReceiveYourIncome > 0 && Enum.IsDefined(typeof(EnumImcomeType), ReceiveYourIncome))
                    return ExtentionHelper.GetDescription((EnumImcomeType)ReceiveYourIncome);
                return result;
            }
        }
        public string HubEmployeesName { get; set; }
        public bool? CustomerBuyInsurance { get; set; }

        public DateTime? EvaluationHomeTime { get; set; }
        public DateTime? EvaluationCompanyTime { get; set; }
        public int PriorityContract { get; set; }
        public string PriorityContractName
        {
            get
            {
                var result = string.Empty;
                if (PriorityContract == 1)
                    return "Đơn vay lại tốt";
                return result;
            }
        }

        public int TimeDelay { get; set; }
        public string TimeDelayValue { get; set; }
        public DateTime BeginStartTime { get; set; }


    }

    public class LenderLoanBriefDTO
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public int? ProductId { get; set; }
        public int? RateTypeId { get; set; }
        public string Phone { get; set; }
        public decimal? LoanAmountFinal { get; set; }
        public int? LoanTime { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public string ProductName { get; set; }
        public int? LenderId { get; set; }
        public DateTime? NextDate { get; set; }

    }
    public class LenderLoanBriefDetailDTO
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? RateTypeId { get; set; }
        public string RateTypeName { get; set; }
        public string Phone { get; set; }
        public int? Gender { get; set; }
        public string CardNumber { get; set; }
        public decimal? LoanAmountFinal { get; set; }
        public int? LoanTime { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? WardId { get; set; }
        public string WardName { get; set; }

        public string Address { get; set; }

        public string CompanyName { get; set; }
        public string JobTitle { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyCityName { get; set; }
        public string CompanyDistrictName { get; set; }
        public string CompanyWardName { get; set; }
        public string CompanyAddress { get; set; }
        public decimal? TotalIncome { get; set; }

        public int? BankId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }
        public string BankAccountName { get; set; }
        public string BankCode { get; set; }

        public int? LenderId { get; set; }
        public int? Status { get; set; }
        public string Description { get; set; }
        public int? EsignState { get; set; }
        public double? LenderRate { get; set; }
    }

    public class ERPLoanBriefDetailDTO
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public int? Status { get; set; }
        public string DeviceId { get; set; }

    }
    public class ERPLoanBriefGetListDTO
    {
        public int LoanBriefId { get; set; }
        public DateTime? ToDate { get; set; }
        public string FullName { get; set; }
        public int? Status { get; set; }
        public string strStatus { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? CodeId { get; set; }
        public string DeviceId { get; set; }

    }
    public class LoanBriefDetail : LoanBriefDTO
    {
        public LoanProductDTO LoanProduct { get; set; }
        public LoanRateTypeDTO LoanRateType { get; set; }
        public ProvinceDTO Province { get; set; }
        public DistrictDTO District { get; set; }
        public WardDTO Ward { get; set; }
        public UserDTO Approve { get; set; }
        public LoanBriefJobDetail Job { get; set; }
        public Customer Customer { get; set; }
        public ShopDTO Shop { get; set; }

        public BankDTO Bank { get; set; }

        public string StatusName { get; set; }
        public string TypeOfOwnershipName { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefDetail()
                {
                    Status = x.Status,
                    FullName = x.Customer.FullName,
                    Phone = x.Phone,
                    ShopId = x.Hub.ShopId,
                    HubName = x.Hub.Name,
                    LoanBriefId = x.LoanBriefId,
                    CardNumber = x.Customer.NationalCard,
                    ProductId = x.ProductId,
                    ProductName = x.Product.Name,
                    Address = x.Customer.Address,
                    ProvinceId = x.ProvinceId,
                    ProvinceName = x.Province.Name,
                    DistrictId = x.DistrictId,
                    DistrictName = x.District.Name,
                    WardId = x.WardId,
                    WardName = x.Ward.Name,
                    LoanTime = x.LoanTime,
                    RateTypeId = x.RateTypeId,
                    Frequency = x.Frequency,
                    LenderId = x.LenderId,
                    CustomerId = x.Customer.CustomerId,
                    DOB = x.Customer.Dob,
                    Gender = x.Gender,
                    BankId = x.BankId,
                    BankAccountName = x.BankAccountName,
                    BankAccountNumber = x.BankAccountNumber,
                    BankCardNumber = x.BankCardNumber,
                    BoundTelesaleId = x.BoundTelesaleId,
                    NationalCardDate = x.NationalCardDate,
                    NationCardPlace = x.NationCardPlace,
                    LoanAmount = x.LoanAmountFirst,
                    LoanAmountFinal = x.LoanAmount,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    ConsultantRate = x.ConsultantRate,
                    ServiceRate = x.ServiceRate,
                    LoanRate = x.LoanRate,
                    CreatedTime = x.CreatedTime,
                    Comment = x.LoanBriefComment,
                    TypeInsurence = x.TypeInsurence,
                    CoordinatorName = x.CoordinatorUser.FullName,
                    ApproveId = x.CoordinatorUser.UserId,
                    CoordinatorPhone = x.CoordinatorUser.Phone,
                    LinkCall = "",
                    HouseholdCityName = x.LoanBriefHousehold.Province.Name,
                    HouseholdDistrictName = x.LoanBriefHousehold.District.Name,
                    HouseholdWardName = x.LoanBriefHousehold.Ward.Name,
                    HouseholdAddress = x.LoanBriefHousehold.Address,
                    DisbursementBy = x.TypeDisbursement,
                    IsLock = x.IsLock,
                    FieldSurveyUserID = x.EvaluationHomeUserId,
                    AddressFieldSurveyStatus = x.EvaluationHomeState,
                    FieldSurveyPhone = x.EvaluationHomeUser != null ? x.EvaluationHomeUser.Phone : string.Empty,
                    FieldSurveyName = x.EvaluationHomeUser != null ? x.EvaluationHomeUser.Username : string.Empty,
                    CompanyFieldSurveyStatus = x.EvaluationCompanyState,
                    CompanyFieldSurveyUserID = x.EvaluationCompanyUserId,
                    EmployeesPhoneCompany = x.EvaluationCompanyUser != null ? x.EvaluationCompanyUser.Phone : string.Empty,
                    EmployeesNameCompany = x.EvaluationCompanyUser != null ? x.EvaluationCompanyUser.Username : string.Empty,
                    PlatformType = x.PlatformType,
                    TypeOfOwnershipId = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType : 0,
                    TypeOfOwnershipName = (x.LoanBriefResident != null && Enum.IsDefined(typeof(EnumTypeofownership), x.LoanBriefResident.ResidentType))
                        ? ExtentionHelper.GetDescription((EnumTypeofownership)x.LoanBriefResident.ResidentType) : "",
                    ReceiveYourIncome = x.LoanBriefJob != null ? x.LoanBriefJob.ImcomeType : 0,
                    //ReceiveYourIncomeName = (x.LoanBriefJob != null && Enum.IsDefined(typeof(TypeReceiveYourIncome), x.LoanBriefJob.ImcomeType))
                    //    ? ExtentionHelper.GetDescription((TypeReceiveYourIncome)x.LoanBriefJob.ImcomeType) : "",
                    HubEmployeesName = x.HubEmployee != null ? x.HubEmployee.FullName : string.Empty,
                    CustomerBuyInsurance = x.BuyInsurenceCustomer
                };
            }
        }


    }
    public class GetListDisbursementWaiting : LoanBriefDTO
    {
        public LoanProductDTO LoanProduct { get; set; }
        public ProvinceDTO Province { get; set; }
        public DistrictDTO District { get; set; }
        public WardDTO Ward { get; set; }
        public ShopDTO Shop { get; set; }
        public static Expression<Func<LoanBrief, GetListDisbursementWaiting>> ProjectionDisbursementWaiting
        {
            get
            {
                return x => new GetListDisbursementWaiting()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    LoanAmountFinal = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Status = x.Status,
                    LenderId = x.LenderId,
                    ProductId = x.ProductId,
                    ProductName = x.Product.Name,
                    ProvinceId = x.ProvinceId,
                    ProvinceName = x.Province.Name,
                    DistrictId = x.DistrictId,
                    DistrictName = x.District.Name,
                    WardId = x.WardId,
                    WardName = x.Ward.Name,
                    ShopId = x.Hub.ShopId,
                    HubName = x.Hub.Name,
                    DisbursementBy = x.TypeDisbursement,
                    IsLock = x.IsLock

                };
            }
        }


    }

    public class LoanBriefWaitAppraiser : LoanBriefDTO
    {
        public string CounselorPhone { get; set; }
        public string CounselorName { get; set; }
        public int CounselorID { get; set; }
        public int IsThamDinhNha { get; set; }
        public int IsThamDinhCongTy { get; set; }
        public int IsThamDinhXe { get; set; }
        public int? MotobikeId { get; set; }

        public string ResultLocationHome { get; set; }
        public string ResultLocationCompany { get; set; }
        public int? JobId { get; set; }
        public bool? BusinessPapers { get; set; }
        public string SuggestDocument { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefWaitAppraiser>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefWaitAppraiser()
                {
                    Status = x.Status,
                    FullName = x.Customer.FullName,
                    Phone = x.Phone,
                    ShopId = x.Hub.ShopId,
                    HubName = x.Hub.Name,
                    LoanBriefId = x.LoanBriefId,
                    CardNumber = x.Customer.NationalCard,
                    ProductId = x.ProductId,
                    ProductName = x.Product.Name,
                    Address = x.Customer.Address,
                    ProvinceId = x.ProvinceId,
                    ProvinceName = x.Province.Name,
                    DistrictId = x.DistrictId,
                    DistrictName = x.District.Name,
                    WardId = x.WardId,
                    WardName = x.Ward.Name,
                    LoanTime = x.LoanTime,
                    RateTypeId = x.RateTypeId,
                    Frequency = x.Frequency,
                    LenderId = x.LenderId,
                    CustomerId = x.Customer.CustomerId,
                    DOB = x.Customer.Dob,
                    Gender = x.Gender,
                    BankId = x.BankId,
                    BankAccountName = x.BankAccountName,
                    BankAccountNumber = x.BankAccountNumber,
                    BankCardNumber = x.BankCardNumber,
                    BoundTelesaleId = x.BoundTelesaleId,
                    NationalCardDate = x.NationalCardDate,
                    NationCardPlace = x.NationCardPlace,
                    LoanAmount = x.LoanAmountFirst,
                    LoanAmountFinal = x.LoanAmount,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    ConsultantRate = x.ConsultantRate,
                    ServiceRate = x.ServiceRate,
                    LoanRate = x.LoanRate,
                    CreatedTime = x.CreatedTime,
                    Comment = x.LoanBriefComment,
                    TypeInsurence = x.TypeInsurence,
                    CoordinatorName = x.CoordinatorUser.FullName,
                    ApproveId = x.CoordinatorUser.UserId,
                    CoordinatorPhone = x.CoordinatorUser.Phone,
                    LinkCall = "",
                    HouseholdCityName = x.LoanBriefHousehold.Province.Name,
                    HouseholdDistrictName = x.LoanBriefHousehold.District.Name,
                    HouseholdWardName = x.LoanBriefHousehold.Ward.Name,
                    HouseholdAddress = x.LoanBriefHousehold.Address,
                    DisbursementBy = x.TypeDisbursement,
                    IsLock = x.IsLock,
                    FieldSurveyUserID = x.EvaluationHomeUserId,
                    AddressFieldSurveyStatus = x.EvaluationHomeState.HasValue ? x.EvaluationHomeState : 0,
                    FieldSurveyPhone = x.EvaluationHomeUser != null ? x.EvaluationHomeUser.Phone : string.Empty,
                    FieldSurveyName = x.EvaluationHomeUser != null ? x.EvaluationHomeUser.Username : string.Empty,
                    CompanyFieldSurveyStatus = x.EvaluationCompanyState.HasValue ? x.EvaluationCompanyState : 0,
                    CompanyFieldSurveyUserID = x.EvaluationCompanyUserId,
                    EmployeesPhoneCompany = x.EvaluationCompanyUser != null ? x.EvaluationCompanyUser.Phone : string.Empty,
                    EmployeesNameCompany = x.EvaluationCompanyUser != null ? x.EvaluationCompanyUser.Username : string.Empty,
                    PlatformType = x.PlatformType,
                    TypeOfOwnershipId = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType : 0,
                    ReceiveYourIncome = x.LoanBriefJob != null ? x.LoanBriefJob.ImcomeType : 0,
                    //ReceiveYourIncomeName = (x.LoanBriefJob != null && x.LoanBriefJob.ImcomeType > 0 && Enum.IsDefined(typeof(TypeReceiveYourIncome), x.LoanBriefJob.ImcomeType))
                    //    ? ExtentionHelper.GetDescription((TypeReceiveYourIncome)x.LoanBriefJob.ImcomeType) : "",
                    HubEmployeesName = x.HubEmployee != null ? x.HubEmployee.FullName : string.Empty,
                    CustomerBuyInsurance = x.BuyInsurenceCustomer,

                    IsThamDinhNha = x.EvaluationHomeUserId.GetValueOrDefault() > 0 ? 1 : 0,
                    IsThamDinhCongTy = x.EvaluationCompanyUserId.GetValueOrDefault() > 0 ? 1 : 0,
                    EvaluationHomeTime = x.EvaluationHomeTime,
                    EvaluationCompanyTime = x.EvaluationCompanyTime,
                    PriorityContract = x.PriorityContract.GetValueOrDefault(),
                    BeginStartTime = x.BeginStartTime.GetValueOrDefault(),
                    MotobikeId = x.LoanBriefProperty != null && x.LoanBriefProperty.ProductId.HasValue ? x.LoanBriefProperty.ProductId : 0,
                    ResultLocationHome = x.LoanBriefResident != null ? x.LoanBriefResident.ResultLocation : "",
                    ResultLocationCompany = x.LoanBriefJob != null ? x.LoanBriefJob.ResultLocation : "",
                    JobId = x.LoanBriefJob != null ? x.LoanBriefJob.JobId : 0,
                    BusinessPapers = x.LoanBriefJob != null ? x.LoanBriefJob.BusinessPapers : null,
                };
            }
        }
    }

    public class LoanBriefViewDetail : LoanBriefDTO
    {
        public Customer Customer { get; set; }
        public LoanBriefHouseholdDTO CustomerHousehold { get; set; }
        public LoanBriefJobDTO CustomerJob { get; set; }
        public LoanBriefPropertyDTO LoanBriefProperty { get; set; }
        public LoanBriefResidentDTO CustomerResident { get; set; }
        public List<LoanBriefRelationshipDTO> CustomerRelationship { get; set; }
        public int IsThamDinhNha { get; set; }
        public int IsThamDinhCongTy { get; set; }
        public decimal? LoanAmountFirst { get; set; }

        public string NameRateType { get; set; }

        public int NumberPeriod
        {
            get
            {
                if (Frequency > 0 && LoanTime > 0)
                    return LoanTime.Value / Frequency.Value;
                return 0;
            }
        }

        public decimal? TotalMoneyConsulant { get; set; } = 0;
        public decimal? TotalMoneyPaymentOnePeriod { get; set; } = 0;
        public decimal? TotalMoneyService { get; set; } = 0;
        public decimal? TotalMoneyAccounting { get; set; } = 0;
        public decimal? TotalMoneyInterest { get; set; } = 0;
        public decimal? MoneyFeeInsuranceOfCustomer { get; set; } = 0;
        public decimal RealMoneyReceived
        {
            get
            {
                if (LoanAmount > 0)
                    return LoanAmount.Value - MoneyFeeInsuranceOfCustomer.Value;
                return 0;
            }
        }

        public string ReceivingMoneyTypeName
        {
            get
            {
                var result = "";
                if (ReceivingMoneyType > 0 && Enum.IsDefined(typeof(EnumReceivingMoneyType), ReceivingMoneyType))
                    return ExtentionHelper.GetDescription((EnumReceivingMoneyType)ReceivingMoneyType);
                return result;
            }
        }

        public int IsThamDinhXe { get; set; } = 0;
        public int? ImcomeType { get; set; }

        public string BankName { get; set; }
        public string TypeOfOwnershipName { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefViewDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefViewDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    ProductId = x.ProductId ?? 0,
                    ProductName = x.Product.Name ?? "",
                    ProvinceId = x.ProvinceId ?? 0,
                    ProvinceName = x.Province.Name ?? "",
                    DistrictId = x.DistrictId ?? 0,
                    DistrictName = x.District.Name ?? "",
                    WardId = x.WardId ?? 0,
                    WardName = x.Ward.Name ?? "",
                    LoanTime = x.LoanTime ?? 0,
                    RateTypeId = x.RateTypeId ?? 0,
                    NameRateType = x.RateTypeId > 0 ? (x.RateTypeId == (int)EnumRateType.RateMonthADay || x.RateTypeId == (int)EnumRateType.TatToanCuoiKy) ? "Tất toán cuối kỳ" : "Dư nợ giảm dần" : "",
                    Frequency = x.Frequency ?? 0,
                    ShopId = x.Hub != null ? x.Hub.ShopId : 0,
                    HubName = x.Hub.Name ?? "",
                    LenderId = x.LenderId ?? 0,
                    CustomerId = x.CustomerId,
                    FullName = x.FullName ?? "",
                    CardNumber = x.NationalCard ?? "",
                    DOB = x.Dob,
                    Gender = x.Gender ?? 0,
                    Address = x.Customer.Address ?? "",
                    NationalCardDate = x.NationalCardDate,
                    NationCardPlace = x.NationCardPlace ?? "",
                    LoanAmount = x.LoanAmountFirst ?? x.LoanAmount,
                    LoanAmountFinal = x.LoanAmount ?? 0,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    ConsultantRate = x.ConsultantRate,
                    ServiceRate = x.ServiceRate,
                    LoanRate = x.LoanRate,
                    CreatedTime = x.CreatedTime,
                    TypeInsurence = x.TypeInsurence ?? 0,
                    CoordinatorName = x.CoordinatorUser.FullName ?? "",
                    ApproveId = x.CoordinatorUser.UserId,
                    CoordinatorPhone = x.CoordinatorUser.Phone ?? "",
                    //MoneyFeeInsuranceOfCustomer = x.BuyInsurenceCustomer
                    Customer = x.Customer,
                    Phone = x.Phone,
                    TypeOfOwnershipId = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType : 0,

                    CustomerJob = new LoanBriefJobDTO()
                    {
                        CompanyProvinceId = x.LoanBriefJob != null && x.LoanBriefJob.CompanyProvinceId > 0 ? x.LoanBriefJob.CompanyProvinceId : 0,
                        CompanyProvinceName = x.LoanBriefJob != null && x.LoanBriefJob.CompanyProvince != null ? x.LoanBriefJob.CompanyProvince.Name : "",
                        CompanyDistrictId = x.LoanBriefJob != null && x.LoanBriefJob.CompanyDistrictId > 0 ? x.LoanBriefJob.CompanyDistrictId : 0,
                        CompanyDistrictName = x.LoanBriefJob != null && x.LoanBriefJob.CompanyDistrict != null ? x.LoanBriefJob.CompanyDistrict.Name : "",
                        CompanyAddress = x.LoanBriefJob != null && !string.IsNullOrEmpty(x.LoanBriefJob.CompanyAddress) ? x.LoanBriefJob.CompanyAddress : "",
                        CompanyWardId = x.LoanBriefJob != null && x.LoanBriefJob.CompanyWardId > 0 ? x.LoanBriefJob.CompanyWardId : 0,
                        CompanyWardName = x.LoanBriefJob != null && x.LoanBriefJob.CompanyWard != null ? x.LoanBriefJob.CompanyWard.Name : "",
                        CompanyName = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyName ?? "" : "",
                        CompanyPhone = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyPhone ?? "" : "",

                        JobId = x.LoanBriefJob != null ? x.LoanBriefJob.JobId ?? 0 : 0,
                        JobName = x.LoanBriefJob != null && x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.Name : "",
                        ImcomeType = x.LoanBriefJob != null && x.LoanBriefJob.ImcomeType != null ? x.LoanBriefJob.ImcomeType ?? 0 : 0,
                        TotalIncome = x.LoanBriefJob != null && x.LoanBriefJob.TotalIncome != null ? x.LoanBriefJob.TotalIncome ?? 0 : 0,
                        Description = x.LoanBriefJob != null && x.LoanBriefJob.Description != null ? x.LoanBriefJob.Description ?? "" : "",

                    },
                    CustomerResident = new LoanBriefResidentDTO()
                    {
                        Address = x.LoanBriefResident != null ? x.LoanBriefResident.Address ?? "" : "",
                        LivingTime = x.LoanBriefResident != null ? x.LoanBriefResident.LivingTime ?? 0 : 0,
                        LivingWith = x.LoanBriefResident != null ? x.LoanBriefResident.LivingWith ?? 0 : 0,
                        DistrictId = x.LoanBriefResident != null ? x.LoanBriefResident.DistrictId ?? 0 : 0,
                        DistrictName = x.LoanBriefResident != null && x.LoanBriefResident.District != null ? x.LoanBriefResident.District.Name ?? "" : "",
                        WardId = x.LoanBriefResident != null ? x.LoanBriefResident.WardId : 0,
                        WardName = x.LoanBriefResident != null && x.LoanBriefResident.Ward != null ? x.LoanBriefResident.Ward.Name ?? "" : "",
                    },
                    CustomerHousehold = new LoanBriefHouseholdDTO()
                    {
                        Address = x.LoanBriefHousehold != null && !string.IsNullOrEmpty(x.LoanBriefHousehold.Address) ? x.LoanBriefHousehold.Address : "",
                        DistrictId = x.LoanBriefHousehold != null && x.LoanBriefHousehold.DistrictId > 0 ? x.LoanBriefHousehold.DistrictId : 0,
                        DistrictName = x.LoanBriefHousehold != null && x.LoanBriefHousehold.District != null ? x.LoanBriefHousehold.District.Name : "",
                        WardId = x.LoanBriefHousehold != null && x.LoanBriefHousehold.WardId > 0 ? x.LoanBriefHousehold.WardId : 0,
                        WardName = x.LoanBriefHousehold.Ward != null ? x.LoanBriefHousehold.Ward.Name : "",
                        ProvinceId = x.LoanBriefHousehold != null && x.LoanBriefHousehold.ProvinceId > 0 ? x.LoanBriefHousehold.ProvinceId : 0,
                        ProvinceName = x.LoanBriefHousehold != null && x.LoanBriefHousehold.Province != null ? x.LoanBriefHousehold.Province.Name : "",
                    },
                    LoanBriefProperty = new LoanBriefPropertyDTO()
                    {
                        Brand = x.LoanBriefProperty != null && x.LoanBriefProperty.Brand != null ? x.LoanBriefProperty.Brand.Name : "",
                        Product = x.LoanBriefProperty != null && x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.FullName : "",
                        YearMade = x.LoanBriefProperty != null && x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.Year : 0,
                    },
                    CustomerRelationship = x.LoanBriefRelationship != null ? x.LoanBriefRelationship.Select(k => new LoanBriefRelationshipDTO()
                    {
                        RelationshipType = k.RelationshipType,
                        RelationshipName = k.RelationshipTypeNavigation != null ? k.RelationshipTypeNavigation.Name : "",
                        FullName = k.FullName,
                        Phone = k.Phone,
                        LinkCall = "",

                    }).ToList() : new List<LoanBriefRelationshipDTO>(),
                    IsThamDinhNha = x.EvaluationHomeUserId.GetValueOrDefault() > 0 ? 1 : 0,
                    IsThamDinhCongTy = x.EvaluationCompanyUserId.GetValueOrDefault() > 0 ? 1 : 0,
                    EvaluationHomeTime = x.EvaluationHomeTime ?? null,
                    EvaluationCompanyTime = x.EvaluationCompanyTime ?? null,
                    ReceivingMoneyType = x.ReceivingMoneyType.GetValueOrDefault(),
                    BankId = x.BankId ?? 0,
                    BankName = x.Bank != null ? x.Bank.Name : "",
                    BankAccountName = x.BankAccountName ?? "",
                    BankCardNumber = x.BankCardNumber ?? "",
                    BankAccountNumber = x.BankAccountNumber ?? ""
                };
            }
        }
    }

    public class LenderListDisbursementWaiting : LenderLoanBriefDTO
    {
        public static Expression<Func<LoanBrief, LenderListDisbursementWaiting>> ProjectionLenderDisbursementWaiting
        {
            get
            {
                return x => new LenderListDisbursementWaiting()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    LoanAmountFinal = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    RateTypeId = x.RateTypeId,
                    ProductId = x.ProductId,
                    ProductName = x.Product.Name,
                    ProvinceName = x.Province.Name,
                    DistrictName = x.District.Name,
                    WardName = x.Ward.Name

                };
            }
        }


    }
    public class LenderLoanBriefDetail : LenderLoanBriefDetailDTO
    {
        public bool? IsLock { get; set; }
        public bool? BuyInsuranceProperty { get; set; }
        public bool? BuyInsuranceCustomer { get; set; }
        public int? LoanId { get; set; }
        public string Manufacturer { get; set; }
        public string CarName { get; set; }
        public static Expression<Func<LoanBrief, LenderLoanBriefDetail>> ProjectionLenderDetail
        {
            get
            {
                return x => new LenderLoanBriefDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.Customer.FullName,
                    ProductId = x.ProductId,
                    ProductName = x.Product.Name,
                    RateTypeId = x.RateTypeId,
                    Phone = x.Phone,
                    Gender = x.Gender,
                    CardNumber = x.Customer.NationalCard,
                    LoanAmountFinal = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    ProvinceId = x.ProvinceId,
                    ProvinceName = x.Province.Name,
                    DistrictId = x.DistrictId,
                    DistrictName = x.District.Name,
                    WardId = x.WardId,
                    WardName = x.Ward.Name,
                    Address = x.Customer.Address,

                    CompanyName = x.LoanBriefJob.CompanyName,
                    JobTitle = x.LoanBriefJob.JobTitle.Name,
                    CompanyPhone = x.LoanBriefJob.CompanyPhone,
                    CompanyCityName = x.LoanBriefJob.CompanyProvince.Name,
                    CompanyDistrictName = x.LoanBriefJob.CompanyDistrict.Name,
                    CompanyWardName = x.LoanBriefJob.CompanyWard.Name,
                    TotalIncome = x.LoanBriefJob.TotalIncome,
                    CompanyAddress = x.LoanBriefJob.CompanyAddress,
                    Description = x.LoanBriefJob.Description,
                    BankId = x.BankId,
                    BankCode = x.Bank.BankCode,
                    BankAccountName = x.BankAccountName,
                    BankAccountNumber = x.BankAccountNumber,
                    BankCardNumber = x.BankCardNumber,
                    IsLock = x.IsLock,
                    LenderId = x.LenderId,
                    Status = x.Status,
                    LoanId = x.LmsLoanId,
                    Manufacturer = x.LoanBriefProperty != null && x.LoanBriefProperty.Brand != null ? x.LoanBriefProperty.Brand.Name : string.Empty,
                    CarName = x.LoanBriefProperty != null && x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.FullName : string.Empty,
                    BuyInsuranceProperty = x.LoanBriefProperty != null ? x.LoanBriefProperty.BuyInsuranceProperty : false,
                    BuyInsuranceCustomer = x.BuyInsurenceCustomer,
                    EsignState = x.EsignState,
                    LenderRate = x.LenderRate
                };
            }
        }


    }

    public class OverTimeDisbursementWaiting : LenderLoanBriefDTO
    {
        public bool? IsLock { get; set; }
        public static Expression<Func<LoanBrief, OverTimeDisbursementWaiting>> ProjectionOverTimeDisbursementWaiting
        {
            get
            {
                return x => new OverTimeDisbursementWaiting()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    LoanAmountFinal = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    RateTypeId = x.RateTypeId,
                    ProductId = x.ProductId,
                    ProductName = x.Product.Name,
                    ProvinceName = x.Province.Name,
                    DistrictName = x.District.Name,
                    WardName = x.Ward.Name,
                    LenderId = x.LenderId,
                    NextDate = x.NextDate,
                    IsLock = x.IsLock
                };
            }
        }
    }

    public class ERPLoanBriefDetail : ERPLoanBriefDetailDTO
    {
        public static Expression<Func<LoanBrief, ERPLoanBriefDetail>> ProjectionLenderDetail
        {
            get
            {
                return x => new ERPLoanBriefDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.Customer.FullName,
                    Status = x.Status,
                    DeviceId = x.DeviceId
                };
            }
        }


    }

    public class ERPDeviceInfo

    {
        public int LoanbriefId { get; set; }
        public int Status { get; set; }
        public string LoanStatus { get; set; }
        public string FullName { get; set; }
        public string DeviceId { get; set; }
        public int? CodeId { get; set; }
        public static Expression<Func<LoanBrief, ERPDeviceInfo>> ProjectionDetail
        {
            get
            {
                return x => new ERPDeviceInfo()
                {
                    LoanbriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Status = x.Status.GetValueOrDefault(0),
                    DeviceId = x.DeviceId,
                    CodeId = x.CodeId
                };
            }
        }


    }
    public class ERPLoanBriefGetList : ERPLoanBriefGetListDTO
    {
        public static Expression<Func<LoanBrief, ERPLoanBriefGetList>> ProjectionDetail
        {
            get
            {
                return x => new ERPLoanBriefGetList()
                {
                    LoanBriefId = x.LoanBriefId,
                    ToDate = x.ToDate,
                    FullName = x.Customer.FullName,
                    Status = x.Status,
                    ProductId = x.Product.LoanProductId,
                    ProductName = x.Product.Name,
                    CodeId = x.CodeId,
                    DeviceId = x.DeviceId
                };
            }
        }


    }

    public class ERPGetFullNameAndStatusTelesales
    {
        public string full_name { get; set; }
        public string status_telesales { get; set; }
        public string detail_status_telesales { get; set; }
        public int? productId { get; set; }
        public int? telesalesId { get; set; }

        public static Expression<Func<LoanBrief, ERPGetFullNameAndStatusTelesales>> ProjectionDetail
        {
            get
            {
                return x => new ERPGetFullNameAndStatusTelesales()
                {
                    full_name = x.FullName,
                    detail_status_telesales = x.DetailStatusTelesales != null ? Common.Helpers.ExtentionHelper.GetDescription((EnumDetailStatusTelesales)x.DetailStatusTelesales) : null,
                    status_telesales = (x.Status == (int)EnumLoanStatus.TELESALE_ADVICE && x.StatusTelesales == null) ? "Chưa liên hệ" : (x.Status == (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN && x.StatusTelesales == null) ? "Chờ chuẩn bị hồ sơ" : x.Status == (int)EnumLoanStatus.CANCELED ? "Đơn đã hủy" : (x.StatusTelesales != null && x.StatusTelesales.Value == 1) ? "Không liên hệ được" : (x.StatusTelesales != null && x.StatusTelesales.Value == 2) ? "Cân nhắc" : (x.StatusTelesales != null && x.StatusTelesales.Value == 3) ? "Chờ chuẩn bị hồ sơ" : (x.StatusTelesales != null && x.StatusTelesales.Value == 33) ? "Hẹn gọi lại" : null,
                    productId = x.ProductId,
                    telesalesId = x.BoundTelesaleId
                };
            }
        }
    }

    public class LoanBriefInfo : LoanBriefDTO
    {
        public LoanBriefResidentDTO CustomerResident { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefInfo>> ProjectionLoanBriefInfo
        {
            get
            {
                return x => new LoanBriefInfo()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    ProductId = x.ProductId,
                    CustomerResident = new LoanBriefResidentDTO()
                    {
                        ResidentType = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType ?? 0 : 0
                    }

                };
            }
        }


    }

    public class LoanbriefInfoEmployee
    {
        public int LoanBriefId { get; set; }
        public int? LoanId { get; set; }
        public int? TelesaleId { get; set; }
        public int? HubId { get; set; }
        public int? HubEmployId { get; set; }
    }
    public class LoanbriefInfoEmployeeDetail : LoanbriefInfoEmployee
    {
        public static Expression<Func<LoanBrief, LoanbriefInfoEmployeeDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanbriefInfoEmployeeDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    LoanId = x.LmsLoanId,
                    TelesaleId = x.ProductId,
                    HubEmployId = x.HubEmployeeId > 0 ? x.HubEmployeeId : x.EvaluationCompanyUserId > 0 ? x.EvaluationCompanyUserId : x.EvaluationHomeUserId > 0 ? x.EvaluationHomeUserId : 0,
                    HubId = x.HubId
                };
            }
        }


    }

    public class LoanbriefFileInfo
    {
        public string FileThumb { get; set; }
        public string FileOrigin { get; set; }
    }

    public class LoanBriefPropertyInfo
    {
        public int LoanbriefId { get; set; }
        public int? CodeId { get; set; }
        public int? HubId { get; set; }
        public int? HubEmployeeId { get; set; }
        public int LoanStatus { get; set; }
        public string FullName { get; set; }
        public string Brand { get; set; }
        public string Product { get; set; }
        public string PlateNumber { get; set; }
        public string Engine { get; set; }
        public string Chassis { get; set; }
        public List<LoanbriefFileInfo> files { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefPropertyInfo>> Projection
        {
            get
            {
                return x => new LoanBriefPropertyInfo()
                {
                    LoanbriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Brand = x.LoanBriefProperty != null ? x.ProductId == (int)EnumProductCredit.OtoCreditType_CC ? x.LoanBriefProperty.CarManufacturer :
                        x.LoanBriefProperty.Brand != null ? x.LoanBriefProperty.Brand.Name : ""
                        : "",
                    Product = x.LoanBriefProperty != null ? x.ProductId == (int)EnumProductCredit.OtoCreditType_CC ? x.LoanBriefProperty.CarName :
                        x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.Name : ""
                        : "",
                    PlateNumber = x.LoanBriefProperty != null ? x.ProductId == (int)EnumProductCredit.OtoCreditType_CC ? x.LoanBriefProperty.PlateNumberCar : x.LoanBriefProperty.PlateNumber : "",

                    Engine = x.LoanBriefProperty != null ? x.LoanBriefProperty.Engine : "",
                    Chassis = x.LoanBriefProperty != null ? x.LoanBriefProperty.Chassis : "",
                    CodeId = x.CodeId,
                    HubId = x.HubId,
                    LoanStatus = x.Status.GetValueOrDefault(0),
                    HubEmployeeId = x.HubEmployeeId
                };
            }


        }

    }

    public class LoanBriefPropertyInfoErp
    {
        public int LoanbriefId { get; set; }
        public string FullName { get; set; }
        public string Brand { get; set; }
        public string Product { get; set; }
        public string PlateNumber { get; set; }
        public string Engine { get; set; }
        public string Chassis { get; set; }
        public List<LoanbriefFileInfo> files { get; set; }
    }
}
