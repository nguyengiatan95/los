﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.LoanBriefDTO
{
    public class LoanBriefNoteDTO
    {
        public int LoanBriefNoteId { get; set; }
        public int? LoanBriefId { get; set; }
        public int? Type { get; set; }

        public int? UserId { get; set; }
        public string Note { get; set; }
        public int? Status { get; set; }

        public string FullName { get; set; }
        public string ShopName { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
    }

    public class LoanBriefNoteDetail : LoanBriefNoteDTO
    {
        public static Expression<Func<LoanBriefNote, LoanBriefNoteDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefNoteDetail()
                {
                    LoanBriefNoteId = x.LoanBriefNoteId,
                    LoanBriefId = x.LoanBriefId,
                    Type = x.Type,
                    Note = x.Note,
                    Status = x.Status,
                    UserId = x.UserId,
                    FullName = x.FullName,
                    ShopName = x.ShopName,
                    CreatedTime = x.CreatedTime
                };
            }
        }
    }
}
