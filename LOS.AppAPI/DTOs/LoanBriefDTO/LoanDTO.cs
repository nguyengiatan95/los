﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.LoanBriefDTO
{
    public class LoanDTO
    {
        public string FullName { get; set; }
        public int HouseHoldCityId { get; set; }
        public string HouseHoldCity { get; set; }
        public int HouseHoldDistrictId { get; set; }
        public string HouseHoldDistrict { get; set; }
        public int HouseHoldWardId { get; set; }
        public string HouseHoldWard { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int WardId { get; set; }
        public string WardName { get; set; }
        public DateTime? Birthday { get; set; }
        public string JobName { get; set; }
        //chi tiết ngành nghề
        public string Gender { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyDistrict { get; set; }
        public string CompanyWard { get; set; }
        public int CompanyCityId { get; set; }
        public int CompanyDistrictId { get; set; }
        public int CompanyWardId { get; set; }
        public int ResidentType { get; set; }
        public string ResidentName { get; set; }
        public int LoanCredit { get; set; }
        public string PropertyName { get; set; }
        public string BrandProperty { get; set; }
        public string NameProperty { get; set; }
        public int YearProperty { get; set; }
        public string Address { get; set; }
        public string AddressHouseHold { get; set; }
    }
    public class LoanDetail : LoanDTO
    {
        public static Expression<Func<LoanBrief, LoanDetail>> Projection
        {
            get
            {
                return x => new LoanDetail()
                {
                    LoanCredit =  x.LoanBriefId,
                    FullName = x.FullName,
                    HouseHoldCityId = x.LoanBriefHousehold != null ? x.LoanBriefHousehold.ProvinceId.Value : 0,
                    HouseHoldDistrictId = x.LoanBriefHousehold != null ? x.LoanBriefHousehold.DistrictId.Value : 0,
                    HouseHoldWardId = x.LoanBriefHousehold != null ? x.LoanBriefHousehold.WardId.Value : 0,

                    //HouseHoldCity = x.LoanBriefHousehold != null && x.LoanBriefHousehold.Province != null ? x.LoanBriefHousehold.Province.Name : "",
                    //HouseHoldDistrict = x.LoanBriefHousehold != null && x.LoanBriefHousehold.District != null ? x.LoanBriefHousehold.District.Name : "",
                    //HouseHoldWard = x.LoanBriefHousehold != null && x.LoanBriefHousehold.Ward != null ? x.LoanBriefHousehold.Ward.Name : "",
                    CityId = x.ProvinceId.Value,
                    DistrictId = x.DistrictId.Value,
                    WardId = x.WardId.Value,

                    //CityName = x.Province != null ? x.Province.Name : "",
                    //DistrictName = x.District != null ? x.District.Name : "",
                    //WardName = x.Ward != null ? x.Ward.Name : "",
                    Birthday = x.Dob,
                    JobName = x.LoanBriefJob != null && x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.Name : "",
                    Gender = x.Gender != null ? x.Gender == 1 ? "Nữ" : "Nam" : "",
                    CompanyName = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyName : "",
                    CompanyCityId = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyProvinceId.Value : 0,
                    CompanyDistrictId = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyDistrictId.Value : 0,
                    CompanyWardId = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyWardId.Value : 0,
                    ResidentType = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType.Value : 0,
                    PropertyName = x.LoanBriefProperty != null && x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.FullName : "",
                    BrandProperty = x.LoanBriefProperty != null && x.LoanBriefProperty.Brand != null ? x.LoanBriefProperty.Brand.Name : "",
                    NameProperty = x.LoanBriefProperty != null && x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.ShortName : "",
                    YearProperty = x.LoanBriefProperty != null && x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.Year.Value : 0,

                };
            }
        }
    }


    public class LoanInfoItem
    {

        public int LoanbriefId { get; set; }
        public string CustomerName { get; set; }
        public decimal TotalMoney { get; set; }
        public int LoanTime { get; set; }
        public int Status { get; set; }
        public string StatusOfLoanView { get; set; }
        public int StatusOfLoan { get; set; }
    }
    public class LoanInfoDetail : LoanInfoItem
    {

        public static Expression<Func<LoanBrief, LoanInfoItem>> Projection
        {
            get
            {
                return x => new LoanInfoItem()
                {
                    LoanbriefId = x.LoanBriefId,
                    CustomerName = x.FullName,
                    TotalMoney = x.LoanAmount??0,
                    LoanTime = x.LoanTime??0,
                    Status = x.Status??0
                };
            }
        }
    }
}
