﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.LoanBriefDTO
{
    public class UpdatePhoneOther
    {
        public int loanbriefId { get; set; }
        public string phone { get; set; }
        public string application { get; set; }
    }
}
