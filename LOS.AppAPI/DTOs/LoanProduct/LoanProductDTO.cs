﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.LoanProduct
{
    public class LoanProductDTO
    {
        public int LoanProductId { get; set; }
        public string Name { get; set; }
        public int? Status { get; set; }
        public double? ConsultantRate { get; set; }
        public double? ServiceRate { get; set; }
        public double? LoanRate { get; set; }
        public decimal? MaximumMoney { get; set; }
    }

    public class LoanProductDetail : LoanProductDTO
    {
        public static Expression<Func<LOS.DAL.EntityFramework.LoanProduct, LoanProductDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanProductDetail()
                {
                    LoanProductId = x.LoanProductId,
                    Name = x.Name
                };
            }
        }
    }
}
