﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.LoanStatusDetail
{
    public class LoanStatusDetail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<DAL.EntityFramework.LoanStatusDetail> lstStatus { get; set; }
    }
}
