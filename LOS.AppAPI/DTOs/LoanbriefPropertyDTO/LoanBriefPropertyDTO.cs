﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs
{
    public class LoanBriefPropertyDTO
    {
        public int LoanBriefId { get; set; }
        public string Brand { get; set; }
        public string Product { get; set; }
        public int? YearMade { get; set; }
        public string PlateNumber { get; set; }
        public string Chassis { get; set; }
        public string Engine { get; set; }
    }

    public class LMSLoanBriefPropertyDetail : LoanBriefPropertyDTO
    {
        public static Expression<Func<LoanBriefProperty, LMSLoanBriefPropertyDetail>> ProjectionDetail
        {
            get
            {
                return x => new LMSLoanBriefPropertyDetail()
                {
                    LoanBriefId = x.LoanBriefPropertyId,
                    PlateNumber = x.LoanBriefPropertyNavigation.ProductId == (int)EnumProductCredit.OtoCreditType_CC ? x.PlateNumberCar :
                                                x.PlateNumber,
                    Brand = x.LoanBriefPropertyNavigation.ProductId == (int)EnumProductCredit.OtoCreditType_CC ? x.CarManufacturer :
                                                x.Brand != null ? x.Brand.Name : "",
                    Product = x.LoanBriefPropertyNavigation.ProductId == (int)EnumProductCredit.OtoCreditType_CC ? x.CarName :
                                                x.Product != null ? x.Product.FullName : "",
                    YearMade = x.Product != null ? x.Product.Year : 0,
                    Chassis = x.Chassis,
                    Engine = x.Engine

                };
            }
        }
    }
}
