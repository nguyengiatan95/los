﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.DTOs.Webhook
{
    public class AuthenRes
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public string Expiration { get; set; }
        public int GroupId { get; set; }
        public int? IpPhone { get; set; }

        public static Expression<Func<User, AuthenRes>> ProjectionDetail
        {
            get
            {
                return x => new AuthenRes()
                {
                    UserId = x.UserId,
                    Username = x.Username,
                    GroupId = x.GroupId.Value,
                    IpPhone = x.Ipphone
                };
            }
        }
    }
}
