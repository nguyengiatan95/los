﻿using LOS.AppAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LOS.AppAPI.Helpers
{
    public class AuthorizeFilterAttribute: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var controller = context.RouteData.Values["Controller"].ToString();
            var action = context.RouteData.Values["Action"].ToString();
            var identity = (ClaimsPrincipal)context.HttpContext.User;
            var userId  = int.Parse(identity.Claims.Where(c => c.Type == "Id").Select(c => c.Value).SingleOrDefault());
            if (!string.IsNullOrEmpty(controller) && !string.IsNullOrEmpty(action))
            {
                if(userId > 0)
                {
                    IPermissionRequest repo = (IPermissionRequest)context.HttpContext.RequestServices.GetService(typeof(IPermissionRequest));
                    if (!repo.IsPermissionRequest(controller, action, userId))
                        context.Result = new JsonResult(new DefaultResponse<Meta>() { meta = new Meta(StatusCodes.Status401Unauthorized, $"Bạn không có quyền truy cập dữ liệu {controller}/{action}") });
                }
                else
                    context.Result = new JsonResult(new DefaultResponse<Meta>() { meta = new Meta(StatusCodes.Status201Created, $"Không tìm thấy thông tin user truy cập") });
            }
            else
                context.Result = new JsonResult(new DefaultResponse<Meta>() { meta = new Meta(StatusCodes.Status201Created, $"Không tìm thấy thông tin action request") });
            base.OnActionExecuting(context);
        }
    }
}
