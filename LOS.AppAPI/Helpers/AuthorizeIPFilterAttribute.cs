﻿using LOS.AppAPI.Service;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LOS.AppAPI.Helpers
{
    public class AuthorizeIPFilterAttribute : ActionFilterAttribute
    {
        private readonly string _servername;
        public AuthorizeIPFilterAttribute(string servername)
        {
            this._servername = servername;
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            StringValues ipRequestHeader;
            var authIpRequest = context.HttpContext.Request.Headers.TryGetValue("X-Forwarded-For", out ipRequestHeader);
            if (authIpRequest)
            {
                var remoteIp = GetIpRequest(ipRequestHeader);
                if (!string.IsNullOrEmpty(remoteIp))
                {
                    System.Net.IPAddress ipAddress = null;
                    bool isValidIp = System.Net.IPAddress.TryParse(remoteIp, out ipAddress);
                    if (isValidIp)
                    {
                        IPermissionRequest repo = (IPermissionRequest)context.HttpContext.RequestServices.GetService(typeof(IPermissionRequest));
                        if (!repo.IsPermissionIPRequest(_servername, ipAddress))
                        {
                            context.Result = new JsonResult(new DefaultResponse<Meta>() { meta = new Meta(StatusCodes.Status401Unauthorized, $"IP: {remoteIp} của bạn không có quyền truy cập") });
                            //Log.Information(string.Format("UnAuthorizeIPFilter IP: {0}", remoteIp));
                        }
                    }
                    else
                    {
                        context.Result = new JsonResult(new DefaultResponse<Meta>() { meta = new Meta(StatusCodes.Status201Created, $"IP không đúng định dạng: {remoteIp}") });
                    }
                }
                else
                {
                    context.Result = new JsonResult(new DefaultResponse<Meta>() { meta = new Meta(StatusCodes.Status201Created, $"Không tìm thấy remoteIP") });
                }
            }
            base.OnActionExecuting(context);
        }

        private string GetIpRequest(string ipRequestHeader)
        {
            if (!string.IsNullOrEmpty(ipRequestHeader))
            {
                if (ipRequestHeader.Contains(","))
                    return ipRequestHeader.Split(',').First().Trim();
                else return ipRequestHeader.Trim();
            }
            return string.Empty;
        }
    }
}
