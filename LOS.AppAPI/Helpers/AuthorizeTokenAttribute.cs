﻿using LOS.AppAPI.Service.AuthorizeToken;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Helpers
{
    public class AuthorizeTokenAttribute : ActionFilterAttribute
    {
        private readonly string _servername;
        private StringValues _token;

        public AuthorizeTokenAttribute(string servername)
        {
            this._servername = servername;
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var authHeader = context.HttpContext.Request.Headers.TryGetValue("Authorization", out _token);
            if (authHeader)
            {
                IAuthorizeTokenService repo = (IAuthorizeTokenService)context.HttpContext.RequestServices.GetService(typeof(IAuthorizeTokenService));
                if (!repo.CheckAuthorizeTokenRequest(_servername, _token))
                {
                    context.Result = new JsonResult(new DefaultResponse<Meta>() { meta = new Meta(StatusCodes.Status401Unauthorized, $"Token không có quyền truy cập hệ thống") });
                }
            }
            else
            {
                context.Result = new StatusCodeResult(StatusCodes.Status401Unauthorized);
            }
            base.OnActionExecuting(context);
        }
    }
}
