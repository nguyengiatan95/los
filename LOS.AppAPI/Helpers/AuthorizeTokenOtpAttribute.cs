﻿using LOS.AppAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LOS.AppAPI.Helpers
{
    public class AuthorizeTokenOtpAttribute : ActionFilterAttribute
    {
        private StringValues _token;
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var authHeader = context.HttpContext.Request.Headers.TryGetValue("Authorization", out _token);
            if (authHeader)
            {
                var token = _token.ToString();
                if (token.StartsWith("Bearer"))
                    token = token.Substring("Bearer ".Length).Trim();
                IPermissionRequest repo = (IPermissionRequest)context.HttpContext.RequestServices.GetService(typeof(IPermissionRequest));
                //kiểm tra token có bị thay đổi hay không
                if (!repo.VerifyToken(token))
                {
                    context.Result = new JsonResult(new DefaultResponse<Meta>() { meta = new Meta(StatusCodes.Status401Unauthorized, $"Token truy cập không hợp lệ") });
                }
            }
            else
            {
                context.Result = new JsonResult(new DefaultResponse<Meta>() { meta = new Meta(StatusCodes.Status401Unauthorized, $"Vui lòng truyền token để truy cập") });
            }            
            base.OnActionExecuting(context);
        }
    }
}
