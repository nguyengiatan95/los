﻿using LOS.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Helpers
{
    public class CommonHelper
    {
        public static void CaculateTimeGroup(DateTime start, int GroupId, ref int TimeDelay, ref string TimeDelayValue)
        {
            int totaldays = 0;
            double totalhours = 0;
            double totalminutes = 0;
            var ts = ProccessTime(start, DateTime.Now, 0, ref totaldays, ref totalhours, ref totalminutes);
            var groupname = string.Empty;
            var lRet = string.Empty;
            if (GroupId == (int)EnumGroupUser.ApproveEmp)
            {
                if (totalhours > 1 || totalminutes > 30)
                {
                    TimeDelayValue = ts.ToString();
                    TimeDelay = 1;
                }
            }
            else if (GroupId == (int)EnumGroupUser.StaffHub)
            {
                if (totalhours > 1)
                {
                    TimeDelayValue = ts.ToString();
                    TimeDelay = 1;
                }
            }
            else
            {
                TimeDelayValue = ts.ToString();
                TimeDelay = 0;
            }
        }

        public static string ProccessTime(DateTime start, DateTime now, int groupId, ref int totaldays, ref double totalhours, ref double totalminutes)
        {
            DateTime start8h30 = new DateTime(start.Year, start.Month, start.Day, 8, 30, 0);
            DateTime start12h = start8h30.AddHours(3.5);
            DateTime start13h = start12h.AddHours(1);
            DateTime start17h30 = start13h.AddHours(4.5);

            DateTime now8h30 = new DateTime(now.Year, now.Month, now.Day, 8, 30, 0);
            DateTime now12h = now8h30.AddHours(3.5);
            DateTime now13h = now12h.AddHours(1);
            DateTime now17h30 = now13h.AddHours(4.5);

            // Tính lại giờ vào và giờ hiện tại
            // Nếu như nằm trước 8h30 thì gán cho 8h30
            // Nếu như nằm trong khoảng nghỉ trưa từ 12h -> 13h thì gán cho 13h
            // Nếu như nằm sau 17h30 thì gán cho 8h30 ngày hôm sau
            // start
            if (start <= start8h30)
            {
                start = start8h30;
            }
            else if (start >= start12h && start <= start13h)
            {
                start = start13h;
            }
            else if (start >= start17h30)
            {
                start = start.AddDays(1);
                start = new DateTime(start.Year, start.Month, start.Day, 8, 30, 0);
            }
            // now
            if (now <= now8h30)
            {
                now = now8h30;
            }
            else if (now >= now12h && now <= now13h)
            {
                now = now12h;
            }
            else if (now >= now17h30)
            {
                now = now17h30;
            }
            if (start > now)
            {
                return "";
            }

            // Phần này cần sửa lại cho phù hợp đây là ví dụ
            // Tính ngày bỏ đi ngày nghỉ cuối tuần
            if (groupId == 12)
            {
                DateTime prevDate = new DateTime();
                if (now.DayOfWeek == DayOfWeek.Saturday)
                {
                    // Tính thời gian kết thúc vào 17h30 ngày hôm trước
                    prevDate = now.AddDays(-1);
                    now = new DateTime(prevDate.Year, prevDate.Month, prevDate.Day, 17, 30, 0);
                }
                else if (now.DayOfWeek == DayOfWeek.Sunday)
                {
                    // Tính thời gian kết thúc vào 17h30 2 ngày trước
                    prevDate = now.AddDays(-2);
                    now = new DateTime(prevDate.Year, prevDate.Month, prevDate.Day, 17, 30, 0);
                }
                DateTime nextDate = new DateTime();
                if (start.DayOfWeek == DayOfWeek.Saturday)
                {
                    // Tính thời gian bắt đầu vào 8h 30 ngày hôm sau
                    nextDate = start.AddDays(2);
                    start = new DateTime(nextDate.Year, nextDate.Month, nextDate.Day, 8, 30, 0);
                }
                else if (start.DayOfWeek == DayOfWeek.Sunday)
                {
                    // Tính thời gian bắt đầu vào 8h 30 2 ngày sau
                    nextDate = start.AddDays(1);
                    start = new DateTime(nextDate.Year, nextDate.Month, nextDate.Day, 8, 30, 0);
                }
            }
            TimeSpan ts = now - start;
            // Tính tổng số ngày
            int totalDays = Convert.ToInt32(Math.Floor(ts.TotalDays));
            int total = 0;
            DateTime newStart = start.AddDays(totalDays);
            DateTime temp = start;
            for (int i = 0; i < totalDays; i++)
            {
                // Phần này cần sửa lại cho phù hợp đây là ví dụ
                if (groupId == 12)
                {
                    if (temp.DayOfWeek == DayOfWeek.Saturday || temp.DayOfWeek == DayOfWeek.Sunday)
                    {
                        // Không tính thời gian vào cuối tuần
                    }
                    else
                    {
                        total++;
                    }
                }
                else
                {
                    total++;
                }
                temp = temp.AddDays(1);
            }

            return (total > 0 ? total.ToString() + " ngày, " : "") + CalcTime(newStart, now, ref totalhours, ref totalminutes);
        }

        private static string CalcTime(DateTime newStart, DateTime now, ref double totalhours, ref double totalminutes)
        {
            TimeSpan rs = new TimeSpan();
            TimeSpan ts1h = new TimeSpan(1, 0, 0);
            if (newStart.Date == now.Date)
            {
                DateTime start12h = new DateTime(newStart.Year, newStart.Month, newStart.Day, 12, 0, 0);
                DateTime start13h = start12h.AddHours(1);
                DateTime now13h = new DateTime(now.Year, now.Month, now.Day, 13, 0, 0);
                if (now >= now13h && newStart <= start12h)
                {
                    rs = now - newStart - ts1h;
                }
                else
                {
                    rs = now - newStart;
                }
            }
            else
            {
                DateTime nextDate = newStart.AddDays(1);
                DateTime now12h = new DateTime(now.Year, now.Month, now.Day, 12, 0, 0);
                DateTime now13h = now12h.AddHours(1);
                DateTime next13h = new DateTime(nextDate.Year, nextDate.Month, nextDate.Day, 13, 0, 0);
                if (nextDate >= next13h && now <= now12h)
                {
                    rs = nextDate - now - ts1h;
                }
                else
                {
                    rs = nextDate - now;
                }
                rs = new TimeSpan(8, 0, 0) - rs;
            }
            totalhours = rs.TotalHours;
            totalminutes = rs.TotalMinutes;
            return (rs.TotalHours > 1 || rs.TotalMinutes > 1) ? rs.ToString("hh\\:mm") : string.Empty;
        }
    }
}
