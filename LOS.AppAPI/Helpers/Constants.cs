﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Helpers
{
    public class Constants
    {
        public static string API_LMS = "API_LMS";
        public static string InterestAndInsurence = "/api/s2s/GetPaymentSchedulePresume";
        public static string CalculatorMoneyInsurance = "/api/loan/CalculatorMoneyInsurance";
        public static string DisbursementLoanCredit = "/api/loan/DisbursementLoanCreditOfApplender";
        public static string DisbursementLoanCreditV2 = "/api/loan/DisbursementLoanCreditOfAppLenderV2";
        public static string UpdateStatusDeviceToERP = "/api/api/updateProductStatus";
        public static string GetPaymentById = "/api/Loan/GetPaymentLoanAPIForLos?LoanId={0}";
        public static string DeferredPayment = "/api/S2S/CheckCustomerDeferredPayment";
        public static string LoginSSO = "/api/v1.0/User/Login";
        public static string GetInfoLender = "/api/Shop/GetInfoLender?LenderId={0}";
        public static string AddBlacklist = "/api/BlackList/InsertBlackList";
        public static string InterestTopup = "/api/Loan/GetPaymentTopUpLoanAPIForLos";
        public static string GetPayAmountForCustomerByLoanID = "/api/s2s/GetPayAmountForCustomerByLoanID";
        public static string CHECK_LOAN_MOMO = "/amazing-crawler/api/v1/momo/check_loan/{0}";
        public static string CHECK_BANK_ENDPOINT = "/tools/api/v1/check_bank?bank={0}&bank_account={1}";

        public static string APP_ID_TIMA_IT3 = "tima_it3";
        public static string APP_KEY_TIMA_IT3 = "AM59NAXm6NdUo5lDmq0u";
        public static string AI_AUTHENTICATION_ENPOINT = "/security/authentication";
        public static string GET_MOTO_PRICE = "/moto-price/get_price";
        public static string close_contract = "/gps-device/tracking/close-contract";
        public static string open_contract = "/gps-device/tracking/open-contract";
        public static string re_active_imei = "/gps-device/tracking/retry-check-imei";
        public static string APP_ID_VAY_SIEU_NHANH = "service_ai_los";
        public static string APP_KEY_VAY_SIEU_NHANH = "GdJszp99aFYGAtUXOfSu";

        public static string AI_LOG_DISBURSEMENT = "/loan-event-log/loan/disbursement";
        public static string AI_LOG_CLOSING = "/loan-event-log/loan/closing/{0}";

        public static string CISCO_MOBILE_GENERATE_CALLID = "/generate/callId";

        public static string Kalapa_GetFamily = "/api/Kapala/GetFamily?cardNumber={0}";
        public static string ProxyGetInsuranceInfo = "/api/Trandata/GetSiByInfo?nationalId1={0}&nationalId2={1}&fullName={2}&dob={3}";

        public static HashSet<string> HashPathLogRequestAndResponse = new HashSet<string>
        {
             "/API/V1.0/LMS/RETURNLOAN",
            "/API/V1.0/LMS/DISBURSEMENT",
            "/API/V1.0/LMS/LOANLOCK",
            "/API/V1.0/LMS/PUSHLOANLENDER",
             "/API/V1.0/LMS/CHANGE_DISBURSEMENT",


            "/API/V1.0/APPRAISER/PRODUCT_REVIEW",
            "/API/V1.0/APPRAISER/UPDATE_APPRAISER",
            "/API/V1.0/APPRAISER/UPDATELOAN",
          "/API/V1.0/APPRAISER/UPDATECUSTOMER",
            "/API/V1.0/APPRAISER/RETURNLOAN",
            "/API/V1.0/APPRAISER/PUSHLOAN",
           "/API/V1.0/DEVICE/UPDATE_STATUS_DEVICE",
            "/API/V1.0/FINANCE/CREATELOAN",
            "/API/V1.0/LANDINGPAGE/CREATELOAN",
           "/API/V1.0/LANDINGPAGE/CREATE_LOAN_AI",
            "/API/V1.0/LANDINGPAGE/UPDATE_LOAN_AI",
            "/API/V1.0/LENDER/RETURNLOAN",
           "/API/V1.0/LENDER/DISBURSEMENT",
            "/API/V1.0/LENDER/LOANLOCK",

            "/API/V1.0/WEBHOOK/RULE_CAC",
            "/API/V1.0/WEBHOOK/LOCATION_VIETTEL",
             "/API/V1.0/WEBHOOK/LOCATION_MOBI",
            "/API/V1.0/WEBHOOK/REF_PHONE_VIETTEL",
            "/API/V1.0/WEBHOOK/REF_PHONE_MOBIFONE",
              "/API/V1.0/WEBHOOK/CREDIT_SCORING",
            "/API/V1.0/WEBHOOK/ACTIVED_DEVICE",
            "/API/V1.0/WEBHOOK/RESULT_AUTOCALL"
        };

        public static HashSet<string> HashPathNoLogRequestAndResponse = new HashSet<string>
        {
             "/api/v1.0/landingpage/get_districts",
            "/api/v1.0/reportlos/f2l_telesale",
            "/api/v1.0/recording/get_recording_by_user",
            "/api/v1.0/dictionary/type_receive_money",
             "/api/v1.0/dictionary/reasons",
             "/api/v1.0/dictionary/rate_type",
             "/api/v1.0/dictionary/loan_time",
             "/api/v1.0/dictionary/frequency",
             "/api/v1.0/dictionary/all_bank",
             "/api/v1.0/dictionary/districts",
             "/api/v1.0/dictionary/all_product",
             "/api/v1.0/dictionary/provinces",
             "/api/v1.0/appraiser/uploadimagev2",
             "/api/v1.0/file/upload_contract_scan",
             "/api/v1.0/LendingOnline/get_bank",
             "/api/v1.0/lendingonline/get_province",
             "/api/v1.0/lendingonline/get_district",
             "/api/v1.0/lendingonline/get_ward",
             "/api/v1.0/lendingonline/upload_image",
             "/api/v1.0/lendingonline/get_loanpurpose",
             "/api/v1.0/lendingonline/get_job",
             "/api/v1.0/lendingonline/get_job_by_group",
             "/api/v1.0/lendingonline/computer_vision_vr",
             "/api/v1.0/lendingonline/ekyc_national_card",
             "/weatherforecast"
        };

        public static string KEY_CACHE_ACCESSTRADE = "Cache_GetStatusLoanAccessTrade";
        public static string KEY_CACHE_ACCESSTRADE_CPS_XEMAY = "Cache_GetStatusLoanAccessTradeCpsXeMay";
        public static string KEY_CACHE_ACCESSTRADE_CPS_OTO = "Cache_GetStatusLoanAccessTradeCpsOto";
        public static string KEY_CACHE_MASOFFER = "Cache_GetStatusLoanMasOffer";
        public static string KEY_CACHE_LEADGID = "Cache_GetStatusLoanLeadGid";
        public static string KEY_CACHE_SALESDOUBLE = "Cache_GetStatusLoanSalesDouble";
        public static string KEY_CACHE_JEFF = "Cache_GetStatusLoanJeff";
        public static string KEY_CACHE_JEFF_API = "Cache_GetStatusLoanJeffApi";
        public static string KEY_CACHE_GOODAFF = "Cache_GetStatusLoanGoodAff";
        public static string KEY_CACHE_TIMA_DISBURSEMENT = "Cache_GetLoanTimaDisbursement";
        public static string KEY_CACHE_AKAMURO = "Cache_GetStatusLoanAkamuro";
        public static string KEY_CACHE_LOANBRIEF_MM0_TIMA = "Cache_GetLoanBriefMMOTima";
        public static string KEY_CACHE_LOANBRIEF_MM0_TIMA_AFFCODE = "Cache_GetLoanBriefMMOTimaByAffCode";
        public static string KEY_CACHE_REPORT_LOAN_HOME_TIMA = "Cache_ReportDetailLoanHomeTime";
        public static string KEY_CACHE_RENTRACKS_XEMAY = "Cache_GetStatusLoanRentracksXeMay";
        public static string KEY_CACHE_RENTRACKS_CPQL = "Cache_GetStatusLoanRentracksCPQL";
        public static string KEY_CACHE_RENTRACKS_OTO = "Cache_GetStatusLoanRentracksOto";
        public static string KEY_CACHE_DINOS = "Cache_GetStatusLoanDinos";
        public static string KEY_CACHE_KBFINA = "Cache_GetStatusLoanKbFina";
        public static string KEY_CACHE_MASOFFER_CPS_XEMAY = "Cache_GetStatusLoanMasOfferCpsXeMay";

        public static string CheckEmployeeTima = "/api/api/userStatus";
        public static string SEND_SMS = "/api/SMSBrandName/SendSMSBrandNameFPT";
        public static string VOICE_OTP = "/otp/request/";
        public static string CheckDeviceErp = "/api/api/productStatus";

        public static string EKYC_MOTORBIKE_REGISTRATION_CERTIFICATE = "/computer-vision/image/ocr/vr";
        public static string EKYC_NATIONAL_CARD = "/computer-vision/api/ocr/v3/id";
        public static string EKYC_CHECK_LIVENESS_V2 = "/computer-vision/image/face/liveness";

 public static string GET_INFO_CHECK_EKYC = "/computer-vision/ekyc/v2";
        public static string CREDIT_SCORING_DEMO = "/credit-score-by-vehicle/get_credit_score_demo";
        public static string FRAUD_DETECTION_DEMO = "/fraud-service/fraud-check/check-fraud-demo-v2";

        public static string ESIGN_PREPARE_CERTIFICATE_FOR_SIGN_CLOUD = "prepareCertificateForSignCloud";
        public static string ESIGN_PREPARE_FILE_FOR_SIGN_CLOUD = "prepareFileForSignCloud";
        public static string ESIGN_AUTHORIZE_SINGLETON_SIGNING_FOR_SIGN_CLOUD = "authorizeSingletonSigningForSignCloud";
        public static string ESIGN_AUTHORIZE_COUNTER_SIGNING_FOR_SIGN_CLOUD = "authorizeCounterSigningForSignCloud";
        public static string ESIGN_REGENERATE_AUTHORIZATION_CODE_FOR_SIGN_CLOUD = "regenerateAuthorizationCodeForSignCloud";

        public static HashSet<string> HashPhoneTestOtp = new HashSet<string>
        {
            "0394515606",
            "0931115855",
            "0898572528",
            "0358829941",
            "0358829936",
            "0358829931",
            "0358829926",
            "0358829921",
            "0358829916",
            "0358829911",
            "0358829906",
            "0358829901",
            "0358829896",
            "0358829891",
            "0358829886",
            "0358829881",
            "0358829876",
            "0358829871",
            "0987786193",
            "0981221740",
            "0358829866"

        };
    }
}
