﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Helpers
{
    public class CustomResponse
    {
        public class CheckLeadResponseTrustingSocial<T>
        {
            public string status { get; set; }
            public string message { get; set; }
            public string date_time { get; set; }
            public T data { get; set; }
        }
        public class SendLeadResponseTrustingSocial<T>
        {
            public string status { get; set; }
            public string message { get; set; }
            public string date_time { get; set; }
            public T data { get; set; }
        }
    }
}
