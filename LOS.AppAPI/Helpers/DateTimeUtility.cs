﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Helpers
{
    public class DateTimeUtility
    {
        public static string DATE_FORMAT = "dd/MM/yyyy";
        public static string DATE_FORMAT_VN2 = "dd/MM/yyyy";
        public static string DATE_FORMAT_VN = "dd/MM/yyyy";
        public static string DATE_FORMAT_VN3 = "MM/dd/yyyy";
        public static DateTime ConvertStringToDate(string strDate, string format)
        {
            DateTime result;
            try
            {
                IFormatProvider celture = new CultureInfo("fr-FR", true);

                var arrayDate = strDate.Split('/');
                if (arrayDate[0].Length < 2)
                {
                    arrayDate[0] = "0" + arrayDate[0];
                }
                if (arrayDate[1].Length < 2)
                {
                    arrayDate[1] = "0" + arrayDate[1];
                }
                strDate = arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2];
                result = DateTime.ParseExact(strDate, format, celture, DateTimeStyles.NoCurrentDateDefault);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void ConvertDateRanger(string DateRanger, ref DateTime fromDate, ref DateTime toDate)
        {
            try
            {
                string[] arrdates = DateRanger.Split('-'); ;
                if (arrdates != null)
                {
                    fromDate = DateTimeUtility.ConvertStringToDate(arrdates[0].Trim(), DateTimeUtility.DATE_FORMAT).AddDays(-1);
                    toDate = DateTimeUtility.ConvertStringToDate(arrdates[1].Trim(), DateTimeUtility.DATE_FORMAT).AddDays(+1);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void ConvertDateRangerV2(string DateRanger, ref DateTime fromDate, ref DateTime toDate)
        {
            try
            {
                string[] arrdates = DateRanger.Split('-'); ;
                if (arrdates != null)
                {
                    fromDate = DateTimeUtility.ConvertStringToDate(arrdates[0].Trim(), DateTimeUtility.DATE_FORMAT);
                    toDate = DateTimeUtility.ConvertStringToDate(arrdates[1].Trim(), DateTimeUtility.DATE_FORMAT).AddDays(+1);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static DateTime FirstDayOfWeek(DateTime dt)
        {
            // set monday is day start of week
            DayOfWeek startOfWeek = DayOfWeek.Monday;
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }
        public static DateTime LastDayOfWeek(DateTime date)
        {
            DateTime ldowDate = FirstDayOfWeek(date).AddDays(6);
            return ldowDate;
        }

        public static DateTime FromeDate(DateTime dt)
        {
            DateTime defaultFromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            TimeSpan t = TimeSpan.FromDays(7);
            defaultFromDate = dt.Subtract(t);
            return defaultFromDate.Date.AddHours(00).AddMinutes(00).AddSeconds(00);
        }

        public static DateTime ToDate(DateTime dt)
        {
            return dt.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
        }

        public static DateTime ConvertGMTUP(DateTime date)
        {
            DateTime dt = date.AddHours(+7);
            return dt;
        }
        public static DateTime ConvertGMTDOW(DateTime date)
        {
            DateTime dt = date.AddHours(-7);
            return dt;
        }

        public static string ConvertHHmmss(long seconds)
        {
            TimeSpan time = TimeSpan.FromSeconds(seconds);
            //here backslash is must to tell that colon is
            //not the part of format, it just a character that we want in output
            return time.ToString(@"hh\:mm\:ss");
        }
    }
}
