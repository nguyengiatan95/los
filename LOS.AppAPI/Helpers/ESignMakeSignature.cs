﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;


namespace LOS.AppAPI.Helpers
{
    class ESignMakeSignature
    {
        private String data;
        private String key;
        private String passKey;

        public ESignMakeSignature(String data, String PriKeyPath, String PriKeyPass)
        {
            this.data = data;
            this.key = PriKeyPath;
            this.passKey = PriKeyPass;
        }

        public String getSignature()
        {
            RSACryptoServiceProvider key = GetKey();
            return Sign(this.data, key);
        }

        public static string Sign(string content, RSACryptoServiceProvider rsa)
        {
            RSACryptoServiceProvider crsa = rsa;
            byte[] Data = Encoding.UTF8.GetBytes(content);
            byte[] signData = crsa.SignData(Data, "sha1");
            return Convert.ToBase64String(signData);

        }
        private RSACryptoServiceProvider GetKey()
        {
            X509Certificate2 cert2 = new X509Certificate2(this.key, this.passKey, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
            var rsa = cert2.GetRSAPrivateKey();
            var paramemters = rsa.ExportParameters(true);
            RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
            csp.ImportParameters(paramemters);
            return csp;            
        }

        public static string getPKCS1Signature(string data, string key, string passkey)
        {
            ESignMakeSignature mks = new ESignMakeSignature(data, key, passkey);
            return mks.getSignature();
        }

        private static readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public static long CurrentTimeMillis()
        {
            return (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
        }

        private static long nanoTime()
        {
            long nano = 10000L * Stopwatch.GetTimestamp();
            nano /= TimeSpan.TicksPerMillisecond;
            nano *= 100L;
            return nano;
        }

        internal static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static byte[] Base64Encode(byte[] rawData)
        {
            //Console.WriteLine(Encoding.Default.GetString(rawData));
            var data = System.Convert.ToBase64String(rawData);
            return System.Text.Encoding.UTF8.GetBytes(data);
        }
        public static byte[] Base64Decode(byte[] base64EncodedData)
        {
            var data = System.Text.Encoding.UTF8.GetString(base64EncodedData);
            var base64EncodedBytes = System.Convert.FromBase64String(data);
            //Console.WriteLine(Encoding.Default.GetString(base64EncodedBytes));
            return base64EncodedBytes;
        }
    }
}
