﻿using LOS.AppAPI.Models.Mongo;
using LOS.AppAPI.Service.MongoService;
using LOS.Common.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IO;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.AppAPI.Helpers
{
    public class LoggingRequestAndResponseMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogRequestResponse _logging;
        private IConfiguration _baseConfig;
        public LoggingRequestAndResponseMiddleware(RequestDelegate next, ILogRequestResponse logging, IConfiguration baseConfig)
        {
            _next = next;
            _logging = logging;
            _baseConfig = baseConfig;
        }
        public async Task Invoke(HttpContext context)
        {
            bool isSaveLog = false;
            //kiểm tra cấu hình có lưu log không
            if (_baseConfig["AppSettings:SaveLogRequestAndResponse"] == "1")
            {
                if (!string.IsNullOrEmpty(context.Request.Path))
                {
                    var path = context.Request.Path.ToString();
                    if (!Constants.HashPathNoLogRequestAndResponse.Contains(path.AsToLower()))
                        isSaveLog = true;
                }
            }
            var objLog = await FormatRequest(context.Request, isSaveLog);
            
            var originalBodyStream = context.Response.Body;

            using (var responseBody = new MemoryStream())
            {
                context.Response.Body = responseBody;
                await _next(context);
                var sResponse = await FormatResponse(context.Response, isSaveLog);
                if (isSaveLog)
                {
                    objLog.ResponseAt = DateTime.Now;
                    objLog.ResponseBody = sResponse;
                    //Log thông tin request => db               
                    _logging.Create(objLog);
                }
                await responseBody.CopyToAsync(originalBodyStream);
            }
        }

        private async Task<LogAppAPI> FormatRequest(HttpRequest request, bool isSaveLog = false)
        {
            request.EnableBuffering();
            var body = request.Body;
            var buffer = new byte[Convert.ToInt32(request.ContentLength)];
            await request.Body.ReadAsync(buffer, 0, buffer.Length);
            var bodyAsText = Encoding.UTF8.GetString(buffer);
            body.Seek(0, SeekOrigin.Begin);
            request.Body = body;

            var log = new LogAppAPI();
            if (isSaveLog)
                log = await LogRequest(request, bodyAsText);
            return log;
        }

        private async Task<string> FormatResponse(HttpResponse response, bool isSaveLog = false)
        {
            var text = string.Empty;
            response.Body.Seek(0, SeekOrigin.Begin);
            if (isSaveLog)
                text = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);
            return text;
        }
        private async Task<LogAppAPI> LogRequest(HttpRequest request, string bodyAsText)
        {
            try
            {
                string headers = String.Empty;
                string token = String.Empty;
                string userName = String.Empty;
                string ip = String.Empty;
                foreach (var key in request.Headers.Keys)
                {
                    headers += key + "=" + request.Headers[key] + Environment.NewLine;
                    if (key == "Authorization")
                        token = request.Headers[key];
                    if (key == "UserName")
                        userName = request.Headers[key];
                    if (key == "X-Forwarded-For")
                        ip = request.Headers[key];
                }
                if (!string.IsNullOrEmpty(ip))
                {
                    if (ip.Contains(","))
                        ip = ip.Split(',').First().Trim();
                    else
                        ip = ip.Trim();
                }

                var log = new LogAppAPI
                {
                    Host = request.Host.ToString(),
                    Path = request.Path.ToString(),
                    Token = token,
                    UserName = userName,
                    IPRequest = ip,
                    Method = request.Method.ToString(),
                    Header = headers,
                    QueryString = request.QueryString.ToString(),
                    RequestBody = bodyAsText,
                    RequestAt = DateTime.Now
                };
                return log;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}