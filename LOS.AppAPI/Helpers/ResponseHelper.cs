﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Helpers
{
    public class ResponseHelper
    {
        public static int SUCCESS_CODE = 200;
        public static string SUCCESS_MESSAGE = "success";
        public static int FAIL_CODE = 201;
        public static string FAIL_MESSAGE = "fail";
        public static int INTERNAL_SERVER_ERROR_CODE = 500;
        public static string INTERNAL_SERVER_ERROR_MESSAGE = "internal server error";
        public static int BAD_REQUEST_CODE = 400;
        public static string BAD_REQUEST_MESSAGE = "bad request";
        public static int NOT_FOUND_CODE = 404;
        public static string NOT_FOUND_MESSAGE = "not found";
        public static int BAD_INPUT_CODE = 210;
        public static string BAD_INPUT_MESSAGE = "bad / invalid input";
        public static int USER_WRONG_PASSWORD_CODE = 211;
        public static string USER_WRONG_PASSWORD_MESSAGE = "wrong password";
        public static int CUSTOMER_BLACKLIST_CODE = 212;
        public static string CUSTOMER_BLACKLIST_MESSAGE = "customer is on the black list";
        public static int UNAUTHORIZED_CODE = 401;
        public static string UNAUTHORIZED_MESSAGE = "UNAUTHORIZED";

    }
}
