﻿
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LOS.AppAPI.Helpers
{
    public class Ultility
    {
        private static IHttpClientFactory clientFactory;

        public Ultility(IHttpClientFactory _clientFactory)
        {
            clientFactory = _clientFactory;
        }

        public static List<ProvinceDetail> provinces;
        public static List<LoanStatus> loanStatutes;
        public static List<PipelineState> pipelineStates;
        public static List<LoanProduct> loanProducts;
        public static List<PropertyDTO> properties;
        public static List<HubType> hubTypes;
        public static List<EventConfig> eventConfigs;
        public static List<TypeRemarketing> typeRemarketings;
        public static List<DocumentTypeNew> GetListDocumentNew(List<DocumentTypeDetail> entity)
        {
            var lst = new List<DocumentTypeNew>();
            foreach (var item in entity)
            {
                if (item.ParentId.GetValueOrDefault(0) == 0)
                {
                    var lstdocChild = entity.Where(x => x.ParentId == item.Id).ToList();
                    if (lstdocChild != null && lstdocChild.Count > 0)
                    {
                        var itemChild = new List<DocumentTypeDTO>();
                        for (int i = 0; i < lstdocChild.Count; i++)
                        {
                            itemChild.Add(new DocumentTypeDTO
                            {
                                Id = lstdocChild[i].Id,
                                Name = lstdocChild[i].Name,
                                Guide = lstdocChild[i].Guide,
                                ParentId = lstdocChild[i].ParentId,
                                NumberImage = lstdocChild[i].NumberImage,
                                ProductId = lstdocChild[i].ProductId
                            });
                        }
                        lst.Add(new DocumentTypeNew
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Guide = item.Guide,
                            ParentId = item.ParentId,
                            LstDocumentChild = itemChild,
                            NumberImage = item.NumberImage
                        });
                    }
                    else
                    {
                        lst.Add(new DocumentTypeNew
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Guide = item.Guide,
                            ParentId = item.ParentId,
                            NumberImage = item.NumberImage
                        });
                    }

                }

            }
            return lst;

        }

        public static List<DocumentTypeNewV2> GetListDocumentNewV2(List<DocumentTypeDetail> entity, int TypeOwnerShip, int ObjectRuleType, List<LoanBriefFileDetail> files, string ServiceURL,
            string ServiceURLAG, string ServiceURLFileTima)
        {
            var lst = new List<DocumentTypeNewV2>();
            var typeOwnerShip = 0;
            if (TypeOwnerShip == (int)EnumTypeofownership.NhaSoHuuTrungSHK || TypeOwnerShip == (int)EnumTypeofownership.NhaSoHuuKhongTrungSHK)
                typeOwnerShip = 1;
            else
                typeOwnerShip = 2;
            entity = entity.Where(x => (x.TypeOwnerShip == typeOwnerShip || x.TypeOwnerShip == 0) && (x.ObjectRuleType.Contains(ObjectRuleType.ToString()) || x.ObjectRuleType == "0")).ToList();
            //entity = entity.Where(x => (x.TypeOwnerShip == typeOwnerShip || x.TypeOwnerShip == 0)).ToList();
            foreach (var item in entity)
            {
                if (item.ParentId.GetValueOrDefault(0) == 0)
                {
                    //var lstdocChild = new List<DocumentTypeDetail>();
                    var lstdocChild = entity.Where(x => x.ParentId == item.Id).ToList();
                    if (lstdocChild != null && lstdocChild.Count > 0)
                    {
                        var itemChild = new List<DocumentTypeDTOV2>();

                        for (int i = 0; i < lstdocChild.Count; i++)
                        {
                            var fileChild = files.Where(x => x.TypeId == lstdocChild[i].Id).ToList();
                            foreach (var itemFile in fileChild)
                            {
                                if (!itemFile.FilePath.Contains("http") && (itemFile.MecashId == null || itemFile.MecashId == 0))
                                {
                                    itemFile.FilePath = ServiceURL + itemFile.FilePath;
                                    if (!string.IsNullOrEmpty(itemFile.FileThumb))
                                        itemFile.FileThumb = ServiceURL + itemFile.FileThumb;
                                }
                                else if (itemFile.MecashId > 0 && (itemFile.S3status == 0 || itemFile.S3status == null))
                                {
                                    itemFile.FilePath = ServiceURLAG + itemFile.FilePath;
                                    if (!string.IsNullOrEmpty(itemFile.FileThumb))
                                        itemFile.FileThumb = ServiceURLAG + itemFile.FileThumb;
                                }
                                else if (itemFile.MecashId > 0 && itemFile.S3status == 1 && !itemFile.FilePath.Contains("http"))
                                {
                                    itemFile.FilePath = ServiceURLFileTima + itemFile.FilePath;
                                    if (!string.IsNullOrEmpty(itemFile.FileThumb))
                                        itemFile.FileThumb = ServiceURLFileTima + itemFile.FileThumb;
                                }

                            }
                            itemChild.Add(new DocumentTypeDTOV2
                            {
                                Id = lstdocChild[i].Id,
                                Name = lstdocChild[i].Name,
                                Guide = lstdocChild[i].Guide,
                                ParentId = lstdocChild[i].ParentId,
                                NumberImage = lstdocChild[i].NumberImage,
                                ProductId = lstdocChild[i].ProductId,
                                LstLoanBriefFiles = fileChild
                            });
                        }
                        lst.Add(new DocumentTypeNewV2
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Guide = item.Guide,
                            ParentId = item.ParentId,
                            LstDocumentChild = itemChild,
                            NumberImage = item.NumberImage,
                            Required = item.Required
                        });
                    }
                    else
                    {
                        var file = files.Where(x => x.TypeId == item.Id).ToList();
                        foreach (var itemFile in file)
                        {
                            if (!itemFile.FilePath.Contains("http") && (itemFile.MecashId == null || itemFile.MecashId == 0))
                            {
                                itemFile.FilePath = ServiceURL + itemFile.FilePath;
                                if (!string.IsNullOrEmpty(itemFile.FileThumb))
                                    itemFile.FileThumb = ServiceURL + itemFile.FileThumb;
                            }
                            else if (itemFile.MecashId > 0 && (itemFile.S3status == 0 || itemFile.S3status == null))
                            {
                                itemFile.FilePath = ServiceURLAG + itemFile.FilePath;
                                if (!string.IsNullOrEmpty(itemFile.FileThumb))
                                    itemFile.FileThumb = ServiceURLAG + itemFile.FileThumb;
                            }
                            else if (itemFile.MecashId > 0 && itemFile.S3status == 1 && !itemFile.FilePath.Contains("http"))
                            {
                                itemFile.FilePath = ServiceURLFileTima + itemFile.FilePath;
                                if (!string.IsNullOrEmpty(itemFile.FileThumb))
                                    itemFile.FileThumb = ServiceURLFileTima + itemFile.FileThumb;
                            }

                        }
                        lst.Add(new DocumentTypeNewV2
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Guide = item.Guide,
                            ParentId = item.ParentId,
                            NumberImage = item.NumberImage,
                            Required = item.Required,
                            LstLoanBriefFiles = file
                        });
                    }

                }

            }
            return lst;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listData">Danh sách chứng từ theo gói sản phẩm và hình thức sở hưu nhà</param>
        /// <param name="listFiles">Danh sách file của đơn đó</param>
        /// <param name="fromUpload">Cho phép upload từ: web, cvkd, tima care</param>
        /// <param name="groupJobId">Nhóm công việc kh: chia làm 6 nhóm</param>
        /// <returns></returns>
        public static List<DocumentMapInfo> ConvertDocumentTree(List<DocumentMapInfo> listData, List<LoanBriefFileDetail> listFiles, int fromUpload = 0, int groupJobId = 0)
        {

            var result = new List<DocumentMapInfo>();

            #region Xử lý dữ liệu cho dictionary
            //Tạo dictionary theo key: loại chứng từ
            //Value: D/s chứng từ của loại đó
            var dicFiles = new Dictionary<int, List<LoanBriefFileDetail>>();
            foreach (var item in listFiles)
            {
                if (item.TypeId > 0)
                {
                    var key = item.TypeId.Value;
                    if (!dicFiles.ContainsKey(key))
                        dicFiles[key] = new List<LoanBriefFileDetail>();
                    dicFiles[key].Add(item);
                }
                else
                {
                    //Nếu không có danh mục chứng từ => add vào danh mục không xác định
                    var key = -100;
                    if (!dicFiles.ContainsKey(key))
                        dicFiles[key] = new List<LoanBriefFileDetail>();
                    dicFiles[key].Add(item);
                }

            }

            //Tạo HashSet chứa các loại chứng từ nằm trong danh mục của bộ sản phẩm
            HashSet<int> hashDocument = new HashSet<int>();

            //Tạo dictionary với key: ID của chứng từ
            //Value: D/s chứng từ con của ID đó
            var dicData = new Dictionary<int, List<DocumentMapInfo>>();
            foreach (var item in listData)
            {
                if (!hashDocument.Contains(item.Id))
                    hashDocument.Add(item.Id);
                if (item.ParentId > 0)
                {
                    var key = item.ParentId.Value;
                    if (!dicData.ContainsKey(key))
                        dicData[key] = new List<DocumentMapInfo>();
                    dicData[key].Add(item);
                }
            }
            #endregion

            foreach (var item in listData)
            {
                //xử lý những chứng từ không có cha
                if (item.ParentId.GetValueOrDefault(0) == 0)
                {
                    var itemDocumentMap = new DocumentMapInfo()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Guide = item.Guide,
                        ParentId = item.ParentId,
                        ProductId = item.ProductId,
                        NumberImage = item.NumberImage,
                        Required = item.Required,
                        TypeRequired = item.TypeRequired,
                        AllowFromUpload = item.AllowFromUpload,
                        GroupJobId = item.GroupJobId,
                        ListChilds = new List<DocumentMapInfo>(),
                        ListFiles = new List<LoanBriefFileDetail>()

                    };
                    //Nếu xác định nguồn upload
                    if (fromUpload > 0 && !string.IsNullOrEmpty(itemDocumentMap.AllowFromUpload))
                    {
                        if (!string.IsNullOrEmpty(itemDocumentMap.AllowFromUpload))
                        {
                            var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(itemDocumentMap.AllowFromUpload);
                            if (arr != null && !arr.Contains(fromUpload))
                                continue;
                        }
                    }
                    //Nếu xác nhóm công việc
                    if (groupJobId > 0 && !string.IsNullOrEmpty(itemDocumentMap.GroupJobId))
                    {
                        if (!string.IsNullOrEmpty(itemDocumentMap.GroupJobId))
                        {
                            var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(itemDocumentMap.GroupJobId);
                            if (arr != null && !arr.Contains(groupJobId))
                                continue;
                        }
                    }
                    //kiểm tra xem có file không
                    if (dicFiles.ContainsKey(itemDocumentMap.Id))
                        itemDocumentMap.ListFiles = dicFiles[itemDocumentMap.Id];
                    var key = item.Id;
                    if (dicData.ContainsKey(item.Id))
                    {
                        //danh sách con
                        var childs = dicData[item.Id];
                        foreach (var child in childs)
                        {
                            var childItem = new DocumentMapInfo()
                            {
                                Id = child.Id,
                                Name = child.Name,
                                Guide = child.Guide,
                                ParentId = child.ParentId,
                                ProductId = child.ProductId,
                                NumberImage = child.NumberImage,
                                Required = child.Required,
                                TypeRequired = child.TypeRequired,
                                AllowFromUpload = child.AllowFromUpload,
                                GroupJobId = child.GroupJobId,
                                ListChilds = new List<DocumentMapInfo>(),
                                ListFiles = new List<LoanBriefFileDetail>()
                            };
                            //Nếu xác định nguồn upload
                            if (fromUpload > 0 && !string.IsNullOrEmpty(childItem.AllowFromUpload))
                            {
                                if (!string.IsNullOrEmpty(childItem.AllowFromUpload))
                                {
                                    var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(childItem.AllowFromUpload);
                                    if (arr != null && !arr.Contains(fromUpload))
                                        continue;
                                }
                            }
                            //Nếu xác nhóm công việc
                            if (groupJobId > 0 && !string.IsNullOrEmpty(childItem.GroupJobId))
                            {
                                if (!string.IsNullOrEmpty(childItem.GroupJobId))
                                {
                                    var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(childItem.GroupJobId);
                                    if (arr != null && !arr.Contains(groupJobId))
                                        continue;
                                }
                            }
                            //kiểm tra xem có file không
                            if (dicFiles.ContainsKey(childItem.Id))
                                childItem.ListFiles = dicFiles[childItem.Id];
                            //Nếu có con tiếp => add vào 
                            if (dicData.ContainsKey(child.Id))
                            {
                                var lstChild = dicData[child.Id].OrderBy(x => x.Id);
                                foreach (var child2 in lstChild)
                                {
                                    var childItem2 = new DocumentMapInfo()
                                    {
                                        Id = child2.Id,
                                        Name = child2.Name,
                                        Guide = child2.Guide,
                                        ParentId = child2.ParentId,
                                        ProductId = child2.ProductId,
                                        NumberImage = child2.NumberImage,
                                        Required = child2.Required,
                                        TypeRequired = child2.TypeRequired,
                                        AllowFromUpload = child2.AllowFromUpload,
                                        GroupJobId = child2.GroupJobId,
                                        ListChilds = new List<DocumentMapInfo>(),
                                        ListFiles = new List<LoanBriefFileDetail>()
                                    };

                                    //Nếu xác định nguồn upload
                                    //if (fromUpload > 0 && !string.IsNullOrEmpty(childItem2.AllowFromUpload))
                                    //{
                                    //    if (!string.IsNullOrEmpty(childItem2.AllowFromUpload))
                                    //    {
                                    //        var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(childItem2.AllowFromUpload);
                                    //        if (arr != null && !arr.Contains(fromUpload))
                                    //            continue;
                                    //    }
                                    //}
                                    ////Nếu xác nhóm công việc
                                    if (groupJobId > 0 && !string.IsNullOrEmpty(childItem2.GroupJobId))
                                    {
                                        if (!string.IsNullOrEmpty(childItem2.GroupJobId))
                                        {
                                            var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(childItem2.GroupJobId);
                                            if (arr != null && !arr.Contains(groupJobId))
                                                continue;
                                        }
                                    }
                                    //kiểm tra xem có file không
                                    if (dicFiles.ContainsKey(childItem2.Id))
                                        childItem2.ListFiles = dicFiles[childItem2.Id];
                                    childItem.ListChilds.Add(childItem2);
                                }
                            }
                            itemDocumentMap.ListChilds.Add(childItem);
                        }
                    }
                    //Thêm chứng từ đó vào kết quả trả về                  
                    result.Add(itemDocumentMap);
                }
            }
            //Kiểm tra xem có file nào khong nằm trong d/s chứng từ của bộ sản phẩm không 
            //=> Gán thành 1 danh mục chứng từ không xác định
            var documentUnknown = new DocumentMapInfo()
            {
                Name = "Không xác định",
                ListFiles = new List<LoanBriefFileDetail>()
            };
            foreach (var key in dicFiles.Keys)
            {
                if (!hashDocument.Contains(key))
                {
                    var files = dicFiles[key];
                    if (files.Count > 0)
                        documentUnknown.ListFiles.AddRange(files);
                }
            }
            if (documentUnknown.ListFiles != null && documentUnknown.ListFiles.Count > 0)
                result.Add(documentUnknown);
            return result;

        }
    }
}
