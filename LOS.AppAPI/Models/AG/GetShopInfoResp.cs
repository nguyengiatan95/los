﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.AG
{
	public class GetShopInfoResp
	{
		public ShopInfo Data { get; set; }
		public int Status { get; set; }
	}

	public class ShopInfo
	{
		public int ShopID { get; set; }
		public decimal RateLender { get; set; }
		public string Name { get; set; }
		public string Represent { get; set; }
		public string Phone { get; set; }
		public string AgreementId { get; set; }
		public string Passcode { get; set; }

		public string PersonContact { get; set; }
		public string PersonCardNumber { get; set; }
		public DateTime? PersonBirthDay { get; set; }
		public string Address { get; set; }
		public string AddressOfResidence { get; set; }
	}
}
