﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.AG
{
    public class RequestCheckBlackListAg
    {
        public string NumberCard { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
    }

    public class RequestPayAmount
    {
        public string Phone { get; set; }
        public string NumberCard { get; set; }
    }
}
