﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.AG
{
    public class ResponseCheckBlackListAg
    {
        public bool Data { get; set; }
        public List<string> Messages { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }
        public int Total { get; set; }
    }
   
}
