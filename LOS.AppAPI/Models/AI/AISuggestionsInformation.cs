﻿namespace LOS.AppAPI.Models.AI
{
    public class AISuggestionsInformation
    {
        public class PriceMotorReq
        {
            public string brand { get; set; }
            public string model { get; set; }
            public string regdate { get; set; }
            public string phanh { get; set; }
            public string vanh { get; set; }
            public string token { get; set; }
            public string access_token { get; set; }
            public int LoanBriefId { get; set; }
        }
    }
}
