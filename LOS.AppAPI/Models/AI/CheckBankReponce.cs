﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.AI
{
    public class CheckBankReponce
    {
        public long? Status { get; set; }
        public CheckBankData Data { get; set; }
        public string Messages { get; set; }
    }
    public class CheckBankData
    {
        public string AccId { get; set; }
        public string AccName { get; set; }
        public string Bank { get; set; }
    }
}
