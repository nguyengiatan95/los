﻿using Amazon.S3.Model;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.AI
{
	public class CheckLivenessReq
	{	
		public IFormFile selfie { get; set; }		
		public List<IFormFile> files { get; set; }
		public int loanBriefId { get; set; }
	}

	public class CheckLivenessBase64Req
	{
		public string selfie { get; set; }
		public List<string> files { get; set; }
		public int loanBriefId { get; set; }
	}

	public class CheckLivenessVideoReq
	{
		public IFormFile selfie { get; set; }
		public IFormFile video { get; set; }
		public int loanBriefId { get; set; }
	}

	public class CheckLivenessV2
	{
		[JsonProperty("status_code")]
		public int StatusCode { get; set; }
		[JsonProperty("status")]
		public string Status { get; set; }
		[JsonProperty("result")]
		public CheckLivenessV2Data Result {get;set;}
	}

	public class CheckLivenessV2Data
	{
		[JsonProperty("is_live_action")]
		public bool? IsLiveAction { get; set; }
		[JsonProperty("num_action")]
		public int? NumAction { get; set; }
		[JsonProperty("is_match_face")]
		public bool? IsMatchFace { get; set; }
		[JsonProperty("match_face_score")]
		public float MatchFaceScore { get; set; }
		[JsonProperty("is_fake")]
		public bool? IsFake { get; set; }
		[JsonProperty("fake_score")]
		public float FakeScore { get; set; }
	}
}
