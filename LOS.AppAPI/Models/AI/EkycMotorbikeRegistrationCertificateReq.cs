﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class EkycMotorbikeRegistrationCertificateReq
    {
        [JsonProperty("images")]
        public List<string> images { get; set; }

        [JsonProperty("loan_brief_id")]
        public int LoanbriefId { get; set; }
        [JsonProperty("name")]
        public string FullName { get; set; }
        [JsonProperty("ownership")]
        public Nullable<bool> Ownership { get; set; }
    }

    public class EkycMotorbikeRegistrationCertificateRes
    {
        [JsonProperty("status_code")]
        public int Code { get; set; }
        [JsonProperty("cards")]
        public List<Cards> Cards { get; set; }

    }

    public class Cards
    {
        [JsonProperty("info")]
        public MotorbikeRegistrationInfo Info { get; set; }
    }

    public class MotorbikeRegistrationInfo
    {
        [JsonProperty("plate")]
        public string Plate { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("district")]
        public string District { get; set; }

        [JsonProperty("brand")]
        public string Brand { get; set; }
        [JsonProperty("engine")]
        public string Engine { get; set; }
        [JsonProperty("chassis")]
        public string Chassis { get; set; }
    }
}
