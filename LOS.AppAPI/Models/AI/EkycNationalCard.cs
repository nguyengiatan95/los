﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.AI
{
    public class EkycNationalCard
    {
        public partial class Output
        {
            [JsonProperty("status")]
            public string Status { get; set; }

            [JsonProperty("status_code")]
            public long StatusCode { get; set; }

            [JsonProperty("has_card")]
            public bool HasCard { get; set; }

            [JsonProperty("cards")]
            public List<Card> Cards { get; set; }
        }
        public partial class Card
        {
            [JsonProperty("side")]
            public string Side { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("gray_image")]
            public bool GrayImage { get; set; }

            [JsonProperty("corner_cut")]
            public bool CornerCut { get; set; }

            [JsonProperty("missing_corner")]
            public bool MissingCorner { get; set; }

            [JsonProperty("info")]
            public Info Info { get; set; }
        }
        public partial class Info
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("domicile")]
            public string Domicile { get; set; }

            [JsonProperty("country")]
            public object Country { get; set; }

            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("birthday")]
            public string Birthday { get; set; }

            [JsonProperty("expiry")]
            public object Expiry { get; set; }

            [JsonProperty("gender")]
            public object Gender { get; set; }

            [JsonProperty("ethnicity")]
            public object Ethnicity { get; set; }

            [JsonProperty("issue_by")]
            public object IssueBy { get; set; }

            [JsonProperty("issue_date")]
            public object IssueDate { get; set; }


        }
    }
}
