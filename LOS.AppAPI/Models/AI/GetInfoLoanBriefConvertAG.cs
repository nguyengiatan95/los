﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class GetInfoLoanBriefConvertAG
    {
        public string phone { get; set; }
        public string national_id { get; set; }
        public string full_name { get; set; }
        public int manufacturerId { get; set; }//id hãng xe 
        public string yearMade { get; set; }// id model xe 
        public string vehicle_full_name { get; set; }// tên đầy đủ của xe (vd: honda airblade 2019)
        public string current_address { get; set; }//địa chỉ hiện tại  
        public int current_address_city_id { get; set; }// id của tỉnh hiện tại
        public int current_address_district_id { get; set; }//id của thành huyện hiện tại
        public int current_address_ward_id { get; set; }//id của thành xã hiện tại
        public int home_ownership_type_id { get; set; }//id loại hình sở hữu nhà
        public string reference_phone { get; set; }//số điện thoại người tham chiếu
        public string reference_name { get; set; }//tên người tham chiếu
        public int reference_relationship_id { get; set; }//id của mối quan hệ tham chiếu 
        public int bank_id { get; set; }// id của ngân hàng 
        public string bank_account_number { get; set; }//số tài khoản ngân hàng 
        public long loan_money { get; set; }//số tiền vay 
        public int total_loan_time { get; set; }// thời gian vay  
        public int company_address_city_id { get; set; }
        public int company_address_district_id { get; set; }
        public int company_address_ward_id { get; set; }
        public string company_address { get; set; }
        public string company_name { get; set; }
        public int loanCreditId { get; set; }
        public int customerId { get; set; }
        public DateTimeOffset? createDate { get; set; }
        public int Status { get; set; }
        public int? ProductId { get; set; }
        public int LoanID { get; set; }
        public string ProductName
        {
            get
            {
                if (ProductId > 0 && Enum.IsDefined(typeof(EnumProductCredit), ProductId))
                {
                    return Common.Helpers.ExtentionHelper.GetDescription((EnumProductCredit)ProductId);
                }
                return "";
            }
        }
        public long createDateToMilliseconds
        {
            get
            {
                long result = 0;
                try
                {
                    result = (long)(createDate.Value.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                }
                catch { result = 0; }
                return result;
            }
        }
        public string StatusText
        {
            get
            {
                if (Status > 0 && Enum.IsDefined(typeof(EnumLoanStatus), Status))
                {
                    return Common.Helpers.ExtentionHelper.GetDescription((EnumLoanStatus)Status);
                }
                return "";
            }
        }
        public List<ListLoanBriefFile> loanbrieffiles { get; set; }

    }
}
