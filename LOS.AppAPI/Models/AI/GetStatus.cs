﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.AI
{
    public class GetStatus
    {
        public int StatusId { get; set; }
        public string StatusStr { get; set; }
    }
}
