﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.AI
{
    public class InputOpenContract
    {
        public string contractid { get; set; }
        public string imei { get; set; }
        public string shopid { get; set; }
        public string vehicle_type { get; set; }
        public string contract_type { get; set; }
    }

    public class OutputOpenContract
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

}
