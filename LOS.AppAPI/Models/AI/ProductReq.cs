﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.AI
{
    public class ProductReq
    {
        public string brand { get; set; }
        public string model { get; set; }
        public int year { get; set; }
        public int type  { get; set; }
    }
}
