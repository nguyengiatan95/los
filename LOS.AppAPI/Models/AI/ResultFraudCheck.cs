﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class ResultFraudCheck
    {
        public int loan_brief_id { get; set; }
        public Result result { get; set; }
    }

    public class CustomerExtractData
    {
        public string id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string birthday { get; set; }
        public string expiry { get; set; }
        public string gender { get; set; }
        public string ethnicity { get; set; }
        public string religion { get; set; }
        public string issue_by { get; set; }
        public string issue_date { get; set; }
        public string image_url { get; set; }
    }

    public class Result
    {
        public int id { get; set; }
        public int loan_credit_id { get; set; }
        public int loan_status_number { get; set; }
        public DateTime create_on { get; set; }
        public bool? diff_id_input { get; set; }
        public bool? diff_name_input { get; set; }
        public bool? diff_birthday_input { get; set; }
        public bool? diff_address_input { get; set; }
        public bool? diff_face_input { get; set; }
        public bool? diff_type_id { get; set; }
        public List<CustomerExtractData> customer_extract_data { get; set; }
        public double? face_sim_score { get; set; }
        public List<string> face_urls { get; set; }
        public bool? diff_province_nationalid_declare { get; set; }
        public bool? wrong_format_national_id { get; set; }
        public string province_house_hold { get; set; }
        public bool? is_expired_date_birth_date_invalid { get; set; }
        public bool? is_expired_date_issue_date_invalid { get; set; }
        public int? phone_appear_in_refer_phone { get; set; }
        public bool? is_fake_id { get; set; }
        public string same_info_ids { get; set; }
        public bool? is_not_match_info_with_old_cus { get; set; }
        public string same_info_vr { get; set; }
        public bool? is_used_vr { get; set; }
        public List<string> id_imgs { get; set; }
        public List<string> vr_imgs { get; set; }
    }
}
