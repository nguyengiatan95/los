﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Affiliate
{
    public class Affiliate
    {
        public class AffiliateSearchDefault
        {
            public DateTime from_date { get; set; }
            public DateTime to_date { get; set; }
        }
        public class CreateLoanAffTima
        {
            public string FullName { get; set; }
            public string Phone { get; set; }
            public long LoanAmount { get; set; }
            public int LoanTime { get; set; }
            public int ProvinceId { get; set; }
            public int DistrictId { get; set; }
            public int ProductId { get; set; }
            public string UtmSource { get; set; }
            public string UtmMedium { get; set; }
            public string UtmCampaign { get; set; }
            public string UtmContent { get; set; }
            public string UtmTerm { get; set; }
            public string DomainName { get; set; }
            public string PlatformType { get; set; }
            public string NationalCard { get; set; }
            public string Aff_Code { get; set; }
            public string Aff_Sid { get; set; }
            public string TId { get; set; }
            public int IsVerifyOtpLDP { get; set; }
            public string ReferralCode { get; set; }
            public int TypeLoanBrief { get; set; }
        }
        public class CreateLoanAffiliate
        {
            public string FullName { get; set; }
            public string Phone { get; set; }
            public long LoanAmount { get; set; }
            public int LoanTime { get; set; }
            public int ProvinceId { get; set; }
            public int DistrictId { get; set; }
            public int ProductId { get; set; }
            public string UtmSource { get; set; }
            public string UtmMedium { get; set; }
            public string UtmCampaign { get; set; }
            public string UtmContent { get; set; }
            public string UtmTerm { get; set; }
            public string DomainName { get; set; }
            public string PlatformType { get; set; }
            public string NationalCard { get; set; }
            public string Aff_Code { get; set; }
            public string Aff_Sid { get; set; }
            public string TId { get; set; }
        }
        public class AffiliateSearchByAffCode
        {
            public string aff_code { get; set; }
            public DateTime from_date { get; set; }
            public DateTime to_date { get; set; }
        }
        public class ResquestCheckLead
        {
            public string phone_number { get; set; }
            public string national_id { get; set; }
        }
        public class ResquestSendLead
        {
            public string phone_number { get; set; }
            public string national_id { get; set; }
            public int ts_lead_id { get; set; }
            public string score_range { get; set; }
            public string product_code { get; set; }
            public string telco_code { get; set; }
            public string source { get; set; }
            public string province { get; set; }
            public int income_level { get; set; }
            public string full_name { get; set; }
            public MetaData metadata { get; set; }
        }
        public class MetaData
        {

        }
        public class ObjectDataTrustingSocial
        {
            public string bank_lead_id { get; set; }
        }
        public class ResquetCreateLoanKbFina
        {
            public string FullName { get; set; }
            public string Phone { get; set; }
            public string NationalCard { get; set; }
            public int ProvinceId { get; set; }
            public int DistrictId { get; set; }
            public int WardId { get; set; }
            public int? ProductId { get; set; }
            public long? LoanAmount { get; set; }
            public int? LoanTime { get; set; }
            public string TransactionId { get; set; }
        }
        public class ResponseCreateLoanKbFina
        {
            public int LoanBriefId { get; set; }
            public string FullName { get; set; }
            public string ProvinceName { get; set; }
            public string DistrictName { get; set; }
            public string ProductName { get; set; }
            public decimal? LoanAmount { get; set; }
            public int? LoanTime { get; set; }
            public DateTime CreateDate { get; set; }
            public string StatusName { get; set; }
            public string ReasonCancel { get; set; }
            public string TransactionId { get; set; }
            public int? StatusCode { get; set; }
        }

        public class RequestInsertLogCallApiPosbackAffiliate
        {
            public int ActionCallApi { get; set; }
            public string LinkCallApi { get; set; }
            public string TokenCallApi { get; set; }
            public int Status { get; set; }
            public int LoanBriefId { get; set; }
            public string Input { get; set; }
            public DateTime CreatedAt { get; set; }
        }

        public class ResquestUpdateLogCallApiPosbackAffiliate
        {
            public int Id { get; set; }
            public string Output { get; set; }
            public int Status { get; set; }
            public DateTime ModifyAt { get; set; }
        }
        public class CreateLoanBriefJeff
        {
            public string FullName { get; set; }
            public string Phone { get; set; }
            public string NationalCard { get; set; }
            public string ProvinceName { get; set; }
            public string DistrictName { get; set; }
            public decimal? LoanAmount { get; set; }
            public int? ProductId { get; set; }
            public string AffSId { get; set; }
        }

        public class RequestGetLoanCreateAffiliateApi
        {
            public DateTime FromDate { get; set; }
            public DateTime ToDate { get; set; }
            public string TId { get; set; }
        }

    }
}
