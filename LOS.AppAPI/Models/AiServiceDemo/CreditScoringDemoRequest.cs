﻿using LOS.Common.Extensions;
using Newtonsoft.Json;

namespace LOS.AppAPI.Models.AiServiceDemo
{
    public class CreditScoringDemoRequest
    {
        public int ProductId { get; set; }
        public int BrandId { get; set; }
        public int TypeIncomeId { get; set; }
        public decimal TotalMoney { get; set; }
        public int IsLivingTogether { get; set; }
        public decimal Salary { get; set; }
        public int Gender { get; set; }
        public int ProductCredit { get; set; }
        public string FullName { get; set; }
        public string CardNumber { get; set; }
        public string Phone { get; set; }
        public int IsMarried { get; set; }
        public int JobId { get; set; }
    }

    public class CreditScoringDemoAiRequest
    {
        public decimal LoanAmountExpertiseAI { get; set; }
        public int JobId { get; set; }
        public string TypeIncomeName { get; set; }
        public decimal TotalMoney { get; set; }
        public int IsLivingTogether { get; set; }
        public int LoanTime { get; set; } = 12;
        public decimal Salary { get; set; }
        public int RateType { get; set; } = (int)EnumRateType.DuNoGiamDan;
        public int Gender { get; set; }
        public bool ReMarketing { get; set; } = false;
        public int ProductID { get; set; }
        public string FullName { get; set; }
        public string CardNumber { get; set; }
        public string Phone { get; set; }
        public int IsMarried { get; set; }
    }

    public class CreditScoringDemoAiResponse
    {
        [JsonProperty("label")]
        public string Label { get; set; }
        [JsonProperty("score")]
        public decimal Score { get; set; }
        [JsonProperty("suggestion")]
        public string Suggestion { get; set; }
    }
}
