﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LOS.AppAPI.Models.AiServiceDemo
{
    public class FraudDetectionDemoRequest
    {
        public string UserName { get; set; }
        public string NationalId { get; set; }
        public string Dob { get; set; }
        public IFormFile SelfieImage { get; set; }
        public IFormFile FrontIdImage { get; set; }
    }

    public class FraudDetectionDemoResponse

    {
        public string channel { get; set; }
        public List<string> details { get; set; }
        public string report_url { get; set; }
    }


    public class FraudDetectionDemoValidator : AbstractValidator<FraudDetectionDemoRequest>
    {
        public FraudDetectionDemoValidator()
        {
            RuleFor(x => x.UserName).NotEmpty().WithMessage("Vui lòng truyền thông tin họ tên KH");
            RuleFor(x => x.NationalId).NotEmpty().WithMessage("Vui lòng truyền thông tin số cmnd/cccd");
            RuleFor(x => x.Dob).NotEmpty().WithMessage("Vui lòng truyền thông tin ngày sinh yyyy-MM-dd");
            RuleFor(x => x.SelfieImage).NotEmpty().WithMessage("Vui lòng truyền ảnh selfie");
            RuleFor(x => x.FrontIdImage).NotEmpty().WithMessage("Vui lòng truyền ảnh cmnd/cccd mặt trước");
        }
    }

    public class FraudDetectionDemoAiRequest
    {
        [JsonProperty("user_name")]
        public string UserName { get; set; }
        [JsonProperty("national_id")]
        public string NationalId { get; set; }
        [JsonProperty("dob")]
        public string Dob { get; set; }
        [JsonProperty("selfie_image")]
        public string SelfieImage { get; set; }
        [JsonProperty("front_id_image")]
        public string FrontIdImage { get; set; }

    }

    public class FraudDetectionDemoAiResponse
    {
        public string mess { get; set; }
        public int response_code { get; set; }
        public int loan_brief_id { get; set; }
        public Timaresult timaresult { get; set; }
        public List<OldLoanInfo> oldLoan { get; set; }
    }
    public class UnifiedResult
    {
        public string channel { get; set; }
        public List<DetailMessage> details { get; set; }
    }
    public class DetailMessage
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class Timaresult
    {
        public bool diff_id_db { get; set; }
        public bool diff_name_db { get; set; }
        public bool diff_birthday_db { get; set; }
        public bool diff_face_input { get; set; }
        public object diff_type_id { get; set; }
        public object diff_province_nationalid_declare { get; set; }
        public object wrong_format_national_id { get; set; }
        public object province_house_hold { get; set; }
        public object is_expired_date_birth_date_invalid { get; set; }
        public object is_expired_date_issue_date_invalid { get; set; }
        public object phone_appear_in_refer_phone { get; set; }
        public object is_fake_id { get; set; }
        public object is_not_match_info_with_old_cus { get; set; }
        public object is_used_vr { get; set; }
        public bool same_face_diff_info { get; set; }
        public bool is_old_loan_bad { get; set; }
        public UnifiedResult unified_result { get; set; }
        public string report_url { get; set; }
        public double face_sim_score { get; set; }

        [JsonProperty("has_other diff_info_id")]
        public bool HasOtherDiffInfoId { get; set; }
    }

    public class OldLoanInfo
    {
        public int loanBriefId { get; set; }
        public int customerId { get; set; }
        public int status { get; set; }
        public int? gender { get; set; }
        public int provinceId { get; set; }
        public string fullName { get; set; }
        public string phone { get; set; }
        public string dob { get; set; }
        public string nationalCard { get; set; }
        public object createdTime { get; set; }
        public object fromDate { get; set; }
        public object loanRate { get; set; }
        public double? rateTypeId { get; set; }
        public List<string> imgs { get; set; }
        public object labelRisk { get; set; }
        public string cancelReason { get; set; }
        public int? lms_LoanID { get; set; }
    }
}
