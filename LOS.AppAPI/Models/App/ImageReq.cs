﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.App
{
    public class ImageReq
    {
        public class ListImage
        {
            public int LoanId { get; set; }
            public int TypeId { get; set; }

            public int UserId { get; set; }
        }

        public class ListImageByUser
        {
            public int UserId { get; set; }
        }

        public class DeleteImage
        {
            public int LoanId { get; set; }
            public int ImgId { get; set; }
            public int UserId { get; set; }
        }

        public class DeleteImageByUser
        {
            public string LstImg { get; set; }
            public int UserId { get; set; }
        }
        public class UpdateImageByType
        {
            public string LstImg { get; set; }
            public int TypeId { get; set; }
            public int LoanId { get; set; }
            public int UserId { get; set; }
        }

        public class ListImageLMS
        {
            public int LoanBriefId { get; set; }
            public int TypeFile { get; set; } = 0;
        }

    }
}
