﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.App
{
    public class InterestToolReq
    {
        [JsonProperty("TotalMoneyDisbursement")]
        public long TotalMoney { get; set; }

        [JsonProperty("LoanTime")]
        public long LoanTime { get; set; }

        [JsonProperty("RateType")]
        public long RateType { get; set; }

        [JsonProperty("Frequency")]
        public long Frequency { get; set; }       

    }
}
