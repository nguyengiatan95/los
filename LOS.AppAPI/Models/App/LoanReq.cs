﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.App
{
    public class LoanInfoReq
    {
        public class UpdateLoanApp
        {
            public int LoanId { get; set; }
            public int ReceivingMoneyType { get; set; }
            public string BankAccountNumber { get; set; }
            public string BankCardNumber { get; set; }
            public int BankId { get; set; }
            public string BankAccountName { get; set; }
            public string LoanBriefComment { get; set; }
            public decimal LoanAmount { get; set; }
            public int LoanTime { get; set; }
            public int Frequency { get; set; }
            public int RateType { get; set; }
            public bool BuyInsurenceCustomer { get; set; }

        }

        public class UpdateCustomerApp
        {
            public int LoanId { get; set; }
            public int CityId { get; set; }
            public int DistrictId { get; set; }
            public int WardId { get; set; }
            public string CustomerAddress { get; set; }
            public string AddressHouseHold { get; set; }
            public int CompanyCityId { get; set; }
            public int CompanyDistrictId { get; set; }
            public int CompanyWardId { get; set; }
            public string CompanyName { get; set; }
            public string AddressCompany { get; set; }            
        }

    }
}
