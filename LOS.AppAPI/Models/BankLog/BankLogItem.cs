﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class BankLogItem
    {
        public int BankLogId { get; set; }
        public int ShopId { get; set; }
        public decimal CustomerInsFee { get; set; }
        public string FromBank { get; set; }
        public decimal Money { get; set; }
        public string? ResonseCode { get; set; }
        public string? VerifyResponseCode { get; set; }
        public bool? HasResult { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
