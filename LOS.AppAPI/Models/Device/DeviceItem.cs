﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Device
{
    public class DeviceItem
    {
        public string Imei { get; set; }
        public int StatusOfDevice { get; set; }
        public string ContractId { get; set; }
    }
}
