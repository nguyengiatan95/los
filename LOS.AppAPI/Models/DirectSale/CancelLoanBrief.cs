﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class CancelLoanBrief
    {
        public int LoanBriefId { get; set; }
        public int ReasonDetailId { get; set; }
        public string CommentCancel { get; set; }
    }
}
