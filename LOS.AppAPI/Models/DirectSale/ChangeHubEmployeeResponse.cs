﻿using System.Collections.Generic;

namespace LOS.AppAPI.Models.DirectSale
{
    public class ChangeHubEmployeeResponse
    {
        public int HubEmployeeId { get; set; }
        public List<UserOfHub> ListUser { get; set; }
        public List<AppraisalOfHub> ListAppraisal { get; set; }
        public int AppraisalId { get; set; }
    }

    public class UserOfHub
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
    }

    public class AppraisalOfHub
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ChangeHubEmployeeRequest
    {
        public int HubEmployeeId { get; set; }
        public int AppraisalId { get; set; }
        public int LoanbriefId { get; set; }
    }

}
