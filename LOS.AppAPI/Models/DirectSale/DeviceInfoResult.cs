﻿using System;

namespace LOS.AppAPI.Models.DirectSale
{
    public class DeviceInfoResult
    {
        public int LoanbriefId { get; set; }
        public DateTime? LastSendRequestActive { get; set; }
        public DateTime? ActivedDeviceAt { get; set; }
        public string DeviceId { get; set; }
        public int? DeviceStatus { get; set; }
        public string DeviceStatusName { get; set; }
    }

    public class AddDeviceReq
    {
        public int LoanbriefId { get; set; }
        public string DeviceId { get; set; }
        public int HubId { get; set; }
    }

    public class ReActiveDeviceReq
    {
        public int LoanbriefId { get; set; }
    }

}
