﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.DirectSale
{
    public class LoanBriefInit
    {
        public int LoanBriefId { get; set; }
        public int CustomerId { get; set; }
        public int? HubId { get; set; }
        public int? PlatformType { get; set; }
        public int? TypeLoanBrief { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string NationalCard { get; set; }
        public string NationCardPlace { get; set; }
        public bool? IsTrackingLocation { get; set; }
        public bool? IsLocate { get; set; }
        public bool? IsLocateCar { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Passport { get; set; }
        public int? Gender { get; set; }
        public int? NumberCall { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? WardId { get; set; }
        public string WardName { get; set; }
        public int? ProductId { get; set; }
        public int? RateTypeId { get; set; }
        public decimal? LoanAmount { get; set; }
        public bool? BuyInsurenceCustomer { get; set; }
        public int? LoanTime { get; set; }
        public int? Frequency { get; set; }
        public decimal? RateMoney { get; set; }
        public double? RatePercent { get; set; }
        public int? ReceivingMoneyType { get; set; }
        public int? BankId { get; set; }
        public int? TypeRemarketing { get; set; }
        public int? ReMarketingLoanBriefId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }
        public string BankAccountName { get; set; }
        public string PresenterCode { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public decimal? LoanAmountFirst { get; set; }
        public decimal? LoanAmountExpertise { get; set; }
        public decimal? LoanAmountExpertiseLast { get; set; }
        public decimal? LoanAmountExpertiseAi { get; set; }
        public int? LoanPurpose { get; set; }
        public bool? IsCheckBank { get; set; }
        public int? HomeNetwork { get; set; }
        public string PhoneOther { get; set; }
        public string BankBranch { get; set; }
        public int? ProductDetailId { get; set; }
        public bool? IsSim { get; set; }
        public bool? IsReborrow { get; set; }
        public bool? IsChangeInsurance { get; set; }
        public string Email { get; set; }
        public int? Ltv { get; set; }
        public bool? IsTransactionsSecured { get; set; }

        //Customer
        public string FacebookAddress { get; set; }
        public string InsurenceNumber { get; set; }
        public string InsurenceNote { get; set; }
        public int? IsMerried { get; set; }
        public int? NumberBaby { get; set; }

        //LoanBriefResident
        public int? LivingTime { get; set; }
        public int? ResidentType { get; set; }
        public int? LivingWith { get; set; }
        public string ResidentAddress { get; set; }
        public string ResidentAddressGoogleMap { get; set; }
        public string ResidentAddressLatLng { get; set; }
        public string AddressNationalCard { get; set; }
        public string CustomerShareLocation { get; set; }

        //LoanBriefHousehold
        public string HouseholdAddress { get; set; }
        public int? HouseholdProvinceId { get; set; }
        public int? HouseholdDistrictId { get; set; }
        public int? HouseholdWardId { get; set; }
        public string FullNameHouseOwner { get; set; }
        public int? RelationshipHouseOwner { get; set; }
        public string BirdayHouseOwner { get; set; }

        //LoanBriefJob
        public int? JobId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyTaxCode { get; set; }
        public int? CompanyProvinceId { get; set; }
        public int? CompanyDistrictId { get; set; }
        public int? CompanyWardId { get; set; }
        public string CompanyAddress { get; set; }
        public int? ImcomeType { get; set; }
        public decimal? TotalIncome { get; set; }
        public string JobDescription { get; set; }
        public string CompanyAddressGoogleMap { get; set; }
        public string CompanyAddressLatLng { get; set; }
        public bool? CompanyInsurance { get; set; }
        public bool? WorkLocation { get; set; }
        public bool? BusinessPapers { get; set; }
        public int? JobDescriptionId { get; set; }

        // LoanBriefCompany
        public string EnterpriseName { get; set; }
        public string CareerBusiness { get; set; }
        public string BusinessCertificationAddress { get; set; }
        public string HeadOffice { get; set; }
        public string EnterpriseAddress { get; set; }
        public string CompanyShareholder { get; set; }
        public DateTime? CardNumberShareholderDate { get; set; }
        public DateTime? BusinessCertificationDate { get; set; }
        public DateTime? BirthdayShareholder { get; set; }
        public string CardNumberShareholder { get; set; }
        public string PlaceOfBirthShareholder { get; set; }

        //LoanBriefProperty
        public int? BrandId { get; set; }
        public int? PropertyProductId { get; set; }
        public string PlateNumber { get; set; }
        public string CarManufacturer { get; set; }
        public string CarName { get; set; }
        public string CarColor { get; set; }
        public string PlateNumberCar { get; set; }
        public string Chassis { get; set; }
        public string Engine { get; set; }
        public bool? BuyInsuranceProperty { get; set; }
        public string OwnerFullName { get; set; }
        public string MotobikeCertificateNumber { get; set; }
        public string MotobikeCertificateDate { get; set; }
        public string MotobikeCertificateAddress { get; set; }
        public bool? PostRegistration { get; set; }
        public string PropertyDescription { get; set; }

        //LoanBriefQuestionScript
        public string WaterSupplier { get; set; }
        public bool? QuestionHouseOwner { get; set; }
        public short? Appraiser { get; set; }
        public int? DocumentBusiness { get; set; }
        public long? MaxPrice { get; set; }
        public bool? QuestionAddressCoincideAreaSupport { get; set; }
        public List<LoanBriefRelationshipModel> LoanBriefRelationshipModels { get; set; } = new List<LoanBriefRelationshipModel>();
    }
    public class LoanBriefRelationshipModel
    {
        public int? RelationshipType { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    }
}
