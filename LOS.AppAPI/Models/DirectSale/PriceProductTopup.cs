﻿namespace LOS.AppAPI.Models.DirectSale
{
    public class PriceProductTopup
    {
        public long PriceAi { get; set; }
        public long MaxPriceProduct { get; set; }
    }
}
