﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.DrawImage
{
    public class DrawImageReq
    {
        public string CustomerName { get; set; }
        public string NationalCard { get; set; }
        public string UrlFileTemp { get; set; }
        public string Ssiid { get; set; }
        public List<InsuranceInfo> InsuranceInfo { get; set; }
    }
    public class InsuranceInfo
    {
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string Smonth { get; set; }
    }
}
