﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Erp
{
    public class Erp
    {
        public class GetList
        {
            public int HubId { get; set; }
            public int CodeId { get; set; }
            public DateTime FromDate { get; set; }
            public DateTime ToDate { get; set; }
            public int Page { get; set; }
            public int PageSize { get; set; }

        }
    }
}
