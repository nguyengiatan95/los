﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Erp
{
    public class LoanInfoForErp
    {
        public int HubEmployeeId { get; set; }
        public int LoanBriefId { get; set; }
        public string Phone { get; set; }
    }
}
