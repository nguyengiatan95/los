﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Erp
{
    public class PushDeviceReq
    {
        public string serial { get; set; }
        public int loanId { get; set; }
        public int status { get; set; }
        public int loanStatus { get; set; }
    }
    public class PushDeviceRes
    {
        public bool success { get; set; }
    }
}
