﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class ResponseCheckStatusDevice
    {
        public string serial { get; set; }
        public string status { get; set; }
    }
}
