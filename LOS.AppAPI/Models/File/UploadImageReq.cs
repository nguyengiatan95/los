﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class UploadImageReq
    {
        public int LoanbriefId { get; set; }
        public int TypeDocumentId { get; set; }
        public int UploadFrom { get; set; }
        public int? SubTypeId { get; set; }
    }
}
