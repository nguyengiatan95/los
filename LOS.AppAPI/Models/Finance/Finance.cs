﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Finance
{
    public class Finance
    {
        public class CreateLoan
        {
            public string FullName { get; set; }
            public string Phone { get; set; }
            public long LoanAmount { get; set; }
            public int LoanTime { get; set; }
            public int ProvinceId { get; set; }
            public int DistrictId { get; set; }
            public int ProductId { get; set; }
            public string UtmSource { get; set; }
            public string UtmMedium { get; set; }
            public string UtmCampaign { get; set; }
            public string UtmContent { get; set; }
            public string UtmTerm { get; set; }
            public string DomainName { get; set; }
            public string PlatformType { get; set; }
            public string NationalCard { get; set; }
            public string Aff_Code { get; set; }
            public string Aff_Sid { get; set; }
            public string TId { get; set; }
            public string BankAccountName { get; set; }
            public string BankCardNumber { get; set; }
            public string BankAccountNumber { get; set; }
            public int Gender { get; set; }
            public DateTime DOB { get; set; }
            public int FinanceLoanCreditId { get; set; }
            public short SynchorizeFinance { get; set; }

        }

        public class CreateLoanTimaVN
        {
            public string FullName { get; set; }
            public string Phone { get; set; }
            public long LoanAmount { get; set; }
            public int LoanTime { get; set; }
            public int ProvinceId { get; set; }
            public int DistrictId { get; set; }
            public int WardId { get; set; }
            public int ProductId { get; set; }
            public string UtmSource { get; set; }
            public string UtmMedium { get; set; }
            public string UtmCampaign { get; set; }
            public string UtmContent { get; set; }
            public string UtmTerm { get; set; }
            public string DomainName { get; set; }
            public string NationalCard { get; set; }
            public int PlatformType { get; set; }
            public string Aff_Code { get; set; }
            public string Tid { get; set; }
        }

        public class ReportDetailLoanHomeTimeModel
        {
            public int total_loan_day { get; set; }
            public long total_loan { get; set; }
            public int total_loan_month { get; set; }
            public long total_loan_advisory { get; set; }
            public long total_money_disbursement { get; set; }
            public long total_people_registered_borrow { get; set; }

        }
        public class RequestReportLoanThirdPartner
        {
            public DateTime from_date { get; set; }
            public DateTime to_date { get; set; }
            public string third_partner_id { get; set; }
        }
        public class ResponeReportLoanThirdParterCPQL
        {
            public DateTime day { get; set; }
            public int total_loan { get; set; }
            public int total_loan_qlf { get; set; }
            public int total_loan_qlf_no_document { get; set; }
            public int total_loan_disbursement { get; set; }
        }
        public class ResponeReportLoanThirdParterCPS
        {
            public DateTime day { get; set; }
            public int total_loan { get; set; }
            public int total_loan_disbursement { get; set; }
        }

        public class ResponseReportDetailLoanThirdPartner
        {
            public int loan_brief_id { get; set; }
            public string pantner_id { get; set; }
            public string aff_code { get; set; }
            public string qlf { get; set; }
            public string status { get; set; }
            public string reason_cancel { get; set; }
            public DateTime day { get; set; }
            public string publisher { get; set; }
        }
    }
}
