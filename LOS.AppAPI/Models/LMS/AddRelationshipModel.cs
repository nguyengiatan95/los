﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class AddRelationshipModel
    {
        public int LoanbriefId { get; set; }
        public string Phone { get; set; }
        public string FullName { get; set; }
        public int TypeId { get; set; }
        public string UserName { get; set; }
    }
}
