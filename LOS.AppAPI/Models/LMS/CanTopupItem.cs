﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.LMS
{
    public class CanTopupItem
    {
        public int LoanbriefId { get; set; }
        public int LoanId { get; set; }
    }
}
