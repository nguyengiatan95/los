﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class CheckReLoan
    {
        public partial class Input
        {
            public string NumberCard { get; set; }
            public string CustomerName { get; set; }
        }

        public partial class OutPut
        {
            [JsonProperty("Result")]
            public long Result { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("LoanID")]
            public long LoanId { get; set; }

            [JsonProperty("LoanBriefID")]
            public long LoanBriefID { get; set; }

            [JsonProperty("LoanBriefID")]
            public long IsAccept { get; set; }
            public long MaxCountDayLate { get; set; }
        }

        public partial class TopupOutPut
        {
            public long IsAccept { get; set; }
            public string Message { get; set; }
            public long LoanBriefId { get; set; }
            public long TotalMoneyCurrent { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("LoanID")]
            public long LoanId { get; set; }

            [JsonProperty("LoanCreditID")]
            public long LoanCreditId { get; set; }

            [JsonProperty("IsAccept")]
            public long IsAccept { get; set; }
        }

        public partial class OutputPayment
        {
            [JsonProperty("Status")]
            public long Result { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("Data")]
            public DataPayment Data { get; set; }
        }

        public partial class DataPayment
        {
            [JsonProperty("lstLoanCustomer")]
            public LstLoanCustomer[] LstLoanCustomer { get; set; }
        }
        public partial class LstLoanCustomer
        {
            [JsonProperty("LoanID")]
            public long LoanId { get; set; }

            [JsonProperty("LoanBriefID")]
            public long LoanBriefID { get; set; }

            [JsonProperty("ContactCode")]
            public string ContactCode { get; set; }

            [JsonProperty("TotalMoney")]
            public long TotalMoney { get; set; }

            [JsonProperty("FromDate")]
            public string FromDate { get; set; }

            [JsonProperty("ToDate")]
            public string ToDate { get; set; }

            [JsonProperty("FinishDate")]
            public string FinishDate { get; set; }

            [JsonProperty("StatusName")]
            public string StatusName { get; set; }

            [JsonProperty("Status")]
            public long Status { get; set; }

            [JsonProperty("CustomerName")]
            public string CustomerName { get; set; }

            [JsonProperty("ProductID")]
            public int ProductID { get; set; }


            [JsonProperty("LoanTime")]
            public long LoanTime { get; set; }

            [JsonProperty("lstPaymentCustomer")]
            public LstPaymentCustomer[] LstPaymentCustomer { get; set; }
        }
        public partial class LstPaymentCustomer
        {
            [JsonProperty("CustomerID")]
            public long CustomerId { get; set; }

            [JsonProperty("CustomerName")]
            public string CustomerName { get; set; }

            [JsonProperty("StrFromDate")]
            public string StrFromDate { get; set; }

            [JsonProperty("StrToDate")]
            public string StrToDate { get; set; }

            [JsonProperty("CountDay")]
            public long CountDay { get; set; }

            [JsonProperty("PayDate")]
            public DateTimeOffset PayDate { get; set; }

            [JsonProperty("CompleteDate")]
            public object CompleteDate { get; set; }

            [JsonProperty("LoanID")]
            public long LoanId { get; set; }
        }
    }
}
