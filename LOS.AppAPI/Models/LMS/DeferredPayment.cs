﻿using Newtonsoft.Json;
using System;

namespace LOS.AppAPI.Models.LMS
{
    public class DeferredPayment
    {
        public partial class Input
        {
            [JsonProperty("CustomerName")]
            public string CustomerName { get; set; }

            [JsonProperty("NumberCard")]
            public string NumberCard { get; set; }

        }

        public partial class Output
        {
            [JsonProperty("Status")]
            public long Result { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("Data")]
            public Data Data { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("lstLoanCustomer")]
            public LstLoanCustomer[] LstLoanCustomer { get; set; }
        }
        public partial class LstLoanCustomer
        {
            [JsonProperty("LoanID")]
            public long LoanId { get; set; }

            [JsonProperty("LoanBriefID")]
            public long LoanBriefID { get; set; }

            [JsonProperty("ContactCode")]
            public string ContactCode { get; set; }

            [JsonProperty("TotalMoney")]
            public long TotalMoney { get; set; }

            [JsonProperty("TotalMoneyCurrent")]
            public long TotalMoneyCurrent { get; set; }

            [JsonProperty("FromDate")]
            public string FromDate { get; set; }

            [JsonProperty("ToDate")]
            public string ToDate { get; set; }

            [JsonProperty("FinishDate")]
            public string FinishDate { get; set; }

            [JsonProperty("StatusName")]
            public string StatusName { get; set; }

            [JsonProperty("Status")]
            public long Status { get; set; }

            [JsonProperty("CustomerName")]
            public string CustomerName { get; set; }

            [JsonProperty("ProductID")]
            public int ProductID { get; set; }


            [JsonProperty("LoanTime")]
            public long LoanTime { get; set; }

            [JsonProperty("lstPaymentCustomer")]
            public LstPaymentCustomer[] LstPaymentCustomer { get; set; }
        }
        public partial class LstPaymentCustomer
        {
            [JsonProperty("CustomerID")]
            public long CustomerId { get; set; }

            [JsonProperty("CustomerName")]
            public string CustomerName { get; set; }

            [JsonProperty("StrFromDate")]
            public string StrFromDate { get; set; }

            [JsonProperty("StrToDate")]
            public string StrToDate { get; set; }

            [JsonProperty("CountDay")]
            public long CountDay { get; set; }

            [JsonProperty("PayDate")]
            public DateTime? PayDate { get; set; }

            [JsonProperty("CompleteDate")]
            public DateTime? CompleteDate { get; set; }

            [JsonProperty("LoanID")]
            public long LoanId { get; set; }
        }
    }
}
