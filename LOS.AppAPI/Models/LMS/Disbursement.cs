﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.LMS
{
    public class Disbursement
    {
        public partial class InPut
        {
            [JsonProperty("LoanCreditId")]
            public int LoanCreditId { get; set; }

            [JsonProperty("ShopLenderId")]
            public int ShopLenderId { get; set; }

            [JsonProperty("MoneyfeeInsuranceOfCustomer")]
            public long MoneyfeeInsuranceOfCustomer { get; set; }

            [JsonProperty("SourceBankDisbursement")]
            public int SourceBankDisbursement { get; set; }

            [JsonProperty("MoneyfeeInsuranceMaterialCovered")]
            public long MoneyfeeInsuranceMaterialCovered{ get; set; }
        }

        public partial class OutPut
        {
            [JsonProperty("Data")]
            public int Data { get; set; }

            [JsonProperty("Messages")]
            public object[] Messages { get; set; }

            [JsonProperty("Message")]
            public object Message { get; set; }

            [JsonProperty("Status")]
            public long Status { get; set; }

        }

        public class DataResult
        {
            [JsonProperty("LoanID")]
            public int LoanId { get; set; }

            [JsonProperty("LoanCode")]
            public int CodeId { get; set; }
        }

        public partial class OutPutV2
        {
            [JsonProperty("Data")]
            public DataResult? Data { get; set; }

            [JsonProperty("Messages")]
            public object[] Messages { get; set; }

            [JsonProperty("Message")]
            public object Message { get; set; }

            [JsonProperty("Status")]
            public long Status { get; set; }

        }

    }
}
