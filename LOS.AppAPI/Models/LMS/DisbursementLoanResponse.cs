﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.LMS
{
	public class DisbursementLoanResponse
	{
		public string FullName { get; set; }
		public int LoanBriefId { get; set; }
		public int CodeId { get; set; }
		public string LenderName { get; set; }
		public DateTime ReceiveTime { get; set; }
		public DateTime DisbursementTime { get; set; }
		public int Diff { get; set; }
	}
}
