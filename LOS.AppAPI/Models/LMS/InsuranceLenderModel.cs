﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class InsuranceLenderModel
    {
        public int LoanbriefId { get; set; }
        public bool BuyInsurance { get; set; }
        public string UserName { get; set; }
    }
}
