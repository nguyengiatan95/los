﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.LMS
{
    public class InterestAndInsurenceInput
    {
        [JsonProperty("TotalMoneyDisbursement")]
        public long TotalMoneyDisbursement { get; set; }

        [JsonProperty("LoanTime")]
        public long LoanTime { get; set; }

        [JsonProperty("RateType")]
        public long RateType { get; set; }

        [JsonProperty("Frequency")]
        public long Frequency { get; set; }

        [JsonProperty("Rate")]
        public decimal Rate { get; set; }

        [JsonProperty("NotSchedule")]
        public int NotSchedule { get; set; }

        [JsonProperty("LoanBriefId")]
        public int? LoanBriefId { get; set; }
    }

    public class InterestAndInsurenceOutput
    {
        [JsonProperty("Result")]
        public long Result { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("Data")]
        public Data Data { get; set; }
    }
    public partial class Data
    {
        [JsonProperty("LstPaymentSchedule")]
        public LstPaymentSchedule[] LstPaymentSchedule { get; set; }

        [JsonProperty("FeeInsurrance")]
        public long FeeInsurrance { get; set; }

        [JsonProperty("FeeInsuranceMaterialCovered")]
        public long FeeInsuranceMaterialCovered { get; set; }
    }
    public partial class LstPaymentSchedule
    {
        [JsonProperty("FromDate")]
        public DateTimeOffset FromDate { get; set; }

        [JsonProperty("ToDate")]
        public DateTimeOffset ToDate { get; set; }

        [JsonProperty("PayDate")]
        public DateTimeOffset PayDate { get; set; }

        [JsonProperty("MoneyOriginal")]
        public long MoneyOriginal { get; set; }

        [JsonProperty("MoneyInterest")]
        public long MoneyInterest { get; set; }

        [JsonProperty("MoneyService")]
        public long MoneyService { get; set; }

        [JsonProperty("MoneyConsultant")]
        public long MoneyConsultant { get; set; }

        [JsonProperty("MoneyFinePayEarly")]
        public long MoneyFinePayEarly { get; set; }

        [JsonProperty("OutStandingBalance")]
        public long OutStandingBalance { get; set; }

        [JsonProperty("TotalMoneyNeedPayment")]
        public long TotalMoneyNeedPayment { get; set; }

        [JsonProperty("TotalMoneyNeedClosePayment")]
        public long TotalMoneyNeedClosePayment { get; set; }
    }

}
