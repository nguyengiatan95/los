﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.LMS
{
    public class LoanBriefReq
    {
        public class LMSDisbursementWaiting
        {
            public int Type { get; set; }
            public string Search { get; set; }
            public int Page { get; set; }
            public int PageSize { get; set; }

        }
        public class LMSLoanLock
        {
            public int LoanId { get; set; }
            public int Type { get; set; }
            public string Note { get; set; }
            public string UserName { get; set; }

        }
        public class LMSLenderLock
        {
            public int LoanBriefId { get; set; }
            public int LenderId { get; set; }

        }

        public class LMSPushLender
        {
            public int LoanId { get; set; }
            public int LenderId { get; set; }
            public string LenderName { get; set; }
            public string UserName { get; set; }
            public string LenderFullName { get; set; }
            public string LenderNationalCard { get; set; }
            public int DisbursementBy { get; set; }
        }

        public class LMSExtendTimeWaitLender
        {
            public int LoanBriefId { get; set; }
            public string NextTime { get; set; }
        }

        public class LMSReturnLoan
        {
            public int LoanId { get; set; }

            public int Type { get; set; }
            public string Note { get; set; }

            public string UserName { get; set; }

        }

        public class LMSUpdateLoan
        {
            public int LoanBriefId { get; set; }
            public int LoanId { get; set; } //lms_loanID mã đơn vay bên LMS
            public int Type { get; set; } //Loai Action: 1 - GN , 2 - Tất toán
            public int CodeId { get; set; } // mã TC
            public string UserName { get; set; } //username người thao tác
            public long FeeInsuranceOfCustomer { get; set; } // bảo hiểm sức khỏe KH
            public long FeeInsuranceOfProperty { get; set; }// phí bảo hiểm tài sản
            public string JsonFeeInsuranceMoney { get; set; }// Json lưu object các loại phí bảo hiểm
            public int IsHandleException { get; set; } = 0; // TH GN lỗi -> Tạo HĐ bằng tay

        }

        public class PushLoan
        {
            public int LoanId { get; set; }

            public string Note { get; set; }
        }

        public class LMSChangeDisbursement
        {
            public int LoanbriefId { get; set; }
            public int DisbursementBy { get; set; }
            public string Note { get; set; }
            public string UserName { get; set; }
        }
    }
}
