﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.LMS
{
    public class LoanNoteReq
    {
        public class AddNote
        {
            public int LoanBriefId { get; set; }
            public string Note { get; set; }
            public int UserId { get; set; }
            public string UserName { get; set; }
        }
    }
}
