﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class OutPutTopup
    {
        public List<string> Messages { get; set; }
        public string Message { get; set; }
        public DataTopup Data { get; set; }
        public int Status { get; set; }
        public int Total { get; set; }
    }
    public class DataTopup
    {
        public List<ListLoanPaymentSchedule> ListLoanPaymentSchedule { get; set; }
        public decimal MoneyFeeInsuranceOfCustomer { get; set; }
        public decimal MoneyInsuranceOfMaterialCovered { get; set; }
    }
    public class ListLoanPaymentSchedule
    {
        public LoanTopup Loan { get; set; }
        public List<PaymentScheduleBasic> PaymentSchedule { get; set; }

    }
    public class LoanTopup
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int ShopID { get; set; }
        public int CustomerID { get; set; }
        public decimal TotalMoney { get; set; }
        public decimal TotalMoneyCurrent { get; set; }
        public string Collateral { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int LoanTime { get; set; }
        public DateTime LastDateOfPay { get; set; }
        public double Rate { get; set; }
        public double RateInterest { get; set; }
        public double RateConsultant { get; set; }
        public decimal RateService { get; set; }
        public int RateType { get; set; }
        public int Frequency { get; set; }
        public int IsBefore { get; set; }
        public decimal PaymentMoney { get; set; }
        public int Status { get; set; }
        public DateTime NextDate { get; set; }
        public int ApproveBy { get; set; }
        public string Note { get; set; }
        public int TotalInterest { get; set; }
        public decimal DebitMoney { get; set; }
        public decimal OverMoney { get; set; }
        public int TotalDebtPayment { get; set; }
        public int CodeID { get; set; }
        public decimal InterestToDay { get; set; }
        public DateTime FinishDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public int BriefID { get; set; }
        public int TypeCloseLoan { get; set; }
        public int AgencyId { get; set; }
        public int AgentShopID { get; set; }
        public string AgencyName { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int KeepOriginal { get; set; }
        public int TypeProduct { get; set; }
        public int UserIdRemindDebt { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int SynchronizationVbi { get; set; }
        public string LinkGCNChoVay { get; set; }
        public string LinkGCNVay { get; set; }
        public string RegistrationCustomerId { get; set; }
        public string RegistrationContractId { get; set; }
        public decimal MoneyFeeInsuranceOfCustomer { get; set; }
        public int HubId { get; set; }
        public bool IsHub { get; set; }
        public decimal Score { get; set; }
        public decimal DebitMoneyFineLate { get; set; }
        public int ValidDocuments { get; set; }
        public string CusName { get; set; }
        public string CusPhone { get; set; }
        public string CusAddress { get; set; }
        public string NumberCard { get; set; }
        public string PhoneRelationShip { get; set; }
        public int StatusSendInsurance { get; set; }
        public string AddressCompany { get; set; }
        public string AddressHouseHold { get; set; }
        public decimal LTotalMoney { get; set; }
        public int LoanCreditId { get; set; }
        public int InsuranceCompensatorId { get; set; }
        public int YearBadDebt { get; set; }
        public DateTime OldLoanPaymentDate { get; set; }
        public int TopUp { get; set; }
    }
    public class PaymentScheduleBasic
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime PayDate { get; set; }
        public decimal MoneyOriginal { get; set; }
        public decimal MoneyInterest { get; set; }
        public decimal MoneyService { get; set; }
        public decimal MoneyConsultant { get; set; }
        public decimal OtherMoney { get; set; }
        public decimal PayMoney { get; set; }
        public bool Done { get; set; }
        public DateTime DaysPayable { get; set; }

    }
    public class ListPaymentScheduleTopup
    {
        public List<PaymentScheduleBasic> DataPaymentScheduleTopup { get; set; }
    }
}
