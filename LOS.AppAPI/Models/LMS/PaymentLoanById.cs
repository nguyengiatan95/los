﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class PaymentLoanById
    {
        public partial class OutPut
        {
            [JsonProperty("Data")]
            public Data Data { get; set; }

            [JsonProperty("Messages")]
            public string[] Messages { get; set; }

            [JsonProperty("Status")]
            public long Status { get; set; }

            [JsonProperty("Total")]
            public long Total { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("Loan")]
            public Loan Loan { get; set; }

            [JsonProperty("PaymentSchedule")]
            public List<PaymentSchedule> PaymentSchedule { get; set; }
        }

        public partial class Loan
        {
            [JsonProperty("ID")]
            public long Id { get; set; }


            [JsonProperty("TotalMoney")]
            public long TotalMoney { get; set; }

            [JsonProperty("TotalMoneyCurrent")]
            public long TotalMoneyCurrent { get; set; }

            public int TopUp { get; set; }

        }

        public partial class PaymentSchedule
        {
            public DateTime FromDate { get; set; }
            public DateTime ToDate { get; set; }
            public DateTime PayDate { get; set; }
            public long MoneyOriginal { get; set; }
            public long MoneyInterest { get; set; }
            public long MoneyService { get; set; }
            public long MoneyConsultant { get; set; }
            public long OtherMoney { get; set; }
            public bool Done { get; set; }
            public DateTime DaysPayable { get; set; }
            public long PayMoney { get; set; }

        }
    }
}
