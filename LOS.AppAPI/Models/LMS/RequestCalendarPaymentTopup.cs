﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class RequestCalendarPaymentTopup
    {
        public int LoanId { get; set; }
        public long TotalMoneyDisbursement { get; set; }
        public int LoanTime { get; set; }
        public int LoanBriefId { get; set; }
    }
}
