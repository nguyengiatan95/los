﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class RequestPrematureInterestLMS
    {
        public int LoanID { get; set; }
        public string CloseDate { get; set; }
    }
}
