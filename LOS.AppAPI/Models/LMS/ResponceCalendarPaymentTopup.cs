﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class ResponceCalendarPaymentTopup
    {
        public Dictionary<string, List<PaymentScheduleBasic>> DataTopUp { get; set; }
        public decimal MoneyFeeInsuranceOfCustomer { get; set; }
        public decimal MoneyInsuranceOfMaterialCovered { get; set; }
    }
}
