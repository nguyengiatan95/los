﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.LMS
{
    public class SendSMSParam
    {
        public class Input
        {
            [JsonProperty("domain")]
            public string Domain { get; set; }

            [JsonProperty("department")]
            public string Department { get; set; }

            [JsonProperty("phone")]
            public string Phone { get; set; }

            [JsonProperty("messageContent")]
            public string MessageContent { get; set; }
        }
        public partial class OutPut
        {
            [JsonProperty("Result")]
            public long Result { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("Data")]
            public Data Data { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("MessageId")]
            public long MessageId { get; set; }

            [JsonProperty("BrandName")]
            public string BrandName { get; set; }

            [JsonProperty("Phone")]
            public string Phone { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("PartenrId")]
            public object PartenrId { get; set; }

            [JsonProperty("Telco")]
            public string Telco { get; set; }

            [JsonProperty("error")]
            public long Error { get; set; }

            [JsonProperty("error_description")]
            public object ErrorDescription { get; set; }
        }
    }
}
