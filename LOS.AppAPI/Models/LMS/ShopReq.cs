﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.LMS
{
    public class ShopReq
    {
        public int ShopId { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public int Company { get; set; }
    }
}
