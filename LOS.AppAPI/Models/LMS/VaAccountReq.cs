﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.LMS
{
    public class VaAccountReq
    {
        public int LoanbriefId { get; set; }
        public string VaAccountNumner { get; set; }
        public string VaAccountName { get; set; }
        public string VaBankCode { get; set; }
        public string VaBankName { get; set; }
    }

    public class GpayAccountReq
    {
        public int LoanbriefId { get; set; }
        public string GpayAccountNumner { get; set; }
        public string GpayAccountName { get; set; }
        public string GpayBankName { get; set; }
    }
}
