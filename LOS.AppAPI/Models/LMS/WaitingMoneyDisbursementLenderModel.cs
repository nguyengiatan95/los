﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.LMS
{
    public class WaitingMoneyDisbursementLenderModel
    {
        public int LenderId { get; set; }
        public string LenderName { get; set; }
        public decimal? TotalMoney { get; set; }
        //public static Expression<Func<LoanBrief, WaitingMoneyDisbursementLenderModel>> ProjectionDetail
        //{
        //    get
        //    {
        //        return x => new WaitingMoneyDisbursementLenderModel()
        //        {
        //            LenderName = x.LenderName,                 


        //        };
        //    }
        //}
    }
}
