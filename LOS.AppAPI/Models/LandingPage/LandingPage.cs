﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.LanDingPage
{
    public class LandingPage
    {
        public class CreateLoan
        {
            public string FullName { get; set; }
            public string Phone { get; set; }
            public long LoanAmount { get; set; }
            public int LoanTime { get; set; }
            public int ProvinceId { get; set; }
            public int DistrictId { get; set; }
            public int WardId { get; set; }
            public int ProductId { get; set; }
            public string UtmSource { get; set; }
            public string UtmMedium { get; set; }
            public string UtmCampaign { get; set; }
            public string UtmContent { get; set; }
            public string UtmTerm { get; set; }
            public string DomainName { get; set; }
            public string PlatformType { get; set; }
            public string NationalCard { get; set; }
            public string Aff_Code { get; set; }
            public string Aff_Sid { get; set; }
            public string TId { get; set; }
            public int IsVerifyOtpLDP { get; set; }
            public string GoogleClickId { get; set; }
        }
        public class CreateLogSendVoiceOtp
        {
            public string Phone { get; set; }
            public string Otp { get; set; }
            public string Domain { get; set; }
            public string Token { get; set; }
            public string Url { get; set; }
            public string Status { get; set; }
            public string Response { get; set; }
        }
        public class UpdateIsVerifyOtp
        {
            public int LoanBriefId { get; set; }
            public int IsVerifyOtp { get; set; }
        }
        public class CreateLoanAI
        {
            public string FullName { get; set; }
            public string Phone { get; set; }
            public long LoanAmount { get; set; }
            public int LoanTime { get; set; }
            public int ProvinceId { get; set; }
            public int DistrictId { get; set; }
            public string UtmSource { get; set; }
            public string UtmMedium { get; set; }
            public string UtmCampaign { get; set; }
            public string UtmContent { get; set; }
            public string UtmTerm { get; set; }
            public string DomainName { get; set; }
            public List<ListImage> ListImages { get; set; } // list ảnh
            public int ResidentType { get; set; } // kiểu sở hữu nhà
            public int JobId { get; set; } // công việc
            public string CompanyName { get; set; }// tên công ty
            public string CompanyAddress { get; set; }// địa chỉ công ty
            public int LoanPurpose { get; set; }// mục đích vay
            public int ImcomeType { get; set; }// hình thức chuyển tiền
        }
        public class ListImage
        {
            public int TypeId { get; set; }
            public string DomainOfPartner { get; set; }
            public string LinkImgOfPartner { get; set; }
        }
        public class ResponseAI
        {
            public int LoanbriefId { get; set; }
            public string Url { get; set; }
        }
        public class UpdateLoanAI
        {

            public int LoanBriefId { get; set; }
            public string FullName { get; set; }
            public long LoanAmount { get; set; }
            public int LoanTime { get; set; }
            public int ProvinceId { get; set; }
            public int DistrictId { get; set; }
            public string UtmSource { get; set; }
            public string UtmMedium { get; set; }
            public string UtmCampaign { get; set; }
            public string UtmContent { get; set; }
            public string UtmTerm { get; set; }
            public string DomainName { get; set; }
            public List<ListImage> ListImages { get; set; } // list ảnh
            public int ResidentType { get; set; } // kiểu sở hữu nhà
            public int JobId { get; set; } // công việc
            public string CompanyName { get; set; }// tên công ty
            public string CompanyAddress { get; set; }// địa chỉ công ty
            public int LoanPurpose { get; set; }// mục đích vay
            public int ImcomeType { get; set; }// hình thức chuyển tiền
            public int Status { get; set; }
            public int? ReasonCancel { get; set; }
            public bool? QuestionBorrow { get; set; }
            public DateTime? DOB { get; set; }
            public int? step { get; set; }
        }
        public class UpdateLoanBriefEmail
        {
            public int LoanBriefId { get; set; }
            public string Email { get; set; }
        }
        public class CreateLoanPushToPartner
        {
            public string CustomerName { get; set; }
            public string CustomerPhone { get; set; }
            public int ProvinceId { get; set; }
            public int DistrictId { get; set; }
            public string ProvinceName { get; set; }
            public string DistrictName { get; set; }
            public string UtmSource { get; set; }
            public string UtmMedium { get; set; }
            public string UtmCampaign { get; set; }
            public string UtmContent { get; set; }
            public string UtmTerm { get; set; }
            public string Domain { get; set; }
            public string AffCode { get; set; }
            public string AffSId { get; set; }
            public string TId { get; set; }
        }
 public class DefaultReponseEnum
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
        public class PushLoanToPartner
		{
            public string partner { get; set; }
            public int loanBriefId { get; set; }
            public bool partnerProcess { get; set; }
            public string tikiCustomerId { get; set; }
		}

        public class UpdatePushLoanToPartnerEmail
        {
            public int Id { get; set; }
            public string Email { get; set; }
        }

        public class CreateLoanWriteLog
        {
            public string FullName { get; set; }
            public string Phone { get; set; }
            public long LoanAmount { get; set; }
            public int LoanTime { get; set; }
            public int ProvinceId { get; set; }
            public int DistrictId { get; set; }
            public int WardId { get; set; }
            public int ProductId { get; set; }
            public string UtmSource { get; set; }
            public string UtmMedium { get; set; }
            public string UtmCampaign { get; set; }
            public string UtmContent { get; set; }
            public string UtmTerm { get; set; }
            public string DomainName { get; set; }
            public string PlatformType { get; set; }
            public string NationalCard { get; set; }
            public string Aff_Code { get; set; }
            public string Aff_Sid { get; set; }
            public string TId { get; set; }
            public int IsVerifyOtpLDP { get; set; }
            public string GoogleClickId { get; set; }
            public DateTime CreateDate { get; set; }
        }
    }
}
