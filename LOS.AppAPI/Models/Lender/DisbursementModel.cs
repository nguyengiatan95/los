﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Lender
{
    public class DisbursementModel
    {
        public int LoanBriefId { get; set; }
        public string SourceBank { get; set; }
        public string LenderName { get; set; }
        public decimal? FeeInsuranceOfCustomer { get; set; }
        public decimal? FeeInsuranceProperty { get; set; }
    }
}
