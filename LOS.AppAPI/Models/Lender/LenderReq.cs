﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Lender
{
    public class LenderReq
    {
        public class LenderReturnLoan
        {
            public int LoanBriefId { get; set; }
            public int LenderId { get; set; }

            public string Reason { get; set; }
            public string Comment { get; set; }

        }
    }
}
