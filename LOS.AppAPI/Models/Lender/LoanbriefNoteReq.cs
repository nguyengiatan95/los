﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Lender
{
    public class LoanbriefNoteReq
    {
        public int LoanBriefId { get; set; }
        public string Comment { get; set; }
        public string UserName { get; set; }
    }
}
