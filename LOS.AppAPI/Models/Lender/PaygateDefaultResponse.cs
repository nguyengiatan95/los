﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
	public class PaygateDefaultResponse<T>
	{
		public PaygateDefaultResponseMeta meta { get; set; }
		public T data { get; set; }
	}

	public class PaygateDefaultResponseMeta
	{
		public int errCode { get; set; }
		public string errMessage { get; set; }

		public PaygateDefaultResponseMeta(int errCode, string errMessage)
		{
			this.errCode = errCode;
			this.errMessage = errMessage;
		}
	}

}
