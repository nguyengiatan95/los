﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class LendingOnline
    {
        public class InitLoanBriefReq
        {
            public string Phone { get; set; }
            public int LoanTime { get; set; }
            public int ProductId { get; set; }
            public int PlatformType { get; set; }
            public decimal LoanAmount { get; set; }
        }

        public class EditLoanBriefReq
        {
            public int LoanBriefId { get; set; }
            public int Step { get; set; }
            #region step1
            public string FullName { get; set; }
            public DateTime? Dob { get; set; }
            public int? Gender { get; set; }
            public int? LoanPurpose { get; set; }
            public int? JobId { get; set; }
            public int? JobDescriptionId { get; set; }
            public decimal? TotalIncome { get; set; }
            #endregion

            #region step2
            public int? ProvinceId { get; set; }
            public int? DistrictId { get; set; }
            public int? WardId { get; set; }
            public string Address { get; set; }
            #endregion

            #region step3
            public string RelationshipPhone1 { get; set; }
            public string RelationshipPhone2 { get; set; }
            #endregion

            #region step4
            public int? BankId { get; set; }
            public string BankAccountNumber { get; set; }
            #endregion
        }

        public class UploadImageReq
        {
            public int LoanBriefId { get; set; }
            public int TypeId { get; set; }
            public string NationalCard { get; set; }
            public int ResidentType { get; set; }
            public int OwnershipCar { get; set; }
        }

        public class ResponceLocation
        {
            public string home { get; set; }
            public string office { get; set; }
            public string other { get; set; }
        }

        public class RequestSendRequestLocation
        {
            public int LoanBriefId { get; set; }
        }

        public class ReponseGetLoan
        {
            public int? CodeId { get; set; }
            public decimal? LoanAmount { get; set; }
            public int? LoanTime { get; set; }
            public string StatusName { get; set; }
            public DateTime? NextPayDate { get; set; }
            public decimal PayMoney { get; set; }
            public List<PaymentSchedule> PaymentSchedule { get; set; }

        }
        public class PaymentSchedule
        {
            public DateTime PayDate { get; set; }
            public decimal PayMoney { get; set; }
            public bool Status { get; set; }
        }
    }
}
