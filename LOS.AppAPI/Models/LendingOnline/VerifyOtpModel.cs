﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class VerifyOtpModel
    {
        public string Phone { get; set; }
        public string Otp { get; set; }
    }
}
