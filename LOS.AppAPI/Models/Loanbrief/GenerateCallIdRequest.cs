﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class GenerateCallIdRequest
    {
        public int LoanbriefId { get; set; }
        public string Phone { get; set; }
    }
}
