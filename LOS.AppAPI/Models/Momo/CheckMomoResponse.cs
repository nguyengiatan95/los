﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class CheckMomoResponse
    {
        public long Status { get; set; }
        public CheckMomoData Data { get; set; }
        public string Message { get; set; }
    }
    public class CheckMomoData
    {
        public string QueryData { get; set; }
        public object[] Unchecked { get; set; }
        public List<Bill> Bills { get; set; }
    }
    public partial class Bill
    {
        public string BillId { get; set; }
        public string ServiceName { get; set; }
        public long TotalAmount { get; set; }
        public string PartnerName { get; set; }
        public string PartnerPhone { get; set; }
        public string PartnerEmail { get; set; }
        public string PartnerPersonalId { get; set; }
        public string ExpiredDate { get; set; }
        public string Status { get; set; }
    }
}
