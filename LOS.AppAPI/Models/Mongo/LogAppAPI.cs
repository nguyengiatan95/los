﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Mongo
{
    public class LogAppAPI
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Host { get; set; }
        public string Path { get; set; }
        public string Token { get; set; }
        public string UserName { get; set; }
        public string IPRequest { get; set; }
        public string Method { get; set; }
        public string Header { get; set; }
        public string QueryString { get; set; }
        public string RequestBody { get; set; }
        public string ResponseBody { get; set; }
        public DateTime RequestAt { get; set; }
        public DateTime ResponseAt { get; set; }
    }
}
