﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Notification
{
    public class NotificationLenderReq
    {
        public string key { get; set; } = "tima@lender#234";
        public List<NotificationItem> data { get; set; }
    }
    public class NotificationItem
    {
        public int loanCreditId { get; set; }
        public int lenderId { get; set; }
    }
    public class NotificationLenderRes
    {
        public MetaRespone meta { get; set; } 
    }

    public class MetaRespone
    {
        public int errCode { get; set; }
        public string errMessage { get; set; }
    }


}
