﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class NotificationUpdateReq
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public string PushTime { get; set; }
    }
}
