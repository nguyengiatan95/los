﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.NotificationAI
{
    public class SendReq
    {
        public int LoanBriefId { get; set; }
        public string DeviceId { get; set; }
        public string Message { get; set; }
    }
    
}
