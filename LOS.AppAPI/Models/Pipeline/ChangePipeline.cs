﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class ChangePipeline
    {
        public int LoanBriefId { get; set; }
        public int ProductId { get; set; }
        public int Status { get; set; }
    }
}
