﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class InfoFamilyRes
    {
        public int Result { get; set; }
        public string Message { get; set; }
        public string Data { get; set; }
    }

    public class DataResult
    {
        public int FamilyCheckID { get; set; }
    }
}
