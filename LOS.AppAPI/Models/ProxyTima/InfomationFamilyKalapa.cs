﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class InfomationFamilyKalapa
    {
        public string houseOwner { get; set; }
        public List<FamilyItem> familyMember { get; set; }
    }
    public class FamilyItem
    {
        public string name { get; set; }
        public string gender { get; set; }
        public string dateOfBirth { get; set; }
        public string address { get; set; }
        public string personalId { get; set; }
        public string medicalInsurance { get; set; }
    }
}
