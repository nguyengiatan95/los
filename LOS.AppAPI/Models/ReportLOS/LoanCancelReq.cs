﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class LoanCancelReq
    {
        public List<LoanCancelItem> Data { get; set; }
    }

    public class LoanCancelItem
    {
        public int LoanbriefId { get; set; }
        public int? ReasonCancel { get; set; }
        public string ReasonComment { get; set; }
    }

    public class CiscoLogDistributionTelesaleReq
    {
        public List<CiscoLogDistributionTelesaleItem> Data { get; set; }
    }

    public class CiscoLogDistributionTelesaleItem
    {
        public int? IsPushToTelesale { get; set; }
        public int? LoanBriefId { get; set; }
        public int? CiscoStatus { get; set; }
    }

    public class ReCreateLoanCancel
    {
        public List<int> LoanbriefIds { get; set; }
        public int TelesaleId { get; set; }
        public int? TeamTelesaleId { get; set; }

    }
}
