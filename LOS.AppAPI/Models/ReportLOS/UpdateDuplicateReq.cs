﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.ReportLOS
{
    public class UpdateDuplicateReq
    {
        public int loanbriefId { get; set; }
        public int duplicateId { get; set; }
    }
}
