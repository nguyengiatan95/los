﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.SSO
{
    public class SSO
    {
        public partial class LoginInput
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string Email { get; set; }
        }
        public partial class LoginOutput
        {
            [JsonProperty("meta")]
            public Meta Meta { get; set; }

            [JsonProperty("data")]
            public Data Data { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("id")]
            public long Id { get; set; }

            [JsonProperty("username")]
            public string Username { get; set; }

            [JsonProperty("email")]
            public string Email { get; set; }
        }

        public partial class Meta
        {
            [JsonProperty("errorCode")]
            public int ErrorCode { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrorMessage { get; set; }
        }
    }
}
