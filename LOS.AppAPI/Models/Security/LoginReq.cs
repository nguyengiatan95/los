﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class LoginReq
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
