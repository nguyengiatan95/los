﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Sercurity
{
    public class UserInfo
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? Status { get; set; }
        public int? GroupId { get; set; }
        public string GroupName { get; set; }
        public int? Ipphone { get; set; }
        public bool? IsDeleted { get; set; }
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
        public string PushTokenAndroid { get; set; }
        public string PushTokenIOS { get; set; }
        public int TypeCallService { get; set; }

        public string MobileExtension { get; set; } 
        public string MobilePassword { get; set; }
        public List<ShopDetail> ListShop { get; set; }

        public static Expression<Func<User, UserInfo>> ProjectionDetail
        {
            get
            {
                return x => new UserInfo()
                {
                    UserId = x.UserId,
                    Username = x.Username,
                    FullName = x.FullName,
                    Email = x.Email,
                    Phone = x.Phone,
                    GroupId = x.GroupId,
                    Status = x.Status,
                    Ipphone = x.Ipphone,
                    IsDeleted = x.IsDeleted,
                    PushTokenAndroid = x.PushTokenAndroid,
                    PushTokenIOS = x.PushTokenIos,
                    TypeCallService = x.TypeCallService.GetValueOrDefault(0),
                    GroupName = x.Group != null ? x.Group.GroupName : null,
                    ListShop = x.UserShop != null ? x.UserShop.Select(x1 => new ShopDetail()
                    {
                        ShopId = x1.Shop.ShopId,
                        Name = x1.Shop.Name
                    }).ToList() : null,
                };
            }
        }

    }
}
