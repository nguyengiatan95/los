﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class AuthenticationReq
    {
        public string app_id { get; set; }
        public string app_key { get; set; }
    }
    public class AuthenticationRes
    {
        public string app_id { get; set; }
        public string token { get; set; }
        public DateTime expired_date { get; set; }
    }
}
