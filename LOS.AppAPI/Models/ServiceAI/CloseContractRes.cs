﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class CloseContractRes
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
    public class ReActiveDeviceRes
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
