﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class EventLogAI
    {
        public class DisbursementInput
        {
            [JsonProperty("disbursement_date")]
            public string DisbursementDate { get; set; }

            [JsonProperty("imei")]
            public string Imei { get; set; }

            [JsonProperty("loan_brief_id")]
            public long LoanBriefId { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("national_id")]
            public string NationalId { get; set; }

            [JsonProperty("phone")]
            public string Phone { get; set; }
        }

        public class DisbursementOutPut
        {
            [JsonProperty("mess")]
            public string Mess { get; set; }

            [JsonProperty("response_code")]
            public long ResponseCode { get; set; }
        }
    }
}
