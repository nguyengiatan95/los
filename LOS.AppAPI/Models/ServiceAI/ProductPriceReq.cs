﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class ProductPriceReq
    {
        public string brand { get; set; }
        public string model { get; set; }
        public string regdate { get; set; }
        public string phanh { get; set; }
        public string vanh { get; set; }
        public string token { get; set; }
        public int LoanBriefId { get; set; }
    }
}
