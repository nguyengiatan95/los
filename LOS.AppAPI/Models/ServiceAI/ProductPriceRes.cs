﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class ranged_price
    {
        public decimal lower_bound { get; set; }
        public decimal upper_bound { get; set; }
    }
    public class ProductPriceRes
    {
        public decimal predicted_price { get; set; }
        public ranged_price ranged_price { get; set; }
        public string status { get; set; }
        public string predicted_price_string
        {
            get
            {
                if (predicted_price > 0)
                {
                    return predicted_price.ToString("#,##0") + " vnđ";
                }
                return status;
            }
        }
    }
}
