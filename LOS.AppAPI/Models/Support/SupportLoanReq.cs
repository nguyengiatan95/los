﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class SupportLoanReq
    {
        public int LoanbriefId { get; set; }
        public int? TelesaleId { get; set; }
    }
}
