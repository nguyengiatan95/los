﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.THN
{
    public class THN
    {
        public class ResquestUpdateFacebook
        {
            public int LoanBriefId { get; set; }
            public string FacebookAddress { get; set; }
        }

        public class InitDebtRevolvingLoanReq
        {
            public int LoanBriefId { get; set; }
            public decimal LoanAmount { get; set; }
            public int RateTypeId { get; set; }
            public int LoanTime { get; set; }
            public string Username { get; set; }

            public string Comment { get; set; }
        }
    }
}
