﻿namespace LOS.AppAPI.Models.Tiki
{
	public class CreateLeadReq
	{
		public string customer_id { get; set; }
		public string inputs { get; set; }
	}
}
