﻿using System;

namespace LOS.AppAPI.Models.Tiki
{
	public class CreateLeadRes
	{
		public CreateLeadResData data { get; set; }
	}

	public class CreateLeadResData
	{
		public string id { get; set; }
		public string app_identifier { get; set; }
		public string customer_id { get; set; }
		public string inputs { get; set; }
		public bool is_sandbox { get; set; }
		public string status { get; set; }
		public DateTime created_at { get; set; }
		public DateTime updated_at { get; set; }
	}
}
