﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class CreateLoanCarChild
    {
        public int LoanBriefId { get; set; }
        public long LoanAmount { get; set; }
    }
}
