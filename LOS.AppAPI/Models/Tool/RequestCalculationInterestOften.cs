﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class RequestCalculationInterestOften
    {
        public long TotalMoney { get; set; }
        public long LoanTime { get; set; }
        public long RateType { get; set; }
        public long Frequency { get; set; }
        public decimal Rate { get; set; }
    }
}
