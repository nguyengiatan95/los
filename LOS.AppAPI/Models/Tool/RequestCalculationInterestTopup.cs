﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class RequestCalculationInterestTopup
    {
        public int CodeId { get; set; }
        public long TotalMoney { get; set; }
        public int LoanTime { get; set; }
    }
}
