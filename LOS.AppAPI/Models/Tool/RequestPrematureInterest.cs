﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class RequestPrematureInterest
    {
        public string TextSearch { get; set; }
        public int LoanBriefId { get; set; }
        public string Date { get; set; }
    }
}
