﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class RequestPriceMotobike
    {
        public int BrandId { get; set; }
        public int MotobikeId { get; set; }
        public int ProductId { get; set; }
        public int Ltv { get; set; }
        public int TypeOwnership { get; set; }
        public int ProductDetailId { get; set; }
    }
}
