﻿using LOS.AppAPI.Models.LMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class ResponceCalculationInterestOften
    {
        public long FeeInsurrance { get; set; }
        public long FeeInsuranceMaterialCovered { get; set; }
        public List<LstPaymentSchedule> lstPaymentSchedule { get; set; }
    }
}
