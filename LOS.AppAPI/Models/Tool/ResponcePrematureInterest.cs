﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class ResponcePrematureInterest
    {
        public List<LoanBriefPrematureInterest> InfomationCutomer { get; set; }
        public List<ItemPrematureInterestMoneyCustomer> ItemPrematureInterestMoneyCustomer { get; set; }
        public List<PrematureInterestPaymentSchedules> PrematureInterestPaymentSchedules { get; set; }
        public PrematureInterestMoneyCustomer PrematureInterestMoneyCustomer { get; set; }
    }

    public class PrematureInterestMoneyCustomer
    {
        public string DateSettlement { get; set; }
        public string CustomerMoney { get; set; }
        public string FullName { get; set; }
    }
    public class ItemPrematureInterestMoneyCustomer
    {
        public string CodeId { get; set; }
        public long TotalMoneyCurrent { get; set; }
        public long MoneyFineOriginal { get; set; }
        public long MoneyInterest { get; set; }
        public long OtherMoney { get; set; }
        public long MoneyCloseLoan { get; set; }
        public long TotalPaymentCustomer { get; set; }
    }
    public class PrematureInterestPaymentSchedules
    {
        public string CodeId { get; set; }
        public List<PaymentScheduleBasic> LstPaymentSchedules { get; set; }
    }
}
