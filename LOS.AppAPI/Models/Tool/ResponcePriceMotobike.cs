﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class ResponcePriceMotobike
    {
        public long CurrentPrice {get;set;}
        public long MaxPrice { get;set;}
        public long MaxPriceLtv { get;set;}
    }
}
