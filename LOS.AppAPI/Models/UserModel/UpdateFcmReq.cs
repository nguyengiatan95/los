﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class UpdateFcmReq
    {
        public int DeviceId { get; set; }
        public string TokenDevice { get; set; }
    }
}
