﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class UpdateTokenReq
    {
        public string Token { get; set; }
    }
}
