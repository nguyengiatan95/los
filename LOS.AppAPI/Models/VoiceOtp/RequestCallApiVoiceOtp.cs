﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class RequestCallApiVoiceOtp
    {
        public string phone { get; set; }
        public string code { get; set; }
    }
}
