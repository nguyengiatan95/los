﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models
{
    public class ResponseCallApiVoiceOtp
    {
        public string status { get; set; }
        public Message message { get; set; }
    }
    public class Message
    {
        public string session { get; set; }
    }
}
