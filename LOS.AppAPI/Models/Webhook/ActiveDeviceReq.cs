﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Webhook
{
    public class ActiveDeviceReq
    {
        public string Imei { get; set; }
        public int StatusOfDevice { get; set; }
        public string ContractId { get; set; }
    }
}
