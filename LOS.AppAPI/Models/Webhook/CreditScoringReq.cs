﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Webhook
{
    public class CreditScoringReq
    {
        public string RequestID { get; set; }
        public int ScoreRange { get; set; }
    }
}
