﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Webhook
{
    public class LocationMobiReq
    {
        public string refCode { get; set; }
        public string finalResult { get; set; }
        public int code { get; set; }
        public int status { get; set; }
        public string desc { get; set; }
        public DataInput data { get; set; }
        public ResultData result { get; set; }
        public ResultDataDistance distance { get; set; }
        public bool workFromHome { get; set; }
    }

    public class ResultData
    {
        public List<data1Item> data1 { get; set; }
        public List<InfoLocationMobifoneItem> data2 { get; set; }
    }

    public class ResultDataDistance
    {
        public decimal[] homeDist { get; set; }
        public decimal[] officeDist { get; set; }
        public decimal[] otherDist { get; set; }
    }

    public class InfoLocationMobifoneItem
    {
        public decimal lng { get; set; }
        public decimal lat { get; set; }
        public float percent { get; set; }
    }

    public class data1Item
    {
        public int[] hour { get; set; }
        public int day { get; set; }
        public int[] refer { get; set; }
        public int[] work { get; set; }
        public int[] home { get; set; }
    }
}
