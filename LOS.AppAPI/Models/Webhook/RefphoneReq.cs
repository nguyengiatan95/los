﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Webhook
{
    public class RefphoneReq
    {
        public string refCode { get; set; }
        public int status { get; set; }
        public string desc { get; set; }
        public ResultDataPoint data { get; set; }
        public ResultRefphone refPhones { get; set; }
        public string finalResult { get; set; }
    }

    public class ResultDataPoint
    {
        public DataPoint num1 { get; set; }
        public DataPoint? num2 { get; set; }
        public DataPoint? num3 { get; set; }
    }

    public class ResultRefphone
    {
        public DataRefphoneItem num1 { get; set; }
        public DataRefphoneItem num2 { get; set; }
        public DataRefphoneItem num3 { get; set; }
    }
    public class DataPoint
    {
        public string result { get; set; }
        public float point { get; set; }
    }

    public class DataRefphoneItem
    {
        public string value { get; set; }
        public float call_rate { get; set; }
        public float duration_rate { get; set; }
    }
}
