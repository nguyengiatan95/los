﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Webhook
{
    public class ResultAutocallReq
    {
        public string phone { get; set; }
        public string step { get; set; }
        public int? value { get; set; }
        public int? campaignId { get; set; }
        public int? loanbriefId { get; set; }
        public string phoneAgent { get; set; }

    }
}
