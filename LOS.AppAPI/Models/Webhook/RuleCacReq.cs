﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Models.Webhook
{
    public class RuleCacReq
    {
        public string ref_code { get; set; }
        public ResponseLogLoanAi response { get; set; }
    }
    public class ResponseLogLoanAi
    {
        public string rule_name { get; set; }
        public int score { get; set; }
        public List<ChildRules> child_rules { get; set; }
        public string result { get; set; }
        public bool enough_data { get; set; }
        public string type { get; set; }
        public List<string> messages { get; set; }

    }
    public class ChildRules
    {
        public string type { get; set; }
        public int score { get; set; }
        public string result { get; set; }
        public bool enough_data { get; set; }
        public List<string> messages { get; set; }
        public string rule_name { get; set; }
        public List<ChildRules> child_rules { get; set; }

    }
}
