using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Compact;
using System;

namespace LOS.AppAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Override("Microsoft", LogEventLevel.Fatal)
                        .Enrich.FromLogContext()
                        .WriteTo.File(new CompactJsonFormatter(), "logs/tima.log", rollOnFileSizeLimit: true, fileSizeLimitBytes: 10_000_000, rollingInterval: RollingInterval.Day)
                        .WriteTo.Seq("http://seq:5341")
                        .CreateLogger();
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            Console.WriteLine("ASPNETCORE_ENVIRONMENT: " + environmentName);
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", environmentName);

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureKestrel(serverOptions =>
                {
                    serverOptions.Limits.MaxConcurrentConnections = 1000;
                    serverOptions.Limits.MaxConcurrentUpgradedConnections = 1000;
                    serverOptions.Limits.KeepAliveTimeout =
                        TimeSpan.FromMinutes(5);
                    serverOptions.Limits.RequestHeadersTimeout =
                        TimeSpan.FromMinutes(5);
                    serverOptions.Limits.MaxRequestBodySize = 100000000;
                })
                .UseUrls("http://0.0.0.0:80", "http://0.0.0.0:8877")
                .UseStartup<Startup>();
    }
}
