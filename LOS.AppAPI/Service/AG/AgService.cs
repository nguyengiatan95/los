﻿using LOS.AppAPI.Models.AG;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service.AG
{
    public interface IAgService
    {
        ResponseCheckBlackListAg CheckBlacklistAg(RequestCheckBlackListAg entity);
        string GetPayAmountForCustomerByCisco(RequestPayAmount entity);
        Task<GetShopInfoResp> GetShopInfo(int lenderId);
    }
    public class AgService : IAgService
    {
        private readonly IConfiguration _configuration;
        public AgService(IConfiguration configuration)
        {
            this._configuration = configuration;
        }
        public ResponseCheckBlackListAg CheckBlacklistAg(RequestCheckBlackListAg entity)
        {
            var body = JsonConvert.SerializeObject(entity);
            var client = new RestClient(_configuration["AppSettings:LMS"]);
            var request = new RestRequest("api/S2S/CheckBlacklistLOS");
            request.Method = Method.POST;
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.Timeout = 5000;
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            // easy async support
            try
            {
                var response = client.Execute<ResponseCheckBlackListAg>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseCheckBlackListAg>(response.Content);
                    return objData;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "AgService/CheckBlacklistLOS Exception");
                return null;
            }
        }

        public string GetPayAmountForCustomerByCisco(RequestPayAmount entity)
        {
            var body = JsonConvert.SerializeObject(entity);
            var client = new RestClient(_configuration["AppSettings:LMS"]);
            var request = new RestRequest("api/s2s/GetPayAmountForCustomerByCisco");
            request.Method = Method.POST;
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            // easy async support
            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(response.Content);
                    return objData;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "AgService/GetPayAmountForCustomerByCisco Exception");
                return null;
            }
        }

        public async Task<GetShopInfoResp> GetShopInfo(int lenderId)
        {         
            var client = new RestClient(_configuration["AppSettings:LMS"]);            
            var request = new RestRequest("api/Shop/GetInfoLender").AddParameter("lenderId", lenderId);
            string token = _configuration["AppSettings:LMSToken"];
            if (String.IsNullOrEmpty(token))
            {
                token = "8228459735";
            }
            request.AddHeader("Authorization", token);
            request.Method = Method.GET;                                    
            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<GetShopInfoResp>(response.Content);
                    return objData;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "AgService/GetShopInfo Exception");
                return null;
            }
        }
    }
}
