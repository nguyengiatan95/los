﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface IToken
    {
        TokenDetail GetLastToken(string app_id);
        int AddToken(AccessToken entity);
    }
    public class TokenService : IToken
    {
        private IUnitOfWork _unitOfWork;
        public TokenService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public TokenDetail GetLastToken(string app_id)
        {
            try
            {
                return _unitOfWork.AccessTokenRepository.Query(x => x.AppId == app_id, null, false).OrderByDescending(x => x.Id).Select(TokenDetail.ProjectionDetail).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int AddToken(AccessToken entity)
        {
            try
            {
                _unitOfWork.AccessTokenRepository.Insert(entity);
                _unitOfWork.Save();
                return entity.Id;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
