﻿using LOS.DAL.UnitOfWork;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service.AuthorizeToken
{
    public interface IAuthorizeTokenService
    {
        bool CheckAuthorizeTokenRequest(string servername, String token);
    }
    public class AuthorizeTokenService : IAuthorizeTokenService
    {
        private IUnitOfWork _unitOfWork;

        public AuthorizeTokenService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public bool CheckAuthorizeTokenRequest(string servername, string token)
        {
            var authorize = false;
            try
            {
                var result = _unitOfWork.AuthorizationTokenRepository.Query(x => x.ServiceName == servername, null, false).FirstOrDefault();
                if(result != null && result.Enlabled == true)
                {
                    if (result.Token == token)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "API_APP AuthorizeTokenService/AuthorizeTokenRequest exception");
            }
            return authorize;
        }
    }
}
