﻿using LOS.AppAPI.Models;
using LOS.DAL.Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service.BankLog
{
    public interface IBankLog
    {
        List<BankLogItem> GetListTransacsion(int loanbriefId);
    }
    public class BankLogService: IBankLog
    {
        private readonly IConfiguration _configuration;
        public BankLogService(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public List<BankLogItem> GetListTransacsion(int loanbriefId)
        {
            try
            {
                var db = new DapperHelper(_configuration.GetConnectionString("AGDatabase"));
                var sql = new StringBuilder();
                sql.AppendFormat("select BankLogId, ShopId, CustomerInsFee, FromBank, Money, ResponseCode, VerifyResponseCode, HasResult,CreatedAt from BankLog where LoanCreditId = {0}", loanbriefId);
                var data = db.Query<BankLogItem>(sql.ToString());
                return data;
            }
            catch
            {
                return null;
            }
        }
    }
}
