﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service.CareSoft
{
    public interface ICareSoftService
    {
        public string GenarateLinkClick2Call(string ipphone, string numercall);
    }
    public class CareSoftService: ICareSoftService
    {
        private readonly IConfiguration _configuration;
        public CareSoftService(IConfiguration configuration)
        {
            this._configuration = configuration;
        }
        public string GenarateLinkClick2Call(string ipphone, string numercall)
        {
            try
            {
                if (string.IsNullOrEmpty(numercall))
                    return "javascript:;";
                var domain = _configuration["AppSettings:DomainAgent"];
                var Secret = _configuration["AppSettings:SecretCaresoft"];
                string strPhone = Regex.Replace(numercall, @"[^\d]", String.Empty);
                if (strPhone.Length > 2)
                {
                    numercall = strPhone.StartsWith("84") ? "0" + strPhone.Substring(2, strPhone.Length - 2) : strPhone;
                    numercall = "0" + Convert.ToInt64(numercall);
                }
                else
                    numercall = string.Empty;
                var token = GenarateJWT(Secret, ipphone, string.Empty, numercall);
                return domain + "/c2call?token=" + token;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private string GenarateJWT(string Secret, string ipphone, string callout_id, string number)
        {
            var claims = new[] { new Claim("ipphone", ipphone), new Claim("number", System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(number))) };
            if (!string.IsNullOrWhiteSpace(callout_id))
            {
                claims = new[] { new Claim("ipphone", ipphone), new Claim("number", System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(number))), new Claim("callout_id", callout_id) };
            }
            var token = new JwtSecurityToken
                (
                    claims: claims,
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret)),
                            SecurityAlgorithms.HmacSha256)
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
