﻿using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public class CiscoService : ICiscoService
    {
        private readonly IConfiguration _baseConfig;
        public CiscoService(IConfiguration configuration)
        {
            _baseConfig = configuration;
        }
        public string GenerateCallId()
        {
            try
            {
                var domain = _baseConfig["AppSettings:CiscoUrlMobile"];
                if (string.IsNullOrEmpty(domain))
                    domain = "http://103.17.237.21:9669";
                var url = $"{domain}{Constants.CISCO_MOBILE_GENERATE_CALLID}";
                var client = new RestClient(url);
                client.Timeout = 5000;
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                CiscoGenarateCallIdRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<CiscoGenarateCallIdRes>(jsonResponse);
                if (result != null && !string.IsNullOrEmpty(result.Data))
                    return result.Data;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            return string.Empty;
        }

    }
}
