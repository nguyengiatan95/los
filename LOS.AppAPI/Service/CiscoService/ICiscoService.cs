﻿using System.Collections.Generic;

namespace LOS.AppAPI.Service
{
    public interface ICiscoService
    {
        string GenerateCallId();
    }
}