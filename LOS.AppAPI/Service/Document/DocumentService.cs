﻿using LOS.DAL.Dapper;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface IDocumentService
    {
        List<DocumentMapInfo> GetDocumentType(int productId, int typeOwnerShip);
    }
    public class DocumentService: IDocumentService
    {
        private IUnitOfWork _unitOfWork;
        protected IConfiguration _baseConfig;
        public DocumentService(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _baseConfig = configuration;
        }

        public List<DocumentMapInfo> GetDocumentType(int productId, int typeOwnerShip)
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select dt.Id as 'Id', dt.Name as 'Name', dt.Guide, dt.ParentId, cd.ProductId, cd.MinImage as 'NumberImage', cd.Required, cd.TypeRequired , cd.AllowFromUpload, cd. GroupJobId ");
                sql.AppendLine("from DocumentType(nolock) dt ");
                sql.AppendLine("inner join ConfigDocument(nolock) cd on dt.Id = cd.DocumentTypeId ");
                sql.AppendLine("where dt.IsEnable = 1 ");
                sql.AppendLine($"and cd.IsEnable = 1  and cd.ProductId = {productId} and cd.TypeOwnerShip = {typeOwnerShip} ");
                sql.AppendLine("order by dt.Id ");
                return db.Query<DocumentMapInfo>(sql.ToString());
            }
            catch (Exception ex)
            {
                return null;
            }

        }

       
    }
}
