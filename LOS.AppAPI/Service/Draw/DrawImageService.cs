﻿using LOS.AppAPI.Models.DrawImage;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface IDrawImageService
    {
        byte[] DrawImage(DrawImageReq req);
    }
    public class DrawImageService : IDrawImageService
    {
        public byte[] DrawImage(DrawImageReq req)
        {
            System.Net.WebRequest request = System.Net.WebRequest.Create(req.UrlFileTemp);
            System.Net.WebResponse response = request.GetResponse();
            System.IO.Stream responseStream =
                response.GetResponseStream();
            Bitmap bitMapImage = new Bitmap(responseStream);

            Graphics graphicImage = Graphics.FromImage(bitMapImage);
            var fontSize = 6;
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                fontSize = 10;

            graphicImage.DrawString(req.CustomerName, new Font("Arial", fontSize, FontStyle.Bold), new SolidBrush(Color.Black), 170, 70, new StringFormat());
            graphicImage.DrawString(req.NationalCard, new Font("Arial", fontSize, FontStyle.Bold), new SolidBrush(Color.Black), 170, 95, new StringFormat());
            graphicImage.DrawString(req.Ssiid, new Font("Arial", fontSize, FontStyle.Bold), new SolidBrush(Color.Black), 170, 120, new StringFormat());
            if (req.InsuranceInfo.Count > 0)
            {
                int number = 170;
                for (int i = 0; i < req.InsuranceInfo.Count; i++)
                {
                    graphicImage.DrawString(req.InsuranceInfo[i].Smonth, new Font("Arial", fontSize, FontStyle.Bold), new SolidBrush(Color.Black), 70, number, new StringFormat());
                    number = number + 25;
                    //graphicImage.DrawString(req.InsuranceInfo[i].CompanyName, new Font("Arial", fontSize), new SolidBrush(Color.Black), 100, number, new StringFormat());
                    //number = number + 20;
                    graphicImage.DrawString(req.InsuranceInfo[i].CompanyAddress, new Font("Arial", fontSize), new SolidBrush(Color.Black), 90, number, new StringFormat());
                    number = number + 25;
                }

            }
            using (var stream = new MemoryStream())
            {
                bitMapImage.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                var a = stream.ToArray();
                return stream.ToArray();
            }

        }
    }
}
