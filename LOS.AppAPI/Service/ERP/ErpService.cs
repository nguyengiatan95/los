﻿using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Models.Erp;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface IErp
    {
        bool UpdateDeviceStatus(PushDeviceReq req);
        CheckEmployeeTima CheckEmployeeTima(string numberCard, string phone);
        ResponseCheckStatusDevice CheckStatusDevice(string deviceId);
    }
    public class ErpService: IErp
    {
        private IConfiguration _configuration;
        private IUnitOfWork _unitOfWork;
        public ErpService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
        }
        public bool UpdateDeviceStatus(PushDeviceReq req)
        {
            var logid = 0;
            try
            {
                var url = _configuration["AppSettings:ERP"] + Constants.UpdateStatusDeviceToERP;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(req);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                request.Timeout = 5000;
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.ApiUpdateDeveiceToERP.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = req.loanId,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                logid = log.Id;
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.ModifyAt = DateTime.Now;
                log.Output = json;
                _unitOfWork.LogCallApiRepository.Update(log);
                _unitOfWork.Save();
                PushDeviceRes result = JsonConvert.DeserializeObject<PushDeviceRes>(json);
                return result != null && result.success;
            }
            catch (Exception ex)
            {
                if (logid > 0)
                {
                    _unitOfWork.LogCallApiRepository.Update(x => x.Id == logid, x => new LogCallApi()
                    {
                        Status = (int)StatusCallApi.Error,
                        Output = ex.ToString(),
                        ModifyAt = DateTime.Now
                    });
                    _unitOfWork.Save();
                }
                return false;
            }
        }
        public CheckEmployeeTima CheckEmployeeTima(string numberCard, string phone)
        {
            try
            {
                var url = _configuration["AppSettings:ERP"] + Constants.CheckEmployeeTima + "?nationalId=" + numberCard + "&tel=" + phone;
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.ApiCheckEployeeTima.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = phone,
                    CreatedAt = DateTime.Now,
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.ModifyAt = DateTime.Now;
                log.Output = json;
                _unitOfWork.LogCallApiRepository.Update(log);
                _unitOfWork.Save();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    CheckEmployeeTima lst = JsonConvert.DeserializeObject<CheckEmployeeTima>(json);
                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ResponseCheckStatusDevice CheckStatusDevice(string deviceId)
        {
            try
            {
                var url = _configuration["AppSettings:ERP"] + Constants.CheckDeviceErp + "/" + deviceId;
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.CheckDeviceErp.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = "",
                    CreatedAt = DateTime.Now,
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.ModifyAt = DateTime.Now;
                log.Output = json;
                _unitOfWork.LogCallApiRepository.Update(log);
                _unitOfWork.Save();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ResponseCheckStatusDevice lst = JsonConvert.DeserializeObject<ResponseCheckStatusDevice>(json);
                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
