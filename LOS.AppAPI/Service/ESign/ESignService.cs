﻿using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models.ESign;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson.IO;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.IO;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service.ESign
{
	public interface IESignService
	{
		SignCloudResp prepareCertificateForSignCloud(string agreementUUID, string personalName, int idType, string personalID, string locationAddess, string stateOrProvince, string country, byte[] frontSideOfIDDocument, byte[] backSideOfIDDocument, string authorizationEmail, string authorizationMobileNo, ref LogPartner logPartner);
		SignCloudResp prepareFileForSignCloud(string agreementUUID, SignCloudMetaData signCloudMetaData, string fileName, byte[] fileData, string passcode, bool isLender, ref LogPartner logPartner);
		//public SignCloudResp authorizeSingletonSigningForSignCloud(string agreementUUID, string otp, string billCode);
		//public SignCloudResp prepareFileForSignCloudLenderTima(string agreementUUID, SignCloudMetaData signCloudMetaData, string smsTemplate, string fileName, byte[] fileData);
		public SignCloudResp authorizeCounterSigningForSignCloud(string agreementUUID, string otp, string billCode, ref LogPartner logPartner);
		public SignCloudResp regenerateAuthorizationCodeForSignCloud(string agreementUUID, ref LogPartner logPartner);
		public Task<string> GetContractPdf(int loanBriefId, bool customerContract, int lenderId);
	}

	public class ESignService : IESignService
	{
		string baseUrl = "https://rssp.mobile-id.vn/eSignCloud/restapi/";
		string relyingParty = "TIMA_DEMO";
		string relyingPartyUser = "timademo";
		string relyingPartyPassword = "12345678";
		string relyingPartySignature = "AXLiLKsHn0iJIVCds6Kc/95VTAswTug8zIpBoAzZ6tHuDDeHD6Khi0Ofgt2iqiytkHifkwnhpqEijWHsMHHHr3yGaN5YvGE0EogJQvCHz7I8/eQxS69T/dPAWJtWQC2lB2W6CDsZuYt3r7pIg5Pq66lqT8p60MGAEHqSPL9w9ZMmTolwexkTjefA3ljkDkndG3HwKO1JNkdhXNU7HHZL8FXTFeKzngvoe4rm3KLxOuZYkl4QQNiVoCnjiFN9aex7MEuV0Eoa66FyFdOXuTWRiJaiGvU/m5Y5/zM63ZGuYeP2JMwjlT/GjF4Tj1IxI4sB1443uyusAcBr/ReCRKzp/g==";
		string relyingPartyKeyStore = "file\\TIMA_DEMO.p12";
		string relyingPartyKeyStorePassword = "12345678";
		string cetificateProfile = "PERS.1D";
		string smsTemplate = "";

		protected IConfiguration _baseConfig;
		protected IAuthenticationAI _authenService;

		public ESignService(IHostingEnvironment environment)
		{
			var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
			var builder = new ConfigurationBuilder()
			  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
			  .AddJsonFile("appsettings.json", true)
			  .AddJsonFile($"appsettings.{environmentName}.json", true);
			_baseConfig = builder.Build();
			baseUrl = _baseConfig["AppSettings:esign_base_url"].ToString();
			relyingParty = _baseConfig["AppSettings:esign_relying_party"].ToString();
			relyingPartyUser = _baseConfig["AppSettings:esign_relying_party_user"].ToString();
			relyingPartyPassword = _baseConfig["AppSettings:esign_relying_party_password"].ToString();
			relyingPartySignature = _baseConfig["AppSettings:esign_relying_party_signature"].ToString();
			if (string.IsNullOrWhiteSpace(environment.WebRootPath))
			{
				environment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "");
			}
			var keystoreName = _baseConfig["AppSettings:esign_relying_party_keystore"].ToString();
			relyingPartyKeyStore = Path.Combine(environment.WebRootPath, "Keypairs\\" + keystoreName);
			if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
				relyingPartyKeyStore = Path.Combine(environment.WebRootPath, "Keypairs/" + keystoreName);			
			relyingPartyKeyStorePassword = _baseConfig["AppSettings:esign_relying_party_keystore_password"].ToString();
			cetificateProfile = _baseConfig["AppSettings:esign_cetificate_profile"].ToString();
			smsTemplate = _baseConfig["AppSettings:esign_sms_template"].ToString();
		}

		public SignCloudResp prepareCertificateForSignCloud(string agreementUUID, string personalName, int idType, string personalID, string locationAddess, string stateOrProvince, string country, 
			byte[] frontSideOfIDDocument, byte[] backSideOfIDDocument, string authorizationEmail, string authorizationMobileNo, ref LogPartner logPartner)
		{
			string timestamp = ESignMakeSignature.CurrentTimeMillis().ToString();
			string data2sign = relyingPartyUser + relyingPartyPassword + relyingPartySignature + timestamp;
			string pkcs1Signature = ESignMakeSignature.getPKCS1Signature(data2sign, relyingPartyKeyStore, relyingPartyKeyStorePassword);

			SignCloudReq signCloudReq = new SignCloudReq();
			signCloudReq.relyingParty = relyingParty;
			signCloudReq.agreementUUID = agreementUUID;
			signCloudReq.email = authorizationEmail;
			signCloudReq.mobileNo = authorizationMobileNo;

			signCloudReq.certificateProfile = cetificateProfile;

			AgreementDetails agreementDetails = new AgreementDetails();
			agreementDetails.personalName = personalName;
			if (idType == 1)
				agreementDetails.personalID = personalID;
			if (idType == 3)
				agreementDetails.citizenID = personalID;
			else
				agreementDetails.passportID = personalID;			
			agreementDetails.email = authorizationEmail;
			agreementDetails.location = locationAddess;
			agreementDetails.stateOrProvince = stateOrProvince;
			agreementDetails.country = country;

			agreementDetails.photoFrontSideIDCard = frontSideOfIDDocument;
			agreementDetails.photoBackSideIDCard = backSideOfIDDocument;

			signCloudReq.agreementDetails = agreementDetails;

			CredentialData credentialData = new CredentialData();
			credentialData.username = relyingPartyUser;
			credentialData.password = relyingPartyPassword;
			credentialData.timestamp = timestamp;
			credentialData.signature = relyingPartySignature;
			credentialData.pkcs1Signature = pkcs1Signature;
			signCloudReq.credentialData = credentialData;

			var url = baseUrl + Constants.ESIGN_PREPARE_CERTIFICATE_FOR_SIGN_CLOUD;

			var shadowReq = signCloudReq.Clone();
			shadowReq.agreementDetails.photoBackSideIDCard = null;
			shadowReq.agreementDetails.photoFrontSideIDCard = null;
			logPartner.CreatedTime = DateTime.Now;
			logPartner.Provider = "FPT";
			logPartner.RequestContent = Newtonsoft.Json.JsonConvert.SerializeObject(shadowReq);
			logPartner.RequestUrl = url;
			logPartner.TransactionType = "CREATE_BORROWER_AGREEMENT";

			string jsonReq = Newtonsoft.Json.JsonConvert.SerializeObject(signCloudReq);			
			
			var client = new RestClient(url);
			client.Timeout = 15000;
			var request = new RestRequest(Method.POST);
			request.AddHeader("cache-control", "no-cache");
			request.AddParameter("application/json", jsonReq, ParameterType.RequestBody);		
			IRestResponse response = client.Execute(request);
			var jsonResponse = response.Content;
			logPartner.HttpStatusCode = (int) response.StatusCode;			
			JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.Objects,
				TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
				Converters = new[] { new ByteArrayConverter() }
			};
			SignCloudResp signCloudResp = Newtonsoft.Json.JsonConvert.DeserializeObject<SignCloudResp>(jsonResponse, jsonSerializerSettings);
			if (signCloudResp != null)
			{
				if (signCloudResp.responseCode == 0)
				{
					return signCloudResp;
				}
				else
				{
					Log.Information("Error while calling prepareCertificateForSignCloud :" + response.StatusCode + ":" + jsonResponse);
					return signCloudResp;
				}
			}			
			return null;
		}

		public SignCloudResp prepareFileForSignCloud(string agreementUUID, SignCloudMetaData signCloudMetaData,	string fileName, byte[] fileData, string passcode, bool isLender, ref LogPartner logPartner)
		{
			string timestamp = ESignMakeSignature.CurrentTimeMillis().ToString();
			string data2sign = relyingPartyUser + relyingPartyPassword + relyingPartySignature + timestamp;
			string pkcs1Signature = ESignMakeSignature.getPKCS1Signature(data2sign, relyingPartyKeyStore, relyingPartyKeyStorePassword);

			SignCloudReq signCloudReq = new SignCloudReq();
			signCloudReq.relyingParty = relyingParty;
			signCloudReq.agreementUUID = agreementUUID;
			if (!isLender)
			{
				signCloudReq.authorizeMethod = ESignCloudConstant.AUTHORISATION_METHOD_SMS;
				signCloudReq.messagingMode = ESignCloudConstant.ASYNCHRONOUS_CLIENTSERVER;
			}
			else
			{
				signCloudReq.authorizeMethod = ESignCloudConstant.AUTHORISATION_METHOD_PASSCODE;
				signCloudReq.messagingMode = ESignCloudConstant.SYNCHRONOUS;
			}			
			signCloudReq.certificateRequired = true;
			signCloudReq.authorizeCode = passcode;

			signCloudReq.signingFileData = fileData;
			signCloudReq.mimeType = ESignCloudConstant.MIMETYPE_PDF;
			signCloudReq.signingFileName = fileName;

			signCloudReq.signCloudMetaData = signCloudMetaData;

			signCloudReq.notificationTemplate = smsTemplate;

			CredentialData credentialData = new CredentialData();
			credentialData.username = relyingPartyUser;
			credentialData.password = relyingPartyPassword;
			credentialData.timestamp = timestamp;
			credentialData.signature = relyingPartySignature;
			credentialData.pkcs1Signature = pkcs1Signature;
			signCloudReq.credentialData = credentialData;

			var url = baseUrl + Constants.ESIGN_PREPARE_FILE_FOR_SIGN_CLOUD;

			var shadowReq = signCloudReq.Clone();
			shadowReq.signingFileData = null;
			logPartner.CreatedTime = DateTime.Now;
			logPartner.Provider = "FPT";
			logPartner.RequestContent = Newtonsoft.Json.JsonConvert.SerializeObject(shadowReq);
			logPartner.RequestUrl = url;
			if (!isLender)
				logPartner.TransactionType = "PREPARE_BORROWER_CONTRACT";
			else
				logPartner.TransactionType = "PREPARE_LENDER_CONTRACT";

			string jsonReq = Newtonsoft.Json.JsonConvert.SerializeObject(signCloudReq);			
			var client = new RestClient(url);
			client.Timeout = 15000;
			var request = new RestRequest(Method.POST);
			request.AddHeader("cache-control", "no-cache");
			request.AddParameter("application/json", jsonReq, ParameterType.RequestBody);
			IRestResponse response = client.Execute(request);
			logPartner.HttpStatusCode = (int)response.StatusCode;
			var jsonResponse = response.Content;			
			JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.Objects,
				TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
				Converters = new[] { new ByteArrayConverter() }
			};
			SignCloudResp signCloudResp = Newtonsoft.Json.JsonConvert.DeserializeObject<SignCloudResp>(jsonResponse, jsonSerializerSettings);
			if (signCloudResp != null)
			{
				if (signCloudResp.responseCode == 0 || signCloudResp.responseCode == 1018)
				{
					if (signCloudResp.signedFileData != null)
					{
						return signCloudResp;
					}
					else
					{
						Console.WriteLine("Oops! no data received");
						Log.Information("Error while calling prepareFileForSignCloud :" + response.StatusCode + ":" + jsonResponse);
						throw new Exception("Error while calling prepareFileForSignCloud");
					}
				}
				else if (signCloudResp.responseCode == 1007)
				{
					Console.WriteLine("ResponseCode: " + signCloudResp.responseCode);
					Console.WriteLine("ResponseMessage: " + signCloudResp.responseMessage);
					Console.WriteLine("BillCode: " + signCloudResp.billCode);
					string otp = signCloudResp.authorizeCredential;
					Console.WriteLine("OTP: " + otp);
					return signCloudResp;
				}
				else
				{
					Log.Information("Error while calling prepareFileForSignCloud :" + response.StatusCode + ":" + jsonResponse);
					//throw new Exception("Error while calling prepareFileForSignCloud");
					return signCloudResp;
				}
			}
			return null;
		}

		//public SignCloudResp authorizeSingletonSigningForSignCloud(string agreementUUID, string otp, string billCode)
		//{
		//	string timestamp = ESignMakeSignature.CurrentTimeMillis().ToString();
		//	string data2sign = relyingPartyUser + relyingPartyPassword + relyingPartySignature + timestamp;
		//	string pkcs1Signature = ESignMakeSignature.getPKCS1Signature(data2sign, relyingPartyKeyStore, relyingPartyKeyStorePassword);

		//	SignCloudReq signCloudReq = new SignCloudReq();
		//	signCloudReq.relyingParty = relyingParty;
		//	signCloudReq.agreementUUID = agreementUUID;
		//	signCloudReq.authorizeCode = otp;
		//	signCloudReq.billCode = billCode;
		//	signCloudReq.messagingMode = ESignCloudConstant.SYNCHRONOUS;

		//	CredentialData credentialData = new CredentialData();
		//	credentialData.username = relyingPartyUser;
		//	credentialData.password = relyingPartyPassword;
		//	credentialData.timestamp = timestamp;
		//	credentialData.signature = relyingPartySignature;
		//	credentialData.pkcs1Signature = pkcs1Signature;
		//	signCloudReq.credentialData = credentialData;

		//	string jsonReq = Newtonsoft.Json.JsonConvert.SerializeObject(signCloudReq);
		//	var url = baseUrl + Constants.ESIGN_AUTHORIZE_SINGLETON_SIGNING_FOR_SIGN_CLOUD;
		//	var client = new RestClient(url);
		//	client.Timeout = 15000;
		//	var request = new RestRequest(Method.POST);
		//	request.AddHeader("cache-control", "no-cache");
		//	request.AddParameter("application/json", jsonReq, ParameterType.RequestBody);
		//	IRestResponse response = client.Execute(request);
		//	var jsonResponse = response.Content;
		//	JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
		//	{
		//		TypeNameHandling = TypeNameHandling.Objects,
		//		TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
		//		Converters = new[] { new ByteArrayConverter() }
		//	};
		//	SignCloudResp signCloudResp = Newtonsoft.Json.JsonConvert.DeserializeObject<SignCloudResp>(jsonResponse, jsonSerializerSettings);
		//	if (signCloudResp.responseCode == 0)
		//	{
		//		if (signCloudResp.signedFileData != null || signCloudResp.multipleSignedFileData != null)
		//		{
		//			return signCloudResp;
		//		}				
		//		else
		//		{
		//			Console.WriteLine("Oops! no data received");
		//			throw new Exception("Error while calling authorizeSingletonSigningForSignCloud");
		//		}
		//	}
		//	else if (signCloudResp.responseCode == 1007)
		//	{
		//		Console.WriteLine("Message: " + signCloudResp.responseMessage);
		//		return signCloudResp;
		//	}
		//	else
		//	{
		//		Log.Information("Error while calling authorizeSingletonSigningForSignCloud :" + response.StatusCode + ":" + jsonResponse);				
		//		throw new Exception("Error while calling authorizeSingletonSigningForSignCloud");
		//	}
		//}

		//public SignCloudResp prepareFileForSignCloudLenderTima(string agreementUUID, SignCloudMetaData signCloudMetaData, string smsTemplate, string fileName, byte[] fileData)
		//{
		//	string timestamp = ESignMakeSignature.CurrentTimeMillis().ToString();
		//	string data2sign = relyingPartyUser + relyingPartyPassword + relyingPartySignature + timestamp;
		//	string pkcs1Signature = ESignMakeSignature.getPKCS1Signature(data2sign, relyingPartyKeyStore, relyingPartyKeyStorePassword);

		//	SignCloudReq signCloudReq = new SignCloudReq();
		//	signCloudReq.relyingParty = relyingParty;
		//	signCloudReq.agreementUUID = agreementUUID;
		//	signCloudReq.authorizeMethod = ESignCloudConstant.AUTHORISATION_METHOD_SMS;
		//	signCloudReq.messagingMode = ESignCloudConstant.ASYNCHRONOUS_CLIENTSERVER;
		//	signCloudReq.certificateRequired = true;

		//	signCloudReq.signingFileData = fileData;
		//	signCloudReq.mimeType = ESignCloudConstant.MIMETYPE_PDF;
		//	signCloudReq.signingFileName = fileName;

		//	signCloudReq.signCloudMetaData = signCloudMetaData;

		//	signCloudReq.notificationTemplate = smsTemplate;

		//	CredentialData credentialData = new CredentialData();
		//	credentialData.username = relyingPartyUser;
		//	credentialData.password = relyingPartyPassword;
		//	credentialData.timestamp = timestamp;
		//	credentialData.signature = relyingPartySignature;
		//	credentialData.pkcs1Signature = pkcs1Signature;
		//	signCloudReq.credentialData = credentialData;

		//	string jsonReq = Newtonsoft.Json.JsonConvert.SerializeObject(signCloudReq);
		//	var url = baseUrl + Constants.ESIGN_PREPARE_FILE_FOR_SIGN_CLOUD;
		//	var client = new RestClient(url);
		//	client.Timeout = 15000;
		//	var request = new RestRequest(Method.POST);
		//	request.AddHeader("cache-control", "no-cache");
		//	request.AddParameter("application/json", jsonReq, ParameterType.RequestBody);
		//	IRestResponse response = client.Execute(request);
		//	var jsonResponse = response.Content;
		//	JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
		//	{
		//		TypeNameHandling = TypeNameHandling.Objects,
		//		TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
		//		Converters = new[] { new ByteArrayConverter() }
		//	};
		//	SignCloudResp signCloudResp = Newtonsoft.Json.JsonConvert.DeserializeObject<SignCloudResp>(jsonResponse, jsonSerializerSettings);
		//	if (signCloudResp.responseCode == 0 || signCloudResp.responseCode == 1018)
		//	{
		//		if (signCloudResp.signedFileData != null)
		//		{
		//			return signCloudResp;
		//		}
		//		else
		//		{
		//			Console.WriteLine("Oops! no data received");
		//			Log.Information("Error while calling prepareFileForSignCloud :" + response.StatusCode + ":" + jsonResponse);
		//			throw new Exception("Error while calling prepareFileForSignCloud");
		//		}
		//	}
		//	else if (signCloudResp.responseCode == 1007)
		//	{
		//		Console.WriteLine("ResponseCode: " + signCloudResp.responseCode);
		//		Console.WriteLine("ResponseMessage: " + signCloudResp.responseMessage);
		//		Console.WriteLine("BillCode: " + signCloudResp.billCode);
		//		string otp = signCloudResp.authorizeCredential;
		//		Console.WriteLine("OTP: " + otp);
		//		return signCloudResp;
		//	}
		//	else
		//	{
		//		Log.Information("Error while calling prepareFileForSignCloud :" + response.StatusCode + ":" + jsonResponse);
		//		throw new Exception("Error while calling prepareFileForSignCloud");
		//	}
		//}

		public SignCloudResp authorizeCounterSigningForSignCloud(string agreementUUID, string otp, string billCode, ref LogPartner logPartner)
		{
			string timestamp = ESignMakeSignature.CurrentTimeMillis().ToString();
			string data2sign = relyingPartyUser + relyingPartyPassword + relyingPartySignature + timestamp;
			string pkcs1Signature = ESignMakeSignature.getPKCS1Signature(data2sign, relyingPartyKeyStore, relyingPartyKeyStorePassword);

			SignCloudReq signCloudReq = new SignCloudReq();
			signCloudReq.relyingParty = relyingParty;
			signCloudReq.agreementUUID = agreementUUID;
			signCloudReq.authorizeCode = otp;
			signCloudReq.billCode = billCode;
			signCloudReq.messagingMode = ESignCloudConstant.SYNCHRONOUS;

			CredentialData credentialData = new CredentialData();
			credentialData.username = relyingPartyUser;
			credentialData.password = relyingPartyPassword;
			credentialData.timestamp = timestamp;
			credentialData.signature = relyingPartySignature;
			credentialData.pkcs1Signature = pkcs1Signature;
			signCloudReq.credentialData = credentialData;

			var url = baseUrl + Constants.ESIGN_AUTHORIZE_COUNTER_SIGNING_FOR_SIGN_CLOUD;

			logPartner.CreatedTime = DateTime.Now;
			logPartner.Provider = "FPT";
			logPartner.RequestContent = Newtonsoft.Json.JsonConvert.SerializeObject(signCloudReq);
			logPartner.RequestUrl = url;
			logPartner.TransactionType = "AUTHORIZE_OTP";

			string jsonReq = Newtonsoft.Json.JsonConvert.SerializeObject(signCloudReq);

			var client = new RestClient(url);
			client.Timeout = 15000;
			var request = new RestRequest(Method.POST);
			request.AddHeader("cache-control", "no-cache");
			request.AddParameter("application/json", jsonReq, ParameterType.RequestBody);
			IRestResponse response = client.Execute(request);
			logPartner.HttpStatusCode = (int)response.StatusCode;
			var jsonResponse = response.Content;
			JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.Objects,
				TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
				Converters = new[] { new ByteArrayConverter() }
			};
			SignCloudResp signCloudResp = Newtonsoft.Json.JsonConvert.DeserializeObject<SignCloudResp>(jsonResponse, jsonSerializerSettings);
			if (signCloudResp != null)
			{
				if (signCloudResp.responseCode == 0)
				{
					if (signCloudResp.signedFileData != null || signCloudResp.multipleSignedFileData != null)
					{
						return signCloudResp;
					}
					else
					{
						Log.Information("Error while calling authorizeCounterSigningForSignCloud :" + response.StatusCode + ":" + jsonResponse);
						return signCloudResp;
					}
				}
				else if (signCloudResp.responseCode == 1007)
				{
					Console.WriteLine("Message: " + signCloudResp.responseMessage);
					return signCloudResp;
				}
				else
				{
					Log.Information("Error while calling authorizeCounterSigningForSignCloud :" + response.StatusCode + ":" + jsonResponse);
					return signCloudResp;
				}
			}			
			return null;
		}

		public SignCloudResp regenerateAuthorizationCodeForSignCloud(string agreementUUID, ref LogPartner logPartner)
		{
			string timestamp = ESignMakeSignature.CurrentTimeMillis().ToString();
			string data2sign = relyingPartyUser + relyingPartyPassword + relyingPartySignature + timestamp;
			string pkcs1Signature = ESignMakeSignature.getPKCS1Signature(data2sign, relyingPartyKeyStore, relyingPartyKeyStorePassword);


			SignCloudReq signCloudReq = new SignCloudReq();
			signCloudReq.relyingParty = relyingParty;
			signCloudReq.agreementUUID = agreementUUID;
			signCloudReq.authorizeMethod = ESignCloudConstant.AUTHORISATION_METHOD_SMS;

			signCloudReq.notificationTemplate = smsTemplate;

			CredentialData credentialData = new CredentialData();
			credentialData.username = relyingPartyUser;
			credentialData.password = relyingPartyPassword;
			credentialData.timestamp = timestamp;
			credentialData.signature = relyingPartySignature;
			credentialData.pkcs1Signature = pkcs1Signature;
			signCloudReq.credentialData = credentialData;

			var url = baseUrl + Constants.ESIGN_REGENERATE_AUTHORIZATION_CODE_FOR_SIGN_CLOUD;

			logPartner.CreatedTime = DateTime.Now;
			logPartner.Provider = "FPT";
			logPartner.RequestContent = Newtonsoft.Json.JsonConvert.SerializeObject(signCloudReq);
			logPartner.RequestUrl = url;
			logPartner.TransactionType = "REGENERATE_OTP";

			string jsonReq = Newtonsoft.Json.JsonConvert.SerializeObject(signCloudReq);
			var client = new RestClient(url);
			client.Timeout = 180000;
			var request = new RestRequest(Method.POST);
			request.AddHeader("cache-control", "no-cache");
			request.AddParameter("application/json", jsonReq, ParameterType.RequestBody);
			IRestResponse response = client.Execute(request);
			logPartner.HttpStatusCode = (int)response.StatusCode;
			var jsonResponse = response.Content;
			JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.Objects,
				TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
				Converters = new[] { new ByteArrayConverter() }
			};
			SignCloudResp signCloudResp = Newtonsoft.Json.JsonConvert.DeserializeObject<SignCloudResp>(jsonResponse, jsonSerializerSettings);
			if (signCloudResp != null)
			{
				if (signCloudResp.responseCode == 0)
				{
					return signCloudResp;
				}
				else
				{
					Log.Information("Error while calling regenerateAuthorizationCodeForSignCloud :" + response.StatusCode + ":" + jsonResponse);
					throw new Exception("Error while calling authorizeCounterSigningForSignCloud");
				}
			}
			return null;
		}

		public async Task<string> GetContractPdf(int loanBriefId, bool customerContract, int lenderId)
		{
			using (var client = new HttpClient())
			{
				string url = "";
				if (customerContract)
					url = _baseConfig["AppSettings:generate_pdf_contract_url"].ToString() + "?loanBriefId=" + loanBriefId;
				else
					url = _baseConfig["AppSettings:generate_pdf_contract_lender_url"].ToString() + "?loanBriefId=" + loanBriefId + "&lenderId=" + lenderId;
				var response = await client.GetAsync(url);
				if (response.StatusCode == System.Net.HttpStatusCode.OK)
				{
					string content = await response.Content.ReadAsStringAsync();

					var def = Newtonsoft.Json.JsonConvert.DeserializeObject<DefaultResponse<string>>(content);
					if (def.meta.errorCode == 200)
						return def.data;
					return "";
				}
				else
				{
					return "";
				}	
			}	
		}
	}
}
