﻿using LOS.AppAPI.Service.LmsService;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using SelectPdf;
using Serilog;
using System;
using System.Drawing;
using System.Linq;
using System.Text;

namespace LOS.AppAPI.Service.ExportPDF
{
    public interface IExportPdfService
    {
        byte[] ContractFinancialAgreement(int loanbriefId);
        string ContractFinancialAgreementBase64(int loanbriefId);
        string ContractFinancialAgreementLenderBase64(int loanbriefId, int lenderId);
    }
    public class ExportPdfService : IExportPdfService
    {
        private IUnitOfWork _unitOfWork;
        private IViewRenderService _viewRenderService;
        private ILmsService _lmsService;

        public ExportPdfService(IUnitOfWork unitOfWork, IViewRenderService viewRenderService, ILmsService lmsService)
        {
            this._unitOfWork = unitOfWork;
            this._viewRenderService = viewRenderService;
            this._lmsService = lmsService;
        }

        public byte[] ContractFinancialAgreement(int loanbriefId)
        {
            try
            {
                var data = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(LoanBriefContractFinancialAgreement.ProjectionViewDetail).FirstOrDefault();
                if (data != null && data.LoanBriefId > 0)
                {
                    if (!string.IsNullOrEmpty(data.FullName))
                    {
                        data.FullName = data.FullName.ReduceWhitespace();
                        data.FullName = data.FullName.TitleCaseString();
                        data.FullName = data.FullName.Replace(" ", "").ToUpper(); // xóa hết khoảng trắng và viết hoa tất cả các chữ cái
                    }
                    if (data.Dob.HasValue && data.Dob != DateTime.MinValue)
                    {
                        data.strDob = data.Dob.Value.ToString("ddMMyyyy");
                        data.strDob = data.strDob.Trim().ToUpper();
                    }
                    var viewString = _viewRenderService.RenderToStringAsync("ExportPDF/ContractFinancialAgreement", data);
                    byte[] byteHtml = Encoding.ASCII.GetBytes(viewString.Result);
                    return byteHtml;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ExportPdfService/ContractFinancialAgreement Exception");
                return null;
            }

        }

        public string ContractFinancialAgreementBase64(int loanbriefId)
        {
            try
            {
                var data = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(LoanBriefContractFinancialAgreement.ProjectionViewDetail).FirstOrDefault();
                if (data != null && data.LoanBriefId > 0)
                {

                    //Gọi api lấy ra tiền bảo hiểm
                    var objInterestAndInsurence = new Models.LMS.InterestAndInsurenceInput()
                    {
                        TotalMoneyDisbursement = data.LoanAmount.HasValue ? (long)data.LoanAmount.Value : 0,
                        LoanTime = data.LoanTime.HasValue ? data.LoanTime.Value * 30 : 0,
                        RateType = data.RateTypeId.HasValue ? data.RateTypeId.Value : 0,
                        Frequency = 30,
                        NotSchedule = 0,
                        LoanBriefId = data.LoanBriefId,
                        Rate = (decimal)data.RatePercent.Value
                    };
                    var resultInterestAndInsurence = _lmsService.InterestAndInsurence(objInterestAndInsurence);
                    if (resultInterestAndInsurence != null && resultInterestAndInsurence.Data != null)
                    {
                        data.FeeInsuranceOfCustomer = resultInterestAndInsurence.Data.FeeInsurrance;
                        data.FeeInsuranceOfProperty = resultInterestAndInsurence.Data.FeeInsuranceMaterialCovered;
                    }

                    if (!string.IsNullOrEmpty(data.FullName))
                    {
                        if (!data.FullName.IsNormalized())
                            data.FullName = data.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                        data.strFullName = data.FullName.ReduceWhitespace();
                        data.strFullName = data.FullName.TitleCaseString();
                        data.strFullName = data.FullName.Replace(" ", "").ToUpper(); // xóa hết khoảng trắng và viết hoa tất cả các chữ cái

                    }
                    if (data.Dob.HasValue && data.Dob != DateTime.MinValue)
                    {
                        data.strDob = data.Dob.Value.ToString("ddMMyyyy");
                        data.strDob = data.strDob.Trim().ToUpper();
                    }

                    if (data.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan)
                    {
                        //đọc số tiên
                        data.StrLoanAmount = Common.Extensions.ConvertExtensions.ReadNumber(data.LoanAmount.Value);

                        //lấy mã TC của HĐ cũ
                        if (data.ReMarketingLoanBriefId.HasValue && data.ReMarketingLoanBriefId > 0)
                            data.CodeIdLoanBriefOld = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == data.ReMarketingLoanBriefId.Value, null, false).Select(x => x.CodeId).FirstOrDefault();
                    }

                    data.DateEsignContract = DateTime.Now;

                    //tính số tiền lãi trên HĐ của lender
                    //kiểm tra xem trong loanbrief có trường LenderRate hay chưa

                    data.CustomerRate = LOS.Common.Helpers.Const.LENDER_RATE;

                    //chưa có thì lấy bản ghi trong bảng ConfigContractRate
                    var configRate = _unitOfWork.ConfigContractRateRepository.Query(x => x.IsEnable == true
                                && DateTime.Now >= x.AppliedAt, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                    //không có thì gán mặc định rate
                    if (configRate != null && configRate.CustomerRate > 0)
                        data.CustomerRate = configRate.CustomerRate.Value;

                    var viewString = _viewRenderService.RenderToStringAsync("ExportPDF/ContractFinancialAgreement", data);
                    HtmlToPdf oHtmlToPdf = new HtmlToPdf();
                    oHtmlToPdf.Options.WebPageHeight = 595;
                    oHtmlToPdf.Options.WebPageWidth = 750;
                    oHtmlToPdf.Options.MarginTop = 30;
                    oHtmlToPdf.Options.MarginLeft = 10;
                    oHtmlToPdf.Options.MarginRight = 10;
                    var oPdfDocument = new PdfDocument();
                    oPdfDocument = oHtmlToPdf.ConvertHtmlString(viewString.Result);
                    byte[] pdf = oPdfDocument.Save();
                    return Convert.ToBase64String(pdf, 0, pdf.Length);
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ExportPdfService/ContractFinancialAgreementBase64 Exception");
                return null;
            }
        }

        public string ContractFinancialAgreementLenderBase64(int loanbriefId, int lenderId)
        {
            try
            {
                var data = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(LoanBriefContractFinancialAgreement.ProjectionViewDetail).FirstOrDefault();
                if (data != null && data.LoanBriefId > 0)
                {
                    //kiểm tra xem có lenderid chưa
                    //có rồi thì gọi api lms để lấy thông tin
                    //chưa có thì bỏ qua
                    if (lenderId > 0)
                    {
                        var dataLender = _lmsService.GetInfoLender(lenderId);
                        if (dataLender != null && dataLender.Data != null)
                        {
                            data.FullName = dataLender.Data.PersonContact;
                            data.NationalCard = dataLender.Data.PersonCardNumber;
                            data.Dob = dataLender.Data.PersonBirthDay;
                            data.HouseholdAddress = dataLender.Data.Address;
                            data.ResidentAddress = dataLender.Data.AddressOfResidence;
                        }
                    }
                    else
                    {
                        data.FullName = null;
                        data.NationalCard = null;
                        data.Dob = null;
                        data.strDob = null;
                        data.HouseholdAddress = null;
                        data.ResidentAddress = null;
                    }
                    // thiếu thông tin của lender return null luôn không cho tạo hợp đồng
                    if (string.IsNullOrEmpty(data.FullName) || string.IsNullOrEmpty(data.NationalCard) || data.Dob == null ||
                        string.IsNullOrEmpty(data.HouseholdAddress) || string.IsNullOrEmpty(data.ResidentAddress))
                        return null;

                    if (!string.IsNullOrEmpty(data.FullName))
                    {
                        data.strFullName = data.FullName.ReduceWhitespace();
                        data.strFullName = data.FullName.TitleCaseString();
                        data.strFullName = data.FullName.Replace(" ", "").ToUpper(); // xóa hết khoảng trắng và viết hoa tất cả các chữ cái
                    }
                    if (data.Dob.HasValue && data.Dob != DateTime.MinValue)
                    {
                        data.strDob = data.Dob.Value.ToString("ddMMyyyy");
                        data.strDob = data.strDob.Trim().ToUpper();
                    }

                    //Gọi api lấy ra tiền bảo hiểm
                    var objInterestAndInsurence = new Models.LMS.InterestAndInsurenceInput()
                    {
                        TotalMoneyDisbursement = data.LoanAmount.HasValue ? (long)data.LoanAmount.Value : 0,
                        LoanTime = data.LoanTime.HasValue ? data.LoanTime.Value * 30 : 0,
                        RateType = data.RateTypeId.HasValue ? data.RateTypeId.Value : 0,
                        Frequency = 30,
                        NotSchedule = 0,
                        LoanBriefId = data.LoanBriefId,
                        Rate = (decimal)data.RatePercent.Value
                    };
                    var resultInterestAndInsurence = _lmsService.InterestAndInsurence(objInterestAndInsurence);
                    if (resultInterestAndInsurence != null && resultInterestAndInsurence.Data != null)
                    {
                        data.FeeInsuranceOfCustomer = resultInterestAndInsurence.Data.FeeInsurrance;
                        data.FeeInsuranceOfProperty = resultInterestAndInsurence.Data.FeeInsuranceMaterialCovered;
                    }

                    // lấy ra ngày ký hợp đồng cuối cùng
                    if (data.EsignState == (int)EsignState.BORROWER_SIGNED || data.EsignState == (int)EsignState.LENDER_SIGNED)
                        data.DateEsignContract = _unitOfWork.EsignContractRepository.Query(x => x.LoanBriefId == data.LoanBriefId && x.TypeEsign == (int)TypeEsign.Customer, null, false).OrderByDescending(x => x.Id).Select(x => x.CreatedAt).FirstOrDefault();

                    //tính số tiền lãi trên HĐ của lender
                    //kiểm tra xem trong loanbrief có trường LenderRate hay chưa
                    if (data.LenderRate == null)
                    {
                        var timeAppliedRate = DateTime.Now;
                        //Tính lãi suất lender
                        var lastEsignCustomer = _unitOfWork.EsignContractRepository.Query(x => x.LoanBriefId == data.LoanBriefId
                            && x.TypeEsign == (int)TypeEsign.Customer, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                        if (lastEsignCustomer != null && lastEsignCustomer.CreatedAt.HasValue)
                        {
                            timeAppliedRate = lastEsignCustomer.CreatedAt.Value;
                            var configRate = _unitOfWork.ConfigContractRateRepository.Query(x => x.IsEnable == true
                                 && timeAppliedRate >= x.AppliedAt, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                            if (configRate != null && configRate.LenderRate > 0)
                                data.LenderRate = configRate.LenderRate.Value;
                            else
                                data.LenderRate = LOS.Common.Helpers.Const.LENDER_RATE;
                        }
                    }

                    var viewString = _viewRenderService.RenderToStringAsync("ExportPDF/ContractFinancialAgreementLender", data);

                    HtmlToPdf oHtmlToPdf = new HtmlToPdf();
                    oHtmlToPdf.Options.WebPageHeight = 595;
                    oHtmlToPdf.Options.WebPageWidth = 750;
                    oHtmlToPdf.Options.MarginTop = 30;
                    oHtmlToPdf.Options.MarginLeft = 10;
                    oHtmlToPdf.Options.MarginRight = 10;
                    var oPdfDocument = new PdfDocument();
                    oPdfDocument = oHtmlToPdf.ConvertHtmlString(viewString.Result);
                    byte[] pdf = oPdfDocument.Save();
                    return Convert.ToBase64String(pdf, 0, pdf.Length);
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ExportPdfService/ContractFinancialAgreementLenderBase64 Exception");
                return null;
            }
        }
    }
}
