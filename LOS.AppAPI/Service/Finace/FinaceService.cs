﻿using LOS.AppAPI.Models.Finance;
using LOS.DAL.Dapper;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface IFinaceService
    {
        Finance.ReportDetailLoanHomeTimeModel GetDetailLoanHomeTima();
        List<Finance.ResponeReportLoanThirdParterCPQL> GetTotalLoanThirdPartnerCPQL(Finance.RequestReportLoanThirdPartner entity);
        List<Finance.ResponeReportLoanThirdParterCPS> GetTotalLoanThirdPartnerCPS(Finance.RequestReportLoanThirdPartner entity);
        List<Finance.ResponseReportDetailLoanThirdPartner> GetDetailLoanThirdPartner(Finance.RequestReportLoanThirdPartner entity);
    }
    public class FinaceService : IFinaceService
    {
        protected IConfiguration _baseConfig;
        public FinaceService(IConfiguration configuration)
        {
            _baseConfig = configuration;
        }

        public Finance.ReportDetailLoanHomeTimeModel GetDetailLoanHomeTima()
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select count(l.LoanBriefId) as total_loan,");
                sql.AppendLine("sum(case when l.Status In (100, 110)  then l.LoanAmount else 0 end) as total_money_disbursement,");
                sql.AppendLine("sum(case when DATEDIFF(month, l.CreatedTime, GETDATE()) = 0 then 1 else 0 end) as total_loan_month,");
                sql.AppendLine("sum(case when DATEDIFF(DAY, l.CreatedTime, GETDATE()) = 0 then 1 else 0 end) as total_loan_day,");
                sql.AppendLine("sum(case when ISNULL(l.ReasonCancel,0) != 660 then 1 else 0 end) as total_loan_advisory,");
                sql.AppendLine("count(distinct l.CustomerId) as total_people_registered_borrow");
                sql.AppendLine("from LoanBrief(nolock) l");
                return db.Query<Finance.ReportDetailLoanHomeTimeModel>(sql.ToString()).FirstOrDefault();
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public List<Finance.ResponeReportLoanThirdParterCPQL> GetTotalLoanThirdPartnerCPQL(Finance.RequestReportLoanThirdPartner entity)
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select CAST(l.CreatedTime as date) as day, count(l.LoanBriefId) as total_loan,");
                sql.AppendLine("sum(case when l.Status In (100, 110)  then 1 else 0 end) as total_loan_disbursement,");
                sql.AppendLine("sum(case when l.ValueCheckQualify = 252  then 1 else 0 end) as total_loan_qlf,");
                sql.AppendLine("sum(case when l.ValueCheckQualify = 248  then 1 else 0 end) as total_loan_qlf_no_document");
                sql.AppendLine("from LoanBrief(nolock) l ");
                sql.AppendFormat("where CAST(CreatedTime as date) > '{0}' and CAST(CreatedTime as date) < '{1}'", entity.from_date.ToString("yyyy-MM-dd"), entity.to_date.ToString("yyyy-MM-dd"));
                sql.AppendFormat("and TId = '{0}'", entity.third_partner_id);
                sql.AppendLine("group by CAST(CreatedTime as date) order by CAST(CreatedTime as date) ");
                return db.Query<Finance.ResponeReportLoanThirdParterCPQL>(sql.ToString());
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public List<Finance.ResponeReportLoanThirdParterCPS> GetTotalLoanThirdPartnerCPS(Finance.RequestReportLoanThirdPartner entity)
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select CAST(l.CreatedTime as date) as day, count(l.LoanBriefId) as total_loan,");
                sql.AppendLine("sum(case when l.Status In (100, 110)  then 1 else 0 end) as total_loan_disbursement");
                sql.AppendLine("from LoanBrief(nolock) l ");
                sql.AppendFormat("where CAST(CreatedTime as date) > '{0}' and CAST(CreatedTime as date) < '{1}'", entity.from_date.ToString("yyyy-MM-dd"), entity.to_date.ToString("yyyy-MM-dd"));
                sql.AppendFormat("and TId = '{0}'", entity.third_partner_id);
                sql.AppendLine("group by CAST(CreatedTime as date) order by CAST(CreatedTime as date) ");
                return db.Query<Finance.ResponeReportLoanThirdParterCPS>(sql.ToString());
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public List<Finance.ResponseReportDetailLoanThirdPartner> GetDetailLoanThirdPartner(Finance.RequestReportLoanThirdPartner entity)
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select LoanBriefId as 'loan_brief_id', Aff_Sid as 'pantner_id', Aff_Code as 'aff_code', ");
                sql.AppendLine("CASE WHEN ValueCheckQualify = 252 THEN N'QL có chứng từ' WHEN ValueCheckQualify = 248 THEN N'QL không chứng từ' ELSE N'Không QL'END as 'qlf',");
                sql.AppendLine("CASE WHEN Status = 100 THEN N'Giả ngân' WHEN Status = 110 THEN N'Đã tất toán' WHEN Status = 99 THEN N'Hủy' ELSE N'Đang xử lý' END as 'status', ");
                sql.AppendLine("RC.Reason as 'reason_cancel',  CAST(CreatedTime as date) as 'day', UtmMedium as 'publisher'");
                sql.AppendLine("from LoanBrief(NOLOCK) L left join ReasonCancel(NOLOCK) RC ON L.ReasonCancel = RC.Id");
                sql.AppendFormat("where CAST(CreatedTime as date) > '{0}' and CAST(CreatedTime as date) < '{1}'", entity.from_date.ToString("yyyy-MM-dd"), entity.to_date.ToString("yyyy-MM-dd"));
                sql.AppendFormat("and TId = '{0}'", entity.third_partner_id);
                sql.AppendLine("order by CreatedTime");
                return db.Query<Finance.ResponseReportDetailLoanThirdPartner>(sql.ToString());
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
