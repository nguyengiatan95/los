﻿using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Models.LMS;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service.LmsService
{
    public interface ILmsService
    {
        InterestAndInsurenceOutput InterestAndInsurence(InterestAndInsurenceInput param);
        CalculatorMoneyInsurance.OutPut CalculatorMoneyInsurance(CalculatorMoneyInsurance.InPut param, int LoanBriefId);
        Disbursement.OutPut DisbursementLoanCredit(Disbursement.InPut param);
        PaymentLoanById.OutPut GetPaymentLoanById(int Lms_loanId);
        Disbursement.OutPutV2 DisbursementLoanCreditV2(Disbursement.InPut param);
        SendSMSParam.OutPut SendOtpSmsBrandName(int typeOpt, string phone, string content, string otp, DateTime expired, string ipAddress = "");
        CheckReLoan.OutPut CheckReBorrow(CheckReLoan.Input param);
        ResponceInfoLender GetInfoLender(int LenderId);
        public CheckReLoan.TopupOutPut CheckTopupOto(CheckReLoan.Input param);
        public CheckReLoan.OutPut CheckReLoan(CheckReLoan.Input param);
        ResponeAddBlacklist AddBlackList(RequestAddBlacklist param);
        OutPutTopup InterestTopup(RequestCalendarPaymentTopup param);
        ResponceCalendarPaymentTopup GenCalendarPaymentTopup(RequestCalendarPaymentTopup param);
        ResponcePrematureInterestLMS GetPayAmountForCustomerByLoanID(RequestPrematureInterestLMS param, int loanbriefId);
    }
    public class LmsService : ILmsService
    {
        private IConfiguration _configuration;
        private IUnitOfWork _unitOfWork;
        public LmsService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
        }
        public InterestAndInsurenceOutput InterestAndInsurence(InterestAndInsurenceInput param)
        {
            try
            {
                var url = _configuration["AppSettings:LMS"] + Constants.InterestAndInsurence;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(param);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                request.Timeout = 5000;
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.ListLender.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = body
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _unitOfWork.LogCallApiRepository.Update(log);
                _unitOfWork.Save();
                InterestAndInsurenceOutput lst = JsonConvert.DeserializeObject<InterestAndInsurenceOutput>(json);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public CalculatorMoneyInsurance.OutPut CalculatorMoneyInsurance(CalculatorMoneyInsurance.InPut param, int LoanBriefId)
        {
            try
            {
                var url = _configuration["AppSettings:LMS"] + Constants.CalculatorMoneyInsurance;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(param);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.CalculatorMoneyInsurance.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = LoanBriefId,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _unitOfWork.LogCallApiRepository.Update(log);
                _unitOfWork.Save();
                CalculatorMoneyInsurance.OutPut lst = JsonConvert.DeserializeObject<CalculatorMoneyInsurance.OutPut>(json);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Disbursement.OutPut DisbursementLoanCredit(Disbursement.InPut param)
        {
            try
            {
                var url = _configuration["AppSettings:LMS"] + Constants.DisbursementLoanCredit;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(param);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.DisbursementLoanCreditOfAppLender.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = param.LoanCreditId,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _unitOfWork.LogCallApiRepository.Update(log);
                _unitOfWork.Save();
                Disbursement.OutPut lst = JsonConvert.DeserializeObject<Disbursement.OutPut>(json);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Disbursement.OutPutV2 DisbursementLoanCreditV2(Disbursement.InPut param)
        {
            try
            {
                var url = _configuration["AppSettings:LMS"] + Constants.DisbursementLoanCreditV2;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(param);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.DisbursementLoanCreditOfAppLender.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = param.LoanCreditId,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _unitOfWork.LogCallApiRepository.Update(log);
                _unitOfWork.Save();
                Disbursement.OutPutV2 data = JsonConvert.DeserializeObject<Disbursement.OutPutV2>(json);
                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public PaymentLoanById.OutPut GetPaymentLoanById(int Lms_loanId)
        {
            var url = _configuration["AppSettings:LMS"] + String.Format(Constants.GetPaymentById, Lms_loanId);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);
            var json = response.Content;

            PaymentLoanById.OutPut lst = JsonConvert.DeserializeObject<PaymentLoanById.OutPut>(json);
            return lst;
        }
        public SendSMSParam.OutPut SendOtpSmsBrandName(int typeOpt, string phone, string content, string otp, DateTime expired, string ipAddress = "")
        {
            try
            {
                string url = _configuration["AppSettings:SMS"] + Constants.SEND_SMS;
                SendSMSParam.Input param = new SendSMSParam.Input();
                param.Domain = "los.tima.vn";
                param.Department = "Phòng IT";
                param.Phone = phone;
                param.MessageContent = content;
                var body = JsonConvert.SerializeObject(param);
                var client = new RestClient(url);
                client.Timeout = 5000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "8JTLS9eqmSKXdQKdNWrtXCMV5DAhC3k7");
                request.AddHeader("account", "LOS");
                request.AddHeader("password", "3cmELn3UsqFkvERbuJdzHtUL7HU6uFuH");
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var log = new LogSendOtp
                {
                    Phone = phone,
                    Otp = otp,
                    Request = body,
                    CreatedAt = DateTime.Now,
                    IpAddress = ipAddress,
                    Url = url,
                    TypeSendOtp = (int)TypeSendOtp.SmsBrandName,
                    ExpiredVerifyOtp = expired,
                    TypeOtp = typeOpt
                };
                _unitOfWork.LogSendOtpRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                _unitOfWork.LogSendOtpRepository.Update(x => x.Id == log.Id, x => new LogSendOtp()
                {
                    Response = json,
                    ModifyAt = DateTime.Now,
                    StatusSend = (int)StatusCallApi.Success
                });
                SendSMSParam.OutPut data = JsonConvert.DeserializeObject<SendSMSParam.OutPut>(json);
                if (data != null && data.Result == (int)StatusCallApi.Success)
                    _unitOfWork.LogSendOtpRepository.Update(x => x.Id == log.Id, x => new LogSendOtp()
                    {
                        StatusSend = (int)StatusCallApi.Success
                    });
                return data;
            }
            catch (Exception ex)
            {
                return null;
            }


        }
        public CheckReLoan.OutPut CheckReBorrow(CheckReLoan.Input param)
        {
            var obj = new CheckReLoan.Input
            {
                NumberCard = param.NumberCard,
                CustomerName = param.CustomerName
            };
            var url = _configuration["AppSettings:LMS"] + Constants.DeferredPayment;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(obj);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var json = response.Content;

                    CheckReLoan.OutputPayment lst = JsonConvert.DeserializeObject<CheckReLoan.OutputPayment>(json);
                    CheckReLoan.OutPut lstReLoan = new CheckReLoan.OutPut();

                    long maxCountDayLate = 0; //Số ngày trả muộn lâu nhất trong các kỳ
                    if (lst.Result == 1)
                    {
                        lstReLoan.Result = 1;
                        if (lst.Data != null && lst.Data.LstLoanCustomer != null && lst.Data.LstLoanCustomer.Count() > 0)
                        {
                            if (lst.Data.LstLoanCustomer[0].Status == EnumLoanStatus.FINISH.GetHashCode())
                            {
                                //Kiểm tra tổng số kỳ thanh toán của tất cả HĐ tối thiểu 3 kỳ

                                var countPayment = 0;// Tổng các kỳ thanh toán
                                var index = 0;
                                foreach (var item in lst.Data.LstLoanCustomer)
                                {
                                    if (item.LstPaymentCustomer != null && item.LstPaymentCustomer.Count() > 0)
                                    {
                                        //tính tổng các kỳ thanh toán
                                        countPayment += item.LstPaymentCustomer.Count();
                                        //Lấy số ngày trả chậm cao nhất trong cac kỳ
                                        var maxCountDay = item.LstPaymentCustomer.Max(x => x.CountDay);
                                        if (maxCountDayLate < maxCountDay)
                                            maxCountDayLate = maxCountDay;
                                    }
                                    index++;
                                }
                                lstReLoan.MaxCountDayLate = maxCountDayLate;
                            }
                            else
                            {
                                foreach (var item in lst.Data.LstLoanCustomer)
                                {
                                    if (item.LstPaymentCustomer != null && item.LstPaymentCustomer.Count() > 0)
                                    {
                                        //Lấy số ngày trả chậm cao nhất trong cac kỳ
                                        var maxCountDay = item.LstPaymentCustomer.Max(x => x.CountDay);
                                        if (maxCountDayLate < maxCountDay)
                                            maxCountDayLate = maxCountDay;
                                    }
                                }
                                lstReLoan.MaxCountDayLate = maxCountDayLate;
                            }
                        }
                    }
                    return lstReLoan;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ResponceInfoLender GetInfoLender(int LenderId)
        {
            try
            {
                var url = _configuration["AppSettings:LMS"] + string.Format(Constants.GetInfoLender, LenderId);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", "8228459735");
                request.Timeout = 5000;
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.InfoLender.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = LenderId.ToString()
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _unitOfWork.LogCallApiRepository.Update(log);
                _unitOfWork.Save();
                ResponceInfoLender lst = JsonConvert.DeserializeObject<ResponceInfoLender>(json);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public CheckReLoan.TopupOutPut CheckTopupOto(CheckReLoan.Input param)
        {
            var obj = new DeferredPayment.Input
            {
                NumberCard = param.NumberCard,
                CustomerName = param.CustomerName
            };
            if (!obj.CustomerName.IsNormalized())
                obj.CustomerName = obj.CustomerName.Normalize();
            var url = _configuration["AppSettings:LMS"] + Constants.DeferredPayment;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(obj);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            var json = response.Content;
            DeferredPayment.Output lst = JsonConvert.DeserializeObject<DeferredPayment.Output>(json);
            CheckReLoan.TopupOutPut lstTopup = new CheckReLoan.TopupOutPut();

            if (lst != null && lst.Result == 1)
            {
                lstTopup.IsAccept = 0;
                if (lst.Data != null && lst.Data.LstLoanCustomer != null)
                {

                    //Lấy ra những đơn đang vay và là gói oto và có đơn đang vay
                    //Kiểm tra điều kiện topup
                    // 1- khoản vay oto đang còn hiệu lực
                    var lstData = lst.Data.LstLoanCustomer.Where(x => (x.Status == (int)EnumStatusLMS.Disbursed || x.Status == (int)EnumStatusLMS.InterestFeeDebt || x.Status == (int)EnumStatusLMS.PaymentNow)
                    && (x.ProductID == (int)EnumProductCredit.OtoCreditType_CC || x.ProductID == (int)EnumProductCredit.OtoCreditType_KCC)).ToList();

                    if (lstData != null && lstData.Count > 0)
                    {
                        var isKyThanhToan = true;
                        var isNoQuaHan = true;
                        int noQuaHan = Convert.ToInt32(_configuration["AppSettings:NoQuaHan"]);
                        var totalMoneyCurrent = 0L;
                        foreach (var item in lstData)
                        {
                            // 2- Kiểm tra các đơn đang vay đã thanh toán tối thiểu 1 kỳ
                            if (item.LstPaymentCustomer.Count() == 0)
                            {
                                isKyThanhToan = false;
                                break;
                            }
                            // 3- Lịch sử thanh toán trên các khoản đang vay hiện tại không nợ quá hạn trên 30 ngày                           
                            foreach (var itemPayment in item.LstPaymentCustomer)
                            {
                                if (itemPayment.CountDay > noQuaHan)
                                {
                                    isNoQuaHan = false;
                                    break;
                                }
                            }
                            if (!isNoQuaHan)
                                break;

                            //TotalMoneyCurrent 
                            totalMoneyCurrent = totalMoneyCurrent + item.TotalMoneyCurrent;
                        }
                        if (!isKyThanhToan)
                            lstTopup.Message = "Không đủ điều kiện do khách hàng có khoản vay chưa thanh toán kỳ nào";
                        if (!isNoQuaHan)
                            lstTopup.Message = "Không đủ điều kiện do khách hàng có kỳ trả chậm > " + noQuaHan + " ngày";
                        if (isKyThanhToan && isNoQuaHan)
                        {
                            lstTopup.IsAccept = 1;
                            if (lstData[0].LoanBriefID > 0)
                                lstTopup.LoanBriefId = lstData[0].LoanBriefID;
                            else
                                lstTopup.LoanBriefId = lstData[0].LoanId;

                            lstTopup.TotalMoneyCurrent = totalMoneyCurrent;
                        }

                    }
                    else
                    {
                        lstTopup.Message = "Khách hàng không có đơn vay ô tô nào ở trạng thái đang vay";
                    }
                }
                else
                {
                    lstTopup.Message = lst.Message;
                }

            }



            return lstTopup;
        }
        public CheckReLoan.OutPut CheckReLoan(CheckReLoan.Input param)
        {
            var obj = new DeferredPayment.Input
            {
                NumberCard = param.NumberCard,
                CustomerName = param.CustomerName
            };
            var url = _configuration["AppSettings:LMS"] + Constants.DeferredPayment;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(obj);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            DeferredPayment.Output lst = JsonConvert.DeserializeObject<DeferredPayment.Output>(json);
            CheckReLoan.OutPut lstReLoan = new CheckReLoan.OutPut();

            if (lst.Result == 1)
            {
                lstReLoan.Result = 1;
                if (lst.Data != null)
                {

                    if (lst.Data.LstLoanCustomer[0].Status == EnumLoanStatus.FINISH.GetHashCode())
                    {
                        var CountPayment = 0;
                        foreach (var item in lst.Data.LstLoanCustomer)
                        {
                            CountPayment = CountPayment + item.LstPaymentCustomer.Count() + 1;
                            if (CountPayment >= 3)
                                break;


                        }
                        if (CountPayment < 3)
                        {
                            lstReLoan.Message = "Không đủ điều kiện do tổng kỳ thanh toán chưa đạt điều kiện";
                            return lstReLoan;
                        }


                        //Khoản vay gần nhất của KH không có kỳ nào quá hạn trên 30 ngày
                        int DayReLoan = Convert.ToInt32(_configuration["AppSettings:DayReLoan"]);

                        var CountDate = 0;
                        foreach (var item in lst.Data.LstLoanCustomer[0].LstPaymentCustomer)
                        {
                            if (item.CountDay > DayReLoan)
                            {
                                CountDate = Convert.ToInt32(item.CountDay);
                                break;
                            }
                        }
                        if (CountDate <= DayReLoan)
                        {
                            lstReLoan.IsAccept = 1;
                            if (lst.Data.LstLoanCustomer[0].LoanBriefID > 0)
                                lstReLoan.LoanBriefID = lst.Data.LstLoanCustomer[0].LoanBriefID;
                            else
                                lstReLoan.LoanBriefID = lst.Data.LstLoanCustomer[0].LoanId;
                            lstReLoan.Message = "Thành công!";
                        }
                        else
                        {
                            lstReLoan.Message = "Khách hàng có kỳ trả chậm " + CountDate + "!";
                        }
                    }
                    else
                    {
                        lstReLoan.Message = "Khách hàng chưa đủ điều kiện tái vay do đơn vay đang ở trạng thái " + lst.Data.LstLoanCustomer[0].StatusName;
                    }
                }
                else
                {
                    lstReLoan.Message = lst.Message;
                }

            }
            return lstReLoan;
        }
        public ResponeAddBlacklist AddBlackList(RequestAddBlacklist param)
        {
            var url = _configuration["AppSettings:LMS"] + Constants.AddBlacklist;
            var body = JsonConvert.SerializeObject(param);
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.AddBlacklist.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = param.LoanCreditId,
                    Input = body,
                    CreatedAt = DateTime.Now,
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                var response = client.Execute<ResponeAddBlacklist>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    log.Status = (int)StatusCallApi.Success;
                    log.Output = response.Content;
                    log.ModifyAt = DateTime.Now;
                    _unitOfWork.LogCallApiRepository.Update(log);
                    _unitOfWork.Save();
                    var lst = JsonConvert.DeserializeObject<ResponeAddBlacklist>(response.Content);
                    return lst;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public OutPutTopup InterestTopup(RequestCalendarPaymentTopup param)
        {
            var url = _configuration["AppSettings:LMS"] + Constants.InterestTopup;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(param);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.IntersestTopUp.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = param.LoanId,
                    Input = body,
                    CreatedAt = DateTime.Now,
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    log.Status = (int)StatusCallApi.Success;
                    log.Output = response.Content;
                    log.ModifyAt = DateTime.Now;
                    _unitOfWork.LogCallApiRepository.Update(log);
                    _unitOfWork.Save();
                    OutPutTopup lst = JsonConvert.DeserializeObject<OutPutTopup>(response.Content);
                    return lst;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public ResponceCalendarPaymentTopup GenCalendarPaymentTopup(RequestCalendarPaymentTopup param)
        {
            var dicPaymentSchedule = new Dictionary<string, List<PaymentScheduleBasic>>();
            var result = InterestTopup(param);
            var lstData = new ResponceCalendarPaymentTopup();
            if (result != null && result.Data != null && result.Data.ListLoanPaymentSchedule != null && result.Data.ListLoanPaymentSchedule.Count > 0)
            {
                lstData.MoneyFeeInsuranceOfCustomer = result.Data.MoneyFeeInsuranceOfCustomer;
                lstData.MoneyInsuranceOfMaterialCovered = result.Data.MoneyInsuranceOfMaterialCovered;
                foreach (var item in result.Data.ListLoanPaymentSchedule)
                {
                    if (item.PaymentSchedule != null && item.PaymentSchedule.Count > 0)
                    {
                        //add dữ liệu vào dic
                        foreach (var item2 in item.PaymentSchedule)
                        {
                            var keyDic = item2.DaysPayable.ToString("dd/MM/yyyy");
                            if (!dicPaymentSchedule.ContainsKey(keyDic))
                                dicPaymentSchedule[keyDic] = new List<PaymentScheduleBasic>();
                            dicPaymentSchedule[keyDic].Add(item2);

                        }
                    }
                }
                lstData.DataTopUp = dicPaymentSchedule;
            }
            return lstData;
        }
        public ResponcePrematureInterestLMS GetPayAmountForCustomerByLoanID(RequestPrematureInterestLMS param, int loanbriefId)
        {
            var url = _configuration["AppSettings:LMS"] + Constants.GetPayAmountForCustomerByLoanID;
            var body = JsonConvert.SerializeObject(param);
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.GetPayAmountForCustomerByLoanID.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    Input = body,
                    CreatedAt = DateTime.Now,
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                var response = client.Execute<ResponcePrematureInterestLMS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    log.Status = (int)StatusCallApi.Success;
                    log.Output = response.Content;
                    log.ModifyAt = DateTime.Now;
                    _unitOfWork.LogCallApiRepository.Update(log);
                    _unitOfWork.Save();
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponcePrematureInterestLMS>(response.Content);
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
