﻿using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface IMomoService
    {
        Task<CheckMomoResponse> CheckMomo(int loanBriefId, string nationalCard);
    }
    public class MomoService : IMomoService
    {
        private IConfiguration _configuration;
        private IUnitOfWork _unitOfWork;
        public MomoService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
        }
        public async Task<CheckMomoResponse> CheckMomo(int loanBriefId, string nationalCard)
        {
            try
            {
                var url = _configuration["AppSettings:AIAuthentication"] + String.Format(Constants.CHECK_LOAN_MOMO, nationalCard);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Timeout = 20000;
                request.AddHeader("Content-Type", "application/json");

                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    ServiceType = ServiceTypeAI.CheckLoanMomo.GetHashCode(),
                    LoanbriefId = loanBriefId,
                    Token = "",
                    Url = url,
                    CreatedAt = DateTime.Now
                };
                await _unitOfWork.LogLoanInfoAiRepository.InsertAsync(logLoanInfoAi);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                logLoanInfoAi.Status = 1;//gọi thành công
                logLoanInfoAi.ResponseRequest = jsonResponse;
                logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode();
                _unitOfWork.LogLoanInfoAiRepository.Update(logLoanInfoAi);
                _unitOfWork.Save();
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<CheckMomoResponse>(jsonResponse);
                if (result != null && result.Status == 0)
                {
                    if(result.Data.Bills.Count > 0)
                    {
                        foreach (var item in result.Data.Bills)
                        {
                            //Lưu log check Momo
                            var objLogCheck = new LogLoanInfoPartner
                            {
                                LoanbriefId = loanBriefId,
                                BillId = item.BillId,
                                ServiceName = item.ServiceName,
                                TotalAmount = item.TotalAmount,
                                PartnerName = item.PartnerName,
                                PartnerPhone = item.PartnerPhone,
                                PartnerEmail = item.PartnerEmail,
                                PartnerPersonalId = item.PartnerPersonalId,
                                ExpiredDate = item.ExpiredDate,
                                Status = item.Status,
                                CreatedDate = DateTime.Now
                            };
                            _unitOfWork.LogLoanInfoPartnerRepository.Insert(objLogCheck);
                            _unitOfWork.Save();
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
