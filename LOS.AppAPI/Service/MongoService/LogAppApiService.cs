﻿using LOS.AppAPI.Models.Mongo;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service.MongoService
{
    public interface ILogRequestResponse
    {
        LogAppAPI Create(LogAppAPI log);
    }
    public class LogAppApiService : ILogRequestResponse
    {
        private readonly IMongoCollection<LogAppAPI> _logAppApi;
        public LogAppApiService(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _logAppApi = database.GetCollection<LogAppAPI>("logappapi");
        }

        public LogAppAPI Create(LogAppAPI log)
        {
            try
            {
                _logAppApi.InsertOne(log);
                return log;
            }
            catch(Exception ex)
            {
                return null;
            }
           
        }
    }
}
