﻿using LOS.AppAPI.Models.Notification;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service.Notification
{
    public interface IPushNotification
    {
        bool SendNotificationToLender(NotificationLenderReq data);
    }
    public class PushNotificationService: IPushNotification
    {
        private IUnitOfWork _unitOfWork;
        public PushNotificationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public bool SendNotificationToLender(NotificationLenderReq data)
        {
            try
            {
                var url = "http://103.35.65.122:7676/api/notifications/push_noti";
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(data);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.PushNotificationLender.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = data.data.First().loanCreditId,
                    CreatedAt = DateTime.Now,
                    Input = body
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _unitOfWork.LogCallApiRepository.Update(log);
                _unitOfWork.Save();
                NotificationLenderRes result = JsonConvert.DeserializeObject<NotificationLenderRes>(json);
                return result.meta.errCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
