﻿using LOS.DAL.Dapper;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface IPermissionRequest
    {
        bool IsPermissionIPRequest(string servername, IPAddress serverip);
        bool IsPermissionRequest(string controller, string action, int userid);

        bool VerifyToken(string token);
    }
    public class PermissionRequestService : IPermissionRequest
    {
        private IUnitOfWork _unitOfWork;
        protected IConfiguration _baseConfig;

        public PermissionRequestService(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
            .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
            .AddJsonFile("appsettings.json", true)
            .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = configuration;
        }


        public bool IsPermissionIPRequest(string servername, IPAddress ipaddress)
        {
            var permission = false;
            try
            {
                var listPer = ListWhiteIp(servername);
                if (listPer != null && listPer.Count > 0)
                {
                    if (listPer.Any(x => x.IpAddress.Trim() == "*"))
                        return true;

                    foreach (var item in listPer)
                    {
                        if (!string.IsNullOrEmpty(item.IpAddress))
                        {
                            IPAddress clientIpAddr;
                            if (IPAddress.TryParse(item.IpAddress.Split(':')[0], out clientIpAddr))
                            {
                                if (ipaddress.Equals(clientIpAddr))
                                    return true;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "API_APP PermissionRequestService/IsPermissionIPRequest exception");
            }

            return permission;
        }

        private List<PermissionIp> ListWhiteIp(string serverName)
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                return db.Query<PermissionIp>($"SELECT * FROM PermissionIp(nolock) WHERE ServerName = '{serverName}' ");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool IsPermissionRequest(string controller, string action, int userid)
        {
            var result = false;
            try
            {
                if (_unitOfWork.UserModuleRepository.Any(x => x.UserId == userid && x.Module.Controller.ToLower() == controller.ToLower()
                 && x.Module.Action.ToLower() == action.ToLower()))
                    return true;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "API_APP PermissionRequestService/IsPermissionRequest exception");
            }
            return result;
        }

        public bool VerifyToken(string token)
        {
           
            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = _baseConfig["JWT:Issuer"],
                    ValidAudience = _baseConfig["JWT:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_baseConfig["JWT:SigningKey"]))
                }, out SecurityToken validatedToken);
            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
