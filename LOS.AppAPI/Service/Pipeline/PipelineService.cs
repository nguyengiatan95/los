﻿using LOS.AppAPI.Models;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface IPipelineService
    {
        int ChangePipeline(ChangePipeline entity);
    }
    public class PipelineService : IPipelineService
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;

        public PipelineService(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
        }
        public int ChangePipeline(ChangePipeline entity)
        {
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                var result = SimulatePipeline(loanBrief, entity.ProductId, entity.Status);
                if (result == 1) //success
                    return 1;
                else if (result == -1) //status not match
                    return -1;
                else if (result == -2) //pipeline not match
                    return -2;
                else //fail
                    return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private int SimulatePipeline(LoanBrief loanBrief, int newProductId, int newStatus)
        {
            try
            {
                var pipelines = GetPipeline();
                bool IsOk = false;
                if (pipelines != null && pipelines.Count > 0)
                {
                    DatabaseHelper<LoanBrief> helper = new DatabaseHelper<LoanBrief>();
                    // gán lại productId
                    loanBrief.ProductId = newProductId;
                    //if (loanBrief.CurrentPipelineId != null)
                    //{
                    PipelineDTO newPipeline = null;
                    foreach (var pipeline in pipelines)
                    {
                        var sections = pipeline.Sections;
                        if (sections != null && sections.Count() > 0)
                        {
                            SectionDetailDTO lastAutomaticSuccess = null;
                            int indexApproveSucess = -1;
                            foreach (var section in sections)
                            {
                                // for each detail	
                                int detailSucess = 0;
                                int detailCount = section.Details != null ? section.Details.Count() : 0;
                                if (detailCount == 0)
                                {
                                    goto gotonextPipeline;
                                }
                                foreach (var detail in section.Details)
                                {
                                    if (detail.Mode == "AUTOMATIC")
                                    {
                                        if (detail.Approves != null && detail.Approves.Count() > 0)
                                        {
                                            bool IsOneSuccess = false;
                                            int index = 0;
                                            foreach (var approve in detail.Approves)
                                            {
                                                IsOneSuccess = false;
                                                int conditionSuccess = 0;
                                                int conditionCount = approve.Conditions != null ? approve.Conditions.Count() : 0;
                                                if (conditionCount <= 0)
                                                {
                                                    goto gotonextPipeline;
                                                }
                                                foreach (var condition in approve.Conditions)
                                                {
                                                    if (helper.CheckFieldOperator(loanBrief, condition.Property.FieldName, condition.Operator, condition.Value, condition.Property.Type))
                                                    {
                                                        indexApproveSucess = index;
                                                        Console.WriteLine("Condition successed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                        conditionSuccess++;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("Condition failed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                    }
                                                }
                                                // nếu điều kiện thành công hết thì chuyển đến aprrove section
                                                if (conditionSuccess == conditionCount)
                                                {
                                                    // detail success
                                                    detailSucess++;
                                                    lastAutomaticSuccess = detail;
                                                    IsOneSuccess = true;
                                                    break;
                                                }
                                                index++;
                                            }
                                            if (!IsOneSuccess)
                                            {
                                                goto gotonextPipeline;
                                            }
                                            else
                                            {
                                                IsOk = true;
                                                newPipeline = pipeline;
                                                goto successed;
                                            }
                                        }
                                        else
                                        {
                                            return 0;
                                        }
                                    }
                                    else
                                    {
                                        // kiêm tra xem có đúng section sau khi kiểm tra không
                                        if (lastAutomaticSuccess != null)
                                        {
                                            if (indexApproveSucess >= 0)
                                            {
                                                if (lastAutomaticSuccess.Approves.ToList()[indexApproveSucess].ApproveId == detail.SectionDetailId)
                                                {
                                                    IsOk = true;
                                                    newPipeline = pipeline;
                                                    goto successed;
                                                }
                                                else
                                                {
                                                    goto gotonextPipeline;
                                                }
                                            }
                                            else
                                            {
                                                goto gotonextPipeline;
                                            }
                                        }
                                        else
                                        {
                                            goto gotonextPipeline;
                                        }
                                    }
                                }
                            }
                        }
                    gotonextPipeline:
                        {
                            Console.WriteLine("Go to next pipeline, Current pipeline : " + pipeline.Name);
                        }
                    }
                successed:
                    {
                        if (IsOk)
                        {
                            if (newPipeline != null)
                            {
                                // tìm xem action có status tương ứng
                                var sectionAction = _unitOfWork.SectionActionRepository.Query(x => x.PropertyId == 4 && x.Value == newStatus.ToString() && x.SectionDetail.Status != 99 && x.SectionDetail.Section.PipelineId == newPipeline.PipelineId, null, false).FirstOrDefault();
                                if (sectionAction != null)
                                {
                                    var sectionDetail = _unitOfWork.SectionDetailRepository.GetById(sectionAction.SectionDetailId);
                                    // gán lại pipeline đơn vay
                                    if (sectionDetail != null)
                                    {
                                        //save log
                                        var currentHistory = new PipelineHistory();
                                        currentHistory.ActionState = null;
                                        currentHistory.BeginSectionDetailId = loanBrief.CurrentSectionDetailId;
                                        currentHistory.BeginSectionId = loanBrief.CurrentSectionId;
                                        currentHistory.BeginStatus = loanBrief.Status;
                                        currentHistory.BeginTime = DateTimeOffset.Now;
                                        currentHistory.CreatedDate = DateTimeOffset.Now;
                                        currentHistory.Data = JsonConvert.SerializeObject(loanBrief);
                                        currentHistory.LoanBriefId = loanBrief.LoanBriefId;
                                        currentHistory.PipelineId = newPipeline.PipelineId;
                                        currentHistory.EndSectionDetailId = sectionDetail.SectionDetailId;
                                        currentHistory.EndSectionId = sectionDetail.SectionId;
                                        if (sectionDetail.Mode.Equals("MANUAL"))
                                            currentHistory.EndPipelineState = 20;
                                        else
                                            currentHistory.EndPipelineState = 30;
                                        currentHistory.EndStatus = newStatus;
                                        currentHistory.ExecuteTime = 0;
                                        _unitOfWork.PipelineHistoryRepository.Insert(currentHistory);
                                        loanBrief.CurrentPipelineId = newPipeline.PipelineId;
                                        loanBrief.CurrentSectionId = sectionDetail.SectionId;
                                        loanBrief.CurrentSectionDetailId = sectionDetail.SectionDetailId;
                                        loanBrief.Status = newStatus;
                                        if (sectionDetail.Mode.Equals("MANUAL"))
                                            loanBrief.PipelineState = 20;
                                        else
                                            loanBrief.PipelineState = 30;
                                        loanBrief.AddedToQueue = false;
                                        loanBrief.InProcess = 0;
                                        loanBrief.ActionState = null;
                                        _unitOfWork.LoanBriefRepository.Update(loanBrief);
                                        _unitOfWork.SaveChanges();
                                        return 1;
                                    }
                                }
                                else
                                {
                                    return -1;
                                }
                                Console.WriteLine("Pipeline Successed:" + loanBrief.LoanBriefId);
                            }
                        }
                        else
                        {
                            return -2;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Pipeline Exception:" + ex.Message);
                Console.WriteLine("Pipeline Exception:" + ex.StackTrace);
            }
            return 0;
        }
        private List<PipelineDTO> GetPipeline()
        {
            try
            {
                return _unitOfWork.PipelineRepository.Query(x => x.Status != 99 && x.Status != 0, x => x.OrderBy(x1 => x1.Priority), false)
                    .Select(x => new PipelineDTO()
                    {
                        CreatedAt = x.CreatedAt,
                        CreatorId = x.CreatorId,
                        ModifiedAt = x.ModifiedAt,
                        Name = x.Name,
                        PipelineId = x.PipelineId,
                        Priority = x.Priority,
                        Status = x.Status,
                        Sections = x.Section.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Order).Select(x1 => new SectionDTO()
                        {
                            Name = x1.Name,
                            Order = x1.Order,
                            SectionGroupId = x1.SectionGroupId,
                            SectionId = x1.SectionId,
                            IsShow = x1.IsShow,
                            PipelineId = x1.PipelineId,
                            Details = x1.SectionDetail.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Step).Select(x2 => new SectionDetailDTO()
                            {
                                Mode = x2.Mode,
                                Name = x2.Name,
                                SectionDetailId = x2.SectionDetailId,
                                SectionId = x2.SectionId,
                                Step = x2.Step,
                                IsShow = x2.IsShow,
                                Approves = x2.SectionApproveSectionDetail.Select(x3 => new SectionApproveDTO()
                                {
                                    Name = x3.Name,
                                    Approve = x3.Approve != null ? new SectionDetailDTO()
                                    {
                                        Name = x3.Approve.Name,
                                        SectionDetailId = x3.Approve.SectionDetailId,
                                        SectionId = x3.Approve.SectionId,
                                        Step = x3.Approve.Step
                                    } : null,
                                    SectionApproveId = x3.SectionApproveId,
                                    SectionDetailId = x3.SectionDetailId,
                                    ApproveId = x3.ApproveId,
                                    Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
                                    {
                                        Operator = x4.Operator,
                                        PropertyId = x4.PropertyId,
                                        SectionConditionId = x4.SectionConditionId,
                                        SectionApproveId = x4.SectionApproveId,
                                        Value = x4.Value,
                                        Property = x4.Property != null ? new PropertyDTO()
                                        {
                                            Object = x4.Property.Object,
                                            FieldName = x4.Property.FieldName,
                                            Priority = x4.Property.Priority,
                                            Regex = x4.Property.Regex,
                                            Type = x4.Property.Type,
                                            PropertyId = x4.Property.PropertyId,
                                            Name = x4.Property.Name,
                                            LinkObject = x4.Property.LinkObject
                                        } : null
                                    })
                                }),
                                DisapproveId = x2.DisapproveId,
                                Disapprove = x2.Disapprove != null ? new SectionDetailDTO()
                                {
                                    Name = x2.Disapprove.Name,
                                    SectionDetailId = x2.Disapprove.SectionDetailId,
                                    SectionId = x2.Disapprove.SectionId,
                                    Step = x2.Disapprove.Step
                                } : null,
                                ReturnId = x2.ReturnId,
                                Return = x2.Return != null ? new SectionDetailDTO()
                                {
                                    Name = x2.Return.Name,
                                    SectionDetailId = x2.Return.SectionDetailId,
                                    SectionId = x2.Return.SectionId,
                                    Step = x2.Return.Step
                                } : null,
                                Actions = x2.SectionAction.Select(x3 => new SectionActionDTO()
                                {
                                    PropertyId = x3.PropertyId,
                                    SectionActionId = x3.SectionActionId,
                                    SectionDetailId = x3.SectionDetailId,
                                    Value = x3.Value,
                                    Type = x3.Type,
                                    Property = x3.Property != null ? new PropertyDTO()
                                    {
                                        Object = x3.Property.Object,
                                        FieldName = x3.Property.FieldName,
                                        Priority = x3.Property.Priority,
                                        Regex = x3.Property.Regex,
                                        Type = x3.Property.Type,
                                        PropertyId = x3.Property.PropertyId,
                                        Name = x3.Property.Name,
                                        LinkObject = x3.Property.LinkObject
                                    } : null
                                })
                            })
                        })
                    }).ToList();
            }
            catch (Exception ex)
            {

            }
            return null;
        }
    }
}
