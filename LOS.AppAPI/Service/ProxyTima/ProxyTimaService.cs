﻿using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface IProxyTimaService
    {
        InfomationFamilyKalapa GetFamily(string nationalCard, int loanbriefId);
        InsuranceInfo.Output GetInsuranceInfo(string nationalId1, string nationalId2, string fullName, string dob, int loanbriefId);
    }
    public class ProxyTimaService : IProxyTimaService
    {
        private IConfiguration _configuration;
        private IUnitOfWork _unitOfWork;
        public ProxyTimaService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
        }
        public InfomationFamilyKalapa GetFamily(string nationalCard, int loanbriefId)
        {
            try
            {
                string url = _configuration["AppSettings:Proxy"] + string.Format(Constants.Kalapa_GetFamily, nationalCard);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "8JTLS9eqmSKXdQKdNWrtXCMV5DAhC3k7");
                request.AddHeader("account", "LOS");
                request.AddHeader("password", "3cmELn3UsqFkvERbuJdzHtUL7HU6uFuH");
                request.AddHeader("Content-Type", "application/json");
                var client = new RestClient(url);
                client.Timeout = 5000;
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.KalapaFamily,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = "",
                    LoanBriefId = loanbriefId,
                    Input = string.Format("{0} - {1}", nationalCard, loanbriefId),
                    CreatedAt = DateTime.Now
                };
                var insertLog = _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = response.Content;
                    insertLog.Status = (int)StatusCallApi.Success;
                    insertLog.Output = json;
                    _unitOfWork.LogCallApiRepository.Update(insertLog);
                    _unitOfWork.Save();
                    InfoFamilyRes data = JsonConvert.DeserializeObject<InfoFamilyRes>(json);
                    if (data.Result == 1 && !string.IsNullOrEmpty(data.Data))
                    {
                        data.Data = data.Data.Replace("\\", "");
                        return JsonConvert.DeserializeObject<InfomationFamilyKalapa>(data.Data);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public InsuranceInfo.Output GetInsuranceInfo(string nationalId1, string nationalId2, string fullName, string dob, int loanbriefId)
        {
            try
            {
                string url = _configuration["AppSettings:SMS"] + string.Format(Constants.ProxyGetInsuranceInfo, nationalId1, nationalId2, fullName, dob);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "8JTLS9eqmSKXdQKdNWrtXCMV5DAhC3k7");
                request.AddHeader("account", "LOS");
                request.AddHeader("password", "3cmELn3UsqFkvERbuJdzHtUL7HU6uFuH");
                request.AddHeader("Content-Type", "application/json");
                var client = new RestClient(url);
                client.Timeout = 5000;
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.InsuranceInfo,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = "",
                    LoanBriefId = loanbriefId,
                    CreatedAt = DateTime.Now
                };
                var insertLog = _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = response.Content;
                    insertLog.Status = (int)StatusCallApi.Success;
                    insertLog.Output = json;
                    insertLog.ModifyAt = DateTime.Now;
                    _unitOfWork.LogCallApiRepository.Update(insertLog);
                    _unitOfWork.Save();
                    InsuranceInfo.Output data = JsonConvert.DeserializeObject<InsuranceInfo.Output>(json);

                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
