﻿using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static LOS.AppAPI.Models.SSO.SSO;

namespace LOS.AppAPI.Service.SSOService
{
    public interface ISSOService
    {
        LoginOutput LoginSSO(LoginInput req);
    }
    public class SSOService: ISSOService
    {
        private IConfiguration _configuration;
        private IUnitOfWork _unitOfWork;
        public SSOService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
        }

        public LoginOutput LoginSSO(LoginInput req)
        {
            try
            {
                var url = _configuration["AppSettings:SSO"] + Constants.LoginSSO;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(req);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.LoginSSO.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _unitOfWork.LogCallApiRepository.Update(log);
                _unitOfWork.Save();
                LoginOutput lst = JsonConvert.DeserializeObject<LoginOutput>(json);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
