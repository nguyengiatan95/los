﻿using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models.AI;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface ICheckBankService
    {
        CheckBankReponce CheckBank(int loanbriefId, string stk, string bankId);
    }
    public class CheckBankService : ICheckBankService
    {
        private IConfiguration _configuration;
        private IUnitOfWork _unitOfWork;
        public CheckBankService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
        }
        public CheckBankReponce CheckBank(int loanbriefId, string stk, string bankId)
        {
            try
            {
                var url = _configuration["AppSettings:CheckBank"] + String.Format(Constants.CHECK_BANK_ENDPOINT, bankId, stk);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);

                //add log
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.CheckBankAI,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    LinkCallApi = url,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();

                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;

                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                objLogCallApi.ModifyAt = DateTime.Now;
                _unitOfWork.LogCallApiRepository.Update(objLogCallApi);
                _unitOfWork.Save();

                CheckBankReponce lst = Newtonsoft.Json.JsonConvert.DeserializeObject<CheckBankReponce>(jsonResponse);
                return lst;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
