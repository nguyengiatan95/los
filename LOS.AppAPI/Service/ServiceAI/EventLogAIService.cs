﻿using LOS.AppAPI.DTOs.Appraiser;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.Common.Extensions;
using LOS.DAL.Dapper;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface IEventLogAI
    {
        EventLogAI.DisbursementOutPut LogDisbursement(EventLogAI.DisbursementInput entity);
        EventLogAI.DisbursementOutPut LogClosing(int loanbriefId);
    }
    public class EventLogAIService : IEventLogAI
    {
        protected IConfiguration _baseConfig;
        private IUnitOfWork _unitOfWork;
        public EventLogAIService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _baseConfig = configuration;
            _unitOfWork = unitOfWork;
        }

        public EventLogAI.DisbursementOutPut LogDisbursement(EventLogAI.DisbursementInput entity)
        {
            var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.AI_LOG_DISBURSEMENT;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(entity);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                //lưu log
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.LogAIDisbursement,
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = (int)entity.LoanBriefId,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
                request.Timeout = 3000;
                var response = client.Execute(request);
                // cập nhập lại log
                var jsonResponse = response.Content;
                objLogCallApi.ModifyAt = DateTime.Now;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _unitOfWork.LogCallApiRepository.Update(objLogCallApi);
                _unitOfWork.Save();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<EventLogAI.DisbursementOutPut>(response.Content);
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public EventLogAI.DisbursementOutPut LogClosing(int loanbriefId)
        {

            var url = _baseConfig["AppSettings:AIAuthentication"] + string.Format(Constants.AI_LOG_CLOSING, loanbriefId);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            try
            {
                //lưu log
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.LogAIClosing,
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = (int)loanbriefId,
                    Input = "",
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
                request.Timeout = 3000;
                var response = client.Execute(request);
                // cập nhập lại log
                var jsonResponse = response.Content;
                objLogCallApi.ModifyAt = DateTime.Now;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _unitOfWork.LogCallApiRepository.Update(objLogCallApi);
                _unitOfWork.Save();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<EventLogAI.DisbursementOutPut>(response.Content);
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
