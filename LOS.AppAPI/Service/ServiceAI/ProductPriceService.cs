﻿using LOS.AppAPI.DTOs.Appraiser;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.Common.Extensions;
using LOS.DAL.Dapper;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static LOS.AppAPI.Models.AI.AISuggestionsInformation;

namespace LOS.AppAPI.Service
{
    public interface IProductPrice
    {
        decimal GetPriceCarAI(ProductDetail product, int LoanBriefId);
        decimal GetPriceAI(int ProductId, int LoanBriefId);
        ProductPriceRes GetPriceCarAIDemo(ProductDetail product);
        ProductPriceRes GetMotoPriceDemo(ProductPriceReq entity);
        List<ProductReviewDTO> GetInfoAppraiser(int loanbriefId, int productTypeId);
        ProductDetail GetProduct(int productId);
    }
    public class ProductPriceService : IProductPrice
    {
        protected IConfiguration _baseConfig;
        private IAuthenticationAI _authenService;
        private IToken _tokenService;
        private IUnitOfWork _unitOfWork;
        public ProductPriceService(IConfiguration configuration, IAuthenticationAI authenService, IToken tokenService, IUnitOfWork unitOfWork)
        {
            _baseConfig = configuration;
            _authenService = authenService;
            _tokenService = tokenService;
            _unitOfWork = unitOfWork;
        }
        public ProductPriceRes GetMotoPriceDemo(ProductPriceReq entity)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
            var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.GET_MOTO_PRICE;
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", token);
            request.AddParameter("application/json", JsonConvert.SerializeObject(entity), ParameterType.RequestBody);
            try
            {
                var response = client.Execute(request);
                // cập nhập lại log
                var jsonResponse = response.Content;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ProductPriceRes>(response.Content);
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public decimal GetPriceCarAI(ProductDetail product, int LoanBriefId)
        {
            decimal priceCarAI = 0;
            try
            {
                if (product != null && LoanBriefId > 0)
                {
                    var productBrand = _unitOfWork.BrandProductRepository.GetById(product.IdBrand);
                    if (productBrand != null)
                    {
                        var phanh = "";
                        var vanh = "";
                        var brand = productBrand.Name;
                        var models = product.ShortName;
                        if (product.BrakeType != null && product.BrakeType > 0)
                            phanh = LOS.Common.Helpers.ExtentionHelper.GetName((BrakeType)product.BrakeType);
                        if (product.RimType != null && product.RimType > 0)
                            vanh = LOS.Common.Helpers.ExtentionHelper.GetName((RimType)product.RimType);
                        var regdate = product.Year ?? 0;
                        var productPriceReq = new ProductPriceReq
                        {
                            brand = brand,
                            model = models,
                            phanh = phanh,
                            vanh = vanh,
                            regdate = regdate.ToString(),
                            LoanBriefId = LoanBriefId
                        };
                        var result = GetMotoPriceDemo(productPriceReq);
                        if (result != null && result.predicted_price > 0)
                        {
                            priceCarAI = result.predicted_price * 1000000;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return priceCarAI;
        }
        //Định giá xe demo web
        public ProductPriceRes GetPriceCarAIDemo(ProductDetail product)
        {
            var result = new ProductPriceRes();
            try
            {
                if (product != null)
                {
                    var productBrand = _unitOfWork.BrandProductRepository.GetById(product.IdBrand);
                    if (productBrand != null)
                    {
                        var phanh = "";
                        var vanh = "";
                        var brand = productBrand.Name;
                        var models = product.ShortName;
                        var regdate = product.Year ?? 0;
                        var productPriceReq = new ProductPriceReq
                        {
                            brand = brand,
                            model = models,
                            phanh = phanh,
                            vanh = vanh,
                            regdate = regdate.ToString()
                        };
                        result = GetMotoPriceDemo(productPriceReq);

                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public ProductPriceRes GetMotoPrice(ProductPriceReq entity)
        {
            var body = JsonConvert.SerializeObject(entity);
            var token = _authenService.GetToken(Constants.APP_ID_TIMA_IT3, Constants.APP_KEY_TIMA_IT3);
            var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.GET_MOTO_PRICE;
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", token);
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                //lưu log
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.GetMotoPrice,
                    LinkCallApi = url,
                    TokenCallApi = token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = entity.LoanBriefId,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
                var response = client.Execute(request);
                // cập nhập lại log
                var jsonResponse = response.Content;
                objLogCallApi.ModifyAt = DateTime.Now;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _unitOfWork.LogCallApiRepository.Update(objLogCallApi);
                _unitOfWork.Save();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ProductPriceRes>(response.Content);
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public List<ProductReviewDTO> GetInfoAppraiser(int loanbriefId, int productTypeId)
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select pr.Id, pr.name, pr.State, prr.IsCheck, pd.MoneyDiscount,pd.PecentDiscount ");
                sql.AppendLine("from ProductReview(nolock) pr  ");
                sql.AppendLine("inner join ProductReviewDetail(nolock) pd on pr.Id = PD.ProductReviewId  ");
                sql.AppendLine("inner join ProductReviewResult prr on prr.ProductReviewId = pr.Id ");
                sql.AppendFormat("where  pr.Status = 1  and pd.ProductTypeId = {0} ", productTypeId);
                sql.AppendFormat("and prr.LoanBriefId ={0} and prr.IsCheck != pr.State ", loanbriefId);
                return db.Query<ProductReviewDTO>(sql.ToString());

            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public decimal GetPriceAI(int ProductId, int LoanBriefId)
        {
            try
            {
                var productAiModel = PrepareProductAiModel(ProductId, LoanBriefId);
                if (productAiModel != null)
                {
                    var data = GetMotoPrice(productAiModel);
                    if (data != null && data.predicted_price > 0)
                        return data.predicted_price * 1000000;
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public ProductPriceReq PrepareProductAiModel(int ProductId, int LoanBriefId)
        {
            try
            {
                var phanh = "";
                var vanh = "";
                var product = _unitOfWork.ProductRepository.Query(x => x.Id == ProductId, null, false).Select(ProductDetail.ProjectionDetail).FirstOrDefault();
                var productBrand = _unitOfWork.BrandProductRepository.Query(x => x.Id == product.IdBrand, null, false).Select(LOS.DAL.DTOs.BrandProductDetail.ProjectionDetail).FirstOrDefault();
                var brand = productBrand.Name;
                var models = product.ShortName;
                if (product.BrakeType != null && product.BrakeType > 0)
                    phanh = LOS.Common.Helpers.ExtentionHelper.GetName((BrakeType)product.BrakeType);
                if (product.RimType != null && product.RimType > 0)
                    vanh = LOS.Common.Helpers.ExtentionHelper.GetName((RimType)product.RimType);

                var regdate = product.Year ?? 0;
                return new ProductPriceReq
                {
                    brand = brand,
                    model = models,
                    phanh = phanh,
                    vanh = vanh,
                    regdate = regdate.ToString(),
                    LoanBriefId = LoanBriefId
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ProductDetail GetProduct(int productId)
        {
            return _unitOfWork.ProductRepository.Query(x => x.Id == productId, null, false).Select(ProductDetail.ProjectionDetail).FirstOrDefault();
        }

    }
}
