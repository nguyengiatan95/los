﻿using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Models.AI;
using LOS.AppAPI.Models.ServiceAI;
using LOS.AppAPI.Models.AiServiceDemo;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface IServiceAI
    {
        MotorbikeRegistrationInfo EkycMotorbikeRegistrationCertificate(byte[] file, string fileName);
        EkycNationalCard.Card EkycNationalCard(byte[] file, string fileName);
        CheckLivenessV2 CheckLivenessV2(string selfieImage, List<string> actionImages,  ref LogLoanInfoAi logData);
   EkycNationalRes GetInfoNationalEkyc(EkycNationalReq input, int loanBriefId);
        CheckLivenessV2 CheckLivenessDemo(string selfieImage, List<string> actionImages);
        CreditScoringDemoAiResponse CreditScoringDemo(CreditScoringDemoAiRequest entity);
        FraudDetectionDemoAiResponse FraudDetectionDemo(FraudDetectionDemoRequest entity);
    }

    public class ServiceAI : IServiceAI
    {
        protected IConfiguration _baseConfig;
        protected IAuthenticationAI _authenService;
        private readonly UnitOfWork _unitOfWork;
        public ServiceAI(IAuthenticationAI authenService)
        {
            //_baseConfig = configuration;
            _authenService = authenService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(_baseConfig.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }
        public MotorbikeRegistrationInfo EkycMotorbikeRegistrationCertificate(byte[] file, string fileName)
        {
            try
            {
                var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.EKYC_MOTORBIKE_REGISTRATION_CERTIFICATE;
                var client = new RestClient(url);
                client.Timeout = 15000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                request.AddFileBytes("image", file, fileName);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                EkycMotorbikeRegistrationCertificateRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<EkycMotorbikeRegistrationCertificateRes>(jsonResponse);
                if (result.Code == 200 && result.Cards != null && result.Cards.Count > 0)
                    return result.Cards[0].Info;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public EkycNationalCard.Card EkycNationalCard(byte[] file, string fileName)
        {
            try
            {
                var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.EKYC_NATIONAL_CARD;
                var client = new RestClient(url);
                client.Timeout = 15000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                request.AddFileBytes("image", file, fileName);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                EkycNationalCard.Output result = Newtonsoft.Json.JsonConvert.DeserializeObject<EkycNationalCard.Output>(jsonResponse);
                if (result.StatusCode == 200 && result.Cards != null && result.Cards.Count > 0)
                    return result.Cards[0];
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public CheckLivenessV2 CheckLivenessV2(string selfieImage, List<string> actionImages, ref LogLoanInfoAi logData)
        {
            try
            {
                var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.EKYC_CHECK_LIVENESS_V2;
                var client = new RestClient(url);
                client.Timeout = 600000;
                var request = new RestRequest(Method.POST);
                var data = new
                {
                    selfie = selfieImage,
                    action_images = actionImages
                };
                var jsonData = JsonConvert.SerializeObject(data);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                request.AddParameter("application/json", jsonData, ParameterType.RequestBody);
                logData = new LogLoanInfoAi()
                {
                    ServiceType = (int)ServiceTypeAI.CheckLiveness,
                    CreatedAt = DateTime.Now,
                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.Excuted,
                    FromApp = "APP_API",
                    Token = token,
                    Url = url,
                    Status = 0,
                    //Request = jsonData
                };
                IRestResponse response = client.Execute(request);
                Log.Information(response.StatusCode + "-" + response.Content);
                var jsonResponse = response.Content;
                logData.Status = 1;
                logData.UpdatedAt = DateTime.Now;
                logData.ResponseRequest = jsonResponse;
                CheckLivenessV2 result = Newtonsoft.Json.JsonConvert.DeserializeObject<CheckLivenessV2>(jsonResponse);
                if (result.StatusCode == 200)
                    return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
public EkycNationalRes GetInfoNationalEkyc(EkycNationalReq input, int loanBriefId)
        {
            try
            {
                var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.GET_INFO_CHECK_EKYC;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var body = JsonConvert.SerializeObject(input);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    ServiceType = ServiceTypeAI.Ekyc.GetHashCode(),
                    LoanbriefId = loanBriefId,
                    Request = body,
                    Token = token,
                    Url = url,
                    CreatedAt = DateTime.Now,
                    IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode()
                };
                _unitOfWork.LogLoanInfoAiRepository.Insert(logLoanInfoAi);
                _unitOfWork.Save();
                if (logLoanInfoAi.Id > 0)
                {
                    IRestResponse response = client.Execute(request);
                    var jsonResponse = response.Content;
                    _unitOfWork.LogLoanInfoAiRepository.Update(x => x.Id == logLoanInfoAi.Id, x => new LogLoanInfoAi()
                    {
                        Status = 1,
                        ResponseRequest = jsonResponse,
                        IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode(),
                        UpdatedAt = DateTime.Now
                    });
                    EkycNationalRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<EkycNationalRes>(jsonResponse);
                    if (result.StatusCode == 200)
                    {
                        //Lưu vào bảng ResultEkyc
                        _unitOfWork.ResultEkycRepository.Insert(new DAL.EntityFramework.ResultEkyc()
                        {
                            LoanbriefId = loanBriefId,
                            CreatedAt = DateTime.Now,
                            AddressValue = result.NationalId.Address.Value,
                            AddressCheck = result.NationalId.Address.Check,
                            IdValue = result.NationalId.Id.Value,
                            IdCheck = result.NationalId.Id.Check,
                            FullNameValue = result.NationalId.Fullname.Value,
                            FullNameCheck = result.NationalId.Fullname.Check,
                            BirthdayValue = result.NationalId.Birthday.Value,
                            BirthdayCheck = result.NationalId.Birthday.Check,
                            ExpiryValue = result.NationalId.Expiry.Value,
                            ExpiryCheck = result.NationalId.Expiry.Check,
                            GenderValue = result.NationalId.Gender.Value,
                            GenderCheck = result.NationalId.Gender.Check,
                            EthnicityValue = result.NationalId.Ethnicity.Value,
                            EthnicityCheck = result.NationalId.Ethnicity.Check,
                            IssueByValue = result.NationalId.IssueBy.Value,
                            IssueByCheck = result.NationalId.IssueBy.Check,
                            IssueDateValue = result.NationalId.IssueDate.Value,
                            IssueDateCheck = result.NationalId.IssueDate.Check,
                            ReligionValue = result.NationalId.Religion.Value,
                            ReligionCheck = result.NationalId.Religion.Check,
                            FaceCompareCode = result.FraudCheck.FaceCompare.Code,
                            FaceCompareMessage = result.FraudCheck.FaceCompare.Message,
                            FaceQueryCode = result.FraudCheck.FaceQuery.Code,
                            FaceQueryMessage = result.FraudCheck.FaceQuery.Message
                        });
                        _unitOfWork.Save();
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public CheckLivenessV2 CheckLivenessDemo(string selfieImage, List<string> actionImages)
        {
            try
            {
                var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.EKYC_CHECK_LIVENESS_V2;
                var client = new RestClient(url);
                client.Timeout = 600000;
                var request = new RestRequest(Method.POST);
                var data = new
                {
                    selfie = selfieImage,
                    action_images = actionImages
                };
                var jsonData = JsonConvert.SerializeObject(data);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                request.AddParameter("application/json", jsonData, ParameterType.RequestBody);
               var logData = new LogLoanInfoAi()
                {
                    ServiceType = (int)ServiceTypeAI.CheckLiveness,
                    CreatedAt = DateTime.Now,
                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.Excuted,
                    FromApp = "APP_DEMO",
                    Token = token,
                    Url = url,
                    Status = 0,
                    //Request = jsonData
                };
                IRestResponse response = client.Execute(request);
                Log.Information(response.StatusCode + "-" + response.Content);
                var jsonResponse = response.Content;
                logData.Status = 1;
                logData.UpdatedAt = DateTime.Now;
                logData.ResponseRequest = jsonResponse;
                _unitOfWork.LogLoanInfoAiRepository.Insert(logData);
                _unitOfWork.Save();
                CheckLivenessV2 result = Newtonsoft.Json.JsonConvert.DeserializeObject<CheckLivenessV2>(jsonResponse);
                if (result.StatusCode == 200)
                    return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public CreditScoringDemoAiResponse CreditScoringDemo(CreditScoringDemoAiRequest entity)
        {
            try
            {
                var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.CREDIT_SCORING_DEMO;
                var client = new RestClient(url);
                client.Timeout = 600000;
                var request = new RestRequest(Method.POST);
                var jsonData = JsonConvert.SerializeObject(entity);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                request.AddParameter("application/json", jsonData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Log.Information(response.StatusCode + "-" + response.Content);
                var jsonResponse = response.Content;
                return Newtonsoft.Json.JsonConvert.DeserializeObject<CreditScoringDemoAiResponse>(jsonResponse);
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public FraudDetectionDemoAiResponse FraudDetectionDemo(FraudDetectionDemoRequest entity)
        {
            try
            {
                var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.FRAUD_DETECTION_DEMO;
                var client = new RestClient(url);
                client.Timeout = 600000;
                var request = new RestRequest(Method.POST);
                var jsonData = JsonConvert.SerializeObject(entity);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);

                var objData = new FraudDetectionDemoAiRequest()
                {
                    UserName = entity.UserName,
                    NationalId = entity.NationalId,
                    Dob = entity.Dob,
                    //SelfieImage = "http://file.tima.vn/uploads/LOS/2022/1/21.165752708224314-106.06491803864567-0acb978c-58ba-4f6c-960e-d8eb6a22ac60.jpg",
                    //FrontIdImage = "http://file.tima.vn/uploads/LOS/2022/1/21.16572724154584-106.06482963788001-85e229da-b096-42b4-9110-b4b908ba7374.jpg"
                };
                using (var ms = new MemoryStream())
                {
                    entity.SelfieImage.CopyTo(ms);
                    objData.SelfieImage = Convert.ToBase64String(ms.ToArray());
                }
                using (var ms = new MemoryStream())
                {
                    entity.FrontIdImage.CopyTo(ms);
                    objData.FrontIdImage = Convert.ToBase64String(ms.ToArray());
                }
                request.AddParameter("application/json", JsonConvert.SerializeObject(objData), ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                return Newtonsoft.Json.JsonConvert.DeserializeObject<FraudDetectionDemoAiResponse>(jsonResponse);
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
