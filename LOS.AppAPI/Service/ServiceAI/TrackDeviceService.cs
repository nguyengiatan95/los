﻿using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.AppAPI.Models.AI;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface ITrackDevice
    {
        bool CloseContract(string maHD, string deviceId, int loanbriefId);
        OutputOpenContract OpenContract(string maHD, string deviceId, string shopid, int productId, int loanbriefId, string contractType);
        bool ReActiveDevice(string maHD, string deviceId, int loanbriefId);
    }

    public class TrackDeviceService: ITrackDevice
    {
        private IUnitOfWork _unitOfWork;
        protected IAuthenticationAI _authenService;
        protected IConfiguration _baseConfig;
        public TrackDeviceService(IUnitOfWork unitOfWork, IConfiguration configuration, IAuthenticationAI authenService)
        {
            _baseConfig = configuration;
            _authenService = authenService;
            _unitOfWork = unitOfWork;
        }

        private bool api_close_contract(string maHD, string deviceId, string token, int loanbriefId)
        {
            try
            {
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.close_contract;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new 
                {
                    contractid = maHD,
                    imei = deviceId
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.CloseContract,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = token,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == objLogCallApi.Id, x => new LogCallApi()
                {
                    Status = (int)StatusCallApi.Success,
                    Output = jsonResponse
                });
                _unitOfWork.Save();
                CloseContractRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<CloseContractRes>(jsonResponse);
                return (result != null && result.StatusCode == 200);
            }
            catch (Exception ex)
            {

            }

            return false;
        }
        private bool api_reactive_imei(string maHD, string token, string deviceId, int loanbriefId)
        {
            try
            {
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.re_active_imei;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new
                {
                    contractId = maHD,
                    imei = deviceId
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);

                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.ReActiveDevice,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    TokenCallApi = token,
                    Input = jsonInput,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
               
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == objLogCallApi.Id, x => new LogCallApi()
                {
                    Status = (int)StatusCallApi.Success,
                    Output = jsonResponse,
                    ModifyAt = DateTime.Now
                });
                _unitOfWork.Save();
                ReActiveDeviceRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<ReActiveDeviceRes>(jsonResponse);
                return (result != null && result.StatusCode == 200);
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        public bool CloseContract(string maHD, string deviceId, int loanbriefId)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
            if (!string.IsNullOrEmpty(token))
                return api_close_contract(maHD, deviceId, token, loanbriefId);
            return false;
        }

        private OutputOpenContract api_open_contract(string maHD, string deviceId, string shopid, int productId, string token, int loanbriefId, string contractType)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.open_contract;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new InputOpenContract()
                {
                    contractid = maHD,
                    imei = deviceId,
                    shopid = shopid,
                    vehicle_type = (productId == EnumProductCredit.CamotoCreditType.GetHashCode() || productId == EnumProductCredit.OtoCreditType_CC.GetHashCode()
                    || productId == EnumProductCredit.OtoCreditType_KCC.GetHashCode() || productId == EnumProductCredit.LoanFastCar_CC.GetHashCode()) ? "oto" : "xe may",
                    contract_type = contractType
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);

                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.OpenContract,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == objLogCallApi.Id, x => new LogCallApi()
                {
                    Status = (int)StatusCallApi.Success,
                    Output = jsonResponse
                });
                _unitOfWork.Save();
                OutputOpenContract result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputOpenContract>(jsonResponse);
                return result;// (result != null && result.StatusCode == 200);
            }
            catch (Exception ex)
            {

            }

            return null;
        }
        public OutputOpenContract OpenContract(string maHD, string deviceId, string shopid, int productId, int loanbriefId, string contractType)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
            if (!string.IsNullOrEmpty(token))
                return api_open_contract(maHD, deviceId, shopid, productId, token, loanbriefId, contractType);
            return null;
        }
        public bool ReActiveDevice(string maHD, string deviceId, int loanbriefId)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
            if (!string.IsNullOrEmpty(token))
                return api_reactive_imei(maHD,token, deviceId, loanbriefId);
            return false;
        }
    }
}
