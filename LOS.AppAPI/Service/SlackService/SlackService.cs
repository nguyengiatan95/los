﻿using LOS.AppAPI.Models.Slack;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service.SlackService
{
    public interface ISlackService
    {
        int SendNotifyOfStack(string author_name, string colorText, string title, string text, string urlApi, int loanbriefId = 0, string department = "");
    }
    public class SlackService : ISlackService
    {
        protected IConfiguration _baseConfig;
        private IUnitOfWork _unitOfWork;
        public SlackService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _baseConfig = configuration;
            _unitOfWork = unitOfWork;
        }
        private int PostMessage(SlackSMS item, string urlApi, int LoanbriefId = 0, string department = "")
        {
            try
            {
                var slackApi = _baseConfig["AppSettings:SlackAPIDomain"].ToString();
                var client = new RestClient(slackApi);
                var request = new RestRequest(urlApi);
                request.Method = Method.POST;
                request.AddHeader("Content-Type", "application/json");
                var body = JsonConvert.SerializeObject(item);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.CallSlack,
                    LinkCallApi = slackApi + urlApi,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = department,
                    LoanBriefId = LoanbriefId,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
                var response = client.Execute(request);
                var content = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = content;
                _unitOfWork.LogCallApiRepository.Update(objLogCallApi);
                _unitOfWork.Save();

                return 1;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        public int SendNotifyOfStack(string author_name, string colorText, string title, string text, string urlApi, int loanbriefId = 0, string department = "")
        {
            try
            {
                var objSms = new SlackSMS();
                objSms.mrkdwn = true;
                objSms.text = String.Empty;
                objSms.attachments = new List<SlackAttachments>();

                SlackAttachments itemAttack = new SlackAttachments()
                {
                    author_name = author_name,
                    color = colorText,
                    pretext = "",
                    title = title,
                    text = text
                };
                objSms.attachments.Add(itemAttack);
                var post = PostMessage(objSms, urlApi, loanbriefId, department);
                if (post == 1)
                    return 1;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
    }
}
