﻿using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models;
using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.AppAPI.Service
{
    public interface IVoiceOtpService
    {
        ResponseCallApiVoiceOtp SendOtpVoiceOtp(int typeOpt, string phone, string otp, DateTime expired, string ipAddress = "");
    }
    public class VoiceOtpService : IVoiceOtpService
    {
        private IConfiguration _configuration;
        private IUnitOfWork _unitOfWork;
        public VoiceOtpService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
        }

        public ResponseCallApiVoiceOtp SendOtpVoiceOtp(int typeOpt, string phone, string otp, DateTime expired, string ipAddress = "")
        {
            try
            {
                var param = new RequestCallApiVoiceOtp()
                {

                    phone = phone,
                    code = otp
                };
                string url = _configuration["AppSettings:LinkApiVoiceOTP"] + Constants.VOICE_OTP;
                var body = JsonConvert.SerializeObject(param);
                var client = new RestClient(url);
                client.Timeout = 5000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("X-Access-Token", _configuration["AppSettings:TokenVoiceOTP"]);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var log = new LogSendOtp
                {
                    Phone = phone,
                    Otp = otp,
                    Request = body,
                    CreatedAt = DateTime.Now,
                    IpAddress = ipAddress,
                    Url = url,
                    TypeSendOtp = (int)TypeSendOtp.VoiceOtp,
                    ExpiredVerifyOtp = expired,
                    TypeOtp = typeOpt
                };
                _unitOfWork.LogSendOtpRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                _unitOfWork.LogSendOtpRepository.Update(x => x.Id == log.Id, x => new LogSendOtp()
                {
                    Response = json,
                    ModifyAt = DateTime.Now
                });
                ResponseCallApiVoiceOtp data = JsonConvert.DeserializeObject<ResponseCallApiVoiceOtp>(json);
                if (data != null)
                    _unitOfWork.LogSendOtpRepository.Update(x => x.Id == log.Id, x => new LogSendOtp()
                    {
                        StatusSend = (int)StatusCallApi.Success
                    });
                return data;
            }
            catch (Exception ex)
            {
                return null;
            }


        }
    }
}
