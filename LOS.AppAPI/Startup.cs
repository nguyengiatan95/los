﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOS.AppAPI.Helpers;
using LOS.AppAPI.Models.Mongo;
using LOS.AppAPI.Service;
using LOS.AppAPI.Service.AG;
using LOS.AppAPI.Service.AuthorizeToken;
using LOS.AppAPI.Service.BankLog;
using LOS.AppAPI.Service.CareSoft;
using LOS.AppAPI.Service.ExportPDF;
using LOS.AppAPI.Service.ESign;
using LOS.AppAPI.Service.LmsService;
using LOS.AppAPI.Service.MongoService;
using LOS.AppAPI.Service.Notification;
using LOS.AppAPI.Service.SlackService;
using LOS.AppAPI.Service.SSOService;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using LOS.DAL.Respository;
using LOS.Services.Services.Loanbrief;
using LOS.Services.Services;
using LOS.Services.Services.LMS;
using LOS.Services.Services.LogCallApi;

namespace LOS.AppAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IdentityModelEventSource.ShowPII = true;

            services.AddMvc().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.IgnoreNullValues = false;
            });

            services.AddMvc(option => option.EnableEndpointRouting = false);


            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
            });


            // Configure DbContext with Scoped lifetime
            services.AddDbContext<LOSContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("LOSDatabase"));
            }
            );

            services.AddScoped<Func<LOSContext>>((provider) => () => provider.GetService<LOSContext>());

            //services.AddDbContext<LOSContext>(options =>
            //    options.UseSqlServer(Configuration.GetConnectionString("LOSDatabase"), providerOptions => providerOptions.EnableRetryOnFailure()), ServiceLifetime.Transient);

            //Xử lý TH load entity lần đầu lâu
            //var options = services.BuildServiceProvider()
            //          .GetRequiredService<DbContextOptions<LOSContext>>();
            //Task.Run(() =>
            //{
            //    using (var dbContext = new LOSContext(options))
            //    {
            //        var model = dbContext.Model; //force the model creation
            //    }
            //});
            //Mongo
            services.Configure<DatabaseSettings>(Configuration.GetSection(nameof(DatabaseSettings)));
            services.AddSingleton<IDatabaseSettings>(x => x.GetRequiredService<IOptions<DatabaseSettings>>().Value);

            services.AddSingleton<LogAppApiService>();

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IPermissionRequest, PermissionRequestService>();
            services.AddTransient<IAuthorizeTokenService, AuthorizeTokenService>();
            services.AddTransient<ICareSoftService, CareSoftService>();
            services.AddTransient<ILmsService, LmsService>();
            services.AddTransient<IAgService, AgService>();
            services.AddTransient<IErp, ErpService>();
            services.AddTransient<IToken, TokenService>();
            services.AddTransient<IAuthenticationAI, AuthenticationService>();
            services.AddTransient<IProductPrice, ProductPriceService>();
            services.AddTransient<IBankLog, BankLogService>();
            services.AddTransient<IPushNotification, PushNotificationService>();
            services.AddTransient<ITrackDevice, TrackDeviceService>();
            services.AddTransient<ISlackService, SlackService>();
            services.AddTransient<ISSOService, SSOService>();
            services.AddTransient<IFinaceService, FinaceService>();
            services.AddTransient<IEventLogAI, EventLogAIService>();
            services.AddTransient<IDocumentService, DocumentService>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<ILogRequestResponse, LogAppApiService>();
            services.AddTransient<IExportPdfService, ExportPdfService>();
            services.AddTransient<INetworkService, NetworkService>();
            services.AddTransient<IServiceAI, ServiceAI>();
            services.AddTransient<IIdentityService, IdentityService>();
            services.AddTransient<IViewRenderService, ViewRenderService>();
            services.AddTransient<IESignService, ESignService>();
            services.AddTransient<IViewRenderService, ViewRenderService>();
            services.AddTransient<IVoiceOtpService, VoiceOtpService>();
            services.AddTransient<ICiscoService, CiscoService>();
            services.AddTransient<IProxyTimaService, ProxyTimaService>();
            services.AddTransient<IDrawImageService, DrawImageService>();
            services.AddTransient<IPipelineService, PipelineService>();
            services.AddTransient<IMomoService, MomoService>();
            services.AddTransient<ICheckBankService, CheckBankService>();

            #region Loanbrief
            services.AddTransient<ILoanbriefRepository, LoanbriefRepository>();
            services.AddTransient<ILoanbriefV2Service, LoanbriefV2Service>();
            #endregion

            #region LoanbriefRelationship
            services.AddTransient<ILoanbriefRelationshipRepository, LoanbriefRelationshipRepository>();
            services.AddTransient<IRelationshipService, RelationshipService>();
            #endregion

            #region LMS
            services.AddTransient<Services.Services.LMS.ILMSV2Service, Services.Services.LMS.LMSV2Service>();
            #endregion

            #region LogCallApi
            services.AddTransient<ILogCallApiRespository, LogCallApiRespository>();
            services.AddTransient<Services.Services.LogCallApi.ILogCallApiV2Service, Services.Services.LogCallApi.LogCallApiV2Service>();
            #endregion

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                    });
            });

            // changed
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateActor = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["JWT:Issuer"],
                    ValidAudience = Configuration["JWT:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:SigningKey"])),
                    ClockSkew = TimeSpan.Zero
                };
            });

            services.AddSwaggerGen(c =>
            {
                c.DocumentFilter<CustomSwaggerFilter>();
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "APP API", Version = "v1" });
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First()); //This line
            });
            // swagger config
            //    services.AddSwaggerGen(c =>
            //    {
            //        c.SwaggerDoc("v1", new OpenApiInfo { Title = "APP API", Version = "v1" });

            //        //First we define the security scheme
            //        c.AddSecurityDefinition("Bearer", //Name the security scheme
            //            new OpenApiSecurityScheme
            //            {
            //                Description = "JWT Authorization header using the Bearer scheme.",
            //                Type = SecuritySchemeType.Http, //We set the scheme type to http since we're using bearer authentication
            //                Scheme = "bearer" //The name of the HTTP Authorization scheme to be used in the Authorization header. In this case "bearer".
            //            });

            //        c.CustomSchemaIds(type => type.ToString());

            //        c.AddSecurityRequirement(new OpenApiSecurityRequirement{
            //            {
            //                new OpenApiSecurityScheme{
            //                    Reference = new OpenApiReference{
            //                        Id = "Bearer", //The name of the previously defined security scheme.
            //Type = ReferenceType.SecurityScheme
            //                    }
            //                },new List<string>()
            //            }
            //        });
            //    });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("./v1/swagger.json", "APP API V1");
                });
                
            }
            app.UseAuthentication();
            //app.UseJwtBearerAuthentication(new JwtBearerOptions()
            //{
            //    AutomaticAuthenticate = true,
            //    AutomaticChallenge = true,
            //    TokenValidationParameters = new TokenValidationParameters()
            //    {
            //        ValidIssuer = Configuration["Tokens:Issuer"],
            //        ValidAudience = Configuration["Tokens:Audience"],
            //        ValidateIssuerSigningKey = true,
            //        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:SigningKey"])),
            //        ValidateLifetime = true,
            //        ValidateIssuer = true,
            //        ValidateAudience = true,
            //        ClockSkew = TimeSpan.Zero
            //    },
            //});

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
               Path.Combine(Directory.GetCurrentDirectory(), "uploads")),
                RequestPath = "/uploads"
            });

            app.UseMiddleware<LoggingRequestAndResponseMiddleware>();
            app.UseMiddleware<CustomResponseMiddleware>();
            app.UseMvc();
            app.UseCors("AllowAll");
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.All
            });
            //app.UseSwagger();
            //// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            //// specifying the Swagger JSON endpoint.
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "APP API V1");
            //});
        }
    }
}
