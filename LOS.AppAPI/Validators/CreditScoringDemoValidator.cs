﻿using FluentValidation;
using LOS.AppAPI.Models.AiServiceDemo;
using LOS.Common.Extensions;
using System;

namespace LOS.AppAPI.Validators
{
    public class CreditScoringDemoValidator: AbstractValidator<CreditScoringDemoRequest>
    {
        public CreditScoringDemoValidator()
        {
            RuleFor(x => x.FullName).NotEmpty().WithMessage("Vui lòng truyền thông tin họ tên KH");
            RuleFor(x => x.CardNumber).NotEmpty().WithMessage("Vui lòng truyền thông tin số cmnd/cccd")
            .Must(x => ConvertExtensions.IsNumber(x)).WithMessage("Không đúng định dạng cmnd/cccd");
            //.Must(x => x.Length == (int)NationalCardLength.CMND || x.Length == (int)NationalCardLength.CCCD).WithMessage("Gói vay không tồn tại trong hệ thống");
            RuleFor(x => x.Phone).NotEmpty().WithMessage("Vui lòng truyền thông tin số điện thoại")
                .Must(x => x.ValidPhone()).WithMessage("Số điện thoại không đúng định dạng");
            //RuleFor(x => x.Gender).NotEmpty().WithMessage("Vui lòng truyền thông tin giới tính");
            //RuleFor(x => x.IsMarried).NotEmpty().WithMessage("Vui lòng truyền thông tin tình trạng hôn nhân");
            //RuleFor(x => x.IsLivingTogether).NotEmpty().WithMessage("Vui lòng truyền thông tin sống với ai");
            RuleFor(x => x.JobId).NotEmpty().WithMessage("Vui lòng truyền thông tin công việc");
            RuleFor(x => x.Salary).NotEmpty().WithMessage("Vui lòng truyền thông tin mức lương");
            RuleFor(x => x.TypeIncomeId).NotEmpty().WithMessage("Vui lòng truyền hình thức nhận lương");
            RuleFor(x => x.BrandId).NotEmpty().WithMessage("Vui lòng truyền thông tin thương hiệu xe");
            RuleFor(x => x.ProductId).NotEmpty().WithMessage("Vui lòng truyền thông tin xe");
            RuleFor(x => x.ProductCredit).NotEmpty().WithMessage("Vui lòng truyền thông tin gói vay")
              .Must(x => Enum.IsDefined(typeof(EnumProductCredit), x)).WithMessage("Gói vay không tồn tại trong hệ thống");
            RuleFor(x => x.TotalMoney).NotEmpty().WithMessage("Vui lòng truyền số tiền vay");
        }
    }
}
