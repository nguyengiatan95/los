﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace LOS.Common.Extensions
{
    public static class ConvertExtensions
    {
        // transform object into Identity data type (integer).
        public static int AsId(this object item, int defaultId = -1)
        {
            if (item == null)
                return defaultId;

            int result;
            if (!int.TryParse(item.ToString(), out result))
                return defaultId;

            return result;
        }

        // transform object into integer data type.
        public static int AsInt(this object item, int defaultInt = default(int))
        {
            if (item == null)
                return defaultInt;

            int result;
            if (!int.TryParse(item.ToString(), out result))
                return defaultInt;

            return result;
        }

        // transform object into double data type
        public static double AsDouble(this object item, double defaultDouble = default(double))
        {
            if (item == null)
                return defaultDouble;

            double result;
            if (!double.TryParse(item.ToString(), out result))
                return defaultDouble;

            return result;
        }

        public static decimal AsDecimail(this object item, decimal defaultDecimal = default(decimal))
        {
            if (item == null)
                return defaultDecimal;

            decimal result;
            if (!decimal.TryParse(item.ToString(), out result))
                return defaultDecimal;

            return result;
        }

        public static byte AsByte(this object item, byte defaultByte = default(byte))
        {
            if (item == null)
                return defaultByte;

            byte result;
            if (!byte.TryParse(item.ToString(), out result))
                return defaultByte;

            return result;
        }
        // transform object into string data type

        public static string AsString(this object item, string defaultString = default(string))
        {
            if (item == null || item.Equals(System.DBNull.Value))
                return defaultString;

            return item.ToString().Trim();
        }

        // transform object into DateTime data type.
        public static DateTime AsDateTime(this object item, DateTime defaultDateTime = default(DateTime))
        {
            if (item == null || string.IsNullOrEmpty(item.ToString()))
                return defaultDateTime;

            DateTime result;
            if (!DateTime.TryParse(item.ToString(), out result))
                return defaultDateTime;

            return result;
        }

        // transform object into bool data type
        public static bool AsBool(this object item, bool defaultBool = default(bool))
        {
            if (item == null)
                return defaultBool;

            return new List<string>() { "yes", "y", "true" }.Contains(item.ToString().ToLower());
        }

        // transform string into byte array
        public static byte[] AsByteArray(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return null;

            return Convert.FromBase64String(s);
        }

        // transform object into base64 string.
        public static string AsBase64String(this object item)
        {
            if (item == null)
                return null;
            return Convert.ToBase64String((byte[])item);
        }

        // transform object into Guid data type
        public static Guid AsGuid(this object item)
        {
            try { return new Guid(item.ToString()); }
            catch { return Guid.Empty; }
        }

        public static string AsToLower(this string item, string defaultString = default(string))
        {
            if (item == null || item.Equals(System.DBNull.Value))
                return defaultString;

            return item.Trim().ToLower();
        }

        public static bool ValidPhone(this string txtPhone)
        {
            if ((txtPhone.StartsWith("09")) && txtPhone.Length == 10)
                return true;

            if ((txtPhone.StartsWith("03") || txtPhone.StartsWith("08") || txtPhone.StartsWith("07") || txtPhone.StartsWith("05")) && txtPhone.Length == 10)
                return true;
            //if ((txtPhone.StartsWith("3") || txtPhone.StartsWith("8") || txtPhone.StartsWith("7") || txtPhone.StartsWith("5")) && txtPhone.Length == 9)
            //{
            //    return true;
            //}
            //if (txtPhone.StartsWith("9") && txtPhone.Length == 9)
            //{
            //    return true;
            //}
            //if (txtPhone.StartsWith("01") && txtPhone.Length == 11)
            //{
            //    return true;
            //}
            return false;
        }

        public static long RoundingTo(long myNum, long roundTo)
        {
            if (roundTo <= 0) return myNum;
            return (myNum + roundTo / 2) / roundTo * roundTo;
        }
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            var a = unixTimeStamp.ToString().Length;
            string b = unixTimeStamp.ToString().Substring(0, (a - 3));
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(b)).ToLocalTime();
            return dtDateTime;
        }

        public static string ReduceWhitespace(this string value)
        {
            try
            {
                if (value == null) return value;
                value = value.Trim();
                var newString = new StringBuilder();
                bool previousIsWhitespace = false;
                for (int i = 0; i < value.Length; i++)
                {
                    if (Char.IsWhiteSpace(value[i]))
                    {
                        if (previousIsWhitespace)
                        {
                            continue;
                        }

                        previousIsWhitespace = true;
                    }
                    else
                    {
                        previousIsWhitespace = false;
                    }

                    newString.Append(value[i]);
                }

                return newString.ToString();
            }
            catch
            {
                return value;
            }

        }

        public static string TitleCaseString(this string s)
        {
            try
            {
                if (s == null) return s;

                String[] words = s.Split(' ');
                for (int i = 0; i < words.Length; i++)
                {
                    if (words[i].Length == 0) continue;

                    Char firstChar = Char.ToUpper(words[i][0]);
                    String rest = "";
                    if (words[i].Length > 1)
                    {
                        rest = words[i].Substring(1).ToLower();
                    }
                    words[i] = firstChar + rest;
                }
                return String.Join(" ", words);
            }
            catch
            {
                return s;
            }

        }
        public static bool IsValidUri(this string url)
        {
            string pattern = @"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
            Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return reg.IsMatch(url);
        }
        //Check IsNumber
        public static bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }

        public static string ConvertToUnSign(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static bool ValidEmail(this string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        public static string ReadNumber(decimal _number)
        {
            if (_number == 0)
                return "Không đồng.";
            bool _startwith = _number < 0 ? true : false;
            string _letter = "";
            _number = Math.Abs(_number);
            string _source = String.Format("{0:0,0}", _number);
            string[] _arrsource = _source.Split(',');
            int _numunit = _arrsource.Length;
            foreach (string _str in _arrsource)
            {
                if (ThreeNumber2Letter(int.Parse(_str)) != "")
                    _letter += String.Format("{0} {1}, ", ThreeNumber2Letter(int.Parse(_str)), NumUnit(_numunit));
                _numunit--;
            }
            _letter = _letter.Substring(0, _letter.Length - 2);
            if (_letter.StartsWith("không trăm"))
                _letter = _letter.Substring(10, _letter.Length - 10).Trim();
            if (_letter.StartsWith("lẻ"))
                _letter = _letter.Substring(2, _letter.Length - 2).Trim();
            _letter = String.Format("{0} {1}", _startwith ? "âm" : "", _letter).Trim();
            return String.Format("{0}{1} đồng.", _letter.Substring(0, 1).ToUpper(), _letter.Substring(1, _letter.Length - 1).Trim()).Replace("lăm trăm", "năm trăm");
        }
        private static string NumUnit(int _unit)
        {
            if (_unit < 2) return "";
            switch (_unit)
            {
                case 2: return "nghìn";
                case 3: return "triệu";
                case 4: return "tỷ";
                default: return String.Format("{0} {1}", NumUnit(_unit - 3), NumUnit(4));
            }
        }
        private static string ThreeNumber2Letter(int _number)
        {
            int _hunit = 0, _tunit = 0, _nunit = 0;
            if (_number > 0 && _number < 10)// Trường hợp _number = [1-9]
                _nunit = _number;
            else if (_number > 9 && _number < 100) // Trường hợp _number = [10-99]
            {
                _tunit = _number / 10;
                _nunit = _number - (_tunit * 10);
            }
            else if (_number > 99 && _number < 1000)// Trường hợp _number = [100-999]
            {
                _hunit = _number / 100;
                _tunit = (_number - (_hunit * 100)) / 10;
                _nunit = _number - (_hunit * 100) - (_tunit * 10);
            }
            else // Trường hợp _number <> [1-999]
                return "";
            string[] _OneNumber2Letter = { "không", "một", "hai", "ba", "bốn", "lăm", "sáu", "bảy", "tám", "chín" };
            switch (_tunit)
            {
                case 0:
                    if (_nunit == 0)
                        return String.Format("{0} trăm", _OneNumber2Letter[_hunit]);
                    else
                    {
                        if (_nunit == 5)
                            return String.Format("{0} trăm lẻ năm", _OneNumber2Letter[_hunit]);
                        else
                            return String.Format("{0} trăm lẻ {1}", _OneNumber2Letter[_hunit], _OneNumber2Letter[_nunit]);
                    }
                case 1:
                    if (_nunit == 0)
                        return String.Format("{0} trăm mười", _OneNumber2Letter[_hunit]);
                    else
                        return String.Format("{0} trăm mười {1}", _OneNumber2Letter[_hunit], _OneNumber2Letter[_nunit]);
                case 5:
                    return String.Format("{0} trăm năm mươi {1}", _OneNumber2Letter[_hunit], _OneNumber2Letter[_nunit]);
                default:
                    if (_nunit == 0)
                        return String.Format("{0} trăm {1} mươi", _OneNumber2Letter[_hunit], _OneNumber2Letter[_tunit]);
                    else if (_nunit == 1)
                        return String.Format("{0} trăm {1} mươi mốt", _OneNumber2Letter[_hunit], _OneNumber2Letter[_tunit]);
                    else if (_nunit == 4)
                        return String.Format("{0} trăm {1} mươi tư", _OneNumber2Letter[_hunit], _OneNumber2Letter[_tunit]);
                    else
                        return String.Format("{0} trăm {1} mươi {2}", _OneNumber2Letter[_hunit], _OneNumber2Letter[_tunit], _OneNumber2Letter[_nunit]);
            }
        }
    }
}