﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LOS.Common.Extensions
{
    public class Description
    {
        public static string GetDescription(Enum value)
        {
            try
            {
                var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return da.Length > 0 ? da[0].Description : value.ToString();
            }
            catch
            {
                return null;
            }
        }
    }
    public enum StatusAff
    {
        [Description("Giới thiệu")]
        Introduced = 1,
        [Description("Bán thành công")]
        SuccessfulSale = 2
    }

    public enum EnumPipelineState
    {
        [Description("Đợi pipeline")]
        WAITING_FOR_PIPELINE = 0,
        [Description("Đang xử lý pipeline")]
        IN_PROCESS_FOR_PIPELINE = 1,
        [Description("Pipeline hoàn thành")]
        PIPELINE_COMPLETED = 9,
        [Description("Chuyển đến Section tiếp theo")]
        WAITING_FOR_NEW_SECTION = 10,
        [Description("Đang xử lý")]
        IN_PROCESS_FOR_SECTION = 11,
        [Description("Đang đợi xử lý bằng tay")]
        WAITING_FOR_MANUAL_PROCESSING = 20,
        [Description("Xử lý bằng tay thành công")]
        MANUAL_PROCCESSED = 21,
        [Description("Pipeline không xác định")]
        UNKNOWN_PIPELINE = 98,
        [Description("Gặp lỗi ")]
        UNEXPECTED_ERROR = 99,
        [Description("Đã xử lý")]
        PROCESSED = 100,
        [Description("Đợi xử lý tự động")]
        WAITING_FOR_AUTOMATIC_PROCESSING = 30,
        [Description("Xử lý tự động thành công")]
        AUTOMATIC_PROCESSED = 31

    }

    public enum LoanBriefType
    {
        [Description("Cá nhân")]
        Customer = 1,
        [Description("Doanh nghiệp")]
        Company = 2
    }

    public enum EnumCitySearch
    {
        [Description("Hà Nội")]
        HN = 1,
        [Description("Hồ Chí Minh")]
        HCM = 79,
        [Description("Bắc Ninh")]
        BN = 27,
        [Description("Hải Dương")]
        HD = 30,
        [Description("Vĩnh Phúc")]
        VP = 26,
        [Description("Bình Dương")]
        BD = 74,
        [Description("Bắc Giang")]
        BacGiang = 24
    }

    public enum UploadStatus
    {
        [Description("Hoàn Thành")]
        FINISH = 1,
        [Description("Chưa hoàn thành")]
        UNTFINISH = 0
    }
    public enum LocateStatus
    {
        [Description("Có định vị")]
        LOCATE = 1,
        [Description("Không có định vị")]
        NOTLOCATE = 0
    }

    public enum EnumReCareLoanbrief
    {
        [Description("Mượn Cavet")]
        BorrowCavet = 1,
        [Description("Không mượn Cavet")]
        NotBorrowCavet = 2,
        [Description("Thu Cavet sau")]
        PostRegistration = 3
    }


    public enum EnumLoanStatus
    {
        [Description("Đơn khởi tạo")] INIT = 1,
        [Description("Đơn đã hủy")] CANCELED = 99,
        [Description("Chờ Saleadmin chia đơn")] SALEADMIN_LOAN_DISTRIBUTING = 10,
        [Description("Chờ chia đơn tự động cho telesales")] SYSTEM_TELESALE_DISTRIBUTING = 15,
        [Description("Chờ khách hàng chuẩn bị hồ sơ")] WAIT_CUSTOMER_PREPARE_LOAN = 16,
        [Description("Chờ Telesae tư vấn")] TELESALE_ADVICE = 20,
        [Description("Chờ chia đơn cho Hub")] HUB_LOAN_DISTRIBUTING = 30,
        [Description("Chờ CHT chia đơn")] HUB_CHT_LOAN_DISTRIBUTING = 35,
        [Description("Chờ thẩm định HO")] WAIT_HO_APPRAISER_REVIEW = 36,
        [Description("Chờ thẩm định")] APPRAISER_REVIEW = 40,
        [Description("Chờ CHT duyệt")] HUB_CHT_APPROVE = 45,
        [Description("Chờ chia đơn cho Thẩm định hồ sơ")] BRIEF_APPRAISER_LOAN_DISTRIBUTING = 50,
        [Description("Chờ Thẩm định hồ sơ xử lý")] BRIEF_APPRAISER_REVIEW = 55,
        [Description("Chốt hợp đồng với khách hàng")] WAIT_APPRAISER_CONFIRM_CUSTOMER = 56,
        [Description("Thẩm định hồ sơ đề xuất duyệt")] BRIEF_APPRAISER_APPROVE_PROPOSION = 60,
        [Description("Leader phê duyệt đề xuất hủy")] APPROVAL_LEADER_APPROVE_CANCEL = 61,
        [Description("Thẩm định hồ sơ đề xuất hủy")] BRIEF_APPRAISER_APPROVE_CANCEL = 65,
        [Description("Chờ chọn Lender")] LENDER_LOAN_DISTRIBUTING = 90,
        [Description("Chờ chia đơn cho BOD")] BOD_LOAN_DISTRIBUTING = 70,
        [Description("BOD phê duyệt hủy")] BOD_APPROVE_CANCEL = 75,
        [Description("COO Phê duyệt hủy")] COO_APPROVE_CANCEL = 80,
        [Description("Đã giải ngân")] DISBURSED = 100,
        [Description("Đóng hợp đồng")] FINISH = 110,
        [Description("Chờ giải ngân")] WAIT_DISBURSE = 101,
        [Description("Chờ kế toán giải ngân")] WAIT_DISBURSE_ACC = 102,
        [Description("Chờ lender giải ngân")] WAIT_DISBURSE_LEND = 103,
        [Description("Chờ giải ngân tự động")] WAIT_DISBURSE_AUTO = 104,
        [Description("Chờ lấy dữ liệu từ AI")] WAIT_GET_RULE_INFO_AI = 31,
        [Description("Chờ BOD duyệt")] BOD_REVIEW = 72,
        [Description("Chờ CVKD chuẩn bị hồ sơ")] WAIT_HUB_EMPLOYEE_PREPARE_LOAN = 32,
        [Description("Chờ giám đốc kinh doanh duyệt")] WAIT_CCO_APPROVE = 77,
    }

    public enum EnumGroupUser
    {
        [Description("Admin")]
        Admin = 1,
        [Description("Nhân viên telesales")]
        Telesale = 2,
        [Description("Nhân viên hub")]
        StaffHub = 45,
        [Description("Cửa hàng trưởng")]
        ManagerHub = 44,
        [Description("Nhân viên thẩm định")]
        ApproveEmp = 47,
        [Description("Leader thẩm định")]
        ApproveLeader = 46,
        [Description("TP telesales")]
        ManagerTelesales = 54,
        [Description("Thẩm định thực địa")]
        FieldSurvey = 58,// Thẩm định thực địa
        [Description("BOD")]
        BOD = 59,
        [Description("Giám đốc kinh doanh")]
        BusinessManagement = 686,
        [Description("Kiểm soát nội bộ")]
        KSNB = 55,
        [Description("Trưởng phòng KSNB")]
        TP_KSNB = 56,
    }


    public enum EnumActionComment
    {
        [Description("LMS Kế toán trả lại đơn")]//  AccountantBackCounselor = -11, //Kế Toán trả lại cho Tư Vấn Viên = -11,
        LMSAccountantReturn = -11,
        [Description("Telesale hủy đơn")]//SupportCancel = -2, // Support từ chối
        TelesaleCancel = -2,
        [Description("Comment LoanBrief")]//AG CommentCredit
        CommentLoanBrief = 3,
        [Description("Hub đẩy hỗ trợ")]
        TranferHub = 7,
        [Description("Sale Admin chia đơn")]
        SaleAdminSplitLoan = 8,
        [Description("Đẩy đơn cho lender")]//InternalControl = 9, // Kiểm soát nội bộ 2 đẩy cho đại lý
        LMSPushLoanLender = 9,
        [Description("Chia đơn cho thẩm định viên")]
        DistributingLoanTDHS = 10,
        [Description("Giải ngân")]//AccountantDisbursement = 11, // Kế Toán Giải Ngân
        LMSDisbursement = 11,
        [Description("Cửa hàng trưởng trả lại")]
        ManagerHubBack = 12,
        [Description("Chia đơn cho telesales")]
        DistributingLoanOfTelesale = 13,
        [Description("Kết quả RefPhone")]
        ResultRefPhone = 14,
        [Description("Kết quả RuleCheck")]
        ResultRuleCheck = 15,
        [Description("Kết quả Location Viettel")]
        ResultLocationViettel = 16,
        [Description("Kết quả Location Mobifone")]
        ResultLocationMobifone = 17,
        [Description("Gửi yêu cầu lấy thông tin Location")]
        RequestLocation = 18,
        [Description("Gửi yêu cầu lấy thông tin Refphone")]
        RequestRefphone = 19,
        [Description("Chia đơn tự động về cho hub")]
        DistributingLoanToHub = 20,
        [Description("Chia đơn cho nhân viên hub")]
        DistributingLoanOfHub = 21,
        [Description("Kết quả CreditScoring")]
        ResultCreditScoring = 22,
        [Description("Telesale đẩy đơn")] //SaleAdminConfirm: TeleSales đẩy hồ sơ lên hệ thống
        TelesalesPush = 23,
        [Description("Đóng hợp đồng erp")]
        CloseContractERP = 24,
        [Description("Đóng hợp đồng ginno")]
        CloseContractGinno = 25,
        [Description("Chuyển CVKD")]
        HubEmployeeChange = 26,
        [Description("Thẩm định trả đơn")]//CoordinatorReturnTDTD = 27, // KSNB[TDHS2] trả lại cho TĐTĐ (TDHS trả lại đơn cho nhân viên thẩm định)
        ApproveReturn = 27,
        [Description("BOD duyệt đơn")]
        BODPush = 28,
        [Description("BOD trả đơn")]
        BODReturn = 29,
        [Description("BOD hủy đơn")]
        BODCancel = 30,
        [Description("Đẩy đơn cho Lender")]
        PushLoanToLender = 51,
        [Description("Xác nhận mua bảo hiểm")]
        ConfirmInsurence = 52,
        [Description("Chốt hợp đồng và thông tin nhận tiền")]
        ConfirmLoanMoney = 55,
        [Description("Hoàn thành upload bổ sung chứng từ")]
        UploadCompleted = 56,
        [Description("Chốt hợp đồng và thông tin nhận tiền")]
        UploadNotCompleted = 57,
        [Description("Comment định vị - Lấy được vị trí")]
        Locate = 58,
        [Description("Comment định vị - Không lấy được vị trí")]
        NotLocate = 59,
        [Description("Hub đẩy đơn")]// Trưởng cửa hàng đẩy lên cho TĐHS
        HubPush = 72,
        [Description("Thực địa thẩm định công ty hoặc nhà ở")]
        FieldSurveyCompleteCompanyOrAddress = 76,
        [Description("Thực địa đang đi thẩm định")]
        FieldSurveyToDoing = 77,
        [Description("thẩm định xe")]
        VehicleAssessment = 78, // thẩm định xe
        [Description("Comment LMS")]
        LMS = 80,
        [Description("Lender trả lại đơn")]
        LMSLenderReturn = 82,
        [Description("Lender Care lấy lại đơn")]
        LMSLenderCareRetake = 83,
        [Description("Hub hủy đơn")] //HubCancelLoanCredit = 84, 
        HubCancel = 84,
        [Description("Tất toán")]
        LMSFinish = 85,
        [Description("LMS Khóa đơn vay")]
        LMSLoanLock = 86,
        [Description("LMS Mở khóa đơn vay")]
        LMSLoanOpenLock = 87,
        [Description("Thẩm định trả lại đơn")]
        APPReturnLoan = 89,
        [Description("Lấy lại thông tin location Sim")]
        ReGetLocation = 90,
        [Description("Hẹn gọi lại")]
        ScheduleTime = 91,
        [Description("TĐHS hủy đơn")]
        TĐHSCancel = 92,
        [Description("Nhân viên hub đẩy đơn")]// Nhân viên đẩy lên cho Trưởng cửa hàng
        StaffHubPush = 96,
        [Description("Lender Giải Ngân")]
        LenderGN = 100,
        [Description("Lender Khóa đơn vay")]
        LenderLoanLock = 101,
        [Description("Lender Mở khóa đơn vay")]
        LenderLoanOpenLock = 102,
        [Description("Đổi hình thức giải ngân")]
        LMSChangeDisbursement = 103,
        [Description("Hệ thống hủy đơn")]
        AutoCancel = 104,
        [Description("Chuyển hóa comment")]
        ChuyenHoaComment = 105,
        [Description("Chuyển thẩm định")]
        ApproveChange = 106,
        [Description("Mượn Cavet")]
        BorrowCavet = 107,
        [Description("Chuyển trạng thái Telesales")]
        ChangeStatusTelesales = 108,
        [Description("AI gọi hủy đơn")]
        AICancelLoan = 109,
        //[Description("Đặt lịch hẹn")]
        //ScheduleTimeHub = 110,
        [Description("ChangePipe")]
        ChangePipe = 111,
        [Description("Đăng ký lấy điểm Scoring")]
        CreditScoring = 112,
        [Description("Check CIC")]
        ResultCIC = 120,
        [Description("Thẩm định đẩy đơn")]//// AG TDHS đẩy lên cho phòng nguồn vốn
        ApprovePush = 124,
        [Description("RemoveLabelReBorrow")]
        RemoveLabelReBorrow = 113,
        [Description("CheckList")]
        CheckList = 114,
        [Description("Chia đơn rmkt oto cho Hub")]
        TranferRemaketingCar = 130,
        [Description("lender comment")]
        LenderComment = 131,
        [Description("Hub chuyển trạng thái đơn")]
        HubNextStep = 132,
        [Description("Thẩm định chuyển trạng thái đơn")]
        ApproverNextStep = 133,
        [Description("GDKD duyệt đơn")]
        BusinessManagePush = 134,
        [Description("CHT trình ngoại lệ")]
        ProposeExceptions = 135,
        [Description("GDKD trả lại đơn")]
        BusinessManageBack = 136,
        [Description("ChangePhone")]
        ChangePhone = 137,
        [Description("NĐT khóa đơn chia tự động")]
        LockDistribute = 138,
        [Description("NĐT trả lại đơn chia tự động")]
        ReturnLockDistribute = 139,
        [Description("CheckLiveness")]
        CheckLiveness = 140,
        [Description("Tái cơ cấu lịch trả nợ")]
        DebtRestructuring = 141,
        [Description("Tạo đơn đảo nợ")]
        InitDebtRevolvingLoan = 142,
        [Description("TP KSNB duyệt đoen")]
        TP_KSNB_Push = 143,
        [Description("TP KSNB trả đơn")]
        TTP_KSNB_Return = 144,
        [Description("Bỏ nhãn trình ngoại lệ")]
        CancelExceptions = 145,
        [Description("QLKV duyệt đơn")]
        QLKV_Push = 146,
        [Description("Tạo giao dịch đảm bảo")]
        TransactionSecured = 147
    }

    public enum EnumLoadLoanPurpose
    {
        [Description("Vay phục vụ mục đích kinh doanh")]
        VayKinhDoanh = 1,

        [Description("Vay tiêu dùng cá nhân")]
        VayTieuDungCaNhan = 2,

        [Description("Vay sắm sửa đồ đạc gia đình")]
        VayMuaDoGiaDinh = 3,

        [Description("Vay xây, sửa nhà cửa")]
        VayXayNha = 4,

        [Description("Vay du lịch")]
        VayDuLich = 5,

        [Description("Vay đóng học phí")]
        VayDongHocPhi = 6,

        [Description("Khác")]
        Khac = 7
    }

    public enum EnumProductCredit
    {
        [Description("Vay theo lương")]
        SalaryCreditType = 1,

        [Description("Vay qua ĐK XMCC")]
        MotorCreditType_CC = 2,

        [Description("Vay theo Sổ hộ khẩu")]
        FamilyCreditType = 4,

        [Description("Vay qua ĐK XMKCC")]
        MotorCreditType_KCC = 5,

        [Description("Ô tô ngân hàng")]
        OtoCreditType_KCC = 6,

        NhaDatCreditType = 7,

        [Description("Đăng ký ô tô")]
        OtoCreditType_CC = 8,

        [Description("Cầm ô tô")]
        CamotoCreditType = 11,

        [Description("Vay tín chấp")]
        GoiTinChap = 14,

        [Description("Vay theo sim")]
        Sim = 19,

        //PawnSim = 20,

        [Description("Vay trực tuyến theo sim")]
        BorrowOnline = 21,

        [Description("Vay theo lương online TM")]
        SalaryOnlineTM = 22,

        [Description("Vay theo lương online CK")]
        SalaryOnlineCK = 23,

        [Description("Vay nhanh ĐK XMCC")]
        LoanFastMoto_CC = 24,

        [Description("Vay nhanh ĐK XMKCC")]
        LoanFastMoto_KCC = 25,

        [Description("Vay siêu nhanh ĐK OTO")]
        LoanFastCar_CC = 26,

        [Description("Vay theo cầm  cố hàng hóa")]
        CamCoHangHoa = 27,

        [Description("Vay qua ĐK XMKGT")]
        MotorCreditType_KGT = 28
    }

    public enum EnumTypeofownership
    {
        [Description("Nhà chính chủ")]
        Sohuucanhan = 1,
        [Description("Đồng sở hữu")]
        Dongsohuu = 2,
        [Description("Đứng tên họ hàng")]
        Thuocsohuucuahohang = 3,
        [Description("Thuê")]
        Thue = 4,
        [Description("Nhà sở hữu")]
        NhaSoHuu = 6,
        [Description("Đứng tên bố mẹ")]
        DungTenBoMe = 7,
        [Description("Đứng tên vợ/ chồng")]
        DungTenVoChong = 8,
        [Description("Nhà sở hữu trùng sổ hộ khẩu(Cùng Tỉnh/TP)")]
        NhaSoHuuTrungSHK = 9,
        [Description("Nhà sở hữu không trùng sổ hộ khẩu(Cùng Tỉnh/TP)")]
        NhaSoHuuKhongTrungSHK = 10,
        [Description("Nhà thuê(Khác Tỉnh/TP)")]
        ThueKT1 = 11,
        [Description("Nhà thuê (KT2)")]
        ThueKT2 = 12,
        [Description("Nhà thuê(Cùng Tỉnh/TP)")]
        NhaThueCungTinh = 13,
        [Description("KT1")]
        KT1 = 14,
        [Description("KT3")]
        KT3 = 15
    }

    public enum EnumTypeReason
    {
        [Description("Thẩm định hồ sơ 2 hủy")]
        Tdhs2Cancel = 7,

        [Description("Hub hủy")]
        HubCancel = 100,

        [Description("Telesales hủy")]
        TeleSalesCancel = 101,

        [Description("TĐTĐ Tích lỗi")]
        TdtdAddError = 97,

        [Description("Telesales hủy đơn ô tô")]
        LoanCarTeleSalesCancel = 102,
    }

    public enum EnumTypeReasonReturn
    {
        [Description("Thẩm định trả lại TĐTĐ")]
        Approve = 85,
        [Description("Lender trả lại đơn")]
        Lender = 88,
    }

    public enum Aplication
    {
        [Description("LOS")]
        LOS = 1,
        [Description("LMS")]
        LMS = 2
    }

    public enum TypeOfQuestion
    {
        Text = 0,
        Select = 1,
        Radio = 2,
        Datetime = 3,
        Checkbox = 4
    }

    public enum EnumOperationType
    {
        EqualTo,

        NotEqualTo,

        GreaterThan,

        GreaterThanEqualTo,

        LessThan,

        LessThanEqualTo,

        Contains,

        StartsWith,

        EndsWith
    }
    public enum EnumActionPush
    {
        [Description("Đẩy đơn")]
        Push = 1,
        [Description("Trả lại đơn")]
        Back = 2,
        [Description("Hủy đơn")]
        Cancel = 3
    }

    public enum EnumActionUser
    {
        [Description("Sửa đơn vay")]
        Edit = 1,
        [Description("Kịch bản đơn vay")]
        Script = 2,
        [Description("Đẩy đơn vay")]
        PushLoan = 4,
        [Description("Trả lại đơn vay")]
        BackLoan = 8,
        [Description("Hủy đơn vay")]
        Cancel = 16,
        [Description("Upload chứng từ")]
        Upload = 32,
        [Description("Phê duyệt hủy")]
        ComfirmCancel = 64,
        //[Description("Nhật ký khoản vay")]
        //HistoryLoanbrief = 128,
        //[Description("Chốt hơp đồng")]
        //ComfirmContract = 256,

    }

    public enum ActionCallApi
    {
        [Description("Gửi tin nhắn")]
        Sms = 1,
        [Description("Check CIC")]
        Cic = 2,
        [Description("Lãi xuất & Bảo hiểm")]
        InterestAndInsurence = 3,
        [Description("Danh sách Lender")]
        ListLender = 4,
        [Description("Danh sách Khách hàng trả chậm")]
        DeferredPayment = 5,
        [Description("Get Lead Score")]
        GetLeadScore = 6,
        [Description("Tính tiền bảo hiểm App Lender")]
        CalculatorMoneyInsurance = 7,
        [Description("Giải ngân từ App Lender")]
        DisbursementLoanCreditOfAppLender = 8,
        [Description("Đăng ký lấy điểm credit scoring")]
        CreditScoring = 10,
        [Description("Detect face")]
        DetectFace = 34,
        [Description("Get Loans by Phone")]
        GetLoanByPhone = 35,
        [Description("Get Location Device")]
        LocationDevice = 36,
        [Description("Open Contract")]
        OpenContract = 37,
        [Description("Close Contract")]
        CloseContract = 38,
        [Description("Stop Point")]
        StopPoint = 39,
        [Description("Check Imei")]
        CheckImei = 40,
        [Description("Update ContractId")]
        UpdateContractId = 41,
        [Description("ReActive Device")]
        ReActiveDevice = 42,
        [Description("Get Customer by Phone")]
        GetCustomerByPhone = 50,
        [Description("Get first location")]
        GetFirstLocation = 51,
        [Description("Get report address")]
        GetReportAddress = 52,
        [Description("Get report location")]
        GetReportLocation = 53,
        [Description("Get Moto Price AI")]
        GetMotoPrice = 54,
        [Description("Send notify slack")]
        CallSlack = 55,
        [Description("update device status to erp")]
        ApiUpdateDeveiceToERP = 56,
        [Description("Push Log Price AI Moto ")]
        PushLogPriceAIMoto = 58,
        [Description("Check Employee Tima")]
        ApiCheckEployeeTima = 57,
        //Cisco
        [Description("Cisco Authorize")]
        CiscoAuthorize = 58,
        [Description("Cisco ExportingRecordings")]
        CiscoExportingRecordings = 59,
        [Description("Cisco ExportingDetails")]
        CiscoExportingDetails = 60,
        [Description("Cisco GetList")]
        CiscoGetList = 61,
        //End Cisco
        [Description("Check BlackList")]
        BlackList = 62,
        [Description("Send notify App lender")]
        PushToAppLender = 64,
        [Description("LMS Call api GN LOS")]
        LMSCallDisLos = 63,
        [Description("Check Bank AI")]
        CheckBankAI = 66,
        [Description("LMS Push Lender")]
        LMSPushLender = 65,
        [Description("Send notify web")]
        PushNotificationWeb = 67,
        [Description("Intersest Topup")]
        IntersestTopUp = 68,
        [Description("LMS Call IsCan Topip")]
        LMSCallIsCanTopup = 69,
        [Description("PushNotifiLender")]
        PushNotificationLender = 70,
        [Description("PushNotiGino")]
        PushNotiGino = 71,
        [Description("ChangePipe")]
        ChangePipe = 72,
        [Description("Hub Employee CheckIn")]
        GetHubEmployeeErp = 73,
        [Description("Update Va Account")]
        UpdateVaAccount = 74,
        [Description("Check Device ERP")]
        CheckDeviceErp = 75,
        [Description("Create User SSO")]
        CreateUserSSO = 76,
        [Description("Call Login SSO")]
        LoginSSO = 77,
        [Description("Kalapa family")]
        KalapaFamily = 78,
        [Description("Update Password SSO")]
        UpdatePassSSO = 79,
        [Description("Pay Amount For Customer")]
        PayAmountForCustomer = 80,
        [Description("Call Event Log AI Disbursement")]
        LogAIDisbursement = 81,
        [Description("Call Event Log AI Closing")]
        LogAIClosing = 82,
        [Description("Get Report Debt LMS")]
        GetReportDebt = 83,
        [Description("Get Employee Info Tima")]
        GetHubEmployeeTima = 84,
        [Description("Get PayAmount For Customer By LoanID")]
        GetPayAmountForCustomerByLoanID = 85,
        [Description("Update Gpay Account")]
        UpdateGpayAccount = 86,
        [Description("Get List Loan Hub Debt")]
        GetListLoanHubDebt = 87,
        [Description("Get List Recording Cisco")]
        GetListRecording = 88,
        [Description("Send notify to app cvkd")]
        FirebasePushNotifyApp = 89,
        [Description("Nhân viên TĐHS CheckIn")]
        GetUserCoordinatorCheckinErp = 90,
        [Description("Get Recording By User")]
        GetRecordingByUser = 91,
        [Description("Get F2L Telesales")]
        F2LTelesales = 92,
        [Description("Lấy file ghi âm của caresoft từ ERP")]
        GetRecordingCaresoftERP = 93,
        [Description("Lấy file ghi âm của từ API Caresoft")]
        GetRecordingCaresoftAPI = 94,
        [Description("Add Blacklist")]
        AddBlacklist = 95,
        [Description("Gen New Otp")]
        GenNewOtp = 96,
        [Description("Postback Dinos")]
        DinosPosback = 97,
        [Description("Esign Create Agreement")]
        CreateAgreement = 98,
        [Description("Get Info Lender")]
        InfoLender = 99,
        [Description("Tái cơ cấu lịch trả nợ")]
        DebtRestructuring = 100,
        [Description("Thông tin bảo hiểm")]
        InsuranceInfo = 101,
        [Description("Tạo mới Quận/Huyện")]
        CreateDistrict = 102,
        [Description("Tạo mới Phường/Xã")]
        CreateWard = 103,
        [Description("Lịch trả nợ")]
        PaymentLoan = 104,
        [Description("Get List Loan TopUp")]
        GetListLoanTopUp = 105,
        [Description("Get List Loan Suggest TopUp")]
        GetListLoanSuggestTopUp = 106,
        [Description("Push Phone To Cisco")]
        PushToCisco = 107
    }
    public enum EnumAppraisal
    {
        [Description("Thẩm định nhà")]
        AppraisalHome = 1,
        [Description("Thẩm định công ty")]
        AppraisalCompany = 2,
        [Description("Thẩm định nhà và công ty")]
        AppraisalHomeAndCompany = 3,
        [Description("Không thẩm định nhà và công ty")]
        NoAppraisalHomeAndCompany = 4

    }

    public enum EnumAppraisalState
    {
        [Description("Chưa thẩm định")]
        NotAppraised = 0,
        [Description("Đã thẩm định")]
        Appraised = 1
    }

    public enum DistributingLoanOfHub
    {

    }


    public enum StatusCallApi
    {
        [Description("Bắt đầu gọi API")]
        CallApi = 0,

        [Description("Thành công")]
        Success = 1,

        [Description("Thất bại")]
        Error = -1,
    }

    public enum StatusOfDeviceID
    {
        [Description("Chưa kích hoạt")]
        InActived = 0,
        [Description("Gắn thiết bị")]
        OpenContract = 1,
        [Description("Đã kích hoạt")]
        Actived = 2,
        [Description("Gỡ thiết bị(chưa lắp)")]
        Close = 3,
        [Description("Kích hoạt không thành công")]
        Fail = -1,
    }

    public enum EnumDeviceStatusGinno
    {
        [Description("Đã kích hoạt")]
        Actived = 1,
        [Description("Trong kho")]
        InStock = 2,
        [Description("Đang kích hoạt")]
        Activing = 10,
        [Description("Không thành công")]
        NoActive = -1,
    }

    public enum EnumShop
    {
        [Description("Credit")]
        Tima = 3703,
        [Description("HN-HUB004")]
        HN_HUB004 = 5782,
        [Description("Thanh Xuân")]
        PDV_THANH_XUAN = 44423,
        [Description("HN-HUB005")]
        HN_HUB005 = 5790,

        [Description("HCM-HUB001")]
        HCM_HUB001 = 5797,
        [Description("HCM-HUB003")]
        HCM_HUB003 = 7725,
        [Description("HCM-HUB004")]
        HCM_HUB004 = 7726,
        [Description("HCM-PDV2")]
        HCM_PDV2 = 34714,
        [Description("HCM-PDV1")]
        HCM_PDV1 = 27338,
        [Description("HCM-HUB006")]
        HCM_HUB006 = 11899,
        [Description("HCM-HUB007")]
        HCM_HUB007 = 14478,

        [Description("HN-HUB001")]
        HN_HUB001 = 5779,
        [Description("PDV-DE1")]
        PDV_DE1 = 44424,
        [Description("PDV-DE2")]
        PDV_DE2 = 44425,
        [Description("PDV-DE3")]
        PDV_DE3 = 44426
    }
    public enum EnumDisbursement
    {
        [Description("Kế Toán giải ngân")]
        DISBURSE_ACC = 1,
        [Description("Lender giải ngân")]
        DISBURSE_LEND = 2,
        [Description("Giải ngân tự động")]
        DISBURSE_AUTO = 3
    }

    public enum EnumDeviceOS
    {
        [Description("Ios")]
        IOS = 1,
        [Description("Android")]
        Android = 2
    }

    public enum EnumFieldSurveyState
    {
        [Description("Chưa thẩm định")]
        NotAppraised = 0,
        [Description("Hoàn thành thẩm định")]
        CompleteAppraised = 1,
        [Description("Đang thẩm định")]
        DoingAppraised = 2
    }

    public enum EnumFieldSurveyType
    {
        [Description("Thẩm định nhà")]
        Home = 1,
        [Description("Thẩm định công ty")]
        Company = 2,
    }

    public enum EnumInProcess
    {
        [Description("Wait process")]
        Done = 0,
        [Description("In Process")]
        Process = 1,
    }
    public enum EnumStatusOfProductAppraiser
    {
        [Description("Chưa gán")]
        NotSet = -2,

        [Description("Nhận")] // chưa định nghĩa
        Set1 = -1,

        [Description("Đang Thẩm Định")]
        Doing = 0,

        [Description("Hoàn thành thẩm định")]
        Done = 1,

        [Description("Nhận")]
        Set = 2,

        [Description("Trả lại")]
        Return = 3,
    }

    public enum EnumRateType
    {
        [Description("Tất toán cuối kỳ")]
        RateMonthADay = 6,
        [Description("Tất toán cuối kỳ")]
        TatToanCuoiKy = 14,
        [Description("Dư nợ giảm dần")]
        RateAmortization30Days = 10,
        [Description("Dư nợ giảm dần")]
        DuNoGiamDan = 13,
        [Description("Dư nợ giảm dần Topup")]
        DuNoGiamDanTopUp = 12
    }
    public enum EnumLoanTime
    {
        [Description("1 tháng")]
        OneMonth = 1,
        [Description("2 tháng")]
        TwoMonth = 2,
        [Description("3 tháng")]
        ThreeMonth = 3,
        [Description("6 tháng")]
        SixMonth = 6,
        [Description("9 tháng")]
        NineMonth = 9,
        [Description("12 tháng")]
        TwentyMonth = 12,
        [Description("18 tháng")]
        EighteenMonth = 18,
        [Description("24 tháng")]
        TwentyFourMonth = 24
    }

    public enum EnumFrequency
    {
        [Description("Hàng tháng")]
        OneMonth = 1,

    }

    public enum EnumTypeReceivingMoney
    {
        [Description("Tiền mặt")]
        MoneyCash = 1, // tiền mặt
        [Description("Chuyển khoản Số tài khoản")]
        NumberAccount = 3, // chuyển khoản Số tài khoản
        [Description("Số thẻ")]
        NumberCard = 4, // Số thẻ
    }

    public enum EnumBankMoneyCash
    {
        [Description("Tiền mặt")]
        MoneyCash = 35 // tiền mặt       
    }

    /// Hình thức nhận lương của khách hàng
    ///// </summary>
    //public enum TypeReceiveYourIncome
    //{
    //    [Description("Chuyển khoản")]
    //    ChuyenKhoan = 5,
    //    [Description("Tiền mặt")]
    //    TienMat = 1
    //}
    //Hình thức nhận tiền của Khách Hàng

    public enum EnumReceivingMoneyType
    {

        [Description("Tiền mặt")]
        TienMat = 1,
        [Description("Chuyển khoản Số tài khoản")]
        ChuyenKhoan = 3,
        [Description("Số thẻ")]
        SoThe = 4,
    }

    public enum HomeNetWorkMobile
    {
        [Description("Viettel")]
        Viettel = 1,

        [Description("Mobifone")]
        Mobi = 2,

        [Description("VinaPhone")]
        Vina = 3,

        [Description("Other")]
        Other = 4
    }

    public enum ServiceTypeAI
    {
        [Description("RuleCheck")]
        RuleCheck = 1,
        [Description("Location")]
        Location = 2,
        [Description("RefPhone")]
        RefPhone = 3,
        [Description("Range CreditScoring")]
        RangeCreditScoring = 4,
        [Description("CreditScoring")]
        CreditScoring = 5,
        [Description("Ekyc")]
        Ekyc = 6,
        [Description("Log LeadScore")]
        PushLogLeadScore = 7,
        [Description("Ekyc Info Network")]
        EkycGetInfoNetwork = 8,
        [Description("Ekyc Motorbike Registration Certificate")]
        EkycMotorbikeRegistrationCertificate = 9,
        [Description("Check Bill Electricity AI")]
        CheckBillElectricityAI = 10,
        [Description("Check Bill Water AI")]
        CheckBillWaterAI = 11,
        [Description("Tima Fraud Detection")]
        FraudDetection = 12,
        [Description("Lấy thông tin xe từ số vin(số khung)")]
        ParseVinNumber = 13,
        [Description("Nhận diện thực thể sống")]
        CheckLiveness = 14,
        [Description("Đăng ký crawl facebook")]
        FacebookInfo = 15,
        [Description("Check nợ xấu Momo")]
        CheckLoanMomo = 16,
        [Description("Đăng ký giao dịch đảm bảo")]
        RegisterSecuredTransaction = 17,
        [Description("Kiểm tra trạng thái giao dịch đảm bảo")]
        CheckStatusSecuredTransaction = 18,
        [Description("Định giá xe")]
        PriceAi = 19
    }

    public enum EnumReGetLocation
    {
        [Description("Gọi lại")]
        callback = 1
    }

    public enum EnumLogLoanInfoAiExcuted
    {
        [Description("Chờ gửi yêu cầu")]
        WaitRequest = -1,
        [Description("Chờ xử lý")]
        WaitHandle = 0,
        [Description("Đã có kết quả")]
        HasResult = 1,
        [Description("Đã xử lý kết quả xong")]
        Excuted = 2,
    }

    public enum EnumRefCodeStatus
    {
        [Description("Success")]
        Success = 0,
        [Description("Chưa có kết quả")]
        NotData = 1,
        [Description("Da gui MT cho khach hang")]
        SendedMT = 2,
        [Description("Khach hang dong y gui thong tin")]
        Accepted = 3,
        [Description("Khach hang khong dong y gui thong tin")]
        NotAccept = 4,
        [Description("Quá số lần thử lại")]
        OverReTry = 5,
        [Description("Thong tin request khong hop le")]
        Invalid = 6,
        [Description("Nguoi dung tu choi cho kiem tra thong tin")]
        RefuseAccept = 15
    }
    public enum StateDetailsReason
    {
        [Description("Tuân theo Role Check CIC")]
        CheckCic = 305,
        [Description("Hủy do k đạt điểm Matrix score")]
        MatrixScore = 553,
    }
    //Hình thức nhận lương của khách hàng
    public enum EnumImcomeType
    {
        [Description("Chuyển khoản")]
        ChuyenKhoan = 5,
        [Description("Tiền mặt")]
        TienMat = 1
    }

    public enum EnumLivingTime
    {
        [Description("Dưới 1 năm")]
        Duoimotnam = 1,
        [Description("1 -3 năm")]
        Modenbanam = 2,
        [Description("3 - 10 năm")]
        Badenmuoinam = 3,
        [Description("Hơn 10 năm")]
        Honmuoinam = 4,
        [Description("Dưới 6 tháng")]
        Duoi6thang = 5,
        [Description("6 tháng")]
        Trong6thang = 6,
        [Description("Từ 7-12 tháng")]
        Tu7den12thang = 7,
        [Description("Từ 12-36 tháng")]
        Tu12den36thang = 8,
        [Description("Từ 36-72 tháng")]
        Tu36den72thang = 9,
        [Description("Trên 72 tháng")]
        Tren72thang = 10
    }

    public enum EnumPlatformType
    {
        [Description("Ios System")]
        Ios = 1,
        [Description("Andriod System")]
        Andriod = 2,
        [Description("Web System")]
        Web = 3,
        [Description("Leadform")]
        Leadform = 4,
        [Description("LandingPage")]
        LandingPage = 5,
        [Description("Cms San Tima")]
        Cms = 6,
        [Description("Ag.Tima.vn")]
        AgTima = 7,
        [Description("SystemAuto")]
        SystemAuto = 8,
        [Description("Ai")]
        Ai = 9,
        [Description("Aff")]
        Aff = 10,

        [Description("ReMarketing")]
        ReMarketing = 97,
        [Description("Đến cửa hàng")]
        GoToShop = 98,
        [Description("Đơn mua từ sàn")]
        BuyOfSan = 99,
        [Description("Partner")]
        Partner = 100,
        [Description("APP Auto")]
        AppAuto = 88,
        [Description("Marketing")]
        Marketing = 101,

        [Description("Page")]// 
        Page = 102,

        [Description("Zalo")]
        Zalo = 103,

        [Description("Hotline")]
        Hotline = 104,
        [Description("Tele Marketing")]
        TeleMarketing = 105,

        [Description("Quảng cáo Facebook")]
        FacebookAds = 106,

        [Description("Tìm kiếm trên mạng")]
        SearchInternet = 107,

        [Description("Nhìn thấy phòng giao dịch")]
        SeeHub = 108,

        [Description("Nhìn thấy biển quảng cáo")]
        SeeBannerAds = 109,

        [Description("Nhìn thấy tờ rơi quảng cáo")]
        ToRoiQuangCao = 110,

        [Description("Nhìn thấy quảng cáo trên hộp đựng giấy ở quán ăn")]
        HopDungGiay = 111,

        [Description("Người quen giới thiệu")]
        FriendIntroduce = 112,

        [Description("Khác")]
        Other = 113,

        [Description("Đơn của AffTima")]
        AffTima = 114,

        [Description("Đơn Import File Excel")]
        ImportFileExcel = 115,

        [Description("Affiliate")]
        Affiliate = 116,

        [Description("MMOTima")]
        MMOTima = 117,

        [Description("TrustingSocial")]
        TrustingSocial = 118,

        [Description("MMOTimaCPQL")]
        MMOTimaCPQL = 119,

        [Description("Đơn con ô tô")]
        LoanCarChild = 120,

        [Description("LendingOnline WEB")]
        LendingOnlineWeb = 121,

        [Description("LendingOnline APP")]
        LendingOnlineApp = 122,

        [Description("Đơn của KBFINA")]
        KBFINA = 123,

        [Description("Người thân/quen giới thiệu")]
        RelativesAbout = 124,

        [Description("Khách hàng giới thiệu")]
        CustomerAbout = 125,

        [Description("Đơn CTV giới thiệu")]
        CTVAbout = 126,

        [Description("Đơn cá nhân tự kiếm")]
        DonTuKiem = 127,

        [Description("Khởi tạo từ đơn hủy")]
        CreateLoanCancel = 128,

        [Description("Khởi tạo từ đơn tất toán")]
        CreateLoanFinsh = 129,

        [Description("Tạo từ Jeff API")]
        JeffApi = 130,
    }

    public enum EnumApplication
    {
        [Description("Web LOS")]
        WebLOS = 1,
        [Description("Api LOS")]
        ApiLOS = 2,
    }
    public enum BrakeType
    {
        [Description("Phanh cơ")]
        phanh_co = 1,
        [Description("Phanh CBS")]
        cbs = 2,
        [Description("Phanh ABS")]
        abs = 3
    }
    public enum RimType
    {
        [Description("Nan hoa")]
        nan_hoa = 1,
        [Description("Vành đúc")]
        vanh_duc = 2,
    }

    public enum EnumCallRate
    {
        [Description("Không liên lạc")]
        NoContact = 0,
        [Description("Lượng Call thấp")]
        CallLow = 1,
        [Description("Lượng Call trung bình")]
        CallMedium = 2,
        [Description("Lượng Call cao")]
        CallHight = 3,
    }

    public enum EnumCallRateMobifone
    {
        [Description("Không nằm trong list số điện thoại hay liên lạc")]
        NotInListContact = 0,
        [Description("Nằm trong list số điện thoại hay liên lạc")]
        InListContact = 1,
        [Description("Không có liên lạc")]
        NoContact = -1
    }

    public enum CallDuration
    {
        [Description("Không liên lạc")]
        NoContact = 0,
        [Description("Thời lượng Call thấp")]
        CallDurationLow = 1,
        [Description("Thời lượng Call trung bình")]
        CallDurationMedium = 2,
        [Description("Thời lượng Call cao")]
        CallDurationHight = 3,
    }

    public enum EnumProvince
    {
        [Description("Hà Nội")]
        HaNoi = 1,
        [Description("Hồ Chí Minh")]
        HCM = 79,
        [Description("Bắc Ninh")]
        BacNinh = 27,
        [Description("Hải Dương")]
        HaiDuong = 30,
        [Description("Vĩnh Phúc")]
        VinhPhuc = 26,
        [Description("Bình Dương")]
        BinhDuong = 74,
        [Description("Bắc Giang")]
        BacGiang = 24
    }

    public enum EnumFilterClickTop
    {
        [Description("Đơn chờ tư vấn")]
        ADVICE_LOAN = 1,
        [Description("Chờ KH chuẩn bị hồ sơ")]
        PREPARE_LOAN = 2,
        [Description("TLS hủy")]
        CANCEL_LOAN = 3,
        [Description("Hot Lead chưa phản hồ")]
        HOTLEAD_NORESPONSE = 4,
        [Description("Hot Lead đã phản hồi")]
        HOTLEAD_RESPONDED = 5,
        [Description("Autocall")]
        AUTOCALL = 6,
        [Description("Chatbot")]
        CHATBOT = 7
    }
    public enum EnumSynchorizeFinance
    {
        [Description("Default")]
        Default = 0,
        [Description("Đơn hủy")]
        LoanCancel = 1,
        [Description("Đơn đã giải ngân")]
        LoanDisbursement = 2,
        [Description("Đơn đã tất toán")]
        LoanFinalized = 3,
        [Description("Đơn chưa đồng bộ về Aff")]
        LoanNotSynchorizeAff = 4,
        [Description("Đơn đã đồng bộ về Aff")]
        LoanDoneSynchorizeAff = 5,
        [Description("Đơn hủy mà đã bắn về Sàn thành công")]
        LoanCancelPushToFinance = 6,
        [Description("Đơn hủy mà đã bắn về Sàn bị từ chối")]
        LoanCancelPushToFinanceRefuse = 7,
        [Description("Đã dồng bộ đơn GN của Aff về Aff")]
        SynchorizeLoanDisbursementAff = 8,
        [Description("Đã đồng bộ đơn lên GoogleSheets")]
        SynchorizeLoanDisbursementToGoogleSheets = 9,
        [Description("Đã dồng bộ đơn QLF của Aff về Aff")]
        SynchorizeLoanQLFAff = 10,
        [Description("Đã đồng bộ đơn có google click lên GoogleSheets")]
        SynchorizeLoanGoogleClickToGoogleSheets = 11,
    }
    public enum EnumRelativeFamily
    {
        [Description("Chồng/Vợ")]
        ChongVo = 1,
        [Description("Mẹ")]
        Me = 2,
        [Description("Bố")]
        Bo = 3,
        [Description("Anh/em trai")]
        AnhEmTrai = 4,
        [Description("Chị/em gái")]
        ChiEmGai = 5,
        [Description("Đồng nghiệp")]
        DongNghiep = 6,
        [Description("Bà")]
        Ba = 7,
        [Description("Bác")]
        Bac = 8,
        [Description("Chú")]
        Chu = 9,
        [Description("Cô/Dì")]
        CoDi = 10,
        [Description("Ông")]
        Ong = 11,
        [Description("Người hay liên hệ")]
        HayLienHe = 12
    }

    public enum EnumCheckQualify
    {
        [Description("Không đạt tiêu chí qualify")]
        NoQualify = 2,
        [Description("Upload đủ chứng từ")]
        UploadImage = 4,
        [Description("Có nhu cầu vay")]
        NeedLoan = 8,
        [Description("Trong độ tuổi cho vay (20 - 60 tuổi)")]
        InAge = 16,
        [Description("Trong khu vực Tima hỗ trợ (HN, HCM)")]
        InAreaSupport = 32,
        [Description("Có xe máy")]
        HaveMotobike = 64,
        [Description("Có đăng ký xe bản gốc")]
        HaveOriginalVehicleRegistration = 128
    }
    public enum EnumHotLead
    {
        [Description("Hotlead chưa phản hồi")]
        NotResponse = 1,
        [Description("Hotlead đã phản hồi")]
        Responsed = 2,
    }

    public enum EnumTypeCallService
    {
        [Description("Caresoft")]
        Caresoft = 1,
        [Description("Metech")]
        Metech = 2,
        [Description("Metech Mobile")]
        MetechMobile = 3,
    }

    public enum EnumLivingWith
    {
        [Description("Chung với bố mẹ")]
        chungbome = 0,
        [Description("Riêng với vợ con")]
        riengvocon = 1,
        [Description("Riêng một mình")]
        riengmotminh = 2
    }

    public enum EnumStatusBase
    {
        [Description("Tất cả")]
        StatusDefault = -1,
        [Description("Đang hoạt động")]
        Active = 1,
        [Description("Đã khóa")]
        Locked = 0
    }

    public enum TelesaleRecievedLoan
    {
        [Description("Không nhận đơn")]
        NoReceivingApplication = 0,
        [Description("Nhận đơn")]
        ReceivingApplication = 1
    }

    public enum EnumTypeProduct
    {
        [Description("Sản phẩm nhỏ")]
        SmallProduct = 1,
        [Description("Sản phẩm lớn")]
        BigProduct = 2
    }

    public enum EnumTypeRecievedProduct
    {
        [Description("Sản phẩm nhỏ")]
        SmallProduct = 1,
        [Description("Sản phẩm lớn")]
        BigProduct = 2,
        [Description("Tất cả")]
        All = 3
    }
    public enum EnumActor
    {
        [Description("Update")]
        UpdateLoanbrief = 1,
        [Description("Update Script")]
        UpdateScript = 2,
        [Description("TDHS confirm")]
        TDHSComfirm = 3,
    }

    public enum EnumProductTypeId
    {
        [Description("Xe ga")]
        XeGa = 1,
        [Description("Xe số")]
        XeSo = 2,
        [Description("Xe côn")]
        XeCon = 3
    }
    public enum EnumOwnershipXMKCC
    {
        [Description("Xe mua lại của người thân, có chứng từ chứng minh quan hệ (Sổ hộ khẩu. Giấy khai sinh. Giấy đăng ký kết hôn ...)")]
        RepurchaseOfRelatives = 1,
        [Description("Xe mua lại có hợp đồng mua bán xe công chứng")]
        SaleContract = 2,
        [Description("Xe mua lại có hợp đồng mua bán của showroom và CMND chủ cũ")]
        ContractShowroom = 3,
        [Description("Xe mua lại có hợp đồng mua bán viết tay và CMND chủ cũ")]
        ContractHandwritten = 4,
        [Description("Xe mua lại chỉ có đăng ký xe và xe máy (không có giấy tờ đi kèm)")]
        RepurchaseOnlyRegistered = 5
    }
    public enum EnumTypeRemarketing
    {
        [Description("Đơn Marketing")]
        IsRemarketing = 1,
        [Description("Đơn topup")]
        IsTopUp = 2,
        [Description("Đơn có thể topup")]
        IsCanTopup = 3,
        [Description("Khởi tạo đơn Autocall")]
        IsCreateLoanAutocall = 4,
        [Description("Remarketing Aff")]
        IsAff = 5,
        [Description("Tái cấu trúc nợ")]
        DebtRevolvingLoan = 6
    }

    public enum EnumStatusTelesales
    {
        [Description("Không liên hệ được")]
        NoContact = 1,
        [Description("Cân nhắc")]
        Consider = 2,
        [Description("Chờ chuẩn bị hồ sơ")]
        WaitPrepareFile = 3,
        [Description("Hẹn gọi lại")]
        SeeLater = 33,
        [Description("Chưa liên hệ")]
        NotContact = 20,
        [Description("Đơn đã hủy")]
        LoanCancel = 99,
        [Description("Đơn hub trả về")]
        HubBack = 40,
        [Description("Đã giải ngân")]
        DISBURSED = 100,
        [Description("Đóng hợp đồng")]
        FINISH = 110,
        [Description("Chờ Hub xử lý")]
        PushHub = 4,
        [Description("Chờ TĐHS xử lý")]
        WaitTDHSHandle = 5,

    }
    public enum EnumAutoCallValue
    {
        [Description("Không đồng ý")]
        NoAccept = 0,
        [Description("Đồng ý")]
        Accepted = 1,
        [Description("Không có hành động gì")]
        NoAction = 3

    }

    public enum EnumFilterReasonCancel
    {
        [Description("KH chê LS cao")]
        CheLaiCao = 1,
        [Description("Không muốn thẩm định nhà")]
        KhongMuonTDNha = 2,
        [Description("Không muốn thẩm định cty")]
        KhongMuonTDCty = 3,
        [Description("Nhu cầu vay ít/nhiều hơn")]
        NhuCauVay = 4,
        [Description("Công việc không ổn định")]
        CongViec = 5,
    }

    public enum EnumCreateLoanAutocall
    {
        [Description("Done")]
        Done = 1,
        [Description("NotDone")]
        NotDone = 0,
    }

    public enum EnumSendSMS
    {
        [Description("Done")]
        Done = 1,
        [Description("NotDone")]
        NotDone = 0,
    }
    public enum EnumTypeSendSMS
    {
        [Description("Qualify autocall")]
        QualifiedAutoCall = 1,
        [Description("Send ZNS Zalo")]
        SendZNS = 2,
        [Description("Send SMS Form To HUB")]
        SendSmsFormToHub = 3
    }

    public enum EnumCampaignAutocall
    {
        [Description("AutoCall giờ hành chính")]
        AutocallHuy = 1805,
        [Description("Autocall đơn đã hủy")]
        AutocallBN = 1812
    }

    public enum EnumStatusLoanBriefImportFileExcel
    {
        [Description("Đơn chưa tạo")]
        LoanDefault = 0,
        [Description("Đơn tạo thành công")]
        LoanSuccess = 1,
        [Description("Đơn tạo thất bại")]
        LoanFail = -1
    }


    public enum EnumCheckLoanInformationType
    {
        [Description("Check Ảnh chứng từ")]
        CheckImage = 1,
        [Description("Check thông tin khách hàng")]
        CheckInfo = 2
    }
    public enum EnumCheckLoanInformation
    {
        [Description("Check thông tin cá nhân")]
        CheckPersonInfo = 1,
        [Description("Check địa chỉ đang ở")]
        CheckAddressHome = 2,
        [Description("Check địa chỉ SHK")]
        CheckAddressHousehold = 3,
        [Description("Check thông tin công việc")]
        CheckWork = 4,
        [Description("Check người thân, đồng nghiệp")]
        CheckRelativesAndColleagues = 5,
        [Description("Check hình thức vay")]
        CheckLoanCredit = 6,
        [Description("Check tài sản")]
        CheckMotobikeInformation = 7,
        [Description("Check thông tin khoản vay")]
        CheckLoanCreditInformation = 8,
        [Description("Check hình thức nhận tiền")]
        CheckDisbursementInfomation = 9
    }

    public enum EnumDetailStatusTelesales
    {
        [Description("Không nghe máy lần 1")]
        NotListening1 = 4,
        [Description("Không nghe máy lần 2")]
        NotListening2 = 5,
        [Description("Không nghe máy lần 3")]
        NotListening3 = 6,
        [Description("Không nghe máy lần 4")]
        NotListening4 = 7,
        [Description("Không nghe máy lần 5")]
        NotListening5 = 8,
        [Description("Không nghe máy lần 6")]
        NotListening6 = 9,
        [Description("Tắt máy/Thuê bao lần 1")]
        ShutdownSubscription1 = 10,
        [Description("Tắt máy/Thuê bao lần 2")]
        ShutdownSubscription2 = 11,
        [Description("Tắt máy/Thuê bao lần 3")]
        ShutdownSubscription3 = 12,
        [Description("Sai số/Số ĐT không có thực")]
        ErrorNumber = 13,
        [Description("Khác")]
        Other = 14,
        [Description("Cân nhắc về bảo hiểm")]
        ConsiderInsurance = 15,
        [Description("Cân nhắc về phí/lãi suất")]
        ConsiderInterestRate = 16,
        [Description("Cân nhắc về thời gian vay")]
        ConsiderTimeBorrow = 17,
        [Description("Cân nhắc việc giữ ĐKX")]
        ConsiderHoldDKX = 18,
        [Description("Cân nhắc việc TĐ nhà/công ty")]
        ConsiderExpertiseHouseAndCompany = 19,
        [Description("Đang bận, hẹn gọi lại sau")]
        BusyCallLater = 20,
        [Description("Đã lấy đủ yếu tố QL, Add zalo để follow")]
        QLFAddZaloFollow = 21,
        [Description("Cân nhắc khác")]
        ConsiderOther = 22,
        [Description("Chờ CMT/CCCD/HC/ĐKX")]
        WaitCMTCCCDHCDKX = 23,
        [Description("Chờ giấy tờ chứng minh NSH")]
        WaitProveNSH = 24,
        [Description("Chờ cung cấp ảnh chụp")]
        WaitProvidedImage = 25,
        [Description("Chờ chứng minh thu nhập")]
        WaitProofOfIncome = 26,
        [Description("Chờ tham khảo người thân")]
        WaitConsultYour = 27,
        [Description("Chờ thông tin tham chiếu")]
        WaitReferenceInformation = 28,
        [Description("Chờ khách đi công tác/về quê")]
        WaitBackFromWork = 29,
        [Description("Chờ vì lý do khác")]
        WaitOther = 30,
        [Description("Gọi lại nhưng khách hàng không nghe máy")]
        CallNotListening1 = 31,
        [Description("Gọi lại nhưng khách hàng không nghe máy")]
        CallNotListening2 = 32,
        [Description("Chờ HS nhưng gọi lại không vay nữa")]
        WaitFileCallReturnNoBorrow = 34,
        [Description("Cân nhắc nhưng gọi lại không vay nữa")]
        ConsiderCallReturnNoBorrow = 35,
        [Description("Khách hàng cúp máy ngang")]
        CupMayNgang = 36,
        [Description("Lý do khác")]
        ReasonOther = 37,
    }

    public enum EnumTypeOwnerShipDocument
    {
        [Description("Nhà sở hữu")]
        NSH = 1,

        [Description("Nhà Thuê")]
        NT = 2
    }
    public enum EnumWorkRuleType
    {
        [Description("Làm thuê lương chuyển khoản")]
        LamThueLuongChuyenKhoan = 1,
        [Description("Làm thuê lương tiền mặt có đóng BHXH/BHYT")]
        LuongTienMatCoBHXH = 2,
        [Description("Làm thuê lương tiền mặt không đóng BHXH/BHYT")]
        LuongTienMatKhongBHXH = 3,
        [Description("Kinh doanh có đăng ký")]
        KinhDoanhCoDangKy = 4,
        [Description("Kinh doanh không có đăng ký")]
        KinhDoanhKhongDangKy = 5,
        [Description("Lao động tự do và các KH không thuộc các nhóm trên")]
        LaoDongTuDo = 6,
        [Description("Grab/Shipper/Lái xe công nghệ")]
        GrabShipper = 7

    }

    public enum EnumJobTitle
    {
        [Description("Đi làm hưởng lương")]
        LamHuongLuong = 123,
        [Description("Tự kinh doanh")]
        TuDoanh = 124,
        [Description("Làm tài xế công nghệ")]
        LamTaiXeCongNghe = 125,
        [Description("Làm tự do")]
        LamTuDo = 126
    }

    public enum EnumDocumentVideo
    {
        [Description("VideoCC")]
        VideoCC = 306,
        [Description("VideoKCC")]
        VideoKCC = 307
    }

    public enum EnumWaterSupplier
    {
        [Description("Nước sạch Hà Nội (Hawacom)")]
        hawacom_water = 1,
        [Description("Nước Viwaco (Hà Nội)")]
        viwaco = 2,
        [Description("Công ty Nước sạch số 2 Hà Nội")]
        nshn2 = 3,
        [Description("Nước sạch số 3 Hà Nội (Hoàn Kiếm)")]
        nshn3 = 4,
        [Description("Nước sạch Tây Hà Nội")]
        tayhanoi_water = 5,
        [Description("Nước Sơn Tây Hà Nội")]
        sontay_water = 6,
        [Description("Nước Đồng Tiến Thành")]
        dongtienthanh_water = 7,
        [Description("Nước Bến Thành")]
        NUOCBENTHANH = 8,
        [Description("Nước Chợ Lớn")]
        NUOCCHOLON = 9,
        [Description("Nước Nhà Bè")]
        NUOCNB = 10,
        [Description("Nước Nông Thôn")]
        nong_thon_water = 11,
        [Description("Nước Thủ Đức")]
        TDWATER = 12,
        [Description("Nước Trung An")]
        TAWATER = 13,
        [Description("Nước Tân Hòa")]
        THWATER = 14,
        [Description("Nước Gia Định")]
        giadinhwater = 15,
        [Description("Nước Phú Hòa Tân")]
        PHTWATER = 16,
        [Description("Nước Đà Nẵng")]
        nuocdanang = 17,
        [Description("CP DV & XD Cấp nước Đồng Nai")]
        bienhoa_water = 18,
        [Description("Nước Bình Dương (Biwase)")]
        biwase_water = 19,
        [Description("Nước Vũng Tàu")]
        bwaco = 20,
        [Description("Trung Tâm NS Bà Rịa - Vũng Tàu")]
        baria_water = 21,
        [Description("Cấp nước Bình Phước")]
        binhphuoc_water = 22,
        [Description("Cấp thoát nước Bình Thuận")]
        binhthuan_water = 23,
        [Description("Nước Đồng Tháp")]
        dongthapwater = 24,
        [Description("Nước Bạc Liêu")]
        baclieu_water = 25,
        [Description("Nước An Giang")]
        angiang_water = 26,
        [Description("Nước Cần Thơ")]
        cantho_water = 27,
        [Description("Nước Cần Thơ 2")]
        cantho2_water = 28,
        [Description("Nước Tiền Giang")]
        tiengiang_water = 29,
        [Description("Nước Trà Vinh")]
        travinh_water = 30,
        [Description("Nước Kiên Giang")]
        kiengiang_water = 31,
        [Description("Nước Bến Tre")]
        bentre_water = 32,
        [Description("Nước Hậu Giang")]
        haugiang_water = 33,
        [Description("Nước Vĩnh Long")]
        vinhlong_water = 34,
        [Description("Nước sạch Hà Nam")]
        hanam_water = 35,
        [Description("Nước Hải Phòng")]
        haiphong_water = 36,
        [Description("Nước Hòa Bình")]
        hoabinh_water = 37,
        [Description("Nước Sơn La")]
        sonla_water = 38,
        [Description("Nước Quảng Ninh")]
        quangninh_water = 39,
        [Description("Nước Cao Bằng")]
        caobang_water = 40,
        [Description("Nước Điện Biên")]
        dienbien_water = 41,
        [Description("Nước Nam Định")]
        namdinh_water = 42,
        [Description("Nước sạch Vĩnh Phúc")]
        vinhphuc_water = 43,
        [Description("Nước Hưng Yên")]
        hungyen_water = 44,
        [Description("Nước Bắc Ninh")]
        bacninh_water = 45,
        [Description("Nước Sạch Bắc Giang")]
        bacgiang_water = 46,
        [Description("Nước Sạch Số 2 Hải Phòng")]
        haiphongv2_water = 47,
        [Description("Nước Thừa Thiên Huế")]
        HUE_WATER = 48,
        [Description("Nước Lâm Đồng")]
        lamdong_water = 49,
        [Description("Nước Gia Lai")]
        gialai_water = 50,
        [Description("Nước Đắk Lắk")]
        daklak_water = 51,
        [Description("Nước Khánh Hòa")]
        khanhhoa_water = 52,
        [Description("Nước Nghệ An")]
        nghean_water = 53,
    }

    public enum EnumMobileUpload
    {
        [Description("Tất cả")]
        All = 0,
        [Description("Album")]
        Album = 1,
        [Description("Chụp/quay trực tiếp")]
        Live = 2
    }
    public enum EnumSourceUpload
    {
        [Description("Web")]
        Web = 1,
        [Description("Mobile")]
        Mobile = 2,
        [Description("TimaCare")]
        TimaCare = 3,
        [Description("WebTimaCare")]
        WebTimaCare = 4,
        [Description("Auto System")]
        AutoSystem = 5,
    }

    public enum EnumDocumentCoincideHouseHold
    {
        [Description("Sổ hộ khẩu, trong sổ có tên KH")]
        KHCoTenTrongSHK = 1,
        [Description("Sổ hộ khẩu, trong sổ chỉ có tên người thân KH")]
        CoTenNguoiThanTrongSHK = 2
    }

    public enum EnumDocumentNoCoincideHouseHold
    {
        [Description("Hóa đơn tiền điện/nước mang tên KH")]
        HoaDonDienNuocMangTenKH = 3,
        [Description("Hóa đơn tiền điện/nước mang tên người thân KH")]
        HoaDonDienNuocMangTenNguoiThan = 4,
        [Description("Sổ đỏ/Sổ hồng/HĐMB nhà (căn hộ) mang tên KH")]
        SoDoHDMBMangTenKH = 5,
        [Description("Sổ đỏ/Sổ hồng/HĐMB nhà (căn hộ) mang tên người thân KH")]
        SoDoHDMBMangTenNguoiThan = 6,
        [Description("Sổ tạm trú tạm vắng trên 6 tháng")]
        SoTamTruTren6thang = 7,
        [Description("Sổ HK thuộc KVHT của Tima")]
        SoHKThuocKVHoTro = 8,
        [Description("KH không cung cấp được giấy tờ nào")]
        KhongCungCapDuocGiayTo = 9,
        [Description("Sổ BHXH")]
        BHXH = 10,
    }

    public enum EnumDocumentSalaryBankTransfer
    {
        [Description("Ảnh tin nhắn lương/App tài khoản lương/Sao kê lương tháng gấn nhất")]
        AnhSmsLuong = 1,
        [Description("Ảnh bản gốc BHYT/BHXH hoặc ảnh trên link tra cứu BHXH")]
        AnhBHYTBHXH = 2,
        [Description("Không có giấy tờ chứng minh công việc")]
        SoDoHDMBMangTenKH = 3
    }

    public enum EnumDocumentType
    {
        [Description("CMND XeCC")]
        CMND_CC = 309,
        [Description("CMND XeKCC")]
        CMND_KCC = 329,
        [Description("Hộ chiếu XeCC")]
        HoChieu_CC = 421,
        [Description("Ảnh chụp trang đầu trong bản gốc Hộ chiếu - CVKD chụp")]
        HoChieu_CVKD_Chup = 423,
        [Description("Hộ chiếu XeKCC")]
        HoChieu_KCC = 424,
        [Description("CMND Khách chụp")]
        CMND_KHACH_CC = 310,
        [Description("CMND Khách chụp")]
        CMND_KHACH_KCC = 330,
        [Description("Thẩm định nơi ở xe CC nhà sở hữu")]
        TDNO_XECC_NhaSH = 362,
        [Description("Thẩm định nơi ở xe KCC nhà sở hữu")]
        TDNO_XEKCC_NhaSH = 378,
        [Description("Thẩm định nơi ở xe CC nhà thuê")]
        TDNO_XECC_NhaThue = 407,
        [Description("Thẩm định nơi ở xe KCC nhà thuê")]
        TDNO_XEKCC_NhaThue = 408,
        [Description("Chứng từ chứng minh công việc xe CC trừ Grab/shipper/lái xe công nghệ")]
        TDCV_XECC_NhaSH = 354,
        [Description("Video CVKD quay tin nhắn lương tháng gần nhất trên internet banking/mobile banking")]
        Video_TinNhanLuong = 357,
        [Description("HĐLĐ có dấu đỏ của công ty")]
        HDLD_CoDauDoCongTy = 359,
        [Description("Chứng từ chứng minh công việc xe KCC trừ Grab/shipper/lái xe công nghệ")]
        TDCV_XEKCC_NhaSH = 370,
        [Description("Hồ sơ chứng minh Nơi ở")]
        ChungMinhNoiO_CC = 381,
        [Description("Hồ sơ chứng minh Nơi ở KCC")]
        ChungMinhNoiO_KCC = 385,

        [Description("CMND/CCCD")]
        EnumNationalCardType = 309,
        [Description("Hộ chiếu")]
        EnumPassportType = 421,

        [Description("Ảnh Nhà do khách hàng chụp - app Tima Care")]
        AnhNha_TimaCare = 431,
        [Description("Ảnh nơi làm việc do KH chụp - App Tima Care")]
        AnhNoiLamViec_TimaCare = 435,
        [Description("CVKD chụp nơi ở khách hàng")]
        CVKD_NoiO = 364,
        [Description("Ảnh thẩm định nơi làm việc")]
        CVKD_NoiLamViec = 417,
        [Description("Ảnh KH chụp bên trong nhà")]
        Tima_Care_Document_In_Home = 440,
        [Description("Video KH quay bên trong nhà")]
        Tima_Care_Video_In_Home = 363,

        [Description("Ảnh KH chụp bên trong nơi làm việc")]
        Tima_Care_Document_In_Company = 443,
        [Description("Video KH quay bên trong nơi làm việc")]
        Tima_Care_Video_In_Company = 415,
        [Description("Ảnh chụp 2 mặt bản gốc CMND/CCCD - CVKD chụp")]
        CMND_CCCD = 311,
        [Description("Ảnh 2 mặt bản gốc ĐKX - CVKD chụp")]
        Motorbike_Registration_Certificate = 314,
        [Description("Ảnh 2 mặt bản gốc ĐKX qua máy soi/đèn soi")]
        Motorbike_Registration_Certificate_Light = 315,
        [Description("Bộ chứng từ ký kết")]
        CTKK = 325,
        [Description("Thông tin thuê bao")]
        TTTB = 326,
        [Description("Ảnh SHK")]
        SHK = 384,
        [Description("Thẩm định nơi làm việc")]
        ThamDinhNoiLamViec = 413,
        [Description("Ảnh bên trong nơi ở KH do cvkd chụp")]
        CVKD_In_Home_Customer = 446,
        [Description("Ảnh checkout nơi ở do cvkd chụp")]
        CVKD_CheckOut_Home_Customer = 447,

        [Description("Ảnh bên trong nơi làm việc KH do cvkd chụp")]
        CVKD_In_Company_Customer = 449,
        [Description("Ảnh checkout nơi làm việc do cvkd chụp")]
        CVKD_CheckOut_Company_Customer = 450,
        [Description("Số điện thoại tham chiếu")]
        SDT_ThamChieu = 451,
        [Description("Ảnh Scan hợp đồng")]
        ScanContract = 453,
        [Description("Ảnh thực tế xe 2 bên xe, đầu xe và phía sau xe")]
        AnhThucTeXe = 317,
        [Description("Ảnh số khung, số máy của xe")]
        AnhSoKhungSoMay = 318,
        [Description("Zalo của KH")]
        Zalo = 328,
        [Description("Danh bạ điện thoại của KH")]
        DanhBa = 419,
        [Description("Ảnh chân dung khách hàng")]
        ChanDungKH = 125,
        [Description("CVKD chụp ảnh nơi ở KH Ảnh 1")]
        CVKD_In_Home_Customer_1 = 445,
        [Description("Ảnh Selfie")]
        Selfie = 125,
        [Description("Ảnh Ekyc")]
        EkycImage = 455
    }

    public enum EnumUser
    {
        [Description("spt_System")]
        spt_System = 26695,
        [Description("ctv_autocall")]
        ctv_autocall = 55684,
        [Description("autocall_coldlead")]
        autocall_coldlead = 55694,
        [Description("rmkt_loan_finish")]
        follow = 55732,//Prod: 55732 dev: 55776
        [Description("linhtq")]
        linhtq = 41687,//Prod: 41687 dev: 41687
        [Description("spt_thoapt")]
        spt_thoapt = 55680,//
        [Description("spt_huyennt")]
        spt_huyennt = 51814,
        [Description("spt_linhntn")]
        spt_linhntn = 55673,
        [Description("spt_phuonglm1")]
        spt_phuonglm1 = 30853,
        [Description("tvv_test")]
        tvv_test = 26464,
        [Description("Ngô Phương Anh")]
        drs_anhnp = 41808,
        [Description("Dương Quốc Thắng")]
        drs_thangdq = 41013,
        [Description("Lại Thị Trang")]
        hub_tranglt = 54484,

        [Description("Lâm Mỹ Tuyền")]
        hub_tuyenlm = 55766,

        [Description("qc_phuongnt")]
        qc_phuongnt = 55764,
        [Description("recare")]
        recare = 55797, //Prod: 55797, dev: 55778

        [Description("Remkt")]
        Remkt = 55743,

        [Description("spt_hoanpt")]
        spt_hoanpt = 55643,

        [Description("spt_nhunglt")]
        spt_nhunglt = 55800,

        [Description("spt_huongkq1")]
        spt_huongkq1 = 55646,

        [Description("system_team")]
        system_team = 55840,

        [Description("follow_team")]
        follow_team = 55841,

        [Description("recare_team")]
        recare_team = 55842,

        [Description("spt_hangmt")]
        spt_hangmt = 51642,

        [Description("spt_linhht")]
        spt_linhht = 55672,

        [Description("rmkt_topup")]
        rmkt_topup = 55854, //Prod: 55854, dev: 55852

        [Description("spt_dutd1")]
        spt_dutd1 = 55870,

        [Description("spt_anhntv")]
        spt_anhntv = 55887,
    }

    public enum EnumIsHeadOffice
    {

    }

    public enum EnumTypeRequired
    {
        [Description("Có đầy đủ con")]
        Full = 1,
        [Description("Một trong số con")]
        Or = 2
    }

    public enum EnumTypeOfOwnerShipDocument
    {
        [Description("Nhà sở hữu")]
        NhaSoHuu = 1,
        [Description("Nhà thuê")]
        NhaThue = 2,
    }

    public enum TypeFile
    {
        [Description("Ảnh")]
        Image = 1,
        [Description("Video")]
        Video = 2,
    }

    public enum TypeGetLatLng
    {
        Home = 1,
        Company = 2
    }
    public enum DocumentBusiness
    {
        [Description("Đăng kí kinh doanh")]
        BusinessRegistration = 1,
        [Description("Giấy CN vệ sinh ATTP")]
        VSATTP = 2,
        [Description("CN địa điểm kinh doanh")]
        BusinessLocation = 3,
    }
    public enum EnumLoanStatusDetail
    {
        [Description("TLS")] TLS = 1,
        [Description("CVKD")] CVKD = 2,
        [Description("TĐHS")] TDHS = 3
    }

    public enum EnumLoanStatusDetailHub
    {
        [Description("Lead chưa liên hệ")] LeadCLH = 40,
        [Description("CVKD đã liên hệ KH")] CvkdDaLienHe = 41,
        [Description("Chốt ký HS tại cửa hàng")] CvkdChoHoSoTaiCuaHang = 46,
        [Description("CVKD gặp được KH")] CvkdGapKh = 54,
        [Description("CVKD đi thẩm định thực địa")] CvkdThucDia = 62,
    }
    public enum EnumLoanStatusDetailApprover
    {
        [Description("Đang xử lý hồ sơ")] DangXuLy = 73,
        [Description("Bổ sung giấy tờ theo yêu cầu TDHS")] BoSungGiayTo = 75,
        [Description("Hồ sơ đẩy lên ngoài giờ làm việc")] NgoaiGioLamViec = 74
    }

    public enum EnumDocumentJob1
    {
        [Description("Bộ 1:HĐLĐ/Quyết định bổ nhiệm/Thẻ NV + THẻ BHYT + Phiếu lương (bảng lương) 1 tháng gần nhất có đóng dấu đỏ)")]
        BO1 = 1,
        [Description("Bộ 2:HĐLĐ/Quyết định bổ nhiệm/Thẻ NV + Sao kê lương qua ngân hàng 1 tháng gần nhất (Bản cứng có đóng dấu đỏ của ngân hàng hoặc bản Online từ internet Banking)")]
        BO2 = 2,
        [Description("Bộ 3: Sao kê lương qua ngân hàng 2 tháng gần nhất (Bản cứng có đóng dấu đỏ của ngân hàng hoặc bản Online từ internet Banking)")]
        BO3 = 3
    }
    public enum EnumDocumentJob2
    {
        [Description("Thẻ BHYT")]
        BO4 = 4,
        [Description("Phiếu lương (bảng lương) 1 tháng gần nhất")]
        BO5 = 5,
        [Description("Quyết định bổ nhiệm")]
        BO6 = 6,
        [Description("HĐLĐ còn hạn tối thiểu 3 tháng")]
        BO7 = 7,
        [Description("Sao kê có dấu của ngân hàng 1 tháng gần nhất")]
        BO8 = 8,
        [Description("ĐKKD/CN đăng ký địa điểm KD/Xácnhận VSATTP")]
        BO11 = 11,
        [Description("Tài xế/Shipper: Video doanh thu trên app chạy xe/app shipper thể hiện tối thiểu 4 triệu trong 4 tuần gần nhất")]
        BO9 = 9,
        [Description("Giấy tờ khác (comment rõ)")]
        BO10 = 10,
    }

    public enum InfomationProductDetail
    {
        [Description("Gói vay nhanh 4 triệu")]
        VayNhanh4tr = 1,
        [Description("KT 1 có SHK, vay tối đa 10 triệu")]
        KT1_10 = 2,
        [Description("KT 1 có SHK, vay tối đa 20 triệu")]
        KT1_20 = 3,
        [Description("KT 1 có SHK, vay tối đa 30 triệu")]
        KT1_30 = 4,
        [Description("KT3 có SHK vay tối đa 10 triệu")]
        KT3_10 = 5,
        [Description("KT3 có SHK vay tối đa 15 triệu")]
        KT3_15 = 6,
        [Description("KT3 có SHK vay tối đa 20 triệu")]
        KT3_20 = 7,

        [Description("KT1-KGT, vay tối đa 10 triệu")]
        KT1_KGT_10 = 8,
        [Description("KT1-KGT, vay tối đa 15 triệu")]
        KT1_KGT_15 = 9,
        [Description("KT1-KGT, vay tối đa 20 triệu")]
        KT1_KGT_20 = 10,
        [Description("KT1-KSHK, vay nhanh 4 triệu")]
        KT1_KSHK_4tr = 11,
        [Description("KT1-KGT, vay nhanh 4 triệu")]
        KT1_KGT_4tr = 12,

        [Description("KT3-KGT, vay tối đa 10 triệu")]
        KT3_KGT_10 = 13,
        [Description("KT3-KGT, vay tối đa 15 triệu")]
        KT3_KGT_15 = 14,
        [Description("KT3-KTN, vay nhanh 4 triệu")]
        KT3_KTN_4tr = 15,
        [Description("KT3-KGT, vay nhanh 4 triệu")]
        KT3_KGT_4tr = 16,

        [Description("KT1-KSHK, vay tối đa 10 triệu")]
        KT1_KSHK_10tr = 17,
        [Description("KT1-CC vay nhanh 100 triệu")]
        OTO_KT1_CC_VN_100tr = 18,
        [Description("KT1-CC vay tối đa 100 triệu")]
        OTO_KT1_CC_TD_100tr = 19,
        [Description("KT1-CC vay tối đa 200 triệu")]
        OTO_KT_CC_200tr = 20,
        [Description("KT1-CC vay tối đa 300 triệu")]
        OTO_KT_CC_300tr = 21,
        [Description("KT3-CC vay tối đa 50 triệu")]
        OTO_KT3_CC_50tr = 22,
        [Description("KT3-CC vay tối đa 100 triệu")]
        OTO_KT3_CC_100tr = 23,
        [Description("KT1-KCC vay nhanh 100 triệu")]
        OTO_KT1_KCC_VN_100tr = 24,
        [Description("KT1-KCC vay tối đa 100 triệu")]
        OTO_KT1_KCC_TD_100tr = 25,
        [Description("KT1-KCC vay tối đa 200 triệu")]
        OTO_KT1_KCC_200tr = 26,
        [Description("KT1-KCC vay tối đa 300 triệu")]
        OTO_KT1_KCC_300tr = 27,
        [Description("KT3-KCC vay tối đa 50 triệu")]
        OTO_KT3_KCC_50tr = 28,
        [Description("KT3-KCC vay tối đa 100 triệu")]
        OTO_KT3_KCC_100tr = 29,
        [Description("KHDN vay nhanh 100 triệu")]
        OTO_KHDN_VN_100tr = 30,
        [Description("KHDN vay tối đa 100 triệu")]
        OTO_KHDN_TD_100tr = 31,
        [Description("KHDN vay tối đa 300 triệu")]
        OTO_KHDN_TD_300tr = 32,

        [Description("LO-KT1-KSHK vay tối đa 5 triệu")]
        LO_KT1_KSHK_5tr = 33,

        [Description("KT1, vay nhanh 5 triệu")]
        XMCC_KT1_5tr = 34,
        [Description("KT1-12, vay tối đa 12 triệu")]
        XMCC_KT1_12 = 36,
        [Description("KT1-22, vay tối đa 22 triệu")]
        XMCC_KT1_22 = 37,
        [Description("KT1-32, vay tối đa 32 triệu")]
        XMCC_KT1_32 = 38,
        [Description("KT1-42, vay tối đa 42 triệu")]
        XMCC_KT1_42 = 39,
        [Description("KT3, vay nhanh 5 triệu")]
        XMCC_KT3_5tr = 40,
        [Description("KT3-12, vay tối đa 12 triệu")]
        XMCC_KT3_12 = 41,
        [Description("KT3-22, vay tối đa 22 triệu")]
        XMCC_KT3_22 = 42,
        [Description("KT3-32, vay tối đa 32 triệu")]
        XMCC_KT3_32 = 43,

        [Description("LO-KT3-KTN vay tối đa 5 triệu")]
        LO_KT3_KTN_5tr = 44,
        [Description("LO-KT1 vay tối đa 12 triệu")]
        LO_KT1_12tr = 45,
        [Description("LO-KT1-KSHK vay tối đa 12 triệu")]
        LO_KT1_KSHK_12tr = 46,

        [Description("KT1-KSHK, vay tối đa 12 triệu")]
        KT1_KSHK_12tr = 47,

        [Description("KT1-CC vay 500 triệu")]
        OTO_KT1_CC_VAY_500tr = 48,
        [Description("KT1-KCC vay 500 triệu")]
        OTO_KT1_KCC_VAY_500tr = 49,

        [Description("KHDN vay 200 triệu")]
        OTO_KHDN_VAY_200tr = 50,
    }

    public enum LogReLoanbriefExcuted
    {
        [Description("Chưa xử lý")]
        NoExcuted = 0,
        [Description("Đã xử lý")]
        IsExcuted = 1,
    }

    public enum EnumDepartment
    {
        [Description("Telesale")]
        TLS = 1,
        [Description("Hub")]
        HUB = 2,
        [Description("TĐHS")]
        TDHS = 3,
        [Description("Lender Care")]
        LENDERCARE = 4
    }

    public enum ActionChangePipeline
    {
        Next = 1,
        Back = 2,
        Init = 3
    }

    public enum TypeReLoanbrief
    {
        [Description("Đơn hủy")]
        CancelLoanbrief = 1,
        [Description("Đơn tất toán")]
        FinishLoanbrief = 2,
        [Description("Tạo đơn Topup")]
        LoanBriefTopup = 3,
        [Description("Tạo đơn Dpd <= 8")]
        LoanBriefDpd = 4,
    }

    public enum EnumDistrict
    {
        [Description("Hoàng Mai")]
        Hoang_Mai = 8,
        [Description("Thanh Trì")]
        Thanh_Tri = 20,
        [Description("Thanh Xuân")]
        Thanh_Xuan = 9,

        [Description("Ba Vì")]
        Ba_Vi = 271,
        [Description("Phúc Thọ")]
        Phuc_Tho = 272,
        [Description("Sơn Tây")]
        Son_Tay = 269,
        [Description("Đan Phượng")]
        Dan_Phuong = 273,
        [Description("Thạch Thất")]
        Thach_That = 276,
        [Description("Hoài Đức")]
        Hoai_Duc = 274,
        [Description("Quốc Oai")]
        Quoc_Oai = 275,
        [Description("Chương Mỹ")]
        Chuong_My = 277,
        [Description("Thanh Oai")]
        Thanh_Oai = 278,
        [Description("Thường Tín")]
        Thuong_Tin = 279,
        [Description("Phú Xuyên")]
        Phu_Xuyen = 280,
        [Description("Ứng Hòa")]
        Ung_Hoa = 281,
        [Description("Mỹ Đức")]
        My_Duc = 282,
        [Description("TP Dĩ An")]
        Di_An = 724

    }

    public enum EnumDisbursementWaitingReq
    {
        [Description("Tất cả")]
        All = 1,

        [Description("Chờ chọn Lender")]
        LENDER_LOAN_DISTRIBUTING = 2,

        [Description("Chờ kế toán giải ngân")]
        DISBURSE_ACC = 3,

        [Description("Chờ lender giải ngân")]
        DISBURSE_LENDER = 4,

        [Description("Chờ GN tự động")]
        DISBURSE_AUTO = 5,

        [Description("Kế toàn GN & GN tự động")]
        DISBURSE_ACC_AND_AUTO = 10
    }

    public enum EnumTelesaleShift
    {
        [Description("8h20 -> 11h55")]
        SANG = 2,
        [Description("13h15 -> 17h30")]
        CHIEU = 4,
        [Description("17h30 -> 20h25")]
        TOI = 8
    }

    public enum TypeDistributionLog
    {
        [Description("Chia đơn cho tls")]
        TELESALE = 1,
        [Description("Chia đơn cho CVKD")]
        CVKD = 2,
        [Description("Chia đơn cho TDHS")]
        TDHS = 3,
    }

    #region Log Loan Action
    public enum EnumLogLoanAction
    {
        [Description("Telesale nhận đơn")]
        TLSReceiveLoan = 1,
        [Description("Gọi điện")]
        Call = 2,
        [Description("Comment đơn vay")]
        Comment = 3,
        [Description("Cập nhật thông tin đơn vay")]
        LoanEdit = 4,
        [Description("TLS chuyển trạng thái đơn vay")]
        TLSChangeStatus = 5,
        [Description("Hub chuyển trạng thái đơn vay")]
        HubChangeStatus = 6,
        [Description("Hub nhận đơn")]
        HubReceiveLoan = 7,
        [Description("Nhân viên hub  nhận")]
        EmpHubReceiveLoan = 8,
        [Description("Đẩy đơn")]
        LoanPush = 9,
        [Description("TĐHS nhận đơn")]
        ApproveReceiveLoan = 10,
        [Description("Hủy")]
        LoanCancel = 11,
        [Description("Trả lại đơn")]
        LoanReturn = 12,
        [Description("Cập nhật đơn trên kịch bản")]
        LoanUpdateScript = 13,
        [Description("Tạo đơn")]
        CreateLoan = 14,

        [Description("Giải ngân")]
        Disbursement = 15

    }
    public enum EnumTypeAction
    {
        [Description("Auto")]
        Auto = 1,
        [Description("Manual")]
        Manual = 2
    }
    #endregion
    public enum TypeLoanBrief
    {
        [Description("Cá nhân")]
        CANHAN = 1,
        [Description("Doanh nghiệp")]
        DOANHNGHIEP = 2,
    }

    public enum SourceBank
    {
        [Description("NamA")]
        NAMA = 1,
        [Description("NCB")]
        NCB = 2,
        [Description("VIB")]
        VIB = 4
    }

    public enum TypeProvince
    {
        [Description("Thành phố")]
        ThanhPho = 1,
        [Description("Tỉnh")]
        Tinh = 2,
    }
    public enum TypeDistrict
    {
        [Description("Quận")]
        Quan = 1,
        [Description("Huyện")]
        Huyen = 2,
        [Description("Thị xã")]
        ThiXa = 5,
        [Description("Thành Phố")]
        ThanhPho = 4,
        [Description("Unknown")]
        Unknown = 6,
    }
    public enum TypeWard
    {
        [Description("Xã")]
        Xa = 1,
        [Description("Phường")]
        Phuong = 2,
        [Description("Thị trấn")]
        ThiTran = 3,
    }

    public enum NationalCardLength
    {
        [Description("Chứng minh nhân dân Quân Đội")]
        CMND_QD = 8,
        [Description("Chứng minh nhân dân")]
        CMND = 9,
        [Description("Căn cước công dân")]
        CCCD = 12
    }

    public enum EnumStatusLMS
    {
        [Description("Đang vay")]
        Disbursed = 11,
        [Description("Hôm nay phải trả")]
        PaymentNow = 12,
        [Description("Nợ lãi phí")]
        InterestFeeDebt = 13
    }

    public enum EnumReasonCancel
    {
        [Description("Hết thời gian quy định")]
        OverDay = 649,
        [Description("1.1.Không liên hệ được")]
        NoContact = 658,
        [Description("1.3.Không thuộc khu vực hỗ trợ")]
        NoAreaSupport = 660,
        [Description("1.4.Không có xe máy")]
        NoMotobike = 661,
    }
    public enum TypeSendOtp
    {
        [Description("Gửi sms brand name")]
        SmsBrandName = 1,
        [Description("Gửi sms voice otp")]
        VoiceOtp = 2,
        [Description("FPT gửi OTP")]
        FtpSendOtp = 3,
    }

    public enum VerifyOtp
    {
        [Description("Chờ verify")]
        WaitingVerify = 0,
        [Description("Đã xác thực")]
        Verified = 1,
    }

    public enum ActionPushNotification
    {
        [Description("Push notify app cvkd")]
        PushAppCvkd = 1,
        [Description("Push notify app lending online")]
        PushAppLendingOnline = 2,
    }

    public enum TypeNotification
    {
        [Description("Slack")]
        Slack = 1,
        [Description("Firebase")]
        FireBase = 2,
        [Description("Sms")]
        Sms = 3,
    }

    public enum FilterSourceTLS
    {
        [Description("Nguồn ReMaketing (Khởi tạo lại)")]
        form_cancel_rmkt = 1,
        [Description("Nguồn Finish Loan (Tái vay)")]
        rmkt_loan_finish = 2,
        [Description("Nguồn tự tạo")]
        SourceSelfCreated = 3,
        [Description("Nguồn Form mới từ MKT")]
        SourceFormNewMKT = 4,
        [Description("Nguồn Finish Loan")]
        rmkt_loan_finish_not_reborrow = 5
    }

    public enum TypeUploadWebTimaCare
    {
        [Description("CMND mặt trước")]
        CMND_FRONT = 1,
        [Description("CMND mặt sau")]
        CMND_BACK = 2
    }

    public enum EsignState
    {
        [Description("Sẵn sàng ký hợp đồng điện tử")]
        READY_TO_SIGN = 0,
        [Description("Người vay đã ký hợp đồng điện tử")]
        BORROWER_SIGNED = 1,
        [Description("Lender đã ký hợp đồng điện tử")]
        LENDER_SIGNED = 2
    }

    public enum TypeEsign
    {
        [Description("Khách hàng")]
        Customer = 1,
        [Description("Nhà đầu tư")]
        Lender = 2
    }

    public enum TypeOtp
    {
        [Description("Verify số điện thoại")]
        VerifyPhone = 1,
        [Description("Verify Esign Customer")]
        VerifyEsignCustomer = 2,
    }

    public enum ActionLogRestruct
    {
        [Description("Khoanh gốc")]
        KhoanhGoc = 1,
        [Description("Khoanh Phí Tư Vấn Chia đều vào các kỳ")]
        KhoanhPhiTuVanChiaDeuCacKy = 2,
        [Description("Khoanh phí Tư Vân chia vào kỳ cuối")]
        KhoanhPhiTuVanChiaVaoKyCuoi = 3,
        [Description("Khoanh phí tư vấn chọn ngày trả mới")]
        KhoanhPhiTuVanChonNgayTraMoi = 4,
        [Description("Miễn Giảm Lãi Phí")]
        MienGiamLaiPhi = 5,
        [Description("Khoanh gốc & phí tư vấn chia đề vào các kỳ")]
        KhoanhGocVaPhiTuVanChiaDeuCacKy = 6,
        [Description("Khoanh gốc & phí tư vấn vào kỳ cuối")]
        KhoanhGocVaPhiTuVanVaoKyCuoi = 7,
        [Description("Cấu trúc lại khoản vay")]
        CauTrucLaiKhoanVay = 10,
    }

    public enum EnumTeamTelesales
    {
        [Description("Hà nội 1")]
        HN1 = 1,
        [Description("Hà nội 2")]
        HN2 = 2,
        [Description("HCM")]
        HCM = 3,
        [Description("Other")]
        Other = 4,
        [Description("System Team")]
        SystemTeam = 5,
        [Description("Topup/Xsell")]
        TeamXsell = 6,
        [Description("Hà nội 3")]
        HN3 = 7,
        [Description("Xsell 1")]
        Xsell1 = 8,
        [Description("Xsell 2")]
        Xsell2 = 9,
        [Description("Xsell 3")]
        Xsell3 = 10,
    }

    public enum IsApplyAddress
    {
        [Description("Default")]
        Default = -1,
        [Description("Hỗ trợ")]
        Support = 1,
        [Description("Không hỗ trợ")]
        NoSupport = 0,
    }

    public enum TypePhone
    {
        [Description("Số điện thoại khác")]
        PhoneOther = 1,
        [Description("Số điện thoại đơn vay")]
        PhoneLoanbrief = 2,
        [Description("Số điện thoại người thân")]
        PhoneRelationship = 3
    }
    public enum EnumTypeLoanSearchBOD
    {
        [Description("Duyệt hủy đơn")]
        Duyet_huy_don = 1,
        [Description("Đơn xe máy ngoại lệ")]
        Xe_may_ngoai_le = 2,
        [Description("Đơn ô tô")]
        Oto = 3
    }

    public enum EnumBusinessFilter
    {
        [Description("Phê duyệt ngoại lệ xe máy")]
        XM = 1,
        [Description("Phê duyệt ô tô")]
        Oto = 2
    }

    public enum EnumTypeLoanSupport
    {
        [Description("Đơn có thể topup")]
        IsCanTopup = 1,
        [Description("Đơn DPD <= 8")]
        DPD = 2,
        [Description("Đơn có thể tái vay")]
        ReBorrow = 3,
    }

    public enum FilterTopupDisbursementContract
    {
        [Description("HĐ Topup")]
        Topup = 1,
        [Description("HĐ của TLS đã nghỉ")]
        TlsOff = 2,
        [Description("HĐ DPD <= 8")]
        DPD = 3,
        [Description("Đóng HĐ")]
        CloseLoan = 4,
    }

    public enum EnumTransactionState
    {
        [Description("Đang chờ duyệt")]
        DangChoDuyet = 0,
        [Description("Đã duyệt")]
        DaDuyet = 1,
        [Description("Hủy")]
        Huy = 2
    }
    public enum TypeRemarketingLtv
    {
        [Description("Topup")]
        TopUp = 1,
        [Description("Tái vay")]
        Reborrow = 2
    }
    public enum ActionPushLoanToPartner
    {
        [Description("Mới tạo")]
        CreateNew = 0,
        [Description("Tạo thành công bên đối tác")]
        CreatedSuccess = 1,
        [Description("Tạo thất bại bên đối tác")]
        CreatedError = -1,
    }
    public enum PartnerPushLoanToPartner
    {
        [Description("Mirae")]
        Mirae = 1,
    }
    public enum StatusPushLoanToPartner
    {
        [Description("Đã hủy")]
        Cancel = 0,
        [Description("Đã giải ngân")]
        Disbursement = 1,
        [Description("Đang xử lý")]
        Processing = 2,
    }

    public enum ActionCallApiPartner
    {
        [Description("Create Loan Mirae")]
        CreateLoanMirae = 1,
    }
    public enum FormatCallCisco
    {
        [Description("Format 1")]
        Format_1 = 711,
    }
    public enum EnumStatusPushToCisco
    {
        [Description("Thành công!")]
        Success = 0,
        [Description("Token không hợp lệ!")]
        ErrorToken = 1,
        [Description("Dữ liệu truyền vào không hợp lệ (token sai hoac so ngay tim kiem qua ̉̉60 ngay)")]
        ErrorParameter = 2,
        [Description("Exception")]
        Exception = 3,
        [Description("Không Thành công!")]
        Fail = 4,
        [Description("Tổng đài còn tồn chưa call hết!")]
        CiscoOver = 5
    }

    public enum LogCallApiCiscoActionId
    {
        [Description("Đăng nhập finesse")]
        LoginFinesse = 1
    }
public enum FilterLoanbriefForHub
    {
        [Description("Chờ tư vấn")] Wait_Support = 1,
        [Description("Chờ CHT duyệt")] Wait_CHT_Approve = 2,
        [Description("Chờ TĐHS xử lý")] Wait_Tdhs_Approve = 3,
        [Description("Chờ đẩy cho Lender")] Wait_Push_Lender = 4,
        [Description("Chờ giải ngân")] Wait_Push_Disburse = 5,
    }

    public enum TypeOfOwnerShipApi
    {
        [Description("KT1")]
        KT1 = 14,
        [Description("KT3")]
        KT3 = 15
    }

    public enum LoanProductSupport
    {
        [Description("Vay qua ĐK XMCC")]
        MotorCreditType_CC = 2,
        [Description("Vay qua ĐK XMKCC")]
        MotorCreditType_KCC = 5,
        [Description("Vay qua ĐK XMKGT")]
        MotorCreditType_KGT = 28,
        [Description("Đăng ký ô tô")]
        OtoCreditType_CC = 8,
    }

    public enum Gender
    {
        [Description("Nam")]
        Male = 0,
        [Description("Nữ")]
        Female = 1
    }

    public enum Merried
    {
        [Description("Chưa có")]
        Notyet = 0,
        [Description("Đã có")]
        Had = 1
    }
    public enum NumberBaby
    {
        [Description("Chưa có")]
        Notyet = 0,
        [Description("1 cháu")]
        One = 1,
        [Description("2 cháu")]
        Two = 2,
        [Description(">= 3cháu")]
        Three = 3
    }
    public enum LivingWith
    {
        [Description("Chung với bố mẹ")]
        Bome = 0,
        [Description("Riêng với vợ con")]
        Vocon = 1,
        [Description("Riêng một mình")]
        Motminh = 2
    }
    public enum EnumAppraiser
    {
        [Description("Location đạt")]
        Location = 1,
        [Description("Thẩm định nơi ở")]
        AppraiserHome = 2,
        [Description("Có hóa đơn điện/nước/internet/truyền hình cáp")]
        ElectricBill = 3
    }
}
