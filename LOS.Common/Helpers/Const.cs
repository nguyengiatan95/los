﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Helpers
{
	public class Const
	{
		public enum ProcessType
		{
			WAITING_FOR_PIPELINE = 0,
			IN_PROCESS_FOR_PIPELINE = 1,
			PIPELINE_COMPLETED = 9,
			WAITING_FOR_PROCESSING = 10,
			IN_PROCESS_FOR_SECTION = 11,
			WAITING_FOR_MANUAL_PROCESSING = 20,
			MANUAL_PROCCESSED = 21,
			WAITING_FOR_AUTOMATIC_PROCESSING = 30,
			AUTOMATIC_PROCESSED = 31,
			INVALID_ACTION_STATE = 50,
			INVALID_APPROVE_STATE = 51,
			INVALID_PIPELINE_STATE = 60,
			UNKNOWN_PIPELINE = 98,
			UNEXPECTED_ERROR = 99,
			PROCESSED = 100,
			MANUAL_PIPELINE = -1
		}

		public enum AutoDistributeState
		{
			READY_TO_DISTRIBUTE = 0,
			DISTRIBUTED = 1,
			LOCKED_DISTRIBUTING = 2,
			EXPIRED = 3,
			RETURNED = 4,
			LENDER_CARE_KT_RETURNED = 14,
			IGNORED = 5,
			RETRIEVED = 6,
			ALL_IGNORED = 10,
			REQUIREMENT_NOT_MET = 20,
			CONFLICT = 30,
			MANUAL_PUSH = 40,
			DISBURSED = 100
		}	

		public enum AutoDistributeCountType
		{
			RECEIVED = 1,
			ACCEPTED = 2,
			RETURNED = 3,
			RETRIEVED = 4
		}

		public static string url_tra_cuu = "https://tima.vn/tc?v={0}";

		public static double LENDER_RATE = 19;

		public static List<int> Document_Tham_Dinh_Nha_Tima_Care = new List<int>() { 437,438,439,440 };
		public static List<int> Document_Tham_Dinh_Cty_Tima_Care = new List<int>() { 441,442,443,444 };
		public static List<int> Document_Tham_Dinh_Nha = new List<int>() {445,446,447 };
		public static List<int> Document_Tham_Dinh_Cty = new List<int>() { 448,449,450};
	}
}
