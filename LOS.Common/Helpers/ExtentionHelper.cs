﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace LOS.Common.Helpers
{
    public static class ExtentionHelper
    {
        public static string GetDescription(Enum value)
        {
            try
            {
                var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
                var a = value.GetType().Name;
                return da.Length > 0 ? da[0].Description : value.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }
        public static string GetName(Enum value)
        {
            try
            {
                return value.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        public static List<int> ConvertToInt(string value)
        {
            try
            {
                return value.Split(",").Select(int.Parse).ToList();
            }
            catch
            {
                return null;
            }
        }
        public static DateTime FirstDayOfMonth(DateTime date)
        {
            var startOfMonth = new DateTime(date.Year, date.Month, 1);
            return startOfMonth;
        }
        public static DateTime LastDayOfMonth(DateTime date)
        {
            var startOfMonth = new DateTime(date.Year, date.Month, 1);
            var endOfMonth = startOfMonth.AddMonths(1).AddDays(-1);
            return endOfMonth;
        }


        public static IEnumerable<T> GetRandom<T>(this IEnumerable<T> list, int count)
        {
            if (count <= 0)
                yield break;
            var r = new Random();
            int limit = (count * 10);
            foreach (var item in list.OrderBy(x => r.Next(0, limit)).Take(count))
                yield return item;
        }
    }
}
