﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Models.Reply
{
	public class Company
	{
		public int CompanyId { get; set; }

		public string Name { get; set; }

		public string Address { get; set; }

		public int? ProvinceId { get; set; }

		public int? DistrictId { get; set; }

		public int? WardId { get; set; }

		public string Phone { get; set; }

		public int? ParentId { get; set; }

		public int? CreatorId { get; set; }

		public DateTimeOffset? CreatedTime { get; set; }

		public DateTimeOffset? UpdatedTime { get; set; }

		public int? Status { get; set; }

		public bool? IsAgency { get; set; }
	}
}
