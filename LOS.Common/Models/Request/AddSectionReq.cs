﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Models.Request
{
	public class AddSectionReq
	{
		public int pipelineId { get; set; }
		public int sectionId { get; set; }
	}
}
