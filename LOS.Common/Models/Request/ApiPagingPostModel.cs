﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LOS.Common.Models.Request
{
    [Serializable]
    public class ApiPagingPostModel
    {
        private string _keyword;
        public string Keyword
        {
            get { return (_keyword == null ? string.Empty : _keyword); }
            set { _keyword = value; }
        }


        [Range(0, int.MaxValue)]
        public int CurrentPage { get; set; } = 1;

        [Range(0, int.MaxValue)]
        public int PageSize { get; set; } = 10;

        private string _sortColumn;

        public string SortColumn
        {
            get { return (_sortColumn == null ? string.Empty : _sortColumn); }
            set { _sortColumn = value; }
        }


        public bool IsSortByAsc { get; set; }
    }
}
