﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Models.Request
{
	public class BoundTelesaleReq
	{
		public int userId { get; set; }
		public int loanBriefId { get; set; }
		public int ActionState { get; set; }
		public int PipelineState { get; set; }
        public int? TeamTelesaleId { get; set; }
    }

    public class RangeBoundTelesaleReq
    {
        public int userId { get; set; }
        public List<int> loanBriefIds { get; set; }
        public int ActionState { get; set; }
        public int PipelineState { get; set; }
        public int? TeamTelesaleId { get; set; }
    }
}
