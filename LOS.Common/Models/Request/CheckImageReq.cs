﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Models.Request
{
    public class CheckImageReq
    {
        public int LoanBriefId { get; set; }
        public int UserId { get; set; }
        public int TypeCheck { get; set; }
        public List<ResultCheckImage> LstCheckImage { get; set; }
    }
    public class ResultCheckImage
    {
        public bool ResultCheck { get; set; }
        public int DocumentType { get; set; }
        public string Description { get; set; }
    }
}
