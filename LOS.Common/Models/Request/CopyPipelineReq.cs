﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Models.Request
{
	public class CopyPipelineReq
	{
		public int id { get; set; }
		public string name { get; set; }
		public int creatorId { get; set; }
	}
}
