﻿namespace LOS.Common.Models.Request
{
    public class DataTableAjaxPostModel
    {
        public string Page { get; set; }
        public string Perpage { get; set; }
        public string Field { get; set; }
        public string Sort { get; set; }
    }
}
