﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Models.Request
{
    public class DocumentTypeReq
    {
        public int LoanBriefId { get; set; }
        public int UserId { get; set; }
        public int DocumentId { get; set; }
        public List<LstFileId> LstFileId { get; set; }
    }
    public class LstFileId
    {
        public int FileId { get; set; }
    }
}
