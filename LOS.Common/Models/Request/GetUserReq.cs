﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Models.Request
{
    public class GetUserReq
    {
        public int Type { get; set; }
        public List<int> lstIpphone { get; set; }
    }
}
