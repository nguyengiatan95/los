﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Models.Request
{
    public class IsUploadReq
    {
        public int loanbriefId { get; set; }
        public bool status { get; set; }
    }
}
