﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Models.Request
{
    public class LenderReq
    {
        public int lenderId { get; set; }
        public int loanBriefId { get; set; }
    }
}
