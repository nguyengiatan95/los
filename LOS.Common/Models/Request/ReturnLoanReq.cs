﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Common.Models.Request
{
    public class ReturnLoanReq
    {
        public int LoanBriefId { get; set; }
        public string Comment { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public List<Reason> LstReason { get; set; }
    }
    public class Reason
    {
        public int ReasonId { get; set; }
        public string ReasonName { get; set; }
    }

    public class ProposeExceptionReq
    {
        public int LoanBriefId { get; set; }
        public int UserId { get; set; }
        public int DocId { get; set; }
        public string DocName { get; set; }
        public int ChildDocId { get; set; }
        public string ChildDocName { get; set; }
        public string Note { get; set; }
        public Decimal Money { get; set; }
    }

}
