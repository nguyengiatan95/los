﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Models.Request
{
    public class ScheduleTimeHubReq
    {
        public int LoanBriefId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int UserId { get; set; }
        public int HubId { get; set; }
        public string Comment { get; set; }
        public string CusName { get; set; }
        public string UserFullName { get; set; }
        public int ScheduleId { get; set; }
        public int TypeSubmit { get; set; }
    }

    public class GetListScheduleTimeHubReq
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int UserId { get; set; }
        public int HubId { get; set; }
        public int GroupId { get; set; }
        public int FilterGroupId { get; set; }
    }

    public class CalendarScheduleTimeReq
    {
        public string ResourceId { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Title { get; set; }
        public string Color { get; set; }
        public string Url { get; set; }
        public int LoanBriefId { get; set; }
    }
}
