﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Models.Request
{
	public class ScheduleTimeReq
    {
        public int LoanBriefId { get; set; }
        public string DateTimeCall { get; set; }
        public string Comment { get; set; }
    }
}
