﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Common.Models.Request
{
	public class UpdateScheduleCallReq
	{
		public int loanActionId { get; set; }
		public int actionStatusId { get; set; }
		public string scheduleTime { get; set; }
	}
}
