﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Common.Models.Request
{
	public class UpdateSectionOrderReq
	{
		public List<SectionOrder> data { get; set; }
	}

	public class SectionOrder
	{
		public int id { get; set; }
		public int order { get; set; }
	}
}
