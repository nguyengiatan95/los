﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Utils
{
    public class MathUtils
    {
        public const decimal unit = 1000000;
        public static decimal RoundMoney(decimal number)
        {
            decimal value = 0L;
            if (number < unit)
                return 0;
            int Milion = (int)(number / unit);
            var Surplus = number % unit;
            value = (Milion * unit);
            if (Surplus >= 500000)
            {
                value = (Milion * unit) + unit;
            }
            return value;
        }
        public static decimal UnitMillion(decimal number)
        {
            if (number < unit)
                return 0;
            int million = (int)(number / unit);
            return (million * unit);
        }

    }
}
