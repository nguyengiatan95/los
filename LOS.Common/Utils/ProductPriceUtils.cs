﻿using LOS.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Common.Utils
{
    public class ProductPriceUtils
    {
        public static long GetMaxPrice(int ProductId, decimal originalCarPrice, int TypeOfOwnerShip, long totalDepreciation = 0)
        {
            var maxPriceProduct = 0L;
            if (ProductId == (int)EnumProductCredit.MotorCreditType_CC || ProductId == (int)EnumProductCredit.LoanFastMoto_CC) //xe cc
            {
                //70% giá trị định giá xe hiện tại
                maxPriceProduct = Convert.ToInt64(MathUtils.RoundMoney(originalCarPrice * 70 / 100));
                maxPriceProduct -= totalDepreciation;
                //nhà sở hữu => cho vay 70% k quá 30tr
                if (TypeOfOwnerShip == EnumTypeofownership.Sohuucanhan.GetHashCode() || TypeOfOwnerShip == EnumTypeofownership.Dongsohuu.GetHashCode()
                    || TypeOfOwnerShip == EnumTypeofownership.DungTenBoMe.GetHashCode() || TypeOfOwnerShip == EnumTypeofownership.DungTenVoChong.GetHashCode()
                    || TypeOfOwnerShip == EnumTypeofownership.NhaSoHuu.GetHashCode())
                {


                    if (maxPriceProduct > 30000000)
                        maxPriceProduct = 30000000;
                }
                //nhà thuê => cho vay 70% k quá 15tr
                else
                {
                    if (maxPriceProduct > 15000000)
                        maxPriceProduct = 15000000;
                }
            }
            else if (ProductId == (int)EnumProductCredit.MotorCreditType_KCC || ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
            {
                maxPriceProduct = Convert.ToInt64(MathUtils.RoundMoney(originalCarPrice * 70 / 100));
                maxPriceProduct -= totalDepreciation;
                //nhà sở hữu => cho vay 50% k quá 30tr
                if (TypeOfOwnerShip == EnumTypeofownership.Sohuucanhan.GetHashCode() || TypeOfOwnerShip == EnumTypeofownership.Dongsohuu.GetHashCode()
                     || TypeOfOwnerShip == EnumTypeofownership.DungTenBoMe.GetHashCode() || TypeOfOwnerShip == EnumTypeofownership.DungTenVoChong.GetHashCode()
                     || TypeOfOwnerShip == EnumTypeofownership.NhaSoHuu.GetHashCode())
                {
                    if (maxPriceProduct > 30000000)
                        maxPriceProduct = 30000000;
                }
                //nhà thuê => cho vay 50% k quá 10tr
                //01/11/2020 thay đổi định giá từ 10tr -> 15tr
                else
                {
                    if (maxPriceProduct > 15000000)
                        maxPriceProduct = 15000000;
                }
            }

            return maxPriceProduct;
        }


        /// <summary>
        /// Chuyển đổi thông tin công việc => Phân loai khách hàng (6 loại)
        /// </summary>
        /// <param name="TypeJob">Loại công việc: làm thuê, kinh doanh, lao động tự do</param>
        /// <param name="ImcomeType">HÌnh thức nhận lương</param>
        /// <param name="IsCompanyInsurance">Công ty có đóng  bảo hiểm không</param>
        /// <param name="BusinessPapers">Có giấy phé kinh doanh không</param>
        /// <returns></returns>
        public static int ConvertTypeDescriptionJob(int TypeJob, int? ImcomeType = null, bool? IsCompanyInsurance = null, bool? BusinessPapers = null)
        {
            if (TypeJob == EnumJobTitle.LamHuongLuong.GetHashCode())
            {
                if (ImcomeType == EnumImcomeType.ChuyenKhoan.GetHashCode())
                    return EnumWorkRuleType.LamThueLuongChuyenKhoan.GetHashCode();
                else if (IsCompanyInsurance == true)
                    return EnumWorkRuleType.LuongTienMatCoBHXH.GetHashCode();
                else if (IsCompanyInsurance == false)
                    return EnumWorkRuleType.LuongTienMatKhongBHXH.GetHashCode();
                return EnumWorkRuleType.LuongTienMatKhongBHXH.GetHashCode();
            }
            else if (TypeJob == EnumJobTitle.TuDoanh.GetHashCode())
            {
                if (BusinessPapers == true)
                    return EnumWorkRuleType.KinhDoanhCoDangKy.GetHashCode();
                else if (BusinessPapers == false)
                    return EnumWorkRuleType.KinhDoanhKhongDangKy.GetHashCode();
                return EnumWorkRuleType.KinhDoanhKhongDangKy.GetHashCode();
            }
            else if (TypeJob == EnumJobTitle.LamTuDo.GetHashCode())
            {
                return EnumWorkRuleType.LaoDongTuDo.GetHashCode();
            }
            if (TypeJob == EnumJobTitle.LamTaiXeCongNghe.GetHashCode())
            {
                return EnumWorkRuleType.GrabShipper.GetHashCode();
            }
            return EnumWorkRuleType.LaoDongTuDo.GetHashCode();
        }

        public static Tuple<decimal, double> GetCalculatorRate(int productId, decimal? loanAmount, int? loanTime, int? rateType)
        {
            decimal rateMoney = 2700;
            double ratePercent = 98.55;
            #region lãi xuất cũ
            //ô tô
            //if (productId == (int)EnumProductCredit.OtoCreditType_CC)
            //{
            //    if (loanAmount >= 30000000)
            //    {
            //        if (rateType == (int)EnumRateType.DuNoGiamDan || rateType == (int)EnumRateType.RateAmortization30Days)
            //        {
            //            if (loanTime == 3)
            //            {
            //                return Tuple.Create(Convert.ToDecimal(2100), 76.65);
            //            }
            //            else if (loanTime == 6)
            //            {
            //                return Tuple.Create(Convert.ToDecimal(1750), 63.88);
            //            }
            //            else if (loanTime == 9 || loanTime == 12)
            //            {
            //                return Tuple.Create(Convert.ToDecimal(1650), 60.23);
            //            }

            //        }
            //        else if (rateType == (int)EnumRateType.TatToanCuoiKy || rateType == (int)EnumRateType.RateMonthADay)
            //        {
            //            if (loanTime == 3)
            //            {
            //                return Tuple.Create(Convert.ToDecimal(2300), 83.95);
            //            }
            //            else if (loanTime == 6)
            //            {
            //                return Tuple.Create(Convert.ToDecimal(1850), 67.53);
            //            }
            //            else if (loanTime == 9 || loanTime == 12)
            //            {
            //                return Tuple.Create(Convert.ToDecimal(1700), 62.05);
            //            }
            //        }
            //    }
            //}
            //return Tuple.Create(rateMoney, ratePercent);
            #endregion

            if (productId == (int)EnumProductCredit.OtoCreditType_CC)
            {
                if (loanTime == 3)
                {
                    return Tuple.Create(Convert.ToDecimal(2300), 83.95);
                }
                else if (loanTime == 6)
                {
                    return Tuple.Create(Convert.ToDecimal(2000), 73.0);
                }
                else if (loanTime == 9 || loanTime == 12)
                {
                    return Tuple.Create(Convert.ToDecimal(1500), 54.75);
                }
            }
            return Tuple.Create(rateMoney, ratePercent);
        }

        public static int ConvertEnumTypeOfOwnerShipDocument(int TypeOfOwnerShip)
        {
            if (TypeOfOwnerShip == EnumTypeofownership.Sohuucanhan.GetHashCode()
                || TypeOfOwnerShip == EnumTypeofownership.Dongsohuu.GetHashCode()
                  || TypeOfOwnerShip == EnumTypeofownership.DungTenBoMe.GetHashCode()
                  || TypeOfOwnerShip == EnumTypeofownership.DungTenVoChong.GetHashCode()
                  || TypeOfOwnerShip == EnumTypeofownership.NhaSoHuu.GetHashCode()
                  || TypeOfOwnerShip == EnumTypeofownership.NhaSoHuuTrungSHK.GetHashCode()
                  || TypeOfOwnerShip == EnumTypeofownership.NhaSoHuuKhongTrungSHK.GetHashCode()
                  || TypeOfOwnerShip == EnumTypeofownership.NhaThueCungTinh.GetHashCode()
                  || TypeOfOwnerShip == EnumTypeofownership.KT1.GetHashCode())
            {
                return (int)EnumTypeOfOwnerShipDocument.NhaSoHuu;
            }
            return (int)EnumTypeOfOwnerShipDocument.NhaThue;
        }
        /// <summary>
        /// Lấy ra số tiền cho vay tối đa V1.4
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="originalCarPrice"></param>
        /// <param name="typeOfOwnerShip"></param>
        /// <param name="productDetailId"></param>
        /// <param name="maxPriceProductDetail"></param>
        /// <param name="totalDepreciation"></param>
        /// <returns></returns>
        public static long GetMaxPriceV3(int productId, decimal originalCarPrice, int typeOfOwnerShip, int productDetailId = 0,
            long maxPriceProductDetail = 0, long totalDepreciation = 0, int typeRemarketingLtv = 0, int ltv = 0)
        {
            var maxPriceProduct = 0L;
            var percentLtv = 0;
            if (ltv > 0)
                percentLtv = ltv;
            else
                percentLtv = GetPercentLtv(productId, typeRemarketingLtv);
            maxPriceProduct = Convert.ToInt64(MathUtils.RoundMoney(originalCarPrice * percentLtv / 100));
            maxPriceProduct -= totalDepreciation;
            var maxPrice = maxPriceProductDetail > 0 ? maxPriceProductDetail : MaxPrice(typeOfOwnerShip, productDetailId);
            if (typeRemarketingLtv == (int)TypeRemarketingLtv.TopUp)
            {
                var limitedLoanAmount = LimitedLoanAmount(productId, typeOfOwnerShip);
                if (maxPrice > limitedLoanAmount)
                    maxPrice = limitedLoanAmount;
            }
            if (maxPriceProduct > maxPrice)
                maxPriceProduct = maxPrice;
            return maxPriceProduct;
        }

        public static long GetMaxPriceLtv(int productId, decimal originalCarPrice, int typeRemarketingLtv = 0, int ltv = 0)
        {
            var percentLtv = 0;
            if (ltv > 0)
                percentLtv = ltv;
            else
                percentLtv = GetPercentLtv(productId, typeRemarketingLtv);
            return Convert.ToInt64(MathUtils.RoundMoney(originalCarPrice * percentLtv / 100));
        }

        public static int GetPercentLtv(int productId, int typeRemarketingLtv = 0)
        {
            var percentLtv = 60;
            if (productId == (int)EnumProductCredit.MotorCreditType_CC)
            {
                percentLtv = 70;
                if (typeRemarketingLtv == (int)TypeRemarketingLtv.TopUp)
                    percentLtv = 90;
                else if (typeRemarketingLtv == (int)TypeRemarketingLtv.Reborrow)
                    percentLtv = 90;
            }
            else if (productId == (int)EnumProductCredit.MotorCreditType_KCC || productId == (int)EnumProductCredit.MotorCreditType_KGT)
            {
                percentLtv = 60;
                if (typeRemarketingLtv == (int)TypeRemarketingLtv.TopUp)
                    percentLtv = 80;
                else if (typeRemarketingLtv == (int)TypeRemarketingLtv.Reborrow)
                    percentLtv = 80;
            }
            return percentLtv;
        }

        private static long MaxPrice(int typeOfOwnerShip, int infomationProductDetailId = 0)
        {
            if (infomationProductDetailId > 0)
            {
                if (infomationProductDetailId == (int)InfomationProductDetail.VayNhanh4tr
                    || infomationProductDetailId == (int)InfomationProductDetail.KT1_KSHK_4tr
                    || infomationProductDetailId == (int)InfomationProductDetail.KT1_KGT_4tr
                    || infomationProductDetailId == (int)InfomationProductDetail.KT3_KTN_4tr
                     || infomationProductDetailId == (int)InfomationProductDetail.KT3_KGT_4tr)
                    return 4000000;
                else if (infomationProductDetailId == (int)InfomationProductDetail.KT1_10
                    || infomationProductDetailId == (int)InfomationProductDetail.KT1_KGT_10
                    || infomationProductDetailId == (int)InfomationProductDetail.KT3_KGT_10
                    || infomationProductDetailId == (int)InfomationProductDetail.KT3_10)
                    return 10000000;
                else if (infomationProductDetailId == (int)InfomationProductDetail.KT1_20
                    || infomationProductDetailId == (int)InfomationProductDetail.KT1_KGT_20)
                    return 20000000;
                else if (infomationProductDetailId == (int)InfomationProductDetail.KT1_30)
                    return 30000000;
                else if (infomationProductDetailId == (int)InfomationProductDetail.KT3_15
                    || infomationProductDetailId == (int)InfomationProductDetail.KT1_KGT_15
                     || infomationProductDetailId == (int)InfomationProductDetail.KT3_KGT_15)
                    return 15000000;
                else if (infomationProductDetailId == (int)InfomationProductDetail.KT3_20)
                    return 20000000;
                else if (infomationProductDetailId == (int)InfomationProductDetail.LO_KT1_KSHK_5tr ||
                    infomationProductDetailId == (int)InfomationProductDetail.LO_KT3_KTN_5tr ||
                    infomationProductDetailId == (int)InfomationProductDetail.XMCC_KT1_5tr ||
                    infomationProductDetailId == (int)InfomationProductDetail.XMCC_KT3_5tr)
                    return 5000000;
                else if (infomationProductDetailId == (int)InfomationProductDetail.XMCC_KT1_12 ||
                    infomationProductDetailId == (int)InfomationProductDetail.XMCC_KT3_12 ||
                    infomationProductDetailId == (int)InfomationProductDetail.LO_KT1_12tr ||
                    infomationProductDetailId == (int)InfomationProductDetail.LO_KT1_KSHK_12tr ||
                    infomationProductDetailId == (int)InfomationProductDetail.KT1_KSHK_12tr)
                    return 12000000;
                else if (infomationProductDetailId == (int)InfomationProductDetail.XMCC_KT1_22 ||
                   infomationProductDetailId == (int)InfomationProductDetail.XMCC_KT3_22)
                    return 22000000;
                else if (infomationProductDetailId == (int)InfomationProductDetail.XMCC_KT1_32 ||
                   infomationProductDetailId == (int)InfomationProductDetail.XMCC_KT3_32)
                    return 32000000;
                else if (infomationProductDetailId == (int)InfomationProductDetail.XMCC_KT1_42)
                    return 42000000;
            }
            else
            {
                //Nếu chưa chọn chi tiết gói vay 
                if (ConvertTypeOfOwnerShip(typeOfOwnerShip) == (int)EnumTypeofownership.KT1)
                    return 30000000;
                else if (ConvertTypeOfOwnerShip(typeOfOwnerShip) == (int)EnumTypeofownership.KT3)
                    return 20000000;
            }
            return 10000000;
        }

        public static int ConvertTypeOfOwnerShip(int typeOfOwnerShip)
        {
            if (typeOfOwnerShip == EnumTypeofownership.Sohuucanhan.GetHashCode()
                || typeOfOwnerShip == EnumTypeofownership.Dongsohuu.GetHashCode()
                    || typeOfOwnerShip == EnumTypeofownership.DungTenBoMe.GetHashCode()
                    || typeOfOwnerShip == EnumTypeofownership.DungTenVoChong.GetHashCode()
                    || typeOfOwnerShip == EnumTypeofownership.NhaSoHuu.GetHashCode()
                    || typeOfOwnerShip == EnumTypeofownership.NhaThueCungTinh.GetHashCode()
                    || typeOfOwnerShip == EnumTypeofownership.NhaSoHuuTrungSHK.GetHashCode()
                    || typeOfOwnerShip == EnumTypeofownership.NhaSoHuuKhongTrungSHK.GetHashCode()
                    || typeOfOwnerShip == EnumTypeofownership.KT1.GetHashCode()
                    )
            {
                return (int)EnumTypeofownership.KT1;
            }
            return (int)EnumTypeofownership.KT3;
        }

        public static int ConvertProductDetail(int typeOfOwnerShip)
        {
            if (ConvertTypeOfOwnerShip(typeOfOwnerShip) == (int)EnumTypeofownership.KT1)
            {
                return (int)InfomationProductDetail.KT1_30;
            }
            return (int)InfomationProductDetail.KT3_20;
        }

        public static decimal GetFeePaymentBeforeLoan(int productId, int? loanTime)
        {
            decimal feePaymentBeforeLoan = 0.08M;
            if (productId == (int)EnumProductCredit.OtoCreditType_CC)
            {
                if (loanTime == 9 || loanTime == 12)
                    feePaymentBeforeLoan = 0.08M;
                else
                    feePaymentBeforeLoan = 0.05M;
            }
            return feePaymentBeforeLoan;
        }

        public static long GetMaxPriceTopup(int productId, decimal originalCarPrice, long totalMoneyDebtCurrent = 0, int typeOfOwnerShip = 0)
        {
            var maxPriceProduct = 0L;
            if (productId == (int)EnumProductCredit.MotorCreditType_CC || productId == (int)EnumProductCredit.LoanFastMoto_CC) //xe cc
            {
                maxPriceProduct = Convert.ToInt64(originalCarPrice * 90 / 100);
                maxPriceProduct -= totalMoneyDebtCurrent;

            }
            else if (productId == (int)EnumProductCredit.MotorCreditType_KCC || productId == (int)EnumProductCredit.MotorCreditType_KGT)
            {
                maxPriceProduct = Convert.ToInt64(originalCarPrice * 80 / 100);
                maxPriceProduct -= totalMoneyDebtCurrent;
            }
            if(typeOfOwnerShip > 0)
            {
                var limitedLoanAmount = LimitedLoanAmount(productId, typeOfOwnerShip);
                if (maxPriceProduct > limitedLoanAmount)
                    maxPriceProduct = limitedLoanAmount;
            }           
            return Convert.ToInt64(MathUtils.RoundMoney(maxPriceProduct));
        }

        public static long LimitedLoanAmount(int productId, int typeOfOwnerShip)
        {
            var finalTypeOfOwnerShip = ConvertTypeOfOwnerShip(typeOfOwnerShip);
            if (finalTypeOfOwnerShip == (int)EnumTypeofownership.KT1)
            {
                if (productId == (int)EnumProductCredit.MotorCreditType_CC) //xe cc
                    return 42000000;
                else if (productId == (int)EnumProductCredit.MotorCreditType_KCC)
                    return 30000000;
                else
                    return 20000000;
            }
            else
            {
                if (productId == (int)EnumProductCredit.MotorCreditType_CC) //xe cc
                    return 32000000;
                else if (productId == (int)EnumProductCredit.MotorCreditType_KCC)
                    return 20000000;
                else
                    return 15000000;
            }
        }

    }

}
