﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using LOS.DAL.EntityFramework;
using Action = LOS.DAL.EntityFramework.Action;

namespace LOS.DAL.DTOs
{
	public class ActionDTO
	{
		public int ActionId { get; set; }
		public string ActionCode { get; set; }
		public string Description { get; set; }
	}

	public class ActionDetail : ActionDTO
	{
		public static Expression<Func<Action, ActionDetail>> ProjectionDetail
		{
			get
			{
				return x => new ActionDetail()
				{
					ActionCode = x.ActionCode,
					ActionId = x.ActionId,
					Description = x.Description					
				};
			}
		}
	}
}
