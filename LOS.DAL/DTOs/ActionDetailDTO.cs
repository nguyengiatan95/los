﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class ActionDetailDTO
	{
		public int ActionDetailId { get; set; }
		public int? ActionId { get; set; }
		public int? PropertyId { get; set; }
		public string Operator { get; set; }
		public string Value { get; set; }
	}
}
