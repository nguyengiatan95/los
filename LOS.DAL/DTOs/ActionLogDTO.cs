﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class ActionLogDTO
	{
		public int ActionLogId { get; set; }
		public int? ActorId { get; set; }
		public string ActionName { get; set; }
		public string Input { get; set; }
		public string Output { get; set; }
		public int? Result { get; set; }
		public DateTimeOffset? CreatedTime { get; set; }
	}
}
