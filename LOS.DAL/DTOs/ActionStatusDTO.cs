﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using LOS.DAL.EntityFramework;
using Action = LOS.DAL.EntityFramework.Action;

namespace LOS.DAL.DTOs
{
	public class ActionStatusDTO
	{
		public int ActionStatusId { get; set; }
		public int? ActionId { get; set; }
		public string Action { get; set; }
		public string Description { get; set; }
	}
}
