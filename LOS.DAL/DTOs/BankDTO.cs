﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class BankDTO
    {
        public int BankId { get; set; }
        public string Name { get; set; }
        public string BankCode { get; set; }
        public string NapasCode { get; set; }
        public int? Type { get; set; }
        public int? BankValue { get; set; }
    }

    public class BankDetail : BankDTO
    {
        public static Expression<Func<Bank, BankDetail>> ProjectionDetail
        {
            get
            {
                return x => new BankDetail()
                {
                    BankId = x.BankId,
                    Name = x.Name,
                    BankCode = x.BankCode,
                    NapasCode = x.NapasCode,
                    Type = x.Type,
                    BankValue = x.BankValue
                };
            }
        }
    }

    public class BankLendingOnline
    {
        public int BankId { get; set; }
        public string Name { get; set; }
        public string BankCode { get; set; }
        public static Expression<Func<Bank, BankLendingOnline>> ProjectionDetail
        {
            get
            {
                return x => new BankLendingOnline()
                {
                    BankId = x.BankId,
                    Name = x.Name,
                    BankCode = x.BankCode,
                };
            }
        }
    }
}
