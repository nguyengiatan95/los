﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class BlackListDTO
	{
		public int BlackListId { get; set; }
		public string FullName { get; set; }
		public string NationalCard { get; set; }
		public string Phone { get; set; }
		public int? CreatorId { get; set; }
		public int? Status { get; set; }
		public DateTimeOffset? CreatedTime { get; set; }
		public DateTimeOffset? UpdatedTime { get; set; }
		public string Note { get; set; }
	}
}
