﻿using LOS.DAL.EntityFramework;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    [ProtoContract]
    public class BrandProductDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
    }

    public class BrandProductDetail : BrandProductDTO
    {
        public static Expression<Func<BrandProduct, BrandProductDetail>> ProjectionDetail
        {
            get
            {
                return x => new BrandProductDetail()
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsEnable = x.IsEnable
                };
            }
        }
    }
}
