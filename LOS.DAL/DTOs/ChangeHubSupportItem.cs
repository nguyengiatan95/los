﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
   public class ChangeHubSupportItem
    {
        public List<int> loanbriefIds { get; set; }
        public int HubId { get; set; }
        public int? HubEmployee { get; set; }

        public int LoanStatus { get; set; }
    }
}
