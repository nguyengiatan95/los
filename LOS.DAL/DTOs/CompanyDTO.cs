﻿using LOS.DAL.EntityFramework;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
	[ProtoContract]
	public class CompanyDTO
	{
		
		public int CompanyId { get; set; }
		
		public string Name { get; set; }
		
		public string Address { get; set; }
		
		public int? ProvinceId { get; set; }
		
		public int? DistrictId { get; set; }
		
		public int? WardId { get; set; }
		
		public string Phone { get; set; }
		
		public int? ParentId { get; set; }
		
		public int? CreatorId { get; set; }
		
		public DateTimeOffset? CreatedTime { get; set; }
	
		public DateTimeOffset? UpdatedTime { get; set; }
	
		public int? Status { get; set; }

		public bool? IsAgency { get; set; }
	}

	public class CompanyDetail : CompanyDTO
	{
		public static Expression<Func<Company, CompanyDetail>> ProjectionDetail
		{
			get
			{
				return x => new CompanyDetail()
				{
					Address = x.Address,
					CompanyId = x.CompanyId,
					CreatedTime = x.CreatedTime,
					CreatorId = x.CreatorId,
					DistrictId = x.DistrictId,
					IsAgency = x.IsAgency,
					Name = x.Name,
					ParentId = x.ParentId,
					Phone = x.Phone,
					ProvinceId = x.ProvinceId,
					Status = x.Status,
					UpdatedTime = x.UpdatedTime,
					WardId = x.WardId
				};
			}
		}
	}
}
