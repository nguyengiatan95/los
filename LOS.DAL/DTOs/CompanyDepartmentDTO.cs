﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class CompanyDepartmentDTO
	{
		public int CompanyId { get; set; }
		public int DepartmentId { get; set; }
	}
}
