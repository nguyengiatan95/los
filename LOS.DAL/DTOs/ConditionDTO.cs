﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class ConditionDTO
	{
		public int ConditionId { get; set; }
		public string Name { get; set; }
		public int? Status { get; set; }
		public DateTimeOffset? CreatedTime { get; set; }
		public DateTimeOffset? UpdatedTime { get; set; }
		public int? Priority { get; set; }
	}
}
