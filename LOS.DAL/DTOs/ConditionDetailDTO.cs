﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class ConditionDetailDTO
	{
		public int ConditionDetailId { get; set; }
		public int? ConditionId { get; set; }
		public int? PropertyId { get; set; }
		public string Operator { get; set; }
		public string Value { get; set; }
	}
}
