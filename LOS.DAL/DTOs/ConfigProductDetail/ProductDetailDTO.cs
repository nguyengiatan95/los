﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class ProductCreditDetailDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public long? MaxMoney { get; set; }
    }

    public class ConfigProductCreditDetail
    {
        public int Id { get; set; }
        public int ProductDetailId { get; set; }
        public string Name { get; set; }
        public long? MaxMoney { get; set; }
    }
}
