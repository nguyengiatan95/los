﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class ConfigProductDetailDTO : ConfigProductDetail
    {
        public static Expression<Func<ConfigProductDetail, ConfigProductDetailDTO>> ProjectionViewDetail
        {
            get
            {
                return x => new ConfigProductDetailDTO()
                {
                    ConfigProductDetailId = x.ConfigProductDetailId,
                    InfomationProductDetailId = x.InfomationProductDetailId,
                    ProductId = x.ProductId,
                    TypeOwnership = x.TypeOwnership,
                    Description = x.Description,
                    Appraiser = x.Appraiser,
                    JobDocument = x.JobDocument,
                    MaxMoney = x.MaxMoney,
                    InfomationProductDetail = x.InfomationProductDetail ?? null,
                };
            }
        }
    }

    public class ProductDetailModel
    {
        public int ConfigProductDetailId { get; set; }
        public int? InfomationProductDetailId { get; set; }
        public int? ProductId { get; set; }
        public int? TypeOwnership { get; set; }
        public string Description { get; set; }
        public string Appraiser { get; set; }
        public string JobDocument { get; set; }
        public long? MaxMoney { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int? Priority { get; set; }
        public bool? IsEnable { get; set; }
        public static Expression<Func<ConfigProductDetail, ProductDetailModel>> ProjectionViewDetail
        {
            get
            {
                return x => new ProductDetailModel()
                {
                    ConfigProductDetailId = x.ConfigProductDetailId,
                    InfomationProductDetailId = x.InfomationProductDetailId,
                    ProductId = x.ProductId,
                    TypeOwnership = x.TypeOwnership,
                    Description = x.Description,
                    Appraiser = x.Appraiser,
                    JobDocument = x.JobDocument,
                    MaxMoney = x.MaxMoney,
                    Id = x.InfomationProductDetail != null? x.InfomationProductDetail.Id : 0,
                    Name = x.InfomationProductDetail != null? x.InfomationProductDetail.Name : "",
                    Priority = x.InfomationProductDetail != null? x.InfomationProductDetail.Priority : 0,
                    IsEnable = x.InfomationProductDetail != null? x.InfomationProductDetail.IsEnable : null
                };
            }
        }
    }
}
