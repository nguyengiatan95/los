﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using LOS.Common.Extensions;
using System.Linq;

namespace LOS.DAL.DTOs
{
    public class ContactCustomerDTO
    {
        public int ContactId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public int? JobId { get; set; }
        public decimal? Income { get; set; }
        public bool? HaveCarsOrNot { get; set; }
        public int? BrandProductId { get; set; }
        public int? ProductId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? LoanBriefId { get; set; }
        public int? Status { get; set; }
    }
    public static class ContactCustomerDTOExtions
    {
        public static ContactCustomer Mapping(this ContactCustomerDTO dto)
        {
            var contactcustomer = new ContactCustomer();
            contactcustomer.ContactId = dto.ContactId;
            contactcustomer.FullName = dto.FullName;
            contactcustomer.ProvinceId = dto.ProvinceId;
            contactcustomer.DistrictId = dto.DistrictId;
            contactcustomer.WardId = dto.WardId;
            contactcustomer.JobId = dto.JobId;
            contactcustomer.FullName = dto.FullName;
            contactcustomer.Income = dto.Income;
            contactcustomer.HaveCarsOrNot = dto.HaveCarsOrNot;
            contactcustomer.BrandProductId = dto.BrandProductId;
            contactcustomer.ProductId = dto.ProductId;
            contactcustomer.CreateDate = dto.CreateDate;
            contactcustomer.Phone = dto.Phone;
            contactcustomer.CreateBy = dto.CreateBy;
            contactcustomer.ModifyDate = dto.ModifyDate;
            contactcustomer.LoanBriefId = dto.LoanBriefId;
            contactcustomer.Status = dto.Status;
            return contactcustomer;
        }
    }
    public class ContactCustomerDetails : ContactCustomerDTO
    {
        public Province Province { get; set; }
        public District District { get; set; }
        public Ward Ward { get; set; }
        public Job Job { get; set; }
        public BrandProduct BrandProduct { get; set; }
        public Product Product { get; set; }

        public static Expression<Func<ContactCustomer, ContactCustomerDetails>> ProjectionDetail
        {
            get
            {
                return x => new ContactCustomerDetails()
                {
                    ContactId = x.ContactId,
                    Phone = x.Phone,
                    FullName = x.FullName,
                    Income = x.Income,
                    HaveCarsOrNot = x.HaveCarsOrNot,
                    CreateDate = x.CreateDate,
                    ProvinceId = x.ProvinceId,
                    DistrictId = x.DistrictId,
                    WardId = x.WardId,
                    JobId = x.JobId,
                    BrandProductId = x.BrandProductId,
                    ProductId = x.ProductId,
                    CreateBy = x.CreateBy,
                    ModifyDate = x.ModifyDate,
                    LoanBriefId = x.LoanBriefId,
                    Status = x.Status,
                    Province = x.ProvinceId != null ? new Province()
                    {
                        ProvinceId = x.Province.ProvinceId,
                        Name = x.Province.Name
                    } : null,
                    District = x.DistrictId != null ? new District()
                    {
                        DistrictId = x.District.DistrictId,
                        Name = x.District.Name
                    } : null,
                    Ward = x.WardId != null ? new Ward()
                    {
                        WardId = x.Ward.WardId,
                        Name = x.Ward.Name
                    } : null,
                    Job = x.JobId != null ? new Job()
                    {
                        JobId = x.Job.JobId,
                        Name = x.Job.Name
                    } : null,
                    BrandProduct = x.BrandProductId != null ? new BrandProduct()
                    {
                        Id = x.BrandProduct.Id,
                        Name = x.BrandProduct.Name
                    } : null,
                    Product = x.ProductId != null ? new Product()
                    {
                        Id = x.Product.Id,
                        Name = x.Product.Name,
                        FullName = x.FullName
                    } : null
                };
            }
        }

        public bool IsCheckCreateLoanbrief
        {
            get
            {
                if (!string.IsNullOrEmpty(Phone) && (HaveCarsOrNot == true)
                    && (LoanBriefId == null || LoanBriefId <= 0)
                    && (ProvinceId != null)
                    && (Enum.GetValues(typeof(LOS.Common.Extensions.EnumProvince)).Cast<LOS.Common.Extensions.EnumProvince>().Select(c => (int)c).Contains((int)ProvinceId)))
                {
                    return true;
                }
                return false;
            }
        }
    }

    public class ContactCustomerDetailsApi : ContactCustomerDTO
    {
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public string JobName { get; set; }
        public string BrandProductName { get; set; }
        public string ProductName { get; set; }
        public static Expression<Func<ContactCustomer, ContactCustomerDetailsApi>> ProjectionDetail
        {
            get
            {
                return x => new ContactCustomerDetailsApi()
                {
                    ContactId = x.ContactId,
                    Phone = x.Phone,
                    FullName = x.FullName,
                    Income = x.Income,
                    HaveCarsOrNot = x.HaveCarsOrNot,
                    CreateDate = x.CreateDate,
                    ProvinceId = x.ProvinceId,
                    DistrictId = x.DistrictId,
                    WardId = x.WardId,
                    JobId = x.JobId,
                    BrandProductId = x.BrandProductId,
                    ProductId = x.ProductId,
                    CreateBy = x.CreateBy,
                    ModifyDate = x.ModifyDate,
                    ProvinceName = x.Province == null ? "" : x.Province.Name,
                    DistrictName = x.District == null ? "" : x.District.Name,
                    WardName = x.Ward == null ? "" : x.Ward.Name,
                    JobName = x.Job == null ? "" : x.Job.Name,
                    BrandProductName = x.BrandProduct == null ? "" : x.BrandProduct.Name,
                    ProductName = x.Product == null ? "" : x.Product.Name
                };
            }
        }
    }
}
