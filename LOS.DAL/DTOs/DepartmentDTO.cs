﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class DepartmentDTO
	{
		public int DepartmentId { get; set; }
		public string Name { get; set; }
		public int? Status { get; set; }
		public DateTimeOffset? CreatedTime { get; set; }
		public DateTimeOffset? UpdatedTime { get; set; }
	}

	public class DepartmentDetail : DepartmentDTO
	{
		public static Expression<Func<Department, DepartmentDetail>> ProjectionDetail
		{
			get
			{
				return x => new DepartmentDetail()
				{
					CreatedTime = x.CreatedTime,
					DepartmentId = x.DepartmentId,
					Name = x.Name,
					Status = x.Status,
					UpdatedTime = x.UpdatedTime
				};
			}
		}
	}
}
