﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class ProvinceLendingOnline
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public static Expression<Func<Province, ProvinceLendingOnline>> ProjectionDetail
        {
            get
            {
                return x => new ProvinceLendingOnline()
                {
                    Id = x.ProvinceId,
                    Name = x.Type != null ? Common.Extensions.Description.GetDescription((Common.Extensions.TypeProvince)x.Type) + " " + x.Name : x.Name,
                };
            }
        }
    }
    public class DistrictLendingOnline
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public static Expression<Func<District, DistrictLendingOnline>> ProjectionDetail
        {
            get
            {
                return x => new DistrictLendingOnline()
                {
                    Id = x.DistrictId,
                    Name = x.Type != null ? Common.Extensions.Description.GetDescription((Common.Extensions.TypeDistrict)x.Type) + " " + x.Name : x.Name,
                };
            }
        }
    }
    public class WardLendingOnline
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public static Expression<Func<Ward, WardLendingOnline>> ProjectionDetail
        {
            get
            {
                return x => new WardLendingOnline()
                {
                    Id = x.WardId,
                    Name = x.Type != null ? Common.Extensions.Description.GetDescription((Common.Extensions.TypeWard)x.Type) + " " + x.Name : x.Name,
                };
            }
        }
    }
}
