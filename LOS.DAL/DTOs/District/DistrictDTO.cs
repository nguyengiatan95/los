﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class DistrictDTO
    {
        public int DistrictId { get; set; }
        public int? ProvinceId { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? Priority { get; set; }
        public int? IsApply { get; set; }
        public string LatLong { get; set; }
    }

    public class DistrictDetail : DistrictDTO
    {
        public static Expression<Func<District, DistrictDetail>> ProjectionDetail
        {
            get
            {
                return x => new DistrictDetail()
                {
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    Name = x.Name,
                    Priority = x.Priority,
                    ProvinceId = x.ProvinceId,
                    Type = x.Type,
                    DistrictId = x.DistrictId,
                    IsApply = x.IsApply,
                    LatLong = x.LatLong
                };
            }
        }
    }
}
