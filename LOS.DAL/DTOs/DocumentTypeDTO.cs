﻿using LOS.Common.Helpers;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class DocumentTypeDTO
	{
        public int Id { get; set; }
        public string Name { get; set; }
        public byte? IsEnable { get; set; }
        public int? ProductId { get; set; }
        public int? NumberImage { get; set; }
        public string Guide { get; set; }

        public string Loanproduct { get; set; }
       
        public int? ParentId { get; set; }
        public int? TypeOwnerShip { get; set; }
        public string ObjectRuleType { get; set; }
        public int? Version { get; set; }
        public DateTime? CreatedTime { get; set; }

        public int? MobileUpload { get; set; }
        public bool? Required { get; set; }

    }

    public class DocumentTypeDTOV2
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte? IsEnable { get; set; }
        public int? ProductId { get; set; }
        public int? NumberImage { get; set; }
        public string Guide { get; set; }

        public string Loanproduct { get; set; }

        public int? ParentId { get; set; }
        public int? TypeOwnerShip { get; set; }
        public string ObjectRuleType { get; set; }
        public int? Version { get; set; }
        public DateTime? CreatedTime { get; set; }

        public int? MobileUpload { get; set; }
        public bool? Required { get; set; }
        public List<LoanBriefFileDetail> LstLoanBriefFiles { get; set; }

    }

    public class DocumentTypeDetail : DocumentTypeDTO
    {
        public static Expression<Func<DocumentType, DocumentTypeDetail>> ProjectionDetail
        {
            get
            {
                return x => new DocumentTypeDetail()
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsEnable = x.IsEnable,
                    ProductId = x.ProductId,
                    NumberImage = x.NumberImage,
                    Guide = x.Guide,
                    Loanproduct = x.Product != null ? x.Product.Name : "",
                   
                    ParentId = x.ParentId,
                    TypeOwnerShip = x.TypeOwnerShip,
                    ObjectRuleType = x.ObjectRuleType,
                    Version = x.Version,
                    CreatedTime = x.CreatedTime,
                    MobileUpload = x.MobileUpload,
                    Required = x.Required
                };
            }
        }
    }

    public class DocumentTypeNew
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? NumberImage { get; set; }
        public string Guide { get; set; }
        public int? ParentId { get; set; }

        public bool? Required { get; set; }
        public bool? RequiredNew { get; set; }

        public List<DocumentTypeDTO> LstDocumentChild { get; set; }
    }

    public class DocumentTypeNewV2
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? NumberImage { get; set; }
        public string Guide { get; set; }
        public int? ParentId { get; set; }

        public bool? Required { get; set; }
        public List<DocumentTypeDTOV2> LstDocumentChild { get; set; }
        public List<LoanBriefFileDetail> LstLoanBriefFiles { get; set; }
    }

    public class DocumentMapConfig
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Guide { get; set; }
        public int? ParentId { get; set; }
        public bool? IsChecked { get; set; }
        public List<int> ListGroupJobId { get; set; }
        public List<int> ListAllowFromUpload { get; set; }
        public ConfigDocument ConfigDocument { get; set; }
        public List<DocumentMapConfig> LstDocumentChild { get; set; }
    }

    public class DocumentChild
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte? IsEnable { get; set; }    
        public string Guide { get; set; }
        public int? ParentId { get; set; }
        public DateTime? CreatedTime { get; set; }
        public ConfigDocument ConfigDocument { get; set; }

    }

    public class DocumentMapInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Guide { get; set; }
        public int? ParentId { get; set; }
        public int? ProductId { get; set; }
        public int? NumberImage { get; set; }
        public bool? Required { get; set; }
        public int? TypeRequired { get; set; }
        public string AllowFromUpload { get; set; }
        public string GroupJobId { get; set; }
        public int? Sort { get; set; }
        public List<DocumentMapInfo> ListChilds { get; set; }
        public List<LoanBriefFileDetail> ListFiles { get; set; }
    }
}
