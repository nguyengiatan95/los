﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;

namespace LOS.DAL.DTOs
{
    public class GroupDTO
    {
        public GroupDTO()
        {
            ListGroupModule = new
                 List<GroupModule>();
        }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public string DefaultPath { get; set; }
        public List<GroupModule> ListGroupModule { get; set; }
    }

    public static class GroupDTOExtentions
    {
        public static Group Mapping(this GroupDTO dto)
        {
            var entity = new Group();
            entity.GroupId = dto.GroupId;
            entity.GroupName = dto.GroupName;
            entity.Description = dto.Description;
            entity.Status = dto.Status;
            entity.CreatedAt = dto.CreatedAt;
            entity.UpdatedAt = dto.UpdatedAt;
            entity.DefaultPath = dto.DefaultPath;
            return entity;
        }
    }


    public class GroupDetail : GroupDTO
    {
        public List<ModuleDetail> Modules { get; set; }
        public List<GroupActionPermission> ListPermissionAction { get; set; }
        public List<GroupCondition> ListCondition { get; set; }
        public List<GroupModule> ListGroupModule { get; set; }

        public static Expression<Func<Group, GroupDetail>> ProjectionDetail
        {
            get
            {
                return x => new GroupDetail()
                {
                    CreatedAt = x.CreatedAt,
                    DefaultPath = x.DefaultPath,
                    Description = x.Description,
                    GroupId = x.GroupId,
                    GroupName = x.GroupName,
                    Status = x.Status,
                    UpdatedAt = x.UpdatedAt,
                    Modules = x.GroupModule.Select(x1 => new ModuleDetail()
                    {
                        Code = x1.Module.Code,
                        Description = x1.Module.Description,
                        ModuleId = x1.Module.ModuleId,
                        Name = x1.Module.Name,
                        Path = x1.Module.Path,
                        Status = x1.Module.Status,
                        ParentId = x1.Module.ParentId,
                        Priority = x1.Module.Priority,
                        Controller = x1.Module.Controller,
                        Action = x1.Module.Action,
                        IsMenu = x1.Module.IsMenu.GetValueOrDefault(),
                        Icon = x1.Module.Icon,
                    }).ToList()
                };
            }
        }

        public static Expression<Func<Group, GroupDetail>> ProjectionPermissionDetail
        {
            get
            {
                return x => new GroupDetail()
                {
                    CreatedAt = x.CreatedAt,
                    DefaultPath = x.DefaultPath,
                    Description = x.Description,
                    GroupId = x.GroupId,
                    GroupName = x.GroupName,
                    Status = x.Status,
                    UpdatedAt = x.UpdatedAt,
                    ListPermissionAction = x.GroupActionPermission != null ? x.GroupActionPermission.Select(x1 => x1).ToList() : null,
                    ListCondition = x.GroupCondition != null ? x.GroupCondition.Select(x1 => x1).ToList() : null,
                    Modules = x.GroupModule.Select(x1 => new ModuleDetail()
                    {
                        Code = x1.Module.Code,
                        Description = x1.Module.Description,
                        ModuleId = x1.Module.ModuleId,
                        Name = x1.Module.Name,
                        Path = x1.Module.Path,
                        Status = x1.Module.Status,
                        ParentId = x1.Module.ParentId,
                        Priority = x1.Module.Priority,
                        Controller = x1.Module.Controller,
                        Action = x1.Module.Action,
                        IsMenu = x1.Module.IsMenu.GetValueOrDefault(),
                        Icon = x1.Module.Icon
                    }).ToList()
                };
            }
        }
    }
}
