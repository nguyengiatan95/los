﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class InfomationProductDetailDTO : InfomationProductDetail
    {
        public static Expression<Func<InfomationProductDetail, InfomationProductDetailDTO>> ProjectionViewDetail
        {
            get
            {
                return x => new InfomationProductDetailDTO()
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsEnable = x.IsEnable,
                    ConfigProductDetail = x.ConfigProductDetail ?? null,
                };
            }
        }
    }
}
