﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class JobDTO
	{
		public int? JobId { get; set; }
		public string Name { get; set; }
		public int? Priority { get; set; }
	}


    public static class JobDTOExtensions
    {
        public static Job Mapping(this JobDTO dto)
        {
            var job = new Job();
            job.JobId = dto.JobId.Value;
            job.Name = dto.Name;
            job.Priority = dto.Priority;
            return job;
        }
    }

    public static class JobExtensions
    {
        public static JobDTO Mapping(this Job dto)
        {
            var job = new JobDTO();
            job.JobId = dto.JobId;
            job.Name = dto.Name;
            job.Priority = dto.Priority;
            return job;
        }
    }


    public class JobDetail: JobDTO
    {
        public static Expression<Func<Job, JobDetail>> ProjectionDetail
        {
            get
            {
                return x => new JobDetail()
                {
                    JobId = x.JobId,
                    Name = x.Name,
                    Priority = x.Priority
                };
            }
        }
    }
}
