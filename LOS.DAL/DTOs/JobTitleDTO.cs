﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class JobTitleDTO
	{
		public int JobTitleId { get; set; }
		public string Name { get; set; }
		public string Priority { get; set; }
	}

    public static class JobTitleDTOExtensions
    {
        public static JobTitle Mapping(this JobTitleDTO dto)
        {
            var job = new JobTitle();
            job.JobTitleId = dto.JobTitleId;
            job.Name = dto.Name;
            job.Priority = dto.Priority;
            return job;
        }
    }

    public static class JobTitleExtensions
    {
        public static JobTitleDTO Mapping(this JobTitle dto)
        {
            var job = new JobTitleDTO();
            job.JobTitleId = dto.JobTitleId;
            job.Name = dto.Name;
            job.Priority = dto.Priority;
            return job;
        }
    }


    public class JobTitleDetail : JobTitleDTO
    {
        public static Expression<Func<JobTitle, JobTitleDetail>> ProjectionDetail
        {
            get
            {
                return x => new JobTitleDetail()
                {
                    JobTitleId = x.JobTitleId,
                    Name = x.Name,
                    Priority = x.Priority
                };
            }
        }
    }
}
