﻿using LOS.Common.Extensions;
using LOS.Common.Helpers;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class LeadScoreInfoDTO
    {
        public int ID { get; set; }
        public int LoanTime { get; set; }
        public int Status { get; set; }
        public DateTime CreateDate { get; set; }
        public int TypeReceivingMoney { get; set; }
        public string NameBanking { get; set; }
        public string NumberCardBanking { get; set; }
        public string AccountNumberBanking { get; set; }
        public int TypeID { get; set; }
        public string utm_source { get; set; }
        public string utm_medium { get; set; }
        public string utm_campaign { get; set; }
        public int TypeTimerId { get; set; }
        public int CityId { get; set; }
        public decimal TotalMoneyFirst { get; set; }
        public decimal TotalMoney { get; set; }
        public string utm_content { get; set; }
        public string utm_term { get; set; }
        public int Step { get; set; }
        public string ModelPhone { get; set; }
        public string YearMade { get; set; }
        public string FullName { get; set; }
        public DateTime? Birthday { get; set; }
        public string Phone { get; set; }
        public string CardNumber { get; set; }
        public int? Gender { get; set; }
        public string Email { get; set; }
        public string Street { get; set; }
        public int? JobId { get; set; }
        public int? WorkingIndustryId { get; set; }
        public decimal? Salary { get; set; }
        public bool? ReMarketing { get; set; }
        public int? LoanAgain { get; set; }
        public string CityName { get; set; }
        public string JobName { get; set; }
        public int? ReceiveYourIncome { get; set; }
        public int? IsMarried { get; set; }
        public int? NumberBaby { get; set; }
        public int? IsLivingTogether { get; set; }
        public int? RateType { get; set; }
        public bool HasBadDebt { get; set; }
        public bool HasLatePayment { get; set; }
        public int RelativeFamilyId { get; set; }
        public string PhoneFamily { get; set; }
        public int? CustomerCreditId { get; set; }
        public int? Code { get; set; }
        public int? TypeOfOwnershipId { get; set; }
        public string CICName { get; set; }

        public LeadScoreItem LeadScoreItem()
        {
            try
            {
                var entity = new LeadScoreItem();
                entity.LoanCreditId = ID;
                entity.LoanTime = LoanTime;
                entity.Status = Status;
                entity.CreateDate = CreateDate != null ? this.CreateDate.ToString("yyyy-MM-dd HH:mm:ss.fff") : string.Empty;
                entity.TypeReceivingMoney = this.TypeReceivingMoney;
                entity.NameBanking = this.NameBanking ?? string.Empty;
                entity.NumberCardBanking = this.NumberCardBanking ?? string.Empty;
                entity.AccountNumberBanking = this.AccountNumberBanking ?? string.Empty;
                entity.TypeID = this.TypeID;
                entity.utm_source = this.utm_source ?? string.Empty;
                entity.utm_medium = this.utm_medium ?? string.Empty;
                entity.utm_campaign = this.utm_campaign ?? string.Empty;
                entity.TypeTime = this.TypeTimerId;
                entity.CityId = this.CityId;
                entity.OriginalPrice = this.TotalMoneyFirst;
                entity.SellPrice = this.TotalMoney;
                entity.utm_content = this.utm_content ?? string.Empty;
                entity.utm_term = this.utm_term ?? string.Empty;
                entity.Step = this.Step;
                entity.UserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Mobile/14G60 [FBAN/MessengerForiOS;FBAV/170.1.0.57.96;FBBV/113832539;FBDV/iPhone7,2;FBMD/iPhone;FBSN/iOS;FBSV/10.3.3;FBSS/2;FBCR/Viettel;FBID/phone;FBLC/vi_VN;FBOP/5;FBRV/0]";
                entity.OriginalTypeID = this.TypeID;
                entity.OriginalCityId = this.CityId;
                entity.VehicleName = this.ModelPhone;
                var VehicleYear = 0;
                int.TryParse(this.YearMade, out VehicleYear);
                entity.VehicleYear = VehicleYear;
                entity.ProductPawnInfo = 0;
                entity.ProductPawName = string.Empty;
                entity.FullName = this.FullName;
                entity.Birthday = this.Birthday != null ? this.Birthday.Value.ToString("yyyy-MM-dd") : string.Empty;
                entity.Phone = this.Phone;
                entity.CardNumber = this.CardNumber;
                entity.Gender = this.Gender;
                entity.Email = this.Email ?? string.Empty;
                entity.Street = this.Street ?? string.Empty;
                entity.JobId = this.JobId;
                entity.WorkingIndustryId = this.WorkingIndustryId;
                entity.Salary = this.Salary;
                return entity;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public LoanCreditForLeadScoreItem ItemGetData()
        {
            var item = new LoanCreditForLeadScoreItem();
            try
            {
                item.TotalMoney = TotalMoney;
                item.LoanTime = this.LoanTime;
                item.ReMarketing = this.ReMarketing;
                item.LoanAgain = this.LoanAgain;
                item.CityName = this.CityName;
                item.Gender = this.Gender;
                item.JobName = this.JobName;
                item.Salary = this.Salary;
                item.TypeReceivingMoney = this.TypeReceivingMoney;
                item.ReceiveYourIncome = this.ReceiveYourIncome;
                item.IsMarried = this.IsMarried;
                item.NumberBaby = this.NumberBaby;
                item.IsLivingTogether = this.IsLivingTogether;
                item.CustomerAge = Birthday.HasValue ? (DateTime.Now.Year - Birthday.Value.Year) + 1 : 0;
                item.RateType = this.RateType;
                item.IsLivingTogether = this.IsLivingTogether;
                item.HasBadDebt = this.HasBadDebt;
                item.HasLatePayment = this.HasLatePayment;
                item.ProductID = this.TypeID;
                item.RelativeFamilyId = this.RelativeFamilyId;
                item.PhoneFamily = this.PhoneFamily;
                item.Phone = this.Phone;
                item.JobId = this.JobId;
                item.Code = this.Code;
                item.TypeOfOwnershipId = this.TypeOfOwnershipId;
                item.TypeIncomeName = ExtentionHelper.GetDescription((EnumImcomeType)this.TypeReceivingMoney);
                item.CICName = this.CICName;
                item.FullName = this.FullName;
                item.CardNumber = this.CardNumber;
            }
            catch
            {
            }
            return item;
        }
    }

    public class LeadScoreInfoDetail : LeadScoreInfoDTO
    {
        public static Expression<Func<LoanBrief, LeadScoreInfoDetail>> ProjectionDetail
        {
            get
            {
                return x => new LeadScoreInfoDetail()
                {
                    ID = x.LoanBriefId,
                    LoanTime = x.LoanTime.GetValueOrDefault(),
                    Status = x.Status.GetValueOrDefault(),
                    CreateDate = x.CreatedTime.Value.DateTime,
                    TypeReceivingMoney = x.ReceivingMoneyType.GetValueOrDefault(),
                    NameBanking = x.Bank != null ? x.Bank.Name : "",
                    NumberCardBanking = x.BankCardNumber,
                    AccountNumberBanking = x.BankAccountNumber,
                    TypeID = x.ProductId.GetValueOrDefault(),
                    utm_source = x.UtmSource,
                    utm_medium = x.UtmMedium,
                    utm_campaign = x.UtmCampaign,
                    TypeTimerId = 0,
                    CityId = x.ProvinceId.GetValueOrDefault(),
                    TotalMoneyFirst = x.LoanAmount.GetValueOrDefault(),
                    TotalMoney = x.LoanAmount.GetValueOrDefault(),
                    utm_content = x.UtmContent,
                    utm_term = x.UtmTerm,
                    Step = 0,
                    // ModelPhone = ,
                    //YearMade
                    FullName = x.FullName,
                    Birthday = x.Dob,
                    Phone = x.Phone,
                    CardNumber = x.NationalCard,
                    Gender = x.Gender,
                    Email = x.Email,
                    Street = x.LoanBriefResident != null && !string.IsNullOrEmpty(x.LoanBriefResident.Address) ? x.LoanBriefResident.Address : "",
                    JobId = x.LoanBriefJob != null ? x.LoanBriefJob.JobId : 0,
                    //WorkingIndustryId = x.
                    Salary = x.LoanBriefJob != null ? x.LoanBriefJob.TotalIncome : 0,
                    ReMarketing = x.TypeRemarketing == (int)EnumTypeRemarketing.IsRemarketing,
                    LoanAgain = 0,
                    CityName = x.Province != null ? x.Province.Name : "",
                    JobName = x.LoanBriefJob != null && x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.Name : "",
                    ReceiveYourIncome = x.LoanBriefJob != null ? x.LoanBriefJob.ImcomeType : 0,
                    IsMarried = x.Customer != null ? x.Customer.IsMerried : 0,
                    NumberBaby = x.Customer != null ? x.Customer.NumberBaby : 0,
                    IsLivingTogether = x.LoanBriefResident != null ? x.LoanBriefResident.LivingWith : 0,
                    RateType = x.RateTypeId,
                    HasBadDebt = false,
                    HasLatePayment = false,
                    //            RelativeFamilyId = x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0 ? x.LoanBriefRelationship.ToList()
                    //PhoneFamily = x.
                    CustomerCreditId = x.CustomerId,
                    Code = x.HubId,
                    TypeOfOwnershipId = x.LoanBriefResident != null && x.LoanBriefResident.ResidentType > 0 ? x.LoanBriefResident.ResidentType.Value : 0,
                    CICName = ""
                };
            }
        }
    }
}
