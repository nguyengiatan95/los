﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class LoanActionDTO
	{
		public int LoanActionId { get; set; }
		public int? LoanBriefId { get; set; }
		public int? ActionStatusId { get; set; }
		public int? ActorId { get; set; }
		public DateTimeOffset? StartTime { get; set; }
		public DateTimeOffset? FinishTime { get; set; }
		public int? ExecutedTime { get; set; }
		public string Note { get; set; }
		public int? LastActionStatusId { get; set; }
	}

	public class LoanActionDetail : LoanActionDTO
	{
		public string ActionStatusText { get; set; }
		public string Username { get; set; }
	}
}
