﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using LOS.DAL.Object;
using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace LOS.DAL.DTOs
{
    public class LoanBriefDTO
    {
        public int LoanBriefId { get; set; }
        public int? ProductId { get; set; }
        public int? CustomerId { get; set; }
        public int? RateTypeId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public DateTime? Dob { get; set; }
        public int? Gender { get; set; }
        public string NationalCard { get; set; }
        public DateTime? NationalCardDate { get; set; }
        public string NationCardPlace { get; set; }
        public decimal? LoanAmount { get; set; }
        public decimal? LoanAmountFirst { get; set; }
        public int? LoanTime { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public double? ConsultantRate { get; set; }
        public double? ServiceRate { get; set; }
        public double? LoanRate { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string UtmSource { get; set; }
        public string UtmMedium { get; set; }
        public string UtmCampaign { get; set; }
        public string UtmTerm { get; set; }
        public string UtmContent { get; set; }
        public int? ScoreCredit { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTimeOffset? UpdatedTime { get; set; }
        public int? Status { get; set; }
        public bool? IsNeeded { get; set; }
        public int? AffCode { get; set; }
        public int? BoundTelesaleId { get; set; }
        public int? AffStatus { get; set; }
        public int? MecashId { get; set; }
        public int? ReceivingMoneyType { get; set; }
        public int? BankId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }
        public string BankAccountName { get; set; }
        public int? OrderSourceId { get; set; }
        public int? OrderSourceParentId { get; set; }
        public bool? IsHeadOffice { get; set; }
        public int? PlatformType { get; set; }
        public int? CurrentPipelineId { get; set; }
        public int? CurrentSectionId { get; set; }
        public int? CurrentSectionDetailId { get; set; }
        public int? PipelineState { get; set; }
        public bool? SectionApprove { get; set; }
        public int? TypeLoanBrief { get; set; }
        public int? Frequency { get; set; }
        public bool? IsCheckCic { get; set; }
        public bool? IsLocate { get; set; }
        public bool? IsTrackingLocation { get; set; }
        public string RefCodeLocation { get; set; }
        public int? RefCodeStatus { get; set; }
        public bool? BuyInsurenceCustomer { get; set; }
        public int? NumberCall { get; set; }
        public int? CreateBy { get; set; }
        public int? LoanPurpose { get; set; }
        public string LoanBriefComment { get; set; }
        public int? ReasonCancel { get; set; }
        public int? ReMarketingLoanBriefId { get; set; }
        public bool? IsReMarketing { get; set; }
        public DateTime? LoanBriefCancelAt { get; set; }
        public int? LoanBriefCancelBy { get; set; }
        public DateTime? TelesalesPushAt { get; set; }
        public DateTime? ScheduleTime { get; set; }
        public int? HubId { get; set; }
        public int? HubEmployeeId { get; set; }
        public DateTime? HubPushAt { get; set; }
        public int? EvaluationHomeUserId { get; set; }
        public bool? EvaluationHomeState { get; set; }
        public int? EvaluationCompanyUserId { get; set; }
        public bool? EvaluationCompanyState { get; set; }
        public int? CoordinatorUserId { get; set; }
        public int? ActionState { get; set; }
        public bool? AutomaticProcessed { get; set; }
        public int? LenderId { get; set; }
        public DateTime? CoordinatorPushAt { get; set; }
        public bool? IsUploadState { get; set; }
        public string DeviceId { get; set; }
        public int? DeviceStatus { get; set; }
        public DateTime? LastSendRequestActive { get; set; }
        public string ResultCic { get; set; }
        public string ResultLoaction { get; set; }
        public int? CheckCic { get; set; }
        public int? CheckLoaction { get; set; }
        public string ContractGinno { get; set; }
        public bool IsEdit { get; set; } = false;
        public bool IsScript { get; set; } = false;
        public bool IsPush { get; set; } = false;
        public bool IsBack { get; set; } = false;
        public bool IsCancel { get; set; } = false;
        public bool IsUpload { get; set; } = false;
        public bool IsConfirmCancel { get; set; } = false;
        public bool IsConfirmAndPush { get; set; } = false;
        public bool IsChangePhone { get; set; } = false;
        public int? InProcess { get; set; }
        public string ResultRuleCheck { get; set; }
        public string ResultLocation { get; set; }
        public int? CountCall { get; set; }
        public DateTime? FirstProcessingTime { get; set; }
        public bool? IsReborrow { get; set; }
        public int TypeHotLead { get; set; }
        public DateTime? DisbursementAt { get; set; }
        public int? StatusTelesales { get; set; }
        public int? DetailStatusTelesales { get; set; }
        public int? TypeRemarketing { get; set; }
        public int? Step { get; set; }
        public int? HomeNetwork { get; set; }

        public DateTime? LastChangeStatusTelesale { get; set; }
        public int? FieldHo { get; set; }
        public int? LoanStatusDetail { get; set; }
        public int? LoanStatusDetailChild { get; set; }
        public int? ApproverStatusDetail { get; set; }
        public DateTime? HubEmployeeCallFirst { get; set; }
        public int? EsignState { get; set; }
        public int? TypeLoanSupport { get; set; }
        public bool IsCheckMomo { get; set; } = false;
    }

    public static class LoanBriefDTOExtensions
    {
        public static LoanBrief Mapping(this LoanBriefDTO dto)
        {
            var loanBrief = new LoanBrief();
            loanBrief.LoanBriefId = dto.LoanBriefId;
            loanBrief.ProductId = dto.LoanBriefId;
            loanBrief.RateTypeId = dto.LoanBriefId;
            loanBrief.FullName = dto.FullName;
            loanBrief.NationalCard = dto.NationalCard;
            loanBrief.NationalCardDate = dto.NationalCardDate;
            loanBrief.NationCardPlace = dto.NationCardPlace;
            loanBrief.LoanAmount = dto.LoanAmount;
            loanBrief.LoanTime = dto.LoanTime;
            loanBrief.FromDate = dto.FromDate;
            loanBrief.ToDate = dto.ToDate;
            loanBrief.ConsultantRate = dto.ConsultantRate;
            loanBrief.ServiceRate = dto.ServiceRate;
            loanBrief.LoanRate = dto.LoanRate;
            loanBrief.ProvinceId = dto.ProvinceId;
            loanBrief.DistrictId = dto.DistrictId;
            loanBrief.WardId = dto.WardId;
            loanBrief.UtmSource = dto.UtmSource;
            loanBrief.UtmMedium = dto.UtmMedium;
            loanBrief.UtmCampaign = dto.UtmCampaign;
            loanBrief.UtmTerm = dto.UtmTerm;
            loanBrief.UtmContent = dto.UtmContent;
            loanBrief.ScoreCredit = dto.ScoreCredit;
            loanBrief.CreatedTime = dto.CreatedTime;
            loanBrief.UpdatedTime = dto.UpdatedTime;
            loanBrief.Status = dto.Status;
            loanBrief.AffStatus = dto.AffStatus;
            loanBrief.LoanBriefComment = dto.LoanBriefComment;
            return loanBrief;
        }
    }

    public class LoanBriefDetail : LoanBriefDTO
    {
        public LoanProductDTO LoanProduct { get; set; }
        public LoanRateTypeDTO LoanRateType { get; set; }
        public ProvinceDTO Province { get; set; }
        public DistrictDTO District { get; set; }
        public WardDTO Ward { get; set; }
        public UserDTO UserHub { get; set; }
        public UserDTO UserTelesale { get; set; }
        public UserDTO UserHubEmployee { get; set; }
        public UserDTO UserFieldHo { get; set; }
        public ShopDTO Hub { get; set; }
        public LoanBriefJobDetail Job { get; set; }
        public Customer Customer { get; set; }
        public LoanBriefResident LoanBriefResident { get; set; }
        public bool UploadImage { get; set; }
        public TimeProcessingLoanBrief TimeProcessingLoan { get; set; }
        public bool PickTLS { get; set; }
        public int ApproverTimeProcessing { get; set; }
        public int? IsExceptions { get; set; }
        public bool? BuyInsuranceProperty { get; set; }
        public int? LmsLoanId { get; set; }
        public bool? PostRegistration { get; set; }
        //public UserDTO BoundTelesale { get; set; }
        //public LoanBriefJobDetail Job { get; set; }
        public decimal? CreditScoring { get; set; }
        public int? TransactionState { get; set; }
        public bool IsTransactionsSecured { get; set; } = false;
        public static Expression<Func<LoanBrief, LoanBriefDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    CustomerId = x.CustomerId,
                    ProductId = x.ProductId,
                    RateTypeId = x.RateTypeId,
                    FullName = x.FullName,
                    Gender = x.Gender,
                    Phone = x.Phone,
                    Dob = x.Dob,
                    NationalCard = x.NationalCard,
                    NationalCardDate = x.NationalCardDate,
                    NationCardPlace = x.NationCardPlace,
                    LoanAmount = x.LoanAmount,
                    LoanAmountFirst = x.LoanAmountFirst,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    ConsultantRate = x.ConsultantRate,
                    ServiceRate = x.ServiceRate,
                    LoanRate = x.LoanRate,
                    ProvinceId = x.ProvinceId,
                    DistrictId = x.DistrictId,
                    WardId = x.WardId,
                    UtmSource = x.UtmSource,
                    UtmMedium = x.UtmMedium,
                    UtmCampaign = x.UtmCampaign,
                    UtmTerm = x.UtmTerm,
                    UtmContent = x.UtmContent,
                    ScoreCredit = x.ScoreCredit,
                    CreateBy = x.CreateBy,
                    CreatedTime = x.CreatedTime,
                    UpdatedTime = x.UpdatedTime,
                    Status = x.Status,
                    AffStatus = x.AffStatus,
                    ScheduleTime = x.ScheduleTime,
                    LoanBriefCancelAt = x.LoanBriefCancelAt,
                    LoanBriefCancelBy = x.LoanBriefCancelBy,
                    TelesalesPushAt = x.TelesalesPushAt,
                    HubPushAt = x.HubPushAt,
                    LoanBriefComment = x.LoanBriefComment,
                    LenderId = x.LenderId,
                    IsUploadState = x.IsUploadState,
                    IsLocate = x.IsLocate,
                    DeviceId = x.DeviceId,
                    DeviceStatus = x.DeviceStatus,
                    CoordinatorUserId = x.CoordinatorUserId,
                    PipelineState = x.PipelineState,
                    HubEmployeeId = x.HubEmployeeId,
                    IsTrackingLocation = x.IsTrackingLocation,
                    BoundTelesaleId = x.BoundTelesaleId,
                    InProcess = x.InProcess.GetValueOrDefault(),
                    ResultLocation = x.ResultLocation,
                    ResultRuleCheck = x.ResultRuleCheck,
                    BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                    FirstProcessingTime = x.FirstProcessingTime,
                    CountCall = x.CountCall,
                    IsReborrow = x.IsReborrow,
                    DisbursementAt = x.DisbursementAt,
                    StatusTelesales = x.StatusTelesales,
                    DetailStatusTelesales = x.DetailStatusTelesales,
                    TypeRemarketing = x.TypeRemarketing,
                    LastChangeStatusTelesale = x.LastChangeStatusOfTelesale,
                    FieldHo = x.FieldHo,
                    LoanStatusDetail = x.LoanStatusDetail,
                    LoanStatusDetailChild = x.LoanStatusDetailChild,
                    ApproverStatusDetail = x.ApproverStatusDetail,
                    IsExceptions = x.IsExceptions,
                    ReMarketingLoanBriefId = x.ReMarketingLoanBriefId,
                    CurrentPipelineId = x.CurrentPipelineId,
                    BuyInsuranceProperty = x.LoanBriefProperty != null ? x.LoanBriefProperty.BuyInsuranceProperty : null,
                    PostRegistration = x.LoanBriefProperty != null ? x.LoanBriefProperty.PostRegistration : null,
                    LmsLoanId = x.LmsLoanId,
                    TypeLoanSupport = x.TypeLoanSupport,
                    TransactionState = x.TransactionState,
                    LoanBriefResident = x.LoanBriefResident != null ? new LoanBriefResident()
                    {
                        ResidentType = x.LoanBriefResident.ResidentType,
                        LivingTime = x.LoanBriefResident.LivingTime
                    } : null,

                    LoanProduct = x.Product != null ? new LoanProductDTO()
                    {
                        LoanProductId = x.Product.LoanProductId,
                        Name = x.Product.Name
                    } : null,
                    LoanRateType = x.RateType != null ? new LoanRateTypeDTO()
                    {
                        LoanRateTypeId = x.RateType.LoanRateTypeId,
                        RateType = x.RateType.RateType
                    } : null,
                    Province = x.Province != null ? new ProvinceDTO()
                    {
                        ProvinceId = x.Province.ProvinceId,
                        Name = x.Province.Name
                    } : null,
                    District = x.District != null ? new DistrictDTO()
                    {
                        DistrictId = x.District.DistrictId,
                        Name = x.District.Name
                    } : null,
                    Ward = x.Ward != null ? new WardDTO()
                    {
                        WardId = x.Ward.WardId,
                        Name = x.Ward.Name
                    } : null,
                    Job = x.LoanBriefJob != null ? new LoanBriefJobDetail()
                    {
                        Job = x.LoanBriefJob.Job != null ? new JobDTO()
                        {
                            JobId = x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.JobId : -1,
                            Name = x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.Name : ""
                        } : null,
                        TotalIncome = x.LoanBriefJob.TotalIncome,
                        ImcomeType = x.LoanBriefJob != null ? x.LoanBriefJob.ImcomeType : 0
                    } : null,
                    UserHub = x.CoordinatorUser != null ? new UserDTO()
                    {
                        FullName = x.CoordinatorUser.FullName,
                        UserId = x.CoordinatorUser.UserId,
                        Phone = x.CoordinatorUser.Phone,
                        Ipphone = x.CoordinatorUser.Ipphone
                    } : null,
                    UserTelesale = x.BoundTelesale != null ? new UserDTO()
                    {
                        FullName = x.BoundTelesale.FullName,
                        UserId = x.BoundTelesale.UserId,
                        Phone = x.BoundTelesale.Phone,
                        Ipphone = x.BoundTelesale.Ipphone,
                        Username = x.BoundTelesale.Username
                    } : null,
                    UserHubEmployee = x.HubEmployeeId != null ? new UserDTO()
                    {
                        FullName = x.HubEmployee.FullName,
                        UserId = x.HubEmployee.UserId,
                        Phone = x.HubEmployee.Phone,
                        Ipphone = x.HubEmployee.Ipphone
                    } : null,
                    //UserFieldHo = x.FieldHo > 0 ? new UserDTO()
                    //{
                    //    FullName = x.FieldHoNavigation.FullName,
                    //    UserId = x.FieldHoNavigation.UserId,
                    //    Phone = x.FieldHoNavigation.Phone
                    //} : null,
                    Hub = x.Hub != null ? new ShopDTO()
                    {
                        ShopId = x.Hub.ShopId,
                        Name = x.Hub.Name
                    } : null,
                    Step = x.Step,
                    PlatformType = x.PlatformType,
                    HomeNetwork = x.HomeNetwork,
                    HubEmployeeCallFirst = x.HubEmployeeCallFirst,
                    EsignState = x.EsignState,
                    CreditScoring = x.LeadScore
                };
            }
        }
        public static Expression<Func<LoanBrief, LoanBriefDetail>> ProjectionHubDetail
        {
            get
            {
                return x => new LoanBriefDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    ProductId = x.ProductId,
                    RateTypeId = x.RateTypeId,
                    FullName = x.FullName,
                    Gender = x.Gender,
                    Phone = x.Phone,
                    Dob = x.Dob,
                    NationalCard = x.NationalCard,
                    NationalCardDate = x.NationalCardDate,
                    NationCardPlace = x.NationCardPlace,
                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    ConsultantRate = x.ConsultantRate,
                    ServiceRate = x.ServiceRate,
                    LoanRate = x.LoanRate,
                    ProvinceId = x.ProvinceId,
                    DistrictId = x.DistrictId,
                    WardId = x.WardId,
                    UtmSource = x.UtmSource,
                    UtmMedium = x.UtmMedium,
                    UtmCampaign = x.UtmCampaign,
                    UtmTerm = x.UtmTerm,
                    UtmContent = x.UtmContent,
                    ScoreCredit = x.ScoreCredit,
                    CreateBy = x.CreateBy,
                    CreatedTime = x.CreatedTime,
                    UpdatedTime = x.UpdatedTime,
                    Status = x.Status,
                    //AffCode = x.AffCode,
                    AffStatus = x.AffStatus,
                    ScheduleTime = x.ScheduleTime,
                    LoanBriefCancelAt = x.LoanBriefCancelAt,
                    LoanBriefCancelBy = x.LoanBriefCancelBy,
                    TelesalesPushAt = x.TelesalesPushAt,
                    LoanBriefComment = x.LoanBriefComment,
                    PipelineState = x.PipelineState,
                    CoordinatorUserId = x.CoordinatorUserId,
                    HubEmployeeId = x.HubEmployeeId,
                    HubPushAt = x.HubPushAt,
                    LastSendRequestActive = x.LastSendRequestActive,
                    DeviceStatus = x.DeviceStatus,
                    DeviceId = x.DeviceId,
                    IsLocate = x.IsLocate,
                    IsTrackingLocation = x.IsTrackingLocation,
                    IsReborrow = x.IsReborrow,
                    TypeRemarketing = x.TypeRemarketing,
                    FieldHo = x.FieldHo,
                    LoanStatusDetail = x.LoanStatusDetail.Value,
                    LoanStatusDetailChild = x.LoanStatusDetailChild.Value,
                    ApproverStatusDetail = x.ApproverStatusDetail,
                    ReMarketingLoanBriefId = x.ReMarketingLoanBriefId,
                    BoundTelesaleId = x.BoundTelesaleId,
                    CurrentPipelineId = x.CurrentPipelineId,
                    HubEmployeeCallFirst = x.HubEmployeeCallFirst,
                    PostRegistration = x.LoanBriefProperty != null ? x.LoanBriefProperty.PostRegistration : null,
                    EsignState = x.EsignState,
                    IsExceptions = x.IsExceptions,
                    LoanProduct = x.Product != null ? new LoanProductDTO()
                    {
                        LoanProductId = x.Product.LoanProductId,
                        Name = x.Product.Name
                    } : null,
                    Province = x.Province != null ? new ProvinceDTO()
                    {
                        ProvinceId = x.Province.ProvinceId,
                        Name = x.Province.Name
                    } : null,
                    District = x.District != null ? new DistrictDTO()
                    {
                        DistrictId = x.District.DistrictId,
                        Name = x.District.Name
                    } : null,
                    UserHub = x.HubEmployee != null ? new UserDTO()
                    {
                        FullName = x.HubEmployee.FullName,
                        UserId = x.HubEmployee.UserId
                    } : null,
                    LoanBriefResident = x.LoanBriefResident != null ? new LoanBriefResident()
                    {
                        ResidentType = x.LoanBriefResident.ResidentType,
                    } : null,
                    Job = x.LoanBriefJob != null ? new LoanBriefJobDetail()
                    {
                        ImcomeType = x.LoanBriefJob != null ? x.LoanBriefJob.ImcomeType : 0,
                        JobId = x.LoanBriefJob != null ? x.LoanBriefJob.JobId : 0,
                    } : null,
                };
            }
        }


    }

    public class LoanBriefInitDetail : LoanBrief
    {
        public int ProductType { get; set; }

        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefInitDetail>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanBriefInitDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    ProductId = x.ProductId,
                    CustomerId = x.CustomerId,
                    RateTypeId = x.RateTypeId,
                    FullName = x.FullName,
                    Gender = x.Gender,
                    Phone = x.Phone,
                    Dob = x.Dob,
                    NationalCard = x.NationalCard,
                    NationalCardDate = x.NationalCardDate,
                    NationCardPlace = x.NationCardPlace,
                    LoanAmount = x.LoanAmount,
                    LoanAmountFirst = x.LoanAmountFirst,
                    LoanAmountExpertise = x.LoanAmountExpertise,
                    LoanAmountExpertiseAi = x.LoanAmountExpertiseAi,
                    LoanAmountExpertiseLast = x.LoanAmountExpertiseLast,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    ConsultantRate = x.ConsultantRate,
                    ServiceRate = x.ServiceRate,
                    LoanRate = x.LoanRate,
                    ProvinceId = x.ProvinceId,
                    ProvinceName = x.Province != null ? x.Province.Name : "",
                    DistrictId = x.DistrictId,
                    DistrictName = x.District != null ? x.District.Name : "",
                    WardId = x.WardId,
                    WardName = x.Ward != null ? x.Ward.Name : "",
                    UtmSource = x.UtmSource,
                    UtmMedium = x.UtmMedium,
                    UtmCampaign = x.UtmCampaign,
                    UtmTerm = x.UtmTerm,
                    UtmContent = x.UtmContent,
                    ScoreCredit = x.ScoreCredit,
                    CreateBy = x.CreateBy,
                    CreatedTime = x.CreatedTime,
                    UpdatedTime = x.UpdatedTime,
                    Status = x.Status,
                    AffCode = x.AffCode,
                    AffStatus = x.AffStatus,
                    PlatformType = x.PlatformType,
                    TypeLoanBrief = x.TypeLoanBrief,
                    BankId = x.BankId,
                    BankAccountName = x.BankAccountName,
                    BankAccountNumber = x.BankAccountNumber,
                    BankCardNumber = x.BankCardNumber,
                    ReceivingMoneyType = x.ReceivingMoneyType,
                    IsTrackingLocation = x.IsTrackingLocation,
                    IsLocate = x.IsLocate,
                    IsCheckCic = x.IsCheckCic,
                    LoanPurpose = x.LoanPurpose,
                    PipelineState = x.PipelineState,
                    ReasonCancel = x.ReasonCancel,
                    ScheduleTime = x.ScheduleTime,
                    ActionState = x.ActionState,
                    CoordinatorUserId = x.CoordinatorUserId,
                    BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                    HubEmployeeId = x.HubEmployeeId,
                    LastSendRequestActive = x.LastSendRequestActive,
                    DeviceStatus = x.DeviceStatus,
                    DeviceId = x.DeviceId,
                    LoanBriefCompany = x.LoanBriefCompany ?? null,
                    LoanBriefDocument = x.LoanBriefDocument ?? null,
                    LoanBriefHousehold = x.LoanBriefHousehold ?? null,
                    LoanBriefJob = x.LoanBriefJob ?? null,
                    LoanBriefProperty = x.LoanBriefProperty ?? null,
                    LoanBriefRelationship = x.LoanBriefRelationship ?? null,
                    LoanBriefResident = x.LoanBriefResident ?? null,
                    Customer = x.Customer ?? null,
                    LoanBriefQuestionScript = x.LoanBriefQuestionScript ?? null,
                    //LoanBriefNote = x.LoanBriefNote ?? null,
                    //LoanBriefFiles = x.LoanBriefFiles ?? null,
                    InProcess = x.InProcess,
                    ContractGinno = x.ContractGinno,
                    ResultLocation = x.ResultLocation,
                    ResultRuleCheck = x.ResultRuleCheck,
                    FirstTimeHubFeedBack = x.FirstTimeHubFeedBack,
                    ValueCheckQualify = x.ValueCheckQualify,
                    //Product = x.Product ?? null,
                    //Province = x.Province ?? null,
                    //District = x.District ?? null,
                    //Ward = x.Ward ?? null,
                    FirstProcessingTime = x.FirstProcessingTime,
                    Passport = x.Passport,
                    RateMoney = x.RateMoney,
                    RatePercent = x.RatePercent,
                    PresenterCode = x.PresenterCode,
                    ProductType = x.Product != null ? x.Product.TypeProductId.Value : 0,
                    CodeId = x.CodeId,
                    LmsLoanId = x.LmsLoanId,
                    HubId = x.HubId,
                    TypeRemarketing = x.TypeRemarketing,
                    IsCheckBank = x.IsCheckBank,
                    IsReborrow = x.IsReborrow,
                    HubEmployee = x.HubEmployee,
                    EvaluationCompanyUserId = x.EvaluationCompanyUserId,
                    EvaluationHomeUserId = x.EvaluationHomeUserId,
                    EvaluationHomeState=x.EvaluationHomeState,
                    EvaluationCompanyState=x.EvaluationCompanyState,
                    IsHeadOffice = x.IsHeadOffice,
                    FirstTimeStaffHubFeedback = x.FirstTimeStaffHubFeedback,
                    HomeNetwork = x.HomeNetwork,
                    CountCall = x.CountCall,
                    PhoneOther = x.PhoneOther,
                    BankBranch = x.BankBranch,
                    StatusTelesales = x.StatusTelesales,
                    FieldHo = x.FieldHo,
                    TelesalesPushAt = x.TelesalesPushAt,
                    HubEmployeeCallFirst = x.HubEmployeeCallFirst,
                    LoanStatusDetail = x.LoanStatusDetail,
                    LoanStatusDetailChild = x.LoanStatusDetailChild,
                    ProductDetailId = x.ProductDetailId,
                    IsSim = x.IsSim,
                    HubPushAt = x.HubPushAt,
                    ApproverStatusDetail = x.ApproverStatusDetail,
                    ScanContractStatus = x.ScanContractStatus,
                    BoundTelesaleId = x.BoundTelesaleId,
                    Email = x.Email,
                    ReMarketingLoanBriefId = x.ReMarketingLoanBriefId,
                    Tid = x.Tid,
                    TypeLoanSupport = x.TypeLoanSupport,
                    TransactionState = x.TransactionState,
                    IsTransactionsSecured = x.IsTransactionsSecured,
                    Ltv = x.Ltv,
                    EsignState = x.EsignState,
                    EsignBorrowerContract = x.EsignBorrowerContract,
                };
            }
        }
    }


    public class LoanBriefTelesale
    {
        public int LoanBriefId { get; set; }
        public int? ProductId { get; set; }
        public int? RateTypeId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? UserId { get; set; }
        public string Username { get; set; }
        public DistrictDTO District { get; set; }
        public ProvinceDTO Province { get; set; }
        public LoanProductDTO Product { get; set; }
        public bool IsRetrieved { get; set; }
        public int LoanActionId { get; set; }
        public LoanActionDetail LoanAction { get; set; }
        public DateTimeOffset? StartTime { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public int ScheduleId { get; set; }
        public DateTimeOffset? ScheduleTime { get; set; }
        public int? ScheduleStatus { get; set; }
        public int? Status { get; set; }
        public int? BoundTelesaleId { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefTelesale>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefTelesale()
                {
                    DistrictId = x.DistrictId,
                    FullName = x.FullName,
                    LoanAmount = x.LoanAmount,
                    LoanBriefId = x.LoanBriefId,
                    LoanTime = x.LoanTime,
                    Phone = x.Phone,
                    ProductId = x.ProductId,
                    ProvinceId = x.ProvinceId,
                    RateTypeId = x.RateTypeId,
                    District = x.District != null ? new DistrictDTO()
                    {
                        Name = x.District.Name
                    } : null,
                    Province = x.Province != null ? new ProvinceDTO()
                    {
                        Name = x.Province.Name
                    } : null,
                    Product = x.Product != null ? new LoanProductDTO()
                    {
                        Name = x.Product.Name
                    } : null
                };
            }
        }
    }

    public class LoanBriefSearchDetail : LoanBriefDTO
    {
        public LoanProductDTO LoanProduct { get; set; }
        public ProvinceDTO Province { get; set; }
        public DistrictDTO District { get; set; }
        public WardDTO Ward { get; set; }
        public UserDTO UserHub { get; set; }
        public UserDTO CoordinatorUser { get; set; }
        public UserDTO BoundTelesale { get; set; }

        public ShopDTO Hub { get; set; }
        public string strStatus { get; set; }
        public int? CodeId { get; set; }
        public string LenderName { get; set; }
        public int? IsExceptions { get; set; }
        public bool IsPush { get; set; }
        public bool IsCancel { get; set; }
        public bool IsBack { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefSearchDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefSearchDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    ProductId = x.ProductId,
                    FullName = x.FullName,
                    Gender = x.Gender,
                    Phone = x.Phone,
                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    ProvinceId = x.ProvinceId,
                    DistrictId = x.DistrictId,
                    WardId = x.WardId,
                    UtmSource = x.UtmSource,
                    CreatedTime = x.CreatedTime,
                    Status = x.Status,
                    IsLocate = x.IsLocate,
                    CoordinatorUserId = x.CoordinatorUserId,
                    HubEmployeeId = x.HubEmployeeId,
                    BoundTelesaleId = x.BoundTelesaleId,
                    InProcess = x.InProcess.GetValueOrDefault(),
                    IsReborrow = x.IsReborrow,
                    IsTrackingLocation = x.IsTrackingLocation,
                    ScheduleTime = x.ScheduleTime,
                    LoanProduct = x.Product != null ? new LoanProductDTO()
                    {
                        LoanProductId = x.Product.LoanProductId,
                        Name = x.Product.Name
                    } : null,
                    Province = x.Province != null ? new ProvinceDTO()
                    {
                        ProvinceId = x.Province.ProvinceId,
                        Name = x.Province.Name
                    } : null,
                    District = x.District != null ? new DistrictDTO()
                    {
                        DistrictId = x.District.DistrictId,
                        Name = x.District.Name
                    } : null,
                    Ward = x.Ward != null ? new WardDTO()
                    {
                        WardId = x.Ward.WardId,
                        Name = x.Ward.Name
                    } : null,
                    UserHub = x.HubEmployee != null ? new UserDTO()
                    {
                        FullName = x.HubEmployee.FullName,
                        UserId = x.HubEmployee.UserId,
                        Username = x.HubEmployee.Username
                    } : null,
                    CoordinatorUser = x.CoordinatorUser != null ? new UserDTO()
                    {
                        FullName = x.CoordinatorUser.FullName,
                        UserId = x.CoordinatorUser.UserId,
                        Username = x.CoordinatorUser.Username

                    } : null,
                    BoundTelesale = x.BoundTelesale != null ? new UserDTO()
                    {
                        FullName = x.BoundTelesale.FullName,
                        UserId = x.BoundTelesale.UserId,
                        Username = x.BoundTelesale.Username,
                    } : null,
                    Hub = x.Hub != null ? new ShopDTO()
                    {
                        ShopId = x.Hub.ShopId,
                        Name = x.Hub.Name
                    } : null,
                    StatusTelesales = x.StatusTelesales,
                    DetailStatusTelesales = x.DetailStatusTelesales,
                    CodeId = x.CodeId,
                    LenderName = x.LenderName,
                    IsExceptions = x.IsExceptions
                };
            }
        }
        public static Expression<Func<LoanBrief, LoanBriefDetail>> ProjectionHubDetail
        {
            get
            {
                return x => new LoanBriefDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    ProductId = x.ProductId,
                    RateTypeId = x.RateTypeId,
                    FullName = x.FullName,
                    Gender = x.Gender,
                    Phone = x.Phone,
                    Dob = x.Dob,
                    NationalCard = x.NationalCard,
                    NationalCardDate = x.NationalCardDate,
                    NationCardPlace = x.NationCardPlace,
                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    ConsultantRate = x.ConsultantRate,
                    ServiceRate = x.ServiceRate,
                    LoanRate = x.LoanRate,
                    ProvinceId = x.ProvinceId,
                    DistrictId = x.DistrictId,
                    WardId = x.WardId,
                    UtmSource = x.UtmSource,
                    UtmMedium = x.UtmMedium,
                    UtmCampaign = x.UtmCampaign,
                    UtmTerm = x.UtmTerm,
                    UtmContent = x.UtmContent,
                    ScoreCredit = x.ScoreCredit,
                    CreatedTime = x.CreatedTime,
                    UpdatedTime = x.UpdatedTime,
                    Status = x.Status,
                    //AffCode = x.AffCode,
                    AffStatus = x.AffStatus,
                    ScheduleTime = x.ScheduleTime,
                    LoanBriefCancelAt = x.LoanBriefCancelAt,
                    LoanBriefCancelBy = x.LoanBriefCancelBy,
                    TelesalesPushAt = x.TelesalesPushAt,
                    LoanBriefComment = x.LoanBriefComment,
                    PipelineState = x.PipelineState,
                    CoordinatorUserId = x.CoordinatorUserId,
                    HubEmployeeId = x.HubEmployeeId,
                    LastSendRequestActive = x.LastSendRequestActive,
                    DeviceStatus = x.DeviceStatus,
                    DeviceId = x.DeviceId,
                    IsLocate = x.IsLocate,
                    IsTrackingLocation = x.IsTrackingLocation,
                    LoanProduct = x.Product != null ? new LoanProductDTO()
                    {
                        LoanProductId = x.Product.LoanProductId,
                        Name = x.Product.Name
                    } : null,
                    Province = x.Province != null ? new ProvinceDTO()
                    {
                        ProvinceId = x.Province.ProvinceId,
                        Name = x.Province.Name
                    } : null,
                    District = x.District != null ? new DistrictDTO()
                    {
                        DistrictId = x.District.DistrictId,
                        Name = x.District.Name
                    } : null,
                    UserHub = x.HubEmployee != null ? new UserDTO()
                    {
                        FullName = x.HubEmployee.FullName,
                        UserId = x.HubEmployee.UserId
                    } : null
                };
            }
        }


    }

    public class CountLoanTLSDetail
    {
        public int TLS_WaitAdvice { get; set; }
        public int TLS_WaitPREPARE { get; set; }
        public int TLS_Cancel { get; set; }
        public int HotLeadResponded { get; set; }
        public int HotLeadNoResponse { get; set; }
        public int AutoCall { get; set; }
        public int Chatbot { get; set; }

        public int TotalLoanbrief { get; set; }
    }

    public class LoanBriefDetailNew : LoanBrief
    {
        public List<DocumentTypeDetail> ListDocumentType { get; set; }
        public List<LoanBriefFileDetail> ListLoanBriefFile { get; set; }
        public List<RelativeFamilyDetail> ListRelativeFamilys { get; set; }
        public List<BrandProductDetail> ListBrandProduct { get; set; }
        public List<LoanProduct> ListLoanProduct { get; set; }
        public List<PlatformTypeDetail> ListPlatformType { get; set; }
        public ResultEkyc ResultEkyc { get; set; }
        public LoanbriefLender LoanbriefLender { get; set; }
        public int TopUpOfLoanbriefID { get; set; }
        public int TypeServiceCall { get; set; }
        public decimal? TotalMoneyNeedPayment { get; set; }
        public List<CheckLoanInformation> CheckLoanInformation { get; set; }
        public string AddressNationalCard { get; set; }
        public bool? BuyInsuranceProperty { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefDetailNew>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanBriefDetailNew()
                {
                    LoanBriefId = x.LoanBriefId,
                    RateTypeId = x.RateTypeId,
                    FullName = x.FullName,
                    Gender = x.Gender,
                    Phone = x.Phone,
                    Dob = x.Dob,
                    NationalCard = x.NationalCard,
                    NationalCardDate = x.NationalCardDate,
                    NationCardPlace = x.NationCardPlace,
                    LoanAmount = x.LoanAmount,
                    ProductId = x.ProductId,
                    BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                    LoanAmountFirst = x.LoanAmountFirst,
                    LoanAmountExpertise = x.LoanAmountExpertise,
                    LoanAmountExpertiseAi = x.LoanAmountExpertiseAi,
                    LoanAmountExpertiseLast = x.LoanAmountExpertiseLast,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    CreatedTime = x.CreatedTime,
                    RateMoney = x.RateMoney,
                    ScoreCredit = x.ScoreCredit,
                    PlatformType = x.PlatformType,
                    TypeLoanBrief = x.TypeLoanBrief,
                    RatePercent = x.RatePercent,
                    BankAccountName = x.BankAccountName,
                    BankAccountNumber = x.BankAccountNumber,
                    BankCardNumber = x.BankCardNumber,
                    ReceivingMoneyType = x.ReceivingMoneyType,
                    IsTrackingLocation = x.IsTrackingLocation,
                    IsLocate = x.IsLocate,
                    IsCheckCic = x.IsCheckCic,
                    LoanPurpose = x.LoanPurpose,
                    Status = x.Status,
                    DeviceStatus = x.DeviceStatus,
                    DeviceId = x.DeviceId,
                    LoanBriefCompany = x.LoanBriefCompany ?? null,
                    LoanBriefDocument = x.LoanBriefDocument ?? null,
                    PhoneOther = x.PhoneOther,
                    BankBranch = x.BankBranch,
                    ProductDetailId = x.ProductDetailId,
                    IsSim = x.IsSim,
                    LoanBriefHousehold = x.LoanBriefHousehold != null ? new LoanBriefHousehold()
                    {
                        LoanBriefHouseholdId = x.LoanBriefHousehold.LoanBriefHouseholdId,
                        Address = x.LoanBriefHousehold.Address,
                        ProvinceId = x.LoanBriefHousehold.ProvinceId,
                        DistrictId = x.LoanBriefHousehold.DistrictId,
                        WardId = x.LoanBriefHousehold.WardId,
                        AppartmentNumber = x.LoanBriefHousehold.AppartmentNumber,
                        Status = x.LoanBriefHousehold.Status,
                        Ownership = x.LoanBriefHousehold.Ownership,
                        JsonInfoFamilyKalapa = x.LoanBriefHousehold.JsonInfoFamilyKalapa,
                        RelationshipHouseOwner = x.LoanBriefHousehold.RelationshipHouseOwner,
                        BirdayHouseOwner = x.LoanBriefHousehold.BirdayHouseOwner,
                        FullNameHouseOwner = x.LoanBriefHousehold.FullNameHouseOwner,
                        Province = new Province()
                        {
                            ProvinceId = x.LoanBriefHousehold.Province.ProvinceId,
                            Name = x.LoanBriefHousehold.Province.Name,
                            Type = x.LoanBriefHousehold.Province.Type,
                        },
                        District = new District()
                        {
                            DistrictId = x.LoanBriefHousehold.District.DistrictId,
                            Name = x.LoanBriefHousehold.District.Name,
                            Type = x.LoanBriefHousehold.District.Type,
                        },
                        Ward = new Ward()
                        {
                            WardId = x.LoanBriefHousehold.Ward.WardId,
                            Name = x.LoanBriefHousehold.Ward.Name,
                            Type = x.LoanBriefHousehold.Ward.Type,
                        },
                    } : null,
                    LoanBriefJob = x.LoanBriefJob != null ? new LoanBriefJob()
                    {
                        LoanBriefJobId = x.LoanBriefJob.LoanBriefJobId,
                        TotalIncome = x.LoanBriefJob.TotalIncome,
                        CompanyName = x.LoanBriefJob.CompanyName,
                        CompanyPhone = x.LoanBriefJob.CompanyPhone,
                        CompanyAddress = x.LoanBriefJob.CompanyAddress,
                        CompanyAddressGoogleMap = x.LoanBriefJob.CompanyAddressGoogleMap,
                        CompanyAddressLatLng = x.LoanBriefJob.CompanyAddressLatLng,
                        CompanyTaxCode = x.LoanBriefJob.CompanyTaxCode,
                        WorkingAddress = x.LoanBriefJob.WorkingAddress,
                        DepartmentName = x.LoanBriefJob.DepartmentName,
                        WorkingTime = x.LoanBriefJob.WorkingTime,
                        ImcomeType = x.LoanBriefJob.ImcomeType,
                        Description = x.LoanBriefJob.Description,
                        CompanyInsurance = x.LoanBriefJob.CompanyInsurance,
                        WorkLocation = x.LoanBriefJob.WorkLocation,
                        BusinessPapers = x.LoanBriefJob.BusinessPapers,
                        JobDescriptionId = x.LoanBriefJob.JobDescriptionId,
                        Job = new Job()
                        {
                            JobId = x.LoanBriefJob.Job.JobId,
                            Name = x.LoanBriefJob.Job.Name
                        },
                        CompanyDistrict = new District()
                        {
                            DistrictId = x.LoanBriefJob.CompanyDistrict.DistrictId,
                            Name = x.LoanBriefJob.CompanyDistrict.Name,
                            Type = x.LoanBriefJob.CompanyDistrict.Type,
                        },
                        CompanyProvince = new Province()
                        {
                            ProvinceId = x.LoanBriefJob.CompanyProvince.ProvinceId,
                            Name = x.LoanBriefJob.CompanyProvince.Name,
                            Type = x.LoanBriefJob.CompanyProvince.Type,
                        },
                        CompanyWard = new Ward()
                        {
                            WardId = x.LoanBriefJob.CompanyWard.WardId,
                            Name = x.LoanBriefJob.CompanyWard.Name,
                            Type = x.LoanBriefJob.CompanyWard.Type,
                        },
                    } : null,
                    LoanBriefProperty = x.LoanBriefProperty != null ? new LoanBriefProperty()
                    {
                        LoanBriefPropertyId = x.LoanBriefProperty.LoanBriefPropertyId,
                        PlateNumber = x.LoanBriefProperty.PlateNumber,
                        Brand = new BrandProduct()
                        {
                            Id = x.LoanBriefProperty.Brand.Id,
                            Name = x.LoanBriefProperty.Brand.Name
                        },
                        Product = new Product()
                        {
                            Id = x.LoanBriefProperty.Product.Id,
                            Name = x.LoanBriefProperty.Product.Name,
                            FullName = x.LoanBriefProperty.Product.FullName,
                            ProductTypeId = x.LoanBriefProperty.Product.ProductTypeId,
                        },
                        PlateNumberCar = x.LoanBriefProperty.PlateNumberCar,
                        CarManufacturer = x.LoanBriefProperty.CarManufacturer,
                        CarName = x.LoanBriefProperty.CarName,
                        Chassis = x.LoanBriefProperty.Chassis,
                        Engine = x.LoanBriefProperty.Engine,
                        BuyInsuranceProperty = x.LoanBriefProperty.BuyInsuranceProperty

                    } : null,
                    LoanBriefRelationship = x.LoanBriefRelationship ?? null,
                    LoanBriefResident = x.LoanBriefResident ?? null,
                    Customer = x.Customer ?? null,
                    LoanBriefQuestionScript = x.LoanBriefQuestionScript ?? null,
                    //LoanBriefNote = x.LoanBriefNote ?? null,
                    //LoanBriefFiles = x.LoanBriefFiles ?? null,
                    //InProcess = x.InProcess,
                    LeadScore = x.LeadScore,
                    LabelScore = x.LabelScore,
                    ScoreRange = x.ScoreRange,
                    ContractGinno = x.ContractGinno,
                    ResultLocation = x.ResultLocation,
                    ResultRuleCheck = x.ResultRuleCheck,
                    FirstTimeHubFeedBack = x.FirstTimeHubFeedBack,
                    ValueCheckQualify = x.ValueCheckQualify,
                    Product = x.Product ?? null,
                    Province = x.Province ?? null,
                    District = x.District ?? null,
                    Ward = x.Ward ?? null,
                    Bank = x.Bank ?? null,
                    LabelRequestLocation = x.LabelRequestLocation,
                    LenderName = x.LenderName,
                    CodeId = x.CodeId,
                    FeeInsuranceOfCustomer = x.FeeInsuranceOfCustomer,
                    Passport = x.Passport,
                    TypeRemarketing = x.TypeRemarketing,
                    ReMarketingLoanBriefId = x.ReMarketingLoanBriefId,
                    TopUpOfLoanbriefID = x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp ? x.ReMarketingLoanBriefId.Value : 0,
                    IsReborrow = x.IsReborrow,
                    AddressNationalCard = x.LoanBriefResident != null && !string.IsNullOrEmpty(x.LoanBriefResident.AddressNationalCard) ? x.LoanBriefResident.AddressNationalCard : "",
                    BuyInsuranceLender = x.BuyInsuranceLender.GetValueOrDefault(true),
                    BuyInsuranceProperty = x.LoanBriefProperty != null ? x.LoanBriefProperty.BuyInsuranceProperty : null,
                    FeeInsuranceOfProperty = x.FeeInsuranceOfProperty,
                };
            }
        }


    }
    public class LoanBriefAffiliateQualify
    {
        public int loancredit_id { get; set; }
        public string aff_code { get; set; }
        public string aff_sid { get; set; }
        public DateTime create_date { get; set; }
        public DateTime update_date { get; set; }
        public string reason_cancel_name { get; set; }
        public string upload_file
        {
            get
            {
                var str = String.Empty;
                if (value_check_qlf > 0)
                {
                    bool uploadImage = (value_check_qlf & (int)EnumCheckQualify.UploadImage) == (int)EnumCheckQualify.UploadImage;

                    if (uploadImage == false)
                        str = "NO";
                    else
                        str = "YES";
                }
                return str;
            }
        }
        public int value_check_qlf { get; set; }
        public string lead_qualify
        {
            get
            {
                var qualify = String.Empty;
                if (value_check_qlf > 0)
                {
                    bool noQualify = (value_check_qlf & (int)EnumCheckQualify.NoQualify) == (int)EnumCheckQualify.NoQualify;
                    bool uploadImage = (value_check_qlf & (int)EnumCheckQualify.UploadImage) == (int)EnumCheckQualify.UploadImage;
                    bool needLoan = (value_check_qlf & (int)EnumCheckQualify.NeedLoan) == (int)EnumCheckQualify.NeedLoan;
                    bool inAge = (value_check_qlf & (int)EnumCheckQualify.InAge) == (int)EnumCheckQualify.InAge;
                    bool inAreaSupport = (value_check_qlf & (int)EnumCheckQualify.InAreaSupport) == (int)EnumCheckQualify.InAreaSupport;
                    bool haveMotobike = (value_check_qlf & (int)EnumCheckQualify.HaveMotobike) == (int)EnumCheckQualify.HaveMotobike;
                    bool haveOriginalVehicleRegistration = (value_check_qlf & (int)EnumCheckQualify.HaveOriginalVehicleRegistration) == (int)EnumCheckQualify.HaveOriginalVehicleRegistration;


                    if (uploadImage == true)
                    {
                        if (needLoan == true && inAge == true && inAreaSupport == true
                        && haveMotobike == true && haveOriginalVehicleRegistration == true)
                        {
                            qualify = "QL có chứng từ";
                        }
                        else
                        {
                            qualify = "Không QL có chứng từ";
                        }
                    }
                    else
                    {
                        if (needLoan == true && inAge == true && inAreaSupport == true
                        && haveMotobike == true && haveOriginalVehicleRegistration == true)
                        {
                            qualify = "QL không chứng từ";
                        }
                        else
                        {
                            qualify = "Không QL không chứng từ";
                        }
                    }


                }
                return qualify;
            }
        }
        public int status
        {
            get
            {
                int status = 0;
                if (update_date == null || update_date == DateTime.MinValue)
                {
                    return status = 0;
                }
                if (value_check_qlf > 0)
                {
                    if (value_check_qlf == 248 || value_check_qlf == 252)
                    {
                        return status = 1;
                    }
                    else
                    {
                        return status = 2;
                    }
                }
                if (update_date != null && value_check_qlf == 0 && reason_cancel_name != null)
                {
                    return status = 2;
                }
                return status;
            }
        }
        public static Expression<Func<LoanBrief, LoanBriefAffiliateQualify>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefAffiliateQualify()
                {
                    loancredit_id = x.LoanBriefId,
                    update_date = x.FirstProcessingTime.Value,
                    aff_sid = x.AffSid,
                    aff_code = x.AffCode,
                    create_date = x.CreatedTime.Value.DateTime,
                    reason_cancel_name = x.ReasonCancelNavigation.Reason,
                    value_check_qlf = x.ValueCheckQualify.Value
                };

            }
        }
    }

    public class LoanBriefAffiliateDisbursement
    {
        public int loancredit_id { get; set; }
        public string aff_code { get; set; }
        public string aff_sid { get; set; }
        public string status_name { get; set; }
        public DateTime create_date { get; set; }
        public DateTime update_date { get; set; }
        public string reason_cancel_name { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefAffiliateDisbursement>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefAffiliateDisbursement()
                {
                    loancredit_id = x.LoanBriefId,
                    update_date = x.FirstProcessingTime.Value,
                    aff_sid = x.AffSid,
                    aff_code = x.AffCode,
                    create_date = x.CreatedTime.Value.DateTime,
                    reason_cancel_name = x.ReasonCancelNavigation.Reason,
                    status_name = x.Status == (int)EnumLoanStatus.DISBURSED ? "success" : x.Status == (int)EnumLoanStatus.CANCELED ? "reject" : x.Status == (int)EnumLoanStatus.INIT ? "registered" : "processing"
                };

            }
        }

    }

    public class LoanBriefAffiliateDisbursementReasonEng
    {
        public int loancredit_id { get; set; }
        public string aff_code { get; set; }
        public string aff_sid { get; set; }
        public string status_name { get; set; }
        public DateTime create_date { get; set; }
        public DateTime update_date { get; set; }
        public string reason_cancel_name { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefAffiliateDisbursementReasonEng>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefAffiliateDisbursementReasonEng()
                {
                    loancredit_id = x.LoanBriefId,
                    update_date = x.FirstProcessingTime.Value,
                    aff_sid = x.AffSid,
                    aff_code = x.AffCode,
                    create_date = x.CreatedTime.Value.DateTime,
                    reason_cancel_name = x.ReasonCancelNavigation.ReasonEng,
                    status_name = x.Status == (int)EnumLoanStatus.DISBURSED ? "success" : x.Status == (int)EnumLoanStatus.CANCELED ? "reject" : x.Status == (int)EnumLoanStatus.INIT ? "registered" : "processing"
                };

            }
        }

    }

    public class LoanBriefAffiliateRentracksXeMay
    {
        public int loancredit_id { get; set; }
        public string aff_code { get; set; }
        public string aff_sid { get; set; }
        public string status_name { get; set; }
        public DateTime create_date { get; set; }
        public string reason_cancel_name { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefAffiliateRentracksXeMay>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefAffiliateRentracksXeMay()
                {
                    loancredit_id = x.LoanBriefId,
                    aff_sid = x.AffSid,
                    aff_code = x.AffCode,
                    create_date = x.CreatedTime.Value.DateTime,
                    reason_cancel_name = x.ReasonCancelNavigation.Reason,
                    status_name = (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH) ? "success" : x.Status == (int)EnumLoanStatus.CANCELED ? "reject" : x.Status == (int)EnumLoanStatus.INIT ? "registered" : "processing"
                };

            }
        }

    }

    public class LoanBriefAffiliateRentracksOto
    {
        public int loancredit_id { get; set; }
        public string aff_code { get; set; }
        public string aff_sid { get; set; }
        public long loan_amount { get; set; }
        public string status_name { get; set; }
        public DateTime create_date { get; set; }
        public string reason_cancel_name { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefAffiliateRentracksOto>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefAffiliateRentracksOto()
                {
                    loancredit_id = x.LoanBriefId,
                    aff_sid = x.AffSid,
                    aff_code = x.AffCode,
                    loan_amount = Convert.ToInt64(x.LoanAmount.Value),
                    create_date = x.CreatedTime.Value.DateTime,
                    reason_cancel_name = x.ReasonCancelNavigation.Reason,
                    status_name = (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH) ? "success" : x.Status == (int)EnumLoanStatus.CANCELED ? "reject" : x.Status == (int)EnumLoanStatus.INIT ? "registered" : "processing"
                };

            }
        }

    }
    public class LoanBriefAffiliateDinos
    {
        public int loancredit_id { get; set; }
        public string aff_code { get; set; }
        public string aff_sid { get; set; }
        public long loan_amount { get; set; }
        public string status_name { get; set; }
        public DateTime create_date { get; set; }
        public string reason_cancel_name { get; set; }
        public int product_id { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefAffiliateDinos>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefAffiliateDinos()
                {
                    loancredit_id = x.LoanBriefId,
                    aff_sid = x.AffSid,
                    aff_code = x.AffCode,
                    loan_amount = x.LoanAmount.HasValue ? Convert.ToInt64(x.LoanAmount.Value) : 10000000,
                    create_date = x.CreatedTime.Value.DateTime,
                    reason_cancel_name = x.ReasonCancelNavigation.Reason,
                    status_name = (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH) ? "success" : x.Status == (int)EnumLoanStatus.CANCELED ? "reject" : x.Status == (int)EnumLoanStatus.INIT ? "registered" : "processing",
                    product_id = (x.ProductId == 2 || x.ProductId == 5) ? 2 : x.ProductId == 8 ? 8 : 0,
                };

            }
        }
    }

    public class StatusLoanBriefAffTima
    {
        public int loancredit_id { get; set; }
        public string reason_name_cancel { get; set; }
        public DateTime? date_disbursement { get; set; }
        public int status { get; set; }
        public int productId { get; set; }
        public long loan_ammount { get; set; }
        public DateTime date_create { get; set; }
        public int? codeId { get; set; }
        public static Expression<Func<LoanBrief, StatusLoanBriefAffTima>> ProjectionDetail
        {
            get
            {
                return x => new StatusLoanBriefAffTima()
                {
                    loancredit_id = x.LoanBriefId,
                    reason_name_cancel = x.ReasonCancelNavigation.Reason,
                    date_disbursement = x.DisbursementAt,
                    status = x.Status == (int)EnumLoanStatus.DISBURSED ? 1 : x.Status == (int)EnumLoanStatus.FINISH ? 2 : x.Status == (int)EnumLoanStatus.CANCELED ? 3 : 4,
                    productId = x.ProductId != null ? x.ProductId.Value : 0,
                    loan_ammount = x.LoanAmount != null ? (long)x.LoanAmount : 0,
                    date_create = x.CreatedTime != null ? x.CreatedTime.Value.DateTime : DateTime.MinValue,
                    codeId = x.CodeId
                };

            }
        }
    }

    public class ListFileDetail
    {
        public int LoanBriefId { get; set; }
        public List<DocumentTypeDetail> ListDocumentType { get; set; }
        public List<LoanBriefFileDetail> ListLoanBriefFile { get; set; }
        public List<CheckLoanInformation> ListCheckImage { get; set; }
        public List<DocumentTypeNew> ListDocumentNew { get; set; }
        public List<DocumentMapInfo> ListDocumentNewV2 { get; set; }

        public string MessageNotify { get; set; }
        public List<int> ListImageWarning { get; set; }
        public bool? ScanContractStatus { get; set; }
    }

    public class LoanBriefCancell : LoanBrief
    {
        public LoanProductDTO LoanProduct { get; set; }
        public UserDTO UserCancel { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefCancell>> ProjectionDetail
        {

            get
            {
                return x => new LoanBriefCancell()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    NationalCard = x.NationalCard,
                    LoanAmount = x.LoanAmount,
                    LoanAmountFirst = x.LoanAmountFirst,
                    CreatedTime = x.CreatedTime,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    ProductId = x.ProvinceId,
                    ProvinceId = x.ProvinceId,
                    DistrictId = x.DistrictId,
                    WardId = x.WardId,
                    Status = x.Status,
                    LoanBriefCancelAt = x.LoanBriefCancelAt,
                    LoanBriefCancelBy = x.LoanBriefCancelBy,
                    TelesalesPushAt = x.TelesalesPushAt,
                    LoanBriefComment = x.LoanBriefComment,
                    CoordinatorUserId = x.CoordinatorUserId,
                    HubEmployeeId = x.HubEmployeeId,
                    BoundTelesaleId = x.BoundTelesaleId,
                    InProcess = x.InProcess.GetValueOrDefault(),
                    BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                    CountCall = x.CountCall,
                    LoanProduct = x.Product != null ? new LoanProductDTO()
                    {
                        LoanProductId = x.Product.LoanProductId,
                        Name = x.Product.Name
                    } : null,
                    Province = x.Province != null ? new Province()
                    {
                        ProvinceId = x.Province.ProvinceId,
                        Name = x.Province.Name
                    } : null,
                    District = x.District != null ? new District()
                    {
                        DistrictId = x.District.DistrictId,
                        Name = x.District.Name
                    } : null,
                    Ward = x.Ward != null ? new Ward()
                    {
                        WardId = x.Ward.WardId,
                        Name = x.Ward.Name
                    } : null,
                    UserCancel = x.BoundTelesale != null ? new UserDTO()
                    {
                        UserId = x.LoanBriefCancelBy.Value,
                        FullName = x.BoundTelesale.FullName,
                        GroupId = x.BoundTelesale.GroupId
                    } : null

                };
            }
        }


    }

    public class LoanBriefAffiliateCPS
    {
        public int loancredit_id { get; set; }
        public string aff_code { get; set; }
        public string aff_sid { get; set; }
        public int status { get; set; }
        public DateTime create_date { get; set; }
        public DateTime update_date { get; set; }
        public string reason_cancel_name { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefAffiliateCPS>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefAffiliateCPS()
                {
                    loancredit_id = x.LoanBriefId,
                    update_date = x.FirstProcessingTime.Value,
                    aff_sid = x.AffSid,
                    aff_code = x.AffCode,
                    create_date = x.CreatedTime.Value.DateTime,
                    reason_cancel_name = x.ReasonCancelNavigation.Reason,
                    status = x.Status == (int)EnumLoanStatus.DISBURSED ? 1 : x.Status == (int)EnumLoanStatus.FINISH ? 1 :
                    (x.Status == (int)EnumLoanStatus.CANCELED
                    || (x.ProductId == (int)EnumProductCredit.MotorCreditType_CC.GetHashCode()
                    || x.ProductId == (int)EnumProductCredit.MotorCreditType_KCC.GetHashCode()
                    || x.ProductId == (int)EnumProductCredit.MotorCreditType_KGT.GetHashCode())) ? 2 : 0
                };

            }
        }
    }

    public class LoanBriefAllMMOTima
    {
        public string loanbriefId { get; set; }
        public string full_name { get; set; }
        public string city_name { get; set; }
        public string district_name { get; set; }
        public string product_name { get; set; }
        public string aff_code { get; set; }
        public int status { get; set; }
        public string reason_cancel { get; set; }
        public DateTime? create_date { get; set; }
        public DateTime? date_disbursement { get; set; }
        public decimal loan_ammount { get; set; }
        public int productId { get; set; }
        public int cityId { get; set; }
        public int districtId { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefAllMMOTima>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefAllMMOTima()
                {
                    loanbriefId = "HĐ-" + x.LoanBriefId.ToString(),
                    full_name = x.FullName,
                    city_name = x.Province.Name,
                    district_name = x.District.Name,
                    product_name = x.ProductId == 2 ? "Gói vay xe máy" : x.ProductId == 5 ? "Gói vay xe máy" : x.ProductId == 8 ? "Gói vay ô tô" : x.ProductId == 28 ? "Gói vay xe máy" : "Không xác định",
                    aff_code = x.AffCode,
                    status = x.Status == (int)EnumLoanStatus.CANCELED ? 0 : x.Status == (int)EnumLoanStatus.DISBURSED ? 1 : x.Status == (int)EnumLoanStatus.FINISH ? 2 : 3,
                    reason_cancel = x.ReasonCancelNavigation.Reason,
                    create_date = x.CreatedTime.Value.DateTime,
                    date_disbursement = x.DisbursementAt,
                    loan_ammount = x.LoanAmount.Value,
                    productId = x.ProductId.Value,
                    cityId = x.ProvinceId.Value,
                    districtId = x.DistrictId.Value
                };

            }
        }

    }

    public class LoanBriefAllMMOTimaCPQL
    {
        public string loanbriefId { get; set; }
        public string full_name { get; set; }
        public string city_name { get; set; }
        public string district_name { get; set; }
        public string product_name { get; set; }
        public string aff_code { get; set; }
        public int status { get; set; }
        public string reason_cancel { get; set; }
        public DateTime? create_date { get; set; }
        public decimal loan_ammount { get; set; }
        public int productId { get; set; }
        public int cityId { get; set; }
        public int districtId { get; set; }
        public int? value_check_qualify { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefAllMMOTimaCPQL>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefAllMMOTimaCPQL()
                {
                    loanbriefId = "HĐ-" + x.LoanBriefId.ToString(),
                    full_name = x.FullName,
                    city_name = x.Province.Name,
                    district_name = x.District.Name,
                    product_name = x.ProductId == 2 ? "Gói vay xe máy" : x.ProductId == 5 ? "Gói vay xe máy" : x.ProductId == 8 ? "Gói vay ô tô" : x.ProductId == 28 ? "Gói vay xe máy" : "Không xác định",
                    aff_code = x.AffCode,
                    status = x.Status == (int)EnumLoanStatus.CANCELED ? 0 : x.Status == (int)EnumLoanStatus.DISBURSED ? 1 : x.Status == (int)EnumLoanStatus.FINISH ? 2 : 3,
                    reason_cancel = x.ReasonCancelNavigation.Reason,
                    create_date = x.CreatedTime.Value.DateTime,
                    loan_ammount = x.LoanAmount.Value,
                    productId = x.ProductId.Value,
                    cityId = x.ProvinceId.Value,
                    districtId = x.DistrictId.Value,
                    value_check_qualify = x.ValueCheckQualify,
                };

            }
        }

    }

    //public class GetInfoLoanBriefConvertAG
    //{
    //    public string phone { get; set; }
    //    public string national_id { get; set; }
    //    public string full_name { get; set; }
    //    public int manufacturerId { get; set; }//id hãng xe 
    //    public string yearMade { get; set; }// id model xe 
    //    public string vehicle_full_name { get; set; }// tên đầy đủ của xe (vd: honda airblade 2019)
    //    public string current_address { get; set; }//địa chỉ hiện tại  
    //    public int current_address_city_id { get; set; }// id của tỉnh hiện tại
    //    public int current_address_district_id { get; set; }//id của thành huyện hiện tại
    //    public int current_address_ward_id { get; set; }//id của thành xã hiện tại
    //    public int home_ownership_type_id { get; set; }//id loại hình sở hữu nhà
    //    public string reference_phone { get; set; }//số điện thoại người tham chiếu
    //    public string reference_name { get; set; }//tên người tham chiếu
    //    public int reference_relationship_id { get; set; }//id của mối quan hệ tham chiếu 
    //    public int bank_id { get; set; }// id của ngân hàng 
    //    public string bank_account_number { get; set; }//số tài khoản ngân hàng 
    //    public long loan_money { get; set; }//số tiền vay 
    //    public int total_loan_time { get; set; }// thời gian vay  
    //    public int company_address_city_id { get; set; }
    //    public int company_address_district_id { get; set; }
    //    public int company_address_ward_id { get; set; }
    //    public string company_address { get; set; }
    //    public string company_name { get; set; }
    //    public int loanCreditId { get; set; }
    //    public int customerId { get; set; }
    //    public DateTime createDate { get; set; }
    //    public int Status { get; set; }
    //    public string StatusText { get; set; }
    //    public int? ProductId { get; set; }

    //    public string ProductName
    //    {
    //        get
    //        {
    //            if (ProductId > 0 && Enum.IsDefined(typeof(EnumProductCredit), ProductId))
    //            {
    //                return Common.Helpers.ExtentionHelper.GetDescription((EnumProductCredit)ProductId);
    //            }
    //            return "";
    //        }
    //    }

    //    public long createDateToMilliseconds
    //    {
    //        get
    //        {
    //            long result = 0;
    //            try
    //            {
    //                result = (long)(createDate.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
    //            }
    //            catch { result = 0; }
    //            return result;
    //        }
    //    }
    //    public List<ListLoanBriefFileConvertAG> loanbrieffiles { get; set; }
    //    public int LoanID { get; set; }

    //    public static Expression<Func<LoanBrief, GetInfoLoanBriefConvertAG>> ProjectionDetail
    //    {
    //        get
    //        {
    //            return x => new GetInfoLoanBriefConvertAG()
    //            {
    //                phone = x.Phone,
    //                national_id = x.NationalCard,
    //                full_name = x.FullName,
    //                manufacturerId = x.LoanBriefProperty != null ? x.LoanBriefProperty.BrandId.Value : 0,
    //                yearMade = (x.LoanBriefProperty != null && x.LoanBriefProperty.Product != null) ? x.LoanBriefProperty.Product.Year.ToString() : null,
    //                vehicle_full_name = (x.LoanBriefProperty != null && x.LoanBriefProperty.Product != null) ? x.LoanBriefProperty.Product.FullName.ToString() : null,
    //                current_address = x.LoanBriefResident != null ? x.LoanBriefResident.Address : null,
    //                current_address_city_id = x.LoanBriefResident != null ? x.LoanBriefResident.ProvinceId.Value : 0,
    //                current_address_district_id = x.LoanBriefResident != null ? x.LoanBriefResident.DistrictId.Value : 0,
    //                current_address_ward_id = x.LoanBriefResident != null ? x.LoanBriefResident.WardId.Value : 0,
    //                home_ownership_type_id = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType.Value : 0,
    //                reference_phone = (x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0 && x.LoanBriefRelationship.ToList()[0].Phone != null) ? x.LoanBriefRelationship.ToList()[0].Phone : null,
    //                reference_name = (x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0 && x.LoanBriefRelationship.ToList()[0].FullName != null) ? x.LoanBriefRelationship.ToList()[0].FullName : null,
    //                reference_relationship_id = (x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0 && x.LoanBriefRelationship.ToList()[0].RelationshipType != null) ? x.LoanBriefRelationship.ToList()[0].RelationshipType.Value : 0,
    //                bank_id = x.BankId.Value,
    //                bank_account_number = x.BankAccountNumber,
    //                loan_money = (long)x.LoanAmount.Value,
    //                total_loan_time = x.LoanTime.Value,
    //                company_address_city_id = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyProvinceId.Value : 0,
    //                company_address_district_id = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyDistrictId.Value : 0,
    //                company_address_ward_id = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyWardId.Value : 0,
    //                company_address = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyAddress : null,
    //                company_name = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyName : null,
    //                loanCreditId = x.LoanBriefId,
    //                customerId = x.CustomerId.Value,
    //                createDate = x.CreatedTime.Value.DateTime,
    //                Status = x.Status.Value,
    //                StatusText = Common.Helpers.ExtentionHelper.GetDescription((EnumLoanStatus)x.Status),
    //                LoanID = x.LmsLoanId.Value,
    //                ProductId = x.ProductId
    //            };

    //        }
    //    }
    //}
    public class ListLoanBriefFile
    {
        public int loanbrief_file_Id { get; set; }
        public int loanbrief_Id { get; set; }
        public int typeId { get; set; }
        public string url { get; set; }
        public static Expression<Func<LoanBriefFiles, ListLoanBriefFile>> ProjectionDetail
        {
            get
            {
                return x => new ListLoanBriefFile()
                {
                    loanbrief_Id = x.LoanBriefId.Value,
                    loanbrief_file_Id = x.Id,
                    typeId = x.TypeId.Value,
                    url = "http://file.tima.vn" + x.FilePath
                };

            }
        }
    }

    public class LoanBriefDisbursementNotSynchorizeAff
    {
        public int loanbriefId { get; set; }
        public string full_name { get; set; }
        public string city_name { get; set; }
        public string district_name { get; set; }
        public string aff_code { get; set; }
        public DateTime? create_date { get; set; }
        public DateTime? date_disbursement { get; set; }
        public decimal loan_ammount { get; set; }
        public int productId { get; set; }
        public int codeId { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefDisbursementNotSynchorizeAff>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefDisbursementNotSynchorizeAff()
                {
                    loanbriefId = x.LoanBriefId,
                    full_name = x.FullName,
                    city_name = x.Province.Name,
                    district_name = x.District.Name,
                    aff_code = x.AffCode,
                    create_date = x.CreatedTime.Value.DateTime,
                    date_disbursement = x.DisbursementAt,
                    loan_ammount = x.LoanAmount.Value,
                    productId = x.ProductId.Value,
                    codeId = x.CodeId.Value
                };

            }
        }
    }

    public class LoanBriefQLFNotSynchorizeAff
    {
        public int loanbriefId { get; set; }
        public string full_name { get; set; }
        public string city_name { get; set; }
        public string district_name { get; set; }
        public string aff_code { get; set; }
        public DateTime? create_date { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefQLFNotSynchorizeAff>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefQLFNotSynchorizeAff()
                {
                    loanbriefId = x.LoanBriefId,
                    full_name = x.FullName,
                    city_name = x.Province.Name,
                    district_name = x.District.Name,
                    aff_code = x.AffCode,
                    create_date = x.CreatedTime.Value.DateTime,
                };

            }
        }
    }

    public class ReportLoanHomeTimaModel
    {
        public int loanbriefId { get; set; }
        public string full_name { get; set; }
        public string phone { get; set; }
        public string city_name { get; set; }
        public string district_name { get; set; }
        public string product_name { get; set; }
        public string loan_time { get; set; }
        public string create_date { get; set; }
        public string loan_amount { get; set; }
        public static Expression<Func<LoanBrief, ReportLoanHomeTimaModel>> ProjectionDetail
        {
            get
            {
                return x => new ReportLoanHomeTimaModel()
                {
                    loanbriefId = x.LoanBriefId,
                    full_name = x.FullName,
                    phone = x.Phone,
                    city_name = x.Province != null ? x.Province.Name : "Hà Nội",
                    district_name = x.District != null ? x.District.Name : "Ba Đình",
                    product_name = (x.ProductId == 2 || x.ProductId == 5) ? "Vay theo xe máy" : x.ProductId == 8 ? "Vay theo ô tô" : "Không xác định",
                    loan_time = x.LoanTime.HasValue ? string.Format("{0} tháng", x.LoanTime) : "12 tháng",
                    create_date = x.CreatedTime != null ? x.CreatedTime.Value.ToString("HH:mm dd/MM/yyyy") : DateTime.Now.ToString("HH: mm dd/MM/yyyy"),
                    loan_amount = x.LoanAmount != null ? x.LoanAmount.Value.ToString("###,0") : "10,000,000",
                };
            }
        }
    }

    public class LoanBriefDetailDebt : LoanBriefDTO
    {
        public LoanProductDTO LoanProduct { get; set; }
        public ProvinceDTO Province { get; set; }
        public DistrictDTO District { get; set; }
        public UserDTO UserHub { get; set; }
        public UserDTO UserFieldHo { get; set; }
        public ShopDTO Hub { get; set; }
        public Customer Customer { get; set; }
        public int? LmsLoanId { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefDetailDebt>> ProjectionDetailDebt
        {
            get
            {
                return x => new LoanBriefDetailDebt()
                {
                    LoanBriefId = x.LoanBriefId,
                    ProductId = x.ProductId,
                    FullName = x.FullName,
                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    LmsLoanId = x.LmsLoanId,

                    ProvinceId = x.ProvinceId,
                    DistrictId = x.DistrictId,
                    WardId = x.WardId,

                    CreatedTime = x.CreatedTime,
                    Status = x.Status,
                    HubEmployeeId = x.HubEmployeeId,

                    DisbursementAt = x.DisbursementAt,

                    FieldHo = x.FieldHo,

                    LoanProduct = x.Product != null ? new LoanProductDTO()
                    {
                        LoanProductId = x.Product.LoanProductId,
                        Name = x.Product.Name
                    } : null,
                    Province = x.Province != null ? new ProvinceDTO()
                    {
                        ProvinceId = x.Province.ProvinceId,
                        Name = x.Province.Name
                    } : null,
                    District = x.District != null ? new DistrictDTO()
                    {
                        DistrictId = x.District.DistrictId,
                        Name = x.District.Name
                    } : null,

                    UserHub = x.HubEmployee != null ? new UserDTO()
                    {
                        FullName = x.HubEmployee.FullName,
                        UserId = x.HubEmployee.UserId
                    } : null,
                    //UserFieldHo = x.FieldHo > 0 ? new UserDTO()
                    //{
                    //    FullName = x.FieldHoNavigation.FullName,
                    //    UserId = x.FieldHoNavigation.UserId,
                    //    Phone = x.FieldHoNavigation.Phone
                    //} : null,
                    Hub = x.Hub != null ? new ShopDTO()
                    {
                        ShopId = x.Hub.ShopId,
                        Name = x.Hub.Name
                    } : null
                };
            }
        }

    }
    public class GetDisbursementAfter
    {
        public Decimal? TotalMoney { get; set; }
        public List<LoanBriefDetail> lstLoan { get; set; }
    }

    public class LoanBriefForAppraiser
    {
        public int LoanBriefId { get; set; }
        public int? ProductCreditId { get; set; }
        public int? ProductPropertyId { get; set; }
        public decimal? LoanAmountExpertiseAi { get; set; }
        public int? TypeOwnerShipId { get; set; }
        public int? ProductDetailId { get; set; }
        public int? Status { get; set; }
        public string CarManufacturer { get; set; }
        public string CarName { get; set; }
        public string ProductPropertyName { get; set; }
        public string BrandPropertyName { get; set; }
        public int? TypeRemarketing { get; set; }
        public int? RemarketingLoanbriefId { get; set; }
        public bool? IsReborrow { get; set; }
        public int? Ltv { get; set; }
        public decimal? LoanAmount { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefForAppraiser>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanBriefForAppraiser()
                {
                    LoanBriefId = x.LoanBriefId,
                    ProductCreditId = x.ProductId,
                    ProductDetailId = x.ProductDetailId,
                    Status = x.Status,
                    LoanAmountExpertiseAi = x.LoanAmountExpertiseAi,
                    TypeOwnerShipId = x.LoanBriefResident != null && x.LoanBriefResident.ResidentType.HasValue ? x.LoanBriefResident.ResidentType.Value : 0,
                    ProductPropertyId = x.LoanBriefProperty != null && x.LoanBriefProperty.ProductId.HasValue ? x.LoanBriefProperty.ProductId.Value : 0,
                    CarManufacturer = x.LoanBriefProperty != null ? x.LoanBriefProperty.CarManufacturer : "",
                    CarName = x.LoanBriefProperty != null ? x.LoanBriefProperty.CarName : "",
                    ProductPropertyName = x.LoanBriefProperty != null && x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.FullName : "",
                    BrandPropertyName = x.LoanBriefProperty != null && x.LoanBriefProperty.Brand != null ? x.LoanBriefProperty.Brand.Name : "",
                    TypeRemarketing = x.TypeRemarketing,
                    RemarketingLoanbriefId = x.ReMarketingLoanBriefId,
                    IsReborrow = x.IsReborrow,
                    Ltv = x.Ltv,
                    LoanAmount = x.LoanAmount
                };
            }
        }
    }
    public class LoanBriefPrematureInterest
    {
        public string FullName { get; set; }
        public bool? IsLocate { get; set; }
        public int? Status { get; set; }
        public int? LmsLoanId { get; set; }
        public int LoanBriefId { get; set; }
        public int? CodeId { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefPrematureInterest>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefPrematureInterest()
                {
                    FullName = x.FullName,
                    IsLocate = x.IsLocate,
                    Status = x.Status,
                    LmsLoanId = x.LmsLoanId,
                    LoanBriefId = x.LoanBriefId,
                    CodeId = x.CodeId,
                };

            }
        }
    }

    public class LoanBriefRelationshipAll
    {
        public LoanBriefResident LoanBriefResident { get; set; }
        public LoanBriefProperty LoanBriefProperty { get; set; }
        public LoanBriefHousehold LoanBriefHousehold { get; set; }
        public LoanBriefJob LoanBriefJob { get; set; }
        public List<LoanBriefRelationship> LoanBriefRelationship { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefRelationshipAll>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefRelationshipAll()
                {
                    LoanBriefResident = x.LoanBriefResident ?? null,
                    LoanBriefProperty = x.LoanBriefProperty ?? null,
                    LoanBriefHousehold = x.LoanBriefHousehold ?? null,
                    LoanBriefJob = x.LoanBriefJob ?? null,
                    LoanBriefRelationship = x.LoanBriefRelationship.ToList() ?? null,
                };

            }
        }
    }

    public class LoanBriefBasicInfo
    {
        public int LoanBriefId { get; set; }
        public int? ProductId { get; set; }
        public int? CustomerId { get; set; }
        public int? RateTypeId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string PhoneOther { get; set; }
        public DateTime? Dob { get; set; }
        public int? Gender { get; set; }
        public string NationalCard { get; set; }
        public string NationCardPlace { get; set; }
        public bool? ScanContractStatus { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public int? CreateBy { get; set; }
        public int? BoundTelesaleId { get; set; }
        public int? Status { get; set; }
        public int? PlatformType { get; set; }
        public bool? IsTrackingLocation { get; set; }
        public bool? IsLocate { get; set; }
        public bool? BuyInsurenceCustomer { get; set; }
        public bool? IsReborrow { get; set; }
        public int? CoordinatorUserId { get; set; }
        public int? HubEmployeeId { get; set; }
        public int? InProcess { get; set; }
        public int? CodeId { get; set; }
        public int? LmsLoanId { get; set; }
        public int? HubId { get; set; }
        public int? TypeRemarketing { get; set; }
        public int? CountCall { get; set; }
        public DateTime? HubEmployeeCallFirst { get; set; }
        public DateTime? FirstProcessingTime { get; set; }
        public DateTime? FirstTimeStaffHubFeedback { get; set; }
        public DateTime? FirstTimeHubFeedBack { get; set; }
        public DateTime? LoanBriefCancelAt { get; set; }
        public DateTime? DisbursementAt { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }

        public string DeviceId { get; set; }//Mã thiết bị
        public int? StatusTelesales { get; set; }// TRạng thái đơn của tls
        public int? DetailStatusTelesales { get; set; }// trạng thái chi tiết đơn của tls
        public int? LoanStatusDetail { get; set; }//Trạng thái đơn của hub
        public int? LoanStatusChild { get; set; }//trạng thái chi tiế đơn của hub
        public int? EsignState { get; set; }//trạng thái chi tiế đơn của hub
        public int? DeviceStatus { get; set; }
        public string ContractGinno { get; set; }
        public int? TypeLoanSupport { get; set; }
        public int? ReMarketingLoanBriefId { get; set; }
public int? ProductDetailId { get; set; }
        public int? LoanStatusDetailChild { get; set; }
        public string EsignAgreementId { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefBasicInfo>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanBriefBasicInfo()
                {
                    LoanBriefId = x.LoanBriefId,
                    ProductId = x.ProductId,
                    CustomerId = x.CustomerId,
                    RateTypeId = x.RateTypeId,
                    FullName = x.FullName,
                    Gender = x.Gender,
                    Phone = x.Phone,
                    Dob = x.Dob,
                    NationalCard = x.NationalCard,
                    NationCardPlace = x.NationCardPlace,
                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    ProvinceId = x.ProvinceId,
                    DistrictId = x.DistrictId,
                    WardId = x.WardId,
                    CreateBy = x.CreateBy,
                    Status = x.Status,
                    PlatformType = x.PlatformType,
                    IsTrackingLocation = x.IsTrackingLocation,
                    IsLocate = x.IsLocate,
                    CoordinatorUserId = x.CoordinatorUserId,
                    BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                    HubEmployeeId = x.HubEmployeeId,
                    InProcess = x.InProcess,
                    CodeId = x.CodeId,
                    LmsLoanId = x.LmsLoanId,
                    HubId = x.HubId,
                    TypeRemarketing = x.TypeRemarketing,
                    IsReborrow = x.IsReborrow,
                    PhoneOther = x.PhoneOther,
                    ScanContractStatus = x.ScanContractStatus,
                    BoundTelesaleId = x.BoundTelesaleId,
                    CountCall = x.CountCall,
                    FirstProcessingTime = x.FirstProcessingTime,
                    HubEmployeeCallFirst = x.HubEmployeeCallFirst,
                    FirstTimeStaffHubFeedback = x.FirstTimeStaffHubFeedback,
                    FirstTimeHubFeedBack = x.FirstTimeHubFeedBack,
                    DetailStatusTelesales = x.DetailStatusTelesales,
                    LoanStatusDetail = x.LoanStatusDetail,
                    LoanStatusChild = x.LoanStatusDetailChild,
                    StatusTelesales = x.StatusTelesales,
                    EsignState = x.EsignState,
                    LoanBriefCancelAt = x.LoanBriefCancelAt,
                    DisbursementAt = x.DisbursementAt,
                    CreatedTime = x.CreatedTime,
                    DeviceId = x.DeviceId,
                    DeviceStatus = x.DeviceStatus,
                    ContractGinno = x.ContractGinno,
                    TypeLoanSupport = x.TypeLoanSupport,
                    ReMarketingLoanBriefId = x.ReMarketingLoanBriefId,
                    LoanStatusDetailChild = x.LoanStatusDetailChild,
                    EsignAgreementId = x.Customer != null ? x.Customer.EsignAgreementId : null,
 ProductDetailId = x.ProductDetailId
                };
            }
        }
    }


    //public class LoanBriefDetailV2 : LoanBrief
    //{

    //    public static Expression<Func<LoanBrief, LoanBriefDetailV2>> ProjectionViewDetail
    //    {
    //        get
    //        {
    //            return x => new LoanBriefDetailV2()
    //            {
    //                LoanBriefId = x.LoanBriefId,
    //                RateTypeId = x.RateTypeId,
    //                FullName = x.FullName,
    //                Gender = x.Gender,
    //                Phone = x.Phone,
    //                Dob = x.Dob,
    //                NationalCard = x.NationalCard,
    //                NationalCardDate = x.NationalCardDate,
    //                NationCardPlace = x.NationCardPlace,
    //                LoanAmount = x.LoanAmount,
    //                ProductId = x.ProductId,
    //                BuyInsurenceCustomer = x.BuyInsurenceCustomer,
    //                LoanAmountFirst = x.LoanAmountFirst,
    //                LoanAmountExpertise = x.LoanAmountExpertise,
    //                LoanAmountExpertiseAi = x.LoanAmountExpertiseAi,
    //                LoanAmountExpertiseLast = x.LoanAmountExpertiseLast,
    //                LoanTime = x.LoanTime,
    //                FromDate = x.FromDate,
    //                ToDate = x.ToDate,
    //                CreatedTime = x.CreatedTime,
    //                RateMoney = x.RateMoney,
    //                ScoreCredit = x.ScoreCredit,
    //                PlatformType = x.PlatformType,
    //                TypeLoanBrief = x.TypeLoanBrief,
    //                RatePercent = x.RatePercent,
    //                BankAccountName = x.BankAccountName,
    //                BankAccountNumber = x.BankAccountNumber,
    //                BankCardNumber = x.BankCardNumber,
    //                ReceivingMoneyType = x.ReceivingMoneyType,
    //                IsTrackingLocation = x.IsTrackingLocation,
    //                IsLocate = x.IsLocate,
    //                IsCheckCic = x.IsCheckCic,
    //                LoanPurpose = x.LoanPurpose,
    //                Status = x.Status,
    //                DeviceStatus = x.DeviceStatus,
    //                DeviceId = x.DeviceId,
    //                PhoneOther = x.PhoneOther,
    //                BankBranch = x.BankBranch,
    //                ProductDetailId = x.ProductDetailId,
    //                IsSim = x.IsSim,
    //                LoanBriefHousehold = x.LoanBriefHousehold != null ? new LoanBriefHousehold()
    //                {
    //                    LoanBriefHouseholdId = x.LoanBriefHousehold.LoanBriefHouseholdId,
    //                    Address = x.LoanBriefHousehold.Address,
    //                    AppartmentNumber = x.LoanBriefHousehold.AppartmentNumber,
    //                    Ownership = x.LoanBriefHousehold.Ownership,
    //                    RelationshipHouseOwner = x.LoanBriefHousehold.RelationshipHouseOwner,
    //                    BirdayHouseOwner = x.LoanBriefHousehold.BirdayHouseOwner,
    //                    FullNameHouseOwner = x.LoanBriefHousehold.FullNameHouseOwner,
    //                    Province = new Province()
    //                    {
    //                        Name = x.LoanBriefHousehold.Province.Name
    //                    },
    //                    District = new District()
    //                    {
    //                        Name = x.LoanBriefHousehold.District.Name
    //                    },
    //                    Ward = new Ward()
    //                    {
    //                        Name = x.LoanBriefHousehold.Ward.Name
    //                    },
    //                } : null,
    //                LoanBriefJob = x.LoanBriefJob != null ? new LoanBriefJob()
    //                {
    //                    LoanBriefJobId = x.LoanBriefJob.LoanBriefJobId,
    //                    TotalIncome = x.LoanBriefJob.TotalIncome,
    //                    CompanyName = x.LoanBriefJob.CompanyName,
    //                    CompanyPhone = x.LoanBriefJob.CompanyPhone,
    //                    CompanyAddress = x.LoanBriefJob.CompanyAddress,
    //                    CompanyAddressGoogleMap = x.LoanBriefJob.CompanyAddressGoogleMap,
    //                    CompanyAddressLatLng = x.LoanBriefJob.CompanyAddressLatLng,
    //                    WorkingAddress = x.LoanBriefJob.WorkingAddress,
    //                    DepartmentName = x.LoanBriefJob.DepartmentName,
    //                    WorkingTime = x.LoanBriefJob.WorkingTime,
    //                    ImcomeType = x.LoanBriefJob.ImcomeType,
    //                    Description = x.LoanBriefJob.Description,
    //                    CompanyInsurance = x.LoanBriefJob.CompanyInsurance,
    //                    WorkLocation = x.LoanBriefJob.WorkLocation,
    //                    BusinessPapers = x.LoanBriefJob.BusinessPapers,
    //                    JobDescriptionId = x.LoanBriefJob.JobDescriptionId,
    //                    Job = new Job()
    //                    {
    //                        JobId = x.LoanBriefJob.Job.JobId,
    //                        Name = x.LoanBriefJob.Job.Name
    //                    },
    //                    CompanyDistrict = new District()
    //                    {
    //                        DistrictId = x.LoanBriefJob.CompanyDistrict.DistrictId,
    //                        Name = x.LoanBriefJob.CompanyDistrict.Name
    //                    },
    //                    CompanyProvince = new Province()
    //                    {
    //                        ProvinceId = x.LoanBriefJob.CompanyProvince.ProvinceId,
    //                        Name = x.LoanBriefJob.CompanyProvince.Name
    //                    },
    //                    CompanyWard = new Ward()
    //                    {
    //                        WardId = x.LoanBriefJob.CompanyWard.WardId,
    //                        Name = x.LoanBriefJob.CompanyWard.Name
    //                    },
    //                } : null,
    //                LoanBriefProperty = x.LoanBriefProperty != null ? new LoanBriefProperty()
    //                {
    //                    LoanBriefPropertyId = x.LoanBriefProperty.LoanBriefPropertyId,
    //                    PlateNumber = x.LoanBriefProperty.PlateNumber,
    //                    Brand = new BrandProduct()
    //                    {
    //                        Id = x.LoanBriefProperty.Brand.Id,
    //                        Name = x.LoanBriefProperty.Brand.Name
    //                    },
    //                    Product = new Product()
    //                    {
    //                        Id = x.LoanBriefProperty.Product.Id,
    //                        Name = x.LoanBriefProperty.Product.Name,
    //                        FullName = x.LoanBriefProperty.Product.FullName,
    //                        ProductTypeId = x.LoanBriefProperty.Product.ProductTypeId,
    //                    },
    //                    PlateNumberCar = x.LoanBriefProperty.PlateNumberCar,
    //                    CarManufacturer = x.LoanBriefProperty.CarManufacturer,
    //                    CarName = x.LoanBriefProperty.CarName,
    //                    Chassis = x.LoanBriefProperty.Chassis,
    //                    Engine = x.LoanBriefProperty.Engine,
    //                    BuyInsuranceProperty = x.LoanBriefProperty.BuyInsuranceProperty

    //                } : null,
    //                LoanBriefRelationship = x.LoanBriefRelationship ?? null,
    //                LoanBriefResident = x.LoanBriefResident ?? null,
    //                Customer = x.Customer ?? null,
    //                LoanBriefQuestionScript = x.LoanBriefQuestionScript ?? null,
    //                LeadScore = x.LeadScore,
    //                LabelScore = x.LabelScore,
    //                ScoreRange = x.ScoreRange,
    //                ContractGinno = x.ContractGinno,
    //                ResultLocation = x.ResultLocation,
    //                ResultRuleCheck = x.ResultRuleCheck,
    //                FirstTimeHubFeedBack = x.FirstTimeHubFeedBack,
    //                ValueCheckQualify = x.ValueCheckQualify,
    //                Product = x.Product ?? null,
    //                Province = x.Province ?? null,
    //                District = x.District ?? null,
    //                Ward = x.Ward ?? null,
    //                Bank = x.Bank ?? null,
    //                LabelRequestLocation = x.LabelRequestLocation,
    //                LenderName = x.LenderName,
    //                CodeId = x.CodeId,
    //                FeeInsuranceOfCustomer = x.FeeInsuranceOfCustomer,
    //                Passport = x.Passport,
    //                TypeRemarketing = x.TypeRemarketing,
    //                ReMarketingLoanBriefId = x.ReMarketingLoanBriefId,
    //                IsReborrow = x.IsReborrow,
    //                BuyInsuranceLender = x.BuyInsuranceLender.GetValueOrDefault(true),
    //                FeeInsuranceOfProperty = x.FeeInsuranceOfProperty,
    //            };
    //        }
    //    }
    //}

    public class LoanBriefForDetail
    {
        public List<DocumentTypeDetail> ListDocumentType { get; set; }
        public List<LoanBriefFileDetail> ListLoanBriefFile { get; set; }
        public List<RelativeFamilyDetail> ListRelativeFamilys { get; set; }
        public List<BrandProductDetail> ListBrandProduct { get; set; }
        public List<LoanProduct> ListLoanProduct { get; set; }
        public List<PlatformTypeDetail> ListPlatformType { get; set; }
        public LoanbriefLender LoanbriefLender { get; set; }
        public List<CheckLoanInformation> CheckLoanInformation { get; set; }
        public LoanBriefResidentForDetail LoanBriefResident { get; set; }
        public LoanBriefHouseholdForDetail LoanBriefHousehold { get; set; }
        public LoanBriefPropertyForDetail LoanBriefProperty { get; set; }
        public LoanBriefCompanyForDetail LoanBriefCompany { get; set; }
        public LoanBriefQuestionScriptForDetail LoanBriefQuestionScript { get; set; }
        public CustomerForDetail Customer { get; set; }
        public LoanBriefJobForDetail LoanBriefJob { get; set; }
        public List<LoanBriefRelationshipForDetail> LoanBriefRelationship { get; set; }
        public int TypeServiceCall { get; set; }
        public int LoanBriefId { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public int? TypeProvince { get; set; }
        public int? TypeDistrict { get; set; }
        public int? TypeWard { get; set; }
        public string FullName { get; set; }
        public string NationalCard { get; set; }
        public DateTime? Dob { get; set; }
        public string ResultRuleCheck { get; set; }
        public string ResultLocation { get; set; }
        public string LabelRequestLocation { get; set; }
        public int? ScoreRange { get; set; }
        public int? ReceivingMoneyType { get; set; }
        public string BankName { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }
        public string BankBranch { get; set; }
        public int? RateTypeId { get; set; }
        public string ProductName { get; set; }
        public int? ProductId { get; set; }
        public bool? IsReborrow { get; set; }
        public int? ProductDetailId { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public bool? BuyInsurenceCustomer { get; set; }
        public int? Status { get; set; }
        public decimal? FeeInsuranceOfCustomer { get; set; }
        public decimal? FeeInsuranceOfProperty { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTime? ToDate { get; set; }
        public string LenderName { get; set; }
        public int? CodeId { get; set; }
        public decimal? RateMoney { get; set; }
        public decimal? LoanAmountExpertiseAi { get; set; }
        public decimal? LoanAmountExpertise { get; set; }
        public decimal? LoanAmountExpertiseLast { get; set; }
        public bool? IsLocate { get; set; }
        public int? CustomerId { get; set; }
        public int? PlatformType { get; set; }
        public int? TypeRemarketing { get; set; }
        public int? TypeLoanBrief { get; set; }
        public int? ReMarketingLoanBriefId { get; set; }
        public string Phone { get; set; }
        public int? CountCall { get; set; }
        public string NationCardPlace { get; set; }
        public string PhoneOther { get; set; }
        public int? Gender { get; set; }
        public bool? IsSim { get; set; }
        public int? Frequency { get; set; }
        public double? RatePercent { get; set; }
        public string JobDesctionName { get; set; }
        public string DeviceId { get; set; }
        public string EsignBorrowerContract { get; set; }
        public string Email { get; set; }
        public string EsignLenderContract { get; set; }
        public double? LenderRate { get; set; }
        public int? TransactionState { get; set; }
        public bool? IsTransactionsSecured { get; set; }
        public int? Ltv { get; set; }
        public int? HubId { get; set; }
        public int? HubEmployeeId { get; set; }
        public decimal? CreditScoring { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefForDetail>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanBriefForDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    ProvinceName = x.Province != null ? x.Province.Name : null,
                    DistrictName = x.District != null ? x.District.Name : null,
                    WardName = x.Ward != null ? x.Ward.Name : null,
                    TypeProvince = x.Province != null ? x.Province.Type : null,
                    TypeDistrict = x.District != null ? x.District.Type : null,
                    TypeWard = x.Ward != null ? x.Ward.Type : null,
                    FullName = x.FullName,
                    NationalCard = x.NationalCard,
                    Dob = x.Dob,
                    ResultRuleCheck = x.ResultRuleCheck,
                    ResultLocation = x.ResultLocation,
                    LabelRequestLocation = x.LabelRequestLocation,
                    ScoreRange = x.ScoreRange,
                    ReceivingMoneyType = x.ReceivingMoneyType,
                    BankName = x.Bank != null ? x.Bank.Name : null,
                    BankAccountName = x.BankAccountName,
                    BankAccountNumber = x.BankAccountNumber,
                    BankCardNumber = x.BankCardNumber,
                    BankBranch = x.BankBranch,
                    RateTypeId = x.RateTypeId,
                    ProductName = x.Product != null ? x.Product.Name : null,
                    ProductId = x.ProductId,
                    IsReborrow = x.IsReborrow,
                    ProductDetailId = x.ProductDetailId,
                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                    Status = x.Status,
                    FeeInsuranceOfCustomer = x.FeeInsuranceOfCustomer,
                    FeeInsuranceOfProperty = x.FeeInsuranceOfProperty,
                    CreatedTime = x.CreatedTime,
                    ToDate = x.ToDate,
                    LenderName = x.LenderName,
                    CodeId = x.CodeId,
                    RateMoney = x.RateMoney,
                    LoanAmountExpertiseAi = x.LoanAmountExpertiseAi,
                    LoanAmountExpertise = x.LoanAmountExpertise,
                    LoanAmountExpertiseLast = x.LoanAmountExpertiseLast,
                    IsLocate = x.IsLocate,
                    CustomerId = x.CustomerId,
                    PlatformType = x.PlatformType,
                    TypeRemarketing = x.TypeRemarketing,
                    TypeLoanBrief = x.TypeLoanBrief,
                    ReMarketingLoanBriefId = x.ReMarketingLoanBriefId,
                    Phone = x.Phone,
                    CountCall = x.CountCall,
                    NationCardPlace = x.NationCardPlace,
                    PhoneOther = x.PhoneOther,
                    Gender = x.Gender,
                    IsSim = x.IsSim,
                    Frequency = x.Frequency,
                    RatePercent = x.RatePercent,
                    DeviceId = x.DeviceId,
                    Email = x.Email,
                    EsignLenderContract = x.EsignLenderContract,
                    LenderRate = x.LenderRate,
                    TransactionState = x.TransactionState,
                    IsTransactionsSecured = x.IsTransactionsSecured,
                    LoanBriefResident = x.LoanBriefResident != null ? new LoanBriefResidentForDetail()
                    {
                        BillElectricityId = x.LoanBriefResident.BillElectricityId,
                        BillWaterId = x.LoanBriefResident.BillWaterId,
                        ResidentType = x.LoanBriefResident.ResidentType,
                        LivingTime = x.LoanBriefResident.LivingTime,
                        Address = x.LoanBriefResident.Address,
                        CustomerShareLocation = x.LoanBriefResident.CustomerShareLocation,
                        AddressGoogleMap = x.LoanBriefResident.AddressGoogleMap,
                        AddressLatLng = x.LoanBriefResident.AddressLatLng,
                        LivingWith = x.LoanBriefResident.LivingWith,
                    } : null,
                    LoanBriefHousehold = x.LoanBriefHousehold != null ? new LoanBriefHouseholdForDetail()
                    {
                        ProvinceName = x.LoanBriefHousehold.Province != null ? x.LoanBriefHousehold.Province.Name : null,
                        DistrictName = x.LoanBriefHousehold.District != null ? x.LoanBriefHousehold.District.Name : null,
                        WardName = x.LoanBriefHousehold.Ward != null ? x.LoanBriefHousehold.Ward.Name : null,
                        TypeProvince = x.LoanBriefHousehold.Province != null ? x.LoanBriefHousehold.Province.Type : null,
                        TypeDistrict = x.LoanBriefHousehold.District != null ? x.LoanBriefHousehold.District.Type : null,
                        TypeWard = x.LoanBriefHousehold.Ward != null ? x.LoanBriefHousehold.Ward.Type : null,
                        Address = x.LoanBriefHousehold.Address,
                        FullNameHouseOwner = x.LoanBriefHousehold.FullNameHouseOwner,
                        RelationshipHouseOwner = x.LoanBriefHousehold.RelationshipHouseOwner,
                        BirdayHouseOwner = x.LoanBriefHousehold.BirdayHouseOwner,
                    } : null,
                    LoanBriefProperty = x.LoanBriefProperty != null ? new LoanBriefPropertyForDetail()
                    {
                        BrandName = x.LoanBriefProperty.Brand != null ? x.LoanBriefProperty.Brand.Name : null,
                        PlateNumber = x.LoanBriefProperty.PlateNumber,
                        BuyInsuranceProperty = x.LoanBriefProperty.BuyInsuranceProperty,
                        CarManufacturer = x.LoanBriefProperty.CarManufacturer,
                        CarName = x.LoanBriefProperty.CarName,
                        ProductFullName = x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.FullName : null,
                        PlateNumberCar = x.LoanBriefProperty.PlateNumberCar,
                        Chassis = x.LoanBriefProperty.Chassis,
                        Engine = x.LoanBriefProperty.Engine,
                        OwnerFullName = x.LoanBriefProperty.OwnerFullName,
                        MotobikeCertificateNumber = x.LoanBriefProperty.MotobikeCertificateNumber,
                        MotobikeCertificateDate = x.LoanBriefProperty.MotobikeCertificateDate,
                        MotobikeCertificateAddress = x.LoanBriefProperty.MotobikeCertificateAddress,
                        PostRegistration = x.LoanBriefProperty.PostRegistration,
                        Description = x.LoanBriefProperty.Description,
                    } : null,
                    LoanBriefCompany = x.LoanBriefCompany != null ? new LoanBriefCompanyForDetail()
                    {
                        LoanBriefId = x.LoanBriefCompany.LoanBriefId,
                        CompanyName = x.LoanBriefCompany.CompanyName,
                        CareerBusiness = x.LoanBriefCompany.CareerBusiness,
                        BusinessCertificationDate = x.LoanBriefCompany.BusinessCertificationDate,
                        BusinessCertificationAddress = x.LoanBriefCompany.BusinessCertificationAddress,
                        HeadOffice = x.LoanBriefCompany.HeadOffice,
                        Address = x.LoanBriefCompany.Address,
                        CompanyShareholder = x.LoanBriefCompany.CompanyShareholder,
                        BirthdayShareholder = x.LoanBriefCompany.BirthdayShareholder,
                        CardNumberShareholder = x.LoanBriefCompany.CardNumberShareholder,
                        CardNumberShareholderDate = x.LoanBriefCompany.CardNumberShareholderDate,
                        PlaceOfBirthShareholder = x.LoanBriefCompany.PlaceOfBirthShareholder,
                    } : null,
                    LoanBriefQuestionScript = x.LoanBriefQuestionScript != null ? new LoanBriefQuestionScriptForDetail()
                    {
                        QuestionCarOwnership = x.LoanBriefQuestionScript.QuestionCarOwnership,
                        Appraiser = x.LoanBriefQuestionScript.Appraiser,
                        DocumentBusiness = x.LoanBriefQuestionScript.DocumentBusiness,
                        QuestionHouseOwner = x.LoanBriefQuestionScript.QuestionHouseOwner,
                        DocumentSalaryBankTransfer = x.LoanBriefQuestionScript.DocumentSalaryBankTransfer,
                        DocumetSalaryCash = x.LoanBriefQuestionScript.DocumetSalaryCash,
                        CommentJob = x.LoanBriefQuestionScript.CommentJob,
                    } : null,
                    Customer = x.Customer != null ? new CustomerForDetail()
                    {
                        Passport = x.Customer.Passport,
                        FacebookAddress = x.Customer.FacebookAddress,
                        InsurenceNumber = x.Customer.InsurenceNumber,
                        InsurenceNote = x.Customer.InsurenceNote,
                        IsMerried = x.Customer.IsMerried,
                        NumberBaby = x.Customer.NumberBaby,
                        VaAccountName = x.Customer.VaAccountName,
                        VaAccountNumber = x.Customer.VaAccountNumber,
                        VaBankName = x.Customer.VaBankName,
                        GpayAccountName = x.Customer.GpayAccountName,
                        GpayAccountNumber = x.Customer.GpayAccountNumber,

                    } : null,
                    LoanBriefJob = x.LoanBriefJob != null ? new LoanBriefJobForDetail()
                    {
                        JobName = x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.Name : null,
                        CompanyName = x.LoanBriefJob.CompanyName,
                        Description = x.LoanBriefJob.Description,
                        CompanyPhone = x.LoanBriefJob.CompanyPhone,
                        CompanyTaxCode = x.LoanBriefJob.CompanyTaxCode,
                        CompanyProvinceName = x.LoanBriefJob.CompanyProvince != null ? x.LoanBriefJob.CompanyProvince.Name : null,
                        CompanyDistrictName = x.LoanBriefJob.CompanyDistrict != null ? x.LoanBriefJob.CompanyDistrict.Name : null,
                        TypeProvince = x.LoanBriefJob.CompanyProvince != null ? x.LoanBriefJob.CompanyProvince.Type : null,
                        TypeDistrict = x.LoanBriefJob.CompanyDistrict != null ? x.LoanBriefJob.CompanyDistrict.Type : null,
                        CompanyAddress = x.LoanBriefJob.CompanyAddress,
                        JobId = x.LoanBriefJob.JobId,
                        CompanyInsurance = x.LoanBriefJob.CompanyInsurance,
                        TotalIncome = x.LoanBriefJob.TotalIncome,
                        ImcomeType = x.LoanBriefJob.ImcomeType,
                        WorkLocation = x.LoanBriefJob.WorkLocation,
                        BusinessPapers = x.LoanBriefJob.BusinessPapers,
                        JobDescriptionId = x.LoanBriefJob.JobDescriptionId,
                        CompanyAddressLatLng = x.LoanBriefJob.CompanyAddressLatLng,
                        CompanyAddressGoogleMap = x.LoanBriefJob.CompanyAddressGoogleMap
                    } : null,
                    LoanBriefRelationship = x.LoanBriefRelationship != null ? x.LoanBriefRelationship
                    .Select(r => new LoanBriefRelationshipForDetail()
                    {
                        Id = r.Id,
                        RelationshipType = r.RelationshipType,
                        FullName = r.FullName,
                        RefPhoneCallRate = r.RefPhoneCallRate,
                        Phone = r.Phone,
                        Address = r.Address,
                    }).ToList() : null,
                    EsignBorrowerContract = x.EsignBorrowerContract,
                    Ltv = x.Ltv,
                    HubId = x.HubId,
                    HubEmployeeId = x.HubEmployeeId,
                    CreditScoring = x.LeadScore
                };
            }
        }
    }
    public class LoanBriefResidentForDetail
    {
        public string BillElectricityId { get; set; }
        public string BillWaterId { get; set; }
        public int? ResidentType { get; set; }
        public int? LivingTime { get; set; }
        public string Address { get; set; }
        public string CustomerShareLocation { get; set; }
        public string AddressGoogleMap { get; set; }
        public string AddressLatLng { get; set; }
        public int? LivingWith { get; set; }
    }
    public class LoanBriefHouseholdForDetail
    {
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public int? TypeProvince { get; set; }
        public int? TypeDistrict { get; set; }
        public int? TypeWard { get; set; }
        public string Address { get; set; }
        public string FullNameHouseOwner { get; set; }
        public int? RelationshipHouseOwner { get; set; }
        public string BirdayHouseOwner { get; set; }
    }
    public class LoanBriefPropertyForDetail
    {
        public string BrandName { get; set; }
        public string PlateNumber { get; set; }
        public bool? BuyInsuranceProperty { get; set; }
        public string CarManufacturer { get; set; }
        public string CarName { get; set; }
        public string ProductFullName { get; set; }
        public string PlateNumberCar { get; set; }
        public string Chassis { get; set; }
        public string Engine { get; set; }
        public string OwnerFullName { get; set; }
        public string MotobikeCertificateNumber { get; set; }
        public string MotobikeCertificateDate { get; set; }
        public string MotobikeCertificateAddress { get; set; }
        public bool? PostRegistration { get; set; }
        public string Description { get; set; }
    }
    public class LoanBriefCompanyForDetail
    {
        public int LoanBriefId { get; set; }
        public string CompanyName { get; set; }
        public string CareerBusiness { get; set; }
        public DateTime? BusinessCertificationDate { get; set; }
        public string BusinessCertificationAddress { get; set; }
        public string HeadOffice { get; set; }
        public string Address { get; set; }
        public string CompanyShareholder { get; set; }
        public DateTime? BirthdayShareholder { get; set; }
        public string CardNumberShareholder { get; set; }
        public DateTime? CardNumberShareholderDate { get; set; }
        public string PlaceOfBirthShareholder { get; set; }
    }
    public class LoanBriefQuestionScriptForDetail
    {
        public bool? QuestionCarOwnership { get; set; }
        public int? Appraiser { get; set; }
        public int? DocumentBusiness { get; set; }
        public bool? QuestionHouseOwner { get; set; }
        public int? DocumentSalaryBankTransfer { get; set; }
        public int? DocumetSalaryCash { get; set; }
        public string CommentJob { get; set; }
    }
    public class CustomerForDetail
    {
        public string Passport { get; set; }
        public string FacebookAddress { get; set; }
        public string InsurenceNumber { get; set; }
        public string InsurenceNote { get; set; }
        public int? IsMerried { get; set; }
        public int? NumberBaby { get; set; }
        public string VaAccountName { get; set; }
        public string VaAccountNumber { get; set; }
        public string VaBankName { get; set; }
        public string GpayAccountName { get; set; }
        public string GpayAccountNumber { get; set; }
    }
    public class LoanBriefJobForDetail
    {
        public string JobName { get; set; }
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyTaxCode { get; set; }
        public string CompanyProvinceName { get; set; }
        public string CompanyDistrictName { get; set; }
        public int? TypeProvince { get; set; }
        public int? TypeDistrict { get; set; }
        public string CompanyAddress { get; set; }
        public int? JobId { get; set; }
        public bool? CompanyInsurance { get; set; }
        public decimal? TotalIncome { get; set; }
        public int? ImcomeType { get; set; }
        public bool? WorkLocation { get; set; }
        public bool? BusinessPapers { get; set; }
        public int? JobDescriptionId { get; set; }
        public string CompanyAddressLatLng { get; set; }
        public string CompanyAddressGoogleMap { get; set; }
    }
    public class LoanBriefRelationshipForDetail
    {
        public int Id { get; set; }
        public int? RelationshipType { get; set; }
        public string FullName { get; set; }
        public int? RefPhoneCallRate { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    }

    public class LoanBriefForBrower
    {
        public TimeProcessingLoanBrief TimeProcessingLoan { get; set; }
        public int LoanBriefId { get; set; }
        public string HubName { get; set; }
        public string UtmSource { get; set; }
        public bool? IsReborrow { get; set; }
        public int? Status { get; set; }
        public string FullName { get; set; }
        public int? CountCall { get; set; }
        public bool UploadImage { get; set; }
        public string DistrictName { get; set; }
        public string ProvinceName { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public decimal? LoanAmount { get; set; }
        public string ProductName { get; set; }
        public bool? IsLocate { get; set; }
        public bool? IsTrackingLocation { get; set; }
        public int TypeHotLead { get; set; }
        public string UserTelesales { get; set; }
        public int? StatusTelesales { get; set; }
        public int? DetailStatusTelesales { get; set; }
        public int? PipelineState { get; set; }
        public DateTime? TelesalesPushAt { get; set; }
        public int? Step { get; set; }
        public DateTime? ScheduleTime { get; set; }
        public DateTime? LastChangeStatusTelesale { get; set; }
        public string Phone { get; set; }
        public int? HomeNetwork { get; set; }
        public string ResultRuleCheck { get; set; }
        public bool IsEdit { get; set; }
        public bool IsScript { get; set; }
        public bool IsPush { get; set; }
        public bool IsCancel { get; set; }
        public bool IsUpload { get; set; }
        public bool IsConfirmCancel { get; set; }
        public int? ProductId { get; set; }
        public int? TypeRemarketing { get; set; }
        public DateTime? FirstProcessingTime { get; set; }
        public string ResultLocation { get; set; }
        public int? BoundTelesaleId { get; set; }
        public string TId { get; set; }

        public bool IsCheckMomo { get; set; }
        public string NationalCard { get; set; }
        public DateTime? Dob { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefForBrower>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanBriefForBrower()
                {
                    LoanBriefId = x.LoanBriefId,
                    HubName = x.Hub != null ? x.Hub.Name : null,
                    UtmSource = x.UtmSource,
                    Status = x.Status,
                    FullName = x.FullName,
                    CountCall = x.CountCall,
                    ProvinceName = x.Province != null ? x.Province.Name : null,
                    DistrictName = x.District != null ? x.District.Name : null,
                    ProvinceId = x.ProvinceId,
                    DistrictId = x.DistrictId,
                    CreatedTime = x.CreatedTime,
                    LoanAmount = x.LoanAmount,
                    ProductName = x.Product != null ? x.Product.Name : null,
                    IsLocate = x.IsLocate,
                    IsTrackingLocation = x.IsTrackingLocation,
                    UserTelesales = x.BoundTelesale != null ? x.BoundTelesale.Username : null,
                    StatusTelesales = x.StatusTelesales,
                    DetailStatusTelesales = x.DetailStatusTelesales,
                    PipelineState = x.PipelineState,
                    TelesalesPushAt = x.TelesalesPushAt,
                    Step = x.Step,
                    ScheduleTime = x.ScheduleTime,
                    LastChangeStatusTelesale = x.LastChangeStatusOfTelesale,
                    Phone = x.Phone,
                    HomeNetwork = x.HomeNetwork,
                    ResultRuleCheck = x.ResultRuleCheck,
                    ProductId = x.ProductId,
                    TypeRemarketing = x.TypeRemarketing,
                    FirstProcessingTime = x.FirstProcessingTime,
                    ResultLocation = x.ResultLocation,
                    BoundTelesaleId = x.BoundTelesaleId,
                    TId = x.Tid,
                    Dob = x.Dob,
                    NationalCard = x.NationalCard,
                    IsReborrow = x.IsReborrow
                };
            }
        }
    }
    public class LoanBriefLendingOnline
    {
        public int LoanBriefId { get; set; }
        public int? CustomerId { get; set; }
        public string Phone { get; set; }
        public string FullName { get; set; }
        public int? ResidentType { get; set; }
        public int? Status { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public int? CodeId { get; set; }
        public int? LmsLoanId { get; set; }
        public int? ProductDetailId { get; set; }
        public int? ProductId { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefLendingOnline>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanBriefLendingOnline()
                {
                    LoanBriefId = x.LoanBriefId,
                    CustomerId = x.CustomerId,
                    Phone = x.Phone,
                    FullName = x.FullName,
                    ResidentType = x.LoanBriefResident != null ? x.LoanBriefResident.ResidentType : null,
                    Status = x.Status,
                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    CodeId = x.CodeId,
                    ProductId = x.ProductId,
                    ProductDetailId = x.ProductDetailId,
                    LmsLoanId = x.LmsLoanId,
                };
            }
        }
    }


    public class LoanBriefContractFinancialAgreement
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string strFullName { get; set; }
        public DateTime? Dob { get; set; }
        public string strDob { get; set; }
        public string NationalCard { get; set; }
        public string BankName { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankBranch { get; set; }
        public string ResidentAddress { get; set; }
        public string HouseholdAddress { get; set; }
        public int? LoanTime { get; set; }
        public int? ProductId { get; set; }
        public string PlateNumber { get; set; }
        public string PlateNumberCar { get; set; }
        public string Chassis { get; set; }
        public string Engine { get; set; }
        public string BrandName { get; set; }
        public string CarManufacturer { get; set; }
        public string CarName { get; set; }
        public string ProductFullName { get; set; }
        public int? ReceivingMoneyType { get; set; }
        public decimal? LoanAmount { get; set; }
        public DateTime? DisbursementAt { get; set; }
        public int? LoanPurpose { get; set; }
        public decimal? FeeInsuranceOfCustomer { get; set; }
        public bool? BuyInsurenceCustomer { get; set; }
        public bool? BuyInsuranceProperty { get; set; }
        public decimal? FeeInsuranceOfProperty { get; set; }
        public int? RateTypeId { get; set; }
        public int? LenderId { get; set; }
        public double? RatePercent { get; set; }
        public string MotobikeCertificateNumber { get; set; }
        public string MotobikeCertificateDate { get; set; }
        public string MotobikeCertificateAddress { get; set; }
        public string OwnerFullname { get; set; }
        public string Phone { get; set; }
        public string ProvinceName { get; set; }
        public int? EsignState { get; set; }
        public DateTime? DateEsignContract { get; set; }
        public bool? PostRegistration { get; set; }
        public int? TypeRemarketing { get; set; }
        public int? ReMarketingLoanBriefId { get; set; }
        public string StrLoanAmount { get; set; }
        public int? CodeIdLoanBriefOld { get; set; }
        public bool? IsLocate { get; set; }
        public double? LenderRate { get; set; }
        public double? CustomerRate { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefContractFinancialAgreement>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanBriefContractFinancialAgreement()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Dob = x.Dob,
                    NationalCard = x.NationalCard,
                    BankName = x.Bank != null ? x.Bank.Name : null,
                    BankAccountNumber = x.BankAccountNumber,
                    BankBranch = x.BankBranch,
                    ResidentAddress = x.LoanBriefResident != null ? x.LoanBriefResident.Address : null,
                    HouseholdAddress = x.LoanBriefHousehold != null ? x.LoanBriefHousehold.Address : null,
                    LoanTime = x.LoanTime,
                    ProductId = x.ProductId,
                    Chassis = x.LoanBriefProperty != null ? x.LoanBriefProperty.Chassis : null,
                    Engine = x.LoanBriefProperty != null ? x.LoanBriefProperty.Engine : null,
                    PlateNumber = x.LoanBriefProperty != null ? x.LoanBriefProperty.PlateNumber : null,
                    PlateNumberCar = x.LoanBriefProperty != null ? x.LoanBriefProperty.PlateNumberCar : null,
                    BrandName = x.LoanBriefProperty != null && x.LoanBriefProperty.Brand != null ? x.LoanBriefProperty.Brand.Name : null,
                    CarManufacturer = x.LoanBriefProperty != null ? x.LoanBriefProperty.CarManufacturer : null,
                    CarName = x.LoanBriefProperty != null ? x.LoanBriefProperty.CarName : null,
                    ProductFullName = x.LoanBriefProperty != null && x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.FullName : null,
                    ReceivingMoneyType = x.ReceivingMoneyType,
                    LoanAmount = x.LoanAmount,
                    DisbursementAt = x.DisbursementAt,
                    LoanPurpose = x.LoanPurpose,
                    BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                    BuyInsuranceProperty = x.LoanBriefProperty != null ? x.LoanBriefProperty.BuyInsuranceProperty : null,
                    RateTypeId = x.RateTypeId,
                    LenderId = x.LenderId,
                    RatePercent = x.RatePercent,
                    MotobikeCertificateNumber = x.LoanBriefProperty != null ? x.LoanBriefProperty.MotobikeCertificateNumber : null,
                    MotobikeCertificateDate = x.LoanBriefProperty != null ? x.LoanBriefProperty.MotobikeCertificateDate : null,
                    MotobikeCertificateAddress = x.LoanBriefProperty != null ? x.LoanBriefProperty.MotobikeCertificateAddress : null,
                    OwnerFullname = x.LoanBriefProperty != null ? x.LoanBriefProperty.OwnerFullName : null,
                    Phone = x.Phone,
                    ProvinceName = x.Province != null ? x.Province.Name : null,
                    EsignState = x.EsignState,
                    PostRegistration = x.LoanBriefProperty != null ? x.LoanBriefProperty.PostRegistration : null,
                    TypeRemarketing = x.TypeRemarketing,
                    ReMarketingLoanBriefId = x.ReMarketingLoanBriefId,
                    IsLocate = x.IsLocate,
                    LenderRate = x.LenderRate,
                };
            }
        }
    }
    public class LoanBriefCommittedPaper
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string NationalCard { get; set; }
        public string Phone { get; set; }
        public string HouseholdAddress { get; set; }
        public int? ProductId { get; set; }
        public string PlateNumber { get; set; }
        public string PlateNumberCar { get; set; }
        public string Chassis { get; set; }
        public string Engine { get; set; }
        public string BrandName { get; set; }
        public string CarManufacturer { get; set; }
        public string CarName { get; set; }
        public string ProductFullName { get; set; }

        public static Expression<Func<LoanBrief, LoanBriefCommittedPaper>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanBriefCommittedPaper()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    NationalCard = x.NationalCard,
                    Phone = x.Phone,
                    HouseholdAddress = x.LoanBriefHousehold != null ? x.LoanBriefHousehold.Address : null,
                    ProductId = x.ProductId,
                    Chassis = x.LoanBriefProperty != null ? x.LoanBriefProperty.Chassis : null,
                    Engine = x.LoanBriefProperty != null ? x.LoanBriefProperty.Engine : null,
                    PlateNumber = x.LoanBriefProperty != null ? x.LoanBriefProperty.PlateNumber : null,
                    PlateNumberCar = x.LoanBriefProperty != null ? x.LoanBriefProperty.PlateNumberCar : null,
                    BrandName = x.LoanBriefProperty != null && x.LoanBriefProperty.Brand != null ? x.LoanBriefProperty.Brand.Name : null,
                    CarManufacturer = x.LoanBriefProperty != null ? x.LoanBriefProperty.CarManufacturer : null,
                    CarName = x.LoanBriefProperty != null ? x.LoanBriefProperty.CarName : null,
                    ProductFullName = x.LoanBriefProperty != null && x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.FullName : null,
                };
            }
        }
    }
    public class LoanInfoCompare
    {
        public int LoanBriefId { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public DateTime? Dob { get; set; }
        public int? Gender { get; set; }
        public string NationalCard { get; set; }
        public string NationCardPlace { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public string Address { get; set; }
        public bool? BuyInsurenceCustomer { get; set; }
        public decimal? FeeInsuranceOfCustomer { get; set; }
        public string BankName { get; set; }
        public string BankAccountNumber { get; set; }
        public virtual List<LoanBriefRelationship> LoanBriefRelationship { get; set; }

        public static Expression<Func<LoanBrief, LoanInfoCompare>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanInfoCompare()
                {
                    LoanBriefId = x.LoanBriefId,
                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    Gender = x.Gender,
                    Dob = x.Dob,
                    NationalCard = x.NationalCard,
                    NationCardPlace = x.NationCardPlace,
                    ProvinceName = x.Province != null ? x.Province.Name : "",
                    DistrictName = x.District != null ? x.District.Name : "",
                    WardName = x.Ward != null ? x.Ward.Name : "",
                    Address = x.LoanBriefResident != null && x.LoanBriefResident.Address != null ? x.LoanBriefResident.Address : "",
                    BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                    FeeInsuranceOfCustomer = x.FeeInsuranceOfCustomer,
                    BankName = x.Bank != null && x.Bank.Name != null ? x.Bank.Name : "",
                    BankAccountNumber = x.BankAccountNumber,
                    LoanBriefRelationship = x.LoanBriefRelationship != null ? x.LoanBriefRelationship.ToList() : null
                };
            }
        }
    }

    public class LoanBriefOfKbFinaCPS
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string ProductName { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public DateTime CreateDate { get; set; }
        public string StatusName { get; set; }
        public string ReasonCancel { get; set; }
        public string TransactionId { get; set; }
        public int? StatusCode { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefOfKbFinaCPS>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefOfKbFinaCPS()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    ProvinceName = x.Province != null ? x.Province.Name : null,
                    DistrictName = x.District != null ? x.District.Name : null,
                    ProductName = Description.GetDescription((EnumProductCredit)x.ProductId),
                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    CreateDate = x.CreatedTime.Value.DateTime,
                    StatusName = Description.GetDescription((EnumLoanStatus)x.Status),
                    ReasonCancel = x.ReasonCancelNavigation != null ? x.ReasonCancelNavigation.Reason : null,
                    TransactionId = x.AffSid,
                    StatusCode = x.Status,
                };
            }
        }
    }

    public class LoanCancelMomo
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string NationalCard { get; set; }
        public decimal? LoanAmount { get; set; }
        public DateTime? Dob { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTime? LoanBriefCancelAt { get; set; }

        public static Expression<Func<LoanBrief, LoanCancelMomo>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanCancelMomo()
                {
                    LoanBriefId = x.LoanBriefId,
                    LoanAmount = x.LoanAmount,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    NationalCard = x.NationalCard,
                    Dob = x.Dob,
                    CreatedTime = x.CreatedTime,
                    LoanBriefCancelAt = x.LoanBriefCancelAt
                };
            }
        }
    }

    public class LoanBriefAffiliateGtv
    {
        [JsonPropertyName("loancredit_id")]
        public int LoanBriefId { get; set; }

        [JsonPropertyName("status_name")]
        public string StatusName { get; set; }

        [JsonPropertyName("create_date")]
        public DateTime CreateDate { get; set; }

        [JsonPropertyName("reason_cancel_name")]
        public string ReasonCancelName { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefAffiliateGtv>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefAffiliateGtv()
                {
                    LoanBriefId = x.LoanBriefId,
                    CreateDate = x.CreatedTime.Value.DateTime,
                    ReasonCancelName = x.ReasonCancelNavigation.Reason,
                    StatusName = (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH) ? "success" : x.Status == (int)EnumLoanStatus.CANCELED ? "reject" : x.Status == (int)EnumLoanStatus.INIT ? "registered" : "processing"
                };

            }
        }

    }
    public class LoanByPhoneChange
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public int? RelationshipType { get; set; } = 0;
    }

    public class LoanTransactionsSecured
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string NationalCard { get; set; }
        public decimal? LoanAmount { get; set; }
        public string CountryId { get; set; }
        public string ProvinceMap { get; set; }
        public string DistrictMap { get; set; }
        public string Address { get; set; }
        //Số khung
        public string Chassis { get; set; }
        //Số máy
        public string Engine { get; set; }
        public string PlateNumberCar { get; set; }
        public string Description { get; set; }
        public int? CustomerId { get; set; }

        public static Expression<Func<LoanBrief, LoanTransactionsSecured>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanTransactionsSecured()
                {
                    LoanBriefId = x.LoanBriefId,
                    LoanAmount = x.LoanAmount,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    CustomerId = x.CustomerId,
                    NationalCard = x.NationalCard,
                    ProvinceMap = x.Province != null ? x.Province.MapTransaction : "",
                    DistrictMap = x.District != null ? x.District.MapTransaction : "",
                    Address = x.LoanBriefResident != null ? x.LoanBriefResident.Address : "",
                    Description = x.LoanBriefProperty != null && !string.IsNullOrEmpty(x.LoanBriefProperty.Description) ? x.LoanBriefProperty.Description : "",
                    Chassis = x.LoanBriefProperty != null && !string.IsNullOrEmpty(x.LoanBriefProperty.Chassis) ? x.LoanBriefProperty.Chassis : "",
                    Engine = x.LoanBriefProperty != null && !string.IsNullOrEmpty(x.LoanBriefProperty.Engine) ? x.LoanBriefProperty.Engine : "",
                    PlateNumberCar = x.LoanBriefProperty != null && !string.IsNullOrEmpty(x.LoanBriefProperty.PlateNumberCar) ? x.LoanBriefProperty.PlateNumberCar : ""
                };
            }
        }
    }

    public class LoanBriefAffiliateCPSMasOfferXeMay
    {
        [JsonPropertyName("loancredit_id")]
        public int LoanBriefId { get; set; }

        [JsonPropertyName("aff_code")]
        public string AffCode { get; set; }

        [JsonPropertyName("aff_sid")]
        public string AffSid { get; set; }

        [JsonPropertyName("status")]
        public int Status { get; set; }

        [JsonPropertyName("create_date")]
        public DateTime CreateDate { get; set; }

        [JsonPropertyName("reason_cancel_name")]
        public string ReasonCancelName { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefAffiliateCPSMasOfferXeMay>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefAffiliateCPSMasOfferXeMay()
                {
                    LoanBriefId = x.LoanBriefId,
                    AffSid = x.AffSid,
                    AffCode = x.AffCode,
                    CreateDate = x.CreatedTime.Value.DateTime,
                    ReasonCancelName = x.ReasonCancelNavigation.Reason,
                    Status = x.Status == (int)EnumLoanStatus.DISBURSED ? 1 : x.Status == (int)EnumLoanStatus.FINISH ? 1 :
                    x.Status == (int)EnumLoanStatus.CANCELED ? 0 : 2
                };

            }
        }
    }

    public class LoanBriefAffiliateCPSItem
    {
        [JsonPropertyName("loancredit_id")]
        public int LoanBriefId { get; set; }

        [JsonPropertyName("aff_code")]
        public string AffCode { get; set; }

        [JsonPropertyName("aff_sid")]
        public string AffSid { get; set; }

        [JsonPropertyName("status")]
        public int Status { get; set; }

        [JsonPropertyName("create_date")]
        public DateTime CreateDate { get; set; }

        [JsonPropertyName("update_date")]
        public DateTime? UpdateDate { get; set; }

        [JsonPropertyName("reason_cancel_name")]
        public string ReasonCancelName { get; set; }
    }

    public class LoanBriefAffiliateCPSNoUpdateTimeItem
    {
        [JsonPropertyName("loancredit_id")]
        public int LoanBriefId { get; set; }

        [JsonPropertyName("aff_code")]
        public string AffCode { get; set; }

        [JsonPropertyName("aff_sid")]
        public string AffSid { get; set; }

        [JsonPropertyName("status")]
        public int Status { get; set; }

        [JsonPropertyName("create_date")]
        public DateTime CreateDate { get; set; }

        [JsonPropertyName("reason_cancel_name")]
        public string ReasonCancelName { get; set; }
    }

    public class LoanBriefAllMMOTimaItem
    {
        [JsonPropertyName("loanbriefId")]
        public string LoanBriefId { get; set; }

        [JsonPropertyName("full_name")]
        public string FullName { get; set; }

        [JsonPropertyName("city_name")]
        public string CityName { get; set; }

        [JsonPropertyName("district_name")]
        public string DistrictName { get; set; }

        [JsonPropertyName("product_name")]
        public string ProductName { get; set; }

        [JsonPropertyName("aff_code")]
        public string AffCode { get; set; }

        [JsonPropertyName("status")]
        public int Status { get; set; }

        [JsonPropertyName("reason_cancel")]
        public string ReasonCancel { get; set; }

        [JsonPropertyName("create_date")]
        public DateTime? CreateDate { get; set; }

        [JsonPropertyName("date_disbursement")]
        public DateTime? DateDisbursement { get; set; }

        [JsonPropertyName("loan_ammount")]
        public decimal LoanAmmount { get; set; }

        [JsonPropertyName("productId")]
        public int ProductId { get; set; }

        [JsonPropertyName("cityId")]
        public int CityId { get; set; }

        [JsonPropertyName("districtId")]
        public int DistrictId { get; set; }
    }

    public class LoanDisplayDocHub
    {
        public int LoanBriefId { get; set; }
        public int? HubId { get; set; }
        public int? HubEmployeeId { get; set; }

        public static Expression<Func<LoanBrief, LoanDisplayDocHub>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanDisplayDocHub()
                {
                    LoanBriefId = x.LoanBriefId,
                    HubId = x.HubId,
                    HubEmployeeId = x.HubEmployeeId
                };
            }
        }
    }
 public class LoanBriefAffiliateCPQLItem
    {
        [JsonPropertyName("loancredit_id")]
        public int LoanBriefId { get; set; }

        [JsonPropertyName("aff_code")]
        public string AffCode { get; set; }

        [JsonPropertyName("aff_sid")]
        public string AffSid { get; set; }

        [JsonPropertyName("create_date")]
        public DateTime CreateDate { get; set; }

        [JsonPropertyName("update_date")]
        public DateTime? UpdateDate { get; set; }

        [JsonPropertyName("reason_cancel_name")]
        public string ReasonCancelName { get; set; }

        [JsonPropertyName("upload_file")]
        public string UploadFile { get; set; }

        [JsonPropertyName("value_check_qlf")]
        public int ValueCheckQlf { get; set; }

        [JsonPropertyName("lead_qualify")]
        public string LeadQualify { get; set; }

        [JsonPropertyName("status")]
        public int Status { get; set; }

    }

    public class LoanBriefStatusJeffApi
    {
        [JsonPropertyName("loanbrief_id")]
        public int LoanBriefId { get; set; }

        [JsonPropertyName("aff_code")]
        public string AffCode { get; set; }

        [JsonPropertyName("aff_sid")]
        public string AffSId { get; set; }

        [JsonPropertyName("status_name")]
        public string StatusName { get; set; }

        [JsonPropertyName("create_date")]
        public DateTime CreateDate { get; set; }

        [JsonPropertyName("reason_cancel_name")]
        public string ReasonCancelName { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefStatusJeffApi>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefStatusJeffApi()
                {
                    LoanBriefId = x.LoanBriefId,
                    AffCode = x.AffCode,
                    AffSId = x.AffSid,
                    CreateDate = x.CreatedTime.Value.DateTime,
                    ReasonCancelName = x.ReasonCancelNavigation.ReasonEng,
                    StatusName = x.Status == (int)EnumLoanStatus.DISBURSED ? "success" : x.Status == (int)EnumLoanStatus.CANCELED ? "reject" : x.Status == (int)EnumLoanStatus.INIT ? "registered" : "processing"
                };

            }
        }

    }

    public class LoanBriefCreateAffiliateApi
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string AffCode { get; set; }
        public string AffSId { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public int Status { get; set; }
        public DateTime CreateTime { get; set; }
        public string ReasonCance { get; set; }
        public int ProductId { get; set; }
        public decimal LoanAmount { get; set; }
        public DateTime? DisbursementTime { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefCreateAffiliateApi>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefCreateAffiliateApi()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    AffCode = x.AffCode,
                    AffSId = x.AffSid,
                    ProvinceName = x.Province != null ? x.Province.Name : null,
                    DistrictName = x.District != null ? x.District.Name : null,
                    Status = x.Status == (int)EnumLoanStatus.CANCELED ? 0 : x.Status == (int)EnumLoanStatus.DISBURSED ? 1 : x.Status == (int)EnumLoanStatus.FINISH ? 2 : 3,
                    CreateTime = x.CreatedTime.Value.DateTime,
                    ReasonCance = x.ReasonCancelNavigation != null ? x.ReasonCancelNavigation.Reason : null,
                    ProductId = x.ProductId.HasValue ? x.ProductId.Value : 0,
                    LoanAmount = x.LoanAmount.HasValue ? x.LoanAmount.Value : 0,
                    DisbursementTime = x.DisbursementAt,
                };

            }
        }
    }

    public class LoanBriefDetailForApi
    {
        public LoanBriefQuestionScriptDetailForApi LoanBriefQuestionScript { get; set; }
        public LoanBriefPropertyDetailForApi LoanBriefProperty { get; set; }
        public LoanBriefCompanyDetailForApi LoanBriefCompany { get; set; }
        public LoanBriefJobDetailForApi LoanBriefJob { get; set; }
        public LoanBriefHouseholdDetailForApi LoanBriefHousehold { get; set; }
        public LoanBriefResidentDetailForApi LoanBriefResident { get; set; }
        public CustomerDetailForApi Customer { get; set; }
        public List<LoanBriefRelationshipDetailForApi> Relationship { get; set; }
        public LoanbriefLender LoanbriefLender { get; set; }
        public ResultEkyc ResultEkyc { get; set; }
        public int LoanBriefId { get; set; }
        public int? CustomerId { get; set; }
        public int? HubId { get; set; }
        public int? PlatformType { get; set; }
        public string PlatformTypeName { get; set; }
        public int? TypeLoanBrief { get; set; }
        public string TypeLoanBriefName { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string NationalCard { get; set; }
        public string NationCardPlace { get; set; }
        public bool? IsTrackingLocation { get; set; }
        public bool? IsLocate { get; set; }
        public bool? IsLocateCar { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Passport { get; set; }
        public int? Gender { get; set; }
        public int? NumberCall { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? WardId { get; set; }
        public string WardName { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? RateTypeId { get; set; }
        public string RateTypeName { get; set; }
        public decimal? LoanAmount { get; set; }
        public bool? BuyInsurenceCustomer { get; set; }
        public int? LoanTime { get; set; }
        public int? Frequency { get; set; }
        public decimal? RateMoney { get; set; }
        public double? RatePercent { get; set; }
        public int? ReceivingMoneyType { get; set; }
        public int? BankId { get; set; }
        public string BankName { get; set; }
        public int? TypeRemarketing { get; set; }
        public int? ReMarketingLoanBriefId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }
        public string BankAccountName { get; set; }
        public string PresenterCode { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public decimal? LoanAmountFirst { get; set; }
        public decimal? LoanAmountExpertise { get; set; }
        public decimal? LoanAmountExpertiseLast { get; set; }
        public decimal? LoanAmountExpertiseAi { get; set; }
        public int? LoanPurpose { get; set; }
        public string LoanPurposeName { get; set; }
        public bool? IsCheckBank { get; set; }
        public int? HomeNetwork { get; set; }
        public string HomeNetworkName { get; set; }
        public string PhoneOther { get; set; }
        public string BankBranch { get; set; }
        public int? ProductDetailId { get; set; }
        public string ProductDetailName { get; set; }
        public bool? IsSim { get; set; }
        public bool? IsReborrow { get; set; }
        public bool? IsChangeInsurance { get; set; }
        public string Email { get; set; }
        public int? Ltv { get; set; }
        public bool? IsTransactionsSecured { get; set; }
        public int? CodeId { get; set; }
        public string LenderName { get; set; }
        public decimal? FeeInsuranceOfCustomer { get; set; }
        public decimal? FeeInsuranceOfProperty { get; set; }
        public double? LenderRate { get; set; }
        public int? Status { get; set; }
        public int? EsignState { get; set; }
        public int? BoundTelesaleId { get; set; }
        public int? CreateBy { get; set; }
        public bool IsChangeStatusDetail { get; set; }
        public bool IsChangeStaff { get; set; }
        public bool IsPushException { get; set; }
        public bool IsRegetLocation { get; set; }
        public bool IsCreatedCarChild { get; set; }
        public bool IsReEsign { get; set; }
        public bool IsGetInsuranceInfo { get; set; }
        public bool IsCheckMomo { get; set; }
        public bool IsEdit { get; set; }
        public bool IsPush { get; set; }
        public bool IsCancel { get; set; }
        public bool IsBack { get; set; }
        public string LinkCall { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefDetailForApi>> ProjectionViewDetail
        {
            get
            {
                return x => new LoanBriefDetailForApi()
                {
                    LoanBriefId = x.LoanBriefId,
                    CustomerId = x.CustomerId,
                    HubId = x.HubId,
                    PlatformType = x.PlatformType,
                    TypeLoanBrief = x.TypeLoanBrief,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    NationalCard = x.NationalCard,
                    NationCardPlace = x.NationCardPlace,
                    IsTrackingLocation = x.IsTrackingLocation,
                    IsLocate = x.IsLocate,
                    BirthDay = x.Dob,
                    Passport = x.Passport,
                    Gender = x.Gender,
                    NumberCall = x.NumberCall,
                    ProvinceId = x.ProvinceId,
                    ProvinceName = x.Province != null ? x.Province.Name : null,
                    DistrictId = x.DistrictId,
                    DistrictName = x.District != null ? x.District.Name : null,
                    WardId = x.WardId,
                    WardName = x.Ward != null ? x.Ward.Name : null,
                    ProductId = x.ProductId,
                    RateTypeId = x.RateTypeId,
                    LoanAmount = x.LoanAmount,
                    BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                    LoanTime = x.LoanTime,
                    Frequency = x.Frequency,
                    RateMoney = x.RateMoney,
                    RatePercent = x.RatePercent,
                    ReceivingMoneyType = x.ReceivingMoneyType,
                    BankId = x.BankId,
                    TypeRemarketing = x.TypeRemarketing,
                    ReMarketingLoanBriefId = x.ReMarketingLoanBriefId,
                    BankAccountNumber = x.BankAccountNumber,
                    BankCardNumber = x.BankCardNumber,
                    BankAccountName = x.BankAccountName,
                    PresenterCode = x.PresenterCode,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    LoanAmountFirst = x.LoanAmountFirst,
                    LoanAmountExpertise = x.LoanAmountExpertise,
                    LoanAmountExpertiseLast = x.LoanAmountExpertiseLast,
                    LoanAmountExpertiseAi = x.LoanAmountExpertiseAi,
                    LoanPurpose = x.LoanPurpose,
                    IsCheckBank = x.IsCheckBank,
                    HomeNetwork = x.HomeNetwork,
                    PhoneOther = x.PhoneOther,
                    BankBranch = x.BankBranch,
                    ProductDetailId = x.ProductDetailId,
                    IsSim = x.IsSim,
                    IsReborrow = x.IsReborrow,
                    Email = x.Email,
                    Ltv = x.Ltv,
                    IsTransactionsSecured = x.IsTransactionsSecured,
                    CodeId = x.CodeId,
                    LenderName = x.LenderName,
                    FeeInsuranceOfCustomer = x.FeeInsuranceOfCustomer,
                    FeeInsuranceOfProperty = x.FeeInsuranceOfProperty,
                    LenderRate = x.LenderRate,
                    BankName = (x.Bank != null && x.BankId.HasValue) ? x.Bank.Name : null,
                    Status = x.Status,
                    EsignState = x.EsignState,
                    BoundTelesaleId = x.BoundTelesaleId,
                    CreateBy = x.CreateBy,
                    LoanBriefQuestionScript = x.LoanBriefQuestionScript != null ? new LoanBriefQuestionScriptDetailForApi()
                    {
                        WaterSupplier = x.LoanBriefQuestionScript.WaterSupplier,
                        QuestionHouseOwner = x.LoanBriefQuestionScript.QuestionHouseOwner,
                        Appraiser = x.LoanBriefQuestionScript.Appraiser,
                        DocumentBusiness = x.LoanBriefQuestionScript.DocumentBusiness,
                        MaxPrice = x.LoanBriefQuestionScript.MaxPrice,
                        QuestionAddressCoincideAreaSupport = x.LoanBriefQuestionScript.QuestionAddressCoincideAreaSupport,
                    } : null,
                    LoanBriefProperty = x.LoanBriefProperty != null ? new LoanBriefPropertyDetailForApi()
                    {
                        BrandId = x.LoanBriefProperty.BrandId,
                        BrandName = x.LoanBriefProperty.Brand != null ? x.LoanBriefProperty.Brand.Name : null,
                        PropertyProductId = x.LoanBriefProperty.ProductId,
                        PropertyProductName = x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.FullName : null,
                        PlateNumber = x.LoanBriefProperty.PlateNumber,
                        CarManufacturer = x.LoanBriefProperty.CarManufacturer,
                        CarName = x.LoanBriefProperty.CarName,
                        CarColor = x.LoanBriefProperty.CarColor,
                        PlateNumberCar = x.LoanBriefProperty.PlateNumberCar,
                        Chassis = x.LoanBriefProperty.Chassis,
                        Engine = x.LoanBriefProperty.Engine,
                        BuyInsuranceProperty = x.LoanBriefProperty.BuyInsuranceProperty,
                        OwnerFullName = x.LoanBriefProperty.OwnerFullName,
                        MotobikeCertificateNumber = x.LoanBriefProperty.MotobikeCertificateNumber,
                        MotobikeCertificateDate = x.LoanBriefProperty.MotobikeCertificateDate,
                        MotobikeCertificateAddress = x.LoanBriefProperty.MotobikeCertificateAddress,
                        PostRegistration = x.LoanBriefProperty.PostRegistration,
                        PropertyDescription = x.LoanBriefProperty.Description,
                    } : null,
                    LoanBriefCompany = x.LoanBriefCompany != null ? new LoanBriefCompanyDetailForApi()
                    {
                        EnterpriseName = x.LoanBriefCompany.CompanyName,
                        CareerBusiness = x.LoanBriefCompany.CareerBusiness,
                        BusinessCertificationAddress = x.LoanBriefCompany.BusinessCertificationAddress,
                        HeadOffice = x.LoanBriefCompany.HeadOffice,
                        EnterpriseAddress = x.LoanBriefCompany.Address,
                        CompanyShareholder = x.LoanBriefCompany.CompanyShareholder,
                        CardNumberShareholderDate = x.LoanBriefCompany.CardNumberShareholderDate,
                        BusinessCertificationDate = x.LoanBriefCompany.BusinessCertificationDate,
                        BirthdayShareholder = x.LoanBriefCompany.BirthdayShareholder,
                        CardNumberShareholder = x.LoanBriefCompany.CardNumberShareholder,
                        PlaceOfBirthShareholder = x.LoanBriefCompany.PlaceOfBirthShareholder,
                    } : null,
                    LoanBriefJob = x.LoanBriefJob != null ? new LoanBriefJobDetailForApi()
                    {
                        JobId = x.LoanBriefJob.JobId,
                        JobName = x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.Name : null,
                        CompanyName = x.LoanBriefJob.CompanyName,
                        CompanyPhone = x.LoanBriefJob.CompanyPhone,
                        CompanyTaxCode = x.LoanBriefJob.CompanyTaxCode,
                        CompanyProvinceId = x.LoanBriefJob.CompanyProvinceId,
                        CompanyProvinceName = x.LoanBriefJob.CompanyProvince != null ? x.LoanBriefJob.CompanyProvince.Name : null,
                        CompanyDistrictId = x.LoanBriefJob.CompanyDistrictId,
                        CompanyDistrictName = x.LoanBriefJob.CompanyDistrict != null ? x.LoanBriefJob.CompanyDistrict.Name : null,
                        CompanyWardId = x.LoanBriefJob.CompanyWardId,
                        CompanyWardName = x.LoanBriefJob.CompanyWard != null ? x.LoanBriefJob.CompanyWard.Name : null,
                        CompanyAddress = x.LoanBriefJob.CompanyAddress,
                        ImcomeType = x.LoanBriefJob.ImcomeType,
                        TotalIncome = x.LoanBriefJob.TotalIncome,
                        JobDescription = x.LoanBriefJob.Description,
                        CompanyAddressGoogleMap = x.LoanBriefJob.CompanyAddressGoogleMap,
                        CompanyAddressLatLng = x.LoanBriefJob.CompanyAddressLatLng,
                        CompanyInsurance = x.LoanBriefJob.CompanyInsurance,
                        WorkLocation = x.LoanBriefJob.WorkLocation,
                        BusinessPapers = x.LoanBriefJob.BusinessPapers,
                        JobDescriptionId = x.LoanBriefJob.JobDescriptionId,
                    } : null,
                    LoanBriefHousehold = x.LoanBriefHousehold != null ? new LoanBriefHouseholdDetailForApi()
                    {
                        HouseholdAddress = x.LoanBriefHousehold.Address,
                        HouseholdProvinceId = x.LoanBriefHousehold.ProvinceId,
                        HouseholdProvinceName = x.LoanBriefHousehold.Province != null ? x.LoanBriefHousehold.Province.Name : null,
                        HouseholdDistrictId = x.LoanBriefHousehold.DistrictId,
                        HouseholdDistrictName = x.LoanBriefHousehold.District != null ? x.LoanBriefHousehold.District.Name : null,
                        HouseholdWardId = x.LoanBriefHousehold.WardId,
                        HouseholdWardName = x.LoanBriefHousehold.Ward != null ? x.LoanBriefHousehold.Ward.Name : null,
                        FullNameHouseOwner = x.LoanBriefHousehold.FullNameHouseOwner,
                        RelationshipHouseOwner = x.LoanBriefHousehold.RelationshipHouseOwner,
                        BirdayHouseOwner = x.LoanBriefHousehold.BirdayHouseOwner,
                    } : null,
                    LoanBriefResident = x.LoanBriefResident != null ? new LoanBriefResidentDetailForApi()
                    {
                        LivingTime = x.LoanBriefResident.LivingTime,
                        ResidentType = x.LoanBriefResident.ResidentType,
                        LivingWith = x.LoanBriefResident.LivingWith,
                        ResidentAddress = x.LoanBriefResident.Address,
                        ResidentAddressGoogleMap = x.LoanBriefResident.AddressGoogleMap,
                        ResidentAddressLatLng = x.LoanBriefResident.AddressLatLng,
                        AddressNationalCard = x.LoanBriefResident.AddressNationalCard,
                        CustomerShareLocation = x.LoanBriefResident.CustomerShareLocation,
                    } : null,
                    Customer = x.Customer != null ? new CustomerDetailForApi()
                    {
                        FacebookAddress = x.Customer.FacebookAddress,
                        InsurenceNumber = x.Customer.InsurenceNumber,
                        InsurenceNote = x.Customer.InsurenceNote,
                        IsMerried = x.Customer.IsMerried,
                        NumberBaby = x.Customer.NumberBaby,
                    } : null,
                    Relationship = x.LoanBriefRelationship != null ? x.LoanBriefRelationship
                    .Select(r => new LoanBriefRelationshipDetailForApi()
                    {
                        Id = r.Id,
                        RelationshipType = r.RelationshipType,
                        FullName = r.FullName,
                        Phone = r.Phone,
                        Address = r.Address,
                        RelationshipName = r.RelationshipType.HasValue ? Description.GetDescription((EnumRelativeFamily)r.RelationshipType) : null,
                    }).ToList() : null,
                };
            }
        }
    }
    public class LoanBriefQuestionScriptDetailForApi
    {
        public string WaterSupplier { get; set; }
        public bool? QuestionHouseOwner { get; set; }
        public short? Appraiser { get; set; }
        public int? DocumentBusiness { get; set; }
        public long? MaxPrice { get; set; }
        public bool? QuestionAddressCoincideAreaSupport { get; set; }
    }
    public class LoanBriefPropertyDetailForApi
    {
        public int? BrandId { get; set; }
        public string BrandName { get; set; }
        public int? PropertyProductId { get; set; }
        public string PropertyProductName { get; set; }
        public string PlateNumber { get; set; }
        public string CarManufacturer { get; set; }
        public string CarName { get; set; }
        public string CarColor { get; set; }
        public string PlateNumberCar { get; set; }
        public string Chassis { get; set; }
        public string Engine { get; set; }
        public bool? BuyInsuranceProperty { get; set; }
        public string OwnerFullName { get; set; }
        public string MotobikeCertificateNumber { get; set; }
        public string MotobikeCertificateDate { get; set; }
        public string MotobikeCertificateAddress { get; set; }
        public bool? PostRegistration { get; set; }
        public string PropertyDescription { get; set; }
    }
    public class LoanBriefCompanyDetailForApi
    {
        public string EnterpriseName { get; set; }
        public string CareerBusiness { get; set; }
        public string BusinessCertificationAddress { get; set; }
        public string HeadOffice { get; set; }
        public string EnterpriseAddress { get; set; }
        public string CompanyShareholder { get; set; }
        public DateTime? CardNumberShareholderDate { get; set; }
        public DateTime? BusinessCertificationDate { get; set; }
        public DateTime? BirthdayShareholder { get; set; }
        public string CardNumberShareholder { get; set; }
        public string PlaceOfBirthShareholder { get; set; }
    }
    public class LoanBriefJobDetailForApi
    {
        public int? JobId { get; set; }
        public string JobName { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyTaxCode { get; set; }
        public int? CompanyProvinceId { get; set; }
        public string CompanyProvinceName { get; set; }
        public int? CompanyDistrictId { get; set; }
        public string CompanyDistrictName { get; set; }
        public int? CompanyWardId { get; set; }
        public string CompanyWardName { get; set; }
        public string CompanyAddress { get; set; }
        public int? ImcomeType { get; set; }
        public string ImcomeTypeName { get; set; }
        public decimal? TotalIncome { get; set; }
        public string JobDescription { get; set; }
        public string CompanyAddressGoogleMap { get; set; }
        public string CompanyAddressLatLng { get; set; }
        public bool? CompanyInsurance { get; set; }
        public bool? WorkLocation { get; set; }
        public bool? BusinessPapers { get; set; }
        public int? JobDescriptionId { get; set; }
        public string JobDescriptionName { get; set; }
    }
    public class LoanBriefHouseholdDetailForApi
    {
        public string HouseholdAddress { get; set; }
        public int? HouseholdProvinceId { get; set; }
        public string HouseholdProvinceName { get; set; }
        public int? HouseholdDistrictId { get; set; }
        public string HouseholdDistrictName { get; set; }
        public int? HouseholdWardId { get; set; }
        public string HouseholdWardName { get; set; }
        public string FullNameHouseOwner { get; set; }
        public int? RelationshipHouseOwner { get; set; }
        public string BirdayHouseOwner { get; set; }
    }
    public class LoanBriefResidentDetailForApi
    {
        public int? LivingTime { get; set; }
        public string LivingTimeName { get; set; }
        public int? ResidentType { get; set; }
        public string ResidentTypeName { get; set; }
        public int? LivingWith { get; set; }
        public string LivingWithName { get; set; }
        public string ResidentAddress { get; set; }
        public string ResidentAddressGoogleMap { get; set; }
        public string ResidentAddressLatLng { get; set; }
        public string AddressNationalCard { get; set; }
        public string CustomerShareLocation { get; set; }

    }
    public class CustomerDetailForApi
    {
        public string FacebookAddress { get; set; }
        public string InsurenceNumber { get; set; }
        public string InsurenceNote { get; set; }
        public int? IsMerried { get; set; }
        public int? NumberBaby { get; set; }
    }
    public class LoanBriefRelationshipDetailForApi
    {
        public int Id { get; set; }
        public int? RelationshipType { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string RelationshipName { get; set; }
        public string LinkCall { get; set; }
    }

 public class LoanBriefDetailApi
    {
        public int LoanBriefId { get; set; }
        public int? CustomerId { get; set; }
        public CustomerInfomation CustomerInfomation { get; set; }
        public VAInfomation VAInfomation { get; set; }
        public CompanyInformation CompanyInformation { get; set; }
        public AddressHome AddressHome { get; set; }
        public AddressHousehold AddressHousehold { get; set; }
        public WorkInformation WorkInformation { get; set; }
        public List<RelativesAndColleagues> ListRelativesAndColleagues { get; set; }
        public LoanBriefForm LoanBriefForm { get; set; }
        public AssetsInformation AssetsInformation { get; set; }
        public LoanAmountInfomation LoanAmountInfomation { get; set; }
        public CICInfomation CICInfomation { get; set; }
        public DisbursementInfomation DisbursementInfomation { get; set; }
        public FraudCheckInfomation FraudCheckInfomation { get; set; }
        public ResultEkyc EkycInfomation { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefDetailApi>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefDetailApi()
                {
                    LoanBriefId = x.LoanBriefId,
                    CustomerId = x.CustomerId,
                    CustomerInfomation = x.Customer != null ? new CustomerInfomation()
                    {
                        PlatformType = x.PlatformType.HasValue ? Description.GetDescription((EnumPlatformType)x.PlatformType) : null,
                        TypeLoanBrief = x.TypeLoanBrief.HasValue ? Description.GetDescription((TypeLoanBrief)x.TypeLoanBrief) : null,
                        CountCall = x.CountCall,
                        FullName = x.FullName,
                        Phone = x.Phone,
                        PhoneOther = x.PhoneOther,
                        NationalCard = x.NationalCard,
                        NationCardPlace = x.NationCardPlace,
                        Dob = (x.Dob != null && x.Dob != DateTime.MinValue) ? x.Dob.Value.ToString("dd/MM/yyyy") : null,
                        Passport = x.Passport,
                        Gender = x.Gender.HasValue ? Description.GetDescription((LOS.Common.Extensions.Gender)x.Gender) : null,
                        FacebookAddress = x.Customer != null ? x.Customer.FacebookAddress : null,
                        InsurenceNumber = x.Customer != null ? x.Customer.InsurenceNumber : null,
                        InsurenceNote = x.Customer != null ? x.Customer.InsurenceNote : null,
                        Merried = (x.Customer != null && x.Customer.IsMerried.HasValue) ? Description.GetDescription((Merried)x.Customer.IsMerried) : null,
                        NumberBaby = (x.Customer != null && x.Customer.NumberBaby.HasValue) ? Description.GetDescription((NumberBaby)x.Customer.NumberBaby) : null,
                        LivingWith = (x.LoanBriefResident != null && x.LoanBriefResident.LivingWith.HasValue) ? Description.GetDescription((LivingWith)x.LoanBriefResident.LivingWith) : null,
                        Sim = x.IsSim == true ? "Chính chủ" : "Không chính chủ",
                        Email = x.Email
                    } : null,
                    VAInfomation = x.Customer != null ? new VAInfomation()
                    {
                        VaAccountName = x.Customer != null ? x.Customer.VaAccountName : null,
                        VaAccountNumber = x.Customer != null ? x.Customer.VaAccountNumber : null,
                        VaBankName = x.Customer != null ? x.Customer.VaBankName : null,
                        GpayAccountName = x.Customer != null ? x.Customer.GpayAccountName : null,
                        GpayAccountNumber = x.Customer != null ? x.Customer.GpayAccountNumber : null,
                    } : null,
                    CompanyInformation = x.LoanBriefCompany != null ? new CompanyInformation()
                    {
                        CompanyName = x.LoanBriefCompany != null ? x.LoanBriefCompany.CompanyName : null,
                        CareerBusiness = x.LoanBriefCompany != null ? x.LoanBriefCompany.CareerBusiness : null,
                        BusinessCertificationDate = (x.LoanBriefCompany != null && x.LoanBriefCompany.BusinessCertificationDate != null && x.LoanBriefCompany.BusinessCertificationDate != DateTime.MinValue) ? x.LoanBriefCompany.BusinessCertificationDate.Value.ToString("dd/MM/yyyy") : null,
                        BusinessCertificationAddress = x.LoanBriefCompany != null ? x.LoanBriefCompany.BusinessCertificationAddress : null,
                        HeadOffice = x.LoanBriefCompany != null ? x.LoanBriefCompany.HeadOffice : null,
                        Address = x.LoanBriefCompany != null ? x.LoanBriefCompany.Address : null,
                        CompanyShareholder = x.LoanBriefCompany != null ? x.LoanBriefCompany.CompanyShareholder : null,
                        BirthdayShareholder = (x.LoanBriefCompany != null && x.LoanBriefCompany.BirthdayShareholder != null && x.LoanBriefCompany.BirthdayShareholder != DateTime.MinValue) ? x.LoanBriefCompany.BirthdayShareholder.Value.ToString("dd/MM/yyyy") : null,
                        CardNumberShareholder = x.LoanBriefCompany != null ? x.LoanBriefCompany.CardNumberShareholder : null,
                        CardNumberShareholderDate = (x.LoanBriefCompany != null && x.LoanBriefCompany.CardNumberShareholderDate != null && x.LoanBriefCompany.CardNumberShareholderDate != DateTime.MinValue) ? x.LoanBriefCompany.CardNumberShareholderDate.Value.ToString("dd/MM/yyyy") : null,
                        PlaceOfBirthShareholder = x.LoanBriefCompany != null ? x.LoanBriefCompany.PlaceOfBirthShareholder : null,
                    } : null,
                    AddressHome = x.LoanBriefResident != null ? new AddressHome()
                    {
                        ProvinceName = (x.LoanBriefResident != null && x.LoanBriefResident.Province != null) ? Description.GetDescription((TypeProvince)x.LoanBriefResident.Province.Type) + " " + x.LoanBriefResident.Province.Name : null,
                        DistrictName = (x.LoanBriefResident != null && x.LoanBriefResident.District != null) ? Description.GetDescription((TypeDistrict)x.LoanBriefResident.District.Type) + " " + x.LoanBriefResident.District.Name : null,
                        WardName = (x.LoanBriefResident != null && x.LoanBriefResident.Ward != null) ? Description.GetDescription((TypeWard)x.LoanBriefResident.Ward.Type) + " " + x.LoanBriefResident.Ward.Name : null,
                        ResidentType = (x.LoanBriefResident != null && x.LoanBriefResident.ResidentType.HasValue) ? Description.GetDescription((EnumTypeofownership)x.LoanBriefResident.ResidentType) : null,
                        LivingTime = (x.LoanBriefResident != null && x.LoanBriefResident.LivingTime.HasValue) ? Description.GetDescription((EnumLivingTime)x.LoanBriefResident.LivingTime) : null,
                        CustomerShareLocation = x.LoanBriefResident != null ? x.LoanBriefResident.CustomerShareLocation : null,
                        AddressLatLng = x.LoanBriefResident != null ? x.LoanBriefResident.AddressLatLng : null,
                        AddressGoogleMap = x.LoanBriefResident != null ? x.LoanBriefResident.AddressGoogleMap : null,
                    } : null,
                    AddressHousehold = x.LoanBriefHousehold != null ? new AddressHousehold()
                    {
                        ProvinceName = (x.LoanBriefHousehold != null && x.LoanBriefHousehold.Province != null) ? Description.GetDescription((TypeProvince)x.LoanBriefHousehold.Province.Type) + " " + x.LoanBriefHousehold.Province.Name : null,
                        DistrictName = (x.LoanBriefHousehold != null && x.LoanBriefHousehold.District != null) ? Description.GetDescription((TypeDistrict)x.LoanBriefHousehold.District.Type) + " " + x.LoanBriefHousehold.District.Name : null,
                        WardName = (x.LoanBriefHousehold != null && x.LoanBriefHousehold.Ward != null) ? Description.GetDescription((TypeWard)x.LoanBriefHousehold.Ward.Type) + " " + x.LoanBriefHousehold.Ward.Name : null,
                        Address = x.LoanBriefHousehold != null ? x.LoanBriefHousehold.Address : null,
                    } : null,
                    WorkInformation = x.LoanBriefJob != null ? new WorkInformation()
                    {
                        JobName = (x.LoanBriefJob != null && x.LoanBriefJob.Job != null) ? x.LoanBriefJob.Job.Name : null,
                        JobDescriptionId = x.LoanBriefJob != null ? x.LoanBriefJob.JobDescriptionId : null,
                        Description = x.LoanBriefJob != null ? x.LoanBriefJob.Description : null,
                        TotalIncome = x.LoanBriefJob != null ? x.LoanBriefJob.TotalIncome : null,
                        ImcomeType = (x.LoanBriefJob != null && x.LoanBriefJob.ImcomeType.HasValue) ? Description.GetDescription((EnumImcomeType)x.LoanBriefJob.ImcomeType) : null,
                        CompanyInsurance = (x.LoanBriefJob != null && x.LoanBriefJob.CompanyInsurance == true) ? "Có tham gia" : "Không tham gia",
                        WorkLocation = (x.LoanBriefJob != null && x.LoanBriefJob.WorkLocation == true) ? "Cố định" : "Không có định",
                        BusinessPapers = (x.LoanBriefJob != null && x.LoanBriefJob.BusinessPapers == true) ? "Có" : "Không",
                        CompanyPhone = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyPhone : null,
                        CompanyTaxCode = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyTaxCode : null,
                        CompanyName = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyName : null,
                        ProvinceName = (x.LoanBriefJob != null && x.LoanBriefJob.CompanyProvince != null) ? Description.GetDescription((TypeProvince)x.LoanBriefJob.CompanyProvince.Type) + " " + x.LoanBriefJob.CompanyProvince.Name : null,
                        DistrictName = (x.LoanBriefJob != null && x.LoanBriefJob.CompanyDistrict != null) ? Description.GetDescription((TypeDistrict)x.LoanBriefJob.CompanyDistrict.Type) + " " + x.LoanBriefJob.CompanyDistrict.Name : null,
                        CompanyAddress = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyAddress : null,
                        CompanyAddressLatLng = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyAddressLatLng : null,
                        CompanyAddressGoogleMap = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyAddressGoogleMap : null,
                    } : null,
                    ListRelativesAndColleagues = x.LoanBriefRelationship != null ? x.LoanBriefRelationship
                    .Select(r => new RelativesAndColleagues()
                    {
                        RelationType = r.RelationshipTypeNavigation != null ? r.RelationshipTypeNavigation.Name : null,
                        FullName = r.FullName,
                        Phone = r.Phone,
                        Address = r.Address,
                    }).ToList() : null,
                    LoanBriefForm = new LoanBriefForm()
                    {
                        ProductName = x.Product != null ? x.Product.Name : null,
                        RateType = x.RateTypeId.HasValue ? Description.GetDescription((EnumRateType)x.RateTypeId) : null,
                        ProductDetail = x.ProductDetailId.HasValue ? Description.GetDescription((LOS.Common.Extensions.InfomationProductDetail)x.RateTypeId) : null,
                        LTV = x.Ltv.HasValue ? x.Ltv.ToString() + "%" : null,
                        Appraiser = (x.LoanBriefQuestionScript != null && x.LoanBriefQuestionScript.Appraiser.HasValue) ? Description.GetDescription((EnumAppraiser)x.LoanBriefQuestionScript.Appraiser) : null,
                        DocumentBusiness = (x.LoanBriefQuestionScript != null && x.LoanBriefQuestionScript.DocumentBusiness.HasValue && x.LoanBriefQuestionScript.DocumentBusiness <= 3) ? Description.GetDescription((EnumDocumentJob1)x.LoanBriefQuestionScript.DocumentBusiness) : x.LoanBriefQuestionScript.DocumentBusiness > 3 ? Description.GetDescription((EnumDocumentJob2)x.LoanBriefQuestionScript.DocumentBusiness) : null,
                    },
                    AssetsInformation = x.LoanBriefProperty != null ? new AssetsInformation()
                    {
                        BrandName = (x.LoanBriefProperty != null && (x.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                                    || x.ProductId == (int)EnumProductCredit.CamotoCreditType
                                    || x.ProductId == (int)EnumProductCredit.OtoCreditType_KCC)) ? x.LoanBriefProperty.CarManufacturer : x.LoanBriefProperty.Brand != null ? x.LoanBriefProperty.Brand.Name : null,
                        PlateNumber = (x.LoanBriefProperty != null && (x.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                                    || x.ProductId == (int)EnumProductCredit.CamotoCreditType
                                    || x.ProductId == (int)EnumProductCredit.OtoCreditType_KCC)) ? x.LoanBriefProperty.PlateNumberCar : x.LoanBriefProperty.PlateNumber,
                        LoanAmountExpertise = x.LoanAmountExpertiseAi,
                        CarName = (x.LoanBriefProperty != null && (x.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                                    || x.ProductId == (int)EnumProductCredit.CamotoCreditType
                                    || x.ProductId == (int)EnumProductCredit.OtoCreditType_KCC)) ? x.LoanBriefProperty.CarName : x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.FullName : null,
                        Locate = x.IsLocate == true ? "Có gắn" : "Không gắn",
                        LoanAmountExpertiseLast = x.LoanAmountExpertiseLast,
                        OwnerFullName = x.LoanBriefProperty != null ? x.LoanBriefProperty.OwnerFullName : null,
                        Chassis = x.LoanBriefProperty != null ? x.LoanBriefProperty.Chassis : null,
                        Engine = x.LoanBriefProperty != null ? x.LoanBriefProperty.Engine : null,
                        MotobikeCertificateNumber = x.LoanBriefProperty != null ? x.LoanBriefProperty.MotobikeCertificateNumber : null,
                        MotobikeCertificateDate = x.LoanBriefProperty != null ? x.LoanBriefProperty.MotobikeCertificateDate : null,
                        MotobikeCertificateAddress = x.LoanBriefProperty != null ? x.LoanBriefProperty.MotobikeCertificateAddress : null,
                        PostRegistration = (x.LoanBriefProperty != null && x.LoanBriefProperty.PostRegistration == true) ? "Có" : "Không",
                        Description = x.LoanBriefProperty != null ? x.LoanBriefProperty.Description : null,
                    } : null,
                    LoanAmountInfomation = new LoanAmountInfomation()
                    {
                        LoanAmount = x.LoanAmount,
                        LoanTime = x.LoanTime,
                        CreatedTime = x.CreatedTime != null ? x.CreatedTime.Value.ToString("dd/MM/yyyy") : null,
                        ToDate = (x.ToDate.HasValue && x.ToDate != DateTime.MinValue) ? x.ToDate.Value.ToString("dd/MM/yyyy") : null,
                        FeeInsuranceOfCustomer = x.FeeInsuranceOfCustomer,
                        RateMoney = x.RateMoney,
                        BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                        BuyInsuranceProperty = x.LoanBriefProperty != null ? x.LoanBriefProperty.BuyInsuranceProperty : null,
                        LenderRate = x.LenderRate,
                        FeeInsuranceOfProperty = x.FeeInsuranceOfProperty
                    },
                    CICInfomation = new CICInfomation()
                    {
                        ResultRuleCheck = x.ResultRuleCheck,
                        ResultLocation = x.ResultLocation,
                        CreditScoring = x.LeadScore
                    },
                    DisbursementInfomation = new DisbursementInfomation()
                    {
                        ReceivingMoneyType = x.ReceivingMoneyType.HasValue ? Description.GetDescription((EnumTypeReceivingMoney)x.ReceivingMoneyType) : null,
                        BankName = x.Bank != null ? x.Bank.Name : null,
                        BankAccountName = x.BankAccountName,
                        BankAccountNumber = x.BankAccountNumber,
                        BankCardNumber = x.BankCardNumber,
                        BankBranch = x.BankBranch,
                    }
                };

            }
        }
    }
    public class CustomerInfomation
    {
        public string PlatformType { get; set; }
        public string TypeLoanBrief { get; set; }
        public int? CountCall { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string PhoneOther { get; set; }
        public string NationalCard { get; set; }
        public string NationCardPlace { get; set; }
        public string Dob { get; set; }
        public string Passport { get; set; }
        public string Gender { get; set; }
        public string FacebookAddress { get; set; }
        public string InsurenceNumber { get; set; }
        public string InsurenceNote { get; set; }
        public string Merried { get; set; }
        public string NumberBaby { get; set; }
        public string LivingWith { get; set; }
        public string Sim { get; set; }
        public string Email { get; set; }
    }
    public class VAInfomation
    {
        public string VaAccountName { get; set; }
        public string VaAccountNumber { get; set; }
        public string VaBankName { get; set; }
        public string GpayAccountName { get; set; }
        public string GpayAccountNumber { get; set; }
    }
    public class FraudCheckInfomation
    {
        public string Channel { get; set; }
        public List<string> Message { get; set; } = new List<string>();
        public string DetailUrl { get; set; }
        public string HvChannel { get; set; }
        public List<string> HvMessage { get; set; } = new List<string>();
    }
    public class CompanyInformation
    {
        public string CompanyName { get; set; }
        public string CareerBusiness { get; set; }
        public string BusinessCertificationDate { get; set; }
        public string BusinessCertificationAddress { get; set; }
        public string HeadOffice { get; set; }
        public string Address { get; set; }
        public string CompanyShareholder { get; set; }
        public string BirthdayShareholder { get; set; }
        public string CardNumberShareholder { get; set; }
        public string CardNumberShareholderDate { get; set; }
        public string PlaceOfBirthShareholder { get; set; }
    }
    public class AddressHome
    {
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public string ResidentType { get; set; }
        public string LivingTime { get; set; }
        public string CustomerShareLocation { get; set; }
        public string AddressLatLng { get; set; }
        public string Address { get; set; }
        public string AddressGoogleMap { get; set; }
    }
    public class AddressHousehold
    {
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public string Address { get; set; }
    }
    public class WorkInformation
    {
        public string JobName { get; set; }
        public int? JobDescriptionId { get; set; }
        public string JobDescriptionName { get; set; }
        public string Description { get; set; }
        public decimal? TotalIncome { get; set; }
        public string ImcomeType { get; set; }
        public string CompanyInsurance { get; set; }
        public string WorkLocation { get; set; }
        public string BusinessPapers { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyTaxCode { get; set; }
        public string CompanyName { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyAddressLatLng { get; set; }
        public string CompanyAddressGoogleMap { get; set; }
    }
    public class RelativesAndColleagues
    {
        public string RelationType { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    }
    public class LoanBriefForm
    {
        public string ProductName { get; set; }
        public string RateType { get; set; }
        public string ProductDetail { get; set; }
        public string LTV { get; set; }
        public string Appraiser { get; set; }
        public string DocumentBusiness { get; set; }
    }
    public class AssetsInformation
    {
        public string BrandName { get; set; }
        public string PlateNumber { get; set; }
        public decimal? LoanAmountExpertise { get; set; }
        public string CarName { get; set; }
        public string Locate { get; set; }
        public decimal? LoanAmountExpertiseLast { get; set; }
        public string OwnerFullName { get; set; }
        public string Chassis { get; set; }
        public string Engine { get; set; }
        public string MotobikeCertificateNumber { get; set; }
        public string MotobikeCertificateDate { get; set; }
        public string MotobikeCertificateAddress { get; set; }
        public string PostRegistration { get; set; }
        public string Description { get; set; }
    }
    public class LoanAmountInfomation
    {
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public string CreatedTime { get; set; }
        public string ToDate { get; set; }
        public decimal? LoanAmountReceived { get; set; }
        public decimal? TotalFeeInsurence { get; set; }
        public decimal? FeeInsuranceOfCustomer { get; set; }
        public decimal? RateMoney { get; set; }
        public bool? BuyInsurenceCustomer { get; set; }
        public bool? BuyInsuranceProperty { get; set; }
        public double? LenderRate { get; set; }
        public decimal? FeeInsuranceOfProperty { get; set; }
    }
    public class CICInfomation
    {
        public string ResultRuleCheck { get; set; }
        public string ResultLocation { get; set; }
        public decimal? CreditScoring { get; set; }
    }
    public class DisbursementInfomation
    {
        public string ReceivingMoneyType { get; set; }
        public string BankName { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }
        public string BankBranch { get; set; }
    }
}


