﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class LoanBriefDocumentDTO
	{
		public int LoanBriefDocumentId { get; set; }
		public int? LoanBriefId { get; set; }
		public int? DocumentId { get; set; }
		public string FilePath { get; set; }
		public string RelativePath { get; set; }
		public DateTimeOffset? UploadedTime { get; set; }
		public int? Status { get; set; }
	}
}
