﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class LoanBriefFileDTO
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public string FilePath { get; set; }
        public int? UserId { get; set; }
        public int? Status { get; set; }
        public int? TypeId { get; set; }
        public int? S3status { get; set; }
        public string LinkImgOfPartner { get; set; }
        public int? TypeFile { get; set; }
        public int? Synchronize { get; set; }
        public string DomainOfPartner { get; set; }
        public int? MecashId { get; set; }
        public int? SourceUpload { get; set; }
        public DateTime? CreateAt { get; set; }
        public string FileThumb { get; set; }
        public string LatLong { get; set; }
        public string LatLongAddress { get; set; }
        public string AddressUpload { get; set; }
        public decimal? DistanceAddressHomeGoogleMap { get; set; }
        public decimal? DistanceAddressCompanyGoogleMap { get; set; }
        public int? AsyncLocal { get; set; }
    }

    public class LoanBriefFileDetail : LoanBriefFileDTO
    {
        public static Expression<Func<LoanBriefFiles, LoanBriefFileDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefFileDetail()
                {
                    Id = x.Id,
                    LoanBriefId = x.LoanBriefId,
                    FilePath = x.FilePath,
                    UserId = x.UserId,
                    Status = x.Status,
                    DomainOfPartner = x.DomainOfPartner,
                    S3status = x.S3status,
                    TypeId = x.TypeId,
                    MecashId = x.MecashId,
                    CreateAt = x.CreateAt,
                    FileThumb = x.FileThumb,
                    LatLong = x.LatLong,
                    TypeFile = x.TypeFile,
                    SourceUpload = x.SourceUpload,
                    AddressUpload = x.AddressUpload,
                    DistanceAddressHomeGoogleMap = x.DistanceAddressHomeGoogleMap,
                    DistanceAddressCompanyGoogleMap = x.DistanceAddressCompanyGoogleMap,
                    AsyncLocal = x.AsyncLocal,
                    LinkImgOfPartner = x.LinkImgOfPartner
                };
            }
        }
    }
}
