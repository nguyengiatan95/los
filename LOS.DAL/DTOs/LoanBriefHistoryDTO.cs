﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class LoanBriefHistoryDTO
	{
		public int LoanBriefHistoryId { get; set; }
		public int? LoanBriefId { get; set; }
		public string OldValue { get; set; }
		public string NewValue { get; set; }
		public int? ActorId { get; set; }
		public string Action { get; set; }
		public DateTimeOffset? CreatedTime { get; set; }
	}
}
