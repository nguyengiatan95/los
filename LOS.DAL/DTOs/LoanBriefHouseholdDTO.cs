﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class LoanBriefHouseholdDTO
	{
		public int LoanBriefId { get; set; }
		public string Address { get; set; }
		public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
		public string DistrictName { get; set; }
		public int? WardId { get; set; }
		public string WardName { get; set; }
		public string AppartmentNumber { get; set; }
		public string Lane { get; set; }
		public string Block { get; set; }
		public int? NoOfFamilyMembers { get; set; }
		public int? Status { get; set; }
		public string Ownership { get; set; }
	}
}
