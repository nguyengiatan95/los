﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class LoanBriefImportFileExcelDTO
    {
        public int Id { get; set; }
        public string FileImportName { get; set; }
        public string FileImportId { get; set; }
        public short? Status { get; set; }
        public int? LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public short? Gender { get; set; }
        public short? Age { get; set; }
        public string NationalCard { get; set; }
        public long? Salary { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string CityNameHouseHold { get; set; }
        public string DistrictNameHouseHold { get; set; }
        public string CompanyName { get; set; }
        public string ProductName { get; set; }
        public string Source { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? CityIdHouseHold { get; set; }
        public int? DistrictIdHouseHold { get; set; }
        public int? ProductId { get; set; }
        public string Note { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }

    public class LoanBriefImportFileExcelDetail : LoanBriefImportFileExcelDTO
    {
        public static Expression<Func<LoanBriefImportFileExcel, LoanBriefImportFileExcelDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefImportFileExcelDetail
                {
                    Id = x.Id,
                    FileImportName = x.FileImportName,
                    FileImportId = x.FileImportId,
                    Status = x.Status,
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    Gender = x.Gender,
                    Age = x.Age,
                    NationalCard = x.NationalCard,
                    Salary = x.Salary,
                    CityName = x.CityName,
                    DistrictName = x.DistrictName,
                    CityNameHouseHold = x.CityNameHouseHold,
                    DistrictNameHouseHold = x.DistrictNameHouseHold,
                    CompanyName = x.CompanyName,
                    ProductName = x.ProductName,
                    Source = x.Source,
                    CityId = x.CityId,
                    DistrictId = x.DistrictId,
                    CityIdHouseHold = x.CityIdHouseHold,
                    DistrictIdHouseHold = x.DistrictIdHouseHold,
                    ProductId = x.ProductId,
                    Note = x.Note,
                    CreateDate = x.CreateDate,
                    UpdateDate = x.UpdateDate,
                };
            }
        }
    }
}
