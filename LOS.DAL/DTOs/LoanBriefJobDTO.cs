﻿using LOS.Common.Extensions;
using LOS.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class LoanBriefJobDTO
	{
		public int LoanBriefId { get; set; }
		public int? ImcomeType { get; set; }
		public int? BusinessType { get; set; }
		public int? JobId { get; set; }
		public string JobName { get; set; }
		public int? JobTitleId { get; set; }
		public decimal? TotalIncome { get; set; }
		public bool? DocumentAvailable { get; set; }
		public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyAddress { get; set; }
		public string CompanyTaxCode { get; set; }
		public int? CompanyProvinceId { get; set; }
        public string CompanyProvinceName { get; set; }
        public int? CompanyDistrictId { get; set; }
		public string CompanyDistrictName { get; set; }
		public int? CompanyWardId { get; set; }
		public string CompanyWardName { get; set; }
		public string WorkingAddress { get; set; }
		public string DepartmentName { get; set; }
		public int? WorkingTime { get; set; }
		public int? ContractType { get; set; }
		public int? ContractRemain { get; set; }
		public int? PaidType { get; set; }
		public string CardNumber { get; set; }
		public string BankName { get; set; }
		public string Description { get; set; }

        public string ImcomeTypeName
        {
            get
            {
                var result = string.Empty;
                if (ImcomeType > 0 && Enum.IsDefined(typeof(EnumImcomeType), ImcomeType))
                    return ExtentionHelper.GetDescription((EnumImcomeType)ImcomeType);
                return result;
            }
        }
    }

	public class LoanBriefJobDetail : LoanBriefJobDTO
	{
		public JobDTO Job { get; set; }
	}
}
