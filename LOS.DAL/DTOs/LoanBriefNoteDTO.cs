﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class LoanBriefNoteDTO
	{
		public int LoanBriefNoteId { get; set; }
		public int? LoanBriefId { get; set; }
		public int? Type { get; set; }

        public int? UserId { get; set; }
        public string Note { get; set; }
		public int? Status { get; set; }

        public string FullName { get; set; }
        public string ShopName { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public int? ActionComment { get; set; }

    }
    public static class LoanBriefNoteDTOExtensions
    {
        public static LoanBriefNote Mapping(this LoanBriefNoteDTO dto)
        {
            var loanbriefnote = new LoanBriefNote();
            loanbriefnote.LoanBriefNoteId = dto.LoanBriefNoteId;
            loanbriefnote.LoanBriefId = dto.LoanBriefId;
            loanbriefnote.Type = dto.Type;
            loanbriefnote.UserId = dto.UserId;
            loanbriefnote.Note = dto.Note;
            loanbriefnote.Status = dto.Status;
            loanbriefnote.FullName = dto.FullName;
            loanbriefnote.CreatedTime = dto.CreatedTime;
            return loanbriefnote;
        }
    }
    public class LoanBriefNoteDetail : LoanBriefNoteDTO
    {
        public static Expression<Func<LoanBriefNote, LoanBriefNoteDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefNoteDetail()
                {
                    LoanBriefNoteId = x.LoanBriefNoteId,
                    LoanBriefId = x.LoanBriefId,
                    Type = x.Type,
                    Note = x.Note,
                    Status = x.Status,
                    UserId = x.UserId,
                    FullName = x.FullName,
                    CreatedTime = x.CreatedTime,
                    ShopName = x.ShopName,
                    ActionComment = x.ActionComment
                };
            }
        }
    }
}
