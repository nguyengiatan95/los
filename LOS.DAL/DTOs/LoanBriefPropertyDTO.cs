﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class LoanBriefPropertyDTO
	{
		public int LoanBriefId { get; set; }
		public int? PropertyType { get; set; }
		public string Brand { get; set; }
		public string Product { get; set; }
		public string YearMade { get; set; }
		public int? BrandId { get; set; }
		public int? ProductId { get; set; }
		public int? Ownership { get; set; }
		public bool? DocumentAvailable { get; set; }
		public int? Name { get; set; }
		public int? Age { get; set; }
		public DateTimeOffset CreatedTime { get; set; }
		public DateTimeOffset? UpdatedTime { get; set; }
	}
}
