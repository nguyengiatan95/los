﻿using LOS.Common.Extensions;
using LOS.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class LoanBriefResidentDTO
	{
		public int LoanBriefId { get; set; }
		public string Address { get; set; }
		public int? ProvinceId { get; set; }
		public int? DistrictId { get; set; }
		public string DistrictName { get; set; }
		public int? WardId { get; set; }
		public string WardName { get; set; }
		public string AppartmentNumber { get; set; }
		public string Lane { get; set; }
		public string Block { get; set; }
		public int? ResidentType { get; set; }
		public int? LivingTime { get; set; }
		public int? Ownership { get; set; }
		public int? LivingWith { get; set; }
		public string Direction { get; set; }

        public string LivingTimeName
        {
            get
            {
                var result = string.Empty;
                if (LivingTime > 0 && Enum.IsDefined(typeof(EnumLivingTime), LivingTime))
                    return ExtentionHelper.GetDescription((EnumLivingTime)LivingTime);
                return result;
            }
        }
    }
}
