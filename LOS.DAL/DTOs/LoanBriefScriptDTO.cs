﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
   public class LoanBriefScriptDTO
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string ScriptView { get; set; }
    }

    public class LoanBriefScriptDetail : LoanBriefScriptDTO
    {
        public static Expression<Func<LoanbriefScript, LoanBriefScriptDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefScriptDetail()
                {
                    Id = x.Id,
                    ProductId = x.ProductId,
                    ScriptView = x.ScriptView
                };
            }
        }
    }
}
