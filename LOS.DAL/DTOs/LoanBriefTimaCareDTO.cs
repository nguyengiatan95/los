﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using LOS.DAL.Object;

namespace LOS.DAL.DTOs
{

    public class LoanBriefTimaCareDTO
    {

        
        public int LoanBriefId { get; set; }
        
        //1
        public string FullName { get; set; }
        public string NationalCard { get; set; }
        public DateTime? Dob { get; set; }
        public string Phone { get; set; }

        //2
        public LoanBriefResidentForDetail LoanBriefResident { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public string Address { get; set; }

        //3
        public int? RateTypeId { get; set; }
        public string ProductName { get; set; }
        public int? ProductId { get; set; }
        public int? ProductDetailId { get; set; }

        //4
        public LoanBriefPropertyForDetail LoanBriefProperty { get; set; }
        //5      
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public bool? BuyInsurenceCustomer { get; set; }
        public int? Status { get; set; }
        public decimal? FeeInsuranceOfCustomer { get; set; }
        public decimal? FeeInsuranceOfProperty { get; set; }

        //6
        public string BankName { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccountNumber { get; set; }

        public int? CustomerId { get; set; }

        public string EsignBorrowerContract { get; set; }
        public string EsignAgreementId { get; set; }

        public bool? IsCheckEkyc { get; set; }

        public string RedirectUrl { get; set; }
        public string AddressHousehold { get; set; }

        public int? TypeRemarketing { get; set; }


        public static Expression<Func<LoanBrief, LoanBriefTimaCareDTO>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefTimaCareDTO()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    NationalCard = x.NationalCard,
                    Dob = x.Dob,
                    Phone = x.Phone,

                    ProvinceName = x.Province != null ? x.Province.Name : null,
                    DistrictName = x.District != null ? x.District.Name : null,
                    WardName = x.Ward != null ? x.Ward.Name : null,
                    LoanBriefResident = x.LoanBriefResident != null ? new LoanBriefResidentForDetail()
                    {                       
                        LivingTime = x.LoanBriefResident.LivingTime,
                        Address = x.LoanBriefResident.Address,
                        ResidentType = x.LoanBriefResident.ResidentType
                    } : null,

                    RateTypeId = x.RateTypeId,
                    ProductName = x.Product != null ? x.Product.Name : null,
                    ProductId = x.ProductId,
                    ProductDetailId = x.ProductDetailId,

                    LoanBriefProperty = x.LoanBriefProperty != null ? new LoanBriefPropertyForDetail()
                    {
                        BrandName = x.LoanBriefProperty.Brand != null ? x.LoanBriefProperty.Brand.Name : null,
                        PlateNumber = x.LoanBriefProperty.PlateNumber,
                        BuyInsuranceProperty = x.LoanBriefProperty.BuyInsuranceProperty,
                        CarManufacturer = x.LoanBriefProperty.CarManufacturer,
                        CarName = x.LoanBriefProperty.CarName,
                        ProductFullName = x.LoanBriefProperty.Product != null ? x.LoanBriefProperty.Product.FullName : null,
                        PlateNumberCar = x.LoanBriefProperty.PlateNumberCar
                    } : null,

                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    BuyInsurenceCustomer = x.BuyInsurenceCustomer,
                    Status = x.Status,
                    FeeInsuranceOfCustomer = x.FeeInsuranceOfCustomer,
                    FeeInsuranceOfProperty = x.FeeInsuranceOfProperty,

                    BankName = x.Bank != null ? x.Bank.Name : null,
                    BankAccountName = x.BankAccountName,
                    BankAccountNumber = x.BankAccountNumber,
                                      
                    CustomerId = x.CustomerId,
                    EsignBorrowerContract = x.EsignBorrowerContract,
                    EsignAgreementId = x.Customer != null ? x.Customer.EsignAgreementId : null,
                    AddressHousehold = x.LoanBriefHousehold != null ? x.LoanBriefHousehold.Address : null,
                    TypeRemarketing = x.TypeRemarketing

                };
            }
        }

    }

    public class CheckValidate : LoanBrief
    {

        public static Expression<Func<LoanBrief, CheckValidate>> ProjectionDetail
        {
            get
            {
                return x => new CheckValidate()
                {
                    LoanBriefId = x.LoanBriefId,
                    CustomerId = x.CustomerId,
                    FullName = x.FullName,
                    NationalCard = x.NationalCard,
                    Dob = x.Dob,
                    Phone = x.Phone,
                    RateTypeId = x.RateTypeId,

                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    Status = x.Status,  
                    LoanPurpose = x.LoanPurpose,

                    ReceivingMoneyType = x.ReceivingMoneyType,
                    BankId = x.BankId,
                    BankAccountName = x.BankAccountName,
                    BankAccountNumber = x.BankAccountNumber,

                    LoanBriefProperty = x.LoanBriefProperty ?? null,

                    LoanBriefResident = x.LoanBriefResident ?? null,
                    LoanBriefHousehold = x.LoanBriefHousehold ?? null,
                    ProductId = x.ProductId,
                    EsignState = x.EsignState,
                    EsignBorrowerContract = x.EsignBorrowerContract,
                    TypeRemarketing = x.TypeRemarketing,
                    IsLocate = x.IsLocate
                };
            }
        }
    }

}



