﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class LoanProductDTO
	{
		public int LoanProductId { get; set; }
		public string Name { get; set; }
		public int? Status { get; set; }
		public double? ConsultantRate { get; set; }
		public double? ServiceRate { get; set; }
		public double? LoanRate { get; set; }
		public decimal? MaximumMoney { get; set; }
	}
}
