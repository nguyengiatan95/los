﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class LoanRateTypeDTO
	{
		public int LoanRateTypeId { get; set; }
		public string RateType { get; set; }
		public int? Priority { get; set; }
	}
}
