﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class LoanRelationshipDTO
	{
        public int Id { get; set; }
        public int? RelationshipType { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int? LoanBriefId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }

	public class LoanRelationshipDetail : LoanRelationshipDTO
	{
		public static Expression<Func<LoanBriefRelationship, LoanRelationshipDetail>> ProjectionDetail
		{
			get
			{
				return x => new LoanRelationshipDetail()
				{
					Id = x.Id,
					LoanBriefId = x.LoanBriefId,
					Phone = x.Phone
				};
			}
		}
	}
}
