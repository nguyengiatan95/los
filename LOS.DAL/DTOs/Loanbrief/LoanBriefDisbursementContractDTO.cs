﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class LoanBriefDisbursementContractDTO
    {
        public int LoanBriefId { get; set; }
        public string UtmSource { get; set; }
        public string FullName { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTime? DisbursementAt { get; set; }
        public decimal? LoanAmount { get; set; }
        public string ProductName { get; set; }
        public int? LoanTime { get; set; }
        public string Phone { get; set; }
        public int? TypeLoanSupport { get; set; }
        public int? StatusUser { get; set; }
        public int? BoundTelesaleId { get; set; }
        public string BoundTelesaleName { get; set; }
    }
    public class LoanBriefDisbursementContractDetail : LoanBriefDisbursementContractDTO
    {
        public static Expression<Func<LoanBrief, LoanBriefDisbursementContractDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefDisbursementContractDetail
                {
                    LoanBriefId = x.LoanBriefId,
                    UtmSource = x.UtmSource,
                    FullName = x.FullName,
                    ProvinceName = x.Province != null ? x.Province.Name : null,
                    DistrictName = x.District != null ? x.District.Name : null,
                    CreatedTime = x.CreatedTime,
                    DisbursementAt = x.DisbursementAt,
                    LoanAmount = x.LoanAmount,
                    ProductName = x.Product != null ? x.Product.Name : null,
                    LoanTime = x.LoanTime,
                    Phone = x.Phone,
                    TypeLoanSupport = x.TypeLoanSupport,
                    StatusUser = x.BoundTelesale != null ? x.BoundTelesale.Status : null,
                    BoundTelesaleId = x.BoundTelesaleId,
                    BoundTelesaleName = x.BoundTelesale != null ? x.BoundTelesale.Username : null,
                };
            }
        }
    }
    public class LoanBriefDisbursementContractView
    {
        public List<LoanBriefDisbursementContractDetail> ListDisbursementContractDetail { get; set; }
        public int TotalCount { get; set; }
    }
}
