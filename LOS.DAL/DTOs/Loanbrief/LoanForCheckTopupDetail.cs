﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs.Loanbrief
{
    public class LoanForCheckTopupDetail
    {
        public int LoanbriefId { get; set; }
        public string FullName { get; set; }
        public string NationalCard { get; set; }
        public int? TypeRemarking { get; set; }
        public int? LoanStatus { get; set; }
        public int? LmsLoanId { get; set; }
        public int? ParentLoanbriefId { get; set; }
        public int? ParentLoanStatus { get; set; }
        public int? ParentLoanTypeRemarketing { get; set; }

    }

    public class CheckTopupResult
    {
        public bool IsCanTopup { get; set; } = false;
        public bool IsTopupOnTopup { get; set; } = false;
        public int LoanbriefId { get; set; }
        public string Message { get; set; }
        public decimal CurrentDebt { get; set; }
    }
}
