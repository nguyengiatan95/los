﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs.Loanbrief
{
   public class LoanbriefNotCancelDTO
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public int? Status { get; set; }
    }

    public class LoanbriefNotCancelDetail : LoanbriefNotCancelDTO
    {
        public static Expression<Func<LoanBrief, LoanbriefNotCancelDetail>> Projection
        {
            get
            {
                return x => new LoanbriefNotCancelDetail
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    Status = x.Status
                };
            }
        }
    }

    public class LoanBriefCheckCavet
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public int? CodeId { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public int? Status { get; set; }
        public DateTime? DisbursementAt { get; set; }
        public bool? IsLocate { get; set; }
        public int? DeviceStatus { get; set; }

        //Tình trạng định vị
        public string StatusDevices { get; set; }
        //Tình trạng cà vẹt
        public string StatusVehicleRegistration { get; set; }
        //Đề xuất mượn cà vẹt
        public string ProposeBorrow { get; set; }
        //Trạng thái đề xuất
        public string StatusPropose { get; set; }
        //Thời gian tạo đề xuất
        public DateTime? CreatedAt { get; set; }
        //Mô tả
        public string Description { get; set; }
        public string ProductName { get; set; }
    }

    public class LoanBriefCheckCavetDetail : LoanBriefCheckCavet
    {
        public static Expression<Func<LoanBrief, LoanBriefCheckCavetDetail>> Projection
        {
            get
            {
                return x => new LoanBriefCheckCavetDetail
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    CodeId = x.CodeId,
                    LoanAmount = x.LoanAmount,
                    LoanTime = x.LoanTime,
                    Status = x.Status,
                    DisbursementAt = x.DisbursementAt,
                    IsLocate = x.IsLocate,
                    DeviceStatus = x.DeviceStatus,
                    ProductName = x.Product != null && x.Product.Name != null ? x.Product.Name : ""
                };
            }
        }
    }
}
