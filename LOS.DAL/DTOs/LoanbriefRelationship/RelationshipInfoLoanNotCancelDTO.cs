﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs.LoanbriefRelationship
{
  public  class RelationshipInfoLoanNotCancelDTO
    {
        public int? LoanBriefId { get; set; }
        public string FullName { get; set; }
        public int? Status { get; set; }
        public int? TypeRelationship { get; set; }
        public string Phone { get; set; }
    }

    public class RelationshipInfoLoanNotCancelDetail: RelationshipInfoLoanNotCancelDTO
    {
        public static Expression<Func<LoanBriefRelationship, RelationshipInfoLoanNotCancelDetail>> Projection
        {
            get
            {
                return x => new RelationshipInfoLoanNotCancelDetail
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Status = x.LoanBrief.Status,
                    Phone = x.Phone,
                    TypeRelationship = x.RelationshipType
                };
            }
        }
    }
}
