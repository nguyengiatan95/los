﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
   public class LogDistributionUserDTO
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public int? UserId { get; set; }
        public int? HubId { get; set; }
        public int? TypeDistribution { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? CreatedBy { get; set; }
    }

    public class LogDistributionUserDetail : LogDistributionUserDTO
    {
        public static Expression<Func<LOS.DAL.EntityFramework.LogDistributionUser, LogDistributionUserDetail>> Projection
        {
            get
            {
                return x => new LogDistributionUserDetail()
                {
                    Id = x.Id,
                    LoanbriefId = x.LoanbriefId,
                    UserId = x.UserId,
                    HubId = x.HubId,
                    TypeDistribution = x.TypeDistribution,
                    CreatedAt = x.CreatedAt,
                    CreatedBy = x.CreatedBy
                };
            }
        }
    }
}
