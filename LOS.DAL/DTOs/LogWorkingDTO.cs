﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class LogWorkingDTO
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? State { get; set; }
        public DateTime? CreateAt { get; set; }
    }
}
