﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class ManagerAreaHubDTO
    {
    }
    public class ManagerAreaHubSaveItem
    {
        public int CityId { get; set; }
        public int Shopid { get; set; }
        public int ProductId { get; set; }
        public List<DistrictAndWardItem> ListWardId { get; set; }
    }
    public class DistrictAndWardItem
    {
        public int DistrictID { get; set; }
        public int WardID { get; set; }
    }
}
