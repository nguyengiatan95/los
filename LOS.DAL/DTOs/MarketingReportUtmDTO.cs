﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class MarketingReportUtmDTO
    {
        public int Status { get; set; }
        public string UtmSource { get; set; }
        public int CountLoanBrief { get; set; }
        public decimal SumMoney { get;set; }
        public int CountWaitTelesale{ get; set; }
        public int CountStatusDisbured { get; set; }
        public int CountStatusCanceled { get; set; }
        public double RateDisbured { get; set; }
        public double RateCanceled { get; set; }

    }

    public static class MarketingReportExtensions
    {
        public static MarketingReportUtmDTO Mapping(this LoanBrief dto)
        {
            var report = new MarketingReportUtmDTO();        
            report.UtmSource = dto.UtmSource;
            report.Status = dto.Status.Value;
            return report;
        }
    }

    public class MarketingReportDetail: MarketingReportUtmDTO
    {
        public static Expression<Func<LoanBrief, MarketingReportDetail>> ReportDetail
        {
            get
            {
                return x => new MarketingReportDetail()
                {
                    UtmSource = x.UtmSource,
                    Status = x.Status.Value
                };
            }
        }
    }
}
