﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class ModuleDTO
    {
        public int ModuleId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public int? ParentId { get; set; }
        public int? Priority { get; set; }
        public string Icon { get; set; }
        public bool IsMenu { get; set; }
        public int ApplicationId { get; set; }
    }

    public static class ModuleDTOExtentions
    {
        public static Module Mapping(this ModuleDTO dto)
        {
            var entity = new Module();
            entity.ModuleId = dto.ModuleId;
            entity.Code = dto.Code;
            entity.Name = dto.Name;
            entity.Path = dto.Path;
            entity.Description = dto.Description;
            entity.Status = dto.Status;
            entity.ParentId = dto.ParentId;
            entity.Priority = dto.Priority;
            entity.IsMenu = dto.IsMenu;
            entity.Icon = dto.Icon;
            entity.Controller = dto.Controller;
            entity.Action = dto.Action;
            entity.ApplicationId = dto.ApplicationId;

            return entity;
        }
    }

    public class ModuleDetail : ModuleDTO
    {
        public List<ModuleDetail> Modules { get; set; }

        public bool Selected { get; set; }

        public static Expression<Func<Module, ModuleDetail>> ProjectionDetail
        {
            get
            {
                return x => new ModuleDetail()
                {
                    ModuleId = x.ModuleId,
                    Code = x.Code,
                    Description = x.Description,
                    Name = x.Name,
                    Path = x.Path,
                    Status = x.Status,
                    ParentId = x.ParentId,
                    Priority = x.Priority,
                    IsMenu = x.IsMenu.GetValueOrDefault(),
                    Icon = x.Icon,
                    Controller = x.Controller,
                    Action = x.Action,
                    ApplicationId = x.ApplicationId.GetValueOrDefault(),
                    Modules = x.InverseParent.Select(x1 => new ModuleDetail()
                    {
                        ModuleId = x1.ModuleId,
                        Code = x1.Code,
                        Description = x1.Description,
                        Name = x1.Name,
                        Path = x1.Path,
                        Status = x1.Status,
                        ParentId = x1.ParentId,
                        Priority = x1.Priority,
                        Controller = x.Controller,
                        Action = x.Action,
                        IsMenu = x1.IsMenu.GetValueOrDefault(),
                        Icon = x1.Icon,
                    }).ToList()
                };
            }
        }
    }
}
