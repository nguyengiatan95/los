﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class NotificationDTO
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public string Topic { get; set; }
        public string Action { get; set; }
        public string Message { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? Status { get; set; }
        public DateTime? PushTime { get; set; }
        public string Read { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }

    public class NotificationDetail : NotificationDTO
    {
        public static Expression<Func<Notification, NotificationDetail>> ProjectionDetail
        {
            get
            {
                return x => new NotificationDetail()
                {
                    Id = x.Id,
                    LoanbriefId = x.LoanbriefId,
                    Topic = x.Topic,
                    Action = x.Action,
                    Status = x.Status,
                    Message = x.Message
                };
            }
        }
    }
}
