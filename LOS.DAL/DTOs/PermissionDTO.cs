﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class PermissionDTO
	{
		public int PermissionId { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int? Status { get; set; }
		public bool? DefaultValue { get; set; }
		public int? Priority { get; set; }
	}

	public class PermissionDetail : PermissionDTO
	{

	}
}
