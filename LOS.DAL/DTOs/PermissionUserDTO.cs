﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class PermissionUserDTO
    {
        public int UserId { get; set; }
        public List<UserCondition> UserConditions { get; set; }
        public List<UserActionPermission> UserActionPermissions { get; set; }
    }


    public class PermissionGroupDTO
    {
        public int GroupId { get; set; }
        public List<GroupCondition> GroupConditions { get; set; }
        public List<GroupActionPermission> GroupActionPermissions { get; set; }
    }
}
