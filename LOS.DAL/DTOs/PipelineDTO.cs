﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class PipelineDTO
	{		
		public int PipelineId { get; set; }
		public string Name { get; set; }
		public DateTimeOffset? CreatedAt { get; set; }
		public DateTimeOffset? ModifiedAt { get; set; }
		public int? CreatorId { get; set; }
		public int? Status { get; set; }
		public int? Priority { get; set; }
		public string Note { get; set; }
		public IEnumerable<SectionDTO> Sections { get; set; }
	}

	public class SectionDTO
	{	
		public int SectionId { get; set; }		
		public int? PipelineId { get; set; }
		public string Name { get; set; }
		public int? SectionGroupId { get; set; }
		public int? Order { get; set; }
		public bool? IsShow { get; set; }
		public IEnumerable<SectionDetailDTO> Details { get; set; }		
	}

	public class SectionDetailDTO
	{		
		public int? PipelineId { get; set; }
		public int? SectionDetailId { get; set; }
		public int? SectionId { get; set; }		
		public string Name { get; set; }
		public int? Step { get; set; }
		public string Mode { get; set; }
		public bool? IsShow { get; set; }		
		public int? ReturnId { get; set; }
		public int? DisapproveId { get; set; }
		public IEnumerable<SectionApproveDTO> Approves { get; set; }
		public IEnumerable<SectionApproveDTO> Disapproves { get; set; }
		public SectionDetailDTO Disapprove { get; set; }
		public SectionDetailDTO Return { get; set; }
		public IEnumerable<SectionActionDTO> Actions { get; set; }				
	}

	public class SectionApproveDTO
	{
		public int SectionApproveId { get; set; }
		public int? SectionDetailId { get; set; }
		public string Name { get; set; }
		public int? ApproveId { get; set; }
		public SectionDetailDTO Approve { get; set; }
		public IEnumerable<SectionConditionDTO> Conditions { get; set; }
		public int Index { get; set; }
		public int PipelineId { get; set; }
		public int? Type { get; set; }
	}

	public class SectionActionDTO
	{
		public int SectionActionId { get; set; }
		public int? SectionDetailId { get; set; }
		public int? PropertyId { get; set; }
		public string Value { get; set; }
		public int? Type { get; set; }
		public PropertyDTO Property { get; set; }	
	}

	public class SectionConditionDTO
	{
		public int SectionConditionId { get; set; }
		public int? SectionApproveId { get; set; }
		public int? PropertyId { get; set; }
		public string Operator { get; set; }
		public string Value { get; set; }
		public PropertyDTO Property { get; set; }
	}
}
