﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class PlatformTypeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }

    }
    public class PlatformTypeDetail : PlatformTypeDTO
    {
        public static Expression<Func<PlatformType, PlatformTypeDetail>> ProjectionDetail
        {
            get
            {
                return x => new PlatformTypeDetail
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsEnable = x.IsEnable
                };
            }
        }
    } 
}
