﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class PositionDTO
	{
		public int PositionId { get; set; }
		public string Name { get; set; }
		public int? Status { get; set; }
	}

    public static class PositionDTOExtensions
    {
        public static Position Mapping(this PositionDTO dto)
        {
            var position = new Position();
            position.PositionId = dto.PositionId;
            position.Name = dto.Name;
            position.Status = dto.Status;
            return position;
        }
    }

    public static class PositionExtensions
    {
        public static PositionDTO Mapping(this Position dto)
        {
            var position = new PositionDTO();
            position.PositionId = dto.PositionId;
            position.Name = dto.Name;
            position.Status = dto.Status;
            return position;
        }
    }


    public class PositionDetail : PositionDTO
    {
        public static Expression<Func<Position, PositionDetail>> ProjectionDetail
        {
            get
            {
                return x => new PositionDetail()
                {
                    PositionId = x.PositionId,
                    Name = x.Name,
                    Status = x.Status
                };
            }
        }
    }
}
