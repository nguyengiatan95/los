﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class ProductDTO
    {
        public int Id { get; set; }
        public int BrandProductId { get; set; }
        public int ProductTypeId { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public long? Price { get; set; }
        public int? Year { get; set; }
        public long? PriceCurrent { get; set; }
        public int? BrakeType { get; set; }
        public int? RimType { get; set; }
        public string ShortName { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? UpdateBy { get; set; }
        public bool? IsEnable { get; set; }
        public DateTime? UpdateTime { get; set; }

        public BrandProductDTO BrandProducts { get; set; }
    }

    public class ProductDetail : ProductDTO
    {
        public static Expression<Func<Product, ProductDetail>> ProjectionDetail
        {
            get
            {
                return x => new ProductDetail()
                {
                    Id = x.Id,
                    BrandProductId = x.BrandProductId,
                    ProductTypeId = x.ProductTypeId,
                    Name = x.Name,
                    FullName = x.FullName,
                    Price = x.Price,
                    Year = x.Year,
                    BrakeType = x.BrakeType,
                    RimType = x.RimType,
                    ShortName = x.ShortName,
                    CreateBy = x.CreateBy,
                    CreateTime = x.CreateTime,
                    UpdateBy = x.UpdateBy,
                    UpdateTime = x.UpdateTime,
                    IsEnable = x.IsEnable,
                    BrandProducts = x.BrandProduct != null ? new BrandProductDTO()
                    {
                        Id = x.BrandProduct.Id,
                        Name = x.BrandProduct.Name
                    } : null,
                };
            }
        }
    }
    public class ProductPercentReductionDTO
    {
        public int Id { get; set; }
        public int BrandProductId { get; set; }
        public int ProductTypeId { get; set; }
        public int? CountYear { get; set; }
        public int? PercentReduction { get; set; }
    }
    public class ProductPercentReductionDetail : ProductPercentReductionDTO
    {
        public static Expression<Func<ProductPercentReduction, ProductPercentReductionDetail>> ProjectionDetail
        {
            get
            {
                return x => new ProductPercentReductionDetail()
                {
                    Id = x.Id,
                    BrandProductId = x.BrandProductId,
                    ProductTypeId = x.ProductTypeId,
                    CountYear = x.CountYear,
                    PercentReduction = x.PercentReduction
                };
            }
        }
    }
    public class ProductReviewDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int? Status { get; set; }
        public bool? State { get; set; }
        public bool? IsCancel { get; set; }
        public int? IdType { get; set; }
        public bool? IsCheck { get; set; }
        public long? MoneyDiscount { get; set; }
        public decimal? PecentDiscount { get; set; }
    }
    public class ProductReviewDetailAPI : ProductReviewDTO
    {
        public static Expression<Func<ProductReview, ProductReviewDetailAPI>> ProjectionDetail
        {
            get
            {
                return x => new ProductReviewDetailAPI()
                {
                    Id = x.Id,
                    Name = x.Name,
                    ParentId = x.ParentId,
                    Status = x.Status,
                    State = x.State,
                    IsCancel = x.IsCancel,
                    IdType = (x.ProductReviewDetail != null && x.ProductReviewDetail.Count > 0) ? x.ProductReviewDetail.First().ProductTypeId : null,
                    IsCheck = (x.ProductReviewResult != null && x.ProductReviewResult.Count > 0) ? x.ProductReviewResult.First().IsCheck : null,
                    MoneyDiscount = 0,
                };
            }
        }

    }
    public class ProductReviewResultObject
    {
        public int LoanBriefId { get; set; }
        public long TotalMoneyExpertise { get; set; }
        public long TotalMoney { get; set; }
        public int BrandId { get; set; }
        public List<ProductReviewResult> ProductReviewResults { get; set; }
        public ProductReviewResultDetail ProductReviewResultDetail { get; set; }
        public LoanBriefNote LoanBriefNote { get; set; }
    }
    public static class ProductDTOExtions
    {
        public static Product Mapping(this ProductDTO dto)
        {
            var product = new Product();
            product.Id = dto.Id;
            product.BrandProductId = dto.BrandProductId;
            product.ProductTypeId = dto.ProductTypeId;
            product.Name = dto.Name;
            product.FullName = dto.FullName;
            product.Price = dto.Price;
            product.Year = dto.Year;
            product.BrakeType = dto.BrakeType;
            product.RimType = dto.RimType;
            product.ShortName = dto.ShortName;
            return product;
        }
    }
    public class ProductBasicApi
    {
        public int Id { get; set; }
        public int BrandProductId { get; set; }
        public string FullName { get; set; }
        public int? Year { get; set; }
        public string ShortName { get; set; }
        public static Expression<Func<Product, ProductBasicApi>> ProjectionDetail
        {
            get
            {
                return x => new ProductBasicApi()
                {
                    Id = x.Id,
                    BrandProductId = x.BrandProductId,
                    FullName = x.FullName,
                    Year = x.Year,
                    ShortName = x.ShortName
                };
            }
        }
    }
}
