﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class ProjectDTO
	{
		public int ProjectId { get; set; }
		public string Name { get; set; }
		public int? Status { get; set; }
		public DateTime? CreatedTime { get; set; }
		public DateTime? UpdatedTime { get; set; }
	}
}
