﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class PropertyDTO
	{
		public int PropertyId { get; set; }
		public string Name { get; set; }
		public string Object { get; set; }
		public string FieldName { get; set; }
		public string DefaultValue { get; set; }
		public string Type { get; set; }
		public string LinkObject { get; set; }
		public int? Priority { get; set; }
		public int? Status { get; set; }
		public string Regex { get; set; }
		public string Action { get; set; }
        public bool? MultipleSelect { get; set; }
    }

	public class EditPropertyDTO : PropertyDTO
	{
		public int ParentIndex { get; set; }
		public int Index { get; set; }
		public string Operator { get; set; }
		public string Value { get; set; }
		public string Action { get; set; }
		public int? ActionType { get; set; }
	}

	public class PropertyDetail : PropertyDTO
	{	

		public static Expression<Func<Property, PropertyDetail>> ProjectionDetail
		{
			get
			{
				return x => new PropertyDetail()
				{				
					Name = x.Name,
					Object = x.Object,
					Priority = x.Priority,
					FieldName = x.FieldName,
					PropertyId = x.PropertyId,
					Status = x.Status,
					Type = x.Type,
					Regex = x.Regex,
					DefaultValue = x.DefaultValue,
					LinkObject = x.LinkObject,
                    MultipleSelect = x.MultipleSelect
                };
			}
		}
	}
}
