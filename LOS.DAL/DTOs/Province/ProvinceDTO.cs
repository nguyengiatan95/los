﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using LOS.DAL.EntityFramework;

namespace LOS.DAL.DTOs
{
    public class ProvinceDTO
    {
        public int ProvinceId { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? Priority { get; set; }
        public int? IsApply { get; set; }
    }
    public class ProvinceDetail : ProvinceDTO
    {
        public static Expression<Func<LOS.DAL.EntityFramework.Province, ProvinceDetail>> ProjectionDetail
        {
            get
            {
                return x => new ProvinceDetail()
                {
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    Name = x.Name,
                    Priority = x.Priority,
                    ProvinceId = x.ProvinceId,
                    Type = x.Type,
                    IsApply = x.IsApply,
                };
            }
        }
    }
}
