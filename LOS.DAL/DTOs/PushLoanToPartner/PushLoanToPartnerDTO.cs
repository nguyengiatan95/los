﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class PushLoanToPartnerDTO
    {
        public int Id { get; set; }
        public int? LoanPartnerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public int? PushAction { get; set; }
        public DateTime? PushDate { get; set; }
        public int? Status { get; set; }
        public DateTime? SynchronizedDate { get; set; }
        public int? Partner { get; set; }
        public string Comment { get; set; }
        public string Tid { get; set; }
        public string AffCode { get; set; }
        public string Domain { get; set; }
        public string UtmSource { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? LoanbriefId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public string UtmMedium { get; set; }
        public string UtmCampaign { get; set; }
        public string UtmTerm { get; set; }
        public string UtmContent { get; set; }
    }
    public class PushLoanToPartnerDetail : PushLoanToPartnerDTO
    {
        public static Expression<Func<LOS.DAL.EntityFramework.PushLoanToPartner, PushLoanToPartnerDetail>> ProjectionDetail
        {
            get
            {
                return x => new PushLoanToPartnerDetail()
                {
                    Id = x.Id,
                    LoanPartnerId = x.LoanPartnerId,
                    CustomerName = x.CustomerName,
                    CustomerPhone = x.CustomerPhone,
                    ProvinceName = x.ProvinceName,
                    DistrictName = x.DistrictName,
                    PushAction = x.PushAction,
                    PushDate = x.PushDate,
                    Status = x.Status,
                    SynchronizedDate = x.SynchronizedDate,
                    Partner = x.Partner,
                    Comment = x.Comment,
                    Tid = x.Tid,
                    AffCode = x.AffCode,
                    Domain = x.Domain,
                    UtmSource = x.UtmSource,
                    CreateDate = x.CreateDate,
                    ProvinceId = x.ProvinceId,
                    DistrictId = x.DistrictId,
                };
            }
        }
    }
    public class PushLoanCancelToPartnerDetail : PushLoanToPartnerDTO
    {
        public string NationalCard { get; set; }
        public static Expression<Func<LOS.DAL.EntityFramework.LoanBrief, PushLoanCancelToPartnerDetail>> ProjectionDetail
        {
            get
            {
                return x => new PushLoanCancelToPartnerDetail()
                {
                    LoanbriefId = x.LoanBriefId,
                    CustomerName = x.FullName,
                    CustomerPhone = x.Phone,
                    ProvinceId = x.ProvinceId,
                    ProvinceName = x.Province != null ? x.Province.Name : "",
                    DistrictId = x.DistrictId,
                    DistrictName = x.District != null ? x.District.Name : "",
                    Tid = x.Tid,
                    AffCode = x.AffCode,
                    Domain = x.DomainName,
                    UtmSource = x.UtmSource,
                    UtmCampaign = x.UtmCampaign,
                    UtmContent = x.UtmContent,
                    UtmMedium = x.UtmMedium,
                    UtmTerm = x.UtmTerm,
                    NationalCard = x.NationalCard
                };
            }
        }
    }
    public class PushLoanToPartnerCPS
    {
        public int Id { get; set; }
        public string AffCode { get; set; }
        public string AffSid { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? PushDate { get; set; }

        public static Expression<Func<LOS.DAL.EntityFramework.PushLoanToPartner, PushLoanToPartnerCPS>> ProjectionDetail
        {
            get
            {
                return x => new PushLoanToPartnerCPS()
                {
                    Id = x.Id,
                    AffCode = x.AffCode,
                    AffSid = x.AffSid,
                    Status = x.Status,
                    CreateDate = x.CreateDate,
                    PushDate = x.PushDate,
                };
            }
        }
    }
}
