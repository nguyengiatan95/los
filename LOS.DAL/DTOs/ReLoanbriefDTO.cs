﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class ReLoanbriefDTO
    {
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public decimal? LoanAmount { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public int? ProductId { get; set; }
        public string ProductCreditName { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public DateTime? LoanBriefCancelAt { get; set; }
        public string TelesaleName { get; set; }
        public string LoanBriefComment { get; set; }
        public DateTime? FinishAt { get; set; }
        public string UtmSource { get; set; }
    }

    public class ReLoanbriefDetail : ReLoanbriefDTO
    {
        public static Expression<Func<LoanBrief, ReLoanbriefDetail>> ProjectionDetail
        {
            get
            {
                return x => new ReLoanbriefDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    LoanAmount = x.LoanAmount,
                    CreatedTime = x.CreatedTime,
                    ProductId = x.ProductId,
                    ProductCreditName = x.Product != null ? x.Product.Name : "",
                    ProvinceName = x.Province != null ? x.Province.Name : "",
                    DistrictName = x.District != null ? x.District.Name : "",
                    LoanBriefCancelAt = x.LoanBriefCancelAt,
                    TelesaleName = x.BoundTelesale != null ? x.BoundTelesale.FullName : "",
                    LoanBriefComment = x.LoanBriefComment,
                    FinishAt = x.FinishAt,
                    UtmSource = x.UtmSource,
                };
            }
        }
    }
}
