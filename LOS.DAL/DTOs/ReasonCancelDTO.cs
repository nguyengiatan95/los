﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class ReasonCancelDTO
    {
        public int? Id { get; set; }
        public string Reason { get; set; }
        public int? Type { get; set; }
        public int? Sort { get; set; }
        public int? IsEnable { get; set; }
        public int? ParentId { get; set; }
        public string ReasonCode { get; set; }
        public int? DayInBlacklist { get; set; }
    }

    public class ReasonCancelDetail: ReasonCancelDTO
    {
        public static Expression<Func<ReasonCancel, ReasonCancelDetail>> ProjectionDetail
        {
            get
            {
                return x => new ReasonCancelDetail
                {
                    Id = x.Id,
                    Reason = x.Reason, 
                    Type = x.Type,
                    Sort = x.Sort,
                    IsEnable = x.IsEnable,
                    ParentId = x.ParentId,
                    ReasonCode = x.ReasonCode,
                    DayInBlacklist = x.DayInBlacklist,
                };
            }
        }

    }

    public class ReasonCancelDetailApi 
    {
        public int? Id { get; set; }
        public string Reason { get; set; }
        public int? Sort { get; set; }
        public static Expression<Func<ReasonCancel, ReasonCancelDetailApi>> ProjectionDetail
        {
            get
            {
                return x => new ReasonCancelDetailApi
                {
                    Id = x.Id,
                    Reason = x.Reason,
                    Sort = x.Sort,
                };
            }
        }
    }
}
