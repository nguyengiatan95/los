﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class RelativeFamilyDTO
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
    }

    public class RelativeFamilyDetail : RelativeFamilyDTO
    {
        public static Expression<Func<RelativeFamily, RelativeFamilyDetail>> ProjectionDetail
        {
            get
            {
                return x => new RelativeFamilyDetail()
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsEnable = x.IsEnable
                };
            }
        }
    }
}
