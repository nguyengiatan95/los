﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class ReportUtmGroupDTO
    {
        public string UtmSource { get; set; }
        public string GroupName { get; set; }
        public int Status { get; set; }
        public int CountLoanBrief { get; set; }
        public int CountStatusDisbured { get; set; }
        public double RateDisbured { get; set; }
        public double Percentage { get; set; }
    }

    public static class ReportUtmExtensions
    {
        public static ReportUtmGroupDTO Mapping(this LoanBrief dto)
        {
            var report = new ReportUtmGroupDTO();
            report.UtmSource = dto.UtmSource;
            report.Status = dto.Status.Value;
            return report;
        }
    }
}
