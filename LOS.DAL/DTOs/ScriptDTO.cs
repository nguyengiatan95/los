﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
	public class ScriptDTO
	{
		public int ScriptId { get; set; }
		public string Name { get; set; }
		public int? Type { get; set; }
		public int? Status { get; set; }
		public DateTimeOffset? CreatedTime { get; set; }
		public int? CreatorId { get; set; }
	}

	public class ScriptDetailDTO
	{
		public int ScriptDetailId { get; set; }
		public int? ScriptId { get; set; }
		public string Question { get; set; }
		public int? PropertyId { get; set; }
		public bool? ValidRequired { get; set; }
		public DateTime? CreatedTime { get; set; }
		public int? Priority { get; set; }
		public string Action { get; set; }

		public PropertyDetail Property { get; set; }
	}

	public class ScriptDetail : ScriptDTO
	{
		public List<ScriptDetailDTO> Details { get; set; }

		public static Expression<Func<Script, ScriptDetail>> ProjectionDetail
		{
			get
			{
				return x => new ScriptDetail()
				{
					CreatedTime = x.CreatedTime,				
					Name = x.Name,
					ScriptId = x.ScriptId,
					Status = x.Status,
					Type = x.Type,
					Details = x.ScriptDetail.Select(x1 => new ScriptDetailDTO()
					{						
						Priority = x1.Priority,
						Question = x1.Question,
						ScriptDetailId = x1.ScriptDetailId,
						ScriptId = x1.ScriptId,
						ValidRequired = x1.ValidRequired,
						Property = x1.Property != null ? new PropertyDetail()
						{
							Name = x1.Property.Name,
							Object = x1.Property.Object,
							Priority = x1.Property.Priority,
							FieldName = x1.Property.FieldName,
							PropertyId = x1.Property.PropertyId,
							Status = x1.Property.Status,
							Type = x1.Property.Type,
							DefaultValue = x1.Property.DefaultValue
						} : null
					}).ToList()
				};
			}
		}
	}
}
