﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
   public class ShopDTO 
    {
        public int ShopId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int? Status { get; set; }
        public int? CityId { get; set; }
    }
    public class ShopDetail : ShopDTO
    {
        public static Expression<Func<Shop, ShopDetail>> ProjectionDetail
        {
            get
            {
                return x => new ShopDetail()
                {
                    ShopId = x.ShopId,
                    Name = x.Name,
                    Address = x.Address,
                    Phone = x.Phone,
                    Status = x.Status,
                    CityId = x.CityId
                };
            }
        }
    }
}
