﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class StatusTelesalesDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
        public int? ParentId { get; set; }
    }
    public class StatusTelesalesDetail : StatusTelesalesDTO
    {
        public static Expression<Func<LoanStatusDetail, StatusTelesalesDetail>> ProjectionDetail
        {
            get
            {
                return x => new StatusTelesalesDetail
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsEnable = x.IsEnable,
                    ParentId = x.ParentId
                };
            }
        }
    }
}
