﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class TeamTelesalesDTO
    {
        public int TeamTelesalesId { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
        public bool? IsTelesaleTeam { get; set; }
    }

    public class TeamTelesaleDetail : TeamTelesalesDTO
    {
        public static Expression<Func<TeamTelesales, TeamTelesaleDetail>> ProjectionDetail
        {
            get
            {
                return x => new TeamTelesaleDetail()
                {
                    TeamTelesalesId = x.TeamTelesalesId,
                    Name = x.Name,
                    IsEnable = x.IsEnable,
                    IsTelesaleTeam = x.IsTelesaleTeam,
                };
            }
        }
    }
}
