﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class UserTelesalesDTO
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
    }

    public class UserTelesalesDetail : UserTelesalesDTO
    {
        public static Expression<Func<User, UserTelesalesDetail>> ProjectionDetail
        {
            get
            {
                return x => new UserTelesalesDetail()
                {
                    UserId = x.UserId,
                    FullName = x.FullName,
                    Username = x.Username,
                };
            }
        }
    }
}
