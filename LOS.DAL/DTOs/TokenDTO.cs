﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class TokenDTO
    {
        public int Id { get; set; }
        public string AccessToken1 { get; set; }
        public string AppId { get; set; }
        public DateTime? DateExpiration { get; set; }
        public DateTime? CreatedAt { get; set; }
    }

    public class TokenDetail : TokenDTO
    {
        public static Expression<Func<AccessToken, TokenDetail>> ProjectionDetail
        {
            get
            {
                return x => new TokenDetail()
                {
                    Id = x.Id,
                    AccessToken1 = x.AccessToken1,
                    AppId = x.AppId,
                    DateExpiration = x.DateExpiration
                };
            }
        }
    }
}
