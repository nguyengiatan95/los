﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
   public class TokenSmartDailerDTO
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime? DateExpiration { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? UserId { get; set; }
    }

    public class TokenSmartDailerDetail : TokenSmartDailerDTO
    {
        public static Expression<Func<TokenSmartDailer, TokenSmartDailerDetail>> ProjectionDetail
        {
            get
            {
                return x => new TokenSmartDailerDetail()
                {
                    Id = x.Id,
                    Token = x.Token,
                    DateExpiration = x.DateExpiration,
                    UserId = x.UserId,
                    CreatedAt = x.CreatedAt
                };
            }
        }
    }
}
