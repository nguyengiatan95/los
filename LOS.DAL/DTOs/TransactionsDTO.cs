﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class TransactionsDTO
    {
        public long Id { get; set; }
        public int? ReferLenderId { get; set; }
        public int? LoanBriefId { get; set; }
        public long? Money { get; set; }
        public int? LenderId { get; set; }
        public long? MoneyRefund { get; set; }
        public int? PaymentType { get; set; }
        public string TranReference { get; set; }
        public int? TranNumber { get; set; }
        public int? ResponseCode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Message { get; set; }
        public long? FeeMoney { get; set; }
        public long? MoneyBefore { get; set; }
        public long? MoneyAfter { get; set; }
        public int? PlatformType { get; set; }
        public long? ReferTransactionId { get; set; }
        public int? ProcessByUserId { get; set; }
        public string ProcessByName { get; set; }
        public int? TimaCommissionFeePercent { get; set; }
        public long? TimaCommissionFeeMoney { get; set; }
        public string ProcessByUserNote { get; set; }
        public int? ReferLenderCommissionFeePercent { get; set; }
        public long? ReferLenderCommissionFeeMoney { get; set; }
        public long? MoneySub { get; set; }
        public long? BonusMoneySub { get; set; }
        public int? BankId { get; set; }
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public int? Status { get; set; }
        public string LenderPhone { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? PromotionId { get; set; }
        public long? ReferTransactionPromotionId { get; set; }
        public int? SellByUserId { get; set; }


    }
    public static class TransactionsDTOExtensions
    {
        public static Transactions Mapping(this TransactionsDTO dto)
        {
            var Transactions = new Transactions();
            Transactions.Id = dto.Id;
            Transactions.Money = dto.Money;
            Transactions.CreatedDate = dto.CreatedDate;
            return Transactions;
        }
    }

    public static class TransactionsExtensions
    {
        public static TransactionsDTO Mapping(this Transactions dto)
        {
            var Transactions = new TransactionsDTO();
            Transactions.Id = dto.Id;
            Transactions.Money = dto.Money;
            Transactions.CreatedDate = dto.CreatedDate;

            return Transactions;
        }
    }

    public class LoanBriefSuccessFullySoldDetail : TransactionsDTO
    {
        public LoanProductDTO LoanProduct { get; set; }
        public LoanRateTypeDTO LoanRateType { get; set; }
        public ProvinceDTO Province { get; set; }
        public DistrictDTO District { get; set; }
        public WardDTO Ward { get; set; }
        public UserMMODTO UserMMO { get; set; }
        public TransactionsDTO Transaction { get; set; }

        public LoanBriefDTO LoanBriefDTO { get; set; }


        public static Expression<Func<Transactions, LoanBriefSuccessFullySoldDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefSuccessFullySoldDetail()
                {
                    Id = x.Id,
                    LoanBriefId = x.LoanBriefId,
                    Money = x.Money,
                    CreatedDate = x.CreatedDate
                    //,
                    //LoanBriefDTO = x.LoanBrief != null ? new LoanBriefDTO()
                    //{
                    //    //AffCode = x.LoanBrief.AffCode,
                    //    Phone = x.LoanBrief.Phone,
                    //    AffStatus = x.LoanBrief.AffStatus,
                    //    FullName = x.LoanBrief.FullName
                    //} : null
                };
            }
        }
    }
}
