﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class UserBasicDTO
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public int? CityId { get; set; }
        public int? GroupId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public int? CreatedBy { get; set; }
        public int? Ipphone { get; set; }
        public int? CountLoan { get; set; }
        public int? UserIdSSO { get; set; }
        public string CiscoUsername { get; set; }
        public string CiscoPassword { get; set; }
        public string CiscoExtension { get; set; }
        public int TypeCallService { get; set; }
        public bool TelesaleRecievedLoan { get; set; }
        public int TypeRecievedProduct { get; set; }
        public int ValueTelesalesShift { get; set; }
        public int? TeamTelesalesId { get; set; }
    }

    public class UserBasicDetail : UserBasicDTO
    {
       
        public static Expression<Func<User, UserBasicDetail>> Projection
        {
            get
            {
                return x => new UserBasicDetail()
                {
                    UserId = x.UserId,
                    UserIdSSO = x.UserIdSso ?? 0,
                    Username = x.Username,
                    FullName = x.FullName,
                    Email = x.Email,
                    Phone = x.Phone,
                    GroupId = x.GroupId,
                    Status = x.Status,
                    Ipphone = x.Ipphone,
                    CiscoUsername = x.CiscoUsername,
                    CiscoPassword = x.CiscoPassword,
                    CiscoExtension = x.CiscoExtension,
                    TypeCallService = x.TypeCallService.GetValueOrDefault(),
                    TelesaleRecievedLoan = x.TelesaleRecievedLoan.Value,
                    TypeRecievedProduct = x.TypeRecievedProduct.Value,
                    ValueTelesalesShift = x.ValueTelesalesShift.Value,
                    TeamTelesalesId = x.TeamTelesalesId.Value,
                    CityId = x.CityId ?? 0                    
                };
            }
        }
    }
}
