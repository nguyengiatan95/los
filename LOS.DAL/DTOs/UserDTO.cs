﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.DAL.DTOs
{
    public class UserDTO
    {
        public UserDTO()
        {
            ListUserModule = new
                List<UserModule>();
        }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTimeOffset? UpdatedTime { get; set; }
        public int? CompanyId { get; set; }
        public int? DepartmentId { get; set; }
        public int? PositionId { get; set; }
        public int? CityId { get; set; }
        public int? GroupId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public int? CreatedBy { get; set; }
        public int? Ipphone { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CountLoan { get; set; }
        public List<LOS.DAL.EntityFramework.Group> ListGroup { get; set; }
        public List<UserModule> ListUserModule { get; set; }

        public List<int> HubIds { get; set; }

        public string UrlEdit { get; set; }
        public int? UserIdSSO { get; set; }
        public string CiscoUsername { get; set; }
        public string CiscoPassword { get; set; }
        public string CiscoExtension { get; set; }
        public int TypeCallService { get; set; }
        public bool TelesaleRecievedLoan { get; set; }
        public int TypeRecievedProduct { get; set; }
        public int ValueTelesalesShift { get; set; }
        public int? TeamTelesalesId { get; set; }

    }

    public static class UserDTOExtions
    {
        public static User Mapping(this UserDTO dto)
        {
            var user = new User();
            user.CityId = dto.CityId;
            user.CompanyId = dto.CompanyId;
            user.CreatedTime = dto.CreatedTime;
            user.DepartmentId = dto.DepartmentId;
            user.DistrictId = dto.DistrictId;
            user.Email = dto.Email;
            user.FullName = dto.FullName;
            user.GroupId = dto.GroupId;
            user.Password = dto.Password;
            user.Phone = dto.Phone;
            user.PositionId = dto.PositionId;
            user.Status = dto.Status;
            user.UpdatedTime = dto.UpdatedTime;
            user.UserId = dto.UserId;
            user.Username = dto.Username;
            user.WardId = dto.WardId;
            user.CreatedBy = dto.CreatedBy;
            user.Ipphone = dto.Ipphone;
            user.CiscoExtension = dto.CiscoExtension;
            user.CiscoPassword = dto.CiscoPassword;
            user.CiscoUsername = dto.CiscoUsername;
            user.TypeCallService = dto.TypeCallService;
            user.TelesaleRecievedLoan = dto.TelesaleRecievedLoan;
            user.TypeRecievedProduct = dto.TypeRecievedProduct;
            user.ValueTelesalesShift = dto.ValueTelesalesShift;
            user.TeamTelesalesId = dto.TeamTelesalesId;
            user.UserIdSso = dto.UserIdSSO;
            return user;
        }
    }

    public class UserDetail : UserDTO
    {
        public CompanyDTO Company { get; set; }
        public DepartmentDTO Department { get; set; }
        public PositionDTO Position { get; set; }
        public string Token { get; set; }
        public string TokenSSO { get; set; }
        public GroupDetail Group { get; set; }
        public List<ModuleDetail> Modules { get; set; }
        public List<ShopDetail> ListShop { get; set; }
        public int ShopGlobal { get; set; }
        public string ShopGlobalName { get; set; }
        public ProvinceDTO City { get; set; }
        public List<UserActionPermission> ListPermissionAction { get; set; }
        public List<UserCondition> ListCondition { get; set; }
        public List<UserTeamTelesales> ListUserTeamTelesales { get; set; }
        public List<TelesalesShift> ListTelesalesShift { get; set; }

        // Work flow
        public bool IsProcessing { get; set; }
        public string TokenSmartDailer { get; set; }

        public static Expression<Func<User, UserDetail>> ProjectionDetail
        {
            get
            {
                return x => new UserDetail()
                {
                    UserId = x.UserId,
                    UserIdSSO = x.UserIdSso ?? 0,
                    Username = x.Username,
                    FullName = x.FullName,
                    Email = x.Email,
                    Phone = x.Phone,
                    GroupId = x.GroupId,
                    Status = x.Status,
                    Ipphone = x.Ipphone,
                    IsDeleted = x.IsDeleted,
                    CiscoUsername = x.CiscoUsername,
                    CiscoPassword = x.CiscoPassword,
                    CiscoExtension = x.CiscoExtension,
                    TypeCallService = x.TypeCallService.GetValueOrDefault(),
                    TelesaleRecievedLoan = x.TelesaleRecievedLoan.Value,
                    TypeRecievedProduct = x.TypeRecievedProduct.Value,
                    ValueTelesalesShift = x.ValueTelesalesShift.Value,
                    TeamTelesalesId = x.TeamTelesalesId.Value,
                    CityId = x.CityId ?? 0,
                    Company = x.Company != null ? new CompanyDTO()
                    {
                        CompanyId = x.Company.CompanyId,
                        Name = x.Company.Name
                    } : null,
                    City = x.CityId != null ? new ProvinceDTO()
                    {
                        ProvinceId = x.City.ProvinceId,
                        Name = x.City.Name
                    } : null,
                    Group = x.Group != null ? new GroupDetail()
                    {
                        GroupId = x.Group.GroupId,
                        GroupName = x.Group.GroupName,
                        DefaultPath = x.Group.DefaultPath

                    } : null,
                    ListShop = x.UserShop != null ? x.UserShop.Select(x1 => new ShopDetail()
                    {
                        ShopId = x1.Shop.ShopId,
                        Name = x1.Shop.Name
                    }).ToList() : null,
                    ListUserTeamTelesales = x.UserTeamTelesales != null ? x.UserTeamTelesales.Select(x1 => new UserTeamTelesales(){ 
                        TeamTelesalesId = x1.TeamTelesalesId,
                        UserId = x1.UserId
                    }).ToList() : null,
                    Modules = x.UserModule.Select(x1 => new ModuleDetail()
                    {
                        Code = x1.Module.Code,
                        Description = x1.Module.Description,
                        ModuleId = x1.Module.ModuleId,
                        Name = x1.Module.Name,
                        Path = x1.Module.Path,
                        Status = x1.Module.Status,
                        ParentId = x1.Module.ParentId,
                        Priority = x1.Module.Priority,
                        IsMenu = x1.Module.IsMenu.GetValueOrDefault(),
                        Controller = x1.Module.Controller,
                        Action = x1.Module.Action,
                        Icon = x1.Module.Icon
                        //Permissions = x1.GroupModulePermission.Select(x2 => new PermissionDetail() {
                        //	DefaultValue = x2.Permission.DefaultValue,
                        //	Description = x2.Permission.Description,
                        //	Name = x2.Permission.Name,
                        //	PermissionId = x2.Permission.PermissionId,
                        //	Priority = x2.Permission.Priority,
                        //	Status = x2.Permission.Status
                        //}).ToList()
                    }).ToList()
                };
            }
        }

        public static Expression<Func<User, UserDetail>> ProjectionPermissionDetail
        {
            get
            {
                return x => new UserDetail()
                {
                    UserId = x.UserId,
                    Username = x.Username,
                    FullName = x.FullName,
                    Email = x.Email,
                    Phone = x.Phone,
                    GroupId = x.GroupId,
                    Status = x.Status,
                    Ipphone = x.Ipphone,
                    Group = x.Group != null ? new GroupDetail()
                    {
                        GroupId = x.Group.GroupId,
                        GroupName = x.Group.GroupName,
                        DefaultPath = x.Group.DefaultPath

                    } : null,
                    ListPermissionAction = x.UserActionPermission != null ? x.UserActionPermission.Select(x1 => x1).ToList() : null,
                    ListCondition = x.UserCondition != null ? x.UserCondition.Select(x1 => x1).ToList() : null
                };
            }
        }
    }

    public class UserPassword
    {
        public string Password { get; set; }
        public static Expression<Func<User, UserPassword>> ProjectionDetail
        {
            get
            {
                return x => new UserPassword()
                {
                    Password = x.Password,
                };
            }
        }
    }

    public class GetUserERP
    {
        public int userId { get; set; }
        public string full_name { get; set; }
        public string user_name { get; set; }
        public static Expression<Func<User, GetUserERP>> ProjectionDetail
        {
            get
            {
                return x => new GetUserERP()
                {
                    userId = x.UserId,
                    full_name = x.FullName,
                    user_name = x.Username
                };
            }
        }
    }

    public class UserGroup
    {
        public int UserId { get; set; }
        public int? GroupId { get; set; }
        public static Expression<Func<User, UserGroup>> ProjectionDetail
        {
            get
            {
                return x => new UserGroup()
                {
                    UserId = x.UserId,
                    GroupId = x.GroupId,
                };
            }
        }
    }
}
