﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class UserMMODTO
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? IsActive { get; set; }
        public string AffCode { get; set; }
        public string Address { get; set; }
        public string TaxCode { get; set; }
        public long? TotalAmountEarning { get; set; }
        public long? AvailableBalance { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? EndMatchingDate { get; set; }
        public DateTime? EndPaymentDate { get; set; }
        public double? PercentAmount { get; set; }
        public double? PercentRefer { get; set; }
        public string Cmt { get; set; }
        public string FacedeCmt { get; set; }
        public string BacksideCmt { get; set; }
        public string Giud { get; set; }
    }

    public static class UserMMODTOExtions
    {
        public static UserMmo Mapping(this UserMMODTO dto)
        {
            var user = new UserMmo();
            user.UserId = dto.UserId;
            user.UserName = dto.UserName;
            user.Password = dto.Password;
            user.Email = dto.Email;
            user.FullName = dto.FullName;
            user.Phone = dto.Phone;
            user.CreateDate = dto.CreateDate;
            user.IsActive = dto.IsActive;
            user.AffCode = dto.AffCode;
            user.Address = dto.Address;
            user.TaxCode = dto.TaxCode;
            user.TotalAmountEarning = dto.TotalAmountEarning;
            user.AvailableBalance = dto.AvailableBalance;
            user.UpdateDate = dto.UpdateDate;
            user.EndMatchingDate = dto.EndMatchingDate;
            user.EndPaymentDate = dto.EndPaymentDate;
            user.PercentAmount = dto.PercentAmount;
            user.PercentRefer = dto.PercentRefer;
            user.Cmt = dto.Cmt;
            user.FacedeCmt = dto.FacedeCmt;
            user.BacksideCmt = dto.BacksideCmt;
            user.Giud = dto.Giud;
            return user;
        }
    }

    public class UserMMODetail : UserMMODTO
    {
        public string Token { get; set; }
        public static Expression<Func<UserMmo, UserMMODetail>> ProjectionDetail
        {
            get
            {
                return x => new UserMMODetail()
                {
                    UserId = x.UserId,
                    UserName = x.UserName,
                    FullName = x.FullName,
                    Password = x.Password,
                    Email = x.Email,
                    Phone = x.Phone,
                    CreateDate = x.CreateDate,
                    IsActive = x.IsActive,
                    AffCode = x.AffCode,
                    Address = x.Address,
                    TaxCode = x.TaxCode,
                    TotalAmountEarning = x.TotalAmountEarning,
                    AvailableBalance = x.AvailableBalance,
                    UpdateDate = x.UpdateDate,
                    EndMatchingDate = x.EndMatchingDate,
                    EndPaymentDate = x.EndPaymentDate,
                    PercentAmount = x.PercentAmount,
                    PercentRefer = x.PercentRefer,
                    Cmt = x.Cmt,
                    FacedeCmt = x.FacedeCmt,
                    BacksideCmt = x.BacksideCmt,
                    Giud = x.Giud,
                };
            }
        }
    }
}
