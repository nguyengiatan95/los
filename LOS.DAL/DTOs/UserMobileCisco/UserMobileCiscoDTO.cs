﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq.Expressions;
using LOS.DAL.EntityFramework;

namespace LOS.DAL.DTOs
{
    public class UserMobileCiscoDTO
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string MbciscoUsername { get; set; }
        public string MbciscoPassword { get; set; }
        public string MbCiscoExtension { get; set; }
    }

    public class UserMobileCiscoDetail : UserMobileCiscoDTO
    {
        public static Expression<Func<UserMobileCisco, UserMobileCiscoDetail>> ProjectionDetail
        {
            get
            {
                return x => new UserMobileCiscoDetail()
                {
                    UserId = x.UserId,
                    Username = x.Username,
                    FullName = x.FullName,
                    MbciscoUsername = x.MbciscoUsername,
                    MbciscoPassword = x.MbciscoPassword,
                    MbCiscoExtension = x.MbCiscoExtension,
                };
            }
        }
    }
}
