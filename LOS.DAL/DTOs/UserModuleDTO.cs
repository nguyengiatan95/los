﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.DTOs
{
    public partial class UserModuleDTO
    {
        public int UserModuleId { get; set; }

        public int UserId { get; set; }

        public int ModuleId { get; set; }
    }
}
