﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.DAL.DTOs
{
    public class UserReferenceDTO
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? UserReferenceId { get; set; }
        public string AffReferenceCode { get; set; }
        public int? Level { get; set; }
        public string UserCode { get; set; }
    }
    public static class UserReferenceDTOExtions
    {
        public static UserReference Mapping(this UserReferenceDTO dto)
        {
            var user = new UserReference();
            user.Id = dto.Id;
            user.UserId = dto.UserId;
            user.UserReferenceId = dto.UserReferenceId;
            user.AffReferenceCode = dto.AffReferenceCode;
            user.Level = dto.Level;
            user.UserCode = dto.UserCode;
            return user;
        }
    }

    public class UserReferenceDetail : UserReferenceDTO
    {
        public List<UserMMODTO> LstUserMMODTO { get; set; }
        public UserMMODTO UserMMO { get; set; }
        public static Expression<Func<UserReference, UserReferenceDetail>> ProjectionDetail
        {
            get
            {
                return x => new UserReferenceDetail()
                {
                    Id = x.Id,
                    UserId = x.UserId,
                    UserReferenceId = x.UserReferenceId,
                    AffReferenceCode = x.AffReferenceCode,
                    Level = x.Level,
                    UserCode = x.UserCode,
                    UserMMO = x.UserReferenceNavigation != null ? new UserMMODTO()
                    {
                        UserId = x.User.UserId,
                        FullName = x.User.FullName,
                        Phone = x.User.Phone,
                        Address = x.User.Address,
                        CreateDate = x.User.CreateDate,
                        Email = x.User.Email,
                        IsActive = x.User.IsActive
                    } : null
                };
            }
        }
    }

}
