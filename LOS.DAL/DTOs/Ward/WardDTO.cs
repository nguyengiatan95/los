﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace LOS.DAL.DTOs
{
    public class WardDTO
    {
        public int WardId { get; set; }
        public int? DistrictId { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? Priority { get; set; }
    }
    public class WardDetail : WardDTO
    {
        public static Expression<Func<Ward, WardDetail>> ProjectionDetail
        {
            get
            {
                return x => new WardDetail()
                {
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    Name = x.Name,
                    Priority = x.Priority,
                    WardId = x.WardId,
                    Type = x.Type,
                    DistrictId = x.DistrictId
                };
            }
        }
    }
}
