﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Dapper
{
    public sealed class DapperHelper
    {
        private readonly string _connString;
        public DapperHelper(string connString)
        {
            _connString = connString;
        }

        public T QueryFirstOrDefault<T>(string sql, object parameters = null)
        {
            using (var connection = new DapperConnection(_connString))
            {
                IDbConnection db = connection.GetConnection;
                return db.QueryFirstOrDefault<T>(sql, parameters);
            }
        }

        public List<T> Query<T>(string sql, object parameters = null)
        {
            using (var connection = new DapperConnection(_connString))
            {
                IDbConnection db = connection.GetConnection;
                return db.Query<T>(sql, parameters).ToList();
            }
        }     

        public int Execute(string sql, object parameters = null)
        {
            using (var connection = new DapperConnection(_connString))
            {
                IDbConnection db = connection.GetConnection;
                return db.Execute(sql, parameters);
            }
        }

        public bool Execute(List<string> sqlsList, object parameters = null)
        {
            var result = true;
            using (var connection = new DapperConnection(_connString))
            {
                IDbConnection db = connection.GetConnection;
                foreach (var o in sqlsList)
                {
                    try
                    {
                        db.Execute(o, parameters);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }

                }
            }
            return result;
        }

        public List<T> QueryStoredProcedure<T>(string storedName, object parameters = null)
        {
            using (var connection = new DapperConnection(_connString))
            {
                IDbConnection db = connection.GetConnection;
                return db.Query<T>(storedName, parameters, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        #region async task
        public async Task<T> QueryFirstOrDefaultAsync<T>(string sql, object parameters = null)
        {
            using (var connection = new DapperConnection(_connString))
            {
                IDbConnection db = connection.GetConnection;
                return await db.QueryFirstOrDefaultAsync<T>(sql, parameters);
            }
        }

        public async Task<List<T>> QueryAsync<T>(string sql, object parameters = null)
        {
            using (var connection = new DapperConnection(_connString))
            {
                IDbConnection db = connection.GetConnection;
                var result = await db.QueryAsync<T>(sql, parameters);
                return result.ToList();
            }
        }

        public async Task<int> ExecuteAsync(string sql, object parameters = null)
        {
            using (var connection = new DapperConnection(_connString))
            {
                IDbConnection db = connection.GetConnection;
                return await db.ExecuteAsync(sql, parameters);
            }
        }

        public async Task<bool> ExecuteAsync(List<string> sqlsList, object parameters = null)
        {
            var result = true;
            using (var connection = new DapperConnection(_connString))
            {
                IDbConnection db = connection.GetConnection;
                foreach (var o in sqlsList)
                {
                    try
                    {
                        await db.ExecuteAsync(o, parameters);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }

                }
            }
            return result;
        }

        public async Task<List<T>> QueryStoredProcedureAsync<T>(string storedName, object parameters = null)
        {
            using (var connection = new DapperConnection(_connString))
            {
                IDbConnection db = connection.GetConnection;
                var result = await db.QueryAsync<T>(storedName, parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        #endregion
      

        // Other Helpers...
        private IDbConnection CreateConnection()
        {
            var connection = new SqlConnection(_connString);

            // Properly initialize your connection here.
            return connection;
        }
     
    }

    public sealed class  DapperConnection : IDisposable
    {
        private IDbConnection _connection;
        public DapperConnection(string connString)
        {
            _connection = new SqlConnection(connString);
        }

        public IDbConnection GetConnection
        {
            get
            {
                return _connection;
            }
        }
        public void Dispose()
        {
            if (_connection != null)
                _connection.Dispose();
        }
    }
}
