﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Action
    {
        public Action()
        {
            ActionStatus = new HashSet<ActionStatus>();
        }

        public int ActionId { get; set; }
        public string ActionCode { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ActionStatus> ActionStatus { get; set; }
    }
}
