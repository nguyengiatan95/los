﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ActionStatus
    {
        public ActionStatus()
        {
            LoanAction = new HashSet<LoanAction>();
        }

        public int ActionStatusId { get; set; }
        public int? ActionId { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }

        public virtual Action ActionNavigation { get; set; }
        public virtual ICollection<LoanAction> LoanAction { get; set; }
    }
}
