﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class AlertAutocall
    {
        public DateTime? Time { get; set; }
        public int? Value { get; set; }
        public string Metric { get; set; }
    }
}
