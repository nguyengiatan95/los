﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ApiMonitor
    {
        public int ApiMonitorId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Desciption { get; set; }
        public string Request { get; set; }
        public string Responce { get; set; }
        public string AppKey { get; set; }
        public string Action { get; set; }
        public string Controler { get; set; }
        public double? TotalMinute { get; set; }
    }
}
