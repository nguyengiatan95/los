﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Aplication
    {
        public int AplicationId { get; set; }
        public string AplicationName { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
    }
}
