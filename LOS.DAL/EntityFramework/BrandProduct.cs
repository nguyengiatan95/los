﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class BrandProduct
    {
        public BrandProduct()
        {
            ContactCustomer = new HashSet<ContactCustomer>();
            LoanBriefProperty = new HashSet<LoanBriefProperty>();
            Product = new HashSet<Product>();
            ProductPercentReduction = new HashSet<ProductPercentReduction>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }

        public virtual ICollection<ContactCustomer> ContactCustomer { get; set; }
        public virtual ICollection<LoanBriefProperty> LoanBriefProperty { get; set; }
        public virtual ICollection<Product> Product { get; set; }
        public virtual ICollection<ProductPercentReduction> ProductPercentReduction { get; set; }
    }
}
