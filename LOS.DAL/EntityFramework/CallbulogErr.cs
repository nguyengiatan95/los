﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class CallbulogErr
    {
        public DateTime? StartTime { get; set; }
        public string Phone { get; set; }
        public bool? Variable3 { get; set; }
        public bool? Variable4 { get; set; }
        public bool? Variable5 { get; set; }
    }
}
