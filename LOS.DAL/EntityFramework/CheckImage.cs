﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class CheckImage
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public int? DocumentTypeId { get; set; }
        public bool? ResultCheck { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdateAt { get; set; }
        public int? Status { get; set; }
    }
}
