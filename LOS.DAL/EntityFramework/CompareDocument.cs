﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class CompareDocument
    {
        public int Id { get; set; }
        public int? Tab { get; set; }
        public int? LoanBriefId { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public int? GroupId { get; set; }
        public int? Step { get; set; }
        public DateTime? CreatedAt { get; set; }
        public bool? IsFinish { get; set; }
        public int? Status { get; set; }
    }
}
