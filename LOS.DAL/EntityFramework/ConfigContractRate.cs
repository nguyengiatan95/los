﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ConfigContractRate
    {
        public int Id { get; set; }
        public double? CustomerRate { get; set; }
        public double? LenderRate { get; set; }
        public DateTime? AppliedAt { get; set; }
        public bool? IsEnable { get; set; }
    }
}
