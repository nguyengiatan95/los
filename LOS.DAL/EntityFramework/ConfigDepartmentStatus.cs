﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ConfigDepartmentStatus
    {
        public int Id { get; set; }
        public int DepartmentId { get; set; }
        public int? LoanStatus { get; set; }
        public bool? IsEnable { get; set; }
        public string LoanStatusName { get; set; }
    }
}
