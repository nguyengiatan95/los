﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ConfigDocument
    {
        public int Id { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? ProductId { get; set; }
        public int? MinImage { get; set; }
        public bool? Required { get; set; }
        public int? TypeRequired { get; set; }
        public string AllowFromUpload { get; set; }
        public string GroupJobId { get; set; }
        public int? TypeOwnerShip { get; set; }
        public int? ParentId { get; set; }
        public bool? IsEnable { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? Sort { get; set; }
    }
}
