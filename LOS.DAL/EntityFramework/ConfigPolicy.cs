﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ConfigPolicy
    {
        public int Id { get; set; }
        public int? TypeConfigValue { get; set; }
        public string ConfigValue { get; set; }
        public int? MaxRequestDay { get; set; }
        public int? MaxRequestTimeAllow { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedTime { get; set; }
        public string UrlEnpoint { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int? StartTimeAllow { get; set; }
        public int? EndTimeAllow { get; set; }
    }
}
