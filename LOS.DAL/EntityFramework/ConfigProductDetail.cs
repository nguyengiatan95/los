﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ConfigProductDetail
    {
        public int ConfigProductDetailId { get; set; }
        public int? InfomationProductDetailId { get; set; }
        public int? ProductId { get; set; }
        public int? TypeOwnership { get; set; }
        public string Description { get; set; }
        public string Appraiser { get; set; }
        public string JobDocument { get; set; }
        public long? MaxMoney { get; set; }
        public int? TypeLoanBrief { get; set; }
        public bool? IsEnable { get; set; }
        public DateTime? CreateAt { get; set; }
        public DateTime? UpdateAt { get; set; }

        public virtual InfomationProductDetail InfomationProductDetail { get; set; }
    }
}
