﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ContactCustomer
    {
        public int ContactId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public int? JobId { get; set; }
        public decimal? Income { get; set; }
        public bool? HaveCarsOrNot { get; set; }
        public int? BrandProductId { get; set; }
        public int? ProductId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? LoanBriefId { get; set; }
        public int? Status { get; set; }

        public virtual BrandProduct BrandProduct { get; set; }
        public virtual District District { get; set; }
        public virtual Job Job { get; set; }
        public virtual LoanBrief LoanBrief { get; set; }
        public virtual Product Product { get; set; }
        public virtual Province Province { get; set; }
        public virtual Ward Ward { get; set; }
    }
}
