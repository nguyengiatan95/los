﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class CoordinatorCheckList
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? GroupId { get; set; }
        public int? LoanBriefId { get; set; }
        public string ContentCheckList { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
