﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Customer
    {
        public Customer()
        {
            LoanBrief = new HashSet<LoanBrief>();
        }

        public int CustomerId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public DateTime? Dob { get; set; }
        public int? Gender { get; set; }
        public string NationalCard { get; set; }
        public DateTime? NationalCardDate { get; set; }
        public string NationCardPlace { get; set; }
        public string Address { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string FacebookAddress { get; set; }
        public int? JobId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public int? MecashId { get; set; }
        public int? IsMerried { get; set; }
        public int? NumberBaby { get; set; }
        public string InsurenceNumber { get; set; }
        public string InsurenceNote { get; set; }
        public decimal? Salary { get; set; }
        public int? CompanyProvinceId { get; set; }
        public int? CompanyDistrictId { get; set; }
        public int? CompanyWardId { get; set; }
        public string AddressLatLong { get; set; }
        public string CompanyAddressLatLong { get; set; }
        public string Passport { get; set; }
        public string VaAccountNumber { get; set; }
        public string VaAccountName { get; set; }
        public string VaBankCode { get; set; }
        public string VaBankName { get; set; }
        public string GpayAccountName { get; set; }
        public string GpayAccountNumber { get; set; }
        public string GpayBankName { get; set; }
        public string EsignAgreementId { get; set; }
        public string TikiId { get; set; }

        public virtual ICollection<LoanBrief> LoanBrief { get; set; }
    }
}
