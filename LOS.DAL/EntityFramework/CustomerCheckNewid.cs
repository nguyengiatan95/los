﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class CustomerCheckNewid
    {
        public double? Customerid { get; set; }
        public DateTime? LoanBriefCancelAt { get; set; }
        public DateTime? DisbursementAt1 { get; set; }
        public double? LoanbriefidDisbursemen1 { get; set; }
        public DateTime? DisbursementAt { get; set; }
        public double? LoanbriefidDisbursemen { get; set; }
    }
}
