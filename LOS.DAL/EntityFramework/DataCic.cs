﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class DataCic
    {
        public int Id { get; set; }
        public string CardNumber { get; set; }
        public DateTime? CreatedTime { get; set; }
        public string CreditInfo { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string CheckTime { get; set; }
        public string Brieft { get; set; }
        public string Mobile { get; set; }
        public int? NumberOfLoans { get; set; }
        public bool? HasBadDebt { get; set; }
        public bool? HasLatePayment { get; set; }
        public string StringJson { get; set; }
    }
}
