﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class DocumentType
    {
        public DocumentType()
        {
            LoanBriefFiles = new HashSet<LoanBriefFiles>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public byte? IsEnable { get; set; }
        public int? ProductId { get; set; }
        public int? NumberImage { get; set; }
        public string Guide { get; set; }
        public int? ParentId { get; set; }
        public int? TypeOwnerShip { get; set; }
        public string ObjectRuleType { get; set; }
        public int? Version { get; set; }
        public DateTime? CreatedTime { get; set; }
        public bool? Required { get; set; }
        public int? MobileUpload { get; set; }

        public virtual LoanProduct Product { get; set; }
        public virtual LoanBriefDocument LoanBriefDocument { get; set; }
        public virtual ICollection<LoanBriefFiles> LoanBriefFiles { get; set; }
    }
}
