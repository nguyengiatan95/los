﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Dstattoan
    {
        public int? Id { get; set; }
        public DateTime? Tattoan { get; set; }
        public DateTime? Daohan { get; set; }
        public double? Day { get; set; }
        public string Status { get; set; }
    }
}
