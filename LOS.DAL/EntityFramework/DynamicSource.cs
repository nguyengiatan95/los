﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class DynamicSource
    {
        public int LoanBriefId { get; set; }
        public int? ConfigDynamicSourceId { get; set; }
    }
}
