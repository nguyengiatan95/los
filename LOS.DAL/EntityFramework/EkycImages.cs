﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class EkycImages
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int? Type { get; set; }
        public int? Order { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? Attempt { get; set; }
    }
}
