﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class EmailPermission
    {
        public int EmailPermissionId { get; set; }
        public int ControllerActionId { get; set; }
        public int EmailLoginId { get; set; }
    }
}
