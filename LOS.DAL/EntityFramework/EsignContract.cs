﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class EsignContract
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public int? TypeEsign { get; set; }
        public string Url { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? Status { get; set; }
        public string AgreementId { get; set; }
        public string BillCode { get; set; }
        public int? LenderId { get; set; }
    }
}
