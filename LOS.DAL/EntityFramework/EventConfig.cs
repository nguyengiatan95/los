﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class EventConfig
    {
        public int Id { get; set; }
        public string EventName { get; set; }
        public DateTime? CreatedAt { get; set; }
        public bool? IsEnable { get; set; }
    }
}
