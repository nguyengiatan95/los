﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class FormAction
    {
        public double? SupportLastId { get; set; }
        public double? UserId { get; set; }
        public double? LoanBriefId { get; set; }
    }
}
