﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Group
    {
        public Group()
        {
            GroupActionPermission = new HashSet<GroupActionPermission>();
            GroupCondition = new HashSet<GroupCondition>();
            GroupModule = new HashSet<GroupModule>();
            User = new HashSet<User>();
        }

        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public string DefaultPath { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual ICollection<GroupActionPermission> GroupActionPermission { get; set; }
        public virtual ICollection<GroupCondition> GroupCondition { get; set; }
        public virtual ICollection<GroupModule> GroupModule { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
