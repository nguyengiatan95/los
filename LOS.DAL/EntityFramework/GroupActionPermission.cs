﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class GroupActionPermission
    {
        public int Id { get; set; }
        public int? GroupId { get; set; }
        public int? LoanStatus { get; set; }
        public int? Value { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? CreatedBy { get; set; }

        public virtual Group Group { get; set; }
    }
}
