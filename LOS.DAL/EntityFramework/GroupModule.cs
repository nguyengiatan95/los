﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class GroupModule
    {
        public int GroupModuleId { get; set; }
        public int GroupId { get; set; }
        public int ModuleId { get; set; }
        public int? ApplicationId { get; set; }

        public virtual Group Group { get; set; }
        public virtual Module Module { get; set; }
    }
}
