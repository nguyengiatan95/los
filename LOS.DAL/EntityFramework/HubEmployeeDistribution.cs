﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class HubEmployeeDistribution
    {
        public int Id { get; set; }
        public int? HubId { get; set; }
        public int? EmployeeId { get; set; }
        public int? Count { get; set; }
        public DateTime? Date { get; set; }
    }
}
