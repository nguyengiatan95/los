﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class InfomationProductDetail
    {
        public InfomationProductDetail()
        {
            ConfigProductDetail = new HashSet<ConfigProductDetail>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsAppraiser { get; set; }
        public bool? IsJobDocument { get; set; }
        public string Description { get; set; }
        public long? MaxMoney { get; set; }
        public int? Priority { get; set; }
        public bool? IsEnable { get; set; }
        public int? TypeOwnership { get; set; }

        public virtual ICollection<ConfigProductDetail> ConfigProductDetail { get; set; }
    }
}
