﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Job
    {
        public Job()
        {
            ContactCustomer = new HashSet<ContactCustomer>();
            LoanBriefJob = new HashSet<LoanBriefJob>();
        }

        public int JobId { get; set; }
        public string Name { get; set; }
        public int? Priority { get; set; }
        public int? MecashId { get; set; }
        public int? ParentId { get; set; }

        public virtual ICollection<ContactCustomer> ContactCustomer { get; set; }
        public virtual ICollection<LoanBriefJob> LoanBriefJob { get; set; }
    }
}
