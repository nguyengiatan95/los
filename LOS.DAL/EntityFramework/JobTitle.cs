﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class JobTitle
    {
        public JobTitle()
        {
            LoanBriefJob = new HashSet<LoanBriefJob>();
        }

        public int JobTitleId { get; set; }
        public string Name { get; set; }
        public string Priority { get; set; }

        public virtual ICollection<LoanBriefJob> LoanBriefJob { get; set; }
    }
}
