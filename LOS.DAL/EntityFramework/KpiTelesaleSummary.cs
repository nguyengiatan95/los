﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class KpiTelesaleSummary
    {
        public int Id { get; set; }
        public string MonthKpi { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public int? Form { get; set; }
        public int? Lead { get; set; }
        public int? Loan { get; set; }
        public long? LoanAmountProductMotor { get; set; }
        public long? LoanAmountProductCar { get; set; }
        public long? TotalAmount { get; set; }
        public double? F2l { get; set; }
        public double? F2s { get; set; }
        public double? TotalKpi { get; set; }
        public double? KpiMotor { get; set; }
        public double? F2lpercent { get; set; }
        public double? F2spercent { get; set; }
        public double? CompletePercent { get; set; }
        public double? QuintilePerformance { get; set; }
        public long? NewIdMoney { get; set; }
        public long? CarMoney { get; set; }
        public long? Incentive { get; set; }
        public double? Bonus { get; set; }
        public long? BonusMoney { get; set; }
        public double? Behavior { get; set; }
        public long? DiscountBehavior { get; set; }
        public long? Discount { get; set; }
        public long? Salary { get; set; }
        public long? ContestMotor { get; set; }
        public long? ContestCar { get; set; }
        public long? ContestVanto { get; set; }
        public long? SalaryAndContest { get; set; }
        public int? Parameter1 { get; set; }
        public int? Parameter2 { get; set; }
        public long? Parameter3 { get; set; }
        public long? Parameter4 { get; set; }
        public double? Parameter5 { get; set; }
        public double? Parameter6 { get; set; }
        public string Parameter7 { get; set; }
        public string Parameter8 { get; set; }
        public string Parameter9 { get; set; }
        public string Parameter10 { get; set; }
    }
}
