﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LOS.DAL.EntityFramework
{
    public partial class LOSContext : DbContext
    {
        public LOSContext()
        {
        }

        public LOSContext(DbContextOptions<LOSContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccessToken> AccessToken { get; set; }
        public virtual DbSet<AlertAutocall> AlertAutocall { get; set; }
        public virtual DbSet<ApiMonitor> ApiMonitor { get; set; }
        public virtual DbSet<AuthorizationToken> AuthorizationToken { get; set; }
        public virtual DbSet<Bank> Bank { get; set; }
        public virtual DbSet<BlackList> BlackList { get; set; }
        public virtual DbSet<BlackListLog> BlackListLog { get; set; }
        public virtual DbSet<BrandProduct> BrandProduct { get; set; }
        public virtual DbSet<CallbulogErr> CallbulogErr { get; set; }
        public virtual DbSet<CheckLoanInformation> CheckLoanInformation { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<CompareDocument> CompareDocument { get; set; }
        public virtual DbSet<ConfigContractRate> ConfigContractRate { get; set; }
        public virtual DbSet<ConfigDepartmentStatus> ConfigDepartmentStatus { get; set; }
        public virtual DbSet<ConfigDocument> ConfigDocument { get; set; }
        public virtual DbSet<ConfigDynamicSource> ConfigDynamicSource { get; set; }
        public virtual DbSet<ConfigPolicy> ConfigPolicy { get; set; }
        public virtual DbSet<ConfigProductDetail> ConfigProductDetail { get; set; }
        public virtual DbSet<ContactCustomer> ContactCustomer { get; set; }
        public virtual DbSet<ControllerAction> ControllerAction { get; set; }
        public virtual DbSet<CoordinatorCheckList> CoordinatorCheckList { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<DataCic> DataCic { get; set; }
        public virtual DbSet<Dauso> Dauso { get; set; }
        public virtual DbSet<Deadlock11> Deadlock11 { get; set; }
        public virtual DbSet<DeviceTrackingReport> DeviceTrackingReport { get; set; }
        public virtual DbSet<DeviceTrackingReportDetail> DeviceTrackingReportDetail { get; set; }
        public virtual DbSet<District> District { get; set; }
        public virtual DbSet<DocumentException> DocumentException { get; set; }
        public virtual DbSet<DocumentType> DocumentType { get; set; }
        public virtual DbSet<DynamicSource> DynamicSource { get; set; }
        public virtual DbSet<EkycImages> EkycImages { get; set; }
        public virtual DbSet<EmailLogin> EmailLogin { get; set; }
        public virtual DbSet<EmailPermission> EmailPermission { get; set; }
        public virtual DbSet<EsignContract> EsignContract { get; set; }
        public virtual DbSet<EventConfig> EventConfig { get; set; }
        public virtual DbSet<GoogleScriptLogReport> GoogleScriptLogReport { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<GroupActionPermission> GroupActionPermission { get; set; }
        public virtual DbSet<GroupCondition> GroupCondition { get; set; }
        public virtual DbSet<GroupModule> GroupModule { get; set; }
        public virtual DbSet<GroupModulePermission> GroupModulePermission { get; set; }
        public virtual DbSet<GroupSource> GroupSource { get; set; }
        public virtual DbSet<HubDistribution> HubDistribution { get; set; }
        public virtual DbSet<HubEmployeeDistribution> HubEmployeeDistribution { get; set; }
        public virtual DbSet<HubLoanBrief> HubLoanBrief { get; set; }
        public virtual DbSet<HubType> HubType { get; set; }
        public virtual DbSet<InfomationProductDetail> InfomationProductDetail { get; set; }
        public virtual DbSet<Inventory> Inventory { get; set; }
        public virtual DbSet<InventoryDetail> InventoryDetail { get; set; }
        public virtual DbSet<Job> Job { get; set; }
        public virtual DbSet<JobTitle> JobTitle { get; set; }
        public virtual DbSet<KpiTelesale> KpiTelesale { get; set; }
        public virtual DbSet<KpiTelesaleSummary> KpiTelesaleSummary { get; set; }
        public virtual DbSet<LenderConfig> LenderConfig { get; set; }
        public virtual DbSet<LenderLoanBrief> LenderLoanBrief { get; set; }
        public virtual DbSet<LoanAction> LoanAction { get; set; }
        public virtual DbSet<LoanBrief> LoanBrief { get; set; }
        public virtual DbSet<LoanBriefCompany> LoanBriefCompany { get; set; }
        public virtual DbSet<LoanBriefDocument> LoanBriefDocument { get; set; }
        public virtual DbSet<LoanBriefFiles> LoanBriefFiles { get; set; }
        public virtual DbSet<LoanBriefHistory> LoanBriefHistory { get; set; }
        public virtual DbSet<LoanBriefHousehold> LoanBriefHousehold { get; set; }
        public virtual DbSet<LoanBriefImportFileExcel> LoanBriefImportFileExcel { get; set; }
        public virtual DbSet<LoanBriefJob> LoanBriefJob { get; set; }
        public virtual DbSet<LoanBriefNote> LoanBriefNote { get; set; }
        public virtual DbSet<LoanBriefProperty> LoanBriefProperty { get; set; }
        public virtual DbSet<LoanBriefQuestionScript> LoanBriefQuestionScript { get; set; }
        public virtual DbSet<LoanBriefRelationship> LoanBriefRelationship { get; set; }
        public virtual DbSet<LoanBriefResident> LoanBriefResident { get; set; }
        public virtual DbSet<LoanConfigStep> LoanConfigStep { get; set; }
        public virtual DbSet<LoanProduct> LoanProduct { get; set; }
        public virtual DbSet<LoanRateType> LoanRateType { get; set; }
        public virtual DbSet<LoanStatus> LoanStatus { get; set; }
        public virtual DbSet<LoanStatusDetail> LoanStatusDetail { get; set; }
        public virtual DbSet<LoanbriefLender> LoanbriefLender { get; set; }
        public virtual DbSet<LoanbriefScript> LoanbriefScript { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<LogActionRestruct> LogActionRestruct { get; set; }
        public virtual DbSet<LogBaoCaoTuDongDinhVi> LogBaoCaoTuDongDinhVi { get; set; }
        public virtual DbSet<LogCallApi> LogCallApi { get; set; }
        public virtual DbSet<LogCallApi1> LogCallApi1 { get; set; }
        public virtual DbSet<LogCallApiCisco> LogCallApiCisco { get; set; }
        public virtual DbSet<LogCallApiPartner> LogCallApiPartner { get; set; }
        public virtual DbSet<LogCancelLoanbrief> LogCancelLoanbrief { get; set; }
        public virtual DbSet<LogChangePhone> LogChangePhone { get; set; }
        public virtual DbSet<LogCiscoPushToTelesale> LogCiscoPushToTelesale { get; set; }
        public virtual DbSet<LogClickToCall> LogClickToCall { get; set; }
        public virtual DbSet<LogCloseLoan> LogCloseLoan { get; set; }
        public virtual DbSet<LogDistributingHubEmployee> LogDistributingHubEmployee { get; set; }
        public virtual DbSet<LogDistributionUser> LogDistributionUser { get; set; }
        public virtual DbSet<LogGeocodingResult> LogGeocodingResult { get; set; }
        public virtual DbSet<LogInfoCreateLoanbrief> LogInfoCreateLoanbrief { get; set; }
        public virtual DbSet<LogLatLngHomeCompany> LogLatLngHomeCompany { get; set; }
        public virtual DbSet<LogLoanAction> LogLoanAction { get; set; }
        public virtual DbSet<LogLoanInfoAi> LogLoanInfoAi { get; set; }
        public virtual DbSet<LogLoanInfoPartner> LogLoanInfoPartner { get; set; }
        public virtual DbSet<LogLoanbriefScoring> LogLoanbriefScoring { get; set; }
        public virtual DbSet<LogPartner> LogPartner { get; set; }
        public virtual DbSet<LogPushNotification> LogPushNotification { get; set; }
        public virtual DbSet<LogPushToCisco> LogPushToCisco { get; set; }
        public virtual DbSet<LogPushToHub> LogPushToHub { get; set; }
        public virtual DbSet<LogPushToPartner> LogPushToPartner { get; set; }
        public virtual DbSet<LogReLoanbrief> LogReLoanbrief { get; set; }
        public virtual DbSet<LogRequestAi> LogRequestAi { get; set; }
        public virtual DbSet<LogResultAutoCall> LogResultAutoCall { get; set; }
        public virtual DbSet<LogScheduleJob> LogScheduleJob { get; set; }
        public virtual DbSet<LogSecuredTransaction> LogSecuredTransaction { get; set; }
        public virtual DbSet<LogSendOtp> LogSendOtp { get; set; }
        public virtual DbSet<LogSendSms> LogSendSms { get; set; }
        public virtual DbSet<LogSendVoiceOtp> LogSendVoiceOtp { get; set; }
        public virtual DbSet<LogTrackingStopPoin> LogTrackingStopPoin { get; set; }
        public virtual DbSet<LogWorking> LogWorking { get; set; }
        public virtual DbSet<Logconnecttion> Logconnecttion { get; set; }
        public virtual DbSet<ManagerAreaHub> ManagerAreaHub { get; set; }
        public virtual DbSet<MapAutocall> MapAutocall { get; set; }
        public virtual DbSet<Material> Material { get; set; }
        public virtual DbSet<Module> Module { get; set; }
        public virtual DbSet<ModulePermission> ModulePermission { get; set; }
        public virtual DbSet<NoteLog> NoteLog { get; set; }
        public virtual DbSet<Notebanklog> Notebanklog { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<OrderSource> OrderSource { get; set; }
        public virtual DbSet<OrderSourceDetail> OrderSourceDetail { get; set; }
        public virtual DbSet<Permission> Permission { get; set; }
        public virtual DbSet<PermissionIp> PermissionIp { get; set; }
        public virtual DbSet<Pipeline> Pipeline { get; set; }
        public virtual DbSet<PipelineHistory> PipelineHistory { get; set; }
        public virtual DbSet<PipelineState> PipelineState { get; set; }
        public virtual DbSet<PlatformType> PlatformType { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductAppraiser> ProductAppraiser { get; set; }
        public virtual DbSet<ProductPercentReduction> ProductPercentReduction { get; set; }
        public virtual DbSet<ProductReview> ProductReview { get; set; }
        public virtual DbSet<ProductReviewDetail> ProductReviewDetail { get; set; }
        public virtual DbSet<ProductReviewResult> ProductReviewResult { get; set; }
        public virtual DbSet<ProductReviewResultDetail> ProductReviewResultDetail { get; set; }
        public virtual DbSet<ProductType> ProductType { get; set; }
        public virtual DbSet<Property> Property { get; set; }
        public virtual DbSet<ProposeExceptions> ProposeExceptions { get; set; }
        public virtual DbSet<Province> Province { get; set; }
        public virtual DbSet<PushLoanToPartner> PushLoanToPartner { get; set; }
        public virtual DbSet<QueueLog> QueueLog { get; set; }
        public virtual DbSet<ReasonCancel> ReasonCancel { get; set; }
        public virtual DbSet<ReasonCoordinator> ReasonCoordinator { get; set; }
        public virtual DbSet<RelativeFamily> RelativeFamily { get; set; }
        public virtual DbSet<ReportAutocall> ReportAutocall { get; set; }
        public virtual DbSet<ReportBehaviorHub> ReportBehaviorHub { get; set; }
        public virtual DbSet<ReportBehaviorTelesale> ReportBehaviorTelesale { get; set; }
        public virtual DbSet<ReportConvertHub> ReportConvertHub { get; set; }
        public virtual DbSet<ReportConvertTelesale> ReportConvertTelesale { get; set; }
        public virtual DbSet<ReportData> ReportData { get; set; }
        public virtual DbSet<ReportDataUpdating> ReportDataUpdating { get; set; }
        public virtual DbSet<ReportDepartmentError> ReportDepartmentError { get; set; }
        public virtual DbSet<ReportToken> ReportToken { get; set; }
        public virtual DbSet<RequestEvent> RequestEvent { get; set; }
        public virtual DbSet<ResultEkyc> ResultEkyc { get; set; }
        public virtual DbSet<RuleCheckLoan> RuleCheckLoan { get; set; }
        public virtual DbSet<Schedule> Schedule { get; set; }
        public virtual DbSet<ScheduleTime> ScheduleTime { get; set; }
        public virtual DbSet<Section> Section { get; set; }
        public virtual DbSet<SectionAction> SectionAction { get; set; }
        public virtual DbSet<SectionApprove> SectionApprove { get; set; }
        public virtual DbSet<SectionCondition> SectionCondition { get; set; }
        public virtual DbSet<SectionDetail> SectionDetail { get; set; }
        public virtual DbSet<SectionGroup> SectionGroup { get; set; }
        public virtual DbSet<Shop> Shop { get; set; }
        public virtual DbSet<StatusTelesales> StatusTelesales { get; set; }
        public virtual DbSet<SystemConfig> SystemConfig { get; set; }
        public virtual DbSet<TeamTelesales> TeamTelesales { get; set; }
        public virtual DbSet<TelesaleLoanbrief> TelesaleLoanbrief { get; set; }
        public virtual DbSet<TelesalesShift> TelesalesShift { get; set; }
        public virtual DbSet<TlsHdTlsNghiviec> TlsHdTlsNghiviec { get; set; }
        public virtual DbSet<TokenSmartDailer> TokenSmartDailer { get; set; }
        public virtual DbSet<Transactions> Transactions { get; set; }
        public virtual DbSet<TypeOwnerShip> TypeOwnerShip { get; set; }
        public virtual DbSet<TypeRemarketing> TypeRemarketing { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserActionPermission> UserActionPermission { get; set; }
        public virtual DbSet<UserCondition> UserCondition { get; set; }
        public virtual DbSet<UserMobileCisco> UserMobileCisco { get; set; }
        public virtual DbSet<UserModule> UserModule { get; set; }
        public virtual DbSet<UserShop> UserShop { get; set; }
        public virtual DbSet<UserTeamTelesales> UserTeamTelesales { get; set; }
        public virtual DbSet<UtmSource> UtmSource { get; set; }
        public virtual DbSet<Ward> Ward { get; set; }
        public virtual DbSet<ZaloDinhVi> ZaloDinhVi { get; set; }
        public virtual DbSet<ZaloLoanCredit> ZaloLoanCredit { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=165.22.49.7,1533;Initial Catalog=LOS;User ID=los;Password=123456a@;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccessToken>(entity =>
            {
                entity.Property(e => e.AccessToken1).HasColumnName("access_token");

                entity.Property(e => e.AppId)
                    .HasColumnName("app_id")
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.DateExpiration).HasColumnType("datetime");
            });

            modelBuilder.Entity<AlertAutocall>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("alert_autocall");

                entity.Property(e => e.Metric)
                    .HasColumnName("metric")
                    .HasMaxLength(100);

                entity.Property(e => e.Time)
                    .HasColumnName("time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<ApiMonitor>(entity =>
            {
                entity.ToTable("ApiMonitor", "rp");

                entity.Property(e => e.Action).HasMaxLength(1000);

                entity.Property(e => e.AppKey).HasMaxLength(1000);

                entity.Property(e => e.Controler).HasMaxLength(1000);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Desciption).HasMaxLength(500);

                entity.Property(e => e.Request).HasMaxLength(1000);

                entity.Property(e => e.Responce).HasMaxLength(1000);
            });

            modelBuilder.Entity<AuthorizationToken>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ServiceName).HasMaxLength(50);

                entity.Property(e => e.Token).HasMaxLength(1204);
            });

            modelBuilder.Entity<Bank>(entity =>
            {
                entity.Property(e => e.BankCode).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.NapasCode).HasMaxLength(50);
            });

            modelBuilder.Entity<BlackList>(entity =>
            {
                entity.Property(e => e.Note).HasColumnType("ntext");
            });

            modelBuilder.Entity<BlackListLog>(entity =>
            {
                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.IdNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BrandProduct>(entity =>
            {
                entity.Property(e => e.IsEnable).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<CallbulogErr>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("callbulogERR");

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(15);

                entity.Property(e => e.StartTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<CheckLoanInformation>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.UpdateAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CompareDocument>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.UserName)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ConfigContractRate>(entity =>
            {
                entity.Property(e => e.AppliedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<ConfigDepartmentStatus>(entity =>
            {
                entity.Property(e => e.LoanStatusName).HasMaxLength(200);
            });

            modelBuilder.Entity<ConfigDocument>(entity =>
            {
                entity.Property(e => e.AllowFromUpload)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.GroupJobId)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ConfigDynamicSource>(entity =>
            {
                entity.ToTable("ConfigDynamicSource", "rp");

                entity.HasComment("cấu hình báo cáo động");

                entity.Property(e => e.CodeQuery).HasComment("lưu điều kiện where");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(1000);

                entity.Property(e => e.Name).HasMaxLength(255);
            });

            modelBuilder.Entity<ConfigPolicy>(entity =>
            {
                entity.Property(e => e.Action)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ConfigValue)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Controller)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.UrlEnpoint)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ConfigProductDetail>(entity =>
            {
                entity.Property(e => e.CreateAt).HasColumnType("datetime");

                entity.Property(e => e.UpdateAt).HasColumnType("datetime");

                entity.HasOne(d => d.InfomationProductDetail)
                    .WithMany(p => p.ConfigProductDetail)
                    .HasForeignKey(d => d.InfomationProductDetailId)
                    .HasConstraintName("FK_InfomationProductDetail_ConfigProductDetail");
            });

            modelBuilder.Entity<ContactCustomer>(entity =>
            {
                entity.HasKey(e => e.ContactId);

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.Income).HasColumnType("money");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.HasOne(d => d.BrandProduct)
                    .WithMany(p => p.ContactCustomer)
                    .HasForeignKey(d => d.BrandProductId)
                    .HasConstraintName("FK__ContactCu__Brand__17AD7836");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.ContactCustomer)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK__ContactCu__Distr__14D10B8B");

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.ContactCustomer)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("FK__ContactCu__JobId__16B953FD");

                entity.HasOne(d => d.LoanBrief)
                    .WithMany(p => p.ContactCustomer)
                    .HasForeignKey(d => d.LoanBriefId)
                    .HasConstraintName("FK__ContactCu__LoanB__27E3DFFF");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ContactCustomer)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK__ContactCu__Produ__18A19C6F");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.ContactCustomer)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK__ContactCu__Provi__13DCE752");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.ContactCustomer)
                    .HasForeignKey(d => d.WardId)
                    .HasConstraintName("FK__ContactCu__WardI__15C52FC4");
            });

            modelBuilder.Entity<ControllerAction>(entity =>
            {
                entity.ToTable("ControllerAction", "rp");

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Controller)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<CoordinatorCheckList>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasIndex(e => e.Phone)
                    .HasName("ix_Customer");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.AddressLatLong)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyAddress).HasMaxLength(500);

                entity.Property(e => e.CompanyAddressLatLong)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyName).HasMaxLength(500);

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("date");

                entity.Property(e => e.Email)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EsignAgreementId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookAddress).HasMaxLength(500);

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.GpayAccountName)
                    .HasColumnName("GPay_AccountName")
                    .HasMaxLength(250);

                entity.Property(e => e.GpayAccountNumber)
                    .HasColumnName("GPay_AccountNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.GpayBankName)
                    .HasColumnName("Gpay_BankName")
                    .HasMaxLength(300);

                entity.Property(e => e.InsurenceNote).HasMaxLength(500);

                entity.Property(e => e.InsurenceNumber).HasMaxLength(50);

                entity.Property(e => e.NationCardPlace).HasMaxLength(200);

                entity.Property(e => e.NationalCard)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NationalCardDate).HasColumnType("date");

                entity.Property(e => e.Passport)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Salary).HasColumnType("money");

                entity.Property(e => e.TikiId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VaAccountName)
                    .HasColumnName("VA_AccountName")
                    .HasMaxLength(250);

                entity.Property(e => e.VaAccountNumber)
                    .HasColumnName("VA_AccountNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.VaBankCode)
                    .HasColumnName("VA_BankCode")
                    .HasMaxLength(50);

                entity.Property(e => e.VaBankName)
                    .HasColumnName("VA_BankName")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DataCic>(entity =>
            {
                entity.ToTable("DataCIC");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.Brieft).HasMaxLength(50);

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CheckTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.StringJson).HasColumnType("ntext");
            });

            modelBuilder.Entity<Dauso>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("dauso");

                entity.Property(e => e.Dauso1)
                    .HasColumnName("dauso")
                    .HasMaxLength(2);

                entity.Property(e => e.Mang)
                    .HasColumnName("mang")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Deadlock11>(entity =>
            {
                entity.HasKey(e => e.RowNumber)
                    .HasName("PK__deadlock__AAAC09D8277B7D5F");

                entity.ToTable("deadlock11");

                entity.Property(e => e.ApplicationName).HasMaxLength(128);

                entity.Property(e => e.BinaryData).HasColumnType("image");

                entity.Property(e => e.ClientProcessId).HasColumnName("ClientProcessID");

                entity.Property(e => e.Cpu).HasColumnName("CPU");

                entity.Property(e => e.DatabaseName).HasMaxLength(128);

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.LoginName).HasMaxLength(128);

                entity.Property(e => e.NtuserName)
                    .HasColumnName("NTUserName")
                    .HasMaxLength(128);

                entity.Property(e => e.ObjectId).HasColumnName("ObjectID");

                entity.Property(e => e.Spid).HasColumnName("SPID");

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.TextData).HasColumnType("ntext");
            });

            modelBuilder.Entity<DeviceTrackingReport>(entity =>
            {
                entity.ToTable("DeviceTrackingReport", "rp");

                entity.Property(e => e.AdressCompany).HasMaxLength(1000);

                entity.Property(e => e.AdressHome).HasMaxLength(1000);

                entity.Property(e => e.DisDate).HasColumnType("datetime");

                entity.Property(e => e.LastUpdateDevice).HasColumnType("datetime");

                entity.Property(e => e.StatusString).HasMaxLength(100);
            });

            modelBuilder.Entity<DeviceTrackingReportDetail>(entity =>
            {
                entity.ToTable("DeviceTrackingReportDetail", "rp");

                entity.Property(e => e.CompanyPercent).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.HomePercent).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TrackingDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<District>(entity =>
            {
                entity.Property(e => e.DistrictId).ValueGeneratedNever();

                entity.Property(e => e.LatLong)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MapTransaction)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<DocumentException>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<DocumentType>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.ObjectRuleType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.DocumentType)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK__DocumentT__Produ__1995C0A8");
            });

            modelBuilder.Entity<DynamicSource>(entity =>
            {
                entity.HasKey(e => e.LoanBriefId);

                entity.ToTable("DynamicSource", "rp");

                entity.HasComment("Báo cáo tổng hợp động");

                entity.Property(e => e.LoanBriefId)
                    .HasColumnName("loanBriefId")
                    .ValueGeneratedNever();

                entity.Property(e => e.ConfigDynamicSourceId).HasComment("id cấu hình trong bản ConfigDynamicSource");
            });

            modelBuilder.Entity<EkycImages>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.FileName).HasMaxLength(500);
            });

            modelBuilder.Entity<EmailLogin>(entity =>
            {
                entity.ToTable("EmailLogin", "rp");

                entity.Property(e => e.ConfirmedDate).HasColumnType("datetime");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email).HasMaxLength(500);

                entity.Property(e => e.Exp).HasComment("thời gian hết hạn token, google trả về");

                entity.Property(e => e.FamilyName).HasMaxLength(1000);

                entity.Property(e => e.FullName).HasMaxLength(1000);

                entity.Property(e => e.GivenName).HasMaxLength(1000);

                entity.Property(e => e.Iat).HasComment("Thời gian google trả về");

                entity.Property(e => e.Idtoken).HasColumnName("IDToken");

                entity.Property(e => e.ImageUrl)
                    .HasColumnName("ImageURL")
                    .HasMaxLength(1000);
            });

            modelBuilder.Entity<EmailPermission>(entity =>
            {
                entity.ToTable("EmailPermission", "rp");
            });

            modelBuilder.Entity<EsignContract>(entity =>
            {
                entity.Property(e => e.AgreementId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BillCode)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(500);
            });

            modelBuilder.Entity<EventConfig>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.EventName).HasMaxLength(500);
            });

            modelBuilder.Entity<GoogleScriptLogReport>(entity =>
            {
                entity.HasComment("Lưu log code trên GoogleScript");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(500);
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.Property(e => e.DefaultPath).HasMaxLength(200);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.GroupName).HasMaxLength(200);

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<GroupActionPermission>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupActionPermission)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("FK_GroupActionPermission_Group");
            });

            modelBuilder.Entity<GroupCondition>(entity =>
            {
                entity.Property(e => e.CreatetAt).HasColumnType("datetime");

                entity.Property(e => e.Operator)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value).HasMaxLength(500);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupCondition)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("FK_GroupCondition_GroupCondition");

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.GroupCondition)
                    .HasForeignKey(d => d.PropertyId)
                    .HasConstraintName("FK_GroupCondition_Property");
            });

            modelBuilder.Entity<GroupModule>(entity =>
            {
                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupModule)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupModule_Group");

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.GroupModule)
                    .HasForeignKey(d => d.ModuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupModule_Module");
            });

            modelBuilder.Entity<GroupModulePermission>(entity =>
            {
                entity.HasKey(e => new { e.GroupModuleId, e.PermissionId });

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.GroupModulePermission)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupModulePermission_Permission");
            });

            modelBuilder.Entity<GroupSource>(entity =>
            {
                entity.Property(e => e.GroupSourceId).HasColumnName("GroupSourceID");

                entity.Property(e => e.GroupSourceName).HasMaxLength(500);
            });

            modelBuilder.Entity<HubEmployeeDistribution>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("date");
            });

            modelBuilder.Entity<HubLoanBrief>(entity =>
            {
                entity.HasOne(d => d.Hub)
                    .WithMany(p => p.HubLoanBrief)
                    .HasForeignKey(d => d.HubId)
                    .HasConstraintName("FK_HubLoanBrief_Shop");
            });

            modelBuilder.Entity<HubType>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<Inventory>(entity =>
            {
                entity.ToTable("Inventory", "rp");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ShopName).HasMaxLength(50);

                entity.Property(e => e.StatusOfDeviceId).HasColumnName("StatusOfDeviceID");

                entity.Property(e => e.UserImportShopId).HasComment("người dùng chuyển thiết bị qua kho khác, khi hệ thống check nếu TB trong db ở kho khác với người dùng, thì sẽ ưu tiên kho của người dùng, nếu  = nhau thì sẽ clear cột này");

                entity.Property(e => e.UserImportShopName).HasMaxLength(50);
            });

            modelBuilder.Entity<InventoryDetail>(entity =>
            {
                entity.ToTable("InventoryDetail", "rp");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<JobTitle>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Priority)
                    .HasMaxLength(10)
                    .IsFixedLength();
            });

            modelBuilder.Entity<KpiTelesale>(entity =>
            {
                entity.Property(e => e.F2l).HasColumnName("F2L");

                entity.Property(e => e.F2lpercent).HasColumnName("F2LPercent");

                entity.Property(e => e.F2s).HasColumnName("F2S");

                entity.Property(e => e.F2spercent).HasColumnName("F2SPercent");

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.InstertedTime).HasColumnType("datetime");

                entity.Property(e => e.MonthKpi)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Parameter10).HasMaxLength(200);

                entity.Property(e => e.Parameter7)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Parameter8)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Parameter9).HasMaxLength(200);
            });

            modelBuilder.Entity<KpiTelesaleSummary>(entity =>
            {
                entity.Property(e => e.F2l).HasColumnName("F2L");

                entity.Property(e => e.F2lpercent).HasColumnName("F2LPercent");

                entity.Property(e => e.F2s).HasColumnName("F2S");

                entity.Property(e => e.F2spercent).HasColumnName("F2SPercent");

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.MonthKpi)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Parameter10).HasMaxLength(200);

                entity.Property(e => e.Parameter7)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Parameter8)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Parameter9).HasMaxLength(200);
            });

            modelBuilder.Entity<LenderConfig>(entity =>
            {
                entity.HasIndex(e => e.LenderName)
                    .HasName("NonClusteredIndex-20210504-140040")
                    .IsUnique();

                entity.Property(e => e.CurrentMoney).HasColumnType("money");

                entity.Property(e => e.Esign).HasColumnName("ESign");

                entity.Property(e => e.EsignUpdatedTime)
                    .HasColumnName("ESignUpdatedTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.InitialMoney).HasColumnType("money");

                entity.Property(e => e.LenderName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LockedMoney).HasColumnType("money");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<LenderLoanBrief>(entity =>
            {
                entity.HasIndex(e => new { e.LenderName, e.LoanBriefId })
                    .HasName("NonClusteredIndex-20210504-135954")
                    .IsUnique();

                entity.Property(e => e.LenderName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PushedTime).HasColumnType("datetime");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<LoanAction>(entity =>
            {
                entity.Property(e => e.Note).HasMaxLength(500);

                entity.HasOne(d => d.Actor)
                    .WithMany(p => p.LoanAction)
                    .HasForeignKey(d => d.ActorId)
                    .HasConstraintName("FK_LoanAction_User");
            });

            modelBuilder.Entity<LoanBrief>(entity =>
            {
                entity.HasIndex(e => e.Phone)
                    .HasName("NonClusteredIndex-20210602-103142");

                entity.HasIndex(e => e.PhoneOther)
                    .HasName("NonClusteredIndex-20210602-103408");

                entity.HasIndex(e => e.PipelineState)
                    .HasName("NonClusteredIndex-20210531-171335");

                entity.HasIndex(e => e.Status)
                    .HasName("NonClusteredIndex_Status");

                entity.HasIndex(e => new { e.DomainName, e.CreatedTime })
                    .HasName("NonClusteredIndex-20210602-103649");

                entity.HasIndex(e => new { e.NationCardPlace, e.PhoneOther })
                    .HasName("NonClusteredIndex-20210509-142345");

                entity.HasIndex(e => new { e.NationalCard, e.PlatformType })
                    .HasName("ix_LoanBrief2");

                entity.HasIndex(e => new { e.BoundTelesaleId, e.PlatformType, e.DisbursementAt })
                    .HasName("ix_LoanBrief_PlatformType");

                entity.HasIndex(e => new { e.CreatedTime, e.IsReborrow, e.BoundTelesaleId })
                    .HasName("ix_LoanBrief4");

                entity.HasIndex(e => new { e.FullName, e.Phone, e.NationalCard })
                    .HasName("NonClusteredIndex");

                entity.HasIndex(e => new { e.InProcess, e.Status, e.HubId })
                    .HasName("ix_LoanBrief3");

                entity.HasIndex(e => new { e.InProcess, e.ProductId, e.Status, e.CreatedTime })
                    .HasName("ix_LoanBrief42");

                entity.HasIndex(e => new { e.InProcess, e.Status, e.BoundTelesaleId, e.CreatedTime })
                    .HasName("ix_LoanBrief");

                entity.HasIndex(e => new { e.InProcess, e.Status, e.HubId, e.DisbursementAt })
                    .HasName("ix_LoanBrief41");

                entity.HasIndex(e => new { e.InProcess, e.ProvinceId, e.Status, e.HubId, e.DisbursementAt })
                    .HasName("ix_LoanBrief_InProcess");

                entity.Property(e => e.ActivedDeviceAt).HasColumnType("datetime");

                entity.Property(e => e.AffCode)
                    .HasColumnName("Aff_Code")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.AffPub).HasMaxLength(500);

                entity.Property(e => e.AffSid)
                    .HasColumnName("Aff_Sid")
                    .HasMaxLength(500);

                entity.Property(e => e.AffSource).HasMaxLength(500);

                entity.Property(e => e.BankAccountName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BankAccountNumber)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BankBranch).HasMaxLength(1024);

                entity.Property(e => e.BankCardNumber)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BeginStartTime).HasColumnType("datetime");

                entity.Property(e => e.BuyInsuranceLender).HasDefaultValueSql("((1))");

                entity.Property(e => e.CheckCic).HasColumnName("CheckCIC");

                entity.Property(e => e.ContractGinno).HasMaxLength(50);

                entity.Property(e => e.CoordinatorPushAt).HasColumnType("datetime");

                entity.Property(e => e.DeviceId).HasMaxLength(50);

                entity.Property(e => e.DisbursementAt).HasColumnType("datetime");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("date");

                entity.Property(e => e.DomainName).HasMaxLength(500);

                entity.Property(e => e.Email)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EsignBillCode)
                    .HasColumnName("ESignBillCode")
                    .HasMaxLength(100);

                entity.Property(e => e.EsignBorrowerContract)
                    .HasColumnName("ESignBorrowerContract")
                    .HasMaxLength(500);

                entity.Property(e => e.EsignLenderContract)
                    .HasColumnName("ESignLenderContract")
                    .HasMaxLength(500);

                entity.Property(e => e.EvaluationCompanyTime).HasColumnType("datetime");

                entity.Property(e => e.EvaluationHomeTime).HasColumnType("datetime");

                entity.Property(e => e.FeeInsuranceOfCustomer).HasColumnType("money");

                entity.Property(e => e.FeeInsuranceOfLender).HasColumnType("money");

                entity.Property(e => e.FeeInsuranceOfProperty).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.FeePaymentBeforeLoan).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.FinishAt).HasColumnType("datetime");

                entity.Property(e => e.FirstProcessingTime)
                    .HasColumnName("First_ProcessingTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.FirstTimeHubFeedBack).HasColumnType("datetime");

                entity.Property(e => e.FirstTimeStaffHubFeedback).HasColumnType("datetime");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.GoogleClickId).HasMaxLength(1024);

                entity.Property(e => e.HubEmployeeCallFirst).HasColumnType("datetime");

                entity.Property(e => e.HubEmployeeReceived).HasColumnType("datetime");

                entity.Property(e => e.HubPushAt).HasColumnType("datetime");

                entity.Property(e => e.InProcess).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsCheckCic).HasColumnName("IsCheckCIC");

                entity.Property(e => e.IsVerifyOtpLdp).HasColumnName("IsVerifyOtpLDP");

                entity.Property(e => e.LabelRequestLocation).HasMaxLength(500);

                entity.Property(e => e.LabelResponseLocation).HasMaxLength(500);

                entity.Property(e => e.LabelScore).HasMaxLength(50);

                entity.Property(e => e.LastChangeStatusOfTelesale).HasColumnType("datetime");

                entity.Property(e => e.LastSendRequestActive).HasColumnType("datetime");

                entity.Property(e => e.LeadScore).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.LenderName).HasMaxLength(200);

                entity.Property(e => e.LenderReceivedDate).HasColumnType("datetime");

                entity.Property(e => e.LmsLoanId).HasColumnName("LMS_LoanID");

                entity.Property(e => e.LoanAmount).HasColumnType("money");

                entity.Property(e => e.LoanAmountExpertise).HasColumnType("money");

                entity.Property(e => e.LoanAmountExpertiseAi)
                    .HasColumnName("LoanAmountExpertiseAI")
                    .HasColumnType("money");

                entity.Property(e => e.LoanAmountExpertiseLast).HasColumnType("money");

                entity.Property(e => e.LoanAmountFirst).HasColumnType("money");

                entity.Property(e => e.LoanBriefCancelAt).HasColumnType("datetime");

                entity.Property(e => e.NationCardPlace).HasMaxLength(200);

                entity.Property(e => e.NationalCard)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NationalCardDate).HasColumnType("date");

                entity.Property(e => e.NextDate).HasColumnType("datetime");

                entity.Property(e => e.Passport)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneOther)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PresenterCode).HasMaxLength(100);

                entity.Property(e => e.PushCiscoAt).HasColumnType("datetime");

                entity.Property(e => e.RateMoney).HasColumnType("money");

                entity.Property(e => e.ResultCic).HasColumnName("ResultCIC");

                entity.Property(e => e.ResultLocation).HasMaxLength(500);

                entity.Property(e => e.ResultRuleCheck).HasMaxLength(500);

                entity.Property(e => e.ScheduleTime).HasColumnType("datetime");

                entity.Property(e => e.Step).HasColumnName("step");

                entity.Property(e => e.SynchorizeFinance).HasDefaultValueSql("((0))");

                entity.Property(e => e.TelesalesPushAt).HasColumnType("datetime");

                entity.Property(e => e.Tid)
                    .HasColumnName("TId")
                    .HasMaxLength(500);

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.Property(e => e.UtmCampaign).HasMaxLength(500);

                entity.Property(e => e.UtmContent).HasMaxLength(500);

                entity.Property(e => e.UtmMedium).HasMaxLength(500);

                entity.Property(e => e.UtmSource).HasMaxLength(500);

                entity.Property(e => e.UtmTerm).HasMaxLength(500);

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("FK_LoanBrief_Bank");

                entity.HasOne(d => d.BoundTelesale)
                    .WithMany(p => p.LoanBriefBoundTelesale)
                    .HasForeignKey(d => d.BoundTelesaleId)
                    .HasConstraintName("FK_LoanBrief_User3");

                entity.HasOne(d => d.CoordinatorUser)
                    .WithMany(p => p.LoanBriefCoordinatorUser)
                    .HasForeignKey(d => d.CoordinatorUserId)
                    .HasConstraintName("FK_LoanBrief_UserCoordinator");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_LoanBrief_Customer");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_LoanBrief_District");

                entity.HasOne(d => d.EvaluationCompanyUser)
                    .WithMany(p => p.LoanBriefEvaluationCompanyUser)
                    .HasForeignKey(d => d.EvaluationCompanyUserId)
                    .HasConstraintName("FK_LoanBrief_User1");

                entity.HasOne(d => d.EvaluationHomeUser)
                    .WithMany(p => p.LoanBriefEvaluationHomeUser)
                    .HasForeignKey(d => d.EvaluationHomeUserId)
                    .HasConstraintName("FK_LoanBrief_User2");

                entity.HasOne(d => d.HubEmployee)
                    .WithMany(p => p.LoanBriefHubEmployee)
                    .HasForeignKey(d => d.HubEmployeeId)
                    .HasConstraintName("FK_LoanBrief_User");

                entity.HasOne(d => d.Hub)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.HubId)
                    .HasConstraintName("FK_LoanBrief_Shop");

                entity.HasOne(d => d.LoanBriefCancelByNavigation)
                    .WithMany(p => p.LoanBriefLoanBriefCancelByNavigation)
                    .HasForeignKey(d => d.LoanBriefCancelBy)
                    .HasConstraintName("FK_LoanBrief_User4");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_LoanBrief_LoanProduct");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_LoanBrief_Province");

                entity.HasOne(d => d.RateType)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.RateTypeId)
                    .HasConstraintName("FK_LoanBrief_LoanRateType");

                entity.HasOne(d => d.ReasonCancelNavigation)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.ReasonCancel)
                    .HasConstraintName("FK_LoanBrief_ReasonCancel");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.WardId)
                    .HasConstraintName("FK_LoanBrief_Ward");
            });

            modelBuilder.Entity<LoanBriefCompany>(entity =>
            {
                entity.HasKey(e => e.LoanBriefId)
                    .HasName("PK_LoanBriefCompany_1");

                entity.Property(e => e.LoanBriefId).ValueGeneratedNever();

                entity.Property(e => e.BirthdayShareholder).HasColumnType("datetime");

                entity.Property(e => e.BusinessCertificationAddress).HasMaxLength(500);

                entity.Property(e => e.BusinessCertificationDate).HasColumnType("datetime");

                entity.Property(e => e.CardNumberShareholder)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CardNumberShareholderDate).HasColumnType("datetime");

                entity.Property(e => e.CareerBusiness).HasMaxLength(500);

                entity.Property(e => e.CompanyName).HasMaxLength(500);

                entity.Property(e => e.CompanyShareholder).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.PlaceOfBirthShareholder).HasMaxLength(500);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.LoanBrief)
                    .WithOne(p => p.LoanBriefCompany)
                    .HasForeignKey<LoanBriefCompany>(d => d.LoanBriefId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoanBriefCompany_LoanBrief");
            });

            modelBuilder.Entity<LoanBriefDocument>(entity =>
            {
                entity.Property(e => e.LoanBriefDocumentId).ValueGeneratedOnAdd();

                entity.Property(e => e.FilePath).HasMaxLength(500);

                entity.Property(e => e.RelativePath).HasMaxLength(500);

                entity.HasOne(d => d.LoanBriefDocumentNavigation)
                    .WithOne(p => p.LoanBriefDocument)
                    .HasForeignKey<LoanBriefDocument>(d => d.LoanBriefDocumentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoanBriefDocument_DocumentType");

                entity.HasOne(d => d.LoanBrief)
                    .WithMany(p => p.LoanBriefDocument)
                    .HasForeignKey(d => d.LoanBriefId)
                    .HasConstraintName("FK_LoanBriefDocument_LoanBrief");
            });

            modelBuilder.Entity<LoanBriefFiles>(entity =>
            {
                entity.HasIndex(e => new { e.LoanBriefId, e.IsDeleted })
                    .HasName("ix_LoanBriefFiles2");

                entity.HasIndex(e => new { e.FilePath, e.TypeId, e.S3status, e.MecashId, e.LoanBriefId, e.IsDeleted })
                    .HasName("ix_LoanBriefFiles");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AddressUpload).HasMaxLength(500);

                entity.Property(e => e.CreateAt).HasColumnType("datetime");

                entity.Property(e => e.DistanceAddressCompanyGoogleMap).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.DistanceAddressHomeGoogleMap).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.DomainOfPartner)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FileThumb)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.LatLong)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.LinkImgOfPartner)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyAt).HasColumnType("datetime");

                entity.Property(e => e.S3status)
                    .HasColumnName("S3Status")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Synchronize).HasDefaultValueSql("((0))");

                entity.Property(e => e.TypeFile)
                    .HasDefaultValueSql("((1))")
                    .HasComment("File Ảnh, File Video hoặc File Âm Thanh");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.LoanBriefFiles)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_LoanBriefFiles_DocumentType");
            });

            modelBuilder.Entity<LoanBriefHistory>(entity =>
            {
                entity.Property(e => e.Action)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NewValue).HasColumnType("ntext");

                entity.Property(e => e.OldValue).HasColumnType("ntext");

                entity.HasOne(d => d.LoanBrief)
                    .WithMany(p => p.LoanBriefHistory)
                    .HasForeignKey(d => d.LoanBriefId)
                    .HasConstraintName("FK_LoanBriefHistory_LoanBrief");
            });

            modelBuilder.Entity<LoanBriefHousehold>(entity =>
            {
                entity.Property(e => e.LoanBriefHouseholdId).ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.AppartmentNumber).HasMaxLength(50);

                entity.Property(e => e.BirdayHouseOwner).HasMaxLength(10);

                entity.Property(e => e.Block).HasMaxLength(50);

                entity.Property(e => e.FullNameHouseOwner).HasMaxLength(250);

                entity.Property(e => e.Lane).HasMaxLength(50);

                entity.HasOne(d => d.District)
                    .WithMany(p => p.LoanBriefHousehold)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_LoanBriefHousehold_District");

                entity.HasOne(d => d.LoanBriefHouseholdNavigation)
                    .WithOne(p => p.LoanBriefHousehold)
                    .HasForeignKey<LoanBriefHousehold>(d => d.LoanBriefHouseholdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoanBriefHousehold_LoanBrief");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.LoanBriefHousehold)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_LoanBriefHousehold_Province");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.LoanBriefHousehold)
                    .HasForeignKey(d => d.WardId)
                    .HasConstraintName("FK_LoanBriefHousehold_Ward");
            });

            modelBuilder.Entity<LoanBriefImportFileExcel>(entity =>
            {
                entity.Property(e => e.CityName).HasMaxLength(500);

                entity.Property(e => e.CityNameHouseHold).HasMaxLength(500);

                entity.Property(e => e.CompanyName).HasMaxLength(1024);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DistrictName).HasMaxLength(500);

                entity.Property(e => e.DistrictNameHouseHold).HasMaxLength(500);

                entity.Property(e => e.FileImportId)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.FileImportName)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.NationalCard)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Note).HasMaxLength(1024);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProductName).HasMaxLength(500);

                entity.Property(e => e.Source)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserName).HasMaxLength(250);
            });

            modelBuilder.Entity<LoanBriefJob>(entity =>
            {
                entity.HasIndex(e => e.LoanBriefJobId)
                    .HasName("_dta_index_LoanBriefJob_5_2037582297__K1_6221");

                entity.HasIndex(e => new { e.CompanyAddress, e.DocumentAvailable })
                    .HasName("_dta_index_LoanBriefJob_5_2037582297__K7_10_9953");

                entity.HasIndex(e => new { e.CompanyAddress, e.LoanBriefJobId })
                    .HasName("_dta_index_LoanBriefJob_5_2037582297__K1_10_8337");

                entity.HasIndex(e => new { e.DocumentAvailable, e.LoanBriefJobId })
                    .HasName("_dta_stat_2037582297_7_1");

                entity.HasIndex(e => new { e.CompanyAddress, e.DocumentAvailable, e.LoanBriefJobId })
                    .HasName("_dta_index_LoanBriefJob_5_2037582297__K7_K1_10_4364");

                entity.Property(e => e.LoanBriefJobId).ValueGeneratedNever();

                entity.Property(e => e.BankName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyAddress).HasMaxLength(500);

                entity.Property(e => e.CompanyAddressGoogleMap).HasMaxLength(500);

                entity.Property(e => e.CompanyAddressLatLng).HasMaxLength(100);

                entity.Property(e => e.CompanyName).HasMaxLength(200);

                entity.Property(e => e.CompanyPhone).HasMaxLength(50);

                entity.Property(e => e.CompanyTaxCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentName).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ResultLocation).HasMaxLength(200);

                entity.Property(e => e.TotalIncome).HasColumnType("money");

                entity.Property(e => e.WorkingAddress).HasMaxLength(500);

                entity.HasOne(d => d.CompanyDistrict)
                    .WithMany(p => p.LoanBriefJob)
                    .HasForeignKey(d => d.CompanyDistrictId)
                    .HasConstraintName("FK_LoanBriefJob_District");

                entity.HasOne(d => d.CompanyProvince)
                    .WithMany(p => p.LoanBriefJob)
                    .HasForeignKey(d => d.CompanyProvinceId)
                    .HasConstraintName("FK_LoanBriefJob_Province");

                entity.HasOne(d => d.CompanyWard)
                    .WithMany(p => p.LoanBriefJob)
                    .HasForeignKey(d => d.CompanyWardId)
                    .HasConstraintName("FK_LoanBriefJob_Ward");

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.LoanBriefJob)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("FK_LoanBriefJob_Job");

                entity.HasOne(d => d.JobTitle)
                    .WithMany(p => p.LoanBriefJob)
                    .HasForeignKey(d => d.JobTitleId)
                    .HasConstraintName("FK_LoanBriefJob_JobTitle");

                entity.HasOne(d => d.LoanBriefJobNavigation)
                    .WithOne(p => p.LoanBriefJob)
                    .HasForeignKey<LoanBriefJob>(d => d.LoanBriefJobId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoanBriefJob_LoanBrief");
            });

            modelBuilder.Entity<LoanBriefNote>(entity =>
            {
                entity.HasIndex(e => new { e.Type, e.UserId, e.FullName, e.Note, e.Status, e.CreatedTime, e.ShopName, e.LoanBriefId })
                    .HasName("ix_LoanBriefNote");

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.Note).HasColumnType("nvarchar(max)");

                entity.Property(e => e.ShopName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.LoanBrief)
                    .WithMany(p => p.LoanBriefNote)
                    .HasForeignKey(d => d.LoanBriefId)
                    .HasConstraintName("FK_LoanBriefNote_LoanBrief");
            });

            modelBuilder.Entity<LoanBriefProperty>(entity =>
            {
                entity.Property(e => e.LoanBriefPropertyId).ValueGeneratedNever();

                entity.Property(e => e.CarColor).HasMaxLength(250);

                entity.Property(e => e.CarManufacturer).HasMaxLength(100);

                entity.Property(e => e.CarName).HasMaxLength(100);

                entity.Property(e => e.Chassis)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Engine)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MotobikeCertificateAddress).HasMaxLength(500);

                entity.Property(e => e.MotobikeCertificateDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MotobikeCertificateNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerFullName).HasMaxLength(50);

                entity.Property(e => e.PlateNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PlateNumberCar)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Brand)
                    .WithMany(p => p.LoanBriefProperty)
                    .HasForeignKey(d => d.BrandId)
                    .HasConstraintName("FK_LoanBriefProperty_BrandProduct");

                entity.HasOne(d => d.LoanBriefPropertyNavigation)
                    .WithOne(p => p.LoanBriefProperty)
                    .HasForeignKey<LoanBriefProperty>(d => d.LoanBriefPropertyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoanBriefProperty_LoanBrief");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.LoanBriefProperty)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_LoanBriefProperty_Product");
            });

            modelBuilder.Entity<LoanBriefQuestionScript>(entity =>
            {
                entity.HasIndex(e => e.LoanBriefId)
                    .HasName("NonClusteredIndex-20201008-175150")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AdvisorySettingGps).HasColumnName("AdvisorySettingGPS");

                entity.Property(e => e.ChooseCareer).HasDefaultValueSql("((0))");

                entity.Property(e => e.CommentJob).HasMaxLength(1024);

                entity.Property(e => e.CommentStatusTelesales).HasMaxLength(1024);

                entity.Property(e => e.DateModify).HasColumnType("datetime");

                entity.Property(e => e.DetailJob).HasMaxLength(250);

                entity.Property(e => e.DriverOfCompany).HasMaxLength(250);

                entity.Property(e => e.HourOpenDoor).HasMaxLength(250);

                entity.Property(e => e.LatestPayday).HasMaxLength(250);

                entity.Property(e => e.NameWeb).HasMaxLength(50);

                entity.Property(e => e.QuestionLicenseDkkd).HasColumnName("QuestionLicenseDKKD");

                entity.Property(e => e.SigningAlaborContract).HasColumnName("SigningALaborContract");

                entity.Property(e => e.StartWokingJob).HasMaxLength(250);

                entity.Property(e => e.TimeDriver).HasMaxLength(100);

                entity.Property(e => e.TimeSellWeb).HasMaxLength(250);

                entity.Property(e => e.TypeJob).HasDefaultValueSql("((0))");

                entity.Property(e => e.WaterSupplier).HasMaxLength(250);

                entity.Property(e => e.WorkingTime).HasMaxLength(250);

                entity.HasOne(d => d.LoanBrief)
                    .WithOne(p => p.LoanBriefQuestionScript)
                    .HasForeignKey<LoanBriefQuestionScript>(d => d.LoanBriefId)
                    .HasConstraintName("FK_LoanBriefQuestionScript_LoanBrief");
            });

            modelBuilder.Entity<LoanBriefRelationship>(entity =>
            {
                entity.HasIndex(e => new { e.RelationshipType, e.FullName, e.Phone, e.LoanBriefId })
                    .HasName("ix_LoanBriefRelationship");

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedFrom).HasMaxLength(50);

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.LoanBrief)
                    .WithMany(p => p.LoanBriefRelationship)
                    .HasForeignKey(d => d.LoanBriefId)
                    .HasConstraintName("FK_LoanBriefRelationship_LoanBrief");

                entity.HasOne(d => d.RelationshipTypeNavigation)
                    .WithMany(p => p.LoanBriefRelationship)
                    .HasForeignKey(d => d.RelationshipType)
                    .HasConstraintName("FK_LoanBriefRelationship_RelativeFamily");
            });

            modelBuilder.Entity<LoanBriefResident>(entity =>
            {
                entity.Property(e => e.LoanBriefResidentId).ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.AddressGoogleMap).HasMaxLength(500);

                entity.Property(e => e.AddressLatLng).HasMaxLength(100);

                entity.Property(e => e.AddressNationalCard).HasMaxLength(500);

                entity.Property(e => e.AppartmentNumber).HasMaxLength(50);

                entity.Property(e => e.BillElectricityId).HasMaxLength(100);

                entity.Property(e => e.BillWaterId).HasMaxLength(100);

                entity.Property(e => e.Block).HasMaxLength(50);

                entity.Property(e => e.CustomerShareLocation).HasMaxLength(500);

                entity.Property(e => e.Direction).HasMaxLength(500);

                entity.Property(e => e.Lane).HasMaxLength(50);

                entity.Property(e => e.PlateNumber).HasMaxLength(200);

                entity.Property(e => e.ResultLocation).HasMaxLength(200);

                entity.HasOne(d => d.District)
                    .WithMany(p => p.LoanBriefResident)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_LoanBriefResident_District");

                entity.HasOne(d => d.LoanBriefResidentNavigation)
                    .WithOne(p => p.LoanBriefResident)
                    .HasForeignKey<LoanBriefResident>(d => d.LoanBriefResidentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoanBriefResident_LoanBrief");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.LoanBriefResident)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_LoanBriefResident_Province");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.LoanBriefResident)
                    .HasForeignKey(d => d.WardId)
                    .HasConstraintName("FK_LoanBriefResident_Ward");
            });

            modelBuilder.Entity<LoanConfigStep>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.NextStep)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LoanProduct>(entity =>
            {
                entity.Property(e => e.MaximumMoney).HasColumnType("money");

                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<LoanRateType>(entity =>
            {
                entity.Property(e => e.LoanRateTypeId).ValueGeneratedNever();

                entity.Property(e => e.RateType).HasMaxLength(500);
            });

            modelBuilder.Entity<LoanStatus>(entity =>
            {
                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasMaxLength(200);
            });

            modelBuilder.Entity<LoanStatusDetail>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(1024);
            });

            modelBuilder.Entity<LoanbriefLender>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.LenderName).HasMaxLength(200);

                entity.Property(e => e.NationalCard).HasMaxLength(50);
            });

            modelBuilder.Entity<LoanbriefScript>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ScriptView)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.Property(e => e.City).HasMaxLength(500);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Device).HasMaxLength(50);

                entity.Property(e => e.Lat).HasMaxLength(50);

                entity.Property(e => e.Long).HasMaxLength(50);
            });

            modelBuilder.Entity<LogActionRestruct>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.FirstPayment)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LoanAmount).HasColumnType("money");
            });

            modelBuilder.Entity<LogBaoCaoTuDongDinhVi>(entity =>
            {
                entity.ToTable("LogBaoCaoTuDongDinhVi", "rp");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ZaloRecipientId).HasMaxLength(100);

                entity.Property(e => e.ZaloSenderId).HasMaxLength(100);
            });

            modelBuilder.Entity<LogCallApi>(entity =>
            {
                entity.ToTable("LogCallAPI");

                entity.HasIndex(e => new { e.LoanBriefId, e.ActionCallApi })
                    .HasName("ix_LogCallAPI");

                entity.Property(e => e.ActionCallApi).HasColumnName("ActionCallAPI");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.LinkCallApi)
                    .HasColumnName("LinkCallAPI")
                    .HasMaxLength(500);

                entity.Property(e => e.ModifyAt).HasColumnType("datetime");

                entity.Property(e => e.TokenCallApi).HasColumnName("TokenCallAPI");
            });

            modelBuilder.Entity<LogCallApi1>(entity =>
            {
                entity.ToTable("LogCallApi", "rp");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.InputStringCallApi).HasColumnType("ntext");

                entity.Property(e => e.ModifyOn).HasColumnType("datetime");

                entity.Property(e => e.NameActionCallApi).HasMaxLength(500);

                entity.Property(e => e.ResultStringCallApi).HasColumnType("ntext");
            });

            modelBuilder.Entity<LogCallApiCisco>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Extension)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LogCallApiPartner>(entity =>
            {
                entity.Property(e => e.CreateAt).HasColumnType("datetime");

                entity.Property(e => e.Link).HasMaxLength(1024);

                entity.Property(e => e.ModifyAt).HasColumnType("datetime");

                entity.Property(e => e.Token).HasMaxLength(1024);
            });

            modelBuilder.Entity<LogCancelLoanbrief>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ReasonComment).HasMaxLength(500);

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogChangePhone>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNew)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LogCiscoPushToTelesale>(entity =>
            {
                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogClickToCall>(entity =>
            {
                entity.Property(e => e.CallId).HasMaxLength(200);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.PhoneOfCustomer)
                    .HasMaxLength(13)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LogCloseLoan>(entity =>
            {
                entity.Property(e => e.CreateAt).HasColumnType("datetime");

                entity.Property(e => e.Link).HasMaxLength(1024);

                entity.Property(e => e.ModifyAt).HasColumnType("datetime");

                entity.Property(e => e.Token).HasMaxLength(1024);
            });

            modelBuilder.Entity<LogDistributingHubEmployee>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogDistributionUser>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogGeocodingResult>(entity =>
            {
                entity.ToTable("LogGeocodingResult", "rp");

                entity.Property(e => e.CityName).HasMaxLength(500);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DistrictName).HasMaxLength(500);

                entity.Property(e => e.FormattedAddress).HasMaxLength(1000);

                entity.Property(e => e.Lat).HasMaxLength(500);

                entity.Property(e => e.Lng).HasMaxLength(500);

                entity.Property(e => e.StreetName).HasMaxLength(1000);

                entity.Property(e => e.StreetNumber).HasMaxLength(100);

                entity.Property(e => e.WardName).HasMaxLength(500);
            });

            modelBuilder.Entity<LogInfoCreateLoanbrief>(entity =>
            {
                entity.Property(e => e.AffCode)
                    .HasColumnName("Aff_Code")
                    .HasMaxLength(1024);

                entity.Property(e => e.AffSid)
                    .HasColumnName("Aff_Sid")
                    .HasMaxLength(1024);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DomainName).HasMaxLength(1204);

                entity.Property(e => e.FullName).HasMaxLength(250);

                entity.Property(e => e.GoogleClickId).HasMaxLength(1024);

                entity.Property(e => e.IsVerifyOtpLdp).HasColumnName("IsVerifyOtpLDP");

                entity.Property(e => e.NationalCard)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Tid)
                    .HasColumnName("TId")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UtmCampaign).HasMaxLength(1204);

                entity.Property(e => e.UtmContent).HasMaxLength(1204);

                entity.Property(e => e.UtmMedium).HasMaxLength(1204);

                entity.Property(e => e.UtmSource).HasMaxLength(1204);

                entity.Property(e => e.UtmTerm).HasMaxLength(1204);
            });

            modelBuilder.Entity<LogLatLngHomeCompany>(entity =>
            {
                entity.ToTable("LogLatLngHomeCompany", "rp");

                entity.Property(e => e.CompanyLat).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyLng).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HomeLat).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HomeLng).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<LogLoanAction>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogLoanInfoAi>(entity =>
            {
                entity.ToTable("LogLoanInfoAI");

                entity.HasIndex(e => e.LoanbriefId)
                    .HasName("ix_LogLoanInfoAI_id");

                entity.HasIndex(e => new { e.ServiceType, e.IsExcuted, e.IsCancel })
                    .HasName("ix_LogLoanInfoAI32");

                entity.HasIndex(e => new { e.ServiceType, e.LoanbriefId, e.Id })
                    .HasName("ix_LogLoanInfoAI");

                entity.HasIndex(e => new { e.ServiceType, e.RefCode, e.HomeNetwok })
                    .HasName("ix_LogLoanInfoAI3");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.FromApp).HasMaxLength(200);

                entity.Property(e => e.HomeNetwok).HasMaxLength(50);

                entity.Property(e => e.IsCancel).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsExcuted).HasDefaultValueSql("((0))");

                entity.Property(e => e.RefCode).HasMaxLength(200);

                entity.Property(e => e.ResultFinal).HasMaxLength(50);

                entity.Property(e => e.Retry).HasColumnName("retry");

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(500);

                entity.HasOne(d => d.Loanbrief)
                    .WithMany(p => p.LogLoanInfoAi)
                    .HasForeignKey(d => d.LoanbriefId)
                    .HasConstraintName("FK_LogLoanInfoAI_LoanBrief");
            });

            modelBuilder.Entity<LogLoanInfoPartner>(entity =>
            {
                entity.Property(e => e.BillId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ExpiredDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerEmail)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerName).HasMaxLength(200);

                entity.Property(e => e.PartnerPersonalId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalAmount).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<LogLoanbriefScoring>(entity =>
            {
                entity.HasIndex(e => e.CityId)
                    .HasName("ix_CityId");

                entity.HasIndex(e => e.IsPushAutoCall)
                    .HasName("ix_IsPushAutoCall");

                entity.HasIndex(e => e.Phone)
                    .HasName("ix_phone");

                entity.HasIndex(e => e.RangeScore)
                    .HasName("ix_RangeScore");

                entity.HasIndex(e => new { e.IsPushAutoCall, e.Phone, e.LastLoanStatus, e.CityId })
                    .HasName("Idx_Rp2");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.FirstLoanDate).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.LastLoanDate).HasColumnType("datetime");

                entity.Property(e => e.LastUpdate).HasColumnType("datetime");

                entity.Property(e => e.LoanbriefCreatedAt).HasColumnType("datetime");

                entity.Property(e => e.NationalCard)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PushAutoCallAt).HasColumnType("datetime");

                entity.Property(e => e.RangeScore).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<LogPartner>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Provider)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RequestUrl).HasMaxLength(500);

                entity.Property(e => e.TransactionType)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LogPushNotification>(entity =>
            {
                entity.Property(e => e.MessagePush).HasMaxLength(500);

                entity.Property(e => e.PushTo).HasMaxLength(50);

                entity.Property(e => e.RequestAt).HasColumnType("datetime");

                entity.Property(e => e.RequestToken).HasMaxLength(500);

                entity.Property(e => e.ResponseAt).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(500);
            });

            modelBuilder.Entity<LogPushToCisco>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.Phone)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LogPushToHub>(entity =>
            {
                entity.HasIndex(e => e.CreatedAt)
                    .HasName("_dta_index_LogPushToHub_5_338100245__K9_1040");

                entity.HasIndex(e => e.Id)
                    .HasName("_dta_index_LogPushToHub_5_338100245__K1_1912");

                entity.HasIndex(e => e.LoanbriefId)
                    .HasName("_dta_index_LogPushToHub_5_338100245__K2_4364");

                entity.HasIndex(e => new { e.CreatedAt, e.Id })
                    .HasName("_dta_stat_338100245_9_1");

                entity.HasIndex(e => new { e.CreatedAt, e.LoanbriefId })
                    .HasName("ix_LogPushToHub");

                entity.HasIndex(e => new { e.Id, e.LoanbriefId })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K1_K2_9987");

                entity.HasIndex(e => new { e.LoanbriefId, e.CreatedAt })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K2_K9_9085");

                entity.HasIndex(e => new { e.LoanbriefId, e.Id })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K2_K1_6497");

                entity.HasIndex(e => new { e.TelesaleId, e.LoanbriefId })
                    .HasName("_dta_stat_338100245_3_2");

                entity.HasIndex(e => new { e.CreatedAt, e.Id, e.HubId })
                    .HasName("_dta_stat_338100245_9_1_8");

                entity.HasIndex(e => new { e.HubId, e.LoanbriefId, e.Id })
                    .HasName("_dta_stat_338100245_8_2_1");

                entity.HasIndex(e => new { e.Id, e.CreatedAt, e.LoanbriefId })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K1_K9_K2_1410");

                entity.HasIndex(e => new { e.Id, e.LoanbriefId, e.TelesaleId })
                    .HasName("_dta_stat_338100245_1_2_3");

                entity.HasIndex(e => new { e.LoanbriefId, e.CreatedAt, e.TelesaleId })
                    .HasName("_dta_stat_338100245_2_9_3");

                entity.HasIndex(e => new { e.LoanbriefId, e.Id, e.CreatedAt })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K2_K1_K9_8258");

                entity.HasIndex(e => new { e.TelesaleId, e.Id, e.CreatedAt })
                    .HasName("_dta_stat_338100245_3_1_9");

                entity.HasIndex(e => new { e.CreatedAt, e.Id, e.HubId, e.LoanbriefId })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K9_K1_K8_K2");

                entity.HasIndex(e => new { e.CreatedAt, e.Id, e.TelesaleId, e.LoanbriefId })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K9_K1_K3_K2_4864");

                entity.HasIndex(e => new { e.CreatedAt, e.LoanbriefId, e.HubId, e.Id })
                    .HasName("_dta_stat_338100245_9_2_8_1");

                entity.HasIndex(e => new { e.CreatedAt, e.LoanbriefId, e.TelesaleId, e.Id })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K9_K2_K3_K1_4149");

                entity.HasIndex(e => new { e.HubId, e.LoanbriefId, e.CreatedAt, e.Id })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K8_K2_K9_K1");

                entity.HasIndex(e => new { e.HubId, e.LoanbriefId, e.Id, e.CreatedAt })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K8_K2_K1_K9");

                entity.HasIndex(e => new { e.Id, e.CreatedAt, e.LoanbriefId, e.HubId })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K1_K9_K2_K8");

                entity.HasIndex(e => new { e.Id, e.CreatedAt, e.LoanbriefId, e.TelesaleId })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K1_K9_K2_K3_6960");

                entity.HasIndex(e => new { e.Id, e.LoanbriefId, e.HubId, e.CreatedAt })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K1_K2_K8_K9");

                entity.HasIndex(e => new { e.Id, e.LoanbriefId, e.TelesaleId, e.CreatedAt })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K1_K2_K3_K9_9850");

                entity.HasIndex(e => new { e.LoanbriefId, e.CreatedAt, e.HubId, e.Id })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K2_K9_K8_K1");

                entity.HasIndex(e => new { e.LoanbriefId, e.CreatedAt, e.TelesaleId, e.Id })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K2_K9_K3_K1_1771");

                entity.HasIndex(e => new { e.LoanbriefId, e.Id, e.CreatedAt, e.HubId })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K2_K1_K9_K8");

                entity.HasIndex(e => new { e.LoanbriefId, e.Id, e.CreatedAt, e.TelesaleId })
                    .HasName("_dta_stat_338100245_2_1_9_3");

                entity.HasIndex(e => new { e.TelesaleId, e.Id, e.CreatedAt, e.LoanbriefId })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K3_K1_K9_K2");

                entity.HasIndex(e => new { e.TelesaleId, e.LoanbriefId, e.CreatedAt, e.Id })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K3_K2_K9_K1_8066");

                entity.HasIndex(e => new { e.TelesaleId, e.LoanbriefId, e.Id, e.CreatedAt })
                    .HasName("_dta_index_LogPushToHub_5_338100245__K3_K2_K1_K9_2533");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogPushToPartner>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Partner)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerCustomerId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogReLoanbrief>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.NationalCard)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ResultMessage).HasMaxLength(500);

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.UtmSource).HasMaxLength(500);
            });

            modelBuilder.Entity<LogRequestAi>(entity =>
            {
                entity.ToTable("LogRequestAI");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogResultAutoCall>(entity =>
            {
                entity.HasIndex(e => e.AreaSupport)
                    .HasName("NonClusteredIndex-20201015-141909");

                entity.HasIndex(e => e.BorrowMotobike)
                    .HasName("NonClusteredIndex-20201015-141839");

                entity.HasIndex(e => e.LoanbriefId)
                    .HasName("NonClusteredIndex-20201015-142026");

                entity.HasIndex(e => e.MotobikeCertificate)
                    .HasName("NonClusteredIndex-20201015-141924");

                entity.HasIndex(e => new { e.BorrowMotobike, e.MotobikeCertificate, e.AreaSupport })
                    .HasName("NonClusteredIndex-20201015-141953");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Numbercall).HasColumnName("numbercall");

                entity.Property(e => e.PhoneAgent)
                    .HasColumnName("phoneAgent")
                    .HasMaxLength(15);

                entity.Property(e => e.SendSms).HasColumnName("SendSMS");

                entity.Property(e => e.TypeCall).HasColumnName("type_call");

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogScheduleJob>(entity =>
            {
                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<LogSecuredTransaction>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.LoanAmount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.NoticeNumber)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Pin)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LogSendOtp>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ExpiredToken).HasColumnType("datetime");

                entity.Property(e => e.ExpiredVerifyOtp).HasColumnType("datetime");

                entity.Property(e => e.IpAddress)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyAt).HasColumnType("datetime");

                entity.Property(e => e.Otp)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Request).HasMaxLength(500);

                entity.Property(e => e.Response).HasMaxLength(500);

                entity.Property(e => e.TokenGennerate).IsUnicode(false);

                entity.Property(e => e.Url).HasMaxLength(200);

                entity.Property(e => e.VerifyAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogSendSms>(entity =>
            {
                entity.ToTable("LogSendSMS");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Smscontent)
                    .HasColumnName("SMSContent")
                    .HasMaxLength(500);

                entity.Property(e => e.TypeSendSms).HasColumnName("TypeSendSMS");

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(200);
            });

            modelBuilder.Entity<LogSendVoiceOtp>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Domain).HasMaxLength(250);

                entity.Property(e => e.Otp)
                    .HasColumnName("OTP")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Response).HasMaxLength(1024);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Token).HasMaxLength(250);

                entity.Property(e => e.Url).HasMaxLength(250);
            });

            modelBuilder.Entity<LogTrackingStopPoin>(entity =>
            {
                entity.ToTable("LogTrackingStopPoin", "rp");

                entity.Property(e => e.CityName).HasMaxLength(1000);

                entity.Property(e => e.DistrictName).HasMaxLength(1000);

                entity.Property(e => e.FormatedAddress).HasMaxLength(1000);

                entity.Property(e => e.FromTime).HasColumnType("datetime");

                entity.Property(e => e.PoinLng).HasColumnType("decimal(18, 9)");

                entity.Property(e => e.PointLat).HasColumnType("decimal(18, 9)");

                entity.Property(e => e.StreetName).HasMaxLength(1000);

                entity.Property(e => e.StreetNumber).HasMaxLength(1000);

                entity.Property(e => e.TotalTime).HasComment("tổng thời gian dừng: phút");

                entity.Property(e => e.TrackingType).HasComment("1: điểm dừng, 2 : di chuyển");

                entity.Property(e => e.WardName).HasMaxLength(1000);
            });

            modelBuilder.Entity<LogWorking>(entity =>
            {
                entity.Property(e => e.CreateAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<Logconnecttion>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("logconnecttion");

                entity.Property(e => e.ClientNetAddress)
                    .HasColumnName("client_net_address")
                    .HasMaxLength(200);

                entity.Property(e => e.Sl).HasColumnName("sl");

                entity.Property(e => e.Stt).HasColumnName("stt");

                entity.Property(e => e.Time)
                    .HasColumnName("time")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<ManagerAreaHub>(entity =>
            {
                entity.HasKey(e => new { e.ShopId, e.DistrictId, e.WardId, e.ProductId });

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Shop)
                    .WithMany(p => p.ManagerAreaHub)
                    .HasForeignKey(d => d.ShopId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ManagerAreaHub_Shop");
            });

            modelBuilder.Entity<MapAutocall>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("map_autocall");

                entity.Property(e => e.Cammame)
                    .HasColumnName("cammame")
                    .HasMaxLength(100);

                entity.Property(e => e.Campaignid).HasColumnName("campaignid");

                entity.Property(e => e.Triggerid).HasColumnName("triggerid");
            });

            modelBuilder.Entity<Material>(entity =>
            {
                entity.ToTable("Material", "rp");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.MaterialCode).HasMaxLength(100);

                entity.Property(e => e.MaterialName).HasMaxLength(500);
            });

            modelBuilder.Entity<Module>(entity =>
            {
                entity.Property(e => e.Action)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Controller)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.Icon).HasMaxLength(200);

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Path)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_Module_Module");
            });

            modelBuilder.Entity<ModulePermission>(entity =>
            {
                entity.HasKey(e => new { e.ModuleId, e.PermissionId })
                    .HasName("PK_ModulePermission_1");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.ModulePermission)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ModulePermission_Permission");
            });

            modelBuilder.Entity<NoteLog>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("note_log");

                entity.Property(e => e.Bank)
                    .HasColumnName("bank")
                    .HasMaxLength(10);

                entity.Property(e => e.Maloi)
                    .HasColumnName("maloi")
                    .HasMaxLength(10);

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<Notebanklog>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("notebanklog");

                entity.Property(e => e.Bank)
                    .HasColumnName("bank")
                    .HasMaxLength(100);

                entity.Property(e => e.Frombank)
                    .HasColumnName("frombank")
                    .HasMaxLength(100);

                entity.Property(e => e.Loanbriefid).HasColumnName("loanbriefid");

                entity.Property(e => e.Loi)
                    .HasColumnName("loi")
                    .HasMaxLength(100);

                entity.Property(e => e.Sotk)
                    .HasColumnName("sotk")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.Property(e => e.Action)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Message).HasMaxLength(500);

                entity.Property(e => e.PushTime).HasColumnType("datetime");

                entity.Property(e => e.Read).IsUnicode(false);

                entity.Property(e => e.Topic)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<OrderSource>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<OrderSourceDetail>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<PermissionIp>(entity =>
            {
                entity.ToTable("PermissionIP");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IpAddress).HasMaxLength(50);

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.ServerName).HasMaxLength(50);
            });

            modelBuilder.Entity<Pipeline>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Note).HasMaxLength(500);
            });

            modelBuilder.Entity<PipelineHistory>(entity =>
            {
                entity.HasIndex(e => e.LoanBriefId)
                    .HasName("ix_PipelineHistory_id");

                entity.HasIndex(e => new { e.LoanBriefId, e.EndStatus })
                    .HasName("ix_PipelineHistory_idstatus");

                entity.Property(e => e.LenderName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PipelineState>(entity =>
            {
                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasMaxLength(200);
            });

            modelBuilder.Entity<PlatformType>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.IsEnable).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.ShortName).HasMaxLength(500);

                entity.Property(e => e.UpdateTime).HasColumnType("datetime");

                entity.HasOne(d => d.BrandProduct)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.BrandProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_BrandProduct");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductType");
            });

            modelBuilder.Entity<ProductAppraiser>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Latitude).HasColumnType("decimal(18, 9)");

                entity.Property(e => e.Longitude).HasColumnType("decimal(18, 9)");
            });

            modelBuilder.Entity<ProductPercentReduction>(entity =>
            {
                entity.HasOne(d => d.BrandProduct)
                    .WithMany(p => p.ProductPercentReduction)
                    .HasForeignKey(d => d.BrandProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductPercentReduction_BrandProduct");
            });

            modelBuilder.Entity<ProductReview>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<ProductReviewDetail>(entity =>
            {
                entity.Property(e => e.PecentDiscount).HasColumnType("numeric(2, 1)");

                entity.HasOne(d => d.ProductReview)
                    .WithMany(p => p.ProductReviewDetail)
                    .HasForeignKey(d => d.ProductReviewId)
                    .HasConstraintName("FK_ProductReviewDetail_ProductReview");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.ProductReviewDetail)
                    .HasForeignKey(d => d.ProductTypeId)
                    .HasConstraintName("FK_ProductReviewDetail_ProductType");
            });

            modelBuilder.Entity<ProductReviewResult>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductReviewResult)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_ProductReviewResult_Product");

                entity.HasOne(d => d.ProductReview)
                    .WithMany(p => p.ProductReviewResult)
                    .HasForeignKey(d => d.ProductReviewId)
                    .HasConstraintName("FK_ProductReviewResult_ProductReview1");
            });

            modelBuilder.Entity<ProductReviewResultDetail>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ProductType>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<Property>(entity =>
            {
                entity.Property(e => e.DefaultValue).HasMaxLength(500);

                entity.Property(e => e.FieldName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LinkObject)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MultipleSelect).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Object)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Regex).HasMaxLength(500);

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProposeExceptions>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ChildDocName).HasMaxLength(250);

                entity.Property(e => e.CreateAt).HasColumnType("datetime");

                entity.Property(e => e.DocName).HasMaxLength(250);

                entity.Property(e => e.Money).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<Province>(entity =>
            {
                entity.Property(e => e.MapTransaction)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<PushLoanToPartner>(entity =>
            {
                entity.Property(e => e.AffCode).HasMaxLength(250);

                entity.Property(e => e.AffSid)
                    .HasColumnName("AffSId")
                    .HasMaxLength(250);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerName).HasMaxLength(250);

                entity.Property(e => e.CustomerPhone).HasMaxLength(250);

                entity.Property(e => e.DistrictName).HasMaxLength(100);

                entity.Property(e => e.Domain).HasMaxLength(250);

                entity.Property(e => e.Email)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceName).HasMaxLength(100);

                entity.Property(e => e.PushDate).HasColumnType("datetime");

                entity.Property(e => e.SynchronizedDate).HasColumnType("datetime");

                entity.Property(e => e.Tid)
                    .HasColumnName("TId")
                    .HasMaxLength(250);

                entity.Property(e => e.UtmCampaign).HasMaxLength(500);

                entity.Property(e => e.UtmContent).HasMaxLength(500);

                entity.Property(e => e.UtmMedium).HasMaxLength(500);

                entity.Property(e => e.UtmSource).HasMaxLength(500);

                entity.Property(e => e.UtmTerm).HasMaxLength(500);
            });

            modelBuilder.Entity<QueueLog>(entity =>
            {
                entity.Property(e => e.QueueLogId).ValueGeneratedNever();

                entity.Property(e => e.Hash)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Message).HasMaxLength(500);
            });

            modelBuilder.Entity<ReasonCancel>(entity =>
            {
                entity.HasIndex(e => e.Id)
                    .HasName("_dta_index_ReasonCancel_5_1671677003__K1_9910");

                entity.HasIndex(e => new { e.Reason, e.Id })
                    .HasName("_dta_index_ReasonCancel_5_1671677003__K1_2_1973");

                entity.HasIndex(e => new { e.Reason, e.Sort })
                    .HasName("_dta_index_ReasonCancel_5_1671677003__K6_2_3369");

                entity.HasIndex(e => new { e.Sort, e.Id })
                    .HasName("_dta_stat_1671677003_6_1");

                entity.HasIndex(e => new { e.Reason, e.Sort, e.Id })
                    .HasName("_dta_index_ReasonCancel_5_1671677003__K6_K1_2_114");

                entity.Property(e => e.Reason).HasColumnType("nvarchar(max)");

                entity.Property(e => e.ReasonCode).HasMaxLength(50);
            });

            modelBuilder.Entity<ReasonCoordinator>(entity =>
            {
                entity.Property(e => e.CreateAt).HasColumnType("datetime");

                entity.Property(e => e.Reason).HasMaxLength(500);
            });

            modelBuilder.Entity<RelativeFamily>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<ReportAutocall>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("REPORT_AUTOCALL");

                entity.Property(e => e.Noidung)
                    .HasColumnName("NOIDUNG")
                    .HasMaxLength(200);

                entity.Property(e => e.Sl).HasColumnName("SL");

                entity.Property(e => e._).HasColumnName("%");
            });

            modelBuilder.Entity<ReportBehaviorHub>(entity =>
            {
                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ShopName).HasMaxLength(255);
            });

            modelBuilder.Entity<ReportBehaviorTelesale>(entity =>
            {
                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(255);

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<ReportConvertHub>(entity =>
            {
                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ShopName).HasMaxLength(255);
            });

            modelBuilder.Entity<ReportConvertTelesale>(entity =>
            {
                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(255);

                entity.Property(e => e.LastUpdate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ReportData>(entity =>
            {
                entity.ToTable("ReportData", "rp");

                entity.HasComment("Đơn về là vào đây, chỉ insert ko update");

                entity.Property(e => e.BeforeCreateDate)
                    .HasColumnName("beforeCreateDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.BeforeLoanCreditId).HasColumnName("beforeLoanCreditId");

                entity.Property(e => e.BeforeStatus).HasColumnName("beforeStatus");

                entity.Property(e => e.BeforeUtmCampaign)
                    .HasColumnName("before_utm_campaign")
                    .HasMaxLength(500);

                entity.Property(e => e.BeforeUtmContent)
                    .HasColumnName("before_utm_content")
                    .HasMaxLength(500);

                entity.Property(e => e.BeforeUtmMedium)
                    .HasColumnName("before_utm_medium")
                    .HasMaxLength(500);

                entity.Property(e => e.BeforeUtmSource)
                    .HasColumnName("before_utm_source")
                    .HasMaxLength(500)
                    .HasComment("Before: dữ liệu của khách hàng theo số đt này của đơn về xa nhất cách đơn hiện tại 30 ngày");

                entity.Property(e => e.BeforeUtmTerm)
                    .HasColumnName("before_utm_term")
                    .HasMaxLength(500);

                entity.Property(e => e.CityId).HasColumnName("cityId");

                entity.Property(e => e.CityName)
                    .HasColumnName("cityName")
                    .HasMaxLength(500);

                entity.Property(e => e.CompanyAddress)
                    .HasColumnName("companyAddress")
                    .HasMaxLength(1000);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustomerCreditId).HasColumnName("customerCreditId");

                entity.Property(e => e.CustomerFullName)
                    .HasColumnName("customerFullName")
                    .HasMaxLength(500);

                entity.Property(e => e.DistrictId).HasColumnName("districtId");

                entity.Property(e => e.DistrictName)
                    .HasColumnName("districtName")
                    .HasMaxLength(500);

                entity.Property(e => e.DomainName)
                    .HasColumnName("domainName")
                    .HasMaxLength(500);

                entity.Property(e => e.FromDate)
                    .HasColumnName("fromDate")
                    .HasColumnType("date");

                entity.Property(e => e.HomeAddress)
                    .HasColumnName("homeAddress")
                    .HasMaxLength(1000);

                entity.Property(e => e.IdCard)
                    .HasColumnName("idCard")
                    .HasMaxLength(100);

                entity.Property(e => e.IsHeadOffice)
                    .HasColumnName("isHeadOffice")
                    .HasDefaultValueSql("((1))")
                    .HasComment("Đơn này có phải của Hội Sở hay không. 0 là không phải, 1 là phải");

                entity.Property(e => e.IsLocate)
                    .HasColumnName("isLocate")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LoanBriefId).HasColumnName("loanBriefId");

                entity.Property(e => e.LoanTime).HasColumnName("loanTime");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.PlatformType).HasColumnName("platformType");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ProductName)
                    .HasColumnName("productName")
                    .HasMaxLength(500)
                    .HasComment("tên gói sản phẩm lấy theo typeId của bảng tblLoancredit");

                entity.Property(e => e.ReMarketing)
                    .HasColumnName("reMarketing")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ReMarketingLoanCreditId)
                    .HasColumnName("reMarketingLoanCreditId")
                    .HasComment("Đơn vay được Remarketing từ đơn nào");

                entity.Property(e => e.ShopId).HasColumnName("shopId");

                entity.Property(e => e.ShopName)
                    .HasColumnName("shopName")
                    .HasMaxLength(500);

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.SupportId).HasColumnName("supportID");

                entity.Property(e => e.SupportLastFullName)
                    .HasColumnName("supportLastFullName")
                    .HasMaxLength(250);

                entity.Property(e => e.SupportLastId).HasColumnName("supportLastID");

                entity.Property(e => e.Tid)
                    .HasColumnName("tid")
                    .HasMaxLength(250);

                entity.Property(e => e.TotalMoney).HasColumnName("totalMoney");

                entity.Property(e => e.TypeProduct)
                    .HasColumnName("typeProduct")
                    .HasComment("Loại gói sản phẩm cao hay thấp");

                entity.Property(e => e.UtmCampaign)
                    .HasColumnName("utm_campaign")
                    .HasMaxLength(500);

                entity.Property(e => e.UtmContent)
                    .HasColumnName("utm_content")
                    .HasMaxLength(500);

                entity.Property(e => e.UtmMedium)
                    .HasColumnName("utm_medium")
                    .HasMaxLength(500);

                entity.Property(e => e.UtmSource)
                    .HasColumnName("utm_source")
                    .HasMaxLength(500);

                entity.Property(e => e.UtmTerm)
                    .HasColumnName("utm_term")
                    .HasMaxLength(500);

                entity.Property(e => e.WardId).HasColumnName("wardId");
            });

            modelBuilder.Entity<ReportDataUpdating>(entity =>
            {
                entity.ToTable("ReportDataUpdating", "rp");

                entity.HasComment("báo cáo thời gian xử lý của các phòng ban, update liên tục 30p 1 lần những đơn chưa hủy hoặc chưa tất toán");

                entity.Property(e => e.CancelDateLoanCredit)
                    .HasColumnName("cancelDateLoanCredit")
                    .HasColumnType("datetime");

                entity.Property(e => e.CancelFullName)
                    .HasColumnName("cancelFullName")
                    .HasMaxLength(250);

                entity.Property(e => e.CancelGroupId).HasColumnName("cancelGroupId");

                entity.Property(e => e.CityId).HasColumnName("cityId");

                entity.Property(e => e.CompanyAddress)
                    .HasColumnName("companyAddress")
                    .HasMaxLength(1000);

                entity.Property(e => e.CoordinatorId).HasColumnName("coordinatorId");

                entity.Property(e => e.CounselorFullName)
                    .HasColumnName("counselorFullName")
                    .HasMaxLength(250);

                entity.Property(e => e.CounselorId).HasColumnName("counselorId");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeviceId)
                    .HasColumnName("deviceId")
                    .HasMaxLength(250);

                entity.Property(e => e.DisbursementDate)
                    .HasColumnName("disbursementDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.DistrictId).HasColumnName("districtId");

                entity.Property(e => e.HomeAddress)
                    .HasColumnName("homeAddress")
                    .HasMaxLength(1000);

                entity.Property(e => e.HubEmployeFullName)
                    .HasColumnName("hubEmployeFullName")
                    .HasMaxLength(250);

                entity.Property(e => e.HubEmployeId)
                    .HasColumnName("hubEmployeId")
                    .HasComment("Nhân viên cửa hàng phụ trách khoản vay");

                entity.Property(e => e.HubPushDate)
                    .HasColumnName("hubPushDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsFirstLoan).HasColumnName("isFirstLoan");

                entity.Property(e => e.IsQlf).HasColumnName("isQlf");

                entity.Property(e => e.IsVerifyOtpLdp).HasColumnName("isVerifyOtpLDP");

                entity.Property(e => e.ListCheckLeadQualify)
                    .HasColumnName("listCheckLeadQualify")
                    .HasMaxLength(250);

                entity.Property(e => e.LoanBriefId).HasColumnName("loanBriefId");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ProductName)
                    .HasColumnName("productName")
                    .HasMaxLength(50)
                    .HasComment("tên gói sản phẩm lấy theo typeId của bảng tblLoancredit");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.StatusOfDeviceId).HasColumnName("statusOfDeviceId");

                entity.Property(e => e.SupportId).HasColumnName("supportId");

                entity.Property(e => e.SupportLastFullName)
                    .HasColumnName("supportLastFullName")
                    .HasMaxLength(250);

                entity.Property(e => e.SupportLastId).HasColumnName("supportLastId");

                entity.Property(e => e.TdhsPushDate)
                    .HasColumnName("tdhsPushDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.TelesalePushDate)
                    .HasColumnName("telesalePushDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.TotalMinute).HasColumnName("totalMinute");

                entity.Property(e => e.TotalMinuteOfHub).HasColumnName("totalMinuteOfHub");

                entity.Property(e => e.TotalMinuteOfLander).HasColumnName("totalMinuteOfLander");

                entity.Property(e => e.TotalMinuteOfOther).HasColumnName("totalMinuteOfOther");

                entity.Property(e => e.TotalMinuteOfTdhs).HasColumnName("totalMinuteOfTdhs");

                entity.Property(e => e.TotalMinuteOfTelesale).HasColumnName("totalMinuteOfTelesale");

                entity.Property(e => e.TotalMinuteSubtracted).HasColumnName("totalMinuteSubtracted");

                entity.Property(e => e.TotalMinuteSubtractedOfHub).HasColumnName("totalMinuteSubtractedOfHub");

                entity.Property(e => e.TotalMinuteSubtractedOfLander).HasColumnName("totalMinuteSubtractedOfLander");

                entity.Property(e => e.TotalMinuteSubtractedOfOther).HasColumnName("totalMinuteSubtractedOfOther");

                entity.Property(e => e.TotalMinuteSubtractedOfTdhs).HasColumnName("totalMinuteSubtractedOfTdhs");

                entity.Property(e => e.TotalMinuteSubtractedOfTelesale).HasColumnName("totalMinuteSubtractedOfTelesale");

                entity.Property(e => e.TotalTimesReturnOfHub).HasColumnName("totalTimesReturnOfHub");

                entity.Property(e => e.TotalTimesReturnOfLander).HasColumnName("totalTimesReturnOfLander");

                entity.Property(e => e.TotalTimesReturnOfOther).HasColumnName("totalTimesReturnOfOther");

                entity.Property(e => e.TotalTimesReturnOfTdhs).HasColumnName("totalTimesReturnOfTdhs");

                entity.Property(e => e.TotalTimesReturnOfTelesale).HasColumnName("totalTimesReturnOfTelesale");

                entity.Property(e => e.UserIdCancel).HasColumnName("userIdCancel");

                entity.Property(e => e.WardId).HasColumnName("wardId");
            });

            modelBuilder.Entity<ReportDepartmentError>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.GroupUserError).HasComment("bộ phận dính lỗi");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.StaffErrorId).HasComment("UserId người mắc lỗi");

                entity.Property(e => e.StaffName)
                    .HasMaxLength(50)
                    .HasComment("Tên người mắc lỗi");

                entity.Property(e => e.UserName).HasMaxLength(50);
            });

            modelBuilder.Entity<ReportToken>(entity =>
            {
                entity.HasKey(e => e.TokenId);

                entity.ToTable("ReportToken", "rp");

                entity.Property(e => e.TokenId).HasColumnName("tokenId");

                entity.Property(e => e.ApplyFor)
                    .HasColumnName("applyFor")
                    .HasComment("cấp quyền cho báo cáo nào");

                entity.Property(e => e.DateEx)
                    .HasColumnName("dateEx")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(1000);

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.Token)
                    .HasColumnName("token")
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<RequestEvent>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RequestAt).HasColumnType("datetime");

                entity.Property(e => e.ResponseAt).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(500);
            });

            modelBuilder.Entity<ResultEkyc>(entity =>
            {
                entity.Property(e => e.AddressCheck).HasColumnName("Address_Check");

                entity.Property(e => e.AddressValue)
                    .HasColumnName("Address_Value")
                    .HasMaxLength(200);

                entity.Property(e => e.BirthdayCheck).HasColumnName("Birthday_Check");

                entity.Property(e => e.BirthdayValue)
                    .HasColumnName("Birthday_Value")
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.EthnicityCheck).HasColumnName("Ethnicity_Check");

                entity.Property(e => e.EthnicityValue)
                    .HasColumnName("Ethnicity_Value")
                    .HasMaxLength(50);

                entity.Property(e => e.ExpiryCheck).HasColumnName("Expiry_Check");

                entity.Property(e => e.ExpiryValue)
                    .HasColumnName("Expiry_Value")
                    .HasMaxLength(50);

                entity.Property(e => e.FaceCompareCode)
                    .HasColumnName("FaceCompare_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.FaceCompareMessage)
                    .HasColumnName("FaceCompare_Message")
                    .HasMaxLength(200);

                entity.Property(e => e.FaceQueryCode)
                    .HasColumnName("FaceQuery_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.FaceQueryMessage)
                    .HasColumnName("FaceQuery_Message")
                    .HasMaxLength(200);

                entity.Property(e => e.FullNameCheck).HasColumnName("FullName_Check");

                entity.Property(e => e.FullNameValue)
                    .HasColumnName("FullName_Value")
                    .HasMaxLength(50);

                entity.Property(e => e.GenderCheck).HasColumnName("Gender_Check");

                entity.Property(e => e.GenderValue)
                    .HasColumnName("Gender_Value")
                    .HasMaxLength(50);

                entity.Property(e => e.IdCheck).HasColumnName("Id_Check");

                entity.Property(e => e.IdValue)
                    .HasColumnName("Id_Value")
                    .HasMaxLength(50);

                entity.Property(e => e.IssueByCheck).HasColumnName("IssueBy_Check");

                entity.Property(e => e.IssueByValue)
                    .HasColumnName("IssueBy_Value")
                    .HasMaxLength(50);

                entity.Property(e => e.IssueDateCheck).HasColumnName("IssueDate_Check");

                entity.Property(e => e.IssueDateValue)
                    .HasColumnName("IssueDate_Value")
                    .HasMaxLength(50);

                entity.Property(e => e.ReligionCheck).HasColumnName("Religion_Check");

                entity.Property(e => e.ReligionValue)
                    .HasColumnName("Religion_Value")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<RuleCheckLoan>(entity =>
            {
                entity.HasKey(e => e.LoanbriefId);

                entity.Property(e => e.LoanbriefId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsCheckMomo).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsCheckPayment).HasDefaultValueSql("((1))");

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<ScheduleTime>(entity =>
            {
                entity.HasKey(e => e.ScheduleId);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.CusName).HasMaxLength(200);

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(300);

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.UpdateAt).HasColumnType("datetime");

                entity.Property(e => e.UserFullName).HasMaxLength(200);
            });

            modelBuilder.Entity<Section>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(200);

                entity.HasOne(d => d.Pipeline)
                    .WithMany(p => p.Section)
                    .HasForeignKey(d => d.PipelineId)
                    .HasConstraintName("FK_Section_Pipeline");
            });

            modelBuilder.Entity<SectionAction>(entity =>
            {
                entity.Property(e => e.Value).HasMaxLength(500);

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.SectionAction)
                    .HasForeignKey(d => d.PropertyId)
                    .HasConstraintName("FK_SectionAction_Property");

                entity.HasOne(d => d.SectionDetail)
                    .WithMany(p => p.SectionAction)
                    .HasForeignKey(d => d.SectionDetailId)
                    .HasConstraintName("FK_SectionAction_SectionDetail");
            });

            modelBuilder.Entity<SectionApprove>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(200);

                entity.HasOne(d => d.Approve)
                    .WithMany(p => p.SectionApproveApprove)
                    .HasForeignKey(d => d.ApproveId)
                    .HasConstraintName("FK_SectionApprove_SectionDetail1");

                entity.HasOne(d => d.SectionDetail)
                    .WithMany(p => p.SectionApproveSectionDetail)
                    .HasForeignKey(d => d.SectionDetailId)
                    .HasConstraintName("FK_SectionApprove_SectionDetail");
            });

            modelBuilder.Entity<SectionCondition>(entity =>
            {
                entity.Property(e => e.Operator)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value).HasMaxLength(500);

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.SectionCondition)
                    .HasForeignKey(d => d.PropertyId)
                    .HasConstraintName("FK_SectionCondition_Property");

                entity.HasOne(d => d.SectionApprove)
                    .WithMany(p => p.SectionCondition)
                    .HasForeignKey(d => d.SectionApproveId)
                    .HasConstraintName("FK_SectionCondition_SectionApprove");
            });

            modelBuilder.Entity<SectionDetail>(entity =>
            {
                entity.Property(e => e.Mode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.HasOne(d => d.Disapprove)
                    .WithMany(p => p.InverseDisapprove)
                    .HasForeignKey(d => d.DisapproveId)
                    .HasConstraintName("FK_SectionDetail_SectionDetail1");

                entity.HasOne(d => d.Return)
                    .WithMany(p => p.InverseReturn)
                    .HasForeignKey(d => d.ReturnId)
                    .HasConstraintName("FK_SectionDetail_SectionDetail");

                entity.HasOne(d => d.Section)
                    .WithMany(p => p.SectionDetail)
                    .HasForeignKey(d => d.SectionId)
                    .HasConstraintName("FK_SectionDetail_Section");
            });

            modelBuilder.Entity<SectionGroup>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(200);

                entity.HasOne(d => d.InitialSection)
                    .WithMany(p => p.SectionGroup)
                    .HasForeignKey(d => d.InitialSectionId)
                    .HasConstraintName("FK_SectionGroup_Section");
            });

            modelBuilder.Entity<Shop>(entity =>
            {
                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email).HasMaxLength(250);

                entity.Property(e => e.InviteCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Lat)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Long)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Note).HasMaxLength(1000);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.WebhookSlack).HasMaxLength(500);
            });

            modelBuilder.Entity<StatusTelesales>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(1024);
            });

            modelBuilder.Entity<SystemConfig>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value).HasMaxLength(500);
            });

            modelBuilder.Entity<TeamTelesales>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(250);
            });

            modelBuilder.Entity<TelesaleLoanbrief>(entity =>
            {
                entity.HasIndex(e => new { e.CreatedAt, e.LoanBriefId })
                    .HasName("ix_TelesaleLoanbrief");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<TelesalesShift>(entity =>
            {
                entity.Property(e => e.TelesalesShiftId).ValueGeneratedNever();

                entity.Property(e => e.EndTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Shift).HasMaxLength(50);

                entity.Property(e => e.StartTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Time).HasMaxLength(100);
            });

            modelBuilder.Entity<TlsHdTlsNghiviec>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("TLS_HD_tlsNghiviec");

                entity.Property(e => e.Loanbriefid).HasColumnName("loanbriefid");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TokenSmartDailer>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.DateExpiration).HasColumnType("datetime");

                entity.Property(e => e.Token)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Transactions>(entity =>
            {
                entity.Property(e => e.BankCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BankName).HasMaxLength(1024);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LenderPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Message).HasMaxLength(1024);

                entity.Property(e => e.ProcessByName).HasMaxLength(500);

                entity.Property(e => e.ProcessByUserNote).HasMaxLength(1024);

                entity.Property(e => e.TranReference)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TypeOwnerShip>(entity =>
            {
                entity.Property(e => e.IsEnable).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<TypeRemarketing>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => new { e.Username, e.Password })
                    .HasName("ix_User");

                entity.Property(e => e.CiscoExtension).HasMaxLength(10);

                entity.Property(e => e.CiscoPassword).HasMaxLength(100);

                entity.Property(e => e.CiscoUsername).HasMaxLength(100);

                entity.Property(e => e.Email).HasMaxLength(200);

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.Ipphone).HasColumnName("IPPhone");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastReceived).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PushTokenAndroid).HasMaxLength(500);

                entity.Property(e => e.PushTokenIos)
                    .HasColumnName("PushTokenIOS")
                    .HasMaxLength(500);

                entity.Property(e => e.UserIdSso).HasColumnName("UserIdSSO");

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK_User_Province");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_User_Company");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("FK_User_Group");
            });

            modelBuilder.Entity<UserActionPermission>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.UpdatetAt).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserActionPermission)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserActionPermission_User");
            });

            modelBuilder.Entity<UserCondition>(entity =>
            {
                entity.Property(e => e.CreatetAt).HasColumnType("datetime");

                entity.Property(e => e.Operator)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.Value).HasMaxLength(500);

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.UserCondition)
                    .HasForeignKey(d => d.PropertyId)
                    .HasConstraintName("FK_UserCondition_Property");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserCondition)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserCondition_User");
            });

            modelBuilder.Entity<UserMobileCisco>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.MbCiscoExtension).HasMaxLength(10);

                entity.Property(e => e.MbciscoPassword)
                    .HasColumnName("MBCiscoPassword")
                    .HasMaxLength(100);

                entity.Property(e => e.MbciscoUsername)
                    .HasColumnName("MBCiscoUsername")
                    .HasMaxLength(100);

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserModule>(entity =>
            {
                entity.HasOne(d => d.Module)
                    .WithMany(p => p.UserModule)
                    .HasForeignKey(d => d.ModuleId)
                    .HasConstraintName("FK_UserModule_Module1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserModule)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserModule_User1");
            });

            modelBuilder.Entity<UserShop>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.HasOne(d => d.Shop)
                    .WithMany(p => p.UserShop)
                    .HasForeignKey(d => d.ShopId)
                    .HasConstraintName("FK_UserShop_Shop");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserShop)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserShop_User");
            });

            modelBuilder.Entity<UserTeamTelesales>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.TeamTelesalesId });

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserTeamTelesales)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserTeamTelesales_User");
            });

            modelBuilder.Entity<UtmSource>(entity =>
            {
                entity.Property(e => e.UtmSourceName).HasMaxLength(200);

                entity.HasOne(d => d.GroupSource)
                    .WithMany(p => p.UtmSource)
                    .HasForeignKey(d => d.GroupSourceId)
                    .HasConstraintName("FK_UtmSource_GroupSource");
            });

            modelBuilder.Entity<Ward>(entity =>
            {
                entity.Property(e => e.WardId).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<ZaloDinhVi>(entity =>
            {
                entity.ToTable("ZaloDinhVi", "rp");

                entity.HasComment("danh sách zalo user nhận tin cảnh báo");

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.RecipientId)
                    .HasMaxLength(50)
                    .HasComment("zalo userID");

                entity.Property(e => e.RecipientName).HasMaxLength(500);

                entity.Property(e => e.UserId).HasComment("AG userId");
            });

            modelBuilder.Entity<ZaloLoanCredit>(entity =>
            {
                entity.ToTable("ZaloLoanCredit", "rp");

                entity.HasComment("Danh sách đơn tạm dừng cảnh báo");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
