﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LenderConfig
    {
        public int Id { get; set; }
        public int? LenderId { get; set; }
        public string LenderName { get; set; }
        public bool? IsLender { get; set; }
        public decimal? InitialMoney { get; set; }
        public decimal? CurrentMoney { get; set; }
        public decimal? LockedMoney { get; set; }
        public int? Received { get; set; }
        public int? Accepted { get; set; }
        public int? Returned { get; set; }
        public int? Retrieved { get; set; }
        public int? Priority { get; set; }
        public int? Status { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public bool? Esign { get; set; }
        public DateTime? EsignUpdatedTime { get; set; }
    }
}
