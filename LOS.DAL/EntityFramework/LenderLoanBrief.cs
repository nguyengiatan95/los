﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LenderLoanBrief
    {
        public int Id { get; set; }
        public int? LenderId { get; set; }
        public string LenderName { get; set; }
        public int? LoanBriefId { get; set; }
        public int? Status { get; set; }
        public DateTime? PushedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
    }
}
