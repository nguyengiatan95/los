﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanAction
    {
        public int LoanActionId { get; set; }
        public int? LoanBriefId { get; set; }
        public int? ActionStatusId { get; set; }
        public int? ActorId { get; set; }
        public DateTimeOffset? StartTime { get; set; }
        public DateTimeOffset? FinishTime { get; set; }
        public int? ExecutedTime { get; set; }
        public string Note { get; set; }
        public int? LastActionStatusId { get; set; }

        public virtual User Actor { get; set; }
    }
}
