﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanBriefDocument
    {
        public int LoanBriefDocumentId { get; set; }
        public int? LoanBriefId { get; set; }
        public int? DocumentId { get; set; }
        public string FilePath { get; set; }
        public string RelativePath { get; set; }
        public DateTimeOffset? UploadedTime { get; set; }
        public int? Status { get; set; }

        public virtual LoanBrief LoanBrief { get; set; }
        public virtual DocumentType LoanBriefDocumentNavigation { get; set; }
    }
}
