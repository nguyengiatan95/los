﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanBriefFiles
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public DateTime? CreateAt { get; set; }
        public string FilePath { get; set; }
        public int? UserId { get; set; }
        public int? Status { get; set; }
        public int? TypeId { get; set; }
        public int? S3status { get; set; }
        public string LinkImgOfPartner { get; set; }
        public int? TypeFile { get; set; }
        public int? Synchronize { get; set; }
        public string DomainOfPartner { get; set; }
        public int? IsDeleted { get; set; }
        public int? DeletedBy { get; set; }
        public DateTime? ModifyAt { get; set; }
        public int? MecashId { get; set; }
        public string FileThumb { get; set; }
        public string LatLong { get; set; }
        public int? SourceUpload { get; set; }
        public string AddressUpload { get; set; }
        public decimal? DistanceAddressHomeGoogleMap { get; set; }
        public decimal? DistanceAddressCompanyGoogleMap { get; set; }
        public int? AsyncLocal { get; set; }
        public int? LoanStatus { get; set; }
        public int? SubTypeId { get; set; }

        public virtual DocumentType Type { get; set; }
    }
}
