﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanBriefHistory
    {
        public int LoanBriefHistoryId { get; set; }
        public int? LoanBriefId { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public int? ActorId { get; set; }
        public string Action { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public int? UserId { get; set; }

        public virtual LoanBrief LoanBrief { get; set; }
    }
}
