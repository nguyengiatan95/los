﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanBriefImportFileExcel
    {
        public int Id { get; set; }
        public string FileImportName { get; set; }
        public string FileImportId { get; set; }
        public short? Status { get; set; }
        public int? LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public short? Gender { get; set; }
        public short? Age { get; set; }
        public string NationalCard { get; set; }
        public long? Salary { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string CityNameHouseHold { get; set; }
        public string DistrictNameHouseHold { get; set; }
        public string CompanyName { get; set; }
        public string ProductName { get; set; }
        public string Source { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? CityIdHouseHold { get; set; }
        public int? DistrictIdHouseHold { get; set; }
        public int? ProductId { get; set; }
        public string Note { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? CreateBy { get; set; }
        public string UserName { get; set; }
        public int? BoundTelesaleId { get; set; }
    }
}
