﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanBriefJob
    {
        public int LoanBriefJobId { get; set; }
        public int? ImcomeType { get; set; }
        public int? BusinessType { get; set; }
        public int? JobId { get; set; }
        public int? JobTitleId { get; set; }
        public decimal? TotalIncome { get; set; }
        public bool? DocumentAvailable { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyTaxCode { get; set; }
        public int? CompanyProvinceId { get; set; }
        public int? CompanyDistrictId { get; set; }
        public int? CompanyWardId { get; set; }
        public string WorkingAddress { get; set; }
        public string DepartmentName { get; set; }
        public int? WorkingTime { get; set; }
        public int? ContractType { get; set; }
        public int? ContractRemain { get; set; }
        public int? PaidType { get; set; }
        public string CardNumber { get; set; }
        public string BankName { get; set; }
        public string Description { get; set; }
        public string CompanyAddressGoogleMap { get; set; }
        public string CompanyAddressLatLng { get; set; }
        public bool? CompanyInsurance { get; set; }
        public bool? WorkLocation { get; set; }
        public bool? BusinessPapers { get; set; }
        public int? JobDescriptionId { get; set; }
        public string ResultLocation { get; set; }

        public virtual District CompanyDistrict { get; set; }
        public virtual Province CompanyProvince { get; set; }
        public virtual Ward CompanyWard { get; set; }
        public virtual Job Job { get; set; }
        public virtual JobTitle JobTitle { get; set; }
        public virtual LoanBrief LoanBriefJobNavigation { get; set; }
    }
}
