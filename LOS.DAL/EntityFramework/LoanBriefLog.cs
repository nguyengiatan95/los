﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanBriefLog
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public string LoanBriefOldValue { get; set; }
        public string LoanBriefValue { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
