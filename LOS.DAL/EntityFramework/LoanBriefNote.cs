﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanBriefNote
    {
        public int LoanBriefNoteId { get; set; }
        public int? LoanBriefId { get; set; }
        public int? Type { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public int? ActionComment { get; set; }
        public string Note { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public int? ShopId { get; set; }
        public string ShopName { get; set; }
        public bool? IsDisplay { get; set; }
        public bool? IsManual { get; set; }

        public virtual LoanBrief LoanBrief { get; set; }
    }
}
