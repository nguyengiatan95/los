﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanBriefProperty
    {
        public int LoanBriefPropertyId { get; set; }
        public int? PropertyType { get; set; }
        public int? BrandId { get; set; }
        public int? ProductId { get; set; }
        public int? Ownership { get; set; }
        public bool? DocumentAvailable { get; set; }
        public int? Name { get; set; }
        public int? Age { get; set; }
        public DateTimeOffset CreatedTime { get; set; }
        public DateTimeOffset? UpdatedTime { get; set; }
        public string CarManufacturer { get; set; }
        public string CarName { get; set; }
        public int? FormRepurchase { get; set; }
        public string PlateNumber { get; set; }
        public string CarColor { get; set; }
        public string PlateNumberCar { get; set; }
        public string Engine { get; set; }
        public string Chassis { get; set; }
        public bool? BuyInsuranceProperty { get; set; }
        public string OwnerFullName { get; set; }
        public string MotobikeCertificateNumber { get; set; }
        public string MotobikeCertificateDate { get; set; }
        public string MotobikeCertificateAddress { get; set; }
        public bool? PostRegistration { get; set; }
        public string Description { get; set; }

        public virtual BrandProduct Brand { get; set; }
        public virtual LoanBrief LoanBriefPropertyNavigation { get; set; }
        public virtual Product Product { get; set; }
    }
}
