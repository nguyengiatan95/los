﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanBriefQuestionScript
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public bool? QuestionUseMotobikeGo { get; set; }
        public bool? QuestionMotobikeCertificate { get; set; }
        public bool? QuestionAccountBank { get; set; }
        public short? TypeJob { get; set; }
        public bool? AdvisoryDeductInsurance { get; set; }
        public bool? AdvisoryKeepRegisteredMotobike { get; set; }
        public bool? AdvisorySettingGps { get; set; }
        public short? ChooseCareer { get; set; }
        public bool? Signs { get; set; }
        public bool? AddressBusiness { get; set; }
        public string WorkingTime { get; set; }
        public string DriverOfCompany { get; set; }
        public bool? AccountDriver { get; set; }
        public bool? QuestionDriverGo { get; set; }
        public string TimeDriver { get; set; }
        public bool? ContractInShop { get; set; }
        public bool? PhotographShop { get; set; }
        public string CommentStatusTelesales { get; set; }
        public bool? QuestionBorrow { get; set; }
        public bool? SigningAlaborContract { get; set; }
        public bool? TypeWork { get; set; }
        public string LatestPayday { get; set; }
        public string NameWeb { get; set; }
        public bool? QuestionWarehouse { get; set; }
        public bool? QuestionImageGoods { get; set; }
        public string DetailJob { get; set; }
        public string TimeSellWeb { get; set; }
        public bool? QuestionCarOwnership { get; set; }
        public DateTime? DateModify { get; set; }
        public bool? QuestionChangeColorMotobike { get; set; }
        public bool? QuestionUseMotobikeGoNormal { get; set; }
        public bool? QuestionAddress { get; set; }
        public bool? QuestionCheckCurrentStatusMotobike { get; set; }
        public int? TypeOwnerShip { get; set; }
        public int? DocumentCoincideHouseHold { get; set; }
        public int? DocumentNoCoincideHouseHold { get; set; }
        public string StartWokingJob { get; set; }
        public bool? QuestionNowInWorkingJob { get; set; }
        public bool? QuestionImageWorkplace { get; set; }
        public int? DocumentSalaryBankTransfer { get; set; }
        public int? DocumetSalaryCash { get; set; }
        public bool? QuestionLicenseDkkd { get; set; }
        public string CommentJob { get; set; }
        public int? DocmentRelationship { get; set; }
        public bool? QuestionBlurredSignCertificate { get; set; }
        public string HourOpenDoor { get; set; }
        public bool? QuestionProvidedImageOrVideo { get; set; }
        public string WaterSupplier { get; set; }
        public bool? RemindCustomerDocument { get; set; }
        public bool? QuestionHouseOwner { get; set; }
        public int? DocumentFormsOfResidence { get; set; }
        public bool? BusinessStatus { get; set; }
        public bool? QuestionBusinessLocation { get; set; }
        public bool? QuestionImageAppAccountRevenue { get; set; }
        public int? DocumentBusiness { get; set; }
        public bool? QuestionImageRegistrationBook { get; set; }
        public bool? QuestionAddressCoincideAreaSupport { get; set; }
        public short? Appraiser { get; set; }
        public long? MaxPrice { get; set; }
        public bool? QuestionOriginCardNumber { get; set; }
        public bool? QuestionOriginCavet { get; set; }
        public bool? QuestionMotobikeNearby { get; set; }
        public bool? QuestionDocumentJob { get; set; }
        public bool? QuestionCustomerBorrowProductFast4tr { get; set; }
        public long? LoanAmountFirst { get; set; }
        public bool? QuestionSupportArea { get; set; }
        public int? OwnerCar { get; set; }
        public bool? QuestionSaleContractCar { get; set; }

        public virtual LoanBrief LoanBrief { get; set; }
    }
}
