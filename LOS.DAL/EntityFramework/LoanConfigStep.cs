﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanConfigStep
    {
        public int? LoanStatusDetailId { get; set; }
        public string NextStep { get; set; }
        public bool? IsEnable { get; set; }
        public int? CurrentStep { get; set; }
    }
}
