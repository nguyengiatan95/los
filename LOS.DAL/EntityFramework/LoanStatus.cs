﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanStatus
    {
        public int LoanStatusId { get; set; }
        public int? Status { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? MecashStatus { get; set; }
    }
}
