﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LoanbiefAtruong
    {
        public double? Loanbriefid { get; set; }
        public string Phone { get; set; }
        public string NationalCard { get; set; }
        public DateTime? DisbursementAt { get; set; }
        public string Name { get; set; }
    }
}
