﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogActionRestruct
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public int? LoanbriefParentId { get; set; }
        public int? TypeAction { get; set; }
        public int? LoanTime { get; set; }
        public string FirstPayment { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? PercenInterest { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? CreatedBy { get; set; }
        public int? Status { get; set; }
    }
}
