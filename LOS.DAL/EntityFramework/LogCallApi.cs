﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogCallApi
    {
        public int Id { get; set; }
        public int? ActionCallApi { get; set; }
        public string Input { get; set; }
        public string LinkCallApi { get; set; }
        public string TokenCallApi { get; set; }
        public int? Status { get; set; }
        public string Output { get; set; }
        public int? LoanBriefId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifyAt { get; set; }
    }
}
