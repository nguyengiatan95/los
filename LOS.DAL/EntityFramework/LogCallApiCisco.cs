﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogCallApiCisco
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Extension { get; set; }
        public int? StatusResponse { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? ActionId { get; set; }
    }
}
