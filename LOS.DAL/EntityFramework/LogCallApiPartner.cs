﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogCallApiPartner
    {
        public int Id { get; set; }
        public int? Action { get; set; }
        public string Link { get; set; }
        public string Token { get; set; }
        public string Input { get; set; }
        public string Output { get; set; }
        public int? Status { get; set; }
        public int? IdSelect { get; set; }
        public DateTime? CreateAt { get; set; }
        public DateTime? ModifyAt { get; set; }
    }
}
