﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogCancelLoanbrief
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public int? ReasonCancel { get; set; }
        public string ReasonComment { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? IsExcuted { get; set; }
    }
}
