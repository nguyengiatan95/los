﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogChangePhone
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public string Phone { get; set; }
        public string PhoneNew { get; set; }
        public int? UserId { get; set; }
        public int? GroupId { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
