﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogCiscoPushToTelesale
    {
        public int LogCiscoPushToTelesaleId { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? IsPushToTelesale { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? LoanBriefId { get; set; }
        public int? CiscoStatus { get; set; }
    }
}
