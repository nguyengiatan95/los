﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogCloseLoan
    {
        public int Id { get; set; }
        public string Link { get; set; }
        public string Token { get; set; }
        public string Input { get; set; }
        public string Output { get; set; }
        public int? LoanBriefId { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateAt { get; set; }
        public DateTime? ModifyAt { get; set; }
    }
}
