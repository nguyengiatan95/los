﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogDistributingHubEmployee
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public int? HubEmployeeId { get; set; }
        public int? HubId { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
