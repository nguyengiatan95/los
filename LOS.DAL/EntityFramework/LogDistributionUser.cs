﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogDistributionUser
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public int? UserId { get; set; }
        public int? HubId { get; set; }
        public int? TypeDistribution { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? CreatedBy { get; set; }
    }
}
