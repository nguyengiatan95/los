﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogGeocodingResult
    {
        public int LogGeocodingResultId { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string FormattedAddress { get; set; }
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? TypeReport { get; set; }
        public int? LoanBriefId { get; set; }
        public double? StopPointTime { get; set; }
        public double? StopPointStart { get; set; }
    }
}
