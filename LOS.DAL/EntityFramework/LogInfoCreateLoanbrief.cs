﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogInfoCreateLoanbrief
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public long? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? ProductId { get; set; }
        public string UtmSource { get; set; }
        public string UtmMedium { get; set; }
        public string UtmCampaign { get; set; }
        public string UtmContent { get; set; }
        public string UtmTerm { get; set; }
        public string DomainName { get; set; }
        public int? PlatformType { get; set; }
        public string NationalCard { get; set; }
        public string AffCode { get; set; }
        public string AffSid { get; set; }
        public string Tid { get; set; }
        public string GoogleClickId { get; set; }
        public int? IsVerifyOtpLdp { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
