﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogLoanAction
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public int? ActionId { get; set; }
        public int? TypeAction { get; set; }
        public int? LoanStatus { get; set; }
        public int? TlsLoanStatusDetail { get; set; }
        public int? HubLoanStatusDetail { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }
        public int? TelesaleId { get; set; }
        public int? HubId { get; set; }
        public int? HubEmployeeId { get; set; }
        public int? CoordinatorUserId { get; set; }
        public int? UserActionId { get; set; }
        public int? GroupUserActionId { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
