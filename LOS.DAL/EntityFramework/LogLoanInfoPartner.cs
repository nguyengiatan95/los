﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogLoanInfoPartner
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public string BillId { get; set; }
        public string ServiceName { get; set; }
        public decimal? TotalAmount { get; set; }
        public string PartnerName { get; set; }
        public string PartnerPhone { get; set; }
        public string PartnerEmail { get; set; }
        public string PartnerPersonalId { get; set; }
        public string ExpiredDate { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
