﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogLoanbriefScoring
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public DateTime? LoanbriefCreatedAt { get; set; }
        public string Phone { get; set; }
        public string NationalCard { get; set; }
        public string FullName { get; set; }
        public decimal? RangeScore { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? IsPushAutoCall { get; set; }
        public DateTime? PushAutoCallAt { get; set; }
        public int? CampaignId { get; set; }
        public int? LastLoanStatus { get; set; }
        public DateTime? LastLoanDate { get; set; }
        public int? HubId { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public int? ProductId { get; set; }
        public DateTime? FirstLoanDate { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
