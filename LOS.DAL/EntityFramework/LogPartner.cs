﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogPartner
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public int? LenderId { get; set; }
        public int? CustomerId { get; set; }
        public int? UserId { get; set; }
        public string Provider { get; set; }
        public string TransactionType { get; set; }
        public DateTime? CreatedTime { get; set; }
        public string RequestUrl { get; set; }
        public string RequestContent { get; set; }
        public int? HttpStatusCode { get; set; }
        public int? ResponseCode { get; set; }
        public string ResponseContent { get; set; }
    }
}
