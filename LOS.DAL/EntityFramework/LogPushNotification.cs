﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogPushNotification
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public string Url { get; set; }
        public int? RequestStatus { get; set; }
        public string RequestToken { get; set; }
        public int? ActionPush { get; set; }
        public string RequestBody { get; set; }
        public string Response { get; set; }
        public DateTime? RequestAt { get; set; }
        public DateTime? ResponseAt { get; set; }
        public string MessagePush { get; set; }
        public string PushTo { get; set; }
        public int? TypeNotification { get; set; }
    }
}
