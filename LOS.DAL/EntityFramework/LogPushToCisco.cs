﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogPushToCisco
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string FullName { get; set; }
        public int? LoanbriefId { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? CreatedBy { get; set; }
    }
}
