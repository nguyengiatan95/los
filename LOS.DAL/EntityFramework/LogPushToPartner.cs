﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogPushToPartner
    {
        public int Id { get; set; }
        public string Partner { get; set; }
        public string PartnerId { get; set; }
        public string PartnerCustomerId { get; set; }
        public bool? PartnerProcess { get; set; }
        public int? LoanBriefId { get; set; }
        public int? Status { get; set; }
        public int? PushStatus { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public int? RetryCount { get; set; }
    }
}
