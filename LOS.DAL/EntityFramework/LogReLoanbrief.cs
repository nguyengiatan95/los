﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogReLoanbrief
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? CreatedBy { get; set; }
        public int? ReLoanbriefId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? TypeRemarketing { get; set; }
        public string UtmSource { get; set; }
        public int? IsExcuted { get; set; }
        public string ResultMessage { get; set; }
        public int? TypeReLoanbrief { get; set; }
        public int? TelesaleId { get; set; }
        public int? HubId { get; set; }
        public int? TeamTelesaleId { get; set; }
        public string NationalCard { get; set; }
    }
}
