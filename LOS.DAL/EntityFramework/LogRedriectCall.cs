﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogRedriectCall
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public int? BorrowMotobike { get; set; }
        public int? AreaSupport { get; set; }
        public int? ConnectNextTime { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? MotobikeCertificate { get; set; }
        public int? ResultCall { get; set; }
        public int? CampaignId { get; set; }
        public int? ReLoanbriefId { get; set; }
        public int? SendSms { get; set; }
        public string Numbercall { get; set; }
        public int? TypeCall { get; set; }

        public virtual LoanBrief Loanbrief { get; set; }
    }
}
