﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogRequestAi
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public int? ServiceType { get; set; }
        public int? IsExcuted { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
