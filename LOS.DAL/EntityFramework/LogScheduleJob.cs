﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogScheduleJob
    {
        public int Id { get; set; }
        public int? ActionId { get; set; }
        public string Content { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? Status { get; set; }
    }
}
