﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogSecuredTransaction
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string NoticeNumber { get; set; }
        public string Pin { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? Status { get; set; }
        public int? UserId { get; set; }
        public int? ConfirmStatus { get; set; }
    }
}
