﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogSendOtp
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string Otp { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? VerifyAt { get; set; }
        public string IpAddress { get; set; }
        public int? VerifyStatus { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public string Url { get; set; }
        public int? TypeSendOtp { get; set; }
        public int? StatusSend { get; set; }
        public DateTime? ModifyAt { get; set; }
        public DateTime? ExpiredVerifyOtp { get; set; }
        public DateTime? ExpiredToken { get; set; }
        public string TokenGennerate { get; set; }
        public int? TypeOtp { get; set; }
    }
}
