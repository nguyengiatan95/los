﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogSendSms
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public int? TypeSendSms { get; set; }
        public string Smscontent { get; set; }
        public string Url { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
