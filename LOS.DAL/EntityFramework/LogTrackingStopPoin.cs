﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogTrackingStopPoin
    {
        public int LogTrackingStopPoinId { get; set; }
        public long? LoanCreditId { get; set; }
        public decimal? PointLat { get; set; }
        public decimal? PoinLng { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public string StreetName { get; set; }
        public string FormatedAddress { get; set; }
        public DateTime? FromTime { get; set; }
        public int? TrackingType { get; set; }
        public int? TotalTime { get; set; }
        public string StreetNumber { get; set; }
        public bool? OutOfCity { get; set; }
    }
}
