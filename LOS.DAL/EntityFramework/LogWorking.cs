﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LogWorking
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? State { get; set; }
        public DateTime? CreateAt { get; set; }
    }
}
