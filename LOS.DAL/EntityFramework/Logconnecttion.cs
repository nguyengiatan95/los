﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Logconnecttion
    {
        public int? Stt { get; set; }
        public DateTime? Time { get; set; }
        public string ClientNetAddress { get; set; }
        public int? Sl { get; set; }
    }
}
