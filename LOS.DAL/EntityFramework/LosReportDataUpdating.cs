﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class LosReportDataUpdating
    {
        public long ReportDataUpdatingId { get; set; }
        public int? LoanBriefId { get; set; }
        public int? Status { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public string HomeAddress { get; set; }
        public string CompanyAddress { get; set; }
        public int? SupportId { get; set; }
        public int? SupportLastId { get; set; }
        public string SupportLastFullName { get; set; }
        public int? CounselorId { get; set; }
        public int? CoordinatorId { get; set; }
        public int? HubEmployeId { get; set; }
        public string HubEmployeFullName { get; set; }
        public int? UserIdCancel { get; set; }
        public int? CancelGroupId { get; set; }
        public string CancelFullName { get; set; }
        public DateTime? TelesalePushDate { get; set; }
        public DateTime? HubPushDate { get; set; }
        public DateTime? TdhsPushDate { get; set; }
        public DateTime? DisbursementDate { get; set; }
        public DateTime? CancelDateLoanCredit { get; set; }
        public int? TotalMinuteOfTelesale { get; set; }
        public int? TotalMinuteOfHub { get; set; }
        public int? TotalMinuteOfTdhs { get; set; }
        public int? TotalMinuteOfOther { get; set; }
        public int? TotalMinuteOfLander { get; set; }
        public int? TotalMinuteSubtractedOfTelesale { get; set; }
        public int? TotalMinuteSubtractedOfHub { get; set; }
        public int? TotalMinuteSubtractedOfTdhs { get; set; }
        public int? TotalMinuteSubtractedOfOther { get; set; }
        public int? TotalMinuteSubtractedOfLander { get; set; }
        public int? TotalTimesReturnOfTelesale { get; set; }
        public int? TotalTimesReturnOfHub { get; set; }
        public int? TotalTimesReturnOfTdhs { get; set; }
        public int? TotalTimesReturnOfOther { get; set; }
        public int? TotalTimesReturnOfLander { get; set; }
        public int? TotalMinute { get; set; }
        public int? TotalMinuteSubtracted { get; set; }
        public bool? IsFirstLoan { get; set; }
        public string CounselorFullName { get; set; }
        public string DeviceId { get; set; }
        public int? StatusOfDeviceId { get; set; }
        public short? IsVerifyOtpLdp { get; set; }
        public string ListCheckLeadQualify { get; set; }
        public int? IsQlf { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? ReasonCancel { get; set; }
        public string ReasonName { get; set; }
        public int? ResidentType { get; set; }
        public int? HouseholdCityId { get; set; }
        public int? HouseholdDistrictId { get; set; }
        public string HouseholdCityName { get; set; }
        public string HouseholdDistrictName { get; set; }
        public string CoordinatorFullName { get; set; }
        public int? LeanderCareId { get; set; }
        public string LeanderCareFullName { get; set; }
        public int? ManufacturerId { get; set; }
        public string YearMade { get; set; }
        public string IdCard { get; set; }
        public long? TotalMoney { get; set; }
        public DateTime? NextDate { get; set; }
        public int? LoanTime { get; set; }
        public DateTime? FinishDate { get; set; }
        public double? TotalTotalMillisecondsOfTelesale { get; set; }
        public double? TotalTotalMillisecondsOfHub { get; set; }
        public double? TotalTotalMillisecondsOfTdhs { get; set; }
        public double? TotalTotalMillisecondsOfOther { get; set; }
        public double? TotalTotalMillisecondsOfLander { get; set; }
        public double? TotalTotalMillisecondsSubtractedOfTelesale { get; set; }
        public double? TotalTotalMillisecondsSubtractedOfHub { get; set; }
        public double? TotalTotalMillisecondsSubtractedOfTdhs { get; set; }
        public double? TotalTotalMillisecondsSubtractedOfOther { get; set; }
        public double? TotalTotalMillisecondsSubtractedOfLander { get; set; }
        public double? TotalTotalMilliseconds { get; set; }
        public double? TotalTotalMillisecondsSubtracted { get; set; }
        public int? ShopId { get; set; }
        public string ShopName { get; set; }
        public int? CodeId { get; set; }
        public int? FirstNoteUserId { get; set; }
        public string FirstNoteUserName { get; set; }
        public DateTime? FirstNoteDate { get; set; }
        public double? FirstNoteTotalMinisecond { get; set; }
        public string FirstNoteContent { get; set; }
        public double? FirstNoteTotalMinisecondsSubtracted { get; set; }
    }
}
