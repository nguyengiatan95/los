﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Module
    {
        public Module()
        {
            GroupModule = new HashSet<GroupModule>();
            InverseParent = new HashSet<Module>();
            UserModule = new HashSet<UserModule>();
        }

        public int ModuleId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public int? Status { get; set; }
        public int? ParentId { get; set; }
        public int? Priority { get; set; }
        public bool? IsMenu { get; set; }
        public int? UserId { get; set; }
        public int? ApplicationId { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual Module Parent { get; set; }
        public virtual ICollection<GroupModule> GroupModule { get; set; }
        public virtual ICollection<Module> InverseParent { get; set; }
        public virtual ICollection<UserModule> UserModule { get; set; }
    }
}
