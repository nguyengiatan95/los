﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ModulePermission
    {
        public int ModuleId { get; set; }
        public int PermissionId { get; set; }

        public virtual Permission Permission { get; set; }
    }
}
