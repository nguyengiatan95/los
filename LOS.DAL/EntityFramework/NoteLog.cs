﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class NoteLog
    {
        public string Maloi { get; set; }
        public string Note { get; set; }
        public string Bank { get; set; }
    }
}
