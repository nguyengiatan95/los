﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Notebanklog
    {
        public string Sotk { get; set; }
        public double? Loanbriefid { get; set; }
        public string Frombank { get; set; }
        public string Bank { get; set; }
        public string Loi { get; set; }
    }
}
