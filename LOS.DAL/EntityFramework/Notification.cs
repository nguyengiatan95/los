﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Notification
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public string Topic { get; set; }
        public string Action { get; set; }
        public string Message { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? Status { get; set; }
        public DateTime? PushTime { get; set; }
        public string Read { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
