﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class OrderSourceDetail
    {
        public int OrderSourceDetailId { get; set; }
        public int? OrderSourceId { get; set; }
        public string Name { get; set; }
        public byte? Status { get; set; }
        public byte? KeyType { get; set; }
        public byte? CompareType { get; set; }
        public byte? CompareTo { get; set; }
        public int? MecashId { get; set; }
    }
}
