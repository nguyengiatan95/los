﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class PlatformType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
        public int? MecashId { get; set; }
    }
}
