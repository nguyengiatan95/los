﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Position
    {
        public Position()
        {
            User = new HashSet<User>();
        }

        public int PositionId { get; set; }
        public string Name { get; set; }
        public int? Status { get; set; }

        public virtual ICollection<User> User { get; set; }
    }
}
