﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Product
    {
        public Product()
        {
            ContactCustomer = new HashSet<ContactCustomer>();
            LoanBriefProperty = new HashSet<LoanBriefProperty>();
            ProductReviewResult = new HashSet<ProductReviewResult>();
        }

        public int Id { get; set; }
        public int BrandProductId { get; set; }
        public int ProductTypeId { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public long? Price { get; set; }
        public int? Year { get; set; }
        public int? BrakeType { get; set; }
        public int? RimType { get; set; }
        public string ShortName { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool? IsEnable { get; set; }

        public virtual BrandProduct BrandProduct { get; set; }
        public virtual ProductType ProductType { get; set; }
        public virtual ICollection<ContactCustomer> ContactCustomer { get; set; }
        public virtual ICollection<LoanBriefProperty> LoanBriefProperty { get; set; }
        public virtual ICollection<ProductReviewResult> ProductReviewResult { get; set; }
    }
}
