﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ProductAppraiser
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public int? ProductCreditId { get; set; }
        public int? TypeAppraiser { get; set; }
        public int? Status { get; set; }
        public int? UserId { get; set; }
        public int? GroupId { get; set; }
        public int? IsWork { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
