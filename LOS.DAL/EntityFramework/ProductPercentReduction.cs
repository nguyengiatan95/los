﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ProductPercentReduction
    {
        public int Id { get; set; }
        public int BrandProductId { get; set; }
        public int ProductTypeId { get; set; }
        public int? CountYear { get; set; }
        public int? PercentReduction { get; set; }

        public virtual BrandProduct BrandProduct { get; set; }
    }
}
