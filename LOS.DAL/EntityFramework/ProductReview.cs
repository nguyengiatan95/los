﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ProductReview
    {
        public ProductReview()
        {
            ProductReviewDetail = new HashSet<ProductReviewDetail>();
            ProductReviewResult = new HashSet<ProductReviewResult>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int? Status { get; set; }
        public int? OrderNo { get; set; }
        public bool? State { get; set; }
        public bool? IsCancel { get; set; }

        public virtual ICollection<ProductReviewDetail> ProductReviewDetail { get; set; }
        public virtual ICollection<ProductReviewResult> ProductReviewResult { get; set; }
    }
}
