﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ProductReviewDetail
    {
        public int Id { get; set; }
        public int? ProductTypeId { get; set; }
        public int? ProductReviewId { get; set; }
        public long? MoneyDiscount { get; set; }
        public decimal? PecentDiscount { get; set; }

        public virtual ProductReview ProductReview { get; set; }
        public virtual ProductType ProductType { get; set; }
    }
}
