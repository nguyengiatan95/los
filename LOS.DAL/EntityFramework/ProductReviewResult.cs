﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ProductReviewResult
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public int? ProductId { get; set; }
        public int? ProductReviewId { get; set; }
        public bool? IsCheck { get; set; }
        public DateTime? CreateDate { get; set; }

        public virtual Product Product { get; set; }
        public virtual ProductReview ProductReview { get; set; }
    }
}
