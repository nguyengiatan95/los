﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ProductReviewResultDetail
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public int? ProductId { get; set; }
        public int? UserId { get; set; }
        public int? GroupUserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? PriceReview { get; set; }
    }
}
