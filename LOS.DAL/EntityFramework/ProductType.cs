﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ProductType
    {
        public ProductType()
        {
            Product = new HashSet<Product>();
            ProductReviewDetail = new HashSet<ProductReviewDetail>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? Status { get; set; }

        public virtual ICollection<Product> Product { get; set; }
        public virtual ICollection<ProductReviewDetail> ProductReviewDetail { get; set; }
    }
}
