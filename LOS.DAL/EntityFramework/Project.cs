﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Project
    {
        public Project()
        {
            UserProject = new HashSet<UserProject>();
        }

        public int ProjectId { get; set; }
        public string Name { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public virtual ICollection<UserProject> UserProject { get; set; }
    }
}
