﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class PropertyValue
    {
        public int PropertyValueId { get; set; }
        public int? PropertyId { get; set; }
        public string Value { get; set; }

        public virtual Property Property { get; set; }
    }
}
