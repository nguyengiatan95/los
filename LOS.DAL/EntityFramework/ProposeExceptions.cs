﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ProposeExceptions
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public int? UserId { get; set; }
        public int? DocId { get; set; }
        public string DocName { get; set; }
        public DateTime? CreateAt { get; set; }
        public int? ChildDocId { get; set; }
        public string ChildDocName { get; set; }
        public decimal? Money { get; set; }
        public string Note { get; set; }
    }
}
