﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Province
    {
        public Province()
        {
            ContactCustomer = new HashSet<ContactCustomer>();
            LoanBrief = new HashSet<LoanBrief>();
            LoanBriefHousehold = new HashSet<LoanBriefHousehold>();
            LoanBriefJob = new HashSet<LoanBriefJob>();
            LoanBriefResident = new HashSet<LoanBriefResident>();
            User = new HashSet<User>();
        }

        public int ProvinceId { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? Priority { get; set; }
        public int? DomainId { get; set; }
        public int? ProvinceCode { get; set; }
        public int? MecashId { get; set; }
        public int? IsApply { get; set; }
        public string MapTransaction { get; set; }

        public virtual ICollection<ContactCustomer> ContactCustomer { get; set; }
        public virtual ICollection<LoanBrief> LoanBrief { get; set; }
        public virtual ICollection<LoanBriefHousehold> LoanBriefHousehold { get; set; }
        public virtual ICollection<LoanBriefJob> LoanBriefJob { get; set; }
        public virtual ICollection<LoanBriefResident> LoanBriefResident { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
