﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class PushLoanToPartner
    {
        public int Id { get; set; }
        public int? LoanPartnerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string ProvinceName { get; set; }
        public int? ProvinceId { get; set; }
        public string DistrictName { get; set; }
        public int? DistrictId { get; set; }
        public int? PushAction { get; set; }
        public DateTime? PushDate { get; set; }
        public int? Status { get; set; }
        public DateTime? SynchronizedDate { get; set; }
        public int? Partner { get; set; }
        public string Comment { get; set; }
        public string Tid { get; set; }
        public string AffCode { get; set; }
        public string AffSid { get; set; }
        public string Domain { get; set; }
        public string UtmSource { get; set; }
        public string UtmMedium { get; set; }
        public string UtmCampaign { get; set; }
        public string UtmTerm { get; set; }
        public string UtmContent { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? LoanbriefId { get; set; }
        public string Email { get; set; }
    }
}
