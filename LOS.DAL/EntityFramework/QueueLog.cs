﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class QueueLog
    {
        public int QueueLogId { get; set; }
        public string Message { get; set; }
        public string Hash { get; set; }
        public DateTimeOffset? PushAt { get; set; }
    }
}
