﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ReasonCoordinator
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public int? UserId { get; set; }
        public int? ReasonId { get; set; }
        public string Reason { get; set; }
        public DateTime? CreateAt { get; set; }
        public int? GroupId { get; set; }
    }
}
