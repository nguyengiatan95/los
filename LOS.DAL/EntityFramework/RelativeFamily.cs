﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class RelativeFamily
    {
        public RelativeFamily()
        {
            LoanBriefRelationship = new HashSet<LoanBriefRelationship>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }

        public virtual ICollection<LoanBriefRelationship> LoanBriefRelationship { get; set; }
    }
}
