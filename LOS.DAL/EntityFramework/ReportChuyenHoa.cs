﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ReportChuyenHoa
    {
        public long ReportChuyenHoaId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ForDate { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? TypeReport { get; set; }
        public int? NumberOne { get; set; }
        public int? NumberTwo { get; set; }
        public int? NumberThree { get; set; }
        public int? NumberFour { get; set; }
        public int? ShopId { get; set; }
        public string ShopName { get; set; }
    }
}
