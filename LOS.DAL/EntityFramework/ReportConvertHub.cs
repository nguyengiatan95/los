﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ReportConvertHub
    {
        public int Id { get; set; }
        public DateTime? ForDate { get; set; }
        public int? ShopId { get; set; }
        public string ShopName { get; set; }
        public int? NumberPushToHub { get; set; }
        public int? NumberDisbursement { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
