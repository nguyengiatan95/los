﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ReportMaketingData
    {
        public int ReportMaketingDataId { get; set; }
        public int? LoanBriefId { get; set; }
        public long? TotalMoney { get; set; }
        public int? LoanTime { get; set; }
        public int? Status { get; set; }
        public DateTime? FromDate { get; set; }
        public string Note { get; set; }
        public int? ShopId { get; set; }
        public string ShopName { get; set; }
        public string UtmSource { get; set; }
        public string UtmMedium { get; set; }
        public string UtmCampaign { get; set; }
        public string UtmTerm { get; set; }
        public string UtmContent { get; set; }
        public string DomainName { get; set; }
        public int? PlatformType { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public bool? ReMarketing { get; set; }
        public int? ReMarketingLoanCreditId { get; set; }
        public bool? IsHeadOffice { get; set; }
        public bool? IsLocate { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public byte? TypeProduct { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public int? CustomerCreditId { get; set; }
        public string CustomerFullName { get; set; }
        public string IdCard { get; set; }
        public string Tid { get; set; }
        public string HomeAddress { get; set; }
        public string CompanyAddress { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? SupportId { get; set; }
        public int? SupportLastId { get; set; }
        public string SupportLastFullName { get; set; }
    }
}
