﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ReportRawData
    {
        public int ReportRawDataId { get; set; }
        public int? LoanCreditId { get; set; }
        public long? LTotalMoney { get; set; }
        public int? LLoanTime { get; set; }
        public int? LcStatus { get; set; }
        public DateTime? LFromDate { get; set; }
        public string LcNote { get; set; }
        public int? LcCode { get; set; }
        public DateTime? LNextDate { get; set; }
        public string UtmSource { get; set; }
        public string UtmMedium { get; set; }
        public string UtmCampaign { get; set; }
        public string UtmTerm { get; set; }
        public string UtmContent { get; set; }
        public string DomainName { get; set; }
        public int? PlatformType { get; set; }
        public int? ReasonId { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public bool? ReMarketing { get; set; }
        public int? ReMarketingLoanCreditId { get; set; }
        public bool? IsHeadOffice { get; set; }
        public bool? IsLocate { get; set; }
        public string DeviceId { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public byte? TypeProduct { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public int? CustomerCreditId { get; set; }
        public string CustomerFullName { get; set; }
        public string BeforeUtmSource { get; set; }
        public string BeforeUtmMedium { get; set; }
        public string BeforeUtmCampaign { get; set; }
        public string BeforeUtmTerm { get; set; }
        public string BeforeUtmContent { get; set; }
        public int? BeforeStatus { get; set; }
        public int? BeforeLoanCreditId { get; set; }
        public DateTime? BeforeCreateDate { get; set; }
        public string IdCard { get; set; }
        public string Tid { get; set; }
        public int? StatusOfDeviceId { get; set; }
        public string HomeAddress { get; set; }
        public string CompanyAddress { get; set; }
        public int? IsQlf { get; set; }
        public DateTime? LcCreateDate { get; set; }
        public int? SupportId { get; set; }
        public int? SupportLastId { get; set; }
        public string SupportLastFullName { get; set; }
        public int? CounselorId { get; set; }
        public int? CoordinatorId { get; set; }
        public int? HubEmployeId { get; set; }
        public string HubEmployeFullName { get; set; }
        public int? UserIdcancel { get; set; }
        public int? CancelGroupId { get; set; }
        public string CancelFullName { get; set; }
        public DateTime? TelesalePushDate { get; set; }
        public DateTime? HubPushDate { get; set; }
        public DateTime? TdhsPushDate { get; set; }
        public DateTime? DisDate { get; set; }
        public DateTime? CancelDateLoanCredit { get; set; }
        public int? TotalMinuteOfTelesale { get; set; }
        public int? TotalMinuteOfHub { get; set; }
        public int? TotalMinuteOfTdhs { get; set; }
        public int? TotalMinuteOfOther { get; set; }
        public int? TotalMinuteOfLander { get; set; }
        public int? TotalMinuteSubtractedOfTelesale { get; set; }
        public int? TotalMinuteSubtractedOfHub { get; set; }
        public int? TotalMinuteSubtractedOfTdhs { get; set; }
        public int? TotalMinuteSubtractedOfOther { get; set; }
        public int? TotalMinuteSubtractedOfLander { get; set; }
        public int? TotalTimesReturnOfTelesale { get; set; }
        public int? TotalTimesReturnOfHub { get; set; }
        public int? TotalTimesReturnOfTdhs { get; set; }
        public int? TotalTimesReturnOfOther { get; set; }
        public int? TotalTimesReturnOfLander { get; set; }
        public int? TotalMinute { get; set; }
        public int? TotalMinuteSubtracted { get; set; }
        public short? IsVerifyOtpLdp { get; set; }
        public string ListCheckLeadQualify { get; set; }
        public int? OrderSourceId { get; set; }
        public string OderSourceName { get; set; }
        public int? OrderSourceParentId { get; set; }
        public string OrderSourceParentName { get; set; }
        public bool? IsFirstLoan { get; set; }
        public string ShopName { get; set; }
    }
}
