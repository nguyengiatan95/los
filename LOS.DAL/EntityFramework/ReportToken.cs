﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ReportToken
    {
        public int TokenId { get; set; }
        public string Token { get; set; }
        public DateTime? DateEx { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public string ApplyFor { get; set; }
    }
}
