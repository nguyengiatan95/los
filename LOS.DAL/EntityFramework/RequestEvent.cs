﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class RequestEvent
    {
        public int Id { get; set; }
        public int? EventId { get; set; }
        public int? LoanbriefId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string Url { get; set; }
        public string Token { get; set; }
        public string Request { get; set; }
        public DateTime? RequestAt { get; set; }
        public string Response { get; set; }
        public DateTime? ResponseAt { get; set; }
        public int? IsExcuted { get; set; }
    }
}
