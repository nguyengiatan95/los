﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ResultEkyc
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string AddressValue { get; set; }
        public bool? AddressCheck { get; set; }
        public string IdValue { get; set; }
        public bool? IdCheck { get; set; }
        public string FullNameValue { get; set; }
        public bool? FullNameCheck { get; set; }
        public string BirthdayValue { get; set; }
        public bool? BirthdayCheck { get; set; }
        public string ExpiryValue { get; set; }
        public bool? ExpiryCheck { get; set; }
        public string GenderValue { get; set; }
        public bool? GenderCheck { get; set; }
        public string EthnicityValue { get; set; }
        public bool? EthnicityCheck { get; set; }
        public string IssueByValue { get; set; }
        public bool? IssueByCheck { get; set; }
        public string IssueDateValue { get; set; }
        public bool? IssueDateCheck { get; set; }
        public string ReligionValue { get; set; }
        public bool? ReligionCheck { get; set; }
        public string FaceCompareCode { get; set; }
        public string FaceCompareMessage { get; set; }
        public string FaceQueryCode { get; set; }
        public string FaceQueryMessage { get; set; }
    }
}
