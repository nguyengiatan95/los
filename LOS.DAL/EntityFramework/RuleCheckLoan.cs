﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class RuleCheckLoan
    {
        public int LoanbriefId { get; set; }
        public bool? IsCheckPayment { get; set; }
        public bool? IsCheckMomo { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? UserId { get; set; }
    }
}
