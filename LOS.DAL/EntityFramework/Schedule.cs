﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Schedule
    {
        public int ScheduleId { get; set; }
        public int? UserId { get; set; }
        public int? LoanBriefId { get; set; }
        public DateTimeOffset? ScheduleTime { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
    }
}
