﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ScheduleTime
    {
        public int? LoanBriefId { get; set; }
        public int? UserId { get; set; }
        public string Note { get; set; }
        public int? HubId { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdateAt { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int? Status { get; set; }
        public int ScheduleId { get; set; }
        public string CusName { get; set; }
        public string UserFullName { get; set; }
    }
}
