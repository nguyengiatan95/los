﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Script
    {
        public Script()
        {
            ScriptDetail = new HashSet<ScriptDetail>();
        }

        public int ScriptId { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTimeOffset? UpdatedTime { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual ICollection<ScriptDetail> ScriptDetail { get; set; }
    }
}
