﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ScriptDetail
    {
        public int ScriptDetailId { get; set; }
        public int? ScriptId { get; set; }
        public string Question { get; set; }
        public int? PropertyId { get; set; }
        public bool? ValidRequired { get; set; }
        public int? Priority { get; set; }

        public virtual Property Property { get; set; }
        public virtual Script Script { get; set; }
    }
}
