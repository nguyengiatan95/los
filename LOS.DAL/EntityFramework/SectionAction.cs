﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class SectionAction
    {
        public int SectionActionId { get; set; }
        public int? SectionDetailId { get; set; }
        public int? PropertyId { get; set; }
        public string Value { get; set; }
        public int? Type { get; set; }

        public virtual Property Property { get; set; }
        public virtual SectionDetail SectionDetail { get; set; }
    }
}
