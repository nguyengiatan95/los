﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class SectionActionDetail
    {
        public int SectionActionDetailId { get; set; }
        public int? SectionActionId { get; set; }
        public int? PropertyId { get; set; }
        public string Value { get; set; }

        public virtual Property Property { get; set; }
        public virtual SectionAction SectionAction { get; set; }
    }
}
