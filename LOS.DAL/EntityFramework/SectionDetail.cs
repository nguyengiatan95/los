﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class SectionDetail
    {
        public SectionDetail()
        {
            InverseDisapprove = new HashSet<SectionDetail>();
            InverseReturn = new HashSet<SectionDetail>();
            SectionAction = new HashSet<SectionAction>();
            SectionApproveApprove = new HashSet<SectionApprove>();
            SectionApproveSectionDetail = new HashSet<SectionApprove>();
        }

        public int SectionDetailId { get; set; }
        public int? SectionId { get; set; }
        public string Name { get; set; }
        public int? Step { get; set; }
        public string Mode { get; set; }
        public int? ApproveId { get; set; }
        public int? DisapproveId { get; set; }
        public int? ReturnId { get; set; }
        public bool? IsShow { get; set; }
        public int? Status { get; set; }

        public virtual SectionDetail Disapprove { get; set; }
        public virtual SectionDetail Return { get; set; }
        public virtual Section Section { get; set; }
        public virtual ICollection<SectionDetail> InverseDisapprove { get; set; }
        public virtual ICollection<SectionDetail> InverseReturn { get; set; }
        public virtual ICollection<SectionAction> SectionAction { get; set; }
        public virtual ICollection<SectionApprove> SectionApproveApprove { get; set; }
        public virtual ICollection<SectionApprove> SectionApproveSectionDetail { get; set; }
    }
}
