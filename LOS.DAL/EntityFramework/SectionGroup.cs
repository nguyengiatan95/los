﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class SectionGroup
    {
        public int SectionGroupId { get; set; }
        public string Name { get; set; }
        public int? InitialSectionId { get; set; }

        public virtual Section InitialSection { get; set; }
    }
}
