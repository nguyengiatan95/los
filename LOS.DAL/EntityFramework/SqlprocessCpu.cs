﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class SqlprocessCpu
    {
        public int? CpuUsage { get; set; }
        public long? RowNumber { get; set; }
    }
}
