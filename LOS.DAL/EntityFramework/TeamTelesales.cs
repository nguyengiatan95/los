﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class TeamTelesales
    {
        public int TeamTelesalesId { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
        public bool? IsTelesaleTeam { get; set; }
    }
}
