﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class TelesaleLoanbrief
    {
        public int Id { get; set; }
        public int? TelesalesId { get; set; }
        public int? LoanBriefId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? CreatedBy { get; set; }
    }
}
