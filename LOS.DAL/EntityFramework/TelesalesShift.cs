﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class TelesalesShift
    {
        public int TelesalesShiftId { get; set; }
        public string Shift { get; set; }
        public string Time { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public bool? IsEnable { get; set; }
    }
}
