﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class Token
    {
        public int Id { get; set; }
        public string Token1 { get; set; }
        public string AppId { get; set; }
        public DateTime? DateExpiration { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
