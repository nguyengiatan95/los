﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class UserActionPermission
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? LoanStatus { get; set; }
        public int? Value { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatetAt { get; set; }
        public int? CreatedBy { get; set; }

        public virtual User User { get; set; }
    }
}
