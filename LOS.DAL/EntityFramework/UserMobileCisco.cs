﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class UserMobileCisco
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTimeOffset? UpdatedTime { get; set; }
        public string MbciscoUsername { get; set; }
        public string MbciscoPassword { get; set; }
        public string MbCiscoExtension { get; set; }
    }
}
