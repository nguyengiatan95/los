﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class UserModule
    {
        public int UserModuleId { get; set; }
        public int? UserId { get; set; }
        public int? ModuleId { get; set; }
        public short? ApplicationId { get; set; }

        public virtual Module Module { get; set; }
        public virtual User User { get; set; }
    }
}
