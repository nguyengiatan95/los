﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class UserOnline
    {
        public int UserOnlineId { get; set; }
        public int? UserId { get; set; }
        public DateTime? Date { get; set; }
        public int? OnlineTime { get; set; }
        public int? RestTime { get; set; }
        public int? WorkingTime { get; set; }
        public string OnlineData { get; set; }
        public bool? IsOnline { get; set; }
        public bool? IsWorking { get; set; }
        public bool? IsProcessing { get; set; }
        public DateTimeOffset? LoginTime { get; set; }
        public DateTimeOffset? LastTriggerTime { get; set; }
        public string Username { get; set; }
    }
}
