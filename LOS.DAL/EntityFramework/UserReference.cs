﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class UserReference
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? UserReferenceId { get; set; }
        public string AffReferenceCode { get; set; }
        public int? Level { get; set; }
        public string UserCode { get; set; }

        public virtual UserMmo User { get; set; }
        public virtual UserMmo UserReferenceNavigation { get; set; }
    }
}
