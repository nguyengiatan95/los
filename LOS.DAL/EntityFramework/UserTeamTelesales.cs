﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class UserTeamTelesales
    {
        public int UserId { get; set; }
        public int TeamTelesalesId { get; set; }
        public bool? MainTeam { get; set; }

        public virtual User User { get; set; }
    }
}
