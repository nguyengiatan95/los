﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ZaloDinhVi
    {
        public int Id { get; set; }
        public string RecipientId { get; set; }
        public string RecipientName { get; set; }
        public bool? IsActive { get; set; }
        public int? ActiveBy { get; set; }
        public DateTime? ActiveDate { get; set; }
        public int? UserId { get; set; }
    }
}
