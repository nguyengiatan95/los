﻿using System;
using System.Collections.Generic;

namespace LOS.DAL.EntityFramework
{
    public partial class ZaloLoanCredit
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public bool? IsActive { get; set; }
    }
}
