﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Helpers
{
    public class Constants
    {
        public static string CreateDistrict = "/api/District/CreateNewDistrict";
        public static string CreateWard = "/api/Ward/CreateNewWard";
        public static string GetPaymentById = "/api/Loan/GetPaymentLoanAPIForLos?LoanId={0}";
        public static string DeferredPayment = "/api/S2S/CheckCustomerDeferredPayment";
    }
}
