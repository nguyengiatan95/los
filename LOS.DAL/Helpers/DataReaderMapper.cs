﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;

namespace LOS.DAL.Helpers
{
	public class DataReaderMapper<T> where T : class
	{
		public List<T> MapToList(DbDataReader dr)
		{
			if (dr != null && dr.HasRows)
			{
				var entity = typeof(T);
				var entities = new List<T>();
				var propDict = new Dictionary<string, PropertyInfo>();
				var props = entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
				propDict = props.ToDictionary(p => p.Name.ToUpper(), p => p);

				T newObject = default(T);
				while (dr.Read())
				{
					newObject = Activator.CreateInstance<T>();

					for (int index = 0; index < dr.FieldCount; index++)
					{
						if (propDict.ContainsKey(dr.GetName(index).ToUpper()))
						{
							var info = propDict[dr.GetName(index).ToUpper()];
							if ((info != null) && info.CanWrite)
							{
								var val = dr.GetValue(index);
								info.SetValue(newObject, (val == DBNull.Value) ? null : val, null);
							}
						}
					}

					entities.Add(newObject);
				}

				return entities;
			}

			return null;
		}
	}
}