﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace LOS.DAL.Helpers
{
	public class DatabaseHelper<T> where T : class
	{
		public T UpdateDynamicField(T obj, string field, string value, string type)
		{
			try
			{
				PropertyInfo f = typeof(T).GetProperty(field, BindingFlags.Instance | BindingFlags.Public);
				if (f != null)
				{
					if (type == "Integer")
					{
						f.SetValue(obj, Int32.Parse(value));
					}
					else if (type == "Long")
					{
						f.SetValue(obj, Int64.Parse(value));
					}
					else if (type == "Decimal")
					{
						f.SetValue(obj, Decimal.Parse(value));
					}
					else if (type == "Date")
					{
						f.SetValue(obj, DateTime.Parse(value));
					}
					else if (type == "Datetime")
					{
						f.SetValue(obj, DateTime.Parse(value));
					}
					else if (type == "Boolean")
					{
						f.SetValue(obj, Boolean.Parse(value));
					}
					else if (type == "String")
					{
						f.SetValue(obj, value);
					}
					else
					{
						f.SetValue(obj, value);
					}
				}
				return obj;
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		public bool CheckFieldOperator(T obj, string field, string op, string value, string type)
		{
			try
			{
				PropertyInfo f = typeof(T).GetProperty(field, BindingFlags.Instance | BindingFlags.Public);
				if (f != null)
				{
					if (op == "EQUAL")
					{
						if (type == "Integer")
						{
							var currentValue = (int?)f.GetValue(obj);
							if (currentValue != null)
							{
								if (currentValue == Int32.Parse(value))
									return true;
							}
							else
							{
								if (value.Equals("null"))
									return true;
							}
						}
						if (type == "NullableInteger")
						{
							var currentValue = (int?)f.GetValue(obj);
							if (currentValue != null)
							{
								if (currentValue == Int32.Parse(value))
									return true;
							}
							else
							{
								if (value.Equals("null") || value.Equals("0"))
									return true;
							}
						}
						else if (type == "Long")
						{
							var currentValue = (long?)f.GetValue(obj);
							if (currentValue != null)
							{
								if (currentValue == Int64.Parse(value))
									return true;
							}
							else
							{
								if (value.Equals("null"))
									return true;
							}
						}
						else if (type == "Decimal")
						{
							var currentValue = (decimal?)f.GetValue(obj);
							if (currentValue != null)
							{
								if (currentValue == Decimal.Parse(value))
									return true;
							}
							else
							{
								if (value.Equals("null"))
									return true;
							}
						}
						else if (type == "Date")
						{
							var currentValue = DateTime.Parse(f.GetValue(obj).ToString());
							if (currentValue != null)
							{
								if (currentValue == DateTime.Parse(value))
									return true;
							}
							else
							{
								if (value.Equals("null"))
									return true;
							}
						}
						else if (type == "Datetime")
						{
							var currentValue = DateTime.Parse(f.GetValue(obj).ToString());
							if (currentValue != null)
							{
								var compareValue = DateTime.Parse(value);
								if (currentValue.Year == compareValue.Year && currentValue.Month == compareValue.Month && currentValue.Day == compareValue.Day
								&& currentValue.Hour == compareValue.Hour && currentValue.Minute == compareValue.Minute && currentValue.Second == compareValue.Second)
									return true;
							}
							else
							{
								if (value.Equals("null"))
									return true;
							}
						}
						else if (type == "Boolean")
						{
							var currentValue = (bool?)f.GetValue(obj);
							if (currentValue != null)
							{
								if (currentValue == bool.Parse(value == "1" ? "true" : "false"))
									return true;
							}
							else
							{
								if (value.Equals("null") || value.Equals("0"))
									return true;
							}
						}
						else if (type == "String")
						{
							var currentValue = (string)f.GetValue(obj);
							if (String.Compare(currentValue, value, StringComparison.InvariantCultureIgnoreCase) == 0)
								return true;
						}
						else
						{
							return false;
						}
					}
					if (op == "NOT_EQUAL")
					{
						if (type == "Integer")
						{
							var currentValue = (int?)f.GetValue(obj);
							if (currentValue != null)
							{
								if (!value.Equals("null"))
								{
									if (currentValue != Int32.Parse(value))
										return true;
								}
								else
									return true;
							}
							else
							{
								if (!value.Equals("null"))
									return true;
							}
						}
						else if (type == "Long")
						{
							var currentValue = (long?)f.GetValue(obj);
							if (currentValue != null)
							{
								if (!value.Equals("null"))
								{
									if (currentValue != Int64.Parse(value))
										return true;
								}
								else
									return true;
							}
							else
							{
								if (!value.Equals("null"))
									return true;
							}
						}
						else if (type == "Decimal")
						{
							var currentValue = (decimal?)f.GetValue(obj);
							if (currentValue != null)
							{
								if (!value.Equals("null"))
								{
									if (currentValue != Decimal.Parse(value))
										return true;
								}
								else
									return true;
							}
							else
							{
								if (!value.Equals("null"))
									return true;
							}
						}
						else if (type == "Date")
						{
							var currentValue = DateTime.Parse(f.GetValue(obj).ToString());
							if (currentValue != null)
							{
								if (!value.Equals("null"))
								{
									if (currentValue != DateTime.Parse(value))
										return true;
								}
								else
									return true;
							}
							else
							{
								if (!value.Equals("null"))
									return true;
							}
						}
						else if (type == "Datetime")
						{
							var currentValue = DateTime.Parse(f.GetValue(obj).ToString());
							if (currentValue != null)
							{
								if (!value.Equals("null"))
								{
									var compareValue = DateTime.Parse(value);
									if (currentValue.Year != compareValue.Year || currentValue.Month != compareValue.Month || currentValue.Day != compareValue.Day
									|| currentValue.Hour != compareValue.Hour || currentValue.Minute != compareValue.Minute || currentValue.Second != compareValue.Second)
										return true;
								}
								else
									return true;
							}
							else
							{
								if (!value.Equals("null"))
									return true;
							}
						}
						else if (type == "Boolean")
						{
							var currentValue = (bool?)f.GetValue(obj);
							if (currentValue != null)
							{
								if (!value.Equals("null"))
								{
									if (currentValue != bool.Parse(value == "1" ? "true" : "false"))
										return true;
								}
								else
									return true;
							}
							else
							{
								if (!value.Equals("null"))
									return true;
							}
						}
						else if (type == "String")
						{
							var currentValue = (string)f.GetValue(obj);
							if (String.Compare(currentValue, value, StringComparison.InvariantCultureIgnoreCase) == 0)
								return true;
						}
						else
						{
							return false;
						}
					}
					else if (op == "GREATER_THAN")
					{
						if (type == "Integer")
						{
							var currentValue = (int)f.GetValue(obj);
							if (currentValue > Int32.Parse(value))
								return true;
						}
						else if (type == "Long")
						{
							var currentValue = (long)f.GetValue(obj);
							if (currentValue > Int64.Parse(value))
								return true;
						}
						else if (type == "Decimal")
						{
							var currentValue = (decimal)f.GetValue(obj);
							if (currentValue > Decimal.Parse(value))
								return true;
						}
						else if (type == "Date")
						{
							var currentValue = DateTime.Parse(f.GetValue(obj).ToString());
							if (currentValue > DateTime.Parse(value))
								return true;
						}
						else if (type == "Datetime")
						{
							var currentValue = DateTime.Parse(f.GetValue(obj).ToString());
							var compareValue = DateTime.Parse(value);
							if (DateTime.Compare(currentValue, compareValue) > 0)
								return true;
						}
						else
						{
							return false;
						}
					}
					else if (op == "GREATER_THAN_OR_EQUAL")
					{
						if (type == "Integer")
						{
							var currentValue = (int)f.GetValue(obj);
							if (currentValue >= Int32.Parse(value))
								return true;
						}
						else if (type == "Long")
						{
							var currentValue = (long)f.GetValue(obj);
							if (currentValue >= Int64.Parse(value))
								return true;
						}
						else if (type == "Decimal")
						{
							var currentValue = (decimal)f.GetValue(obj);
							if (currentValue >= Decimal.Parse(value))
								return true;
						}
						else if (type == "Date")
						{
							var currentValue = DateTime.Parse(f.GetValue(obj).ToString());
							if (currentValue >= DateTime.Parse(value))
								return true;
						}
						else if (type == "Datetime")
						{
							var currentValue = DateTime.Parse(f.GetValue(obj).ToString());
							var compareValue = DateTime.Parse(value);
							if (DateTime.Compare(currentValue, compareValue) >= 0)
								return true;
						}
						else
						{
							return false;
						}
					}
					else if (op == "LESS_THAN")
					{
						if (type == "Integer")
						{
							var currentValue = (int)f.GetValue(obj);
							if (currentValue < Int32.Parse(value))
								return true;
						}
						else if (type == "Long")
						{
							var currentValue = (long)f.GetValue(obj);
							if (currentValue < Int64.Parse(value))
								return true;
						}
						else if (type == "Decimal")
						{
							var currentValue = (decimal)f.GetValue(obj);
							if (currentValue < Decimal.Parse(value))
								return true;
						}
						else if (type == "Date")
						{
							var currentValue = DateTime.Parse(f.GetValue(obj).ToString());
							if (currentValue < DateTime.Parse(value))
								return true;
						}
						else if (type == "Datetime")
						{
							var currentValue = DateTime.Parse(f.GetValue(obj).ToString());
							var compareValue = DateTime.Parse(value);
							if (DateTime.Compare(currentValue, compareValue) < 0)
								return true;
						}
						else
						{
							return false;
						}
					}
					else if (op == "LESS_THAN_OR_EQUAL")
					{
						if (type == "Integer")
						{
							var currentValue = (int)f.GetValue(obj);
							if (currentValue <= Int32.Parse(value))
								return true;
						}
						else if (type == "Long")
						{
							var currentValue = (long)f.GetValue(obj);
							if (currentValue <= Int64.Parse(value))
								return true;
						}
						else if (type == "Decimal")
						{
							var currentValue = (decimal)f.GetValue(obj);
							if (currentValue <= Decimal.Parse(value))
								return true;
						}
						else if (type == "Date")
						{
							var currentValue = DateTime.Parse(f.GetValue(obj).ToString());
							if (currentValue <= DateTime.Parse(value))
								return true;
						}
						else if (type == "Datetime")
						{
							var currentValue = DateTime.Parse(f.GetValue(obj).ToString());
							var compareValue = DateTime.Parse(value);
							if (DateTime.Compare(currentValue, compareValue) <= 0)
								return true;
						}
						else
						{
							return false;
						}
					}
					else if (op == "IN")
					{
						if (type == "Integer")
						{
							var currentValue = (int)f.GetValue(obj);
							if (value.IndexOf(",") > 0)
							{
								string[] compareValues = value.Split(",");
								foreach (var compareValue in compareValues)
								{
									if (currentValue == Int32.Parse(compareValue))
									{
										return true;
									}
								}
							}
							else
							{
								if (currentValue == Int32.Parse(value))
									return true;
							}
						}
						else if (type == "Long")
						{
							var currentValue = (long)f.GetValue(obj);
							if (value.IndexOf(",") > 0)
							{
								string[] compareValues = value.Split(",");
								foreach (var compareValue in compareValues)
								{
									if (currentValue == Int64.Parse(compareValue))
									{
										return true;
									}
								}
							}
							else
							{
								if (currentValue == Int64.Parse(value))
									return true;
							}
						}
						else if (type == "Decimal")
						{
							var currentValue = (decimal)f.GetValue(obj);
							if (value.IndexOf(",") > 0)
							{
								string[] compareValues = value.Split(",");
								foreach (var compareValue in compareValues)
								{
									if (currentValue == Decimal.Parse(compareValue))
									{
										return true;
									}
								}
							}
							else
							{
								if (currentValue == Decimal.Parse(value))
									return true;
							}
						}
						else if (type == "String")
						{
							var currentValue = (string)f.GetValue(obj);
							if (value.IndexOf(",") > 0)
							{
								string[] compareValues = value.Split(",");
								foreach (var compareValue in compareValues)
								{
									if (String.Compare(currentValue, compareValue, StringComparison.InvariantCultureIgnoreCase) == 0)
									{
										return true;
									}
								}
							}
							else
							{
								if (String.Compare(currentValue, value, StringComparison.InvariantCultureIgnoreCase) == 0)
									return true;
							}
						}
						else
						{
							return false;
						}
					}
					else if (op == "NOT_IN")
					{
						if (type == "Integer")
						{
							var currentValue = (int?)f.GetValue(obj);
							if (currentValue != null)
							{
								if (value.IndexOf(",") > 0)
								{
									string[] compareValues = value.Split(",");
									bool flag = false;
									foreach (var compareValue in compareValues)
									{
										if (currentValue == Int32.Parse(compareValue))
										{
											flag = true;
										}
									}
									if (!flag)
									{
										return true;
									}
									return false;
								}
								else
								{
									if (currentValue != Int32.Parse(value))
										return true;
								}
							}
							else
							{
								return true;							
							}	
						}
						else if (type == "Long")
						{
							var currentValue = (long?)f.GetValue(obj);
							if (currentValue != null)
							{
								if (value.IndexOf(",") > 0)
								{
									string[] compareValues = value.Split(",");
									bool flag = false;
									foreach (var compareValue in compareValues)
									{
										if (currentValue == Int64.Parse(compareValue))
										{
											flag = true;
										}
									}
									if (!flag)
									{
										return true;
									}
									return false;
								}
								else
								{
									if (currentValue != Int64.Parse(value))
										return true;
								}
							}
							else
							{
								return true;
							}	
						}
						else if (type == "Decimal")
						{
							var currentValue = (decimal?)f.GetValue(obj);
							if (currentValue != null)
							{
								if (value.IndexOf(",") > 0)
								{
									string[] compareValues = value.Split(",");
									bool flag = false;
									foreach (var compareValue in compareValues)
									{
										if (currentValue == Decimal.Parse(compareValue))
										{
											flag = true;
										}
									}
									if (!flag)
									{
										return true;
									}
									return false;
								}
								else
								{
									if (currentValue != Decimal.Parse(value))
										return true;
								}
							}
							else
							{
								return true;
							}	
						}
						else if (type == "String")
						{
							var currentValue = (string)f.GetValue(obj);
							if (currentValue != null)
							{
								if (value.IndexOf(",") > 0)
								{
									string[] compareValues = value.Split(",");
									bool flag = false;
									foreach (var compareValue in compareValues)
									{
										if (String.Compare(currentValue, compareValue, StringComparison.InvariantCultureIgnoreCase) == 0)
										{
											flag = true;
										}
									}
									if (!flag)
									{
										return true;
									}
									return false;
								}
								else
								{
									if (String.Compare(currentValue, value, StringComparison.InvariantCultureIgnoreCase) != 0)
										return true;
								}
							}
							else
							{
								return true;
							}	
						}
						else
						{
							return false;
						}
					}
				}
				return false;
			}
			catch (Exception ex)
			{
				return false;
			}
		}
	}
}
