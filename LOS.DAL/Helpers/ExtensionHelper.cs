﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace LOS.DAL.Helpers
{
	public static class ExtensionHelper
	{
		public static bool HasProperty(this object obj, string propertyName)
		{
			return obj.GetType().GetProperty(propertyName) != null;
		}

        public static void SetProperty(this object obj, string propertyName, object value)
        {
            PropertyInfo propertyInfo = obj.GetType().GetProperty(propertyName);
            propertyInfo.SetValue(obj, value, null); 
        }
    }
}
