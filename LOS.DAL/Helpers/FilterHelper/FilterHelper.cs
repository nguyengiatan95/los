﻿using LOS.Common.Extensions;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.DAL.Helpers.FilterHelper
{
    public class FilterHelper<T>
    {
        private readonly Expression<Func<T, bool>> _filterExpression;

        internal FilterHelper(Expression<Func<T, bool>> filterExpression)
        {
            _filterExpression = filterExpression;
        }

        public static FilterHelper<T> Create(Expression<Func<T, bool>> expression)
        {
            return new FilterHelper<T>(expression);
        }

        public static FilterHelper<T> Create(string propertyName, EnumOperationType operation, object value)
        {
            return new FilterHelper<T>(ExpressionBuilder.Build<T>(propertyName, operation, value));
        }

        public static Expression<Func<T, bool>> CreateFilterDynamic(string propertyName, EnumOperationType operation, object value)
        {
            return ExpressionBuilder.Build<T>(propertyName, operation, value);
        }

        public static readonly FilterHelper<T> Nothing = new FilterHelper<T>(t => true);

        public virtual Expression<Func<T, bool>> Apply()
        {
            return _filterExpression;
        }
        public FilterHelper<T> And(FilterHelper<T> other) => new FilterHelper<T>(_filterExpression.And(other._filterExpression));

        public FilterHelper<T> AndProperty(string propertyName, EnumOperationType operation, object value)
        {
            var filter = FilterHelper<T>.Create(propertyName, operation, value);
            return new FilterHelper<T>(_filterExpression.And(filter._filterExpression));
        }

        public FilterHelper<T> OrProperty(string propertyName, EnumOperationType operation, object value)
        {
            var filter = FilterHelper<T>.Create(propertyName, operation, value);
            return new FilterHelper<T>(_filterExpression.Or(filter._filterExpression));
        }

        private FilterHelper<T> And(Expression<Func<T, bool>> other) => And(Create(other));

        private FilterHelper<T> And(string propertyName, EnumOperationType operation, object value) => And(Create(propertyName, operation, value));

        private FilterHelper<T> Or(FilterHelper<T> other) => new FilterHelper<T>(_filterExpression.Or(other._filterExpression));

        private FilterHelper<T> Or(Expression<Func<T, bool>> other) => Or(Create(other));

        private FilterHelper<T> Or(string propertyName, EnumOperationType operation, object value) => Or(Create(propertyName, operation, value));
    }
}
