﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Models.LMS
{
    public class CreateDistrict
    {
        public int CityId { get; set; }
        public string Name { get; set; }
        public string TypeDistrict { get; set; }
    }
}
