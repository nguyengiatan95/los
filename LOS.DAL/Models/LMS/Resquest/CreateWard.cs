﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Models.LMS
{
    public class CreateWard
    {
        public int DistrictID { get; set; }
        public string Name { get; set; }
        public string TypeWard { get; set; }
    }
}
