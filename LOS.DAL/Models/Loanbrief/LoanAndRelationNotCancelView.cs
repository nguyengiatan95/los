﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Models
{
  public  class LoanAndRelationNotCancelView
    {
        public int LoanbriefId { get; set; }
        public string Phone { get; set; }
        public string FullName { get; set; }
        public string RelationshipName { get; set; }
        public string LoanStatus { get; set; }
    }
}
