﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Models
{
    public class PaymentLoanById
    {
        public partial class OutPut
        {
            [JsonProperty("Data")]
            public Data Data { get; set; }

            [JsonProperty("Messages")]
            public string[] Messages { get; set; }

            [JsonProperty("Status")]
            public long Status { get; set; }

            [JsonProperty("Total")]
            public long Total { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("Loan")]
            public Loan Loan { get; set; }

        }

        public partial class Loan
        {
            [JsonProperty("ID")]
            public long Id { get; set; }


            [JsonProperty("TotalMoney")]
            public long TotalMoney { get; set; }

            [JsonProperty("TotalMoneyCurrent")]
            public long TotalMoneyCurrent { get; set; }

            public int TopUp { get; set; }

        }

        public partial class PaymentSchedule
        {
            [JsonProperty("FromDate")]
            public DateTimeOffset FromDate { get; set; }

            [JsonProperty("ToDate")]
            public DateTimeOffset ToDate { get; set; }

            [JsonProperty("PayDate")]
            public DateTimeOffset PayDate { get; set; }

            [JsonProperty("MoneyOriginal")]
            public long MoneyOriginal { get; set; }

            [JsonProperty("MoneyInterest")]
            public long MoneyInterest { get; set; }

            [JsonProperty("MoneyService")]
            public long MoneyService { get; set; }

            [JsonProperty("MoneyConsultant")]
            public long MoneyConsultant { get; set; }

            [JsonProperty("OtherMoney")]
            public long OtherMoney { get; set; }

            [JsonProperty("Done")]
            public bool Done { get; set; }

        }
    }

    public class DeferredPaymentLms
    {
        public partial class Input
        {
            [JsonProperty("CustomerName")]
            public string CustomerName { get; set; }

            [JsonProperty("NumberCard")]
            public string NumberCard { get; set; }

        }

        public partial class Output
        {
            [JsonProperty("Status")]
            public long Result { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("Data")]
            public Data Data { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("lstLoanCustomer")]
            public LstLoanCustomer[] LstLoanCustomer { get; set; }
        }
        public partial class LstLoanCustomer
        {
            [JsonProperty("LoanID")]
            public long LoanId { get; set; }

            [JsonProperty("LoanBriefID")]
            public long LoanBriefID { get; set; }

            [JsonProperty("ContactCode")]
            public string ContactCode { get; set; }

            [JsonProperty("TotalMoney")]
            public long TotalMoney { get; set; }

            [JsonProperty("TotalMoneyCurrent")]
            public long TotalMoneyCurrent { get; set; }

            [JsonProperty("FromDate")]
            public string FromDate { get; set; }

            [JsonProperty("ToDate")]
            public string ToDate { get; set; }

            [JsonProperty("FinishDate")]
            public string FinishDate { get; set; }

            [JsonProperty("StatusName")]
            public string StatusName { get; set; }

            [JsonProperty("Status")]
            public long Status { get; set; }

            [JsonProperty("CustomerName")]
            public string CustomerName { get; set; }

            [JsonProperty("ProductID")]
            public int ProductID { get; set; }


            [JsonProperty("LoanTime")]
            public long LoanTime { get; set; }

            [JsonProperty("lstPaymentCustomer")]
            public LstPaymentCustomer[] LstPaymentCustomer { get; set; }
        }
        public partial class LstPaymentCustomer
        {
            [JsonProperty("CustomerID")]
            public long CustomerId { get; set; }

            [JsonProperty("CustomerName")]
            public string CustomerName { get; set; }

            [JsonProperty("StrFromDate")]
            public string StrFromDate { get; set; }

            [JsonProperty("StrToDate")]
            public string StrToDate { get; set; }

            [JsonProperty("CountDay")]
            public long CountDay { get; set; }

            [JsonProperty("PayDate")]
            public DateTime? PayDate { get; set; }

            [JsonProperty("CompleteDate")]
            public DateTime? CompleteDate { get; set; }

            [JsonProperty("LoanID")]
            public long LoanId { get; set; }
        }
    }
}
