﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Models.Request.ChangePhone
{
    public class ChangePhone
    {
        public class ChangePhoneReq
        {
            public int LoanBriefId { get;set; }
            public string PhoneOld { get;set; }
            public string PhoneNew { get;set; }
        }
    }
}
