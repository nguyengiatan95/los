﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Models.Request.DrawImage
{
    public class DrawImageReq
    {
        public string CustomerName { get; set; }
        public string NationalCard { get; set; }
        public string UrlFileTemp { get; set; }
        public string Ssiid { get; set; }
        public List<InsuranceInfo> InsuranceInfo { get; set; }
    }
    public class InsuranceInfo
    {
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string Smonth { get; set; }
    }
}
