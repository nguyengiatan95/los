﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Models.Request.LoanBrief
{
    public class LoanBrief
    {
        public class UpdateTransactionStateReq
        {
            public int LoanBriefId { get; set; }
            public int Status { get; set; }
        }
    }
}
