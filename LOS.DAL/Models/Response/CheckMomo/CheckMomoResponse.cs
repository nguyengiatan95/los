﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Models.Response.CheckMomo
{
    public class CheckMomoResponse
    {
        public partial class Output
        {
            [JsonProperty("status")]
            public long Status { get; set; }

            [JsonProperty("data")]
            public Data Data { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("query_data")]
            public string QueryData { get; set; }

            [JsonProperty("unchecked")]
            public object[] Unchecked { get; set; }

            [JsonProperty("bills")]
            public List<Bill> Bills { get; set; }
        }

        public partial class Bill
        {
            [JsonProperty("bill_id")]
            public string BillId { get; set; }

            [JsonProperty("service_name")]
            public string ServiceName { get; set; }

            [JsonProperty("total_amount")]
            public long TotalAmount { get; set; }

            [JsonProperty("partner_name")]
            public string PartnerName { get; set; }

            [JsonProperty("partner_phone")]
            public string PartnerPhone { get; set; }

            [JsonProperty("partner_email")]
            public string PartnerEmail { get; set; }

            [JsonProperty("partner_personal_id")]
            public string PartnerPersonalId { get; set; }

            [JsonProperty("expired_date")]
            public string ExpiredDate { get; set; }

            [JsonProperty("status")]
            public string Status { get; set; }
        }
    }
}
