﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace LOS.DAL.Models.Response
{
    public class SercuredTransactionReq
    {
        [JsonProperty(PropertyName = "contract_number")]
        public string ContractNumber { get; set; }
        [JsonPropertyAttribute("contracting_day")]
        public string ContractingDay { get; set; }
        [JsonPropertyAttribute("obligation_amount")]
        public int LoanAmount { get; set; }

        [JsonPropertyAttribute("securing_parties")]
        public List<SecuringPartyItem> SecuringParties { get; set; }

        [JsonPropertyAttribute("description")]
        public string Description { get; set; }
        [JsonPropertyAttribute("extra_data")]
        public ExtraData ExtraData { get; set; }
       
    }

    public class ExtraData
    {
        [JsonPropertyAttribute("vehicle[1][vin_number_1]")]
        public string VehicleVin { get; set; }
        [JsonPropertyAttribute("vehicle[1][engine_number_1]")]
        public string VehicleEngine { get; set; }
        [JsonPropertyAttribute("vehicle[1][license_plates_1]")]
        public string VehiclePlate { get; set; }
       
    }
    public class SecuringPartyItem
    {
        [JsonPropertyAttribute("name")]
        public string Name { get; set; }
        [JsonPropertyAttribute("national_id_number")]
        public string NationalNumber { get; set; }
        [JsonPropertyAttribute("country_id")]
        public string CountryId { get; set; }
        [JsonPropertyAttribute("city_id")]
        public string CityId { get; set; }
        [JsonPropertyAttribute("district_id")]
        public string DistrictId { get; set; }
        [JsonPropertyAttribute("address")]
        public string Address { get; set; }
    }
    public class SercuredTransactionRes
    {
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("data")]
        public SercuredTransactionData Data { get; set; }

    }

    public class SercuredTransactionData
    {
        [JsonProperty("notice_number")]
        public string NoticeNumber { get; set; }
        [JsonProperty("pin")]
        public string Pin { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("status_code")]
        public int StatusCode { get; set; }

    }
}
