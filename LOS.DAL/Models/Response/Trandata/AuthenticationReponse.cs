﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Models.Response.Trandata
{
    public class AuthenticationReponse
    {
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public AuthenticationReponseData Data { get; set; }
    }

    public class AuthenticationReponseData
    {
        [JsonProperty("username")]
        public string UserName { get; set; }
        [JsonProperty("sessionToken")]
        public string SessionToken { get; set; }
    }
}
