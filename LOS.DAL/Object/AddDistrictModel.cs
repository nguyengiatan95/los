﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class AddDistrictModel
    {
        public int ProvinceId { get; set; }
        public string DistrictName { get; set; }
        public string LatLong { get; set; }
        public int TypeDistrict { get; set; }
    }
}
