﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class AddWardModel
    {
        public int DistrictId { get; set; }
        public string WardName { get; set; }
        public int TypeWard { get; set; }
    }
}
