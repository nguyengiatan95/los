﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object.Appsettings
{
   public class TrandataSettings
    {
        public string Domain { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
