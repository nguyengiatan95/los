﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class GetLogDistributionUserModel
    {
        public List<int> LstLoanBriefId { get; set; }
        public int TypeDistribution { get; set; }
    }
}
