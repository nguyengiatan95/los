﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class LoanCreditForLeadScoreItem
    {
        public decimal TotalMoney { get; set; }
        public int LoanTime { get; set; }
        public int? RateType { get; set; }
        public int ProductID { get; set; }
        public string FullName { get; set; }
        public string CardNumber { get; set; }
        public string Phone { get; set; }
        public int? Gender { get; set; }
        public decimal? Salary { get; set; }
        public int? JobId { get; set; }
        public string JobName { get; set; }
        public int? TypeReceivingMoney { get; set; }
        public int RelativeFamilyId { get; set; }
        public bool? ReMarketing { get; set; }
        public int? IsMarried { get; set; }
        public int? NumberBaby { get; set; }
        public int? CustomerAge { get; set; }
        public bool HasBadDebt { get; set; }
        public int? LoanAgain { get; set; }
        public string CityName { get; set; }
        public string PhoneFamily { get; set; }
        public bool HasLatePayment { get; set; }
        public int? ReceiveYourIncome { get; set; }
        public string TypeIncomeName { get; set; }
        public int? IsLivingTogether { get; set; }
         public int? Code { get; set; }
        public int? TypeOfOwnershipId { get; set; }        
        public string CICName { get; set; }

        public decimal LoanAmountExpertiseAI { get; set; }



    }
    public class CreditScoringResult
    {
        public string label { get; set; }
        public decimal score { get; set; }
        public string version { get; set; }
    }

    public class LeadScoreItem
    {
        public int LoanCreditId { get; set; }
        public int LoanTime { get; set; }
        public int Status { get; set; }
        public string CreateDate { get; set; }
        public int TypeReceivingMoney { get; set; }
        public string NameBanking { get; set; }
        public string NumberCardBanking { get; set; }
        public string AccountNumberBanking { get; set; }
        public int TypeID { get; set; }
        public string utm_source { get; set; }
        public string utm_medium { get; set; }
        public string utm_campaign { get; set; }
        public int TypeTime { get; set; }
        public int CityId { get; set; }
        public decimal OriginalPrice { get; set; }
        public decimal SellPrice { get; set; }
        public string utm_content { get; set; }
        public string utm_term { get; set; }
        public int Step { get; set; }
        public string UserAgent { get; set; }
        public int OriginalTypeID { get; set; }
        public int OriginalCityId { get; set; }
        public string VehicleName { get; set; }
        public int VehicleYear { get; set; }
        public int ProductPawnInfo { get; set; }
        public string ProductPawName { get; set; }
        public string FullName { get; set; }
        public string Birthday { get; set; }
        public string Phone { get; set; }
        public string CardNumber { get; set; }
        public int? Gender { get; set; }
        public string Email { get; set; }
        public string Street { get; set; }
        public int? JobId { get; set; }
        public int? WorkingIndustryId { get; set; }
        public decimal? Salary { get; set; }
    }
}