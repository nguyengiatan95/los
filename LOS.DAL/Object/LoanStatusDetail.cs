﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class LoanStatusDetail
    {
        public class NextStepLoanHub
        {
            public int LoanBriefId { get; set; }
            public int StatusDetail { get; set; }
            public int StatusDetailChild { get; set; }
            public string NoteLoanStatusDetail { get; set; }
        }
    }
}
