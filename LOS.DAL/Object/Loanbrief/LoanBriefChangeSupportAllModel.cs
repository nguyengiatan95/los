﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class LoanBriefChangeSupportAllModel
    {
        public string provinceId { get; set; }
        public string productId { get; set; }
        public string status { get; set; }
        public string search { get; set; }
        public string loanBriefId { get; set; }
        public string filterSource { get; set; }
        public string filtercreateTime { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string actionId { get; set; }
        public int? boundTelesaleId { get; set; }
        public string countCall { get; set; }
        public string filterClickTop { get; set; }
        public string detailStatusTelesales { get; set; }
        public string statusTelesales { get; set; }
        public string filterStaffTelesales { get; set; }
        public string teamTelesales { get; set; }
        public string team { get; set; }
        public string dateRangerLastChangeStatus { get; set; }
        public string FromDateLastChangeStatus { get; set; }
        public string ToDateLastChangeStatus { get; set; }
        public int? groupId { get; set; }
        public string utmSource { get; set; }
        public string filterRecare { get; set; }
        public int? userId { get; set; }
    }
}
