﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class LoanBriefFeedback
    {
        public int LoanBriefId { get; set; }
        public int CountCall { get; set; } // Số lần gọi của KH
        public DateTime? HubEmployeeCallFirst { get; set; } //Thời gian hub call cho KH đầu tiên
        public DateTime? FirstTimeHubFeedBack { get; set; } //Thời gian hub phản hồi đầu tiên
        public DateTime? FirstProcessingTime { get; set; } //Thời gian TLS phản hồi đầu tiên (comment/ call)
        public DateTime? LastProcessingTime { get; set; } //Thời gian TLS phản hồi cuối (comment/ call)
        public DateTime? FirstTimeStaffHubFeedback { get; set; } //Thời gian nhân viên hub phản hồi đầu tiên (cmt/ call)
    }
}
