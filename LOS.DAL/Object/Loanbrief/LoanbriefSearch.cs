﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class LoanbriefSearch
    {
        public int LoanBriefId { get; set; }
        public int? ProductId { get; set; }
        public string FullName { get; set; }
        //public int? Gender { get; set; }
        public string Phone { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? LoanTime { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string UtmSource { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public int? Status { get; set; }
        public bool? IsLocate { get; set; }
        public int? CoordinatorUserId { get; set; }
        public int? HubEmployeeId { get; set; }
        public int? BoundTelesaleId { get; set; }
        public int? InProcess { get; set; }
        public bool? IsReborrow { get; set; }
        public bool? IsTrackingLocation { get; set; }
        public DateTime? ScheduleTime { get; set; }
        public int? StatusTelesales { get; set; }
        public int? DetailStatusTelesales { get; set; }
        //public int? CodeId { get; set; }
        public int? HubId { get; set; }
        public string LenderName { get; set; }

        public string LoanProductName { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }

        public string HubStaffFullName { get; set; }
        public string HubStaffUserName { get; set; }

        public string CoordinatorFullName { get; set; }
        public string CoordinatorUserName { get; set; }

        public string TelesaleFullName { get; set; }
        public string TelesaleUserName { get; set; }
        public string HubName { get; set; }
        public int TypeRemarketing { get; set; }
        public string DeviceId { get; set; }
    }
}
