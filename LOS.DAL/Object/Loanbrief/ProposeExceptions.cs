﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object.Loanbrief
{
    public class ProposeExceptions
    {
        public int LoanBriefId { get; set; }
        public int Status { get; set; }
        public bool IsPushPipeline { get; set; }
        public int DocId { get; set; }
        public decimal LoanAmount { get; set; }
    }

    public class ProposeExceptionsInit
    {
        public int LoanBriefId { get; set; }
        public List<DocumentException> DocumentException { get; set; }
        public DAL.EntityFramework.ProposeExceptions ProposeExceptions { get; set; }
    }
}
