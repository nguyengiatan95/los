﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
   public class NotificationSystem
    {
        public string UserName { get; set; }
        public string Message { get; set; }
        public int loanbriefId { get; set; }
    }
}
