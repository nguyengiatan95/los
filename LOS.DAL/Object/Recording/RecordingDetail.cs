﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class RecordingDetail
    {
        public DateTime? StartTime { get; set; }
        public int? CallDuration { get; set; }
        public string Line { get; set; }
        public string AgentUserName { get; set; }
        public string RecordingUrl { get; set; }
        public string PlayUrl { get; set; }
        public string PhoneNumber { get; set; }
        public string ContactType { get; set; }
    }
}
