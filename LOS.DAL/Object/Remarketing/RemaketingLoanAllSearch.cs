﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class RemaketingLoanAllSearch
    {
        public int Status { get; set; }
        public int? ReasonCancel { get; set; }
        public int? DetailReason { get; set; }
        public string strCreateDateRanger { get; set; }
        public string FromCreateDate { get; set; } 
        public string ToCreateDate { get; set; } 
        public string strCancelTimeRanger { get; set; }
        public string FromCancelTime { get; set; }
        public string ToCancelTime { get; set; }
        public string TextSearch { get; set; }
        public int? Source { get; set; }
        public string UtmSource { get; set; }
    }
}
