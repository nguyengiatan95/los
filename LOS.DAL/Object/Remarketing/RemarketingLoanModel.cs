﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
   public class RemarketingLoanModel
    {
        public List<int> ListLoanbrief { get; set; }
        public int TypeRemarketing { get; set; }
        public string UtmSource { get; set; }
        public int UserId { get; set; }
        public int TypeReLoanbrief { get; set; }
        public int? TelesalesId { get; set; }
        public int? HubId { get; set; }
    }
}
