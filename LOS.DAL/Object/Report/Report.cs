﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object.Report
{
    public class Report
    {
        public class ReportApproveEmployee
        {
            public int UserId { get; set; }
            public string FullName { get; set; }
            public int Disbursement { get; set; }
            public int Finish { get; set; }
            public int Other { get; set; }
            public int TotalPush { get; set; }
        }

        public class ReportEsignCustomer
        {
            public int LoanBriefId { get; set; }
            public string AgreementId { get; set; }
            public DateTime CreatedAt { get; set; }
            public string FullName { get; set; }
            public string Phone { get; set; }
        }
    }
}
