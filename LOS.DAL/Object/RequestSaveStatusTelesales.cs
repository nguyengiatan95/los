﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class RequestSaveStatusTelesales
    {
        public int LoanBriefId { get; set; }
        public int StatusTelesales { get; set; }
        public int StatusTelesalesDetail { get; set; }
        public string CommentStatusTelesales { get; set; }
        public string TelesaleComment { get; set; }
    }
}
