﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class ResultFraudCheckModel
    {
        public int loan_brief_id { get; set; }
        public TimaResult tima_result { get; set; }
        public HypervergeResult? hyperverge_result { get; set; }
    }

    public class DetailMessage
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class UnifiedResult
    {
        public string channel { get; set; }
        public List<DetailMessage> details { get; set; }
    }

    public class UnifiedHypervResult
    {
        public string channel { get; set; }
        public DetailMessage details { get; set; }
    }

    public class TimaResult
    {
        public bool? diff_id_db { get; set; }
        public bool? diff_name_db { get; set; }
        public bool? diff_birthday_db { get; set; }
        public bool? diff_face_input { get; set; }
        public bool? diff_type_id { get; set; }
        public double? face_sim_score { get; set; }
        public bool? diff_province_nationalid_declare { get; set; }
        public bool? wrong_format_national_id { get; set; }
        public string province_house_hold { get; set; }
        public bool? is_expired_date_birth_date_invalid { get; set; }
        public bool? is_expired_date_issue_date_invalid { get; set; }
        public int? phone_appear_in_refer_phone { get; set; }
        public bool? is_fake_id { get; set; }
        public bool? is_not_match_info_with_old_cus { get; set; }
        public bool? is_used_vr { get; set; }
        public bool? same_face_diff_info { get; set; }
        public bool? is_old_loan_bad { get; set; }
        public UnifiedResult unified_result { get; set; }
        public string report_url { get; set; }
    }

    public class VerificationPerformed
    {
        public string value { get; set; }
        public string reason { get; set; }
    }

    public class EnrolPerformed
    {
        public string value { get; set; }
        public string reason { get; set; }
    }

    public class MatchScore
    {
        public double? idFace { get; set; }
        public double? selfie { get; set; }
    }

    public class Images
    {
        public string idFace { get; set; }
        public string selfie { get; set; }
    }

    public class Mismatch
    {
        public string clientId { get; set; }
        public string idNumber { get; set; }
        public string dob { get; set; }
        public string name { get; set; }

        [JsonProperty("match-score")]
        public MatchScore MatchScore { get; set; }
        public Images images { get; set; }
        public string reason { get; set; }
    }

    public class UnifiedResult2
    {
        public string channel { get; set; }
        public DetailMessage details { get; set; }
    }

    public class HypervergeResult
    {
        public string applicationId { get; set; }
        public string requestId { get; set; }
        public VerificationPerformed verificationPerformed { get; set; }
        public EnrolPerformed enrolPerformed { get; set; }
        public int? mismatchCount { get; set; }
        public int? matchCount { get; set; }
        public List<Mismatch> mismatches { get; set; }
        public UnifiedHypervResult unifiedResult { get; set; }
    }

   
}
