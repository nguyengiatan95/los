﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
   public class F2LModel
    {
        public int TelesaleId { get; set; }
        public int? Form { get; set; }
        public int? Lead { get; set; }
    }
}
