﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object
{
    public class TimeProcessingLoanBrief
    {
        public int LoanBriefId { get; set; }
        public int TimeProcessing { get; set; }//số phút xử lý đơn
        public bool IsFeedBack { get; set; }
    }
}
