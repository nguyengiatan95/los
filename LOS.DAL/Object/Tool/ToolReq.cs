﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.DAL.Object.Tool
{
    public class ToolReq
    {
        public class ChangePhoneReq
        {
            public int LoanBriefId { get; set; }
            public string Phone { get; set; }
        }

        public class ReEsignReq
        {
            public int LoanBriefId { get; set; }
            public int? EsignNumber { get; set; }
        }
    }
}
