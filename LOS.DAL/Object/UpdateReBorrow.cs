﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.DAL.Object
{
    public class UpdateReBorrow
    {
        public int LoanBriefId { get; set; }
        public bool? IsReBorrow { get; set; }
    }
}
