﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Procedure
{
	public interface IProcedureManage<T>
	{			
		void ExecuteNonQuery(string commandText, CommandType commandType, SqlParameter[] parameters = null);
		T ExecuteReader(string commandText, CommandType commandType, SqlParameter[] parameters = null);
	}
}
