﻿using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Procedure
{
	public class ProcedureManager<T> : IDisposable where T : class
	{
		private LOSContext context;

		public ProcedureManager(LOSContext context)
		{
			this.context = context;
		}


		public void ExecuteNonQuery(string commandText, CommandType commandType, SqlParameter[] parameters = null)
		{
			if (context.Database.GetDbConnection().State == ConnectionState.Closed)
			{
				context.Database.GetDbConnection().Open();
			}

			var command = context.Database.GetDbConnection().CreateCommand();
			command.CommandText = commandText;
			command.CommandType = commandType;

			if (parameters != null)
			{
				foreach (var parameter in parameters)
				{
					command.Parameters.Add(parameter);
				}
			}

			command.ExecuteNonQuery();
		}

		public List<T> ExecuteReader(string commandText, CommandType commandType, SqlParameter[] parameters = null)
		{
			if (context.Database.GetDbConnection().State == ConnectionState.Closed)
			{
				context.Database.GetDbConnection().Open();
			}

			var command = context.Database.GetDbConnection().CreateCommand();
			command.CommandText = commandText;
			command.CommandType = commandType;

			if (parameters != null)
			{
				foreach (var parameter in parameters)
				{
					command.Parameters.Add(parameter);
				}
			}

			using (var reader = command.ExecuteReader())
			{
				var mapper = new DataReaderMapper<T>();
				return mapper.MapToList(reader);
			}
		}
		
		private bool disposed = false;
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					context.Dispose();
				}
			}
			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true); //I am calling you from Dispose, it's safe
			GC.SuppressFinalize(this); //Hey, GC: don't bother calling finalize later
		}
	}
}
