﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers.FilterHelper;
using LOS.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LOS.DAL.Respository
{
    public class ApproveRepository : IApproveRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public ApproveRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public GetDisbursementAfter GetDisbursementAfter(int telesaleId, int hubId, int provinceId, int loanBriefId, int status, int productId,
            int locate, int borrowCavet, int typeSearch, int topup, DateTime fromDate, DateTime toDate, string search, int empHubId, int coordinatorUserId, int page, int pageSize, ref int recordsTotal)
        {
            var data = new GetDisbursementAfter();
            try
            {
                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done && (x.Status == EnumLoanStatus.DISBURSED.GetHashCode()));
                if (loanBriefId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LoanBriefId == loanBriefId));

                if (!string.IsNullOrEmpty(search))
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Phone.Contains(search) || x.NationalCard.Contains(search) || x.FullName.Contains(search)));

                if (provinceId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ProvinceId == provinceId));

                if (productId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ProductId == productId));

                if (hubId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.HubId == hubId));

                if (topup > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.TypeLoanSupport == topup));

                if (telesaleId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.BoundTelesaleId == telesaleId));

                if (empHubId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.HubEmployeeId == empHubId));

                if (coordinatorUserId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.CoordinatorUserId == coordinatorUserId));

                if (borrowCavet == (int)EnumReCareLoanbrief.BorrowCavet)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ReCareLoanbrief == (int)EnumReCareLoanbrief.BorrowCavet));
                else if (borrowCavet == (int)EnumReCareLoanbrief.NotBorrowCavet)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ReCareLoanbrief != (int)EnumReCareLoanbrief.BorrowCavet));
                else if (borrowCavet == (int)EnumReCareLoanbrief.PostRegistration)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LoanBriefProperty.PostRegistration == true));

                //là hợp đồng giải ngân và 
                if (typeSearch == -1 && topup == -1)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.DisbursementAt >= fromDate && x.DisbursementAt < toDate));
                if (typeSearch == 1)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ToDate >= fromDate && x.ToDate < toDate));

                if (status > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.IsUploadState == Convert.ToBoolean(status)));

                if (locate > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.IsLocate == Convert.ToBoolean(locate)));

                var query = _unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefDetail.ProjectionDetail);
                recordsTotal = query.Count();

                //gán dữ liệu về object
                if (recordsTotal > 0)
                    data.TotalMoney = query.Sum(x => x.LoanAmount.Value);
                List<LoanBriefDetail> lstData;
                lstData = query.OrderByDescending(x => x.DisbursementAt).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                data.lstLoan = lstData;
            }
            catch (Exception ex)
            {
            }
            return data;
        }
    }
}
