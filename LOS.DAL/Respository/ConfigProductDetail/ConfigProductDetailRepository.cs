﻿using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class ConfigProductDetailRepository : IConfigProductDetailRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public ConfigProductDetailRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<ConfigProductCreditDetail> GetConfigProductCreditDetail(int productId, int productDetailId, int typeOwnership)
        {
            try
            {
                return await _unitOfWork.ConfigProductDetailRepository.Query(x => x.ProductId == productId &&
                             x.InfomationProductDetailId == productDetailId && x.TypeOwnership == typeOwnership && x.IsEnable == true, null, false).Select(x => new ConfigProductCreditDetail()
                             {
                                 Id = x.ConfigProductDetailId,
                                 ProductDetailId = x.InfomationProductDetail.Id,
                                 Name = x.InfomationProductDetail.Name,
                                 MaxMoney = x.MaxMoney
                             }).FirstOrDefaultAsync();
            }
            catch
            {
                return null;
            }

        }

        public async Task<List<ConfigProductCreditDetail>> GetConfigProductCreditDetailNext(int productId, int typeOwnership, long maxMoney)
        {
            try
            {
                return await _unitOfWork.ConfigProductDetailRepository.Query(x => x.ProductId == productId
               && x.MaxMoney > maxMoney
               && x.TypeOwnership == typeOwnership && x.IsEnable == true, null, false).Select(x => new ConfigProductCreditDetail()
               {
                   Id = x.ConfigProductDetailId,
                   ProductDetailId = x.InfomationProductDetail.Id,
                   Name = x.InfomationProductDetail.Name,
                   MaxMoney = x.MaxMoney
               }).OrderBy(x=>x.MaxMoney).ToListAsync();
            }
            catch
            {
                return null;
            }
        }
    }
}
