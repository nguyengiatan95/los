﻿using LOS.DAL.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public interface IConfigProductDetailRepository
    {
        Task<ConfigProductCreditDetail> GetConfigProductCreditDetail(int productId, int productDetailId, int typeOwnership);
        Task<List<ConfigProductCreditDetail>> GetConfigProductCreditDetailNext(int productId, int typeOwnership, long maxMoney);
    }
}