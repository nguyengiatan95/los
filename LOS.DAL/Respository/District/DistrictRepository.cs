﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class DistrictRepository : IDistrictRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public DistrictRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public List<DistrictDetail> Search(string searchName, int provinceId, int status, int page, int pageSize, ref int recordsTotal)
        {
            var lstData = new List<DistrictDetail>();
            try
            {
                var query = _unitOfWork.DistrictRepository.Query(x => x.ProvinceId == provinceId && (x.IsApply.GetValueOrDefault(0) == status || status == -1)
                    && (searchName == null || x.Name.ToLower().Contains(searchName.ToLower())), null, false)
                    .Select(DistrictDetail.ProjectionDetail).ToList();

                recordsTotal = query.Count();
                if (query != null && query.Count > 0)
                    //phân trang
                    lstData = query.OrderBy(x => x.Priority).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            catch (Exception ex)
            {
            }
            return lstData;
        }

        public async Task<bool> UpdateIsApplyDistrict(int districtId, int isApply)
        {
            try
            {
                //kiểm tra xem có tồn tại thành phố không
                if (_unitOfWork.DistrictRepository.Any(x => x.DistrictId == districtId))
                {
                    _unitOfWork.DistrictRepository.Update(x => x.DistrictId == districtId, x => new District()
                    {
                        IsApply = Convert.ToInt16(isApply)
                    });
                    await _unitOfWork.SaveAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public async Task<bool> Add(DAL.EntityFramework.District entity)
        {
            if (entity != null && entity.DistrictId > 0)
            {
                _unitOfWork.DistrictRepository.Insert(entity);
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> Update(DAL.EntityFramework.District entity)
        {
            if (entity != null && entity.DistrictId > 0)
            {
                _unitOfWork.DistrictRepository.Update(x => x.DistrictId == entity.DistrictId, x => new DAL.EntityFramework.District()
                {
                    Name = entity.Name,
                    Type = entity.Type,
                    LatLong = entity.LatLong,
                });
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }

        public async Task<List<DistrictDetail>> GetDistrictByProvinceId(int provinceId)
        {
            var lstData = new List<DistrictDetail>();
            try
            {
                lstData = await _unitOfWork.DistrictRepository.Query(x => x.ProvinceId == provinceId, null, false)
                    .Select(DistrictDetail.ProjectionDetail).ToListAsync();

            }
            catch (Exception ex)
            {
            }
            return lstData;
        }
        public async Task<int> GetProvinceIdByDistrictId(int districtId)
        {
            return await _unitOfWork.DistrictRepository.Query(x => x.DistrictId == districtId, null, false).
                Select(x => x.ProvinceId.Value).FirstOrDefaultAsync();
        }

        public async Task<bool> CheckDistrict(string name, int provinceId)
        {
            if (_unitOfWork.DistrictRepository.Any(x => x.Name.ToLower() == name.ToLower() && x.ProvinceId == provinceId))
                return false;
            else
                return true;
        }
    }
}
