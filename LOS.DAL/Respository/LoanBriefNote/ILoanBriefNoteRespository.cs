﻿using LOS.DAL.EntityFramework;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public interface ILoanBriefNoteRespository
    {
        Task<bool> Add(LoanBriefNote entity);
    }
}