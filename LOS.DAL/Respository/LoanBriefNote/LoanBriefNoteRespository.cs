﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class LoanBriefNoteRespository : ILoanBriefNoteRespository
    {
        private readonly IUnitOfWork _unitOfWork;
        public LoanBriefNoteRespository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<bool> Add(LoanBriefNote entity)
        {
            if (entity != null)
            {
                _unitOfWork.LoanBriefNoteRepository.Insert(entity);
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
    }
}
