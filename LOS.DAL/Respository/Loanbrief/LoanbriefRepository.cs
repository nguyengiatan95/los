﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.DTOs.Loanbrief;
using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers.FilterHelper;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class LoanbriefRepository : ILoanbriefRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public LoanbriefRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<List<LoanbriefNotCancelDetail>> ListLoanbriefNotCancel(string phone)
        {
            return await _unitOfWork.LoanBriefRepository.Query(x => x.Status != (int)EnumLoanStatus.CANCELED && (x.Phone == phone
                            || (!string.IsNullOrEmpty(x.Phone) && x.PhoneOther == phone)), null, false)
                                .Select(LoanbriefNotCancelDetail.Projection).ToListAsync();
        }

        public async Task<LoanBriefBasicInfo> GetBasicInfo(int loanbriefId)
        {
            return await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false)
                .Select(LoanBriefBasicInfo.ProjectionViewDetail).FirstOrDefaultAsync();
        }

        public async Task<bool> CancelEsign(int loanBriefId)
        {
            if (loanBriefId > 0)
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    EsignBorrowerContract = null,
                    EsignLenderContract = null,
                    EsignState = (int)EsignState.READY_TO_SIGN
                });
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> CancelExceptions(int loanBriefId)
        {
            if (loanBriefId > 0)
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    IsExceptions = null,
                    ExceptionsId = null
                });
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }

        public async Task<LoanBriefDisbursementContractView> GetDisbursementContract(int provinceId, int loanBriefId, int productId, int topup, DateTime fromDate,
            DateTime toDate, string search, int userId, int? groupId, int page, int pageSize)
        {
            var data = new LoanBriefDisbursementContractView();
            try
            {
                //nếu là tài khoản spt_hangmt và spt_linhht thì phải search HĐ cụ thể mới cho xem danh sách
                if (userId == (int)EnumUser.spt_hangmt || userId == (int)EnumUser.spt_linhht
                    || userId == (int)EnumUser.spt_dutd1 || userId == (int)EnumUser.spt_anhntv || groupId == (int)EnumGroupUser.Telesale)
                {
                    if (loanBriefId > 0 || !string.IsNullOrEmpty(search))
                    {
                        var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done && (x.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                        && x.DisbursementAt >= fromDate && x.DisbursementAt < toDate);
                        if (loanBriefId > 0)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LoanBriefId == loanBriefId));

                        else if (!string.IsNullOrEmpty(search))
                        {
                            var textSearch = search.Trim();
                            if (ConvertExtensions.IsNumber(textSearch)) //Nếu là số
                            {
                                if (textSearch.Length == (int)NationalCardLength.CMND_QD || textSearch.Length == (int)NationalCardLength.CMND || textSearch.Length == (int)NationalCardLength.CCCD)
                                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.NationalCard == textSearch || x.NationCardPlace == textSearch)));
                                else//search phone
                                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Phone == textSearch || x.PhoneOther == textSearch)));
                            }
                            else//nếu là text =>search tên
                                filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.FullName.Contains(search)));
                        }
                        if (provinceId > 0)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ProvinceId == provinceId));

                        if (productId > 0)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ProductId == productId));

                        if (topup > 0)
                        {
                            //đơn có thể topup
                            if (topup == (int)FilterTopupDisbursementContract.Topup)
                                filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.TypeLoanSupport == (int)EnumTypeLoanSupport.IsCanTopup));
                            //Đơn DPD <=8 
                            else if (topup == (int)FilterTopupDisbursementContract.DPD)
                                filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.TypeLoanSupport == (int)EnumTypeLoanSupport.DPD));
                        }
                        var query = await _unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefDisbursementContractDetail.ProjectionDetail).ToListAsync();
                        data.TotalCount = query.Count();
                        //gán dữ liệu về object
                        data.ListDisbursementContractDetail = query.OrderByDescending(x => x.DisbursementAt).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    }
                    else
                        return data;
                }
                else
                {
                    var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done && (x.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                    && x.DisbursementAt >= fromDate && x.DisbursementAt < toDate /*&& !lstLoanBriefCreated.Contains(x.LoanBriefId)*/);
                    if (loanBriefId > 0)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LoanBriefId == loanBriefId));
                    else
                    {
                        //lấy danh sách đơn đã tạo trong vòng 15 ngày
                        var lstLoanBriefCreated = _unitOfWork.LogReLoanbriefRepository.Query(x => x.LoanbriefId > 0
                            && x.CreatedAt.Value.AddDays(15) >= DateTime.Now
                            && (x.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefTopup.GetHashCode() || x.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefDpd.GetHashCode()), null, false).Select(x => x.LoanbriefId.Value);
                        //Kiểm tra chỉ lấy những đơn đã tạo trên 15 ngày
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => !lstLoanBriefCreated.Contains(x.LoanBriefId)));
                        if (!string.IsNullOrEmpty(search))
                        {
                            var textSearch = search.Trim();
                            if (ConvertExtensions.IsNumber(textSearch)) //Nếu là số
                            {
                                if (textSearch.Length == (int)NationalCardLength.CMND_QD || textSearch.Length == (int)NationalCardLength.CMND || textSearch.Length == (int)NationalCardLength.CCCD)
                                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.NationalCard == textSearch || x.NationCardPlace == textSearch)));
                                else//search phone
                                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Phone == textSearch || x.PhoneOther == textSearch)));
                            }
                            else//nếu là text =>search tên
                                filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.FullName.Contains(search)));
                        }
                    }

                    if (provinceId > 0)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ProvinceId == provinceId));

                    if (productId > 0)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ProductId == productId));

                    if (topup > 0)
                    {
                        //đơn có thể topup
                        if (topup == (int)FilterTopupDisbursementContract.Topup)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.TypeLoanSupport == (int)EnumTypeLoanSupport.IsCanTopup));
                        //đơn của tls đã nghỉ
                        else if (topup == (int)FilterTopupDisbursementContract.TlsOff)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.BoundTelesale != null
                            || x.BoundTelesaleId == (int)EnumUser.system_team || x.BoundTelesaleId == (int)EnumUser.follow_team
                            || x.BoundTelesaleId == (int)EnumUser.follow || x.BoundTelesaleId == (int)EnumUser.autocall_coldlead)
                            && x.BoundTelesale.Status != (int)EnumStatusBase.Active
                            ));
                        //Đơn DPD <=8 
                        else if (topup == (int)FilterTopupDisbursementContract.DPD)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.TypeLoanSupport == (int)EnumTypeLoanSupport.DPD));
                    }

                    var query = await _unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefDisbursementContractDetail.ProjectionDetail).ToListAsync();
                    data.TotalCount = query.Count();
                    //gán dữ liệu về object
                    data.ListDisbursementContractDetail = query.OrderByDescending(x => x.DisbursementAt).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                }
            }
            catch (Exception ex)
            {
            }
            return data;
        }

        public async Task<bool> CheckLoanBriefTopupProcessing(int loanBriefId)
        {
            if (loanBriefId > 0)
            {
                return await _unitOfWork.LoanBriefRepository.AnyAsync(x => x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp
                && x.ReMarketingLoanBriefId == loanBriefId && x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.FINISH);
            }
            return false;
        }
        public async Task<bool> UpdateBoundTelesaleId(int loanBriefId, int telesaleId)
        {
            if (loanBriefId > 0)
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    BoundTelesaleId = telesaleId,
                });

                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
        public async Task<bool> UpdateDevice(int loanBriefId, int deviceStatus, string contractGinno, string deviceId)
        {
            if (loanBriefId > 0)
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    DeviceId = deviceId,
                    DeviceStatus = deviceStatus,
                    ContractGinno = contractGinno ?? null,
                    LastSendRequestActive = DateTime.Now,
                });

                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
        public async Task<bool> UpdateTypeLoanSupportByList(List<int> lstLoanBriefId)
        {
            if (lstLoanBriefId != null && lstLoanBriefId.Count > 0)
            {
                _unitOfWork.LoanBriefRepository.Update(x => lstLoanBriefId.Contains(x.LoanBriefId), x => new LoanBrief()
                {
                    TypeLoanSupport = null,
                });
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
        public async Task<List<LoanBriefPrematureInterest>> GetListLoanBriefByPhoneOrNationalCard(string text_search)
        {
            var lstData = new List<LoanBriefPrematureInterest>();
            try
            {
                //tìm theo sđt
                if (text_search.Length == 10)
                    lstData = await _unitOfWork.LoanBriefRepository.Query(x => x.Phone == text_search && (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH), null, false)
                        .OrderByDescending(x => x.LoanBriefId).Select(LoanBriefPrematureInterest.ProjectionDetail).ToListAsync();
                //tìm theo số cmnd
                else
                    lstData = await _unitOfWork.LoanBriefRepository.Query(x => x.NationalCard == text_search && (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH), null, false)
                        .OrderByDescending(x => x.LoanBriefId).Select(LoanBriefPrematureInterest.ProjectionDetail).ToListAsync();

            }
            catch (Exception ex)
            {
            }
            return lstData;
        }
        public async Task<bool> CloseLoan(int loanbriefId, DateTime finishAt)
        {
            if (loanbriefId > 0)
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbriefId, x => new LoanBrief()
                {
                    Status = (int)EnumLoanStatus.FINISH,
                    PipelineState = -1,
                    FinishAt = finishAt,
                });

                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
        public async Task<bool> UpdateTypeLoanSupport(int loanbriefId, int typeLoanSupport)
        {
            if (loanbriefId > 0)
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbriefId, x => new LoanBrief()
                {
                    TypeLoanSupport = typeLoanSupport,
                });
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
        public async Task<bool> RestoreTopup(int loanBriefId)
        {
            if (loanBriefId > 0)
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    Status = (int)EnumLoanStatus.INIT,
                    CurrentPipelineId = null,
                    PipelineState = 0,
                    AddedToQueue = false,
                    RatePercent = 98.55,
                    RateMoney = 2700
                });
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
        public async Task<List<LoanbriefNotCancelDetail>> AllLoanByPhone(string phone)
        {
            return await _unitOfWork.LoanBriefRepository.Query(x => (x.Phone == phone
                            || (!string.IsNullOrEmpty(x.Phone) && x.PhoneOther == phone)), null, false)
                                .Select(LoanbriefNotCancelDetail.Projection).ToListAsync();
        }


        public async Task<LoanForCheckTopupDetail> GetLoanForCheckTopup(int loanbriefId)
        {
            var loanbrief = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(x => new LoanForCheckTopupDetail
            {
                LoanbriefId = x.LoanBriefId,
                FullName = x.FullName,
                NationalCard = x.NationalCard,
                TypeRemarking = x.TypeRemarketing,
                LoanStatus = x.Status,
                ParentLoanbriefId = x.ReMarketingLoanBriefId,
                LmsLoanId = x.LmsLoanId
            }).FirstOrDefaultAsync();
            if (loanbrief.LoanStatus == (int)EnumLoanStatus.DISBURSED)
            {
                if (loanbrief.TypeRemarking == (int)EnumTypeRemarketing.IsTopUp)
                {
                    var loanParent = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbrief.ParentLoanbriefId, null, false).Select(x => new
                    {
                        LoanStatus = x.Status,
                        TypeRemarketing = x.TypeRemarketing
                    }).FirstOrDefaultAsync();
                    if(loanParent!= null)
                    {
                        loanbrief.ParentLoanStatus = loanParent.LoanStatus;
                        loanbrief.ParentLoanTypeRemarketing = loanParent.TypeRemarketing;
                    }                   
                }
            }

            return loanbrief;

        }

    }
}
