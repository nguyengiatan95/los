﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LOS.DAL.DTOs.LoanbriefRelationship;

namespace LOS.DAL.Respository
{
    public interface ILoanbriefRelationshipRepository
    {
        Task<RelationshipDetail> GetById(int id);
        Task<List<RelationshipInfoLoanNotCancelDetail>> ListRelationshipInfoLoanNotCancel(string phone);
        Task<List<RelationshipInfoLoanNotCancelDetail>> AllListRelationshipInfoLoan(string phone);
    }
}