﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs.LoanbriefRelationship;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class LoanbriefRelationshipRepository : ILoanbriefRelationshipRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public LoanbriefRelationshipRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public async Task<RelationshipDetail> GetById(int id)
        {
            return await _unitOfWork.LoanBriefRelationshipRepository.Query(x => x.Id == id, null, false)
                                .Select(RelationshipDetail.Projection).FirstOrDefaultAsync();
        }
        public async Task<List<RelationshipInfoLoanNotCancelDetail>> ListRelationshipInfoLoanNotCancel(string phone)
        {
            return await _unitOfWork.LoanBriefRelationshipRepository.Query(x => x.LoanBrief != null
                            && x.LoanBrief.Status != (int)EnumLoanStatus.CANCELED && x.Phone == phone, null, false)
                                .Select(RelationshipInfoLoanNotCancelDetail.Projection).ToListAsync();
        }
        public async Task<List<RelationshipInfoLoanNotCancelDetail>> AllListRelationshipInfoLoan(string phone)
        {
            return await _unitOfWork.LoanBriefRelationshipRepository.Query(x => x.LoanBrief != null
                            && x.Phone == phone, null, false)
                                .Select(RelationshipInfoLoanNotCancelDetail.Projection).ToListAsync();
        }
    }
}
