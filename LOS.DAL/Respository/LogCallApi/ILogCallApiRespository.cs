﻿using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public interface ILogCallApiRespository
    {
        Task<bool> Add(EntityFramework.LogCallApi entity);
        Task<bool> Update(DAL.EntityFramework.LogCallApi entity);
    }
}