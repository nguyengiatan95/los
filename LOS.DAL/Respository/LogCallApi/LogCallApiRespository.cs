﻿using LOS.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class LogCallApiRespository : ILogCallApiRespository
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogCallApiRespository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<bool> Add(DAL.EntityFramework.LogCallApi entity)
        {
            if (entity != null)
            {
                _unitOfWork.LogCallApiRepository.Insert(entity);
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> Update(DAL.EntityFramework.LogCallApi entity)
        {
            if (entity.Id > 0)
            {
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == entity.Id, x => new DAL.EntityFramework.LogCallApi()
                {
                    Status = entity.Status,
                    Output = entity.Output,
                    ModifyAt = entity.ModifyAt,
                });
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
    }
}
