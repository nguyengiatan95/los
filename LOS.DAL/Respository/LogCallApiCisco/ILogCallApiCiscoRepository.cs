﻿using LOS.DAL.EntityFramework;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public interface ILogCallApiCiscoRepository
    {
        Task<bool> Add(LogCallApiCisco entity);
    }
}