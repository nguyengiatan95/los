﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class LogCallApiCiscoRepository : ILogCallApiCiscoRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogCallApiCiscoRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<bool> Add(LogCallApiCisco entity)
        {
            try
            {
                if (entity != null)
                {
                    _unitOfWork.LogCallApiCiscoRepository.Insert(entity);
                    await _unitOfWork.SaveAsync();
                    return true;
                }
            }
            catch(Exception ex)
            {

            }
            
            return false;
        }

    }
}
