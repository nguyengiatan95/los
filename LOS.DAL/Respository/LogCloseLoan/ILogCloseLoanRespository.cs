﻿using LOS.DAL.EntityFramework;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public interface ILogCloseLoanRespository
    {
        int Add(LogCloseLoan entity);
        bool Update(LogCloseLoan entity);
        bool CheckLoanProcess(int loanBriefId);
    }
}