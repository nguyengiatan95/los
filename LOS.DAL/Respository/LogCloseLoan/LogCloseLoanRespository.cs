﻿using LOS.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class LogCloseLoanRespository : ILogCloseLoanRespository
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogCloseLoanRespository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public int Add(DAL.EntityFramework.LogCloseLoan entity)
        {
            if (entity != null)
            {
                var insert = _unitOfWork.LogCloseLoanRepository.Insert(entity);
                _unitOfWork.SaveAsync();
                return insert.Id;
            }
            return 0;
        }

        public bool Update(DAL.EntityFramework.LogCloseLoan entity)
        {
            if (entity.Id > 0)
            {
                _unitOfWork.LogCloseLoanRepository.Update(x => x.Id == entity.Id, x => new DAL.EntityFramework.LogCloseLoan()
                {
                    Status = entity.Status,
                    Output = entity.Output,
                    ModifyAt = entity.ModifyAt,
                });
                _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
        public bool CheckLoanProcess(int loanBriefId)
        {
            if (_unitOfWork.LogCloseLoanRepository.Any(x => x.LoanBriefId == loanBriefId))
                return false;

            return true;
        }
    }
}
