﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;

namespace LOS.DAL.Respository
{
    public interface ILogDistributionUserRepository
    {
        Task<List<LogDistributionUserDetail>> GetLogDistributionUser(int typeDistribution, List<int> listLoanbriefId);
        Task<bool> Add(LogDistributionUser entity);
    }
}