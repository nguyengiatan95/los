﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class LogDistributionUserRepository : ILogDistributionUserRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogDistributionUserRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<List<LogDistributionUserDetail>> GetLogDistributionUser(int typeDistribution, List<int> listLoanbriefId)
        {
            return await _unitOfWork.LogDistributionUserRepository.Query(x => x.TypeDistribution == typeDistribution
                    && x.LoanbriefId > 0 && listLoanbriefId.Contains(x.LoanbriefId.Value), null, false)
                    .Select(LogDistributionUserDetail.Projection).ToListAsync();
        }

        public async Task<bool> Add(LogDistributionUser entity)
        {
            if (entity != null)
            {
                _unitOfWork.LogDistributionUserRepository.Insert(entity);
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
    }
}
