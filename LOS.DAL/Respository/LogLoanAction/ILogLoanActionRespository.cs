﻿using LOS.DAL.EntityFramework;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public interface ILogLoanActionRespository
    {
        Task<bool> Add(LogLoanAction entity);
    }
}