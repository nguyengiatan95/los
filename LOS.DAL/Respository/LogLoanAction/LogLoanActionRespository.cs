﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class LogLoanActionRespository : ILogLoanActionRespository
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogLoanActionRespository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public async Task<bool> Add(LogLoanAction entity)
        {
            if (entity != null)
            {
                _unitOfWork.LogLoanActionRepository.Insert(entity);
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
    }
}
