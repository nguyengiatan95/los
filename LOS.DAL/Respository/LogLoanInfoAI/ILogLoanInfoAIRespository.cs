﻿using LOS.DAL.EntityFramework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public interface ILogLoanInfoAIRespository
    {
        Task<bool> Add(LogLoanInfoAi entity);
        Task<bool> AddRange(List<LogLoanInfoAi> entities);
    }
}