﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class LogLoanInfoAIRespository : ILogLoanInfoAIRespository
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogLoanInfoAIRespository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public async Task<bool> Add(LogLoanInfoAi entity)
        {
            if (entity != null)
            {
                _unitOfWork.LogLoanInfoAiRepository.Insert(entity);
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
        public async Task<bool> AddRange(List<LogLoanInfoAi> entities)
        {
            if (entities != null && entities.Count > 0)
            {
                _unitOfWork.LogLoanInfoAiRepository.Insert(entities);
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
    }
}
