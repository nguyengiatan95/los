﻿using LOS.DAL.EntityFramework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public interface ILogReLoanBriefRepository
    {
        Task<bool> Add(LogReLoanbrief entity);
        Task<bool> AddRange(List<LogReLoanbrief> entities);
    }
}