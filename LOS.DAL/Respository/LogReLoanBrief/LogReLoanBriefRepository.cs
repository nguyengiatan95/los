﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class LogReLoanBriefRepository : ILogReLoanBriefRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogReLoanBriefRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<bool> Add(LogReLoanbrief entity)
        {
            if (entity != null)
            {
                _unitOfWork.LogReLoanbriefRepository.Insert(entity);
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> AddRange(List<LogReLoanbrief> entities)
        {
            if (entities != null && entities.Count > 0)
            {
                _unitOfWork.LogReLoanbriefRepository.Insert(entities);
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
    }
}
