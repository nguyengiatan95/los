﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class ProvinceRepository : IProvinceRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public ProvinceRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<ProvinceDetail> GetById(int id)
        {
            return await _unitOfWork.ProvinceRepository.Query(x => x.ProvinceId == id, null, false)
               .Select(ProvinceDetail.ProjectionDetail).FirstOrDefaultAsync();
        }

        public List<ProvinceDetail> Search(string searchName, int status, int page, int pageSize, ref int recordsTotal)
        {
            var lstData = new List<ProvinceDetail>();
            try
            {
                var query = _unitOfWork.ProvinceRepository.Query(x => (x.IsApply.GetValueOrDefault(0) == status || status == -1)
                    && (searchName == null || x.Name.ToLower().Contains(searchName.ToLower())), null, false)
                    .Select(ProvinceDetail.ProjectionDetail).ToList();

                recordsTotal = query.Count();
                if (query != null && query.Count > 0)
                    //phân trang
                    lstData = query.OrderBy(x => x.Priority).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            catch (Exception ex)
            {
            }
            return lstData;
        }

        public async Task<bool> UpdateIsApplyProvince(int provinceId, int isApply)
        {
            try
            {
                //kiểm tra xem có tồn tại thành phố không
                if (_unitOfWork.ProvinceRepository.Any(x => x.ProvinceId == provinceId))
                {
                    _unitOfWork.ProvinceRepository.Update(x => x.ProvinceId == provinceId, x => new Province()
                    {
                        IsApply = isApply
                    });
                    await _unitOfWork.SaveAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public async Task<List<ProvinceDetail>> GetAll()
        {
            return await _unitOfWork.ProvinceRepository.All()
               .Select(ProvinceDetail.ProjectionDetail).ToListAsync();
        }

        public async Task<List<ProvinceDetail>> GetProvinceSupport()
        {
            return await _unitOfWork.ProvinceRepository.Query(x => x.IsApply == 1, null, false)
              .Select(ProvinceDetail.ProjectionDetail).ToListAsync();
        }
    }
}
