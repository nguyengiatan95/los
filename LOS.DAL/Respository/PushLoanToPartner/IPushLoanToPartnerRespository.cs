﻿using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public interface IPushLoanToPartnerRespository
    {
        Task<bool> UpdateStatusDisbursement(int id);
    }
}