﻿using LOS.Common.Extensions;
using LOS.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class PushLoanToPartnerRespository : IPushLoanToPartnerRespository
    {
        private readonly IUnitOfWork _unitOfWork;
        public PushLoanToPartnerRespository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public async Task<bool> UpdateStatusDisbursement(int id)
        {
            if (id > 0)
            {
                _unitOfWork.PushLoanToPartnerRepository.Update(x => x.Id == id, x => new EntityFramework.PushLoanToPartner()
                {
                    Status = (int)StatusPushLoanToPartner.Disbursement,
                    SynchronizedDate = DateTime.Now
                });
                await _unitOfWork.SaveAsync();
                return true;
            }
            else
                return false;
        }
    }
}
