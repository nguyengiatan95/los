﻿using LOS.DAL.EntityFramework;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public interface IRuleCheckLoanRepository
    {
        Task<bool> AddOrUpdate(RuleCheckLoan entity);
    }
}