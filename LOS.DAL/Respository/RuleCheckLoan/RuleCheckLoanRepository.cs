﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class RuleCheckLoanRepository : IRuleCheckLoanRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public RuleCheckLoanRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<bool> AddOrUpdate(RuleCheckLoan entity)
        {
            if (entity != null)
            {
                //cập nhập
                if (_unitOfWork.RuleCheckLoanRepository.Any(x => x.LoanbriefId == entity.LoanbriefId))
                {
                    //cập nhập check momo
                    if(entity.IsCheckMomo != null)
                    {
                        _unitOfWork.RuleCheckLoanRepository.Update(x => x.LoanbriefId == entity.LoanbriefId, x => new RuleCheckLoan()
                        {
                            IsCheckMomo = entity.IsCheckMomo,
                            UpdatedAt = DateTime.Now,
                            UserId = entity.UserId,
                        });
                    }
                    //cập nhập check kỳ thanh toán
                    if(entity.IsCheckPayment != null)
                    {
                        _unitOfWork.RuleCheckLoanRepository.Update(x => x.LoanbriefId == entity.LoanbriefId, x => new RuleCheckLoan()
                        {
                            IsCheckPayment = entity.IsCheckPayment,
                            UpdatedAt = DateTime.Now,
                            UserId = entity.UserId,
                        });
                    }
                    //cập nhập cả 2
                    if(entity.IsCheckMomo != null && entity.IsCheckPayment != null)
                    {
                        _unitOfWork.RuleCheckLoanRepository.Update(x => x.LoanbriefId == entity.LoanbriefId, x => new RuleCheckLoan()
                        {
                            IsCheckMomo = entity.IsCheckMomo,
                            IsCheckPayment = entity.IsCheckPayment,
                            UpdatedAt = DateTime.Now,
                            UserId = entity.UserId,
                        });
                    }
                    await _unitOfWork.SaveAsync();
                    return true;
                }
                //thêm mới
                else
                {
                    _unitOfWork.RuleCheckLoanRepository.Insert(entity);
                    await _unitOfWork.SaveAsync();
                    return true;
                }
            }
            return false;
        }
    }
}
