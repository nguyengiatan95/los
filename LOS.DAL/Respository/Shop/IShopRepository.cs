﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LOS.DAL.DTOs;

namespace LOS.DAL.Respository
{
    public interface IShopRepository
    {
        Task<ShopDetail> GetShopSupport(int productCreditId, int districtId, int? wardId);
    }
}