﻿using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class ShopRepository : IShopRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public ShopRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<ShopDetail> GetShopSupport(int productCreditId, int districtId, int? wardId)
        {
            var result = new ShopDetail();
            if(wardId > 0)
                result = await  _unitOfWork.ManagerAreaHubRepository.Query(x => x.WardId == wardId && x.Shop.Status == 1 
                    && x.Shop.IsReceiveLoan == true 
                    && (x.ProductId == -1 || x.ProductId == productCreditId),
                   x => x.OrderBy(k => k.Shop.Received), false, x => x.Shop).Select(x => new ShopDetail()
                   {
                       ShopId = x.ShopId,
                       Name = x.Shop.Name,
                       Address = x.Shop.Address,
                       Phone = x.Shop.Phone,
                       Status = x.Shop.Status,
                       CityId = x.Shop.CityId
                   }).FirstOrDefaultAsync();

            if(result.ShopId == 0)
            {
                result = await _unitOfWork.ManagerAreaHubRepository.Query(x => x.DistrictId == districtId
                && x.Shop.Status == 1 && x.Shop.IsReceiveLoan == true && (x.ProductId == -1 || x.ProductId == productCreditId),
                  x => x.OrderBy(k => k.Shop.Received), false, x => x.Shop).Select(x => new ShopDetail()
                  {
                      ShopId = x.ShopId,
                      Name = x.Shop.Name,
                      Address = x.Shop.Address,
                      Phone = x.Shop.Phone,
                      Status = x.Shop.Status,
                      CityId = x.Shop.CityId
                  }).FirstOrDefaultAsync();
            }
            return result;
        }
    }
}
