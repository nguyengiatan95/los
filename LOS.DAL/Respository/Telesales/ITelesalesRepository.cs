﻿using LOS.DAL.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public interface ITelesalesRepository
    {
        Task<List<TeamTelesaleDetail>> GetTeamEnable();
        Task<List<UserTelesalesDetail>> GetTelesalesByTeam(int team);
        Task<List<UserTelesalesDetail>> GetTelesalesEnable();
    }
}