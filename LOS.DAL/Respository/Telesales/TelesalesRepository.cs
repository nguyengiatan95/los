﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class TelesalesRepository : ITelesalesRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public TelesalesRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<List<TeamTelesaleDetail>> GetTeamEnable()
        {
            return await _unitOfWork.TeamTelesalesRepository.Query(x => x.IsEnable == true, null, false)
                .Select(TeamTelesaleDetail.ProjectionDetail).ToListAsync();
        }
        public async Task<List<UserTelesalesDetail>> GetTelesalesByTeam(int team)
        {
            return await _unitOfWork.UserRepository.Query(x => x.TeamTelesalesId == team && x.Status == (int)EnumStatusBase.Active, null, false)
                .Select(UserTelesalesDetail.ProjectionDetail).ToListAsync();
        }
        public async Task<List<UserTelesalesDetail>> GetTelesalesEnable()
        {
            return await _unitOfWork.UserRepository.Query(x => x.GroupId == (int)EnumGroupUser.Telesale && x.Status == (int)EnumStatusBase.Active, null, false)
              .Select(UserTelesalesDetail.ProjectionDetail).ToListAsync();
        }
    }
}
