﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LOS.DAL.DTOs;

namespace LOS.DAL.Respository
{
    public interface IUserRepository
    {
        Task<List<UserBasicDetail>> GetUserBasicByGroup(List<int> listGroupId);
        Task<UserMobileCiscoDetail> GetUserMobileCisco(int userId);
        Task<bool> SaveConfigCiscoMobile(EntityFramework.UserMobileCisco entity);
        Task<int> CreateConfigCiscoMobile(EntityFramework.UserMobileCisco entity);
        Task<UserBasicDetail> GetById(int userId);
    }
}