﻿using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class UserRepository : IUserRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public UserRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<List<UserBasicDetail>> GetUserBasicByGroup(List<int> listGroupId)
        {
            return await _unitOfWork.UserRepository.Query(x => x.GroupId > 0 && x.Status == 1 && listGroupId.Contains(x.GroupId.Value), null, false)
                .Select(UserBasicDetail.Projection).ToListAsync();

        }

        public async Task<UserMobileCiscoDetail> GetUserMobileCisco(int userId)
        {
            return await _unitOfWork.UserMobileCiscoRepository.Query(x => x.UserId == userId, null, false)
                 .Select(UserMobileCiscoDetail.ProjectionDetail).FirstOrDefaultAsync();
        }

        public async Task<bool> SaveConfigCiscoMobile(EntityFramework.UserMobileCisco entity)
        {
            if (entity.UserId > 0)
            {
                _unitOfWork.UserMobileCiscoRepository.Update(x => x.UserId == entity.UserId, x => new EntityFramework.UserMobileCisco()
                {
                    MbCiscoExtension = entity.MbCiscoExtension,
                    MbciscoPassword = entity.MbciscoPassword
                });
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }

        public async Task<int> CreateConfigCiscoMobile(EntityFramework.UserMobileCisco entity)
        {
            if (entity != null && entity.UserId > 0)
            {
                var result = _unitOfWork.UserMobileCiscoRepository.Insert(entity);
                await _unitOfWork.SaveAsync();
                return result.UserId;
            }
            return 0;
        }
        public async Task<UserBasicDetail> GetById(int userId)
        {
            if (userId > 0)
            {
                return await _unitOfWork.UserRepository.Query(x => x.UserId == userId, null, false)
                    .Select(UserBasicDetail.Projection).FirstOrDefaultAsync();
            }
            return null;
        }
    }
}
