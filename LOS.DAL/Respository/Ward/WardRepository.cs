﻿using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.DAL.Respository
{
    public class WardRepository : IWardRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public WardRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public List<WardDetail> Search(string searchName, int districtId, int page, int pageSize, ref int recordsTotal)
        {
            var lstData = new List<WardDetail>();
            try
            {
                var query = _unitOfWork.WardRepository.Query(x => x.DistrictId == districtId
                    && (searchName == null || x.Name.ToLower().Contains(searchName.ToLower())), null, false)
                    .Select(WardDetail.ProjectionDetail).ToList();

                recordsTotal = query.Count();
                if (query != null && query.Count > 0)
                    //phân trang
                    lstData = query.OrderBy(x => x.Priority).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            catch (Exception ex)
            {
            }
            return lstData;
        }

        public async Task<bool> Add(DAL.EntityFramework.Ward entity)
        {
            if (entity != null && entity.WardId > 0)
            {
                _unitOfWork.WardRepository.Insert(entity);
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }
        public async Task<bool> Update(DAL.EntityFramework.Ward entity)
        {
            if (entity != null && entity.WardId > 0)
            {
                _unitOfWork.WardRepository.Update(x => x.WardId == entity.WardId, x => new DAL.EntityFramework.Ward()
                {
                    DistrictId = entity.DistrictId,
                    Name = entity.Name,
                    Type = entity.Type,
                });
                await _unitOfWork.SaveAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> CheckWard(string name, int districtId)
        {
            if (_unitOfWork.WardRepository.Any(x => x.Name.ToLower() == name.ToLower() && x.DistrictId == districtId))
                return false;
            else
                return true;
        }
    }
}
