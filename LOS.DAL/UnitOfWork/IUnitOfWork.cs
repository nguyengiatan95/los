﻿using LOS.DAL.EntityFramework;
using LOS.DAL.Respository;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Action = LOS.DAL.EntityFramework.Action;

namespace LOS.DAL.UnitOfWork
{
    public interface IUnitOfWork
    {
        IGenericRepository<User> UserRepository { get; }
        IGenericRepository<Group> GroupRepository { get; }
        IGenericRepository<Province> ProvinceRepository { get; }
        IGenericRepository<District> DistrictRepository { get; }
        IGenericRepository<Ward> WardRepository { get; }
        IGenericRepository<Company> CompanyRepository { get; }
        IGenericRepository<Department> DepartmentRepository { get; }
        IGenericRepository<CompanyDepartment> CompanyDepartmentRepository { get; }
        IGenericRepository<Position> PositionRepository { get; }
        IGenericRepository<Project> ProjectRepository { get; }
        IGenericRepository<UserProject> UserProjectRepository { get; }
        IGenericRepository<LoanProduct> LoanProductRepository { get; }
        IGenericRepository<LoanBrief> LoanBriefRepository { get; }
        IGenericRepository<LoanBriefDocument> LoanBriefDocumentRepository { get; }
        IGenericRepository<LoanBriefHistory> LoanBriefHistoryRepository { get; }
        IGenericRepository<LoanBriefHousehold> LoanBriefHouseholdRepository { get; }
        IGenericRepository<LoanBriefJob> LoanBriefJobRepository { get; }
        IGenericRepository<LoanBriefNote> LoanBriefNoteRepository { get; }
        IGenericRepository<LoanBriefProperty> LoanBriefPropertyRepository { get; }
        IGenericRepository<LoanBriefResident> LoanBriefResidentRepository { get; }
        IGenericRepository<Job> JobRepository { get; }
        IGenericRepository<JobTitle> JobTitleRepository { get; }
        IGenericRepository<LoanRateType> LoanRateTypeRepository { get; }
        IGenericRepository<Module> ModulesRepository { get; }
        IGenericRepository<UserModule> UserModuleRepository { get; }
        IGenericRepository<GroupModule> GroupModuleRepository { get; }
        IGenericRepository<GroupSource> GroupSourceRepository { get; }
        IGenericRepository<Property> PropertyRepository { get; }
        IGenericRepository<PropertyValue> PropertyValueRepository { get; }
        IGenericRepository<Script> ScriptRepository { get; }
        IGenericRepository<ScriptDetail> ScriptDetailRepository { get; }
        IGenericRepository<UserOnline> UserOnlineRepository { get; }
        IGenericRepository<LoanAction> LoanActionRepository { get; }
        IGenericRepository<Action> ActionRepository { get; }
        IGenericRepository<ActionStatus> ActionStatusRepository { get; }
        IGenericRepository<Schedule> ScheduleRepository { get; }
        IGenericRepository<UtmSource> UtmSourceRepository { get; }
        IGenericRepository<UserMmo> UserMMORepository { get; }
        IGenericRepository<UserReference> UserReferenceRepository { get; }
        IGenericRepository<Transactions> TransactionsRepository { get; }
        IGenericRepository<TlsScenario> TlsScenarioRepository { get; }
        IGenericRepository<TlsQuestion> TlsQuestionRepository { get; }
        IGenericRepository<TlsScenarioQuestion> TlsScenarioQuestionRepository { get; }
        IGenericRepository<Pipeline> PipelineRepository { get; }
        IGenericRepository<Section> SectionRepository { get; }
        IGenericRepository<SectionDetail> SectionDetailRepository { get; }
        IGenericRepository<SectionApprove> SectionApproveRepository { get; }
        IGenericRepository<SectionAction> SectionActionRepository { get; }
        IGenericRepository<SectionCondition> SectionConditionRepository { get; }
        IGenericRepository<LoanStatus> LoanStatusRepository { get; }
        IGenericRepository<PipelineState> PipelineStateRepository { get; }
        IGenericRepository<RelativeFamily> RelativeFamilyRepository { get; }
        IGenericRepository<BrandProduct> BrandProductRepository { get; }
        IGenericRepository<Product> ProductRepository { get; }
        IGenericRepository<Bank> BankRepository { get; }
        IGenericRepository<Customer> CustomerRepository { get; }
        IGenericRepository<LoanBriefCompany> LoanBriefCompanyRepository { get; }
        IGenericRepository<LoanBriefRelationship> LoanBriefRelationshipRepository { get; }
        IGenericRepository<PlatformType> PlatformTypeRepository { get; }
        IGenericRepository<LoanBriefLog> LoanBriefLogRepository { get; }
        IGenericRepository<LoanbriefScript> LoanbriefScriptRepository { get; }
        IGenericRepository<ProductPercentReduction> ProductPercentReductionRepository { get; }
        IGenericRepository<ReasonCancel> ReasonCancelRepository { get; }
        IGenericRepository<TokenSmartDailer> TokenSmartDailerRepository { get; }
        IGenericRepository<LogWorking> LogWorkingRepository { get; }
        IGenericRepository<Shop> ShopRepository { get; }
        IGenericRepository<UserShop> UserShopRepository { get; }
        IGenericRepository<DocumentType> DocumentTypeRepository { get; }
        IGenericRepository<LoanBriefFiles> LoanBriefFileRepository { get; }
        IGenericRepository<UserCondition> UserConditionRepository { get; }
        IGenericRepository<UserActionPermission> UserActionPermissionRepository { get; }
        IGenericRepository<ManagerAreaHub> ManagerAreaHubRepository { get; }
        IGenericRepository<GroupCondition> GroupConditionRepository { get; }
        IGenericRepository<GroupActionPermission> GroupActionPermissionRepository { get; }
        IGenericRepository<AccessToken> AccessTokenRepository { get; }
        IGenericRepository<LogCallApi> LogCallApiRepository { get; }
        IGenericRepository<PermissionIp> PermissionIpRepository { get; }
        IGenericRepository<ProductReview> ProductReviewRepository { get; }
        IGenericRepository<ProductReviewResult> ProductReviewResultRepository { get; }
        IGenericRepository<ProductReviewDetail> ProductReviewDetailRepository { get; }
        IGenericRepository<ProductReviewResultDetail> ProductReviewResultDetailRepository { get; }
        IGenericRepository<CoordinatorCheckList> CoordinatorCheckListRepository { get; }
        IGenericRepository<ProductAppraiser> ProductAppraiserRepository { get; }
        IGenericRepository<Location> LocationRepository { get; }
        IGenericRepository<LogLoanInfoAi> LogLoanInfoAiRepository { get; }
        IGenericRepository<HubDistribution> HubDistributionRepository { get; }
        IGenericRepository<HubLoanBrief> HubLoanBriefRepository { get; }
        IGenericRepository<LogRequestAi> LogRequestAiRepository { get; }
        IGenericRepository<TelesaleLoanbrief> TelesaleLoanbriefRepository { get; }
        IGenericRepository<PipelineHistory> PipelineHistoryRepository { get; }
        IGenericRepository<LogSendVoiceOtp> LogSendVoiceOtpRepository { get; }
        IGenericRepository<LogPushToHub> LogPushToHubRepository { get; }
        IGenericRepository<Notification> NotificationRepository { get; }

        IGenericRepository<LogClickToCall> LogClickToCallRepository { get; }

        IGenericRepository<TypeOwnerShip> TypeOwnerShipRepository { get; }
        IGenericRepository<AuthorizationToken> AuthorizationTokenRepository { get; }

        IGenericRepository<ResultEkyc> ResultEkycRepository { get; }
        IGenericRepository<RequestEvent> RequestEventRepository { get; }
        IGenericRepository<EventConfig> EventConfigRepository { get; }
        IGenericRepository<LogResultAutoCall> LogResultAutoCallRepository { get; }
        IGenericRepository<QueueLog> QueueLogRepository { get; }

        IGenericRepository<LoanbriefLender> LoanBriefLenderRepository { get; }

        IGenericRepository<LoanBriefQuestionScript> LoanBriefQuestionScriptRepository { get; }

        IGenericRepository<LoanStatusDetail> LoanStatusDetailRepository { get; }
        IGenericRepository<LogSendSms> LogSendSmsRepository { get; }
        IGenericRepository<TypeRemarketing> TypeRemarketingRepository { get; }

        IGenericRepository<LogLoanbriefScoring> LogLoanbriefScoringRepository { get; }

        IGenericRepository<LoanBriefImportFileExcel> LoanBriefImportFileExcelRepository { get; }
        IGenericRepository<TelesalesShift> TelesalesShiftRepository { get; }
        IGenericRepository<TeamTelesales> TeamTelesalesRepository { get; }
        IGenericRepository<UserTeamTelesales> UserTeamTelesalesRepository { get; }
        IGenericRepository<ContactCustomer> ContactCustomerRepository { get; }
        IGenericRepository<CheckLoanInformation> CheckImageRepository { get; }
        IGenericRepository<ScheduleTime> ScheduleTimeRepository { get; }
        IGenericRepository<HubEmployeeDistribution> HubEmployeeDistributionRepository { get; }
        IGenericRepository<LogDistributingHubEmployee> LogDistributingHubEmployeeRepository { get; }
        IGenericRepository<ReasonCoordinator> ReasonCoordinatorRepository { get; }
        IGenericRepository<LogCancelLoanbrief> LogCancelLoanbriefRepository { get; }
        IGenericRepository<LogCiscoPushToTelesale> LogCiscoPushToTelesaleRepository { get; }
        IGenericRepository<ConfigDocument> ConfigDocumentRepository { get; }
        IGenericRepository<LoanConfigStep> LoanConfigStepRepository { get; }
        IGenericRepository<InfomationProductDetail> InfomationProductDetailRepository { get; }
        IGenericRepository<ConfigProductDetail> ConfigProductDetailRepository { get; }
        IGenericRepository<LogReLoanbrief> LogReLoanbriefRepository { get; }
        IGenericRepository<ConfigDepartmentStatus> ConfigDepartmentStatusRepository { get; }
        IGenericRepository<ProposeExceptions> ProposeExceptionsRepository { get; }
        IGenericRepository<LenderConfig> LenderConfigRepository { get; }
        IGenericRepository<LenderLoanBrief> LenderLoanBriefRepository { get; }
        IGenericRepository<LogInfoCreateLoanbrief> LogInfoCreateLoanbriefRepository { get; }
        IGenericRepository<LogDistributionUser> LogDistributionUserRepository { get; }
        IGenericRepository<LogLoanAction> LogLoanActionRepository { get; }
        IGenericRepository<LogSendOtp> LogSendOtpRepository { get; }
        IGenericRepository<LogPushNotification> LogPushNotificationRepository { get; }
        IGenericRepository<CompareDocument> CompareDocumentRepository { get; }
        IGenericRepository<EsignContract> EsignContractRepository { get; }
        IGenericRepository<LogPartner> LogPartnerRepository { get; }
        IGenericRepository<LogActionRestruct> LogActionRestructRepository { get; }
        IGenericRepository<EkycImages> EkycImagesRepository { get; }
        IGenericRepository<LogLoanInfoPartner> LogLoanInfoPartnerRepository { get; }
        IGenericRepository<SystemConfig> SystemConfigRepository { get; }
        IGenericRepository<DocumentException> DocumentExceptionRepository { get; }
        IGenericRepository<UserMobileCisco> UserMobileCiscoRepository { get; }
        IGenericRepository<RuleCheckLoan> RuleCheckLoanRepository { get; }
        IGenericRepository<LogChangePhone> LogChangePhoneRepository { get; }
        IGenericRepository<ConfigContractRate> ConfigContractRateRepository { get; }
        IGenericRepository<LogSecuredTransaction> LogSecuredTransactionRepository { get; }
        IGenericRepository<PushLoanToPartner> PushLoanToPartnerRepository { get; }
        IGenericRepository<LogCallApiPartner> LogCallApiPartnerRepository { get; }
        IGenericRepository<KpiTelesale> KpiTelesaleRepository { get; }
        IGenericRepository<KpiTelesaleSummary> KpiTelesaleSummaryRepository { get; }
        IGenericRepository<LogPushToPartner> LogPushToPartnerRepository { get; }
        IGenericRepository<LogCloseLoan> LogCloseLoanRepository { get; }
        IGenericRepository<LogPushToCisco> LogPushToCiscoRepository { get; }
        IGenericRepository<LogCallApiCisco> LogCallApiCiscoRepository { get; }

        void Save();
        void SaveChanges();
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();
        Task SaveAsync();
        LOSContext GetContext();
        void Dispose();
    }
}
