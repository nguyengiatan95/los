﻿using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers;
using LOS.DAL.Respository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Action = LOS.DAL.EntityFramework.Action;

namespace LOS.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, System.IDisposable
    {
        private readonly LOSContext context;
        private IGenericRepository<User> _userRepository;
        private IGenericRepository<Group> _groupRepository;
        private IGenericRepository<Province> _provinceRepository;
        private IGenericRepository<District> _districtRepository;
        private IGenericRepository<Ward> _wardRepository;
        private IGenericRepository<Company> _companyRepository;
        private IGenericRepository<Department> _departmentRepository;
        private IGenericRepository<CompanyDepartment> _companyDepartmentRepository;
        private IGenericRepository<Position> _positionRepository;
        private IGenericRepository<Project> _projectRepository;
        private IGenericRepository<UserProject> _userProjectRepository;
        private IGenericRepository<LoanProduct> _loanProductRepository;
        private IGenericRepository<LoanBrief> _loanBriefRepository;
        private IGenericRepository<LoanBriefDocument> _loanBriefDocumentRepository;
        private IGenericRepository<LoanBriefHistory> _loanBriefHistoryRepository;
        private IGenericRepository<LoanBriefHousehold> _loanBriefHouseholdRepository;
        private IGenericRepository<LoanBriefJob> _loanBriefJobRepository;
        private IGenericRepository<LoanBriefNote> _loanBriefNoteRepository;
        private IGenericRepository<LoanBriefProperty> _loanBriefPropertyRepository;
        private IGenericRepository<LoanBriefResident> _loanBriefResidentRepository;
        private IGenericRepository<Job> _jobRepository;
        private IGenericRepository<JobTitle> _jobTitleRepository;
        private IGenericRepository<LoanRateType> _loanRateTypeRepository;
        private IGenericRepository<Module> _moduleRepository;
        private IGenericRepository<UserModule> _userModuleRepository;
        private IGenericRepository<GroupModule> _groupModuleRepository;
        private IGenericRepository<GroupSource> _groupSourceRepository;
        private IGenericRepository<Property> _propertyRepository;
        private IGenericRepository<PropertyValue> _propertyValueRepository;
        private IGenericRepository<Script> _scriptRepository;
        private IGenericRepository<ScriptDetail> _scriptDetailRepository;
        private IGenericRepository<UserOnline> _userOnlineRepository;
        private IGenericRepository<LoanAction> _loanActionRepository;
        private IGenericRepository<Action> _actionRepository;
        private IGenericRepository<ActionStatus> _actionStatusRepository;
        private IGenericRepository<Schedule> _scheduleRepository;
        private IGenericRepository<UtmSource> _utmSourceRepository;
        private IDbContextTransaction _dbContextTransaction;
        private IGenericRepository<UserMmo> _userMMORepository;
        private IGenericRepository<UserReference> _userReferenceRepository;
        private IGenericRepository<Transactions> _transactionsRepository;
        private IGenericRepository<TlsScenario> _tlsScenarioRepository;
        private IGenericRepository<TlsQuestion> _tlsQuestionRepository;
        private IGenericRepository<TlsScenarioQuestion> _tlsScenarioQuestionRepository;
        private IGenericRepository<Pipeline> _pipelineRepository;
        private IGenericRepository<Section> _sectionRepository;
        private IGenericRepository<SectionDetail> _sectionDetailRepository;
        private IGenericRepository<SectionApprove> _sectionApproveRepository;
        private IGenericRepository<SectionAction> _sectionActionRepository;
        private IGenericRepository<SectionCondition> _sectionConditionRepository;
        private IGenericRepository<LoanStatus> _loanStatusRepository;
        private IGenericRepository<PipelineState> _pipelineStateRepository;
        private IGenericRepository<RelativeFamily> _relativeFamilyRepository;
        private IGenericRepository<BrandProduct> _brandProductRepository;
        private IGenericRepository<Product> _productRepository;
        private IGenericRepository<Bank> _bankRepository;
        private IGenericRepository<Customer> _customerRepository;
        private IGenericRepository<LoanBriefCompany> _loanBriefCompanyRepository;
        private IGenericRepository<LoanBriefRelationship> _loanBriefRelationshipRepository;
        private IGenericRepository<PlatformType> _platformTypeRepository;
        private IGenericRepository<LoanBriefLog> _loanBriefLogRepository;
        private IGenericRepository<LoanbriefScript> _loanbriefScriptRepository;
        private IGenericRepository<ProductPercentReduction> _productPercentReductionRepository;
        private IGenericRepository<ReasonCancel> _reasonCancelRepository;
        private IGenericRepository<TokenSmartDailer> _tokenSmartDailerRepository;
        private IGenericRepository<LogWorking> _logWorkingRepository;
        private IGenericRepository<Shop> _shopRepository;
        private IGenericRepository<UserShop> _userShopRepository;
        private IGenericRepository<DocumentType> _documentTypeRepository;
        private IGenericRepository<LoanBriefFiles> _loanBriefFileRepository;
        private IGenericRepository<UserCondition> _userConditionRepository;
        private IGenericRepository<UserActionPermission> _userActionPermissionRepository;
        private IGenericRepository<ManagerAreaHub> _managerAreaHubRepository;
        private IGenericRepository<GroupCondition> _groupConditionRepository;
        private IGenericRepository<GroupActionPermission> _groupActionPermissionRepository;
        private IGenericRepository<AccessToken> _accessTokenRepository;
        private IGenericRepository<LogCallApi> _logCallApiRepository;
        private IGenericRepository<PermissionIp> _permissionIpRepository;
        private IGenericRepository<ProductReview> _productReviewRepository;
        private IGenericRepository<ProductReviewResult> _productReviewResultRepository;
        private IGenericRepository<ProductReviewDetail> _productReviewDetailRepository;
        private IGenericRepository<ProductReviewResultDetail> _productReviewResultDetailRepository;
        private IGenericRepository<CoordinatorCheckList> _coordinatorCheckListRepository;
        private IGenericRepository<ProductAppraiser> _productAppraiserRepository;
        private IGenericRepository<Location> _locationRepository;
        private IGenericRepository<LogLoanInfoAi> _logLoanInfoAiRepository;
        private IGenericRepository<HubDistribution> _hubDistributionRepository;
        private IGenericRepository<HubLoanBrief> _hubLoanBriefRepository;
        private IGenericRepository<LogRequestAi> _logRequestAiRepository;
        private IGenericRepository<TelesaleLoanbrief> _telesaleLoanbriefRepository;
        private IGenericRepository<PipelineHistory> _pipelineHistoryRepository;
        private IGenericRepository<LogSendVoiceOtp> _logSendVoiceOtpRepository;
        private IGenericRepository<LogPushToHub> _logPushToHubRepository;
        private IGenericRepository<Notification> _notificationRepository;
        private IGenericRepository<TypeOwnerShip> _typeOwnerShipRepository;
        private IGenericRepository<AuthorizationToken> _authorizationTokenRepository;
        private IGenericRepository<ResultEkyc> _resultEkycRepository;
        private IGenericRepository<RequestEvent> _requestEventRepository;
        private IGenericRepository<EventConfig> _eventConfigRepository;
        private IGenericRepository<LogResultAutoCall> _logResultAutoCallRepository;
        private IGenericRepository<LoanbriefLender> _loanbriefLenderRepository;
        private IGenericRepository<QueueLog> _queueLogRepository;
        private IGenericRepository<LoanBriefQuestionScript> _loanBriefQuestionScriptRepository;
        private IGenericRepository<LoanStatusDetail> _loanStatusDetailRepository;
        private IGenericRepository<LogClickToCall> _logClickToCallRepository;
        private IGenericRepository<LogLoanbriefScoring> _logLoanbriefScoring;
        private IGenericRepository<LogSendSms> _logSendSmsRepository;
        private IGenericRepository<TypeRemarketing> _typeRemarketingRepository;
        private IGenericRepository<LoanBriefImportFileExcel> _loanBriefImportFileExcelRepository;
        private IGenericRepository<TelesalesShift> _telesalesShiftRepository;
        private IGenericRepository<TeamTelesales> _teamTelesalesRepository;
        private IGenericRepository<UserTeamTelesales> _userTeamTelesalesRepository;
        private IGenericRepository<CheckLoanInformation> _checkImageRepository;
        private IGenericRepository<ScheduleTime> _scheduleTimeRepository;
        private IGenericRepository<HubEmployeeDistribution> _hubEmployeeDistributionRepository;
        private IGenericRepository<LogDistributingHubEmployee> _logDistributingHubEmployeeRepository;
        private IGenericRepository<ReasonCoordinator> _reasonCoordinatorRepository;
        private IGenericRepository<LogCancelLoanbrief> _logCancelLoanbriefRepository;
        private IGenericRepository<ContactCustomer> _contactCustomerRepository;
        private IGenericRepository<LogCiscoPushToTelesale> _logCiscoPushToTelesaleRepository;
        private IGenericRepository<ConfigDocument> _configDocumentRepository;
        private IGenericRepository<LoanConfigStep> _loanConfigStepRepository;
        private IGenericRepository<InfomationProductDetail> _infomationProductDetailRepository;
        private IGenericRepository<ConfigProductDetail> _configProductDetailRepository;
        private IGenericRepository<LogReLoanbrief> _logReLoanbrief;
        private IGenericRepository<ConfigDepartmentStatus> _configDepartmentStatusRepository;
        private IGenericRepository<ProposeExceptions> _proposeExceptionsRepository;
        private IGenericRepository<LenderConfig> _lenderConfigRepository;
        private IGenericRepository<LenderLoanBrief> _lenderLoanBriefRepository;
        private IGenericRepository<LogInfoCreateLoanbrief> _logInfoCreateLoanbriefRepository;
        private IGenericRepository<LogDistributionUser> _logDistributionUserRepository;
        private IGenericRepository<LogLoanAction> _logLoanActionRepository;
        private IGenericRepository<LogSendOtp> _logSendOtpRepository;
        private IGenericRepository<LogPushNotification> _logPushNotificationRepository;
        private IGenericRepository<CompareDocument> _compareDocumentRepository;
        private IGenericRepository<EsignContract> _esignContractRepository;
        private IGenericRepository<LogPartner> _logPartnerRepository;
        private IGenericRepository<LogActionRestruct> _logActionRestructRepository;
        private IGenericRepository<EkycImages> _ekycImagesRepository;
        private IGenericRepository<LogLoanInfoPartner> _logLoanInfoPartnerRepository;
        private IGenericRepository<SystemConfig> _systemConfigRepository;
        private IGenericRepository<DocumentException> _documentExceptionRepository;
        private IGenericRepository<UserMobileCisco> _userMobileCiscoRepository;
        private IGenericRepository<RuleCheckLoan> _ruleCheckLoanRepository;
        private IGenericRepository<LogChangePhone> _logChangePhoneRepository;
        private IGenericRepository<ConfigContractRate> _configContractRateRepository;
        private IGenericRepository<LogSecuredTransaction> _logSecuredTransactionRepository;
        private IGenericRepository<PushLoanToPartner> _pushLoanToPartnerRepository;
        private IGenericRepository<LogCallApiPartner> _logCallApiPartnerRepository;
        private IGenericRepository<KpiTelesale> _kpiTelesaleRepository;
        private IGenericRepository<KpiTelesaleSummary> _kpiTelesaleSummaryRepository;
        private IGenericRepository<LogPushToPartner> _logPushToPartnerRepository;
        private IGenericRepository<LogCloseLoan> _logCloseLoanRepository;
        private IGenericRepository<LogPushToCisco> _logPushToCiscoRepository;
        private IGenericRepository<LogCallApiCisco> _logCallApiCiscoRepository;

        public UnitOfWork(LOSContext context)
        {
            this.context = context;
        }

        public IGenericRepository<User> UserRepository
        {
            get { return _userRepository ?? (_userRepository = new GenericRepository<User>(context)); }
        }
        public IGenericRepository<UserReference> UserReferenceRepository
        {
            get { return _userReferenceRepository ?? (_userReferenceRepository = new GenericRepository<UserReference>(context)); }
        }

        public IGenericRepository<Group> GroupRepository
        {
            get { return _groupRepository ?? (_groupRepository = new GenericRepository<Group>(context)); }
        }

        public IGenericRepository<Province> ProvinceRepository
        {
            get { return _provinceRepository ?? (_provinceRepository = new GenericRepository<Province>(context)); }
        }

        public IGenericRepository<District> DistrictRepository
        {
            get { return _districtRepository ?? (_districtRepository = new GenericRepository<District>(context)); }
        }

        public IGenericRepository<Ward> WardRepository
        {
            get { return _wardRepository ?? (_wardRepository = new GenericRepository<Ward>(context)); }
        }

        public IGenericRepository<Company> CompanyRepository
        {
            get { return _companyRepository ?? (_companyRepository = new GenericRepository<Company>(context)); }
        }

        public IGenericRepository<Department> DepartmentRepository
        {
            get { return _departmentRepository ?? (_departmentRepository = new GenericRepository<Department>(context)); }
        }

        public IGenericRepository<CompanyDepartment> CompanyDepartmentRepository
        {
            get { return _companyDepartmentRepository ?? (_companyDepartmentRepository = new GenericRepository<CompanyDepartment>(context)); }
        }

        public IGenericRepository<Position> PositionRepository
        {
            get { return _positionRepository ?? (_positionRepository = new GenericRepository<Position>(context)); }
        }

        public IGenericRepository<Project> ProjectRepository
        {
            get { return _projectRepository ?? (_projectRepository = new GenericRepository<Project>(context)); }
        }

        public IGenericRepository<UserProject> UserProjectRepository
        {
            get { return _userProjectRepository ?? (_userProjectRepository = new GenericRepository<UserProject>(context)); }
        }

        public IGenericRepository<LoanProduct> LoanProductRepository
        {
            get { return _loanProductRepository ?? (_loanProductRepository = new GenericRepository<LoanProduct>(context)); }
        }

        public IGenericRepository<LoanBrief> LoanBriefRepository
        {
            get { return _loanBriefRepository ?? (_loanBriefRepository = new GenericRepository<LoanBrief>(context)); }
        }
        public IGenericRepository<LoanBriefDocument> LoanBriefDocumentRepository
        {
            get { return _loanBriefDocumentRepository ?? (_loanBriefDocumentRepository = new GenericRepository<LoanBriefDocument>(context)); }
        }

        public IGenericRepository<LoanBriefHistory> LoanBriefHistoryRepository
        {
            get { return _loanBriefHistoryRepository ?? (_loanBriefHistoryRepository = new GenericRepository<LoanBriefHistory>(context)); }
        }

        public IGenericRepository<LoanBriefHousehold> LoanBriefHouseholdRepository
        {
            get { return _loanBriefHouseholdRepository ?? (_loanBriefHouseholdRepository = new GenericRepository<LoanBriefHousehold>(context)); }
        }

        public IGenericRepository<LoanBriefJob> LoanBriefJobRepository
        {
            get { return _loanBriefJobRepository ?? (_loanBriefJobRepository = new GenericRepository<LoanBriefJob>(context)); }
        }

        public IGenericRepository<LoanBriefNote> LoanBriefNoteRepository
        {
            get { return _loanBriefNoteRepository ?? (_loanBriefNoteRepository = new GenericRepository<LoanBriefNote>(context)); }
        }

        public IGenericRepository<LoanBriefProperty> LoanBriefPropertyRepository
        {
            get { return _loanBriefPropertyRepository ?? (_loanBriefPropertyRepository = new GenericRepository<LoanBriefProperty>(context)); }
        }

        public IGenericRepository<LoanBriefResident> LoanBriefResidentRepository
        {
            get { return _loanBriefResidentRepository ?? (_loanBriefResidentRepository = new GenericRepository<LoanBriefResident>(context)); }
        }

        public IGenericRepository<Job> JobRepository
        {
            get { return _jobRepository ?? (_jobRepository = new GenericRepository<Job>(context)); }
        }

        public IGenericRepository<JobTitle> JobTitleRepository
        {
            get { return _jobTitleRepository ?? (_jobTitleRepository = new GenericRepository<JobTitle>(context)); }
        }

        public IGenericRepository<LoanRateType> LoanRateTypeRepository
        {
            get { return _loanRateTypeRepository ?? (_loanRateTypeRepository = new GenericRepository<LoanRateType>(context)); }
        }

        public IGenericRepository<Module> ModulesRepository
        {
            get { return _moduleRepository ?? (_moduleRepository = new GenericRepository<Module>(context)); }
        }

        public IGenericRepository<UserModule> UserModuleRepository
        {
            get { return _userModuleRepository ?? (_userModuleRepository = new GenericRepository<UserModule>(context)); }
        }

        public IGenericRepository<GroupModule> GroupModuleRepository
        {
            get { return _groupModuleRepository ?? (_groupModuleRepository = new GenericRepository<GroupModule>(context)); }
        }

        public IGenericRepository<GroupSource> GroupSourceRepository
        {
            get { return _groupSourceRepository ?? (_groupSourceRepository = new GenericRepository<GroupSource>(context)); }
        }

        public IGenericRepository<Property> PropertyRepository
        {
            get { return _propertyRepository ?? (_propertyRepository = new GenericRepository<Property>(context)); }
        }

        public IGenericRepository<PropertyValue> PropertyValueRepository
        {
            get { return _propertyValueRepository ?? (_propertyValueRepository = new GenericRepository<PropertyValue>(context)); }
        }

        public IGenericRepository<Script> ScriptRepository
        {
            get { return _scriptRepository ?? (_scriptRepository = new GenericRepository<Script>(context)); }
        }

        public IGenericRepository<ScriptDetail> ScriptDetailRepository
        {
            get { return _scriptDetailRepository ?? (_scriptDetailRepository = new GenericRepository<ScriptDetail>(context)); }
        }
        public IGenericRepository<UserMmo> UserMMORepository
        {
            get { return _userMMORepository ?? (_userMMORepository = new GenericRepository<UserMmo>(context)); }
        }

        public IGenericRepository<UtmSource> UtmSourceRepository
        {
            get { return _utmSourceRepository ?? (_utmSourceRepository = new GenericRepository<UtmSource>(context)); }
        }
        public IGenericRepository<Transactions> TransactionsRepository
        {
            get { return _transactionsRepository ?? (_transactionsRepository = new GenericRepository<Transactions>(context)); }
        }

        public IGenericRepository<UserOnline> UserOnlineRepository
        {
            get { return _userOnlineRepository ?? (_userOnlineRepository = new GenericRepository<UserOnline>(context)); }
        }

        public IGenericRepository<LoanAction> LoanActionRepository
        {
            get { return _loanActionRepository ?? (_loanActionRepository = new GenericRepository<LoanAction>(context)); }
        }

        public IGenericRepository<Action> ActionRepository
        {
            get { return _actionRepository ?? (_actionRepository = new GenericRepository<Action>(context)); }
        }

        public IGenericRepository<ActionStatus> ActionStatusRepository
        {
            get { return _actionStatusRepository ?? (_actionStatusRepository = new GenericRepository<ActionStatus>(context)); }
        }

        public IGenericRepository<Schedule> ScheduleRepository
        {
            get { return _scheduleRepository ?? (_scheduleRepository = new GenericRepository<Schedule>(context)); }
        }

        public IGenericRepository<TlsScenario> TlsScenarioRepository
        {
            get { return _tlsScenarioRepository ?? (_tlsScenarioRepository = new GenericRepository<TlsScenario>(context)); }
        }
        public IGenericRepository<TlsQuestion> TlsQuestionRepository
        {
            get { return _tlsQuestionRepository ?? (_tlsQuestionRepository = new GenericRepository<TlsQuestion>(context)); }
        }

        public IGenericRepository<TlsScenarioQuestion> TlsScenarioQuestionRepository
        {
            get { return _tlsScenarioQuestionRepository ?? (_tlsScenarioQuestionRepository = new GenericRepository<TlsScenarioQuestion>(context)); }
        }

        public IGenericRepository<Pipeline> PipelineRepository
        {
            get { return _pipelineRepository ?? (_pipelineRepository = new GenericRepository<Pipeline>(context)); }
        }

        public IGenericRepository<Section> SectionRepository
        {
            get { return _sectionRepository ?? (_sectionRepository = new GenericRepository<Section>(context)); }
        }
        public IGenericRepository<SectionDetail> SectionDetailRepository
        {
            get { return _sectionDetailRepository ?? (_sectionDetailRepository = new GenericRepository<SectionDetail>(context)); }
        }
        public IGenericRepository<SectionApprove> SectionApproveRepository
        {
            get { return _sectionApproveRepository ?? (_sectionApproveRepository = new GenericRepository<SectionApprove>(context)); }
        }

        public IGenericRepository<SectionAction> SectionActionRepository
        {
            get { return _sectionActionRepository ?? (_sectionActionRepository = new GenericRepository<SectionAction>(context)); }
        }

        public IGenericRepository<SectionCondition> SectionConditionRepository
        {
            get { return _sectionConditionRepository ?? (_sectionConditionRepository = new GenericRepository<SectionCondition>(context)); }
        }

        public IGenericRepository<LoanStatus> LoanStatusRepository
        {
            get { return _loanStatusRepository ?? (_loanStatusRepository = new GenericRepository<LoanStatus>(context)); }
        }

        public IGenericRepository<PipelineState> PipelineStateRepository
        {
            get { return _pipelineStateRepository ?? (_pipelineStateRepository = new GenericRepository<PipelineState>(context)); }
        }

        public IGenericRepository<RelativeFamily> RelativeFamilyRepository
        {
            get { return _relativeFamilyRepository ?? (_relativeFamilyRepository = new GenericRepository<RelativeFamily>(context)); }
        }

        public IGenericRepository<BrandProduct> BrandProductRepository
        {
            get { return _brandProductRepository ?? (_brandProductRepository = new GenericRepository<BrandProduct>(context)); }
        }

        public IGenericRepository<Product> ProductRepository
        {
            get { return _productRepository ?? (_productRepository = new GenericRepository<Product>(context)); }
        }

        public IGenericRepository<Bank> BankRepository
        {
            get { return _bankRepository ?? (_bankRepository = new GenericRepository<Bank>(context)); }
        }

        public IGenericRepository<Customer> CustomerRepository
        {
            get { return _customerRepository ?? (_customerRepository = new GenericRepository<Customer>(context)); }
        }
        public IGenericRepository<LoanBriefCompany> LoanBriefCompanyRepository
        {
            get { return _loanBriefCompanyRepository ?? (_loanBriefCompanyRepository = new GenericRepository<LoanBriefCompany>(context)); }
        }
        public IGenericRepository<LoanBriefRelationship> LoanBriefRelationshipRepository
        {
            get { return _loanBriefRelationshipRepository ?? (_loanBriefRelationshipRepository = new GenericRepository<LoanBriefRelationship>(context)); }
        }
        public IGenericRepository<PlatformType> PlatformTypeRepository
        {
            get { return _platformTypeRepository ?? (_platformTypeRepository = new GenericRepository<PlatformType>(context)); }
        }
        public IGenericRepository<LoanBriefLog> LoanBriefLogRepository
        {
            get { return _loanBriefLogRepository ?? (_loanBriefLogRepository = new GenericRepository<LoanBriefLog>(context)); }
        }
        public IGenericRepository<LoanbriefScript> LoanbriefScriptRepository
        {
            get { return _loanbriefScriptRepository ?? (_loanbriefScriptRepository = new GenericRepository<LoanbriefScript>(context)); }
        }
        public IGenericRepository<ProductPercentReduction> ProductPercentReductionRepository
        {
            get { return _productPercentReductionRepository ?? (_productPercentReductionRepository = new GenericRepository<ProductPercentReduction>(context)); }
        }
        public IGenericRepository<ReasonCancel> ReasonCancelRepository
        {
            get { return _reasonCancelRepository ?? (_reasonCancelRepository = new GenericRepository<ReasonCancel>(context)); }
        }
        public IGenericRepository<TokenSmartDailer> TokenSmartDailerRepository
        {
            get { return _tokenSmartDailerRepository ?? (_tokenSmartDailerRepository = new GenericRepository<TokenSmartDailer>(context)); }
        }
        public IGenericRepository<LogWorking> LogWorkingRepository
        {
            get { return _logWorkingRepository ?? (_logWorkingRepository = new GenericRepository<LogWorking>(context)); }
        }
        public IGenericRepository<Shop> ShopRepository
        {
            get { return _shopRepository ?? (_shopRepository = new GenericRepository<Shop>(context)); }
        }
        public IGenericRepository<UserShop> UserShopRepository
        {
            get { return _userShopRepository ?? (_userShopRepository = new GenericRepository<UserShop>(context)); }
        }
        public IGenericRepository<DocumentType> DocumentTypeRepository
        {
            get { return _documentTypeRepository ?? (_documentTypeRepository = new GenericRepository<DocumentType>(context)); }
        }
        public IGenericRepository<LoanBriefFiles> LoanBriefFileRepository
        {
            get { return _loanBriefFileRepository ?? (_loanBriefFileRepository = new GenericRepository<LoanBriefFiles>(context)); }
        }
        public IGenericRepository<UserCondition> UserConditionRepository
        {
            get { return _userConditionRepository ?? (_userConditionRepository = new GenericRepository<UserCondition>(context)); }
        }
        public IGenericRepository<UserActionPermission> UserActionPermissionRepository
        {
            get { return _userActionPermissionRepository ?? (_userActionPermissionRepository = new GenericRepository<UserActionPermission>(context)); }
        }
        public IGenericRepository<GroupCondition> GroupConditionRepository
        {
            get { return _groupConditionRepository ?? (_groupConditionRepository = new GenericRepository<GroupCondition>(context)); }
        }
        public IGenericRepository<ManagerAreaHub> ManagerAreaHubRepository
        {
            get { return _managerAreaHubRepository ?? (_managerAreaHubRepository = new GenericRepository<ManagerAreaHub>(context)); }
        }
        public IGenericRepository<GroupActionPermission> GroupActionPermissionRepository
        {
            get { return _groupActionPermissionRepository ?? (_groupActionPermissionRepository = new GenericRepository<GroupActionPermission>(context)); }
        }

        public IGenericRepository<AccessToken> AccessTokenRepository
        {
            get { return _accessTokenRepository ?? (_accessTokenRepository = new GenericRepository<AccessToken>(context)); }
        }
        public IGenericRepository<LogCallApi> LogCallApiRepository
        {
            get { return _logCallApiRepository ?? (_logCallApiRepository = new GenericRepository<LogCallApi>(context)); }
        }
        public IGenericRepository<PermissionIp> PermissionIpRepository
        {
            get { return _permissionIpRepository ?? (_permissionIpRepository = new GenericRepository<PermissionIp>(context)); }
        }

        public IGenericRepository<ProductReview> ProductReviewRepository
        {
            get { return _productReviewRepository ?? (_productReviewRepository = new GenericRepository<ProductReview>(context)); }
        }

        public IGenericRepository<ProductReviewResult> ProductReviewResultRepository
        {
            get { return _productReviewResultRepository ?? (_productReviewResultRepository = new GenericRepository<ProductReviewResult>(context)); }
        }
        public IGenericRepository<ProductReviewDetail> ProductReviewDetailRepository
        {
            get { return _productReviewDetailRepository ?? (_productReviewDetailRepository = new GenericRepository<ProductReviewDetail>(context)); }
        }
        public IGenericRepository<ProductReviewResultDetail> ProductReviewResultDetailRepository
        {
            get { return _productReviewResultDetailRepository ?? (_productReviewResultDetailRepository = new GenericRepository<ProductReviewResultDetail>(context)); }
        }
        public IGenericRepository<CoordinatorCheckList> CoordinatorCheckListRepository
        {
            get { return _coordinatorCheckListRepository ?? (_coordinatorCheckListRepository = new GenericRepository<CoordinatorCheckList>(context)); }
        }
        public IGenericRepository<ProductAppraiser> ProductAppraiserRepository
        {
            get { return _productAppraiserRepository ?? (_productAppraiserRepository = new GenericRepository<ProductAppraiser>(context)); }
        }
        public IGenericRepository<Location> LocationRepository
        {
            get { return _locationRepository ?? (_locationRepository = new GenericRepository<Location>(context)); }
        }
        public IGenericRepository<LogLoanInfoAi> LogLoanInfoAiRepository
        {
            get { return _logLoanInfoAiRepository ?? (_logLoanInfoAiRepository = new GenericRepository<LogLoanInfoAi>(context)); }
        }

        public IGenericRepository<HubDistribution> HubDistributionRepository
        {
            get { return _hubDistributionRepository ?? (_hubDistributionRepository = new GenericRepository<HubDistribution>(context)); }
        }
        public IGenericRepository<HubLoanBrief> HubLoanBriefRepository
        {
            get { return _hubLoanBriefRepository ?? (_hubLoanBriefRepository = new GenericRepository<HubLoanBrief>(context)); }
        }

        public IGenericRepository<LogRequestAi> LogRequestAiRepository
        {
            get { return _logRequestAiRepository ?? (_logRequestAiRepository = new GenericRepository<LogRequestAi>(context)); }
        }
        public IGenericRepository<TelesaleLoanbrief> TelesaleLoanbriefRepository
        {
            get { return _telesaleLoanbriefRepository ?? (_telesaleLoanbriefRepository = new GenericRepository<TelesaleLoanbrief>(context)); }
        }

        public IGenericRepository<PipelineHistory> PipelineHistoryRepository
        {
            get { return _pipelineHistoryRepository ?? (_pipelineHistoryRepository = new GenericRepository<PipelineHistory>(context)); }
        }

        public IGenericRepository<LogSendVoiceOtp> LogSendVoiceOtpRepository
        {
            get { return _logSendVoiceOtpRepository ?? (_logSendVoiceOtpRepository = new GenericRepository<LogSendVoiceOtp>(context)); }
        }

        public IGenericRepository<LogPushToHub> LogPushToHubRepository
        {
            get { return _logPushToHubRepository ?? (_logPushToHubRepository = new GenericRepository<LogPushToHub>(context)); }
        }
        public IGenericRepository<Notification> NotificationRepository
        {
            get { return _notificationRepository ?? (_notificationRepository = new GenericRepository<Notification>(context)); }
        }
        public IGenericRepository<AuthorizationToken> AuthorizationTokenRepository
        {
            get { return _authorizationTokenRepository ?? (_authorizationTokenRepository = new GenericRepository<AuthorizationToken>(context)); }
        }

        public IGenericRepository<LogClickToCall> LogClickToCallRepository
        {
            get { return _logClickToCallRepository ?? (_logClickToCallRepository = new GenericRepository<LogClickToCall>(context)); }
        }

        public IGenericRepository<TypeOwnerShip> TypeOwnerShipRepository
        {
            get { return _typeOwnerShipRepository ?? (_typeOwnerShipRepository = new GenericRepository<TypeOwnerShip>(context)); }
        }

        public IGenericRepository<ResultEkyc> ResultEkycRepository
        {
            get { return _resultEkycRepository ?? (_resultEkycRepository = new GenericRepository<ResultEkyc>(context)); }
        }

        public IGenericRepository<RequestEvent> RequestEventRepository
        {
            get { return _requestEventRepository ?? (_requestEventRepository = new GenericRepository<RequestEvent>(context)); }
        }
        public IGenericRepository<EventConfig> EventConfigRepository
        {
            get { return _eventConfigRepository ?? (_eventConfigRepository = new GenericRepository<EventConfig>(context)); }
        }
        public IGenericRepository<LogResultAutoCall> LogResultAutoCallRepository
        {
            get { return _logResultAutoCallRepository ?? (_logResultAutoCallRepository = new GenericRepository<LogResultAutoCall>(context)); }
        }
        public IGenericRepository<QueueLog> QueueLogRepository
        {
            get { return _queueLogRepository ?? (_queueLogRepository = new GenericRepository<QueueLog>(context)); }
        }

        public IGenericRepository<LoanbriefLender> LoanBriefLenderRepository
        {
            get { return _loanbriefLenderRepository ?? (_loanbriefLenderRepository = new GenericRepository<LoanbriefLender>(context)); }
        }

        public IGenericRepository<LoanBriefQuestionScript> LoanBriefQuestionScriptRepository
        {
            get { return _loanBriefQuestionScriptRepository ?? (_loanBriefQuestionScriptRepository = new GenericRepository<LoanBriefQuestionScript>(context)); }
        }
        public IGenericRepository<LoanStatusDetail> LoanStatusDetailRepository
        {
            get { return _loanStatusDetailRepository ?? (_loanStatusDetailRepository = new GenericRepository<LoanStatusDetail>(context)); }
        }
        public IGenericRepository<LogLoanbriefScoring> LogLoanbriefScoringRepository
        {
            get { return _logLoanbriefScoring ?? (_logLoanbriefScoring = new GenericRepository<LogLoanbriefScoring>(context)); }
        }
        public IGenericRepository<LogSendSms> LogSendSmsRepository
        {
            get { return _logSendSmsRepository ?? (_logSendSmsRepository = new GenericRepository<LogSendSms>(context)); }
        }
        public IGenericRepository<TypeRemarketing> TypeRemarketingRepository
        {
            get { return _typeRemarketingRepository ?? (_typeRemarketingRepository = new GenericRepository<TypeRemarketing>(context)); }
        }
        public IGenericRepository<LoanBriefImportFileExcel> LoanBriefImportFileExcelRepository
        {
            get { return _loanBriefImportFileExcelRepository ?? (_loanBriefImportFileExcelRepository = new GenericRepository<LoanBriefImportFileExcel>(context)); }
        }
        public IGenericRepository<TelesalesShift> TelesalesShiftRepository
        {
            get { return _telesalesShiftRepository ?? (_telesalesShiftRepository = new GenericRepository<TelesalesShift>(context)); }
        }
        public IGenericRepository<TeamTelesales> TeamTelesalesRepository
        {
            get { return _teamTelesalesRepository ?? (_teamTelesalesRepository = new GenericRepository<TeamTelesales>(context)); }
        }
        public IGenericRepository<UserTeamTelesales> UserTeamTelesalesRepository
        {
            get { return _userTeamTelesalesRepository ?? (_userTeamTelesalesRepository = new GenericRepository<UserTeamTelesales>(context)); }
        }
        public IGenericRepository<CheckLoanInformation> CheckImageRepository
        {
            get { return _checkImageRepository ?? (_checkImageRepository = new GenericRepository<CheckLoanInformation>(context)); }
        }

        public IGenericRepository<ScheduleTime> ScheduleTimeRepository
        {
            get { return _scheduleTimeRepository ?? (_scheduleTimeRepository = new GenericRepository<ScheduleTime>(context)); }
        }

        public IGenericRepository<HubEmployeeDistribution> HubEmployeeDistributionRepository
        {
            get { return _hubEmployeeDistributionRepository ?? (_hubEmployeeDistributionRepository = new GenericRepository<HubEmployeeDistribution>(context)); }
        }
        public IGenericRepository<LogDistributingHubEmployee> LogDistributingHubEmployeeRepository
        {
            get { return _logDistributingHubEmployeeRepository ?? (_logDistributingHubEmployeeRepository = new GenericRepository<LogDistributingHubEmployee>(context)); }
        }
        public IGenericRepository<ReasonCoordinator> ReasonCoordinatorRepository
        {
            get { return _reasonCoordinatorRepository ?? (_reasonCoordinatorRepository = new GenericRepository<ReasonCoordinator>(context)); }
        }
        public IGenericRepository<LogCancelLoanbrief> LogCancelLoanbriefRepository
        {

            get { return _logCancelLoanbriefRepository ?? (_logCancelLoanbriefRepository = new GenericRepository<LogCancelLoanbrief>(context)); }
        }
        public IGenericRepository<ContactCustomer> ContactCustomerRepository
        {
            get { return _contactCustomerRepository ?? (_contactCustomerRepository = new GenericRepository<ContactCustomer>(context)); }
        }

        public IGenericRepository<LogCiscoPushToTelesale> LogCiscoPushToTelesaleRepository
        {
            get { return _logCiscoPushToTelesaleRepository ?? (_logCiscoPushToTelesaleRepository = new GenericRepository<LogCiscoPushToTelesale>(context)); }
        }

        public IGenericRepository<ConfigDocument> ConfigDocumentRepository
        {
            get { return _configDocumentRepository ?? (_configDocumentRepository = new GenericRepository<ConfigDocument>(context)); }
        }
        public IGenericRepository<LoanConfigStep> LoanConfigStepRepository
        {
            get { return _loanConfigStepRepository ?? (_loanConfigStepRepository = new GenericRepository<LoanConfigStep>(context)); }
        }

        public IGenericRepository<InfomationProductDetail> InfomationProductDetailRepository
        {
            get { return _infomationProductDetailRepository ?? (_infomationProductDetailRepository = new GenericRepository<InfomationProductDetail>(context)); }
        }

        public IGenericRepository<ConfigProductDetail> ConfigProductDetailRepository
        {
            get { return _configProductDetailRepository ?? (_configProductDetailRepository = new GenericRepository<ConfigProductDetail>(context)); }
        }
        public IGenericRepository<LogReLoanbrief> LogReLoanbriefRepository
        {
            get { return _logReLoanbrief ?? (_logReLoanbrief = new GenericRepository<LogReLoanbrief>(context)); }
        }

        public IGenericRepository<ConfigDepartmentStatus> ConfigDepartmentStatusRepository
        {
            get { return _configDepartmentStatusRepository ?? (_configDepartmentStatusRepository = new GenericRepository<ConfigDepartmentStatus>(context)); }
        }

        public IGenericRepository<ProposeExceptions> ProposeExceptionsRepository
        {
            get { return _proposeExceptionsRepository ?? (_proposeExceptionsRepository = new GenericRepository<ProposeExceptions>(context)); }
        }
        public IGenericRepository<LenderConfig> LenderConfigRepository
        {
            get { return _lenderConfigRepository ?? (_lenderConfigRepository = new GenericRepository<LenderConfig>(context)); }
        }

        public IGenericRepository<LenderLoanBrief> LenderLoanBriefRepository
        {
            get { return _lenderLoanBriefRepository ?? (_lenderLoanBriefRepository = new GenericRepository<LenderLoanBrief>(context)); }
        }

        public IGenericRepository<LogInfoCreateLoanbrief> LogInfoCreateLoanbriefRepository
        {
            get { return _logInfoCreateLoanbriefRepository ?? (_logInfoCreateLoanbriefRepository = new GenericRepository<LogInfoCreateLoanbrief>(context)); }
        }

        public IGenericRepository<LogDistributionUser> LogDistributionUserRepository
        {
            get { return _logDistributionUserRepository ?? (_logDistributionUserRepository = new GenericRepository<LogDistributionUser>(context)); }
        }

        public IGenericRepository<LogLoanAction> LogLoanActionRepository
        {
            get { return _logLoanActionRepository ?? (_logLoanActionRepository = new GenericRepository<LogLoanAction>(context)); }
        }
        public IGenericRepository<LogSendOtp> LogSendOtpRepository
        {
            get { return _logSendOtpRepository ?? (_logSendOtpRepository = new GenericRepository<LogSendOtp>(context)); }
        }

        public IGenericRepository<LogPushNotification> LogPushNotificationRepository
        {
            get { return _logPushNotificationRepository ?? (_logPushNotificationRepository = new GenericRepository<LogPushNotification>(context)); }
        }

        public IGenericRepository<CompareDocument> CompareDocumentRepository
        {
            get { return _compareDocumentRepository ?? (_compareDocumentRepository = new GenericRepository<CompareDocument>(context)); }
        }

        public IGenericRepository<EsignContract> EsignContractRepository
        {
            get { return _esignContractRepository ?? (_esignContractRepository = new GenericRepository<EsignContract>(context)); }
        }

        public IGenericRepository<LogPartner> LogPartnerRepository
        {
            get { return _logPartnerRepository ?? (_logPartnerRepository = new GenericRepository<LogPartner>(context)); }
        }
        public IGenericRepository<LogActionRestruct> LogActionRestructRepository
        {
            get { return _logActionRestructRepository ?? (_logActionRestructRepository = new GenericRepository<LogActionRestruct>(context)); }
        }

        public IGenericRepository<EkycImages> EkycImagesRepository
        {
            get { return _ekycImagesRepository ?? (_ekycImagesRepository = new GenericRepository<EkycImages>(context)); }
        }

        public IGenericRepository<LogLoanInfoPartner> LogLoanInfoPartnerRepository
        {
            get { return _logLoanInfoPartnerRepository ?? (_logLoanInfoPartnerRepository = new GenericRepository<LogLoanInfoPartner>(context)); }
        }
        public IGenericRepository<SystemConfig> SystemConfigRepository
        {
            get { return _systemConfigRepository ?? (_systemConfigRepository = new GenericRepository<SystemConfig>(context)); }
        }
        public IGenericRepository<DocumentException> DocumentExceptionRepository
        {
            get { return _documentExceptionRepository ?? (_documentExceptionRepository = new GenericRepository<DocumentException>(context)); }
        }

        public IGenericRepository<UserMobileCisco> UserMobileCiscoRepository
        {
            get { return _userMobileCiscoRepository ?? (_userMobileCiscoRepository = new GenericRepository<UserMobileCisco>(context)); }
        }

        public IGenericRepository<RuleCheckLoan> RuleCheckLoanRepository
        {
            get { return _ruleCheckLoanRepository ?? (_ruleCheckLoanRepository = new GenericRepository<RuleCheckLoan>(context)); }
        }

        public IGenericRepository<LogChangePhone> LogChangePhoneRepository
        {
            get { return _logChangePhoneRepository ?? (_logChangePhoneRepository = new GenericRepository<LogChangePhone>(context)); }
        }

        public IGenericRepository<ConfigContractRate> ConfigContractRateRepository
        {
            get { return _configContractRateRepository ?? (_configContractRateRepository = new GenericRepository<ConfigContractRate>(context)); }
        }

        public IGenericRepository<LogSecuredTransaction> LogSecuredTransactionRepository
        {
            get { return _logSecuredTransactionRepository ?? (_logSecuredTransactionRepository = new GenericRepository<LogSecuredTransaction>(context)); }
        }
        public IGenericRepository<PushLoanToPartner> PushLoanToPartnerRepository
        {
            get { return _pushLoanToPartnerRepository ?? (_pushLoanToPartnerRepository = new GenericRepository<PushLoanToPartner>(context)); }
        }

        public IGenericRepository<LogCallApiPartner> LogCallApiPartnerRepository
        {
            get { return _logCallApiPartnerRepository ?? (_logCallApiPartnerRepository = new GenericRepository<LogCallApiPartner>(context)); }
        }

        public IGenericRepository<KpiTelesale> KpiTelesaleRepository
        {
            get { return _kpiTelesaleRepository ?? (_kpiTelesaleRepository = new GenericRepository<KpiTelesale>(context)); }
        }

        public IGenericRepository<KpiTelesaleSummary> KpiTelesaleSummaryRepository
        {
            get { return _kpiTelesaleSummaryRepository ?? (_kpiTelesaleSummaryRepository = new GenericRepository<KpiTelesaleSummary>(context)); }
        }

        public IGenericRepository<LogPushToPartner> LogPushToPartnerRepository
        {
            get { return _logPushToPartnerRepository ?? (_logPushToPartnerRepository = new GenericRepository<LogPushToPartner>(context)); }
        }

        public IGenericRepository<LogCloseLoan> LogCloseLoanRepository
        {
            get { return _logCloseLoanRepository ?? (_logCloseLoanRepository = new GenericRepository<LogCloseLoan>(context)); }
        }
        public IGenericRepository<LogPushToCisco> LogPushToCiscoRepository
        {
            get { return _logPushToCiscoRepository ?? (_logPushToCiscoRepository = new GenericRepository<LogPushToCisco>(context)); }
        }
        public IGenericRepository<LogCallApiCisco> LogCallApiCiscoRepository
        {
            get { return _logCallApiCiscoRepository ?? (_logCallApiCiscoRepository = new GenericRepository<LogCallApiCisco>(context)); }
        }


        public void Save()
        {
            context.SaveChanges();
        }
        public void SaveChanges()
        {
            var modifiedEntries = context.ChangeTracker.Entries().Where(x => x.State == EntityState.Added || x.State == EntityState.Modified);
            foreach (var entity in modifiedEntries)
            {
                //((EntityInformation)entity.Entity).ModifiedDate = DateTime.Now;
                //((EntityInformation)entity.Entity).ModifiedUserName = userName;
            }
            context.SaveChanges();
        }

        public void BeginTransaction()
        {
            _dbContextTransaction = context.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            SaveChanges();
            try
            {
                SaveChanges();
                _dbContextTransaction.Commit();
            }
            finally
            {
                _dbContextTransaction.Dispose();
            }
        }
        public void RollbackTransaction()
        {
            _dbContextTransaction.Rollback();
            _dbContextTransaction.Dispose();
        }

        public LOSContext GetContext()
        {
            return context;
        }
        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                    if (_dbContextTransaction != null)
                        _dbContextTransaction.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}
