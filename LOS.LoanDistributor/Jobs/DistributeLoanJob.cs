﻿using Hazelcast.Client;
using Hazelcast.Config;
using Hazelcast.Core;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.LoanDistributor.Jobs
{
	public class DistributeLoanJob : IBaseJob
	{	
		public DistributeLoanJob()
		{
		
		}

		public void Process()
		{
			// create new unit of work instance			
			var builder = new ConfigurationBuilder()
			  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
			  .AddJsonFile("appsettings.json");
			var configuration = builder.Build();
			var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
			optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
			DbContextOptions<LOSContext> options = optionsBuilder.Options;
			var db = new LOSContext(options);
			var unitOfWork = new UnitOfWork(db);
			// get online users
			var now = DateTimeOffset.Now;
			//var onlineUsers = await unitOfWork.UserOnlineRepository.Query(x => x.IsOnline == true && x.IsWorking == true && x.IsProcessing == false && x.LastTriggerTime.AddMinutes(1) >= DateTime.Now, null, false).ToListAsync();
			//var onlineUsers = (from userOnline in unitOfWork.UserOnlineRepository.All()
			//				   join user in unitOfWork.UserRepository.All() on userOnline.UserId equals user.UserId
			//				   where userOnline.IsOnline == true && userOnline.IsWorking == true && userOnline.IsProcessing == false && userOnline.LastTriggerTime.AddMinutes(1) >= now							   
			//				   select new
			//				   {
			//					   user = user
			//				   }).ToList();
			// read online user from hazel
			var cfg = new ClientConfig();
			// set group name and password
			// GroupConfig groupConfig = new GroupConfig();
			// groupConfig.SetName("dev");
			// grougroupConfigp.SetPassword("J7MLhWQtUC");
			// cfg.SetGroupConfig(groupConfig);
			cfg.GetNetworkConfig().AddAddress("178.128.86.175:5701");
			var client = HazelcastClient.NewHazelcastClient(cfg);
			try
			{
				//get session				
#if !DEBUG
				var users = client.GetMap<int, HazelcastJsonValue>("online_users");
#else
				var users = client.GetMap<int, HazelcastJsonValue>("online_users_test");
#endif
				if (users != null)
				{
					var values = users.Values();
					var onlineUsers = new List<UserOnline>();
					// check user has schedule calls, less than 3 minutes and status = 0						
					var schelduleCalls = unitOfWork.ScheduleRepository.Query(x => x.ScheduleTime.Value.AddMinutes(-3) < now && x.ScheduleTime.Value.AddMinutes(10) > now &&  x.Status == 0, null, false).Select(x => x.UserId).ToList();
					foreach (var value in values)
					{
						var onlineUser = JsonConvert.DeserializeObject<UserOnline>(value.ToString());
						// check if user trigger online in a minute						
						if (onlineUser.IsOnline.Value && onlineUser.IsWorking.Value && !onlineUser.IsProcessing.Value && onlineUser.LastTriggerTime.Value.AddMinutes(1) > now)
						{
							if (schelduleCalls != null && schelduleCalls.Count > 0)
							{
								if (!schelduleCalls.Contains(onlineUser.UserId))
									onlineUsers.Add(onlineUser);
							}
							else
							{
								onlineUsers.Add(onlineUser);
							}
						}
					 }

					if (onlineUsers != null && onlineUsers.Count > 0)
					{
						var factory = new ConnectionFactory() { HostName = "178.128.86.175", Port = 5673 };
						using (var connection = factory.CreateConnection())
						using (var channel = connection.CreateModel())
						{
							var loans = (from loan in unitOfWork.LoanBriefRepository.All()
										 join district in unitOfWork.DistrictRepository.All() on loan.DistrictId equals district.DistrictId into LoanDistricts
										 from LoanDistrict in LoanDistricts.DefaultIfEmpty()
										 join province in unitOfWork.ProvinceRepository.All() on loan.ProvinceId equals province.ProvinceId into LoanProvinces
										 from LoanProvince in LoanProvinces.DefaultIfEmpty()
										 join product in unitOfWork.LoanProductRepository.All() on loan.ProductId equals product.LoanProductId into LoanProducts
										 from LoanProduct in LoanProducts.DefaultIfEmpty()
										 join loanAction in unitOfWork.LoanActionRepository.Query(x => x.ActionStatusId >= 14, x => x.OrderByDescending(x1 => x1.StartTime), 1, 1, false, x => x.ActionStatus) on loan.LoanBriefId equals loanAction.LoanBriefId into LoanActions
										 from LoanAction in LoanActions.DefaultIfEmpty()
										 where loan.Status == 10
										 orderby loan.UpdatedTime descending
										 select new LoanBriefTelesale()
										 {
											 CreatedTime = loan.CreatedTime,
											 DistrictId = loan.DistrictId,
											 District = LoanDistrict != null ? new DistrictDTO()
											 {
												 DistrictId = LoanDistrict.DistrictId,
												 Name = LoanDistrict.Name
											 } : null,
											 FullName = loan.FullName,
											 LoanAmount = loan.LoanAmount,
											 IsRetrieved = false,
											 LoanBriefId = loan.LoanBriefId,
											 LoanTime = loan.LoanTime,
											 Phone = loan.Phone,
											 Product = LoanProduct != null ? new LoanProductDTO()
											 {
												 LoanProductId = LoanProduct.LoanProductId,
												 Name = LoanProduct.Name
											 } : null,
											 ProductId = loan.ProductId,
											 Province = LoanProvince != null ? new ProvinceDTO()
											 {
												 ProvinceId = LoanProvince.ProvinceId,
												 Name = LoanProvince.Name
											 } : null,
											 ProvinceId = loan.ProvinceId,
											 RateTypeId = loan.RateTypeId,
											 Status = loan.Status,
											 LoanAction = LoanAction != null ? new LoanActionDetail()
											 {
												 LoanActionId = LoanAction.LoanActionId,
												 StartTime = LoanAction.StartTime,
												 FinishTime = LoanAction.FinishTime,
												 Note = LoanAction.Note,
												 ActionStatusText = LoanAction.ActionStatus.Description,
												 ActorId = LoanAction.ActorId
											 } : null
										 }).ToList();
							if (loans.Count > 0)
							{								
								if (loans.Count > onlineUsers.Count)
								{
									// put to any users
									int index = 0;
									foreach (var loan in loans)
									{										
										if (index == onlineUsers.Count)
											break;
										// check last loan action sent to telesales
										// var loanAction = unitOfWork.LoanActionRepository.Query(x => x.LoanBriefId == loan.LoanBriefId, x => x.OrderByDescending(x1 => x1.StartTime), 1, 1, false);										
										// end check
										var randomUser = onlineUsers.OrderBy(x => Guid.NewGuid()).FirstOrDefault();
										// push to users
										var loanAction = unitOfWork.LoanActionRepository.Insert(new LoanAction()
										{
											ActionStatusId = 2, // SENT_TO_TELESALE
											ActorId = randomUser.UserId,
											LoanBriefId = loan.LoanBriefId,
											StartTime = now							
										});
										// change status loan
										unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loan.LoanBriefId, x => new LoanBrief()
										{
											Status = 11,
											UpdatedTime = now
										});
										unitOfWork.Save();
										// update hazel user
										randomUser.IsProcessing = true;
										users.Set(randomUser.UserId.Value, new HazelcastJsonValue(JsonConvert.SerializeObject(randomUser)));
										// assign loan to users
										loan.IsRetrieved = false;
										loan.UserId = randomUser.UserId;
										loan.Username = randomUser.Username;
										loan.LoanActionId = loanAction.LoanActionId;
										// send mq to client
#if !DEBUG
										channel.QueueDeclare(queue: "telesale_queue",
												 durable: false,
												 exclusive: false,
												 autoDelete: false,
												 arguments: null);
										JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
										serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
										var message = JsonConvert.SerializeObject(loan,serializerSettings);
										var body = Encoding.UTF8.GetBytes(message);

										channel.BasicPublish(exchange: "los.exchange",
															 routingKey: "los.route",
															 basicProperties: null,
															 body: body);
#else
										
										channel.QueueDeclare(queue: "telesale_queue_test",
												 durable: false,
												 exclusive: false,
												 autoDelete: false,
												 arguments: null);
										JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
										serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
										var message = JsonConvert.SerializeObject(loan, serializerSettings);
										var body = Encoding.UTF8.GetBytes(message);

										channel.BasicPublish(exchange: "los.exchange.test",														
															 routingKey: "los.route.test",
															 basicProperties: null,
															 body: body);
#endif
										Console.WriteLine("----- Loan Brief Sent {0}", message);
										index++;
									}
								}
								else
								{
									// put to any users
									foreach (var loan in loans)
									{
										var randomUser = onlineUsers.OrderBy(x => Guid.NewGuid()).FirstOrDefault();
										// push to users
										var loanAction = unitOfWork.LoanActionRepository.Insert(new LoanAction()
										{
											ActionStatusId = 2, // SENT_TO_TELESALE
											ActorId = randomUser.UserId,
											LoanBriefId = loan.LoanBriefId,
											StartTime = now
										});
										// change status loan
										unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loan.LoanBriefId, x => new LoanBrief()
										{
											Status = 11,
											UpdatedTime = now			
										});
										unitOfWork.Save();
										// update hazel
										randomUser.IsProcessing = true;
										users.Set(randomUser.UserId.Value, new HazelcastJsonValue(JsonConvert.SerializeObject(randomUser)));
										// assign loan to users
										loan.IsRetrieved = false;
										loan.UserId = randomUser.UserId;
										loan.Username = randomUser.Username;
										loan.LoanActionId = loanAction.LoanActionId;
										// send mq to client
#if !DEBUG
										channel.QueueDeclare(queue: "telesale_queue",
												 durable: false,
												 exclusive: false,
												 autoDelete: false,
												 arguments: null);
										JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
										serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
										var message = JsonConvert.SerializeObject(loan,serializerSettings);
										var body = Encoding.UTF8.GetBytes(message);

										channel.BasicPublish(exchange: "los.exchange",
															 routingKey: "los.route",
															 basicProperties: null,
															 body: body);
#else
										channel.QueueDeclare(queue: "telesale_queue_test",
												 durable: false,
												 exclusive: false,
												 autoDelete: false,
												 arguments: null);
										JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
										serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
										var message = JsonConvert.SerializeObject(loan, serializerSettings);
										var body = Encoding.UTF8.GetBytes(message);

										channel.BasicPublish(exchange: "los.exchange.test",
															 routingKey: "los.route.test",
															 basicProperties: null,
															 body: body);
#endif
										Console.WriteLine("----- Loan Brief Sent {0}", message);
									}
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
			}
			finally
			{
				client.Shutdown();
			}
			unitOfWork.Dispose();
		}
	}
}
