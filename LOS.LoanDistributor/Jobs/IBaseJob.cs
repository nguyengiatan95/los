﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.LoanDistributor.Jobs
{
	public interface IBaseJob
	{
		void Process();
	}
}
