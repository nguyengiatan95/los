﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.LoanDistributor.Jobs
{
	public interface IUpdateOnlineJob
	{
		void UpdateUserOnline();
	}
}
