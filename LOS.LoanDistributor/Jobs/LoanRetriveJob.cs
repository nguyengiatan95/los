﻿using Hazelcast.Client;
using Hazelcast.Config;
using Hazelcast.Core;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.LoanDistributor.Jobs
{
	public class LoanRetriveJob : IBaseJob
	{
		public LoanRetriveJob()
		{

		}

		public void Process()
		{
			// create new unit of work instance			
			var builder = new ConfigurationBuilder()
			  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
			  .AddJsonFile("appsettings.json");
			var configuration = builder.Build();
			var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
			optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
			DbContextOptions<LOSContext> options = optionsBuilder.Options;
			var db = new LOSContext(options);
			var unitOfWork = new UnitOfWork(db);
			// check all loan sent to telesales
			var now = DateTimeOffset.Now;
			var loanActions = unitOfWork.LoanActionRepository.Query(x => x.ActionStatusId == 2 && x.StartTime < now.AddMinutes(-3), null, false).ToList();
			var factory = new ConnectionFactory() { HostName = "178.128.86.175", Port = 5673 };
			using (var connection = factory.CreateConnection())
			using (var channel = connection.CreateModel())
			{
				foreach (var loanAction in loanActions)
				{
					// start retrieve loan				
					// update loan brief
					unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanAction.LoanBriefId, x => new LoanBrief()
					{
						Status = 10, // Back to telesale call
						BoundTelesaleId = null
					});
					// update loan action
					unitOfWork.LoanActionRepository.Update(x => x.LoanActionId == loanAction.LoanActionId, x => new LoanAction()
					{
						ActionStatusId = 6, // Telesale missed,
						LastActionStatusId = loanAction.ActionStatusId.Value
					});
					unitOfWork.Save();
					// update hazel					
					var cfg = new ClientConfig();
					cfg.GetNetworkConfig().AddAddress("178.128.86.175:5701");
					var client = HazelcastClient.NewHazelcastClient(cfg);					
					try
					{
#if !DEBUG
				var users = client.GetMap<int, HazelcastJsonValue>("online_users");
#else
						var users = client.GetMap<int, HazelcastJsonValue>("online_users_test");
#endif
						if (users != null)
						{
							var value = users.Get(loanAction.ActorId);							
							var onlineUser = JsonConvert.DeserializeObject<UserOnline>(value.ToString());		
							if (onlineUser != null)
							{
								onlineUser.IsProcessing = false;
								users.Set(onlineUser.UserId.Value, new HazelcastJsonValue(JsonConvert.SerializeObject(onlineUser)));
								// send mq to clien to notify loan has retrived		
								var loan = new LoanBriefTelesale()
								{
									IsRetrieved = true,
									LoanBriefId = loanAction.LoanBriefId.Value,
									Username = onlineUser.Username
								};
#if !DEBUG
										channel.QueueDeclare(queue: "telesale_queue",
												 durable: false,
												 exclusive: false,
												 autoDelete: false,
												 arguments: null);
										JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
										serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
										var message = JsonConvert.SerializeObject(loan,serializerSettings);
										var body = Encoding.UTF8.GetBytes(message);

										channel.BasicPublish(exchange: "los.exchange",
															 routingKey: "los.route",
															 basicProperties: null,
															 body: body);
#else
								channel.QueueDeclare(queue: "telesale_queue_test",
										 durable: false,
										 exclusive: false,
										 autoDelete: false,
										 arguments: null);
								JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
								serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
								var message = JsonConvert.SerializeObject(loan, serializerSettings);
								var body = Encoding.UTF8.GetBytes(message);

								channel.BasicPublish(exchange: "los.exchange.test",
													 routingKey: "los.route.test",
													 basicProperties: null,
													 body: body);
#endif
								Console.WriteLine("----- Loan Brief Retrieved {0}", message);
							}
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
					}
					finally
					{
						client.Shutdown();
					}
					// end retrive loan
				}
			}
			unitOfWork.Dispose();
		}
	}
}
