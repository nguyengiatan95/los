﻿using Hazelcast.Client;
using Hazelcast.Config;
using Hazelcast.Core;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.LoanDistributor.Jobs
{
	public class UpdateOnlineJob : IUpdateOnlineJob
	{
		private UnitOfWork unitOfWork;

		public UpdateOnlineJob(UnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

		public void UpdateUserOnline()
		{
			var cfg = new ClientConfig();
			// set group name and password
			// GroupConfig groupConfig = new GroupConfig();
			// groupConfig.SetName("dev");
			// grougroupConfigp.SetPassword("J7MLhWQtUC");
			// cfg.SetGroupConfig(groupConfig);
			cfg.GetNetworkConfig().AddAddress("178.128.86.175:5701");
			var client = HazelcastClient.NewHazelcastClient(cfg);
			try
			{
				//get session				
#if !DEBUG
				var users = client.GetMap<int, HazelcastJsonValue>("online_users");
#else
				var users = client.GetMap<int, HazelcastJsonValue>("online_users_test");
#endif
				if (users != null)
				{
					var values = users.Values();
					foreach (var value in values)
					{
						try
						{
							var userOnline = JsonConvert.DeserializeObject<OnlineUser>(value.ToString());
							if (userOnline != null)
							{
								var now = DateTimeOffset.Now;
								var exist = unitOfWork.UserOnlineRepository.Query(x => x.UserId == userOnline.UserId && x.Date == now, null, false).Any();
								if (exist)
								{
									unitOfWork.UserOnlineRepository.Update(x => x.UserId == userOnline.UserId && x.Date == now, x => new UserOnline()
									{
										IsOnline = userOnline.IsOnline,
										IsProcessing = userOnline.IsProcessing,
										IsWorking = userOnline.IsWorking,
										OnlineData = JsonConvert.SerializeObject(userOnline.OnlineData),
										OnlineTime = userOnline.OnlineTime,
										RestTime = userOnline.RestTime,
										WorkingTime = userOnline.WorkingTime
									});
									unitOfWork.Save();
								}
								else
								{
									if (userOnline.LastTriggerTime.DayOfYear == now.DayOfYear)
									{
										// create new day
										unitOfWork.UserOnlineRepository.Insert(new UserOnline()
										{
											IsOnline = userOnline.IsOnline,
											IsProcessing = userOnline.IsProcessing,
											IsWorking = userOnline.IsWorking,
											OnlineData = JsonConvert.SerializeObject(userOnline.OnlineData),
											OnlineTime = userOnline.OnlineTime,
											RestTime = userOnline.RestTime,
											WorkingTime = userOnline.WorkingTime,
											Date = DateTime.Now,
											UserId = userOnline.UserId
										});
										unitOfWork.Save();
									}
								}
								// dispose
								unitOfWork.Dispose();
							}
						}
						catch (Exception ex)
						{
							continue;
						}
					}
				}
			}
			catch (Exception ex)
			{

			}
		}
	}
}
