﻿using Hazelcast.Client;
using Hazelcast.Config;
using Hazelcast.Core;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.LoanDistributor.Jobs
{
	public class UpdateUserJob : IBaseJob
	{
		public UpdateUserJob()
		{

		}

		public void Process()
		{
			var cfg = new ClientConfig();
			cfg.GetNetworkConfig().AddAddress("178.128.86.175:5701");
			var client = HazelcastClient.NewHazelcastClient(cfg);
			try
			{
				//get session				
#if !DEBUG
				var users = client.GetMap<int, HazelcastJsonValue>("online_users");
#else
				var users = client.GetMap<int, HazelcastJsonValue>("online_users_test");
#endif
				if (users != null)
				{
					var values = users.Values();
					// create unit of work instance
					var builder = new ConfigurationBuilder()
					  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
					  .AddJsonFile("appsettings.json");
					var configuration = builder.Build();
					var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
					optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
					DbContextOptions<LOSContext> options = optionsBuilder.Options;
					var db = new LOSContext(options);
					var unitOfWork = new UnitOfWork(db);
					Console.WriteLine("----- Updating user online data");
					foreach (var value in values)
					{					
						try
						{
							Console.WriteLine(value.ToString());
							var userOnline = JsonConvert.DeserializeObject<OnlineUser>(value.ToString());
							if (userOnline != null)
							{
								var now = DateTimeOffset.Now;
								// check if row exists
								var exist = unitOfWork.UserOnlineRepository.Query(x => x.UserId == userOnline.UserId && x.Date.Value.DayOfYear == now.DayOfYear, null, false).Any();
								if (exist)
								{
									unitOfWork.UserOnlineRepository.Update(x => x.UserId == userOnline.UserId && x.Date.Value.DayOfYear == now.DayOfYear, x => new UserOnline()
									{
										IsOnline = userOnline.IsOnline,
										IsProcessing = userOnline.IsProcessing,
										IsWorking = userOnline.IsWorking,
										OnlineData = JsonConvert.SerializeObject(userOnline.OnlineData),
										OnlineTime = userOnline.OnlineTime,
										RestTime = userOnline.RestTime,
										WorkingTime = userOnline.WorkingTime,
										LoginTime = userOnline.LoginTime,
										LastTriggerTime = userOnline.LastTriggerTime
									});
									unitOfWork.Save();
								}
								else
								{
									// create new day
									unitOfWork.UserOnlineRepository.Insert(new UserOnline()
									{
										IsOnline = userOnline.IsOnline,
										IsProcessing = userOnline.IsProcessing,
										IsWorking = userOnline.IsWorking,
										OnlineData = JsonConvert.SerializeObject(userOnline.OnlineData),
										OnlineTime = userOnline.OnlineTime,
										RestTime = userOnline.RestTime,
										WorkingTime = userOnline.WorkingTime,
										Date = DateTime.Now,
										UserId = userOnline.UserId,
										Username = userOnline.Username,
										LoginTime = userOnline.LoginTime,
										LastTriggerTime = userOnline.LastTriggerTime
									});
									unitOfWork.Save();
								}
								// check if last trigger time less than 5 minutes
								if (userOnline.LastTriggerTime.AddMinutes(5) < now)
								{
									userOnline.IsProcessing = false;
									userOnline.IsOnline = false;
									userOnline.IsWorking = false;
									users.Set(userOnline.UserId, new HazelcastJsonValue(JsonConvert.SerializeObject(userOnline)));
								}
								// dispose
								unitOfWork.Dispose();
							}
							
						}
						catch (Exception ex)
						{
							Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
							continue;
						}
					}
					Console.WriteLine("----- End Updating user online data");
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
			}
		}
	}
}
