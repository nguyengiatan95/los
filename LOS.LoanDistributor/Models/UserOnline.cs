﻿using Hazelcast.IO.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.LoanDistributor
{
	public class OnlineUser
	{
		public int UserId { get; set; }
		public string Username { get; set; }
		public int? GroupId { get; set; }
		public DateTime LoginTime { get; set; }
		public DateTime LastTriggerTime { get; set; }
		public int OnlineTime { get; set; }
		public int WorkingTime { get; set; }
		public int RestTime { get; set; }
		public bool IsOnline { get; set; }
		public bool IsWorking { get; set; }
		public bool IsProcessing { get; set; }
		public List<OnlineData> OnlineData { get; set; }
	}

	public class OnlineData
	{
		public DateTime StartTime { get; set; }
		public DateTime EndTime { get; set; }
		public int Duration { get; set; }
	}
}
