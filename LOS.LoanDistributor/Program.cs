﻿using Hangfire;
using Hangfire.MemoryStorage;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.LoanDistributor.Jobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using Serilog;
using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Threading;
using System.Runtime.Loader;

namespace LOS.LoanDistributor
{
	class Program
	{
		private static IServiceProvider _serviceProvider;
		private static readonly AutoResetEvent autoResetEvent = new AutoResetEvent(false);
		static void Main(string[] args)
		{		
			// RegisterServices();
			
			// create hangfire service, update online user to database
			GlobalConfiguration.Configuration.UseMemoryStorage();
			// GlobalConfiguration.Configuration.UseActivator(new ContainerJobActivator(_serviceProvider));

			using (var server = new BackgroundJobServer())
			{
				Console.WriteLine("Hangfire Server started. Press ENTER to exit...");
				// Using Dependency Injection
				// RecurringJob.AddOrUpdate<UpdateOnlineJob>("UpdateUserOnline", x => x.UpdateUserOnline(), Cron.Minutely());				
				// BackgroundJob.Enqueue<UpdateOnlineJob>(x => x.UpdateUserOnline());
				// Using instance
				#region update user online
				RecurringJob.AddOrUpdate<UpdateUserJob>("UpdateUserOnline", x => x.Process(), Cron.MinuteInterval(5));
				#endregion				
				#region distribute loan
				BackgroundJob.Enqueue<DistributeLoanJob>(x => x.Process());
				RecurringJob.AddOrUpdate<DistributeLoanJob>("DistributeLoanJob", x => x.Process(), Cron.MinuteInterval(1));
				#endregion
				#region retrive loan
				BackgroundJob.Enqueue<LoanRetriveJob>(x => x.Process());
				RecurringJob.AddOrUpdate<LoanRetriveJob>("RetriveLoanJob", x => x.Process(), "*/30 * * * * *");
				#endregion
				#region schedule call
				BackgroundJob.Enqueue<ScheduleJob>(x => x.Process());
				RecurringJob.AddOrUpdate<ScheduleJob>("ScheduleJob", x => x.Process(), Cron.MinuteInterval(1));
				#endregion
				AppDomain.CurrentDomain.ProcessExit += (o, e) =>
				{
					server.Dispose();
					Console.WriteLine("Terminating...");
					autoResetEvent.Set();
				};
				autoResetEvent.WaitOne();
			}		
		}

		private static void RegisterServices()
		{
			var collection = new ServiceCollection();
			var builder = new ConfigurationBuilder()
					  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
					  .AddJsonFile("appsettings.json");
			var configuration = builder.Build();
			var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
			collection.AddDbContext<LOSContext>(options =>
				options.UseSqlServer(configuration.GetConnectionString("LOSDatabase")));			
			collection.AddTransient<IUnitOfWork, UnitOfWork>();
			collection.AddTransient<IUpdateOnlineJob, UpdateOnlineJob>();
			_serviceProvider = collection.BuildServiceProvider();
		}
		private static void DisposeServices()
		{
			if (_serviceProvider == null)
			{
				return;
			}
			if (_serviceProvider is IDisposable)
			{
				((IDisposable)_serviceProvider).Dispose();
			}
		}
	}
}
