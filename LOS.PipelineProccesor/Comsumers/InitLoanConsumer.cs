﻿using LOS.Common.Helpers;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers;
using LOS.DAL.UnitOfWork;
using LOS.PipelineProccesor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace LOS.PipelineProccesor.Comsumers
{
	public class InitLoanConsumer : IHostedService, IDisposable
	{
		private static IConnection connection;
		private IUnitOfWork unitOfWork;
		private List<PipelineDTO> pipelines;
		private Timer _timer;
		IConfiguration configuration;
		private int current_thread = 0;

		public InitLoanConsumer(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;	
			_timer = new Timer(getPipeline, null, TimeSpan.FromSeconds(5),
				TimeSpan.FromSeconds(300)); // refresh every 5 minutes			
		}

		private void InitRabbitMQ()
		{
			var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
			var builder = new ConfigurationBuilder()
				  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
				  .AddJsonFile("appsettings.json", true)
				  .AddJsonFile($"appsettings.{environmentName}.json", true);
			configuration = builder.Build();
			var factory = new ConnectionFactory { HostName = configuration["AppSettings:RabbitmqHost"], Port = 5673, UserName = "root" , Password = "QPv8qLtiQ5" };

			// create connection  
			connection = factory.CreateConnection();
			// create channel  
			IModel channel = connection.CreateModel();
			//channel.ExchangeDeclare("los.exchange", ExchangeType.Direct);
			channel.QueueDeclare(queue: "los.init_loan_queue",
								 durable: true,
								 exclusive: false,
								 autoDelete: false,
								 arguments: null);
			//channel.QueueBind("los.init_loan_queue", "", "", null);
			channel.BasicQos(0, 0, false);
			connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
			
			var consumer = new EventingBasicConsumer(channel);
			consumer.Received += (ch, ea) =>
			{
				if (current_thread <= 20)
				{
					current_thread++;
					// received message  
					var content = System.Text.Encoding.UTF8.GetString(ea.Body.ToArray());

					Thread t = new Thread(() => HandleMessage(content));
					t.Start();
					// handle the received message  
					//HandleMessage(content);

					channel.BasicAck(ea.DeliveryTag, false);
					Thread.Sleep(50);
				}
				else
				{

				}	
			};
		
			consumer.Shutdown += OnConsumerShutdown;
			consumer.Registered += OnConsumerRegistered;
			consumer.Unregistered += OnConsumerUnregistered;
			consumer.ConsumerCancelled += OnConsumerConsumerCancelled;

			channel.BasicConsume(queue: "los.init_loan_queue",
								 autoAck: false,
								 consumer: consumer);			
		}

		private void HandleMessage(string content)
		{
			try
			{
				// we just print this message   
				Console.WriteLine($"init loan consumer received:  {content}");
				var idReq = JsonConvert.DeserializeObject<IdOnlyReq>(content);
				if (this.pipelines == null || idReq.forceRefresh)
				{
					getPipeline(null);
				}				
				processPipeline(idReq.id);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message + "\n" + ex.StackTrace);				
			}
		}

		//void processPipeline(LoanBrief loanBrief, List<PipelineDTO> pipelines)
		void processPipeline(int id)
		{
			var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
			optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
			DbContextOptions<LOSContext> options = optionsBuilder.Options;
			using (var db = new LOSContext(options))
			{
				try
				{
					LoanBrief loanBrief = db.LoanBrief.Find(id);
					DateTime beginTime = DateTime.Now;
					//unitOfWork.BeginTransaction();
					Console.WriteLine("Begin processing pipeline : " + loanBrief.LoanBriefId);
					// get all pipeline				
					if (pipelines != null && pipelines.Count > 0)
					{
						DatabaseHelper<LoanBrief> helper = new DatabaseHelper<LoanBrief>();
						// check loan brief current pipeline
						// reload lại entity						
						if (loanBrief.CurrentPipelineId == null)
						{
							var pipelineAssigned = false;
							foreach (var pipeline in pipelines)
							{
								var sections = pipeline.Sections;
								if (sections != null && sections.Count() > 0)
								{
									SectionDetailDTO lastAutomaticSuccess = null;
									int indexApproveSucess = -1;
									foreach (var section in sections)
									{
										// for each detail	
										int detailSucess = 0;
										int detailCount = section.Details != null ? section.Details.Count() : 0;
										if (detailCount == 0)
										{
											goto gotonextPipeline;
										}
										foreach (var detail in section.Details)
										{
											if (detail.Mode == "AUTOMATIC")
											{
												if (detail.Approves != null && detail.Approves.Count() > 0)
												{
													bool IsOneSuccess = false;
													int index = 0;
													foreach (var approve in detail.Approves)
													{
														IsOneSuccess = false;
														// kiểm tra đk detail
														int conditionSuccess = 0;
														int conditionCount = approve.Conditions != null ? approve.Conditions.Count() : 0;
														if (conditionCount <= 0)
														{
															// pipeline error												
															db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
															{
																PipelineState = (int)Const.ProcessType.UNEXPECTED_ERROR,
																AddedToQueue = false,
																InProcess = 0
															});
															Console.WriteLine("Pipeline Error:" + loanBrief.LoanBriefId);
															goto processed;

														}
														foreach (var condition in approve.Conditions)
														{
															if (helper.CheckFieldOperator(loanBrief, condition.Property.FieldName, condition.Operator, condition.Value, condition.Property.Type))
															{
																indexApproveSucess = index;
																Console.WriteLine("Condition successed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
																conditionSuccess++;
															}
															else
															{
																Console.WriteLine("Condition failed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
															}

														}
														// nếu điều kiện thành công hết thì chuyển đến aprrove section
														if (conditionSuccess == conditionCount)
														{
															// detail success
															detailSucess++;
															lastAutomaticSuccess = detail;
															IsOneSuccess = true;
															// set property and event
															// gán giá trị của section detail		
															if (detail.Actions != null && detail.Actions.Count() > 0)
															{
																foreach (var action in detail.Actions.Where(x => x.Type == 1))
																{
																	helper.UpdateDynamicField(loanBrief, action.Property.FieldName, action.Value, action.Property.Type);
																}
																// event 
																foreach (var action in detail.Actions.Where(x => x.Type == 10))
																{
																	if (action.Value != null && !"".Equals(action.Value))
																	{
																		if (action.Value.IndexOf(",") > 0)
																		{
																			string[] values = action.Value.Split(",");
																			foreach (var v in values)
																			{
																				// push event 
																				db.RequestEvent.Add(new RequestEvent()
																				{
																					CreatedAt = DateTime.Now,
																					EventId = Int32.Parse(v),
																					IsExcuted = 0,
																					LoanbriefId = loanBrief.LoanBriefId
																				});
																			}
																		}
																		else
																		{
																			// push event 
																			db.RequestEvent.Add(new RequestEvent()
																			{
																				CreatedAt = DateTime.Now,
																				EventId = Int32.Parse(action.Value),
																				IsExcuted = 0,
																				LoanbriefId = loanBrief.LoanBriefId
																			});
																		}
																	}
																}
															}
															break;
														}
														index++;
													}
													if (!IsOneSuccess)
													{
														goto gotonextPipeline;
													}
												}
												else
												{

												}
											}
											else
											{
												// kiêm tra xem có đúng section sau khi kiểm tra không
												if (lastAutomaticSuccess != null)
												{
													if (indexApproveSucess >= 0)
													{
														if (lastAutomaticSuccess.Approves.ToList()[indexApproveSucess].ApproveId == detail.SectionDetailId)
														{
															// chạy cho đến khi gặp section detail = MANUAL 
															// save log
															var currentHistory = new PipelineHistory();
															currentHistory.ActionState = null;
															currentHistory.BeginSectionDetailId = null;
															currentHistory.BeginSectionId = null;
															currentHistory.BeginStatus = loanBrief.Status;
															currentHistory.BeginTime = DateTimeOffset.Now;
															currentHistory.CreatedDate = DateTimeOffset.Now;
															currentHistory.Data = JsonConvert.SerializeObject(loanBrief);
															currentHistory.LoanBriefId = loanBrief.LoanBriefId;
															currentHistory.PipelineId = loanBrief.CurrentPipelineId;
															currentHistory.EndSectionDetailId = pipeline.PipelineId;
															currentHistory.EndSectionId = section.SectionId;
															currentHistory.EndPipelineState = null;
															// đã xác định được pipeline			
															pipelineAssigned = true;
															loanBrief.CurrentPipelineId = pipeline.PipelineId;
															loanBrief.CurrentSectionId = section.SectionId;
															loanBrief.CurrentSectionDetailId = detail.SectionDetailId;
															loanBrief.InProcess = 0;
															if (detail.Mode == "MANUAL")
																loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_MANUAL_PROCESSING;
															else
																loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_AUTOMATIC_PROCESSING;
															loanBrief.AddedToQueue = false;
															// gán giá trị của section detail		
															if (detail.Actions != null && detail.Actions.Count() > 0)
															{
																foreach (var action in detail.Actions.Where(x => x.Type == 1))
																{
																	helper.UpdateDynamicField(loanBrief, action.Property.FieldName, action.Value, action.Property.Type);
																}
																// event 
																foreach (var action in detail.Actions.Where(x => x.Type == 10))
																{
																	if (action.Value != null && !"".Equals(action.Value))
																	{
																		if (action.Value.IndexOf(",") > 0)
																		{
																			string[] values = action.Value.Split(",");
																			foreach (var v in values)
																			{
																				// push event 
																				db.RequestEvent.Add(new RequestEvent()
																				{
																					CreatedAt = DateTime.Now,
																					EventId = Int32.Parse(v),
																					IsExcuted = 0,
																					LoanbriefId = loanBrief.LoanBriefId
																				});
																			}
																		}
																		else
																		{
																			// push event 
																			db.RequestEvent.Add(new RequestEvent()
																			{
																				CreatedAt = DateTime.Now,
																				EventId = Int32.Parse(action.Value),
																				IsExcuted = 0,
																				LoanbriefId = loanBrief.LoanBriefId
																			});
																		}
																	}
																}
															}
															currentHistory.EndStatus = loanBrief.Status;
															currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
															InsertPipelineHistory(db, currentHistory);
															db.LoanBrief.Update(loanBrief);
															db.SaveChanges();
															Console.WriteLine("Finish processing pipeline : " + loanBrief.LoanBriefId + " - New section :" + loanBrief.CurrentSectionId + " New Detail: " + loanBrief.CurrentSectionDetailId);
															goto processed;
														}
														else
														{
															continue;
														}
													}
													else
													{

													}
												}
												else
												{
													// invalid
													db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
													{
														PipelineState = (int)Const.ProcessType.INVALID_APPROVE_STATE,
														AddedToQueue = false,
														InProcess = 0
													});
													Console.WriteLine("Pipeline Unknown:" + loanBrief.LoanBriefId);
													goto processed;
												}

											}
										}
									}
								}
								gotonextPipeline:
								{
									Console.WriteLine("Go to next pipeline, Current pipeline : " + pipeline.Name);
								}
							}
							// nếu chưa chọn được pipeline ?
							if (!pipelineAssigned)
							{
								db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
								{
									PipelineState = (int)Const.ProcessType.UNKNOWN_PIPELINE,
									AddedToQueue = false,
									InProcess = 0
								});
								Console.WriteLine("Pipeline Unknown:" + loanBrief.LoanBriefId);
								//unitOfWork.CommitTransaction();
							}
							processed:
							{
								Console.WriteLine("Pipeline Proccessed:" + loanBrief.LoanBriefId);
								//unitOfWork.CommitTransaction();
							}
						}
						else
						{

						}
					}
					else
					{
						// update state						
						db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
						{
							PipelineState = (int)Const.ProcessType.WAITING_FOR_PIPELINE,
							AddedToQueue = false,
							InProcess = 0
						});						
					}				
				}
				catch (Exception ex)
				{
					Console.WriteLine("Pipeline Exception:" + ex.Message);
					Console.WriteLine("Pipeline Exception:" + ex.StackTrace);					
				}
			}
			current_thread--;
			// RAA
		}

		void InsertPipelineHistory(LOSContext db, PipelineHistory history)
		{
			db.PipelineHistory.Add(history);
			db.SaveChanges();
		}

		private void getPipeline(object state)
		{			
			try
			{
				var pipelines = unitOfWork.PipelineRepository.Query(x => x.Status != 99 && x.Status != 0, x => x.OrderBy(x1 => x1.Priority), false)
					.Select(x => new PipelineDTO()
					{
						CreatedAt = x.CreatedAt,
						CreatorId = x.CreatorId,
						ModifiedAt = x.ModifiedAt,
						Name = x.Name,
						PipelineId = x.PipelineId,
						Priority = x.Priority,
						Status = x.Status,
						Sections = x.Section.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Order).Select(x1 => new SectionDTO()
						{
							Name = x1.Name,
							Order = x1.Order,
							SectionGroupId = x1.SectionGroupId,
							SectionId = x1.SectionId,
							IsShow = x1.IsShow,
							PipelineId = x1.PipelineId,
							Details = x1.SectionDetail.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Step).Select(x2 => new SectionDetailDTO()
							{
								Mode = x2.Mode,
								Name = x2.Name,
								SectionDetailId = x2.SectionDetailId,
								SectionId = x2.SectionId,
								Step = x2.Step,
								IsShow = x2.IsShow,
								Approves = x2.SectionApproveSectionDetail.Select(x3 => new SectionApproveDTO()
								{
									Name = x3.Name,
									Approve = x3.Approve != null ? new SectionDetailDTO()
									{
										Name = x3.Approve.Name,
										SectionDetailId = x3.Approve.SectionDetailId,
										SectionId = x3.Approve.SectionId,
										Step = x3.Approve.Step
									} : null,
									SectionApproveId = x3.SectionApproveId,
									SectionDetailId = x3.SectionDetailId,
									ApproveId = x3.ApproveId,
									Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
									{
										Operator = x4.Operator,
										PropertyId = x4.PropertyId,
										SectionConditionId = x4.SectionConditionId,
										SectionApproveId = x4.SectionApproveId,
										Value = x4.Value,
										Property = x4.Property != null ? new PropertyDTO()
										{
											Object = x4.Property.Object,
											FieldName = x4.Property.FieldName,
											Priority = x4.Property.Priority,
											Regex = x4.Property.Regex,
											Type = x4.Property.Type,
											PropertyId = x4.Property.PropertyId,
											Name = x4.Property.Name,
											LinkObject = x4.Property.LinkObject
										} : null
									})
								}),
								DisapproveId = x2.DisapproveId,
								Disapprove = x2.Disapprove != null ? new SectionDetailDTO()
								{
									Name = x2.Disapprove.Name,
									SectionDetailId = x2.Disapprove.SectionDetailId,
									SectionId = x2.Disapprove.SectionId,
									Step = x2.Disapprove.Step
								} : null,
								ReturnId = x2.ReturnId,
								Return = x2.Return != null ? new SectionDetailDTO()
								{
									Name = x2.Return.Name,
									SectionDetailId = x2.Return.SectionDetailId,
									SectionId = x2.Return.SectionId,
									Step = x2.Return.Step
								} : null,
								Actions = x2.SectionAction.Select(x3 => new SectionActionDTO()
								{
									PropertyId = x3.PropertyId,
									SectionActionId = x3.SectionActionId,
									SectionDetailId = x3.SectionDetailId,
									Value = x3.Value,
									Type = x3.Type,
									Property = x3.Property != null ? new PropertyDTO()
									{
										Object = x3.Property.Object,
										FieldName = x3.Property.FieldName,
										Priority = x3.Property.Priority,
										Regex = x3.Property.Regex,
										Type = x3.Property.Type,
										PropertyId = x3.Property.PropertyId,
										Name = x3.Property.Name,
										LinkObject = x3.Property.LinkObject
									} : null
								})
							})
						})
					}).ToList();
				this.pipelines = pipelines;
				Console.WriteLine("Config reloaded");
			}
			catch (Exception ex)
			{

			}
		}

		private void OnConsumerConsumerCancelled(object sender, ConsumerEventArgs e) {
			Console.WriteLine($"init loan consumer cancelled");
		}
		private void OnConsumerUnregistered(object sender, ConsumerEventArgs e) {
			Console.WriteLine($"init loan consumer unregistered");
		}
		private void OnConsumerRegistered(object sender, ConsumerEventArgs e) {
			Console.WriteLine($"init loan consumer registered");
		}
		private void OnConsumerShutdown(object sender, ShutdownEventArgs e) {
			Console.WriteLine($"init loan consumer shut down");
		}
		private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e) {
			Console.WriteLine($"init loan connection shut down");
		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			//Console.WriteLine($"init loan service started");
			InitRabbitMQ();
			return Task.CompletedTask;			
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			Console.WriteLine($"init loan service stopped");
			connection.Close();	
			_timer?.Change(Timeout.Infinite, 0);
			return Task.CompletedTask;
		}

		public void Dispose()
		{
			_timer?.Dispose();
			connection.Dispose();
		}
	}
}
