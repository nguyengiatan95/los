﻿using LOS.Common.Helpers;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers;
using LOS.DAL.UnitOfWork;
using LOS.PipelineProccesor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace LOS.PipelineProccesor.Comsumers
{
    public class LoanConsumer : IHostedService, IDisposable
    {
        private static IConnection connection;
        private IUnitOfWork unitOfWork;
        private List<PipelineDTO> pipelines;
        private Timer _timer;
        IConfiguration configuration;
        private int current_thread = 0;

        public LoanConsumer(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _timer = new Timer(getPipeline, null, TimeSpan.FromSeconds(10),
                TimeSpan.FromSeconds(300)); // refresh every 5 minutes			
        }

        public void InitRabbitMQ()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
                  .AddJsonFile("appsettings.json", true)
                  .AddJsonFile($"appsettings.{environmentName}.json", true);
            configuration = builder.Build();
            var factory = new ConnectionFactory { HostName = configuration["AppSettings:RabbitmqHost"], Port = 5673, UserName = "root", Password = "QPv8qLtiQ5" };

            // create connection  
            connection = factory.CreateConnection();
            // create channel  
            IModel channel = connection.CreateModel();
            //channel.ExchangeDeclare("los.exchange", ExchangeType.Direct);
            channel.QueueDeclare(queue: "los.normal_loan_queue",
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
            channel.BasicQos(0, 0, false);
            connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (ch, ea) =>
            {
                if (current_thread <= 20)
                {
                    current_thread++;
                    // received message  
                    var content = System.Text.Encoding.UTF8.GetString(ea.Body.ToArray());

                    // handle the received message  
                    Thread t = new Thread(() => HandleMessage(content));
                    t.Start();
                    //HandleMessage(content);				

                    channel.BasicAck(ea.DeliveryTag, false);
                    Thread.Sleep(50);
                }
                else
				{

				}                    
            };

            consumer.Shutdown += OnConsumerShutdown;
            consumer.Registered += OnConsumerRegistered;
            consumer.Unregistered += OnConsumerUnregistered;
            consumer.ConsumerCancelled += OnConsumerConsumerCancelled;

            channel.BasicConsume(queue: "los.normal_loan_queue",
                                 autoAck: false,
                                 consumer: consumer);
        }

        private void HandleMessage(string content)
        {
            try
            {
                // update code moi
                Console.WriteLine($"loan consumer received:  {content}");
                var idReq = JsonConvert.DeserializeObject<IdOnlyReq>(content);
                if (this.pipelines == null || idReq.forceRefresh)
                {
                    getPipeline(null);
                }
                //LoanBrief brief = unitOfWork.LoanBriefRepository.GetById(idReq.id);
                // reload brief
                //unitOfWork.LoanBriefRepository.Reload(brief);
                //processPipeline(brief, this.pipelines);
                processPipeline(idReq.id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
            }
        }

        void processPipeline(int id)
        {
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            using (var db = new LOSContext(options))
            {
                try
                {
                    LoanBrief loanBrief = db.LoanBrief.Find(id);
                    decimal? totalLoanAmount = 0l;
                    //Nếu là đơn oto => gom nhóm tổng số tiền cần vay 
                    if (loanBrief.ProductId == (int)Common.Extensions.EnumProductCredit.OtoCreditType_CC)
                    {
                        //lấy tổng số tiền KH đăng ký vay theo gói oto + các đơn đang xư ly
                        //loại trừ các đơn đóng HĐ hoặc đơn bị hủy
                        totalLoanAmount = db.LoanBrief.Where(x => x.CustomerId == loanBrief.CustomerId
                           && x.ProductId == (int)Common.Extensions.EnumProductCredit.OtoCreditType_CC
                           && x.Status != (int)Common.Extensions.EnumLoanStatus.FINISH
                           && x.Status != (int)Common.Extensions.EnumLoanStatus.CANCELED
                           ).Sum(x => x.LoanAmount);
                        if (totalLoanAmount > 0)
                            loanBrief.LoanAmount = totalLoanAmount;

                        //loanBrief.PipelineState = (int)Const.ProcessType.MANUAL_PROCCESSED;
                        //loanBrief.ActionState = 1;


                    }
                    //unitOfWork.BeginTransaction();
                    Console.WriteLine("Begin processing pipeline : " + loanBrief.LoanBriefId);
                    // get current pipeline
                    var lastHistory = db.PipelineHistory.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                    if (lastHistory != null)
                    {
                        lastHistory.EndTime = DateTimeOffset.Now;
                    }
                    DateTime beginTime = DateTime.Now;
                    var currentHistory = new PipelineHistory();
                    currentHistory.ActionState = loanBrief.ActionState;
                    currentHistory.BeginSectionDetailId = loanBrief.CurrentSectionDetailId;
                    currentHistory.BeginSectionId = loanBrief.CurrentSectionId;
                    currentHistory.BeginStatus = loanBrief.Status.Value;
                    currentHistory.BeginTime = DateTimeOffset.Now;
                    currentHistory.CreatedDate = DateTimeOffset.Now;
                    currentHistory.Data = JsonConvert.SerializeObject(loanBrief);
                    currentHistory.LoanBriefId = loanBrief.LoanBriefId;
                    currentHistory.PipelineId = loanBrief.CurrentPipelineId;
                    currentHistory.LenderName = loanBrief.LenderName;
                    DatabaseHelper<LoanBrief> helper = new DatabaseHelper<LoanBrief>();
                    if (loanBrief.PipelineState == (int)Const.ProcessType.MANUAL_PROCCESSED)
                    {
                        // update state
                        db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                        {
                            PipelineState = (int)Const.ProcessType.IN_PROCESS_FOR_SECTION
                        });
                        db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                        db.SaveChanges();
                        if (totalLoanAmount > 0)
                            loanBrief.LoanAmount = totalLoanAmount;
                        // process
                        // get current pipeline
                        var currentPipeline = pipelines.Where(x => x.PipelineId == loanBrief.CurrentPipelineId).FirstOrDefault();
                        if (currentPipeline != null)
                        {
                            var currentSection = currentPipeline.Sections.Where(x => x.SectionId == loanBrief.CurrentSectionId).FirstOrDefault();
                            if (currentSection != null)
                            {
                                var currentSectionDetail = currentSection.Details.Where(x => x.SectionDetailId == loanBrief.CurrentSectionDetailId).FirstOrDefault();
                                if (currentSectionDetail != null)
                                {
                                    // foreach action state >> 
                                    if (loanBrief.ActionState == 1) // Approve
                                    {
                                        // check approves condition
                                        if (currentSectionDetail.Approves != null && currentSectionDetail.Approves.Count() > 0)
                                        {
                                            int approvesSuccess = 0;
                                            int approvesSuccessCount = currentSectionDetail.Approves.Count();
                                            foreach (var approve in currentSectionDetail.Approves)
                                            {
                                                // kiểm tra điều kiện
                                                int conditionSuccess = 0;
                                                int conditionCount = approve.Conditions != null ? approve.Conditions.Count() : 0;
                                                if (conditionCount > 0)
                                                {
                                                    foreach (var condition in approve.Conditions)
                                                    {
                                                        if (helper.CheckFieldOperator(loanBrief, condition.Property.FieldName, condition.Operator, condition.Value, condition.Property.Type))
                                                        {
                                                            conditionSuccess++;
                                                            Console.WriteLine("Condition successed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("Condition failed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                        }
                                                    }
                                                    if (conditionCount == conditionSuccess)
                                                    {
                                                        approvesSuccess++;
                                                        // detail thành công
                                                        // chuyển đến detail tiếp theo
                                                        Console.WriteLine("Update Current State Approve :" + loanBrief.LoanBriefId + ":" + approve.Approve.SectionId + ":" + approve.ApproveId);
                                                        loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                                        loanBrief.CurrentSectionId = approve.Approve.SectionId;
                                                        loanBrief.CurrentSectionDetailId = approve.ApproveId;
                                                        db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                                        {
                                                            PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                                            CurrentSectionId = approve.Approve.SectionId,
                                                            CurrentSectionDetailId = approve.ApproveId
                                                        });
                                                        db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                                        db.SaveChanges();
                                                        if (totalLoanAmount > 0)
                                                            loanBrief.LoanAmount = totalLoanAmount;
                                                        var data = processPipelineSection(db, loanBrief, pipelines);
                                                        // pipeline processed, save history			
                                                        currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                                        currentHistory.EndSectionId = data.CurrentSectionId;
                                                        currentHistory.EndStatus = data.Status.Value;
                                                        currentHistory.EndPipelineState = data.PipelineState.Value;
                                                        currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                                        InsertPipelineHistory(db, currentHistory);
                                                        if (lastHistory != null)
                                                            UpdatePipelineHistory(db, lastHistory);
                                                        goto pipline_processed;
                                                    }
                                                }
                                                else
                                                {
                                                    // không cần kiểm tra điều kiện gì > Thực hiện theo ActionState											
                                                    approvesSuccess++;
                                                    Console.WriteLine("Update Current State Approve :" + loanBrief.LoanBriefId + ":" + approve.Approve.SectionId + ":" + approve.ApproveId);
                                                    loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                                    loanBrief.CurrentSectionId = approve.Approve.SectionId;
                                                    loanBrief.CurrentSectionDetailId = approve.ApproveId;
                                                    db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                                    {
                                                        PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                                        CurrentSectionId = approve.Approve.SectionId,
                                                        CurrentSectionDetailId = approve.ApproveId
                                                    });
                                                    db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                                    db.SaveChanges();
                                                    if (totalLoanAmount > 0)
                                                        loanBrief.LoanAmount = totalLoanAmount;
                                                    var data = processPipelineSection(db, loanBrief, pipelines);
                                                    // pipeline processed, save history			
                                                    currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                                    currentHistory.EndSectionId = data.CurrentSectionId;
                                                    currentHistory.EndStatus = data.Status.Value;
                                                    currentHistory.EndPipelineState = data.PipelineState.Value;
                                                    currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                                    InsertPipelineHistory(db, currentHistory);
                                                    if (lastHistory != null)
                                                        UpdatePipelineHistory(db, lastHistory);
                                                    goto pipline_processed;
                                                }
                                            }
                                            if (approvesSuccess == 0)
                                            {
                                                //get current section											
                                                // không có đk nào thành công > chuyển đến disapprove
                                                Console.WriteLine("Update Current State Disapprove :" + loanBrief.LoanBriefId + ":" + currentSectionDetail.Disapprove.SectionId + ":" + currentSectionDetail.DisapproveId);
                                                loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                                loanBrief.CurrentSectionId = currentSectionDetail.Disapprove.SectionId;
                                                loanBrief.CurrentSectionDetailId = currentSectionDetail.DisapproveId;
                                                db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                                {
                                                    PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                                    CurrentSectionDetailId = currentSectionDetail.DisapproveId,
                                                    CurrentSectionId = currentSectionDetail.Disapprove.SectionId
                                                });
                                                db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                                db.SaveChanges();
                                                if (totalLoanAmount > 0)
                                                    loanBrief.LoanAmount = totalLoanAmount;
                                                var data = processPipelineSection(db, loanBrief, pipelines);
                                                // pipeline processed, save history			
                                                currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                                currentHistory.EndSectionId = data.CurrentSectionId;
                                                currentHistory.EndStatus = data.Status.Value;
                                                currentHistory.EndPipelineState = data.PipelineState.Value;
                                                currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                                InsertPipelineHistory(db, currentHistory);
                                                if (lastHistory != null)
                                                    UpdatePipelineHistory(db, lastHistory);
                                                goto pipline_processed;
                                            }
                                        }
                                    }
                                    else if (loanBrief.ActionState == 2) //  Trả lại
                                    {
                                        if (currentSectionDetail.ReturnId > 0)
                                        {
                                            Console.WriteLine("Update Current State Return :" + loanBrief.LoanBriefId + ":" + currentSectionDetail.Return.SectionId + ":" + currentSectionDetail.ReturnId);
                                            loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                            loanBrief.CurrentSectionId = currentSectionDetail.Return.SectionId;
                                            loanBrief.CurrentSectionDetailId = currentSectionDetail.ReturnId;
                                            db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                            {
                                                PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                                CurrentSectionDetailId = currentSectionDetail.ReturnId,
                                                CurrentSectionId = currentSectionDetail.Return.SectionId
                                            });
                                            db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                            db.SaveChanges();
                                            if (totalLoanAmount > 0)
                                                loanBrief.LoanAmount = totalLoanAmount;
                                            var data = processPipelineSection(db, loanBrief, pipelines);
                                            // pipeline processed, save history			
                                            currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                            currentHistory.EndSectionId = data.CurrentSectionId;
                                            currentHistory.EndStatus = data.Status.Value;
                                            currentHistory.EndPipelineState = data.PipelineState.Value;
                                            currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                            InsertPipelineHistory(db, currentHistory);
                                            if (lastHistory != null)
                                                UpdatePipelineHistory(db, lastHistory);
                                            goto pipline_processed;
                                        }
                                        else
                                        {
                                            goto unknown_pipeline;
                                        }
                                    }
                                    else if (loanBrief.ActionState == 3) // Hủy
                                    {
                                        // check approves condition
                                        if (currentSectionDetail.Disapproves != null && currentSectionDetail.Disapproves.Count() > 0)
                                        {
                                            int disapprovesSuccess = 0;
                                            int disapprovesSuccessCount = currentSectionDetail.Disapproves.Count();
                                            foreach (var disapprove in currentSectionDetail.Disapproves)
                                            {
                                                // kiểm tra điều kiện
                                                int conditionSuccess = 0;
                                                int conditionCount = disapprove.Conditions != null ? disapprove.Conditions.Count() : 0;
                                                if (conditionCount > 0)
                                                {
                                                    foreach (var condition in disapprove.Conditions)
                                                    {
                                                        if (helper.CheckFieldOperator(loanBrief, condition.Property.FieldName, condition.Operator, condition.Value, condition.Property.Type))
                                                        {
                                                            conditionSuccess++;
                                                            Console.WriteLine("Disapprove Condition successed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("Disapprove Condition failed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                        }
                                                    }
                                                    if (conditionCount == conditionSuccess)
                                                    {
                                                        disapprovesSuccess++;
                                                        // detail thành công
                                                        // chuyển đến detail tiếp theo
                                                        Console.WriteLine("Update Current State Approve :" + loanBrief.LoanBriefId + ":" + disapprove.Approve.SectionId + ":" + disapprove.ApproveId);
                                                        loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                                        loanBrief.CurrentSectionId = disapprove.Approve.SectionId;
                                                        loanBrief.CurrentSectionDetailId = disapprove.ApproveId;
                                                        db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                                        {
                                                            PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                                            CurrentSectionDetailId = disapprove.ApproveId,
                                                            CurrentSectionId = disapprove.Approve.SectionId
                                                        });
                                                        db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                                        db.SaveChanges();
                                                        if (totalLoanAmount > 0)
                                                            loanBrief.LoanAmount = totalLoanAmount;
                                                        var data = processPipelineSection(db, loanBrief, pipelines);
                                                        // pipeline processed, save history			
                                                        currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                                        currentHistory.EndSectionId = data.CurrentSectionId;
                                                        currentHistory.EndStatus = data.Status.Value;
                                                        currentHistory.EndPipelineState = data.PipelineState.Value;
                                                        currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                                        InsertPipelineHistory(db, currentHistory);
                                                        if (lastHistory != null)
                                                            UpdatePipelineHistory(db, lastHistory);
                                                        foreach (var action in currentSectionDetail.Actions.Where(x => x.Type == 11))
                                                        {
                                                            if (action.Value != null && !"".Equals(action.Value))
                                                            {
                                                                if (action.Value.IndexOf(",") > 0)
                                                                {
                                                                    string[] values = action.Value.Split(",");
                                                                    foreach (var v in values)
                                                                    {
                                                                        // push event 
                                                                        db.RequestEvent.Add(new RequestEvent()
                                                                        {
                                                                            CreatedAt = DateTime.Now,
                                                                            EventId = Int32.Parse(v),
                                                                            IsExcuted = 0,
                                                                            LoanbriefId = loanBrief.LoanBriefId
                                                                        });
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    // push event 
                                                                    db.RequestEvent.Add(new RequestEvent()
                                                                    {
                                                                        CreatedAt = DateTime.Now,
                                                                        EventId = Int32.Parse(action.Value),
                                                                        IsExcuted = 0,
                                                                        LoanbriefId = loanBrief.LoanBriefId
                                                                    });
                                                                }
                                                            }
                                                        }
                                                        goto pipline_processed;
                                                    }
                                                }
                                                else
                                                {
                                                    // không cần kiểm tra điều kiện gì > Thực hiện theo ActionState											
                                                    disapprovesSuccess++;
                                                    Console.WriteLine("Update Current State Disapprove :" + loanBrief.LoanBriefId + ":" + disapprove.Approve.SectionId + ":" + disapprove.ApproveId);
                                                    loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                                    loanBrief.CurrentSectionId = disapprove.Approve.SectionId;
                                                    loanBrief.CurrentSectionDetailId = disapprove.ApproveId;
                                                    loanBrief.CurrentSectionDetailId = disapprove.ApproveId;
                                                    db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                                    {
                                                        PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                                        CurrentSectionDetailId = disapprove.ApproveId,
                                                        CurrentSectionId = disapprove.Approve.SectionId
                                                    });
                                                    db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                                    db.SaveChanges();
                                                    if (totalLoanAmount > 0)
                                                        loanBrief.LoanAmount = totalLoanAmount;
                                                    var data = processPipelineSection(db, loanBrief, pipelines);
                                                    // pipeline processed, save history			
                                                    currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                                    currentHistory.EndSectionId = data.CurrentSectionId;
                                                    currentHistory.EndStatus = data.Status.Value;
                                                    currentHistory.EndPipelineState = data.PipelineState.Value;
                                                    currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                                    InsertPipelineHistory(db, currentHistory);
                                                    if (lastHistory != null)
                                                        UpdatePipelineHistory(db, lastHistory);
                                                    foreach (var action in currentSectionDetail.Actions.Where(x => x.Type == 11))
                                                    {
                                                        if (action.Value != null && !"".Equals(action.Value))
                                                        {
                                                            if (action.Value.IndexOf(",") > 0)
                                                            {
                                                                string[] values = action.Value.Split(",");
                                                                foreach (var v in values)
                                                                {
                                                                    // push event 
                                                                    db.RequestEvent.Add(new RequestEvent()
                                                                    {
                                                                        CreatedAt = DateTime.Now,
                                                                        EventId = Int32.Parse(v),
                                                                        IsExcuted = 0,
                                                                        LoanbriefId = loanBrief.LoanBriefId
                                                                    });
                                                                }
                                                            }
                                                            else
                                                            {
                                                                // push event 
                                                                db.RequestEvent.Add(new RequestEvent()
                                                                {
                                                                    CreatedAt = DateTime.Now,
                                                                    EventId = Int32.Parse(action.Value),
                                                                    IsExcuted = 0,
                                                                    LoanbriefId = loanBrief.LoanBriefId
                                                                });
                                                            }
                                                        }
                                                    }
                                                    goto pipline_processed;
                                                }
                                            }
                                            if (disapprovesSuccess == 0)
                                            {
                                                // không có đk nào thành công > chuyển đến disapprove
                                                goto unknown_pipeline;
                                            }
                                        }
                                        else
										{
                                            goto unknown_pipeline;
										}                                            
                                    }
                                    else
                                    {
                                        // invalid state
                                        db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                        {
                                            PipelineState = (int)Const.ProcessType.INVALID_ACTION_STATE,
                                            InProcess = 0,
                                            AddedToQueue = false
                                        });
                                        db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                        db.SaveChanges();
                                        goto pipline_processed;
                                    }
                                }
                                else
                                    goto unknown_pipeline;
                            }
                            else
                                goto unknown_pipeline;
                        }
                        else
                        {
                            goto unknown_pipeline;
                        }
                    }
                    else if (loanBrief.PipelineState == (int)Const.ProcessType.AUTOMATIC_PROCESSED)
                    {
                        // update state
                        db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                        {
                            PipelineState = (int)Const.ProcessType.IN_PROCESS_FOR_SECTION,
                            AddedToQueue = false
                        });
                        db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                        db.SaveChanges();
                        if (totalLoanAmount > 0)
                            loanBrief.LoanAmount = totalLoanAmount;
                        // process
                        // get current pipeline
                        var currentPipeline = pipelines.Where(x => x.PipelineId == loanBrief.CurrentPipelineId).FirstOrDefault();
                        if (currentPipeline != null)
                        {
                            var currentSection = currentPipeline.Sections.Where(x => x.SectionId == loanBrief.CurrentSectionId).FirstOrDefault();
                            if (currentSection != null)
                            {
                                var currentSectionDetail = currentSection.Details.Where(x => x.SectionDetailId == loanBrief.CurrentSectionDetailId).FirstOrDefault();
                                if (currentSectionDetail != null)
                                {
                                    // foreach action state >> 
                                    if (loanBrief.ActionState == 1) // Approve
                                    {
                                        // check approves condition
                                        if (currentSectionDetail.Approves != null && currentSectionDetail.Approves.Count() > 0)
                                        {
                                            int approvesSuccess = 0;
                                            int approvesSuccessCount = currentSectionDetail.Approves.Count();
                                            foreach (var approve in currentSectionDetail.Approves)
                                            {
                                                // kiểm tra điều kiện
                                                int conditionSuccess = 0;
                                                int conditionCount = approve.Conditions != null ? approve.Conditions.Count() : 0;
                                                if (conditionCount > 0)
                                                {
                                                    foreach (var condition in approve.Conditions)
                                                    {
                                                        if (helper.CheckFieldOperator(loanBrief, condition.Property.FieldName, condition.Operator, condition.Value, condition.Property.Type))
                                                        {
                                                            conditionSuccess++;
                                                            Console.WriteLine("Condition successed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("Condition failed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                        }
                                                    }
                                                    if (conditionCount == conditionSuccess)
                                                    {
                                                        approvesSuccess++;
                                                        // detail thành công
                                                        // chuyển đến detail tiếp theo
                                                        Console.WriteLine("Update Current State Approve :" + loanBrief.LoanBriefId + ":" + approve.Approve.SectionId + ":" + approve.ApproveId);
                                                        loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                                        loanBrief.CurrentSectionId = approve.Approve.SectionId;
                                                        loanBrief.CurrentSectionDetailId = approve.ApproveId;
                                                        db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                                        {
                                                            PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                                            CurrentSectionDetailId = approve.ApproveId,
                                                            CurrentSectionId = approve.Approve.SectionId
                                                        });
                                                        db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                                        db.SaveChanges();
                                                        if (totalLoanAmount > 0)
                                                            loanBrief.LoanAmount = totalLoanAmount;
                                                        var data = processPipelineSection(db, loanBrief, pipelines);
                                                        // pipeline processed, save history			
                                                        currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                                        currentHistory.EndSectionId = data.CurrentSectionId;
                                                        currentHistory.EndStatus = data.Status.Value;
                                                        currentHistory.EndPipelineState = data.PipelineState.Value;
                                                        currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                                        InsertPipelineHistory(db, currentHistory);
                                                        if (lastHistory != null)
                                                            UpdatePipelineHistory(db, lastHistory);
                                                        goto pipline_processed;
                                                    }
                                                }
                                                else
                                                {
                                                    // không cần kiểm tra điều kiện gì > Thực hiện theo ActionState											
                                                    approvesSuccess++;
                                                    Console.WriteLine("Update Current State Approve :" + loanBrief.LoanBriefId + ":" + approve.Approve.SectionId + ":" + approve.ApproveId);
                                                    loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                                    loanBrief.CurrentSectionId = approve.Approve.SectionId;
                                                    loanBrief.CurrentSectionDetailId = approve.ApproveId;
                                                    loanBrief.CurrentSectionDetailId = approve.ApproveId;
                                                    db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                                    {
                                                        PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                                        CurrentSectionDetailId = approve.ApproveId,
                                                        CurrentSectionId = approve.Approve.SectionId
                                                    });
                                                    db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                                    db.SaveChanges();
                                                    if (totalLoanAmount > 0)
                                                        loanBrief.LoanAmount = totalLoanAmount;
                                                    var data = processPipelineSection(db, loanBrief, pipelines);
                                                    // pipeline processed, save history			
                                                    currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                                    currentHistory.EndSectionId = data.CurrentSectionId;
                                                    currentHistory.EndStatus = data.Status.Value;
                                                    currentHistory.EndPipelineState = data.PipelineState.Value;
                                                    currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                                    InsertPipelineHistory(db, currentHistory);
                                                    if (lastHistory != null)
                                                        UpdatePipelineHistory(db, lastHistory);
                                                    goto pipline_processed;
                                                }
                                            }
                                            if (approvesSuccess == 0)
                                            {
                                                // không có đk nào thành công > chuyển đến disapprove
                                                Console.WriteLine("Update Current State Disapprove :" + loanBrief.LoanBriefId + ":" + currentSectionDetail.Disapprove.SectionId + ":" + currentSectionDetail.DisapproveId);
                                                loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                                loanBrief.CurrentSectionId = currentSectionDetail.Disapprove.SectionId;
                                                loanBrief.CurrentSectionDetailId = currentSectionDetail.DisapproveId;
                                                db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                                {
                                                    PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                                    CurrentSectionDetailId = currentSectionDetail.DisapproveId,
                                                    CurrentSectionId = currentSectionDetail.Disapprove.SectionId
                                                });
                                                db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                                db.SaveChanges();
                                                if (totalLoanAmount > 0)
                                                    loanBrief.LoanAmount = totalLoanAmount;
                                                var data = processPipelineSection(db, loanBrief, pipelines);
                                                // pipeline processed, save history			
                                                currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                                currentHistory.EndSectionId = data.CurrentSectionId;
                                                currentHistory.EndStatus = data.Status.Value;
                                                currentHistory.EndPipelineState = data.PipelineState.Value;
                                                currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                                InsertPipelineHistory(db, currentHistory);
                                                if (lastHistory != null)
                                                    UpdatePipelineHistory(db, lastHistory);
                                                goto pipline_processed;
                                            }
                                        }
                                    }
                                    else if (loanBrief.ActionState == 2) //  Trả lại
                                    {
                                        if (currentSectionDetail.ReturnId > 0)
                                        {
                                            Console.WriteLine("Update Current State Return :" + loanBrief.LoanBriefId + ":" + currentSectionDetail.Return.SectionId + ":" + currentSectionDetail.ReturnId);
                                            // xu ly event
                                            foreach (var action in currentSectionDetail.Actions.Where(x => x.Type == 12))
                                            {
                                                if (action.Value != null && !"".Equals(action.Value))
                                                {
                                                    if (action.Value.IndexOf(",") > 0)
                                                    {
                                                        string[] values = action.Value.Split(",");
                                                        foreach (var v in values)
                                                        {
                                                            // push event 
                                                            db.RequestEvent.Add(new RequestEvent()
                                                            {
                                                                CreatedAt = DateTime.Now,
                                                                EventId = Int32.Parse(v),
                                                                IsExcuted = 0,
                                                                LoanbriefId = loanBrief.LoanBriefId
                                                            });
                                                        }
                                                    }
                                                    else
                                                    {
                                                        // push event 
                                                        db.RequestEvent.Add(new RequestEvent()
                                                        {
                                                            CreatedAt = DateTime.Now,
                                                            EventId = Int32.Parse(action.Value),
                                                            IsExcuted = 0,
                                                            LoanbriefId = loanBrief.LoanBriefId
                                                        });
                                                    }
                                                }
                                            }
                                            // end xu ly
                                            loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                            loanBrief.CurrentSectionId = currentSectionDetail.Return.SectionId;
                                            loanBrief.CurrentSectionDetailId = currentSectionDetail.ReturnId;
                                            db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                            {
                                                PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                                CurrentSectionDetailId = currentSectionDetail.ReturnId,
                                                CurrentSectionId = currentSectionDetail.Return.SectionId
                                            });
                                            db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                            db.SaveChanges();
                                            if (totalLoanAmount > 0)
                                                loanBrief.LoanAmount = totalLoanAmount;
                                            var data = processPipelineSection(db, loanBrief, pipelines);
                                            // pipeline processed, save history			
                                            currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                            currentHistory.EndSectionId = data.CurrentSectionId;
                                            currentHistory.EndStatus = data.Status.Value;
                                            currentHistory.EndPipelineState = data.PipelineState.Value;
                                            currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                            InsertPipelineHistory(db, currentHistory);
                                            if (lastHistory != null)
                                                UpdatePipelineHistory(db, lastHistory);
                                            goto pipline_processed;
                                        }
                                        else
                                        {
                                            goto unknown_pipeline;
                                        }
                                    }
                                    else if (loanBrief.ActionState == 3) // Hủy
                                    {
                                        // check approves condition
                                        if (currentSectionDetail.Disapproves != null && currentSectionDetail.Disapproves.Count() > 0)
                                        {
                                            int disapprovesSuccess = 0;
                                            int disapprovesSuccessCount = currentSectionDetail.Disapproves.Count();
                                            foreach (var disapprove in currentSectionDetail.Disapproves)
                                            {
                                                // kiểm tra điều kiện
                                                int conditionSuccess = 0;
                                                int conditionCount = disapprove.Conditions != null ? disapprove.Conditions.Count() : 0;
                                                if (conditionCount > 0)
                                                {
                                                    foreach (var condition in disapprove.Conditions)
                                                    {
                                                        if (helper.CheckFieldOperator(loanBrief, condition.Property.FieldName, condition.Operator, condition.Value, condition.Property.Type))
                                                        {
                                                            conditionSuccess++;
                                                            Console.WriteLine("Disapprove Condition successed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("Disapprove Condition failed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                        }
                                                    }
                                                    if (conditionCount == conditionSuccess)
                                                    {
                                                        disapprovesSuccess++;
                                                        // detail thành công
                                                        // chuyển đến detail tiếp theo
                                                        Console.WriteLine("Update Current State Approve :" + loanBrief.LoanBriefId + ":" + disapprove.Approve.SectionId + ":" + disapprove.ApproveId);
                                                        loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                                        loanBrief.CurrentSectionId = disapprove.Approve.SectionId;
                                                        loanBrief.CurrentSectionDetailId = disapprove.ApproveId;
                                                        db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                                        {
                                                            PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                                            CurrentSectionDetailId = disapprove.ApproveId,
                                                            CurrentSectionId = disapprove.Approve.SectionId
                                                        });
                                                        db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                                        db.SaveChanges();
                                                        if (totalLoanAmount > 0)
                                                            loanBrief.LoanAmount = totalLoanAmount;
                                                        var data = processPipelineSection(db, loanBrief, pipelines);
                                                        // pipeline processed, save history			
                                                        currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                                        currentHistory.EndSectionId = data.CurrentSectionId;
                                                        currentHistory.EndStatus = data.Status.Value;
                                                        currentHistory.EndPipelineState = data.PipelineState.Value;
                                                        currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                                        InsertPipelineHistory(db, currentHistory);
                                                        if (lastHistory != null)
                                                            UpdatePipelineHistory(db, lastHistory);
														foreach (var action in currentSectionDetail.Actions.Where(x => x.Type == 11))
														{
															if (action.Value != null && !"".Equals(action.Value))
															{
																if (action.Value.IndexOf(",") > 0)
																{
																	string[] values = action.Value.Split(",");
																	foreach (var v in values)
																	{
																		// push event 
																		db.RequestEvent.Add(new RequestEvent()
																		{
																			CreatedAt = DateTime.Now,
																			EventId = Int32.Parse(v),
																			IsExcuted = 0,
																			LoanbriefId = loanBrief.LoanBriefId
																		});
																	}
																}
																else
																{
																	// push event 
																	db.RequestEvent.Add(new RequestEvent()
																	{
																		CreatedAt = DateTime.Now,
																		EventId = Int32.Parse(action.Value),
																		IsExcuted = 0,
																		LoanbriefId = loanBrief.LoanBriefId
																	});
																}
															}
														}
														goto pipline_processed;
                                                    }
                                                }
                                                else
                                                {
                                                    // không cần kiểm tra điều kiện gì > Thực hiện theo ActionState											
                                                    disapprovesSuccess++;
                                                    Console.WriteLine("Update Current State Disapprove :" + loanBrief.LoanBriefId + ":" + disapprove.Approve.SectionId + ":" + disapprove.ApproveId);
                                                    loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                                    loanBrief.CurrentSectionId = disapprove.Approve.SectionId;
                                                    loanBrief.CurrentSectionDetailId = disapprove.ApproveId;
                                                    loanBrief.CurrentSectionDetailId = disapprove.ApproveId;
                                                    db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                                    {
                                                        PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                                        CurrentSectionDetailId = disapprove.ApproveId,
                                                        CurrentSectionId = disapprove.Approve.SectionId
                                                    });
                                                    db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                                    db.SaveChanges();
                                                    if (totalLoanAmount > 0)
                                                        loanBrief.LoanAmount = totalLoanAmount;
                                                    var data = processPipelineSection(db, loanBrief, pipelines);
                                                    // pipeline processed, save history			
                                                    currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                                    currentHistory.EndSectionId = data.CurrentSectionId;
                                                    currentHistory.EndStatus = data.Status.Value;
                                                    currentHistory.EndPipelineState = data.PipelineState.Value;
                                                    currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                                    InsertPipelineHistory(db, currentHistory);
                                                    if (lastHistory != null)
                                                        UpdatePipelineHistory(db, lastHistory);
                                                    foreach (var action in currentSectionDetail.Actions.Where(x => x.Type == 11))
                                                    {
                                                        if (action.Value != null && !"".Equals(action.Value))
                                                        {
                                                            if (action.Value.IndexOf(",") > 0)
                                                            {
                                                                string[] values = action.Value.Split(",");
                                                                foreach (var v in values)
                                                                {
                                                                    // push event 
                                                                    db.RequestEvent.Add(new RequestEvent()
                                                                    {
                                                                        CreatedAt = DateTime.Now,
                                                                        EventId = Int32.Parse(v),
                                                                        IsExcuted = 0,
                                                                        LoanbriefId = loanBrief.LoanBriefId
                                                                    });
                                                                }
                                                            }
                                                            else
                                                            {
                                                                // push event 
                                                                db.RequestEvent.Add(new RequestEvent()
                                                                {
                                                                    CreatedAt = DateTime.Now,
                                                                    EventId = Int32.Parse(action.Value),
                                                                    IsExcuted = 0,
                                                                    LoanbriefId = loanBrief.LoanBriefId
                                                                });
                                                            }
                                                        }
                                                    }
                                                    goto pipline_processed;
                                                }
                                            }
                                            if (disapprovesSuccess == 0)
                                            {
                                                // không có đk nào thành công > chuyển đến disapprove
                                                goto unknown_pipeline;
                                            }                                                                                          
                                        }
                                        else
                                        {
                                            goto unknown_pipeline;
                                        }
                                        //if (currentSectionDetail.DisapproveId > 0)
                                        //{
                                        //    Console.WriteLine("Update Current State Cancel :" + loanBrief.LoanBriefId + ":" + currentSectionDetail.Disapprove.SectionId + ":" + currentSectionDetail.DisapproveId);
                                        //    // xu ly event
                                        //    foreach (var action in currentSectionDetail.Actions.Where(x => x.Type == 11))
                                        //    {
                                        //        if (action.Value != null && !"".Equals(action.Value))
                                        //        {
                                        //            if (action.Value.IndexOf(",") > 0)
                                        //            {
                                        //                string[] values = action.Value.Split(",");
                                        //                foreach (var v in values)
                                        //                {
                                        //                    // push event 
                                        //                    db.RequestEvent.Add(new RequestEvent()
                                        //                    {
                                        //                        CreatedAt = DateTime.Now,
                                        //                        EventId = Int32.Parse(v),
                                        //                        IsExcuted = 0,
                                        //                        LoanbriefId = loanBrief.LoanBriefId
                                        //                    });
                                        //                }
                                        //            }
                                        //            else
                                        //            {
                                        //                // push event 
                                        //                db.RequestEvent.Add(new RequestEvent()
                                        //                {
                                        //                    CreatedAt = DateTime.Now,
                                        //                    EventId = Int32.Parse(action.Value),
                                        //                    IsExcuted = 0,
                                        //                    LoanbriefId = loanBrief.LoanBriefId
                                        //                });
                                        //            }
                                        //        }
                                        //    }
                                        //    // end xu ly event
                                        //    loanBrief.PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING;
                                        //    loanBrief.CurrentSectionId = currentSectionDetail.Disapprove.SectionId;
                                        //    loanBrief.CurrentSectionDetailId = currentSectionDetail.DisapproveId;
                                        //    db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                        //    {
                                        //        PipelineState = (int)Const.ProcessType.WAITING_FOR_PROCESSING,
                                        //        CurrentSectionDetailId = currentSectionDetail.DisapproveId,
                                        //        CurrentSectionId = currentSectionDetail.Disapprove.SectionId
                                        //    });
                                        //    db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                        //    db.SaveChanges();
                                        //    if (totalLoanAmount > 0)
                                        //        loanBrief.LoanAmount = totalLoanAmount;
                                        //    var data = processPipelineSection(db, loanBrief, pipelines);
                                        //    // pipeline processed, save history			
                                        //    currentHistory.EndSectionDetailId = data.CurrentSectionDetailId;
                                        //    currentHistory.EndSectionId = data.CurrentSectionId;
                                        //    currentHistory.EndStatus = data.Status.Value;
                                        //    currentHistory.EndPipelineState = data.PipelineState.Value;
                                        //    currentHistory.ExecuteTime = (int)(DateTime.Now - beginTime).TotalMilliseconds;
                                        //    InsertPipelineHistory(db, currentHistory);
                                        //    if (lastHistory != null)
                                        //        UpdatePipelineHistory(db, lastHistory);
                                        //    goto pipline_processed;
                                        //}
                                        //else
                                        //{
                                        //    goto unknown_pipeline;
                                        //}
                                    }
                                    else
                                    {
                                        Console.WriteLine("Update Current State Invalid :" + loanBrief.LoanBriefId);
                                        // invalid state
                                        db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                                        {
                                            PipelineState = (int)Const.ProcessType.INVALID_ACTION_STATE,
                                            InProcess = 0,
                                            AddedToQueue = false
                                        });
                                        db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                                        db.SaveChanges();
                                        goto pipline_processed;
                                    }
                                }
                                else
                                    goto unknown_pipeline;
                            }
                            else
                                goto unknown_pipeline;
                        }
                        else
                        {
                            goto unknown_pipeline;
                        }
                    }
                    #region old
                    //else if (loanBrief.PipelineState == (int)Const.ProcessType.WAITING_FOR_PROCESSING)
                    //{
                    //	// update state
                    //	unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBrief.LoanBriefId, x => new LoanBrief()
                    //	{
                    //		PipelineState = (int)Const.ProcessType.IN_PROCESS_FOR_SECTION
                    //	});
                    //	unitOfWork.Save();
                    //	// process
                    //	// detail đã được xử lý thành công, thiết lập các thuộc tính của detail hiện tại
                    //	var currentPipeline = pipelines.Where(x => x.PipelineId == loanBrief.CurrentPipelineId).FirstOrDefault();
                    //	if (currentPipeline != null)
                    //	{
                    //		var currentSection = currentPipeline.Sections.Where(x => x.SectionId == loanBrief.CurrentSectionId).FirstOrDefault();
                    //		if (currentSection != null)
                    //		{
                    //			var currentSectionDetail = currentSection.Details.Where(x => x.SectionDetailId == loanBrief.CurrentSectionDetailId).FirstOrDefault();
                    //			if (currentSectionDetail != null)
                    //			{
                    //				if (loanBrief.ActionState == 1) // Approve
                    //				{
                    //					foreach (var action in currentSectionDetail.Actions.Where(x => x.Type == 1))
                    //					{
                    //						helper.UpdateDynamicField(loanBrief, action.Property.FieldName, action.Value, action.Property.Type);
                    //						Console.WriteLine("Update dynamic field:" + loanBrief.LoanBriefId + ":" + action.Property.FieldName + ":" + action.Value);
                    //					}
                    //				}
                    //				else if (loanBrief.ActionState == 2)
                    //				{
                    //					foreach (var action in currentSectionDetail.Actions)
                    //					{
                    //						helper.UpdateDynamicField(loanBrief, action.Property.FieldName, action.Value, action.Property.Type);
                    //						Console.WriteLine("Update dynamic field:" + loanBrief.LoanBriefId + ":" + action.Property.FieldName + ":" + action.Value);
                    //					}
                    //				}
                    //				unitOfWork.LoanBriefRepository.Update(loanBrief);
                    //				unitOfWork.Save();
                    //				// kiểm tra xem detail có phải là AUTOMATIC hay không
                    //				if (currentSectionDetail.Mode == "AUTOMATIC")
                    //				{
                    //					// xử lý tự động
                    //					unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBrief.LoanBriefId, x => new LoanBrief()
                    //					{
                    //						PipelineState = (int)Const.ProcessType.WAITING_FOR_AUTOMATIC_PROCESSING,
                    //						ActionState = null
                    //					});
                    //					unitOfWork.Save();
                    //					goto pipline_processed;
                    //				}
                    //				else
                    //				{
                    //					// xử lý bằng tay
                    //					unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBrief.LoanBriefId, x => new LoanBrief()
                    //					{
                    //						PipelineState = (int)Const.ProcessType.WAITING_FOR_MANUAL_PROCESSING,
                    //						ActionState = null
                    //					});
                    //					unitOfWork.Save();
                    //					goto pipline_processed;
                    //				}
                    //			}
                    //			else
                    //				goto unknown_pipeline;
                    //		}
                    //		else
                    //			goto unknown_pipeline;
                    //	}
                    //	else
                    //	{
                    //		goto unknown_pipeline;
                    //	}
                    //}
                    #endregion
                    else
                    {
                        // update state
                        db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                        {
                            PipelineState = (int)Const.ProcessType.INVALID_PIPELINE_STATE,
                            AddedToQueue = false,
                            InProcess = 0
                        });
                        db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                        db.SaveChanges();
                        goto pipline_processed;
                        // process
                        // các trạng thái khác ?
                    }
                unknown_pipeline:
                    {
                        Console.WriteLine("Pipeline Unknown:" + loanBrief.LoanBriefId);
                        db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                        {
                            PipelineState = (int)Const.ProcessType.UNKNOWN_PIPELINE,
                            AddedToQueue = false,
                            InProcess = 0
                        });
                        db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                        db.SaveChanges();
                        //unitOfWork.CommitTransaction();					
                    }
                pipline_processed:
                    {
                        Console.WriteLine("Pipeline Processed:" + loanBrief.LoanBriefId);
                        //unitOfWork.CommitTransaction();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Pipeline Exception:" + ex.Message);
                    Console.WriteLine("Pipeline Exception:" + ex.StackTrace);
                    //unitOfWork.RollbackTransaction();				
                    db.LoanBrief.Where(x => x.LoanBriefId == id).Update(x => new LoanBrief()
                    {
                        PipelineState = (int)Const.ProcessType.UNEXPECTED_ERROR,
                        InProcess = 0,
                        AddedToQueue = false
                    });
                }
            }
            current_thread--;
        }

        void InsertPipelineHistory(LOSContext db, PipelineHistory history)
        {
            db.PipelineHistory.Add(history);
            db.SaveChanges();
        }

        void UpdatePipelineHistory(LOSContext db, PipelineHistory history)
        {
            if (history != null && history.LoanBriefId > 0)
            {
                db.PipelineHistory.Where(x => x.PipelineHistoryId == history.PipelineHistoryId).Update(x => new PipelineHistory()
                {
                    EndTime = history.EndTime
                });
                db.SaveChanges();
            }
        }

        LoanBrief processPipelineSection(LOSContext db, LoanBrief loanBrief, List<PipelineDTO> pipelines)
        {
            Thread.Sleep(5);
            DatabaseHelper<LoanBrief> helper = new DatabaseHelper<LoanBrief>();
            // detail đã được xử lý thành công, thiết lập các thuộc tính của detail hiện tại
            var currentPipeline = pipelines.Where(x => x.PipelineId == loanBrief.CurrentPipelineId).FirstOrDefault();
            if (currentPipeline != null)
            {
                var currentSection = currentPipeline.Sections.Where(x => x.SectionId == loanBrief.CurrentSectionId).FirstOrDefault();
                if (currentSection != null)
                {
                    var currentSectionDetail = currentSection.Details.Where(x => x.SectionDetailId == loanBrief.CurrentSectionDetailId).FirstOrDefault();
                    if (currentSectionDetail != null)
                    {
                        foreach (var action in currentSectionDetail.Actions.Where(x => x.Type == 1))
                        {
                            helper.UpdateDynamicField(loanBrief, action.Property.FieldName, action.Value, action.Property.Type);
                            Console.WriteLine("Update dynamic field:" + loanBrief.LoanBriefId + ":" + action.Property.FieldName + ":" + action.Value);
                        }
                        // event 
                        foreach (var action in currentSectionDetail.Actions.Where(x => x.Type == 10))
                        {
                            if (action.Value != null && !"".Equals(action.Value))
                            {
                                if (action.Value.IndexOf(",") > 0)
                                {
                                    string[] values = action.Value.Split(",");
                                    foreach (var v in values)
                                    {
                                        // push event 
                                        db.RequestEvent.Add(new RequestEvent()
                                        {
                                            CreatedAt = DateTime.Now,
                                            EventId = Int32.Parse(v),
                                            IsExcuted = 0,
                                            LoanbriefId = loanBrief.LoanBriefId
                                        });
                                    }
                                }
                                else
                                {
                                    // push event 
                                    db.RequestEvent.Add(new RequestEvent()
                                    {
                                        CreatedAt = DateTime.Now,
                                        EventId = Int32.Parse(action.Value),
                                        IsExcuted = 0,
                                        LoanbriefId = loanBrief.LoanBriefId
                                    });
                                }
                            }
                        }
                        if (loanBrief.ActionState == 2)
                        {
                            foreach (var action in currentSectionDetail.Actions.Where(x => x.Type == 2))
                            {
                                helper.UpdateDynamicField(loanBrief, action.Property.FieldName, action.Value, action.Property.Type);
                                Console.WriteLine("Update dynamic field:" + loanBrief.LoanBriefId + ":" + action.Property.FieldName + ":" + action.Value);
                            }
                        }
                        db.LoanBrief.Update(loanBrief);
                        db.Entry(loanBrief).Property(x => x.LoanAmount).IsModified = false;
                        db.SaveChanges();
                        // kiểm tra xem detail có phải là AUTOMATIC hay không
                        if (currentSectionDetail.Mode == "MANUAL")
                        {
                            // xử lý bằng tay
                            db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                            {
                                PipelineState = (int)Const.ProcessType.WAITING_FOR_MANUAL_PROCESSING,
                                ActionState = null,
                                InProcess = 0,
                                AddedToQueue = false
                            });
                            goto pipline_processed;
                        }
                        else
                        {
                            // xử lý tự động
                            db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                            {
                                PipelineState = (int)Const.ProcessType.WAITING_FOR_AUTOMATIC_PROCESSING,
                                ActionState = null,
                                InProcess = 0,
                                AddedToQueue = false
                            });
                            goto pipline_processed;
                        }
                    }
                    else
                        goto unknown_pipeline;
                }
                else
                    goto unknown_pipeline;
            }
            else
            {
                goto unknown_pipeline;
            }
        unknown_pipeline:
            {
                Console.WriteLine("Pipeline Unknown:" + loanBrief.LoanBriefId);
                db.LoanBrief.Where(x => x.LoanBriefId == loanBrief.LoanBriefId).Update(x => new LoanBrief()
                {
                    PipelineState = (int)Const.ProcessType.UNKNOWN_PIPELINE,
                    ActionState = null,
                    InProcess = 0,
                    AddedToQueue = false
                });
            }
        pipline_processed:
            {
                Console.WriteLine("Pipeline Section Processed:" + loanBrief.LoanBriefId);
            }
            return loanBrief;
        }

        private void getPipeline(object state)
        {
            try
            {
                var pipelines = unitOfWork.PipelineRepository.Query(x => x.Status != 99, x => x.OrderBy(x1 => x1.Priority), false)
                    .Select(x => new PipelineDTO()
                    {
                        CreatedAt = x.CreatedAt,
                        CreatorId = x.CreatorId,
                        ModifiedAt = x.ModifiedAt,
                        Name = x.Name,
                        PipelineId = x.PipelineId,
                        Priority = x.Priority,
                        Status = x.Status,
                        Sections = x.Section.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Order).Select(x1 => new SectionDTO()
                        {
                            Name = x1.Name,
                            Order = x1.Order,
                            SectionGroupId = x1.SectionGroupId,
                            SectionId = x1.SectionId,
                            IsShow = x1.IsShow,
                            PipelineId = x1.PipelineId,
                            Details = x1.SectionDetail.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Step).Select(x2 => new SectionDetailDTO()
                            {
                                Mode = x2.Mode,
                                Name = x2.Name,
                                SectionDetailId = x2.SectionDetailId,
                                SectionId = x2.SectionId,
                                Step = x2.Step,
                                IsShow = x2.IsShow,
                                Approves = x2.SectionApproveSectionDetail.Where(x => x.Type == 1).Select(x3 => new SectionApproveDTO()
                                {
                                    Name = x3.Name,
                                    Type = x3.Type,
                                    Approve = x3.Approve != null ? new SectionDetailDTO()
                                    {
                                        Name = x3.Approve.Name,
                                        SectionDetailId = x3.Approve.SectionDetailId,
                                        SectionId = x3.Approve.SectionId,
                                        Step = x3.Approve.Step
                                    } : null,
                                    SectionApproveId = x3.SectionApproveId,
                                    SectionDetailId = x3.SectionDetailId,
                                    ApproveId = x3.ApproveId,
                                    Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
                                    {
                                        Operator = x4.Operator,
                                        PropertyId = x4.PropertyId,
                                        SectionConditionId = x4.SectionConditionId,
                                        SectionApproveId = x4.SectionApproveId,
                                        Value = x4.Value,
                                        Property = x4.Property != null ? new PropertyDTO()
                                        {
                                            Object = x4.Property.Object,
                                            FieldName = x4.Property.FieldName,
                                            Priority = x4.Property.Priority,
                                            Regex = x4.Property.Regex,
                                            Type = x4.Property.Type,
                                            PropertyId = x4.Property.PropertyId,
                                            Name = x4.Property.Name,
                                            LinkObject = x4.Property.LinkObject
                                        } : null
                                    })
                                }),
                                Disapproves = x2.SectionApproveSectionDetail.Where(x =>x.Type == 2).Select(x3 => new SectionApproveDTO()
                                {
                                    Name = x3.Name,
                                    Type = x3.Type,
                                    Approve = x3.Approve != null ? new SectionDetailDTO()
                                    {
                                        Name = x3.Approve.Name,
                                        SectionDetailId = x3.Approve.SectionDetailId,
                                        SectionId = x3.Approve.SectionId,
                                        Step = x3.Approve.Step
                                    } : null,
                                    SectionApproveId = x3.SectionApproveId,
                                    SectionDetailId = x3.SectionDetailId,
                                    ApproveId = x3.ApproveId,
                                    Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
                                    {
                                        Operator = x4.Operator,
                                        PropertyId = x4.PropertyId,
                                        SectionConditionId = x4.SectionConditionId,
                                        SectionApproveId = x4.SectionApproveId,
                                        Value = x4.Value,
                                        Property = x4.Property != null ? new PropertyDTO()
                                        {
                                            Object = x4.Property.Object,
                                            FieldName = x4.Property.FieldName,
                                            Priority = x4.Property.Priority,
                                            Regex = x4.Property.Regex,
                                            Type = x4.Property.Type,
                                            PropertyId = x4.Property.PropertyId,
                                            Name = x4.Property.Name,
                                            LinkObject = x4.Property.LinkObject
                                        } : null
                                    })
                                }),                               
                                ReturnId = x2.ReturnId,
                                Return = x2.Return != null ? new SectionDetailDTO()
                                {
                                    Name = x2.Return.Name,
                                    SectionDetailId = x2.Return.SectionDetailId,
                                    SectionId = x2.Return.SectionId,
                                    Step = x2.Return.Step
                                } : null,
                                Actions = x2.SectionAction.Select(x3 => new SectionActionDTO()
                                {
                                    PropertyId = x3.PropertyId,
                                    SectionActionId = x3.SectionActionId,
                                    SectionDetailId = x3.SectionDetailId,
                                    Value = x3.Value,
                                    Type = x3.Type,
                                    Property = x3.Property != null ? new PropertyDTO()
                                    {
                                        Object = x3.Property.Object,
                                        FieldName = x3.Property.FieldName,
                                        Priority = x3.Property.Priority,
                                        Regex = x3.Property.Regex,
                                        Type = x3.Property.Type,
                                        PropertyId = x3.Property.PropertyId,
                                        Name = x3.Property.Name,
                                        LinkObject = x3.Property.LinkObject
                                    } : null
                                })
                            })
                        })
                    }).ToList();
                this.pipelines = pipelines;
                Console.WriteLine("Config reloaded");
            }
            catch (Exception ex)
            {

            }
        }

        private static void OnConsumerConsumerCancelled(object sender, ConsumerEventArgs e)
        {
            Console.WriteLine($"loan consumer cancelled");
        }
        private static void OnConsumerUnregistered(object sender, ConsumerEventArgs e)
        {
            Console.WriteLine($"loan consumer unregistered");
        }
        private static void OnConsumerRegistered(object sender, ConsumerEventArgs e)
        {
            Console.WriteLine($"loan consumer registered");
        }
        private static void OnConsumerShutdown(object sender, ShutdownEventArgs e)
        {
            Console.WriteLine($"loan consumer shut down");
        }
        private static void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e)
        {
            Console.WriteLine($"loan connection shut down");
        }

        public void Dispose()
        {
            connection.Dispose();
            _timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            //Console.WriteLine($"loan service started");			
            InitRabbitMQ();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine($"loan service stopped");
            connection.Close();
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
    }
}
