﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using LOS.Common.Helpers;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.Helpers;
using RabbitMQ.Client;
using LOS.PipelineProccesor.Models;
using System.Threading;
using System.Net.Http;

namespace LOS.PipelineProccesor.Jobs
{
	public class AutocallTelegramBot
	{		
		public AutocallTelegramBot()
		{
			
		}

		public void Process(DateTime startTime)
		{
			try
			{
				var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
				if (environmentName.Equals("Production"))
				{
					// create new unit of work instance			
					var builder = new ConfigurationBuilder()
					  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
					  .AddJsonFile("appsettings.json", true)
					  .AddJsonFile($"appsettings.{environmentName}.json", true);
					var configuration = builder.Build();
					var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
					optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
					DbContextOptions<LOSContext> options = optionsBuilder.Options;
					using (var db = new LOSContext(options))
					{
						using (var unitOfWork = new UnitOfWork(db))
						{
							DateTime now = DateTime.Now;
							List<int> productIds = new List<int>() { 2, 5 };
							var lbs = unitOfWork.LoanBriefRepository.Query(x => x.CreatedTime <= now.AddMinutes(-5) && x.CreatedTime > startTime && x.Status == 20 && x.PlatformType != 9 && productIds.Contains(x.ProductId.Value) && x.PushCiscoAt == null && (!x.IsReborrow.HasValue || !x.IsReborrow.Value) && (x.CreateBy == 0 || x.CreateBy == null) && x.UtmSource != "Autocall-Remarketing" && x.LastChangeStatusOfTelesale == null, null, false).Select(x => x.LoanBriefId).ToList();
							using (var client = new HttpClient())
							{
								string message = "";
								if (lbs.Count >= 1)
								{
									// ?
									if (lbs.Count < 50)
									{
										message = "Alert Autocall - Remain loan brief id :" + String.Join(",", lbs);
									}
									else
									{
										message = "Alert Autocall - Remain loan count :" + lbs.Count;
									}	
									var response = client.GetAsync("https://api.telegram.org/bot1393561830:AAGa8-u0JAIdrk3QbvYmo2-bkgFmevFPf0I/sendMessage?chat_id=-483658891&text=" + message).Result;
									Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " - Bot has been sent");
								}
								else
								{
									Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " - No Loan Brief Found");
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
				Log.Error("Exception:" + ex.Message);
				Log.Error("Exception:" + ex.StackTrace);
			}
		}

		
	}
}
