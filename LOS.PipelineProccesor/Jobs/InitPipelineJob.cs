﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using LOS.Common.Helpers;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.Helpers;
using RabbitMQ.Client;
using LOS.PipelineProccesor.Models;
using System.Threading;

namespace LOS.PipelineProccesor.Jobs
{
	public class InitPipelineJob : IBaseJob
	{
		UnitOfWork unitOfWork;
		public InitPipelineJob()
		{
			
		}

		public void Process()
		{
			try
			{
				var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
				// create new unit of work instance			
				var builder = new ConfigurationBuilder()
				  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
				  .AddJsonFile("appsettings.json", true)
				  .AddJsonFile($"appsettings.{environmentName}.json", true);
				var configuration = builder.Build();
				var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
				optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
				DbContextOptions<LOSContext> options = optionsBuilder.Options;
				var db = new LOSContext(options);
				unitOfWork = new UnitOfWork(db);
				var briefs = unitOfWork.LoanBriefRepository.Get(x => (x.PipelineState == (int)Const.ProcessType.WAITING_FOR_PIPELINE || x.PipelineState == null) && x.ProductId > 0 && (x.AddedToQueue == null || !x.AddedToQueue.Value) , null, false).ToList();
				if (briefs != null && briefs.Count() > 0)
				{
					// add to queue
					//var factory = new ConnectionFactory() { HostName = "178.128.86.175", Port = 5673 };					
					var factory = new ConnectionFactory { HostName = configuration["AppSettings:RabbitmqHost"], Port = 5673, UserName = "root", Password = "QPv8qLtiQ5" };
					using (var connection = factory.CreateConnection())
					using (var channel = connection.CreateModel())
					{
						// channel.ExchangeDeclare("los.exchange", ExchangeType.Direct);
						channel.QueueDeclare(queue: "los.init_loan_queue",
											 durable: true,
											 exclusive: false,
											 autoDelete: false,
											 arguments: null);
						//channel.QueueBind("los.init_loan_queue", "los.exchange", "", null);

						var properties = channel.CreateBasicProperties();
						properties.Persistent = true;

						foreach (var brief in briefs)
						{
							// check xem pipeline state có bị thay đổi k
							unitOfWork.LoanBriefRepository.Reload(brief);
							if (brief.PipelineState == 0 || brief.PipelineState == null)
							{
								// update added to queue
								unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == brief.LoanBriefId, x => new LoanBrief()
								{
									AddedToQueue = true,
									InProcess = 1,
									PipelineState = (int)Const.ProcessType.IN_PROCESS_FOR_PIPELINE
								});
								unitOfWork.Save();
								Thread.Sleep(1000);
								IdOnlyReq idReq = new IdOnlyReq();
								idReq.id = brief.LoanBriefId;
								var message = JsonConvert.SerializeObject(idReq);
								var body = Encoding.UTF8.GetBytes(message);
								channel.BasicPublish(exchange: "",
													 routingKey: "los.init_loan_queue",
													 basicProperties: properties,
													 body: body);
								Console.WriteLine("Pipeline: LoanBriefID: " + idReq.id + " added to queue init");
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Log.Error("Pipeline Exception:" + ex.Message);
				Log.Error("Pipeline Exception:" + ex.StackTrace);
			}
		}

		
	}
}
