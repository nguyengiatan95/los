﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using LOS.Common.Helpers;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.Helpers;
using RabbitMQ.Client;
using LOS.PipelineProccesor.Models;
using System.Threading;

namespace LOS.PipelineProccesor.Jobs
{
	public class PipelineProcessJob : IBaseJob
	{		
		public PipelineProcessJob()
		{
		
		}

		public void Process()
		{
			try
			{
				var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
				// create new unit of work instance			
				var builder = new ConfigurationBuilder()
				  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
				  .AddJsonFile("appsettings.json", true)
				  .AddJsonFile($"appsettings.{environmentName}.json", true);
				var configuration = builder.Build();
				var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
				optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
				DbContextOptions<LOSContext> options = optionsBuilder.Options;
				using (var db = new LOSContext(options))
				{
					using (var unitOfWork = new UnitOfWork(db))
					{						
						// tìm đơn vay cần được xử lý
						var briefs = unitOfWork.LoanBriefRepository.Get(x => (x.PipelineState == (int)Const.ProcessType.MANUAL_PROCCESSED
							|| x.PipelineState == (int)Const.ProcessType.AUTOMATIC_PROCESSED) && (x.AddedToQueue == null || !x.AddedToQueue.Value), null, false).ToList();
						if (briefs != null && briefs.Count() > 0)
						{
							// add to queue					
							var factory = new ConnectionFactory { HostName = configuration["AppSettings:RabbitmqHost"], Port = 5673, UserName = "root", Password = "QPv8qLtiQ5" };
							using (var connection = factory.CreateConnection())
							using (var channel = connection.CreateModel())
							{
								//channel.ExchangeDeclare("los.exchange", ExchangeType.Direct);
								channel.QueueDeclare(queue: "los.normal_loan_queue",
													 durable: true,
													 exclusive: false,
													 autoDelete: false,
													 arguments: null);
								//channel.QueueBind("los.normal_loan_queue", "los.exchange", "", null);

								var properties = channel.CreateBasicProperties();
								properties.Persistent = true;

								foreach (var brief in briefs)
								{
									// update added to queue
									unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == brief.LoanBriefId, x => new LoanBrief()
									{
										AddedToQueue = true,
										InProcess = 1
									});
									unitOfWork.Save();
									Thread.Sleep(1000);
									IdOnlyReq idReq = new IdOnlyReq();
									idReq.id = brief.LoanBriefId;
									var message = JsonConvert.SerializeObject(idReq);
									var body = Encoding.UTF8.GetBytes(message);
									channel.BasicPublish(exchange: "",
														 routingKey: "los.normal_loan_queue",
														 basicProperties: properties,
														 body: body);
									Console.WriteLine("Pipeline: LoanBriefID: " + idReq.id + " added to queue ");
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Pipeline Exception:" + ex.Message);
				Console.WriteLine("Pipeline Exception:" + ex.StackTrace);
			}
		}
	}
}
