﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using LOS.Common.Helpers;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.Helpers;
using RabbitMQ.Client;
using LOS.PipelineProccesor.Models;
using System.Threading;

namespace LOS.PipelineProccesor.Jobs
{
	public class ResetReceivedJob : IBaseJob
	{
		UnitOfWork unitOfWork;
		public ResetReceivedJob()
		{
			
		}

		public void Process()
		{
			try
			{
				var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
				// create new unit of work instance			
				var builder = new ConfigurationBuilder()
				  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
				  .AddJsonFile("appsettings.json", true)
				  .AddJsonFile($"appsettings.{environmentName}.json", true);
				var configuration = builder.Build();
				var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
				optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
				DbContextOptions<LOSContext> options = optionsBuilder.Options;
				var db = new LOSContext(options);
				unitOfWork = new UnitOfWork(db);
				unitOfWork.ShopRepository.Update(x => x.ShopId > 0, x => new Shop() { 
					Received = 0
				});
				unitOfWork.Save();
				Log.Information("HUB Received has been reseted");
			}
			catch (Exception ex)
			{
				Log.Error("Exception:" + ex.Message);
				Log.Error("Exception:" + ex.StackTrace);
			}
		}

		
	}
}
