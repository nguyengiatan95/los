﻿using Hangfire;
using Hangfire.MemoryStorage;
using System;
using LOS.PipelineProccesor.Jobs;
using System.Threading;
using LOS.PipelineProccesor.Comsumers;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using LOS.PipelineProccesor.Services;
using LOS.DAL.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using LOS.DAL.UnitOfWork;

namespace PipelineProccesor
{
	class Program
	{
		static void Main(string[] args)
		{
			var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
			Console.WriteLine(environmentName);
			// Dependency Injection					
			new HostBuilder().ConfigureServices((hostContext, services) =>
			{
				var builder = new ConfigurationBuilder()
				  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
				  .AddJsonFile("appsettings.json", true)
				  .AddJsonFile($"appsettings.{environmentName}.json", true);
				var configuration = builder.Build();
				
				services.AddDbContext<LOSContext>(options =>
					options.UseSqlServer(configuration.GetConnectionString("LOSDatabase")),ServiceLifetime.Transient);
				services.AddTransient<IUnitOfWork, UnitOfWork>();
				services.AddHostedService<InitLoanConsumer>();
				services.AddHostedService<LoanConsumer>();
				services.AddHostedService<LoanService>();				
				services.AddHostedService<HangfireService>();				
			}).RunConsoleAsync();
		}
	}
}
