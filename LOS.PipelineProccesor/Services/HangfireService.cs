using Hangfire;
using Hangfire.MemoryStorage;
using LOS.PipelineProccesor.Jobs;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.PipelineProccesor.Services
{
	public class HangfireService : IHostedService, IDisposable
	{
		private static readonly AutoResetEvent autoResetEvent = new AutoResetEvent(false);

		public void Dispose()
		{
			throw new NotImplementedException();
		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			// create hangfire service
			GlobalConfiguration.Configuration.UseMemoryStorage();
			// GlobalConfiguration.Configuration.UseActivator(new ContainerJobActivator(_serviceProvider));

			using (var server = new BackgroundJobServer())
			{
				Console.WriteLine("Hangfire Server started. Press ENTER to exit...");
				#region init loan credit pipeline
				// refresh every 1m
				//RecurringJob.AddOrUpdate<InitPipelineJob>("InitPipelineJob", x => x.Process(), "*/10 * * * * *");
				//BackgroundJob.Enqueue<InitPipelineJob>(x => x.Process());
				#endregion
				#region process loan credit pipeline
				// refresh every 1m
				//RecurringJob.AddOrUpdate<PipelineProcessJob>("PipelineProcessJob", x => x.Process(), "*/10 * * * * *");
				//BackgroundJob.Enqueue<PipelineProcessJob>(x => x.Process());
				#endregion
				#region reset received every day
				RecurringJob.AddOrUpdate<ResetReceivedJob>("ResetReceivedJob", x => x.Process(), "0 0 * * *", TimeZoneInfo.Local);
				//BackgroundJob.Enqueue<PipelineProcessJob>(x => x.Process());
				#endregion
				#region process alert auto call telegram
				// refresh every 1m
				//DateTime dt = new DateTime(2020, 10, 28, 17, 0, 0);
				//RecurringJob.AddOrUpdate<AutocallTelegramBot>("AutocallTelegramBot", x => x.Process(dt), "* * * * *");	
				
				#endregion
				AppDomain.CurrentDomain.ProcessExit += (o, e) =>
				{
					server.Dispose();
					Console.WriteLine("Terminating...");
					autoResetEvent.Set();
				};
				autoResetEvent.WaitOne();
			}
			return Task.CompletedTask;
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			throw new NotImplementedException();
		}
	}
}
