﻿using LOS.Common.Helpers;
using LOS.DAL.EntityFramework;
using LOS.PipelineProccesor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace LOS.PipelineProccesor.Services
{
	class LoanService : IHostedService, IDisposable
	{
		List<LoanBrief> briefs = new List<LoanBrief>();

		public LoanService()
		{

		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			Console.WriteLine($"loan service started");
			var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");	
			var builder = new ConfigurationBuilder()
			  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
			  .AddJsonFile("appsettings.json", true)
			  .AddJsonFile($"appsettings.{environmentName}.json", true);
			var configuration = builder.Build();
			var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
			optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
			DbContextOptions<LOSContext> options = optionsBuilder.Options;
			while (true)
			{
				try
				{
					if (briefs != null && briefs.Count > 0)
					{
						var factory = new ConnectionFactory { HostName = configuration["AppSettings:RabbitmqHost"], Port = 5673, UserName = "root", Password = "QPv8qLtiQ5" };
						using (var connection = factory.CreateConnection())
						using (var channel = connection.CreateModel())
						{
							channel.QueueDeclare(queue: "los.init_loan_queue",
												 durable: true,
												 exclusive: false,
												 autoDelete: false,
												 arguments: null);

							channel.QueueDeclare(queue: "los.normal_loan_queue",
												 durable: true,
												 exclusive: false,
												 autoDelete: false,
												 arguments: null);

							var properties = channel.CreateBasicProperties();
							properties.Persistent = true;
							using (var los = new LOSContext(options))
							{
								foreach (var brief in briefs)
								{
									// update db
									if (brief.PipelineState == 0 || brief.PipelineState == null)
									{
										los.LoanBrief.Where(x => x.LoanBriefId == brief.LoanBriefId).Update(x => new LoanBrief()
										{
											AddedToQueue = true,
											InProcess = 1,
											PipelineState = (int)Const.ProcessType.IN_PROCESS_FOR_PIPELINE
										});
										IdOnlyReq idReq = new IdOnlyReq();
										idReq.id = brief.LoanBriefId;
										var message = JsonConvert.SerializeObject(idReq);
										var body = Encoding.UTF8.GetBytes(message);
										channel.BasicPublish(exchange: "",
															 routingKey: "los.init_loan_queue",
															 basicProperties: properties,
															 body: body);
										Console.WriteLine("Pipeline: LoanBriefID: " + brief.LoanBriefId + " added to queue init");
									}
									else
									{
										los.LoanBrief.Where(x => x.LoanBriefId == brief.LoanBriefId).Update(x => new LoanBrief()
										{
											AddedToQueue = true,
											InProcess = 1
										});
										IdOnlyReq idReq = new IdOnlyReq();
										idReq.id = brief.LoanBriefId;
										var message = JsonConvert.SerializeObject(idReq);
										var body = Encoding.UTF8.GetBytes(message);
										channel.BasicPublish(exchange: "",
															 routingKey: "los.normal_loan_queue",
															 basicProperties: properties,
															 body: body);
										Console.WriteLine("Pipeline: LoanBriefID: " + brief.LoanBriefId + " added to queue");
									}
								}
								briefs.Clear();
							}
						}
						Thread.Sleep(1000);
					}
					else
					{
						using (var los = new LOSContext(options))
						{
							briefs = los.LoanBrief.Where(x => (x.PipelineState == (int)Const.ProcessType.WAITING_FOR_PIPELINE
								|| x.PipelineState == (int)Const.ProcessType.AUTOMATIC_PROCESSED || x.PipelineState == (int)Const.ProcessType.MANUAL_PROCCESSED || x.PipelineState == null)
								&& x.ProductId > 0 && (x.AddedToQueue == null || !x.AddedToQueue.Value)).ToList();
						}
						Thread.Sleep(1000);
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
				}
			}
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			Console.WriteLine($"loan service stopped");
			return Task.CompletedTask;
		}

		public void Dispose()
		{

		}
	}
}
