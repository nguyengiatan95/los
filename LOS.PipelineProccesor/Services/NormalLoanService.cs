﻿using LOS.Common.Helpers;
using LOS.DAL.EntityFramework;
using LOS.PipelineProccesor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace LOS.PipelineProccesor.Services
{
	class NormalLoanService : IHostedService, IDisposable
	{
		HashSet<int> ids = new HashSet<int>();	

		public NormalLoanService()
		{

		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			Console.WriteLine($"normal loan service started");
			var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
			// create new unit of work instance			
			var builder = new ConfigurationBuilder()
			  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
			  .AddJsonFile("appsettings.json", true)
			  .AddJsonFile($"appsettings.{environmentName}.json", true);
			var configuration = builder.Build();
			var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
			optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
			DbContextOptions<LOSContext> options = optionsBuilder.Options;
			while (true)
			{
				if (ids.Count > 0)
				{
					var factory = new ConnectionFactory { HostName = configuration["AppSettings:RabbitmqHost"], Port = 5673, UserName = "root", Password = "QPv8qLtiQ5" };
					using (var connection = factory.CreateConnection())
					using (var channel = connection.CreateModel())
					{						
						channel.QueueDeclare(queue: "los.normal_loan_queue",
											 durable: true,
											 exclusive: false,
											 autoDelete: false,
											 arguments: null);						

						var properties = channel.CreateBasicProperties();
						properties.Persistent = true;
						using (var los = new LOSContext(options))
						{
							foreach (var id in ids)
							{
								// update db
								los.LoanBrief.Where(x => x.LoanBriefId == id).Update(x => new LoanBrief()
								{
									AddedToQueue = true,
									InProcess = 1									
								});
								IdOnlyReq idReq = new IdOnlyReq();
								idReq.id = id;
								var message = JsonConvert.SerializeObject(idReq);
								var body = Encoding.UTF8.GetBytes(message);
								channel.BasicPublish(exchange: "",
													 routingKey: "los.normal_loan_queue",
													 basicProperties: properties,
													 body: body);								
								Console.WriteLine("Pipeline: LoanBriefID: " + id + " added to queue");
							}
							// clear all ids
							ids.Clear();
						}
					}
					Thread.Sleep(1000);
				}
				else
				{
					using (var los = new LOSContext(options))
					{
						var briefs = los.LoanBrief.Where(x => (x.PipelineState == (int)Const.ProcessType.MANUAL_PROCCESSED
							|| x.PipelineState == (int)Const.ProcessType.AUTOMATIC_PROCESSED) && (x.AddedToQueue == null || !x.AddedToQueue.Value)).Select(x => x.LoanBriefId).ToList();
						if (briefs != null && briefs.Count() > 0)
						{
							foreach (var id in briefs)
							{
								// add to list
								ids.Add(id);
							}
						}
					}
					Thread.Sleep(1000);
				}
			}
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			Console.WriteLine($"normal loan service stopped");
			return Task.CompletedTask;
		}

		public void Dispose()
		{

		}
	}
}
