﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ProtoHelper.Models.Reply
{
	[ProtoContract]
	public class DefaultReply
	{
		[ProtoMember(1)]
		public Meta meta { get; set; }
	}

	[ProtoContract]
	public class Meta
	{
		[ProtoMember(1)]
		public int code { get; set; }
		[ProtoMember(2)]
		public string message { get; set; }
	}
}
