﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ProtoHelper.Models.Request
{
	[ProtoContract]
	public class LoginRequest
	{
		[ProtoMember(1)]
		public string username { get; set; }
		[ProtoMember(2)]
		public string pasword { get; set; }
	}
}
