﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ProtoHelper.Models.Request
{
	[ProtoContract]
	public class UpdateCallStatusRequest
	{
		[ProtoMember(1)]
		public int loanActionId { get; set; }
		[ProtoMember(1)]
		public int actionStatusId { get; set; }
	}
}
