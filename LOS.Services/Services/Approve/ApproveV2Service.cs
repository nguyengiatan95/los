﻿using LOS.DAL.DTOs;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Services.Services.Approve
{
    public class ApproveV2Service : IApproveV2Service
    {
        public readonly IApproveRepository _approveRepository;
        public ApproveV2Service(IApproveRepository approveRepository)
        {
            this._approveRepository = approveRepository;
        }
        public GetDisbursementAfter GetDisbursementAfter(int telesaleId, int hubId, int provinceId, int loanBriefId, int status, int productId,
            int locate, int borrowCavet, int typeSearch, int topup, DateTime fromDate, DateTime toDate, string search, int empHubId, int coordinatorUserId, int page, int pageSize, ref int recordsTotal)
        {
            return _approveRepository.GetDisbursementAfter(telesaleId, hubId, provinceId, loanBriefId, status, productId, locate, borrowCavet,
                typeSearch, topup, fromDate, toDate, search, empHubId, coordinatorUserId, page, pageSize, ref recordsTotal);
        }
    }
}
