﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Services.Services.Approve
{
    public interface IApproveV2Service
    {
        GetDisbursementAfter GetDisbursementAfter(int telesaleId, int hubId, int provinceId, int loanBriefId, int status, int productId,
           int locate, int borrowCavet, int typeSearch, int topup, DateTime fromDate, DateTime toDate, string search, int empHubId, int coordinatorUserId, int page, int pageSize, ref int recordsTotal);
    }
}
