﻿using LOS.DAL.DTOs;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services.ConfigProductDetail
{
    public class ConfigProductDetailService : IConfigProductDetailService
    {
        public readonly IConfigProductDetailRepository _configProductDetailRepository;
        public ConfigProductDetailService(IConfigProductDetailRepository configProductDetailRepository)
        {
            this._configProductDetailRepository = configProductDetailRepository;
        }
        public async Task<ConfigProductCreditDetail> GetConfigProductCreditDetail(int productId, int productDetailId, int typeOwnership)
        {
            return await _configProductDetailRepository.GetConfigProductCreditDetail(productId, productDetailId, typeOwnership);
        }

        public async Task<List<ConfigProductCreditDetail>> GetConfigProductCreditDetailNext(int productId, int typeOwnership, long maxMoney)
        {
            return await _configProductDetailRepository.GetConfigProductCreditDetailNext(productId, typeOwnership, maxMoney);
        }
    }
}
