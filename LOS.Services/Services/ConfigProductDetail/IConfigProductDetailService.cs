﻿using LOS.DAL.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.Services.Services.ConfigProductDetail
{
    public interface IConfigProductDetailService
    {
        Task<ConfigProductCreditDetail> GetConfigProductCreditDetail(int productId, int productDetailId, int typeOwnership);
        Task<List<ConfigProductCreditDetail>> GetConfigProductCreditDetailNext(int productId, int typeOwnership, long maxMoney);
    }
}