﻿using LOS.DAL.DTOs;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services.District
{
    public class DistrictService : IDistrictService
    {
        public readonly IDistrictRepository _districtRepository;
        public DistrictService(IDistrictRepository districtRepository)
        {
            this._districtRepository = districtRepository;
        }

        public List<DistrictDetail> Search(string searchName, int provinceId, int status, int page, int pageSize, ref int recordsTotal)
        {
            return _districtRepository.Search(searchName, provinceId, status, page, pageSize, ref recordsTotal);
        }

        public async Task<bool> UpdateIsApplyDistrict(int districtId, int isApply)
        {
            return await _districtRepository.UpdateIsApplyDistrict(districtId, isApply);
        }

        public async Task<bool> Add(DAL.EntityFramework.District entity)
        {
            return await _districtRepository.Add(entity);
        }

        public async Task<bool> Update(DAL.EntityFramework.District entity)
        {
            return await _districtRepository.Update(entity);
        }
        
        public async Task<List<DistrictDetail>> GetDistrictByProvinceId(int provinceId)
        {
            return await _districtRepository.GetDistrictByProvinceId(provinceId);
        }

        public async Task<int> GetProvinceIdByDistrictId(int districtId)
        {
            return await _districtRepository.GetProvinceIdByDistrictId(districtId);
        }

        public async Task<bool> CheckDistrict(string name, int provinceId)
        {
            return await _districtRepository.CheckDistrict(name, provinceId);
        }
    }
}
