﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services.District
{
    public interface IDistrictService
    {
        List<DistrictDetail> Search(string searchName, int provinceId, int status, int page, int pageSize, ref int recordsTotal);
        Task<bool> UpdateIsApplyDistrict(int districtId, int isApply);
        Task<bool> Add(DAL.EntityFramework.District entity);
        Task<bool> Update(DAL.EntityFramework.District entity);
        Task<List<DistrictDetail>> GetDistrictByProvinceId(int provinceId);
        Task<int> GetProvinceIdByDistrictId(int districtId);
        Task<bool> CheckDistrict(string name, int provinceId);
    }
}
