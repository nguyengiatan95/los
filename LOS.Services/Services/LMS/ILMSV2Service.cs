﻿using LOS.DAL.Models;
using LOS.DAL.Models.LMS;

namespace LOS.Services.Services.LMS
{
    public interface ILMSV2Service
    {
        DefaultResponce CreateDistrict(CreateDistrict entity);
        DefaultResponce CreateWard(CreateWard entity);
        PaymentLoanById.OutPut GetPaymentLoanById(int loanId);
        DeferredPaymentLms.Output DeferredPayment(DeferredPaymentLms.Input param);
    }
}