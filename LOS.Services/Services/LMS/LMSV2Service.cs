﻿using LOS.Common.Extensions;
using LOS.DAL.Helpers;
using LOS.DAL.Models;
using LOS.DAL.Models.LMS;
using LOS.Services.Services.LogCallApi;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Services.Services.LMS
{
    public class LMSV2Service : ILMSV2Service
    {
        private readonly IConfiguration _configuration;
        private ILogCallApiV2Service _logCallApiV2Service;

        public LMSV2Service(IConfiguration configuration, ILogCallApiV2Service logCallApiV2Service)
        {
            _configuration = configuration;
            this._logCallApiV2Service = logCallApiV2Service;
        }

        public DefaultResponce CreateDistrict(CreateDistrict entity)
        {
            try
            {
                var url = _configuration["AppSettings:LMS"] + Constants.CreateDistrict;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(entity);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Thêm log
                var log = new DAL.EntityFramework.LogCallApi
                {
                    ActionCallApi = ActionCallApi.CreateDistrict.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = body
                };
                _logCallApiV2Service.Add(log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                log.ModifyAt = DateTime.Now;
                _logCallApiV2Service.Update(log);
                DefaultResponce lst = JsonConvert.DeserializeObject<DefaultResponce>(json);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DefaultResponce CreateWard(CreateWard entity)
        {
            try
            {
                var url = _configuration["AppSettings:LMS"] + Constants.CreateWard;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(entity);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Thêm log
                var log = new DAL.EntityFramework.LogCallApi
                {
                    ActionCallApi = ActionCallApi.CreateWard.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = body
                };
                _logCallApiV2Service.Add(log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                log.ModifyAt = DateTime.Now;
                _logCallApiV2Service.Update(log);
                DefaultResponce lst = JsonConvert.DeserializeObject<DefaultResponce>(json);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public PaymentLoanById.OutPut GetPaymentLoanById(int loanId)
        {
            var url = _configuration["AppSettings:LMS"] + String.Format(Constants.GetPaymentById, loanId);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            PaymentLoanById.OutPut result = JsonConvert.DeserializeObject<PaymentLoanById.OutPut>(response.Content);
            return result;
        }

        public DeferredPaymentLms.Output DeferredPayment(DeferredPaymentLms.Input param)
        {
            var url = _configuration["AppSettings:LMS"] + Constants.DeferredPayment;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(param);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            DeferredPaymentLms.Output result = JsonConvert.DeserializeObject<DeferredPaymentLms.Output>(response.Content);
            return result;
        }

    }
}
