﻿using System.Threading.Tasks;

namespace LOS.Services.Services.LoanBriefNote
{
    public interface ILoanBriefNoteV2Service
    {
        Task<bool> Add(DAL.EntityFramework.LoanBriefNote entity);
    }
}