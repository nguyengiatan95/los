﻿using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services.LoanBriefNote
{
    public class LoanBriefNoteV2Service : ILoanBriefNoteV2Service
    {
        public readonly ILoanBriefNoteRespository _loanBriefNoteRespository;
        public LoanBriefNoteV2Service(ILoanBriefNoteRespository loanBriefNoteRespository)
        {
            this._loanBriefNoteRespository = loanBriefNoteRespository;
        }

        public async Task<bool> Add(DAL.EntityFramework.LoanBriefNote entity)
        {
            return await _loanBriefNoteRespository.Add(entity);
        }
    }
}
