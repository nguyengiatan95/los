﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.DTOs.Loanbrief;
using LOS.DAL.Models;

namespace LOS.Services.Services.Loanbrief
{
    public interface ILoanbriefV2Service
    {
        Task<List<LoanAndRelationNotCancelView>> ListLoanAndRelationNotCancel(string phone);
        Task<LoanBriefBasicInfo> GetBasicInfo(int loanbriefId);
        Task<bool> CancelEsign(int loanBriefId);
        Task<bool> CancelExceptions(int loanBriefId);
        Task<LoanBriefDisbursementContractView> GetDisbursementContract(int provinceId, int loanBriefId, int productId, int topup, DateTime fromDate,
            DateTime toDate, string search, int userId, int? groupId, int page, int pageSize);
        Task<bool> CheckLoanBriefTopupProcessing(int loanBriefId);
        Task<bool> UpdateBoundTelesaleId(int loanBriefId, int telesaleId);
        Task<bool> UpdateDevice(int loanBriefId, int deviceStatus, string contractGinno, string deviceId);
        Task<bool> UpdateTypeLoanSupportByList(List<int> lstLoanBriefId);
        Task<List<LoanBriefPrematureInterest>> GetListLoanBriefByPhoneOrNationalCard(string text_search);
        Task<bool> CloseLoan(int loanbriefId, DateTime finishAt);
        Task<bool> UpdateTypeLoanSupport(int loanbriefId, int typeLoanSupport);
        Task<bool> RestoreTopup(int loanBriefId);
        Task<CheckTopupResult> CheckCanTopup(int loanBriefId);
    }
}