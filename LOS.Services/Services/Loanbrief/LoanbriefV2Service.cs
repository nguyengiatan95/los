﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.DTOs.Loanbrief;
using LOS.DAL.Models;
using LOS.DAL.Respository;
using LOS.Services.Services.LMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services.Loanbrief
{
    public class LoanbriefV2Service : ILoanbriefV2Service
    {
        public readonly ILoanbriefRepository _loanbriefRepository;
        public readonly ILoanbriefRelationshipRepository _loanbriefRelationshipRepository;
        public readonly ILMSV2Service _lmsService;
        public LoanbriefV2Service(ILoanbriefRepository loanbriefRepository,
            ILoanbriefRelationshipRepository loanbriefRelationshipRepository,
            ILMSV2Service lmsService)
        {
            this._loanbriefRepository = loanbriefRepository;
            this._loanbriefRelationshipRepository = loanbriefRelationshipRepository;
            this._lmsService = lmsService;
        }

        public async Task<List<LoanAndRelationNotCancelView>> ListLoanAndRelationNotCancel(string phone)
        {
            var resultData = new List<LoanAndRelationNotCancelView>();
            //var taskLoanData = _loanbriefRepository.ListLoanbriefNotCancel(phone);
            //var taskRelationshipData = _loanbriefRelationshipRepository.ListRelationshipInfoLoanNotCancel(phone);
            var taskLoanData = _loanbriefRepository.AllLoanByPhone(phone);
            var taskRelationshipData = _loanbriefRelationshipRepository.AllListRelationshipInfoLoan(phone);
            Task.WaitAll(taskLoanData, taskRelationshipData);
            if (taskLoanData.Result != null && taskLoanData.Result.Count > 0)
            {
                foreach (var item in taskLoanData.Result)
                {
                    var data = new LoanAndRelationNotCancelView()
                    {
                        LoanbriefId = item.LoanBriefId,
                        FullName = item.FullName,
                        Phone = item.Phone,
                        RelationshipName = "Khách vay"
                    };
                    if (item.Status != null && Enum.IsDefined(typeof(EnumLoanStatus), item.Status))
                        data.LoanStatus = Description.GetDescription((EnumLoanStatus)item.Status);

                    resultData.Add(data);
                }
            }
            if (taskRelationshipData.Result != null && taskRelationshipData.Result.Count > 0)
            {
                foreach (var item in taskRelationshipData.Result)
                {
                    if (item.LoanBriefId > 0)
                    {
                        var data = new LoanAndRelationNotCancelView()
                        {
                            LoanbriefId = item.LoanBriefId.GetValueOrDefault(0),
                            FullName = item.FullName,
                            Phone = item.Phone,
                        };
                        if (item.Status != null && Enum.IsDefined(typeof(EnumLoanStatus), item.Status))
                            data.LoanStatus = Description.GetDescription((EnumLoanStatus)item.Status);
                        if (item.TypeRelationship != null && Enum.IsDefined(typeof(EnumRelativeFamily), item.TypeRelationship))
                            data.RelationshipName = Description.GetDescription((EnumRelativeFamily)item.TypeRelationship);

                        resultData.Add(data);
                    }
                }

            }
            return resultData;
        }

        public async Task<LoanBriefBasicInfo> GetBasicInfo(int loanbriefId)
        {
            return await _loanbriefRepository.GetBasicInfo(loanbriefId);
        }

        public async Task<bool> CancelEsign(int loanBriefId)
        {
            return await _loanbriefRepository.CancelEsign(loanBriefId);
        }

        public async Task<bool> CancelExceptions(int loanBriefId)
        {
            return await _loanbriefRepository.CancelExceptions(loanBriefId);
        }
        public async Task<LoanBriefDisbursementContractView> GetDisbursementContract(int provinceId, int loanBriefId, int productId, int topup, DateTime fromDate,
            DateTime toDate, string search, int userId, int? groupId, int page, int pageSize)
        {
            return await _loanbriefRepository.GetDisbursementContract(provinceId, loanBriefId, productId, topup, fromDate, toDate, search, userId, groupId, page, pageSize);
        }

        public async Task<bool> CheckLoanBriefTopupProcessing(int loanBriefId)
        {
            return await _loanbriefRepository.CheckLoanBriefTopupProcessing(loanBriefId);
        }
        public async Task<bool> UpdateBoundTelesaleId(int loanBriefId, int telesaleId)
        {
            return await _loanbriefRepository.UpdateBoundTelesaleId(loanBriefId, telesaleId);
        }
        public async Task<bool> UpdateDevice(int loanBriefId, int deviceStatus, string contractGinno, string deviceId)
        {
            return await _loanbriefRepository.UpdateDevice(loanBriefId, deviceStatus, contractGinno, deviceId);
        }
        public async Task<bool> UpdateTypeLoanSupportByList(List<int> lstLoanBriefId)
        {
            return await _loanbriefRepository.UpdateTypeLoanSupportByList(lstLoanBriefId);
        }
        public async Task<List<LoanBriefPrematureInterest>> GetListLoanBriefByPhoneOrNationalCard(string text_search)
        {
            return await _loanbriefRepository.GetListLoanBriefByPhoneOrNationalCard(text_search);
        }
        public async Task<bool> CloseLoan(int loanbriefId, DateTime finishAt)
        {
            return await _loanbriefRepository.CloseLoan(loanbriefId, finishAt);
        }
        public async Task<bool> UpdateTypeLoanSupport(int loanbriefId, int typeLoanSupport)
        {
            return await _loanbriefRepository.UpdateTypeLoanSupport(loanbriefId, typeLoanSupport);
        }
        public async Task<bool> RestoreTopup(int loanBriefId)
        {
            return await _loanbriefRepository.RestoreTopup(loanBriefId);
        }
        public async Task<CheckTopupResult> CheckCanTopup(int loanBriefId)
        {
            var result = new CheckTopupResult();
            //result.IsCanTopup = true;
            //result.CurrentDebt = 100000;
            //return result;
            var loanInfo = await _loanbriefRepository.GetLoanForCheckTopup(loanBriefId);
            if (loanInfo != null && loanInfo.LoanbriefId > 0)
            {
                if (loanInfo.LoanStatus == (int)EnumLoanStatus.DISBURSED)
                {
                    if (loanInfo.TypeRemarking == (int)EnumTypeRemarketing.IsTopUp)
                    {
                        //Kiểm tra điều kiện topup lần thứ 2
                        //Đơn gốc phải đóng hd
                        if (loanInfo.ParentLoanStatus == (int)EnumLoanStatus.FINISH)
                        {
                            //Lấy lịch trả nợ của KH
                            var paymentResult = _lmsService.DeferredPayment(new DeferredPaymentLms.Input()
                            {
                                CustomerName = !loanInfo.FullName.IsNormalized() ? loanInfo.FullName.Normalize() : loanInfo.FullName,
                                NumberCard = loanInfo.NationalCard
                            });
                            if (paymentResult != null && paymentResult.Data != null && paymentResult.Data.LstLoanCustomer != null)
                            {
                                var curentDebt = 0l;
                                foreach (var item in paymentResult.Data.LstLoanCustomer)
                                {
                                    curentDebt += item.TotalMoneyCurrent;
                                    if (item.LstPaymentCustomer != null && item.LstPaymentCustomer.Count() > 0
                                        && item.LstPaymentCustomer.Any(x=>x.CountDay > 30))
                                    {
                                        result.Message = "Khách hàng có kỳ thanh toán trả chậm quá 30 ngày";
                                        return result;
                                    }
                                    
                                }
                                result.IsTopupOnTopup = true;
                                result.IsCanTopup = true;
                                result.CurrentDebt = curentDebt;
                            }
                            else
                            {
                                result.Message = "Không tìm thấy lịch trả nợ của KH";
                            }                      
                        }
                    }
                    else
                    {
                        //Kiểm tra điều kiện topup lần 1
                        var resulPayment = _lmsService.GetPaymentLoanById(loanInfo.LmsLoanId.Value);
                        if (resulPayment != null && resulPayment.Status == 1 && resulPayment.Data != null && resulPayment.Data.Loan != null)
                        {
                            result.CurrentDebt = resulPayment.Data.Loan.TotalMoneyCurrent;
                            result.IsCanTopup =
                                (resulPayment.Data.Loan.TopUp == 1);
                        }
                        else
                        {
                            result.Message = resulPayment != null?  resulPayment.Messages.FirstOrDefault(): "Xảy ra lỗi khi lấy thông tin lịch sử thanh toán của KH";
                        }
                    }
                }
                else
                {
                    result.Message = "Đơn vay kiểm tra không phải đơn Đang vay";
                }
            }
            else
            {
                result.Message = "Không tìm thấy thông tin đơn vay";
            }
            return result;
        }
    }
}
