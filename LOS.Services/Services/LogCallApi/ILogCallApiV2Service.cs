﻿using System.Threading.Tasks;

namespace LOS.Services.Services.LogCallApi
{
    public interface ILogCallApiV2Service
    {
        Task<bool> Add(DAL.EntityFramework.LogCallApi entity);
        Task<bool> Update(DAL.EntityFramework.LogCallApi entity);
    }
}