﻿using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services.LogCallApi
{
    public class LogCallApiV2Service : ILogCallApiV2Service
    {
        public readonly ILogCallApiRespository _logCallApiRespository;
        public LogCallApiV2Service(ILogCallApiRespository logCallApiRespository)
        {
            this._logCallApiRespository = logCallApiRespository;
        }

        public async Task<bool> Add(DAL.EntityFramework.LogCallApi entity)
        {
            return await _logCallApiRespository.Add(entity);
        }

        public async Task<bool> Update(DAL.EntityFramework.LogCallApi entity)
        {
            return await _logCallApiRespository.Update(entity);
        }
    }
}
