﻿using LOS.DAL.EntityFramework;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public interface ILogCallApiCiscoService
    {
        Task<bool> Add(LogCallApiCisco entity);
    }
}