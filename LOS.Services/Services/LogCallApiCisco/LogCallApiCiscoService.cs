﻿using LOS.DAL.EntityFramework;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public class LogCallApiCiscoService : ILogCallApiCiscoService
    {
        public readonly ILogCallApiCiscoRepository _logCallApiCiscoRepository;
        public LogCallApiCiscoService(ILogCallApiCiscoRepository logCallApiCiscoRepository)
        {
            this._logCallApiCiscoRepository = logCallApiCiscoRepository;
        }

        public async Task<bool> Add(LogCallApiCisco entity)
        {
            return await _logCallApiCiscoRepository.Add(entity);
        }
    }
}
