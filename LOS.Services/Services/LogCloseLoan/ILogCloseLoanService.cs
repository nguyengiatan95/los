﻿using LOS.DAL.EntityFramework;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public interface ILogCloseLoanService
    {
        int Add(LogCloseLoan entity);
        bool Update(LogCloseLoan entity);
        bool CheckLoanProcess(int loanBriefId);
    }
}