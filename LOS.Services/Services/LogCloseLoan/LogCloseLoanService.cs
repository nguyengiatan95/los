﻿using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public class LogCloseLoanService : ILogCloseLoanService
    {
        public readonly ILogCloseLoanRespository _logCloseLoanRespository;
        public LogCloseLoanService(ILogCloseLoanRespository logCloseLoanRespository)
        {
            this._logCloseLoanRespository = logCloseLoanRespository;
        }

        public int Add(DAL.EntityFramework.LogCloseLoan entity)
        {
            return _logCloseLoanRespository.Add(entity);
        }

        public bool Update(DAL.EntityFramework.LogCloseLoan entity)
        {
            return _logCloseLoanRespository.Update(entity);
        }
        public bool CheckLoanProcess(int loanBriefId)
        {
            return _logCloseLoanRespository.CheckLoanProcess(loanBriefId);
        }
    }
}
