﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public interface ILogDistributionUserService
    {
        Task<List<LogDistributionUserDetail>> GetLogDistributionUser(int typeDistribution, List<int> listLoanbriefId);
        Task<bool> Add(LogDistributionUser entity);
    }
}