﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public class LogDistributionUserService : ILogDistributionUserService
    {
        public readonly ILogDistributionUserRepository _logDistributionUserRepository;
        public LogDistributionUserService(ILogDistributionUserRepository logDistributionUserRepository)
        {
            this._logDistributionUserRepository = logDistributionUserRepository;
        }

        public async Task<List<LogDistributionUserDetail>> GetLogDistributionUser(int typeDistribution, List<int> listLoanbriefId)
        {
            return await _logDistributionUserRepository.GetLogDistributionUser(typeDistribution, listLoanbriefId);
        }

        public async Task<bool> Add(LogDistributionUser entity)
        {
            return await _logDistributionUserRepository.Add(entity);
        }
    }
}
