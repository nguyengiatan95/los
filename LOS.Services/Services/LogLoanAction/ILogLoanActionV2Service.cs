﻿using LOS.DAL.EntityFramework;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public interface ILogLoanActionV2Service
    {
        Task<bool> Add(LogLoanAction entity);
    }
}