﻿using LOS.DAL.EntityFramework;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public class LogLoanActionV2Service : ILogLoanActionV2Service
    {
        public readonly ILogLoanActionRespository _logLoanActionRespository;
        public LogLoanActionV2Service(ILogLoanActionRespository logLoanActionRespository)
        {
            this._logLoanActionRespository = logLoanActionRespository;
        }
        public async Task<bool> Add(LogLoanAction entity)
        {
            return await _logLoanActionRespository.Add(entity);
        }
    }
}
