﻿using LOS.DAL.EntityFramework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.Services.Services.LogLoanInfoAI
{
    public interface ILogLoanInfoAIV2Service
    {
        Task<bool> Add(LogLoanInfoAi entity);
        Task<bool> AddRange(List<LogLoanInfoAi> entities);
    }
}