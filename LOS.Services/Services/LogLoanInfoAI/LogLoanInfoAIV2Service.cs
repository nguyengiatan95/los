﻿using LOS.DAL.EntityFramework;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services.LogLoanInfoAI
{
    public class LogLoanInfoAIV2Service : ILogLoanInfoAIV2Service
    {
        public readonly ILogLoanInfoAIRespository _logLoanInfoAIRespository;
        public LogLoanInfoAIV2Service(ILogLoanInfoAIRespository logLoanInfoAIRespository)
        {
            this._logLoanInfoAIRespository = logLoanInfoAIRespository;
        }

        public async Task<bool> Add(LogLoanInfoAi entity)
        {
            return await _logLoanInfoAIRespository.Add(entity);
        }
        public async Task<bool> AddRange(List<LogLoanInfoAi> entities)
        {
            return await _logLoanInfoAIRespository.AddRange(entities);
        }
    }
}
