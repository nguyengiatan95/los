﻿using LOS.DAL.EntityFramework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public interface ILogReLoanBriefService
    {
        Task<bool> Add(LogReLoanbrief entity);
        Task<bool> AddRange(List<LogReLoanbrief> entities);
    }
}