﻿using LOS.DAL.EntityFramework;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public class LogReLoanBriefService : ILogReLoanBriefService
    {
        public readonly ILogReLoanBriefRepository _logReLoanBriefRepository;
        public LogReLoanBriefService(ILogReLoanBriefRepository logReLoanBriefRepository)
        {
            this._logReLoanBriefRepository = logReLoanBriefRepository;
        }
        public async Task<bool> Add(LogReLoanbrief entity)
        {
            return await _logReLoanBriefRepository.Add(entity);
        }
        public async Task<bool> AddRange(List<LogReLoanbrief> entities)
        {
            return await _logReLoanBriefRepository.AddRange(entities);
        }
    }
}
