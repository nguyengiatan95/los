﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LOS.DAL.DTOs;

namespace LOS.Services.Services.Province
{
    public interface IProvinceService
    {
        Task<ProvinceDetail> GetById(int id);
        Task<List<ProvinceDetail>> GetAll();
        List<ProvinceDetail> Search(string searchName, int status, int page, int pageSize, ref int recordsTotal);
        Task<bool> UpdateIsApplyProvince(int provinceId, int isApply);
        Task<List<ProvinceDetail>> GetProvinceSupport();
    }
}