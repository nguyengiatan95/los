﻿using LOS.DAL.DTOs;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services.Province
{
    public class ProvinceService : IProvinceService
    {
        public readonly IProvinceRepository _provinceRepository;
        public ProvinceService(IProvinceRepository provinceRepository)
        {
            this._provinceRepository = provinceRepository;
        }

        public async Task<ProvinceDetail> GetById(int id)
        {
            return await _provinceRepository.GetById(id);
        }

        public async Task<List<ProvinceDetail>> GetAll()
        {
            return await _provinceRepository.GetAll();
        }

        public List<ProvinceDetail> Search(string searchName, int status, int page, int pageSize, ref int recordsTotal)
        {
            return _provinceRepository.Search(searchName, status, page, pageSize, ref recordsTotal);
        }
        public async Task<bool> UpdateIsApplyProvince(int provinceId, int isApply)
        {
            return await _provinceRepository.UpdateIsApplyProvince(provinceId, isApply);
        }

        public async Task<List<ProvinceDetail>> GetProvinceSupport()
        {
            return await _provinceRepository.GetProvinceSupport();
        }
    }
}
