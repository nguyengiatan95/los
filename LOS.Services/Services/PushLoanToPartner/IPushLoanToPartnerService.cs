﻿using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public interface IPushLoanToPartnerService
    {
        Task<bool> UpdateStatusDisbursement(int id);
    }
}