﻿using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public class PushLoanToPartnerService : IPushLoanToPartnerService
    {
        public readonly IPushLoanToPartnerRespository _pushLoanToPartnerRespository;
        public PushLoanToPartnerService(IPushLoanToPartnerRespository pushLoanToPartnerRespository)
        {
            this._pushLoanToPartnerRespository = pushLoanToPartnerRespository;
        }
        public async Task<bool> UpdateStatusDisbursement(int id)
        {
            return await _pushLoanToPartnerRespository.UpdateStatusDisbursement(id);
        }
    }
}
