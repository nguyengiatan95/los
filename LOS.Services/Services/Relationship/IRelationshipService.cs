﻿using LOS.DAL.DTOs.LoanbriefRelationship;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public interface IRelationshipService
    {
        Task<RelationshipDetail> GetById(int id);
    }
}