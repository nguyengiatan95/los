﻿using LOS.DAL.DTOs.LoanbriefRelationship;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public class RelationshipService : IRelationshipService
    {
        public readonly ILoanbriefRelationshipRepository _loanbriefRelationshipRepository;
        public RelationshipService(ILoanbriefRelationshipRepository loanbriefRelationshipRepository)
        {
            this._loanbriefRelationshipRepository = loanbriefRelationshipRepository;
        }
        public async Task<RelationshipDetail> GetById(int id)
        {
            return await _loanbriefRelationshipRepository.GetById(id);
        }
    }
}
