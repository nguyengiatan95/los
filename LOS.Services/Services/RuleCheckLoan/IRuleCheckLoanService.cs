﻿using LOS.DAL.EntityFramework;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public interface IRuleCheckLoanService
    {
        Task<bool> AddOrUpdate(RuleCheckLoan entity);
    }
}