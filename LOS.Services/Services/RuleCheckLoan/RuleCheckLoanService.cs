﻿using LOS.DAL.EntityFramework;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public class RuleCheckLoanService : IRuleCheckLoanService
    {
        public readonly IRuleCheckLoanRepository _ruleCheckLoanRepository;
        public RuleCheckLoanService(IRuleCheckLoanRepository ruleCheckLoanRepository)
        {
            this._ruleCheckLoanRepository = ruleCheckLoanRepository;
        }
        public async Task<bool> AddOrUpdate(RuleCheckLoan entity)
        {
            return await _ruleCheckLoanRepository.AddOrUpdate(entity);
        }
    }
}
