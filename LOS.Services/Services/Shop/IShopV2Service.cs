﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public interface IShopV2Service
    {
        Task<ShopDetail> GetShopSupport(int productCreditId, int districtId, int? wardId);
    }
}