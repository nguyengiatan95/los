﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public class ShopV2Service : IShopV2Service
    {
        private readonly IShopRepository _shopRepository;
        public ShopV2Service(IShopRepository shopRepository)
        {
            this._shopRepository = shopRepository;
        }

        public Task<ShopDetail> GetShopSupport(int productCreditId, int districtId, int? wardId)
        {
            return _shopRepository.GetShopSupport(productCreditId, districtId, wardId);
        }
    }
}
