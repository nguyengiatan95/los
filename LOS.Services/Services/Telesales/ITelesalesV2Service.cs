﻿using LOS.DAL.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.Services.Services.Telesales
{
    public interface ITelesalesV2Service
    {
        Task<List<TeamTelesaleDetail>> GetTeamEnable();
        Task<List<UserTelesalesDetail>> GetTelesalesByTeam(int team);
        Task<List<UserTelesalesDetail>> GetTelesalesEnable();
    }
}