﻿using LOS.DAL.DTOs;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services.Telesales
{
    public class TelesalesV2Service : ITelesalesV2Service
    {
        public readonly ITelesalesRepository _telesalesRepository;
        public TelesalesV2Service(ITelesalesRepository telesalesRepository)
        {
            this._telesalesRepository = telesalesRepository;
        }
        public async Task<List<TeamTelesaleDetail>> GetTeamEnable()
        {
            return await _telesalesRepository.GetTeamEnable();
        }

        public async Task<List<UserTelesalesDetail>> GetTelesalesByTeam(int team)
        {
            return await _telesalesRepository.GetTelesalesByTeam(team);
        }
        public async Task<List<UserTelesalesDetail>> GetTelesalesEnable()
        {
            return await _telesalesRepository.GetTelesalesEnable();
        }
    }
}
