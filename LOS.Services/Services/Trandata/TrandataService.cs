﻿using LOS.Common.Helpers;
using LOS.DAL.Models.Request.Trandata;
using LOS.DAL.Models.Response.Trandata;
using LOS.DAL.Object.Appsettings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.Services.Services.Trandata
{
    public class TrandataService : ITrandataService
    {
        protected IConfiguration _baseConfig;
        private readonly TrandataSettings _trandataSettings;
        public TrandataService(IConfiguration configuration, IOptions<TrandataSettings> trandataSettingAccessor)
        {
            _baseConfig = configuration;
            _trandataSettings = trandataSettingAccessor.Value;
        }


        public string Authentication()
        {
            try
            {
                var url = $"{_trandataSettings.Domain}{ConstantHelpers.TRANDATA_LOGIN_ENDPOINT}";
                var client = new RestClient(url);
                var requestData = new AuthenticationRequest()
                {
                    UserName = _trandataSettings.Username,
                    Password = _trandataSettings.Password
                };
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddParameter("application/json", Newtonsoft.Json.JsonConvert.SerializeObject(requestData), ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                AuthenticationReponse result = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthenticationReponse>(jsonResponse);
                if (result != null && result.Data != null && !string.IsNullOrEmpty(result.Data.SessionToken))
                    return result.Data.SessionToken;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            return string.Empty;
        }
    }
}
