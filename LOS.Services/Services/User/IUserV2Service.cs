﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public interface IUserV2Service
    {
        Task<List<UserBasicDetail>> GetUserBasicByGroup(List<int> listGroupId);
        Task<UserMobileCiscoDetail> GetUserMobileCisco(int userId);
        Task<bool> SaveConfigCiscoMobile(UserMobileCisco entity);
        Task<int> CreateConfigCiscoMobile(UserMobileCisco entity);
        Task<UserBasicDetail> GetById(int userId);
    }
}