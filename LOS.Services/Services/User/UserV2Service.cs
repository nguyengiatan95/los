﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services
{
    public class UserV2Service : IUserV2Service
    {
        private readonly IUserRepository _userRepository;
        public UserV2Service(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }
        public async Task<List<UserBasicDetail>> GetUserBasicByGroup(List<int> listGroupId)
        {
            return await _userRepository.GetUserBasicByGroup(listGroupId);
        }

        public async Task<UserMobileCiscoDetail> GetUserMobileCisco(int userId)
        {
            return await _userRepository.GetUserMobileCisco(userId);
        }

        public async Task<bool> SaveConfigCiscoMobile(UserMobileCisco entity)
        {
            return await _userRepository.SaveConfigCiscoMobile(entity);
        }

        public async Task<int> CreateConfigCiscoMobile(UserMobileCisco entity)
        {
            return await _userRepository.CreateConfigCiscoMobile(entity);
        }
        public async Task<UserBasicDetail> GetById(int userId)
        {
            return await _userRepository.GetById(userId);
        }
    }
}
