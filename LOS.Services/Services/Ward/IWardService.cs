﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services.Ward
{
    public interface IWardService
    {
        List<WardDetail> Search(string searchName, int districtId, int page, int pageSize, ref int recordsTotal);
        Task<bool> Add(DAL.EntityFramework.Ward entity);
        Task<bool> Update(DAL.EntityFramework.Ward entity);
        Task<bool> CheckWard(string name, int districtId);
    }
}
