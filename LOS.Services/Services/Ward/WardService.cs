﻿using LOS.DAL.DTOs;
using LOS.DAL.Respository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Services.Services.Ward
{
    public class WardService : IWardService
    {
        public readonly IWardRepository _wardRepository;
        public WardService(IWardRepository wardRepository)
        {
            this._wardRepository = wardRepository;
        }
        public List<WardDetail> Search(string searchName, int districtId, int page, int pageSize, ref int recordsTotal) 
        {
            return _wardRepository.Search(searchName, districtId, page, pageSize, ref recordsTotal);
        }

        public async Task<bool> Add(DAL.EntityFramework.Ward entity)
        {
            return await _wardRepository.Add(entity);
        }
        public async Task<bool> Update(DAL.EntityFramework.Ward entity)
        {
            return await _wardRepository.Update(entity);
        }

        public async Task<bool> CheckWard(string name, int districtId)
        {
            return await _wardRepository.CheckWard(name, districtId);
        }
    }
}
