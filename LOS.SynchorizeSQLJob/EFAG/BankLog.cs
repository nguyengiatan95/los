﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class BankLog
    {
        public int BankLogId { get; set; }
        public int? ShopId { get; set; }
        public int? LoanCreditId { get; set; }
        public decimal? CustomerInsFee { get; set; }
        public decimal? LenderInsFee { get; set; }
        public string FromBank { get; set; }
        public string BankCode { get; set; }
        public string TransactionId { get; set; }
        public string TransactionType { get; set; }
        public string Data { get; set; }
        public decimal? Money { get; set; }
        public string ResponseCode { get; set; }
        public string Response { get; set; }
        public string BankRef { get; set; }
        public string VerifyTransactionId { get; set; }
        public string VerifyResponseCode { get; set; }
        public string VerifyResponse { get; set; }
        public string VerifyBankRef { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? SyncCount { get; set; }
        public string SyncResponseCode { get; set; }
        public bool? HasResult { get; set; }
        public string CallbackUrl { get; set; }
    }
}
