﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class Comission
    {
        public int ComissionId { get; set; }
        public string Collaborator { get; set; }
        public decimal? FromMoney { get; set; }
        public decimal? ToMoney { get; set; }
        public double? Comission1 { get; set; }
        public double? ExtraComission { get; set; }
    }
}
