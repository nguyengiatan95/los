﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class DataTest
    {
        public int Id { get; set; }
        public DateTime ForDate { get; set; }
        public string Name { get; set; }
        public string StrLoanTime { get; set; }
        public string ProductName { get; set; }
        public string ShopName { get; set; }
        public long? TotalMoney { get; set; }
        public int? NumberPayment { get; set; }
        public long? Salary { get; set; }
        public string ReceiveYourInCome { get; set; }
        public long? TotalMoneyDisbursement { get; set; }
    }
}
