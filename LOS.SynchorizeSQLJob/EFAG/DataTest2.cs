﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class DataTest2
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public string CityNameRegistration { get; set; }
        public string DistrictNameRegistration { get; set; }
        public string WardNameRegistration { get; set; }
        public DateTime? BirthDay { get; set; }
        public string JobName { get; set; }
        public string Gender { get; set; }
        public string CompanyName { get; set; }
        public string AddressCompany { get; set; }
        public int? LoanCreditId { get; set; }
        public int? LoanId { get; set; }
        public int? LoanCodeId { get; set; }
        public string ShopAdvisory { get; set; }
        public string ProductName { get; set; }
        public long? Salary { get; set; }
        public long? TotalMoneyNeedBorrowing { get; set; }
        public long? TotalMoneyDisbursement { get; set; }
        public int? LoanTime { get; set; }
        public long? TotalMoneyPaid { get; set; }
        public int? NumberPaymentPaid { get; set; }
        public string StrRateType { get; set; }
        public DateTime? FromDateDisbursement { get; set; }
        public int? NumberDayLate { get; set; }
        public string InfoCic { get; set; }
        public bool? ThamDinhNha { get; set; }
        public bool? ThamDinhCongty { get; set; }
        public string StrStatus { get; set; }
        public string JobDescription { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public string ShopNameDisbursement { get; set; }
        public string TypeOfOwnershipName { get; set; }
        public string ThucDiaNha { get; set; }
        public string ThucDiaCongTy { get; set; }
        public string ThucDiaHub { get; set; }
        public string ApproverLoanCredit { get; set; }
    }
}
