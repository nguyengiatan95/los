﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class DataTest3
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public int? ShopId { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public float? Rate { get; set; }
        public byte? RateType { get; set; }
        public DateTime? NextDate { get; set; }
        public int? NumberDayDelay { get; set; }
        public DateTime? NextDatefuture { get; set; }
        public int? Status { get; set; }
        public long? InterestMoney { get; set; }
        public long? TotalMoney { get; set; }
        public long? OtherMoney { get; set; }
        public int? MonthDate { get; set; }
        public int? YearDate { get; set; }
    }
}
