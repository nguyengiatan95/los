﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class DataTest4
    {
        public int Id { get; set; }
        public DateTime? ForDate { get; set; }
        public int? LoanCreditId { get; set; }
        public int? LoanCodeId { get; set; }
        public long? TotalMoneyNeedPay { get; set; }
        public int? ShopId { get; set; }
        public int? LoanId { get; set; }
        public long? TotalMoneyCurrent { get; set; }
    }
}
