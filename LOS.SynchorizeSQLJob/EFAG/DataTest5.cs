﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class DataTest5
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CodeId { get; set; }
        public string CustomerName { get; set; }
        public string CardNumber { get; set; }
        public string StrGender { get; set; }
        public string ProductName { get; set; }
        public DateTime? Birthday { get; set; }
        public string CustomerPhone { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string CustomerAddress { get; set; }
        public string CityNameHouseHold { get; set; }
        public string DistrictNameHouseHold { get; set; }
        public string AddressHouseHold { get; set; }
        public string CompanyName { get; set; }
        public string AddressCompany { get; set; }
        public string CompanyPhone { get; set; }
        public string PhoneFamily { get; set; }
        public string RelativeFamilyName { get; set; }
        public string FullNameFamily { get; set; }
        public DateTime? FromDate { get; set; }
        public long? TotalMoney { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public DateTime? NextDate { get; set; }
        public DateTime? LastDateOfPay { get; set; }
        public int? NumberDayDelay { get; set; }
        public int? NumberPeriodsDelay { get; set; }
        public long? TotalMoneyToPaid { get; set; }
        public DateTime? ToDate { get; set; }
        public long? MoneyCloseLoan { get; set; }
    }
}
