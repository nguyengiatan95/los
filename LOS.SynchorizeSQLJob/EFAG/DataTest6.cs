﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class DataTest6
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public long? TotalMoney { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public long? TotalMoneyCurrent30 { get; set; }
        public DateTime? NgayGiaiNgan { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string StrRate { get; set; }
        public long? MoneyInterest { get; set; }
        public long? MoneyOriginal { get; set; }
        public DateTime? PayDate { get; set; }
        public bool? Status { get; set; }
        public int? RateType { get; set; }
        public int? LoanId { get; set; }
    }
}
