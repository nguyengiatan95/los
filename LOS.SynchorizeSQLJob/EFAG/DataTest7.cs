﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class DataTest7
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public string HubName { get; set; }
        public string EmployeesName { get; set; }
        public string CustomerName { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public DateTime? Birthday { get; set; }
        public int? Gender { get; set; }
        public string JobName { get; set; }
        public string CompanyName { get; set; }
        public string AddressCompany { get; set; }
        public string Street { get; set; }
        public string TypeOfOwnershipName { get; set; }
        public long? Salary { get; set; }
        public long? TotalMoneyFirst { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? FromDate { get; set; }
        public string CapPheDuyet { get; set; }
        public long? TotalMoneyExpertise { get; set; }
        public long? TotalMoneyExpertiseLast { get; set; }
        public string ProductName { get; set; }
        public long? TotalMoneyDis { get; set; }
        public long? TotalMoneyToPaid { get; set; }
        public int? NumberDelayMax { get; set; }
        public int? Status { get; set; }
    }
}
