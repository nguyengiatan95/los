﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class LenderPayment
    {
        public int LenderPaymentId { get; set; }
        public int? ShopId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public decimal? LastBalance { get; set; }
        public decimal? NeedToPay { get; set; }
        public decimal? TotalMoney { get; set; }
        public decimal? PricipalMoney { get; set; }
        public decimal? InterestMoney { get; set; }
        public decimal? FineMoney { get; set; }
        public decimal? CustomerInsuranceMoney { get; set; }
        public decimal? CustomerInsurancePaid { get; set; }
        public decimal? LenderInsuranceMoney { get; set; }
        public decimal? LenderInsurancePaid { get; set; }
        public int? Status { get; set; }
        public string InsurancePaidLoans { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? LenderCareState { get; set; }
        public int? LenderCareId { get; set; }
        public DateTime? ConfirmedDate { get; set; }
        public int? AccountantState { get; set; }
        public int? AccountantId { get; set; }
        public DateTime? TransferedDate { get; set; }
        public decimal? DiferenceMoney { get; set; }
        public decimal? LenderPaid { get; set; }
        public decimal? TimaPaid { get; set; }
    }
}
