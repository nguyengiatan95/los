﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class LinkageBank
    {
        public int LinkageBankId { get; set; }
        public int? ShopId { get; set; }
        public string BankCode { get; set; }
        public string WalletId { get; set; }
        public string OwnerName { get; set; }
        public string CardNumber { get; set; }
        public string AccountNumber { get; set; }
        public string IdCardNumber { get; set; }
        public string Phone { get; set; }
        public int? Status { get; set; }
        public bool? IsMainBank { get; set; }
    }
}
