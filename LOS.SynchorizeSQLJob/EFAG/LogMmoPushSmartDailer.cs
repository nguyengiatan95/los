﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class LogMmoPushSmartDailer
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CampaignDataId { get; set; }
        public int? CampaignId { get; set; }
        public bool? IsMmo { get; set; }
        public int? LoanCreditId { get; set; }
    }
}
