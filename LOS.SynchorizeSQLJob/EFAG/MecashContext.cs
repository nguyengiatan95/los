﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class MecashContext : DbContext
    {
        public MecashContext()
        {
        }

        public MecashContext(DbContextOptions<MecashContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BankLog> BankLog { get; set; }
        public virtual DbSet<Comission> Comission { get; set; }
        public virtual DbSet<DataTest> DataTest { get; set; }
        public virtual DbSet<DataTest2> DataTest2 { get; set; }
        public virtual DbSet<DataTest3> DataTest3 { get; set; }
        public virtual DbSet<DataTest4> DataTest4 { get; set; }
        public virtual DbSet<DataTest5> DataTest5 { get; set; }
        public virtual DbSet<DataTest6> DataTest6 { get; set; }
        public virtual DbSet<DataTest7> DataTest7 { get; set; }
        public virtual DbSet<LenderPayment> LenderPayment { get; set; }
        public virtual DbSet<LinkageBank> LinkageBank { get; set; }
        public virtual DbSet<LogMmoPushSmartDailer> LogMmoPushSmartDailer { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<PermissionHub> PermissionHub { get; set; }
        public virtual DbSet<ProcessingTime> ProcessingTime { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<TblAddressCounseling> TblAddressCounseling { get; set; }
        public virtual DbSet<TblAdsForm> TblAdsForm { get; set; }
        public virtual DbSet<TblAdsPage> TblAdsPage { get; set; }
        public virtual DbSet<TblAff> TblAff { get; set; }
        public virtual DbSet<TblAffLender> TblAffLender { get; set; }
        public virtual DbSet<TblAgencyRefuse> TblAgencyRefuse { get; set; }
        public virtual DbSet<TblAlarmDateLog> TblAlarmDateLog { get; set; }
        public virtual DbSet<TblArea> TblArea { get; set; }
        public virtual DbSet<TblAttCustomerCreditValidator> TblAttCustomerCreditValidator { get; set; }
        public virtual DbSet<TblAwardContract> TblAwardContract { get; set; }
        public virtual DbSet<TblBadDebReason> TblBadDebReason { get; set; }
        public virtual DbSet<TblBank> TblBank { get; set; }
        public virtual DbSet<TblBankCard> TblBankCard { get; set; }
        public virtual DbSet<TblBankCardShop> TblBankCardShop { get; set; }
        public virtual DbSet<TblBankConfig> TblBankConfig { get; set; }
        public virtual DbSet<TblBlackList> TblBlackList { get; set; }
        public virtual DbSet<TblBlackListLog> TblBlackListLog { get; set; }
        public virtual DbSet<TblBrand> TblBrand { get; set; }
        public virtual DbSet<TblCampaign> TblCampaign { get; set; }
        public virtual DbSet<TblCampaign2District> TblCampaign2District { get; set; }
        public virtual DbSet<TblCampaignBuilderInfo> TblCampaignBuilderInfo { get; set; }
        public virtual DbSet<TblCampaignGenUrl> TblCampaignGenUrl { get; set; }
        public virtual DbSet<TblCampaignUrlbuilder> TblCampaignUrlbuilder { get; set; }
        public virtual DbSet<TblCapital> TblCapital { get; set; }
        public virtual DbSet<TblCapitalExtra> TblCapitalExtra { get; set; }
        public virtual DbSet<TblCapitalMember> TblCapitalMember { get; set; }
        public virtual DbSet<TblCapitalUserOwner> TblCapitalUserOwner { get; set; }
        public virtual DbSet<TblCashBankCardByDate> TblCashBankCardByDate { get; set; }
        public virtual DbSet<TblCashByDate> TblCashByDate { get; set; }
        public virtual DbSet<TblCategory> TblCategory { get; set; }
        public virtual DbSet<TblCategoryProduct> TblCategoryProduct { get; set; }
        public virtual DbSet<TblCity> TblCity { get; set; }
        public virtual DbSet<TblCollaborators> TblCollaborators { get; set; }
        public virtual DbSet<TblComment> TblComment { get; set; }
        public virtual DbSet<TblCommentCredit> TblCommentCredit { get; set; }
        public virtual DbSet<TblCommentCreditReport> TblCommentCreditReport { get; set; }
        public virtual DbSet<TblCommentCustomerBad> TblCommentCustomerBad { get; set; }
        public virtual DbSet<TblCommentDebtPrompted> TblCommentDebtPrompted { get; set; }
        public virtual DbSet<TblCompany> TblCompany { get; set; }
        public virtual DbSet<TblConfigCoordinator> TblConfigCoordinator { get; set; }
        public virtual DbSet<TblConfigInterest> TblConfigInterest { get; set; }
        public virtual DbSet<TblConfigRole> TblConfigRole { get; set; }
        public virtual DbSet<TblConfigSmartDailer> TblConfigSmartDailer { get; set; }
        public virtual DbSet<TblConfigTimeToDecideWhoWillCollectLoan> TblConfigTimeToDecideWhoWillCollectLoan { get; set; }
        public virtual DbSet<TblConfigUserRemindDebtByLoanAndMonth> TblConfigUserRemindDebtByLoanAndMonth { get; set; }
        public virtual DbSet<TblCoordinatorCheckList> TblCoordinatorCheckList { get; set; }
        public virtual DbSet<TblCoordinatorCriteria> TblCoordinatorCriteria { get; set; }
        public virtual DbSet<TblCounselorsError> TblCounselorsError { get; set; }
        public virtual DbSet<TblCustomer> TblCustomer { get; set; }
        public virtual DbSet<TblCustomerBad> TblCustomerBad { get; set; }
        public virtual DbSet<TblCustomerBadDebitReport> TblCustomerBadDebitReport { get; set; }
        public virtual DbSet<TblCustomerCare> TblCustomerCare { get; set; }
        public virtual DbSet<TblCustomerCredit> TblCustomerCredit { get; set; }
        public virtual DbSet<TblCustomerCreditFiles> TblCustomerCreditFiles { get; set; }
        public virtual DbSet<TblCustomerFiles> TblCustomerFiles { get; set; }
        public virtual DbSet<TblCustomerOtp> TblCustomerOtp { get; set; }
        public virtual DbSet<TblCustomerUser> TblCustomerUser { get; set; }
        public virtual DbSet<TblCustomerWc> TblCustomerWc { get; set; }
        public virtual DbSet<TblDashBoardDebt> TblDashBoardDebt { get; set; }
        public virtual DbSet<TblDataCic> TblDataCic { get; set; }
        public virtual DbSet<TblDataPayoo> TblDataPayoo { get; set; }
        public virtual DbSet<TblDataSimViettel> TblDataSimViettel { get; set; }
        public virtual DbSet<TblDebit> TblDebit { get; set; }
        public virtual DbSet<TblDevice> TblDevice { get; set; }
        public virtual DbSet<TblDistrict> TblDistrict { get; set; }
        public virtual DbSet<TblDocument> TblDocument { get; set; }
        public virtual DbSet<TblDocumentCustomer> TblDocumentCustomer { get; set; }
        public virtual DbSet<TblDocumentLoanCredit> TblDocumentLoanCredit { get; set; }
        public virtual DbSet<TblDomain> TblDomain { get; set; }
        public virtual DbSet<TblExcessCashCustomer> TblExcessCashCustomer { get; set; }
        public virtual DbSet<TblExpectedPayment> TblExpectedPayment { get; set; }
        public virtual DbSet<TblExtension> TblExtension { get; set; }
        public virtual DbSet<TblExtensionCapital> TblExtensionCapital { get; set; }
        public virtual DbSet<TblFieldEvaluationReport> TblFieldEvaluationReport { get; set; }
        public virtual DbSet<TblFieldSaleManagement> TblFieldSaleManagement { get; set; }
        public virtual DbSet<TblFieldSurveyManagement> TblFieldSurveyManagement { get; set; }
        public virtual DbSet<TblFileManagement> TblFileManagement { get; set; }
        public virtual DbSet<TblGroup> TblGroup { get; set; }
        public virtual DbSet<TblHistory> TblHistory { get; set; }
        public virtual DbSet<TblHistoryMoneyShop> TblHistoryMoneyShop { get; set; }
        public virtual DbSet<TblHomeNetwork> TblHomeNetwork { get; set; }
        public virtual DbSet<TblIndividualBusiness> TblIndividualBusiness { get; set; }
        public virtual DbSet<TblInfoCustomerHub> TblInfoCustomerHub { get; set; }
        public virtual DbSet<TblInformationOfBusiness> TblInformationOfBusiness { get; set; }
        public virtual DbSet<TblInformationToReceiveSalary> TblInformationToReceiveSalary { get; set; }
        public virtual DbSet<TblInstallment> TblInstallment { get; set; }
        public virtual DbSet<TblInstallmentDebit> TblInstallmentDebit { get; set; }
        public virtual DbSet<TblInstallmentFiles> TblInstallmentFiles { get; set; }
        public virtual DbSet<TblInstallmentInterestExpected> TblInstallmentInterestExpected { get; set; }
        public virtual DbSet<TblInvestment> TblInvestment { get; set; }
        public virtual DbSet<TblJob> TblJob { get; set; }
        public virtual DbSet<TblKpicounselors> TblKpicounselors { get; set; }
        public virtual DbSet<TblLenderSpicesConfig> TblLenderSpicesConfig { get; set; }
        public virtual DbSet<TblLendingCertificateInformation> TblLendingCertificateInformation { get; set; }
        public virtual DbSet<TblLiquidation> TblLiquidation { get; set; }
        public virtual DbSet<TblLivingTime> TblLivingTime { get; set; }
        public virtual DbSet<TblLoan> TblLoan { get; set; }
        public virtual DbSet<TblLoanBadDebitReport> TblLoanBadDebitReport { get; set; }
        public virtual DbSet<TblLoanBadDebitReportDaily> TblLoanBadDebitReportDaily { get; set; }
        public virtual DbSet<TblLoanCredit> TblLoanCredit { get; set; }
        public virtual DbSet<TblLoanCreditHide> TblLoanCreditHide { get; set; }
        public virtual DbSet<TblLoanCreditOpenning> TblLoanCreditOpenning { get; set; }
        public virtual DbSet<TblLoanCreditOpenningField> TblLoanCreditOpenningField { get; set; }
        public virtual DbSet<TblLoanCreditOtpTs> TblLoanCreditOtpTs { get; set; }
        public virtual DbSet<TblLoanCreditOverView> TblLoanCreditOverView { get; set; }
        public virtual DbSet<TblLoanCreditPriorityContract> TblLoanCreditPriorityContract { get; set; }
        public virtual DbSet<TblLoanCreditProduct> TblLoanCreditProduct { get; set; }
        public virtual DbSet<TblLoanCreditProductAttribute> TblLoanCreditProductAttribute { get; set; }
        public virtual DbSet<TblLoanCreditShop> TblLoanCreditShop { get; set; }
        public virtual DbSet<TblLoanCreditSynchronizePlatformTima> TblLoanCreditSynchronizePlatformTima { get; set; }
        public virtual DbSet<TblLoanCreditThirdParty> TblLoanCreditThirdParty { get; set; }
        public virtual DbSet<TblLoanDebit> TblLoanDebit { get; set; }
        public virtual DbSet<TblLoanDebitReport> TblLoanDebitReport { get; set; }
        public virtual DbSet<TblLoanExtra> TblLoanExtra { get; set; }
        public virtual DbSet<TblLoanIndemnifyInsurrance> TblLoanIndemnifyInsurrance { get; set; }
        public virtual DbSet<TblLoanInterestExpected> TblLoanInterestExpected { get; set; }
        public virtual DbSet<TblLoanStatus> TblLoanStatus { get; set; }
        public virtual DbSet<TblLocation> TblLocation { get; set; }
        public virtual DbSet<TblLogAccountExtend> TblLogAccountExtend { get; set; }
        public virtual DbSet<TblLogActionThn> TblLogActionThn { get; set; }
        public virtual DbSet<TblLogAlarmDate> TblLogAlarmDate { get; set; }
        public virtual DbSet<TblLogBehaviorHub> TblLogBehaviorHub { get; set; }
        public virtual DbSet<TblLogBehaviorTelesale> TblLogBehaviorTelesale { get; set; }
        public virtual DbSet<TblLogBrief> TblLogBrief { get; set; }
        public virtual DbSet<TblLogCallApi> TblLogCallApi { get; set; }
        public virtual DbSet<TblLogCancelCustomerInSight> TblLogCancelCustomerInSight { get; set; }
        public virtual DbSet<TblLogCancelMmo> TblLogCancelMmo { get; set; }
        public virtual DbSet<TblLogChangeLoanBelongTo> TblLogChangeLoanBelongTo { get; set; }
        public virtual DbSet<TblLogChangePhone> TblLogChangePhone { get; set; }
        public virtual DbSet<TblLogChangeValidDocuments> TblLogChangeValidDocuments { get; set; }
        public virtual DbSet<TblLogCheckLoanAi> TblLogCheckLoanAi { get; set; }
        public virtual DbSet<TblLogCheckMoney> TblLogCheckMoney { get; set; }
        public virtual DbSet<TblLogClickToCall> TblLogClickToCall { get; set; }
        public virtual DbSet<TblLogConfirmSuspendedMoney> TblLogConfirmSuspendedMoney { get; set; }
        public virtual DbSet<TblLogConvertHub> TblLogConvertHub { get; set; }
        public virtual DbSet<TblLogConvertTelesale> TblLogConvertTelesale { get; set; }
        public virtual DbSet<TblLogCriteria> TblLogCriteria { get; set; }
        public virtual DbSet<TblLogCustomer> TblLogCustomer { get; set; }
        public virtual DbSet<TblLogInformationDisbursement> TblLogInformationDisbursement { get; set; }
        public virtual DbSet<TblLogLiquid> TblLogLiquid { get; set; }
        public virtual DbSet<TblLogLoanByDay> TblLogLoanByDay { get; set; }
        public virtual DbSet<TblLogLoanCreditIdentical> TblLogLoanCreditIdentical { get; set; }
        public virtual DbSet<TblLogLoanCreditPushHub> TblLogLoanCreditPushHub { get; set; }
        public virtual DbSet<TblLogLocationThn> TblLogLocationThn { get; set; }
        public virtual DbSet<TblLogLogIn> TblLogLogIn { get; set; }
        public virtual DbSet<TblLogMessagePayment> TblLogMessagePayment { get; set; }
        public virtual DbSet<TblLogMoneyFee> TblLogMoneyFee { get; set; }
        public virtual DbSet<TblLogOneSms> TblLogOneSms { get; set; }
        public virtual DbSet<TblLogPartnerDealingBadDebts> TblLogPartnerDealingBadDebts { get; set; }
        public virtual DbSet<TblLogPayBack> TblLogPayBack { get; set; }
        public virtual DbSet<TblLogPaymentNotify> TblLogPaymentNotify { get; set; }
        public virtual DbSet<TblLogProcessStaffThn> TblLogProcessStaffThn { get; set; }
        public virtual DbSet<TblLogPushPredictive> TblLogPushPredictive { get; set; }
        public virtual DbSet<TblLogSendOtp> TblLogSendOtp { get; set; }
        public virtual DbSet<TblLogSendStatusInsurance> TblLogSendStatusInsurance { get; set; }
        public virtual DbSet<TblLogShop> TblLogShop { get; set; }
        public virtual DbSet<TblLogSupport> TblLogSupport { get; set; }
        public virtual DbSet<TblLogThnKpireportByDay> TblLogThnKpireportByDay { get; set; }
        public virtual DbSet<TblLogTimeCollector> TblLogTimeCollector { get; set; }
        public virtual DbSet<TblLogTrackingCall> TblLogTrackingCall { get; set; }
        public virtual DbSet<TblLogTransferLoanOfLender> TblLogTransferLoanOfLender { get; set; }
        public virtual DbSet<TblLogUploadFile> TblLogUploadFile { get; set; }
        public virtual DbSet<TblLogWorkingUser> TblLogWorkingUser { get; set; }
        public virtual DbSet<TblManageSpices> TblManageSpices { get; set; }
        public virtual DbSet<TblManufacturer> TblManufacturer { get; set; }
        public virtual DbSet<TblMarkStep> TblMarkStep { get; set; }
        public virtual DbSet<TblMessage> TblMessage { get; set; }
        public virtual DbSet<TblMessageUser> TblMessageUser { get; set; }
        public virtual DbSet<TblModule> TblModule { get; set; }
        public virtual DbSet<TblOddDebtDaily> TblOddDebtDaily { get; set; }
        public virtual DbSet<TblOrderSource> TblOrderSource { get; set; }
        public virtual DbSet<TblOrderSourceDetail> TblOrderSourceDetail { get; set; }
        public virtual DbSet<TblOtpContract> TblOtpContract { get; set; }
        public virtual DbSet<TblOtpTs> TblOtpTs { get; set; }
        public virtual DbSet<TblPawnFiles> TblPawnFiles { get; set; }
        public virtual DbSet<TblPawnNew> TblPawnNew { get; set; }
        public virtual DbSet<TblPawnNewDebit> TblPawnNewDebit { get; set; }
        public virtual DbSet<TblPawnNewExtra> TblPawnNewExtra { get; set; }
        public virtual DbSet<TblPawnNewFiles> TblPawnNewFiles { get; set; }
        public virtual DbSet<TblPayment> TblPayment { get; set; }
        public virtual DbSet<TblPayment2> TblPayment2 { get; set; }
        public virtual DbSet<TblPayment3> TblPayment3 { get; set; }
        public virtual DbSet<TblPayment4> TblPayment4 { get; set; }
        public virtual DbSet<TblPaymentDetail> TblPaymentDetail { get; set; }
        public virtual DbSet<TblPaymentDetail2> TblPaymentDetail2 { get; set; }
        public virtual DbSet<TblPaymentNotify> TblPaymentNotify { get; set; }
        public virtual DbSet<TblPaymentNotifyByDay> TblPaymentNotifyByDay { get; set; }
        public virtual DbSet<TblPaymentScheduler> TblPaymentScheduler { get; set; }
        public virtual DbSet<TblPermission> TblPermission { get; set; }
        public virtual DbSet<TblPermissionDistrict> TblPermissionDistrict { get; set; }
        public virtual DbSet<TblPermissionDistrictBebtRecovery> TblPermissionDistrictBebtRecovery { get; set; }
        public virtual DbSet<TblPermissionGroup> TblPermissionGroup { get; set; }
        public virtual DbSet<TblPermissionUser> TblPermissionUser { get; set; }
        public virtual DbSet<TblPlanDebt> TblPlanDebt { get; set; }
        public virtual DbSet<TblPlanOfDisbursement> TblPlanOfDisbursement { get; set; }
        public virtual DbSet<TblPlatformAndTima> TblPlatformAndTima { get; set; }
        public virtual DbSet<TblPointVnpt> TblPointVnpt { get; set; }
        public virtual DbSet<TblProcessData> TblProcessData { get; set; }
        public virtual DbSet<TblProcessPayment> TblProcessPayment { get; set; }
        public virtual DbSet<TblProcessTimeDepartMent> TblProcessTimeDepartMent { get; set; }
        public virtual DbSet<TblProduct> TblProduct { get; set; }
        public virtual DbSet<TblProductAppraiser> TblProductAppraiser { get; set; }
        public virtual DbSet<TblProductCredit> TblProductCredit { get; set; }
        public virtual DbSet<TblProductEvaluation> TblProductEvaluation { get; set; }
        public virtual DbSet<TblProductPercentReduction> TblProductPercentReduction { get; set; }
        public virtual DbSet<TblProductReview> TblProductReview { get; set; }
        public virtual DbSet<TblProductReviewDetail> TblProductReviewDetail { get; set; }
        public virtual DbSet<TblProductReviewResult> TblProductReviewResult { get; set; }
        public virtual DbSet<TblProductReviewResultDetail> TblProductReviewResultDetail { get; set; }
        public virtual DbSet<TblProductStep> TblProductStep { get; set; }
        public virtual DbSet<TblProductType> TblProductType { get; set; }
        public virtual DbSet<TblProportionProductAgency> TblProportionProductAgency { get; set; }
        public virtual DbSet<TblPushSmartDailer> TblPushSmartDailer { get; set; }
        public virtual DbSet<TblReason> TblReason { get; set; }
        public virtual DbSet<TblReasonReturn> TblReasonReturn { get; set; }
        public virtual DbSet<TblReceiveYourIncome> TblReceiveYourIncome { get; set; }
        public virtual DbSet<TblReference> TblReference { get; set; }
        public virtual DbSet<TblRegion> TblRegion { get; set; }
        public virtual DbSet<TblReimbursement> TblReimbursement { get; set; }
        public virtual DbSet<TblRelationshipEmployees> TblRelationshipEmployees { get; set; }
        public virtual DbSet<TblRelativeFamily> TblRelativeFamily { get; set; }
        public virtual DbSet<TblReportCounselors> TblReportCounselors { get; set; }
        public virtual DbSet<TblReportGoogleAds> TblReportGoogleAds { get; set; }
        public virtual DbSet<TblReportHubAdministrator> TblReportHubAdministrator { get; set; }
        public virtual DbSet<TblReportPtsp> TblReportPtsp { get; set; }
        public virtual DbSet<TblReportShop> TblReportShop { get; set; }
        public virtual DbSet<TblRequestCac> TblRequestCac { get; set; }
        public virtual DbSet<TblScheduledCollection> TblScheduledCollection { get; set; }
        public virtual DbSet<TblScore> TblScore { get; set; }
        public virtual DbSet<TblShop> TblShop { get; set; }
        public virtual DbSet<TblSimVip> TblSimVip { get; set; }
        public virtual DbSet<TblSms> TblSms { get; set; }
        public virtual DbSet<TblSmsAnalytics> TblSmsAnalytics { get; set; }
        public virtual DbSet<TblSmsBrandName> TblSmsBrandName { get; set; }
        public virtual DbSet<TblSmsCredit> TblSmsCredit { get; set; }
        public virtual DbSet<TblSmscompleted> TblSmscompleted { get; set; }
        public virtual DbSet<TblStatementOfAccounts> TblStatementOfAccounts { get; set; }
        public virtual DbSet<TblTarget> TblTarget { get; set; }
        public virtual DbSet<TblTelesaleCall> TblTelesaleCall { get; set; }
        public virtual DbSet<TblTempCountSms> TblTempCountSms { get; set; }
        public virtual DbSet<TblTempData> TblTempData { get; set; }
        public virtual DbSet<TblTempInterestToday> TblTempInterestToday { get; set; }
        public virtual DbSet<TblTempThnPushData> TblTempThnPushData { get; set; }
        public virtual DbSet<TblThnFiles> TblThnFiles { get; set; }
        public virtual DbSet<TblThnlogActionManager> TblThnlogActionManager { get; set; }
        public virtual DbSet<TblThnlogPushData> TblThnlogPushData { get; set; }
        public virtual DbSet<TblTimaAt> TblTimaAt { get; set; }
        public virtual DbSet<TblTimaVbi> TblTimaVbi { get; set; }
        public virtual DbSet<TblToken> TblToken { get; set; }
        public virtual DbSet<TblTokenCareSoft> TblTokenCareSoft { get; set; }
        public virtual DbSet<TblTransactionBankCard> TblTransactionBankCard { get; set; }
        public virtual DbSet<TblTransactionCapital> TblTransactionCapital { get; set; }
        public virtual DbSet<TblTransactionCollectionExpense> TblTransactionCollectionExpense { get; set; }
        public virtual DbSet<TblTransactionInstallment> TblTransactionInstallment { get; set; }
        public virtual DbSet<TblTransactionLoan> TblTransactionLoan { get; set; }
        public virtual DbSet<TblTransactionMoneyInSafe> TblTransactionMoneyInSafe { get; set; }
        public virtual DbSet<TblTransactionPawnNew> TblTransactionPawnNew { get; set; }
        public virtual DbSet<TblTrustResults> TblTrustResults { get; set; }
        public virtual DbSet<TblTypeOfOwnership> TblTypeOfOwnership { get; set; }
        public virtual DbSet<TblUser> TblUser { get; set; }
        public virtual DbSet<TblUserCheckInByApp> TblUserCheckInByApp { get; set; }
        public virtual DbSet<TblUserDomainRegion> TblUserDomainRegion { get; set; }
        public virtual DbSet<TblUserShop> TblUserShop { get; set; }
        public virtual DbSet<TblVaLog> TblVaLog { get; set; }
        public virtual DbSet<TblVibInsurrance> TblVibInsurrance { get; set; }
        public virtual DbSet<TblWard> TblWard { get; set; }
        public virtual DbSet<TblWithdraw> TblWithdraw { get; set; }
        public virtual DbSet<TblWorkingIndustry> TblWorkingIndustry { get; set; }
        public virtual DbSet<VvGetDataSuspendedMoneyForFast> VvGetDataSuspendedMoneyForFast { get; set; }
        public virtual DbSet<VvGetDataSuspendedMoneyForFastOfHub> VvGetDataSuspendedMoneyForFastOfHub { get; set; }
        public virtual DbSet<VvGetDataTransactionBankCardForFast> VvGetDataTransactionBankCardForFast { get; set; }
        public virtual DbSet<VvGetDataTransactionBankCardForFastHub> VvGetDataTransactionBankCardForFastHub { get; set; }
        public virtual DbSet<VvReportCapitalForFast> VvReportCapitalForFast { get; set; }
        public virtual DbSet<VvReportCapitalForFastHub> VvReportCapitalForFastHub { get; set; }
        public virtual DbSet<VvReportCollectionExpense> VvReportCollectionExpense { get; set; }
        public virtual DbSet<VvReportCollectionExpenseHub> VvReportCollectionExpenseHub { get; set; }
        public virtual DbSet<VvReportLendingForFast> VvReportLendingForFast { get; set; }
        public virtual DbSet<VvReportLendingForFast4> VvReportLendingForFast4 { get; set; }
        public virtual DbSet<VvReportLendingForFastHub> VvReportLendingForFastHub { get; set; }
        public virtual DbSet<VvReportSellSoftwareForFast> VvReportSellSoftwareForFast { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=42.112.23.25;Database=Mecash;uid=timalender;password=Timalender@2019;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BankLog>(entity =>
            {
                entity.HasIndex(e => new { e.ShopId, e.LoanCreditId, e.BankCode, e.TransactionType, e.ResponseCode, e.VerifyResponseCode, e.VerifyBankRef })
                    .HasName("NonClusteredIndex-20190315-135115");

                entity.Property(e => e.BankCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BankRef)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CallbackUrl)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CustomerInsFee).HasColumnType("money");

                entity.Property(e => e.Data).HasMaxLength(1000);

                entity.Property(e => e.FromBank)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LenderInsFee).HasColumnType("money");

                entity.Property(e => e.Money).HasColumnType("money");

                entity.Property(e => e.Response).HasMaxLength(1000);

                entity.Property(e => e.ResponseCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SyncResponseCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyBankRef)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyResponse).HasMaxLength(1000);

                entity.Property(e => e.VerifyResponseCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyTransactionId)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Comission>(entity =>
            {
                entity.Property(e => e.Collaborator)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Comission1).HasColumnName("Comission");

                entity.Property(e => e.FromMoney).HasColumnType("money");

                entity.Property(e => e.ToMoney).HasColumnType("money");
            });

            modelBuilder.Entity<DataTest>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.ForDate });

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.ProductName).HasMaxLength(50);

                entity.Property(e => e.ReceiveYourInCome).HasMaxLength(50);

                entity.Property(e => e.ShopName).HasMaxLength(50);

                entity.Property(e => e.StrLoanTime).HasMaxLength(50);
            });

            modelBuilder.Entity<DataTest2>(entity =>
            {
                entity.Property(e => e.ApproverLoanCredit).HasMaxLength(50);

                entity.Property(e => e.BirthDay).HasColumnType("date");

                entity.Property(e => e.FromDateDisbursement).HasColumnType("date");

                entity.Property(e => e.InfoCic).HasColumnName("InfoCIC");

                entity.Property(e => e.StrRateType).HasColumnName("strRateType");

                entity.Property(e => e.StrStatus).HasColumnName("strStatus");

                entity.Property(e => e.ThucDiaCongTy).HasMaxLength(50);

                entity.Property(e => e.ThucDiaHub).HasMaxLength(50);

                entity.Property(e => e.ThucDiaNha).HasMaxLength(50);

                entity.Property(e => e.TypeOfOwnershipName).HasMaxLength(50);
            });

            modelBuilder.Entity<DataTest3>(entity =>
            {
                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.NextDate).HasColumnType("date");

                entity.Property(e => e.NextDatefuture).HasColumnType("date");

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<DataTest4>(entity =>
            {
                entity.Property(e => e.ForDate).HasColumnType("date");
            });

            modelBuilder.Entity<DataTest5>(entity =>
            {
                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.LastDateOfPay).HasColumnType("date");

                entity.Property(e => e.NextDate).HasColumnType("date");

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<DataTest6>(entity =>
            {
                entity.ToTable("dataTest6");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.NgayGiaiNgan).HasColumnType("date");

                entity.Property(e => e.PayDate).HasColumnType("date");

                entity.Property(e => e.StrRate)
                    .HasColumnName("strRate")
                    .HasMaxLength(50);

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<DataTest7>(entity =>
            {
                entity.Property(e => e.AddressCompany).HasMaxLength(500);

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.CapPheDuyet).HasMaxLength(50);

                entity.Property(e => e.CityName).HasMaxLength(500);

                entity.Property(e => e.CompanyName).HasMaxLength(500);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(500);

                entity.Property(e => e.DistrictName).HasMaxLength(500);

                entity.Property(e => e.EmployeesName).HasMaxLength(500);

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.HubName).HasMaxLength(500);

                entity.Property(e => e.JobName).HasMaxLength(500);

                entity.Property(e => e.ProductName).HasMaxLength(50);

                entity.Property(e => e.Street).HasMaxLength(500);

                entity.Property(e => e.TypeOfOwnershipName).HasMaxLength(500);

                entity.Property(e => e.WardName).HasMaxLength(500);
            });

            modelBuilder.Entity<LenderPayment>(entity =>
            {
                entity.Property(e => e.ConfirmedDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CustomerInsuranceMoney).HasColumnType("money");

                entity.Property(e => e.CustomerInsurancePaid).HasColumnType("money");

                entity.Property(e => e.DiferenceMoney).HasColumnType("money");

                entity.Property(e => e.FineMoney).HasColumnType("money");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.InsurancePaidLoans)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.InterestMoney).HasColumnType("money");

                entity.Property(e => e.LastBalance).HasColumnType("money");

                entity.Property(e => e.LenderInsuranceMoney).HasColumnType("money");

                entity.Property(e => e.LenderInsurancePaid).HasColumnType("money");

                entity.Property(e => e.LenderPaid).HasColumnType("money");

                entity.Property(e => e.NeedToPay).HasColumnType("money");

                entity.Property(e => e.PricipalMoney).HasColumnType("money");

                entity.Property(e => e.TimaPaid).HasColumnType("money");

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.Property(e => e.TotalMoney).HasColumnType("money");

                entity.Property(e => e.TransferedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<LinkageBank>(entity =>
            {
                entity.Property(e => e.AccountNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BankCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdCardNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WalletId)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LogMmoPushSmartDailer>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.IsMmo).HasColumnName("IsMMO");

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.Property(e => e.Action)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Message).HasMaxLength(500);

                entity.Property(e => e.PushTime).HasColumnType("datetime");

                entity.Property(e => e.Read).IsUnicode(false);

                entity.Property(e => e.Topic)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProcessingTime>(entity =>
            {
                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.FinishDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Cust)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Product1)
                    .HasColumnName("Product")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Qty).HasColumnName("QTY");
            });

            modelBuilder.Entity<TblAddressCounseling>(entity =>
            {
                entity.ToTable("tblAddressCounseling");

                entity.Property(e => e.Address).HasMaxLength(250);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Time).HasMaxLength(30);
            });

            modelBuilder.Entity<TblAdsForm>(entity =>
            {
                entity.ToTable("tblAdsForm");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.UtmSource)
                    .HasColumnName("utm_source")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblAdsPage>(entity =>
            {
                entity.ToTable("tblAdsPage");

                entity.Property(e => e.AccessToken)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblAff>(entity =>
            {
                entity.ToTable("tblAff");

                entity.HasIndex(e => e.PhoneNumber)
                    .HasName("UQ__tblAff__85FB4E38BD1DB401")
                    .IsUnique();

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.IsAdmin).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(13)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblAffLender>(entity =>
            {
                entity.ToTable("tblAffLender");

                entity.HasIndex(e => new { e.AffId, e.LenderId })
                    .HasName("NonClusteredIndex-20200306-101935");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RateAff).HasColumnType("decimal(18, 6)");
            });

            modelBuilder.Entity<TblAgencyRefuse>(entity =>
            {
                entity.ToTable("tblAgencyRefuse");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblAlarmDateLog>(entity =>
            {
                entity.ToTable("tblAlarmDateLog");

                entity.Property(e => e.AlarmDate).HasColumnType("datetime");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NoteAlarm).HasMaxLength(1024);

                entity.Property(e => e.UserAction).HasMaxLength(450);
            });

            modelBuilder.Entity<TblArea>(entity =>
            {
                entity.ToTable("tblArea");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<TblAttCustomerCreditValidator>(entity =>
            {
                entity.ToTable("tblAttCustomerCreditValidator");

                entity.Property(e => e.ContentValidatorAttribute)
                    .HasColumnType("ntext")
                    .HasComment("Thuộc tính thẩm định");
            });

            modelBuilder.Entity<TblAwardContract>(entity =>
            {
                entity.ToTable("tblAwardContract");

                entity.Property(e => e.AwardContractId).HasColumnName("AwardContractID");
            });

            modelBuilder.Entity<TblBadDebReason>(entity =>
            {
                entity.ToTable("tblBadDebReason");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CusId).HasColumnName("CusID");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.ReasonContent).HasMaxLength(1000);

                entity.Property(e => e.ReasonId).HasColumnName("ReasonID");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblBank>(entity =>
            {
                entity.ToTable("tblBank");

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NameBank).HasMaxLength(100);
            });

            modelBuilder.Entity<TblBankCard>(entity =>
            {
                entity.ToTable("tblBankCard");

                entity.Property(e => e.AccountHolderName)
                    .HasMaxLength(50)
                    .HasComment("Tên chủ tài khoản");

                entity.Property(e => e.AliasName).HasMaxLength(250);

                entity.Property(e => e.BankCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BranchName).HasMaxLength(50);

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.ModifyOn).HasColumnType("datetime");

                entity.Property(e => e.NumberAccount).HasMaxLength(50);

                entity.Property(e => e.ParentId)
                    .HasDefaultValueSql("((0))")
                    .HasComment("làm thẻ con của thẻ nào");

                entity.Property(e => e.ShopName).HasMaxLength(50);

                entity.Property(e => e.TypePurpose).HasComment("Mục đích sử dụng");
            });

            modelBuilder.Entity<TblBankCardShop>(entity =>
            {
                entity.ToTable("tblBankCardShop");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.ModifyOn).HasColumnType("datetime");

                entity.Property(e => e.UserFullName).HasMaxLength(50);

                entity.Property(e => e.UserFullNameModify).HasMaxLength(50);

                entity.Property(e => e.UserName).HasMaxLength(50);

                entity.Property(e => e.UserNameModify).HasMaxLength(50);
            });

            modelBuilder.Entity<TblBankConfig>(entity =>
            {
                entity.ToTable("tblBankConfig");

                entity.Property(e => e.BankAlias)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BankName).HasMaxLength(50);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.TextBegin)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TextEnd)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TextFirst)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TextLast)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblBlackList>(entity =>
            {
                entity.ToTable("tblBlackList");

                entity.Property(e => e.BirthDay).HasColumnType("date");

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.ExpirationDate)
                    .HasColumnType("date")
                    .HasComment("Ngày hết hạn");

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.FullNameApprove).HasMaxLength(50);

                entity.Property(e => e.FullNameCreate).HasMaxLength(50);

                entity.Property(e => e.FullNameModify).HasMaxLength(50);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.NumberDay).HasComment("Số ngày BlackList");

                entity.Property(e => e.NumberPhone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TimeApprove).HasColumnType("datetime");

                entity.Property(e => e.UserNameApprove).HasMaxLength(50);

                entity.Property(e => e.UserNameCreate).HasMaxLength(50);

                entity.Property(e => e.UserNameModify).HasMaxLength(50);
            });

            modelBuilder.Entity<TblBlackListLog>(entity =>
            {
                entity.ToTable("tblBlackListLog");

                entity.Property(e => e.BirthDay)
                    .HasColumnType("date")
                    .HasComment("Ngày sinh khách hàng");

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("Số CMT khách hàng");

                entity.Property(e => e.CheckResultResult).HasComment(@"//result = 0 ko có trong BlackList
                    //result = 1 cảnh báo
                    //result = 2 có trong blackList");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FullName)
                    .HasMaxLength(50)
                    .HasComment("Tên khách hàng");

                entity.Property(e => e.NumberPhone)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("Số đt khách hàng");

                entity.Property(e => e.UserId).HasComment("người thực hiện yêu cầu");
            });

            modelBuilder.Entity<TblBrand>(entity =>
            {
                entity.ToTable("tblBrand");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCampaign>(entity =>
            {
                entity.ToTable("tblCampaign");

                entity.Property(e => e.CampaignType).HasComment("Loại chiến dịch 1: Tiền mặt");

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Mã khuyến mại");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.EndDate)
                    .HasColumnType("datetime")
                    .HasComment("Ngày kết thúc chiến dịch");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.PercentPromotion).HasComment("Tổng số tiền khuyến mại");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.StartDate)
                    .HasColumnType("datetime")
                    .HasComment("Ngày bắt đầu chiến dịch");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UrlPathImage)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UtmCampaign)
                    .HasColumnName("utm_campaign")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UtmContent)
                    .HasColumnName("utm_content")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UtmMedium)
                    .HasColumnName("utm_medium")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UtmSource)
                    .HasColumnName("utm_source")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UtmTerm)
                    .HasColumnName("utm_term")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCampaign2District>(entity =>
            {
                entity.HasKey(e => new { e.CampaignId, e.CityId, e.DistrictId });

                entity.ToTable("tblCampaign2District");

                entity.Property(e => e.CampaignId).HasColumnName("CampaignID");

                entity.Property(e => e.CityId).HasColumnName("CityID");

                entity.Property(e => e.DistrictId).HasColumnName("DistrictID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblCampaignBuilderInfo>(entity =>
            {
                entity.ToTable("tblCampaignBuilderInfo");

                entity.Property(e => e.CampaignManager)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(250);
            });

            modelBuilder.Entity<TblCampaignGenUrl>(entity =>
            {
                entity.ToTable("tblCampaignGenURL");

                entity.Property(e => e.CampaignBuilderInfoId).HasColumnName("CampaignBuilderInfoID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.LinkUrl)
                    .HasColumnName("LinkURL")
                    .HasMaxLength(5000)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UtmCampaign)
                    .HasColumnName("utm_campaign")
                    .HasMaxLength(250);

                entity.Property(e => e.UtmContent)
                    .HasColumnName("utm_content")
                    .HasMaxLength(250);

                entity.Property(e => e.UtmMedium)
                    .HasColumnName("utm_medium")
                    .HasMaxLength(250);

                entity.Property(e => e.UtmSource)
                    .HasColumnName("utm_source")
                    .HasMaxLength(250);

                entity.Property(e => e.WebsiteUrl)
                    .HasColumnName("WebsiteURL")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCampaignUrlbuilder>(entity =>
            {
                entity.ToTable("tblCampaignURLBuilder");

                entity.Property(e => e.CampaignRunner).HasMaxLength(4000);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.UtmContent)
                    .HasColumnName("utm_content")
                    .HasMaxLength(4000)
                    .HasComment("Lưu tên gói sản phẩm cách nhau bằng dấu phẩy");

                entity.Property(e => e.UtmMedium)
                    .HasColumnName("utm_medium")
                    .HasMaxLength(4000)
                    .HasComment("lưu hình thức quảng cáo cách nhau bằng dấu phẩy");

                entity.Property(e => e.UtmSource)
                    .HasColumnName("utm_source")
                    .HasMaxLength(4000)
                    .HasComment("lưu nguồn quảng cáo cách nhau bằng dấu phẩy");
            });

            modelBuilder.Entity<TblCapital>(entity =>
            {
                entity.ToTable("tblCapital");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BankCode).HasMaxLength(150);

                entity.Property(e => e.BankId).HasColumnName("BankID");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.LastDateOfPay).HasColumnType("date");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NextDate).HasColumnType("date");

                entity.Property(e => e.Note).HasMaxLength(1024);

                entity.Property(e => e.PaymentMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblCapitalExtra>(entity =>
            {
                entity.ToTable("tblCapitalExtra");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CapitalId).HasColumnName("CapitalID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DateExtra).HasColumnType("date");

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblCapitalMember>(entity =>
            {
                entity.ToTable("tblCapitalMember");

                entity.Property(e => e.Address).HasMaxLength(256);

                entity.Property(e => e.Cmnd)
                    .HasColumnName("CMND")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateContribution).HasColumnType("datetime");

                entity.Property(e => e.NameMember).HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCapitalUserOwner>(entity =>
            {
                entity.ToTable("tblCapitalUserOwner");
            });

            modelBuilder.Entity<TblCashBankCardByDate>(entity =>
            {
                entity.ToTable("tblCashBankCardByDate");

                entity.Property(e => e.BankCardName).HasMaxLength(250);

                entity.Property(e => e.Changed).HasDefaultValueSql("((0))");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.ShopName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblCashByDate>(entity =>
            {
                entity.HasKey(e => new { e.ShopId, e.ForDate });

                entity.ToTable("tblCashByDate");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<TblCategory>(entity =>
            {
                entity.HasKey(e => e.CategoryId)
                    .HasName("PK__tblCateg__19093A2B6319B466");

                entity.ToTable("tblCategory");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.Code)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Extend1).HasMaxLength(100);

                entity.Property(e => e.Extend2).HasMaxLength(100);

                entity.Property(e => e.Extend3).HasMaxLength(100);

                entity.Property(e => e.Extend4).HasMaxLength(100);

                entity.Property(e => e.Extend5).HasMaxLength(100);

                entity.Property(e => e.Extend6).HasMaxLength(100);

                entity.Property(e => e.Extend7).HasMaxLength(100);

                entity.Property(e => e.Extend8).HasMaxLength(100);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");
            });

            modelBuilder.Entity<TblCategoryProduct>(entity =>
            {
                entity.ToTable("tblCategoryProduct");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<TblCity>(entity =>
            {
                entity.ToTable("tblCity");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.RegionId).HasColumnName("RegionID");

                entity.Property(e => e.TypeCity).HasMaxLength(50);
            });

            modelBuilder.Entity<TblCollaborators>(entity =>
            {
                entity.ToTable("tblCollaborators");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FullName).HasMaxLength(250);

                entity.Property(e => e.NumberCard)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblComment>(entity =>
            {
                entity.ToTable("tblComment");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<TblCommentCredit>(entity =>
            {
                entity.ToTable("tblCommentCredit");

                entity.HasIndex(e => e.LoanCreditId)
                    .HasName("index_LoanCredit_tblCommentCredit");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.IsDisplay).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsManual).HasDefaultValueSql("((0))");

                entity.Property(e => e.ReasonId).HasColumnName("ReasonID");

                entity.Property(e => e.ToFullName).HasMaxLength(100);
            });

            modelBuilder.Entity<TblCommentCreditReport>(entity =>
            {
                entity.ToTable("tblCommentCreditReport");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.Comment).HasMaxLength(1000);

                entity.Property(e => e.CounselorName).HasMaxLength(50);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateDateFromLoanCredit).HasColumnType("datetime");

                entity.Property(e => e.CustomerCreditName).HasMaxLength(100);

                entity.Property(e => e.CustomerPhone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictName).HasMaxLength(50);

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.ReasonId).HasColumnName("ReasonID");
            });

            modelBuilder.Entity<TblCommentCustomerBad>(entity =>
            {
                entity.ToTable("tblCommentCustomerBad");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Result).HasMaxLength(500);

                entity.Property(e => e.ShopName)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<TblCommentDebtPrompted>(entity =>
            {
                entity.ToTable("tblCommentDebtPrompted");

                entity.HasIndex(e => new { e.CreateDate, e.LoanId, e.UserId })
                    .HasName("index_tblCommentDebtPrompted");

                entity.Property(e => e.CReasonCode)
                    .HasColumnName("C_ReasonCode")
                    .HasMaxLength(50);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.NextDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblCompany>(entity =>
            {
                entity.ToTable("tblCompany");

                entity.Property(e => e.NameCompany).HasMaxLength(250);
            });

            modelBuilder.Entity<TblConfigCoordinator>(entity =>
            {
                entity.ToTable("tblConfigCoordinator");

                entity.Property(e => e.Createdate).HasColumnType("datetime");

                entity.Property(e => e.UserName).HasMaxLength(200);
            });

            modelBuilder.Entity<TblConfigInterest>(entity =>
            {
                entity.ToTable("tblConfigInterest");

                entity.Property(e => e.Interest).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<TblConfigRole>(entity =>
            {
                entity.ToTable("tblConfigRole");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BadDebt).HasMaxLength(250);

                entity.Property(e => e.Code).HasMaxLength(250);

                entity.Property(e => e.FromCondition)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("điều kiện cho (từ)");

                entity.Property(e => e.ToCondition)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("điều kiện cho (đến)");

                entity.Property(e => e.TotalMoneyFirstFr).HasComment("Số tiền đề nghị vay (từ)");

                entity.Property(e => e.TotalMoneyFirstTo).HasComment("Số tiền đề nghị vay (đến)");

                entity.Property(e => e.WarningDebt).HasMaxLength(250);
            });

            modelBuilder.Entity<TblConfigSmartDailer>(entity =>
            {
                entity.ToTable("tblConfigSmartDailer");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CampaignId).HasColumnName("campaign_id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndAmCall)
                    .HasColumnName("end_am_call")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndPmCall)
                    .HasColumnName("end_pm_call")
                    .HasColumnType("datetime");

                entity.Property(e => e.MaxCall).HasColumnName("max_call");

                entity.Property(e => e.ModifyDate)
                    .HasColumnName("modify_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.StartAmCall)
                    .HasColumnName("start_am_call")
                    .HasColumnType("datetime");

                entity.Property(e => e.StartPmCall)
                    .HasColumnName("start_pm_call")
                    .HasColumnType("datetime");

                entity.Property(e => e.TimeRecall).HasColumnName("time_recall");
            });

            modelBuilder.Entity<TblConfigTimeToDecideWhoWillCollectLoan>(entity =>
            {
                entity.HasKey(e => e.UserType)
                    .HasName("PK__tblConfi__87E78690434DDA7E");

                entity.ToTable("tblConfigTimeToDecideWhoWillCollectLoan");

                entity.Property(e => e.UserType)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblConfigUserRemindDebtByLoanAndMonth>(entity =>
            {
                entity.ToTable("tblConfigUserRemindDebtByLoanAndMonth");

                entity.Property(e => e.CityId).HasColumnName("CityID");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.CodeId).HasColumnName("CodeID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreateUserId).HasColumnName("CreateUserID");

                entity.Property(e => e.CurrentUserId).HasColumnName("CurrentUserID");

                entity.Property(e => e.CurrentUserName).HasMaxLength(150);

                entity.Property(e => e.CustomerName).HasMaxLength(150);

                entity.Property(e => e.DepartmentManagerUserId).HasColumnName("DepartmentManagerUserID");

                entity.Property(e => e.DepartmentManagerUsername)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictId).HasColumnName("DistrictID");

                entity.Property(e => e.DistrictName).HasMaxLength(150);

                entity.Property(e => e.FirstDate).HasColumnType("date");

                entity.Property(e => e.FirstUpdate).HasColumnType("datetime");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.FreeUserId).HasColumnName("FreeUserID");

                entity.Property(e => e.FreeUserName).HasMaxLength(50);

                entity.Property(e => e.IsResolve).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastUserId).HasColumnName("LastUserID");

                entity.Property(e => e.LastUserName).HasMaxLength(150);

                entity.Property(e => e.MoneyNeedCollect).HasDefaultValueSql("((0))");

                entity.Property(e => e.MonthAndYear)
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.NextDate).HasColumnType("datetime");

                entity.Property(e => e.ProcessUserId).HasColumnName("ProcessUserID");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.ProductName).HasMaxLength(50);

                entity.Property(e => e.SectionManagerUserId).HasColumnName("SectionManagerUserID");

                entity.Property(e => e.SectionManagerUsername)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.TeamleadUserId).HasColumnName("TeamleadUserID");

                entity.Property(e => e.TeamleadUserName)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.TotalMoneyReceived).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblCoordinatorCheckList>(entity =>
            {
                entity.ToTable("tblCoordinatorCheckList");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblCoordinatorCriteria>(entity =>
            {
                entity.HasKey(e => e.CoordinatorCriteriaId);

                entity.ToTable("tblCoordinatorCriteria");

                entity.Property(e => e.CoordinatorCriteriaId).ValueGeneratedNever();

                entity.Property(e => e.IsCancel).HasComment("1: hủy ngay, 0: duyệt vay");

                entity.Property(e => e.IsUse).HasComment("được sử dụng");

                entity.Property(e => e.LableName).HasMaxLength(500);

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.ParentId).HasComment("= CoordinatorCriteriaId");

                entity.Property(e => e.Type).HasComment("1: Dropdownlist, 2: Radio");
            });

            modelBuilder.Entity<TblCounselorsError>(entity =>
            {
                entity.ToTable("tblCounselorsError");

                entity.Property(e => e.CounselorId).HasComment("UserId người mắc lỗi");

                entity.Property(e => e.CounselorName)
                    .HasMaxLength(50)
                    .HasComment("Tên người mắc lỗi");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.GroupUserError).HasComment("bộ phận dính lỗi");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.UserName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblCustomer>(entity =>
            {
                entity.HasKey(e => e.CustomerId);

                entity.ToTable("tblCustomer");

                entity.HasIndex(e => new { e.Name, e.Phone })
                    .HasName("NonClusteredIndex-20190402-153611");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.AccountName)
                    .HasColumnName("Account_name")
                    .HasMaxLength(200);

                entity.Property(e => e.AccountNo)
                    .HasColumnName("Account_no")
                    .HasMaxLength(100);

                entity.Property(e => e.Address).HasMaxLength(256);

                entity.Property(e => e.AddressHouseHold).HasMaxLength(500);

                entity.Property(e => e.BankCodeVa)
                    .HasColumnName("BankCode_Va")
                    .HasMaxLength(200);

                entity.Property(e => e.BankNameVa)
                    .HasColumnName("BankName_Va")
                    .HasMaxLength(200);

                entity.Property(e => e.CardDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DocumentValue).HasMaxLength(50);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ListPhone)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.MapId)
                    .HasColumnName("Map_id")
                    .HasMaxLength(200);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.NumberCard)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PermanentAddress).HasMaxLength(250);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneRelationShip)
                    .HasMaxLength(500)
                    .HasComment("Các số điện thoại liên quan của khách hàng");

                entity.Property(e => e.Place).HasMaxLength(100);

                entity.Property(e => e.RegistrationCustomerId)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ShopId)
                    .HasColumnName("ShopID")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");

                entity.Property(e => e.StatusReadImgCmt)
                    .HasColumnName("StatusReadImgCMT")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.StatusVa).HasColumnName("Status_Va");

                entity.Property(e => e.TotalMoney).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblCustomerBad>(entity =>
            {
                entity.ToTable("tblCustomerBad");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UrlImg)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UserName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblCustomerBadDebitReport>(entity =>
            {
                entity.ToTable("tblCustomerBadDebitReport");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CusAddress).HasMaxLength(500);

                entity.Property(e => e.CusId).HasColumnName("CusID");

                entity.Property(e => e.CusName).HasMaxLength(500);

                entity.Property(e => e.CusPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.NumberCard)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");
            });

            modelBuilder.Entity<TblCustomerCare>(entity =>
            {
                entity.ToTable("tblCustomerCare");

                entity.Property(e => e.Content).HasMaxLength(500);

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblCustomerCredit>(entity =>
            {
                entity.HasKey(e => e.CustomerId);

                entity.ToTable("tblCustomerCredit");

                entity.Property(e => e.AccountNo).HasMaxLength(100);

                entity.Property(e => e.AddressCompany).HasMaxLength(500);

                entity.Property(e => e.AddressHouseHold).HasMaxLength(500);

                entity.Property(e => e.AddressPersonFamily).HasMaxLength(500);

                entity.Property(e => e.AliaseName)
                    .HasMaxLength(50)
                    .HasComment("Bí danh");

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.CarDesign).HasMaxLength(250);

                entity.Property(e => e.CarManufacturer).HasMaxLength(250);

                entity.Property(e => e.CarName).HasMaxLength(250);

                entity.Property(e => e.CarNumberSeat).HasComment("Số chỗ ngồi");

                entity.Property(e => e.CarVersion).HasMaxLength(150);

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CardNumberOther)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CardNumberRegister)
                    .HasColumnType("date")
                    .HasComment("Ngày cấp chứng minh thư");

                entity.Property(e => e.CardOrigin).HasComment("Xuất sứ");

                entity.Property(e => e.CompanyName).HasMaxLength(300);

                entity.Property(e => e.CompanyPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyTaxCode)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasComment("Mã số thuế công ty");

                entity.Property(e => e.ContactNameCompany).HasComment("người liên hệ ở công ty");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DescriptionPositionJob).HasComment("Mô tả vị trí công việc ở công ty");

                entity.Property(e => e.DocumentValue).HasMaxLength(50);

                entity.Property(e => e.Email)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Facebook).HasMaxLength(400);

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.FullNameColleague).HasMaxLength(100);

                entity.Property(e => e.FullNameFamily).HasMaxLength(100);

                entity.Property(e => e.FullNameFamily1).HasMaxLength(100);

                entity.Property(e => e.IsOutProvince).HasComment("Khách hàng ngoại tỉnh hay không");

                entity.Property(e => e.LatLongAddress)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LatLongAddressCompany)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LicensePlates).HasMaxLength(50);

                entity.Property(e => e.LivingApartmentNumber).HasMaxLength(255);

                entity.Property(e => e.ModelPhone).HasMaxLength(50);

                entity.Property(e => e.NumberLiving).HasMaxLength(255);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneColleague)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneFamily)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneFamily1)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneOther)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("Số điện thoại khác");

                entity.Property(e => e.ReceiveYourIncome).HasComment("Hình thức nhận lương: 1 : Chuyển khoản: 2: Tiền mặt");

                entity.Property(e => e.RegistrationCustomerId)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.SocialInsuranceNote).HasMaxLength(200);

                entity.Property(e => e.SocialInsuranceNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Street).HasMaxLength(500);

                entity.Property(e => e.ValuationAsset).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.YearMade).HasMaxLength(50);

                entity.Property(e => e.Zalo)
                    .HasMaxLength(50)
                    .HasComment("Thông tin zalo");
            });

            modelBuilder.Entity<TblCustomerCreditFiles>(entity =>
            {
                entity.ToTable("tblCustomerCreditFiles");

                entity.HasIndex(e => e.CommentCreditId)
                    .HasName("IX_tblCustomerCreditFiles");

                entity.HasIndex(e => new { e.LoanCreditId, e.TypeId })
                    .HasName("NonClusteredIndex-20190925-013622");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CommentThnid).HasColumnName("CommentTHNID");

                entity.Property(e => e.CountProcess).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.DomainOfPartner)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LinkImgOfPartner)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.S3status)
                    .HasColumnName("S3Status")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Synchronize).HasDefaultValueSql("((0))");

                entity.Property(e => e.TypeFile)
                    .HasDefaultValueSql("((1))")
                    .HasComment("File Ảnh, File Video hoặc File Âm Thanh");

                entity.Property(e => e.UploadDate).HasColumnType("datetime");

                entity.Property(e => e.UrlImg)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCustomerFiles>(entity =>
            {
                entity.ToTable("tblCustomerFiles");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.FilePath)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblCustomerOtp>(entity =>
            {
                entity.ToTable("tblCustomerOTP");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ExprireDate).HasColumnType("datetime");

                entity.Property(e => e.Otp).HasColumnName("OTP");

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Smsid).HasColumnName("SMSID");
            });

            modelBuilder.Entity<TblCustomerUser>(entity =>
            {
                entity.ToTable("tblCustomerUser");

                entity.Property(e => e.AddressCompany).HasMaxLength(500);

                entity.Property(e => e.AddressHouseHold).HasMaxLength(500);

                entity.Property(e => e.Birthday).HasColumnType("datetime");

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyName).HasMaxLength(300);

                entity.Property(e => e.CompanyPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ExprireToken).HasColumnType("datetime");

                entity.Property(e => e.Facebook).HasMaxLength(400);

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.FullNameFamily).HasMaxLength(100);

                entity.Property(e => e.Password)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneFamily)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Token)
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCustomerWc>(entity =>
            {
                entity.ToTable("tblCustomerWC");

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SendSms).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblDashBoardDebt>(entity =>
            {
                entity.ToTable("tblDashBoardDebt");

                entity.Property(e => e.ShopName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblDataCic>(entity =>
            {
                entity.ToTable("tblDataCIC");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.Brieft).HasMaxLength(50);

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CheckTime).HasMaxLength(50);

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.StringJson).HasColumnType("ntext");
            });

            modelBuilder.Entity<TblDataPayoo>(entity =>
            {
                entity.ToTable("tblDataPayoo");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateCheck).HasColumnType("datetime");

                entity.Property(e => e.FinCompany).HasMaxLength(50);

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.MoneyCheck).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.NumberCard).HasMaxLength(500);

                entity.Property(e => e.Phone).HasMaxLength(50);
            });

            modelBuilder.Entity<TblDataSimViettel>(entity =>
            {
                entity.ToTable("tblDataSimViettel");

                entity.Property(e => e.AuthorizedFacebook).HasDefaultValueSql("((1))");

                entity.Property(e => e.AuthorizedSim).HasDefaultValueSql("((1))");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DataFacebookOfSimViettel).HasColumnType("ntext");

                entity.Property(e => e.DataTelcoOfSimViettel).HasColumnType("ntext");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDebit>(entity =>
            {
                entity.ToTable("tblDebit");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<TblDevice>(entity =>
            {
                entity.ToTable("tblDevice");

                entity.HasIndex(e => e.DeviceId)
                    .HasName("unique_deviceId")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DeviceId)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDistrict>(entity =>
            {
                entity.ToTable("tblDistrict");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AreaId).HasColumnName("AreaID");

                entity.Property(e => e.LongitudeLatitude).HasMaxLength(200);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.TypeDistrict).HasMaxLength(50);
            });

            modelBuilder.Entity<TblDocument>(entity =>
            {
                entity.ToTable("tblDocument");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Type).HasComment("Loại giấy tờ");
            });

            modelBuilder.Entity<TblDocumentCustomer>(entity =>
            {
                entity.ToTable("tblDocumentCustomer");

                entity.Property(e => e.ConfirmKt3).HasColumnName("ConfirmKT3");
            });

            modelBuilder.Entity<TblDocumentLoanCredit>(entity =>
            {
                entity.ToTable("tblDocumentLoanCredit");
            });

            modelBuilder.Entity<TblDomain>(entity =>
            {
                entity.ToTable("tblDomain");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<TblExcessCashCustomer>(entity =>
            {
                entity.ToTable("tblExcessCashCustomer");

                entity.HasIndex(e => new { e.ForDate, e.CustomerId })
                    .HasName("index_tblExcessCashCustomer");

                entity.Property(e => e.Address).HasMaxLength(256);

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblExpectedPayment>(entity =>
            {
                entity.ToTable("tblExpectedPayment");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.ScheduledInstallmentDate).HasColumnType("date");

                entity.Property(e => e.ScheduledVat).HasColumnName("ScheduledVAT");
            });

            modelBuilder.Entity<TblExtension>(entity =>
            {
                entity.ToTable("tblExtension");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NewDate).HasColumnType("date");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.OldDate).HasColumnType("date");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblExtensionCapital>(entity =>
            {
                entity.ToTable("tblExtensionCapital");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.NewDate).HasColumnType("date");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.OldDate).HasColumnType("date");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblFieldEvaluationReport>(entity =>
            {
                entity.ToTable("tblFieldEvaluationReport");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.ReportDate).HasColumnType("date");

                entity.Property(e => e.Type).HasComment("1: Thẩm định nhà, 2: Thẩm định công ty");

                entity.Property(e => e.TypeReport).HasComment("Loại tồn: 1- Do Khách Hàng, 2- Do nhân viên");
            });

            modelBuilder.Entity<TblFieldSaleManagement>(entity =>
            {
                entity.ToTable("tblFieldSaleManagement");

                entity.Property(e => e.CreationDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblFieldSurveyManagement>(entity =>
            {
                entity.ToTable("tblFieldSurveyManagement");

                entity.Property(e => e.CreationDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblFileManagement>(entity =>
            {
                entity.ToTable("tblFileManagement");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.CustomerBadId).HasColumnName("CustomerBadID");

                entity.Property(e => e.UrlImg)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblGroup>(entity =>
            {
                entity.ToTable("tblGroup");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Permissions)
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblHistory>(entity =>
            {
                entity.ToTable("tblHistory");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(1024);
            });

            modelBuilder.Entity<TblHistoryMoneyShop>(entity =>
            {
                entity.ToTable("tblHistoryMoneyShop");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MoneyTranfer).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasMaxLength(1024);
            });

            modelBuilder.Entity<TblHomeNetwork>(entity =>
            {
                entity.ToTable("tblHomeNetwork");

                entity.Property(e => e.HomeNetwork).HasMaxLength(50);

                entity.Property(e => e.NetworkAlias)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.TextBirtday)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TextCmt)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TextEnd)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TextFullName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblIndividualBusiness>(entity =>
            {
                entity.ToTable("tblIndividualBusiness");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FeeAndInterest).HasComment("Phí và lãi tháng đầu");

                entity.Property(e => e.GroupCic)
                    .HasColumnName("GroupCIC")
                    .HasComment("1: Nhóm 1, 2: Nhóm 2");

                entity.Property(e => e.LowestRevenueMonth).HasComment("Doanh thu tháng thấp nhất");

                entity.Property(e => e.MoneyDeposit).HasComment("Giá trị tiền đặt cọc");

                entity.Property(e => e.MoneyRemainingRent).HasComment("Giá trị tiền thuê còn lại");

                entity.Property(e => e.RemainingTimeContract).HasComment("Thời gian còn lại của HĐ thuê: 1: Tháng, 2: Tháng,3: Tháng");

                entity.Property(e => e.TotalMoney).HasComment("Số tiền có thể vay");

                entity.Property(e => e.TypePlace).HasComment("Loại địa điểm: 0: Nhà riêng chính chủ,1: Thuê cửa hàng");

                entity.Property(e => e.TypeResident).HasComment("Hình thức cứ trú: 1: Hà Nội, 0: Tỉnh khác");
            });

            modelBuilder.Entity<TblInfoCustomerHub>(entity =>
            {
                entity.ToTable("tblInfoCustomerHub");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Street).HasMaxLength(255);
            });

            modelBuilder.Entity<TblInformationOfBusiness>(entity =>
            {
                entity.ToTable("tblInformationOfBusiness");

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.BusinessAddress).HasMaxLength(500);

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyName).HasMaxLength(250);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DateCardNumber).HasColumnType("date");

                entity.Property(e => e.DateCertificate).HasColumnType("date");

                entity.Property(e => e.HeadquarterBusinessRegistration).HasMaxLength(500);

                entity.Property(e => e.IssuedByCardNumber).HasMaxLength(250);

                entity.Property(e => e.IssuedByCertificate).HasMaxLength(250);

                entity.Property(e => e.LoanCreditId).HasColumnName("LoanCreditID");

                entity.Property(e => e.MainBusinessSectors).HasMaxLength(250);

                entity.Property(e => e.Shareholder).HasMaxLength(250);
            });

            modelBuilder.Entity<TblInformationToReceiveSalary>(entity =>
            {
                entity.ToTable("tblInformationToReceiveSalary");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContractDateEnds).HasColumnType("date");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateStartWork).HasColumnType("date");

                entity.Property(e => e.LoanCreditId).HasColumnName("LoanCreditID");

                entity.Property(e => e.ModifyDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserNameCreate).HasMaxLength(50);

                entity.Property(e => e.UserNameModify).HasMaxLength(50);
            });

            modelBuilder.Entity<TblInstallment>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.ShopId })
                    .HasName("PK_tblInstallment_1");

                entity.ToTable("tblInstallment");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.CodeId)
                    .HasColumnName("CodeID")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.FrequencyMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.LastDateOfPay).HasColumnType("date");

                entity.Property(e => e.ManageId)
                    .HasColumnName("ManageID")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NextDate).HasColumnType("date");

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblInstallmentDebit>(entity =>
            {
                entity.HasKey(e => e.DebitId)
                    .HasName("PK__tblInsta__9B556A5865C116E7");

                entity.ToTable("tblInstallmentDebit");

                entity.Property(e => e.DebitId).HasColumnName("DebitID");

                entity.Property(e => e.ForDate).HasColumnType("datetime");

                entity.Property(e => e.InstallmentId).HasColumnName("InstallmentID");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblInstallmentFiles>(entity =>
            {
                entity.ToTable("tblInstallmentFiles");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FilePath)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.InstallmentId).HasColumnName("InstallmentID");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblInstallmentInterestExpected>(entity =>
            {
                entity.HasKey(e => e.InstallmentId);

                entity.ToTable("tblInstallmentInterestExpected");

                entity.HasIndex(e => new { e.InstallmentId, e.ForDate })
                    .HasName("IX_tblInstallmentInterestExpected");

                entity.Property(e => e.InstallmentId)
                    .HasColumnName("InstallmentID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ForDate).HasColumnType("date");
            });

            modelBuilder.Entity<TblInvestment>(entity =>
            {
                entity.ToTable("tblInvestment");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.FullNameUser).HasMaxLength(50);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NumberPhone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Rate).HasColumnType("decimal(18, 4)");
            });

            modelBuilder.Entity<TblJob>(entity =>
            {
                entity.HasKey(e => e.JobId);

                entity.ToTable("tblJob");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblKpicounselors>(entity =>
            {
                entity.ToTable("tblKPICounselors");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.NumberKpi).HasColumnName("NumberKPI");
            });

            modelBuilder.Entity<TblLenderSpicesConfig>(entity =>
            {
                entity.HasKey(e => e.ShopId)
                    .HasName("PK__tmp_ms_x__67C557C9A7DC71B6");

                entity.ToTable("tblLenderSpicesConfig");

                entity.Property(e => e.ShopId).ValueGeneratedNever();

                entity.Property(e => e.LoanTimes)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblLendingCertificateInformation>(entity =>
            {
                entity.ToTable("tblLendingCertificateInformation");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerAddress).HasMaxLength(1024);

                entity.Property(e => e.CustomerContactNumber)
                    .HasColumnName("CustomerContact_number")
                    .HasMaxLength(20);

                entity.Property(e => e.CustomerDateOfBirth)
                    .HasColumnName("CustomerDate_of_birth")
                    .HasMaxLength(20);

                entity.Property(e => e.CustomerEmail).HasMaxLength(150);

                entity.Property(e => e.CustomerFullname).HasMaxLength(250);

                entity.Property(e => e.CustomerIdCard)
                    .HasColumnName("CustomerId_card")
                    .HasMaxLength(20);

                entity.Property(e => e.CustomerPhone).HasMaxLength(20);

                entity.Property(e => e.FromDate)
                    .HasColumnName("From_date")
                    .HasMaxLength(10);

                entity.Property(e => e.FromHour)
                    .HasColumnName("From_hour")
                    .HasMaxLength(10);

                entity.Property(e => e.FromMonth)
                    .HasColumnName("From_month")
                    .HasMaxLength(10);

                entity.Property(e => e.FromYear)
                    .IsRequired()
                    .HasColumnName("From_year")
                    .HasMaxLength(10);

                entity.Property(e => e.InvestorAddress).HasMaxLength(1024);

                entity.Property(e => e.InvestorContactNumber)
                    .HasColumnName("InvestorContact_number")
                    .HasMaxLength(20);

                entity.Property(e => e.InvestorDateOfBirth)
                    .HasColumnName("InvestorDate_of_birth")
                    .HasMaxLength(20);

                entity.Property(e => e.InvestorEmail).HasMaxLength(150);

                entity.Property(e => e.InvestorFullname).HasMaxLength(250);

                entity.Property(e => e.InvestorIdCard)
                    .HasColumnName("InvestorId_card")
                    .HasMaxLength(20);

                entity.Property(e => e.InvestorInvestMoney)
                    .HasColumnName("InvestorInvest_money")
                    .HasMaxLength(50);

                entity.Property(e => e.InvestorPhone).HasMaxLength(20);

                entity.Property(e => e.InvestorProduct).HasMaxLength(500);

                entity.Property(e => e.InvestorRate).HasMaxLength(10);

                entity.Property(e => e.InvestorTaxCode)
                    .HasColumnName("InvestorTax_code")
                    .HasMaxLength(20);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.MoneyIndemnification)
                    .HasColumnName("Money_Indemnification")
                    .HasMaxLength(20);

                entity.Property(e => e.NumberInsurance).HasMaxLength(50);

                entity.Property(e => e.ToDate)
                    .HasColumnName("To_date")
                    .HasMaxLength(10);

                entity.Property(e => e.ToHour)
                    .HasColumnName("To_hour")
                    .HasMaxLength(10);

                entity.Property(e => e.ToMonth)
                    .HasColumnName("To_month")
                    .HasMaxLength(10);

                entity.Property(e => e.ToYear)
                    .HasColumnName("To_year")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<TblLiquidation>(entity =>
            {
                entity.ToTable("tblLiquidation");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Content).HasMaxLength(250);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.JsonImage).HasMaxLength(250);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.NameAction).HasMaxLength(250);

                entity.Property(e => e.NameReceiver).HasMaxLength(50);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.UsernameAction).HasMaxLength(250);
            });

            modelBuilder.Entity<TblLivingTime>(entity =>
            {
                entity.HasKey(e => e.LivingTimeId);

                entity.ToTable("tblLivingTime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<TblLoan>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.ShopId });

                entity.ToTable("tblLoan");

                entity.HasIndex(e => new { e.ShopId, e.Status })
                    .HasName("NonClusteredIndex-20190322-222024");

                entity.HasIndex(e => new { e.ShopId, e.FromDate, e.ToDate, e.Status })
                    .HasName("NonClusteredIndex-20190326-102259");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.AlarmDate).HasColumnType("datetime");

                entity.Property(e => e.AlarmNote).HasMaxLength(450);

                entity.Property(e => e.BadDebtTransferDate).HasColumnType("datetime");

                entity.Property(e => e.BriefId)
                    .HasColumnName("BriefID")
                    .HasDefaultValueSql("((1))")
                    .HasComment("Hồ sơ đang lưu ở bước bộ phận nào. 1: Ký HD , 2: Kế toán , 3: Đại Lý");

                entity.Property(e => e.ChannelSale).HasComment("Kênh bán");

                entity.Property(e => e.CityId).HasColumnName("CityID");

                entity.Property(e => e.CodeId)
                    .HasColumnName("CodeID")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Collateral).HasMaxLength(1024);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DebitMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.DebitMoneyFineLate).HasDefaultValueSql("((0))");

                entity.Property(e => e.DistrictId).HasColumnName("DistrictID");

                entity.Property(e => e.FinishDate).HasColumnType("date");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.GetInformationInsuranVib)
                    .HasColumnName("GetInformationInsuranVIB")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.HubId).HasColumnName("HubID");

                entity.Property(e => e.InsuranceCompensatorId)
                    .HasColumnName("InsuranceCompensatorID")
                    .HasComment("bên đền bù bảo hiểm");

                entity.Property(e => e.InterestToDay).HasDefaultValueSql("((0))");

                entity.Property(e => e.KeepOriginal).HasComment("Có giữ chứng từ gốc hay không");

                entity.Property(e => e.LastDateOfPay).HasColumnType("date");

                entity.Property(e => e.LastUserId).HasColumnName("LastUserID");

                entity.Property(e => e.LinkGcnchoVay)
                    .HasColumnName("LinkGCNChoVay")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LinkGcnvay)
                    .HasColumnName("LinkGCNVay")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LoanTime)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Thời gian vay");

                entity.Property(e => e.MarkLoan).HasDefaultValueSql("((5))");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NextDate).HasColumnType("date");

                entity.Property(e => e.Note).HasMaxLength(1024);

                entity.Property(e => e.OverMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.PaymentMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.RateAffLender).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RateLender).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RegistrationContractId)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RegistrationCustomerId)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.StatusSendInsurance).HasDefaultValueSql("((0))");

                entity.Property(e => e.SynchronizationVbi)
                    .HasColumnName("SynchronizationVBI")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.Property(e => e.TotalInterest).HasDefaultValueSql("((0))");

                entity.Property(e => e.TypeCloseLoan)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Kiểu đóng hợp đồng Thanh Lý / Đóng tất toán");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.ValidDocuments).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TblLoanBadDebitReport>(entity =>
            {
                entity.ToTable("tblLoanBadDebitReport");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.LastDateOfPay).HasColumnType("date");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");

                entity.Property(e => e.TypeNote).HasMaxLength(50);
            });

            modelBuilder.Entity<TblLoanBadDebitReportDaily>(entity =>
            {
                entity.ToTable("tblLoanBadDebitReportDaily");

                entity.Property(e => e.CodeId).HasColumnName("CodeID");

                entity.Property(e => e.CountProcess).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreationTime).HasColumnType("datetime");

                entity.Property(e => e.CusAddress).HasMaxLength(256);

                entity.Property(e => e.CusId).HasColumnName("CusID");

                entity.Property(e => e.CusName).HasMaxLength(50);

                entity.Property(e => e.CusPhone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.LastDateOfPay).HasColumnType("date");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.ProductNote).HasMaxLength(50);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ShopName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusDoing).HasDefaultValueSql("((0))");

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<TblLoanCredit>(entity =>
            {
                entity.ToTable("tblLoanCredit");

                entity.HasIndex(e => new { e.CustomerCreditId, e.Status, e.OwerShopId, e.LoanId, e.TypeId })
                    .HasName("NonClusteredIndex-20190322-222102");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AccountNumberBanking)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressFieldSurveyMeetingTime)
                    .HasColumnType("datetime")
                    .HasComment("Thẩm định thực địa hẹn giờ đi gặp khách hàng tại nhà");

                entity.Property(e => e.AddressFieldSurveyTime)
                    .HasColumnType("datetime")
                    .HasComment("thời gian TĐTĐ bắt đầu khảo sát nhà");

                entity.Property(e => e.AffCode).HasMaxLength(50);

                entity.Property(e => e.AffPub)
                    .HasColumnName("aff_pub")
                    .HasMaxLength(200);

                entity.Property(e => e.AffSid)
                    .HasColumnName("Aff_Sid")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AffSource)
                    .HasColumnName("aff_source")
                    .HasMaxLength(200);

                entity.Property(e => e.AgencyBuyInsurance).HasDefaultValueSql("((1))");

                entity.Property(e => e.AgentLoanId).HasColumnName("AgentLoanID");

                entity.Property(e => e.ApproveDate).HasColumnType("datetime");

                entity.Property(e => e.AwardContractId)
                    .HasColumnName("AwardContractID")
                    .HasComment("Người đi ký HĐ");

                entity.Property(e => e.BeginStartTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.BodId).HasColumnName("BodID");

                entity.Property(e => e.CancelDateLoanCredit).HasColumnType("datetime");

                entity.Property(e => e.ChangedAi).HasDefaultValueSql("((0))");

                entity.Property(e => e.CompanyFieldSurveyMeetingTime)
                    .HasColumnType("datetime")
                    .HasComment("Thẩm định thực địa hẹn giờ đi gặp khách hàng tại công ty");

                entity.Property(e => e.CompanyFieldSurveyTime)
                    .HasColumnType("datetime")
                    .HasComment("thời gian TĐTĐ bắt đầu khảo sát cty");

                entity.Property(e => e.CompanyFieldSurveyUserId).HasColumnName("CompanyFieldSurveyUserID");

                entity.Property(e => e.ConfirmationProfile)
                    .HasDefaultValueSql("((0))")
                    .HasComment("dùng để xác thực hồ sơ");

                entity.Property(e => e.CoordinatorId).HasColumnName("CoordinatorID");

                entity.Property(e => e.CounselorDate).HasColumnType("datetime");

                entity.Property(e => e.CounselorId).HasColumnName("CounselorID");

                entity.Property(e => e.CountSms).HasColumnName("CountSMS");

                entity.Property(e => e.CreateByUserName).HasMaxLength(250);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DateTimePushSan).HasColumnType("datetime");

                entity.Property(e => e.DeviceId)
                    .HasColumnName("DeviceID")
                    .HasMaxLength(50);

                entity.Property(e => e.DomainName).HasMaxLength(50);

                entity.Property(e => e.Dti)
                    .HasColumnName("DTI")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.FieldSurveyUserId)
                    .HasColumnName("FieldSurveyUserID")
                    .HasComment("Thẩm định thực địa");

                entity.Property(e => e.FirstModifyDate).HasColumnType("datetime");

                entity.Property(e => e.FirstTimeHubFeedBack).HasColumnType("datetime");

                entity.Property(e => e.FirstTimeHubReceived).HasColumnType("datetime");

                entity.Property(e => e.FirstTimeLenderCareReceived).HasColumnType("datetime");

                entity.Property(e => e.Frequency).HasDefaultValueSql("((1))");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.GiftCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HubEmployeId).HasComment("Nhân viên cửa hàng phụ trách khoản vay");

                entity.Property(e => e.IdOfPartenter)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.IsCheckCic)
                    .HasColumnName("IsCheckCIC")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsGetLeadScoring).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsHeadOffice)
                    .HasDefaultValueSql("((1))")
                    .HasComment("Đơn này có phải của Hội Sở hay không. 0 là không phải, 1 là phải");

                entity.Property(e => e.IsLocate).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsSan).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsTakenRegistrationCertificate).HasComment("1: TĐTĐ đã thu giấy đăng ký xe/ cà vẹt xe của khách hàng");

                entity.Property(e => e.IsTrackingLocation).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsVerify).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsVerifyOtpLdp).HasColumnName("IsVerifyOtpLDP");

                entity.Property(e => e.LabelLocation).HasMaxLength(500);

                entity.Property(e => e.LabelScore).HasMaxLength(50);

                entity.Property(e => e.LabelVerifyLocation).HasMaxLength(500);

                entity.Property(e => e.LastCall).HasColumnType("datetime");

                entity.Property(e => e.LastDateSendSms).HasColumnType("datetime");

                entity.Property(e => e.LastUpdateOfVay1h).HasColumnType("datetime");

                entity.Property(e => e.LeadScore).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.LeaderId).HasColumnName("LeaderID");

                entity.Property(e => e.LinkAuthorization).HasMaxLength(500);

                entity.Property(e => e.LinkGcnchoVay)
                    .HasColumnName("LinkGCNChoVay")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LinkGcnvay)
                    .HasColumnName("LinkGCNVay")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ListCheckLeadQualify).HasMaxLength(50);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.LoanMarkId).HasComment("Để lưu chấm điểm khoản vay");

                entity.Property(e => e.LoanProposed).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MinLoanProposed).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NameBanking).HasMaxLength(250);

                entity.Property(e => e.NameCardHolder)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NextDate).HasColumnType("datetime");

                entity.Property(e => e.NumberCardBanking)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumberCheckCic).HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberPassDataToAi).HasDefaultValueSql("((0))");

                entity.Property(e => e.OrderSourceId).HasComment("Khóa ngoại tblOrderSource (dùng để báo cáo động ) được gán từ tự động");

                entity.Property(e => e.OrderSourceParentId).HasComment("cột parentId của bảng  tblOrderSource (dùng để báo cáo động ) được gán từ tự động");

                entity.Property(e => e.OtherPartnerLoanInfo).HasColumnType("ntext");

                entity.Property(e => e.PaymentLeaseTime).HasColumnType("datetime");

                entity.Property(e => e.PriorityContract).HasDefaultValueSql("((0))");

                entity.Property(e => e.PushToSmartDailer).HasDefaultValueSql("((0))");

                entity.Property(e => e.Rate).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RateAffLender).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RateConsultant).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RateLender)
                    .HasColumnType("decimal(18, 6)")
                    .HasDefaultValueSql("((18))");

                entity.Property(e => e.RateService).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RateType).HasDefaultValueSql("((6))");

                entity.Property(e => e.ReBorrow).HasDefaultValueSql("((0))");

                entity.Property(e => e.ReMarketing).HasDefaultValueSql("((0))");

                entity.Property(e => e.ReMarketingLoanCreditId).HasComment("Đơn vay được Remarketing từ đơn nào");

                entity.Property(e => e.ReasonId).HasColumnName("ReasonID");

                entity.Property(e => e.RefCodeLocation).HasMaxLength(100);

                entity.Property(e => e.RequestIdOtp)
                    .HasColumnName("RequestIdOTP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ResultCac)
                    .HasColumnName("ResultCAC")
                    .HasMaxLength(50);

                entity.Property(e => e.ResultLocation).HasMaxLength(50);

                entity.Property(e => e.Score).HasComment("Điểm AI chấm");

                entity.Property(e => e.SourceProduct).HasColumnName("source_product");

                entity.Property(e => e.StatusOfDeviceId)
                    .HasColumnName("StatusOfDeviceID")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SupportId).HasColumnName("SupportID");

                entity.Property(e => e.SupportLastId).HasColumnName("SupportLastID");

                entity.Property(e => e.SynchronizationDataPartenter).HasDefaultValueSql("((0))");

                entity.Property(e => e.Tid)
                    .HasColumnName("TId")
                    .HasMaxLength(50);

                entity.Property(e => e.TimeSendActiveDevice).HasColumnType("datetime");

                entity.Property(e => e.TimeSendOtp)
                    .HasColumnName("TimeSendOTP")
                    .HasColumnType("datetime");

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.Property(e => e.TotalMoneyExpertise).HasComment("Giá thẩm định xe ( chưa giảm trừ theo tiêu chí thẩm định, chỉ giảm trừ theo năm và theo hộ khẩu nội - ngoại tỉnh )");

                entity.Property(e => e.TotalMoneyExpertiseLast).HasComment("Giá thẩm định xe sau khi đã trừ đi các tiêu chí");

                entity.Property(e => e.TotalMoneyOld).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TsOtp)
                    .HasColumnName("TS_Otp")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TypeId)
                    .HasColumnName("TypeID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TypeTimerId).HasComment("Loại hẹn giờ 1: KH Hẹn, 2: KH Follow");

                entity.Property(e => e.UserIdcancel).HasColumnName("UserIDCancel");

                entity.Property(e => e.UtmCampaign)
                    .HasColumnName("utm_campaign")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UtmContent)
                    .HasColumnName("utm_content")
                    .HasMaxLength(250);

                entity.Property(e => e.UtmMedium)
                    .HasColumnName("utm_medium")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UtmSource)
                    .HasColumnName("utm_source")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UtmTerm)
                    .HasColumnName("utm_term")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<TblLoanCreditHide>(entity =>
            {
                entity.ToTable("tblLoanCreditHide");

                entity.HasComment("Bảng Mapping những đơn vay bị ẩn không hiển thị trên chợ đơn của đại lý");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblLoanCreditOpenning>(entity =>
            {
                entity.HasKey(e => e.LoanCreditOpenningId)
                    .HasName("PK_tblLoanCreditOpening");

                entity.ToTable("tblLoanCreditOpenning");

                entity.HasComment(@"-- Theo Quận/Huyện/TVV
OpenningType = 1 (Call receive)
OpenningType = 10 (Counselor receive)
OpenningType = 25 (Counselor receive)
OpenningType = 15 (Coordinator receive)
OpenningType = 20 (Agency receive)
OpenningType = 30 (Accountant receive)");

                entity.HasIndex(e => new { e.LoanCreditId, e.OpenningDay })
                    .HasName("index_tblLoanCreditOpenning");

                entity.Property(e => e.LoanCreditOpenningId).HasColumnName("LoanCreditOpenningID");

                entity.Property(e => e.CounselorName).HasMaxLength(50);

                entity.Property(e => e.DistrictId).HasColumnName("DistrictID");

                entity.Property(e => e.DistrictName).HasMaxLength(50);

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.FullName2).HasMaxLength(50);

                entity.Property(e => e.GroupUserId).HasColumnName("GroupUserID");

                entity.Property(e => e.LoanCreditId).HasColumnName("LoanCreditID");

                entity.Property(e => e.OpenningDay)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OpenningNote).HasMaxLength(50);

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLoanCreditOpenningField>(entity =>
            {
                entity.HasKey(e => new { e.LoanCreditOpenningFieldId, e.LoanCreditId, e.UserId, e.ProductId, e.CityId, e.DistrictId });

                entity.ToTable("tblLoanCreditOpenningField");

                entity.Property(e => e.LoanCreditOpenningFieldId)
                    .HasColumnName("LoanCreditOpenningFieldID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.LoanCreditId).HasColumnName("LoanCreditID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.CustomerCreditName).HasMaxLength(50);

                entity.Property(e => e.DistrictName).HasMaxLength(50);

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.IsThamDinh).HasComment("1: Thẩm Định Nhà, 2: Thẩm Định Công Ty");

                entity.Property(e => e.OpenningDay).HasColumnType("date");
            });

            modelBuilder.Entity<TblLoanCreditOtpTs>(entity =>
            {
                entity.ToTable("tblLoanCreditOtpTS");

                entity.Property(e => e.Createdate).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.Otp).HasColumnName("OTP");

                entity.Property(e => e.TradingCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblLoanCreditOverView>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.LoanCreditId, e.Status, e.GroupId, e.Steps })
                    .HasName("PK_Table_1");

                entity.ToTable("tblLoanCreditOverView");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Status).HasComment("1: Nhận về, 0: hủy tạm thời, 2: chuyển đi, -1: hủy hẳn, 3: Chờ bổ xung,4: Lấy lại");

                entity.Property(e => e.GroupId).HasComment("9: Support , 10: TVV, 11: Kế toán , 12: KSNB , 99: Đại Lý");

                entity.Property(e => e.AwardContractId).HasColumnName("AwardContractID");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.ContentCancel).HasMaxLength(500);

                entity.Property(e => e.Count).HasDefaultValueSql("((1))");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.DistrictName).HasMaxLength(50);

                entity.Property(e => e.FieldSurveyUserId).HasColumnName("FieldSurveyUserID");

                entity.Property(e => e.IsCancel).HasDefaultValueSql("((0))");

                entity.Property(e => e.LoanCreditCreateDate).HasColumnType("datetime");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblLoanCreditPriorityContract>(entity =>
            {
                entity.ToTable("tblLoanCreditPriorityContract");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.DistrictName).HasMaxLength(50);

                entity.Property(e => e.FinishDate).HasColumnType("date");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.JobName).HasMaxLength(250);

                entity.Property(e => e.ProductName).HasMaxLength(50);

                entity.Property(e => e.ReMarketing).HasDefaultValueSql("((0))");

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblLoanCreditProduct>(entity =>
            {
                entity.ToTable("tblLoanCreditProduct");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModelPhone).HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ValuationAsset).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.YearMade).HasMaxLength(50);
            });

            modelBuilder.Entity<TblLoanCreditProductAttribute>(entity =>
            {
                entity.ToTable("tblLoanCredit_ProductAttribute");
            });

            modelBuilder.Entity<TblLoanCreditShop>(entity =>
            {
                entity.HasKey(e => new { e.ShopId, e.LoanCreditId });

                entity.ToTable("tblLoanCreditShop");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NextSendSms).HasColumnType("datetime");

                entity.Property(e => e.ShopName).HasMaxLength(255);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TblLoanCreditSynchronizePlatformTima>(entity =>
            {
                entity.ToTable("tblLoanCreditSynchronizePlatformTima");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Synchronized).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblLoanCreditThirdParty>(entity =>
            {
                entity.ToTable("tblLoanCreditThirdParty");
            });

            modelBuilder.Entity<TblLoanDebit>(entity =>
            {
                entity.HasKey(e => e.DebitId)
                    .HasName("PK__tblLoanD__9B556A586991A7CB");

                entity.ToTable("tblLoanDebit");

                entity.Property(e => e.DebitId).HasColumnName("DebitID");

                entity.Property(e => e.BankCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ForDate).HasColumnType("datetime");

                entity.Property(e => e.ForeignKeyDebitId).HasColumnName("ForeignKeyDebitID");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLoanDebitReport>(entity =>
            {
                entity.ToTable("tblLoanDebitReport");

                entity.HasIndex(e => new { e.LoanId, e.CreateDate })
                    .HasName("index_tblLoanDebitReport");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.LastDateOfPay).HasColumnType("date");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");

                entity.Property(e => e.TypeNote).HasMaxLength(50);
            });

            modelBuilder.Entity<TblLoanExtra>(entity =>
            {
                entity.ToTable("tblLoanExtra");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CodeBank)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateExtra).HasColumnType("date");

                entity.Property(e => e.Fined).HasComment("Phạt gốc trả trước hay không");

                entity.Property(e => e.LoanExtraId).HasColumnName("LoanExtraID");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.MoneyFined).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLoanIndemnifyInsurrance>(entity =>
            {
                entity.ToTable("tblLoanIndemnifyInsurrance");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.InsuranceCompensatorId).HasColumnName("InsuranceCompensatorID");
            });

            modelBuilder.Entity<TblLoanInterestExpected>(entity =>
            {
                entity.ToTable("tblLoanInterestExpected");

                entity.HasIndex(e => new { e.LoanId, e.ForDate })
                    .HasName("IX_tblLoanInterestExpected");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");
            });

            modelBuilder.Entity<TblLoanStatus>(entity =>
            {
                entity.ToTable("tblLoanStatus");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description).HasMaxLength(512);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<TblLocation>(entity =>
            {
                entity.ToTable("tblLocation");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Device)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Lat)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Long)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLogAccountExtend>(entity =>
            {
                entity.ToTable("tblLogAccountExtend");

                entity.Property(e => e.AccountCustomer).HasMaxLength(50);

                entity.Property(e => e.CityId).HasColumnName("CityID");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.CreadateOn).HasColumnType("datetime");

                entity.Property(e => e.IsSettlement).HasDefaultValueSql("((1))");

                entity.Property(e => e.NameBus).HasMaxLength(50);

                entity.Property(e => e.NameUser).HasMaxLength(50);

                entity.Property(e => e.Note).HasMaxLength(500);
            });

            modelBuilder.Entity<TblLogActionThn>(entity =>
            {
                entity.ToTable("tblLogActionTHN");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FinishTime).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(250);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Note).HasMaxLength(250);

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLogAlarmDate>(entity =>
            {
                entity.ToTable("tblLogAlarmDate");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AlarmDate).HasColumnType("datetime");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Note).HasMaxLength(250);

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLogBehaviorHub>(entity =>
            {
                entity.HasKey(e => e.LogId)
                    .HasName("PK__tblLogBe__5E5499A8933625E2");

                entity.ToTable("tblLogBehaviorHub");

                entity.Property(e => e.LogId).HasColumnName("LogID");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ShopName).HasMaxLength(255);
            });

            modelBuilder.Entity<TblLogBehaviorTelesale>(entity =>
            {
                entity.HasKey(e => e.LogId)
                    .HasName("PK__tblLogBe__5E5499A8DAB4F57B");

                entity.ToTable("tblLogBehaviorTelesale");

                entity.Property(e => e.LogId).HasColumnName("LogID");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(255);

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLogBrief>(entity =>
            {
                entity.ToTable("tblLogBrief");

                entity.Property(e => e.BriefId).HasColumnName("BriefID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.UserName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblLogCallApi>(entity =>
            {
                entity.ToTable("tblLogCallApi");

                entity.HasIndex(e => new { e.ActionCallApi, e.LoanId, e.LoanCreditId, e.CreateOn })
                    .HasName("index_tblLogCallApi");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.InputStringCallApi).HasColumnType("ntext");

                entity.Property(e => e.LinkCallApi).HasMaxLength(500);

                entity.Property(e => e.ModifyOn).HasColumnType("datetime");

                entity.Property(e => e.NameActionCallApi).HasMaxLength(50);

                entity.Property(e => e.ResultStringCallApi).HasColumnType("ntext");
            });

            modelBuilder.Entity<TblLogCancelCustomerInSight>(entity =>
            {
                entity.ToTable("tblLogCancelCustomerInSight");
            });

            modelBuilder.Entity<TblLogCancelMmo>(entity =>
            {
                entity.ToTable("tblLogCancelMMO");

                entity.Property(e => e.CityName).HasMaxLength(150);

                entity.Property(e => e.Content).HasMaxLength(500);

                entity.Property(e => e.CreateLoan).HasColumnType("datetime");

                entity.Property(e => e.CreateOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DistrictName).HasMaxLength(150);

                entity.Property(e => e.FullName).HasMaxLength(250);

                entity.Property(e => e.FullNameCustomer).HasMaxLength(250);

                entity.Property(e => e.ProductName).HasMaxLength(150);

                entity.Property(e => e.TimeLoan).HasMaxLength(150);
            });

            modelBuilder.Entity<TblLogChangeLoanBelongTo>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__tblLogCh__3214EC0622D3291B")
                    .IsClustered(false);

                entity.ToTable("tblLogChangeLoanBelongTo");

                entity.HasIndex(e => new { e.Id, e.CreatorId, e.LoanId, e.CreationTime })
                    .HasName("UQ__tblLogCh__9FDF06670AA3D369")
                    .IsUnique()
                    .IsClustered();

                entity.Property(e => e.CreationTime).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(100);
            });

            modelBuilder.Entity<TblLogChangePhone>(entity =>
            {
                entity.ToTable("tblLogChangePhone");

                entity.Property(e => e.CreateOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PhoneNew)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneOld)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UserFullName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblLogChangeValidDocuments>(entity =>
            {
                entity.ToTable("tblLogChangeValidDocuments");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.UserName)
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblLogCheckLoanAi>(entity =>
            {
                entity.ToTable("tblLogCheckLoanAI");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LoanCreditId).HasColumnName("LoanCreditID");

                entity.Property(e => e.RefCode).HasMaxLength(200);

                entity.Property(e => e.ResultFinal).HasMaxLength(50);

                entity.Property(e => e.Token).HasMaxLength(500);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(500);
            });

            modelBuilder.Entity<TblLogCheckMoney>(entity =>
            {
                entity.ToTable("tblLogCheckMoney");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BeginDate).HasColumnType("datetime");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ShopName).HasMaxLength(500);
            });

            modelBuilder.Entity<TblLogClickToCall>(entity =>
            {
                entity.ToTable("tblLogClickToCall");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FullNameCall).HasMaxLength(50);

                entity.Property(e => e.NameOfCustomer).HasMaxLength(50);

                entity.Property(e => e.PhoneOfCustomer)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UserNameCall).HasMaxLength(50);
            });

            modelBuilder.Entity<TblLogConfirmSuspendedMoney>(entity =>
            {
                entity.ToTable("tblLogConfirmSuspendedMoney");

                entity.Property(e => e.Content).HasMaxLength(500);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.UserName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblLogConvertHub>(entity =>
            {
                entity.HasKey(e => e.LogId)
                    .HasName("PK__tblLogCo__5E5499A8C6A002C6");

                entity.ToTable("tblLogConvertHub");

                entity.Property(e => e.LogId).HasColumnName("LogID");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ShopName).HasMaxLength(255);
            });

            modelBuilder.Entity<TblLogConvertTelesale>(entity =>
            {
                entity.HasKey(e => e.LogId);

                entity.ToTable("tblLogConvertTelesale");

                entity.Property(e => e.LogId).HasColumnName("LogID");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(255);

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLogCriteria>(entity =>
            {
                entity.HasKey(e => e.LogCriteriaId);

                entity.ToTable("tblLogCriteria");

                entity.HasComment("Lưu log tiêu chí thẩm định");

                entity.Property(e => e.Createdate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblLogCustomer>(entity =>
            {
                entity.ToTable("tblLogCustomer");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(250);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.Username).HasMaxLength(250);
            });

            modelBuilder.Entity<TblLogInformationDisbursement>(entity =>
            {
                entity.ToTable("tblLogInformationDisbursement");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.InfomationCustomerCredit).HasColumnType("ntext");

                entity.Property(e => e.InformationLoanCredit).HasColumnType("ntext");
            });

            modelBuilder.Entity<TblLogLiquid>(entity =>
            {
                entity.ToTable("tblLogLiquid");

                entity.Property(e => e.AccountantComment).HasMaxLength(500);

                entity.Property(e => e.AccountantUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AgencyComment).HasMaxLength(500);

                entity.Property(e => e.AgencyUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DebtMoney).HasComment("Tiền phải trả");

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.LiquidMoney).HasComment("-- Tiền khách đưa");

                entity.Property(e => e.ShopId).HasComment("--- Id agency");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblLogLoanByDay>(entity =>
            {
                entity.HasKey(e => new { e.ForDate, e.LoanId })
                    .HasName("PK_tblLogLoanByDay1");

                entity.ToTable("tblLogLoanByDay");

                entity.HasIndex(e => e.FromDate)
                    .HasName("index_tblLogLoanByDay");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.CodeId).HasColumnName("CodeID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.HubId).HasColumnName("HubID");

                entity.Property(e => e.LastDateOfPay).HasColumnType("date");

                entity.Property(e => e.NextDate).HasColumnType("date");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<TblLogLoanCreditIdentical>(entity =>
            {
                entity.ToTable("tblLogLoanCreditIdentical");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblLogLoanCreditPushHub>(entity =>
            {
                entity.ToTable("tblLogLoanCreditPushHub");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CityName).HasMaxLength(250);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DistrictName).HasMaxLength(250);

                entity.Property(e => e.FullName).HasMaxLength(250);

                entity.Property(e => e.LoanCreditId).HasColumnName("LoanCreditID");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.ShopName).HasMaxLength(250);
            });

            modelBuilder.Entity<TblLogLocationThn>(entity =>
            {
                entity.ToTable("tblLogLocationTHN");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.LatUser)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LongUser)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLogLogIn>(entity =>
            {
                entity.ToTable("tblLogLogIn");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.Ip)
                    .HasColumnName("IP")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UserName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblLogMessagePayment>(entity =>
            {
                entity.ToTable("tblLogMessagePayment");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Input).HasColumnType("ntext");

                entity.Property(e => e.MessageError).HasColumnType("ntext");

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblLogMoneyFee>(entity =>
            {
                entity.ToTable("tblLogMoneyFee");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.DateToPayInterest).HasColumnType("date");
            });

            modelBuilder.Entity<TblLogOneSms>(entity =>
            {
                entity.ToTable("tblLogOneSMS");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CountSms)
                    .HasColumnName("CountSMS")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.IdoneSms).HasColumnName("IDOneSMS");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Smscontent)
                    .HasColumnName("SMSContent")
                    .HasMaxLength(1024)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblLogPartnerDealingBadDebts>(entity =>
            {
                entity.ToTable("tblLogPartnerDealingBadDebts");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FullNameAssign).HasMaxLength(250);

                entity.Property(e => e.FullNameCreate).HasMaxLength(250);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Reason).HasMaxLength(500);

                entity.Property(e => e.ReasonId).HasColumnName("ReasonID");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UserAssignId).HasColumnName("UserAssignID");

                entity.Property(e => e.UserCreateId).HasColumnName("UserCreateID");

                entity.Property(e => e.UserNameAssign).HasMaxLength(250);

                entity.Property(e => e.UserNameCreate).HasMaxLength(250);
            });

            modelBuilder.Entity<TblLogPayBack>(entity =>
            {
                entity.ToTable("tblLogPayBack");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.UserName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblLogPaymentNotify>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.TypeId, e.ShopId, e.ForDate });

                entity.ToTable("tblLogPaymentNotify");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ForDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.AgencyId).HasColumnName("AgencyID");

                entity.Property(e => e.AgencyName).HasMaxLength(50);

                entity.Property(e => e.AutoId)
                    .HasColumnName("AutoID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CodeId).HasColumnName("CodeID");

                entity.Property(e => e.CodeItem)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CountProcess).HasDefaultValueSql("((0))");

                entity.Property(e => e.CusAddress).HasMaxLength(256);

                entity.Property(e => e.CusId).HasColumnName("CusID");

                entity.Property(e => e.CusName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CusPhone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DebitMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsKeep).HasDefaultValueSql("((0))");

                entity.Property(e => e.ItemName).HasMaxLength(512);

                entity.Property(e => e.ManageId).HasColumnName("ManageID");

                entity.Property(e => e.Note).HasMaxLength(100);

                entity.Property(e => e.Number).HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberCard)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PayMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.PhoneRelationShip).HasMaxLength(500);

                entity.Property(e => e.ProductName).HasMaxLength(50);

                entity.Property(e => e.RegistrationContractId)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RegistrationCustomerId)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblLogProcessStaffThn>(entity =>
            {
                entity.ToTable("tblLogProcessStaffTHN");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BreakTime).HasColumnType("datetime");

                entity.Property(e => e.CountProcess).HasDefaultValueSql("((0))");

                entity.Property(e => e.FinishBreakTime).HasColumnType("datetime");

                entity.Property(e => e.FinishTime).HasColumnType("datetime");

                entity.Property(e => e.ForDate).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.StatusDoing).HasDefaultValueSql("((0))");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLogPushPredictive>(entity =>
            {
                entity.ToTable("tblLogPushPredictive");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.Token).HasMaxLength(500);

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(500);
            });

            modelBuilder.Entity<TblLogSendOtp>(entity =>
            {
                entity.ToTable("tblLogSendOtp");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Domain).HasMaxLength(250);

                entity.Property(e => e.Otp)
                    .HasColumnName("OTP")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Response).HasMaxLength(1024);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Token).HasMaxLength(250);

                entity.Property(e => e.Url).HasMaxLength(250);
            });

            modelBuilder.Entity<TblLogSendStatusInsurance>(entity =>
            {
                entity.ToTable("tblLogSendStatusInsurance");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLogShop>(entity =>
            {
                entity.ToTable("tblLogShop");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email).HasMaxLength(250);

                entity.Property(e => e.InviteCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.OwerNameOfShop).HasMaxLength(500);

                entity.Property(e => e.PersonAvatar).HasMaxLength(500);

                entity.Property(e => e.PersonBirthDay).HasColumnType("date");

                entity.Property(e => e.PersonCardNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PersonContact).HasMaxLength(50);

                entity.Property(e => e.PersonContactPhone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PersonGender).HasComment("0: nam , 1: nữ");

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RateAff).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RateInterest)
                    .HasColumnType("decimal(18, 6)")
                    .HasComment("Lãi suất tính theo đồng của Lender");

                entity.Property(e => e.RateLender).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.Represent).HasMaxLength(100);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLogSupport>(entity =>
            {
                entity.ToTable("tblLogSupport");

                entity.Property(e => e.Comment).HasMaxLength(1000);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(100);
            });

            modelBuilder.Entity<TblLogThnKpireportByDay>(entity =>
            {
                entity.HasKey(e => new { e.Fordate, e.UserId, e.TypeId });

                entity.ToTable("tblLogThnKPIReportByDay");

                entity.Property(e => e.Fordate).HasColumnType("date");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");

                entity.Property(e => e.CurrentUserName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.TimeSearch).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblLogTimeCollector>(entity =>
            {
                entity.ToTable("tblLogTimeCollector");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblLogTrackingCall>(entity =>
            {
                entity.ToTable("tblLogTrackingCall");

                entity.Property(e => e.AgentConnectTime)
                    .HasColumnName("agent_connect_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.AgentIpphone).HasColumnName("agent_ipphone");

                entity.Property(e => e.AgentRingTime)
                    .HasColumnName("agent_ring_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.AgentUserId).HasColumnName("agent_user_id");

                entity.Property(e => e.CallAttemptIndex).HasColumnName("call_attempt_index");

                entity.Property(e => e.CallDuration).HasColumnName("call_duration");

                entity.Property(e => e.CallId)
                    .HasColumnName("call_id")
                    .HasMaxLength(100);

                entity.Property(e => e.CampaignDataId).HasColumnName("campaign_data_id");

                entity.Property(e => e.CampaignId).HasColumnName("campaign_id");

                entity.Property(e => e.ConnectTime)
                    .HasColumnName("connect_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.CustomerInputDtmf)
                    .HasColumnName("customer_input_dtmf")
                    .HasMaxLength(150);

                entity.Property(e => e.CustomerName)
                    .HasColumnName("customer_name")
                    .HasMaxLength(250);

                entity.Property(e => e.EndTime)
                    .HasColumnName("end_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.EventType)
                    .HasColumnName("event_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsExecute).HasDefaultValueSql("((0))");

                entity.Property(e => e.Line)
                    .HasColumnName("line")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phone_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RingAgentDuration).HasColumnName("ring_agent_duration");

                entity.Property(e => e.RingCustomerDuration).HasColumnName("ring_customer_duration");

                entity.Property(e => e.StartTime)
                    .HasColumnName("start_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.StatusCode).HasColumnName("status_code");
            });

            modelBuilder.Entity<TblLogTransferLoanOfLender>(entity =>
            {
                entity.ToTable("tblLogTransferLoanOfLender");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");
            });

            modelBuilder.Entity<TblLogUploadFile>(entity =>
            {
                entity.HasKey(e => e.LogUploadFileId);

                entity.ToTable("tblLogUploadFile");

                entity.Property(e => e.LogUploadFileId).HasColumnName("LogUploadFileID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DepartmentManagerUserId).HasColumnName("DepartmentManagerUserID");

                entity.Property(e => e.DepartmentManagerUsername)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.SectionManagerUserId).HasColumnName("SectionManagerUserID");

                entity.Property(e => e.SectionManagerUsername)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.StaffId).HasColumnName("StaffID");

                entity.Property(e => e.StaffName)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.Username)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblLogWorkingUser>(entity =>
            {
                entity.ToTable("tblLogWorkingUser");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblManageSpices>(entity =>
            {
                entity.HasKey(e => new { e.ShopId, e.ProductId, e.DistrictId, e.WardId });

                entity.ToTable("tblManageSpices");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TblManufacturer>(entity =>
            {
                entity.HasKey(e => e.ManufacturerId);

                entity.ToTable("tblManufacturer");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblMarkStep>(entity =>
            {
                entity.HasKey(e => e.StepId);

                entity.ToTable("tblMarkStep");

                entity.Property(e => e.StepName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblMessage>(entity =>
            {
                entity.HasKey(e => e.MsgId);

                entity.ToTable("tblMessage");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MsgContent).HasMaxLength(4000);

                entity.Property(e => e.MsgFilePath)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.MsgTitle).HasMaxLength(500);
            });

            modelBuilder.Entity<TblMessageUser>(entity =>
            {
                entity.HasKey(e => new { e.MsgId, e.UserId });

                entity.ToTable("tblMessageUser");

                entity.Property(e => e.ReadDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TblModule>(entity =>
            {
                entity.ToTable("tblModule");

                entity.Property(e => e.ActionName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ControllerName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.LinkText).HasMaxLength(50);

                entity.Property(e => e.Priority).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblOddDebtDaily>(entity =>
            {
                entity.ToTable("tblOddDebtDaily");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.ProductName).HasMaxLength(50);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ShopName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblOrderSource>(entity =>
            {
                entity.ToTable("tblOrderSource");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ApplyFor).HasComment("áp dụng cho báo cáo Tima (1)  hoặc Hub (2)");

                entity.Property(e => e.Condition).HasComment("Xuất điều kiện ra code dùng trong javascript");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.OderSourceName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasComment("/ReportLoanCredit/SourceLevel4: tên hiển thị ở cột nguồn và cột nguồn con");

                entity.Property(e => e.Status).HasComment("trạng thái : 0 - ko sử dụng, 1- được sử dụng");
            });

            modelBuilder.Entity<TblOrderSourceDetail>(entity =>
            {
                entity.ToTable("tblOrderSourceDetail");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompareTo).HasComment("giá trị quy đổi 1: Utm_Source, 2:  Utm_Campaign");

                entity.Property(e => e.CompareType).HasComment("1: Contains , 2: Equals, 3: StartsWith");

                entity.Property(e => e.KeyType).HasComment("1: platform , 2: keyword");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .HasComment("giá trị của từ khóa");

                entity.Property(e => e.OrderSourceId).HasComment("Khóa ngoại của bảng OrderSource");

                entity.Property(e => e.Status).HasComment("trạng thái: 0- ko sử dụng, 1- sử dụng");
            });

            modelBuilder.Entity<TblOtpContract>(entity =>
            {
                entity.ToTable("tblOtpContract");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ExpiredTime).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");

                entity.Property(e => e.UserFullName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblOtpTs>(entity =>
            {
                entity.ToTable("tblOtpTS");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RequestId)
                    .HasColumnName("Request_id")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblPawnFiles>(entity =>
            {
                entity.ToTable("tblPawnFiles");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FilePath)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblPawnNew>(entity =>
            {
                entity.ToTable("tblPawnNew");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.CodeId).HasColumnName("CodeID");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DebitMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.ExpireDate).HasColumnType("date");

                entity.Property(e => e.Extend1).HasMaxLength(100);

                entity.Property(e => e.Extend2).HasMaxLength(100);

                entity.Property(e => e.Extend3).HasMaxLength(100);

                entity.Property(e => e.Extend4).HasMaxLength(100);

                entity.Property(e => e.Extend5).HasMaxLength(100);

                entity.Property(e => e.FinishDate).HasColumnType("date");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.InterestToDay).HasDefaultValueSql("((0))");

                entity.Property(e => e.ItemName).HasMaxLength(255);

                entity.Property(e => e.LastDateOfPay).HasColumnType("date");

                entity.Property(e => e.LiquidationDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NextDate).HasColumnType("date");

                entity.Property(e => e.Note).HasMaxLength(1024);

                entity.Property(e => e.PaymentMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblPawnNewDebit>(entity =>
            {
                entity.HasKey(e => e.DebitId)
                    .HasName("PK__tblPawnNewDebit__9B556A58671F4F74");

                entity.ToTable("tblPawnNewDebit");

                entity.Property(e => e.DebitId).HasColumnName("DebitID");

                entity.Property(e => e.ForDate).HasColumnType("datetime");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblPawnNewExtra>(entity =>
            {
                entity.ToTable("tblPawnNewExtra");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateExtra).HasColumnType("date");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblPawnNewFiles>(entity =>
            {
                entity.ToTable("tblPawnNewFiles");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FilePath)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblPayment>(entity =>
            {
                entity.ToTable("tblPayment");

                entity.Property(e => e.BankCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DaysPayable).HasColumnType("date");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.OriginalMoney).HasComment("");

                entity.Property(e => e.OverMoney).HasComment("Tiền thừa");

                entity.Property(e => e.PayDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<TblPayment2>(entity =>
            {
                entity.ToTable("tblPayment2");

                entity.HasIndex(e => new { e.InstallmentId, e.UserId })
                    .HasName("IX_tblPayment2");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.InstallmentId).HasColumnName("InstallmentID");

                entity.Property(e => e.PayDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<TblPayment3>(entity =>
            {
                entity.ToTable("tblPayment3");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.PayDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<TblPayment4>(entity =>
            {
                entity.ToTable("tblPayment4");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.PayDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<TblPaymentDetail>(entity =>
            {
                entity.HasKey(e => new { e.ShopId, e.PaymentId, e.ForDate });

                entity.ToTable("tblPaymentDetail");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.PaymentId).HasColumnName("PaymentID");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.MoneyPay).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblPaymentDetail2>(entity =>
            {
                entity.HasKey(e => new { e.ShopId, e.PaymentId, e.ForDate });

                entity.ToTable("tblPaymentDetail2");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.PaymentId).HasColumnName("PaymentID");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.InstallmentId).HasColumnName("InstallmentID");

                entity.Property(e => e.MoneyPay).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblPaymentNotify>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.TypeId, e.ShopId });

                entity.ToTable("tblPaymentNotify");

                entity.HasIndex(e => e.AutoId)
                    .HasName("IX_tblPaymentNotify")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.AgencyId).HasColumnName("AgencyID");

                entity.Property(e => e.AgencyName).HasMaxLength(50);

                entity.Property(e => e.AutoId)
                    .HasColumnName("AutoID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ChooseToday).HasDefaultValueSql("((0))");

                entity.Property(e => e.CodeId).HasColumnName("CodeID");

                entity.Property(e => e.CodeItem)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CountProcess).HasDefaultValueSql("((0))");

                entity.Property(e => e.CusAddress).HasMaxLength(256);

                entity.Property(e => e.CusId).HasColumnName("CusID");

                entity.Property(e => e.CusName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CusPhone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DebitMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsSmartDialer).HasDefaultValueSql("((1))");

                entity.Property(e => e.ItemName).HasMaxLength(512);

                entity.Property(e => e.LastLogId).HasColumnName("LastLogID");

                entity.Property(e => e.ManageId).HasColumnName("ManageID");

                entity.Property(e => e.NextCall)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Note).HasMaxLength(100);

                entity.Property(e => e.Number).HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberCard)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PayMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.PhoneRelationShip).HasMaxLength(500);

                entity.Property(e => e.ProductName).HasMaxLength(50);

                entity.Property(e => e.RegistrationContractId)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RegistrationCustomerId)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.StatusDoing).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblPaymentNotifyByDay>(entity =>
            {
                entity.ToTable("tblPaymentNotifyByDay");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.CusId).HasColumnName("CusID");

                entity.Property(e => e.CusName).HasMaxLength(50);

                entity.Property(e => e.DebitMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.ForDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ManageId).HasColumnName("ManageID");

                entity.Property(e => e.MoneyPaid).HasDefaultValueSql("((0))");

                entity.Property(e => e.Number).HasDefaultValueSql("((0))");

                entity.Property(e => e.PayMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");
            });

            modelBuilder.Entity<TblPaymentScheduler>(entity =>
            {
                entity.ToTable("tblPaymentScheduler");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateToPay).HasColumnType("date");

                entity.Property(e => e.Done).HasDefaultValueSql("((0))");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.MoneyInterest).HasComment("tiền lãi");

                entity.Property(e => e.MoneyInterestCollected).HasComment("Tiền lãi thực thu");

                entity.Property(e => e.MoneyOriginal).HasComment("Tiền gốc");

                entity.Property(e => e.MoneyOriginalCollected).HasComment("Tiền gốc thực thu");

                entity.Property(e => e.MoneyOtherOfTima).HasComment("Phí của Tima");

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<TblPermission>(entity =>
            {
                entity.ToTable("tblPermission");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ActionName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ControllerName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.DisplayText).HasMaxLength(512);

                entity.Property(e => e.IsThnWeb).HasDefaultValueSql("((0))");

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TblPermissionDistrict>(entity =>
            {
                entity.ToTable("tblPermissionDistrict");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AreaId).HasColumnName("AreaID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DistrictId).HasColumnName("DistrictID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblPermissionDistrictBebtRecovery>(entity =>
            {
                entity.ToTable("tblPermissionDistrictBebtRecovery");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DistrictId).HasColumnName("DistrictID");

                entity.Property(e => e.Level).HasDefaultValueSql("((1))");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblPermissionGroup>(entity =>
            {
                entity.ToTable("tblPermissionGroup");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.PermissionId).HasColumnName("PermissionID");
            });

            modelBuilder.Entity<TblPermissionUser>(entity =>
            {
                entity.ToTable("tblPermissionUser");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.PermissionId).HasColumnName("PermissionID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblPlanDebt>(entity =>
            {
                entity.ToTable("tblPlanDebt");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CityId).HasColumnName("CityID");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.ForDateId).HasColumnName("ForDateID");

                entity.Property(e => e.SaleChanelId).HasColumnName("SaleChanelID");
            });

            modelBuilder.Entity<TblPlanOfDisbursement>(entity =>
            {
                entity.ToTable("tblPlanOfDisbursement");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CityId).HasColumnName("CityID");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.ForDateId).HasColumnName("ForDateID");

                entity.Property(e => e.SaleChanelId).HasColumnName("SaleChanelID");
            });

            modelBuilder.Entity<TblPlatformAndTima>(entity =>
            {
                entity.HasKey(e => new { e.PlatformLoanCreditId, e.LoanCreditId, e.LenderId });

                entity.ToTable("tblPlatformAndTima");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblPointVnpt>(entity =>
            {
                entity.ToTable("tblPointVNPT");

                entity.Property(e => e.Account)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(150);

                entity.Property(e => e.ErrorDesc).HasMaxLength(150);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Profile).HasMaxLength(50);

                entity.Property(e => e.RequestId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ResponseId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Time).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblProcessData>(entity =>
            {
                entity.HasKey(e => e.ProcessDataId);

                entity.ToTable("tblProcessData");

                entity.HasIndex(e => new { e.Id, e.TypeId, e.ActionId })
                    .HasName("IX_tblProcessData");

                entity.Property(e => e.ProcessDataId).HasColumnName("ProcessDataID");

                entity.Property(e => e.ActionId)
                    .HasColumnName("ActionID")
                    .HasComment("0 : Process Payment Notify ,  1 = Generate Interest Excepted");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");
            });

            modelBuilder.Entity<TblProcessPayment>(entity =>
            {
                entity.HasKey(e => e.ProcessPaymentId);

                entity.ToTable("tblProcessPayment");

                entity.HasIndex(e => new { e.Id, e.PaymentId, e.Type })
                    .HasName("IX_tblProcessPayment");

                entity.Property(e => e.ProcessPaymentId).HasColumnName("ProcessPaymentID");

                entity.Property(e => e.Action).HasComment("1= Thêm, 0 là xóa");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.PaymentId).HasColumnName("PaymentID");

                entity.Property(e => e.Retry).HasDefaultValueSql("((0))");

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<TblProcessTimeDepartMent>(entity =>
            {
                entity.ToTable("tblProcessTimeDepartMent");

                entity.Property(e => e.GroupName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblProduct>(entity =>
            {
                entity.ToTable("tblProduct");

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.ShortName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblProductAppraiser>(entity =>
            {
                entity.ToTable("tblProductAppraiser");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FromUserId).HasComment("chuyển đơn cho người khác");

                entity.Property(e => e.FullNameAssigner).HasMaxLength(50);

                entity.Property(e => e.Latitude)
                    .HasColumnType("decimal(18, 9)")
                    .HasComment("vĩ độ ");

                entity.Property(e => e.Longitude)
                    .HasColumnType("decimal(18, 9)")
                    .HasComment("kinh độ");

                entity.Property(e => e.UserNameAssigner).HasMaxLength(50);
            });

            modelBuilder.Entity<TblProductCredit>(entity =>
            {
                entity.ToTable("tblProductCredit");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Rate)
                    .HasColumnType("decimal(18, 6)")
                    .HasComment("Tổng lãi");

                entity.Property(e => e.RateConsultant)
                    .HasColumnType("decimal(18, 6)")
                    .HasComment("Phí tư vấn");

                entity.Property(e => e.RateInterest)
                    .HasColumnType("decimal(18, 6)")
                    .HasComment("Lãi");

                entity.Property(e => e.RateService)
                    .HasColumnType("decimal(18, 6)")
                    .HasComment("Phí Dịch vụ");

                entity.Property(e => e.TypeProduct).HasComment("Loại gói sản phẩm cao hay thấp");
            });

            modelBuilder.Entity<TblProductEvaluation>(entity =>
            {
                entity.ToTable("tblProductEvaluation");
            });

            modelBuilder.Entity<TblProductPercentReduction>(entity =>
            {
                entity.ToTable("tblProductPercentReduction");
            });

            modelBuilder.Entity<TblProductReview>(entity =>
            {
                entity.ToTable("tblProductReview");

                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<TblProductReviewDetail>(entity =>
            {
                entity.ToTable("tblProductReviewDetail");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.PecentDiscount).HasColumnType("numeric(2, 1)");
            });

            modelBuilder.Entity<TblProductReviewResult>(entity =>
            {
                entity.ToTable("tblProductReviewResult");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TblProductReviewResultDetail>(entity =>
            {
                entity.ToTable("tblProductReviewResultDetail");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblProductStep>(entity =>
            {
                entity.ToTable("tblProductStep");
            });

            modelBuilder.Entity<TblProductType>(entity =>
            {
                entity.ToTable("tblProductType");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<TblProportionProductAgency>(entity =>
            {
                entity.ToTable("tblProportionProductAgency");

                entity.Property(e => e.ProductName).HasMaxLength(50);

                entity.Property(e => e.RateConsultant)
                    .HasColumnType("decimal(18, 6)")
                    .HasComment("Phí tư vấn");

                entity.Property(e => e.RateInterest)
                    .HasColumnType("decimal(18, 6)")
                    .HasComment("Lãi");

                entity.Property(e => e.RateService)
                    .HasColumnType("decimal(18, 6)")
                    .HasComment("Phí Dịch vụ");

                entity.Property(e => e.ShopName).HasMaxLength(50);
            });

            modelBuilder.Entity<TblPushSmartDailer>(entity =>
            {
                entity.ToTable("tblPushSmartDailer");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.PushDate).HasColumnType("datetime");

                entity.Property(e => e.ResponseApi).HasColumnName("ResponseAPI");

                entity.Property(e => e.Source).HasMaxLength(50);
            });

            modelBuilder.Entity<TblReason>(entity =>
            {
                entity.ToTable("tblReason");

                entity.Property(e => e.IsCallBack).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.ReSend).HasComment("có đẩy lại hay không , 1 = No , 0 = Yes");

                entity.Property(e => e.Reason).HasMaxLength(200);

                entity.Property(e => e.ReasonCode)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblReasonReturn>(entity =>
            {
                entity.ToTable("tblReasonReturn");
            });

            modelBuilder.Entity<TblReceiveYourIncome>(entity =>
            {
                entity.HasKey(e => e.ReceiveYourIncomeId);

                entity.ToTable("tblReceiveYourIncome");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblReference>(entity =>
            {
                entity.ToTable("tblReference");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.DescriptionCustomer).HasMaxLength(250);

                entity.Property(e => e.DescriptionTypeId).HasMaxLength(250);

                entity.Property(e => e.LoanCreditTypeId).HasColumnName("LoanCredit_TypeId");
            });

            modelBuilder.Entity<TblRegion>(entity =>
            {
                entity.ToTable("tblRegion");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<TblReimbursement>(entity =>
            {
                entity.ToTable("tblReimbursement");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ConfirmDate).HasColumnType("datetime");

                entity.Property(e => e.FullNameAgencyGcash).HasMaxLength(100);

                entity.Property(e => e.FullNameConfirm).HasMaxLength(100);

                entity.Property(e => e.IdagencyGcash).HasColumnName("IDAgencyGcash");

                entity.Property(e => e.Idconfirm).HasColumnName("IDConfirm");

                entity.Property(e => e.NoteConfirm).HasMaxLength(500);

                entity.Property(e => e.NoteReimbursementAgencyGcash).HasMaxLength(500);

                entity.Property(e => e.ReimbursementDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserNameAgencyGcash).HasMaxLength(50);

                entity.Property(e => e.UserNameConfirm).HasMaxLength(50);
            });

            modelBuilder.Entity<TblRelationshipEmployees>(entity =>
            {
                entity.ToTable("tblRelationshipEmployees");

                entity.Property(e => e.CreationDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblRelativeFamily>(entity =>
            {
                entity.HasKey(e => e.RelativeFamilyId);

                entity.ToTable("tblRelativeFamily");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblReportCounselors>(entity =>
            {
                entity.HasKey(e => e.ReportId);

                entity.ToTable("tblReportCounselors");

                entity.HasComment("");

                entity.Property(e => e.ActionJobString)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CreationDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.CustomerPhone)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.DescriptionWork).HasMaxLength(500);

                entity.Property(e => e.FullName).HasMaxLength(20);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ReasonFail).HasMaxLength(500);

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");

                entity.Property(e => e.TitleWorkString).HasMaxLength(50);

                entity.Property(e => e.WorkingTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblReportGoogleAds>(entity =>
            {
                entity.ToTable("tblReportGoogleAds");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TabName).HasMaxLength(250);
            });

            modelBuilder.Entity<TblReportHubAdministrator>(entity =>
            {
                entity.ToTable("tblReportHubAdministrator");

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ToDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblReportPtsp>(entity =>
            {
                entity.ToTable("tblReportPTSP");

                entity.Property(e => e.Birthday).HasMaxLength(250);

                entity.Property(e => e.CardNumber).HasMaxLength(250);

                entity.Property(e => e.CityName).HasMaxLength(250);

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(250);

                entity.Property(e => e.HistoryPayMent).HasMaxLength(250);

                entity.Property(e => e.LivingName).HasMaxLength(250);

                entity.Property(e => e.LoanCreditId).HasColumnName("LoanCreditID");

                entity.Property(e => e.RyincomeName)
                    .HasColumnName("RYIncomeName")
                    .HasMaxLength(250);

                entity.Property(e => e.Salary).HasMaxLength(250);

                entity.Property(e => e.TypeOwnerShipName).HasMaxLength(250);
            });

            modelBuilder.Entity<TblReportShop>(entity =>
            {
                entity.ToTable("tblReportShop");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.TotalIndentureClose).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalIndentureOpen).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalInterestEarned).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalInterestExpected).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalMoneyDebtor).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalMoneyInvestment).HasDefaultValueSql("((0))");

                entity.Property(e => e.TypeId)
                    .HasColumnName("TypeID")
                    .HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TblRequestCac>(entity =>
            {
                entity.ToTable("tblRequestCAC");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerCreditId).HasColumnName("CustomerCreditID");

                entity.Property(e => e.LoanCreditId).HasColumnName("LoanCreditID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblScheduledCollection>(entity =>
            {
                entity.ToTable("tblScheduledCollection");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(250);

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.LoanCodeId).HasColumnName("LoanCodeID");

                entity.Property(e => e.LoanCreditId).HasColumnName("LoanCreditID");

                entity.Property(e => e.PayDate).HasColumnType("date");
            });

            modelBuilder.Entity<TblScore>(entity =>
            {
                entity.ToTable("tblScore");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Message).HasMaxLength(250);

                entity.Property(e => e.Score).HasColumnType("decimal(18, 5)");

                entity.Property(e => e.Status).HasMaxLength(250);
            });

            modelBuilder.Entity<TblShop>(entity =>
            {
                entity.HasKey(e => e.ShopId);

                entity.ToTable("tblShop");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.BankBalance).HasMaxLength(2000);

                entity.Property(e => e.Changed).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email).HasMaxLength(250);

                entity.Property(e => e.GroupShop)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Loại nhóm , 1.là thuộc GCash");

                entity.Property(e => e.InviteCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsAgent).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsGcash)
                    .HasColumnName("IsGCash")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsGetOriginalPenalty).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsHub).HasDefaultValueSql("((0))");

                entity.Property(e => e.Lat)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Long)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Note).HasMaxLength(1000);

                entity.Property(e => e.OwerNameOfShop).HasMaxLength(500);

                entity.Property(e => e.PersonAvatar).HasMaxLength(500);

                entity.Property(e => e.PersonBirthDay).HasColumnType("date");

                entity.Property(e => e.PersonCardNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PersonContact).HasMaxLength(50);

                entity.Property(e => e.PersonContactPhone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PersonGender).HasComment("0: nam , 1: nữ");

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RateAff).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RateInterest)
                    .HasColumnType("decimal(18, 6)")
                    .HasDefaultValueSql("((0.000493))");

                entity.Property(e => e.RateLender)
                    .HasColumnType("decimal(18, 6)")
                    .HasDefaultValueSql("((18))");

                entity.Property(e => e.ReceivedDate).HasColumnType("datetime");

                entity.Property(e => e.Represent).HasMaxLength(100);

                entity.Property(e => e.Score).HasDefaultValueSql("((100))");

                entity.Property(e => e.SelfEmployed)
                    .HasDefaultValueSql("((5))")
                    .HasComment("Tự Doanh:  1: Tự Doanh DRS, 2: Tự Doanh QHĐN,  3: HUB");

                entity.Property(e => e.ShopNo)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Spices).HasMaxLength(1000);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.TaxCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalAbnormalCollectionMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalConvertInterestIntoCapital).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalInterestAvailable).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalInterestEarned).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalInterestExpected).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalInterestWithdraw).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalMoneyCapitalCurrent).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalMoneyDebtor).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalMoneyInSafe).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalMoneySpent).HasDefaultValueSql("((0))");

                entity.Property(e => e.TypePayment)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('KQ')");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblSimVip>(entity =>
            {
                entity.ToTable("tblSimVip");

                entity.Property(e => e.DescriptionSim)
                    .HasMaxLength(200)
                    .HasComment("Mô tả sim");

                entity.Property(e => e.ExpiryDate)
                    .HasColumnType("date")
                    .HasComment("Hạn sử dụng");

                entity.Property(e => e.FullNameOwnerSim).HasMaxLength(50);

                entity.Property(e => e.OwnerShipSim).HasComment("1: Chính chủ,0: Không chính chủ");

                entity.Property(e => e.PhoneOwnerSim)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RegisterDate)
                    .HasColumnType("date")
                    .HasComment("Thời gian đăng ký");

                entity.Property(e => e.SubscriptionForm).HasComment("Hình thức thuê bao: trả trước / trả sau");

                entity.Property(e => e.TelCo).HasComment("Nhà mạng: viettel/vina/mobi/vietnammobile/Gtel");
            });

            modelBuilder.Entity<TblSms>(entity =>
            {
                entity.ToTable("tblSMS");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.NextSend).HasColumnType("datetime");

                entity.Property(e => e.NumberPhone)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblSmsAnalytics>(entity =>
            {
                entity.ToTable("tblSmsAnalytics");

                entity.Property(e => e.AcountNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Số thài khoản");

                entity.Property(e => e.BankCardAliasName).HasMaxLength(250);

                entity.Property(e => e.BankCardId).HasComment("Số TK Phát sinh ");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(250)
                    .HasComment("Tên khách hàng trong nội dung tin nhắn");

                entity.Property(e => e.IdCard)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Số CMTND");

                entity.Property(e => e.IncreaseMoney).HasComment("Số tiền phát sinh");

                entity.Property(e => e.IncreaseTime)
                    .HasColumnType("datetime")
                    .HasComment("ngày giờ phát sinh giao dịch");

                entity.Property(e => e.LoanId).HasComment("mã hợp đồng tách từ nội dung tin");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NameTransactionBank).HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("số đt tách từ nội dung tin");

                entity.Property(e => e.Sender)
                    .HasMaxLength(250)
                    .HasComment("người gửi");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ShopName).HasMaxLength(50);

                entity.Property(e => e.SmsContent)
                    .HasMaxLength(500)
                    .HasComment("nội dung tin nhắn");

                entity.Property(e => e.SmsreceivedDate).HasColumnType("datetime");

                entity.Property(e => e.Status).HasComment("0: ko sử lý, 1: chờ sử lý, 2: đã sử lý");

                entity.Property(e => e.TransactionBankCardStatus).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblSmsBrandName>(entity =>
            {
                entity.ToTable("tblSmsBrandName");

                entity.Property(e => e.Content).HasMaxLength(500);

                entity.Property(e => e.CountSms)
                    .HasColumnName("CountSMS")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CreateByFullName).HasMaxLength(50);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ResultSend).HasDefaultValueSql("((0))");

                entity.Property(e => e.SessionId).HasMaxLength(50);

                entity.Property(e => e.SupplierBrandName)
                    .HasMaxLength(50)
                    .HasComment("tên nhà cung cấp để gửi tin nhắn");

                entity.Property(e => e.SupplierId).HasComment("mã nhà cung cấp");
            });

            modelBuilder.Entity<TblSmsCredit>(entity =>
            {
                entity.ToTable("tblSmsCredit");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Smsid)
                    .HasColumnName("SMSID")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblSmscompleted>(entity =>
            {
                entity.ToTable("tblSMSCompleted");

                entity.Property(e => e.CompletedDate).HasColumnType("datetime");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.FullName).HasMaxLength(50);

                entity.Property(e => e.NumberPhone)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblStatementOfAccounts>(entity =>
            {
                entity.ToTable("tblStatementOfAccounts");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerCreditName).HasMaxLength(50);

                entity.Property(e => e.FullNameCreate).HasMaxLength(50);

                entity.Property(e => e.FullNameModify).HasMaxLength(50);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NameStatementOfAccount).HasMaxLength(250);

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.OwnedShopName).HasMaxLength(50);

                entity.Property(e => e.PayShopName).HasMaxLength(50);

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");

                entity.Property(e => e.UserNameCreate).HasMaxLength(50);

                entity.Property(e => e.UserNameModify).HasMaxLength(50);
            });

            modelBuilder.Entity<TblTarget>(entity =>
            {
                entity.ToTable("tblTarget");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.TotalLoanTarget).HasComment("Chỉ tiêu tổng đơn giải ngân");

                entity.Property(e => e.TotalMoneyTarget).HasComment("Chỉ tiêu tổng tiền giải ngân");
            });

            modelBuilder.Entity<TblTelesaleCall>(entity =>
            {
                entity.ToTable("tblTelesaleCall");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BankName).HasMaxLength(50);

                entity.Property(e => e.CardnumberNew).HasMaxLength(50);

                entity.Property(e => e.CompanyName).HasMaxLength(500);

                entity.Property(e => e.ContactName).HasMaxLength(200);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DescriptionPositionJob).HasMaxLength(250);

                entity.Property(e => e.FoundedTimeCompany).HasMaxLength(500);

                entity.Property(e => e.LevelMoney).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.LifeTimeAddress).HasMaxLength(200);

                entity.Property(e => e.LoanCreditId).HasColumnName("LoanCreditID");

                entity.Property(e => e.MoneyLoanbank).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SameFullnameShk).HasColumnName("SameFullnameSHK");

                entity.Property(e => e.StrTimeWorking)
                    .HasColumnName("strTimeWorking")
                    .HasMaxLength(20);

                entity.Property(e => e.TypeId).HasColumnName("TypeID");
            });

            modelBuilder.Entity<TblTempCountSms>(entity =>
            {
                entity.ToTable("tblTemp_CountSMS");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Content).HasMaxLength(1020);
            });

            modelBuilder.Entity<TblTempData>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.Code });

                entity.ToTable("tblTempData");

                entity.Property(e => e.Id).HasColumnName("ID");
            });

            modelBuilder.Entity<TblTempInterestToday>(entity =>
            {
                entity.HasKey(e => new { e.LoanId, e.TypeId });

                entity.ToTable("tblTempInterestToday");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");

                entity.Property(e => e.InterestToday).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblTempThnPushData>(entity =>
            {
                entity.HasKey(e => e.LoanId);

                entity.ToTable("tblTempTHN_PushData");

                entity.Property(e => e.LoanId)
                    .HasColumnName("LoanID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DepartmentManagerUserId).HasColumnName("DepartmentManagerUserID");

                entity.Property(e => e.DepartmentManagerUsername)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.SectionManagerUserId).HasColumnName("SectionManagerUserID");

                entity.Property(e => e.SectionManagerUsername)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.StaffId).HasColumnName("StaffID");

                entity.Property(e => e.TeamleadUserId).HasColumnName("TeamleadUserID");

                entity.Property(e => e.TeamleadUserName)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblThnFiles>(entity =>
            {
                entity.ToTable("tblThnFiles");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CommentId).HasColumnName("CommentID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FilePath)
                    .HasMaxLength(550)
                    .IsUnicode(false);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblThnlogActionManager>(entity =>
            {
                entity.ToTable("tblTHNLogActionManager");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreateUserId).HasColumnName("CreateUserID");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Note).HasMaxLength(250);

                entity.Property(e => e.StaffId).HasColumnName("StaffID");
            });

            modelBuilder.Entity<TblThnlogPushData>(entity =>
            {
                entity.ToTable("tblTHNLogPushData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.FirstUpdate).HasColumnType("datetime");

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.LastUpdate).HasColumnType("datetime");

                entity.Property(e => e.LoanAndMonthId).HasColumnName("LoanAndMonthID");

                entity.Property(e => e.NextDate).HasColumnType("datetime");

                entity.Property(e => e.ProcessUserId).HasColumnName("ProcessUserID");

                entity.Property(e => e.ProcessUsername)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UserActionName)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblTimaAt>(entity =>
            {
                entity.HasKey(e => e.Vid);

                entity.ToTable("tblTimaAT");

                entity.Property(e => e.Vid).ValueGeneratedNever();

                entity.Property(e => e.AffSid)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblTimaVbi>(entity =>
            {
                entity.ToTable("tblTimaVbi");

                entity.Property(e => e.CreateOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MaDviQl)
                    .HasColumnName("MA_DVI_QL")
                    .HasMaxLength(50);

                entity.Property(e => e.SoHd)
                    .HasColumnName("SO_HD")
                    .HasMaxLength(50);

                entity.Property(e => e.SoIdChoVay).HasColumnName("SO_ID_CHO_VAY");

                entity.Property(e => e.SoIdDtac).HasColumnName("SO_ID_DTAC");

                entity.Property(e => e.SoIdVay).HasColumnName("SO_ID_VAY");

                entity.Property(e => e.SoIdVbi).HasColumnName("SO_ID_VBI");

                entity.Property(e => e.StatusGetGcnChoVay)
                    .HasColumnName("StatusGetGCN_CHO_VAY")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.StatusGetGcnvay)
                    .HasColumnName("StatusGetGCNVay")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TongPhi).HasColumnName("TONG_PHI");
            });

            modelBuilder.Entity<TblToken>(entity =>
            {
                entity.ToTable("tblToken");

                entity.Property(e => e.AppId)
                    .HasColumnName("app_id")
                    .HasMaxLength(500);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateExpiration).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblTokenCareSoft>(entity =>
            {
                entity.ToTable("tblTokenCareSoft");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateExpiration).HasColumnType("datetime");

                entity.Property(e => e.Token).HasMaxLength(500);
            });

            modelBuilder.Entity<TblTransactionBankCard>(entity =>
            {
                entity.ToTable("tblTransactionBankCard");

                entity.Property(e => e.BankCardAliasName).HasMaxLength(250);

                entity.Property(e => e.BankCardAliasNameReceive).HasMaxLength(250);

                entity.Property(e => e.CollectorsShopName).HasMaxLength(50);

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.DateTimeTransaction).HasColumnType("datetime");

                entity.Property(e => e.FullNameAction).HasMaxLength(50);

                entity.Property(e => e.FullNameUserRemind).HasMaxLength(250);

                entity.Property(e => e.FullnameModify).HasMaxLength(50);

                entity.Property(e => e.ModifyOn).HasColumnType("datetime");

                entity.Property(e => e.NameTransactionBank).HasMaxLength(50);

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.OtherMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.OwnedShopName).HasMaxLength(50);

                entity.Property(e => e.PayShopName).HasMaxLength(50);

                entity.Property(e => e.SmsAnalyticsId).HasComment("ID tblSmsAnalytics");

                entity.Property(e => e.UserNameAction).HasMaxLength(50);

                entity.Property(e => e.UserNameModify).HasMaxLength(50);
            });

            modelBuilder.Entity<TblTransactionCapital>(entity =>
            {
                entity.ToTable("tblTransactionCapital");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.BankCode).HasMaxLength(150);

                entity.Property(e => e.BankId).HasColumnName("BankID");

                entity.Property(e => e.CapitalId).HasColumnName("CapitalID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("getdate()");

                entity.Property(e => e.CusId).HasColumnName("CusID");

                entity.Property(e => e.CusName).HasMaxLength(100);

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblTransactionCollectionExpense>(entity =>
            {
                entity.ToTable("tblTransactionCollectionExpense");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.BankCode).HasMaxLength(150);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CusName).HasMaxLength(100);

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.IsCancel).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.TypePatternId).HasColumnName("TypePatternID");

                entity.Property(e => e.TypePatternName).HasMaxLength(100);

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblTransactionInstallment>(entity =>
            {
                entity.ToTable("tblTransactionInstallment");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CusId).HasColumnName("CusID");

                entity.Property(e => e.CusName).HasMaxLength(100);

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.InstallmentId).HasColumnName("InstallmentID");

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblTransactionLoan>(entity =>
            {
                entity.ToTable("tblTransactionLoan");

                entity.HasIndex(e => new { e.ShopId, e.ActionId, e.CreateDate })
                    .HasName("NonClusteredIndex-20190710-110447");

                entity.HasIndex(e => new { e.ShopId, e.LoanId, e.ActionId, e.CreateDate })
                    .HasName("NonClusteredIndex-20190322-222259");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.BankCode).HasMaxLength(150);

                entity.Property(e => e.BankId).HasColumnName("BankID");

                entity.Property(e => e.CodeId).HasColumnName("CodeID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("getdate()");

                entity.Property(e => e.CusId).HasColumnName("CusID");

                entity.Property(e => e.CusName).HasMaxLength(100);

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.FullNameModify).HasMaxLength(50);

                entity.Property(e => e.ItemName).HasMaxLength(255);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.ModifyOn).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.OverMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.PaymentId).HasDefaultValueSql("((0))");

                entity.Property(e => e.RateAffLender).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RateLender).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.TransactionLoanId).HasColumnName("TransactionLoanID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.UserNameModify).HasMaxLength(50);
            });

            modelBuilder.Entity<TblTransactionMoneyInSafe>(entity =>
            {
                entity.ToTable("tblTransactionMoneyInSafe");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("getdate()");

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblTransactionPawnNew>(entity =>
            {
                entity.ToTable("tblTransactionPawnNew");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CodeId)
                    .HasColumnName("CodeID")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("getdate()");

                entity.Property(e => e.CusId).HasColumnName("CusID");

                entity.Property(e => e.CusName).HasMaxLength(100);

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.ItemName).HasMaxLength(255);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.MoneyInterest).HasDefaultValueSql("((0))");

                entity.Property(e => e.MoneyPawn).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.OtherMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblTrustResults>(entity =>
            {
                entity.ToTable("tblTrustResults");

                entity.Property(e => e.Message).HasMaxLength(500);
            });

            modelBuilder.Entity<TblTypeOfOwnership>(entity =>
            {
                entity.HasKey(e => e.TypeOfOwnershipId);

                entity.ToTable("tblTypeOfOwnership");

                entity.Property(e => e.IsActived).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblUser>(entity =>
            {
                entity.ToTable("tblUser");

                entity.Property(e => e.AccessIpAddress)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AccessPrivate).HasDefaultValueSql("((1))");

                entity.Property(e => e.BankBranch).HasMaxLength(50);

                entity.Property(e => e.BankName).HasMaxLength(50);

                entity.Property(e => e.BankNumber).HasMaxLength(50);

                entity.Property(e => e.BankOwner).HasMaxLength(50);

                entity.Property(e => e.CiscoExtension).HasMaxLength(50);

                entity.Property(e => e.CiscoPassword).HasMaxLength(500);

                entity.Property(e => e.CiscoUsername).HasMaxLength(100);

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DayOffWork).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiredDate).HasColumnType("datetime");

                entity.Property(e => e.ExprireToken).HasColumnType("datetime");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.IdManageThn).HasColumnName("IdManageTHN");

                entity.Property(e => e.Ipphone)
                    .HasColumnName("IPPhone")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ManageThn)
                    .HasColumnName("ManageTHN")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MaxShop).HasDefaultValueSql("((1))");

                entity.Property(e => e.ParentId).HasDefaultValueSql("((-1))");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.PersonalCard).HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PushTokenAndroid)
                    .HasMaxLength(5000)
                    .IsUnicode(false);

                entity.Property(e => e.PushTokenIos)
                    .HasColumnName("PushTokenIOS")
                    .HasMaxLength(5000)
                    .IsUnicode(false);

                entity.Property(e => e.Token)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UserReceivedDate).HasColumnType("datetime");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.WorkingTime).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TblUserCheckInByApp>(entity =>
            {
                entity.ToTable("tblUserCheckInByApp");

                entity.Property(e => e.Adress).HasMaxLength(1000);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ImgUrl).HasMaxLength(1024);
            });

            modelBuilder.Entity<TblUserDomainRegion>(entity =>
            {
                entity.ToTable("tblUserDomainRegion");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AreaId).HasColumnName("AreaID");

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.RegionId).HasColumnName("RegionID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblUserShop>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.ShopId });

                entity.ToTable("tblUserShop");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");
            });

            modelBuilder.Entity<TblVaLog>(entity =>
            {
                entity.ToTable("tblVaLog");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Pcode).HasMaxLength(20);

                entity.Property(e => e.ReferenceId).HasMaxLength(200);
            });

            modelBuilder.Entity<TblVibInsurrance>(entity =>
            {
                entity.ToTable("tblVibInsurrance");

                entity.Property(e => e.BirthDay).HasColumnType("datetime");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.DataTransfer)
                    .HasColumnType("ntext")
                    .HasComment("Dữ liệu Json truyền sang cho phía đối tác");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ReasonCancel).HasMaxLength(500);
            });

            modelBuilder.Entity<TblWard>(entity =>
            {
                entity.ToTable("tblWard");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DistrictId).HasColumnName("DistrictID");

                entity.Property(e => e.LongitudeLatitude).HasMaxLength(200);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.TypeWard).HasMaxLength(50);
            });

            modelBuilder.Entity<TblWithdraw>(entity =>
            {
                entity.ToTable("tblWithdraw");

                entity.Property(e => e.BankCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BankId).HasColumnName("BankID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FullNameAgency).HasMaxLength(50);

                entity.Property(e => e.FullNameHeadquaters).HasMaxLength(50);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NoteAgency).HasMaxLength(500);

                entity.Property(e => e.NoteHeadquaters).HasMaxLength(500);
            });

            modelBuilder.Entity<TblWorkingIndustry>(entity =>
            {
                entity.HasKey(e => e.WorkingIndustryId);

                entity.ToTable("tblWorkingIndustry");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<VvGetDataSuspendedMoneyForFast>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vv_GetDataSuspendedMoneyForFast");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DiaChi)
                    .IsRequired()
                    .HasColumnName("dia_chi")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DienGiaiPhi)
                    .IsRequired()
                    .HasColumnName("dien_giai_phi")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DienGiaiTima)
                    .IsRequired()
                    .HasColumnName("dien_giai_tima")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HtTt)
                    .IsRequired()
                    .HasColumnName("ht_tt")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(52)
                    .IsUnicode(false);

                entity.Property(e => e.MaBp)
                    .IsRequired()
                    .HasColumnName("ma_bp")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh)
                    .IsRequired()
                    .HasColumnName("ma_kh")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh2)
                    .IsRequired()
                    .HasColumnName("ma_kh2")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaPhi)
                    .IsRequired()
                    .HasColumnName("ma_phi")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd1)
                    .IsRequired()
                    .HasColumnName("ma_td1")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd2)
                    .IsRequired()
                    .HasColumnName("ma_td2")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd3)
                    .IsRequired()
                    .HasColumnName("ma_td3")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaVv)
                    .IsRequired()
                    .HasColumnName("ma_vv")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.TenBp)
                    .IsRequired()
                    .HasColumnName("ten_bp")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TenKh)
                    .IsRequired()
                    .HasColumnName("ten_kh")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TenTd1)
                    .IsRequired()
                    .HasColumnName("ten_td1")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Tien).HasColumnName("tien");

                entity.Property(e => e.Tien1).HasColumnName("tien1");

                entity.Property(e => e.Tien2).HasColumnName("tien2");

                entity.Property(e => e.Tien3).HasColumnName("tien3");

                entity.Property(e => e.Tien4).HasColumnName("tien4");

                entity.Property(e => e.Tien5).HasColumnName("tien5");

                entity.Property(e => e.TkNh)
                    .HasColumnName("tk_nh")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<VvGetDataSuspendedMoneyForFastOfHub>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vv_GetDataSuspendedMoneyForFastOfHub");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DiaChi)
                    .IsRequired()
                    .HasColumnName("dia_chi")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DienGiaiPhi)
                    .IsRequired()
                    .HasColumnName("dien_giai_phi")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DienGiaiTima)
                    .IsRequired()
                    .HasColumnName("dien_giai_tima")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HtTt)
                    .IsRequired()
                    .HasColumnName("ht_tt")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(52)
                    .IsUnicode(false);

                entity.Property(e => e.MaBp)
                    .IsRequired()
                    .HasColumnName("ma_bp")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh)
                    .IsRequired()
                    .HasColumnName("ma_kh")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh2)
                    .IsRequired()
                    .HasColumnName("ma_kh2")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaPhi)
                    .IsRequired()
                    .HasColumnName("ma_phi")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd1)
                    .IsRequired()
                    .HasColumnName("ma_td1")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd2)
                    .IsRequired()
                    .HasColumnName("ma_td2")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd3)
                    .IsRequired()
                    .HasColumnName("ma_td3")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaVv)
                    .IsRequired()
                    .HasColumnName("ma_vv")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Note).HasMaxLength(50);

                entity.Property(e => e.TenBp)
                    .IsRequired()
                    .HasColumnName("ten_bp")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TenKh)
                    .IsRequired()
                    .HasColumnName("ten_kh")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TenTd1)
                    .IsRequired()
                    .HasColumnName("ten_td1")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Tien).HasColumnName("tien");

                entity.Property(e => e.Tien1).HasColumnName("tien1");

                entity.Property(e => e.Tien2).HasColumnName("tien2");

                entity.Property(e => e.Tien3).HasColumnName("tien3");

                entity.Property(e => e.Tien4).HasColumnName("tien4");

                entity.Property(e => e.Tien5).HasColumnName("tien5");

                entity.Property(e => e.TkNh)
                    .HasColumnName("tk_nh")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<VvGetDataTransactionBankCardForFast>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vv_GetDataTransactionBankCardForFast");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DiaChi)
                    .IsRequired()
                    .HasColumnName("dia_chi")
                    .HasMaxLength(256);

                entity.Property(e => e.DienGiaiPhi)
                    .IsRequired()
                    .HasColumnName("dien_giai_phi")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DienGiaiTima)
                    .IsRequired()
                    .HasColumnName("dien_giai_tima")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HtTt)
                    .IsRequired()
                    .HasColumnName("ht_tt")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(56)
                    .IsUnicode(false);

                entity.Property(e => e.MaBp)
                    .HasColumnName("ma_bp")
                    .HasMaxLength(31)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh)
                    .HasColumnName("ma_kh")
                    .HasMaxLength(255);

                entity.Property(e => e.MaKh2)
                    .HasColumnName("ma_kh2")
                    .HasMaxLength(255);

                entity.Property(e => e.MaPhi)
                    .IsRequired()
                    .HasColumnName("ma_phi")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd1)
                    .HasColumnName("ma_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.MaTd2)
                    .HasColumnName("ma_td2")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd3)
                    .IsRequired()
                    .HasColumnName("ma_td3")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaVv)
                    .HasColumnName("ma_vv")
                    .HasMaxLength(3);

                entity.Property(e => e.Note).HasMaxLength(63);

                entity.Property(e => e.TenBp)
                    .IsRequired()
                    .HasColumnName("ten_bp")
                    .HasMaxLength(50);

                entity.Property(e => e.TenKh)
                    .HasColumnName("ten_kh")
                    .HasMaxLength(50);

                entity.Property(e => e.TenTd1)
                    .HasColumnName("ten_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.Tien).HasColumnName("tien");

                entity.Property(e => e.Tien1).HasColumnName("tien1");

                entity.Property(e => e.Tien2).HasColumnName("tien2");

                entity.Property(e => e.Tien3).HasColumnName("tien3");

                entity.Property(e => e.Tien4).HasColumnName("tien4");

                entity.Property(e => e.Tien5).HasColumnName("tien5");

                entity.Property(e => e.TkNh)
                    .HasColumnName("tk_nh")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<VvGetDataTransactionBankCardForFastHub>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vv_GetDataTransactionBankCardForFastHub");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DiaChi)
                    .HasColumnName("dia_chi")
                    .HasMaxLength(256);

                entity.Property(e => e.DienGiaiPhi)
                    .IsRequired()
                    .HasColumnName("dien_giai_phi")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DienGiaiTima)
                    .IsRequired()
                    .HasColumnName("dien_giai_tima")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HtTt)
                    .IsRequired()
                    .HasColumnName("ht_tt")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(56)
                    .IsUnicode(false);

                entity.Property(e => e.MaBp)
                    .HasColumnName("ma_bp")
                    .HasMaxLength(31)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh)
                    .HasColumnName("ma_kh")
                    .HasMaxLength(255);

                entity.Property(e => e.MaKh2)
                    .HasColumnName("ma_kh2")
                    .HasMaxLength(255);

                entity.Property(e => e.MaPhi)
                    .IsRequired()
                    .HasColumnName("ma_phi")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd1)
                    .HasColumnName("ma_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.MaTd2)
                    .HasColumnName("ma_td2")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd3)
                    .IsRequired()
                    .HasColumnName("ma_td3")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaVv)
                    .HasColumnName("ma_vv")
                    .HasMaxLength(3);

                entity.Property(e => e.Note).HasMaxLength(63);

                entity.Property(e => e.TenBp)
                    .HasColumnName("ten_bp")
                    .HasMaxLength(50);

                entity.Property(e => e.TenKh)
                    .HasColumnName("ten_kh")
                    .HasMaxLength(50);

                entity.Property(e => e.TenTd1)
                    .HasColumnName("ten_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.Tien).HasColumnName("tien");

                entity.Property(e => e.Tien1).HasColumnName("tien1");

                entity.Property(e => e.Tien2).HasColumnName("tien2");

                entity.Property(e => e.Tien3).HasColumnName("tien3");

                entity.Property(e => e.Tien4).HasColumnName("tien4");

                entity.Property(e => e.Tien5).HasColumnName("tien5");

                entity.Property(e => e.TkNh)
                    .HasColumnName("tk_nh")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<VvReportCapitalForFast>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vv_ReportCapitalForFast");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DiaChi)
                    .HasColumnName("dia_chi")
                    .HasMaxLength(500);

                entity.Property(e => e.DienGiaiPhi)
                    .IsRequired()
                    .HasColumnName("dien_giai_phi")
                    .HasMaxLength(1);

                entity.Property(e => e.DienGiaiTima)
                    .IsRequired()
                    .HasColumnName("dien_giai_tima")
                    .HasMaxLength(1);

                entity.Property(e => e.HtTt)
                    .IsRequired()
                    .HasColumnName("ht_tt")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MaBp)
                    .HasColumnName("ma_bp")
                    .HasMaxLength(31)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh)
                    .HasColumnName("ma_kh")
                    .HasMaxLength(255);

                entity.Property(e => e.MaKh2)
                    .HasColumnName("ma_kh2")
                    .HasMaxLength(255);

                entity.Property(e => e.MaPhi)
                    .IsRequired()
                    .HasColumnName("ma_phi")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd1)
                    .HasColumnName("ma_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.MaTd2)
                    .HasColumnName("ma_td2")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd3)
                    .IsRequired()
                    .HasColumnName("ma_td3")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaVv)
                    .HasColumnName("ma_vv")
                    .HasMaxLength(3);

                entity.Property(e => e.Note).HasMaxLength(8);

                entity.Property(e => e.TenBp)
                    .HasColumnName("ten_bp")
                    .HasMaxLength(50);

                entity.Property(e => e.TenKh)
                    .HasColumnName("ten_kh")
                    .HasMaxLength(255);

                entity.Property(e => e.TenTd1)
                    .HasColumnName("ten_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.Tien).HasColumnName("tien");

                entity.Property(e => e.Tien1).HasColumnName("tien1");

                entity.Property(e => e.Tien2).HasColumnName("tien2");

                entity.Property(e => e.Tien3).HasColumnName("tien3");

                entity.Property(e => e.Tien4).HasColumnName("tien4");

                entity.Property(e => e.Tien5).HasColumnName("tien5");

                entity.Property(e => e.TkNh)
                    .IsRequired()
                    .HasColumnName("tk_nh")
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<VvReportCapitalForFastHub>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vv_ReportCapitalForFast_hub");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DiaChi)
                    .HasColumnName("dia_chi")
                    .HasMaxLength(500);

                entity.Property(e => e.DienGiaiPhi)
                    .IsRequired()
                    .HasColumnName("dien_giai_phi")
                    .HasMaxLength(1);

                entity.Property(e => e.DienGiaiTima)
                    .IsRequired()
                    .HasColumnName("dien_giai_tima")
                    .HasMaxLength(1);

                entity.Property(e => e.HtTt)
                    .IsRequired()
                    .HasColumnName("ht_tt")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MaBp)
                    .HasColumnName("ma_bp")
                    .HasMaxLength(31)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh)
                    .HasColumnName("ma_kh")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh2)
                    .HasColumnName("ma_kh2")
                    .HasMaxLength(255);

                entity.Property(e => e.MaPhi)
                    .IsRequired()
                    .HasColumnName("ma_phi")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd1)
                    .HasColumnName("ma_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.MaTd2)
                    .HasColumnName("ma_td2")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd3)
                    .IsRequired()
                    .HasColumnName("ma_td3")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaVv)
                    .HasColumnName("ma_vv")
                    .HasMaxLength(3);

                entity.Property(e => e.Note).HasMaxLength(8);

                entity.Property(e => e.TenBp)
                    .HasColumnName("ten_bp")
                    .HasMaxLength(50);

                entity.Property(e => e.TenKh)
                    .HasColumnName("ten_kh")
                    .HasMaxLength(255);

                entity.Property(e => e.TenTd1)
                    .HasColumnName("ten_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.Tien).HasColumnName("tien");

                entity.Property(e => e.Tien1).HasColumnName("tien1");

                entity.Property(e => e.Tien2).HasColumnName("tien2");

                entity.Property(e => e.Tien3).HasColumnName("tien3");

                entity.Property(e => e.Tien4).HasColumnName("tien4");

                entity.Property(e => e.Tien5).HasColumnName("tien5");

                entity.Property(e => e.TkNh)
                    .IsRequired()
                    .HasColumnName("tk_nh")
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VvReportCollectionExpense>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vv_ReportCollectionExpense");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DiaChi)
                    .HasColumnName("dia_chi")
                    .HasMaxLength(500);

                entity.Property(e => e.DienGiaiPhi)
                    .IsRequired()
                    .HasColumnName("dien_giai_phi")
                    .HasMaxLength(1);

                entity.Property(e => e.DienGiaiTima)
                    .IsRequired()
                    .HasColumnName("dien_giai_tima")
                    .HasMaxLength(1);

                entity.Property(e => e.HtTt)
                    .IsRequired()
                    .HasColumnName("ht_tt")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(33)
                    .IsUnicode(false);

                entity.Property(e => e.MaBp)
                    .HasColumnName("ma_bp")
                    .HasMaxLength(31)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh)
                    .HasColumnName("ma_kh")
                    .HasMaxLength(255);

                entity.Property(e => e.MaKh2)
                    .HasColumnName("ma_kh2")
                    .HasMaxLength(255);

                entity.Property(e => e.MaPhi)
                    .IsRequired()
                    .HasColumnName("ma_phi")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd1)
                    .HasColumnName("ma_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.MaTd2)
                    .HasColumnName("ma_td2")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd3)
                    .IsRequired()
                    .HasColumnName("ma_td3")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaVv)
                    .HasColumnName("ma_vv")
                    .HasMaxLength(3);

                entity.Property(e => e.Note).HasMaxLength(17);

                entity.Property(e => e.TenBp)
                    .HasColumnName("ten_bp")
                    .HasMaxLength(50);

                entity.Property(e => e.TenKh)
                    .HasColumnName("ten_kh")
                    .HasMaxLength(255);

                entity.Property(e => e.TenTd1)
                    .HasColumnName("ten_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.Tien).HasColumnName("tien");

                entity.Property(e => e.Tien1).HasColumnName("tien1");

                entity.Property(e => e.Tien2).HasColumnName("tien2");

                entity.Property(e => e.Tien3).HasColumnName("tien3");

                entity.Property(e => e.Tien4).HasColumnName("tien4");

                entity.Property(e => e.Tien5).HasColumnName("tien5");

                entity.Property(e => e.TkNh)
                    .IsRequired()
                    .HasColumnName("tk_nh")
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<VvReportCollectionExpenseHub>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vv_ReportCollectionExpense_hub");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DiaChi)
                    .HasColumnName("dia_chi")
                    .HasMaxLength(500);

                entity.Property(e => e.DienGiaiPhi)
                    .IsRequired()
                    .HasColumnName("dien_giai_phi")
                    .HasMaxLength(1);

                entity.Property(e => e.DienGiaiTima)
                    .IsRequired()
                    .HasColumnName("dien_giai_tima")
                    .HasMaxLength(1);

                entity.Property(e => e.HtTt)
                    .IsRequired()
                    .HasColumnName("ht_tt")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(33)
                    .IsUnicode(false);

                entity.Property(e => e.MaBp)
                    .HasColumnName("ma_bp")
                    .HasMaxLength(31)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh)
                    .HasColumnName("ma_kh")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh2)
                    .HasColumnName("ma_kh2")
                    .HasMaxLength(255);

                entity.Property(e => e.MaPhi)
                    .IsRequired()
                    .HasColumnName("ma_phi")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd1)
                    .HasColumnName("ma_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.MaTd2)
                    .HasColumnName("ma_td2")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd3)
                    .IsRequired()
                    .HasColumnName("ma_td3")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaVv)
                    .HasColumnName("ma_vv")
                    .HasMaxLength(3);

                entity.Property(e => e.Note).HasMaxLength(17);

                entity.Property(e => e.TenBp)
                    .HasColumnName("ten_bp")
                    .HasMaxLength(50);

                entity.Property(e => e.TenKh)
                    .HasColumnName("ten_kh")
                    .HasMaxLength(255);

                entity.Property(e => e.TenTd1)
                    .HasColumnName("ten_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.Tien).HasColumnName("tien");

                entity.Property(e => e.Tien1).HasColumnName("tien1");

                entity.Property(e => e.Tien2).HasColumnName("tien2");

                entity.Property(e => e.Tien3).HasColumnName("tien3");

                entity.Property(e => e.Tien4).HasColumnName("tien4");

                entity.Property(e => e.Tien5).HasColumnName("tien5");

                entity.Property(e => e.TkNh)
                    .IsRequired()
                    .HasColumnName("tk_nh")
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VvReportLendingForFast>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vv_ReportLendingForFast");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DiaChi)
                    .HasColumnName("dia_chi")
                    .HasMaxLength(256);

                entity.Property(e => e.DienGiaiPhi)
                    .IsRequired()
                    .HasColumnName("dien_giai_phi")
                    .HasMaxLength(14);

                entity.Property(e => e.DienGiaiTima)
                    .IsRequired()
                    .HasColumnName("dien_giai_tima")
                    .HasMaxLength(16);

                entity.Property(e => e.HtTt)
                    .IsRequired()
                    .HasColumnName("ht_tt")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MaBp)
                    .HasColumnName("ma_bp")
                    .HasMaxLength(31)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh)
                    .HasColumnName("ma_kh")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh2)
                    .HasColumnName("ma_kh2")
                    .HasMaxLength(255);

                entity.Property(e => e.MaPhi)
                    .IsRequired()
                    .HasColumnName("ma_phi")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd1)
                    .HasColumnName("ma_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.MaTd2)
                    .HasColumnName("ma_td2")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd3)
                    .IsRequired()
                    .HasColumnName("ma_td3")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaVv)
                    .HasColumnName("ma_vv")
                    .HasMaxLength(3);

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.TenBp)
                    .HasColumnName("ten_bp")
                    .HasMaxLength(50);

                entity.Property(e => e.TenKh)
                    .HasColumnName("ten_kh")
                    .HasMaxLength(50);

                entity.Property(e => e.TenTd1)
                    .HasColumnName("ten_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.Tien).HasColumnName("tien");

                entity.Property(e => e.Tien1).HasColumnName("tien1");

                entity.Property(e => e.Tien2).HasColumnName("tien2");

                entity.Property(e => e.Tien3).HasColumnName("tien3");

                entity.Property(e => e.Tien4).HasColumnName("tien4");

                entity.Property(e => e.Tien5).HasColumnName("tien5");

                entity.Property(e => e.TkNh)
                    .IsRequired()
                    .HasColumnName("tk_nh")
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VvReportLendingForFast4>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vv_ReportLendingForFast4");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CityId)
                    .HasMaxLength(31)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerAddress).HasMaxLength(256);

                entity.Property(e => e.CustomerId)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.DienGiai)
                    .IsRequired()
                    .HasColumnName("Dien Giai")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.DienGiaiPhi)
                    .IsRequired()
                    .HasColumnName("dien_giai_phi")
                    .HasMaxLength(14);

                entity.Property(e => e.DienGiaiTima)
                    .IsRequired()
                    .HasColumnName("dien_giai_tima")
                    .HasMaxLength(16);

                entity.Property(e => e.HtTt)
                    .IsRequired()
                    .HasColumnName("ht_tt")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LoaiSp)
                    .IsRequired()
                    .HasColumnName("LoaiSP")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaGd)
                    .HasColumnName("MaGD")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh2)
                    .HasColumnName("ma_kh2")
                    .HasMaxLength(255);

                entity.Property(e => e.MaTd1)
                    .HasColumnName("ma_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.MaVv)
                    .HasColumnName("ma_vv")
                    .HasMaxLength(3);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.TenTd1)
                    .HasColumnName("ten_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.Tien).HasColumnName("tien");

                entity.Property(e => e.Tien4).HasColumnName("tien4");

                entity.Property(e => e.Tien5).HasColumnName("tien5");

                entity.Property(e => e.TkNh)
                    .IsRequired()
                    .HasColumnName("tk_nh")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TypePayment)
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VvReportLendingForFastHub>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vv_ReportLendingForFast_Hub");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DiaChi)
                    .HasColumnName("dia_chi")
                    .HasMaxLength(256);

                entity.Property(e => e.DienGiaiPhi)
                    .IsRequired()
                    .HasColumnName("dien_giai_phi")
                    .HasMaxLength(14);

                entity.Property(e => e.DienGiaiTima)
                    .IsRequired()
                    .HasColumnName("dien_giai_tima")
                    .HasMaxLength(16);

                entity.Property(e => e.HtTt)
                    .IsRequired()
                    .HasColumnName("ht_tt")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MaBp)
                    .HasColumnName("ma_bp")
                    .HasMaxLength(31)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh)
                    .HasColumnName("ma_kh")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh2)
                    .HasColumnName("ma_kh2")
                    .HasMaxLength(255);

                entity.Property(e => e.MaPhi)
                    .IsRequired()
                    .HasColumnName("ma_phi")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd1)
                    .HasColumnName("ma_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.MaTd2)
                    .HasColumnName("ma_td2")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.MaTd3)
                    .IsRequired()
                    .HasColumnName("ma_td3")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MaVv)
                    .HasColumnName("ma_vv")
                    .HasMaxLength(3);

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.TenBp)
                    .HasColumnName("ten_bp")
                    .HasMaxLength(50);

                entity.Property(e => e.TenKh)
                    .HasColumnName("ten_kh")
                    .HasMaxLength(50);

                entity.Property(e => e.TenTd1)
                    .HasColumnName("ten_td1")
                    .HasMaxLength(2);

                entity.Property(e => e.Tien).HasColumnName("tien");

                entity.Property(e => e.Tien1).HasColumnName("tien1");

                entity.Property(e => e.Tien2).HasColumnName("tien2");

                entity.Property(e => e.Tien3).HasColumnName("tien3");

                entity.Property(e => e.Tien4).HasColumnName("tien4");

                entity.Property(e => e.Tien5).HasColumnName("tien5");

                entity.Property(e => e.TkNh)
                    .IsRequired()
                    .HasColumnName("tk_nh")
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VvReportSellSoftwareForFast>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vv_ReportSellSoftwareForFast");

                entity.Property(e => e.CityId)
                    .HasMaxLength(31)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.DiaChi).HasMaxLength(500);

                entity.Property(e => e.DienGiai)
                    .IsRequired()
                    .HasColumnName("Dien Giai")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.HtTt)
                    .IsRequired()
                    .HasColumnName("ht_tt")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LoaiDl)
                    .IsRequired()
                    .HasColumnName("Loai DL")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LoaiSp)
                    .IsRequired()
                    .HasColumnName("Loai SP")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaCh)
                    .IsRequired()
                    .HasColumnName("MaCH")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaDl)
                    .IsRequired()
                    .HasColumnName("MaDL")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MaGd)
                    .HasColumnName("MaGD")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MaKh2)
                    .IsRequired()
                    .HasColumnName("ma_kh2")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.NoiDung)
                    .IsRequired()
                    .HasMaxLength(6);

                entity.Property(e => e.TenCh)
                    .IsRequired()
                    .HasColumnName("TenCH")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Tien4).HasColumnName("tien4");

                entity.Property(e => e.Tien5).HasColumnName("tien5");

                entity.Property(e => e.TienGiaoDich).HasColumnName("Tien giao dich");

                entity.Property(e => e.TienLaiTraDl).HasColumnName("Tien Lai tra DL");

                entity.Property(e => e.TkNh)
                    .IsRequired()
                    .HasColumnName("tk_nh")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TongTienLai).HasColumnName("Tong Tien Lai");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
