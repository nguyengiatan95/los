﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class Notification
    {
        public int NotificationId { get; set; }
        public int? LoanCreditId { get; set; }
        public string Topic { get; set; }
        public string Action { get; set; }
        public string Message { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? Status { get; set; }
        public DateTime? PushTime { get; set; }
        public string Read { get; set; }
    }
}
