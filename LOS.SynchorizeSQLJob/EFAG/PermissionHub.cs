﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class PermissionHub
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? ProductId { get; set; }
        public int? Status { get; set; }
        public bool? IsCanCel { get; set; }
        public bool? IsVerify { get; set; }
        public bool? IsReturn { get; set; }
    }
}
