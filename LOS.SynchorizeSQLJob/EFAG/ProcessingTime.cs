﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class ProcessingTime
    {
        public long Id { get; set; }
        public long? LoanCreditId { get; set; }
        public int? UserId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public int? Type { get; set; }
        public int? Status { get; set; }
        public short? Steps { get; set; }
    }
}
