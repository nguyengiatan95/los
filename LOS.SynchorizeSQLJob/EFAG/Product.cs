﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class Product
    {
        public string Cust { get; set; }
        public string Product1 { get; set; }
        public int? Qty { get; set; }
        public int Id { get; set; }
    }
}
