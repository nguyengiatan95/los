﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblAdsForm
    {
        public int Id { get; set; }
        public long? FormId { get; set; }
        public int? AdsPageId { get; set; }
        public long? LastIdGet { get; set; }
        public short? Status { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Description { get; set; }
        public int? ProductId { get; set; }
        public string UtmSource { get; set; }
        public int? CityId { get; set; }
        public long? TotalMoney { get; set; }
        public int? LoanTime { get; set; }
        public short? TypeTime { get; set; }
    }
}
