﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblAdsPage
    {
        public int Id { get; set; }
        public long? PageId { get; set; }
        public string AccessToken { get; set; }
        public short? Status { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
    }
}
