﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblAffLender
    {
        public int Id { get; set; }
        public int? AffId { get; set; }
        public int? LenderId { get; set; }
        public decimal? RateAff { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? Status { get; set; }
    }
}
