﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblAgencyRefuse
    {
        public int Id { get; set; }
        public int? ShopId { get; set; }
        public int? LoanCreditId { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
