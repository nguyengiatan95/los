﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblAlarmDateLog
    {
        public int Id { get; set; }
        public long LoanId { get; set; }
        public string NoteAlarm { get; set; }
        public DateTime? AlarmDate { get; set; }
        public byte? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? TypeId { get; set; }
        public string UserAction { get; set; }
    }
}
