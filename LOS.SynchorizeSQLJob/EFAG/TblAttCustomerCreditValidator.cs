﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblAttCustomerCreditValidator
    {
        public int Id { get; set; }
        public int? CustomerCreditId { get; set; }
        public int? LoanCreditId { get; set; }
        public string ContentValidatorAttribute { get; set; }
    }
}
