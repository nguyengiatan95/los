﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblAwardContract
    {
        public int Id { get; set; }
        public int? AwardContractId { get; set; }
        public int? Status { get; set; }
    }
}
