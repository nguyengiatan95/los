﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblBadDebReason
    {
        public long Id { get; set; }
        public int? LoanId { get; set; }
        public int? ShopId { get; set; }
        public int? UserId { get; set; }
        public int? CusId { get; set; }
        public int? ReasonId { get; set; }
        public string ReasonContent { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
