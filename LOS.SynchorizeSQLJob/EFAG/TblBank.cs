﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblBank
    {
        public int Id { get; set; }
        public string NameBank { get; set; }
        public string Code { get; set; }
        public bool? Status { get; set; }
        public short? TypeBank { get; set; }
        public int? ValueOfBank { get; set; }
    }
}
