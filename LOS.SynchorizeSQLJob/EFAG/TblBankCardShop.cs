﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblBankCardShop
    {
        public int Id { get; set; }
        public int BankCardId { get; set; }
        public int ShopId { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public DateTime? CreateOn { get; set; }
        public byte? Status { get; set; }
        public int? UserIdModify { get; set; }
        public string UserNameModify { get; set; }
        public string UserFullNameModify { get; set; }
        public DateTime? ModifyOn { get; set; }
    }
}
