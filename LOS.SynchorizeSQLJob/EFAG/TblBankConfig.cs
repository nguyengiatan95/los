﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblBankConfig
    {
        public int Id { get; set; }
        public string BankName { get; set; }
        public string BankAlias { get; set; }
        public string TextFirst { get; set; }
        public string TextLast { get; set; }
        public string TextBegin { get; set; }
        public string TextEnd { get; set; }
        public int? Status { get; set; }
    }
}
