﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblBlackList
    {
        public int Id { get; set; }
        public int? CustomerCreditId { get; set; }
        public string FullName { get; set; }
        public string NumberPhone { get; set; }
        public string CardNumber { get; set; }
        public DateTime? BirthDay { get; set; }
        public int? LoanId { get; set; }
        public int? UserIdCreate { get; set; }
        public string UserNameCreate { get; set; }
        public string FullNameCreate { get; set; }
        public DateTime? CreateOn { get; set; }
        public int? Status { get; set; }
        public int? UserIdModify { get; set; }
        public string UserNameModify { get; set; }
        public string FullNameModify { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string Note { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? NumberDay { get; set; }
        public int? LoanCreditId { get; set; }
        public int? UserIdApprove { get; set; }
        public string UserNameApprove { get; set; }
        public string FullNameApprove { get; set; }
        public DateTime? TimeApprove { get; set; }
    }
}
