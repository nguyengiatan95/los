﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblBlackListLog
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? UserIdGroupId { get; set; }
        public int? ShopId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? BlackListId { get; set; }
        public string FullName { get; set; }
        public string NumberPhone { get; set; }
        public string CardNumber { get; set; }
        public DateTime? BirthDay { get; set; }
        public int? CheckResultResult { get; set; }
        public int? LoanCreditId { get; set; }
        public int? PlatformLoanCreditId { get; set; }
    }
}
