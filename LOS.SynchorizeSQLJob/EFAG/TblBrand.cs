﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblBrand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? IdCategory { get; set; }
        public int? Status { get; set; }
    }
}
