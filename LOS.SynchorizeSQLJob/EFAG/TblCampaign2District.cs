﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCampaign2District
    {
        public int CampaignId { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
