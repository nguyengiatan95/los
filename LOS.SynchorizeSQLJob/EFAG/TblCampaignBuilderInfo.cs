﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCampaignBuilderInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? TotalMoney { get; set; }
        public string CampaignManager { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
