﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCampaignGenUrl
    {
        public int Id { get; set; }
        public int? CampaignBuilderInfoId { get; set; }
        public string UtmSource { get; set; }
        public string UtmMedium { get; set; }
        public string UtmCampaign { get; set; }
        public string UtmContent { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string LinkUrl { get; set; }
        public int? UserId { get; set; }
        public string WebsiteUrl { get; set; }
        public int? Status { get; set; }
    }
}
