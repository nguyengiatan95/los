﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCampaignUrlbuilder
    {
        public int Id { get; set; }
        public string UtmSource { get; set; }
        public string UtmMedium { get; set; }
        public string UtmContent { get; set; }
        public string CampaignRunner { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
