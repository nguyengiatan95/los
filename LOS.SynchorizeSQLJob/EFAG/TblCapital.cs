﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCapital
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? ShopId { get; set; }
        public int? CustomerId { get; set; }
        public long? TotalMoney { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? LoanTime { get; set; }
        public DateTime? LastDateOfPay { get; set; }
        public DateTime? NextDate { get; set; }
        public float? Rate { get; set; }
        public byte? RateType { get; set; }
        public int? Frequency { get; set; }
        public long? PaymentMoney { get; set; }
        public byte? Status { get; set; }
        public string Note { get; set; }
        public byte? IsBefore { get; set; }
        public long? DebitMoney { get; set; }
        public long? TotalInterest { get; set; }
        public string BankCode { get; set; }
        public int? BankId { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
