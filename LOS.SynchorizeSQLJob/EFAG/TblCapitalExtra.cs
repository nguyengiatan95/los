﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCapitalExtra
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ShopId { get; set; }
        public int CapitalId { get; set; }
        public long TotalMoney { get; set; }
        public DateTime DateExtra { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Note { get; set; }
    }
}
