﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCapitalMember
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string NameMember { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Cmnd { get; set; }
        public DateTime? DateContribution { get; set; }
        public long? Money { get; set; }
        public string Note { get; set; }
        public short? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
