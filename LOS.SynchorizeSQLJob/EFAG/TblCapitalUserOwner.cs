﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCapitalUserOwner
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public long? CapitalMobilization { get; set; }
        public long? CapitalAllShop { get; set; }
        public long? CapitalExtant { get; set; }
    }
}
