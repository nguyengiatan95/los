﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCashBankCardByDate
    {
        public int Id { get; set; }
        public int? ShopId { get; set; }
        public string ShopName { get; set; }
        public int? BankCardId { get; set; }
        public string BankCardName { get; set; }
        public DateTime? ForDate { get; set; }
        public long? MoneyBeginDate { get; set; }
        public long? MoneyEndDate { get; set; }
        public long? MoneyInBound { get; set; }
        public long? MoneyOutBound { get; set; }
        public bool? Changed { get; set; }
    }
}
