﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCashByDate
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public DateTime ForDate { get; set; }
        public long CapitalBeginDate { get; set; }
        public long TotalAssetsBeginDate { get; set; }
        public long MoneyBeginDate { get; set; }
        public long CapitalAdd { get; set; }
        public long TransactionLoan { get; set; }
        public long TransactionInstallment { get; set; }
        public long? TransactionPawnNew { get; set; }
        public long Collapse { get; set; }
        public long Expenses { get; set; }
        public long CapitalSub { get; set; }
        public long MoneyEndDate { get; set; }
        public long LoanDebts { get; set; }
        public long InstallmentDebts { get; set; }
        public long? PawnNewDebts { get; set; }
        public long CapitalBorrowing { get; set; }
        public long TotalAssets { get; set; }
        public long CapitalEndDate { get; set; }
        public long LoanDebit { get; set; }
        public long InstallmentDebit { get; set; }
        public long? PawnNewDebit { get; set; }
    }
}
