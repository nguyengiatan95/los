﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCategory
    {
        public int CategoryId { get; set; }
        public int? ShopId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public byte? TypeId { get; set; }
        public long? TotalMoney { get; set; }
        public float? Rate { get; set; }
        public byte? RateType { get; set; }
        public int? Frequency { get; set; }
        public byte? IsBefore { get; set; }
        public byte? MaxDayOverdue { get; set; }
        public byte? Status { get; set; }
        public string Extend1 { get; set; }
        public string Extend2 { get; set; }
        public string Extend3 { get; set; }
        public string Extend4 { get; set; }
        public string Extend5 { get; set; }
        public string Extend6 { get; set; }
        public string Extend7 { get; set; }
        public string Extend8 { get; set; }
    }
}
