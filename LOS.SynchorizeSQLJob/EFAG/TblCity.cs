﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TypeCity { get; set; }
        public byte? Position { get; set; }
        public byte? IsApply { get; set; }
        public byte? IsCity { get; set; }
        public byte? DomainId { get; set; }
        public byte? RegionId { get; set; }
        public int? ProvinceCode { get; set; }
    }
}
