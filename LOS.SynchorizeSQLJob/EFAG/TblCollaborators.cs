﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCollaborators
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string NumberCard { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UserIdCreated { get; set; }
    }
}
