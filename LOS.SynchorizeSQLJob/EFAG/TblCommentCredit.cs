﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCommentCredit
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? ShopId { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public short? ActionId { get; set; }
        public string Comment { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? GroupUserId { get; set; }
        public int? ReasonId { get; set; }
        public int? StatusLoanCredit { get; set; }
        public int? ToUserId { get; set; }
        public string ToFullName { get; set; }
        public byte? IsDisplay { get; set; }
        public byte? IsManual { get; set; }
    }
}
