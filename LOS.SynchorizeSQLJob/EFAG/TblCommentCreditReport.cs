﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCommentCreditReport
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? ShopId { get; set; }
        public int? UserId { get; set; }
        public int? GroupUserId { get; set; }
        public string FullName { get; set; }
        public short? ActionId { get; set; }
        public string Comment { get; set; }
        public DateTime? CreateDate { get; set; }
        public byte? ReasonId { get; set; }
        public int? StatusLoanCredit { get; set; }
        public DateTime? CreateDateFromLoanCredit { get; set; }
        public int? CustomerCreditId { get; set; }
        public string CustomerPhone { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public string CustomerCreditName { get; set; }
        public int? CommentCreditId { get; set; }
        public int? CounselorId { get; set; }
        public string CounselorName { get; set; }
    }
}
