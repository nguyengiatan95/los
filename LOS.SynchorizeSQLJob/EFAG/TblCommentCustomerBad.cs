﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCommentCustomerBad
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public int CustomerBadId { get; set; }
        public int UserId { get; set; }
        public byte Status { get; set; }
        public string Phone { get; set; }
        public int ShopId { get; set; }
        public DateTime CreateDate { get; set; }
        public string ShopName { get; set; }
        public string Result { get; set; }
    }
}
