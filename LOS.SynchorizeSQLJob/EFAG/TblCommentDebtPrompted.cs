﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCommentDebtPrompted
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public int? TypeId { get; set; }
        public int? UserId { get; set; }
        public int? ShopId { get; set; }
        public string FullName { get; set; }
        public string Comment { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? ReasonId { get; set; }
        public short? ActionPerson { get; set; }
        public short? ActionAddress { get; set; }
        public DateTime? NextDate { get; set; }
        public int? GroupId { get; set; }
        public string CReasonCode { get; set; }
    }
}
