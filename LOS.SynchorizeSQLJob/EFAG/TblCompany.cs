﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCompany
    {
        public int Id { get; set; }
        public string NameCompany { get; set; }
        public bool? IsEnable { get; set; }
        public int? Position { get; set; }
    }
}
