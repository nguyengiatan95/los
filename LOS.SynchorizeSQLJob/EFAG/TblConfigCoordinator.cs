﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblConfigCoordinator
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public int? IsLocate { get; set; }
        public int? ProductId { get; set; }
        public DateTime? Createdate { get; set; }
    }
}
