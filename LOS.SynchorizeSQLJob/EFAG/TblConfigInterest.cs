﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblConfigInterest
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public int? LoanTime { get; set; }
        public long? FromMoney { get; set; }
        public long? ToMoney { get; set; }
        public decimal? Interest { get; set; }
    }
}
