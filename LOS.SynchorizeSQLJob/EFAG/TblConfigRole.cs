﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblConfigRole
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int? NumberOfLoans { get; set; }
        public string WarningDebt { get; set; }
        public string BadDebt { get; set; }
        public byte? TypeOfOwnership { get; set; }
        public int? Tdn { get; set; }
        public int? Tdct { get; set; }
        public string FromCondition { get; set; }
        public long? TotalMoneyFirstFr { get; set; }
        public string ToCondition { get; set; }
        public long? TotalMoneyFirstTo { get; set; }
        public int? ProductId { get; set; }
        public int? ReceiveYourIncome { get; set; }
        public int? JobId { get; set; }
    }
}
