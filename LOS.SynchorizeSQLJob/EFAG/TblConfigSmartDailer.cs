﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblConfigSmartDailer
    {
        public int Id { get; set; }
        public int? CampaignId { get; set; }
        public DateTime? StartAmCall { get; set; }
        public DateTime? EndAmCall { get; set; }
        public DateTime? StartPmCall { get; set; }
        public DateTime? EndPmCall { get; set; }
        public int? MaxCall { get; set; }
        public int? TimeRecall { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
