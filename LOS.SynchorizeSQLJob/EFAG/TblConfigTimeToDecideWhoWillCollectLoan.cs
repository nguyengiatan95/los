﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblConfigTimeToDecideWhoWillCollectLoan
    {
        public string UserType { get; set; }
        public int? FromLateDate { get; set; }
        public int? ToLateDate { get; set; }
    }
}
