﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblConfigUserRemindDebtByLoanAndMonth
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public string MonthAndYear { get; set; }
        public DateTime? ForDate { get; set; }
        public string CustomerName { get; set; }
        public DateTime? NextDate { get; set; }
        public string CityName { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public int? LastUserId { get; set; }
        public string LastUserName { get; set; }
        public int? CurrentUserId { get; set; }
        public string CurrentUserName { get; set; }
        public int? FreeUserId { get; set; }
        public string FreeUserName { get; set; }
        public short? IsResolve { get; set; }
        public int? CreateUserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? IsWorking { get; set; }
        public int? CodeId { get; set; }
        public int? ProcessUserId { get; set; }
        public long? MoneyNeedCollect { get; set; }
        public long? TotalMoneyReceived { get; set; }
        public int? CountDay { get; set; }
        public DateTime? FirstUpdate { get; set; }
        public DateTime? FirstDate { get; set; }
        public short? IsGoodContract { get; set; }
        public int? TeamleadUserId { get; set; }
        public string TeamleadUserName { get; set; }
        public int? SectionManagerUserId { get; set; }
        public string SectionManagerUsername { get; set; }
        public int? DepartmentManagerUserId { get; set; }
        public string DepartmentManagerUsername { get; set; }
        public int? DpdBom { get; set; }
    }
}
