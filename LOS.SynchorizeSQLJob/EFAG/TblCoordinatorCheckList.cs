﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCoordinatorCheckList
    {
        public int Id { get; set; }
        public int? GroupId { get; set; }
        public int? UserId { get; set; }
        public int? LoanCreditId { get; set; }
        public string ContentCheckList { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
