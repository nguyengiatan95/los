﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCoordinatorCriteria
    {
        public int CoordinatorCriteriaId { get; set; }
        public int? ParentId { get; set; }
        public string LableName { get; set; }
        public int? Type { get; set; }
        public string Note { get; set; }
        public bool? IsUse { get; set; }
        public bool? IsCancel { get; set; }
        public int? ReasonId { get; set; }
    }
}
