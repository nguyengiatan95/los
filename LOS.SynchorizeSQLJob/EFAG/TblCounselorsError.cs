﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCounselorsError
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CounselorId { get; set; }
        public int? ErrorId { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreateOn { get; set; }
        public string Note { get; set; }
        public string CounselorName { get; set; }
        public string UserName { get; set; }
        public int? GroupUserError { get; set; }
        public int? Step { get; set; }
    }
}
