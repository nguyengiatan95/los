﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCustomer
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string NumberCard { get; set; }
        public short? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ShopId { get; set; }
        public string CardDate { get; set; }
        public string Place { get; set; }
        public int? DocumentId { get; set; }
        public string DocumentValue { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public string PhoneRelationShip { get; set; }
        public long? TotalMoney { get; set; }
        public string RegistrationCustomerId { get; set; }
        public string PermanentAddress { get; set; }
        public string AddressHouseHold { get; set; }
        public string ListPhone { get; set; }
        public short? StatusReadImgCmt { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string BankCodeVa { get; set; }
        public string BankNameVa { get; set; }
        public string MapId { get; set; }
        public int? StatusVa { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
