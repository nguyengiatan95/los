﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCustomerBad
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string CardNumber { get; set; }
        public int? BirthdayYear { get; set; }
        public short Status { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int ShopId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UrlImg { get; set; }
    }
}
