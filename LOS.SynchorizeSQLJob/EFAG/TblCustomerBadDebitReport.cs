﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCustomerBadDebitReport
    {
        public int Id { get; set; }
        public int? ShopId { get; set; }
        public int? LoanId { get; set; }
        public long? TotalMoney { get; set; }
        public long? InterestMoney { get; set; }
        public int? DebitMoney { get; set; }
        public int? CusId { get; set; }
        public string CusName { get; set; }
        public string CusPhone { get; set; }
        public string CusAddress { get; set; }
        public string Note { get; set; }
        public int? FromDate { get; set; }
        public int? ToDate { get; set; }
        public int? CountDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public string NumberCard { get; set; }
    }
}
