﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCustomerCare
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public int? LoanId { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? CreateOn { get; set; }
        public string Content { get; set; }
        public double? Rate { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CustomerCreditId { get; set; }
    }
}
