﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCustomerCreditFiles
    {
        public int Id { get; set; }
        public int? CustomerCreditId { get; set; }
        public DateTime? CreateOn { get; set; }
        public string UrlImg { get; set; }
        public int? UserId { get; set; }
        public byte? Status { get; set; }
        public int? LoanCreditId { get; set; }
        public int? TypeId { get; set; }
        public int? StatusLoanCredit { get; set; }
        public int? S3status { get; set; }
        public int? TransactionBankCardId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? ModifyUserId { get; set; }
        public string LinkImgOfPartner { get; set; }
        public byte? TypeFile { get; set; }
        public int? CommentCreditId { get; set; }
        public int? PlatformCustomerCreditId { get; set; }
        public long? PlatformImgFileId { get; set; }
        public byte? Synchronize { get; set; }
        public string DomainOfPartner { get; set; }
        public int? DeleteStatus { get; set; }
        public int? CountProcess { get; set; }
        public DateTime? UploadDate { get; set; }
        public long? CommentThnid { get; set; }
    }
}
