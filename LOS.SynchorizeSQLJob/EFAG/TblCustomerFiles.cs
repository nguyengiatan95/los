﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCustomerFiles
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int? UserId { get; set; }
        public string FilePath { get; set; }
        public DateTime? CreateDate { get; set; }
        public byte? Status { get; set; }
    }
}
