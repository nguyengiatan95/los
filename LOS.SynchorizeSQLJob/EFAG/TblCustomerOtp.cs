﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCustomerOtp
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public long? Otp { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ExprireDate { get; set; }
        public int? Status { get; set; }
        public int? CodeResult { get; set; }
        public int? Smsid { get; set; }
    }
}
