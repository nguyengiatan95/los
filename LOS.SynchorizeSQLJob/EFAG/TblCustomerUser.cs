﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCustomerUser
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Token { get; set; }
        public DateTime? ExprireToken { get; set; }
        public int? Status { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public string Facebook { get; set; }
        public string FullName { get; set; }
        public string CardNumber { get; set; }
        public int? Gender { get; set; }
        public DateTime? Birthday { get; set; }
        public int? WardId { get; set; }
        public int? LivingTimeId { get; set; }
        public int? TypeOfOwnershipId { get; set; }
        public string AddressHouseHold { get; set; }
        public int? JobId { get; set; }
        public string CompanyName { get; set; }
        public string AddressCompany { get; set; }
        public string CompanyPhone { get; set; }
        public long? Salary { get; set; }
        public string FullNameFamily { get; set; }
        public string PhoneFamily { get; set; }
        public int? RelativeFamilyId { get; set; }
        public string Street { get; set; }
    }
}
