﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblCustomerWc
    {
        public int Id { get; set; }
        public int? CustomerCreditId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public bool? SendSms { get; set; }
    }
}
