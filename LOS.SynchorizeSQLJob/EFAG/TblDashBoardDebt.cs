﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblDashBoardDebt
    {
        public int Id { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public int? CategoryShop { get; set; }
        public string ShopName { get; set; }
        public long? TotalMoney { get; set; }
        public long? MoneyDisbursement { get; set; }
        public long? MoneyDisbursementProceeds { get; set; }
        public long? MoneyDisbursementExtra { get; set; }
    }
}
