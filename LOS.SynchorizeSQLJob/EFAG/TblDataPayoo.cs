﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblDataPayoo
    {
        public int Id { get; set; }
        public string NumberCard { get; set; }
        public string Phone { get; set; }
        public decimal? MoneyCheck { get; set; }
        public DateTime? DateCheck { get; set; }
        public string FinCompany { get; set; }
        public string FullName { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
