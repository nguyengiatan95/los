﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblDataSimViettel
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public string DataTelcoOfSimViettel { get; set; }
        public string DataFacebookOfSimViettel { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public byte? StatusCrawlTelco { get; set; }
        public byte? StatusCrawlFacebook { get; set; }
        public double? Score { get; set; }
        public bool? Decision { get; set; }
        public bool? AuthorizedFacebook { get; set; }
        public bool? AuthorizedSim { get; set; }
        public bool? CrawledFacebook { get; set; }
        public bool? CrawledSim { get; set; }
    }
}
