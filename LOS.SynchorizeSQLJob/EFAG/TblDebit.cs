﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblDebit
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public long? Debit { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? PaymentId { get; set; }
        public long? PayMoney { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? ShopId { get; set; }
        public byte? Status { get; set; }
    }
}
