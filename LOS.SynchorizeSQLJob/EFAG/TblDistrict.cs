﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblDistrict
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TypeDistrict { get; set; }
        public string LongitudeLatitude { get; set; }
        public int? CityId { get; set; }
        public int? AreaId { get; set; }
        public byte? IsApply { get; set; }
        public int? DistrictCode { get; set; }
    }
}
