﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblDocument
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? Status { get; set; }
        public int? Type { get; set; }
    }
}
