﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblDocumentCustomer
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? ConfirmCardNumber { get; set; }
        public int? ConfirmMotobikeCertificate { get; set; }
        public int? ComfirmHousehold { get; set; }
        public int? ConfirmKt3 { get; set; }
    }
}
