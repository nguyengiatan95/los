﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblDocumentLoanCredit
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? DocumentId { get; set; }
    }
}
