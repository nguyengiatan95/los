﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblDomain
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
