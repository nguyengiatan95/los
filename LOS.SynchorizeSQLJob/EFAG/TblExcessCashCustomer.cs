﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblExcessCashCustomer
    {
        public long Id { get; set; }
        public int? CustomerId { get; set; }
        public int? ShopId { get; set; }
        public string FullName { get; set; }
        public string CardNumber { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public long? TotalMoney { get; set; }
        public DateTime? ForDate { get; set; }
    }
}
