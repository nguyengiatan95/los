﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblExpectedPayment
    {
        public long Id { get; set; }
        public long? LoanId { get; set; }
        public int? Period { get; set; }
        public DateTime? ScheduledInstallmentDate { get; set; }
        public long? ScheduledInstallmentAmount { get; set; }
        public long? ScheduledPrincipal { get; set; }
        public long? ScheduledInterest { get; set; }
        public long? ScheduledVat { get; set; }
        public long? ScheduledOutstanding { get; set; }
        public long? MoneyInterest { get; set; }
        public long? MoneyServices { get; set; }
        public long? MoneyConsultant { get; set; }
        public int? RateType { get; set; }
    }
}
