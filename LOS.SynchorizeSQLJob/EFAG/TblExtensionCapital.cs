﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblExtensionCapital
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int CapitalId { get; set; }
        public DateTime OldDate { get; set; }
        public DateTime NewDate { get; set; }
        public string Note { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
