﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblFieldEvaluationReport
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? GroupId { get; set; }
        public int? LoanCreditId { get; set; }
        public int? ProductId { get; set; }
        public int? TypeReport { get; set; }
        public int? Type { get; set; }
        public string Note { get; set; }
        public DateTime? ReportDate { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
