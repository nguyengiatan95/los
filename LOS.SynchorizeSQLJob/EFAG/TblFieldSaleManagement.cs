﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblFieldSaleManagement
    {
        public int Id { get; set; }
        public int FeildSaleManagerId { get; set; }
        public int FieldSaleId { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
