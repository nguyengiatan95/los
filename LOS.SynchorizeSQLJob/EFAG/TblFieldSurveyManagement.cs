﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblFieldSurveyManagement
    {
        public int Id { get; set; }
        public int FieldSurveyManagerId { get; set; }
        public int FieldSurveyId { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
