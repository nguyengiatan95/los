﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblFileManagement
    {
        public int Id { get; set; }
        public int? CustomerBadId { get; set; }
        public DateTime? CreateOn { get; set; }
        public string UrlImg { get; set; }
    }
}
