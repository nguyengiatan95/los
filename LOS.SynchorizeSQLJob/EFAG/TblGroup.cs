﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Permissions { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
