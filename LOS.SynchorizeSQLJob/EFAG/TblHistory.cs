﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblHistory
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Note { get; set; }
    }
}
