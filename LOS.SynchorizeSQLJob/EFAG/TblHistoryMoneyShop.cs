﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblHistoryMoneyShop
    {
        public int Id { get; set; }
        public int? ShopId { get; set; }
        public int? UserId { get; set; }
        public int? TypeId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Note { get; set; }
        public long? MoneyTranfer { get; set; }
    }
}
