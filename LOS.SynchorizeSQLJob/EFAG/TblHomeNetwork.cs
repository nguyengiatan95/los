﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblHomeNetwork
    {
        public int Id { get; set; }
        public string HomeNetwork { get; set; }
        public string NetworkAlias { get; set; }
        public string TextFullName { get; set; }
        public string TextBirtday { get; set; }
        public string TextCmt { get; set; }
        public string TextEnd { get; set; }
        public int? Status { get; set; }
    }
}
