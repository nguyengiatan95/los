﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblIndividualBusiness
    {
        public int Id { get; set; }
        public long? TotalMoneyFirst { get; set; }
        public long? AverageRevenue3Month { get; set; }
        public long? LowestRevenueMonth { get; set; }
        public int? TypePlace { get; set; }
        public int? RemainingTimeContract { get; set; }
        public long? MoneyDeposit { get; set; }
        public long? MoneyRemainingRent { get; set; }
        public long? AssetValue { get; set; }
        public int? GroupCic { get; set; }
        public int? TypeResident { get; set; }
        public long? TotalMoney { get; set; }
        public long? FeeAndInterest { get; set; }
        public int? LoanCreditId { get; set; }
        public int? UserId { get; set; }
        public int? GroupUserId { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
