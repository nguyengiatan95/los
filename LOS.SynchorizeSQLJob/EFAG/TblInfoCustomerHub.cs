﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblInfoCustomerHub
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public DateTime? Birthday { get; set; }
        public string Phone { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? WardId { get; set; }
        public string Street { get; set; }
        public short? Gender { get; set; }
        public string Note { get; set; }
    }
}
