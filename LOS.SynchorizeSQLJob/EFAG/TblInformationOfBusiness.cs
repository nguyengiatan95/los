﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblInformationOfBusiness
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? Birthday { get; set; }
        public string CardNumber { get; set; }
        public DateTime? DateCardNumber { get; set; }
        public string IssuedByCardNumber { get; set; }
        public string Shareholder { get; set; }
        public string CompanyName { get; set; }
        public string MainBusinessSectors { get; set; }
        public string BusinessAddress { get; set; }
        public string HeadquarterBusinessRegistration { get; set; }
        public DateTime? DateCertificate { get; set; }
        public string IssuedByCertificate { get; set; }
    }
}
