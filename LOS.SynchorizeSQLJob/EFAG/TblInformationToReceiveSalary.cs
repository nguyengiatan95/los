﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblInformationToReceiveSalary
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public DateTime? DateStartWork { get; set; }
        public DateTime? ContractDateEnds { get; set; }
        public long? UserSalaryDeclaration { get; set; }
        public long? SalaryContract { get; set; }
        public long? SalaryLastMonth { get; set; }
        public long? Salary2rdMonthNearest { get; set; }
        public long? Salary3rdMonthNearest { get; set; }
        public int? UserIdCreate { get; set; }
        public string UserNameCreate { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UserIdModify { get; set; }
        public string UserNameModify { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
