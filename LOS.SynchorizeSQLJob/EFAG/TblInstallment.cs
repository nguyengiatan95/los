﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblInstallment
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ShopId { get; set; }
        public int CustomerId { get; set; }
        public long TotalMoney { get; set; }
        public long TotalMoneyReceived { get; set; }
        public long TotalMoneyCurrent { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int LoanTime { get; set; }
        public DateTime? LastDateOfPay { get; set; }
        public float Rate { get; set; }
        public short Frequency { get; set; }
        public byte IsBefore { get; set; }
        public long PaymentMoney { get; set; }
        public short Status { get; set; }
        public DateTime? NextDate { get; set; }
        public int? ApproveBy { get; set; }
        public string Note { get; set; }
        public long TotalInterest { get; set; }
        public long InterestMoneyReceived { get; set; }
        public int DebitMoney { get; set; }
        public int? ManageId { get; set; }
        public int? CodeId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? FrequencyMoney { get; set; }
    }
}
