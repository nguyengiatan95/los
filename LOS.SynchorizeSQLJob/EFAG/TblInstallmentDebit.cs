﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblInstallmentDebit
    {
        public int DebitId { get; set; }
        public int? ShopId { get; set; }
        public int? UserId { get; set; }
        public int? InstallmentId { get; set; }
        public DateTime? ForDate { get; set; }
        public long? PayMoney { get; set; }
        public byte? Status { get; set; }
    }
}
