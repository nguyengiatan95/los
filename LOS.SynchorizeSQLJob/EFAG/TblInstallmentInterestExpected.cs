﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblInstallmentInterestExpected
    {
        public int InstallmentId { get; set; }
        public DateTime ForDate { get; set; }
        public int InterestMoney { get; set; }
    }
}
