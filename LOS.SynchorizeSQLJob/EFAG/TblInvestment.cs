﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblInvestment
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string NumberPhone { get; set; }
        public long? InvestmentMoney { get; set; }
        public int? InvestmentMonth { get; set; }
        public decimal? Rate { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? UserId { get; set; }
        public string FullNameUser { get; set; }
    }
}
