﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblJob
    {
        public int JobId { get; set; }
        public string Name { get; set; }
        public byte? Sort { get; set; }
        public int? IsApply { get; set; }
        public int? TypeReason { get; set; }
        public DateTime? CreatedOn { get; set; }
        public byte? Status { get; set; }
    }
}
