﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblKpicounselors
    {
        public int Id { get; set; }
        public int? SuperAreaId { get; set; }
        public int? DistrictId { get; set; }
        public byte? Month { get; set; }
        public short? Year { get; set; }
        public int? UserId { get; set; }
        public int? NumberKpi { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
