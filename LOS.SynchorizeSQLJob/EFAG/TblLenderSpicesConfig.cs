﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLenderSpicesConfig
    {
        public int ShopId { get; set; }
        public string LoanTimes { get; set; }
        public int? RateType { get; set; }
        public long MoneyMin { get; set; }
        public long MoneyMax { get; set; }
    }
}
