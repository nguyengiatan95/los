﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLiquidation
    {
        public long Id { get; set; }
        public int? LoanId { get; set; }
        public string Content { get; set; }
        public string JsonImage { get; set; }
        public int? Status { get; set; }
        public string UsernameAction { get; set; }
        public long? UserAction { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? IdReceiver { get; set; }
        public string NameReceiver { get; set; }
        public string NameAction { get; set; }
        public int? ShopId { get; set; }
    }
}
