﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLivingTime
    {
        public int LivingTimeId { get; set; }
        public string Name { get; set; }
    }
}
