﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoan
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ShopId { get; set; }
        public int CustomerId { get; set; }
        public long TotalMoney { get; set; }
        public long TotalMoneyCurrent { get; set; }
        public string Collateral { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? LoanTime { get; set; }
        public DateTime? LastDateOfPay { get; set; }
        public float? Rate { get; set; }
        public byte? RateType { get; set; }
        public short? Frequency { get; set; }
        public byte? IsBefore { get; set; }
        public float? PaymentMoney { get; set; }
        public short? Status { get; set; }
        public DateTime? NextDate { get; set; }
        public int? ApproveBy { get; set; }
        public string Note { get; set; }
        public long? TotalInterest { get; set; }
        public int? DebitMoney { get; set; }
        public int? CodeId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? InterestToDay { get; set; }
        public DateTime? FinishDate { get; set; }
        public byte? ProductId { get; set; }
        public short? BriefId { get; set; }
        public byte? TypeCloseLoan { get; set; }
        public short? KeepOriginal { get; set; }
        public long? OverMoney { get; set; }
        public int? AgencyId { get; set; }
        public float? RateAgency { get; set; }
        public float? RateInterest { get; set; }
        public float? RateConsultant { get; set; }
        public float? RateService { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public DateTime? BadDebtTransferDate { get; set; }
        public long? TotalMoneyFine { get; set; }
        public long? TotalMoneyFineCurrent { get; set; }
        public long? FineDate { get; set; }
        public int? UserIdRemindDebt { get; set; }
        public byte? SynchronizationVbi { get; set; }
        public string LinkGcnchoVay { get; set; }
        public string LinkGcnvay { get; set; }
        public long? MoneyFeeInsuranceOfCustomer { get; set; }
        public string RegistrationCustomerId { get; set; }
        public string RegistrationContractId { get; set; }
        public int? PartnerIdProcessBadDebt { get; set; }
        public long? HubId { get; set; }
        public byte? CustomerInsuranceFeeState { get; set; }
        public DateTime? AlarmDate { get; set; }
        public string AlarmNote { get; set; }
        public double? Score { get; set; }
        public int? LastUserId { get; set; }
        public int? MarkLoan { get; set; }
        public long? DebitMoneyFineLate { get; set; }
        public int? StatusSendInsurance { get; set; }
        public short? ValidDocuments { get; set; }
        public int? SourceBankDisbursement { get; set; }
        public int? YearBadDebt { get; set; }
        public int? GetInformationInsuranVib { get; set; }
        public decimal? RateLender { get; set; }
        public decimal? RateAffLender { get; set; }
        public int? ChannelSale { get; set; }
        public int? InsuranceCompensatorId { get; set; }
    }
}
