﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanBadDebitReport
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public int? ShopId { get; set; }
        public int? CustomerId { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public long? DebitMoney { get; set; }
        public long? LoanExtra { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? LastDateOfPay { get; set; }
        public float? Rate { get; set; }
        public byte? RateType { get; set; }
        public byte? Status { get; set; }
        public byte? TypeId { get; set; }
        public string TypeNote { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
