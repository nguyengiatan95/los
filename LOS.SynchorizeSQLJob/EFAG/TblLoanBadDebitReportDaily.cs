﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanBadDebitReportDaily
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public int? Code { get; set; }
        public int? AgentCode { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public long? InterestToDay { get; set; }
        public long? DebitMoney { get; set; }
        public long? TotaMoney { get; set; }
        public long? TotalMoneyPaid { get; set; }
        public DateTime? LastDateOfPay { get; set; }
        public int? CountDate { get; set; }
        public int? ProductId { get; set; }
        public string ProductNote { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public int? ShopId { get; set; }
        public string ShopName { get; set; }
        public DateTime? CreationTime { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? CusId { get; set; }
        public string CusName { get; set; }
        public string CusPhone { get; set; }
        public string CusAddress { get; set; }
        public int? CodeId { get; set; }
        public int? UserIdRemindDebt { get; set; }
        public int? CountProcess { get; set; }
        public int? StatusDoing { get; set; }
    }
}
