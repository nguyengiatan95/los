﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanCreditHide
    {
        public long Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? ShopId { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
