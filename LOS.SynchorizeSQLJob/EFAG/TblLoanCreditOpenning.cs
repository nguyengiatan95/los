﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanCreditOpenning
    {
        public int LoanCreditOpenningId { get; set; }
        public int? LoanCreditId { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public int? GroupUserId { get; set; }
        public int? OpenningType { get; set; }
        public string OpenningNote { get; set; }
        public DateTime? OpenningDay { get; set; }
        public int? CounselorId { get; set; }
        public string CounselorName { get; set; }
        public int? StatusReally { get; set; }
        public int? UserId2 { get; set; }
        public string FullName2 { get; set; }
        public int? GroupUserId2 { get; set; }
        public int? AddressFieldSurveyStatus { get; set; }
        public int? CompanyFieldSurveyStatus { get; set; }
        public int? Code { get; set; }
    }
}
