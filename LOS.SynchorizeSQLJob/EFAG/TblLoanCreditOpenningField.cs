﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanCreditOpenningField
    {
        public int LoanCreditOpenningFieldId { get; set; }
        public int LoanCreditId { get; set; }
        public int UserId { get; set; }
        public string FullName { get; set; }
        public DateTime? OpenningDay { get; set; }
        public int ProductId { get; set; }
        public bool? IsThamDinh { get; set; }
        public int? Type { get; set; }
        public int? LoanTime { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string CustomerCreditName { get; set; }
        public long? TotalMoney { get; set; }
    }
}
