﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanCreditOtpTs
    {
        public long Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? Otp { get; set; }
        public DateTime? Createdate { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string TradingCode { get; set; }
    }
}
