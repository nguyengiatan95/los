﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanCreditOverView
    {
        public int Id { get; set; }
        public int LoanCreditId { get; set; }
        public int GroupId { get; set; }
        public int Steps { get; set; }
        public int Status { get; set; }
        public int? UserId { get; set; }
        public int? SupportId { get; set; }
        public int? CounselorId { get; set; }
        public int? CoordinatorId { get; set; }
        public int? FieldSurveyUserId { get; set; }
        public int? CoordinatorId2 { get; set; }
        public int? AwardContractId { get; set; }
        public int? AgencyId { get; set; }
        public int? CounselorId2 { get; set; }
        public int? AccountantId { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? ProductType { get; set; }
        public int? Count { get; set; }
        public int? IsCancel { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public DateTime? LoanCreditCreateDate { get; set; }
        public long? TotalMoneyFirst { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public int? LoanTime { get; set; }
        public int? ReasonId { get; set; }
        public string ContentCancel { get; set; }
        public int? OldStatus { get; set; }
        public int? NewStatus { get; set; }
    }
}
