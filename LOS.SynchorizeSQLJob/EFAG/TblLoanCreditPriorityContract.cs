﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanCreditPriorityContract
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public string CustomerName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public string ProductName { get; set; }
        public long? TotalMoney { get; set; }
        public bool? ReMarketing { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public int? ProductId { get; set; }
        public string JobName { get; set; }
        public int? Status { get; set; }
        public int? CustomerCreditId { get; set; }
    }
}
