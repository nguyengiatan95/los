﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanCreditProduct
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? ManufacturerId { get; set; }
        public string ModelPhone { get; set; }
        public string YearMade { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public decimal? ValuationAsset { get; set; }
    }
}
