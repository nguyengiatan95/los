﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanCreditProductAttribute
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? ProductAttributeId { get; set; }
        public int? ProductAttributeValueId { get; set; }
        public int? ProductCreditId { get; set; }
        public int? ProductAttributeType { get; set; }
        public string AttributeValue { get; set; }
    }
}
