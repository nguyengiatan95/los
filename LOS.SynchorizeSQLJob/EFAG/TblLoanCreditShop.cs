﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanCreditShop
    {
        public int ShopId { get; set; }
        public int LoanCreditId { get; set; }
        public short Status { get; set; }
        public string ShopName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? ContactLoanCredit { get; set; }
        public int? NumberSend { get; set; }
        public DateTime? NextSendSms { get; set; }
    }
}
