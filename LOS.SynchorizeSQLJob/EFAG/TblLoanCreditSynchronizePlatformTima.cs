﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanCreditSynchronizePlatformTima
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? PlatformLoanCreditId { get; set; }
        public int? PlatformCustomerCreditId { get; set; }
        public int? CustomerCreditId { get; set; }
        public DateTime? CreateOn { get; set; }
        public bool? Synchronized { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
