﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanCreditThirdParty
    {
        public int Id { get; set; }
        public int? LoanCreditid { get; set; }
    }
}
