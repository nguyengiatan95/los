﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanDebit
    {
        public int DebitId { get; set; }
        public int? ShopId { get; set; }
        public int? UserId { get; set; }
        public int? LoanId { get; set; }
        public DateTime? ForDate { get; set; }
        public long? PayMoney { get; set; }
        public byte? Status { get; set; }
        public string BankCode { get; set; }
        public int? ForeignKeyDebitId { get; set; }
        public int? BankId { get; set; }
    }
}
