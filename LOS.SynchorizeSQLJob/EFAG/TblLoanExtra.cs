﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanExtra
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ShopId { get; set; }
        public int LoanId { get; set; }
        public long TotalMoney { get; set; }
        public DateTime DateExtra { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Note { get; set; }
        public string CodeBank { get; set; }
        public int? LoanExtraId { get; set; }
        public int? PaymentId { get; set; }
        public int? BankId { get; set; }
        public bool? Fined { get; set; }
        public long? MoneyFined { get; set; }
    }
}
