﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanIndemnifyInsurrance
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public int? LoanCodeId { get; set; }
        public long? MoneyIndemnification { get; set; }
        public long? MoneyInterest { get; set; }
        public byte? Status { get; set; }
        public DateTime? CreateOn { get; set; }
        public int? InsuranceCompensatorId { get; set; }
    }
}
