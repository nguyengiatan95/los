﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLoanInterestExpected
    {
        public int LoanId { get; set; }
        public DateTime ForDate { get; set; }
        public int InterestMoney { get; set; }
        public int Id { get; set; }
    }
}
