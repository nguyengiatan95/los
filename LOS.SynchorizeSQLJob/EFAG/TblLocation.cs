﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLocation
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? GroupId { get; set; }
        public int? LoanCreditId { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Device { get; set; }
        public int? CommentCreditId { get; set; }
    }
}
