﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogAccountExtend
    {
        public int Id { get; set; }
        public DateTime? CreadateOn { get; set; }
        public int? CustomertId { get; set; }
        public string AccountCustomer { get; set; }
        public int? UserIdAction { get; set; }
        public string NameUser { get; set; }
        public int? UserBus { get; set; }
        public string NameBus { get; set; }
        public string Note { get; set; }
        public int? Money { get; set; }
        public bool? IsSettlement { get; set; }
        public int? TypeExtend { get; set; }
        public string CityName { get; set; }
        public int? CityId { get; set; }
    }
}
