﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogActionThn
    {
        public long Id { get; set; }
        public string Note { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public long? LoanId { get; set; }
        public int? NewStatus { get; set; }
        public int? OldStatus { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? FinishTime { get; set; }
    }
}
