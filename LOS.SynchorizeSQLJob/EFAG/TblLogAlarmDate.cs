﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogAlarmDate
    {
        public long Id { get; set; }
        public DateTime? AlarmDate { get; set; }
        public int? UserId { get; set; }
        public string Note { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? LoanId { get; set; }
    }
}
