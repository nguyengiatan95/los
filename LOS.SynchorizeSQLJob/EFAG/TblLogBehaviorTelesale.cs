﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogBehaviorTelesale
    {
        public long LogId { get; set; }
        public DateTime? ForDate { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public long? TotalForm { get; set; }
        public long? TotalFormUnder { get; set; }
        public long? TotalFormHigher { get; set; }
        public long? TotalNoProcess { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
