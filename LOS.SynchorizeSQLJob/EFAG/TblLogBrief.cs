﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogBrief
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? LoanId { get; set; }
        public short? BriefId { get; set; }
        public string Note { get; set; }
        public string UserName { get; set; }
    }
}
