﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogCallApi
    {
        public int Id { get; set; }
        public int? ActionCallApi { get; set; }
        public string NameActionCallApi { get; set; }
        public string InputStringCallApi { get; set; }
        public string LinkCallApi { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateOn { get; set; }
        public DateTime? ModifyOn { get; set; }
        public int? LoanId { get; set; }
        public string ResultStringCallApi { get; set; }
        public int? LoanCreditId { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
    }
}
