﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogCancelCustomerInSight
    {
        public long Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? LoanId { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? JobId { get; set; }
        public long? Salary { get; set; }
        public int? ShopId { get; set; }
        public int? SaleChannelId { get; set; }
        public int? ProductGroupId { get; set; }
        public int? ProductId { get; set; }
        public int? CustomerId { get; set; }
        public int? ForDate { get; set; }
    }
}
