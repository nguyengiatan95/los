﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogCancelMmo
    {
        public int Id { get; set; }
        public int? LoanCreditPlatFromId { get; set; }
        public DateTime? CreateOn { get; set; }
        public string Content { get; set; }
        public int? Resend { get; set; }
        public int? ReasonId { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public string FullNameCustomer { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string ProductName { get; set; }
        public long? TotalMoney { get; set; }
        public string TimeLoan { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public DateTime? CreateLoan { get; set; }
        public int? ProductPlatFromId { get; set; }
    }
}
