﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogChangeLoanBelongTo
    {
        public long Id { get; set; }
        public int? CreatorId { get; set; }
        public int? UserSourceId { get; set; }
        public int? LoanId { get; set; }
        public int? UserTargetId { get; set; }
        public byte? TransferType { get; set; }
        public string Note { get; set; }
        public DateTime? CreationTime { get; set; }
    }
}
