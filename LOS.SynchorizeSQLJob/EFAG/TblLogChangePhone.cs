﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogChangePhone
    {
        public int Id { get; set; }
        public int? CustomerCreditId { get; set; }
        public int? CustomerId { get; set; }
        public int? LoanCreditId { get; set; }
        public int? LoanId { get; set; }
        public string PhoneNew { get; set; }
        public string PhoneOld { get; set; }
        public DateTime? CreateOn { get; set; }
        public int? UserId { get; set; }
        public string UserFullName { get; set; }
    }
}
