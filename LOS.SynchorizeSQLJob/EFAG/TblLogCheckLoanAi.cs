﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogCheckLoanAi
    {
        public int Id { get; set; }
        public int? TypeService { get; set; }
        public int? LoanCreditId { get; set; }
        public string RefCode { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? Status { get; set; }
        public string Url { get; set; }
        public string Token { get; set; }
        public int? HomeNetwok { get; set; }
        public string ResultFinal { get; set; }
    }
}
