﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogCheckMoney
    {
        public long Id { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ShopId { get; set; }
        public string ShopName { get; set; }
        public long? MoneyBeginDate { get; set; }
        public long? BeginMoneyEndDate { get; set; }
        public long? TotalMoneyInshop { get; set; }
        public long? MoneyEndDateAfterCheck { get; set; }
        public long? BeginMoneyNextDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? Status { get; set; }
    }
}
