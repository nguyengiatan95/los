﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogClickToCall
    {
        public long Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CustomerCreditId { get; set; }
        public int? UserIdCall { get; set; }
        public string UserNameCall { get; set; }
        public string FullNameCall { get; set; }
        public DateTime? CreateDate { get; set; }
        public string PhoneOfCustomer { get; set; }
        public string NameOfCustomer { get; set; }
    }
}
