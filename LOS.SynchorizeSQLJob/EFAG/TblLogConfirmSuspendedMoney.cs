﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogConfirmSuspendedMoney
    {
        public int Id { get; set; }
        public int? TransactionBankCardId { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Content { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
