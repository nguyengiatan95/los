﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogConvertTelesale
    {
        public long LogId { get; set; }
        public DateTime? ForDate { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public string CityName { get; set; }
        public long? NumberRegistration { get; set; }
        public long? NumberPushToHub { get; set; }
        public long? NumberDisbursement { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
