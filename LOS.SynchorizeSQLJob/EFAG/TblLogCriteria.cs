﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogCriteria
    {
        public int LogCriteriaId { get; set; }
        public int? CoordinatorCriteriaId { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? Createdate { get; set; }
    }
}
