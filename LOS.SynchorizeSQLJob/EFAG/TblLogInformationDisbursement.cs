﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogInformationDisbursement
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CustomerCreditId { get; set; }
        public int? LoanId { get; set; }
        public string InformationLoanCredit { get; set; }
        public string InfomationCustomerCredit { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
