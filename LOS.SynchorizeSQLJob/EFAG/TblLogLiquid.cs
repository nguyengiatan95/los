﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogLiquid
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public long? LiquidMoney { get; set; }
        public long? DebtMoney { get; set; }
        public int? ShopId { get; set; }
        public int? AccountantConfirm { get; set; }
        public int? AgencyConfirm { get; set; }
        public string AccountantComment { get; set; }
        public string AgencyComment { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string AccountantUser { get; set; }
        public string AgencyUser { get; set; }
        public int? Status { get; set; }
        public int? CodeId { get; set; }
        public int? AgencyCodeId { get; set; }
        public string FullName { get; set; }
    }
}
