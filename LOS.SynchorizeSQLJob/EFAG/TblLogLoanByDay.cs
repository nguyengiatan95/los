﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogLoanByDay
    {
        public DateTime ForDate { get; set; }
        public int LoanId { get; set; }
        public int? ShopId { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public DateTime? NextDate { get; set; }
        public int? Status { get; set; }
        public DateTime? LastDateOfPay { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? LoanTime { get; set; }
        public float? Rate { get; set; }
        public byte? RateType { get; set; }
        public int? Frequency { get; set; }
        public long? PaymentMoney { get; set; }
        public long? DebitMoney { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? AgencyId { get; set; }
        public int? ProductId { get; set; }
        public int? CustomerId { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? UserIdRemindDebt { get; set; }
        public int? YearBadDebt { get; set; }
        public int? HubId { get; set; }
        public long? InterestToDay { get; set; }
        public float? RateInterest { get; set; }
        public float? RateConsultant { get; set; }
        public float? RateService { get; set; }
        public int? CodeId { get; set; }
        public long? TotalMoney { get; set; }
    }
}
