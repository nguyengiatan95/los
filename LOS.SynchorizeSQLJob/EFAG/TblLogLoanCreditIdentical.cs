﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogLoanCreditIdentical
    {
        public int Id { get; set; }
        public int? PlatformLoanCreditId { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
