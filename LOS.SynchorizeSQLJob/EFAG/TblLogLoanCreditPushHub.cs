﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogLoanCreditPushHub
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CustomerCreditId { get; set; }
        public string FullName { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? ProductId { get; set; }
        public int? ShopId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string ShopName { get; set; }
    }
}
