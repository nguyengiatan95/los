﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogLocationThn
    {
        public long Id { get; set; }
        public long? UserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string LatUser { get; set; }
        public string LongUser { get; set; }
        public short? CheckIn { get; set; }
    }
}
