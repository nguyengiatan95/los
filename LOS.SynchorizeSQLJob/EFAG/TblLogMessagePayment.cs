﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogMessagePayment
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string MessageError { get; set; }
        public string Input { get; set; }
        public int? Status { get; set; }
    }
}
