﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogMoneyFee
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public DateTime? DateToPayInterest { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? MoneyFeeLate { get; set; }
    }
}
