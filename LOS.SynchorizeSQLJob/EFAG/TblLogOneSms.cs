﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogOneSms
    {
        public long Id { get; set; }
        public DateTime? ForDate { get; set; }
        public long? LoanId { get; set; }
        public string Phone { get; set; }
        public string Smscontent { get; set; }
        public long? IdoneSms { get; set; }
        public short? TypeSent { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CountSms { get; set; }
    }
}
