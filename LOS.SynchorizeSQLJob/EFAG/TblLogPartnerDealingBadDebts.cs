﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogPartnerDealingBadDebts
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public int? UserCreateId { get; set; }
        public string UserNameCreate { get; set; }
        public string FullNameCreate { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? ReasonId { get; set; }
        public string Reason { get; set; }
        public int? UserAssignId { get; set; }
        public string UserNameAssign { get; set; }
        public string FullNameAssign { get; set; }
        public int? Status { get; set; }
    }
}
