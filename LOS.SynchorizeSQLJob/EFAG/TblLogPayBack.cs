﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogPayBack
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Note { get; set; }
        public string UserName { get; set; }
        public int? LoanId { get; set; }
    }
}
