﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogProcessStaffThn
    {
        public long Id { get; set; }
        public DateTime? ForDate { get; set; }
        public int? LoanId { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public int? CountProcess { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? FinishTime { get; set; }
        public int? StatusDoing { get; set; }
        public int? MinuteBreak { get; set; }
        public int? MinuteDoing { get; set; }
        public DateTime? BreakTime { get; set; }
        public DateTime? FinishBreakTime { get; set; }
    }
}
