﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogPushPredictive
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CampaignId { get; set; }
        public string Phone { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? Status { get; set; }
        public string Url { get; set; }
        public string Token { get; set; }
    }
}
