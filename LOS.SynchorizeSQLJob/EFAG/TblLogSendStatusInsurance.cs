﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogSendStatusInsurance
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public int? UserId { get; set; }
        public string Note { get; set; }
        public short? StatusNew { get; set; }
        public short? StatusOld { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
