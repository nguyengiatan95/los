﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogShop
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public short? Status { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Represent { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string PersonContact { get; set; }
        public string PersonContactPhone { get; set; }
        public DateTime? PersonBirthDay { get; set; }
        public string PersonCardNumber { get; set; }
        public int? PersonGender { get; set; }
        public string PersonAvatar { get; set; }
        public string OwerNameOfShop { get; set; }
        public string Email { get; set; }
        public string InviteCode { get; set; }
        public decimal? RateLender { get; set; }
        public decimal? RateAff { get; set; }
        public decimal? RateInterest { get; set; }
        public int? AffId { get; set; }
    }
}
