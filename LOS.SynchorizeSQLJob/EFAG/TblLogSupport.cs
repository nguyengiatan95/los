﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogSupport
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? ShopId { get; set; }
        public int? UserId { get; set; }
        public int? GroupUserId { get; set; }
        public string FullName { get; set; }
        public string Comment { get; set; }
        public int? StatusLoanCredit { get; set; }
        public int? ToUserId { get; set; }
        public int? ToGroupUserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? ActionUserId { get; set; }
        public int? ActionGroupUserId { get; set; }
    }
}
