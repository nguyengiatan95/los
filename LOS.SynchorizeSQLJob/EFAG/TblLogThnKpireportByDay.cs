﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogThnKpireportByDay
    {
        public long Id { get; set; }
        public DateTime Fordate { get; set; }
        public int UserId { get; set; }
        public int TypeId { get; set; }
        public short? TimeSearch { get; set; }
        public string CurrentUserName { get; set; }
        public long? TotalLoanCount { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public long? TotalMoneyReceived { get; set; }
        public long? TotalLoanReceived { get; set; }
        public long? TotalMoneyCollected { get; set; }
        public int? GroupId { get; set; }
    }
}
