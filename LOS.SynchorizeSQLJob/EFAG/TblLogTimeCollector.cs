﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogTimeCollector
    {
        public long Id { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? LoanId { get; set; }
        public int? Status { get; set; }
    }
}
