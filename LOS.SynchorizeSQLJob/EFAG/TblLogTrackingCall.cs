﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogTrackingCall
    {
        public int Id { get; set; }
        public string EventType { get; set; }
        public int? CampaignId { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int? CampaignDataId { get; set; }
        public string CallId { get; set; }
        public string Line { get; set; }
        public string PhoneNumber { get; set; }
        public int? CallAttemptIndex { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? ConnectTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int? RingCustomerDuration { get; set; }
        public int? CallDuration { get; set; }
        public int? StatusCode { get; set; }
        public string CustomerInputDtmf { get; set; }
        public int? IsExecute { get; set; }
        public DateTime? AgentRingTime { get; set; }
        public DateTime? AgentConnectTime { get; set; }
        public int? AgentUserId { get; set; }
        public int? RingAgentDuration { get; set; }
        public int? AgentIpphone { get; set; }
    }
}
