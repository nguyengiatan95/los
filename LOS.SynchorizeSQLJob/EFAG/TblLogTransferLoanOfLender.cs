﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogTransferLoanOfLender
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public int? AgencyIdOld { get; set; }
        public int? AgencyIdNew { get; set; }
        public DateTime? CreateOn { get; set; }
        public long? TotalMoneyCurrent { get; set; }
    }
}
