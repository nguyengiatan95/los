﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogUploadFile
    {
        public long LogUploadFileId { get; set; }
        public DateTime? ForDate { get; set; }
        public long? LoanId { get; set; }
        public long? StaffId { get; set; }
        public string StaffName { get; set; }
        public long? SectionManagerUserId { get; set; }
        public string SectionManagerUsername { get; set; }
        public string DepartmentManagerUsername { get; set; }
        public long? DepartmentManagerUserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? UserId { get; set; }
        public string Username { get; set; }
    }
}
