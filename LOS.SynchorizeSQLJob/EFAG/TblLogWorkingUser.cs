﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblLogWorkingUser
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
