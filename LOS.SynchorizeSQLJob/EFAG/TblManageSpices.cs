﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblManageSpices
    {
        public int ShopId { get; set; }
        public int ProductId { get; set; }
        public int DistrictId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int WardId { get; set; }
        public int? CityId { get; set; }
    }
}
