﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblManufacturer
    {
        public int ManufacturerId { get; set; }
        public string Name { get; set; }
        public byte? TypeId { get; set; }
        public int? BrandId { get; set; }
        public bool? IsEnable { get; set; }
    }
}
