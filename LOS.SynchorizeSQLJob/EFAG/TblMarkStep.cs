﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblMarkStep
    {
        public int StepId { get; set; }
        public string StepName { get; set; }
        public int? Status { get; set; }
    }
}
