﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblMessage
    {
        public int MsgId { get; set; }
        public string MsgTitle { get; set; }
        public string MsgContent { get; set; }
        public string MsgFilePath { get; set; }
        public int? GroupId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? MsgStatus { get; set; }
    }
}
