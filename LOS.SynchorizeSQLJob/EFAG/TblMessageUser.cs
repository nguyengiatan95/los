﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblMessageUser
    {
        public int MsgId { get; set; }
        public int UserId { get; set; }
        public int? MsgUserStatus { get; set; }
        public DateTime? ReadDate { get; set; }
    }
}
