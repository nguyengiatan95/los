﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblModule
    {
        public int Id { get; set; }
        public string LinkText { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? Priority { get; set; }
    }
}
