﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblOddDebtDaily
    {
        public long Id { get; set; }
        public int? ShopId { get; set; }
        public string ShopName { get; set; }
        public int? IsHub { get; set; }
        public string ProductName { get; set; }
        public int? ProductId { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public int? NumberLoan { get; set; }
        public long? TotalMoneyCurrentOfBadDebt { get; set; }
        public int? NumberLoanBadDebt { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ForDate { get; set; }
        public int? NumberLoanDisbursementOfMonth { get; set; }
        public long? TotalMoneyDisbursementOfMonth { get; set; }
    }
}
