﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblOrderSource
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string OderSourceName { get; set; }
        public byte Status { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string Condition { get; set; }
        public byte? ApplyFor { get; set; }
        public byte? IsScaned { get; set; }
    }
}
