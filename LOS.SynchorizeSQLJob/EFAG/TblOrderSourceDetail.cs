﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblOrderSourceDetail
    {
        public int Id { get; set; }
        public int OrderSourceId { get; set; }
        public string Name { get; set; }
        public byte Status { get; set; }
        public byte? KeyType { get; set; }
        public byte? CompareType { get; set; }
        public byte? CompareTo { get; set; }
    }
}
