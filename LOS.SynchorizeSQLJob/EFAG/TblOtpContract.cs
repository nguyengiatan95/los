﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblOtpContract
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public short? Otp { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ExpiredTime { get; set; }
        public int? Status { get; set; }
        public int? UserId { get; set; }
        public string UserFullName { get; set; }
        public string Phone { get; set; }
    }
}
