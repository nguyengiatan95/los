﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblOtpTs
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public string RequestId { get; set; }
        public string Phone { get; set; }
        public string CardNumber { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
