﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPawnNew
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? ShopId { get; set; }
        public int? CustomerId { get; set; }
        public long? TotalMoney { get; set; }
        public long? TotalMoneyCurrent { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? LastDateOfPay { get; set; }
        public DateTime? NextDate { get; set; }
        public float? Rate { get; set; }
        public byte? RateType { get; set; }
        public int? Frequency { get; set; }
        public long? PaymentMoney { get; set; }
        public short? Status { get; set; }
        public string Note { get; set; }
        public int? CodeId { get; set; }
        public int? CategoryId { get; set; }
        public string ItemName { get; set; }
        public string Extend1 { get; set; }
        public string Extend2 { get; set; }
        public string Extend3 { get; set; }
        public string Extend4 { get; set; }
        public string Extend5 { get; set; }
        public byte? IsBefore { get; set; }
        public byte? MaxDayOverdue { get; set; }
        public long? DebitMoney { get; set; }
        public DateTime? ExpireDate { get; set; }
        public long? TotalInterest { get; set; }
        public DateTime? LiquidationDate { get; set; }
        public long? LiquidationMoney { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? InterestToDay { get; set; }
        public DateTime? FinishDate { get; set; }
    }
}
