﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPawnNewFiles
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public int? UserId { get; set; }
        public string FilePath { get; set; }
        public byte? Status { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
