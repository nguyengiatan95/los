﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPayment
    {
        public int Id { get; set; }
        public int? LoanId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? PayMoney { get; set; }
        public int? UserId { get; set; }
        public byte? Status { get; set; }
        public DateTime? PayDate { get; set; }
        public long? InterestMoney { get; set; }
        public long? OtherMoney { get; set; }
        public long? PayNeed { get; set; }
        public string BankCode { get; set; }
        public int? PaymentId { get; set; }
        public long? OriginalMoney { get; set; }
        public long? OverMoney { get; set; }
        public int? BankId { get; set; }
        public DateTime? DaysPayable { get; set; }
    }
}
