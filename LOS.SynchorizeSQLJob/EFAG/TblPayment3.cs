﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPayment3
    {
        public int Id { get; set; }
        public int? CapitalId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? PayMoney { get; set; }
        public int? UserId { get; set; }
        public byte? Status { get; set; }
        public DateTime? PayDate { get; set; }
        public long? InterestMoney { get; set; }
        public long? OtherMoney { get; set; }
    }
}
