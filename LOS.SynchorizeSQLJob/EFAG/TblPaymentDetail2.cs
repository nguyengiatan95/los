﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPaymentDetail2
    {
        public int ShopId { get; set; }
        public int? InstallmentId { get; set; }
        public int PaymentId { get; set; }
        public DateTime ForDate { get; set; }
        public int? MoneyPay { get; set; }
    }
}
