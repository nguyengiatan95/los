﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPaymentNotify
    {
        public int Id { get; set; }
        public byte TypeId { get; set; }
        public int ShopId { get; set; }
        public long TotalMoney { get; set; }
        public long InterestMoney { get; set; }
        public int? DebitMoney { get; set; }
        public int CusId { get; set; }
        public string CusName { get; set; }
        public string CusPhone { get; set; }
        public string CusAddress { get; set; }
        public string Note { get; set; }
        public int? Status { get; set; }
        public int? ManageId { get; set; }
        public int? Number { get; set; }
        public long? PayMoney { get; set; }
        public int AutoId { get; set; }
        public int? CodeId { get; set; }
        public string CodeItem { get; set; }
        public string ItemName { get; set; }
        public int? FromDate { get; set; }
        public int? ToDate { get; set; }
        public int? CountDate { get; set; }
        public string NumberCard { get; set; }
        public int? AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string PhoneRelationShip { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? UserIdRemindDebt { get; set; }
        public byte? RateType { get; set; }
        public long? ShopIdConsulting { get; set; }
        public int? CountProcess { get; set; }
        public string RegistrationContractId { get; set; }
        public string RegistrationCustomerId { get; set; }
        public int? StatusDoing { get; set; }
        public long? LastLogId { get; set; }
        public long? OtherMoney { get; set; }
        public bool? IsHub { get; set; }
        public short? ChooseToday { get; set; }
        public DateTime? NextCall { get; set; }
        public short? IsSmartDialer { get; set; }
    }
}
