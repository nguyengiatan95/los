﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPaymentNotifyByDay
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public DateTime ForDate { get; set; }
        public int? ManageId { get; set; }
        public int ContractId { get; set; }
        public byte TypeId { get; set; }
        public int? CusId { get; set; }
        public string CusName { get; set; }
        public int? DebitMoney { get; set; }
        public int? Number { get; set; }
        public long? PayMoney { get; set; }
        public long? TotalMoney { get; set; }
        public byte? Status { get; set; }
        public long? MoneyPaid { get; set; }
    }
}
