﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPaymentScheduler
    {
        public long Id { get; set; }
        public int? LoanId { get; set; }
        public int? ShopId { get; set; }
        public int? LoanAgencyId { get; set; }
        public int? ShopAgencyId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? DateToPay { get; set; }
        public long? MoneyOriginal { get; set; }
        public long? MoneyInterest { get; set; }
        public long? MoneyOtherOfTima { get; set; }
        public long? MoneyOriginalCollected { get; set; }
        public long? MoneyInterestCollected { get; set; }
        public long? MoneyOtherOfTimaCollected { get; set; }
        public long? MoneyFinePrepaid { get; set; }
        public long? MoneyOtherDeferred { get; set; }
        public bool? Done { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
