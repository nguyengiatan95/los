﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPermission
    {
        public int Id { get; set; }
        public string DisplayText { get; set; }
        public int? ParentId { get; set; }
        public short? Position { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public byte? Status { get; set; }
        public string Description { get; set; }
        public byte? IsMenu { get; set; }
        public short? IsThnWeb { get; set; }
    }
}
