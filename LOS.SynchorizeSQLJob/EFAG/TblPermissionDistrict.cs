﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPermissionDistrict
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? DistrictId { get; set; }
        public int? AreaId { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
