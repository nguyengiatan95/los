﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPermissionDistrictBebtRecovery
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? Level { get; set; }
        public int? ProductId { get; set; }
        public int? FromDay { get; set; }
        public int? ToDay { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? ProductTypeId { get; set; }
    }
}
