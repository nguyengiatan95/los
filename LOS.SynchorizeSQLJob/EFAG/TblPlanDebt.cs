﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPlanDebt
    {
        public int Id { get; set; }
        public int? CityId { get; set; }
        public int? SaleChanelId { get; set; }
        public DateTime? ForDate { get; set; }
        public int? ForDateId { get; set; }
        public long? MoneyDisbursement { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
