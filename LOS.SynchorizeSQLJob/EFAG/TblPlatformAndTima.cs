﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPlatformAndTima
    {
        public int PlatformLoanCreditId { get; set; }
        public int LoanCreditId { get; set; }
        public int LenderId { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? PlatformCustomerCreditId { get; set; }
        public int? ShopId { get; set; }
        public int? CustomerCreditId { get; set; }
        public int? ProductId { get; set; }
        public int? DistrictId { get; set; }
        public int? CityId { get; set; }
    }
}
