﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPointVnpt
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string RequestId { get; set; }
        public string Account { get; set; }
        public int? ErrorCode { get; set; }
        public string ResponseId { get; set; }
        public string ErrorDesc { get; set; }
        public int? Score { get; set; }
        public string Description { get; set; }
        public string Profile { get; set; }
        public DateTime? Time { get; set; }
    }
}
