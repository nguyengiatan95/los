﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProcessData
    {
        public int Id { get; set; }
        public byte TypeId { get; set; }
        public byte Retry { get; set; }
        public byte ActionId { get; set; }
        public int ProcessDataId { get; set; }
    }
}
