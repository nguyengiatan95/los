﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProcessPayment
    {
        public int Id { get; set; }
        public int PaymentId { get; set; }
        public byte Type { get; set; }
        public byte Action { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public byte? Retry { get; set; }
        public int ProcessPaymentId { get; set; }
    }
}
