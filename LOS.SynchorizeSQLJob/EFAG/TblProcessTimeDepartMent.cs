﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProcessTimeDepartMent
    {
        public long Id { get; set; }
        public int? ProductId { get; set; }
        public int? TypeProduct { get; set; }
        public int? CityId { get; set; }
        public int? ThoiGianXuLy { get; set; }
        public int? Step { get; set; }
        public string GroupName { get; set; }
    }
}
