﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProduct
    {
        public int Id { get; set; }
        public int IdBrand { get; set; }
        public int IdType { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public long? Price { get; set; }
        public int? Year { get; set; }
        public int? BrakeType { get; set; }
        public int? RimType { get; set; }
        public string ShortName { get; set; }
    }
}
