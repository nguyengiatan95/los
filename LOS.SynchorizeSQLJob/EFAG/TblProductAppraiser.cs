﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProductAppraiser
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? ProductId { get; set; }
        public int? Type { get; set; }
        public int? UserId { get; set; }
        public int? GroupId { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? IsWord { get; set; }
        public string FullNameAssigner { get; set; }
        public string UserNameAssigner { get; set; }
        public int? UserIdAssigner { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public int? FromUserId { get; set; }
    }
}
