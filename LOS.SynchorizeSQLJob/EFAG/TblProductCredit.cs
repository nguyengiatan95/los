﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProductCredit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public short? Status { get; set; }
        public decimal? Rate { get; set; }
        public decimal? RateConsultant { get; set; }
        public decimal? RateService { get; set; }
        public decimal? RateInterest { get; set; }
        public int? PlatformProductId { get; set; }
        public byte? TypeProduct { get; set; }
        public long? LimitMoney { get; set; }
        public int? Sort { get; set; }
    }
}
