﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProductEvaluation
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public int? IsThamDinhNha { get; set; }
        public int? IsThamDinhCongTy { get; set; }
    }
}
