﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProductPercentReduction
    {
        public int Id { get; set; }
        public int IdBrand { get; set; }
        public int IdType { get; set; }
        public int? CountYear { get; set; }
        public int? PercentReduction { get; set; }
    }
}
