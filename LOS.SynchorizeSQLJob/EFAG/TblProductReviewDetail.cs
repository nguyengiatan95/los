﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProductReviewDetail
    {
        public int Id { get; set; }
        public int? IdType { get; set; }
        public int? IdProductReview { get; set; }
        public long? MoneyDiscount { get; set; }
        public decimal? PecentDiscount { get; set; }
    }
}
