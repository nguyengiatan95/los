﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProductReviewResult
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? ProductId { get; set; }
        public int? ProductReviewId { get; set; }
        public bool? IsCheck { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
