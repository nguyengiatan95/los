﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProductReviewResultDetail
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? ProductId { get; set; }
        public int? UserId { get; set; }
        public int? GroupUserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? PriceReview { get; set; }
    }
}
