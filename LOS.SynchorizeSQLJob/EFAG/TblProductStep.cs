﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProductStep
    {
        public int Id { get; set; }
        public int? StepId { get; set; }
        public int? ProductId { get; set; }
        public int? OrderBy { get; set; }
    }
}
