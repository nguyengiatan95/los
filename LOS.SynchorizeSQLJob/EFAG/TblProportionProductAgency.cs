﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblProportionProductAgency
    {
        public int Id { get; set; }
        public int? ShopId { get; set; }
        public string ShopName { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal? RateConsultant { get; set; }
        public decimal? RateService { get; set; }
        public decimal? RateInterest { get; set; }
    }
}
