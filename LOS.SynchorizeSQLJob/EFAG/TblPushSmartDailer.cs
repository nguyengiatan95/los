﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblPushSmartDailer
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string Source { get; set; }
        public int? CampaignId { get; set; }
        public int? CampaignDataId { get; set; }
        public string ResponseApi { get; set; }
        public bool? IsPushCaresoft { get; set; }
        public DateTime? PushDate { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
