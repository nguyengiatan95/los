﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblReason
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public byte? Type { get; set; }
        public byte? Sort { get; set; }
        public bool? IsEnable { get; set; }
        public byte? ReSend { get; set; }
        public int? TypeRemarketing { get; set; }
        public int? IsApplyBlackList { get; set; }
        public int? NumberDayBlackList { get; set; }
        public string ReasonCode { get; set; }
        public string Note { get; set; }
        public int? ReasonGroupId { get; set; }
        public int? GroupUserId { get; set; }
        public byte? IsCallBack { get; set; }
        public bool? IsQlf { get; set; }
        public string ReasonEng { get; set; }
    }
}
