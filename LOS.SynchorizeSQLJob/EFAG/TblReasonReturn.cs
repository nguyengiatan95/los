﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblReasonReturn
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? UserId { get; set; }
        public int? GroupUserId { get; set; }
        public int? ReasonId { get; set; }
    }
}
