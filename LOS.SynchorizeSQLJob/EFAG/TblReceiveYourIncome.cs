﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblReceiveYourIncome
    {
        public int ReceiveYourIncomeId { get; set; }
        public string Name { get; set; }
    }
}
