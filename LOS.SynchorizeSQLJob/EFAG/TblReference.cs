﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblReference
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string DescriptionCustomer { get; set; }
        public int? TypeId { get; set; }
        public string DescriptionTypeId { get; set; }
        public byte? IsEnable { get; set; }
        public int? LoanCreditTypeId { get; set; }
        public int? Step { get; set; }
    }
}
