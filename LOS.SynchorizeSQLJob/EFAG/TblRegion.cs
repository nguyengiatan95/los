﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblRegion
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
