﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblReimbursement
    {
        public int Id { get; set; }
        public int? IdagencyGcash { get; set; }
        public string UserNameAgencyGcash { get; set; }
        public string FullNameAgencyGcash { get; set; }
        public string NoteReimbursementAgencyGcash { get; set; }
        public DateTime? ReimbursementDate { get; set; }
        public long? RefundAmount { get; set; }
        public int? Idconfirm { get; set; }
        public string UserNameConfirm { get; set; }
        public string FullNameConfirm { get; set; }
        public string NoteConfirm { get; set; }
        public DateTime? ConfirmDate { get; set; }
        public int? Status { get; set; }
    }
}
