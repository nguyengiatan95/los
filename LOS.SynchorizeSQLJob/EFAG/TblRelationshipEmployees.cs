﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblRelationshipEmployees
    {
        public int Id { get; set; }
        public int? SupRegion { get; set; }
        public int SupAreaId { get; set; }
        public int CounselorsId { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
