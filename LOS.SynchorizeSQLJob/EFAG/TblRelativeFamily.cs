﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblRelativeFamily
    {
        public int RelativeFamilyId { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
    }
}
