﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblReportCounselors
    {
        public int ReportId { get; set; }
        public int UserId { get; set; }
        public string FullName { get; set; }
        public DateTime WorkingTime { get; set; }
        public int TitleWorkId { get; set; }
        public string TitleWorkString { get; set; }
        public string DescriptionWork { get; set; }
        public string ActionJobString { get; set; }
        public int ActionJobId { get; set; }
        public int? LoanCreditId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public int? Result { get; set; }
        public string ReasonFail { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? Status { get; set; }
        public int? GroupId { get; set; }
    }
}
