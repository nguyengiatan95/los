﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblReportGoogleAds
    {
        public int Id { get; set; }
        public string EmbedCode { get; set; }
        public string TabName { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? IdLinkReport { get; set; }
    }
}
