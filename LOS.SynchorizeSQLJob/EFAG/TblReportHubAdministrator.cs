﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblReportHubAdministrator
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public long? GrossProfit { get; set; }
        public int? NewContract { get; set; }
        public long? MoneyToLend { get; set; }
        public long? TotalMoneyDebt { get; set; }
        public long? MoneyBadDebtIncurred { get; set; }
        public long? TotalMoneyBadDebt { get; set; }
        public int? Inventory { get; set; }
        public long ShopId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
