﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblReportPtsp
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public string FullName { get; set; }
        public string CardNumber { get; set; }
        public string Birthday { get; set; }
        public string CityName { get; set; }
        public string LivingName { get; set; }
        public string Salary { get; set; }
        public string RyincomeName { get; set; }
        public string HistoryPayMent { get; set; }
        public DateTime? FromDate { get; set; }
        public int? IsBlackList { get; set; }
        public string TypeOwnerShipName { get; set; }
    }
}
