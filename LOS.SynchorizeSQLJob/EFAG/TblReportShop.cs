﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblReportShop
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public int TypeId { get; set; }
        public int? TotalIndentureClose { get; set; }
        public int? TotalIndentureOpen { get; set; }
        public long? TotalMoneyInvestment { get; set; }
        public long? TotalInterestExpected { get; set; }
        public long? TotalInterestEarned { get; set; }
        public long? TotalMoneyDebtor { get; set; }
    }
}
