﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblRequestCac
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CustomerCreditId { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UserId { get; set; }
        public int? TypeRequest { get; set; }
        public int? HomeNetwork { get; set; }
    }
}
