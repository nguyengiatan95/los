﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblScheduledCollection
    {
        public long Id { get; set; }
        public int? LoanId { get; set; }
        public DateTime? ForDate { get; set; }
        public DateTime? PayDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? ScheduledPrincipal { get; set; }
        public long? MoneyInterest { get; set; }
        public long? MoneyServices { get; set; }
        public long? MoneyConsultant { get; set; }
        public int? LoanCreditId { get; set; }
        public int? LoanCodeId { get; set; }
        public int? LoanCreditCode { get; set; }
        public string CustomerName { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
    }
}
