﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblScore
    {
        public long Id { get; set; }
        public int? LoanCreditId { get; set; }
        public string Message { get; set; }
        public decimal? Score { get; set; }
        public bool? Decision { get; set; }
        public string Status { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
