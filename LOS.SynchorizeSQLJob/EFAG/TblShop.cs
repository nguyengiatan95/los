﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblShop
    {
        public int ShopId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public short? Status { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? TotalMoney { get; set; }
        public long? TotalMoneyInSafe { get; set; }
        public long? TotalMoneyCapitalCurrent { get; set; }
        public long? TotalInterestEarned { get; set; }
        public long? TotalInterestExpected { get; set; }
        public long? TotalInterestAvailable { get; set; }
        public long? TotalConvertInterestIntoCapital { get; set; }
        public long? TotalInterestWithdraw { get; set; }
        public long? TotalMoneyDebtor { get; set; }
        public long? TotalMoneySpent { get; set; }
        public long? TotalAbnormalCollectionMoney { get; set; }
        public byte? Changed { get; set; }
        public string Represent { get; set; }
        public int? IsAgent { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string TypePayment { get; set; }
        public string ShopNo { get; set; }
        public string Spices { get; set; }
        public string PersonContact { get; set; }
        public string PersonContactPhone { get; set; }
        public string Note { get; set; }
        public bool? IsHub { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public int? Score { get; set; }
        public byte? SelfEmployed { get; set; }
        public int? QualityReceived { get; set; }
        public int? Received { get; set; }
        public bool? IsReceipt { get; set; }
        public int? GroupShop { get; set; }
        public DateTime? PersonBirthDay { get; set; }
        public string PersonCardNumber { get; set; }
        public int? PersonGender { get; set; }
        public string PersonAvatar { get; set; }
        public string OwerNameOfShop { get; set; }
        public string TaxCode { get; set; }
        public bool? RegFromApp { get; set; }
        public string Email { get; set; }
        public bool? IsGcash { get; set; }
        public string InviteCode { get; set; }
        public string BankBalance { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public decimal RateLender { get; set; }
        public decimal RateAff { get; set; }
        public decimal RateInterest { get; set; }
        public byte? IsGetOriginalPenalty { get; set; }
    }
}
