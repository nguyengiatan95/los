﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblSimVip
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public string FullNameOwnerSim { get; set; }
        public string PhoneOwnerSim { get; set; }
        public bool? OwnerShipSim { get; set; }
        public short? TelCo { get; set; }
        public short? SubscriptionForm { get; set; }
        public DateTime? RegisterDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string DescriptionSim { get; set; }
    }
}
