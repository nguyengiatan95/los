﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblSms
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public int CustomerId { get; set; }
        public int? TypeId { get; set; }
        public string NumberPhone { get; set; }
        public byte HomeNetworkId { get; set; }
        public string Content { get; set; }
        public DateTime? CreateOn { get; set; }
        public byte Status { get; set; }
        public byte NumberError { get; set; }
        public DateTime? NextSend { get; set; }
        public string CustomerName { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
    }
}
