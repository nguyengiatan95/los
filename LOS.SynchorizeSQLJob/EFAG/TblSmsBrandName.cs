﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblSmsBrandName
    {
        public int Id { get; set; }
        public int? SupplierId { get; set; }
        public string SupplierBrandName { get; set; }
        public string Phone { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CreateByUserId { get; set; }
        public string CreateByFullName { get; set; }
        public int? PlatformType { get; set; }
        public string Content { get; set; }
        public int? TypeSend { get; set; }
        public bool? ResultSend { get; set; }
        public string SessionId { get; set; }
        public int? LoanId { get; set; }
        public int? CountSms { get; set; }
    }
}
