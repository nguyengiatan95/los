﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblSmsCredit
    {
        public int Id { get; set; }
        public long Phone { get; set; }
        public int Code { get; set; }
        public DateTime CreateDate { get; set; }
        public byte Status { get; set; }
        public int? CodeResult { get; set; }
        public string Smsid { get; set; }
    }
}
