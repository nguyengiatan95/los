﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblSmscompleted
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public int CustomerId { get; set; }
        public byte TypeId { get; set; }
        public string NumberPhone { get; set; }
        public byte HomeNetworkId { get; set; }
        public string Content { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime CompletedDate { get; set; }
        public string CustomerName { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
    }
}
