﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblStatementOfAccounts
    {
        public int Id { get; set; }
        public int? TransactionBankCardId { get; set; }
        public int? OwnedShopId { get; set; }
        public string OwnedShopName { get; set; }
        public int? PayShopId { get; set; }
        public string PayShopName { get; set; }
        public int? TypeStatementOfAccount { get; set; }
        public string NameStatementOfAccount { get; set; }
        public long? MoneyAdd { get; set; }
        public long? MoneySub { get; set; }
        public short? Status { get; set; }
        public string Note { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UserIdCreate { get; set; }
        public string UserNameCreate { get; set; }
        public string FullNameCreate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? UserIdModify { get; set; }
        public string UserNameModify { get; set; }
        public string FullNameModify { get; set; }
        public int? CompleteByTransactionBankCardId { get; set; }
        public int? BankLogId { get; set; }
        public string CustomerCreditName { get; set; }
        public int? LoanCreditId { get; set; }
        public int? LoanCodeId { get; set; }
    }
}
