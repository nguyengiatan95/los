﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTarget
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string Name { get; set; }
        public byte? Month { get; set; }
        public long? TotalMoneyTarget { get; set; }
        public int? TotalLoanTarget { get; set; }
    }
}
