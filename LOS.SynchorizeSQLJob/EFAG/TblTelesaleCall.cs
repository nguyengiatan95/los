﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTelesaleCall
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CustomerId { get; set; }
        public int? TypeId { get; set; }
        public int? ReceiveYourIncome { get; set; }
        public int? TermOfLaborContract { get; set; }
        public string DescriptionPositionJob { get; set; }
        public string CompanyName { get; set; }
        public string FoundedTimeCompany { get; set; }
        public int? WorkingTimeCompany { get; set; }
        public bool? HealthInsurance { get; set; }
        public string LifeTimeAddress { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CardnumberNew { get; set; }
        public string ContactName { get; set; }
        public string BankName { get; set; }
        public decimal? MoneyLoanbank { get; set; }
        public decimal? LevelMoney { get; set; }
        public int? TypeRelationship { get; set; }
        public int? SameFullnameShk { get; set; }
        public int? TypesOfContract { get; set; }
        public int? TimeContractCompany { get; set; }
        public short? MotobikeCertificate { get; set; }
        public short? HaveMotorbike { get; set; }
        public short? HomeownerId { get; set; }
        public string StrTimeWorking { get; set; }
        public short? FurtherExploitationInformation { get; set; }
        public short? ProofOfIncome { get; set; }
    }
}
