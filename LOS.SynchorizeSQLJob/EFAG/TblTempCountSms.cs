﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTempCountSms
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public int? CountSms { get; set; }
    }
}
