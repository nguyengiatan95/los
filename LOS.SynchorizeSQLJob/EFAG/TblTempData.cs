﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTempData
    {
        public int Id { get; set; }
        public int Code { get; set; }
    }
}
