﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTempInterestToday
    {
        public long LoanId { get; set; }
        public int TypeId { get; set; }
        public long? InterestToday { get; set; }
    }
}
