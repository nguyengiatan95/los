﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTempThnPushData
    {
        public long LoanId { get; set; }
        public int? StaffId { get; set; }
        public int? UserId { get; set; }
        public int? IsPushOwner { get; set; }
        public int? TeamleadUserId { get; set; }
        public string TeamleadUserName { get; set; }
        public int? SectionManagerUserId { get; set; }
        public string SectionManagerUsername { get; set; }
        public int? DepartmentManagerUserId { get; set; }
        public string DepartmentManagerUsername { get; set; }
    }
}
