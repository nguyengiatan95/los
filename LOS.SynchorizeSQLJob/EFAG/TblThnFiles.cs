﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblThnFiles
    {
        public long Id { get; set; }
        public string FilePath { get; set; }
        public int? UserId { get; set; }
        public long? CommentId { get; set; }
        public DateTime? CreateDate { get; set; }
        public short? Status { get; set; }
        public long? LoanId { get; set; }
    }
}
