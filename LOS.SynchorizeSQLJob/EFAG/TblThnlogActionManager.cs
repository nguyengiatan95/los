﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblThnlogActionManager
    {
        public long Id { get; set; }
        public int? LoanId { get; set; }
        public int? StaffId { get; set; }
        public string Note { get; set; }
        public int? CreateUserId { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
