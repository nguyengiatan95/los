﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblThnlogPushData
    {
        public long Id { get; set; }
        public long LoanId { get; set; }
        public string CustomerName { get; set; }
        public DateTime? ForDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CountDay { get; set; }
        public long? ProcessUserId { get; set; }
        public string ProcessUsername { get; set; }
        public int? IsResolve { get; set; }
        public short? Status { get; set; }
        public long? MoneyNeedCollect { get; set; }
        public long? TotalMoneyReceived { get; set; }
        public long LoanAndMonthId { get; set; }
        public long? UserAction { get; set; }
        public string UserActionName { get; set; }
        public DateTime? FirstUpdate { get; set; }
        public DateTime? LastUpdate { get; set; }
        public DateTime? NextDate { get; set; }
        public short? IsGoodContract { get; set; }
    }
}
