﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTimaAt
    {
        public int Vid { get; set; }
        public int? CanonicalVid { get; set; }
        public long? PortalId { get; set; }
        public bool? IsContact { get; set; }
        public int? LoanCreditId { get; set; }
        public string AffSid { get; set; }
    }
}
