﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTimaVbi
    {
        public int Id { get; set; }
        public double? SoIdVbi { get; set; }
        public double? SoIdDtac { get; set; }
        public string SoHd { get; set; }
        public double? TongPhi { get; set; }
        public double? SoIdVay { get; set; }
        public double? SoIdChoVay { get; set; }
        public string MaDviQl { get; set; }
        public int? LoanId { get; set; }
        public int? LoanCreditId { get; set; }
        public int? StatusGetGcnvay { get; set; }
        public int? StatusGetGcnChoVay { get; set; }
        public DateTime? CreateOn { get; set; }
        public long? MoneyFeeInsuranceOfCustomer { get; set; }
        public long? MoneyFeeInsuranceOfLender { get; set; }
    }
}
