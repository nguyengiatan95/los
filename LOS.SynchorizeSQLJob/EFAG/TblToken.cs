﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime? DateExpiration { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string AppId { get; set; }
    }
}
