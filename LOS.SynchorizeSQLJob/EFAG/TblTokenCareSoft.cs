﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTokenCareSoft
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime? DateExpiration { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UserId { get; set; }
    }
}
