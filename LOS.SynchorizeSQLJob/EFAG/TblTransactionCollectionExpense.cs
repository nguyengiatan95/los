﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTransactionCollectionExpense
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public byte? ActionId { get; set; }
        public int UserId { get; set; }
        public string FullName { get; set; }
        public long MoneyAdd { get; set; }
        public long MoneySub { get; set; }
        public string Note { get; set; }
        public DateTime CreateDate { get; set; }
        public byte? IsCancel { get; set; }
        public string CusName { get; set; }
        public byte? TypePatternId { get; set; }
        public string TypePatternName { get; set; }
        public int? BankId { get; set; }
        public string BankCode { get; set; }
    }
}
