﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTransactionInstallment
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public int InstallmentId { get; set; }
        public byte? ActionId { get; set; }
        public int UserId { get; set; }
        public string FullName { get; set; }
        public int? CusId { get; set; }
        public string CusName { get; set; }
        public long MoneyAdd { get; set; }
        public long MoneySub { get; set; }
        public string Note { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
