﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTransactionMoneyInSafe
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public int UserId { get; set; }
        public string FullName { get; set; }
        public long Money { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
