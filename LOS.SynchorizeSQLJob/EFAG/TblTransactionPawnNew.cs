﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTransactionPawnNew
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public int LoanId { get; set; }
        public byte? ActionId { get; set; }
        public int UserId { get; set; }
        public string FullName { get; set; }
        public int CusId { get; set; }
        public string CusName { get; set; }
        public long MoneyAdd { get; set; }
        public long MoneySub { get; set; }
        public string Note { get; set; }
        public DateTime CreateDate { get; set; }
        public int? CodeId { get; set; }
        public string ItemName { get; set; }
        public long? MoneyPawn { get; set; }
        public long? MoneyInterest { get; set; }
        public long? OtherMoney { get; set; }
    }
}
