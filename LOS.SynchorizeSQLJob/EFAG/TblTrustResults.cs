﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTrustResults
    {
        public long Id { get; set; }
        public int? LoanCreditId { get; set; }
        public int? Code { get; set; }
        public string Message { get; set; }
    }
}
