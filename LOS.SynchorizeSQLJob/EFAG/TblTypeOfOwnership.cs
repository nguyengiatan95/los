﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblTypeOfOwnership
    {
        public int TypeOfOwnershipId { get; set; }
        public string Name { get; set; }
        public bool? IsActived { get; set; }
        public short? IsCheckKb { get; set; }
    }
}
