﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblUserCheckInByApp
    {
        public int Id { get; set; }
        public DateTime? CreateDate { get; set; }
        public string ImgUrl { get; set; }
        public string Adress { get; set; }
        public long? UserId { get; set; }
    }
}
