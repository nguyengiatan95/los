﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblUserDomainRegion
    {
        public byte Id { get; set; }
        public int? UserId { get; set; }
        public byte? DomainId { get; set; }
        public byte? RegionId { get; set; }
        public int? AreaId { get; set; }
    }
}
