﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblUserShop
    {
        public int UserId { get; set; }
        public int ShopId { get; set; }
    }
}
