﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblVaLog
    {
        public int Id { get; set; }
        public string Pcode { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CustomerCreditId { get; set; }
        public int? Status { get; set; }
        public string ReferenceId { get; set; }
    }
}
