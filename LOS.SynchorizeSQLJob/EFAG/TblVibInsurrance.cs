﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblVibInsurrance
    {
        public int Id { get; set; }
        public int? IdOfVbi { get; set; }
        public int? SoHdOfVbi { get; set; }
        public int? LoanId { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CustomerCreditId { get; set; }
        public string CustomerName { get; set; }
        public DateTime? BirthDay { get; set; }
        public long? TotalMoneyLoan { get; set; }
        public long? TotalFee { get; set; }
        public short? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string DataTransfer { get; set; }
        public string ReasonCancel { get; set; }
    }
}
