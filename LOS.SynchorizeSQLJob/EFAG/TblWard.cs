﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblWard
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TypeWard { get; set; }
        public string LongitudeLatitude { get; set; }
        public int? DistrictId { get; set; }
    }
}
