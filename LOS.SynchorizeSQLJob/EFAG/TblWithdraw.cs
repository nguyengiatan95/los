﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblWithdraw
    {
        public int Id { get; set; }
        public int? HeadquatersId { get; set; }
        public int? AgencyId { get; set; }
        public long? Money { get; set; }
        public short? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? UserAgency { get; set; }
        public string FullNameAgency { get; set; }
        public string NoteAgency { get; set; }
        public int? UserHeadquaters { get; set; }
        public string FullNameHeadquaters { get; set; }
        public string NoteHeadquaters { get; set; }
        public string BankCode { get; set; }
        public int? BankId { get; set; }
    }
}
