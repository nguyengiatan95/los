﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class TblWorkingIndustry
    {
        public int WorkingIndustryId { get; set; }
        public string Name { get; set; }
    }
}
