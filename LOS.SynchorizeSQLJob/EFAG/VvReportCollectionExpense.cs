﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class VvReportCollectionExpense
    {
        public string MaBp { get; set; }
        public string TenBp { get; set; }
        public string MaVv { get; set; }
        public string MaTd1 { get; set; }
        public string TenTd1 { get; set; }
        public string MaKh2 { get; set; }
        public string MaKh { get; set; }
        public string TenKh { get; set; }
        public string DiaChi { get; set; }
        public DateTime CreateDate { get; set; }
        public byte? ActionId { get; set; }
        public string Note { get; set; }
        public long? Tien { get; set; }
        public long? Tien1 { get; set; }
        public int MoneyInterest { get; set; }
        public int Tien2 { get; set; }
        public int Tien3 { get; set; }
        public string MaTd2 { get; set; }
        public string MaPhi { get; set; }
        public string MaTd3 { get; set; }
        public string Id { get; set; }
        public int Tien4 { get; set; }
        public int Tien5 { get; set; }
        public string DienGiaiPhi { get; set; }
        public string DienGiaiTima { get; set; }
        public string TkNh { get; set; }
        public string HtTt { get; set; }
    }
}
