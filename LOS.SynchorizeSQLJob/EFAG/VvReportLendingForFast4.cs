﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class VvReportLendingForFast4
    {
        public string CityId { get; set; }
        public string Name { get; set; }
        public string MaVv { get; set; }
        public string MaTd1 { get; set; }
        public string TenTd1 { get; set; }
        public string MaKh2 { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public DateTime CreateDate { get; set; }
        public byte? ActionId { get; set; }
        public string Note { get; set; }
        public long? Tien { get; set; }
        public long? MoneyPawn { get; set; }
        public long? MoneyInterest { get; set; }
        public long? MoneyConsultant { get; set; }
        public long? MoneyAgency { get; set; }
        public string TypePayment { get; set; }
        public string LoaiSp { get; set; }
        public string DienGiai { get; set; }
        public string MaGd { get; set; }
        public long? Tien4 { get; set; }
        public long? Tien5 { get; set; }
        public string DienGiaiPhi { get; set; }
        public string DienGiaiTima { get; set; }
        public string TkNh { get; set; }
        public string HtTt { get; set; }
    }
}
