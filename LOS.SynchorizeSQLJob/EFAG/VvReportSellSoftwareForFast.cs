﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFAG
{
    public partial class VvReportSellSoftwareForFast
    {
        public string CityId { get; set; }
        public string Name { get; set; }
        public string MaDl { get; set; }
        public string MaCh { get; set; }
        public string TenCh { get; set; }
        public string MaKh2 { get; set; }
        public string CustomerId { get; set; }
        public string FullName { get; set; }
        public string DiaChi { get; set; }
        public DateTime? CreateDate { get; set; }
        public int MaNoiDung { get; set; }
        public string NoiDung { get; set; }
        public int? TienGiaoDich { get; set; }
        public int? TienChoVay { get; set; }
        public int TongTienLai { get; set; }
        public int PhiTuVan { get; set; }
        public int TienLaiTraDl { get; set; }
        public string LoaiDl { get; set; }
        public string LoaiSp { get; set; }
        public string DienGiai { get; set; }
        public string MaGd { get; set; }
        public int Tien4 { get; set; }
        public int Tien5 { get; set; }
        public string TkNh { get; set; }
        public string HtTt { get; set; }
    }
}
