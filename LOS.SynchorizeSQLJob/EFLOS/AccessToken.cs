﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class AccessToken
    {
        public int Id { get; set; }
        public string AccessToken1 { get; set; }
        public string AppId { get; set; }
        public DateTime? DateExpiration { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
