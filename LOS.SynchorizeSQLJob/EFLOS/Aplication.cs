﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class Aplication
    {
        public int AplicationId { get; set; }
        public string AplicationName { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
    }
}
