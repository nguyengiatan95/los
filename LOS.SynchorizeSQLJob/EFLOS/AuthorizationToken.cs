﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class AuthorizationToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ServiceName { get; set; }
        public bool? Enlabled { get; set; }
    }
}
