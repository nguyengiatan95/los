﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class Bank
    {
        public Bank()
        {
            LoanBrief = new HashSet<LoanBrief>();
        }

        public int BankId { get; set; }
        public string Name { get; set; }
        public string BankCode { get; set; }
        public string NapasCode { get; set; }
        public int? Type { get; set; }
        public int? BankValue { get; set; }
        public int? MecashId { get; set; }

        public virtual ICollection<LoanBrief> LoanBrief { get; set; }
    }
}
