﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class BlackList
    {
        public int BlackListId { get; set; }
        public int? CustomerId { get; set; }
        public int? LoanBriefId { get; set; }
        public int? CreatedUserId { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public int? Status { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTimeOffset? ModifiedTime { get; set; }
        public string Note { get; set; }
        public DateTimeOffset? ExpirationTime { get; set; }
        public int? NumberOfDays { get; set; }
        public int? ApprovedUserId { get; set; }
        public DateTimeOffset? ApprovedTime { get; set; }
    }
}
