﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class BlackListLog
    {
        public int BlackListLogId { get; set; }
        public int? UserId { get; set; }
        public int? ShopId { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public int? BlackListId { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string IdNumber { get; set; }
        public DateTime? Dob { get; set; }
        public int? CheckResultValue { get; set; }
        public int? LoanBriefId { get; set; }
        public int? PlatformLoanBriefId { get; set; }
    }
}
