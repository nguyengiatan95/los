﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class BrandProduct
    {
        public BrandProduct()
        {
            LoanBriefProperty = new HashSet<LoanBriefProperty>();
            Product = new HashSet<Product>();
            ProductPercentReduction = new HashSet<ProductPercentReduction>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }

        public virtual ICollection<LoanBriefProperty> LoanBriefProperty { get; set; }
        public virtual ICollection<Product> Product { get; set; }
        public virtual ICollection<ProductPercentReduction> ProductPercentReduction { get; set; }
    }
}
