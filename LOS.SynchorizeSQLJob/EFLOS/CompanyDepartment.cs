﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class CompanyDepartment
    {
        public int CompanyId { get; set; }
        public int DepartmentId { get; set; }

        public virtual Company Company { get; set; }
        public virtual Department Department { get; set; }
    }
}
