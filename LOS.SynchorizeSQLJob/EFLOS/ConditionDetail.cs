﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class ConditionDetail
    {
        public int ConditionDetailId { get; set; }
        public int? ConditionId { get; set; }
        public int? PropertyId { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
    }
}
