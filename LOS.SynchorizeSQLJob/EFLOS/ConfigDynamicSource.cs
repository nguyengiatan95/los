﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class ConfigDynamicSource
    {
        public int ConfigDynamicSourceId { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string CodeQuery { get; set; }
        public bool? IsActive { get; set; }
        public string Description { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
