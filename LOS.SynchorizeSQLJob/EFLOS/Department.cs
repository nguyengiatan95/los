﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class Department
    {
        public Department()
        {
            CompanyDepartment = new HashSet<CompanyDepartment>();
            User = new HashSet<User>();
        }

        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public virtual ICollection<CompanyDepartment> CompanyDepartment { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
