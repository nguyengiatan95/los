﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class DeviceTrackingReport
    {
        public int DeviceTrackingReportId { get; set; }
        public int? LoanCreditId { get; set; }
        public int? Status { get; set; }
        public string AdressHome { get; set; }
        public string AdressCompany { get; set; }
        public DateTime? DisDate { get; set; }
        public DateTime? LastUpdateDevice { get; set; }
        public string StatusString { get; set; }
    }
}
