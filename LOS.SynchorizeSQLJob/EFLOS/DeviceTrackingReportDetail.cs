﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class DeviceTrackingReportDetail
    {
        public int DeviceTrackingReportDetailId { get; set; }
        public int? LoanCreditId { get; set; }
        public decimal? HomePercent { get; set; }
        public decimal? CompanyPercent { get; set; }
        public DateTime? TrackingDate { get; set; }
    }
}
