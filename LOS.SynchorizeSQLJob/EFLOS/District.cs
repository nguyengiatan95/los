﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class District
    {
        public District()
        {
            LoanBrief = new HashSet<LoanBrief>();
            LoanBriefHousehold = new HashSet<LoanBriefHousehold>();
            LoanBriefJob = new HashSet<LoanBriefJob>();
            LoanBriefResident = new HashSet<LoanBriefResident>();
        }

        public int DistrictId { get; set; }
        public int? ProvinceId { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
        public string LatLong { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? Priority { get; set; }
        public int? AreaId { get; set; }
        public int? DistrictCode { get; set; }
        public int? MecashId { get; set; }
        public short? IsApply { get; set; }

        public virtual ICollection<LoanBrief> LoanBrief { get; set; }
        public virtual ICollection<LoanBriefHousehold> LoanBriefHousehold { get; set; }
        public virtual ICollection<LoanBriefJob> LoanBriefJob { get; set; }
        public virtual ICollection<LoanBriefResident> LoanBriefResident { get; set; }
    }
}
