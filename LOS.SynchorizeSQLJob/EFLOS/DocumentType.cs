﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class DocumentType
    {
        public DocumentType()
        {
            LoanBriefFiles = new HashSet<LoanBriefFiles>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public byte? IsEnable { get; set; }
        public int? ProductId { get; set; }

        public virtual LoanBriefDocument LoanBriefDocument { get; set; }
        public virtual ICollection<LoanBriefFiles> LoanBriefFiles { get; set; }
    }
}
