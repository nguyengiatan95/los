﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class DynamicSource
    {
        public int LoanBriefId { get; set; }
        public int? ConfigDynamicSourceId { get; set; }
    }
}
