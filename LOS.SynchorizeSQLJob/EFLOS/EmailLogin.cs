﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class EmailLogin
    {
        public int EmailLoginId { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string ImageUrl { get; set; }
        public string Idtoken { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsConfirmed { get; set; }
        public int ConfirmedBy { get; set; }
        public DateTime? ConfirmedDate { get; set; }
        public bool IsActive { get; set; }
        public double? Iat { get; set; }
        public double? Exp { get; set; }
    }
}
