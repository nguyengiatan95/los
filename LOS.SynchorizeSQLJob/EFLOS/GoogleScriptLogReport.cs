﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class GoogleScriptLogReport
    {
        public int Id { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CreateBy { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
    }
}
