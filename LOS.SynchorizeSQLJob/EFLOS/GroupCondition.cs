﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class GroupCondition
    {
        public int Id { get; set; }
        public int? GroupId { get; set; }
        public int? PropertyId { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
        public DateTime? CreatetAt { get; set; }
        public int? CreatedBy { get; set; }

        public virtual Group Group { get; set; }
        public virtual Property Property { get; set; }
    }
}
