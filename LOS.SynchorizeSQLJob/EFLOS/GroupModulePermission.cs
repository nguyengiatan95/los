﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class GroupModulePermission
    {
        public int GroupModuleId { get; set; }
        public int PermissionId { get; set; }

        public virtual Permission Permission { get; set; }
    }
}
