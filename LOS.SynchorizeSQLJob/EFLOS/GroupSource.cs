﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class GroupSource
    {
        public GroupSource()
        {
            UtmSource = new HashSet<UtmSource>();
        }

        public int GroupSourceId { get; set; }
        public string GroupSourceName { get; set; }

        public virtual ICollection<UtmSource> UtmSource { get; set; }
    }
}
