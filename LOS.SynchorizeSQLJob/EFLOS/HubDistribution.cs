﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class HubDistribution
    {
        public int HubDistributionId { get; set; }
        public int? ProductId { get; set; }
        public int? ProvinceId { get; set; }
        public int? ServiceHub { get; set; }
        public int? PhysicalHub { get; set; }
        public DateTimeOffset? ApplyTime { get; set; }
    }
}
