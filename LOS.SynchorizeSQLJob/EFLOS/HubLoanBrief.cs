﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class HubLoanBrief
    {
        public int HubLoanBriefId { get; set; }
        public int? HubId { get; set; }
        public int? HubTypeId { get; set; }
        public int Count { get; set; }
        public DateTimeOffset? ApplyTime { get; set; }

        public virtual Shop Hub { get; set; }
    }
}
