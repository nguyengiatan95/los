﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class HubType
    {
        public int HubTypeId { get; set; }
        public string Name { get; set; }
    }
}
