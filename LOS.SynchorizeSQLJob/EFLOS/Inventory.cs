﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class Inventory
    {
        public int InventoryId { get; set; }
        public int? MaterialId { get; set; }
        public int? ShopId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? LoanCreditId { get; set; }
        public string ShopName { get; set; }
        public int? StatusOfDeviceId { get; set; }
        public int? Status { get; set; }
        public int? UserImportShopId { get; set; }
        public string UserImportShopName { get; set; }
    }
}
