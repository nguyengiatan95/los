﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class InventoryDetail
    {
        public int InventoryDetailId { get; set; }
        public int? MaterialId { get; set; }
        public int? FromShopId { get; set; }
        public int? ToShopId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CreateBy { get; set; }
        public string Description { get; set; }
        public int? InventoryType { get; set; }
    }
}
