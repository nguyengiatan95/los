﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class Job
    {
        public Job()
        {
            LoanBriefJob = new HashSet<LoanBriefJob>();
        }

        public int JobId { get; set; }
        public string Name { get; set; }
        public int? Priority { get; set; }
        public int? MecashId { get; set; }

        public virtual ICollection<LoanBriefJob> LoanBriefJob { get; set; }
    }
}
