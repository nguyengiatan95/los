﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LOSContext : DbContext
    {
        public LOSContext()
        {
        }

        public LOSContext(DbContextOptions<LOSContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccessToken> AccessToken { get; set; }
        public virtual DbSet<ApiMonitor> ApiMonitor { get; set; }
        public virtual DbSet<AuthorizationToken> AuthorizationToken { get; set; }
        public virtual DbSet<Bank> Bank { get; set; }
        public virtual DbSet<BlackList> BlackList { get; set; }
        public virtual DbSet<BlackListLog> BlackListLog { get; set; }
        public virtual DbSet<BrandProduct> BrandProduct { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<ConfigDynamicSource> ConfigDynamicSource { get; set; }
        public virtual DbSet<ControllerAction> ControllerAction { get; set; }
        public virtual DbSet<CoordinatorCheckList> CoordinatorCheckList { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<DataCic> DataCic { get; set; }
        public virtual DbSet<DeviceTrackingReport> DeviceTrackingReport { get; set; }
        public virtual DbSet<DeviceTrackingReportDetail> DeviceTrackingReportDetail { get; set; }
        public virtual DbSet<District> District { get; set; }
        public virtual DbSet<DocumentType> DocumentType { get; set; }
        public virtual DbSet<DynamicSource> DynamicSource { get; set; }
        public virtual DbSet<EmailLogin> EmailLogin { get; set; }
        public virtual DbSet<EmailPermission> EmailPermission { get; set; }
        public virtual DbSet<GoogleScriptLogReport> GoogleScriptLogReport { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<GroupActionPermission> GroupActionPermission { get; set; }
        public virtual DbSet<GroupCondition> GroupCondition { get; set; }
        public virtual DbSet<GroupModule> GroupModule { get; set; }
        public virtual DbSet<GroupModulePermission> GroupModulePermission { get; set; }
        public virtual DbSet<GroupSource> GroupSource { get; set; }
        public virtual DbSet<HubDistribution> HubDistribution { get; set; }
        public virtual DbSet<HubLoanBrief> HubLoanBrief { get; set; }
        public virtual DbSet<HubType> HubType { get; set; }
        public virtual DbSet<Inventory> Inventory { get; set; }
        public virtual DbSet<InventoryDetail> InventoryDetail { get; set; }
        public virtual DbSet<Job> Job { get; set; }
        public virtual DbSet<JobTitle> JobTitle { get; set; }
        public virtual DbSet<LoanAction> LoanAction { get; set; }
        public virtual DbSet<LoanBrief> LoanBrief { get; set; }
        public virtual DbSet<LoanBriefCompany> LoanBriefCompany { get; set; }
        public virtual DbSet<LoanBriefDocument> LoanBriefDocument { get; set; }
        public virtual DbSet<LoanBriefFiles> LoanBriefFiles { get; set; }
        public virtual DbSet<LoanBriefHistory> LoanBriefHistory { get; set; }
        public virtual DbSet<LoanBriefHousehold> LoanBriefHousehold { get; set; }
        public virtual DbSet<LoanBriefJob> LoanBriefJob { get; set; }
        public virtual DbSet<LoanbriefLender> LoanBriefLender { get; set; }
        public virtual DbSet<LoanBriefNote> LoanBriefNote { get; set; }
        public virtual DbSet<LoanBriefProperty> LoanBriefProperty { get; set; }
        public virtual DbSet<LoanBriefRelationship> LoanBriefRelationship { get; set; }
        public virtual DbSet<LoanBriefResident> LoanBriefResident { get; set; }
        public virtual DbSet<LoanProduct> LoanProduct { get; set; }
        public virtual DbSet<LoanRateType> LoanRateType { get; set; }
        public virtual DbSet<LoanStatus> LoanStatus { get; set; }
        public virtual DbSet<LoanbriefScript> LoanbriefScript { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<LogBaoCaoTuDongDinhVi> LogBaoCaoTuDongDinhVi { get; set; }
        public virtual DbSet<LogCallApi> LogCallApi { get; set; }
        public virtual DbSet<LogCallApi1> LogCallApi1 { get; set; }
        public virtual DbSet<LogClickToCall> LogClickToCall { get; set; }
        public virtual DbSet<LogGeocodingResult> LogGeocodingResult { get; set; }
        public virtual DbSet<LogLatLngHomeCompany> LogLatLngHomeCompany { get; set; }
        public virtual DbSet<LogLoanInfoAi> LogLoanInfoAi { get; set; }
        public virtual DbSet<LogPushToHub> LogPushToHub { get; set; }
        public virtual DbSet<LogRequestAi> LogRequestAi { get; set; }
        public virtual DbSet<LogSendVoiceOtp> LogSendVoiceOtp { get; set; }
        public virtual DbSet<LogTrackingStopPoin> LogTrackingStopPoin { get; set; }
        public virtual DbSet<LogWorking> LogWorking { get; set; }
        public virtual DbSet<ManagerAreaHub> ManagerAreaHub { get; set; }
        public virtual DbSet<Material> Material { get; set; }
        public virtual DbSet<Module> Module { get; set; }
        public virtual DbSet<ModulePermission> ModulePermission { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<OrderSource> OrderSource { get; set; }
        public virtual DbSet<OrderSourceDetail> OrderSourceDetail { get; set; }
        public virtual DbSet<Permission> Permission { get; set; }
        public virtual DbSet<PermissionIp> PermissionIp { get; set; }
        public virtual DbSet<Pipeline> Pipeline { get; set; }
        public virtual DbSet<PipelineHistory> PipelineHistory { get; set; }
        public virtual DbSet<PipelineState> PipelineState { get; set; }
        public virtual DbSet<PlatformType> PlatformType { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductAppraiser> ProductAppraiser { get; set; }
        public virtual DbSet<ProductPercentReduction> ProductPercentReduction { get; set; }
        public virtual DbSet<ProductReview> ProductReview { get; set; }
        public virtual DbSet<ProductReviewDetail> ProductReviewDetail { get; set; }
        public virtual DbSet<ProductReviewResult> ProductReviewResult { get; set; }
        public virtual DbSet<ProductReviewResultDetail> ProductReviewResultDetail { get; set; }
        public virtual DbSet<ProductType> ProductType { get; set; }
        public virtual DbSet<Property> Property { get; set; }
        public virtual DbSet<Province> Province { get; set; }
        public virtual DbSet<ReasonCancel> ReasonCancel { get; set; }
        public virtual DbSet<RelativeFamily> RelativeFamily { get; set; }
        public virtual DbSet<ReportBehaviorHub> ReportBehaviorHub { get; set; }
        public virtual DbSet<ReportBehaviorTelesale> ReportBehaviorTelesale { get; set; }
        public virtual DbSet<ReportConvertHub> ReportConvertHub { get; set; }
        public virtual DbSet<ReportConvertTelesale> ReportConvertTelesale { get; set; }
        public virtual DbSet<ReportData> ReportData { get; set; }
        public virtual DbSet<ReportDataUpdating> ReportDataUpdating { get; set; }
        public virtual DbSet<ReportDepartmentError> ReportDepartmentError { get; set; }
        public virtual DbSet<ReportToken> ReportToken { get; set; }
        public virtual DbSet<Schedule> Schedule { get; set; }
        public virtual DbSet<Section> Section { get; set; }
        public virtual DbSet<SectionAction> SectionAction { get; set; }
        public virtual DbSet<SectionApprove> SectionApprove { get; set; }
        public virtual DbSet<SectionCondition> SectionCondition { get; set; }
        public virtual DbSet<SectionDetail> SectionDetail { get; set; }
        public virtual DbSet<SectionGroup> SectionGroup { get; set; }
        public virtual DbSet<Shop> Shop { get; set; }
        public virtual DbSet<TelesaleLoanbrief> TelesaleLoanbrief { get; set; }
        public virtual DbSet<TokenSmartDailer> TokenSmartDailer { get; set; }
        public virtual DbSet<Transactions> Transactions { get; set; }
        public virtual DbSet<TypeOwnerShip> TypeOwnerShip { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserActionPermission> UserActionPermission { get; set; }
        public virtual DbSet<UserCondition> UserCondition { get; set; }
        public virtual DbSet<UserModule> UserModule { get; set; }
        public virtual DbSet<UserShop> UserShop { get; set; }
        public virtual DbSet<UtmSource> UtmSource { get; set; }
        public virtual DbSet<Ward> Ward { get; set; }
        public virtual DbSet<ZaloDinhVi> ZaloDinhVi { get; set; }
        public virtual DbSet<ZaloLoanCredit> ZaloLoanCredit { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=188.166.250.12,1533;Database=LOS;uid=los;password=Tima@2020!#;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccessToken>(entity =>
            {
                entity.Property(e => e.AccessToken1).HasColumnName("access_token");

                entity.Property(e => e.AppId)
                    .HasColumnName("app_id")
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.DateExpiration).HasColumnType("datetime");
            });

            modelBuilder.Entity<ApiMonitor>(entity =>
            {
                entity.ToTable("ApiMonitor", "rp");

                entity.Property(e => e.Action).HasMaxLength(1000);

                entity.Property(e => e.AppKey).HasMaxLength(1000);

                entity.Property(e => e.Controler).HasMaxLength(1000);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Desciption).HasMaxLength(500);

                entity.Property(e => e.Request).HasMaxLength(1000);

                entity.Property(e => e.Responce).HasMaxLength(1000);
            });

            modelBuilder.Entity<AuthorizationToken>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ServiceName).HasMaxLength(50);

                entity.Property(e => e.Token).HasMaxLength(1204);
            });

            modelBuilder.Entity<Bank>(entity =>
            {
                entity.Property(e => e.BankCode).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.NapasCode).HasMaxLength(50);
            });

            modelBuilder.Entity<BlackList>(entity =>
            {
                entity.Property(e => e.Note).HasColumnType("ntext");
            });

            modelBuilder.Entity<BlackListLog>(entity =>
            {
                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.IdNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BrandProduct>(entity =>
            {
                entity.Property(e => e.IsEnable).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ConfigDynamicSource>(entity =>
            {
                entity.ToTable("ConfigDynamicSource", "rp");

                entity.HasComment("cấu hình báo cáo động");

                entity.Property(e => e.CodeQuery).HasComment("lưu điều kiện where");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(1000);

                entity.Property(e => e.Name).HasMaxLength(255);
            });

            modelBuilder.Entity<ControllerAction>(entity =>
            {
                entity.ToTable("ControllerAction", "rp");

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Controller)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<LoanbriefLender>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.LenderName).HasMaxLength(200);

                entity.Property(e => e.NationalCard).HasMaxLength(50);
            });

            modelBuilder.Entity<CoordinatorCheckList>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.AddressLatLong)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyAddress).HasMaxLength(500);

                entity.Property(e => e.CompanyAddressLatLong)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyName).HasMaxLength(500);

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("date");

                entity.Property(e => e.Email)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookAddress).HasMaxLength(500);

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.InsurenceNote).HasMaxLength(500);

                entity.Property(e => e.InsurenceNumber).HasMaxLength(50);

                entity.Property(e => e.NationCardPlace).HasMaxLength(200);

                entity.Property(e => e.NationalCard)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NationalCardDate).HasColumnType("date");

                entity.Property(e => e.Phone)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Salary).HasColumnType("money");
            });

            modelBuilder.Entity<DataCic>(entity =>
            {
                entity.ToTable("DataCIC");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.Brieft).HasMaxLength(50);

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CheckTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.StringJson).HasColumnType("ntext");
            });

            modelBuilder.Entity<DeviceTrackingReport>(entity =>
            {
                entity.ToTable("DeviceTrackingReport", "rp");

                entity.Property(e => e.AdressCompany).HasMaxLength(1000);

                entity.Property(e => e.AdressHome).HasMaxLength(1000);

                entity.Property(e => e.DisDate).HasColumnType("datetime");

                entity.Property(e => e.LastUpdateDevice).HasColumnType("datetime");

                entity.Property(e => e.StatusString).HasMaxLength(100);
            });

            modelBuilder.Entity<DeviceTrackingReportDetail>(entity =>
            {
                entity.ToTable("DeviceTrackingReportDetail", "rp");

                entity.Property(e => e.CompanyPercent).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.HomePercent).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TrackingDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<District>(entity =>
            {
                entity.Property(e => e.LatLong)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<DocumentType>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(250);
            });

            modelBuilder.Entity<DynamicSource>(entity =>
            {
                entity.HasKey(e => e.LoanBriefId);

                entity.ToTable("DynamicSource", "rp");

                entity.HasComment("Báo cáo tổng hợp động");

                entity.Property(e => e.LoanBriefId)
                    .HasColumnName("loanBriefId")
                    .ValueGeneratedNever();

                entity.Property(e => e.ConfigDynamicSourceId).HasComment("id cấu hình trong bản ConfigDynamicSource");
            });

            modelBuilder.Entity<EmailLogin>(entity =>
            {
                entity.ToTable("EmailLogin", "rp");

                entity.Property(e => e.ConfirmedDate).HasColumnType("datetime");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email).HasMaxLength(500);

                entity.Property(e => e.Exp).HasComment("thời gian hết hạn token, google trả về");

                entity.Property(e => e.FamilyName).HasMaxLength(1000);

                entity.Property(e => e.FullName).HasMaxLength(1000);

                entity.Property(e => e.GivenName).HasMaxLength(1000);

                entity.Property(e => e.Iat).HasComment("Thời gian google trả về");

                entity.Property(e => e.Idtoken).HasColumnName("IDToken");

                entity.Property(e => e.ImageUrl)
                    .HasColumnName("ImageURL")
                    .HasMaxLength(1000);
            });

            modelBuilder.Entity<EmailPermission>(entity =>
            {
                entity.ToTable("EmailPermission", "rp");
            });

            modelBuilder.Entity<GoogleScriptLogReport>(entity =>
            {
                entity.HasComment("Lưu log code trên GoogleScript");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(500);
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.Property(e => e.DefaultPath).HasMaxLength(200);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.GroupName).HasMaxLength(200);

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<GroupActionPermission>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupActionPermission)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("FK_GroupActionPermission_Group");
            });

            modelBuilder.Entity<GroupCondition>(entity =>
            {
                entity.Property(e => e.CreatetAt).HasColumnType("datetime");

                entity.Property(e => e.Operator)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value).HasMaxLength(500);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupCondition)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("FK_GroupCondition_GroupCondition");

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.GroupCondition)
                    .HasForeignKey(d => d.PropertyId)
                    .HasConstraintName("FK_GroupCondition_Property");
            });

            modelBuilder.Entity<GroupModule>(entity =>
            {
                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupModule)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupModule_Group");

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.GroupModule)
                    .HasForeignKey(d => d.ModuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupModule_Module");
            });

            modelBuilder.Entity<GroupModulePermission>(entity =>
            {
                entity.HasKey(e => new { e.GroupModuleId, e.PermissionId });

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.GroupModulePermission)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupModulePermission_Permission");
            });

            modelBuilder.Entity<GroupSource>(entity =>
            {
                entity.Property(e => e.GroupSourceId).HasColumnName("GroupSourceID");

                entity.Property(e => e.GroupSourceName).HasMaxLength(500);
            });

            modelBuilder.Entity<HubLoanBrief>(entity =>
            {
                entity.HasOne(d => d.Hub)
                    .WithMany(p => p.HubLoanBrief)
                    .HasForeignKey(d => d.HubId)
                    .HasConstraintName("FK_HubLoanBrief_Shop");
            });

            modelBuilder.Entity<HubType>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<Inventory>(entity =>
            {
                entity.ToTable("Inventory", "rp");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ShopName).HasMaxLength(50);

                entity.Property(e => e.StatusOfDeviceId).HasColumnName("StatusOfDeviceID");

                entity.Property(e => e.UserImportShopId).HasComment("người dùng chuyển thiết bị qua kho khác, khi hệ thống check nếu TB trong db ở kho khác với người dùng, thì sẽ ưu tiên kho của người dùng, nếu  = nhau thì sẽ clear cột này");

                entity.Property(e => e.UserImportShopName).HasMaxLength(50);
            });

            modelBuilder.Entity<InventoryDetail>(entity =>
            {
                entity.ToTable("InventoryDetail", "rp");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<JobTitle>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Priority)
                    .HasMaxLength(10)
                    .IsFixedLength();
            });

            modelBuilder.Entity<LoanAction>(entity =>
            {
                entity.Property(e => e.Note).HasMaxLength(500);

                entity.HasOne(d => d.Actor)
                    .WithMany(p => p.LoanAction)
                    .HasForeignKey(d => d.ActorId)
                    .HasConstraintName("FK_LoanAction_User");

                entity.HasOne(d => d.LoanBrief)
                    .WithMany(p => p.LoanAction)
                    .HasForeignKey(d => d.LoanBriefId)
                    .HasConstraintName("FK_LoanAction_LoanBrief");
            });

            modelBuilder.Entity<LoanBrief>(entity =>
            {
                entity.Property(e => e.AffCode)
                    .HasColumnName("Aff_Code")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.AffPub).HasMaxLength(500);

                entity.Property(e => e.AffSid)
                    .HasColumnName("Aff_Sid")
                    .HasMaxLength(500);

                entity.Property(e => e.AffSource).HasMaxLength(500);

                entity.Property(e => e.BankAccountName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BankAccountNumber)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BankCardNumber)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BeginStartTime).HasColumnType("datetime");

                entity.Property(e => e.CheckCic).HasColumnName("CheckCIC");

                entity.Property(e => e.ContractGinno).HasMaxLength(50);

                entity.Property(e => e.CoordinatorPushAt).HasColumnType("datetime");

                entity.Property(e => e.DeviceId).HasMaxLength(50);

                entity.Property(e => e.DisbursementAt).HasColumnType("datetime");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("date");

                entity.Property(e => e.DomainName).HasMaxLength(500);

                entity.Property(e => e.Email)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EvaluationCompanyTime).HasColumnType("datetime");

                entity.Property(e => e.EvaluationHomeTime).HasColumnType("datetime");

                entity.Property(e => e.FeeInsuranceOfCustomer).HasColumnType("money");

                entity.Property(e => e.FeeInsuranceOfLender).HasColumnType("money");

                entity.Property(e => e.FirstProcessingTime)
                    .HasColumnName("First_ProcessingTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.FirstTimeHubFeedBack).HasColumnType("datetime");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.HubPushAt).HasColumnType("datetime");

                entity.Property(e => e.InProcess).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsCheckCic).HasColumnName("IsCheckCIC");

                entity.Property(e => e.IsVerifyOtpLdp).HasColumnName("IsVerifyOtpLDP");

                entity.Property(e => e.LabelRequestLocation).HasMaxLength(500);

                entity.Property(e => e.LabelResponseLocation).HasMaxLength(500);

                entity.Property(e => e.LabelScore).HasMaxLength(50);

                entity.Property(e => e.LastSendRequestActive).HasColumnType("datetime");

                entity.Property(e => e.LeadScore).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.LenderReceivedDate).HasColumnType("datetime");

                entity.Property(e => e.LmsLoanId).HasColumnName("LMS_LoanID");

                entity.Property(e => e.LoanAmount).HasColumnType("money");

                entity.Property(e => e.LoanAmountExpertise).HasColumnType("money");

                entity.Property(e => e.LoanAmountExpertiseLast).HasColumnType("money");

                entity.Property(e => e.LoanAmountFirst).HasColumnType("money");

                entity.Property(e => e.LoanBriefCancelAt).HasColumnType("datetime");

                entity.Property(e => e.NationCardPlace).HasMaxLength(200);

                entity.Property(e => e.NationalCard)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NationalCardDate).HasColumnType("date");

                entity.Property(e => e.NextDate).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RateMoney).HasColumnType("money");

                entity.Property(e => e.ResultCic).HasColumnName("ResultCIC");

                entity.Property(e => e.ResultLocation).HasMaxLength(500);

                entity.Property(e => e.ResultRuleCheck).HasMaxLength(500);

                entity.Property(e => e.ScheduleTime).HasColumnType("datetime");          

                entity.Property(e => e.TelesalesPushAt).HasColumnType("datetime");

                entity.Property(e => e.Tid)
                    .HasColumnName("TId")
                    .HasMaxLength(500);

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.Property(e => e.UtmCampaign).HasMaxLength(500);

                entity.Property(e => e.UtmContent).HasMaxLength(500);

                entity.Property(e => e.UtmMedium).HasMaxLength(500);

                entity.Property(e => e.UtmSource).HasMaxLength(500);

                entity.Property(e => e.UtmTerm).HasMaxLength(500);

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("FK_LoanBrief_Bank");

                entity.HasOne(d => d.BoundTelesale)
                    .WithMany(p => p.LoanBriefBoundTelesale)
                    .HasForeignKey(d => d.BoundTelesaleId)
                    .HasConstraintName("FK_LoanBrief_User3");

                entity.HasOne(d => d.CoordinatorUser)
                    .WithMany(p => p.LoanBriefCoordinatorUser)
                    .HasForeignKey(d => d.CoordinatorUserId)
                    .HasConstraintName("FK_LoanBrief_UserCoordinator");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_LoanBrief_Customer");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_LoanBrief_District");

                entity.HasOne(d => d.EvaluationCompanyUser)
                    .WithMany(p => p.LoanBriefEvaluationCompanyUser)
                    .HasForeignKey(d => d.EvaluationCompanyUserId)
                    .HasConstraintName("FK_LoanBrief_User1");

                entity.HasOne(d => d.EvaluationHomeUser)
                    .WithMany(p => p.LoanBriefEvaluationHomeUser)
                    .HasForeignKey(d => d.EvaluationHomeUserId)
                    .HasConstraintName("FK_LoanBrief_User2");

                entity.HasOne(d => d.HubEmployee)
                    .WithMany(p => p.LoanBriefHubEmployee)
                    .HasForeignKey(d => d.HubEmployeeId)
                    .HasConstraintName("FK_LoanBrief_User");

                entity.HasOne(d => d.Hub)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.HubId)
                    .HasConstraintName("FK_LoanBrief_Shop");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_LoanBrief_LoanProduct");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_LoanBrief_Province");

                entity.HasOne(d => d.RateType)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.RateTypeId)
                    .HasConstraintName("FK_LoanBrief_LoanRateType");

                entity.HasOne(d => d.ReasonCancelNavigation)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.ReasonCancel)
                    .HasConstraintName("FK_LoanBrief_ReasonCancel");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.LoanBrief)
                    .HasForeignKey(d => d.WardId)
                    .HasConstraintName("FK_LoanBrief_Ward");
            });

            modelBuilder.Entity<LoanBriefCompany>(entity =>
            {
                entity.HasKey(e => e.LoanBriefId)
                    .HasName("PK_LoanBriefCompany_1");

                entity.Property(e => e.LoanBriefId).ValueGeneratedNever();

                entity.Property(e => e.BirthdayShareholder).HasColumnType("datetime");

                entity.Property(e => e.BusinessCertificationAddress).HasMaxLength(500);

                entity.Property(e => e.BusinessCertificationDate).HasColumnType("datetime");

                entity.Property(e => e.CardNumberShareholder)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CardNumberShareholderDate).HasColumnType("datetime");

                entity.Property(e => e.CareerBusiness).HasMaxLength(500);

                entity.Property(e => e.CompanyName).HasMaxLength(500);

                entity.Property(e => e.CompanyShareholder).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.PlaceOfBirthShareholder).HasMaxLength(500);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.LoanBrief)
                    .WithOne(p => p.LoanBriefCompany)
                    .HasForeignKey<LoanBriefCompany>(d => d.LoanBriefId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoanBriefCompany_LoanBrief");
            });

            modelBuilder.Entity<LoanBriefDocument>(entity =>
            {
                entity.Property(e => e.LoanBriefDocumentId).ValueGeneratedOnAdd();

                entity.Property(e => e.FilePath).HasMaxLength(500);

                entity.Property(e => e.RelativePath).HasMaxLength(500);

                entity.HasOne(d => d.LoanBriefDocumentNavigation)
                    .WithOne(p => p.LoanBriefDocument)
                    .HasForeignKey<LoanBriefDocument>(d => d.LoanBriefDocumentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoanBriefDocument_DocumentType");

                entity.HasOne(d => d.LoanBrief)
                    .WithMany(p => p.LoanBriefDocument)
                    .HasForeignKey(d => d.LoanBriefId)
                    .HasConstraintName("FK_LoanBriefDocument_LoanBrief");
            });

            modelBuilder.Entity<LoanBriefFiles>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateAt).HasColumnType("datetime");

                entity.Property(e => e.DomainOfPartner)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.LinkImgOfPartner)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyAt).HasColumnType("datetime");

                entity.Property(e => e.S3status)
                    .HasColumnName("S3Status")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Synchronize).HasDefaultValueSql("((0))");

                entity.Property(e => e.TypeFile)
                    .HasDefaultValueSql("((1))")
                    .HasComment("File Ảnh, File Video hoặc File Âm Thanh");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.LoanBriefFiles)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_LoanBriefFiles_DocumentType");
            });

            modelBuilder.Entity<LoanBriefHistory>(entity =>
            {
                entity.Property(e => e.Action)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NewValue).HasColumnType("ntext");

                entity.Property(e => e.OldValue).HasColumnType("ntext");

                entity.HasOne(d => d.LoanBrief)
                    .WithMany(p => p.LoanBriefHistory)
                    .HasForeignKey(d => d.LoanBriefId)
                    .HasConstraintName("FK_LoanBriefHistory_LoanBrief");
            });

            modelBuilder.Entity<LoanBriefHousehold>(entity =>
            {
                entity.Property(e => e.LoanBriefHouseholdId).ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.AppartmentNumber).HasMaxLength(50);

                entity.Property(e => e.Block).HasMaxLength(50);

                entity.Property(e => e.Lane).HasMaxLength(50);

                entity.HasOne(d => d.District)
                    .WithMany(p => p.LoanBriefHousehold)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_LoanBriefHousehold_District");

                entity.HasOne(d => d.LoanBriefHouseholdNavigation)
                    .WithOne(p => p.LoanBriefHousehold)
                    .HasForeignKey<LoanBriefHousehold>(d => d.LoanBriefHouseholdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoanBriefHousehold_LoanBrief");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.LoanBriefHousehold)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_LoanBriefHousehold_Province");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.LoanBriefHousehold)
                    .HasForeignKey(d => d.WardId)
                    .HasConstraintName("FK_LoanBriefHousehold_Ward");
            });

            modelBuilder.Entity<LoanBriefJob>(entity =>
            {
                entity.Property(e => e.LoanBriefJobId).ValueGeneratedNever();

                entity.Property(e => e.BankName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyAddress).HasMaxLength(500);

                entity.Property(e => e.CompanyAddressGoogleMap).HasMaxLength(500);

                entity.Property(e => e.CompanyAddressLatLng).HasMaxLength(100);

                entity.Property(e => e.CompanyName).HasMaxLength(200);

                entity.Property(e => e.CompanyPhone).HasMaxLength(50);

                entity.Property(e => e.CompanyTaxCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentName).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.TotalIncome).HasColumnType("money");

                entity.Property(e => e.WorkingAddress).HasMaxLength(500);

                entity.HasOne(d => d.CompanyDistrict)
                    .WithMany(p => p.LoanBriefJob)
                    .HasForeignKey(d => d.CompanyDistrictId)
                    .HasConstraintName("FK_LoanBriefJob_District");

                entity.HasOne(d => d.CompanyProvince)
                    .WithMany(p => p.LoanBriefJob)
                    .HasForeignKey(d => d.CompanyProvinceId)
                    .HasConstraintName("FK_LoanBriefJob_Province");

                entity.HasOne(d => d.CompanyWard)
                    .WithMany(p => p.LoanBriefJob)
                    .HasForeignKey(d => d.CompanyWardId)
                    .HasConstraintName("FK_LoanBriefJob_Ward");

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.LoanBriefJob)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("FK_LoanBriefJob_Job");

                entity.HasOne(d => d.JobTitle)
                    .WithMany(p => p.LoanBriefJob)
                    .HasForeignKey(d => d.JobTitleId)
                    .HasConstraintName("FK_LoanBriefJob_JobTitle");

                entity.HasOne(d => d.LoanBriefJobNavigation)
                    .WithOne(p => p.LoanBriefJob)
                    .HasForeignKey<LoanBriefJob>(d => d.LoanBriefJobId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoanBriefJob_LoanBrief");
            });

            modelBuilder.Entity<LoanBriefNote>(entity =>
            {
                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.ShopName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.LoanBrief)
                    .WithMany(p => p.LoanBriefNote)
                    .HasForeignKey(d => d.LoanBriefId)
                    .HasConstraintName("FK_LoanBriefNote_LoanBrief");
            });

            modelBuilder.Entity<LoanBriefProperty>(entity =>
            {
                entity.Property(e => e.LoanBriefPropertyId).ValueGeneratedNever();

                entity.Property(e => e.CarManufacturer).HasMaxLength(100);

                entity.Property(e => e.CarName).HasMaxLength(100);

                entity.HasOne(d => d.Brand)
                    .WithMany(p => p.LoanBriefProperty)
                    .HasForeignKey(d => d.BrandId)
                    .HasConstraintName("FK_LoanBriefProperty_BrandProduct");

                entity.HasOne(d => d.LoanBriefPropertyNavigation)
                    .WithOne(p => p.LoanBriefProperty)
                    .HasForeignKey<LoanBriefProperty>(d => d.LoanBriefPropertyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoanBriefProperty_LoanBrief");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.LoanBriefProperty)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_LoanBriefProperty_Product");
            });

            modelBuilder.Entity<LoanBriefRelationship>(entity =>
            {
                entity.Property(e => e.CardNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.LoanBrief)
                    .WithMany(p => p.LoanBriefRelationship)
                    .HasForeignKey(d => d.LoanBriefId)
                    .HasConstraintName("FK_LoanBriefRelationship_LoanBrief");

                entity.HasOne(d => d.RelationshipTypeNavigation)
                    .WithMany(p => p.LoanBriefRelationship)
                    .HasForeignKey(d => d.RelationshipType)
                    .HasConstraintName("FK_LoanBriefRelationship_RelativeFamily");
            });

            modelBuilder.Entity<LoanBriefResident>(entity =>
            {
                entity.Property(e => e.LoanBriefResidentId).ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.AddressGoogleMap).HasMaxLength(500);

                entity.Property(e => e.AddressLatLng).HasMaxLength(100);

                entity.Property(e => e.AppartmentNumber).HasMaxLength(50);

                entity.Property(e => e.Block).HasMaxLength(50);

                entity.Property(e => e.Direction).HasMaxLength(500);

                entity.Property(e => e.Lane).HasMaxLength(50);

                entity.HasOne(d => d.District)
                    .WithMany(p => p.LoanBriefResident)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_LoanBriefResident_District");

                entity.HasOne(d => d.LoanBriefResidentNavigation)
                    .WithOne(p => p.LoanBriefResident)
                    .HasForeignKey<LoanBriefResident>(d => d.LoanBriefResidentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoanBriefResident_LoanBrief");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.LoanBriefResident)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_LoanBriefResident_Province");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.LoanBriefResident)
                    .HasForeignKey(d => d.WardId)
                    .HasConstraintName("FK_LoanBriefResident_Ward");
            });

            modelBuilder.Entity<LoanProduct>(entity =>
            {
                entity.Property(e => e.MaximumMoney).HasColumnType("money");

                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<LoanRateType>(entity =>
            {
                entity.Property(e => e.LoanRateTypeId).ValueGeneratedNever();

                entity.Property(e => e.RateType).HasMaxLength(500);
            });

            modelBuilder.Entity<LoanStatus>(entity =>
            {
                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasMaxLength(200);
            });

            modelBuilder.Entity<LoanbriefScript>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ScriptView)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.Property(e => e.City).HasMaxLength(500);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Device).HasMaxLength(50);

                entity.Property(e => e.Lat).HasMaxLength(50);

                entity.Property(e => e.Long).HasMaxLength(50);
            });

            modelBuilder.Entity<LogBaoCaoTuDongDinhVi>(entity =>
            {
                entity.ToTable("LogBaoCaoTuDongDinhVi", "rp");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ZaloRecipientId).HasMaxLength(100);

                entity.Property(e => e.ZaloSenderId).HasMaxLength(100);
            });

            modelBuilder.Entity<LogCallApi>(entity =>
            {
                entity.ToTable("LogCallAPI");

                entity.Property(e => e.ActionCallApi).HasColumnName("ActionCallAPI");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.LinkCallApi)
                    .HasColumnName("LinkCallAPI")
                    .HasMaxLength(500);

                entity.Property(e => e.ModifyAt).HasColumnType("datetime");

                entity.Property(e => e.TokenCallApi).HasColumnName("TokenCallAPI");
            });

            modelBuilder.Entity<LogCallApi1>(entity =>
            {
                entity.ToTable("LogCallApi", "rp");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.InputStringCallApi).HasColumnType("ntext");

                entity.Property(e => e.ModifyOn).HasColumnType("datetime");

                entity.Property(e => e.NameActionCallApi).HasMaxLength(500);

                entity.Property(e => e.ResultStringCallApi).HasColumnType("ntext");
            });

            modelBuilder.Entity<LogClickToCall>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.PhoneOfCustomer)
                    .HasMaxLength(13)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LogGeocodingResult>(entity =>
            {
                entity.ToTable("LogGeocodingResult", "rp");

                entity.Property(e => e.CityName).HasMaxLength(500);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DistrictName).HasMaxLength(500);

                entity.Property(e => e.FormattedAddress).HasMaxLength(1000);

                entity.Property(e => e.Lat).HasMaxLength(500);

                entity.Property(e => e.Lng).HasMaxLength(500);

                entity.Property(e => e.StreetName).HasMaxLength(1000);

                entity.Property(e => e.StreetNumber).HasMaxLength(100);

                entity.Property(e => e.WardName).HasMaxLength(500);
            });

            modelBuilder.Entity<LogLatLngHomeCompany>(entity =>
            {
                entity.ToTable("LogLatLngHomeCompany", "rp");

                entity.Property(e => e.CompanyLat).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyLng).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HomeLat).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HomeLng).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<LogLoanInfoAi>(entity =>
            {
                entity.ToTable("LogLoanInfoAI");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.HomeNetwok).HasMaxLength(50);

                entity.Property(e => e.IsCancel).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsExcuted).HasDefaultValueSql("((0))");

                entity.Property(e => e.RefCode).HasMaxLength(200);

                entity.Property(e => e.ResultFinal).HasMaxLength(50);

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(500);

                entity.HasOne(d => d.Loanbrief)
                    .WithMany(p => p.LogLoanInfoAi)
                    .HasForeignKey(d => d.LoanbriefId)
                    .HasConstraintName("FK_LogLoanInfoAI_LoanBrief");
            });

            modelBuilder.Entity<LogPushToHub>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogRequestAi>(entity =>
            {
                entity.ToTable("LogRequestAI");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.HasOne(d => d.Loanbrief)
                    .WithMany(p => p.LogRequestAi)
                    .HasForeignKey(d => d.LoanbriefId)
                    .HasConstraintName("FK_LogRequestAI_LoanBrief");
            });

            modelBuilder.Entity<LogSendVoiceOtp>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Domain).HasMaxLength(250);

                entity.Property(e => e.Otp)
                    .HasColumnName("OTP")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Response).HasMaxLength(1024);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Token).HasMaxLength(250);

                entity.Property(e => e.Url).HasMaxLength(250);
            });

            modelBuilder.Entity<LogTrackingStopPoin>(entity =>
            {
                entity.ToTable("LogTrackingStopPoin", "rp");

                entity.Property(e => e.CityName).HasMaxLength(1000);

                entity.Property(e => e.DistrictName).HasMaxLength(1000);

                entity.Property(e => e.FormatedAddress).HasMaxLength(1000);

                entity.Property(e => e.FromTime).HasColumnType("datetime");

                entity.Property(e => e.PoinLng).HasColumnType("decimal(18, 9)");

                entity.Property(e => e.PointLat).HasColumnType("decimal(18, 9)");

                entity.Property(e => e.StreetName).HasMaxLength(1000);

                entity.Property(e => e.StreetNumber).HasMaxLength(1000);

                entity.Property(e => e.TotalTime).HasComment("tổng thời gian dừng: phút");

                entity.Property(e => e.TrackingType).HasComment("1: điểm dừng, 2 : di chuyển");

                entity.Property(e => e.WardName).HasMaxLength(1000);
            });

            modelBuilder.Entity<LogWorking>(entity =>
            {
                entity.Property(e => e.CreateAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<ManagerAreaHub>(entity =>
            {
                entity.HasKey(e => new { e.ShopId, e.DistrictId, e.WardId, e.ProductId });

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Shop)
                    .WithMany(p => p.ManagerAreaHub)
                    .HasForeignKey(d => d.ShopId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ManagerAreaHub_Shop");
            });

            modelBuilder.Entity<Material>(entity =>
            {
                entity.ToTable("Material", "rp");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.MaterialCode).HasMaxLength(100);

                entity.Property(e => e.MaterialName).HasMaxLength(500);
            });

            modelBuilder.Entity<Module>(entity =>
            {
                entity.Property(e => e.Action)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Controller)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.Icon).HasMaxLength(200);

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Path)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_Module_Module");
            });

            modelBuilder.Entity<ModulePermission>(entity =>
            {
                entity.HasKey(e => new { e.ModuleId, e.PermissionId })
                    .HasName("PK_ModulePermission_1");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.ModulePermission)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ModulePermission_Permission");
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.Property(e => e.Action)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Message).HasMaxLength(500);

                entity.Property(e => e.PushTime).HasColumnType("datetime");

                entity.Property(e => e.Read).IsUnicode(false);

                entity.Property(e => e.Topic)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<OrderSource>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<OrderSourceDetail>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<PermissionIp>(entity =>
            {
                entity.ToTable("PermissionIP");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IpAddress).HasMaxLength(50);

                entity.Property(e => e.ServerName).HasMaxLength(50);
            });

            modelBuilder.Entity<Pipeline>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<PipelineState>(entity =>
            {
                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasMaxLength(200);
            });

            modelBuilder.Entity<PlatformType>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.IsEnable).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.ShortName).HasMaxLength(500);

                entity.HasOne(d => d.BrandProduct)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.BrandProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_BrandProduct");
            });

            modelBuilder.Entity<ProductAppraiser>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Latitude).HasColumnType("decimal(18, 9)");

                entity.Property(e => e.Longitude).HasColumnType("decimal(18, 9)");
            });

            modelBuilder.Entity<ProductPercentReduction>(entity =>
            {
                entity.HasOne(d => d.BrandProduct)
                    .WithMany(p => p.ProductPercentReduction)
                    .HasForeignKey(d => d.BrandProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductPercentReduction_BrandProduct");
            });

            modelBuilder.Entity<ProductReview>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<ProductReviewDetail>(entity =>
            {
                entity.Property(e => e.PecentDiscount).HasColumnType("numeric(2, 1)");

                entity.HasOne(d => d.ProductCredit)
                    .WithMany(p => p.ProductReviewDetail)
                    .HasForeignKey(d => d.ProductCreditId)
                    .HasConstraintName("FK_ProductReviewDetail_Product");

                entity.HasOne(d => d.ProductReview)
                    .WithMany(p => p.ProductReviewDetail)
                    .HasForeignKey(d => d.ProductReviewId)
                    .HasConstraintName("FK_ProductReviewDetail_ProductReview");
            });

            modelBuilder.Entity<ProductReviewResult>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductReviewResult)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_ProductReviewResult_Product");

                entity.HasOne(d => d.ProductReview)
                    .WithMany(p => p.ProductReviewResult)
                    .HasForeignKey(d => d.ProductReviewId)
                    .HasConstraintName("FK_ProductReviewResult_ProductReview1");
            });

            modelBuilder.Entity<ProductReviewResultDetail>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ProductType>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<Property>(entity =>
            {
                entity.Property(e => e.DefaultValue).HasMaxLength(500);

                entity.Property(e => e.FieldName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LinkObject)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MultipleSelect).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Object)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Regex).HasMaxLength(500);

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Province>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<ReasonCancel>(entity =>
            {
                entity.Property(e => e.ReasonCode).HasMaxLength(50);
            });

            modelBuilder.Entity<RelativeFamily>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<ReportBehaviorHub>(entity =>
            {
                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ShopName).HasMaxLength(255);
            });

            modelBuilder.Entity<ReportBehaviorTelesale>(entity =>
            {
                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(255);

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<ReportConvertHub>(entity =>
            {
                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.ShopName).HasMaxLength(255);
            });

            modelBuilder.Entity<ReportConvertTelesale>(entity =>
            {
                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.ForDate).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(255);

                entity.Property(e => e.LastUpdate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ReportData>(entity =>
            {
                entity.ToTable("ReportData", "rp");

                entity.HasComment("Đơn về là vào đây, chỉ insert ko update");

                entity.Property(e => e.BeforeCreateDate)
                    .HasColumnName("beforeCreateDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.BeforeLoanCreditId).HasColumnName("beforeLoanCreditId");

                entity.Property(e => e.BeforeStatus).HasColumnName("beforeStatus");

                entity.Property(e => e.BeforeUtmCampaign)
                    .HasColumnName("before_utm_campaign")
                    .HasMaxLength(500);

                entity.Property(e => e.BeforeUtmContent)
                    .HasColumnName("before_utm_content")
                    .HasMaxLength(500);

                entity.Property(e => e.BeforeUtmMedium)
                    .HasColumnName("before_utm_medium")
                    .HasMaxLength(500);

                entity.Property(e => e.BeforeUtmSource)
                    .HasColumnName("before_utm_source")
                    .HasMaxLength(500)
                    .HasComment("Before: dữ liệu của khách hàng theo số đt này của đơn về xa nhất cách đơn hiện tại 30 ngày");

                entity.Property(e => e.BeforeUtmTerm)
                    .HasColumnName("before_utm_term")
                    .HasMaxLength(500);

                entity.Property(e => e.CityId).HasColumnName("cityId");

                entity.Property(e => e.CityName)
                    .HasColumnName("cityName")
                    .HasMaxLength(500);

                entity.Property(e => e.CompanyAddress)
                    .HasColumnName("companyAddress")
                    .HasMaxLength(1000);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustomerCreditId).HasColumnName("customerCreditId");

                entity.Property(e => e.CustomerFullName)
                    .HasColumnName("customerFullName")
                    .HasMaxLength(500);

                entity.Property(e => e.DistrictId).HasColumnName("districtId");

                entity.Property(e => e.DistrictName)
                    .HasColumnName("districtName")
                    .HasMaxLength(500);

                entity.Property(e => e.DomainName)
                    .HasColumnName("domainName")
                    .HasMaxLength(500);

                entity.Property(e => e.FromDate)
                    .HasColumnName("fromDate")
                    .HasColumnType("date");

                entity.Property(e => e.HomeAddress)
                    .HasColumnName("homeAddress")
                    .HasMaxLength(1000);

                entity.Property(e => e.IdCard)
                    .HasColumnName("idCard")
                    .HasMaxLength(100);             

                entity.Property(e => e.LoanBriefId).HasColumnName("loanBriefId");

                entity.Property(e => e.LoanTime).HasColumnName("loanTime");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.PlatformType).HasColumnName("platformType");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ProductName)
                    .HasColumnName("productName")
                    .HasMaxLength(500)
                    .HasComment("tên gói sản phẩm lấy theo typeId của bảng tblLoancredit");

                entity.Property(e => e.ReMarketing)
                    .HasColumnName("reMarketing")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ReMarketingLoanCreditId)
                    .HasColumnName("reMarketingLoanCreditId")
                    .HasComment("Đơn vay được Remarketing từ đơn nào");

                entity.Property(e => e.ShopId).HasColumnName("shopId");

                entity.Property(e => e.ShopName)
                    .HasColumnName("shopName")
                    .HasMaxLength(500);

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.SupportId).HasColumnName("supportID");

                entity.Property(e => e.SupportLastFullName)
                    .HasColumnName("supportLastFullName")
                    .HasMaxLength(250);

                entity.Property(e => e.SupportLastId).HasColumnName("supportLastID");

                entity.Property(e => e.Tid)
                    .HasColumnName("tid")
                    .HasMaxLength(250);

                entity.Property(e => e.TotalMoney).HasColumnName("totalMoney");

                entity.Property(e => e.TypeProduct)
                    .HasColumnName("typeProduct")
                    .HasComment("Loại gói sản phẩm cao hay thấp");

                entity.Property(e => e.UtmCampaign)
                    .HasColumnName("utm_campaign")
                    .HasMaxLength(500);

                entity.Property(e => e.UtmContent)
                    .HasColumnName("utm_content")
                    .HasMaxLength(500);

                entity.Property(e => e.UtmMedium)
                    .HasColumnName("utm_medium")
                    .HasMaxLength(500);

                entity.Property(e => e.UtmSource)
                    .HasColumnName("utm_source")
                    .HasMaxLength(500);

                entity.Property(e => e.UtmTerm)
                    .HasColumnName("utm_term")
                    .HasMaxLength(500);

                entity.Property(e => e.WardId).HasColumnName("wardId");
            });

            modelBuilder.Entity<ReportDataUpdating>(entity =>
            {
                entity.ToTable("ReportDataUpdating", "rp");

                entity.HasComment("báo cáo thời gian xử lý của các phòng ban, update liên tục 30p 1 lần những đơn chưa hủy hoặc chưa tất toán");

                entity.Property(e => e.CancelDateLoanCredit)
                    .HasColumnName("cancelDateLoanCredit")
                    .HasColumnType("datetime");

                entity.Property(e => e.CancelFullName)
                    .HasColumnName("cancelFullName")
                    .HasMaxLength(250);

                entity.Property(e => e.CancelGroupId).HasColumnName("cancelGroupId");

                entity.Property(e => e.CityId).HasColumnName("cityId");

                entity.Property(e => e.CompanyAddress)
                    .HasColumnName("companyAddress")
                    .HasMaxLength(1000);

                entity.Property(e => e.CoordinatorId).HasColumnName("coordinatorId");

                entity.Property(e => e.CounselorFullName)
                    .HasColumnName("counselorFullName")
                    .HasMaxLength(250);

                entity.Property(e => e.CounselorId).HasColumnName("counselorId");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeviceId)
                    .HasColumnName("deviceId")
                    .HasMaxLength(250);

                entity.Property(e => e.DisbursementDate)
                    .HasColumnName("disbursementDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.DistrictId).HasColumnName("districtId");

                entity.Property(e => e.HomeAddress)
                    .HasColumnName("homeAddress")
                    .HasMaxLength(1000);

                entity.Property(e => e.HubEmployeFullName)
                    .HasColumnName("hubEmployeFullName")
                    .HasMaxLength(250);

                entity.Property(e => e.HubEmployeId)
                    .HasColumnName("hubEmployeId")
                    .HasComment("Nhân viên cửa hàng phụ trách khoản vay");

                entity.Property(e => e.HubPushDate)
                    .HasColumnName("hubPushDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsFirstLoan).HasColumnName("isFirstLoan");

                entity.Property(e => e.IsQlf).HasColumnName("isQlf");

                entity.Property(e => e.IsVerifyOtpLdp).HasColumnName("isVerifyOtpLDP");

                entity.Property(e => e.ListCheckLeadQualify)
                    .HasColumnName("listCheckLeadQualify")
                    .HasMaxLength(250);

                entity.Property(e => e.LoanBriefId).HasColumnName("loanBriefId");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ProductName)
                    .HasColumnName("productName")
                    .HasMaxLength(50)
                    .HasComment("tên gói sản phẩm lấy theo typeId của bảng tblLoancredit");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.StatusOfDeviceId).HasColumnName("statusOfDeviceId");

                entity.Property(e => e.SupportId).HasColumnName("supportId");

                entity.Property(e => e.SupportLastFullName)
                    .HasColumnName("supportLastFullName")
                    .HasMaxLength(250);

                entity.Property(e => e.SupportLastId).HasColumnName("supportLastId");

                entity.Property(e => e.TdhsPushDate)
                    .HasColumnName("tdhsPushDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.TelesalePushDate)
                    .HasColumnName("telesalePushDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.TotalMinute).HasColumnName("totalMinute");

                entity.Property(e => e.TotalMinuteOfHub).HasColumnName("totalMinuteOfHub");

                entity.Property(e => e.TotalMinuteOfLander).HasColumnName("totalMinuteOfLander");

                entity.Property(e => e.TotalMinuteOfOther).HasColumnName("totalMinuteOfOther");

                entity.Property(e => e.TotalMinuteOfTdhs).HasColumnName("totalMinuteOfTdhs");

                entity.Property(e => e.TotalMinuteOfTelesale).HasColumnName("totalMinuteOfTelesale");

                entity.Property(e => e.TotalMinuteSubtracted).HasColumnName("totalMinuteSubtracted");

                entity.Property(e => e.TotalMinuteSubtractedOfHub).HasColumnName("totalMinuteSubtractedOfHub");

                entity.Property(e => e.TotalMinuteSubtractedOfLander).HasColumnName("totalMinuteSubtractedOfLander");

                entity.Property(e => e.TotalMinuteSubtractedOfOther).HasColumnName("totalMinuteSubtractedOfOther");

                entity.Property(e => e.TotalMinuteSubtractedOfTdhs).HasColumnName("totalMinuteSubtractedOfTdhs");

                entity.Property(e => e.TotalMinuteSubtractedOfTelesale).HasColumnName("totalMinuteSubtractedOfTelesale");

                entity.Property(e => e.TotalTimesReturnOfHub).HasColumnName("totalTimesReturnOfHub");

                entity.Property(e => e.TotalTimesReturnOfLander).HasColumnName("totalTimesReturnOfLander");

                entity.Property(e => e.TotalTimesReturnOfOther).HasColumnName("totalTimesReturnOfOther");

                entity.Property(e => e.TotalTimesReturnOfTdhs).HasColumnName("totalTimesReturnOfTdhs");

                entity.Property(e => e.TotalTimesReturnOfTelesale).HasColumnName("totalTimesReturnOfTelesale");

                entity.Property(e => e.UserIdCancel).HasColumnName("userIdCancel");

                entity.Property(e => e.WardId).HasColumnName("wardId");
            });

            modelBuilder.Entity<ReportDepartmentError>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.GroupUserError).HasComment("bộ phận dính lỗi");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.StaffErrorId).HasComment("UserId người mắc lỗi");

                entity.Property(e => e.StaffName)
                    .HasMaxLength(50)
                    .HasComment("Tên người mắc lỗi");

                entity.Property(e => e.UserName).HasMaxLength(50);
            });

            modelBuilder.Entity<ReportToken>(entity =>
            {
                entity.HasKey(e => e.TokenId);

                entity.ToTable("ReportToken", "rp");

                entity.Property(e => e.TokenId).HasColumnName("tokenId");

                entity.Property(e => e.ApplyFor)
                    .HasColumnName("applyFor")
                    .HasComment("cấp quyền cho báo cáo nào");

                entity.Property(e => e.DateEx)
                    .HasColumnName("dateEx")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(1000);

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.Token)
                    .HasColumnName("token")
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<Section>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(200);

                entity.HasOne(d => d.Pipeline)
                    .WithMany(p => p.Section)
                    .HasForeignKey(d => d.PipelineId)
                    .HasConstraintName("FK_Section_Pipeline");
            });

            modelBuilder.Entity<SectionAction>(entity =>
            {
                entity.Property(e => e.Value).HasMaxLength(500);

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.SectionAction)
                    .HasForeignKey(d => d.PropertyId)
                    .HasConstraintName("FK_SectionAction_Property");

                entity.HasOne(d => d.SectionDetail)
                    .WithMany(p => p.SectionAction)
                    .HasForeignKey(d => d.SectionDetailId)
                    .HasConstraintName("FK_SectionAction_SectionDetail");
            });

            modelBuilder.Entity<SectionApprove>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(200);

                entity.HasOne(d => d.Approve)
                    .WithMany(p => p.SectionApproveApprove)
                    .HasForeignKey(d => d.ApproveId)
                    .HasConstraintName("FK_SectionApprove_SectionDetail1");

                entity.HasOne(d => d.SectionDetail)
                    .WithMany(p => p.SectionApproveSectionDetail)
                    .HasForeignKey(d => d.SectionDetailId)
                    .HasConstraintName("FK_SectionApprove_SectionDetail");
            });

            modelBuilder.Entity<SectionCondition>(entity =>
            {
                entity.Property(e => e.Operator)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value).HasMaxLength(500);

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.SectionCondition)
                    .HasForeignKey(d => d.PropertyId)
                    .HasConstraintName("FK_SectionCondition_Property");

                entity.HasOne(d => d.SectionApprove)
                    .WithMany(p => p.SectionCondition)
                    .HasForeignKey(d => d.SectionApproveId)
                    .HasConstraintName("FK_SectionCondition_SectionApprove");
            });

            modelBuilder.Entity<SectionDetail>(entity =>
            {
                entity.Property(e => e.Mode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.HasOne(d => d.Disapprove)
                    .WithMany(p => p.InverseDisapprove)
                    .HasForeignKey(d => d.DisapproveId)
                    .HasConstraintName("FK_SectionDetail_SectionDetail1");

                entity.HasOne(d => d.Return)
                    .WithMany(p => p.InverseReturn)
                    .HasForeignKey(d => d.ReturnId)
                    .HasConstraintName("FK_SectionDetail_SectionDetail");

                entity.HasOne(d => d.Section)
                    .WithMany(p => p.SectionDetail)
                    .HasForeignKey(d => d.SectionId)
                    .HasConstraintName("FK_SectionDetail_Section");
            });

            modelBuilder.Entity<SectionGroup>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(200);

                entity.HasOne(d => d.InitialSection)
                    .WithMany(p => p.SectionGroup)
                    .HasForeignKey(d => d.InitialSectionId)
                    .HasConstraintName("FK_SectionGroup_Section");
            });

            modelBuilder.Entity<Shop>(entity =>
            {
                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email).HasMaxLength(250);

                entity.Property(e => e.InviteCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Lat)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Long)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Note).HasMaxLength(1000);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<TelesaleLoanbrief>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<TokenSmartDailer>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.DateExpiration).HasColumnType("datetime");

                entity.Property(e => e.Token)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Transactions>(entity =>
            {
                entity.Property(e => e.BankCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BankName).HasMaxLength(1024);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LenderPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Message).HasMaxLength(1024);

                entity.Property(e => e.ProcessByName).HasMaxLength(500);

                entity.Property(e => e.ProcessByUserNote).HasMaxLength(1024);

                entity.Property(e => e.TranReference)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.LoanBrief)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.LoanBriefId)
                    .HasConstraintName("FK_Transactions_LoanBrief");
            });

            modelBuilder.Entity<TypeOwnerShip>(entity =>
            {
                entity.Property(e => e.IsEnable).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.CiscoExtension).HasMaxLength(10);

                entity.Property(e => e.CiscoPassword).HasMaxLength(100);

                entity.Property(e => e.CiscoUsername).HasMaxLength(100);

                entity.Property(e => e.Email).HasMaxLength(200);

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.Ipphone).HasColumnName("IPPhone");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastReceived).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PushTokenAndroid).HasMaxLength(500);

                entity.Property(e => e.PushTokenIos)
                    .HasColumnName("PushTokenIOS")
                    .HasMaxLength(500);

                entity.Property(e => e.UserIdSso).HasColumnName("UserIdSSO");

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK_User_Province");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_User_Company");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("FK_User_Group");
            });

            modelBuilder.Entity<UserActionPermission>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.UpdatetAt).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserActionPermission)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserActionPermission_User");
            });

            modelBuilder.Entity<UserCondition>(entity =>
            {
                entity.Property(e => e.CreatetAt).HasColumnType("datetime");

                entity.Property(e => e.Operator)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.Value).HasMaxLength(500);

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.UserCondition)
                    .HasForeignKey(d => d.PropertyId)
                    .HasConstraintName("FK_UserCondition_Property");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserCondition)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserCondition_User");
            });

            modelBuilder.Entity<UserModule>(entity =>
            {
                entity.HasOne(d => d.Module)
                    .WithMany(p => p.UserModule)
                    .HasForeignKey(d => d.ModuleId)
                    .HasConstraintName("FK_UserModule_Module1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserModule)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserModule_User1");
            });

            modelBuilder.Entity<UserShop>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.HasOne(d => d.Shop)
                    .WithMany(p => p.UserShop)
                    .HasForeignKey(d => d.ShopId)
                    .HasConstraintName("FK_UserShop_Shop");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserShop)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserShop_User");
            });

            modelBuilder.Entity<UtmSource>(entity =>
            {
                entity.Property(e => e.UtmSourceName).HasMaxLength(200);

                entity.HasOne(d => d.GroupSource)
                    .WithMany(p => p.UtmSource)
                    .HasForeignKey(d => d.GroupSourceId)
                    .HasConstraintName("FK_UtmSource_GroupSource");
            });

            modelBuilder.Entity<Ward>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<ZaloDinhVi>(entity =>
            {
                entity.ToTable("ZaloDinhVi", "rp");

                entity.HasComment("danh sách zalo user nhận tin cảnh báo");

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.RecipientId)
                    .HasMaxLength(50)
                    .HasComment("zalo userID");

                entity.Property(e => e.RecipientName).HasMaxLength(500);

                entity.Property(e => e.UserId).HasComment("AG userId");
            });

            modelBuilder.Entity<ZaloLoanCredit>(entity =>
            {
                entity.ToTable("ZaloLoanCredit", "rp");

                entity.HasComment("Danh sách đơn tạm dừng cảnh báo");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
