﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanBrief
    {
        public LoanBrief()
        {
            LoanAction = new HashSet<LoanAction>();
            LoanBriefDocument = new HashSet<LoanBriefDocument>();
            LoanBriefHistory = new HashSet<LoanBriefHistory>();
            LoanBriefNote = new HashSet<LoanBriefNote>();
            LoanBriefRelationship = new HashSet<LoanBriefRelationship>();
            LogLoanInfoAi = new HashSet<LogLoanInfoAi>();
            LogRequestAi = new HashSet<LogRequestAi>();
            Transactions = new HashSet<Transactions>();
        }

        public int LoanBriefId { get; set; }
        public int? ProductId { get; set; }
        public int? CustomerId { get; set; }
        public int? RateTypeId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public DateTime? Dob { get; set; }
        public int? Gender { get; set; }
        public string NationalCard { get; set; }
        public DateTime? NationalCardDate { get; set; }
        public string NationCardPlace { get; set; }
        public decimal? LoanAmount { get; set; }
        public decimal? LoanAmountFirst { get; set; }
        public decimal? LoanAmountExpertise { get; set; }
        public decimal? LoanAmountExpertiseLast { get; set; }
        public int? LoanTime { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public double? ConsultantRate { get; set; }
        public double? ServiceRate { get; set; }
        public double? LoanRate { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string UtmSource { get; set; }
        public string UtmMedium { get; set; }
        public string UtmCampaign { get; set; }
        public string UtmTerm { get; set; }
        public string UtmContent { get; set; }
        public int? ScoreCredit { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTimeOffset? UpdatedTime { get; set; }
        public int? Status { get; set; }
        public int? BoundTelesaleId { get; set; }
        public int? AffStatus { get; set; }
        public int? MecashId { get; set; }
        public int? ReceivingMoneyType { get; set; }
        public int? BankId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }
        public string BankAccountName { get; set; }
        public int? OrderSourceId { get; set; }
        public int? OrderSourceParentId { get; set; }
        public bool? IsHeadOffice { get; set; }
        public int? PlatformType { get; set; }
        public int? CurrentPipelineId { get; set; }
        public int? CurrentSectionId { get; set; }
        public int? CurrentSectionDetailId { get; set; }
        public int? PipelineState { get; set; }
        public bool? SectionApprove { get; set; }
        public int? TypeLoanBrief { get; set; }
        public int? Frequency { get; set; }
        public bool? IsCheckCic { get; set; }
        public bool? IsLocate { get; set; }
        public bool? IsTrackingLocation { get; set; }
        public bool? BuyInsurenceCustomer { get; set; }
        public int? NumberCall { get; set; }
        public int? CreateBy { get; set; }
        public int? LoanPurpose { get; set; }
        public string LoanBriefComment { get; set; }
        public int? ReasonCancel { get; set; }
        public int? ReMarketingLoanBriefId { get; set; }
        public bool? IsReMarketing { get; set; }
        public DateTime? LoanBriefCancelAt { get; set; }
        public int? LoanBriefCancelBy { get; set; }
        public DateTime? TelesalesPushAt { get; set; }
        public DateTime? ScheduleTime { get; set; }
        public int? HubId { get; set; }
        public int? HubEmployeeId { get; set; }
        public DateTime? HubPushAt { get; set; }
        public int? EvaluationHomeUserId { get; set; }
        public int? EvaluationHomeState { get; set; }
        public DateTime? EvaluationHomeTime { get; set; }
        public int? EvaluationCompanyUserId { get; set; }
        public int? EvaluationCompanyState { get; set; }
        public DateTime? EvaluationCompanyTime { get; set; }
        public int? CoordinatorUserId { get; set; }
        public int? ActionState { get; set; }
        public bool? AutomaticProcessed { get; set; }
        public int? LenderId { get; set; }
        public DateTime? CoordinatorPushAt { get; set; }
        public bool? IsUploadState { get; set; }
        public string DeviceId { get; set; }
        public int? DeviceStatus { get; set; }
        public DateTime? LastSendRequestActive { get; set; }
        public string ResultCic { get; set; }
        public int? CheckCic { get; set; }
        public string ContractGinno { get; set; }
        public int? TypeInsurence { get; set; }
        public int? InProcess { get; set; }
        public bool? IsLock { get; set; }
        public bool? AddedToQueue { get; set; }
        public int? HomeNetwork { get; set; }
        public int? PriorityContract { get; set; }
        public DateTime? BeginStartTime { get; set; }
        public string LabelRequestLocation { get; set; }
        public string LabelResponseLocation { get; set; }
        public string DomainName { get; set; }
        public string AffSid { get; set; }
        public string Tid { get; set; }
        public int? IsVerifyOtpLdp { get; set; }
        public string AffCode { get; set; }
        public string Email { get; set; }
        public int? HubTypeId { get; set; }
        public string ResultRuleCheck { get; set; }
        public string ResultLocation { get; set; }
        public int? Version { get; set; }
        public string AffPub { get; set; }
        public string AffSource { get; set; }
        public decimal? RateMoney { get; set; }
        public double? RatePercent { get; set; }
        public bool? IsReborrow { get; set; }
        public int? LmsLoanId { get; set; }
        public DateTime? DisbursementAt { get; set; }
        public DateTime? NextDate { get; set; }
        public int? TypeDisbursement { get; set; }
        public int? CountCall { get; set; }
        public DateTime? FirstProcessingTime { get; set; }
        public DateTime? LenderReceivedDate { get; set; }
        public DateTime? FirstTimeHubFeedBack { get; set; }
        public short? SynchorizeFinance { get; set; }
        public int? FinanceLoanCreditId { get; set; }
        public int? ValueCheckQualify { get; set; }
        public decimal? LeadScore { get; set; }
        public string LabelScore { get; set; }
        public decimal? FeeInsuranceOfCustomer { get; set; }
        public decimal? FeeInsuranceOfLender { get; set; }
        public int? ScoreRange { get; set; }    
        public DateTime? ActivedDeviceAt { get; set; }
        public decimal? LoanAmountExpertiseAi { get; set; }
        public int? CodeId { get; set; }
        public string LenderName { get; set; }

        public virtual Bank Bank { get; set; }
        public virtual User BoundTelesale { get; set; }
        public virtual User CoordinatorUser { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual District District { get; set; }
        public virtual User EvaluationCompanyUser { get; set; }
        public virtual User EvaluationHomeUser { get; set; }
        public virtual Shop Hub { get; set; }
        public virtual User HubEmployee { get; set; }
        public virtual LoanProduct Product { get; set; }
        public virtual Province Province { get; set; }
        public virtual LoanRateType RateType { get; set; }
        public virtual ReasonCancel ReasonCancelNavigation { get; set; }
        public virtual Ward Ward { get; set; }
        public virtual LoanBriefCompany LoanBriefCompany { get; set; }
        public virtual LoanBriefHousehold LoanBriefHousehold { get; set; }
        public virtual LoanBriefJob LoanBriefJob { get; set; }
        public virtual LoanBriefProperty LoanBriefProperty { get; set; }
        public virtual LoanBriefResident LoanBriefResident { get; set; }
        public virtual ICollection<LoanAction> LoanAction { get; set; }
        public virtual ICollection<LoanBriefDocument> LoanBriefDocument { get; set; }
        public virtual ICollection<LoanBriefHistory> LoanBriefHistory { get; set; }
        public virtual ICollection<LoanBriefNote> LoanBriefNote { get; set; }
        public virtual ICollection<LoanBriefRelationship> LoanBriefRelationship { get; set; }
        public virtual ICollection<LogLoanInfoAi> LogLoanInfoAi { get; set; }
        public virtual ICollection<LogRequestAi> LogRequestAi { get; set; }
        public virtual ICollection<Transactions> Transactions { get; set; }
    }
}
