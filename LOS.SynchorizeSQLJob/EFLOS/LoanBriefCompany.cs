﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanBriefCompany
    {
        public int LoanBriefId { get; set; }
        public string CompanyName { get; set; }
        public string CareerBusiness { get; set; }
        public DateTime? BusinessCertificationDate { get; set; }
        public string BusinessCertificationAddress { get; set; }
        public string HeadOffice { get; set; }
        public string Address { get; set; }
        public string CompanyShareholder { get; set; }
        public DateTime? BirthdayShareholder { get; set; }
        public string CardNumberShareholder { get; set; }
        public DateTime? CardNumberShareholderDate { get; set; }
        public string PlaceOfBirthShareholder { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public virtual LoanBrief LoanBrief { get; set; }
    }
}
