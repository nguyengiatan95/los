﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanBriefFiles
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public DateTime? CreateAt { get; set; }
        public string FilePath { get; set; }
        public int? UserId { get; set; }
        public int? Status { get; set; }
        public int? TypeId { get; set; }
        public int? S3status { get; set; }
        public string LinkImgOfPartner { get; set; }
        public int? TypeFile { get; set; }
        public int? Synchronize { get; set; }
        public string DomainOfPartner { get; set; }
        public int? IsDeleted { get; set; }
        public int? DeletedBy { get; set; }
        public DateTime? ModifyAt { get; set; }
        public int? MecashId { get; set; }

        public virtual DocumentType Type { get; set; }
    }
}
