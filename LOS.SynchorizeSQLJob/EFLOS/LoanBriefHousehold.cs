﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanBriefHousehold
    {
        public int LoanBriefHouseholdId { get; set; }
        public string Address { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string AppartmentNumber { get; set; }
        public string Lane { get; set; }
        public string Block { get; set; }
        public int? NoOfFamilyMembers { get; set; }
        public int? Status { get; set; }
        public int? Ownership { get; set; }

        public virtual District District { get; set; }
        public virtual LoanBrief LoanBriefHouseholdNavigation { get; set; }
        public virtual Province Province { get; set; }
        public virtual Ward Ward { get; set; }
    }
}
