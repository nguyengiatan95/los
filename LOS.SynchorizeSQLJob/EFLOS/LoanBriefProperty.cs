﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanBriefProperty
    {
        public int LoanBriefPropertyId { get; set; }
        public int? PropertyType { get; set; }
        public int? BrandId { get; set; }
        public int? ProductId { get; set; }
        public int? Ownership { get; set; }
        public bool? DocumentAvailable { get; set; }
        public int? Name { get; set; }
        public int? Age { get; set; }
        public DateTimeOffset CreatedTime { get; set; }
        public DateTimeOffset? UpdatedTime { get; set; }
        public string CarManufacturer { get; set; }
        public string CarName { get; set; }

        public virtual BrandProduct Brand { get; set; }
        public virtual LoanBrief LoanBriefPropertyNavigation { get; set; }
        public virtual Product Product { get; set; }
    }
}
