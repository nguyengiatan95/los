﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanBriefRelationship
    {
        public int Id { get; set; }
        public int? RelationshipType { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int? LoanBriefId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CardNumber { get; set; }
        public int? RefPhoneDurationRate { get; set; }
        public int? RefPhoneCallRate { get; set; }

        public virtual LoanBrief LoanBrief { get; set; }
        public virtual RelativeFamily RelationshipTypeNavigation { get; set; }
    }
}
