﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanBriefResident
    {
        public int LoanBriefResidentId { get; set; }
        public string Address { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string AppartmentNumber { get; set; }
        public string Lane { get; set; }
        public string Block { get; set; }
        public int? ResidentType { get; set; }
        public int? LivingTime { get; set; }
        public int? Ownership { get; set; }
        public int? LivingWith { get; set; }
        public string Direction { get; set; }
        public string AddressGoogleMap { get; set; }
        public string AddressLatLng { get; set; }

        public virtual District District { get; set; }
        public virtual LoanBrief LoanBriefResidentNavigation { get; set; }
        public virtual Province Province { get; set; }
        public virtual Ward Ward { get; set; }
    }
}
