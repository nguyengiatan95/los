﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanFlow
    {
        public int LoanFlowId { get; set; }
        public int? Name { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTimeOffset? UpdatedTime { get; set; }
        public int? Status { get; set; }
        public int? Priority { get; set; }
        public int? SuccessActionId { get; set; }
        public int? FailActionId { get; set; }
    }
}
