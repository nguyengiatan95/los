﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanProduct
    {
        public LoanProduct()
        {
            LoanBrief = new HashSet<LoanBrief>();
        }

        public int LoanProductId { get; set; }
        public string Name { get; set; }
        public int? Status { get; set; }
        public double? TotalRate { get; set; }
        public double? ConsultantRate { get; set; }
        public double? ServiceRate { get; set; }
        public double? LoanRate { get; set; }
        public decimal? MaximumMoney { get; set; }
        public int? PlatformProductId { get; set; }
        public int? TypeProductId { get; set; }
        public int? Priority { get; set; }
        public int? MecashId { get; set; }

        public virtual ICollection<LoanBrief> LoanBrief { get; set; }
    }
}
