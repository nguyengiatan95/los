﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanRateType
    {
        public LoanRateType()
        {
            LoanBrief = new HashSet<LoanBrief>();
        }

        public int LoanRateTypeId { get; set; }
        public string RateType { get; set; }
        public int? Priority { get; set; }

        public virtual ICollection<LoanBrief> LoanBrief { get; set; }
    }
}
