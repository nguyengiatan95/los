﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanStatus
    {
        public int LoanStatusId { get; set; }
        public int? Status { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? MecashStatus { get; set; }
    }
}
