﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanbriefLender
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public int? LenderId { get; set; }
        public string LenderName { get; set; }
        public string FullName { get; set; }
        public string NationalCard { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
