﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LoanbriefScript
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string ScriptView { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
