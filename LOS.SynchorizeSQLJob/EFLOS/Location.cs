﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class Location
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int? GroupId { get; set; }
        public int? LoanbriefId { get; set; }
        public int? LoanbriefNoteId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string Device { get; set; }
    }
}
