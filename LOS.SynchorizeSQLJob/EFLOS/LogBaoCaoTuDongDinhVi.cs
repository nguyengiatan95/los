﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LogBaoCaoTuDongDinhVi
    {
        public int Id { get; set; }
        public string ReceiveMessage { get; set; }
        public string SentMessage { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? LoanCreditId { get; set; }
        public string ZaloRecipientId { get; set; }
        public string ZaloSenderId { get; set; }
    }
}
