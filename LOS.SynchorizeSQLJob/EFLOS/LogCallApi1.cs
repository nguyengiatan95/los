﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LogCallApi1
    {
        public int Id { get; set; }
        public int? ActionCallApi { get; set; }
        public string NameActionCallApi { get; set; }
        public string InputStringCallApi { get; set; }
        public string LinkCallApi { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateOn { get; set; }
        public DateTime? ModifyOn { get; set; }
        public int? LoanCreditId { get; set; }
        public int? LoanId { get; set; }
        public string ResultStringCallApi { get; set; }
    }
}
