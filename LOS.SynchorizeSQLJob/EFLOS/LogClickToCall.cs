﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LogClickToCall
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public int? UserId { get; set; }
        public string PhoneOfCustomer { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
