﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LogLatLngHomeCompany
    {
        public int LogLatLngHomeCompanyId { get; set; }
        public int? LoanCreditId { get; set; }
        public int? CustomerId { get; set; }
        public decimal? HomeLat { get; set; }
        public decimal? HomeLng { get; set; }
        public decimal? CompanyLat { get; set; }
        public decimal? CompanyLng { get; set; }
    }
}
