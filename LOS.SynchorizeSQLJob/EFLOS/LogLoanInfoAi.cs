﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LogLoanInfoAi
    {
        public int Id { get; set; }
        public int? ServiceType { get; set; }
        public int? LoanbriefId { get; set; }
        public string RefCode { get; set; }
        public string Request { get; set; }
        public string ResponseRequest { get; set; }
        public string Response { get; set; }
        public string ResultFinal { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? Status { get; set; }
        public string Url { get; set; }
        public string Token { get; set; }
        public string HomeNetwok { get; set; }
        public int? IsExcuted { get; set; }
        public int? PushNextState { get; set; }
        public int? IsCancel { get; set; }

        public virtual LoanBrief Loanbrief { get; set; }
    }
}
