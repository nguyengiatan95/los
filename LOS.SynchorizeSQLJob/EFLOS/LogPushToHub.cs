﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LogPushToHub
    {
        public int Id { get; set; }
        public int? LoanbriefId { get; set; }
        public int? TelesaleId { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? ProductId { get; set; }
        public int? WardId { get; set; }
        public int? HubId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? CustomerId { get; set; }
    }
}
