﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class LogSendVoiceOtp
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string Otp { get; set; }
        public string Domain { get; set; }
        public string Token { get; set; }
        public string Url { get; set; }
        public string Status { get; set; }
        public string Response { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
