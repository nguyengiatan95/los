﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class ManagerAreaHub
    {
        public int ShopId { get; set; }
        public int DistrictId { get; set; }
        public int WardId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? ProvinceId { get; set; }
        public int ProductId { get; set; }

        public virtual Shop Shop { get; set; }
    }
}
