﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class OrderSource
    {
        public int OrderSourceId { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public int? Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedAt { get; set; }
        public string Condition { get; set; }
        public int? ApplyFor { get; set; }
        public int? IsScaned { get; set; }
        public int? MecashId { get; set; }
    }
}
