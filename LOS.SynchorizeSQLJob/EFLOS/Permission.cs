﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class Permission
    {
        public Permission()
        {
            GroupModulePermission = new HashSet<GroupModulePermission>();
            ModulePermission = new HashSet<ModulePermission>();
        }

        public int PermissionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public bool? DefaultValue { get; set; }
        public int? Priority { get; set; }

        public virtual ICollection<GroupModulePermission> GroupModulePermission { get; set; }
        public virtual ICollection<ModulePermission> ModulePermission { get; set; }
    }
}
