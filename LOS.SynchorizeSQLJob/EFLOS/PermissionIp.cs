﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class PermissionIp
    {
        public int Id { get; set; }
        public string IpAddress { get; set; }
        public string ServerName { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
