﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class Pipeline
    {
        public Pipeline()
        {
            Section = new HashSet<Section>();
        }

        public int PipelineId { get; set; }
        public string Name { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? ModifiedAt { get; set; }
        public int? CreatorId { get; set; }
        public int? Status { get; set; }
        public int? Priority { get; set; }

        public virtual ICollection<Section> Section { get; set; }
    }
}
