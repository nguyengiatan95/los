﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class PipelineHistory
    {
        public int PipelineHistoryId { get; set; }
        public int? LoanBriefId { get; set; }
        public int? PipelineId { get; set; }
        public int? BeginSectionId { get; set; }
        public int? BeginSectionDetailId { get; set; }
        public int? EndSectionId { get; set; }
        public int? EndSectionDetailId { get; set; }
        public string Data { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? ActionState { get; set; }
        public DateTimeOffset? BeginTime { get; set; }
        public DateTimeOffset? EndTime { get; set; }
        public int? BeginStatus { get; set; }
        public int? EndStatus { get; set; }
        public int? EndPipelineState { get; set; }
        public int? ExecuteTime { get; set; }
    }
}
