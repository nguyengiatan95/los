﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class PipelineState
    {
        public int PipelineStateId { get; set; }
        public int? State { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
