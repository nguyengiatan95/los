﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class Product
    {
        public Product()
        {
            LoanBriefProperty = new HashSet<LoanBriefProperty>();
            ProductReviewDetail = new HashSet<ProductReviewDetail>();
            ProductReviewResult = new HashSet<ProductReviewResult>();
        }

        public int Id { get; set; }
        public int BrandProductId { get; set; }
        public int ProductTypeId { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public long? Price { get; set; }
        public int? Year { get; set; }
        public int? BrakeType { get; set; }
        public int? RimType { get; set; }
        public string ShortName { get; set; }

        public virtual BrandProduct BrandProduct { get; set; }
        public virtual ICollection<LoanBriefProperty> LoanBriefProperty { get; set; }
        public virtual ICollection<ProductReviewDetail> ProductReviewDetail { get; set; }
        public virtual ICollection<ProductReviewResult> ProductReviewResult { get; set; }
    }
}
