﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class ProductReviewDetail
    {
        public int Id { get; set; }
        public int? ProductCreditId { get; set; }
        public int? ProductReviewId { get; set; }
        public long? MoneyDiscount { get; set; }
        public decimal? PecentDiscount { get; set; }

        public virtual Product ProductCredit { get; set; }
        public virtual ProductReview ProductReview { get; set; }
    }
}
