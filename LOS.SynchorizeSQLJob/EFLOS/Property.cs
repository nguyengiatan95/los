﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class Property
    {
        public Property()
        {
            GroupCondition = new HashSet<GroupCondition>();
            SectionAction = new HashSet<SectionAction>();
            SectionCondition = new HashSet<SectionCondition>();
            UserCondition = new HashSet<UserCondition>();
        }

        public int PropertyId { get; set; }
        public string Name { get; set; }
        public string Object { get; set; }
        public string FieldName { get; set; }
        public string DefaultValue { get; set; }
        public string Type { get; set; }
        public string LinkObject { get; set; }
        public int? Priority { get; set; }
        public int? Status { get; set; }
        public string Regex { get; set; }
        public bool? MultipleSelect { get; set; }

        public virtual ICollection<GroupCondition> GroupCondition { get; set; }
        public virtual ICollection<SectionAction> SectionAction { get; set; }
        public virtual ICollection<SectionCondition> SectionCondition { get; set; }
        public virtual ICollection<UserCondition> UserCondition { get; set; }
    }
}
