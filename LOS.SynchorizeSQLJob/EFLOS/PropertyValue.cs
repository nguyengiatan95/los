﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class PropertyValue
    {
        public int PropertyValueId { get; set; }
        public int? PropertyId { get; set; }
        public string Value { get; set; }

        public virtual Property Property { get; set; }
    }
}
