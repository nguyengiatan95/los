﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class ReasonCancel
    {
        public ReasonCancel()
        {
            LoanBrief = new HashSet<LoanBrief>();
        }

        public int Id { get; set; }
        public string Reason { get; set; }
        public string ReasonEng { get; set; }
        public int? Type { get; set; }
        public int? IsEnable { get; set; }
        public int? Sort { get; set; }
        public int? ParentId { get; set; }
        public string ReasonCode { get; set; }

        public virtual ICollection<LoanBrief> LoanBrief { get; set; }
    }
}
