﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class ReportBehaviorTelesale
    {
        public int Id { get; set; }
        public DateTime? ForDate { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public int? TotalForm { get; set; }
        public int? TotalFormUnder { get; set; }
        public int? TotalFormHigher { get; set; }
        public int? TotalNoProcess { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
