﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class ReportConvertTelesale
    {
        public long Id { get; set; }
        public DateTime? ForDate { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? NumberRegistration { get; set; }
        public int? NumberPushToHub { get; set; }
        public int? NumberDisbursement { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
