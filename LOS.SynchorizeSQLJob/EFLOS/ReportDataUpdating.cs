﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class ReportDataUpdating
    {
        public int ReportDataUpdatingId { get; set; }
        public int? LoanBriefId { get; set; }
        public int? Status { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public string HomeAddress { get; set; }
        public string CompanyAddress { get; set; }
        public int? SupportId { get; set; }
        public int? SupportLastId { get; set; }
        public string SupportLastFullName { get; set; }
        public int? CounselorId { get; set; }
        public int? CoordinatorId { get; set; }
        public int? HubEmployeId { get; set; }
        public string HubEmployeFullName { get; set; }
        public int? UserIdCancel { get; set; }
        public int? CancelGroupId { get; set; }
        public string CancelFullName { get; set; }
        public DateTime? TelesalePushDate { get; set; }
        public DateTime? HubPushDate { get; set; }
        public DateTime? TdhsPushDate { get; set; }
        public DateTime? DisbursementDate { get; set; }
        public DateTime? CancelDateLoanCredit { get; set; }
        public int? TotalMinuteOfTelesale { get; set; }
        public int? TotalMinuteOfHub { get; set; }
        public int? TotalMinuteOfTdhs { get; set; }
        public int? TotalMinuteOfOther { get; set; }
        public int? TotalMinuteOfLander { get; set; }
        public int? TotalMinuteSubtractedOfTelesale { get; set; }
        public int? TotalMinuteSubtractedOfHub { get; set; }
        public int? TotalMinuteSubtractedOfTdhs { get; set; }
        public int? TotalMinuteSubtractedOfOther { get; set; }
        public int? TotalMinuteSubtractedOfLander { get; set; }
        public int? TotalTimesReturnOfTelesale { get; set; }
        public int? TotalTimesReturnOfHub { get; set; }
        public int? TotalTimesReturnOfTdhs { get; set; }
        public int? TotalTimesReturnOfOther { get; set; }
        public int? TotalTimesReturnOfLander { get; set; }
        public int? TotalMinute { get; set; }
        public int? TotalMinuteSubtracted { get; set; }
        public bool? IsFirstLoan { get; set; }
        public string CounselorFullName { get; set; }
        public string DeviceId { get; set; }
        public int? StatusOfDeviceId { get; set; }
        public short? IsVerifyOtpLdp { get; set; }
        public string ListCheckLeadQualify { get; set; }
        public int? IsQlf { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
