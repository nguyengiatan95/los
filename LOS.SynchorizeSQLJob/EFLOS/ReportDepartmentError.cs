﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class ReportDepartmentError
    {
        public int Id { get; set; }
        public int? LoanBriefId { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public int? ErrorId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string Note { get; set; }
        public int? StaffErrorId { get; set; }
        public string StaffName { get; set; }
        public int? GroupUserError { get; set; }
    }
}
