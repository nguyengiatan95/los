﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class Section
    {
        public Section()
        {
            SectionDetail = new HashSet<SectionDetail>();
            SectionGroup = new HashSet<SectionGroup>();
        }

        public int SectionId { get; set; }
        public int? PipelineId { get; set; }
        public string Name { get; set; }
        public int? SectionGroupId { get; set; }
        public int? Order { get; set; }
        public bool? IsShow { get; set; }
        public int? Status { get; set; }

        public virtual Pipeline Pipeline { get; set; }
        public virtual ICollection<SectionDetail> SectionDetail { get; set; }
        public virtual ICollection<SectionGroup> SectionGroup { get; set; }
    }
}
