﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class SectionApprove
    {
        public SectionApprove()
        {
            SectionCondition = new HashSet<SectionCondition>();
        }

        public int SectionApproveId { get; set; }
        public int? SectionDetailId { get; set; }
        public string Name { get; set; }
        public int? ApproveId { get; set; }

        public virtual SectionDetail Approve { get; set; }
        public virtual SectionDetail SectionDetail { get; set; }
        public virtual ICollection<SectionCondition> SectionCondition { get; set; }
    }
}
