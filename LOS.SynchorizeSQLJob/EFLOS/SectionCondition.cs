﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class SectionCondition
    {
        public int SectionConditionId { get; set; }
        public int? SectionApproveId { get; set; }
        public int? PropertyId { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }

        public virtual Property Property { get; set; }
        public virtual SectionApprove SectionApprove { get; set; }
    }
}
