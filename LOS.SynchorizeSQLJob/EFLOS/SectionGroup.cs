﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class SectionGroup
    {
        public int SectionGroupId { get; set; }
        public string Name { get; set; }
        public int? InitialSectionId { get; set; }

        public virtual Section InitialSection { get; set; }
    }
}
