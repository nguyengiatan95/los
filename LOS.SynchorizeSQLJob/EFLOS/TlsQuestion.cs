﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class TlsQuestion
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public int TypeOfQuestion { get; set; }
        public string JsonOptions { get; set; }
        public string FieldMapping { get; set; }
    }
}
