﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class TlsScenario
    {
        public int Id { get; set; }
        public string ConfigName { get; set; }
        public string Description { get; set; }
        public DateTime CreationTime { get; set; }
        public bool IsActive { get; set; }
    }
}
