﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class TlsScenarioQuestion
    {
        public int Id { get; set; }
        public int ScenarioId { get; set; }
        public int QuestionId { get; set; }
        public int Priority { get; set; }
    }
}
