﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class TokenSmartDailer
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime? DateExpiration { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? UserId { get; set; }
    }
}
