﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class Transactions
    {
        public long Id { get; set; }
        public int? ReferLenderId { get; set; }
        public int? LoanBriefId { get; set; }
        public long? Money { get; set; }
        public int? LenderId { get; set; }
        public long? MoneyRefund { get; set; }
        public int? PaymentType { get; set; }
        public string TranReference { get; set; }
        public int? TranNumber { get; set; }
        public int? ResponseCode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Message { get; set; }
        public long? FeeMoney { get; set; }
        public long? MoneyBefore { get; set; }
        public long? MoneyAfter { get; set; }
        public int? PlatformType { get; set; }
        public long? ReferTransactionId { get; set; }
        public int? ProcessByUserId { get; set; }
        public string ProcessByName { get; set; }
        public int? TimaCommissionFeePercent { get; set; }
        public long? TimaCommissionFeeMoney { get; set; }
        public string ProcessByUserNote { get; set; }
        public int? ReferLenderCommissionFeePercent { get; set; }
        public long? ReferLenderCommissionFeeMoney { get; set; }
        public long? MoneySub { get; set; }
        public long? BonusMoneySub { get; set; }
        public int? BankId { get; set; }
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public int? Status { get; set; }
        public string LenderPhone { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? PromotionId { get; set; }
        public long? ReferTransactionPromotionId { get; set; }
        public int? SellByUserId { get; set; }

        public virtual LoanBrief LoanBrief { get; set; }
    }
}
