﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class TypeOwnerShip
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
    }
}
