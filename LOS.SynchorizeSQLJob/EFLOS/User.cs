﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class User
    {
        public User()
        {
            LoanAction = new HashSet<LoanAction>();
            LoanBriefBoundTelesale = new HashSet<LoanBrief>();
            LoanBriefCoordinatorUser = new HashSet<LoanBrief>();
            LoanBriefEvaluationCompanyUser = new HashSet<LoanBrief>();
            LoanBriefEvaluationHomeUser = new HashSet<LoanBrief>();
            LoanBriefHubEmployee = new HashSet<LoanBrief>();
            UserActionPermission = new HashSet<UserActionPermission>();
            UserCondition = new HashSet<UserCondition>();
            UserModule = new HashSet<UserModule>();
            UserShop = new HashSet<UserShop>();
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTimeOffset? UpdatedTime { get; set; }
        public int? CompanyId { get; set; }
        public int? DepartmentId { get; set; }
        public int? PositionId { get; set; }
        public int? GroupId { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public int? Ipphone { get; set; }
        public int? CoordinateRecord { get; set; }
        public bool? CoordinatorIsEnable { get; set; }
        public string PushTokenAndroid { get; set; }
        public string PushTokenIos { get; set; }
        public string CiscoUsername { get; set; }
        public string CiscoPassword { get; set; }
        public string CiscoExtension { get; set; }
        public int? UserIdSso { get; set; }
        public DateTime? LastReceived { get; set; }
        public int? TypeCallService { get; set; }
        public bool? TelesaleRecievedLoan { get; set; }
        public int? TypeRecievedProduct { get; set; }

        public virtual Province City { get; set; }
        public virtual Company Company { get; set; }
        public virtual Group Group { get; set; }
        public virtual ICollection<LoanAction> LoanAction { get; set; }
        public virtual ICollection<LoanBrief> LoanBriefBoundTelesale { get; set; }
        public virtual ICollection<LoanBrief> LoanBriefCoordinatorUser { get; set; }
        public virtual ICollection<LoanBrief> LoanBriefEvaluationCompanyUser { get; set; }
        public virtual ICollection<LoanBrief> LoanBriefEvaluationHomeUser { get; set; }
        public virtual ICollection<LoanBrief> LoanBriefHubEmployee { get; set; }
        public virtual ICollection<UserActionPermission> UserActionPermission { get; set; }
        public virtual ICollection<UserCondition> UserCondition { get; set; }
        public virtual ICollection<UserModule> UserModule { get; set; }
        public virtual ICollection<UserShop> UserShop { get; set; }
    }
}
