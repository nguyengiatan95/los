﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class UserCondition
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? PropertyId { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
        public DateTime? CreatetAt { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public virtual Property Property { get; set; }
        public virtual User User { get; set; }
    }
}
