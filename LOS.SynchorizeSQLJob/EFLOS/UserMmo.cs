﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class UserMmo
    {
        public UserMmo()
        {
            LoanBrief = new HashSet<LoanBrief>();
            UserReferenceUser = new HashSet<UserReference>();
            UserReferenceUserReferenceNavigation = new HashSet<UserReference>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? IsActive { get; set; }
        public string AffCode { get; set; }
        public string Address { get; set; }
        public string TaxCode { get; set; }
        public long? TotalAmountEarning { get; set; }
        public long? AvailableBalance { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? EndMatchingDate { get; set; }
        public DateTime? EndPaymentDate { get; set; }
        public double? PercentAmount { get; set; }
        public double? PercentRefer { get; set; }
        public string Cmt { get; set; }
        public string FacedeCmt { get; set; }
        public string BacksideCmt { get; set; }
        public string Giud { get; set; }

        public virtual ICollection<LoanBrief> LoanBrief { get; set; }
        public virtual ICollection<UserReference> UserReferenceUser { get; set; }
        public virtual ICollection<UserReference> UserReferenceUserReferenceNavigation { get; set; }
    }
}
