﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class UserProject
    {
        public int UserId { get; set; }
        public int ProjectId { get; set; }

        public virtual Project Project { get; set; }
        public virtual User ProjectNavigation { get; set; }
        public virtual User User { get; set; }
    }
}
