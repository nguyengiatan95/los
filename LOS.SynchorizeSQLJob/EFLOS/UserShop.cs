﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class UserShop
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? ShopId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public bool? Status { get; set; }

        public virtual Shop Shop { get; set; }
        public virtual User User { get; set; }
    }
}
