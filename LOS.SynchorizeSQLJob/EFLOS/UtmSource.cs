﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class UtmSource
    {
        public int UtmSourceId { get; set; }
        public string UtmSourceName { get; set; }
        public int? GroupSourceId { get; set; }

        public virtual GroupSource GroupSource { get; set; }
    }
}
