﻿using System;
using System.Collections.Generic;

namespace LOS.SynchorizeSQLJob.EFLOS
{
    public partial class ZaloLoanCredit
    {
        public int Id { get; set; }
        public int? LoanCreditId { get; set; }
        public bool? IsActive { get; set; }
    }
}
