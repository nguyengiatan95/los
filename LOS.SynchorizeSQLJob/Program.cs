﻿using LOS.Common.Helpers;
using LOS.SynchorizeSQLJob.EFAG;
using LOS.SynchorizeSQLJob.EFLOS;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.Async;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;
using LOS.SynchorizeSQLJob;
using System.Threading;

namespace LOS.synchronizingSQLJob
{
	class Program
	{
		static LOSContext los = new LOSContext();
		static MecashContext ag = new MecashContext();

		static void Main(string[] args)
		{
			Console.WriteLine("Begin Sync");
			#region dictionary
			//SyncProvince(); // Done
			//SyncRelativeFamily(); // Done
			//SyncDistrict(); // Done
			//SyncWard(); // Done
			//SyncLoanProduct(); // Done
			//SyncJob(); // Done
			//SyncBank(); // Done
			//SyncOrderSource(0); // Done
			//SyncOrderSourceDetail(0); // Done
			//SyncReasonCancel(); // Done
			//SyncDataCIC(21584); // Not Done
			//SyncBrandProduct(); // Done
			//SyncProduct(966); // Done
			//SyncProductType(); // Done
			//SyncProductPercentReduction(261); // Done
			//SyncProductAppraiser(200000); // Done
			#endregion
			#region User - Shop - Group
			Dictionary<int, int> groupMapping = new Dictionary<int, int>() {
				{ 1, 1 }, //admin
                { 9, 2 }, //telesale
                { 14, 48 }, //QLKV
                { 28, 44 },//Trưởng phòng GD - Hub
                { 23, 45 },//Chuyên viên kinh doanh - Hub
                { 20, 46 },//Leader Thẩm định
                { 12, 47 }//Thẩm định hồ sơ
            };
			//SyncUser(groupMapping,0); // Done
			//SyncShop(0); // Done
			//SyncUserShop(); // Done
			//SyncManagerAreaHub(); // Done
			#endregion
			#region Loan			
			//SyncCustomer(0); // Sync lại
			//SyncLoanBrief(999710); // 999710
			//SyncLoanBriefNext(620881); // 
			//////SyncLoanBrief(false, false, 920000);
			//SyncLocation(100000); // Done
			//SyncLoanCreditFiles(9000000); // Delete from 9165068
			//SyncLoanCreditComment(5000000); // Delete from 5726761
			//SyncLoanCreditCompany(0); // Done
			//SyncLoanBriefLender();
			//FixLoanBriefJob(0);
			//FixLoanBriefResident(0);
			//FixLoanBriefHousehold(0);
			//FixLoanBriefProperty(0);
			FixLoanBriefRelationship(0);
			#endregion
			#region Product
			//SyncProductReview(0); // Done
			//SyncProductReviewDetail(0); // Done
			//SyncProductReviewResult(700000); // Done , Delete from 771697
			//SyncProductReviewResultDetail(40000); // Done , Delete from 47884
			#endregion
			#region etc
			//SyncBlackList(8626); // No
			//SyncBlackListLog(486133); // No
			//SyncLogPushToHub(20000); // Done, Delete from 20000
			#endregion
			Console.ReadLine();
		}

		static void SyncProductType()
		{
			Console.WriteLine("Begin synchronizing ProductType");
			los.Database.BeginTransaction();
			los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.ProductType ON");
			var types = ag.TblProductType.Where(x => x.Id > 0).ToList();
			foreach (var type in types)
			{
				los.ProductType.Add(new ProductType()
				{
					Id = type.Id,
					Name = type.Name,
					Status = type.Status
				});
			}
			los.SaveChanges();
			los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.LogPushToHub OFF");
			los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing LogPushToHub");
		}

		static void SyncLogPushToHub(int startId)
		{
			Console.WriteLine("Begin synchronizing LogPushToHub");
			los.Database.BeginTransaction();
			los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.LogPushToHub ON");
			var logs = ag.TblLogLoanCreditPushHub.Where(x => x.Id > startId).ToList();
			foreach (var log in logs)
			{
				los.LogPushToHub.Add(new LogPushToHub()
				{
					CityId = log.CityId,
					CreatedAt = log.CreateDate,
					DistrictId = log.DistrictId,
					HubId = log.ShopId,
					Id = log.Id,
					LoanbriefId = log.LoanCreditId,
					ProductId = log.ProductId,
					TelesaleId = null,
					WardId = null,
					CustomerId = log.CustomerCreditId
				});
			}
			los.SaveChanges();
			los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.LogPushToHub OFF");
			los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing LogPushToHub");
		}

		static void SyncReasonCancel()
		{
			Console.WriteLine("Begin synchronizing Reason");
			los.Database.BeginTransaction();
			los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.ReasonCancel ON");
			var reasons = ag.TblReason.Where(x => x.Id > 0).ToList();
			int i = 1;
			foreach (var reason in reasons)
			{
				los.ReasonCancel.Add(new ReasonCancel()
				{
					Id = reason.Id,
					IsEnable = reason.IsEnable.HasValue ? (reason.IsEnable.Value ? 1 : 0) : 0,
					ParentId = reason.ReasonGroupId,
					Reason = reason.Reason,
					ReasonCode = reason.ReasonCode,
					Sort = reason.Sort,
					Type = reason.Type,
					ReasonEng = reason.ReasonEng
				});
				i++;
			}
			//los.SaveChanges();
			los.SaveChanges();
			los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.ReasonCancel OFF");
			los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing Reason");
		}

		static void SyncBrandProduct()
		{
			Console.WriteLine("Begin synchronizing BrandProduct");
			los.Database.BeginTransaction();
			los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.BrandProduct ON");
			var brands = ag.TblBrand.Where(x => x.Id > 0).ToList();
			int i = 1;
			foreach (var brand in brands)
			{
				if (!los.BrandProduct.Any(x => x.Id == brand.Id))
				{
					los.BrandProduct.SingleInsert(new BrandProduct()
					{
						Id = brand.Id,
						IsEnable = true,
						Name = brand.Name
					});
				}
				i++;
			}
			//los.SaveChanges();
			los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.BrandProduct OFF");
			los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing BrandProduct");
		}

		static void SyncProduct(int startId)
		{
			Console.WriteLine("Begin synchronizing Product");
			//los.Database.BeginTransaction();
			//los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Product ON");
			var products = ag.TblProduct.Where(x => x.Id > startId).ToList();
			foreach (var product in products)
			{
				los.Product.SingleInsert(new SynchorizeSQLJob.EFLOS.Product()
				{
					BrakeType = product.BrakeType,
					BrandProductId = product.IdBrand,
					FullName = product.FullName,
					Id = product.Id,
					Name = product.Name,
					Price = product.Price,
					ProductTypeId = product.IdType,
					RimType = product.RimType,
					ShortName = product.ShortName,
					Year = product.Year
				}, options => options.InsertKeepIdentity = true);
			}
			//los.SaveChanges();
			//los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Product OFF");
			//los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing Product");
		}

		static void SyncProductPercentReduction(int startId)
		{
			Console.WriteLine("Begin synchronizing ProductPercentReduction");
			//los.Database.BeginTransaction();
			//los.Database.ExecuteSqlCommand("truncate table dbo.ProductPercentReduction");
			//los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.ProductPercentReduction ON");
			var products = ag.TblProductPercentReduction.Where(x => x.Id > startId).ToList();
			foreach (var product in products)
			{
				los.ProductPercentReduction.SingleInsert(new ProductPercentReduction()
				{
					BrandProductId = product.IdBrand,
					CountYear = product.CountYear,
					Id = product.Id,
					PercentReduction = product.PercentReduction,
					ProductTypeId = product.IdType
				}, options => options.InsertKeepIdentity = true);
			}
			//los.SaveChanges();
			//los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.ProductPercentReduction OFF");
			//los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing ProductPercentReduction");
		}

		static void SyncProvince()
		{
			Console.WriteLine("Begin synchronizing Cities");
			los.Database.BeginTransaction();
			los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Province ON");
			var mCities = ag.TblCity.Where(x => x.Id > 0).ToList();
			int i = 1;
			foreach (var city in mCities)
			{
				los.Province.SingleInsert(new Province()
				{
					ProvinceId = city.Id,
					DomainId = city.DomainId,
					MecashId = city.Id,
					Name = city.Name,
					Priority = i,
					ProvinceCode = city.ProvinceCode,
					Type = city.TypeCity == "Thành Phố" ? 1 : 2
				});
				i++;
			}
			//los.SaveChanges();
			los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Province OFF");
			los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing Cities");
		}

		static void SyncRelativeFamily()
		{
			Console.WriteLine("Begin synchronizing Relationship Family");
			los.Database.BeginTransaction();
			los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.RelativeFamily ON");
			var relativeFamilies = ag.TblRelativeFamily.Where(x => x.RelativeFamilyId > 0).ToList();
			int i = 1;
			foreach (var relativeFamily in relativeFamilies)
			{
				los.RelativeFamily.SingleInsert(new RelativeFamily()
				{
					Id = relativeFamily.RelativeFamilyId,
					Name = relativeFamily.Name,
					IsEnable = relativeFamily.IsEnable
				});
				i++;
			}
			//los.SaveChanges();
			los.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.RelativeFamily OFF");
			los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing Cities");
		}

		static void SyncDistrict()
		{
			Console.WriteLine("Begin synchronizing Districts");
			los.Database.BeginTransaction();
			los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.District ON");
			var lProvinces = los.Province.Where(x => x.ProvinceId > 0).ToList();
			var mDistricts = ag.TblDistrict.Where(x => x.Id > 0).ToList();
			int i = 0;
			foreach (var district in mDistricts)
			{
				var d = new District()
				{
					DistrictId = district.Id,
					Name = district.Name,
					Priority = i,
					AreaId = district.AreaId,
					DistrictCode = district.DistrictCode,
					MecashId = district.Id,
					LatLong = district.LongitudeLatitude,
					IsApply = district.IsApply,
					ProvinceId = district.CityId
				};
				if (district.TypeDistrict == "Quận")
					d.Type = 1;
				else if (district.TypeDistrict == "Huyện")
					d.Type = 2;
				else if (district.TypeDistrict == "Thị xã")
					d.Type = 3;
				else if (district.TypeDistrict == "Thành Phố")
					d.Type = 4;
				else
					d.Type = 5;
				los.District.Add(d);
			}
			los.SaveChanges();
			los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.District OFF");
			los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing Districts");
		}

		static void SyncWard()
		{
			Console.WriteLine("Begin synchronizing Wards");
			los.Database.BeginTransaction();
			los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Ward ON");
			var lDistricts = los.District.Where(x => x.DistrictId > 0).ToList();
			var mWards = ag.TblWard.Where(x => x.Id > 0).ToList();
			int i = 0;
			foreach (var ward in mWards)
			{
				var w = new Ward()
				{
					WardId = ward.Id,
					Name = ward.Name,
					Priority = i,
					MecashId = ward.Id,
				};
				w.DistrictId = lDistricts.Where(x => x.MecashId == ward.DistrictId).FirstOrDefault().DistrictId;
				if (ward.TypeWard == "Xã")
					w.Type = 1;
				else if (ward.TypeWard == "Phường")
					w.Type = 2;
				else
					w.Type = 3;
				i++;
				los.Ward.SingleInsert(w);
			}
			//los.SaveChanges();
			los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Ward OFF");
			los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing Wards");
		}

		static void SyncLoanProduct()
		{
			Console.WriteLine("Begin synchronizing Loan Products");
			los.Database.BeginTransaction();
			los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanProduct ON");
			var products = ag.TblProductCredit.Where(x => x.Id > 0).ToList();
			var i = 0;
			foreach (var product in products)
			{
				if (!los.LoanProduct.Any(x => x.MecashId == product.Id))
				{
					los.LoanProduct.SingleInsert(new LoanProduct()
					{
						LoanProductId = product.Id,
						ConsultantRate = (double)product.RateConsultant,
						LoanRate = (double)product.RateInterest,
						MaximumMoney = product.LimitMoney,
						MecashId = product.Id,
						Name = product.Name,
						PlatformProductId = product.PlatformProductId,
						Priority = i,
						ServiceRate = (double)product.RateService,
						Status = product.Status,
						TotalRate = (double)product.Rate,
						TypeProductId = product.TypeProduct
					});
				}
				else
				{
					los.LoanProduct.Where(x => x.MecashId == product.Id).Update(x => new LoanProduct()
					{
						LoanProductId = product.Id,
						ConsultantRate = (double)product.RateConsultant,
						LoanRate = (double)product.RateInterest,
						MaximumMoney = product.LimitMoney,
						MecashId = product.Id,
						Name = product.Name,
						PlatformProductId = product.PlatformProductId,
						Priority = i,
						ServiceRate = (double)product.RateService,
						Status = product.Status,
						TotalRate = (double)product.Rate,
						TypeProductId = product.TypeProduct
					});
				}
				i++;
			}
			//los.SaveChanges();
			los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanProduct OFF");
			los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing Loan Products");
		}

		static void SyncJob()
		{
			Console.WriteLine("Begin synchronizing Jobs");
			los.Database.BeginTransaction();
			los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Job ON");
			var mJobs = ag.TblJob.Where(x => x.JobId > 0).ToList();
			int i = 0;
			foreach (var job in mJobs)
			{
				los.Job.SingleInsert(new Job()
				{
					Name = job.Name,
					Priority = i,
					MecashId = job.JobId,
					JobId = job.JobId
				});
				i++;
			}
			//los.SaveChanges();
			los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Job OFF");
			los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing Jobs");
		}

		static void SyncBank()
		{
			Console.WriteLine("Begin synchronizing Banks");
			los.Database.BeginTransaction();
			los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Bank ON");
			var mBanks = ag.TblBank.Where(x => x.Id > 0).ToList();
			foreach (var bank in mBanks)
			{
				los.Bank.SingleInsert(new Bank()
				{
					BankCode = bank.Code,
					BankValue = bank.ValueOfBank,
					MecashId = bank.Id,
					Name = bank.NameBank,
					Type = bank.TypeBank,
					BankId = bank.Id
				});
			}
			//los.SaveChanges();
			los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Bank OFF");
			los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing Banks");
		}

		static void SyncOrderSource(int startId)
		{
			Console.WriteLine("Begin synchronizing Order Sources");
			int count = ag.TblOrderSource.Where(x => x.Id > startId).Count();
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				los.Database.BeginTransaction();
				los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.OrderSource ON");
				var mOrderSources = ag.TblOrderSource.Where(x => x.Id > startId).Skip((i - 1) * 1000).Take(1000).ToList();
				for (int j = 0; j < mOrderSources.Count; j++)
				{
					var orderSource = mOrderSources[j];
					if (!los.OrderSource.Any(x => x.MecashId == orderSource.Id))
					{
						var o = new OrderSource()
						{
							ApplyFor = orderSource.ApplyFor,
							Condition = orderSource.Condition,
							CreatedAt = orderSource.CreateDate,
							CreatedBy = orderSource.CreateBy,
							IsScaned = orderSource.IsScaned,
							MecashId = orderSource.Id,
							ModifiedBy = orderSource.ModifyBy,
							ModifiedAt = orderSource.ModifyDate,
							Name = orderSource.OderSourceName,
							Status = orderSource.Status,
							OrderSourceId = orderSource.Id,
							ParentId = orderSource.ParentId
						};
						los.OrderSource.SingleInsert(o);
					}
					else
					{
						var o = los.OrderSource.Find(orderSource.Id);
						o.ApplyFor = orderSource.ApplyFor;
						o.Condition = orderSource.Condition;
						o.CreatedAt = orderSource.CreateDate;
						o.CreatedBy = orderSource.CreateBy;
						o.IsScaned = orderSource.IsScaned;
						o.MecashId = orderSource.Id;
						o.ModifiedBy = orderSource.ModifyBy;
						o.ModifiedAt = orderSource.ModifyDate;
						o.Name = orderSource.OderSourceName;
						o.ParentId = orderSource.ParentId;
						o.Status = orderSource.Status;
						o.OrderSourceId = orderSource.Id;
						los.OrderSource.SingleUpdate(o);
					}
				}
				//los.SaveChanges();
				los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.OrderSource OFF");
				los.Database.CommitTransaction();
			}
			Console.WriteLine("Finish synchronizing Order Sources");
		}

		static void SyncOrderSourceDetail(int startId)
		{
			Console.WriteLine("Begin synchronizing Order Source Detail");
			int count = ag.TblOrderSourceDetail.Where(x => x.Id > startId).Count();
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				los.Database.BeginTransaction();
				los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.OrderSourceDetail ON");
				var mOrderSourceDetails = ag.TblOrderSourceDetail.Where(x => x.Id > startId).Skip((i - 1) * 1000).Take(1000).ToList();
				foreach (var detail in mOrderSourceDetails)
				{
					if (!los.OrderSourceDetail.Any(x => x.MecashId == detail.Id))
					{
						los.OrderSourceDetail.SingleInsert(new OrderSourceDetail()
						{
							CompareTo = detail.CompareTo,
							CompareType = detail.CompareType,
							KeyType = detail.KeyType,
							MecashId = detail.Id,
							Name = detail.Name,
							OrderSourceId = detail.OrderSourceId,
							Status = detail.Status,
							OrderSourceDetailId = detail.Id
						});
					}
					else
					{
						var o = los.OrderSourceDetail.Find(detail.Id);
						o.CompareTo = detail.CompareTo;
						o.CompareType = detail.CompareType;
						o.KeyType = detail.KeyType;
						o.MecashId = detail.Id;
						o.Name = detail.Name;
						o.OrderSourceId = detail.OrderSourceId;
						o.Status = detail.Status;
						o.OrderSourceDetailId = detail.Id;
						los.OrderSourceDetail.SingleUpdate(o);
					}
				}
				los.SaveChanges();
				los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.OrderSourceDetail OFF");
				los.Database.CommitTransaction();
			}
			Console.WriteLine("Finish synchronizing Order Source Detail");
		}

		static void SyncUser(Dictionary<int, int> groupMapping, int startId)
		{
			Console.WriteLine("Begin synchronizing User");
			int count = ag.TblUser.Where(x => x.Id > startId).Count();
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				//los.Database.BeginTransaction();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT [User] ON");
				var users = ag.TblUser.Where(x => x.Id > startId).Skip((i - 1) * 1000).Take(1000).ToList();
				var us = new List<User>();
				foreach (var user in users)
				{
					int? groupId = groupMapping.GetValueOrDefault(user.GroupId, 0);
					us.Add(new User()
					{
						CiscoExtension = user.CiscoExtension,
						CiscoPassword = user.CiscoPassword,
						CiscoUsername = user.CiscoUsername,
						CityId = user.CityId,
						CompanyId = null,
						CoordinateRecord = user.CoordinateRecord,
						CoordinatorIsEnable = user.CoordinatorIsEnable,
						CreatedBy = null,
						CreatedTime = user.CreateDate,
						DepartmentId = null,
						DistrictId = null,
						Email = user.Email,
						FullName = user.FullName,
						GroupId = groupId > 0 ? groupId : null,
						Ipphone = user.Ipphone != null && !user.Ipphone.Trim().Equals("") ? Int32.Parse(user.Ipphone) : 0,
						IsDeleted = false,
						Password = user.Password,
						Phone = user.Phone,
						PositionId = null,
						PushTokenAndroid = user.PushTokenAndroid,
						PushTokenIos = user.PushTokenIos,
						Status = user.Status,
						UpdatedTime = user.CreateDate,
						UserId = user.Id,
						Username = user.Username,
						WardId = null
					});
				}
				los.User.BulkInsert(us, options => options.InsertKeepIdentity = true);
				//los.SaveChanges();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT [User] OFF");
				//los.Database.CommitTransaction();
			}
			Console.WriteLine("Finish synchronizing User");
		}

		static void SyncShop(int startId)
		{
			Console.WriteLine("Begin synchronizing Shop");

			int count = ag.TblShop.Where(x => x.ShopId > startId && x.IsAgent == 1 && x.IsReceipt == true && x.Status == 1).Count();
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				//los.Database.BeginTransaction();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Shop ON");
				var shops = ag.TblShop.Where(x => x.ShopId > startId && x.IsAgent == 1 && x.IsReceipt == true && x.Status == 1).Skip((i - 1) * 1000).Take(1000).ToList();
				var ss = new List<Shop>();
				foreach (var shop in shops)
				{
					//if (!los.Shop.Any(x => x.ShopId == shop.ShopId))
					//{
					ss.Add(new Shop()
					{
						Address = shop.Address,
						CityId = shop.CityId,
						CreatedAt = shop.CreatedDate,
						DistrictId = shop.DistrictId,
						Email = shop.Email,
						HubType = shop.GroupShop == 1 ? 1 : (shop.GroupShop == 3 ? 2 : 0),
						InviteCode = shop.InviteCode,
						IsReceiveLoan = shop.IsHub.Value ? true : false,
						Lat = shop.Lat,
						Long = shop.Long,
						Name = shop.Name,
						Note = shop.Note,
						Phone = shop.Phone,
						ShopId = shop.ShopId,
						Received = 0,
						Status = shop.Status,
						UpdatedAt = shop.CreatedDate,
						UserId = shop.UserId.Value,
						WardId = shop.WardId
					});
					//}
					//else
					//{
					//	var s = los.Shop.Find(shop.ShopId);
					//	s.Address = shop.Address;
					//	s.CityId = shop.CityId;
					//	s.CreatedAt = shop.CreatedDate;
					//	s.DistrictId = shop.DistrictId;
					//	s.Email = shop.Email;
					//	s.HubType = 1;
					//	s.InviteCode = shop.InviteCode;
					//	s.IsReceiveLoan = shop.IsHub.Value ? true : false;
					//	s.Lat = shop.Lat;
					//	s.Long = shop.Long;
					//	s.Name = shop.Name;
					//	s.Note = shop.Note;
					//	s.Phone = shop.Phone;
					//	s.ShopId = shop.ShopId;
					//	s.Received = 0;
					//	s.Status = shop.Status;
					//	s.UpdatedAt = shop.CreatedDate;
					//	s.UserId = shop.UserId.Value;
					//	s.WardId = shop.WardId;
					//	los.Entry(s).State = EntityState.Modified;
					//}
				}
				los.Shop.BulkInsert(ss, options => options.InsertKeepIdentity = true);
				//los.SaveChanges();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Shop OFF");
				//los.Database.CommitTransaction();
			}
			Console.WriteLine("Finish synchronizing Shop");
		}

		static void SyncUserShop()
		{
			Console.WriteLine("Begin synchronizing UserShop");
			// get all shop 			
			var count = ag.TblUserShop.Where(x => x.ShopId > 0 && x.UserId > 0).Count();
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				var shops = ag.TblUserShop.Where(x => x.ShopId > 0 && x.UserId > 0).Skip((i - 1) * 1000).Take(1000).ToList();
				var uss = new List<UserShop>();
				foreach (var shop in shops)
				{
					uss.Add(new UserShop()
					{
						UserId = shop.UserId,
						ShopId = shop.ShopId,
						Status = true,
						CreatedAt = DateTime.Now
					});
				}
				los.UserShop.BulkInsert(uss);
				los.SaveChanges();
			}
			Console.WriteLine("Finish synchronizing UserShop");
		}

		static void SyncManagerAreaHub()
		{
			Console.WriteLine("Begin synchronizing ManagerAreaHub");
			//los.Database.BeginTransaction();			
			// dong bo chi hub sang
			var shopIds = ag.TblShop.Where(x => x.ShopId > 0 && x.IsAgent == 1 && x.IsReceipt == true && x.Status == 1).Select(x => x.ShopId).ToList();
			var shops = ag.TblManageSpices.Where(x => x.ShopId > 0 && shopIds.Contains(x.ShopId)).ToList();
			var ms = new List<ManagerAreaHub>();
			foreach (var shop in shops)
			{
				if (los.Shop.Find(shop.ShopId) != null)
				{
					ms.Add(new ManagerAreaHub()
					{
						CreateDate = shop.CreateDate,
						DistrictId = shop.DistrictId,
						ProductId = shop.ProductId,
						ProvinceId = shop.CityId,
						ShopId = shop.ShopId,
						WardId = shop.WardId
					});
				}
			}
			los.ManagerAreaHub.BulkInsert(ms);
			los.SaveChanges();
			//los.SaveChanges();
			//los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing ManagerAreaHub");
		}

		static void SyncProductAppraiser(int startId)
		{
			Console.WriteLine("Begin synchronizing Product Appraiser");
			var count = ag.TblProductAppraiser.Count(x => x.Id > startId);
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				Console.WriteLine("page:" + i);
				//los.Database.BeginTransaction();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.ProductAppraiser ON");
				var products = ag.TblProductAppraiser.Where(x => x.Id > startId).Skip((i - 1) * 1000).Take(1000).ToList();
				var ps = new List<ProductAppraiser>();
				foreach (var product in products)
				{
					int? groupId = null;
					if (product.GroupId == 23)
						groupId = 45;
					ps.Add(new ProductAppraiser()
					{
						CreatedAt = product.CreateDate,
						GroupId = groupId,
						Id = product.Id,
						IsWork = product.IsWord,
						Latitude = product.Latitude,
						Longitude = product.Longitude,
						ProductCreditId = product.ProductId,
						Status = product.Status,
						TypeAppraiser = product.Type,
						UserId = product.UserId,
						LoanBriefId = product.LoanCreditId.Value
					});
				}
				los.ProductAppraiser.BulkInsert(ps, options => options.InsertKeepIdentity = true);
				//los.SaveChanges();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.ProductAppraiser OFF");
				//los.Database.CommitTransaction();
			}
			Console.WriteLine("Finish synchronizing Product Appraiser");
		}

		static void SyncProductReview(int startId)
		{
			Console.WriteLine("Begin synchronizing Product Review");
			var count = ag.TblProductReview.Count(x => x.Id > startId);
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				//los.Database.BeginTransaction();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.ProductReview ON");
				var products = ag.TblProductReview.Where(x => x.Id > startId).Skip((i - 1) * 1000).Take(1000).ToList();
				var ps = new List<ProductReview>();
				foreach (var product in products)
				{
					//if (!los.ProductReview.Any(x => x.Id == product.Id))
					//{
					ps.Add(new ProductReview()
					{
						Id = product.Id,
						IsCancel = product.IsCancel,
						Name = product.Name,
						OrderNo = product.OrderNo,
						ParentId = product.ParentId,
						State = product.State,
						Status = product.Status
					});
					//}
					//else
					//{
					//	var p = los.ProductReview.Find(product.Id);
					//	p.Id = product.Id;
					//	p.IsCancel = product.IsCancel;
					//	p.Name = product.Name;
					//	p.OrderNo = product.OrderNo;
					//	p.ParentId = product.ParentId;
					//	p.State = product.State;
					//	p.Status = product.Status;
					//	los.Entry(p).State = EntityState.Modified;
					//}				
				}
				los.ProductReview.BulkInsert(ps, options => options.InsertKeepIdentity = true);
				//los.SaveChanges();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.ProductReview OFF");
				//los.Database.CommitTransaction();
			}
			Console.WriteLine("Finish synchronizing Product Review");
		}

		static void SyncProductReviewDetail(int startId)
		{
			Console.WriteLine("Begin synchronizing Product Review Detail");
			var count = ag.TblProductReviewDetail.Count(x => x.Id > startId);
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				//los.Database.BeginTransaction();
				//los.Database.ExecuteSqlRaw("truncate table dbo.ProductReviewDetail");
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.ProductReviewDetail ON");
				var products = ag.TblProductReviewDetail.Where(x => x.Id > startId).Skip((i - 1) * 1000).Take(1000).ToList();
				var ps = new List<ProductReviewDetail>();
				foreach (var product in products)
				{
					ps.Add(new ProductReviewDetail()
					{
						Id = product.Id,
						MoneyDiscount = product.MoneyDiscount,
						PecentDiscount = product.PecentDiscount,
						ProductCreditId = product.IdType,
						ProductReviewId = product.IdProductReview
					});
					i++;
				}
				los.ProductReviewDetail.BulkInsert(ps, options => options.InsertKeepIdentity = true);
				//los.SaveChanges();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.ProductReviewDetail OFF");
				//los.Database.CommitTransaction();
			}
			Console.WriteLine("Finish synchronizing Product Review Detail");
		}

		static void SyncProductReviewResult(int id)
		{
			Console.WriteLine("Begin synchronizing Product Review Result");
			var count = ag.TblProductReviewResult.Count(x => x.Id > id);
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				//los.Database.BeginTransaction();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.ProductReviewResult ON");
				Console.WriteLine("page :" + i);
				var products = ag.TblProductReviewResult.Where(x => x.Id > id).Skip((i - 1) * 1000).Take(1000).ToList();
				var ps = new List<ProductReviewResult>();
				foreach (var product in products)
				{
					ps.Add(new ProductReviewResult()
					{
						CreateDate = product.CreateDate,
						Id = product.Id,
						IsCheck = product.IsCheck,
						LoanBriefId = product.LoanCreditId,
						ProductId = product.ProductId,
						ProductReviewId = product.ProductReviewId
					});
				}
				los.ProductReviewResult.BulkInsert(ps, options => options.InsertKeepIdentity = true);
				//los.SaveChanges();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.ProductReviewResult OFF");
				//los.Database.CommitTransaction();
			}
			Console.WriteLine("Finish synchronizing Product Review Result");
		}

		static void SyncProductReviewResultDetail(int startId)
		{
			Console.WriteLine("Begin synchronizing Product Review Result Detail");
			var count = ag.TblProductReviewResultDetail.Count(x => x.Id > 0);
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				//los.Database.BeginTransaction();
				//los.Database.ExecuteSqlRaw("truncate table dbo.ProductReviewResultDetail");
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.ProductReviewResultDetail ON");
				var products = ag.TblProductReviewResultDetail.Where(x => x.Id > startId).Skip((i - 1) * 1000).Take(1000).ToList();
				var ps = new List<ProductReviewResultDetail>();
				foreach (var product in products)
				{
					ps.Add(new ProductReviewResultDetail()
					{
						CreateDate = product.CreateDate,
						GroupUserId = null,
						ProductId = product.ProductId,
						LoanBriefId = product.LoanCreditId,
						Id = product.Id,
						PriceReview = product.PriceReview,
						UserId = product.UserId
					});
				}
				los.ProductReviewResultDetail.BulkInsert(ps, options => options.InsertKeepIdentity = true);
				//los.SaveChanges();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.ProductReviewResultDetail OFF");
				//los.Database.CommitTransaction();
			}
			Console.WriteLine("Finish synchronizing Product Review Result Detail");
		}

		static void SyncLoanBrief(bool refresh, bool truncate, int startId)
		{
			Console.WriteLine("Begin synchronizing Loan Briefs");
			List<int> timing = new List<int>();
			if (!refresh)
			{
				//DateTime dt = new DateTime(2020, 6, 30, 0, 0, 0);
				int count = ag.TblLoanCredit.Where(x => x.Id > startId && x.Id <= 1000000).Count();
				//int count = 1;
				var page = (count / 100) + 1;
				var pageSize = 100;
				for (int i = 1; i < page + 1; i++)
				{
					var loanCredits = ag.TblLoanCredit.AsNoTracking().Where(x => x.Id > startId && x.Id <= 1000000).Skip((i - 1) * pageSize).Take(pageSize).ToList();
					//var loanCredits = ag.TblLoanCredit.Where(x => x.Id == 911143).OrderByDescending(x => x.Id).Skip((i - 1) * pageSize).Take(pageSize).ToList();
					Parallel.ForEach(loanCredits, new ParallelOptions { MaxDegreeOfParallelism = 10 }, loanCredit =>
					{
						using (var los = new LOSContext())
						{
							using (var agParr = new MecashContext())
							{
								var startTime = DateTime.Now;
								Console.WriteLine("Begin Import LoanCredit Id = " + loanCredit.Id);
								los.Database.BeginTransaction();
								try
								{
									var mCustomer = agParr.TblCustomerCredit.Find(loanCredit.CustomerCreditId);
									if (mCustomer != null)
									{
										#region customer
										Customer customer = null;
										if (los.Customer.Find(mCustomer.CustomerId) == null)
										{
											try
											{
												//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Customer ON");
												// Insert
												customer = new Customer();
												customer.Address = mCustomer.Street;
												customer.AddressLatLong = mCustomer.LatLongAddress;
												customer.CompanyAddress = mCustomer.AddressCompany;
												customer.CompanyAddressLatLong = mCustomer.LatLongAddressCompany;
												customer.CompanyName = mCustomer.CompanyName;
												if (mCustomer.CompanyDistrictId > 0)
													customer.CompanyDistrictId = mCustomer.CompanyDistrictId;
												if (mCustomer.CompanyCityId > 0)
													customer.CompanyProvinceId = mCustomer.CompanyCityId;
												if (mCustomer.CompanyWardId > 0)
													customer.CompanyWardId = mCustomer.CompanyWardId;
												customer.CreatedAt = mCustomer.CreateDate;
												customer.CustomerId = mCustomer.CustomerId;
												customer.DistrictId = mCustomer.DistrictId;
												customer.Dob = mCustomer.Birthday;
												customer.Email = mCustomer.Email;
												customer.FacebookAddress = mCustomer.Facebook;
												customer.FullName = mCustomer.FullName;
												customer.Gender = mCustomer.Gender;
												customer.InsurenceNote = mCustomer.SocialInsuranceNote;
												customer.InsurenceNumber = mCustomer.SocialInsuranceNumber;
												customer.IsMerried = mCustomer.IsMarried;
												customer.JobId = mCustomer.JobId;
												customer.MecashId = mCustomer.CustomerId;
												customer.NationalCard = mCustomer.CardNumber;
												customer.NationalCardDate = null;
												customer.NationCardPlace = null;
												customer.NumberBaby = mCustomer.NumberBaby;
												customer.Phone = mCustomer.Phone;
												customer.ProvinceId = mCustomer.CityId;
												customer.Salary = mCustomer.Salary;
												customer.Status = 1;
												customer.UpdatedAt = mCustomer.CreateDate;
												customer.WardId = mCustomer.WardId;
												los.Customer.SingleInsert(customer);
											}
											catch (Exception ex)
											{
												Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
												if (ex.InnerException != null)
													Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
											}
											finally
											{
												//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Customer OFF");
											}
										}
										else
										{
											customer = los.Customer.Where(x => x.CustomerId == mCustomer.CustomerId).FirstOrDefault();
											customer.Address = mCustomer.Street;
											customer.AddressLatLong = mCustomer.LatLongAddress;
											customer.CompanyAddress = mCustomer.AddressCompany;
											customer.CompanyAddressLatLong = mCustomer.LatLongAddressCompany;
											customer.CompanyName = mCustomer.CompanyName;
											if (mCustomer.CompanyDistrictId > 0)
												customer.CompanyDistrictId = mCustomer.CompanyDistrictId;
											if (mCustomer.CompanyCityId > 0)
												customer.CompanyProvinceId = mCustomer.CompanyCityId;
											if (mCustomer.CompanyWardId > 0)
												customer.CompanyWardId = mCustomer.CompanyWardId;
											customer.CreatedAt = mCustomer.CreateDate;
											customer.CustomerId = mCustomer.CustomerId;
											customer.DistrictId = mCustomer.DistrictId;
											customer.Dob = mCustomer.Birthday;
											customer.Email = mCustomer.Email;
											customer.FacebookAddress = mCustomer.Facebook;
											customer.FullName = mCustomer.FullName;
											customer.Gender = mCustomer.Gender;
											customer.InsurenceNote = mCustomer.SocialInsuranceNote;
											customer.InsurenceNumber = mCustomer.SocialInsuranceNumber;
											customer.IsMerried = mCustomer.IsMarried;
											customer.JobId = mCustomer.JobId;
											customer.MecashId = mCustomer.CustomerId;
											customer.NationalCard = mCustomer.CardNumber;
											customer.NationalCardDate = null;
											customer.NationCardPlace = null;
											customer.NumberBaby = mCustomer.NumberBaby;
											customer.Phone = mCustomer.Phone;
											customer.ProvinceId = mCustomer.CityId;
											customer.Salary = mCustomer.Salary;
											customer.Status = 1;
											customer.UpdatedAt = mCustomer.CreateDate;
											customer.WardId = mCustomer.WardId;
											los.Customer.SingleUpdate(customer);
										}
										#endregion
										#region Loan Brief
										var loanBrief = los.LoanBrief.Where(x => x.LoanBriefId == loanCredit.Id || x.MecashId == loanCredit.Id).FirstOrDefault();
										if (loanBrief == null)
										{
											//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanBrief ON");
											try
											{
												loanBrief = new LoanBrief();
												loanBrief.ActionState = null;
												loanBrief.AddedToQueue = null;
												//loanBrief.AffCode = null;
												loanBrief.AffCode = loanCredit.AffCode;
												//loanBrief.AffCode = null;
												loanBrief.AffSid = loanCredit.AffSid;
												//loanBrief.AffStatus = null;
												loanBrief.AffSource = loanCredit.AffSource;
												loanBrief.AffPub = loanCredit.AffPub;
												//loanBrief.AutomaticProcessed = null;
												loanBrief.BankAccountName = loanCredit.NameCardHolder;
												loanBrief.BankAccountNumber = loanCredit.AccountNumberBanking;
												loanBrief.BankCardNumber = loanCredit.NumberCardBanking;
												if (loanCredit.BankId > 0)
													loanBrief.BankId = loanCredit.BankId;
												loanBrief.BeginStartTime = loanCredit.BeginStartTime;
												if (loanCredit.SupportLastId > 0)
													loanBrief.BoundTelesaleId = loanCredit.SupportLastId;
												loanBrief.BuyInsurenceCustomer = loanCredit.CustomerBuyInsurance;
												loanBrief.CheckCic = loanCredit.IsCheckCic.HasValue ? (loanCredit.IsCheckCic.Value ? 1 : 0) : 0;
												//loanBrief.CheckLocation = null;
												if (loanCredit.RateConsultant > 0)
													loanBrief.ConsultantRate = (double)loanCredit.RateConsultant;
												//loanBrief.ContractGinno = null;
												//loanBrief.CoordinatorPushAt = null;
												if (loanCredit.CoordinatorId > 0)
													loanBrief.CoordinatorUserId = loanCredit.CoordinatorId;
												loanBrief.CreatedTime = loanCredit.CreateDate;
												loanBrief.CreateBy = loanCredit.CreatedBy;
												loanBrief.CustomerId = loanCredit.CustomerCreditId;
												loanBrief.DeviceId = loanCredit.DeviceId;
												loanBrief.DeviceStatus = loanCredit.StatusOfDeviceId;
												loanBrief.Dob = mCustomer.Birthday;
												if (loanCredit.DistrictId > 0)
													loanBrief.DistrictId = loanCredit.DistrictId;
												loanBrief.Email = customer.Email;
												if (loanCredit.CompanyFieldSurveyUserId > 0)
													loanBrief.EvaluationCompanyUserId = loanCredit.CompanyFieldSurveyUserId;
												loanBrief.EvaluationHomeState = loanCredit.AddressFieldSurveyStatus;
												loanBrief.EvaluationHomeTime = loanCredit.AddressFieldSurveyTime;
												if (loanCredit.CompanyFieldSurveyUserId > 0)
													loanBrief.EvaluationCompanyUserId = loanCredit.CompanyFieldSurveyUserId;
												loanBrief.EvaluationCompanyTime = loanCredit.CompanyFieldSurveyTime;
												loanBrief.EvaluationCompanyState = loanCredit.CompanyFieldSurveyStatus;
												loanBrief.Frequency = loanCredit.Frequency;
												loanBrief.FromDate = loanCredit.FromDate;
												loanBrief.FullName = customer.FullName;
												loanBrief.Gender = mCustomer.Gender;
												loanBrief.DomainName = loanCredit.DomainName;
												//loanBrief.HomeNetwork = null;				
												var shop = los.Shop.Find(loanCredit.Code);
												if (loanCredit.Code > 0 && shop != null)
													loanBrief.HubId = loanCredit.Code;
												if (loanCredit.HubEmployeId > 0)
													loanBrief.HubEmployeeId = loanCredit.HubEmployeId;
												loanBrief.IsHeadOffice = loanCredit.IsHeadOffice;
												loanBrief.IsLocate = loanCredit.IsLocate;
												loanBrief.IsLock = null;
												//loanBrief.IsLock = null;
												//loanBrief.IsNeeded = null;
												loanBrief.IsReMarketing = loanCredit.ReMarketing;
												loanBrief.IsTrackingLocation = loanCredit.IsTrackingLocation;
												//loanBrief.IsUploadState = null;
												loanBrief.IsVerifyOtpLdp = loanCredit.IsVerifyOtpLdp;
												loanBrief.LabelRequestLocation = loanCredit.LabelLocation;
												loanBrief.LabelResponseLocation = loanCredit.LabelVerifyLocation;
												//loanBrief.LastSendRequestActive = null;												
												if (loanCredit.OwerShopId > 0 && loanCredit.Status >= 20)
													loanBrief.LenderId = loanCredit.OwerShopId;
												loanBrief.LoanAmountFirst = loanCredit.TotalMoneyFirst;
												loanBrief.LoanAmountExpertise = loanCredit.TotalMoneyExpertise;
												loanBrief.LoanAmountExpertiseLast = loanCredit.TotalMoneyExpertiseLast;
												if (loanCredit.Status >= 35 && loanCredit.LoanId > 0)
												{
													var loan = ag.TblLoan.Where(x => x.Id == loanCredit.LoanId).FirstOrDefault();
													if (loan != null)
													{
														loanBrief.LoanAmount = loan.TotalMoney;
													}
												}
												else
												{
													loanBrief.LoanAmount = loanCredit.TotalMoney;
												}
												loanBrief.LoanBriefCancelAt = loanCredit.CancelDateLoanCredit;
												loanBrief.LoanBriefCancelBy = loanCredit.UserIdcancel;
												//loanBrief.LoanBriefComment = null;
												loanBrief.LoanBriefId = loanCredit.Id;
												loanBrief.LoanPurpose = loanCredit.LoanPurpose;
												if (loanCredit.Rate > 0)
													loanBrief.LoanRate = (double)loanCredit.Rate;
												loanBrief.LoanTime = loanCredit.LoanTime;
												//loanBrief.LogLoanInfoAi = null;
												//loanBrief.LogRequestAi = null;							
												loanBrief.MecashId = loanCredit.Id;
												loanBrief.NationalCard = customer.NationalCard;
												loanBrief.NationalCardDate = customer.NationalCardDate;
												loanBrief.NationCardPlace = customer.NationCardPlace;
												loanBrief.NumberCall = loanCredit.CountCall;
												loanBrief.OrderSourceId = loanCredit.OrderSourceId;
												loanBrief.OrderSourceParentId = loanCredit.OrderSourceParentId;
												loanBrief.Phone = customer.Phone;
												//loanBrief.PipelineState = null;
												loanBrief.PlatformType = loanCredit.PlatformType;
												loanBrief.PriorityContract = loanCredit.PriorityContract;
												if (loanCredit.TypeId > 0)
													loanBrief.ProductId = loanCredit.TypeId;
												if (loanCredit.CityId > 0)
													loanBrief.ProvinceId = loanCredit.CityId;
												loanBrief.RateTypeId = loanCredit.RateType;
												loanBrief.ReasonCancel = loanCredit.ReasonId;
												loanBrief.ReceivingMoneyType = loanCredit.TypeReceivingMoney;
												//loanBrief.RefCodeLocation = loanCredit.RefCodeLocation;
												//loanBrief.RefCodeStatus = loanCredit.RefCodeStatus;
												loanBrief.ReMarketingLoanBriefId = loanCredit.ReMarketingLoanCreditId;
												//loanBrief.ResultCic = null;
												loanBrief.ResultLocation = loanCredit.ResultLocation;
												loanBrief.ResultRuleCheck = loanCredit.ResultCac;
												if (loanCredit.ListCheckLeadQualify != null && !loanCredit.ListCheckLeadQualify.Equals(""))
													loanBrief.ValueCheckQualify = ConvertQualify(loanCredit.ListCheckLeadQualify);
												//loanBrief.ScheduleTime = null;
												if (loanCredit.Score.HasValue)
													loanBrief.ScoreCredit = (int)loanCredit.Score.Value;
												if (loanCredit.RateService > 0)
													loanBrief.ServiceRate = (double)loanCredit.RateService;
												var status = los.LoanStatus.Where(x => x.MecashStatus == loanCredit.Status).FirstOrDefault();
												if (status != null)
													loanBrief.Status = status.Status;
												else
													loanBrief.Status = -99;
												if (loanCredit.Status == 35 || loanCredit.Status == 100)
												{
													// Pipeline đã kết thúc
													loanBrief.PipelineState = 9;
													loanBrief.CurrentPipelineId = null;
													loanBrief.CurrentSectionDetailId = null;
													loanBrief.CurrentSectionId = null;
													loanBrief.ActionState = null;
													loanBrief.InProcess = 0;
													loanBrief.AddedToQueue = false;
												}
												else
												{
													// đơn đang trong trạng thái chưa cho vay > 
													// xác định luồng hay cho chọn luồng đích
													loanBrief.PipelineState = (int)Const.ProcessType.MANUAL_PIPELINE;
												}
												//loanBrief.TelesalesPushAt = null;							
												loanBrief.Tid = loanCredit.Tid;
												loanBrief.ToDate = loanCredit.ToDate;
												loanBrief.TypeInsurence = loanCredit.TypeInsurance;
												int type = 0;
												if (!mCustomer.TypeCustomerCreditId.HasValue || mCustomer.TypeCustomerCreditId == 0)
													type = 1;
												else if (mCustomer.TypeCustomerCreditId == 1)
													type = 2;
												loanBrief.TypeLoanBrief = type;
												loanBrief.UpdatedTime = loanCredit.ModifyDate;
												loanBrief.UtmCampaign = loanCredit.UtmCampaign;
												loanBrief.UtmContent = loanCredit.UtmContent;
												loanBrief.UtmMedium = loanCredit.UtmMedium;
												loanBrief.UtmSource = loanCredit.UtmSource;
												loanBrief.UtmTerm = loanCredit.UtmTerm;
												loanBrief.Version = 1;
												if (loanCredit.WardId > 0)
													loanBrief.WardId = loanCredit.WardId;
												los.LoanBrief.SingleInsert(loanBrief);
											}
											catch (Exception ex)
											{
												Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
												if (ex.InnerException != null)
													Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
											}
											finally
											{
												//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanBrief OFF");
											}
										}
										else
										{
											loanBrief.ActionState = null;
											loanBrief.AddedToQueue = null;
											//loanBrief.AffCode = null;
											loanBrief.AffCode = loanCredit.AffCode;
											//loanBrief.AffCode = null;
											loanBrief.AffSid = loanCredit.AffSid;
											//loanBrief.AffStatus = null;
											loanBrief.AffSource = loanCredit.AffSource;
											loanBrief.AffPub = loanCredit.AffPub;
											//loanBrief.AutomaticProcessed = null;
											loanBrief.BankAccountName = loanCredit.NameCardHolder;
											loanBrief.BankAccountNumber = loanCredit.AccountNumberBanking;
											loanBrief.BankCardNumber = loanCredit.NumberCardBanking;
											if (loanCredit.BankId > 0)
												loanBrief.BankId = loanCredit.BankId;
											loanBrief.BeginStartTime = loanCredit.BeginStartTime;
											if (loanCredit.SupportLastId > 0)
												loanBrief.BoundTelesaleId = loanCredit.SupportLastId;
											loanBrief.BuyInsurenceCustomer = loanCredit.CustomerBuyInsurance;
											loanBrief.CheckCic = loanCredit.IsCheckCic.HasValue ? (loanCredit.IsCheckCic.Value ? 1 : 0) : 0;
											//loanBrief.CheckLocation = null;
											if (loanCredit.RateConsultant > 0)
												loanBrief.ConsultantRate = (double)loanCredit.RateConsultant;
											//loanBrief.ContractGinno = null;
											//loanBrief.CoordinatorPushAt = null;
											if (loanCredit.CoordinatorId > 0)
												loanBrief.CoordinatorUserId = loanCredit.CoordinatorId;
											loanBrief.CreatedTime = loanCredit.CreateDate;
											loanBrief.CreateBy = loanCredit.CreatedBy;
											loanBrief.CustomerId = loanCredit.CustomerCreditId;
											loanBrief.DeviceId = loanCredit.DeviceId;
											loanBrief.DeviceStatus = loanCredit.StatusOfDeviceId;
											loanBrief.Dob = mCustomer.Birthday;
											if (loanCredit.DistrictId > 0)
												loanBrief.DistrictId = loanCredit.DistrictId;
											loanBrief.Email = customer.Email;
											if (loanCredit.CompanyFieldSurveyUserId > 0)
												loanBrief.EvaluationCompanyUserId = loanCredit.CompanyFieldSurveyUserId;
											loanBrief.EvaluationHomeState = loanCredit.AddressFieldSurveyStatus;
											loanBrief.EvaluationHomeTime = loanCredit.AddressFieldSurveyTime;
											if (loanCredit.CompanyFieldSurveyUserId > 0)
												loanBrief.EvaluationCompanyUserId = loanCredit.CompanyFieldSurveyUserId;
											loanBrief.EvaluationCompanyTime = loanCredit.CompanyFieldSurveyTime;
											loanBrief.EvaluationCompanyState = loanCredit.CompanyFieldSurveyStatus;
											loanBrief.Frequency = loanCredit.Frequency;
											loanBrief.FromDate = loanCredit.FromDate;
											loanBrief.FullName = customer.FullName;
											loanBrief.Gender = mCustomer.Gender;
											loanBrief.DomainName = loanCredit.DomainName;
											//loanBrief.HomeNetwork = null;				
											var shop = los.Shop.Find(loanCredit.Code);
											if (loanCredit.Code > 0 && shop != null)
												loanBrief.HubId = loanCredit.Code;
											if (loanCredit.HubEmployeId > 0)
												loanBrief.HubEmployeeId = loanCredit.HubEmployeId;
											loanBrief.IsHeadOffice = loanCredit.IsHeadOffice;
											loanBrief.IsLocate = loanCredit.IsLocate;
											loanBrief.IsLock = null;
											//loanBrief.IsLock = null;
											//loanBrief.IsNeeded = null;
											loanBrief.IsReMarketing = loanCredit.ReMarketing;
											loanBrief.IsTrackingLocation = loanCredit.IsTrackingLocation;
											//loanBrief.IsUploadState = null;
											loanBrief.IsVerifyOtpLdp = loanCredit.IsVerifyOtpLdp;
											loanBrief.LabelRequestLocation = loanCredit.LabelLocation;
											loanBrief.LabelResponseLocation = loanCredit.LabelVerifyLocation;
											//loanBrief.LastSendRequestActive = null;											
											if (loanCredit.OwerShopId > 0 && loanCredit.Status >= 20)
												loanBrief.LenderId = loanCredit.OwerShopId;
											loanBrief.LoanAmountFirst = loanCredit.TotalMoneyFirst;
											loanBrief.LoanAmountExpertise = loanCredit.TotalMoneyExpertise;
											loanBrief.LoanAmountExpertiseLast = loanCredit.TotalMoneyExpertiseLast;
											if (loanCredit.Status >= 35 && loanCredit.LoanId > 0)
											{
												var loan = agParr.TblLoan.Where(x => x.Id == loanCredit.LoanId).FirstOrDefault();
												if (loan != null)
												{
													loanBrief.LoanAmount = loan.TotalMoney;
												}
											}
											else
											{
												loanBrief.LoanAmount = loanCredit.TotalMoney;
											}
											loanBrief.LoanBriefCancelAt = loanCredit.CancelDateLoanCredit;
											loanBrief.LoanBriefCancelBy = loanCredit.UserIdcancel;
											//loanBrief.LoanBriefComment = null;
											loanBrief.LoanBriefId = loanCredit.Id;
											loanBrief.LoanPurpose = loanCredit.LoanPurpose;
											if (loanCredit.Rate > 0)
												loanBrief.LoanRate = (double)loanCredit.Rate;
											loanBrief.LoanTime = loanCredit.LoanTime;
											//loanBrief.LogLoanInfoAi = null;
											//loanBrief.LogRequestAi = null;							
											loanBrief.MecashId = loanCredit.Id;
											loanBrief.NationalCard = customer.NationalCard;
											loanBrief.NationalCardDate = customer.NationalCardDate;
											loanBrief.NationCardPlace = customer.NationCardPlace;
											loanBrief.NumberCall = loanCredit.CountCall;
											loanBrief.OrderSourceId = loanCredit.OrderSourceId;
											loanBrief.OrderSourceParentId = loanCredit.OrderSourceParentId;
											loanBrief.Phone = customer.Phone;
											//loanBrief.PipelineState = null;
											loanBrief.PlatformType = loanCredit.PlatformType;
											loanBrief.PriorityContract = loanCredit.PriorityContract;
											if (loanCredit.TypeId > 0)
												loanBrief.ProductId = loanCredit.TypeId;
											if (loanCredit.CityId > 0)
												loanBrief.ProvinceId = loanCredit.CityId;
											loanBrief.RateTypeId = loanCredit.RateType;
											loanBrief.ReasonCancel = loanCredit.ReasonId;

											loanBrief.ReceivingMoneyType = loanCredit.TypeReceivingMoney;
											//loanBrief.RefCodeLocation = loanCredit.RefCodeLocation;
											//loanBrief.RefCodeStatus = loanCredit.RefCodeStatus;
											loanBrief.ReMarketingLoanBriefId = loanCredit.ReMarketingLoanCreditId;
											//loanBrief.ResultCic = null;
											loanBrief.ResultLocation = loanCredit.ResultLocation;
											loanBrief.ResultRuleCheck = loanCredit.ResultCac;
											if (loanCredit.ListCheckLeadQualify != null && !loanCredit.ListCheckLeadQualify.Equals(""))
												loanBrief.ValueCheckQualify = ConvertQualify(loanCredit.ListCheckLeadQualify);
											//loanBrief.ScheduleTime = null;
											if (loanCredit.Score.HasValue)
												loanBrief.ScoreCredit = (int)loanCredit.Score.Value;
											if (loanCredit.RateService > 0)
												loanBrief.ServiceRate = (double)loanCredit.RateService;
											var status = los.LoanStatus.Where(x => x.MecashStatus == loanCredit.Status).FirstOrDefault();
											if (status != null)
												loanBrief.Status = status.Status;
											else
												loanBrief.Status = -99;
											if (loanCredit.Status == 35 || loanCredit.Status == 100)
											{
												// Pipeline đã kết thúc
												loanBrief.PipelineState = 9;
												loanBrief.CurrentPipelineId = null;
												loanBrief.CurrentSectionDetailId = null;
												loanBrief.CurrentSectionId = null;
												loanBrief.ActionState = null;
												loanBrief.InProcess = 0;
												loanBrief.AddedToQueue = false;
											}
											else
											{
												// đơn đang trong trạng thái chưa cho vay > 
												// xác định luồng hay cho chọn luồng đích
												loanBrief.PipelineState = (int)Const.ProcessType.MANUAL_PIPELINE;
											}
											//loanBrief.TelesalesPushAt = null;							
											loanBrief.Tid = loanCredit.Tid;
											loanBrief.ToDate = loanCredit.ToDate;
											loanBrief.TypeInsurence = loanCredit.TypeInsurance;
											int type = 0;
											if (!mCustomer.TypeCustomerCreditId.HasValue || mCustomer.TypeCustomerCreditId == 0)
												type = 1;
											else if (mCustomer.TypeCustomerCreditId == 1)
												type = 2;
											loanBrief.TypeLoanBrief = type;
											loanBrief.UpdatedTime = loanCredit.ModifyDate;
											loanBrief.UtmCampaign = loanCredit.UtmCampaign;
											loanBrief.UtmContent = loanCredit.UtmContent;
											loanBrief.UtmMedium = loanCredit.UtmMedium;
											loanBrief.UtmSource = loanCredit.UtmSource;
											loanBrief.UtmTerm = loanCredit.UtmTerm;
											loanBrief.Version = 1;
											if (loanCredit.WardId > 0)
												loanBrief.WardId = loanCredit.WardId;
											los.LoanBrief.SingleUpdate(loanBrief);
										}
										#endregion
										#region Loan Brief Note > chuyen rieng
										//var comments = agParr.TblCommentCredit.Where(x => x.LoanCreditId == loanCredit.Id).ToList();
										//if (comments != null && comments.Count > 0)
										//{
										//	try
										//	{
										//		los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanBriefNote ON");
										//		foreach (var comment in comments)
										//		{
										//			if (!los.LoanBriefNote.Any(x => x.LoanBriefNoteId == comment.Id))
										//			{
										//				LoanBriefNote note = new LoanBriefNote();
										//				note.ActionComment = comment.ActionId;
										//				note.CreatedTime = comment.CreateDate;
										//				note.FullName = comment.FullName;
										//				note.IsDisplay = comment.IsDisplay == 1 ? true : false;
										//				note.IsManual = comment.IsManual == 1 ? true : false;
										//				note.LoanBriefId = loanCredit.Id;
										//				note.LoanBriefNoteId = comment.Id;
										//				note.Note = comment.Comment;
										//				note.ShopId = comment.ShopId;
										//				note.Status = 1;
										//				//note.Type = null;
										//				note.UserId = comment.UserId;
										//				los.LoanBriefNote.Add(note);
										//			}
										//			else
										//			{
										//				LoanBriefNote note = los.LoanBriefNote.Find(comment.Id);
										//				note.ActionComment = comment.ActionId;
										//				note.CreatedTime = comment.CreateDate;
										//				note.FullName = comment.FullName;
										//				note.IsDisplay = comment.IsDisplay == 1 ? true : false;
										//				note.IsManual = comment.IsManual == 1 ? true : false;
										//				note.LoanBriefId = loanCredit.Id;
										//				note.LoanBriefNoteId = comment.Id;
										//				note.Note = comment.Comment;
										//				note.ShopId = comment.ShopId;
										//				note.Status = 1;
										//				//note.Type = null;
										//				note.UserId = comment.UserId;
										//				los.Entry(note).State = EntityState.Modified;
										//			}
										//		}
										//		los.SaveChanges();
										//	}
										//	catch (Exception ex)
										//	{
										//		Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
										//		if (ex.InnerException != null)
										//			Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
										//	}
										//	finally
										//	{
										//		los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanBriefNote OFF");
										//	}											
										//}
										#endregion
										#region Loan Brief Job
										try
										{
											var job = los.LoanBriefJob.Find(loanCredit.Id);
											if (job == null)
											{
												job = new LoanBriefJob();
												//job.BankName = null;
												job.BusinessType = mCustomer.TypeCompany;
												//job.CardNumber = null;
												job.CompanyAddress = mCustomer.AddressCompany;
												//job.CompanyAddressGoogleMap = null;
												job.CompanyAddressLatLng = mCustomer.LatLongAddressCompany;
												if (mCustomer.CompanyDistrictId > 0)
													job.CompanyDistrictId = mCustomer.CompanyDistrictId;
												job.CompanyName = mCustomer.CompanyName;
												job.CompanyPhone = mCustomer.CompanyPhone;
												if (mCustomer.CompanyCityId > 0)
													job.CompanyProvinceId = mCustomer.CompanyCityId;
												job.CompanyTaxCode = mCustomer.CompanyTaxCode;
												if (mCustomer.CompanyWardId > 0 && los.Ward.Find(mCustomer.CompanyWardId) != null)
													job.CompanyWardId = mCustomer.CompanyWardId;
												//job.ContractRemain = null;
												//job.ContractType = null;
												//job.DepartmentName = null;
												job.Description = mCustomer.DescriptionPositionJob;
												job.DocumentAvailable = null;
												job.ImcomeType = mCustomer.ReceiveYourIncome;
												if (mCustomer.JobId > 0 && los.Job.Any(x => x.JobId == mCustomer.JobId))
													job.JobId = mCustomer.JobId;
												job.LoanBriefJobId = loanCredit.Id;
												job.PaidType = null;
												job.TotalIncome = mCustomer.Salary;
												job.WorkingAddress = mCustomer.AddressCompany;
												job.WorkingTime = null;
												los.LoanBriefJob.SingleInsert(job);
											}
											else
											{
												//job.BankName = null;
												job.BusinessType = mCustomer.TypeCompany;
												//job.CardNumber = null;
												job.CompanyAddress = mCustomer.AddressCompany;
												//job.CompanyAddressGoogleMap = null;
												job.CompanyAddressLatLng = mCustomer.LatLongAddressCompany;
												if (mCustomer.CompanyDistrictId > 0)
													job.CompanyDistrictId = mCustomer.CompanyDistrictId;
												job.CompanyName = mCustomer.CompanyName;
												job.CompanyPhone = mCustomer.CompanyPhone;
												if (mCustomer.CompanyCityId > 0)
													job.CompanyProvinceId = mCustomer.CompanyCityId;
												job.CompanyTaxCode = mCustomer.CompanyTaxCode;
												if (mCustomer.CompanyWardId > 0 && los.Ward.Find(mCustomer.CompanyWardId) != null)
													job.CompanyWardId = mCustomer.CompanyWardId;
												//job.ContractRemain = null;
												//job.ContractType = null;
												//job.DepartmentName = null;
												job.Description = mCustomer.DescriptionPositionJob;
												job.DocumentAvailable = null;
												job.ImcomeType = mCustomer.ReceiveYourIncome;
												if (mCustomer.JobId > 0 && los.Job.Any(x => x.JobId == mCustomer.JobId))
													job.JobId = mCustomer.JobId;
												job.LoanBriefJobId = loanCredit.Id;
												job.PaidType = null;
												job.TotalIncome = mCustomer.Salary;
												job.WorkingAddress = mCustomer.AddressCompany;
												job.WorkingTime = null;
												los.LoanBriefJob.SingleUpdate(job);
											}
										}
										catch (Exception ex)
										{
											Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
											if (ex.InnerException != null)
												Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
										}
										#endregion
										#region Loan Brief Resident
										if (los.LoanBriefResident.Find(loanCredit.Id) == null)
										{
											LoanBriefResident resident = new LoanBriefResident();
											resident.AddressLatLng = mCustomer.LatLongAddress;
											resident.Address = mCustomer.Street;
											//resident.AddressGoogleMap = null;
											resident.AppartmentNumber = mCustomer.LivingApartmentNumber;
											//resident.Block = null;
											//resident.Direction = null;
											if (mCustomer.DistrictId > 0)
												resident.DistrictId = mCustomer.DistrictId;
											resident.Lane = null;
											resident.LivingTime = mCustomer.LivingTimeId;
											resident.LivingWith = null;
											resident.LoanBriefResidentId = loanCredit.Id;
											resident.Ownership = mCustomer.TypeOfOwnershipId;
											if (mCustomer.CityId > 0)
												resident.ProvinceId = mCustomer.CityId;
											//resident.ResidentType = null;
											if (mCustomer.WardId > 0)
												resident.WardId = mCustomer.WardId;
											los.LoanBriefResident.SingleInsert(resident);
										}
										else
										{
											LoanBriefResident resident = los.LoanBriefResident.Find(loanCredit.Id);
											resident.AddressLatLng = mCustomer.LatLongAddress;
											resident.Address = mCustomer.Street;
											//resident.AddressGoogleMap = null;
											resident.AppartmentNumber = mCustomer.LivingApartmentNumber;
											//resident.Block = null;
											//resident.Direction = null;
											if (mCustomer.DistrictId > 0)
												resident.DistrictId = mCustomer.DistrictId;
											resident.Lane = null;
											resident.LivingTime = mCustomer.LivingTimeId;
											resident.LivingWith = null;
											resident.LoanBriefResidentId = loanCredit.Id;
											resident.Ownership = mCustomer.TypeOfOwnershipId;
											if (mCustomer.CityId > 0)
												resident.ProvinceId = mCustomer.CityId;
											//resident.ResidentType = null;
											if (mCustomer.WardId > 0)
												resident.WardId = mCustomer.WardId;
											los.LoanBriefResident.SingleUpdate(resident);
										}
										#endregion
										#region Loan Brief Household
										if (los.LoanBriefHousehold.Find(loanCredit.Id) == null)
										{
											LoanBriefHousehold household = new LoanBriefHousehold();
											household.Address = mCustomer.AddressHouseHold;
											//household.AppartmentNumber = null;
											//household.Block = null;
											//household.DistrictId = null;
											//household.Lane = null;
											household.LoanBriefHouseholdId = loanCredit.Id;
											household.NoOfFamilyMembers = null;
											household.Ownership = mCustomer.TypeOfOwnershipId;
											//household.ProvinceId = null;
											household.Status = null;
											//household.WardId = null;
											los.LoanBriefHousehold.SingleInsert(household);
										}
										else
										{
											LoanBriefHousehold household = los.LoanBriefHousehold.Find(loanCredit.Id);
											household.Address = mCustomer.AddressHouseHold;
											//household.AppartmentNumber = null;
											//household.Block = null;
											//household.DistrictId = null;
											//household.Lane = null;
											household.LoanBriefHouseholdId = loanCredit.Id;
											household.NoOfFamilyMembers = null;
											household.Ownership = mCustomer.TypeOfOwnershipId;
											//household.ProvinceId = null;
											household.Status = null;
											//household.WardId = null;
											los.LoanBriefHousehold.SingleUpdate(household);
										}
										#endregion
										#region Loan Brief Relationship
										if (mCustomer.RelativeFamilyId > 0)
										{
											try
											{
												if (los.RelativeFamily.Find(mCustomer.RelativeFamilyId) != null)
												{
													var relationship = los.LoanBriefRelationship.Where(x => x.LoanBriefId == loanCredit.Id && x.RelationshipType == mCustomer.RelativeFamilyId).FirstOrDefault();
													if (relationship == null)
													{
														relationship = new LoanBriefRelationship();
														// relationship.Address = null;
														relationship.CardNumber = null;
														relationship.CreatedDate = null;
														relationship.FullName = mCustomer.FullNameFamily;
														relationship.LoanBriefId = loanCredit.Id;
														relationship.Phone = mCustomer.PhoneFamily;
														relationship.RefPhoneCallRate = null;
														relationship.RefPhoneDurationRate = null;
														relationship.RelationshipType = mCustomer.RelativeFamilyId;
														los.LoanBriefRelationship.SingleInsert(relationship);
													}
													else
													{
														// relationship.Address = null;
														relationship.CardNumber = null;
														relationship.CreatedDate = null;
														relationship.FullName = mCustomer.FullNameFamily;
														relationship.LoanBriefId = loanCredit.Id;
														relationship.Phone = mCustomer.PhoneFamily;
														relationship.RefPhoneCallRate = null;
														relationship.RefPhoneDurationRate = null;
														relationship.RelationshipType = mCustomer.RelativeFamilyId;
														los.LoanBriefRelationship.SingleUpdate(relationship);
													}
												}
											}
											catch (Exception ex)
											{

											}
										}
										if (mCustomer.RelativeFamilyId1 > 0)
										{
											try
											{
												if (los.RelativeFamily.Find(mCustomer.RelativeFamilyId) != null)
												{
													var relationship = los.LoanBriefRelationship.Where(x => x.LoanBriefId == loanCredit.Id && x.RelationshipType == mCustomer.RelativeFamilyId1).FirstOrDefault();
													if (relationship == null)
													{
														relationship = new LoanBriefRelationship();
														// relationship.Address = null;
														relationship.CardNumber = null;
														relationship.CreatedDate = null;
														relationship.FullName = mCustomer.FullNameFamily1;
														relationship.LoanBriefId = loanCredit.Id;
														relationship.Phone = mCustomer.PhoneFamily1;
														relationship.RefPhoneCallRate = null;
														relationship.RefPhoneDurationRate = null;
														relationship.RelationshipType = mCustomer.RelativeFamilyId1;
														los.LoanBriefRelationship.SingleInsert(relationship);
													}
													else
													{
														// relationship.Address = null;
														relationship.CardNumber = null;
														relationship.CreatedDate = null;
														relationship.FullName = mCustomer.FullNameFamily;
														relationship.LoanBriefId = loanCredit.Id;
														relationship.Phone = mCustomer.PhoneFamily;
														relationship.RefPhoneCallRate = null;
														relationship.RefPhoneDurationRate = null;
														relationship.RelationshipType = mCustomer.RelativeFamilyId;
														los.LoanBriefRelationship.SingleUpdate(relationship);
													}
												}
											}
											catch (Exception ex)
											{

											}
										}
										#endregion
										#region Loan Brief Property
										try
										{
											if (mCustomer.ModelPhone != null && !mCustomer.Equals(""))
											{
												var product = los.Product.Where(x => x.FullName.Contains(mCustomer.ModelPhone)).FirstOrDefault();
												if (product != null)
												{
													var property = los.LoanBriefProperty.Find(loanCredit.Id);
													if (property == null)
													{
														property = new LoanBriefProperty();
														//property.Age = null;
														property.BrandId = product.BrandProductId;
														property.CarManufacturer = null;
														property.CarName = null;
														//property.CreatedTime = null;
														property.DocumentAvailable = null;
														property.LoanBriefPropertyId = loanCredit.Id;
														//property.Name = null;
														//property.Ownership = null;
														property.ProductId = product.Id;
														property.PropertyType = null;
														property.UpdatedTime = null;
														los.LoanBriefProperty.SingleUpdate(property);
													}
													else
													{
														//property.Age = null;
														property.BrandId = product.BrandProductId;
														property.CarManufacturer = null;
														property.CarName = null;
														//property.CreatedTime = null;
														property.DocumentAvailable = null;
														property.LoanBriefPropertyId = loanCredit.Id;
														//property.Name = null;
														//property.Ownership = null;													
														property.ProductId = product.Id;
														property.PropertyType = null;
														property.UpdatedTime = null;
														los.LoanBriefProperty.SingleUpdate(property);
													}
												}
											}
										}
										catch (Exception ex)
										{
											Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
											if (ex.InnerException != null)
												Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
										}
										#endregion
										#region Loan Brief company > chuyen rieng
										//var business = agParr.TblInformationOfBusiness.Where(x => x.LoanCreditId == loanCredit.Id).FirstOrDefault();
										//if (business != null)
										//{
										//	var company = los.LoanBriefCompany.Where(x => x.LoanBriefId == loanCredit.Id).FirstOrDefault();
										//	if (company == null)
										//	{
										//		company = new LoanBriefCompany();
										//		company.Address = business.BusinessAddress;
										//		company.BirthdayShareholder = business.Birthday;
										//		company.BusinessCertificationAddress = business.BusinessAddress;
										//		company.BusinessCertificationDate = business.DateCertificate;
										//		company.CardNumberShareholder = business.CardNumber;
										//		company.CardNumberShareholderDate = business.DateCardNumber;
										//		company.CareerBusiness = business.MainBusinessSectors;
										//		company.CompanyName = business.CompanyName;
										//		company.CompanyShareholder = business.Shareholder;
										//		company.CreatedDate = DateTime.Now;
										//		company.HeadOffice = business.HeadquarterBusinessRegistration;
										//		company.LoanBriefId = loanCredit.Id;
										//		company.PlaceOfBirthShareholder = null;
										//		company.UpdatedDate = DateTime.Now;
										//		los.LoanBriefCompany.Add(company);
										//		los.SaveChanges();
										//	}
										//	else
										//	{
										//		company.Address = business.BusinessAddress;
										//		company.BirthdayShareholder = business.Birthday;
										//		company.BusinessCertificationAddress = business.BusinessAddress;
										//		company.BusinessCertificationDate = business.DateCertificate;
										//		company.CardNumberShareholder = business.CardNumber;
										//		company.CardNumberShareholderDate = business.DateCardNumber;
										//		company.CareerBusiness = business.MainBusinessSectors;
										//		company.CompanyName = business.CompanyName;
										//		company.CompanyShareholder = business.Shareholder;
										//		company.CreatedDate = DateTime.Now;
										//		company.HeadOffice = business.HeadquarterBusinessRegistration;
										//		company.LoanBriefId = loanCredit.Id;
										//		company.PlaceOfBirthShareholder = null;
										//		company.UpdatedDate = DateTime.Now;
										//		los.Entry(company).State = EntityState.Modified;
										//		los.SaveChanges();
										//	}
										//}
										#endregion
										#region Loan Brief Document > Chuyen rieng
										//var files = agParr.TblCustomerCreditFiles.Where(x => x.LoanCreditId == loanCredit.Id).ToList();
										//if (files != null && files.Count > 0)
										//{
										//	try
										//	{
										//		los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanBriefFiles ON");
										//		foreach (var file in files)
										//		{
										//			var loanBriefFile = los.LoanBriefFiles.Find(file.Id);
										//			if (loanBriefFile == null)
										//			{
										//				loanBriefFile = new LoanBriefFiles();
										//				loanBriefFile.CreateAt = file.CreateOn;
										//				loanBriefFile.IsDeleted = file.DeleteStatus;
										//				loanBriefFile.DomainOfPartner = file.DomainOfPartner;
										//				loanBriefFile.FilePath = file.UrlImg;
										//				loanBriefFile.Id = file.Id;
										//				loanBriefFile.LinkImgOfPartner = file.LinkImgOfPartner;
										//				loanBriefFile.LoanBriefId = loanCredit.Id;
										//				//loanBriefFile.S3status = null;
										//				loanBriefFile.Status = loanCredit.Status;
										//				//loanBriefFile.Synchronize = null;
										//				loanBriefFile.TypeFile = file.TypeFile;
										//				if (file.TypeId > 0)
										//					loanBriefFile.TypeId = file.TypeId;
										//				loanBriefFile.UserId = file.UserId;
										//				los.LoanBriefFiles.Add(loanBriefFile);
										//				// create Loan Brief Document ??
										//			}
										//			else
										//			{
										//				loanBriefFile.CreateAt = file.CreateOn;
										//				loanBriefFile.IsDeleted = file.DeleteStatus;
										//				loanBriefFile.DomainOfPartner = file.DomainOfPartner;
										//				loanBriefFile.FilePath = file.UrlImg;
										//				loanBriefFile.Id = file.Id;
										//				loanBriefFile.LinkImgOfPartner = file.LinkImgOfPartner;
										//				loanBriefFile.LoanBriefId = loanCredit.Id;
										//				//loanBriefFile.S3status = null;
										//				loanBriefFile.Status = loanCredit.Status;
										//				//loanBriefFile.Synchronize = null;
										//				loanBriefFile.TypeFile = file.TypeFile;
										//				if (file.TypeId > 0)
										//					loanBriefFile.TypeId = file.TypeId;
										//				loanBriefFile.UserId = file.UserId;
										//				los.Entry(loanBriefFile).State = EntityState.Modified;
										//				// create Loan Brief Document ??
										//			}
										//		}
										//		los.SaveChanges();												
										//	}
										//	catch (Exception ex)
										//	{
										//		Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
										//		if (ex.InnerException != null)
										//			Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
										//	}
										//	finally
										//	{
										//		los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanBriefFiles OFF");
										//	}
										//}
										#endregion
										#region update ag
										//loanCredit.SyncTime = DateTime.Now;										
										//agParr.Attach(loanCredit);
										//agParr.Entry(loanCredit).State = EntityState.Modified;
										//agParr.SaveChanges();
										#endregion
										los.Database.CommitTransaction();
									}
								}
								catch (Exception ex)
								{
									Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
									if (ex.InnerException != null)
										Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
									//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Customer OFF");
									//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanBrief OFF");
									//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanBriefNote OFF");
									//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanBriefFiles OFF");
									//los.Database.RollbackTransaction();
								}
							}
						}
					});
					page++;
				}
			}
			Console.WriteLine("Finish synchronizing Loan Briefs");
		}

		static void SyncLoanBrief(int startId)
		{
			Console.WriteLine("Begin synchronizing Loan Briefs");
			List<int> timing = new List<int>();
			int count = ag.TblLoanCredit.Where(x => x.Id > startId && x.Id <= 1100000).Count();
			//int count = 1;
			var page = (count / 5000) + 1;
			var pageSize = 5000;
			var loanStatuses = los.LoanStatus.ToList();
			for (int i = 1; i < page + 1; i++)
			{
				var loanCredits = ag.TblLoanCredit.AsNoTracking().Where(x => x.Id > startId && x.Id <= 1100000).Skip((i - 1) * pageSize).Take(pageSize).ToList();
				//var loanCredits = ag.TblLoanCredit.AsNoTracking().Where(x => x.Id == 275239).Skip((i - 1) * pageSize).Take(pageSize).ToList();
				//var lbs = new List<LoanBrief>();
				//var lbjs = new List<LoanBriefJob>();
				//var lbss = new List<LoanBriefResident>();
				//var lbhs = new List<LoanBriefHousehold>();
				//var lbrs = new List<LoanBriefRelationship>();
				//var lbps = new List<LoanBriefProperty>();			
				loanCredits.AsyncParallelForEach(async loanCredit =>
				{
					Console.WriteLine($"Processing entry '{loanCredit.Id}'");

					using (var los = new LOSContext())
					{
						using (var ag = new MecashContext())
						{
							var mCustomer = ag.TblCustomerCredit.Find(loanCredit.CustomerCreditId);
							if (mCustomer != null)
							{
								#region Loan Brief		
								try
								{
									var loanBrief = new LoanBrief();
									loanBrief.ActionState = null;
									loanBrief.AddedToQueue = null;
									//loanBrief.AffCode = null;
									loanBrief.AffCode = loanCredit.AffCode;
									//loanBrief.AffCode = null;
									loanBrief.AffSid = loanCredit.AffSid;
									//loanBrief.AffStatus = null;
									loanBrief.AffSource = loanCredit.AffSource;
									loanBrief.AffPub = loanCredit.AffPub;
									//loanBrief.AutomaticProcessed = null;
									loanBrief.BankAccountName = loanCredit.NameCardHolder;
									loanBrief.BankAccountNumber = loanCredit.AccountNumberBanking;
									loanBrief.BankCardNumber = loanCredit.NumberCardBanking;
									if (loanCredit.BankId > 0)
										loanBrief.BankId = loanCredit.BankId;
									loanBrief.BeginStartTime = loanCredit.BeginStartTime;
									if (loanCredit.SupportLastId > 0)
										loanBrief.BoundTelesaleId = loanCredit.SupportLastId;
									loanBrief.BuyInsurenceCustomer = loanCredit.CustomerBuyInsurance;
									loanBrief.CheckCic = loanCredit.IsCheckCic.HasValue ? (loanCredit.IsCheckCic.Value ? 1 : 0) : 0;
									//loanBrief.CheckLocation = null;
									if (loanCredit.RateConsultant > 0)
										loanBrief.ConsultantRate = (double)loanCredit.RateConsultant;
									//loanBrief.ContractGinno = null;
									//loanBrief.CoordinatorPushAt = null;
									if (loanCredit.CoordinatorId > 0)
										loanBrief.CoordinatorUserId = loanCredit.CoordinatorId;
									loanBrief.CreatedTime = loanCredit.CreateDate;
									loanBrief.CreateBy = loanCredit.CreatedBy;
									loanBrief.CustomerId = loanCredit.CustomerCreditId;
									loanBrief.DeviceId = loanCredit.DeviceId;
									loanBrief.DeviceStatus = loanCredit.StatusOfDeviceId;
									loanBrief.Dob = mCustomer.Birthday;
									if (loanCredit.DistrictId > 0)
										loanBrief.DistrictId = loanCredit.DistrictId;
									loanBrief.Email = mCustomer.Email;
									if (loanCredit.CompanyFieldSurveyUserId > 0)
										loanBrief.EvaluationCompanyUserId = loanCredit.CompanyFieldSurveyUserId;
									loanBrief.EvaluationHomeState = loanCredit.AddressFieldSurveyStatus;
									loanBrief.EvaluationHomeTime = loanCredit.AddressFieldSurveyTime;
									if (loanCredit.CompanyFieldSurveyUserId > 0)
										loanBrief.EvaluationCompanyUserId = loanCredit.CompanyFieldSurveyUserId;
									loanBrief.EvaluationCompanyTime = loanCredit.CompanyFieldSurveyTime;
									loanBrief.EvaluationCompanyState = loanCredit.CompanyFieldSurveyStatus;
									loanBrief.FirstProcessingTime = loanCredit.FirstModifyDate;
									loanBrief.Frequency = loanCredit.Frequency;
									loanBrief.FromDate = loanCredit.FromDate;
									loanBrief.FullName = mCustomer.FullName;
									loanBrief.Gender = mCustomer.Gender;
									loanBrief.DomainName = loanCredit.DomainName;
									//loanBrief.HomeNetwork = null;											
									if (loanCredit.Code > 0)
										loanBrief.HubId = loanCredit.Code;
									if (loanCredit.HubEmployeId > 0)
										loanBrief.HubEmployeeId = loanCredit.HubEmployeId;
									loanBrief.IsHeadOffice = loanCredit.IsHeadOffice.Value;
									loanBrief.IsLocate = loanCredit.IsLocate;
									loanBrief.IsLock = null;
									//loanBrief.IsLock = null;
									//loanBrief.IsNeeded = null;
									loanBrief.IsReMarketing = loanCredit.ReMarketing;
									loanBrief.IsTrackingLocation = loanCredit.IsTrackingLocation;
									//loanBrief.IsUploadState = null;
									loanBrief.IsVerifyOtpLdp = loanCredit.IsVerifyOtpLdp;
									loanBrief.LabelRequestLocation = loanCredit.LabelLocation;
									loanBrief.LabelResponseLocation = loanCredit.LabelVerifyLocation;
									//loanBrief.LastSendRequestActive = null;						
									if (loanCredit.OwerShopId > 0)
										loanBrief.LenderId = loanCredit.OwerShopId;
									loanBrief.LoanAmountFirst = loanCredit.TotalMoneyFirst;
									loanBrief.LoanAmountExpertise = loanCredit.TotalMoneyExpertise;
									loanBrief.LoanAmountExpertiseLast = loanCredit.TotalMoneyExpertiseLast;
									if (loanCredit.Status >= 35 && loanCredit.LoanId > 0)
									{
										var loan = ag.TblLoan.Where(x => x.Id == loanCredit.LoanId).FirstOrDefault();
										if (loan != null)
										{
											loanBrief.LoanAmount = loan.TotalMoney;
										}
									}
									else
									{
										loanBrief.LoanAmount = loanCredit.TotalMoney;
									}
									loanBrief.LoanBriefCancelAt = loanCredit.CancelDateLoanCredit;
									loanBrief.LoanBriefCancelBy = loanCredit.UserIdcancel;
									//loanBrief.LoanBriefComment = null;
									loanBrief.LoanBriefId = loanCredit.Id;
									loanBrief.LoanPurpose = loanCredit.LoanPurpose;
									if (loanCredit.Rate > 0)
										loanBrief.LoanRate = (double)loanCredit.Rate;
									loanBrief.LoanTime = loanCredit.LoanTime;
									//loanBrief.LogLoanInfoAi = null;
									//loanBrief.LogRequestAi = null;							
									loanBrief.MecashId = loanCredit.Id;
									loanBrief.NationalCard = mCustomer.CardNumber;
									loanBrief.NationalCardDate = null;
									loanBrief.NationCardPlace = null;
									loanBrief.NumberCall = loanCredit.CountCall;
									loanBrief.OrderSourceId = loanCredit.OrderSourceId;
									loanBrief.OrderSourceParentId = loanCredit.OrderSourceParentId;
									loanBrief.Phone = mCustomer.Phone;
									//loanBrief.PipelineState = null;
									loanBrief.PlatformType = loanCredit.PlatformType;
									loanBrief.PriorityContract = loanCredit.PriorityContract;
									if (loanCredit.TypeId > 0)
										loanBrief.ProductId = loanCredit.TypeId;
									if (loanCredit.CityId > 0)
										loanBrief.ProvinceId = loanCredit.CityId;
									if (loanCredit.ListCheckLeadQualify != null && !loanCredit.ListCheckLeadQualify.Equals(""))
										loanBrief.ValueCheckQualify = ConvertQualify(loanCredit.ListCheckLeadQualify);
									loanBrief.RateTypeId = loanCredit.RateType;
									loanBrief.ReasonCancel = loanCredit.ReasonId;
									loanBrief.ReceivingMoneyType = loanCredit.TypeReceivingMoney;
									//loanBrief.RefCodeLocation = loanCredit.RefCodeLocation;
									//loanBrief.RefCodeStatus = loanCredit.RefCodeStatus;
									loanBrief.ReMarketingLoanBriefId = loanCredit.ReMarketingLoanCreditId;
									//loanBrief.ResultCic = null;
									loanBrief.ResultLocation = loanCredit.ResultLocation;
									loanBrief.ResultRuleCheck = loanCredit.ResultCac;
									//loanBrief.ScheduleTime = null;
									if (loanCredit.Score.HasValue)
										loanBrief.ScoreCredit = (int)loanCredit.Score.Value;
									if (loanCredit.RateService > 0)
										loanBrief.ServiceRate = (double)loanCredit.RateService;
									var status = loanStatuses.Where(x => x.MecashStatus == loanCredit.Status).FirstOrDefault();
									if (status != null)
										loanBrief.Status = status.Status;
									else
										loanBrief.Status = -99;
									if (loanCredit.Status == 35 || loanCredit.Status == 100)
									{
										// Pipeline đã kết thúc
										loanBrief.PipelineState = 9;
										loanBrief.CurrentPipelineId = null;
										loanBrief.CurrentSectionDetailId = null;
										loanBrief.CurrentSectionId = null;
										loanBrief.ActionState = null;
										loanBrief.InProcess = 0;
										loanBrief.AddedToQueue = false;
									}
									else
									{
										// đơn đang trong trạng thái chưa cho vay > 
										// xác định luồng hay cho chọn luồng đích
										loanBrief.PipelineState = (int)Const.ProcessType.MANUAL_PIPELINE;
									}
									//loanBrief.TelesalesPushAt = null;							
									loanBrief.Tid = loanCredit.Tid;
									loanBrief.ToDate = loanCredit.ToDate;
									loanBrief.TypeInsurence = loanCredit.TypeInsurance;
									int type = 0;
									if (!mCustomer.TypeCustomerCreditId.HasValue || mCustomer.TypeCustomerCreditId == 0)
										type = 1;
									else if (mCustomer.TypeCustomerCreditId == 1)
										type = 2;
									loanBrief.TypeLoanBrief = type;
									loanBrief.UpdatedTime = loanCredit.ModifyDate;
									loanBrief.UtmCampaign = loanCredit.UtmCampaign;
									loanBrief.UtmContent = loanCredit.UtmContent;
									loanBrief.UtmMedium = loanCredit.UtmMedium;
									loanBrief.UtmSource = loanCredit.UtmSource;
									loanBrief.UtmTerm = loanCredit.UtmTerm;
									loanBrief.Version = 1;
									if (loanCredit.WardId > 0)
										loanBrief.WardId = loanCredit.WardId;
									los.LoanBrief.SingleInsert(loanBrief, options => options.InsertKeepIdentity = true);
								}
								catch (Exception ex)
								{
									Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
									if (ex.InnerException != null)
										Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
								}
								#endregion
							}
							else
							{
								// không có customer ??
								#region Loan Brief		
								try
								{
									var loanBrief = new LoanBrief();
									loanBrief.ActionState = null;
									loanBrief.AddedToQueue = null;
									//loanBrief.AffCode = null;
									loanBrief.AffCode = loanCredit.AffCode;
									//loanBrief.AffCode = null;
									loanBrief.AffSid = loanCredit.AffSid;
									//loanBrief.AffStatus = null;
									loanBrief.AffSource = loanCredit.AffSource;
									loanBrief.AffPub = loanCredit.AffPub;
									//loanBrief.AutomaticProcessed = null;
									loanBrief.BankAccountName = loanCredit.NameCardHolder;
									loanBrief.BankAccountNumber = loanCredit.AccountNumberBanking;
									loanBrief.BankCardNumber = loanCredit.NumberCardBanking;
									if (loanCredit.BankId > 0)
										loanBrief.BankId = loanCredit.BankId;
									loanBrief.BeginStartTime = loanCredit.BeginStartTime;
									if (loanCredit.SupportLastId > 0)
										loanBrief.BoundTelesaleId = loanCredit.SupportLastId;
									loanBrief.BuyInsurenceCustomer = loanCredit.CustomerBuyInsurance;
									loanBrief.CheckCic = loanCredit.IsCheckCic.HasValue ? (loanCredit.IsCheckCic.Value ? 1 : 0) : 0;
									//loanBrief.CheckLocation = null;
									if (loanCredit.RateConsultant > 0)
										loanBrief.ConsultantRate = (double)loanCredit.RateConsultant;
									//loanBrief.ContractGinno = null;
									//loanBrief.CoordinatorPushAt = null;
									if (loanCredit.CoordinatorId > 0)
										loanBrief.CoordinatorUserId = loanCredit.CoordinatorId;
									loanBrief.CreatedTime = loanCredit.CreateDate;
									loanBrief.CreateBy = loanCredit.CreatedBy;
									loanBrief.CustomerId = loanCredit.CustomerCreditId;
									loanBrief.DeviceId = loanCredit.DeviceId;
									loanBrief.DeviceStatus = loanCredit.StatusOfDeviceId;
									//loanBrief.Dob = null;
									if (loanCredit.DistrictId > 0)
										loanBrief.DistrictId = loanCredit.DistrictId;
									loanBrief.Email = null;
									if (loanCredit.CompanyFieldSurveyUserId > 0)
										loanBrief.EvaluationCompanyUserId = loanCredit.CompanyFieldSurveyUserId;
									loanBrief.EvaluationHomeState = loanCredit.AddressFieldSurveyStatus;
									loanBrief.EvaluationHomeTime = loanCredit.AddressFieldSurveyTime;
									if (loanCredit.CompanyFieldSurveyUserId > 0)
										loanBrief.EvaluationCompanyUserId = loanCredit.CompanyFieldSurveyUserId;
									loanBrief.EvaluationCompanyTime = loanCredit.CompanyFieldSurveyTime;
									loanBrief.EvaluationCompanyState = loanCredit.CompanyFieldSurveyStatus;
									loanBrief.Frequency = loanCredit.Frequency;
									loanBrief.FromDate = loanCredit.FromDate;
									//loanBrief.FullName = null;
									//loanBrief.Gender = null;
									loanBrief.DomainName = loanCredit.DomainName;
									//loanBrief.HomeNetwork = null;											
									if (loanCredit.Code > 0)
										loanBrief.HubId = loanCredit.Code;
									if (loanCredit.HubEmployeId > 0)
										loanBrief.HubEmployeeId = loanCredit.HubEmployeId;
									loanBrief.IsHeadOffice = loanCredit.IsHeadOffice;
									loanBrief.IsLocate = loanCredit.IsLocate;
									loanBrief.IsLock = null;
									//loanBrief.IsLock = null;
									//loanBrief.IsNeeded = null;
									loanBrief.IsReMarketing = loanCredit.ReMarketing;
									loanBrief.IsTrackingLocation = loanCredit.IsTrackingLocation;
									//loanBrief.IsUploadState = null;
									loanBrief.IsVerifyOtpLdp = loanCredit.IsVerifyOtpLdp;
									loanBrief.LabelRequestLocation = loanCredit.LabelLocation;
									loanBrief.LabelResponseLocation = loanCredit.LabelVerifyLocation;
									//loanBrief.LastSendRequestActive = null;						
									if (loanCredit.OwerShopId > 0)
										loanBrief.LenderId = loanCredit.OwerShopId;
									loanBrief.LoanAmountFirst = loanCredit.TotalMoneyFirst;
									loanBrief.LoanAmountExpertise = loanCredit.TotalMoneyExpertise;
									loanBrief.LoanAmountExpertiseLast = loanCredit.TotalMoneyExpertiseLast;
									if (loanCredit.Status >= 35 && loanCredit.LoanId > 0)
									{
										var loan = ag.TblLoan.Where(x => x.Id == loanCredit.LoanId).FirstOrDefault();
										if (loan != null)
										{
											loanBrief.LoanAmount = loan.TotalMoney;
										}
									}
									else
									{
										loanBrief.LoanAmount = loanCredit.TotalMoney;
									}
									loanBrief.LoanBriefCancelAt = loanCredit.CancelDateLoanCredit;
									loanBrief.LoanBriefCancelBy = loanCredit.UserIdcancel;
									//loanBrief.LoanBriefComment = null;
									loanBrief.LoanBriefId = loanCredit.Id;
									loanBrief.LoanPurpose = loanCredit.LoanPurpose;
									if (loanCredit.Rate > 0)
										loanBrief.LoanRate = (double)loanCredit.Rate;
									loanBrief.LoanTime = loanCredit.LoanTime;
									//loanBrief.LogLoanInfoAi = null;
									//loanBrief.LogRequestAi = null;							
									loanBrief.MecashId = loanCredit.Id;
									//loanBrief.NationalCard = null;
									loanBrief.NationalCardDate = null;
									loanBrief.NationCardPlace = null;
									loanBrief.NumberCall = loanCredit.CountCall;
									loanBrief.OrderSourceId = loanCredit.OrderSourceId;
									loanBrief.OrderSourceParentId = loanCredit.OrderSourceParentId;
									//loanBrief.Phone = null;
									//loanBrief.PipelineState = null;
									loanBrief.PlatformType = loanCredit.PlatformType;
									loanBrief.PriorityContract = loanCredit.PriorityContract;
									if (loanCredit.TypeId > 0)
										loanBrief.ProductId = loanCredit.TypeId;
									if (loanCredit.CityId > 0)
										loanBrief.ProvinceId = loanCredit.CityId;
									if (loanCredit.ListCheckLeadQualify != null && !loanCredit.ListCheckLeadQualify.Equals(""))
										loanBrief.ValueCheckQualify = ConvertQualify(loanCredit.ListCheckLeadQualify);
									loanBrief.RateTypeId = loanCredit.RateType;
									loanBrief.ReasonCancel = loanCredit.ReasonId;
									loanBrief.ReceivingMoneyType = loanCredit.TypeReceivingMoney;
									//loanBrief.RefCodeLocation = loanCredit.RefCodeLocation;
									//loanBrief.RefCodeStatus = loanCredit.RefCodeStatus;
									loanBrief.ReMarketingLoanBriefId = loanCredit.ReMarketingLoanCreditId;
									//loanBrief.ResultCic = null;
									loanBrief.ResultLocation = loanCredit.ResultLocation;
									loanBrief.ResultRuleCheck = loanCredit.ResultCac;
									//loanBrief.ScheduleTime = null;
									if (loanCredit.Score.HasValue)
										loanBrief.ScoreCredit = (int)loanCredit.Score.Value;
									if (loanCredit.RateService > 0)
										loanBrief.ServiceRate = (double)loanCredit.RateService;
									var status = loanStatuses.Where(x => x.MecashStatus == loanCredit.Status).FirstOrDefault();
									if (status != null)
										loanBrief.Status = status.Status;
									else
										loanBrief.Status = -99;
									if (loanCredit.Status == 35 || loanCredit.Status == 100)
									{
										// Pipeline đã kết thúc
										loanBrief.PipelineState = 9;
										loanBrief.CurrentPipelineId = null;
										loanBrief.CurrentSectionDetailId = null;
										loanBrief.CurrentSectionId = null;
										loanBrief.ActionState = null;
										loanBrief.InProcess = 0;
										loanBrief.AddedToQueue = false;
									}
									else
									{
										// đơn đang trong trạng thái chưa cho vay > 
										// xác định luồng hay cho chọn luồng đích
										loanBrief.PipelineState = (int)Const.ProcessType.MANUAL_PIPELINE;
									}
									//loanBrief.TelesalesPushAt = null;							
									loanBrief.Tid = loanCredit.Tid;
									loanBrief.ToDate = loanCredit.ToDate;
									loanBrief.TypeInsurence = loanCredit.TypeInsurance;
									int type = 0;
									loanBrief.TypeLoanBrief = type;
									loanBrief.UpdatedTime = loanCredit.ModifyDate;
									loanBrief.UtmCampaign = loanCredit.UtmCampaign;
									loanBrief.UtmContent = loanCredit.UtmContent;
									loanBrief.UtmMedium = loanCredit.UtmMedium;
									loanBrief.UtmSource = loanCredit.UtmSource;
									loanBrief.UtmTerm = loanCredit.UtmTerm;
									loanBrief.Version = 1;
									if (loanCredit.WardId > 0)
										loanBrief.WardId = loanCredit.WardId;
									//lbs.Add(loanBrief);
									los.LoanBrief.SingleInsert(loanBrief, options => options.InsertKeepIdentity = true);
								}
								catch (Exception ex)
								{
									Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
									if (ex.InnerException != null)
										Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
								}
								#endregion
							}
						}
					}
				}, 30);
				//foreach (var loanCredit in loanCredits)
				//{
				//	Console.WriteLine("Id:" + loanCredit.Id);
				//	var mCustomer = ag.TblCustomerCredit.Find(loanCredit.CustomerCreditId);
				//	if (mCustomer != null)
				//	{
				//		#region Loan Brief		
				//		try
				//		{
				//			var loanBrief = new LoanBrief();
				//			loanBrief.ActionState = null;
				//			loanBrief.AddedToQueue = null;
				//			//loanBrief.AffCode = null;
				//			loanBrief.AffCode1 = loanCredit.AffCode;
				//			//loanBrief.AffCode = null;
				//			loanBrief.AffSid = loanCredit.AffSid;
				//			//loanBrief.AffStatus = null;
				//			loanBrief.AffSource = loanCredit.AffSource;
				//			loanBrief.AffPub = loanCredit.AffPub;
				//			//loanBrief.AutomaticProcessed = null;
				//			loanBrief.BankAccountName = loanCredit.NameCardHolder;
				//			loanBrief.BankAccountNumber = loanCredit.AccountNumberBanking;
				//			loanBrief.BankCardNumber = loanCredit.NumberCardBanking;
				//			if (loanCredit.BankId > 0)
				//				loanBrief.BankId = loanCredit.BankId;
				//			loanBrief.BeginStartTime = loanCredit.BeginStartTime;
				//			if (loanCredit.SupportLastId > 0)
				//				loanBrief.BoundTelesaleId = loanCredit.SupportLastId;
				//			loanBrief.BuyInsurenceCustomer = loanCredit.CustomerBuyInsurance;
				//			loanBrief.CheckCic = loanCredit.IsCheckCic.HasValue ? (loanCredit.IsCheckCic.Value ? 1 : 0) : 0;
				//			//loanBrief.CheckLocation = null;
				//			if (loanCredit.RateConsultant > 0)
				//				loanBrief.ConsultantRate = (double)loanCredit.RateConsultant;
				//			//loanBrief.ContractGinno = null;
				//			//loanBrief.CoordinatorPushAt = null;
				//			if (loanCredit.CoordinatorId > 0)
				//				loanBrief.CoordinatorUserId = loanCredit.CoordinatorId;
				//			loanBrief.CreatedTime = loanCredit.CreateDate;
				//			loanBrief.CreateBy = loanCredit.CreatedBy;
				//			loanBrief.CustomerId = loanCredit.CustomerCreditId;
				//			loanBrief.DeviceId = loanCredit.DeviceId;
				//			loanBrief.DeviceStatus = loanCredit.StatusOfDeviceId;
				//			loanBrief.Dob = mCustomer.Birthday;
				//			if (loanCredit.DistrictId > 0)
				//				loanBrief.DistrictId = loanCredit.DistrictId;
				//			loanBrief.Email = mCustomer.Email;
				//			if (loanCredit.CompanyFieldSurveyUserId > 0)
				//				loanBrief.EvaluationCompanyUserId = loanCredit.CompanyFieldSurveyUserId;
				//			loanBrief.EvaluationHomeState = loanCredit.AddressFieldSurveyStatus;
				//			loanBrief.EvaluationHomeTime = loanCredit.AddressFieldSurveyTime;
				//			if (loanCredit.CompanyFieldSurveyUserId > 0)
				//				loanBrief.EvaluationCompanyUserId = loanCredit.CompanyFieldSurveyUserId;
				//			loanBrief.EvaluationCompanyTime = loanCredit.CompanyFieldSurveyTime;
				//			loanBrief.EvaluationCompanyState = loanCredit.CompanyFieldSurveyStatus;
				//			loanBrief.Frequency = loanCredit.Frequency;
				//			loanBrief.FromDate = loanCredit.FromDate;
				//			loanBrief.FullName = mCustomer.FullName;
				//			loanBrief.Gender = mCustomer.Gender;
				//			loanBrief.DomainName = loanCredit.DomainName;
				//			//loanBrief.HomeNetwork = null;											
				//			if (loanCredit.Code > 0)
				//				loanBrief.HubId = loanCredit.Code;
				//			if (loanCredit.HubEmployeId > 0)
				//				loanBrief.HubEmployeeId = loanCredit.HubEmployeId;
				//			loanBrief.IsHeadOffice = loanCredit.IsHeadOffice;
				//			loanBrief.IsLocate = loanCredit.IsLocate;
				//			loanBrief.IsLock = null;
				//			//loanBrief.IsLock = null;
				//			//loanBrief.IsNeeded = null;
				//			loanBrief.IsReMarketing = loanCredit.ReMarketing;
				//			loanBrief.IsTrackingLocation = loanCredit.IsTrackingLocation;
				//			//loanBrief.IsUploadState = null;
				//			loanBrief.IsVerifyOtpLdp = loanCredit.IsVerifyOtpLdp;
				//			loanBrief.LabelRequestLocation = loanCredit.LabelLocation;
				//			loanBrief.LabelResponseLocation = loanCredit.LabelVerifyLocation;
				//			//loanBrief.LastSendRequestActive = null;						
				//			if (loanCredit.OwerShopId > 0)
				//				loanBrief.LenderId = loanCredit.OwerShopId;
				//			loanBrief.LoanAmount = loanCredit.TotalMoneyFirst;
				//			loanBrief.LoanAmountExpertise = loanCredit.TotalMoneyExpertise;
				//			loanBrief.LoanAmountExpertiseLast = loanCredit.TotalMoneyExpertiseLast;
				//			loanBrief.LoanAmountFinal = loanCredit.TotalMoney;
				//			loanBrief.LoanBriefCancelAt = loanCredit.CancelDateLoanCredit;
				//			//loanBrief.LoanBriefCancelBy = null;
				//			//loanBrief.LoanBriefComment = null;
				//			loanBrief.LoanBriefId = loanCredit.Id;
				//			loanBrief.LoanPurpose = loanCredit.LoanPurpose;
				//			if (loanCredit.Rate > 0)
				//				loanBrief.LoanRate = (double)loanCredit.Rate;
				//			loanBrief.LoanTime = loanCredit.LoanTime;
				//			//loanBrief.LogLoanInfoAi = null;
				//			//loanBrief.LogRequestAi = null;							
				//			loanBrief.MecashId = loanCredit.Id;
				//			loanBrief.NationalCard = mCustomer.CardNumber;
				//			loanBrief.NationalCardDate = null;
				//			loanBrief.NationCardPlace = null;
				//			loanBrief.NumberCall = loanCredit.CountCall;
				//			loanBrief.OrderSourceId = loanCredit.OrderSourceId;
				//			loanBrief.OrderSourceParentId = loanCredit.OrderSourceParentId;
				//			loanBrief.Phone = mCustomer.Phone;
				//			//loanBrief.PipelineState = null;
				//			loanBrief.PlatformType = loanCredit.PlatformType;
				//			loanBrief.PriorityContract = loanCredit.PriorityContract;
				//			if (loanCredit.TypeId > 0)
				//				loanBrief.ProductId = loanCredit.TypeId;
				//			if (loanCredit.CityId > 0)
				//				loanBrief.ProvinceId = loanCredit.CityId;
				//			loanBrief.RateTypeId = loanCredit.RateType;
				//			loanBrief.ReasonCancel = loanCredit.ReasonId;
				//			loanBrief.ReceivingMoneyType = loanCredit.TypeReceivingMoney;
				//			loanBrief.RefCodeLocation = loanCredit.RefCodeLocation;
				//			loanBrief.RefCodeStatus = loanCredit.RefCodeStatus;
				//			loanBrief.ReMarketingLoanBriefId = loanCredit.ReMarketingLoanCreditId;
				//			//loanBrief.ResultCic = null;
				//			loanBrief.ResultLocation = loanCredit.ResultLocation;
				//			//loanBrief.ResultRuleCheck = null;
				//			//loanBrief.ScheduleTime = null;
				//			if (loanCredit.Score.HasValue)
				//				loanBrief.ScoreCredit = (int)loanCredit.Score.Value;
				//			if (loanCredit.RateService > 0)
				//				loanBrief.ServiceRate = (double)loanCredit.RateService;
				//			var status = los.LoanStatus.Where(x => x.MecashStatus == loanCredit.Status).FirstOrDefault();
				//			if (status != null)
				//				loanBrief.Status = status.Status;
				//			else
				//				loanBrief.Status = -99;
				//			if (loanCredit.Status == 35 || loanCredit.Status == 100)
				//			{
				//				// Pipeline đã kết thúc
				//				loanBrief.PipelineState = 9;
				//				loanBrief.CurrentPipelineId = null;
				//				loanBrief.CurrentSectionDetailId = null;
				//				loanBrief.CurrentSectionId = null;
				//				loanBrief.ActionState = null;
				//				loanBrief.InProcess = 0;
				//				loanBrief.AddedToQueue = false;
				//			}
				//			else
				//			{
				//				// đơn đang trong trạng thái chưa cho vay > 
				//				// xác định luồng hay cho chọn luồng đích
				//				loanBrief.PipelineState = (int)Const.ProcessType.MANUAL_PIPELINE;
				//			}
				//			//loanBrief.TelesalesPushAt = null;							
				//			loanBrief.Tid = loanCredit.Tid;
				//			loanBrief.ToDate = loanCredit.ToDate;
				//			loanBrief.TypeInsurence = loanCredit.TypeInsurance;
				//			int type = 0;
				//			if (!mCustomer.TypeCustomerCreditId.HasValue || mCustomer.TypeCustomerCreditId == 0)
				//				type = 1;
				//			else if (mCustomer.TypeCustomerCreditId == 1)
				//				type = 2;
				//			loanBrief.TypeLoanBrief = type;
				//			loanBrief.UpdatedTime = loanCredit.ModifyDate;
				//			loanBrief.UtmCampaign = loanCredit.UtmCampaign;
				//			loanBrief.UtmContent = loanCredit.UtmContent;
				//			loanBrief.UtmMedium = loanCredit.UtmMedium;
				//			loanBrief.UtmSource = loanCredit.UtmSource;
				//			loanBrief.UtmTerm = loanCredit.UtmTerm;
				//			loanBrief.Version = 1;
				//			if (loanCredit.WardId > 0)
				//				loanBrief.WardId = loanCredit.WardId;
				//			lbs.Add(loanBrief);
				//		}
				//		catch (Exception ex)
				//		{
				//			Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
				//			if (ex.InnerException != null)
				//				Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
				//		}
				//		#endregion
				//		//#region Loan Brief Job
				//		//try
				//		//{
				//		//	var job = new LoanBriefJob();
				//		//	//job.BankName = null;
				//		//	job.BusinessType = mCustomer.TypeCompany;
				//		//	//job.CardNumber = null;
				//		//	job.CompanyAddress = mCustomer.AddressCompany;
				//		//	//job.CompanyAddressGoogleMap = null;
				//		//	job.CompanyAddressLatLng = mCustomer.LatLongAddressCompany;
				//		//	if (mCustomer.CompanyDistrictId > 0)
				//		//		job.CompanyDistrictId = mCustomer.CompanyDistrictId;
				//		//	job.CompanyName = mCustomer.CompanyName;
				//		//	job.CompanyPhone = mCustomer.CompanyPhone;
				//		//	if (mCustomer.CompanyCityId > 0)
				//		//		job.CompanyProvinceId = mCustomer.CompanyCityId;
				//		//	job.CompanyTaxCode = mCustomer.CompanyTaxCode;
				//		//	if (mCustomer.CompanyWardId > 0)
				//		//		job.CompanyWardId = mCustomer.CompanyWardId;
				//		//	//job.ContractRemain = null;
				//		//	//job.ContractType = null;
				//		//	//job.DepartmentName = null;
				//		//	job.Description = mCustomer.DescriptionPositionJob;
				//		//	job.DocumentAvailable = null;
				//		//	job.ImcomeType = mCustomer.ReceiveYourIncome;
				//		//	if (mCustomer.JobId > 0)
				//		//		job.JobId = mCustomer.JobId;
				//		//	job.LoanBriefJobId = loanCredit.Id;
				//		//	job.PaidType = null;
				//		//	job.TotalIncome = mCustomer.Salary;
				//		//	job.WorkingAddress = mCustomer.AddressCompany;
				//		//	job.WorkingTime = null;
				//		//	lbjs.Add(job);
				//		//}
				//		//catch (Exception ex)
				//		//{
				//		//	Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
				//		//	if (ex.InnerException != null)
				//		//		Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
				//		//}
				//		//#endregion
				//		//#region Loan Brief Resident						
				//		//LoanBriefResident resident = new LoanBriefResident();
				//		//resident.AddressLatLng = mCustomer.LatLongAddress;
				//		//resident.Address = mCustomer.Street;
				//		////resident.AddressGoogleMap = null;
				//		//resident.AppartmentNumber = mCustomer.LivingApartmentNumber;
				//		////resident.Block = null;
				//		////resident.Direction = null;
				//		//if (mCustomer.DistrictId > 0)
				//		//	resident.DistrictId = mCustomer.DistrictId;
				//		//resident.Lane = null;
				//		//resident.LivingTime = mCustomer.LivingTimeId;
				//		//resident.LivingWith = null;
				//		//resident.LoanBriefResidentId = loanCredit.Id;
				//		//resident.Ownership = mCustomer.TypeOfOwnershipId;
				//		//if (mCustomer.CityId > 0)
				//		//	resident.ProvinceId = mCustomer.CityId;
				//		////resident.ResidentType = null;
				//		//if (mCustomer.WardId > 0)
				//		//	resident.WardId = mCustomer.WardId;
				//		//lbss.Add(resident);
				//		//#endregion
				//		//#region Loan Brief Household						
				//		//LoanBriefHousehold household = new LoanBriefHousehold();
				//		//household.Address = mCustomer.AddressHouseHold;
				//		////household.AppartmentNumber = null;
				//		////household.Block = null;
				//		////household.DistrictId = null;
				//		////household.Lane = null;
				//		//household.LoanBriefHouseholdId = loanCredit.Id;
				//		//household.NoOfFamilyMembers = null;
				//		//household.Ownership = mCustomer.TypeOfOwnershipId;
				//		////household.ProvinceId = null;
				//		//household.Status = null;
				//		////household.WardId = null;
				//		//lbhs.Add(household);
				//		//#endregion
				//		//#region Loan Brief Relationship
				//		//if (mCustomer.RelativeFamilyId > 0)
				//		//{
				//		//	var relationship = new LoanBriefRelationship();
				//		//	// relationship.Address = null;
				//		//	relationship.CardNumber = null;
				//		//	relationship.CreatedDate = null;
				//		//	relationship.FullName = mCustomer.FullNameFamily;
				//		//	relationship.LoanBriefId = loanCredit.Id;
				//		//	relationship.Phone = mCustomer.PhoneFamily;
				//		//	relationship.RefPhoneCallRate = null;
				//		//	relationship.RefPhoneDurationRate = null;
				//		//	relationship.RelationshipType = mCustomer.RelativeFamilyId;
				//		//	lbrs.Add(relationship);
				//		//}
				//		//if (mCustomer.RelativeFamilyId1 > 0)
				//		//{
				//		//	var relationship = new LoanBriefRelationship();
				//		//	// relationship.Address = null;
				//		//	relationship.CardNumber = null;
				//		//	relationship.CreatedDate = null;
				//		//	relationship.FullName = mCustomer.FullNameFamily1;
				//		//	relationship.LoanBriefId = loanCredit.Id;
				//		//	relationship.Phone = mCustomer.PhoneFamily1;
				//		//	relationship.RefPhoneCallRate = null;
				//		//	relationship.RefPhoneDurationRate = null;
				//		//	relationship.RelationshipType = mCustomer.RelativeFamilyId1;
				//		//	lbrs.Add(relationship);
				//		//}
				//		//#endregion
				//		//#region Loan Brief Property						
				//		//if (mCustomer.ModelPhone != null && !mCustomer.Equals(""))
				//		//{
				//		//	var product = los.Product.Where(x => x.FullName.Contains(mCustomer.ModelPhone)).FirstOrDefault();
				//		//	if (product != null)
				//		//	{
				//		//		var property = new LoanBriefProperty();
				//		//		//property.Age = null;
				//		//		property.BrandId = product.BrandProductId;
				//		//		property.CarManufacturer = null;
				//		//		property.CarName = null;
				//		//		//property.CreatedTime = null;
				//		//		property.DocumentAvailable = null;
				//		//		property.LoanBriefPropertyId = loanCredit.Id;
				//		//		//property.Name = null;
				//		//		//property.Ownership = null;
				//		//		property.ProductId = product.Id;
				//		//		property.PropertyType = null;
				//		//		property.UpdatedTime = null;
				//		//		lbps.Add(property);
				//		//	}
				//		//}
				//		//#endregion
				//	}
				//}
				// save changes								
				//los.Database.BeginTransaction();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanBrief ON");
				//los.LoanBrief.AddRange(lbs);						
				//Console.WriteLine("Count:" + lbs.Count);
				//los.LoanBrief.BulkInsert(lbs);
				//los.LoanBriefJob.AddRange(lbjs);
				//los.LoanBriefResident.AddRange(lbss);
				//los.LoanBriefHousehold.AddRange(lbhs);
				//los.LoanBriefRelationship.AddRange(lbrs);
				//los.LoanBriefProperty.AddRange(lbps);
				///los.SaveChanges();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanBrief OFF");
				//los.Database.CommitTransaction();
				Thread.Sleep(10000);
				page++;
			}
			Console.WriteLine("Finish synchronizing Loan Briefs");
		}

		static void SyncLoanBriefNext(int startId)
		{
			Console.WriteLine("Begin synchronizing Loan Briefs");
			List<int> timing = new List<int>();
			//DateTime dt = new DateTime(2020, 6, 30, 0, 0, 0);
			int count = ag.TblLoanCredit.Where(x => x.Id > startId && x.Id <= 1100000).Count();
			//int count = 1;
			var page = (count / 5000) + 1;
			var pageSize = 5000;
			var loanStatuses = los.LoanStatus.ToList();
			for (int i = 1; i < page + 1; i++)
			{
				var loanCredits = ag.TblLoanCredit.AsNoTracking().Where(x => x.Id > startId && x.Id <= 1100000).Skip((i - 1) * pageSize).Take(pageSize).ToList();
				//var loanCredits = ag.TblLoanCredit.AsNoTracking().Where(x => x.Id == 3395).Skip((i - 1) * pageSize).Take(pageSize).ToList();
				var lbs = new List<LoanBrief>();
				//var lbjs = new List<LoanBriefJob>();
				//var lbss = new List<LoanBriefResident>();
				//var lbhs = new List<LoanBriefHousehold>();
				//var lbrs = new List<LoanBriefRelationship>();
				//var lbps = new List<LoanBriefProperty>();			
				loanCredits.AsyncParallelForEach(async loanCredit =>
				{
					Console.WriteLine($"Processing entry '{loanCredit.Id}'");

					using (var los = new LOSContext())
					{
						using (var ag = new MecashContext())
						{
							var mCustomer = ag.TblCustomerCredit.Find(loanCredit.CustomerCreditId);
							if (mCustomer != null)
							{
								#region Loan Brief Job
								try
								{
									var job = new LoanBriefJob();
									//job.BankName = null;
									job.BusinessType = mCustomer.TypeCompany;
									//job.CardNumber = null;
									job.CompanyAddress = mCustomer.AddressCompany;
									//job.CompanyAddressGoogleMap = null;
									job.CompanyAddressLatLng = mCustomer.LatLongAddressCompany;
									if (mCustomer.CompanyDistrictId > 0)
										job.CompanyDistrictId = mCustomer.CompanyDistrictId;
									job.CompanyName = mCustomer.CompanyName;
									job.CompanyPhone = mCustomer.CompanyPhone;
									if (mCustomer.CompanyCityId > 0)
										job.CompanyProvinceId = mCustomer.CompanyCityId;
									job.CompanyTaxCode = mCustomer.CompanyTaxCode;
									if (mCustomer.CompanyWardId > 0)
										job.CompanyWardId = mCustomer.CompanyWardId;
									//job.ContractRemain = null;
									//job.ContractType = null;
									//job.DepartmentName = null;
									job.Description = mCustomer.DescriptionPositionJob;
									job.DocumentAvailable = null;
									job.ImcomeType = mCustomer.ReceiveYourIncome;
									if (mCustomer.JobId > 0)
										job.JobId = mCustomer.JobId;
									job.LoanBriefJobId = loanCredit.Id;
									job.PaidType = null;
									job.TotalIncome = mCustomer.Salary;
									job.WorkingAddress = mCustomer.AddressCompany;
									job.WorkingTime = null;
									los.LoanBriefJob.SingleInsert(job);
								}
								catch (Exception ex)
								{
									Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
									if (ex.InnerException != null)
										Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
								}
								#endregion
								#region Loan Brief Resident						
								LoanBriefResident resident = new LoanBriefResident();
								resident.AddressLatLng = mCustomer.LatLongAddress;
								resident.Address = mCustomer.Street;
								//resident.AddressGoogleMap = null;
								resident.AppartmentNumber = mCustomer.LivingApartmentNumber;
								//resident.Block = null;
								//resident.Direction = null;
								if (loanCredit.DistrictId > 0)
									resident.DistrictId = loanCredit.DistrictId;
								resident.Lane = null;
								resident.LivingTime = mCustomer.LivingTimeId;
								resident.LivingWith = null;
								resident.LoanBriefResidentId = loanCredit.Id;
								resident.Ownership = mCustomer.TypeOfOwnershipId;
								if (loanCredit.CityId > 0)
									resident.ProvinceId = loanCredit.CityId;
								resident.ResidentType = mCustomer.TypeOfOwnershipId;
								if (loanCredit.WardId > 0)
									resident.WardId = loanCredit.WardId;
								los.LoanBriefResident.SingleInsert(resident);
								#endregion
								#region Loan Brief Household						
								LoanBriefHousehold household = new LoanBriefHousehold();
								household.Address = mCustomer.AddressHouseHold;
								//household.AppartmentNumber = null;
								//household.Block = null;
								if (mCustomer.DistrictId > 0)
									household.DistrictId = mCustomer.DistrictId;
								//household.Lane = null;
								household.LoanBriefHouseholdId = loanCredit.Id;
								household.NoOfFamilyMembers = null;
								household.Ownership = mCustomer.TypeOfOwnershipId;
								if (mCustomer.CityId > 0)
									household.ProvinceId = mCustomer.CityId;
								household.Status = null;
								if (mCustomer.WardId > 0)
									household.WardId = mCustomer.WardId;
								los.LoanBriefHousehold.SingleInsert(household);
								#endregion
								#region Loan Brief Relationship
								if (mCustomer.RelativeFamilyId > 0)
								{
									var relationship = new LoanBriefRelationship();
									// relationship.Address = null;
									relationship.CardNumber = null;
									relationship.CreatedDate = null;
									relationship.FullName = mCustomer.FullNameFamily;
									relationship.LoanBriefId = loanCredit.Id;
									relationship.Phone = mCustomer.PhoneFamily;
									relationship.RefPhoneCallRate = null;
									relationship.RefPhoneDurationRate = null;
									relationship.RelationshipType = mCustomer.RelativeFamilyId;
									los.LoanBriefRelationship.SingleInsert(relationship);
								}
								if (mCustomer.RelativeFamilyId1 > 0)
								{
									var relationship = new LoanBriefRelationship();
									// relationship.Address = null;
									relationship.CardNumber = null;
									relationship.CreatedDate = null;
									relationship.FullName = mCustomer.FullNameFamily1;
									relationship.LoanBriefId = loanCredit.Id;
									relationship.Phone = mCustomer.PhoneFamily1;
									relationship.RefPhoneCallRate = null;
									relationship.RefPhoneDurationRate = null;
									relationship.RelationshipType = mCustomer.RelativeFamilyId1;
									los.LoanBriefRelationship.SingleInsert(relationship);
								}
								#endregion
								#region Loan Brief Property						
								if (mCustomer.ModelPhone != null && !mCustomer.Equals(""))
								{
									var product = los.Product.Where(x => x.FullName.Contains(mCustomer.ModelPhone)).FirstOrDefault();
									if (product != null)
									{
										var property = new LoanBriefProperty();
										//property.Age = null;
										property.BrandId = product.BrandProductId;
										property.CarManufacturer = null;
										property.CarName = null;
										//property.CreatedTime = null;
										property.DocumentAvailable = null;
										property.LoanBriefPropertyId = loanCredit.Id;
										//property.Name = null;
										//property.Ownership = null;
										property.ProductId = product.Id;
										property.PropertyType = null;
										property.UpdatedTime = null;
										los.LoanBriefProperty.SingleInsert(property);
									}
								}
								#endregion
							}
						}
					}
				}, 20);
				page++;
			}
			Console.WriteLine("Finish synchronizing Loan Briefs");
		}

		static void FixLoanBriefResident(int startId)
		{
			Console.WriteLine("Begin synchronizing Loan Briefs");
			List<int> timing = new List<int>();
			//DateTime dt = new DateTime(2020, 6, 30, 0, 0, 0);
			int count = los.LoanBrief.Where(x => (x.Status == 100 || x.Status == 110)).Count();
			//int count = 1;
			var page = (count / 5000) + 1;
			var pageSize = 5000;
			for (int i = 1; i < page + 1; i++)
			{
				var lbs = los.LoanBrief.AsNoTracking().Where(x => (x.Status == 100 || x.Status == 110)).Skip((i - 1) * pageSize).Take(pageSize).ToList();
				lbs.AsyncParallelForEach(async lb =>
				{
					Console.WriteLine($"Processing entry '{lb.LoanBriefId}'");
					using (var los = new LOSContext())
					{
						// check exists loan brief resident
						var resident = los.LoanBriefResident.Find(lb.LoanBriefId);
						if (resident == null)
						{
							using (var ag = new MecashContext())
							{
								var loanCredit = ag.TblLoanCredit.Find(lb.LoanBriefId);
								if (loanCredit != null)
								{
									var mCustomer = ag.TblCustomerCredit.Find(loanCredit.CustomerCreditId);
									if (mCustomer != null)
									{
										#region Loan Brief Resident						
										resident = new LoanBriefResident();
										resident.AddressLatLng = mCustomer.LatLongAddress;
										resident.Address = mCustomer.Street;
										//resident.AddressGoogleMap = null;
										resident.AppartmentNumber = mCustomer.LivingApartmentNumber;
										//resident.Block = null;
										//resident.Direction = null;
										if (loanCredit.DistrictId > 0)
											resident.DistrictId = loanCredit.DistrictId;
										resident.Lane = null;
										resident.LivingTime = mCustomer.LivingTimeId;
										resident.LivingWith = null;
										resident.LoanBriefResidentId = loanCredit.Id;
										resident.Ownership = mCustomer.TypeOfOwnershipId;
										if (loanCredit.CityId > 0)
											resident.ProvinceId = loanCredit.CityId;
										resident.ResidentType = mCustomer.TypeOfOwnershipId;
										if (loanCredit.WardId > 0)
											resident.WardId = loanCredit.WardId;
										los.LoanBriefResident.SingleInsert(resident);
										#endregion
									}
								}
							}
						}
					}
				}, 20);
				page++;
			}
			Console.WriteLine("Finish synchronizing Loan Briefs");
		}

		static void FixLoanBriefHousehold(int startId)
		{
			Console.WriteLine("Begin synchronizing Loan Briefs");
			List<int> timing = new List<int>();
			//DateTime dt = new DateTime(2020, 6, 30, 0, 0, 0);
			int count = los.LoanBrief.Where(x => (x.Status == 100 || x.Status == 110)).Count();
			//int count = 1;
			var page = (count / 5000) + 1;
			var pageSize = 5000;
			for (int i = 1; i < page + 1; i++)
			{
				var lbs = los.LoanBrief.AsNoTracking().Where(x => (x.Status == 100 || x.Status == 110)).Skip((i - 1) * pageSize).Take(pageSize).ToList();
				lbs.AsyncParallelForEach(async lb =>
				{
					Console.WriteLine($"Processing entry '{lb.LoanBriefId}'");
					using (var los = new LOSContext())
					{
						// check exists loan brief resident
						var household = los.LoanBriefHousehold.Find(lb.LoanBriefId);
						if (household == null)
						{
							using (var ag = new MecashContext())
							{
								var loanCredit = ag.TblLoanCredit.Find(lb.LoanBriefId);
								if (loanCredit != null)
								{
									var mCustomer = ag.TblCustomerCredit.Find(loanCredit.CustomerCreditId);
									if (mCustomer != null)
									{
										#region Loan Brief Household						
										household = new LoanBriefHousehold();
										household.Address = mCustomer.AddressHouseHold;
										//household.AppartmentNumber = null;
										//household.Block = null;
										if (mCustomer.DistrictId > 0)
											household.DistrictId = mCustomer.DistrictId;
										//household.Lane = null;
										household.LoanBriefHouseholdId = loanCredit.Id;
										household.NoOfFamilyMembers = null;
										household.Ownership = mCustomer.TypeOfOwnershipId;
										if (mCustomer.CityId > 0)
											household.ProvinceId = mCustomer.CityId;
										household.Status = null;
										if (mCustomer.WardId > 0)
											household.WardId = mCustomer.WardId;
										los.LoanBriefHousehold.SingleInsert(household);
										#endregion
									}
								}
							}
						}
					}
				}, 20);
				page++;
			}
			Console.WriteLine("Finish synchronizing Loan Briefs");
		}

		static void FixLoanBriefJob(int startId)
		{
			Console.WriteLine("Begin synchronizing Loan Briefs");
			List<int> timing = new List<int>();
			//DateTime dt = new DateTime(2020, 6, 30, 0, 0, 0);
			int count = los.LoanBrief.Where(x => (x.Status == 100 || x.Status == 110)).Count();
			//int count = 1;
			var page = (count / 5000) + 1;
			var pageSize = 5000;
			for (int i = 1; i < page + 1; i++)
			{
				var lbs = los.LoanBrief.AsNoTracking().Where(x => (x.Status == 100 || x.Status == 110)).Skip((i - 1) * pageSize).Take(pageSize).ToList();
				lbs.AsyncParallelForEach(async lb =>
				{
					Console.WriteLine($"Processing entry '{lb.LoanBriefId}'");
					using (var los = new LOSContext())
					{
						// check exists loan brief resident
						var job = los.LoanBriefJob.Find(lb.LoanBriefId);
						if (job == null)
						{
							using (var ag = new MecashContext())
							{
								var loanCredit = ag.TblLoanCredit.Find(lb.LoanBriefId);
								if (loanCredit != null)
								{
									var mCustomer = ag.TblCustomerCredit.Find(loanCredit.CustomerCreditId);
									if (mCustomer != null)
									{
										#region Loan Brief Job
										try
										{
											job = new LoanBriefJob();
											//job.BankName = null;
											job.BusinessType = mCustomer.TypeCompany;
											//job.CardNumber = null;
											job.CompanyAddress = mCustomer.AddressCompany;
											//job.CompanyAddressGoogleMap = null;
											job.CompanyAddressLatLng = mCustomer.LatLongAddressCompany;
											if (mCustomer.CompanyDistrictId > 0)
												job.CompanyDistrictId = mCustomer.CompanyDistrictId;
											job.CompanyName = mCustomer.CompanyName;
											job.CompanyPhone = mCustomer.CompanyPhone;
											if (mCustomer.CompanyCityId > 0)
												job.CompanyProvinceId = mCustomer.CompanyCityId;
											job.CompanyTaxCode = mCustomer.CompanyTaxCode;
											if (mCustomer.CompanyWardId > 0)
												job.CompanyWardId = mCustomer.CompanyWardId;
											//job.ContractRemain = null;
											//job.ContractType = null;
											//job.DepartmentName = null;
											job.Description = mCustomer.DescriptionPositionJob;
											job.DocumentAvailable = null;
											job.ImcomeType = mCustomer.ReceiveYourIncome;
											if (mCustomer.JobId > 0)
												job.JobId = mCustomer.JobId;
											job.LoanBriefJobId = loanCredit.Id;
											job.PaidType = null;
											job.TotalIncome = mCustomer.Salary;
											job.WorkingAddress = mCustomer.AddressCompany;
											job.WorkingTime = null;
											los.LoanBriefJob.SingleInsert(job);
										}
										catch (Exception ex)
										{
											Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
											if (ex.InnerException != null)
												Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
										}
										#endregion
									}
								}
							}
						}
					}
				}, 20);
				page++;
			}
			Console.WriteLine("Finish synchronizing Loan Briefs");
		}

		static void FixLoanBriefProperty(int startId)
		{
			Console.WriteLine("Begin synchronizing Loan Briefs");
			List<int> timing = new List<int>();
			//DateTime dt = new DateTime(2020, 6, 30, 0, 0, 0);
			int count = los.LoanBrief.Where(x => (x.Status == 100 || x.Status == 110)).Count();
			//int count = 1;
			var page = (count / 5000) + 1;
			var pageSize = 5000;
			for (int i = 1; i < page + 1; i++)
			{
				var lbs = los.LoanBrief.AsNoTracking().Where(x => (x.Status == 100 || x.Status == 110)).Skip((i - 1) * pageSize).Take(pageSize).ToList();
				lbs.AsyncParallelForEach(async lb =>
				{
					Console.WriteLine($"Processing entry '{lb.LoanBriefId}'");
					using (var los = new LOSContext())
					{
						// check exists loan brief resident
						var property = los.LoanBriefProperty.Find(lb.LoanBriefId);
						if (property == null)
						{
							using (var ag = new MecashContext())
							{
								var loanCredit = ag.TblLoanCredit.Find(lb.LoanBriefId);
								if (loanCredit != null)
								{
									var mCustomer = ag.TblCustomerCredit.Find(loanCredit.CustomerCreditId);
									if (mCustomer != null)
									{
										#region Loan Brief Property						
										if (mCustomer.ModelPhone != null && !mCustomer.Equals(""))
										{
											var product = los.Product.Where(x => x.FullName.Contains(mCustomer.ModelPhone)).FirstOrDefault();
											if (product != null)
											{
												property = new LoanBriefProperty();
												//property.Age = null;
												property.BrandId = product.BrandProductId;
												property.CarManufacturer = null;
												property.CarName = null;
												//property.CreatedTime = null;
												property.DocumentAvailable = null;
												property.LoanBriefPropertyId = loanCredit.Id;
												//property.Name = null;
												//property.Ownership = null;
												property.ProductId = product.Id;
												property.PropertyType = null;
												property.UpdatedTime = null;
												los.LoanBriefProperty.SingleInsert(property);
											}
										}
										#endregion
									}
								}
							}
						}
					}
				}, 20);
				page++;
			}
			Console.WriteLine("Finish synchronizing Loan Briefs");
		}

		static void FixLoanBriefRelationship(int startId)
		{
			Console.WriteLine("Begin synchronizing Loan Briefs");
			List<int> timing = new List<int>();
			//DateTime dt = new DateTime(2020, 6, 30, 0, 0, 0);
			int count = ag.TblLoanCredit.Where(x => x.Id > startId && x.Id <= 980000).Count();
			//int count = 1;
			var page = (count / 5000) + 1;
			var pageSize = 5000;
			var loanStatuses = los.LoanStatus.ToList();
			for (int i = 1; i < page + 1; i++)
			{
				var loanCredits = ag.TblLoanCredit.AsNoTracking().Where(x => x.Id > startId && x.Id <= 980000).Skip((i - 1) * pageSize).Take(pageSize).ToList();
				
				var lbs = new List<LoanBrief>();				
				loanCredits.AsyncParallelForEach(async loanCredit =>
				{
					Console.WriteLine($"Processing entry '{loanCredit.Id}'");

					using (var los = new LOSContext())
					{
						using (var ag = new MecashContext())
						{
							var mCustomer = ag.TblCustomerCredit.Find(loanCredit.CustomerCreditId);
							if (mCustomer != null)
							{
								#region Loan Brief Relationship
								if (mCustomer.FullNameColleague != null && !mCustomer.FullNameColleague.Equals(""))
								{
									var relationship = new LoanBriefRelationship();
									// relationship.Address = null;
									relationship.CardNumber = null;
									relationship.CreatedDate = null;
									relationship.FullName = mCustomer.FullNameColleague;
									relationship.LoanBriefId = loanCredit.Id;
									relationship.Phone = mCustomer.PhoneColleague;
									relationship.RefPhoneCallRate = null;
									relationship.RefPhoneDurationRate = null;
									relationship.RelationshipType = 6;
									los.LoanBriefRelationship.SingleInsert(relationship);
								}
								if (mCustomer.RelativeFamilyId > 0)
								{
									var relationship = new LoanBriefRelationship();
									// relationship.Address = null;
									relationship.CardNumber = null;
									relationship.CreatedDate = null;
									relationship.FullName = mCustomer.FullNameFamily;
									relationship.LoanBriefId = loanCredit.Id;
									relationship.Phone = mCustomer.PhoneFamily;
									relationship.RefPhoneCallRate = null;
									relationship.RefPhoneDurationRate = null;
									relationship.RelationshipType = mCustomer.RelativeFamilyId;
									los.LoanBriefRelationship.SingleInsert(relationship);
								}
								if (mCustomer.RelativeFamilyId1 > 0)
								{
									var relationship = new LoanBriefRelationship();
									// relationship.Address = null;
									relationship.CardNumber = null;
									relationship.CreatedDate = null;
									relationship.FullName = mCustomer.FullNameFamily1;
									relationship.LoanBriefId = loanCredit.Id;
									relationship.Phone = mCustomer.PhoneFamily1;
									relationship.RefPhoneCallRate = null;
									relationship.RefPhoneDurationRate = null;
									relationship.RelationshipType = mCustomer.RelativeFamilyId1;
									los.LoanBriefRelationship.SingleInsert(relationship);
								}
								#endregion
							}
						}
					}
				}, 20);
				page++;
			}
			Console.WriteLine("Finish synchronizing Loan Briefs");
		}

		static void SyncLoanBriefLender()
		{
			#region old
			Console.WriteLine("Begin synchronizing Loan Briefs Lender");
			List<int> timing = new List<int>();
			int count = ag.TblLoanCredit.Where(x => x.Id > 0 && x.Id <= 1100000 && x.Status >= 35).Count();
			var shops = ag.TblShop.Where(x => x.Status != 20).ToList();
			//int count = 1;
			var page = (count / 5000) + 1;
			var pageSize = 5000;
			for (int i = 1; i < page + 1; i++)
			{
				var loanCredits = ag.TblLoanCredit.AsNoTracking().Where(x => x.Id > 0 && x.Id <= 1100000 && x.Status >= 35).Skip((i - 1) * pageSize).Take(pageSize).ToList();
				//var loanCredits = ag.TblLoanCredit.AsNoTracking().Where(x => x.Id == 275239).Skip((i - 1) * pageSize).Take(pageSize).ToList();
				//var lbs = new List<LoanBrief>();
				//var lbjs = new List<LoanBriefJob>();
				//var lbss = new List<LoanBriefResident>();
				//var lbhs = new List<LoanBriefHousehold>();
				//var lbrs = new List<LoanBriefRelationship>();
				//var lbps = new List<LoanBriefProperty>();			
				loanCredits.AsyncParallelForEach(async loanCredit =>
				{
					Console.WriteLine($"Processing entry '{loanCredit.Id}'");

					using (var los = new LOSContext())
					{
						using (var ag = new MecashContext())
						{
							#region Loan Brief		
							try
							{
								var loan = ag.TblLoan.Where(x => x.Id == loanCredit.LoanId).FirstOrDefault();
								if (loan != null)
								{
									var shop = shops.Where(x => x.ShopId == loanCredit.OwerShopId).FirstOrDefault();
									if (loanCredit.Id < 990000)
									{
										if (shop != null)
										{
											// tìm lender giải ngân
											los.LoanBriefLender.SingleInsert(new LoanbriefLender()
											{
												FullName = shop.Represent,
												LenderId = shop.ShopId,
												LenderName = shop.Name,
												LoanbriefId = loanCredit.Id,
												NationalCard = shop.PersonCardNumber,
												CreatedAt = loan.FromDate
											});
										}
									}
									else
									{
										// Find by id
										if (loanCredit.IdOfPartenter != null && !"".Equals(loanCredit.IdOfPartenter))
										{
											// giải ngân bên LOS
											var lbl = los.LoanBriefLender.Where(x => x.LoanbriefId == Int32.Parse(loanCredit.IdOfPartenter)).FirstOrDefault();
											if (lbl == null)
											{
												los.LoanBriefLender.SingleInsert(new LoanbriefLender()
												{
													FullName = shop.Represent,
													LenderId = shop.ShopId,
													LenderName = shop.Name,
													LoanbriefId = Int32.Parse(loanCredit.IdOfPartenter),
													NationalCard = shop.PersonCardNumber,
													CreatedAt = loan.FromDate
												});
											}
										}
										else
										{
											// giải ngân bên LOS
											var lbl = los.LoanBriefLender.Where(x => x.LoanbriefId == loanCredit.Id).FirstOrDefault();
											if (lbl == null)
											{
												los.LoanBriefLender.SingleInsert(new LoanbriefLender()
												{
													FullName = shop.Represent,
													LenderId = shop.ShopId,
													LenderName = shop.Name,
													LoanbriefId = loanCredit.Id,
													NationalCard = shop.PersonCardNumber,
													CreatedAt = loan.FromDate
												});
											}
										}
									}
								}
							}
							catch (Exception ex)
							{
								Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
								if (ex.InnerException != null)
									Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
							}
							#endregion
						}
					}
				}, 20);
				page++;
			}
			Console.WriteLine("Finish synchronizing Loan Briefs Lender");
			#endregion
			//Console.WriteLine("Begin");
			//var loans = los.LoanBrief.Where(x => (x.Status == 100 || x.Status == 110) && x.DisbursementAt == null).ToList();
			//if (loans != null && loans.Count > 0)
			//{
			//	foreach (var loan in loans)
			//	{
			//		var lc = ag.TblLoanCredit.Where(x => x.IdOfPartenter == loan.LoanBriefId.ToString()).FirstOrDefault();
			//		if (lc != null)
			//		{
			//			var l = ag.TblLoan.Where(x => x.Id == lc.LoanId).FirstOrDefault();
			//			if (l != null)
			//			{
			//				loan.DisbursementAt = l.FromDate;
			//				los.Entry(loan).State = EntityState.Modified;
			//			}	
			//		}	
			//		else
			//		{
			//			// tìm theo id
			//			lc = ag.TblLoanCredit.Find(loan.MecashId);
			//			if (lc != null)
			//			{
			//				var l = ag.TblLoan.Where(x => x.Id == lc.LoanId).FirstOrDefault();
			//				if (l != null)
			//				{
			//					loan.DisbursementAt = l.FromDate;
			//					los.Entry(loan).State = EntityState.Modified;
			//				}
			//			}	
			//		}	
			//	}
			//	los.SaveChanges();
			//}
			Console.WriteLine("End");
		}

		static void SyncCustomer(int startId)
		{
			Console.WriteLine("Begin synchronizing Customers");
			var count = ag.TblCustomerCredit.Where(x => x.CustomerId > startId).Count();
			int page = count / 5000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				var cs = new List<Customer>();
				var customers = ag.TblCustomerCredit.Where(x => x.CustomerId > startId).Skip((i - 1) * 5000).Take(5000).ToList();
				//los.Database.BeginTransaction();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Customer ON");
				foreach (var mCustomer in customers)
				{
					Console.WriteLine("Customer :" + mCustomer.CustomerId);
					var customer = new Customer();
					customer.Address = mCustomer.Street;
					customer.AddressLatLong = mCustomer.LatLongAddress;
					customer.CompanyAddress = mCustomer.AddressCompany;
					customer.CompanyAddressLatLong = mCustomer.LatLongAddressCompany;
					customer.CompanyName = mCustomer.CompanyName;
					if (mCustomer.CompanyDistrictId > 0)
						customer.CompanyDistrictId = mCustomer.CompanyDistrictId;
					if (mCustomer.CompanyCityId > 0)
						customer.CompanyProvinceId = mCustomer.CompanyCityId;
					if (mCustomer.CompanyWardId > 0)
						customer.CompanyWardId = mCustomer.CompanyWardId;
					customer.CreatedAt = mCustomer.CreateDate;
					customer.CustomerId = mCustomer.CustomerId;
					customer.DistrictId = mCustomer.DistrictId;
					customer.Dob = mCustomer.Birthday;
					customer.Email = mCustomer.Email;
					customer.FacebookAddress = mCustomer.Facebook;
					customer.FullName = mCustomer.FullName;
					customer.Gender = mCustomer.Gender;
					customer.InsurenceNote = mCustomer.SocialInsuranceNote;
					customer.InsurenceNumber = mCustomer.SocialInsuranceNumber;
					customer.IsMerried = mCustomer.IsMarried;
					customer.JobId = mCustomer.JobId;
					customer.MecashId = mCustomer.CustomerId;
					customer.NationalCard = mCustomer.CardNumber;
					customer.NationalCardDate = null;
					customer.NationCardPlace = null;
					customer.NumberBaby = mCustomer.NumberBaby;
					customer.Phone = mCustomer.Phone;
					customer.ProvinceId = mCustomer.CityId;
					customer.Salary = mCustomer.Salary;
					customer.Status = 1;
					customer.UpdatedAt = mCustomer.CreateDate;
					customer.WardId = mCustomer.WardId;
					cs.Add(customer);
				}
				los.Customer.BulkInsert(cs, options => options.InsertKeepIdentity = true);
				//los.SaveChanges();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Customer OFF");
				//los.Database.CommitTransaction();
			}
		}

		static void SyncLoanCreditFiles(int startId)
		{
			Console.WriteLine("Begin synchronizing Loan Brief Files");
			var count = ag.TblCustomerCreditFiles.Where(x => x.Id > startId).Count();
			int page = count / 5000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				var files = ag.TblCustomerCreditFiles.AsNoTracking().Where(x => x.Id > startId).Skip((i - 1) * 5000).Take(5000).ToList();
				var fs = new List<LoanBriefFiles>();
				if (files != null && files.Count > 0)
				{
					foreach (var file in files)
					{
						Console.WriteLine("File :" + file.Id);
						var loanBriefFile = new LoanBriefFiles();
						loanBriefFile.CreateAt = file.CreateOn;
						loanBriefFile.IsDeleted = file.DeleteStatus;
						loanBriefFile.DomainOfPartner = file.DomainOfPartner;
						loanBriefFile.FilePath = file.UrlImg;
						loanBriefFile.Id = file.Id;
						loanBriefFile.LinkImgOfPartner = file.LinkImgOfPartner;
						loanBriefFile.LoanBriefId = file.LoanCreditId;
						loanBriefFile.S3status = file.S3status;
						loanBriefFile.Status = file.Status;
						loanBriefFile.Synchronize = file.Synchronize;
						loanBriefFile.TypeFile = file.TypeFile;
						if (file.TypeId > 0)
							loanBriefFile.TypeId = file.TypeId;
						loanBriefFile.UserId = file.UserId;
						loanBriefFile.MecashId = file.Id;
						fs.Add(loanBriefFile);
					}
					//Parallel.ForEach(files, new ParallelOptions { MaxDegreeOfParallelism = 5 }, file =>
					//{
					//	using (var los = new LOSContext())
					//	{
					//		try
					//		{
					//			Console.WriteLine("File :" + file.Id);
					//			var loanBriefFile = los.LoanBriefFiles.Find(file.Id);
					//			if (loanBriefFile == null)
					//			{
					//				loanBriefFile = new LoanBriefFiles();
					//				loanBriefFile.CreateAt = file.CreateOn;
					//				loanBriefFile.IsDeleted = file.DeleteStatus;
					//				loanBriefFile.DomainOfPartner = file.DomainOfPartner;
					//				loanBriefFile.FilePath = file.UrlImg;
					//				loanBriefFile.Id = file.Id;
					//				loanBriefFile.LinkImgOfPartner = file.LinkImgOfPartner;
					//				loanBriefFile.LoanBriefId = file.LoanCreditId;
					//				//loanBriefFile.S3status = null;
					//				loanBriefFile.Status = file.Status;
					//				//loanBriefFile.Synchronize = null;
					//				loanBriefFile.TypeFile = file.TypeFile;
					//				if (file.TypeId > 0)
					//					loanBriefFile.TypeId = file.TypeId;
					//				loanBriefFile.UserId = file.UserId;
					//				loanBriefFile.MecashId = file.LoanCreditId;
					//				los.LoanBriefFiles.Add(loanBriefFile);
					//				// create Loan Brief Document ??
					//			}
					//			else
					//			{
					//				loanBriefFile.CreateAt = file.CreateOn;
					//				loanBriefFile.IsDeleted = file.DeleteStatus;
					//				loanBriefFile.DomainOfPartner = file.DomainOfPartner;
					//				loanBriefFile.FilePath = file.UrlImg;
					//				loanBriefFile.Id = file.Id;
					//				loanBriefFile.LinkImgOfPartner = file.LinkImgOfPartner;
					//				loanBriefFile.LoanBriefId = file.LoanCreditId;
					//				//loanBriefFile.S3status = null;
					//				loanBriefFile.Status = file.Status;
					//				//loanBriefFile.Synchronize = null;
					//				loanBriefFile.TypeFile = file.TypeFile;
					//				if (file.TypeId > 0)
					//					loanBriefFile.TypeId = file.TypeId;
					//				loanBriefFile.UserId = file.UserId;
					//				loanBriefFile.MecashId = file.LoanCreditId;
					//				los.Entry(loanBriefFile).State = EntityState.Modified;
					//				// create Loan Brief Document ??
					//			}
					//			los.SaveChanges();
					//		}
					//		catch (Exception ex)
					//		{
					//			Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
					//			if (ex.InnerException != null)
					//				Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
					//		}
					//		finally
					//		{
					//			//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LoanBriefFiles OFF");
					//		}
					//	}
					//});
				}
				los.LoanBriefFiles.BulkInsert(fs, options => options.InsertKeepIdentity = true);
				//los.SaveChanges();
			}
			Console.WriteLine("Finish synchronizing Loan Brief Files");
		}

		static void SyncLoanCreditComment(int startId)
		{
			Console.WriteLine("Begin synchronizing Loan Brief Comment");
			var count = ag.TblCommentCredit.Where(x => x.Id > startId).Count();
			int page = count / 5000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				var comments = ag.TblCommentCredit.AsNoTracking().Where(x => x.Id > startId).Skip((i - 1) * 5000).Take(5000).ToList();
				if (comments != null && comments.Count > 0)
				{
					var fs = new List<LoanBriefNote>();
					foreach (var comment in comments)
					{
						Console.WriteLine("Comment :" + comment.Id);
						var note = new LoanBriefNote();
						note.ActionComment = comment.ActionId;
						note.CreatedTime = comment.CreateDate;
						note.FullName = comment.FullName;
						note.IsDisplay = comment.IsDisplay == 1 ? true : false;
						note.IsManual = comment.IsManual == 1 ? true : false;
						note.LoanBriefId = comment.LoanCreditId;
						note.LoanBriefNoteId = comment.Id;
						note.Note = comment.Comment;
						note.ShopId = comment.ShopId;
						note.Status = 1;
						//note.Type = null;
						note.UserId = comment.UserId;
						fs.Add(note);
					}
					//Parallel.ForEach(comments, new ParallelOptions { MaxDegreeOfParallelism = 5 }, comment =>
					//{
					//	using (var los = new LOSContext())
					//	{
					//		try
					//		{
					//			Console.WriteLine("Comment :" + comment.Id);
					//			var note = los.LoanBriefNote.Find(comment.Id);
					//			if (note == null)
					//			{
					//				note = new LoanBriefNote();
					//				note.ActionComment = comment.ActionId;
					//				note.CreatedTime = comment.CreateDate;
					//				note.FullName = comment.FullName;
					//				note.IsDisplay = comment.IsDisplay == 1 ? true : false;
					//				note.IsManual = comment.IsManual == 1 ? true : false;
					//				note.LoanBriefId = comment.LoanCreditId;
					//				note.LoanBriefNoteId = comment.Id;
					//				note.Note = comment.Comment;
					//				note.ShopId = comment.ShopId;
					//				note.Status = 1;
					//				//note.Type = null;
					//				note.UserId = comment.UserId;
					//				los.LoanBriefNote.Add(note);
					//			}
					//			else
					//			{
					//				note.ActionComment = comment.ActionId;
					//				note.CreatedTime = comment.CreateDate;
					//				note.FullName = comment.FullName;
					//				note.IsDisplay = comment.IsDisplay == 1 ? true : false;
					//				note.IsManual = comment.IsManual == 1 ? true : false;
					//				note.LoanBriefId = comment.LoanCreditId;
					//				note.LoanBriefNoteId = comment.Id;
					//				note.Note = comment.Comment;
					//				note.ShopId = comment.ShopId;
					//				note.Status = 1;
					//				//note.Type = null;
					//				note.UserId = comment.UserId;
					//				los.Entry(note).State = EntityState.Modified;
					//			}
					//			los.SaveChanges();
					//		}
					//		catch (Exception ex)
					//		{
					//			Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
					//			if (ex.InnerException != null)
					//				Console.WriteLine(ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
					//		}
					//		finally
					//		{

					//		}
					//	}
					//});
					los.LoanBriefNote.BulkInsert(fs, options => options.InsertKeepIdentity = true);
					//los.SaveChanges();
				}
			}
			Console.WriteLine("Finish synchronizing Loan Brief Comment");
		}

		static void SyncLoanCreditCompany(int startId)
		{
			Console.WriteLine("Begin synchronizing Loan Brief Company");
			var count = ag.TblInformationOfBusiness.Where(x => x.Id > startId).Count();
			int page = count / 5000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				var businesses = ag.TblInformationOfBusiness.AsNoTracking().Where(x => x.Id > startId).Skip((i - 1) * 5000).Take(5000).ToList();
				if (businesses != null)
				{
					var lbcs = new List<LoanBriefCompany>();
					foreach (var business in businesses)
					{
						Console.WriteLine("Company :" + business.Id);
						//var company = los.LoanBriefCompany.Where(x => x.LoanBriefId == business.LoanCreditId).FirstOrDefault();
						//if (company == null)
						//{
						var company = new LoanBriefCompany();
						company.Address = business.BusinessAddress;
						company.BirthdayShareholder = business.Birthday;
						company.BusinessCertificationAddress = business.BusinessAddress;
						company.BusinessCertificationDate = business.DateCertificate;
						company.CardNumberShareholder = business.CardNumber;
						company.CardNumberShareholderDate = business.DateCardNumber;
						company.CareerBusiness = business.MainBusinessSectors;
						company.CompanyName = business.CompanyName;
						company.CompanyShareholder = business.Shareholder;
						company.CreatedDate = DateTime.Now;
						company.HeadOffice = business.HeadquarterBusinessRegistration;
						company.LoanBriefId = business.LoanCreditId.Value;
						company.PlaceOfBirthShareholder = null;
						company.UpdatedDate = DateTime.Now;
						lbcs.Add(company);
						//los.SaveChanges();
						//}
						//else
						//{
						//	company.Address = business.BusinessAddress;
						//	company.BirthdayShareholder = business.Birthday;
						//	company.BusinessCertificationAddress = business.BusinessAddress;
						//	company.BusinessCertificationDate = business.DateCertificate;
						//	company.CardNumberShareholder = business.CardNumber;
						//	company.CardNumberShareholderDate = business.DateCardNumber;
						//	company.CareerBusiness = business.MainBusinessSectors;
						//	company.CompanyName = business.CompanyName;
						//	company.CompanyShareholder = business.Shareholder;
						//	company.CreatedDate = DateTime.Now;
						//	company.HeadOffice = business.HeadquarterBusinessRegistration;
						//	company.LoanBriefId = business.LoanCreditId.Value;
						//	company.PlaceOfBirthShareholder = null;
						//	company.UpdatedDate = DateTime.Now;
						//	los.LoanBriefCompany.SingleUpdate(company);
						//}
					}
					los.LoanBriefCompany.BulkInsert(lbcs, options => options.InsertKeepIdentity = true);
				}
			}
			Console.WriteLine("Finish synchronizing Loan Brief Company");
		}

		static void SyncDataCIC(int startId)
		{
			var count = ag.TblDataCic.Where(x => x.Id > startId).Count();
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				Console.WriteLine("Begin synchronizing Data CIC");
				//los.Database.BeginTransaction();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.DataCIC ON");
				var data = ag.TblDataCic.Where(x => x.Id > startId).Skip((i - 1) * 1000).Take(1000).ToList();
				List<DataCic> dataCics = new List<DataCic>();
				foreach (var cic in data)
				{
					dataCics.Add(new DataCic()
					{
						Address = cic.Address,
						Brieft = cic.Brieft,
						CardNumber = cic.CardNumber,
						CheckTime = cic.CheckTime,
						CreatedTime = cic.CreateOn,
						CreditInfo = cic.CreditInfo,
						Id = cic.Id,
						HasBadDebt = cic.HasBadDebt,
						HasLatePayment = cic.HasLatePayment,
						Mobile = cic.Mobile,
						Name = cic.Name,
						NumberOfLoans = cic.NumberOfLoans,
						StringJson = cic.StringJson
					});
				}
				los.DataCic.BulkInsert(dataCics, options => options.InsertKeepIdentity = true);
				//los.SaveChanges();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.ProductReviewResultDetail OFF");
				//los.Database.CommitTransaction();
				Console.WriteLine("Finish synchronizing Data CIC");
			}
		}

		static void SyncLocation(int startId)
		{
			Console.WriteLine("Begin synchronizing Location");
			//los.Database.BeginTransaction();
			//los.Database.ExecuteSqlRaw("truncate table dbo.Location");
			//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Location ON");
			int count = ag.TblLocation.Where(x => x.Id > startId).Count();
			int page = count / 5000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				var locations = ag.TblLocation.Where(x => x.Id > startId).Skip((i - 1) * 5000).Take(5000).ToList();
				var ls = new List<Location>();
				foreach (var loc in locations)
				{
					ls.Add(new Location()
					{
						Address = loc.Address,
						City = loc.City,
						CreatedAt = loc.CreateDate,
						Device = loc.Device,
						GroupId = null,
						Id = loc.Id,
						Lat = loc.Lat,
						LoanbriefId = loc.LoanCreditId,
						LoanbriefNoteId = loc.CommentCreditId,
						Long = loc.Long,
						UserId = loc.UserId
					});
				}
				los.Location.BulkInsert(ls, options => options.InsertKeepIdentity = true);
			}
			//los.SaveChanges();
			//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Location OFF");
			//los.Database.CommitTransaction();
			Console.WriteLine("Finish synchronizing Location");
		}

		static void SyncBlackList(int startId)
		{
			Console.WriteLine("Begin synchronizing Black list");
			var count = ag.TblBlackList.Count(x => x.Id > startId);
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				//los.Database.BeginTransaction();
				//los.Database.ExecuteSqlRaw("truncate table dbo.BlackList");
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.BlackList ON");
				var blacks = ag.TblBlackList.Where(x => x.Id > startId).Skip((i - 1) * 1000).Take(1000).ToList();
				var bs = new List<BlackList>();
				foreach (var black in blacks)
				{
					//if (!los.BlackList.Any(x => x.BlackListId == black.Id))
					//{
					bs.Add(new BlackList()
					{
						ApprovedTime = black.TimeApprove,
						ApprovedUserId = black.UserIdApprove,
						BlackListId = black.Id,
						CreatedTime = black.CreateOn,
						CreatedUserId = black.UserIdCreate,
						CustomerId = black.CustomerCreditId,
						ExpirationTime = black.ExpirationDate,
						LoanBriefId = black.LoanCreditId,
						ModifiedTime = black.ModifyDate,
						ModifiedUserId = black.UserIdModify,
						Note = black.Note,
						NumberOfDays = black.NumberDay,
						Status = black.Status
					});
					//}
					//else
					//{
					//	var b = los.BlackList.Find(black.Id);
					//	b.ApprovedTime = black.TimeApprove;
					//	b.ApprovedUserId = black.UserIdApprove;
					//	b.BlackListId = black.Id;
					//	b.CreatedTime = black.CreateOn;
					//	b.CreatedUserId = black.UserIdCreate;
					//	b.CustomerId = black.CustomerCreditId;
					//	b.ExpirationTime = black.ExpirationDate;
					//	b.LoanBriefId = black.LoanCreditId;
					//	b.ModifiedTime = black.ModifyDate;
					//	b.ModifiedUserId = black.UserIdModify;
					//	b.Note = black.Note;
					//	b.NumberOfDays = black.NumberDay;
					//	b.Status = black.Status;
					//	los.Entry(b).State = EntityState.Modified;
					//}					
				}
				los.BlackList.BulkInsert(bs, options => options.InsertKeepIdentity = true);
				//los.SaveChanges();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.BlackList OFF");
				//los.Database.CommitTransaction();
			}
			Console.WriteLine("Finish synchronizing Black list");
		}

		static void SyncBlackListLog(int startId)
		{
			Console.WriteLine("Begin synchronizing Black list log");
			int count = ag.TblBlackListLog.Where(x => x.Id > startId).Count();
			int page = count / 1000 + 1;
			for (int i = 1; i <= page + 1; i++)
			{
				//los.Database.BeginTransaction();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.BlackListLog ON");
				var blacks = ag.TblBlackListLog.Where(x => x.Id > startId).Skip((i - 1) * 1000).Take(1000).ToList();
				var bs = new List<BlackListLog>();
				foreach (var black in blacks)
				{
					bs.Add(new BlackListLog()
					{
						BlackListId = black.BlackListId,
						BlackListLogId = black.Id,
						CheckResultValue = black.CheckResultResult,
						CreatedTime = black.CreateDate,
						Dob = black.BirthDay,
						FullName = black.FullName,
						IdNumber = black.CardNumber,
						LoanBriefId = black.LoanCreditId,
						PhoneNumber = black.NumberPhone,
						PlatformLoanBriefId = black.PlatformLoanCreditId,
						ShopId = black.ShopId,
						UserId = black.UserId
					});
				}
				los.BlackListLog.BulkInsert(bs, options => options.InsertKeepIdentity = true);
				//los.SaveChanges();
				//los.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.BlackListLog OFF");
				//los.Database.CommitTransaction();
			}
			Console.WriteLine("Finish synchronizing Black list log");
		}

		public class ListQlf
		{
			public int DefaultQlf { get; set; }
			public int NhuCauVay { get; set; }
			public int DoTuoi { get; set; }
			public int KhuVucHoTro { get; set; }
			public int CoXeMay { get; set; }
			public int CoDangKyBanGoc { get; set; }
		}
		static int ConvertQualify(string qlf)
		{
			int qlfLOS = 0;
			var objQlf = new ListQlf();
			List<string> resultcheckleadQualify = qlf.Split(',').ToList();
			objQlf.DefaultQlf = Int32.Parse(resultcheckleadQualify[0]);
			objQlf.NhuCauVay = Int32.Parse(resultcheckleadQualify[1]);
			objQlf.DoTuoi = Int32.Parse(resultcheckleadQualify[2]);
			objQlf.KhuVucHoTro = Int32.Parse(resultcheckleadQualify[3]);
			objQlf.CoXeMay = Int32.Parse(resultcheckleadQualify[4]);
			objQlf.CoDangKyBanGoc = Int32.Parse(resultcheckleadQualify[5]);
			if (objQlf.DefaultQlf == 1)
				qlfLOS += 4;
			if (objQlf.NhuCauVay == 1)
				qlfLOS += 8;
			if (objQlf.DoTuoi == 1)
				qlfLOS += 16;
			if (objQlf.KhuVucHoTro == 1)
				qlfLOS += 32;
			if (objQlf.CoXeMay == 1)
				qlfLOS += 64;
			if (objQlf.CoDangKyBanGoc == 1)
				qlfLOS += 128;
			return qlfLOS;
		}
	}
}
