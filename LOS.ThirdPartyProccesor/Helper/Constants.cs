﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Helper
{
    public static class Constants
    {
        public static string AI_AUTHENTICATION_ENPOINT = "/security/authentication";
        public static string gps_device_tracking_app_id = "service_ai_los";
        public static string gps_device_tracking_app_key = "GdJszp99aFYGAtUXOfSu";
        public static string scoring_app_id = "tima_it3";
        public static string scoring_app_key = "AM59NAXm6NdUo5lDmq0u";
        public static string verify_address = "/cac/verify/address";
        public static string register_get_infor_AI = "/identify-service/tima-vay-dinhvi-los/{0}/register";
        public static string register_get_infor_xmcc_AI = "/identify-service/los-vay-dinhvi-chinhchu/{0}/register";
        public static string register_get_infor_xmkcc_AI = "/identify-service/los-vay-dinhvi-ko-chinhchu/{0}/register";
        public static string register_get_infor_taivay_AI = "/identify-service/los-vay-dinhvi-taivay-xe/{0}/register";
        public static string webhook_location_viettel = "/api/v1.0/webhook/location_viettel";
        public static string webhook_location_mobi = "/api/v1.0/webhook/location_mobi";
        public static string webhook_refphone_viettel = "/api/v1.0/webhook/ref_phone_viettel";
        public static string webhook_refphone_mobifone = "/api/v1.0/webhook/ref_phone_mobifone";
        public static string verify_refphone = "/cac/verify/refphone";
        public static string GET_CREDIT_SCORING = "/credit-score-by-vehicle/get_credit_score";
        public static string GET_RANGE_CREDIT_SCORE = "/credit-score-by-vehicle-new-label/{0}/register";
        public static string push_log_lead_score = "http://52.77.231.119:6060/credit-score-log";
        public static string GetPaymentById = "/api/Loan/GetPaymentLoanAPIForLos?LoanId={0}";
        public static string APP_ID_VAY_SIEU_NHANH = "service_ai_los";
        public static string APP_KEY_VAY_SIEU_NHANH = "GdJszp99aFYGAtUXOfSu";
        public static string GET_INFO_CHECK_EKYC = "/computer-vision/ekyc/v2";
        public static string GetListLoanTopUp = "/api/Loan/GetListLoanTopUp";
        public static string GetListLoanSuggestTopUp = "/api/Loan/GetListLoanSuggestTopUp";

        public static string APP_ID_TIMA_IT3 = "tima_it3";
        public static string APP_KEY_TIMA_IT3 = "AM59NAXm6NdUo5lDmq0u";
        public static string GET_MOTO_PRICE = "/moto-price/get_price";
        public static string SAVE_LOG_PRICE_MOTO_AI = "/moto-price-report/save-log";

        public static string SendSMS = "/api/SMSBrandName/SendSMSBrandNameFPT";

        public static string DeferredPayment = "/api/S2S/CheckCustomerDeferredPayment";

        public static string ChangePipeline = "/api/v1.0/Pipeline/change_pipeline";

        public static string get_info_network = "/message-validate/check_info_message";

        public static string ekyc_motorbike_registration_certificate = "/appraisal-auto-check/register";

        public static string chat_app_id = "tima-cskh";
        public static string chat_app_key = "qRQJMXzwQP2Z20wBKVcA";

        public static string Kalapa_GetFamily = "/api/Kapala/GetFamily?cardNumber={0}";
        public static string CheckBlackList = "/api/S2S/CheckBlacklistLOS";

        public static string register_fraud_detection = "/fraud-service/fraud-check/register";

        public static string parse_vin_number = "/vin-parser/parse_vin_number?vin_number={0}";

        public static string CHECK_BANK_ENDPOINT = "/tools/api/v1/check_bank?bank={0}&bank_account={1}";

        public static string FACEBOOK_PROFILE = "/credit-score-by-vehicle/facebook-profile/register?phone={0}";

        public static string CHECK_LOAN_MOMO = "/amazing-crawler/api/v1/momo/check_loan/{0}";
        public static string AddBlacklist = "/api/BlackList/InsertBlackList";
        public static string open_contract = "/gps-device/tracking/open-contract";
        public static string close_contract = "/gps-device/tracking/close-contract";

        #region ERP
        public static string get_employee_hub_checkin = "https://erp.tima.vn/api/api/userCheckedIn/{0}";
        public static string get_info_employee_hub_checkin = "https://erp.tima.vn/api/api/userCheckedInDetail/{0}";
        public static string get_all_coordinator_checkin = "https://erp.tima.vn/api/api/appraiserCheckedInDetail";
        #endregion


        #region Cisco
        public static string CISCO_AUTHORIZE = "/api/rest/authorize";
        public static string CISCO_EXPORTINGRECORDINGS = "/api/rest/recording/contact/{0}/export";
        public static string CISCO_EXPORTINGDETAILS = "/api/rest/recording/contact/{0}/export/{1}";
        public static string CISCO_GETLIST = "/api/rest/recording/contact?beginTime={0}";
        public static string CISCO_GETLIST_PHONE = "/api/rest/recording/contact?number={0}&beginTime={1}&endTime={2}";
        public static string CISCO_GETLIST_TIME = "/api/rest/recording/contact?beginTime={0}&endTime={1}";


        public static string CISCO_URL = "http://192.168.100.4";

        public static string CISCO_ID = "recording";
        public static string CISCO_USERID = "ccadmin";
        public static string CISCO_PASSWORD = "Tima@2020";
        public static string CISCO_DOMAIN = "tima.vn";



        #endregion

        public const string TOKEN_FIREBASE_HUB = "key=AAAA4Z8VT3w:APA91bGQyxLtoReh-_9aODXGjdVOYnnZJzcOmFV3OT_ikUflm8j7unGxC5uI09Q6u0yQ6nvGGU0UfQPOGqQtizbucylH2S06v_M8VIHdVAbCeGs5Kg6lgKD4MgG3qop30Ern9JxT_fYD";
        public const string TOKEN_FIREBASE_LENDING_ONLINE = "key=AAAARuLfC_U:APA91bEBLgsliSMhxBCK5ITCdK_XBXgDeDQ_bM-SMcsidQZGadU-FAwe3ZT-4am8o_S_zcHFGlZ2_KrbeDXUYVUUOF_SIFHirQ5yxhbZpS4S1L-6XzrHk4TZZebTQ0M8R9s1hrdDloIb";

        public const string LIST_PRODUCT_MOTOBIKE = "2, 5, 28";
        public const int LIMITED_DAY_15 = 15;

        public static string CREATE_LOAN_MIRAE = "/lead/createlead";

        public static string CLOUDFLARE_LIST_ZONE = "/client/v4/zones";
        public static string CLOUDFLARE_LIST_DNS_RECORD = "/client/v4/zones/{0}/dns_records?per_page=1000";
        public static string CLOUDFLARE_UPDATE_DNS_RECORD = "/client/v4/zones/{0}/dns_records/{1}";
    }
}
