﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;

namespace LOS.ThirdPartyProccesor.Helper
{
    public class ExtensionHelper
    {
        public static string DATE_FORMAT = "dd/MM/yyyy";
        public static string DATE_FORMAT_VN2 = "dd/MM/yyyy";
        public static string DATE_FORMAT_VN = "dd/MM/yyyy";
        public static string DATE_FORMAT_VN3 = "MM/dd/yyyy";
        public static string GetDescription(Enum value)
        {
            try
            {
                var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return da.Length > 0 ? da[0].Description : value.ToString();
            }
            catch
            {
                return null;
            }
        }

       
        public static DateTime ConvertStringToDate(string strDate, string format)
        {
            DateTime result;
            try
            {
                IFormatProvider celture = new CultureInfo("fr-FR", true);

                var arrayDate = strDate.Split('/');
                if (arrayDate[0].Length < 2)
                {
                    arrayDate[0] = "0" + arrayDate[0];
                }
                if (arrayDate[1].Length < 2)
                {
                    arrayDate[1] = "0" + arrayDate[1];
                }
                strDate = arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2];
                result = DateTime.ParseExact(strDate, format, celture, DateTimeStyles.NoCurrentDateDefault);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
