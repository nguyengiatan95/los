﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
	public class ChangePipelineReq
	{
		public class Input
        {
			public int loanBriefId { get; set; }
			public int productId { get; set; }
			public int status { get; set; }
		}

        public partial class Output
        {
            [JsonProperty("meta")]
            public Meta Meta { get; set; }
        }

        public partial class Meta
        {
            [JsonProperty("errorCode")]
            public long ErrorCode { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrorMessage { get; set; }
        }
    }
}
