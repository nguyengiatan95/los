﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class CheckBank
    {
        public partial class OutPut
        {
            [JsonProperty("status")]
            public long? Status { get; set; }

            [JsonProperty("data")]
            public Data Data { get; set; }

            [JsonProperty("message")]
            public string Messages { get; set; }

        }

        public partial class Data
        {
            [JsonProperty("accId")]
            public string AccId { get; set; }

            [JsonProperty("accName")]
            public string AccName { get; set; }

            [JsonProperty("bank")]
            public string Bank { get; set; }
        }
    }
}
