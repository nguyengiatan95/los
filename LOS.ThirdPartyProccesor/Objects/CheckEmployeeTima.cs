﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class CheckEmployeeTima
    {
        [JsonProperty("warehouses")]
        public object[] Warehouses { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }

        [JsonProperty("tel")]
        public string[] Tel { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("picture")]
        public Uri Picture { get; set; }

        [JsonProperty("nationalId")]
        public string NationalId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("createdAt")]
        public long CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public long UpdatedAt { get; set; }

        [JsonProperty("__v")]
        public long V { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public class HubEmployeeInfo
    {
        public string tel { get; set; }
        public string fullName { get; set; }
        public HubManagerInfo manager { get; set; }
    }

    public class HubManagerInfo
    {
        public string tel { get; set; }
        public string fullName { get; set; }
    }
}
