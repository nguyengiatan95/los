﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class Cisco
    {
        #region Authorize
        public partial class AuthorizeInput
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("userId")]
            public string UserId { get; set; }

            [JsonProperty("password")]
            public string Password { get; set; }

            [JsonProperty("data")]
            public Data Data { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("domain")]
            public string Domain { get; set; }
        }
        public partial class AuthorizeOutput
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("status")]
            public long Status { get; set; }

            [JsonProperty("userName")]
            public UserName UserName { get; set; }

            [JsonProperty("lang")]
            public string Lang { get; set; }

            [JsonProperty("country")]
            public string Country { get; set; }

            [JsonProperty("roles")]
            public string[] Roles { get; set; }

            [JsonProperty("bundle")]
            public string Bundle { get; set; }

            public string CSRFTOKEN { get; set; }
            public string JSESSIONID { get; set; }

        }
        public partial class UserName
        {
            [JsonProperty("first")]
            public string First { get; set; }

            [JsonProperty("last")]
            public string Last { get; set; }
        }
        #endregion

        #region List by Date
        public partial class GetList
        {
            [JsonProperty("queryCalculations")]
            public QueryCalculations QueryCalculations { get; set; }

            [JsonProperty("id")]
            public long Id { get; set; }

            public string Username { get; set; }
            public string DepartmentName { get; set; }

            [JsonProperty("startTime")]
            public long StartTime { get; set; }

            [JsonProperty("tz")]
            public string Tz { get; set; }

            [JsonProperty("callDuration")]
            public int? CallDuration { get; set; }

            [JsonProperty("offset")]
            public long Offset { get; set; }

            [JsonProperty("ani")]
            public string Ani { get; set; }
            [JsonProperty("line")]
            public string Line { get; set; }

            [JsonProperty("dnis")]
            public string Dnis { get; set; }

            [JsonProperty("audioUploadState")]
            public long AudioUploadState { get; set; }

            [JsonProperty("screenUploadState")]
            public long ScreenUploadState { get; set; }

            [JsonProperty("recordingType")]
            public string RecordingType { get; set; }

            [JsonProperty("icmCallId")]
            public string IcmCallId { get; set; }

            [JsonProperty("assocCallId")]
            public string AssocCallId { get; set; }

            [JsonProperty("contactType")]
            public string ContactType { get; set; }

            [JsonProperty("agent")]
            public Agent Agent { get; set; }

            [JsonProperty("isRootContact")]
            public bool IsRootContact { get; set; }


            [JsonProperty("group")]
            public Group Group { get; set; }

            [JsonProperty("recordingUrl")]
            public string RecordingUrl { get; set; }

        }
        public partial class QueryCalculations
        {
            [JsonProperty("avgCallDuration")]
            public string AvgCallDuration { get; set; }

            [JsonProperty("avgScore")]
            public string AvgScore { get; set; }

            [JsonProperty("count")]
            public string Count { get; set; }
        }
        public partial class Agent
        {
            [JsonProperty("displayId")]
            public string DisplayId { get; set; }

            [JsonProperty("lastName")]
            public string LastName { get; set; }

            [JsonProperty("firstName")]
            public string FirstName { get; set; }

            [JsonProperty("username")]
            public string Username { get; set; }
        }
        public partial class Group
        {
            [JsonProperty("name")]
            public string Name { get; set; }
        }
        #endregion

        #region Detail
        public partial class ExportInput
        {
            [JsonProperty("mediaFormat")]
            public string MediaFormat { get; set; }
        }
        public partial class ExportDetail
        {
            [JsonProperty("id")]
            public long Id { get; set; }

            [JsonProperty("exportUrl")]
            public string ExportUrl { get; set; }

            [JsonProperty("mediaFormat")]
            public string MediaFormat { get; set; }

            [JsonProperty("isComplete")]
            public bool IsComplete { get; set; }
        }
        #endregion
        public partial class ListExport
        {
            public string Phone { get; set; }
            public string DepartmentName { get; set; }

            public string Prefix { get; set; }
            public long CallDuration { get; set; }
            public TimeSpan Time { get; set; }

        }


        public class AuthenMobileReq
        {
            [JsonProperty("username")]
            public string UserName { get; set; }

            [JsonProperty("password")]
            public string Password { get; set; }
        }

        public class AuthenMobileRes
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("data")]
            public AuthenMobleData Data { get; set; }
        }


        public class AuthenMobleData
        {
            [JsonProperty("username")]
            public string UserName { get; set; }
            [JsonProperty("email")]
            public string Email { get; set; }
            [JsonProperty("fullname")]
            public string FullName { get; set; }
            [JsonProperty("role")]
            public string Rule { get; set; }
            [JsonProperty("token")]
            public string Token { get; set; }
        }

        public class MobileRecordReq
        {
            [JsonProperty("domain_id")]
            public string DomainId { get; set; }
            [JsonProperty("call_id")]
            public string CallId { get; set; }
        }
        public class MobileRecordRes
        {
            [JsonProperty("success")]
            public bool Success { get; set; }
            [JsonProperty("data")]
            public List<MobileRecordData> ListRecording { get; set; }
        }

        public class MobileRecordData
        {
            [JsonProperty("start_stamp")]
            public DateTime StartCall { get; set; }
            [JsonProperty("answer_stamp")]
            public DateTime AnswerCall { get; set; }
            [JsonProperty("end_stamp")]
            public DateTime EndCall { get; set; }
            [JsonProperty("caller_destination")]
            public string PhoneNumber { get; set; }
            [JsonProperty("record_path")]
            public string RecordPath { get; set; }
            [JsonProperty("call_id")]
            public string CallId { get; set; }
            [JsonProperty("user_id")]
            public string UserId { get; set; }
          
        }
    }
}
