﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class RecordingModel
    {
        public RecordingItem wav { get; set; }
    }
    public class RecordingItem
    {
        public int duration { get; set; }
        public bool? isUploaded { get; set; }
        public string mimeType { get; set; }
        public int offset { get; set; }
        public string playUrl { get; set; }
        public int mediaFileId { get; set; }
    }

    public class LogRecording
    {
        public int Id { get; set; }
        public string Dnis { get; set; }
        public string Ani { get; set; }
        public string PhoneNumber { get; set; }
        public string RecordingUrl { get; set; }
    }
}
