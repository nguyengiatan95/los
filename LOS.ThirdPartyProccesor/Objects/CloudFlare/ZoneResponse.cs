﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace LOS.ThirdPartyProccesor.Objects.CloudFlare
{
    public class ZoneResponse
    {
        [JsonProperty("result")]
        public List<ZoneItem> ListZone { get; set; }
        [JsonProperty("success")]
        public bool Success { get; set; }
    }

    public class ZoneItem
    {
        public string id { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public bool paused { get; set; }
        public string type { get; set; }
        public int development_mode { get; set; }
    }

    public class DnsRecordResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("result")]
        public List<DnsRecordItem> ListDnsRecord { get; set; }

        [JsonProperty("result_info")]
        public ResultInfo ResultInfo { get; set; }
    }

    public class ResultInfo
    {
        public int page { get; set; }
        public int per_page { get; set; }
        public int count { get; set; }
        public int total_count { get; set; }
        public int total_pages { get; set; }
    }

    public class DnsRecordItem
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("zone_id")]
        public string ZoneId { get; set; }
        [JsonProperty("zone_name")]
        public string ZoneName { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("content")]
        public string IpAddress { get; set; }
        [JsonProperty("proxied")]
        public bool Proxied { get; set; }
        [JsonProperty("ttl")]
        public int Ttl { get; set; }

    }
    public class UpdateDnsResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("result")]
        public DnsRecordItem DnsRecord { get; set; }
    }
    public class UpdateDnsRequest
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public int Ttl { get; set; }
        public bool Proxied { get; set; }
    }

    public class UpdateDnsItem
    {
        public string TypeId { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string Tll { get; set; }
        public bool Proxied { get; set; }
        public string ZoneId { get; set; }
        public string DnsRecordId { get; set; }
    }

}
