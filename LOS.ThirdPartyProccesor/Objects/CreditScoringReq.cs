﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class CreditScoringReq
    {
        public string RequestID { get; set; }
        public int ScoreRange { get; set; }
    }
}
