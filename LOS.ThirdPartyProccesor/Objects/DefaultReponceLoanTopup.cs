﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class DefaultReponceLoanTopup
    {
        public List<int> Data { get; set; }
        public List<string> Messages { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }
        public int Total { get; set; }
    }
}
