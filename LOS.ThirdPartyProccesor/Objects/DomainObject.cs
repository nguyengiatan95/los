﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class DomainObject
    {
        public string Url { get; set; }
        public string Domain { get; set; }
        public string IpAddress { get; set; }
        public string IpAddressBackup { get; set; }
        public int CountDownTime { get; set; }
        public bool IsChangeDns { get; set; } = false;
    }
}
