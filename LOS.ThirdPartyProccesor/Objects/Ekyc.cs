﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class Ekyc
    {
        public partial class EkycInput
        {
            [JsonProperty("front_id")]
            public string FrontId { get; set; }

            [JsonProperty("back_id")]
            public string BackId { get; set; }

            [JsonProperty("selfie")]
            public string Selfie { get; set; }

            [JsonProperty("fullname")]
            public string Fullname { get; set; }

            [JsonProperty("birthday")]
            public string Birthday { get; set; }

            [JsonProperty("national_id")]
            public string NationalId { get; set; }
        }

        public partial class EkycOutput
        {
            [JsonProperty("status")]
            public string Status { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }

            [JsonProperty("status_code")]
            public long StatusCode { get; set; }

            [JsonProperty("national_id")]
            public NationalId NationalId { get; set; }

            [JsonProperty("fraud_check")]
            public FraudCheck FraudCheck { get; set; }
        }

        public partial class FraudCheck
        {
            [JsonProperty("face_compare")]
            public Face FaceCompare { get; set; }

            [JsonProperty("face_query")]
            public Face FaceQuery { get; set; }
        }

        public partial class Face
        {
            [JsonProperty("code")]
            public string Code { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }
        }

        public partial class NationalId
        {
            [JsonProperty("address")]
            public Address Address { get; set; }

            [JsonProperty("id")]
            public Address Id { get; set; }

            [JsonProperty("fullname")]
            public Address Fullname { get; set; }

            [JsonProperty("birthday")]
            public Address Birthday { get; set; }

            [JsonProperty("expiry")]
            public Address Expiry { get; set; }

            [JsonProperty("gender")]
            public Address Gender { get; set; }

            [JsonProperty("ethnicity")]
            public Address Ethnicity { get; set; }

            [JsonProperty("issue_by")]
            public Address IssueBy { get; set; }

            [JsonProperty("issue_date")]
            public Address IssueDate { get; set; }

            [JsonProperty("religion")]
            public Address Religion { get; set; }
        }

        public partial class Address
        {
            [JsonProperty("value")]
            public string Value { get; set; }

            [JsonProperty("check")]
            public bool? Check { get; set; }
        }
    }
}
