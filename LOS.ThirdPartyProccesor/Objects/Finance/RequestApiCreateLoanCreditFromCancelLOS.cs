﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects.Finance
{
    public class RequestApiCreateLoanCreditFromCancelLOS
    {
        public string Phone { get; set; }
        public int ProductId { get; set; }
        public long TotalMoney { get; set; }
        public int LoanTime { get; set; }
        public int TypeTime { get; set; }
        public string FullName { get; set; }
        public int PlatformType { set; get; }
        public int CityId { set; get; }
        public int DistrictId { set; get; }
        public string AffCode { set; get; }
        public string CardNumber { set; get; }
        public string UtmSource { get; set; }
        public string UtmMedium { get; set; }
        public string UtmCampaign { get; set; }
        public string UtmContent { get; set; }
        public string UtmTerm { get; set; }
        public string Aff_Sid { get; set; }
        public string TId { get; set; }
        public string DomainName { get; set; }
    }
}
