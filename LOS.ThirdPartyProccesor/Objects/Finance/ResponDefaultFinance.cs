﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects.Finance
{
    public class ResponDefaultFinance
    {
        public List<string> Messages { get; set; }
        public int Status { get; set; }
        public string Data { get; set; }
        public int LoanCreditId { get; set; }
    }
}
