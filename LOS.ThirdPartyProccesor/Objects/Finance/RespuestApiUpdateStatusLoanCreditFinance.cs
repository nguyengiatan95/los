﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects.Finance
{
    public class RespuestApiUpdateStatusLoanCreditFinance
    {
        public int LoanCreditId { get; set; }
        public int Status { get; set; }
        public string ReasonName { get; set; }
    }
}
