﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class GeocodingResult
    {
        public PlusCode plus_code { get; set; }
        public List<result> results { get; set; }
    }
    public class result
    {
        public List<address_component> address_components { get; set; }
        public string formatted_address { get; set; }
        public string place_id { get; set; }
    }
    public class PlusCode
    {
        public string compound_code { get; set; }
        public string global_code { get; set; }
    }
    public class address_component
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }


    public class DistancematrixResult
    {
        public List<string> destination_addresses { get; set; }
        public List<string> origin_addresses { get; set; }
        public string status { get; set; }

        public List<Elements> rows { get; set; }
    }

    public class Elements
    {
        public List<ElementItem> elements { get; set; }
    }
    public class ElementItem
    {
        public ResultItem distance { get; set; }
        public ResultItem duration { get; set; }
        public string status { get; set; }
    }
    public class ResultItem
    {
        public string text { get; set; }
        public double value { get; set; }
    }
}
