﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class LoanDisbursementGoogleSheets
    {
        public string GoogleClickId { get; set; }
        public string ConversionName { get; set; }
        public string ConversionTime { get; set; }
    }
}
