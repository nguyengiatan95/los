﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class InfoFamilyRes
    {
        public int Result { get; set; }
        public string Message { get; set; }
        public string Data { get; set; }
    }

    public class DataResult
    {
        public int FamilyCheckID { get; set; }
    }

    public class DataFamily
    {
        public string houseOwner { get; set; }
        public List<FamilyItem> familyMember { get; set; }
    }

    public class FamilyItem
    {
        public string name { get; set; }
        public string gender { get; set; }
        public string dateOfBirth { get; set; }
        public string address { get; set; }
        public string personalId { get; set; }
        public string medicalInsurance { get; set; }
    }
}
