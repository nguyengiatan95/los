﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class LoanBriefVerifyDTO
    {
        public int LoanBriefId { get; set; }
        public int? ProductId { get; set; }
        public int? CustomerId { get; set; }
        public int? RateTypeId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public DateTime? Dob { get; set; }
        public int? Gender { get; set; }
        public string NationalCard { get; set; }
        public DateTime? NationalCardDate { get; set; }
        public string NationCardPlace { get; set; }
        public decimal? LoanAmount { get; set; }
        public string CityName { get; set; }
        public string AddressHome { get; set; }

        public string AddressHomeLatLng { get; set; }
        public string AddressOffice { get; set; }
        public string AddressOfficeLatLng { get; set; }
        public string JobName { get; set; }
        public bool IsTrackingLocation { get; set; }
        public int HomeNetwork { get; set; }
        public bool IsReborrow { get; set; }
        public List<LoanRelationshipDTO> Relationships { get; set; }
                     
    }
    public class LoanBriefVerifyDetail : LoanBriefVerifyDTO
    {
        public static Expression<Func<LoanBrief, LoanBriefVerifyDetail>> ProjectionDetail
        {
            get
            {
                return x => new LoanBriefVerifyDetail()
                {
                    LoanBriefId = x.LoanBriefId,
                    ProductId = x.ProductId,
                    Phone = x.Phone,
                    FullName = x.FullName,
                    Dob = x.Dob,
                    NationalCard = x.NationalCard,
                    CityName = x.Province != null ? x.Province.Name : string.Empty,
                    AddressHome = x.LoanBriefResident != null ? x.LoanBriefResident.Address : string.Empty,
                    AddressHomeLatLng = x.LoanBriefResident != null ? x.LoanBriefResident.AddressLatLng : string.Empty,
                    AddressOffice = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyAddress : string.Empty,
                    AddressOfficeLatLng = x.LoanBriefJob != null ? x.LoanBriefJob.CompanyAddressLatLng : string.Empty,
                    JobName = x.LoanBriefJob != null && x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.Name : string.Empty,
                    IsTrackingLocation = x.IsTrackingLocation.GetValueOrDefault(),
                    HomeNetwork = x.HomeNetwork.GetValueOrDefault(),
                    IsReborrow = x.IsReborrow.GetValueOrDefault(),
                    Relationships = x.LoanBriefRelationship != null ? x.LoanBriefRelationship.Select(k => new LoanRelationshipDTO()
                    {
                        RelationshipType = k.RelationshipType,
                        FullName = k.FullName,
                        Phone = k.Phone

                    }).ToList() : null,

                };
            }
        }
    }
}
