﻿using LOS.Common.Extensions;
using LOS.Common.Helpers;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class LoanBriefInfoDTO
    {
        public int ID { get; set; }
        public Decimal TotalMoney { get; set; }
        public int LoanTime { get; set; }
        public int RateType { get; set; }
        public int ProductID { get; set; }

        public string FullName { get; set; }
        public string CardNumber { get; set; }
        public string Phone { get; set; }
        public int? Gender { get; set; }
        public decimal? Salary { get; set; }
        public int JobId { get; set; }
        public string JobName { get; set; }

        //hình thức nhận tiền giải ngân
        public int? TypeReceivingMoney { get; set; }

        //id hình thức người tham chiếu
        public int RelativeFamilyId { get; set; }
        public bool ReMarketing { get; set; }
        //tình trạng hôn nhân của khách hàng
        public int? IsMarried { get; set; }
        public int? NumberBaby { get; set; }
        //tuổi của khách hàng
        public int CustomerAge { get; set; }
        //khách hàng có nợ xấu hay không theo CIC
        public bool HasBadDebt { get; set; }
        //số lần khách hàng vay lại
        public int LoanAgain { get; set; }
        // thành phố khách hàng đang sinh sống
        public string CityName { get; set; }

        public string PhoneFamily { get; set; }
        // khách hàng có trợ nợ muộn hay không theo CIC
        public bool HasLatePayment { get; set; }

        //hình thức nhận lương
        public int? ReceiveYourIncome { get; set; }
        public string TypeIncomeName { get; set; }
        public int? IsLivingTogether { get; set; }

        //mã hub tiếp nhận đơn vay
        public int Code { get; set; }
        //hình thức sở hữu
        public int TypeOfOwnershipId { get; set; }
        //tên khách hàng tại CIC
        public string CICName { get; set; }

        public decimal? LoanAmountExpertiseAI { get; set; } //Giá xe trên thị trường AI
    }

    public class LoanBriefInfoTopupDTO
    {
        public int LoanBriefId { get; set; }
        public int ProductId { get; set; }
        public int LmsLoanId { get; set; }
        public LoanBriefProperty LoanBriefProperty { get; set; }
        public LoanBriefResident LoanBriefResident { get; set; }
        public LoanBriefJob LoanBriefJob { get; set; }
        public  ICollection<LoanBriefRelationship> LoanBriefRelationship { get; set; }
    }

    public class LoanBriefInfo : LoanBriefInfoDTO
    {
        //public Province Province { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefInfo>> ProjectionDetail
        {

            get
            {
                return x => new LoanBriefInfo()
                {
                    ID = x.LoanBriefId,
                    TotalMoney = x.LoanAmount.GetValueOrDefault(),
                    LoanTime = x.LoanTime.GetValueOrDefault(),
                    RateType = x.RateTypeId.GetValueOrDefault(),
                    ProductID = x.ProductId.GetValueOrDefault(),
                    FullName = x.FullName,
                    CardNumber = x.NationalCard,
                    Phone = x.Phone,
                    Gender = x.Gender.HasValue ? x.Gender : null,
                    Salary = x.LoanBriefJob != null ? x.LoanBriefJob.TotalIncome : 0,
                    JobId = x.LoanBriefJob != null ? x.LoanBriefJob.JobId.GetValueOrDefault() : 0,
                    JobName = x.LoanBriefJob != null && x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.Name : "",
                    TypeReceivingMoney = x.ReceivingMoneyType.HasValue ? x.ReceivingMoneyType : null,
                    RelativeFamilyId = x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0 ? x.LoanBriefRelationship.ToList()[0].RelationshipType.GetValueOrDefault() : 0,
                    ReMarketing = x.TypeRemarketing == (int)EnumTypeRemarketing.IsRemarketing,
                    IsMarried = x.Customer != null && x.Customer.IsMerried.HasValue ? x.Customer.IsMerried : null,
                    NumberBaby = x.Customer != null && x.Customer.NumberBaby.HasValue ? x.Customer.NumberBaby : null,
                    CustomerAge = x.Customer.Dob != null ? (DateTime.Now.Year - x.Customer.Dob.Value.Year) + 1 : 0,
                    HasBadDebt = false,
                    HasLatePayment = false,
                    LoanAgain = 0,
                    CityName = x.Province != null ? x.Province.Name : "",
                    PhoneFamily = x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0 ? x.LoanBriefRelationship.ToList()[0].Phone : "",
                    ReceiveYourIncome = x.LoanBriefJob != null && x.LoanBriefJob.ImcomeType.HasValue ? x.LoanBriefJob.ImcomeType : null,
                    TypeIncomeName = x.LoanBriefJob.ImcomeType != null ? ExtentionHelper.GetDescription((EnumImcomeType)x.LoanBriefJob.ImcomeType) : "",
                    IsLivingTogether = x.LoanBriefResident != null && x.LoanBriefResident.LivingWith.HasValue ? x.LoanBriefResident.LivingWith : null,
                    Code = x.HubId.GetValueOrDefault(),
                    TypeOfOwnershipId = x.LoanBriefResident != null && x.LoanBriefResident.ResidentType > 0 ? x.LoanBriefResident.ResidentType.Value : 0,
                    CICName = "",
                    LoanAmountExpertiseAI= x.LoanAmountExpertiseAi
                };
            }
        }
    }

    public class LoanBriefInfoCreditScoring : LoanBriefInfoDTO
    {
        public int? NumberCall { get; set; }
        public bool? IsHeadOffice { get; set; }
        public bool? IsLocate { get; set; }
        public int? HubId { get; set; }
        public bool? IsReborrow { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefInfoCreditScoring>> ProjectionDetail
        {

            get
            {
                return x => new LoanBriefInfoCreditScoring()
                {
                    ID = x.LoanBriefId,
                    TotalMoney = x.LoanAmount.GetValueOrDefault(),
                    LoanTime = x.LoanTime.GetValueOrDefault(),
                    RateType = x.RateTypeId.GetValueOrDefault(),
                    ProductID = x.ProductId.GetValueOrDefault(),
                    FullName = x.FullName,
                    CardNumber = x.NationalCard,
                    Phone = x.Phone,
                    Gender = x.Gender.HasValue ? x.Gender : null,
                    //Salary = x.LoanBriefJob != null ? x.LoanBriefJob.TotalIncome : 0,
                    JobId = x.LoanBriefJob != null ? x.LoanBriefJob.JobId.GetValueOrDefault() : 0,
                    JobName = x.LoanBriefJob != null && x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.Name : "",
                    TypeReceivingMoney = x.ReceivingMoneyType.HasValue ? x.ReceivingMoneyType : null,
                    RelativeFamilyId = x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0 ? x.LoanBriefRelationship.ToList()[0].RelationshipType.GetValueOrDefault() : 0,
                    ReMarketing = x.TypeRemarketing == (int)EnumTypeRemarketing.IsRemarketing,
                    IsMarried = x.Customer != null && x.Customer.IsMerried.HasValue ? x.Customer.IsMerried : null,
                    NumberBaby = x.Customer != null && x.Customer.NumberBaby.HasValue ? x.Customer.NumberBaby : null,
                    CustomerAge = x.Customer.Dob != null ? (DateTime.Now.Year - x.Customer.Dob.Value.Year) + 1 : 0,
                    HasBadDebt = false,
                    HasLatePayment = false,
                    LoanAgain = 0,
                    CityName = x.Province != null ? x.Province.Name : "",
                    PhoneFamily = x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0 ? x.LoanBriefRelationship.ToList()[0].Phone : "",
                    ReceiveYourIncome = x.LoanBriefJob != null && x.LoanBriefJob.ImcomeType.HasValue ? x.LoanBriefJob.ImcomeType : null,
                    TypeIncomeName = x.LoanBriefJob.ImcomeType != null ? ExtentionHelper.GetDescription((EnumImcomeType)x.LoanBriefJob.ImcomeType) : "",
                    IsLivingTogether = x.LoanBriefResident != null && x.LoanBriefResident.LivingWith.HasValue ? x.LoanBriefResident.LivingWith : null,
                    Code = x.HubId.GetValueOrDefault(),
                    TypeOfOwnershipId = x.LoanBriefResident != null && x.LoanBriefResident.ResidentType > 0 ? x.LoanBriefResident.ResidentType.Value : 0,
                    CICName = "",
                    LoanAmountExpertiseAI = x.LoanAmountExpertiseAi,
                    NumberCall = x.NumberCall,
                    IsHeadOffice = x.IsHeadOffice,
                    IsLocate = x.IsLocate,
                    HubId = x.HubId,
                    IsReborrow = x.IsReborrow
                };
            }
        }
    }

    public class LoanBriefInfoLogDTO
    {
        public int ID { get; set; }
        public Decimal TotalMoney { get; set; }
        public int LoanTime { get; set; }
        public int RateType { get; set; }
        public int ProductID { get; set; }

        public string FullName { get; set; }
        public string CardNumber { get; set; }
        public string Phone { get; set; }
        public int Gender { get; set; }
        public decimal? Salary { get; set; }
        public int JobId { get; set; }
        public string JobName { get; set; }

        //hình thức nhận tiền giải ngân
        public int TypeReceivingMoney { get; set; }

        //id hình thức người tham chiếu
        public int RelativeFamilyId { get; set; }
        public bool ReMarketing { get; set; }
        //tình trạng hôn nhân của khách hàng
        public int IsMarried { get; set; }
        public int NumberBaby { get; set; }
        //tuổi của khách hàng
        public int CustomerAge { get; set; }
        //khách hàng có nợ xấu hay không theo CIC
        public bool HasBadDebt { get; set; }
        //số lần khách hàng vay lại
        public int LoanAgain { get; set; }
        // thành phố khách hàng đang sinh sống
        public string CityName { get; set; }

        public string PhoneFamily { get; set; }
        // khách hàng có trợ nợ muộn hay không theo CIC
        public bool HasLatePayment { get; set; }

        //hình thức nhận lương
        public int ReceiveYourIncome { get; set; }
        public string TypeIncomeName { get; set; }
        public int IsLivingTogether { get; set; }

        //mã hub tiếp nhận đơn vay
        public int Code { get; set; }
        //hình thức sở hữu
        public int TypeOfOwnershipId { get; set; }
        //tên khách hàng tại CIC
        public string CICName { get; set; }

        public decimal Score { get; set; }
        public string Label { get; set; }
        public string Version { get; set; }
        public bool IsPass { get; set; }
    }

    public class LoanBriefInfoLog : LoanBriefInfoLogDTO
    {
        public static Expression<Func<LoanBrief, LoanBriefInfoLog>> ProjectionLogDetail
        {

            get
            {
                return x => new LoanBriefInfoLog()
                {
                    ID = x.LoanBriefId,
                    TotalMoney = x.LoanAmount.GetValueOrDefault(),
                    LoanTime = x.LoanTime.GetValueOrDefault(),
                    RateType = x.RateTypeId.GetValueOrDefault(),
                    ProductID = x.ProductId.GetValueOrDefault(),
                    FullName = x.FullName,
                    CardNumber = x.NationalCard,
                    Phone = x.Phone,
                    Gender = x.Gender.GetValueOrDefault(),
                    Salary = x.LoanBriefJob != null ? x.LoanBriefJob.TotalIncome : 0,
                    JobId = x.LoanBriefJob != null ? x.LoanBriefJob.JobId.GetValueOrDefault() : 0,
                    JobName = x.LoanBriefJob != null && x.LoanBriefJob.Job != null ? x.LoanBriefJob.Job.Name : "",
                    TypeReceivingMoney = x.ReceivingMoneyType.GetValueOrDefault(),
                    RelativeFamilyId = x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0 ? x.LoanBriefRelationship.ToList()[0].RelationshipType.GetValueOrDefault() : 0,
                    ReMarketing = x.TypeRemarketing == (int)EnumTypeRemarketing.IsRemarketing,
                    IsMarried = x.Customer != null ? x.Customer.IsMerried.GetValueOrDefault() : 0,
                    NumberBaby = x.Customer != null ? x.Customer.NumberBaby.GetValueOrDefault() : 0,
                    CustomerAge = x.Customer.Dob != null ? (DateTime.Now.Year - x.Customer.Dob.Value.Year) + 1 : 0,
                    HasBadDebt = false,
                    HasLatePayment = false,
                    LoanAgain = 0,
                    CityName = x.Province != null ? x.Province.Name : "",
                    PhoneFamily = x.LoanBriefRelationship != null && x.LoanBriefRelationship.Count > 0 ? x.LoanBriefRelationship.ToList()[0].Phone : "",
                    ReceiveYourIncome = x.LoanBriefJob != null ? x.LoanBriefJob.ImcomeType.GetValueOrDefault() : 0,
                    TypeIncomeName = x.LoanBriefJob.ImcomeType != null ? ExtentionHelper.GetDescription((EnumImcomeType)x.LoanBriefJob.ImcomeType) : "",
                    IsLivingTogether = x.LoanBriefResident != null ? x.LoanBriefResident.LivingWith.GetValueOrDefault() : 0,
                    Code = x.HubId.GetValueOrDefault(),
                    TypeOfOwnershipId = x.LoanBriefResident != null && x.LoanBriefResident.ResidentType > 0 ? x.LoanBriefResident.ResidentType.Value : 0,
                    CICName = "",
                    Score = x.LeadScore.Value,
                    Label = x.LabelScore,
                    Version = "2.0"
                };
            }
        }
    }

    public class LoanBriefInfoTopup : LoanBriefInfoTopupDTO
    {
        public int? ProductDetailId { get; set; }
        public int? Status { get; set; }
        public int? TypeLoanSupport { get; set; }
        public int? TypeRemarketing { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefInfoTopup>> ProjectionDetail
        {

            get
            {
                return x => new LoanBriefInfoTopup()
                {
                    LoanBriefId = x.LoanBriefId,
                    Status = x.Status,
                    LmsLoanId = x.LmsLoanId.Value,
                    ProductId = x.ProductId.Value,
                    LoanBriefRelationship = x.LoanBriefRelationship ?? null,
                    LoanBriefResident = x.LoanBriefResident ?? null,
                    LoanBriefProperty = x.LoanBriefProperty ?? null,
                    LoanBriefJob = x.LoanBriefJob ?? null,
                    ProductDetailId = x.ProductDetailId,
                    TypeLoanSupport = x.TypeLoanSupport,
                    TypeRemarketing = x.TypeRemarketing
                };
            }
        }
    }

    public class LoanBriefInfoForTopup 
    {
        public int LoanBriefId { get; set; }
        public int? ProductId { get; set; }
        public int? ProductDetailId { get; set; }
        public int? LmsLoanId { get; set; }
        public string NationalCard { get; set; }
        public LoanBriefProperty LoanBriefProperty { get; set; }
        public int? TypeRemarketing { get; set; }
        public int ResidentType { get; set; }
        public static Expression<Func<LoanBrief, LoanBriefInfoForTopup>> ProjectionDetail
        {

            get
            {
                return x => new LoanBriefInfoForTopup()
                {
                    LoanBriefId = x.LoanBriefId,
                    LmsLoanId = x.LmsLoanId.Value,
                    ProductId = x.ProductId.Value,
                    LoanBriefProperty = x.LoanBriefProperty ?? null,
                    ProductDetailId = x.ProductDetailId,
                    NationalCard = x.NationalCard,
                    TypeRemarketing = x.TypeRemarketing,
                    ResidentType = x.LoanBriefResident != null? x.LoanBriefResident.ResidentType.Value : 0
                };
            }
        }
    }
}
