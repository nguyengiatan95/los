﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class LoanDisbursed
    {
        public int LoanbriefId { get; set; }
        public int CustomerId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string NationalCard { get; set; }
        public int? TypeRemarketing { get; set; }
        public int? TypeLoanSupport { get; set; }
    }
}
