﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class LoanbriefDistributingTelelsale
    {
        public int LoanBriefId { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public int? BoundTelesaleId { get; set; }
        public int? CreateBy { get; set; }
        public int? HubId { get; set; }
        public int? Status { get; set; }
        public int? PlatformType { get; set; }
        public int? ProvinceId { get; set; }
        public int? ProductId { get; set; }
        public int? DistrictId { get; set; }
        public int? TeamTelesalesId { get; set; }
    }
}
