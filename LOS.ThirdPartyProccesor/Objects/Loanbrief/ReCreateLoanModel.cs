﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class ReCreateLoanModel
    {
        public int Id { get; set; }
        public int LoanbriefId { get; set; }
        public int CreatedBy { get; set; }
        public string Phone { get; set; }
        public string FullName { get; set; }
        public string NationalCard { get; set; }
        public string UtmSource { get; set; }
        public int TypeRemarketing { get; set; }
        public int? TypeReLoanbrief { get; set; }
        public int? TelesaleId { get; set; }
        public bool? ReBorrow { get; set; }
        public int? HubId { get; set; }
        public int? TeamTelesaleId { get; set; }

        public decimal? LoanAmountExpertiseNew { get; set; }
        public decimal? LoanAmountExpertiseLastNew { get; set; }
        public decimal? LoanAmountNew { get; set; }

    }
}
