﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class LocationAIReq
    {
        public LocationCode locationCodes { get; set; }
        public string phone { get; set; }
        public int age { get; set; }
        public string url { get; set; }
        public string carrier { get; set; }
        public bool office { get; set; }
        public bool validateAddress { get; set; }
    }
    public class LocationCode
    {
        public string home { get; set; }
        public string office { get; set; }
        public string other { get; set; }
    }

    public class LocationAIRes
    {
        public int code { get; set; }
        public string messages { get; set; }
        public DataVerify data { get; set; }
    }

    public class DataVerify
    {
        public string refCode { get; set; }
        public string status { get; set; }
    }

    public class RefPhoneAIRes
    {
        public int code { get; set; }
        public string messages { get; set; }
        public DataVerify data { get; set; }
    }

    public class RangeCreditScoringRes
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public string RequestID { get; set; }
    }

    public class FacebookInfoRes
    {
        public int status_code { get; set; }
        public string messages { get; set; }
    }

    public class FraudDetectionResponse
    {
        public int? status { get; set; }
        public string mess { get; set; }
        public int response_code { get; set; }
        public ResultRegisterFraudDetection result { get; set; }
        public string warning_message { get; set; }
    }

    public class ResultRegisterFraudDetection
    {
        public string reference_code { get; set; }
        public string register_time { get; set; }
    }

    public class ParseVinNumberResponse
    {
        public string message { get; set; }
        public int status_code { get; set; }
        public ParseVinNumberItem result { get; set; }
    }

    public class ParseVinNumberItem
    {
        public string full_name { get; set; }
        public int year { get; set; }
    }

}
