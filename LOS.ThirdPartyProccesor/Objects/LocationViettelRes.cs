﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class LocationViettelRes
    {
        public string refCode { get; set; }
        public int status { get; set; }
        public int code { get; set; }
        public string desc { get; set; }
        public DataInput data { get; set; }
        public InfoLocationViettel locations { get; set; }
        public bool workFromHome { get; set; }
        public string finalResult { get; set; }
    }

    public class DataInput
    {
        public DataItem home { get; set; }
        public DataItem office { get; set; }
        public DataItem other { get; set; }
    }

    public class DataItem
    {
        public string result { get; set; }
        public float point { get; set; }
    }

    public class ResultLocationItem
    {
        public int radius { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public int[] rates { get; set; }
    }

    public class InfoLocationViettel
    {
        public ResultLocationItem home { get; set; }
        public ResultLocationItem office { get; set; }
        public ResultLocationItem other { get; set; }
    }
}