﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class RequestCreateLoan
    {
        public string phoneNbr { get; set; }
        public string currAddress { get; set; }
        public string priAddress { get; set; }
        public string income { get; set; }
        public string company { get; set; }
        public string companyAddress { get; set; }
        public string workingTime { get; set; }
        public string nationalId { get; set; }
        public string vendorCode { get; set; }
        public DateTime createDatetime { get; set; }
        public string typeCreate { get; set; }
        public string campaign { get; set; }
        public string product { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string importId { get; set; }
        public string age { get; set; }
        public string allowQualified { get; set; }
        public string loanAmt { get; set; }
        public string tenure { get; set; }
        public string scoreMaketing { get; set; }
        public string scoreIncome { get; set; }
    }
}
