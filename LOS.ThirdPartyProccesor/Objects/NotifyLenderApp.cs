﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
  public  class NotifyLenderApp
    {
        public string key { get; set; }
        public List<DataItemNotify> data { get; set; }
    }

    public class DataItemNotify
    {
        public int loanCreditId { get; set; }
        public string topic { get; set; }
    }
}
