﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class InputOpenContract
    {
        public string contractid { get; set; }
        public string imei { get; set; }
        public string shopid { get; set; }
        public string vehicle_type { get; set; }
        public string contract_type { get; set; }
    }

    public class OutputOpenContract
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class InputCloseContract
    {
        public string contractid { get; set; }
        public string imei { get; set; }
    }

    public class OutputCloseContract
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

}
