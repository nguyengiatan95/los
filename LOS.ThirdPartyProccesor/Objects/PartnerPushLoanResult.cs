﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
	public class PartnerPushLoanResult
	{
		public int status { get; set; }
		public int customerId { get; set; }
		public LogPushToPartner log { get; set; }
	}
}
