﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class ProductAiSupportItem
    {
        public string brand { get; set; }
        public int first_year { get; set; }
        public int last_year { get; set; }
        public string model { get; set; }
        public int type { get; set; }
    }

}
