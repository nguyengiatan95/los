﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class RefphoneMobiReq
    {
        public string refCode { get; set; }
        public int status { get; set; }
        public string desc { get; set; }
        public ResultDataPointMobi data { get; set; }
        public ResultRefphoneMobi refPhones { get; set; }
        public string finalResult { get; set; }
    }
    public class ResultDataPointMobi
    {
        public DataPoint num1 { get; set; }
        public DataPoint? num2 { get; set; }
        public DataPoint? num3 { get; set; }
    }

    public class ResultRefphoneMobi
    {
        public string requestId { get; set; }
        public string providerCode { get; set; }
        public string result { get; set; }
        public string phone { get; set; }
    }
}
