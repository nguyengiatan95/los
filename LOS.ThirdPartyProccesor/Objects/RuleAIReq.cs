﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
   public class RuleAIReq
    {
        public string name { get; set; }
        public string age { get; set; }
        public string national_id { get; set; }
        public PhoneCustomer phone { get; set; }
        public string city_name { get; set; }
        public string address_home { get; set; }
        public string address_office { get; set; }
        public string address_other { get; set; }
        public string reference_name { get; set; }
        public string reference_phone { get; set; }
        public string job { get; set; }
        public bool is_office { get; set; }
    }

    public class PhoneCustomer
    {
        public string number { get; set; }
        public string carrier { get; set; }
        public string carrierCode { get; set; }
    }
    public class RuleAIRes
    {
        public string status { get; set; }
        public string message { get; set; }
        public string register_code { get; set; }
    }
}
