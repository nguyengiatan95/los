﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class ScoringReq
    {
        public class Create
        {
            public int ID { get; set; }
            public int TotalMoney { get; set; }
            public int LoanTime { get; set; }
            public int RateType { get; set; }
            public int ProductID { get; set; }

            public string FullName { get; set; }
            public string CardNumber { get; set; }
            public string Phone { get; set; }
            public int Gender { get; set; }
            public int Salary { get; set; }
            public int JobId { get; set; }
            public string JobName { get; set; }

            //hình thức nhận tiền giải ngân
            public int TypeReceivingMoney { get; set; }

            //id hình thức người tham chiếu
            public int RelativeFamilyId { get; set; }
            public bool ReMarketing { get; set; }
            //tình trạng hôn nhân của khách hàng
            public int IsMarried { get; set; }
            public int NumberBaby { get; set; }
            //tuổi của khách hàng
            public int CustomerAge { get; set; }
            //khách hàng có nợ xấu hay không theo CIC
            public bool HasBadDebt { get; set; }
            //số lần khách hàng vay lại
            public int LoanAgain { get; set; }
            // thành phố khách hàng đang sinh sống
            public string CityName { get; set; }
            
            public string PhoneFamily { get; set; }
            // khách hàng có trợ nợ muộn hay không theo CIC
            public bool HasLatePayment { get; set; }

            //hình thức nhận lương
            public int ReceiveYourIncome { get; set; }
            public string TypeIncomeName { get; set; }
            public int IsLivingTogether { get; set; }

            //mã hub tiếp nhận đơn vay
            public int Code { get; set; }
            //hình thức sở hữu
            public int TypeOfOwnershipId { get; set; }
            //tên khách hàng tại CIC
            public string CICName { get; set; }
            
        }
    }
}
