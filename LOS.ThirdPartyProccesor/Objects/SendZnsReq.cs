﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
   public class SendZnsReq
    {
        public string command { get; set; } = "SEND_ZNS_RECALL";

        public string customer_id { get; set; }
        public string customer_type { get; set; } = "ZALO";
        public string nv_id { get; set; }
        public TemplateDataZns template_data { get; set; }
    }

    public class TemplateDataZns
    {
        public string customer_name { get; set; }
        public string loan_id { get; set; }
        public string ngay_dang_ky { get; set; }
    }

    public class SendZnsRes
    {

        public int StatusCode { get; set; }
        public string customer_type { get; set; }
    }
}
