﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects.Setttings
{
    public class AppCiscoMobileSettings
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string BaseUrl { get;set; }
        public string AuthenUrl { get;set; }
        public string DomainId { get;set; }
        public string RecordBaseUrl { get;set; }
        public string DetailRecordUrl { get;set; }
    }
}
