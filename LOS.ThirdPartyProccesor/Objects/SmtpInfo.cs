﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class SmtpInfo
    {
        public string ApiKey { get; set; }

        public string AuthenticationUserName { get; set; }

        public string AuthenticationPassword { get; set; }

        public bool HasAuthentication { get; set; }

        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }
    }
}
