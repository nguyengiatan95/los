﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.Objects
{
    public class SuggestionsInformationInput
    {
        public string brand { get; set; }
        public string model { get; set; }
        public string regdate { get; set; }
        public string phanh { get; set; }
        public string vanh { get; set; } 
        public string token { get; set; }  
        public string access_token { get; set; } 
        public int LoanBriefId { get; set; } 
    }
    public class PriceMotobikeAiReqest
    {
        public string brand { get; set; }
        public string model { get; set; }
        public string regdate { get; set; }
        public string phanh { get; set; }
        public string vanh { get; set; }
    }
    public class SuggestionsInformationResult
    {
        public decimal predicted_price { get; set; }
        public ranged_price ranged_price { get; set; }
        public string status { get; set; }
        public string predicted_price_string { 
            get {
                if(predicted_price > 0)
                {
                    return predicted_price.ToString("#,##0") + " vnđ";
                }
                return status;
            } }
    }
    public class ranged_price
    {
        public decimal lower_bound { get; set; }
        public decimal upper_bound { get; set; }
    }

    public class RequestSaveLogMotoPrice
    {
        public string brand { get; set; }
        public string model { get; set; }
        public int regdate { get; set; }
        public long AI_price { get; set; }
        public long IT_price { get; set; }
        public string phanh { get; set; }
        public string vanh { get; set; }
        public string data_raw { get; set; }
        public int loan_credit_id { get; set; }
    }
    public class ResponSaveLogMotoPrice
    {
        public string status { get; set; }
        public int status_code { get; set; }
    }
}
