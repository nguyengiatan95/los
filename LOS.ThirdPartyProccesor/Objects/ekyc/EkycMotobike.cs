﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects.ekyc
{
    public class EkycMotobike
    {
        public string mess { get; set; }
        public int response_code { get; set; }
        public ResultInfoMotor result { get; set; }
    }
    public class ResultInfoMotor
    {
        public EnoughImageTypeItemMotor is_enough_image_types { get; set; }
        public bool? is_same_name { get; set; }
        public bool? is_same_plate { get; set; }
        public bool? is_same_brand { get; set; }
        public ExtractInfoMotor extract_info { get; set; }
    }

    public class EnoughImageTypeItemMotor
    {
        public bool? back_vr { get; set; }
        public bool? front_vr { get; set; }
        public bool? back_lighted_vr { get; set; }
        public bool? front_lighted_vr { get; set; }
    }

    public class ExtractInfoMotor
    {
        public string name { get; set; }
        public string plate { get; set; }
        public string engine { get; set; }
        public string chassis { get; set; }
        public string brand { get; set; }
        public string address { get; set; }
        public string color { get; set; }
        public string capacity { get; set; }
        public string district { get; set; }
        public string model_code { get; set; }
        public string first_registration { get; set; }
        public string issue_date { get; set; }
    }
}
