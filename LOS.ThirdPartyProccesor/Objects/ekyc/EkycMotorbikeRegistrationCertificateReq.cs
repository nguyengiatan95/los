﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects.ekyc
{
    public class EkycMotorbikeRegistrationCertificateReq
    {
        [JsonProperty("brand")]
        public string brand { get; set; }
        [JsonProperty("images")]
        public List<string> images { get; set; }

        [JsonProperty("loan_brief_id")]
        public int LoanbriefId { get; set; }
        [JsonProperty("name")]
        public string FullName { get; set; }
        [JsonProperty("ownership")]
        public Nullable<bool> Ownership { get; set; }
        [JsonProperty("plate")]
        public string NumberPlate { get; set; }
    }

    public class EkycMotorbikeRegistrationCertificateRes
    {
        public string mess { get; set; }
        public int response_code { get; set; }
        public EkycMotorbikeRegistrationCertificateResultRequest result { get; set; }
    }

    public class EkycMotorbikeRegistrationCertificateResultRequest {
        public string reference_code { get; set; }
        public string register_time { get; set; }
    }


    public class ResultInfo
    {
        public EnoughImageTypeItem is_enough_image_types { get; set; }
        public bool? is_same_name { get; set; }
        public bool? is_same_plate { get; set; }
        public bool? is_same_brand { get; set; }
        public ExtractInfo extract_info { get; set; }
    }

    public class EnoughImageTypeItem
    {
        public bool? back_vr { get; set; }
        public bool? front_vr { get; set; }
        public bool? back_lighted_vr { get; set; }
        public bool? front_lighted_vr { get; set; }
    }

    public class ExtractInfo
    {
        public string name { get; set; }
        public string plate { get; set; }
        public string engine { get; set; }
        public string chassis { get; set; }
        public string brand { get; set; }
        public string address { get; set; }
        public string color { get; set; }
        public string capacity { get; set; }
        public string district { get; set; }
        public string model_code { get; set; }
        public string first_registration { get; set; }
        public string issue_date { get; set; }
    }
}
