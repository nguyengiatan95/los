﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.ThirdPartyProccesor.Objects.ekyc
{
    public class InfoNetworkReq
    {
        [JsonProperty("image_url")]
        public string ImageUrl { get; set; }
        [JsonProperty("fullname")]
        public string FullName { get; set; }
        [JsonProperty("id_number")]
        public string NationalCard { get; set; }
        [JsonProperty("date_of_birth")]
        public string BirthDay { get; set; }
        [JsonProperty("loan_brief_id")]
        public int LoanbriefId { get; set; }
    }

    public class InfoNetworkRes
    {
        public string response_code { get; set; }
        public string mess { get; set; }
        public bool? info_check_result { get; set; }
        public ExtractedInfo extracted_info { get; set; }
        public CompareInfo compare_results { get; set; }
    }

    public class ExtractedInfo
    {
        public string dob { get; set; }
        public string fullname { get; set; }
        public string id_number { get; set; }
        public string issue_date { get; set; }
    }
    public class CompareInfo
    {
        public bool? same_dob { get; set; }
        public bool? same_fullname { get; set; }
        public bool? same_id_number { get; set; }
    }

}
