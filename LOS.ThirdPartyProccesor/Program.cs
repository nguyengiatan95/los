﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Hangfire;
using Hangfire.MemoryStorage;
using LOS.DAL.EntityFramework;
using LOS.DAL.Respository;
using LOS.DAL.UnitOfWork;
using LOS.Services.Services.LMS;
using LOS.Services.Services.Loanbrief;
using LOS.Services.Services.LogCallApi;
using LOS.ThirdPartyProccesor.Objects.Setttings;
using LOS.ThirdPartyProccesor.Services;
using LOS.ThirdPartyProccesor.worker;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Compact;

namespace LOS.ThirdPartyProccesor
{
    public class Program
    {
        private static readonly AutoResetEvent autoResetEvent = new AutoResetEvent(false);
        public static void Main(string[] args)
        {

            CreateHostBuilder(args).Build().Run();
            Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Override("Microsoft", LogEventLevel.Fatal)
                        .Enrich.FromLogContext()
                        .WriteTo.File(new CompactJsonFormatter(), "logs/tima.log", rollOnFileSizeLimit: true, fileSizeLimitBytes: 10_000_000, rollingInterval: RollingInterval.Day)
                        .WriteTo.Seq("http://seq:5341")
                        .CreateLogger();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                    Console.WriteLine(environmentName);

                    var builder = new ConfigurationBuilder()
                  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
                  .AddJsonFile("appsettings.json", true)
                  .AddJsonFile($"appsettings.{environmentName}.json", true);
                    var configuration = builder.Build();
                    services.AddDbContext<LOSContext>(options =>
                    options.UseSqlServer(configuration.GetConnectionString("LOSDatabase")), ServiceLifetime.Transient);

                    var identitySettingsSection = configuration.GetSection("CiscoMobile");
                    services.Configure<AppCiscoMobileSettings>(identitySettingsSection);

					//services.AddHostedService<PushUserLosToSSO>();
					//services.AddHostedService<AutoReadAddressImageJob>();
					services.AddHostedService<AutoPushLoanBriefImportFileExcelJob>();
					services.AddHostedService<PushToTDHSJob>();
					services.AddHostedService<AutoDistributingHubJob>();
					services.AddHostedService<AutoCallServiceAIJob>();
					services.AddHostedService<ReadResultAndPushLoanJob>();
					//Chia đơn từ chờ chia đơn => chờ tư vấn giỏ system
					services.AddHostedService<AutoDistributingTelesaleJob>();
					//Chia đơn từ giỏ system => cho tls
					services.AddHostedService<AutoDistributingTelesaleAdviceJob>();
					services.AddHostedService<AutoDistributingTelesaleCTVJob>();
					services.AddHostedService<AutoDistributingTelesaleAdviceCTVJob>();
					services.AddHostedService<EventRequestJob>();
					services.AddHostedService<AutoSendNotificationSlack>();
					services.AddHostedService<AutoCancelLoanbriefJob>();
					if (environmentName == "Production")
					{
						services.AddHostedService<AutoSynchronizeLoanDisbursementToGoogleSheets>();
						services.AddHostedService<AutoSynchronizeLoanGoogleClickToGoogleSheets>();
						//Chạy 5h sáng hằng ngày
						services.AddHostedService<AutoCheckLoanDpd>();
						services.AddHostedService<AutoCheckLoanTopupCar>();
						services.AddHostedService<AutoGetRecordingCiscoJob>();
						services.AddHostedService<AutoGetRecordingCiscoMobileJob>();
						services.AddHostedService<CreateLoanAutocall>();
						services.AddHostedService<AutoSendSMSJob>();
						services.AddHostedService<AutoCreateLoanTopup>();
						//Job tự động khởi tạo đơn tái vay vào 3h sáng
						services.AddHostedService<AutoCreateLoanFinishJob>();
						//bán đơn vay cho đối tác
						services.AddHostedService<AutoPushLoanToPartner>();
						//bán dơn hủy của tima cho đối tác
						services.AddHostedService<AutoPushLoanCancelToPartner>();
                        //hủy tự động đơn hủy cho đối tác
                        services.AddHostedService<AutoCancelPushLoanToPartner>();
                        //Tự động thay đổi dns của landingpage khi downtime 3 lần
                        services.AddHostedService<AutoChangeDnsJob>();
                    }
                    services.AddHostedService<AutoReCreateLoanJob>();
                    services.AddHostedService<AutoSendNotificationLendingOnline>();
                    services.AddHostedService<AutoDistributingSystemTeamJob>();
                    //Chia đơn tự động
                    services.AddHostedService<AutoDistributingLenderJob>();
                    services.AddHostedService<AutoRetrievingLenderJob>();
                    // Đẩy trạng thái sang đối tác
                    services.AddHostedService<AutoPushLoanStatusToPartner>();
                    //DI
                    services.AddTransient<ITokenService, TokenService>();
                    services.AddTransient<INetworkService, NetworkService>();
                    services.AddTransient<IAuthenticationAI, AuthenticationService>();
                    services.AddTransient<IServiceAI, ServiceAI>();
                    services.AddTransient<ILogLoanInfoAIService, LogLoanInfoAIService>();
                    services.AddTransient<ILoanBriefService, LoanBriefServices>();
                    services.AddTransient<IShopService, ShopServices>();
                    services.AddTransient<IUserService, UserServices>();
                    services.AddTransient<ILocationService, LocationService>();
                    services.AddTransient<IHubDistributingService, HubDistributingServices>();
                    services.AddTransient<ILogPushHubService, LogPushHubService>();
                    services.AddTransient<IFinanceService, FinanceService>();
                    services.AddTransient<ILogCallApiService, LogCallApiService>();
                    services.AddTransient<ISlackService, SlackService>();
                    services.AddTransient<IFCMService, FCMService>();
                    services.AddTransient<IRequestEvent, RequestEventService>();
                    services.AddTransient<ILogRequestAi, LogRequestAiService>();
                    services.AddTransient<ISendMessage, SendMessageService>();
                    services.AddTransient<ILMSService, LMSService>();
                    services.AddTransient<ILogResultAutoCallServices, LogResultAutoCallServices>();
                    services.AddTransient<IProductServices, ProductServices>();
                    services.AddTransient<ISuggestionsInformation, SuggestionsInformation>();
                    services.AddTransient<ILoanBriefImportFileExcelServices, LoanBriefImportFileExcelServices>();
                    services.AddTransient<IErpService, ErpService>();
                    services.AddTransient<ILogCancelLoanbrief, LogCancelLoanbriefService>();
                    services.AddTransient<ILogCiscoPushToTelesaleService, LogCiscoPushToTelesaleService>();
                    services.AddTransient<IUnitOfWork, UnitOfWork>();
                    services.AddTransient<ILogSendSmsService, LogSendSmsService>();
                    services.AddTransient<ICiscoService, CiscoService>();
                    services.AddTransient<ILogReLoanbrief, LogReLoanbriefService>();
                    services.AddTransient<ILogDistributionUserService, LogDistributionUserService>();
                    services.AddTransient<ILogPushNotificationService, LogPushNotificationService>();
                    services.AddTransient<ILogLoanCheckMomoService, LogLoanCheckMomoService>();
                    services.AddTransient<ILenderAutoDistributeService, LenderAutoDistributeService>();
                    services.AddTransient<IRuleCheckLoanService, RuleCheckLoanService>();
                    services.AddTransient<ILogClickToCall, LogClickToCallService>();
                    services.AddTransient<ILogActionService, LogActionService>();
                    services.AddTransient<IPushLoanToPartnerService, PushLoanToPartnerService>();
                    services.AddTransient<IMiraeService, MiraeService>();
                    services.AddTransient<ILogCallApiPartnerService, LogCallApiPartnerService>();
                    services.AddTransient<IPushLoanStatusToPartnerService, PushLoanStatusToPartnerService>();
                    services.AddTransient<ITikiService, TikiService>();
                    services.AddTransient<ICloudFlareService, CloudFlareService>();
 services.AddTransient<IEmailService, EmailService>();
                    services.AddTransient<ILoanbriefRepository, LoanbriefRepository>();
                    services.AddTransient<ILoanbriefRelationshipRepository, LoanbriefRelationshipRepository>();
                    services.AddTransient<ILoanbriefV2Service, LoanbriefV2Service>();
                    services.AddTransient<ILMSV2Service, LMSV2Service>();
                    services.AddTransient<ILogCallApiRespository, LogCallApiRespository>();
                    services.AddTransient<ILogCallApiV2Service, LogCallApiV2Service>();
                });
    }
}
