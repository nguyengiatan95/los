﻿using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IAuthenticationAI
    {
        string GetToken(string app_id, string app_key, bool reGetToken = false);
    }
    public class AuthenticationService : IAuthenticationAI
    {
        protected IConfiguration _baseConfig;
        protected ITokenService _tokenService;
        public AuthenticationService(IConfiguration configuration, ITokenService tokenService)
        {
            _baseConfig = configuration;
            _tokenService = tokenService;
        }

        private AuthenticationResult Authentication(string app_id, string app_key)
        {
            try
            {

                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.AI_AUTHENTICATION_ENPOINT;
                var client = new RestClient(url);
                var objInput = new AuthenticationInput()
                {
                    app_id = app_id,
                    app_key = app_key
                };
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddParameter("application/json", Newtonsoft.Json.JsonConvert.SerializeObject(objInput), ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                AuthenticationResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthenticationResult>(jsonResponse);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GetToken(string app_id, string app_key, bool reGetToken = false)
        {
            try
            {
                var token = string.Empty;
                var tokenAPI = _tokenService.GetLastToken(app_id);
                if (tokenAPI == null || string.IsNullOrEmpty(tokenAPI.AccessToken1)
                    || (tokenAPI.DateExpiration.HasValue && tokenAPI.DateExpiration.Value.Date <= DateTime.Now.Date) || reGetToken)
                {
                    var reToken = Authentication(app_id, app_key);
                    if (!string.IsNullOrEmpty(reToken.token))
                    {
                        token = reToken.token;
                        //insert to db
                        var objToken = new AccessToken()
                        {
                            AccessToken1 = reToken.token,
                            DateExpiration = reToken.expired_date,
                            AppId = app_id
                        };
                        _tokenService.AddToken(objToken);
                        //ThreadPool.QueueUserWorkItem(item => _tokenService.AddToken(objToken));
                    }
                }
                else
                {
                    token = tokenAPI.AccessToken1;
                }
                return token;
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }
    }
}
