﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Objects.Setttings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static LOS.ThirdPartyProccesor.Objects.Cisco;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ICiscoService
    {
        List<AuthorizeOutput> Authorize();
        List<GetList> GetListByTime(string fromeDate, string toDate, string CSRFTOKEN, string JSESSIONID, ref int logCallApiId);

        RecordingModel GetPlayUrl(string recordingUrl, string CSRFTOKEN, string JSESSIONID);
        string AuthorizeMobile();
        List<MobileRecordData> GetMobileRecording(string callId);
    }
     public   class CiscoService : ICiscoService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly AppCiscoMobileSettings _appCiscoMobileSettings;
        public CiscoService(IOptions<AppCiscoMobileSettings> appCiscoMobileSettingsAccessor)
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
           var _baseConfig = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(_baseConfig.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
            _appCiscoMobileSettings = appCiscoMobileSettingsAccessor.Value;
        }
        public List<AuthorizeOutput> Authorize()
        {
            try
            {
                var input = new AuthorizeInput
                {
                    Id = Constants.CISCO_ID,
                    UserId = Constants.CISCO_USERID,
                    Password = Constants.CISCO_PASSWORD,
                    Data = new LOS.ThirdPartyProccesor.Objects.Cisco.Data
                    {
                        Domain = Constants.CISCO_DOMAIN
                    }
                };
                var url = Constants.CISCO_URL + Constants.CISCO_AUTHORIZE;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(input);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", "[" + body + "]", ParameterType.RequestBody);
              
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    List<AuthorizeOutput> lst = JsonConvert.DeserializeObject<List<AuthorizeOutput>>(json);
                    lst[0].CSRFTOKEN = response.Cookies[0].Value;
                    lst[0].JSESSIONID = response.Cookies[1].Value;
                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<GetList> GetListByTime(string fromeDate, string toDate, string CSRFTOKEN, string JSESSIONID, ref int logCallApiId)
        {
            try
            {
                var url = Constants.CISCO_URL + string.Format(Constants.CISCO_GETLIST_TIME, fromeDate, toDate);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Cookie", "CSRFTOKEN=" + CSRFTOKEN + ";JSESSIONID=" + JSESSIONID);
                request.AddHeader("Range", "items=0-100000");

                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.GetListRecording,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    Input = url,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
                logCallApiId = objLogCallApi.Id;
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = json;
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == objLogCallApi.Id, x => new LogCallApi()
                {
                    Output = json,
                    Status = (int)StatusCallApi.Success,
                    ModifyAt = DateTime.Now
                });
                _unitOfWork.Save();
                json = json.Replace("$", "");
                List<GetList> lst = JsonConvert.DeserializeObject<List<GetList>>(json);
                return lst;
            }
            catch(Exception ex)
            {
                return null;
            }
            
        }

        public RecordingModel GetPlayUrl(string recordingUrl, string CSRFTOKEN, string JSESSIONID)
        {
            var url = Constants.CISCO_URL + recordingUrl;
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Cookie", "CSRFTOKEN=" + CSRFTOKEN + ";JSESSIONID=" + JSESSIONID);
            request.AddHeader("Range", "items=0-100000");
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            json = json.Replace("$", "");
            return JsonConvert.DeserializeObject<RecordingModel>(json); ;
        }

        #region mobile
        public string AuthorizeMobile()
        {
            try
            {
                var input = new AuthenMobileReq
                {
                    UserName = _appCiscoMobileSettings.UserName,
                    Password = _appCiscoMobileSettings.Password,
                };
                var url = _appCiscoMobileSettings.BaseUrl + _appCiscoMobileSettings.AuthenUrl;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", JsonConvert.SerializeObject(input), ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    AuthenMobileRes data = JsonConvert.DeserializeObject<AuthenMobileRes>(json);
                    if (data != null && data.Data != null && !string.IsNullOrEmpty(data.Data.Token))
                        return data.Data.Token;
                }
            }
            catch (Exception ex)
            {
            }
            return string.Empty;
        }

        public List<MobileRecordData> GetMobileRecording(string callId)
        {
            try
            {
                var input = new MobileRecordReq
                {
                    DomainId = _appCiscoMobileSettings.DomainId,
                    CallId = callId,
                };
                var url = _appCiscoMobileSettings.BaseUrl + _appCiscoMobileSettings.DetailRecordUrl;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", JsonConvert.SerializeObject(input), ParameterType.RequestBody);
                request.AddHeader("Authorization", $"Bearer {AuthorizeMobile()}" );
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    MobileRecordRes data = JsonConvert.DeserializeObject<MobileRecordRes>(json);
                    if (data != null && data.ListRecording != null && data.ListRecording.Count(x => !string.IsNullOrEmpty(x.RecordPath)) > 0)
                        return data.ListRecording.Where(x => !string.IsNullOrEmpty(x.RecordPath)).ToList();
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}
