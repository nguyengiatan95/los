﻿using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects.CloudFlare;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ICloudFlareService
    {
        List<ZoneItem> GetAllZone();
        List<DnsRecordItem> GetAllDnsRecord(string zoneId);
        bool UpdateDnsRecord(DnsRecordItem dnsRecord, string IpAddress);
    }
    public class CloudFlareService : ICloudFlareService
    {
        protected IConfiguration _baseConfig;
        public CloudFlareService() {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        public List<ZoneItem> GetAllZone()
        {
            try
            {
                var url = _baseConfig["ClouldFlare:BaseUrl"] + Constants.CLOUDFLARE_LIST_ZONE;
                var client = new RestClient(url);
                client.Timeout = 10000;
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", $"Bearer {_baseConfig["ClouldFlare:Token"]}");
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                ZoneResponse result = Newtonsoft.Json.JsonConvert.DeserializeObject<ZoneResponse>(jsonResponse);
                if (result != null && result.Success)
                    return result.ListZone;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor CloudFlareService  GetAllZone exception");
            }
            return null;
        }
        public List<DnsRecordItem> GetAllDnsRecord(string zoneId)
        {
            try
            {
                var url = _baseConfig["ClouldFlare:BaseUrl"] + String.Format( Constants.CLOUDFLARE_LIST_DNS_RECORD, zoneId);
                var client = new RestClient(url);
                client.Timeout = 10000;
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", $"Bearer {_baseConfig["ClouldFlare:Token"]}");
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                DnsRecordResponse result = Newtonsoft.Json.JsonConvert.DeserializeObject<DnsRecordResponse>(jsonResponse);
                if (result != null && result.Success)
                    return result.ListDnsRecord;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor CloudFlareService  GetAllDnsRecord exception");
            }
            return null;
        }

        public bool UpdateDnsRecord(DnsRecordItem dnsRecord, string IpAddress)
        {
            try
            {
                var url = _baseConfig["ClouldFlare:BaseUrl"] + String.Format(Constants.CLOUDFLARE_UPDATE_DNS_RECORD, dnsRecord.ZoneId, dnsRecord.Id);
                var client = new RestClient(url);
                client.Timeout = 10000;
                var request = new RestRequest(Method.PUT);
                var body = JsonConvert.SerializeObject(new UpdateDnsRequest
                {
                    Type = dnsRecord.Type,
                    Name = dnsRecord.Name,
                    Content = IpAddress,
                    Ttl = dnsRecord.Ttl,
                    Proxied = dnsRecord.Proxied
                });
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", $"Bearer {_baseConfig["ClouldFlare:Token"]}");
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                UpdateDnsResponse result = Newtonsoft.Json.JsonConvert.DeserializeObject<UpdateDnsResponse>(jsonResponse);
                return (result != null && result.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor CloudFlareService  UpdateDnsRecord exception");
            }
            return false;
        }
    }
}
