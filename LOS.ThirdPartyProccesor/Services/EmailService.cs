﻿using LOS.ThirdPartyProccesor.Objects;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IEmailService
    {
        bool SendMail(string sendTo, string title, string body);
        bool SendListMail(List<string> sendTo, string title, string body);
    }
    public class EmailService : IEmailService
    {
        protected IConfiguration _baseConfig;
        public EmailService()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        public bool SendMail(string sendTo, string title, string body)
        {
            try
            {
                var fromEmail = _baseConfig["AppSettings:MailServerUserName"];
                var fromPass = _baseConfig["AppSettings:MailServerPassword"];
                var mailFrom = new MailAddress(fromEmail, title);
                var mailTo = new MailAddress(sendTo);
                var smtpInfo = new SmtpInfo()
                {
                    AuthenticationPassword = fromPass,
                    AuthenticationUserName = fromEmail,
                    HasAuthentication = true,
                    SmtpHost = "smtp.gmail.com",
                    SmtpPort = 587
                };
                return SendMail(mailFrom, mailTo, title, body, smtpInfo).Result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error("Email Service SendMail: " + ex.ToString());
                return false;
            }
        }
        public bool SendListMail(List<string> sendTo, string title, string body)
        {
            try
            {
                var fromEmail = _baseConfig["AppSettings:MailServerUserName"];
                var fromPass = _baseConfig["AppSettings:MailServerPassword"];
                var mailFrom = new MailAddress(fromEmail, title);
                var mailTo = new List<MailAddress>();
                foreach (var item in sendTo)
                {
                    mailTo.Add(new MailAddress(item));
                }
                var smtpInfo = new SmtpInfo()
                {
                    AuthenticationPassword = fromPass,
                    AuthenticationUserName = fromEmail,
                    HasAuthentication = true,
                    SmtpHost = "smtp.gmail.com",
                    SmtpPort = 587
                };
                return SendListMail(mailFrom, mailTo, title, body, smtpInfo).Result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error("Email Service SendMail: " + ex.ToString());
                return false;
            }
        }
        public Task<bool> SendMail(MailAddress mailFrom, MailAddress mailTo, string subject, string body, SmtpInfo smtp)
        {
            try
            {
                //Send mail.
                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Host = smtp.SmtpHost;
                    smtpClient.Port = smtp.SmtpPort;
                    smtpClient.UseDefaultCredentials = false;
                    if (smtp.HasAuthentication)
                    {
                        smtpClient.Credentials = new NetworkCredential(smtp.AuthenticationUserName,
                                                                       smtp.AuthenticationPassword);
                    }

                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.EnableSsl = true;
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    using (var mailMessage = new MailMessage())
                    {
                        mailMessage.From = mailFrom;
                        mailMessage.To.Add(mailTo);
                        mailMessage.Subject = subject;
                        mailMessage.Body = body;
                        mailMessage.IsBodyHtml = true;
                        smtpClient.Send(mailMessage);
                        return Task.FromResult(true);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Information(Newtonsoft.Json.JsonConvert.SerializeObject(mailFrom));
                Serilog.Log.Information(Newtonsoft.Json.JsonConvert.SerializeObject(mailTo));
                Serilog.Log.Error("SSO SendMail: " + ex.StackTrace);
                return Task.FromResult(false);
            }

        }
        public Task<bool> SendListMail(MailAddress mailFrom, List<MailAddress> mailTo, string subject, string body, SmtpInfo smtp)
        {
            try
            {
                //Send mail.
                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Host = smtp.SmtpHost;
                    smtpClient.Port = smtp.SmtpPort;
                    smtpClient.UseDefaultCredentials = false;
                    if (smtp.HasAuthentication)
                    {
                        smtpClient.Credentials = new NetworkCredential(smtp.AuthenticationUserName,
                                                                       smtp.AuthenticationPassword);
                    }

                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.EnableSsl = true;
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    using (var mailMessage = new MailMessage())
                    {
                        mailMessage.From = mailFrom;
                        mailMessage.To.Add(string.Join(",", mailTo));
                        mailMessage.Subject = subject;
                        mailMessage.Body = body;
                        mailMessage.IsBodyHtml = true;
                        smtpClient.Send(mailMessage);
                        return Task.FromResult(true);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Information(Newtonsoft.Json.JsonConvert.SerializeObject(mailFrom));
                Serilog.Log.Information(Newtonsoft.Json.JsonConvert.SerializeObject(mailTo));
                Serilog.Log.Error("SSO SendMail: " + ex.StackTrace);
                return Task.FromResult(false);
            }

        }
    }
}
