﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IErpService
    {
        List<int> GetStaffHub(int HubId, int loanbriefId);
        List<UserErpModel> GetInfoStaffHub(int HubId, int loanbriefId);
        CheckEmployeeTima CheckEmployeeTima(string NumberCard, string Phone, int loanbriefId);
        HubEmployeeInfo GetHubEmployeeInfo(int userId, int loanbriefId);
        List<UserErpModel> GetUserCoordinatorCheckIn(int loanbriefId);
    }
    public class ErpService : IErpService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ErpService()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }
        public List<int> GetStaffHub(int HubId, int loanbriefId)
        {
            try
            {
                var url = string.Format(Constants.get_employee_hub_checkin, HubId);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Timeout = 5000;
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.GetHubEmployeeErp,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    Input = string.Format("{0} - {1}", HubId, loanbriefId),
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
                var response = client.Execute(request);
                var content = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = content;
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == objLogCallApi.Id, x => new LogCallApi()
                {
                    Output = content,
                    Status = (int)StatusCallApi.Success,
                    ModifyAt = DateTime.Now
                });
                _unitOfWork.Save();
              return JsonConvert.DeserializeObject<List<int>>(content);
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public List<UserErpModel> GetInfoStaffHub(int HubId, int loanbriefId)
        {
            try
            {
                var url = string.Format(Constants.get_info_employee_hub_checkin, HubId);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Timeout = 5000;
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.GetHubEmployeeErp,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    Input = string.Format("{0} - {1}", HubId, loanbriefId),
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
                var response = client.Execute(request);
                var content = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = content;
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == objLogCallApi.Id, x => new LogCallApi()
                {
                    Output = content,
                    Status = (int)StatusCallApi.Success,
                    ModifyAt = DateTime.Now
                });
                _unitOfWork.Save();
                return JsonConvert.DeserializeObject<List<UserErpModel>>(content);
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public CheckEmployeeTima CheckEmployeeTima(string NumberCard, string Phone, int loanbriefId)
        {
            try
            {
                var url = "https://erp.tima.vn/api/api/userStatus" + "?nationalId=" + NumberCard + "&tel=" + Phone;
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.ApiCheckEployeeTima.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    Input = "nationalId=" + NumberCard + "&tel=" + Phone
            };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == log.Id, x => new LogCallApi()
                {
                    Output = json,
                    Status = (int)StatusCallApi.Success,
                    ModifyAt = DateTime.Now
                });
                _unitOfWork.Save();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    CheckEmployeeTima lst = JsonConvert.DeserializeObject<CheckEmployeeTima>(json);
                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public HubEmployeeInfo GetHubEmployeeInfo(int userId, int loanbriefId)
        {
            try
            {
                var url = "https://erp.tima.vn/api/api/getPhoneHub/" + userId;
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.GetHubEmployeeTima.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    Input = $"user={userId}, loanbriefId={loanbriefId}"
                };
                _unitOfWork.LogCallApiRepository.Insert(log);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == log.Id, x => new LogCallApi()
                {
                    Output = json,
                    Status = (int)StatusCallApi.Success,
                    ModifyAt = DateTime.Now
                });
                _unitOfWork.Save();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    HubEmployeeInfo lst = JsonConvert.DeserializeObject<HubEmployeeInfo>(json);
                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<UserErpModel> GetUserCoordinatorCheckIn(int loanbriefId)
        {
            try
            {
                var url = Constants.get_all_coordinator_checkin;
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Timeout = 5000;
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.GetUserCoordinatorCheckinErp,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    Input = loanbriefId.ToString(),
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
                var response = client.Execute(request);
                var content = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = content;
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == objLogCallApi.Id, x => new LogCallApi()
                {
                    Output = content,
                    Status = (int)StatusCallApi.Success,
                    ModifyAt = DateTime.Now
                });
                _unitOfWork.Save();
                return JsonConvert.DeserializeObject<List<UserErpModel>>(content);
            }
            catch (Exception ex)
            {

            }
            return null;
        }
    }
}
