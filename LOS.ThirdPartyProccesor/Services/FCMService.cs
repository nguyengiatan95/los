﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IFCMService
    {
        public void PushNotification(string token, string topic, string message, int actionPush, int loanBriefId = 0);
    }

    public class FCMService : IFCMService
    {
        protected IConfiguration _baseConfig;
        private readonly IUnitOfWork _unitOfWork;
        public FCMService()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }
        public async void PushNotification(string token, string topic, string message, int actionPush, int loanBriefId = 0)
        {
            try
            {
                var client = new RestClient("https://fcm.googleapis.com/fcm/send");
                client.Timeout = 3000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", token);
                request.AddHeader("Content-Type", "application/json");
                PushBody body = new PushBody();
                body.LoanBriefId = loanBriefId;
                body.Action = "NOTIFY";
                PushNotification noti = new PushNotification();
                noti.body = message;
                noti.sound = "default";
                noti.badge = 1;
                PushData data = new PushData();
                data.data = body;
                data.to = "/topics/" + topic;
                data.notification = noti;
                var json = JsonConvert.SerializeObject(data);
                request.AddParameter("application/json", json, ParameterType.RequestBody);
                var objLogPush = new LogPushNotification
                {
                    LoanbriefId = loanBriefId,
                    Url = "https://fcm.googleapis.com/fcm/send",
                    RequestStatus = (int)StatusCallApi.CallApi,
                    RequestToken = token,
                    ActionPush = actionPush,
                    RequestBody = json,
                    RequestAt = DateTime.Now,
                    MessagePush = message,
                    PushTo = topic,
                    TypeNotification = (int)TypeNotification.FireBase
                };
                _unitOfWork.LogPushNotificationRepository.Insert(objLogPush);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var result = response.Content;
                await _unitOfWork.LogPushNotificationRepository.UpdateAsync(x => x.Id == objLogPush.Id, x => new LogPushNotification()
                {
                    Response = result,
                    ResponseAt = DateTime.Now,
                    RequestStatus = (int)StatusCallApi.Success
                });
                var resp = JsonConvert.DeserializeObject<FirebaseResp>(result);
                if (resp != null)
                {
                    if (resp.message_id != null && resp.message_id != "")
                        Console.WriteLine("Push notify successed - topic : " + topic + " - " + message);
                    else
                        Console.WriteLine("Push notify failed - topic : " + topic + " - " + message);
                }
                else
                {
                    Console.WriteLine("Push notify failed - topic : " + topic + " - " + message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Push notify failed - topic : " + topic + " - " + message + "\n" + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }

    public class PushData
    {
        public PushBody data { get; set; }
        public PushNotification notification { get; set; }
        public string to { get; set; }
    }

    public class PushBody
    {
        public string Action { get; set; }
        public int LoanBriefId { get; set; }
    }

    public class PushNotification
    {
        public string body { get; set; }
        public string title { get; set; }
        public string icon { get; set; }
        public string sound { get; set; }
        public int badge { get; set; }
    }

    public class FirebaseResp
    {
        public string message_id { get; set; }
        public long multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
    }
}
