﻿using LOS.ThirdPartyProccesor.Objects.Finance;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IFinanceService
    {
        ResponDefaultFinance SynchorizeStatusLOSAndFinance(RespuestApiUpdateStatusLoanCreditFinance entity);
        ResponDefaultFinance CreateLoanCreditFromCancelLOS(RequestApiCreateLoanCreditFromCancelLOS entity);
    }
    public class FinanceService : IFinanceService
    {
        protected IConfiguration _baseConfig;

        public FinanceService()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        public ResponDefaultFinance SynchorizeStatusLOSAndFinance(RespuestApiUpdateStatusLoanCreditFinance entity)
        {
            var body = JsonConvert.SerializeObject(entity);
            var client = new RestClient(_baseConfig["AppSettings:LinkFinance"]);
            var request = new RestRequest("api/loan/updatestatusloancreditbylos");
            request.Method = Method.POST;
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            // easy async support
            try
            {
                var response = client.Execute<ResponDefaultFinance>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponDefaultFinance>(response.Content);
                    return objData;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "FinanceService/SynchorizeStatusLOSAndFinance Exception");
                return null;
            }
        }
        public ResponDefaultFinance CreateLoanCreditFromCancelLOS(RequestApiCreateLoanCreditFromCancelLOS entity)
        {
            var body = JsonConvert.SerializeObject(entity);
            var client = new RestClient(_baseConfig["AppSettings:LinkFinance"]);
            var request = new RestRequest("api/loan/createloancreditfromcancellos");
            request.Method = Method.POST;
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            // easy async support
            try
            {
                var response = client.Execute<ResponDefaultFinance>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponDefaultFinance>(response.Content);
                    return objData;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "FinanceService/CreateLoanCreditFromCancelLOS Exception");
                return null;
            }
        }
    }
}
