﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static LOS.ThirdPartyProccesor.Objects.Ekyc;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IHubDistributingService
    {
        HubDistribution GetHubDistribution(int ProvinceId, int productId);
        bool UpdateHub(int LoanBriefId, int HubId, int hubTypeId);
        bool UpdateHubEmployee(int LoanBriefId, int employeeID, int typeAppraiserName);
        bool UpdateNumberReceived(int ShopId, DateTimeOffset applyTime);
        bool UpdateNumberReceivedHubEmployee(int ShopId, int employeeId);
        Tuple<int, int> GetLoanCountByHub(int ProvinceId, DateTimeOffset applyTime);
        public LogLoanInfoAi InsertApiLog(LogLoanInfoAi log);
        public LogLoanInfoAi UpdateApiLog(LogLoanInfoAi log);
        public ResultEkyc InsertEkycResult(ResultEkyc result);
        public bool EkycResultExists(int loanBriefId);
        public int? GetHubEmployeeByCount(int shopId);

        public UserDetail GetHubEmployeeLoanAsc(int shopId, List<int> hubEmployees = null);

        public EkycInput PrepareEkycModel(LoanBrief entity, ref bool IsCheck);
        bool AddLogDistributingHubEmployee(LogDistributingHubEmployee entity);
        bool UpdateFieldHo(int LoanBriefId, int employeeID);
        bool RemoveFieldHo(int LoanBriefId);
        bool UpdateHubEmployeeReceived(int LoanBriefId);
    }
    public class HubDistributingServices : IHubDistributingService
    {
        private readonly IUnitOfWork _unitOfWork;
        protected IConfiguration _baseConfig;
        private readonly ILoanBriefService _loanBriefServices;
        public HubDistributingServices(ILoanBriefService loanBriefServices)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(_baseConfig.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
            this._loanBriefServices = loanBriefServices;
        }

        public HubDistribution GetHubDistribution(int ProvinceId, int productId)
        {
            try
            {
                return _unitOfWork.HubDistributionRepository.Query(x => x.ProvinceId == ProvinceId && x.ProductId == productId, null, false).OrderByDescending(x => x.ApplyTime).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateHub(int LoanBriefId, int HubId, int hubTypeId)
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    HubTypeId = hubTypeId,
                    HubId = HubId,
                    InProcess = 0
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool UpdateHubEmployee(int LoanBriefId, int employeeID, int typeAppraiserName)
        {
            try
            {
                var entity = _unitOfWork.LoanBriefRepository.GetById(LoanBriefId);
                if (entity != null && entity.LoanBriefId > 0)
                {
                    if (typeAppraiserName == (int)EnumAppraisal.AppraisalHome)
                    {
                        _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                        {
                            HubEmployeeId = employeeID,
                            //HubPushAt = DateTime.Now,
                            EvaluationHomeUserId = employeeID,
                            EvaluationHomeState = EnumFieldSurveyState.NotAppraised.GetHashCode(),
                            EvaluationCompanyUserId = null,
                            EvaluationCompanyState = null

                        });
                    }else if (typeAppraiserName == (int)EnumAppraisal.AppraisalCompany)
                    {
                        _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                        {
                            HubEmployeeId = employeeID,
                            //HubPushAt = DateTime.Now,
                            EvaluationHomeUserId = null,
                            EvaluationHomeState = null,
                            EvaluationCompanyUserId = employeeID,
                            EvaluationCompanyState = EnumFieldSurveyState.NotAppraised.GetHashCode()

                        });
                    }
                    else if (typeAppraiserName == (int)EnumAppraisal.AppraisalHomeAndCompany)
                    {
                        _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                        {
                            HubEmployeeId = employeeID,
                            //HubPushAt = DateTime.Now,
                            EvaluationHomeUserId = employeeID,
                            EvaluationHomeState = EnumFieldSurveyState.NotAppraised.GetHashCode(),
                            EvaluationCompanyUserId = employeeID,
                            EvaluationCompanyState = EnumFieldSurveyState.NotAppraised.GetHashCode()

                        });
                    }
                    else
                    {
                        _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                        {

                            HubEmployeeId = employeeID,
                            //HubPushAt = DateTime.Now,
                            EvaluationHomeUserId = null,
                            EvaluationHomeState = null,
                            EvaluationCompanyUserId = null,
                            EvaluationCompanyState = null

                        });
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }


        public bool UpdateFieldHo(int LoanBriefId, int employeeID)
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    FieldHo = employeeID,
                    EvaluationCompanyUserId = employeeID,
                    EvaluationCompanyState = (int)EnumFieldSurveyState.NotAppraised,
                    EvaluationHomeUserId = employeeID,
                    EvaluationHomeState = (int)EnumFieldSurveyState.NotAppraised
                });
                //cập nhật thời gian nhận cuối của user
                _unitOfWork.UserRepository.Update(x => x.UserId == employeeID, x => new User()
                {
                    LastReceived = DateTime.Now
                });

                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool RemoveFieldHo(int LoanBriefId)
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    FieldHo = null
                });
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }
        public bool UpdateNumberReceived(int ShopId, DateTimeOffset applyTime)
        {
            try
            {
                var entity = _unitOfWork.HubLoanBriefRepository.Query(x => x.HubId == ShopId && x.ApplyTime == applyTime, null, true).FirstOrDefault();
                if (entity != null)
                {
                    entity.Count += 1;
                    _unitOfWork.HubLoanBriefRepository.Update(entity);
                    _unitOfWork.Save();
                    // save received to shop
                    var shop = _unitOfWork.ShopRepository.GetById(ShopId);
                    shop.Received = entity.Count;
                    _unitOfWork.ShopRepository.Update(shop);
                    _unitOfWork.Save();
                    return true;
                }
                else
                {
                    var hub = _unitOfWork.ShopRepository.GetById(ShopId);
                    HubLoanBrief hubLoanBrief = new HubLoanBrief();
                    hubLoanBrief.HubId = ShopId;
                    hubLoanBrief.Count = 1;
                    hubLoanBrief.HubTypeId = hub.HubType;
                    hubLoanBrief.ApplyTime = applyTime;
                    _unitOfWork.HubLoanBriefRepository.Insert(hubLoanBrief);
                    _unitOfWork.Save();
                    // save received to shop
                    var shop = _unitOfWork.ShopRepository.GetById(ShopId);
                    shop.Received = hubLoanBrief.Count;
                    _unitOfWork.ShopRepository.Update(shop);
                    _unitOfWork.Save();
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        public bool UpdateNumberReceivedHubEmployee(int ShopId, int employeeId)
        {
            try
            {
                var now = DateTime.Now;
                var entity = _unitOfWork.HubEmployeeDistributionRepository.Query(x => x.HubId == ShopId && x.EmployeeId == employeeId
                    && x.Date.Value.Year == now.Year && x.Date.Value.Month == now.Month && x.Date.Value.Day == now.Day, null, true).FirstOrDefault();
                if (entity != null)
                {
                    entity.Count += 1;
                    _unitOfWork.HubEmployeeDistributionRepository.Update(entity);
                    _unitOfWork.Save();
                    return true;
                }
                else
                {
                    var hub = _unitOfWork.ShopRepository.GetById(ShopId);
                    HubEmployeeDistribution hubEmployee = new HubEmployeeDistribution();
                    hubEmployee.HubId = ShopId;
                    hubEmployee.Count = 1;
                    hubEmployee.EmployeeId = employeeId;
                    hubEmployee.Date = now;
                    _unitOfWork.HubEmployeeDistributionRepository.Insert(hubEmployee);
                    _unitOfWork.Save();
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public Tuple<int, int> GetLoanCountByHub(int ProvinceId, DateTimeOffset applyTime)
        {
            try
            {
                var countPhysicalHub = _unitOfWork.HubLoanBriefRepository.Query(x => x.HubTypeId == 2 && x.Hub.CityId == ProvinceId && x.ApplyTime == applyTime, null, false).Sum(x => x.Count);
                var countServiceHub = _unitOfWork.HubLoanBriefRepository.Query(x => x.HubTypeId == 1 && x.Hub.CityId == ProvinceId && x.ApplyTime == applyTime, null, false).Sum(x => x.Count);
                Console.WriteLine(countPhysicalHub + ":" + countServiceHub);
                return Tuple.Create(countPhysicalHub, countServiceHub);
            }
            catch (Exception ex)
            {

            }
            return Tuple.Create(0, 0);
        }

        public LogLoanInfoAi InsertApiLog(LogLoanInfoAi log)
        {
            try
            {
                _unitOfWork.LogLoanInfoAiRepository.Insert(log);
                _unitOfWork.Save();
                return log;
            }
            catch (Exception ex)
            {

            }
            return log;
        }

        public LogLoanInfoAi UpdateApiLog(LogLoanInfoAi log)
        {
            try
            {
                _unitOfWork.LogLoanInfoAiRepository.Update(log);
                _unitOfWork.Save();
                return log;
            }
            catch (Exception ex)
            {

            }
            return log;
        }

        public ResultEkyc InsertEkycResult(ResultEkyc result)
        {
            try
            {
                _unitOfWork.ResultEkycRepository.Insert(result);
                _unitOfWork.Save();
                return result;
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public bool EkycResultExists(int loanBriefId)
        {
            try
            {
                return _unitOfWork.ResultEkycRepository.Any(x => x.LoanbriefId == loanBriefId);
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public int? GetHubEmployeeByCount(int shopId)
        {

            var query = _unitOfWork.UserShopRepository.Query(x => x.ShopId == shopId
               , null, false, x => x.User).Where(x => x.User.GroupId == (int)EnumGroupUser.StaffHub && x.User.Status == 1 && x.User.IsDeleted != true)
               .OrderBy(k => k.User.LoanBriefHubEmployee.Count(k => k.Status == (int)EnumLoanStatus.APPRAISER_REVIEW)).Select(x => new UserDetail
               {
                   UserId = x.User.UserId,
                   FullName = x.User.FullName,
                   Username = x.User.Username,
                   Email = x.User.Email,
                   Phone = x.User.Phone,
                   GroupId = x.User.GroupId,
                   Status = x.User.Status,
                   Ipphone = x.User.Ipphone
               }).FirstOrDefault();



            var now = DateTime.Now;
            var rs = (from userShop in _unitOfWork.UserShopRepository.All()
                      join user in _unitOfWork.UserRepository.All() on userShop.UserId equals user.UserId into _users
                      from users in _users.DefaultIfEmpty()
                      join hubEmployee in _unitOfWork.HubEmployeeDistributionRepository.All() on users.UserId equals hubEmployee.EmployeeId into uss
                      from result in uss.DefaultIfEmpty()
                      where userShop.ShopId == shopId && userShop.Status.HasValue && userShop.Status.Value && users.Status.HasValue && users.Status.Value == 1 && users.GroupId == 45
                      select new
                      {
                          users = users,
                          hubEmployee = result
                      }
            ).ToList();
            // ưu tiên employee chưa có dữ liệu trong ngày			
            if (rs != null && rs.Count > 0)
            {
                #region chia theo ai nhận được ít đơn nhất
                //foreach (var s in rs)
                //{
                //	//if (s.hubEmployee != null && s.hubEmployee.Date.HasValue && s.hubEmployee.Date.Value.Year == now.Year && s.hubEmployee.Date.Value.Year == now.Year && s.hubEmployee.Date.Value.Year == now.Year)
                //	if (s.hubEmployee == null)
                //	{ 	
                //		return s.users.UserId;
                //	}
                //}
                //return rs.OrderBy(x => x.hubEmployee.Count).FirstOrDefault().users.UserId;
                #endregion
                var userIds = rs.Select(x => x.users.UserId).ToList();
                #region chia theo nhân viên đang xử lý ít đơn nhất
                var userId = (from loanBrief in _unitOfWork.LoanBriefRepository.All()
                              where loanBrief.Status == 40 && loanBrief.HubId == shopId
                                  && loanBrief.HubEmployeeId.HasValue
                                  && loanBrief.HubEmployeeId.Value > 0 && userIds.Contains(loanBrief.HubEmployeeId.Value)
                              group loanBrief by loanBrief.HubEmployeeId into g
                              select new
                              {
                                  hubEmployeeId = g.Key,
                                  count = g.Count()
                              }).ToList().OrderBy(x => x.count).Select(x => x.hubEmployeeId).FirstOrDefault();
                if (userId == null || userId <= 0)
                {
                    return userIds.OrderBy(r => Guid.NewGuid()).FirstOrDefault();
                }
                else
                {
                    return userId;
                }
                #endregion
            }
            return -1;
        }

        public UserDetail GetHubEmployeeLoanAsc(int shopId, List<int> hubEmployees = null)
        {
            try
            {
                return _unitOfWork.UserShopRepository.Query(x => x.ShopId == shopId && (hubEmployees == null || hubEmployees.Contains(x.UserId.Value))
                , null, false, x => x.User).Where(x => x.User.GroupId == (int)EnumGroupUser.StaffHub && x.User.Status == 1 && x.User.IsDeleted != true)
                    .OrderBy(k => k.User.LoanBriefHubEmployee.Count(k => k.Status == (int)EnumLoanStatus.APPRAISER_REVIEW 
                    && (k.UtmSource != "form_remkt_hub" || (k.UtmSource == "form_remkt_hub" && k.BoundTelesaleId.GetValueOrDefault(0) > 0))
                    && (k.ProductId == (int)EnumProductCredit.MotorCreditType_CC || k.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                    || k.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))).Select(x => new UserDetail
                {
                    UserId = x.User.UserId,
                    FullName = x.User.FullName,
                    Username = x.User.Username,
                    Email = x.User.Email,
                    Phone = x.User.Phone,
                    GroupId = x.User.GroupId,
                    Status = x.User.Status,
                    Ipphone = x.User.Ipphone
                }).FirstOrDefault();
            }
            catch
            {

            }
            return null;
        }
        public EkycInput PrepareEkycModel(LoanBrief item, ref bool IsCheck)
        {
            try
            {
                IsCheck = true;
                var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
                var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
                var listLoanBriefFile = _loanBriefServices.GetFilesByLoanId(item.LoanBriefId);
                var listDocument = _loanBriefServices.GetDocumentTypeProductId(item.ProductId.Value);
                if (item.Dob == null)
                {
                    IsCheck = false;
                }
                
                var lstCMND = listLoanBriefFile.Where(x => x.TypeId == (int)EnumDocumentType.CMND_CCCD).ToList();
                if (lstCMND.Count < 2)
                    IsCheck = false;
                //var FrontId = listLoanBriefFile.Where(x => x.TypeId == listDocument.Where(x => x.Name.Contains("CMT mặt trước")).ToList()[0].Id).OrderByDescending(s => s.Id).FirstOrDefault();
                //var BackId = listLoanBriefFile.Where(x => x.TypeId == listDocument.Where(x => x.Name.Contains("CMT mặt sau")).ToList()[0].Id).OrderByDescending(s => s.Id).FirstOrDefault();

                //var Selfie = new LoanBriefFileDetail();
                //try
                //{
                //    Selfie = listLoanBriefFile.Where(x => x.TypeId == listDocument.Where(x => x.Name.Contains("Ảnh Mặt")).ToList()[0].Id).OrderByDescending(s => s.Id).FirstOrDefault();
                //}
                //catch (Exception ex)
                //{
                //    Selfie.FilePath = "";
                //}
                //if (FrontId == null || BackId == null)
                //    IsCheck = false;
                if (!IsCheck)
                    return null;
                foreach (var f in lstCMND)
                {
                    if (!f.FilePath.Contains("http") && (f.MecashId == null || f.MecashId == 0))
                        f.FilePath = ServiceURL + f.FilePath;
                    if (f.MecashId > 0 && (f.S3status == 0 || f.S3status == null))
                        f.FilePath = ServiceURLAG + f.FilePath;
                    if (f.MecashId > 0 && f.S3status == 1)
                        f.FilePath = ServiceURLFileTima + f.FilePath;
                }

                var obj = new EkycInput
                {
                    FrontId = lstCMND[0].FilePath,
                    BackId = lstCMND[1].FilePath,
                    Selfie = "",
                    Fullname = item.FullName,
                    Birthday = item.Dob != null ? item.Dob.Value.ToString("dd-MM-yyyy") : "",
                    NationalId = item.NationalCard
                };
                return obj;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public bool AddLogDistributingHubEmployee(LogDistributingHubEmployee entity)
        {
            try
            {
                _unitOfWork.LogDistributingHubEmployeeRepository.Insert(entity);
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool UpdateHubEmployeeReceived(int LoanBriefId)
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    HubEmployeeReceived = DateTime.Now,
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }
    }
}
