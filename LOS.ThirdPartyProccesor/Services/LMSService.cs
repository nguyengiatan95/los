﻿using LOS.Common.Extensions;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILMSService
    {
        PaymentLoanById.OutPut GetPaymentLoanById(int loanId, int loanbriefId);
        SendSMSParam.OutPut SendSMS(string Phone, string Content, int loanbriefId, int typeSendSms);
        CheckReLoan.OutPut CheckReBorrow(CheckReLoan.Input param, int loanbriefId);
        ChangePipelineReq.Output ChangePipeline(ChangePipelineReq.Input req);
        DataFamily GetFamily(string nationalCard, int loanbriefId);
        CheckBlackList.Output CheckBlackList(string FullName, string NumberCard, string Phone, int loanbriefId);
        TopupRes CheckTopupOto(CheckReLoan.Input param, int loanbriefId);
        Task<GetShopInfoResp> GetShopInfo(int lenderId);
        ResponeAddBlacklist AddBlackList(RequestAddBlacklist param);
        DefaultReponceLoanTopup GetListLoanTopUp();
        DefaultReponceLoanTopup GetListLoanSuggestTopUp();
        DefaultReponceLoanTopup GetListLoanTopUpLastDb();
    }
    public class LMSService : ILMSService
    {
        protected IConfiguration _baseConfig;
        private readonly ILogCallApiService _logCallApiService;
        private readonly IUnitOfWork _unitOfWork;
        public LMSService(ILogCallApiService logCallApiService)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(_baseConfig.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
            _logCallApiService = logCallApiService;
        }
        public PaymentLoanById.OutPut GetPaymentLoanById(int loanId, int loanbriefId)
        {
            var url = _baseConfig["AppSettings:LMS"] + String.Format(Constants.GetPaymentById, loanId);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            var objLogCallApi = new LogCallApi
            {
                ActionCallApi = (int)ActionCallApi.DeferredPayment,
                LinkCallApi = url,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = loanbriefId,
                Input = loanId.ToString(),
                CreatedAt = DateTime.Now
            };
            objLogCallApi.Id = _logCallApiService.Insert(objLogCallApi);

            IRestResponse response = client.Execute(request);
            var json = response.Content;
            objLogCallApi.Status = (int)StatusCallApi.Success;
            objLogCallApi.Output = json;
            _logCallApiService.Update(objLogCallApi);
            PaymentLoanById.OutPut lst = JsonConvert.DeserializeObject<PaymentLoanById.OutPut>(json);
            return lst;
        }

        public SendSMSParam.OutPut SendSMS(string Phone, string Content, int loanbriefId, int typeSendSms)
        {
            try
            {
                string url = _baseConfig["AppSettings:Proxy"] + Constants.SendSMS;
                SendSMSParam.Input param = new SendSMSParam.Input();
                param.Domain = "los.tima.vn";
                param.Department = "Hệ thống tự động";
                param.Phone = Phone;
                param.MessageContent = Content;
                var body = JsonConvert.SerializeObject(param);
                var logSendSms = new LogSendSms
                {
                    LoanbriefId = loanbriefId,
                    TypeSendSms = typeSendSms,
                    Url = url,
                    Smscontent = Content,
                    Request = body,
                    Status = (int)StatusCallApi.Success,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogSendSmsRepository.Insert(logSendSms);
                _unitOfWork.Save();
                var client = new RestClient(url);
                //client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "8JTLS9eqmSKXdQKdNWrtXCMV5DAhC3k7");
                request.AddHeader("account", "LOS");
                request.AddHeader("password", "3cmELn3UsqFkvERbuJdzHtUL7HU6uFuH");
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                _unitOfWork.LogSendSmsRepository.Update(x => x.Id == logSendSms.Id, x => new LogSendSms()
                {
                    Response = json,
                    UpdatedAt = DateTime.Now
                });
                _unitOfWork.Save();
                SendSMSParam.OutPut lst = JsonConvert.DeserializeObject<SendSMSParam.OutPut>(json);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        //public CheckReLoan.OutPut CheckReBorrow(CheckReLoan.Input param)
        //{
        //    var obj = new CheckReLoan.Input
        //    {
        //        NumberCard = param.NumberCard,
        //        CustomerName = param.CustomerName
        //    };
        //    var url = _baseConfig["AppSettings:LMS"] + Constants.DeferredPayment;
        //    var client = new RestClient(url);
        //    var body = JsonConvert.SerializeObject(obj);
        //    var request = new RestRequest(Method.POST);
        //    request.AddHeader("Content-Type", "application/json");
        //    request.AddParameter("application/json", body, ParameterType.RequestBody);
        //    try
        //    {
        //        IRestResponse response = client.Execute(request);
        //        if (response.StatusCode == System.Net.HttpStatusCode.OK)
        //        {
        //            var json = response.Content;

        //            CheckReLoan.OutputPayment lst = JsonConvert.DeserializeObject<CheckReLoan.OutputPayment>(json);
        //            CheckReLoan.OutPut lstReLoan = new CheckReLoan.OutPut();

        //            if (lst.Result == 1)
        //            {
        //                lstReLoan.Result = 1;
        //                if (lst.Data != null && lst.Data.LstLoanCustomer != null && lst.Data.LstLoanCustomer.Count() > 0)
        //                {
        //                    if (lst.Data.LstLoanCustomer[0].Status == EnumLoanStatus.FINISH.GetHashCode())
        //                    //&& (lst.Data.LstLoanCustomer[0].ProductID == (int)EnumProductCredit.MotorCreditType_CC
        //                    //    || lst.Data.LstLoanCustomer[0].ProductID == (int)EnumProductCredit.MotorCreditType_KCC
        //                    //    || lst.Data.LstLoanCustomer[0].ProductID == (int)EnumProductCredit.MotorCreditType_KGT))
        //                    {
        //                        //int MonthReLoan = Convert.ToInt32(_baseConfig["AppSettings:MonthReLoan"]);
        //                        int DayReLoan = Convert.ToInt32(_baseConfig["AppSettings:DayReLoan"]);
        //                        //var days = (DateTime.Now - Convert.ToDateTime(lst.Data.LstLoanCustomer[0].FinishDate)).Days;
        //                        //if (days <= (MonthReLoan * 30))
        //                        //{
        //                        var CountDate = 0;
        //                        if (lst.Data.LstLoanCustomer[0].LstPaymentCustomer.Count() > 0)
        //                        {
        //                            foreach (var item in lst.Data.LstLoanCustomer[0].LstPaymentCustomer)
        //                            {
        //                                if (item.CountDay > DayReLoan)
        //                                {
        //                                    CountDate = Convert.ToInt32(item.CountDay);
        //                                    break;
        //                                }
        //                            }
        //                        }
        //                        if (CountDate <= DayReLoan)
        //                        {
        //                            lstReLoan.IsAccept = 1;
        //                            if (lst.Data.LstLoanCustomer[0].LoanBriefID > 0)
        //                                lstReLoan.LoanBriefID = lst.Data.LstLoanCustomer[0].LoanBriefID;
        //                            else
        //                                lstReLoan.LoanBriefID = lst.Data.LstLoanCustomer[0].LoanId;
        //                            lstReLoan.Message = "Thành công!";
        //                        }
        //                        else
        //                        {
        //                            lstReLoan.Message = "Khách hàng có kỳ trả chậm " + CountDate + "!";
        //                        }
        //                        //}
        //                        //else
        //                        //{
        //                        //    lstReLoan.Message = "Thời gian tất toán khoản vay cũ > " + MonthReLoan + " tháng!";
        //                        //}
        //                    }
        //                    else
        //                    {
        //                        lstReLoan.Message = "Khách hàng chưa đủ điều kiện tái vay do đơn vay đang ở trạng thái " + lst.Data.LstLoanCustomer[0].StatusName;
        //                    }
        //                }
        //                else
        //                {
        //                    lstReLoan.Message = lst.Message;
        //                }

        //            }
        //            return lstReLoan;
        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex, "LMS/CheckReLoan Exception");
        //        return null;
        //    }




        //}
        public CheckReLoan.OutPut CheckReBorrow(CheckReLoan.Input param, int loanbriefId)
        {
            var obj = new CheckReLoan.Input
            {
                NumberCard = param.NumberCard,
                CustomerName = param.CustomerName
            };
            var url = _baseConfig["AppSettings:LMS"] + Constants.DeferredPayment;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(obj);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.DeferredPayment,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = "AUTO",
                    LoanBriefId = loanbriefId,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                objLogCallApi.Id = _logCallApiService.Insert(objLogCallApi);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var json = response.Content;
                    objLogCallApi.Status = (int)StatusCallApi.Success;
                    objLogCallApi.Output = json;
                    _logCallApiService.Update(objLogCallApi);
                    CheckReLoan.OutputPayment lst = JsonConvert.DeserializeObject<CheckReLoan.OutputPayment>(json);
                    CheckReLoan.OutPut lstReLoan = new CheckReLoan.OutPut();

                    long maxCountDayLate = 0; //Số ngày trả muộn lâu nhất trong các kỳ
                    long maxCountDayLateNearest = 0; // Số ngày trả muộn lâu nhất kỳ gần nhất
                    if (lst.Result == 1)
                    {
                        lstReLoan.Result = 1;
                        if (lst.Data != null && lst.Data.LstLoanCustomer != null && lst.Data.LstLoanCustomer.Count() > 0)
                        {
                            if (lst.Data.LstLoanCustomer[0].Status == EnumLoanStatus.FINISH.GetHashCode())
                            {
                                //Kiểm tra tổng số kỳ thanh toán của tất cả HĐ tối thiểu 3 kỳ

                                var countPayment = 0;// Tổng các kỳ thanh toán

                                foreach (var item in lst.Data.LstLoanCustomer)
                                {
                                    countPayment = countPayment + item.LstPaymentCustomer.Count() + 1;
                                    if (countPayment >= 3)
                                        break;
                                }
                                //lstReLoan.MaxCountDayLate = maxCountDayLate;
                                if (countPayment < 3)
                                {
                                    lstReLoan.Message = "Không đủ điều kiện do tổng kỳ thanh toán chưa đạt điều kiện";
                                    return lstReLoan;
                                }
                                foreach (var item in lst.Data.LstLoanCustomer)
                                {
                                    if (item.LstPaymentCustomer != null && item.LstPaymentCustomer.Count() > 0)
                                    {
                                        //Lấy số ngày trả chậm cao nhất trong cac kỳ
                                        var maxCountDay = item.LstPaymentCustomer.Max(x => x.CountDay);
                                        if (maxCountDayLate < maxCountDay)
                                            maxCountDayLate = maxCountDay;
                                    }
                                }
                                lstReLoan.MaxCountDayLate = maxCountDayLate;
                                int DayReLoan = Convert.ToInt32(_baseConfig["AppSettings:DayReLoan"]);
                                foreach (var item in lst.Data.LstLoanCustomer[0].LstPaymentCustomer)
                                {
                                    if (item.CountDay > DayReLoan)
                                    {
                                        maxCountDayLateNearest = Convert.ToInt32(item.CountDay);
                                        break;
                                    }
                                }
                                if (maxCountDayLateNearest <= DayReLoan)
                                {
                                    lstReLoan.IsAccept = 1;
                                    if (lst.Data.LstLoanCustomer[0].LoanBriefID > 0)
                                        lstReLoan.LoanBriefID = lst.Data.LstLoanCustomer[0].LoanBriefID;
                                    else
                                        lstReLoan.LoanBriefID = lst.Data.LstLoanCustomer[0].LoanId;
                                    lstReLoan.Message = "Thành công!";
                                }
                                else
                                {
                                    lstReLoan.Message = "Khách hàng có kỳ trả chậm " + maxCountDayLateNearest + "!";
                                }

                            }
                            else
                            {
                                foreach (var item in lst.Data.LstLoanCustomer)
                                {
                                    if (item.LstPaymentCustomer != null && item.LstPaymentCustomer.Count() > 0)
                                    {
                                        //Lấy số ngày trả chậm cao nhất trong cac kỳ
                                        var maxCountDay = item.LstPaymentCustomer.Max(x => x.CountDay);
                                        if (maxCountDayLate < maxCountDay)
                                            maxCountDayLate = maxCountDay;
                                    }
                                }
                                lstReLoan.MaxCountDayLate = maxCountDayLate;
                                lstReLoan.Message = "Khách hàng chưa đủ điều kiện tái vay do đơn vay đang ở trạng thái " + lst.Data.LstLoanCustomer[0].StatusName;
                            }
                        }
                        else
                        {
                            lstReLoan.Message = lst.Message;
                        }

                    }
                    return lstReLoan;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/CheckReLoan Exception");
                return null;
            }




        }

        public ChangePipelineReq.Output ChangePipeline(ChangePipelineReq.Input req)
        {
            var url = _baseConfig["AppSettings:WebApi"] + Constants.ChangePipeline;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(req);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.ChangePipe,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = "",
                    LoanBriefId = req.loanBriefId,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                objLogCallApi.Id = _logCallApiService.Insert(objLogCallApi);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = response.Content;
                    objLogCallApi.Status = (int)StatusCallApi.Success;
                    objLogCallApi.Output = json;
                    _logCallApiService.Update(objLogCallApi);

                    ChangePipelineReq.Output lst = JsonConvert.DeserializeObject<ChangePipelineReq.Output>(json);
                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LMS/ChangePipeline Exception");
                return null;
            }
        }


        public DataFamily GetFamily(string nationalCard, int loanbriefId)
        {
            try
            {
                if (string.IsNullOrEmpty(nationalCard))
                    return null;
                string url = _baseConfig["AppSettings:Proxy"] + string.Format(Constants.Kalapa_GetFamily, nationalCard);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "8JTLS9eqmSKXdQKdNWrtXCMV5DAhC3k7");
                request.AddHeader("account", "LOS");
                request.AddHeader("password", "3cmELn3UsqFkvERbuJdzHtUL7HU6uFuH");
                request.AddHeader("Content-Type", "application/json");
                var client = new RestClient(url);
                client.Timeout = 5000;
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.KalapaFamily,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = "",
                    LoanBriefId = loanbriefId,
                    Input = string.Format("{0} - {1}", nationalCard, loanbriefId),
                    CreatedAt = DateTime.Now
                };
                objLogCallApi.Id = _logCallApiService.Insert(objLogCallApi);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = response.Content;
                    objLogCallApi.Status = (int)StatusCallApi.Success;
                    objLogCallApi.Output = json;
                    _logCallApiService.Update(objLogCallApi);
                    InfoFamilyRes data = JsonConvert.DeserializeObject<InfoFamilyRes>(json);
                    if (data.Result == 1 && !string.IsNullOrEmpty(data.Data))
                    {
                        data.Data = data.Data.Replace("\\", "");
                        return JsonConvert.DeserializeObject<DataFamily>(data.Data);
                    }
                }
                return null;

            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public CheckBlackList.Output CheckBlackList(string FullName, string NumberCard, string Phone, int loanbriefId)
        {
            var entity = new CheckBlackList.Input
            {
                FullName = FullName,
                Phone = Phone,
                NumberCard = NumberCard
            };
            var url = _baseConfig["AppSettings:LMS"] + Constants.CheckBlackList;
            var body = JsonConvert.SerializeObject(entity);
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            //Add log
            var objLogCallApi = new LogCallApi
            {
                ActionCallApi = (int)ActionCallApi.BlackList,
                LinkCallApi = url,
                Status = (int)StatusCallApi.CallApi,
                TokenCallApi = "",
                LoanBriefId = loanbriefId,
                Input = body,
                CreatedAt = DateTime.Now
            };
            objLogCallApi.Id = _logCallApiService.Insert(objLogCallApi);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            objLogCallApi.Status = (int)StatusCallApi.Success;
            objLogCallApi.Output = json;
            _logCallApiService.Update(objLogCallApi);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                CheckBlackList.Output lst = JsonConvert.DeserializeObject<CheckBlackList.Output>(json);
                return lst;
            }
            return null;
        }

        public TopupRes CheckTopupOto(CheckReLoan.Input param, int loanbriefId)
        {
            var obj = new DeferredPaymentReq
            {
                NumberCard = param.NumberCard,
                CustomerName = param.CustomerName
            };
            var url = _baseConfig["AppSettings:LMS"] + Constants.DeferredPayment;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(obj);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            //Add log
            var objLogCallApi = new LogCallApi
            {
                ActionCallApi = ActionCallApi.DeferredPayment.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = "CAR-AUTO",
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = loanbriefId,
                Input = body,
                CreatedAt = DateTime.Now
            };
            objLogCallApi.Id = _logCallApiService.Insert(objLogCallApi);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            objLogCallApi.Status = (int)StatusCallApi.Success;
            objLogCallApi.Output = json;
            objLogCallApi.ModifyAt = DateTime.Now;
            _logCallApiService.Update(objLogCallApi);
            DeferredPaymentRes lst = JsonConvert.DeserializeObject<DeferredPaymentRes>(json);
            TopupRes lstTopup = new TopupRes();

            if (lst != null && lst.Result == 1)
            {
                lstTopup.IsAccept = 0;
                if (lst.Data != null && lst.Data.LstLoanCustomer != null)
                {

                    //Lấy ra những đơn đang vay và là gói oto và có đơn đang vay
                    //Kiểm tra điều kiện topup
                    // 1- khoản vay oto đang còn hiệu lực
                    var lstData = lst.Data.LstLoanCustomer.Where(x => x.Status == (int)EnumStatusLMS.Disbursed &&
                    (x.ProductID == (int)EnumProductCredit.OtoCreditType_CC || x.ProductID == (int)EnumProductCredit.OtoCreditType_KCC)).ToList();

                    if (lstData != null && lstData.Count > 0)
                    {
                        var isKyThanhToan = true;
                        var isNoQuaHan = true;
                        int noQuaHan = Convert.ToInt32(_baseConfig["AppSettings:NoQuaHan"]);
                        var totalMoneyCurrent = 0L;
                        foreach (var item in lstData)
                        {
                            // 2- Kiểm tra các đơn đang vay đã thanh toán tối thiểu 1 kỳ
                            if (item.LstPaymentCustomer.Count() == 0)
                            {
                                isKyThanhToan = false;
                                break;
                            }
                            // 3- Lịch sử thanh toán trên các khoản đang vay hiện tại không nợ quá hạn trên 30 ngày                           
                            foreach (var itemPayment in item.LstPaymentCustomer)
                            {
                                if (itemPayment.CountDay > noQuaHan)
                                {
                                    isNoQuaHan = false;
                                    break;
                                }
                            }
                            if (!isNoQuaHan)
                                break;

                            //TotalMoneyCurrent 
                            totalMoneyCurrent = totalMoneyCurrent + item.TotalMoneyCurrent;
                        }
                        if (!isKyThanhToan)
                            lstTopup.Message = "Không đủ điều kiện do khách hàng có khoản vay chưa thanh toán kỳ nào";
                        if (!isNoQuaHan)
                            lstTopup.Message = "Không đủ điều kiện do khách hàng có kỳ trả chậm > " + noQuaHan + " ngày";
                        if (isKyThanhToan && isNoQuaHan)
                        {
                            lstTopup.IsAccept = 1;
                            if (lstData[0].LoanBriefID > 0)
                                lstTopup.LoanBriefId = lstData[0].LoanBriefID;
                            else
                                lstTopup.LoanBriefId = lstData[0].LoanId;

                            lstTopup.TotalMoneyCurrent = totalMoneyCurrent;
                        }
                    }
                    else
                    {
                        lstTopup.Message = "Khách hàng không có đơn vay ô tô nào ở trạng thái đang vay";
                    }
                }
                else
                {
                    lstTopup.Message = lst.Message;
                }
            }
            return lstTopup;
        }

        public async Task<GetShopInfoResp> GetShopInfo(int lenderId)
        {
            var client = new RestClient(_baseConfig["AppSettings:LMS"]);
            var request = new RestRequest("api/Shop/GetInfoLender").AddParameter("lenderId", lenderId);
            string token = _baseConfig["AppSettings:LMSToken"];
            if (String.IsNullOrEmpty(token))
            {
                token = "8228459735";
            }
            request.AddHeader("Authorization", token);
            request.Method = Method.GET;
            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<GetShopInfoResp>(response.Content);
                    return objData;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "AgService/GetShopInfo Exception");
            }
            return null;
        }

        public ResponeAddBlacklist AddBlackList(RequestAddBlacklist param)
        {
            {
                var url = _baseConfig["AppSettings:LMS"] + Constants.AddBlacklist;
                //var url = "http://gcollect.tima.vn" + Constants.AddBlacklist;
                var body = JsonConvert.SerializeObject(param);
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                try
                {
                    //Lưu lại log
                    var log = new LogCallApi
                    {
                        ActionCallApi = ActionCallApi.AddBlacklist.GetHashCode(),
                        LinkCallApi = url,
                        Status = (int)StatusCallApi.CallApi,
                        LoanBriefId = param.LoanCreditId,
                        Input = body,
                        CreatedAt = DateTime.Now,
                    };
                    log.Id = _logCallApiService.Insert(log);
                    var response = client.Execute<ResponeAddBlacklist>(request);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        log.Status = (int)StatusCallApi.Success;
                        log.Output = response.Content;
                        log.ModifyAt = DateTime.Now;
                        _logCallApiService.Update(log);
                        var lst = JsonConvert.DeserializeObject<ResponeAddBlacklist>(response.Content);
                        return lst;
                    }
                }
                catch (Exception ex)
                {
                }
                return null;
            }
        }

        public DefaultReponceLoanTopup GetListLoanTopUp()
        {
            var url = _baseConfig["AppSettings:LMS"] + Constants.GetListLoanTopUp;
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            var objLogCallApi = new LogCallApi
            {
                ActionCallApi = (int)ActionCallApi.GetListLoanTopUp,
                LinkCallApi = url,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = 0,
                Input = "",
                CreatedAt = DateTime.Now
            };
            objLogCallApi.Id = _logCallApiService.Insert(objLogCallApi);

            IRestResponse response = client.Execute(request);
            var json = response.Content;
            objLogCallApi.Status = (int)StatusCallApi.Success;
            objLogCallApi.Output = json;
            _logCallApiService.Update(objLogCallApi);
            DefaultReponceLoanTopup lst = JsonConvert.DeserializeObject<DefaultReponceLoanTopup>(json);
            return lst;
        }
        public DefaultReponceLoanTopup GetListLoanSuggestTopUp()
        {
            var url = _baseConfig["AppSettings:LMS"] + Constants.GetListLoanSuggestTopUp;
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            var objLogCallApi = new LogCallApi
            {
                ActionCallApi = (int)ActionCallApi.GetListLoanSuggestTopUp,
                LinkCallApi = url,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = 0,
                Input = "",
                CreatedAt = DateTime.Now
            };
            objLogCallApi.Id = _logCallApiService.Insert(objLogCallApi);

            IRestResponse response = client.Execute(request);
            var json = response.Content;
            objLogCallApi.Status = (int)StatusCallApi.Success;
            objLogCallApi.Output = json;
            _logCallApiService.Update(objLogCallApi);
            DefaultReponceLoanTopup lst = JsonConvert.DeserializeObject<DefaultReponceLoanTopup>(json);
            return lst;
        }
        public DefaultReponceLoanTopup GetListLoanTopUpLastDb()
        {

            var data = _logCallApiService.GetLastType((int)ActionCallApi.GetListLoanTopUp);
            if (data != null)
                return JsonConvert.DeserializeObject<DefaultReponceLoanTopup>(data.Output);

            return null;
        }

    }
}
