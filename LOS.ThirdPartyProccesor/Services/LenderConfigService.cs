﻿using LOS.Common.Extensions;
using LOS.DAL.Dapper;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers;
using LOS.DAL.UnitOfWork;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
	public interface ILenderAutoDistributeService
	{
		List<LoanBrief> GetWaitDistributingLoanBriefs();
		List<LoanBrief> GetRetrieveableLoanBriefs();
		List<LenderConfig> GetLenderConfigs(int level, bool isLender);
		bool ReleaseLock(int LoanBriefId, int lenderId, int status);
		bool ReleaseLockedMoney(int lenderId, decimal amount);
		bool InsertLenderLoanBriefs(List<LenderLoanBrief> lenderLoanBriefs);
		bool InsertLenderLoanBrief(LenderLoanBrief lenderLoanBrief);
		bool UpdateReceived(int lenderId, int type);
		bool UpdateReceivedLock(int lenderId, decimal amount);
		bool UpdateNotMet(int LoanBriefId);
		bool UpdateConflict(int LoanBriefId);
		bool UpdateDistributed(int LoanBriefId, int level);
		bool UpdateLockDistributed(int LoanBriefId, int level);
		bool UpdateManualPush(int LoanBriefId);
		bool UpdateLoanBriefRetrieve(int loanBriefId);
		bool UpdateLoanBriefAllIgnore(int loanBriefId);
		List<LenderLoanBrief> GetLenderLoanBriefByState(int loanBriefId, int state);
		bool UpdateLenderLoanBrief(int loanBriefId, int lenderId, int status);
		bool IsAutoDistributing();
		string AutoDistributingLender();
		bool UpdateEsignLenderConfig(List<int> ids, bool esign);
	}

	public class LenderAutoDistributeService : ILenderAutoDistributeService
	{
		private readonly IUnitOfWork _unitOfWork;
		protected IConfiguration _baseConfig;
		public LenderAutoDistributeService()
		{
			// create new unit of work instance			
			var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
			// create new unit of work instance			
			var builder = new ConfigurationBuilder()
			  .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
			  .AddJsonFile("appsettings.json", true)
			  .AddJsonFile($"appsettings.{environmentName}.json", true);
			_baseConfig = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(_baseConfig.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
		}

		public List<LenderConfig> GetLenderConfigs(int level, bool isLender)
		{
			try
			{
				return _unitOfWork.LenderConfigRepository.Query(x => x.Priority == level && x.IsLender == isLender && x.Status == 1, null, false).ToList();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<LoanBrief> GetWaitDistributingLoanBriefs()
		{
			try
			{
				return _unitOfWork.LoanBriefRepository.Query(x => x.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.READY_TO_DISTRIBUTE
					&& x.Status == (int)EnumLoanStatus.LENDER_LOAN_DISTRIBUTING && x.IsLock != true, null, false).ToList();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<LoanBrief> GetRetrieveableLoanBriefs()
		{
			try
			{
				return _unitOfWork.LoanBriefRepository.Query(x => (x.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING
					|| x.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED)
					&& (x.Status == (int)EnumLoanStatus.WAIT_DISBURSE || x.Status == (int)EnumLoanStatus.LENDER_LOAN_DISTRIBUTING) && x.IsLock != true, null, false).ToList();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool InsertLenderLoanBriefs(List<LenderLoanBrief> lenderLoanBriefs)
		{
			try
			{
				_unitOfWork.LenderLoanBriefRepository.BulkInsert(lenderLoanBriefs);
				_unitOfWork.SaveChanges();
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool InsertLenderLoanBrief(LenderLoanBrief lenderLoanBrief)
		{
			try
			{
				_unitOfWork.LenderLoanBriefRepository.SingleInsert(lenderLoanBrief);
				_unitOfWork.SaveChanges();
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool ReleaseLock(int LoanBriefId, int lenderId, int status)
		{
			try
			{
				// trả về trạng thái trước
				_unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
				{
					InProcess = (int)EnumInProcess.Process,
					ActionState = (int)EnumActionPush.Back,
					PipelineState = (int)EnumPipelineState.MANUAL_PROCCESSED,
					AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.READY_TO_DISTRIBUTE,
					PriorityLevel = x.PriorityLevel + 1
				});
				// Cập nhật bảng lender loan brief
				_unitOfWork.LenderLoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId && x.LenderId == lenderId, x => new LenderLoanBrief()
				{
					Status = status,
					UpdatedTime = DateTime.Now
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateReceived(int lenderId, int type)
		{
			try
			{
				if (type == 1)
				{
					_unitOfWork.LenderConfigRepository.Update(x => x.LenderId == lenderId, x => new LenderConfig()
					{
						Received = x.Received + 1
					});
				}
				else if (type == 2)
				{
					_unitOfWork.LenderConfigRepository.Update(x => x.LenderId == lenderId, x => new LenderConfig()
					{
						Accepted = x.Accepted + 1
					});
				}
				else if (type == 3)
				{
					_unitOfWork.LenderConfigRepository.Update(x => x.LenderId == lenderId, x => new LenderConfig()
					{
						Returned = x.Returned + 1
					});
				}
				else if (type == 4)
				{
					_unitOfWork.LenderConfigRepository.Update(x => x.LenderId == lenderId, x => new LenderConfig()
					{
						Retrieved = x.Retrieved + 1
					});
				}
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool ReleaseLockedMoney(int lenderId, decimal amount)
		{
			try
			{
				_unitOfWork.LenderConfigRepository.Update(x => x.LenderId == lenderId, x => new LenderConfig()
				{
					LockedMoney = x.LockedMoney - amount,
					Retrieved = x.Retrieved + 1,
					UpdatedTime = DateTime.Now
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateReceivedLock(int lenderId, decimal amount)
		{
			try
			{
				_unitOfWork.LenderConfigRepository.Update(x => x.LenderId == lenderId, x => new LenderConfig()
				{
					LockedMoney = x.LockedMoney + amount,
					Retrieved = x.Received + 1,
					UpdatedTime = DateTime.Now
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateLoanBriefRetrieve(int loanBriefId)
		{
			try
			{
				_unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
				{
					PriorityLevel = x.PriorityLevel + 1,
					AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.READY_TO_DISTRIBUTE,
					InProcess = (int)EnumInProcess.Process,
					ActionState = (int)EnumActionPush.Back,
					PipelineState = (int)EnumPipelineState.AUTOMATIC_PROCESSED,
					TypeDisbursement = null,
					LenderId = null,
					LenderReceivedDate = null,
					NextDate = null,
					LenderName = null
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateLoanBriefAllIgnore(int loanBriefId)
		{
			try
			{
				_unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
				{
					PriorityLevel = x.PriorityLevel + 1,
					AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.READY_TO_DISTRIBUTE
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateNotMet(int LoanBriefId)
		{
			try
			{
				_unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
				{
					AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.REQUIREMENT_NOT_MET,
					PriorityLevel = null
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateConflict(int LoanBriefId)
		{
			try
			{
				_unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
				{
					AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.CONFLICT,
					PriorityLevel = null
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateDistributed(int LoanBriefId, int level)
		{
			try
			{
				_unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
				{
					AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED,
					PriorityLevel = level
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateLockDistributed(int LoanBriefId, int level)
		{
			try
			{
				_unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
				{
					AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING,
					PriorityLevel = level
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateManualPush(int LoanBriefId)
		{
			try
			{
				_unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
				{
					AutoDistributeState = (int)Common.Helpers.Const.AutoDistributeState.MANUAL_PUSH
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<LenderLoanBrief> GetLenderLoanBriefByState(int loanBriefId, int state)
		{
			try
			{
				return _unitOfWork.LenderLoanBriefRepository.Query(x => x.LoanBriefId == loanBriefId && x.Status == state, null, false).OrderByDescending(x => x.PushedTime).ToList();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateLenderLoanBrief(int loanBriefId, int lenderId, int status)
		{
			try
			{
				_unitOfWork.LenderLoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId && x.LenderId == lenderId && x.Status != status, x => new LenderLoanBrief()
				{
					Status = status,
					UpdatedTime = DateTime.Now
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool IsAutoDistributing()
		{
			try
			{
				var config = _unitOfWork.SystemConfigRepository.Query(x => x.Name == ConfigHelper.LENDER_AUTO_DISTRIBUTING_KEY, null, false).FirstOrDefault();
				if (config != null)
				{
					if (config.Value.Equals("true"))
						return true;
				}
				return false;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public string AutoDistributingLender()
		{
			try
			{
				var config = _unitOfWork.SystemConfigRepository.Query(x => x.Name == ConfigHelper.LENDER_AUTO_DISTRIBUTING_LENDER_KEY, null, false).FirstOrDefault();
				if (config != null)
				{
					return config.Value;
				}
				return "NA";
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateEsignLenderConfig(List<int> ids, bool esign)
		{
			try
			{
				_unitOfWork.LenderConfigRepository.Update(x => ids.Contains(x.LenderId.Value), x => new LenderConfig()
				{
					Esign = esign,
					EsignUpdatedTime = DateTime.Now
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
