﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILoanBriefImportFileExcelServices
    {
        List<LoanBriefImportFileExcel> GetLoanBriefImportFileExcelNoPush();
        int CreateLoanImportFileExcel(LoanBriefImportFileExcel entity);
    }
    public class LoanBriefImportFileExcelServices : ILoanBriefImportFileExcelServices
    {
        private readonly IUnitOfWork _unitOfWork;
        public LoanBriefImportFileExcelServices()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public List<LoanBriefImportFileExcel> GetLoanBriefImportFileExcelNoPush()
        {
            try
            {
                //lấy ra các đơn chưa đc đẩy trong bảng
                return _unitOfWork.LoanBriefImportFileExcelRepository.Get(x => x.Status == (int)EnumStatusLoanBriefImportFileExcel.LoanDefault, null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int CreateLoanImportFileExcel(LoanBriefImportFileExcel entity)
        {
            try
            {
                //Check xem khách hàng có đơn vay nào đang xử lý hay không
                var loanInfo = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == entity.Phone, null, false).OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                if (loanInfo != null)
                {
                    if (loanInfo.Status != EnumLoanStatus.CANCELED.GetHashCode() && loanInfo.Status != EnumLoanStatus.FINISH.GetHashCode())
                    {
                        // có đơn vay đang xử lý thì update lại status của bảng ImportFileExcel thành -1
                        _unitOfWork.LoanBriefImportFileExcelRepository.Update(x => x.Id == entity.Id, x => new LoanBriefImportFileExcel()
                        {
                            Status = (int)EnumStatusLoanBriefImportFileExcel.LoanFail,
                            UpdateDate = DateTime.Now,
                            Note = "Khách hàng đã có đơn vay đang xử lý"
                        });
                        _unitOfWork.Save();
                        return 0;
                    }
                }
                //check xem có sđt.....hoặc sđt có đúng định dạng
                if(!string.IsNullOrEmpty(entity.Phone))
                {
                    if (!entity.Phone.ValidPhone())
                    {
                        //Không có sđt
                        _unitOfWork.LoanBriefImportFileExcelRepository.Update(x => x.Id == entity.Id, x => new LoanBriefImportFileExcel()
                        {
                            Status = (int)EnumStatusLoanBriefImportFileExcel.LoanFail,
                            UpdateDate = DateTime.Now,
                            Note = "Số điện thoại không đúng định dạng"
                        });
                        _unitOfWork.Save();
                        return 0;
                    }
                }
                else
                {
                    //Không có sđt
                    _unitOfWork.LoanBriefImportFileExcelRepository.Update(x => x.Id == entity.Id, x => new LoanBriefImportFileExcel()
                    {
                        Status = (int)EnumStatusLoanBriefImportFileExcel.LoanFail,
                        UpdateDate = DateTime.Now,
                        Note = "Khách không có số điện thoại không tạo được đơn"
                    });
                    _unitOfWork.Save();
                    return 0;

                }


                //xử lý tên KH
                if (!string.IsNullOrEmpty(entity.FullName))
                {
                    entity.FullName = entity.FullName.ReduceWhitespace();
                    entity.FullName = entity.FullName.TitleCaseString();
                }

                var obj = new LoanBrief();
                var objCus = new Customer();

                //Check xem customer đã tồn tại hay chưa
                var customerInfo = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == entity.Phone);
                if (customerInfo != null && customerInfo.CustomerId > 0)
                {
                    //cập nhật thông tin customer
                    customerInfo.CreatedAt = DateTime.Now;
                    customerInfo.FullName = entity.FullName;
                    customerInfo.Phone = entity.Phone;
                    customerInfo.ProvinceId = entity.CityId;
                    customerInfo.DistrictId = entity.DistrictId;
                    customerInfo.NationalCard = entity.NationalCard;
                    customerInfo.Gender = entity.Gender;
                    _unitOfWork.CustomerRepository.Update(customerInfo);
                    _unitOfWork.Save();
                    obj.CustomerId = customerInfo.CustomerId;
                }
                else
                {
                    objCus.CreatedAt = DateTime.Now;
                    objCus.FullName = entity.FullName;
                    objCus.Phone = entity.Phone;
                    objCus.ProvinceId = entity.CityId;
                    objCus.DistrictId = entity.DistrictId;
                    objCus.NationalCard = entity.NationalCard;
                    objCus.Gender = entity.Gender;
                    _unitOfWork.CustomerRepository.Insert(objCus);
                    _unitOfWork.Save();
                    obj.CustomerId = objCus.CustomerId;
                }
                if (entity.CityId > 0 || entity.DistrictId > 0)
                {
                    obj.LoanBriefResident = new LoanBriefResident()
                    {
                        ProvinceId = entity.CityId,
                        DistrictId = entity.DistrictId
                    };
                }
                if (entity.CityIdHouseHold > 0 || entity.DistrictIdHouseHold > 0)
                {
                    obj.LoanBriefHousehold = new LoanBriefHousehold()
                    {
                        ProvinceId = entity.CityIdHouseHold,
                        DistrictId = entity.DistrictIdHouseHold
                    };
                }
                if(entity.Salary > 0 || entity.CompanyName != "")
                {
                    obj.LoanBriefJob = new LoanBriefJob()
                    {
                        TotalIncome = entity.Salary,
                        CompanyName = entity.CompanyName
                    };
                }
                obj.IsHeadOffice = true;
                obj.CreatedTime = DateTime.Now;
                obj.FullName = entity.FullName;
                obj.Phone = entity.Phone;
                obj.NationalCard = entity.NationalCard;
                obj.Status = EnumLoanStatus.INIT.GetHashCode();
                obj.ProvinceId = entity.CityId;
                obj.DistrictId = entity.DistrictId;
                obj.ProductId = entity.ProductId;
                obj.UtmSource = entity.Source;
                obj.Gender = entity.Gender;
                obj.CreateBy = entity.CreateBy;
                obj.BoundTelesaleId = entity.BoundTelesaleId.GetValueOrDefault(0) == 0 ? null : entity.BoundTelesaleId;
                obj.PlatformType = EnumPlatformType.ImportFileExcel.GetHashCode();
                obj.TypeLoanBrief = LoanBriefType.Customer.GetHashCode();
                obj.LoanBriefNote = new List<LoanBriefNote>();
                obj.LoanBriefNote.Add(new LoanBriefNote()
                {
                    FullName = "Auto System",
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    Note = string.Format("Đơn vay được khởi tạo bằng ImportExcel"),
                    Status = 1,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                });

                //insert to db
                _unitOfWork.LoanBriefRepository.Insert(obj);
                _unitOfWork.Save();

                if(obj.LoanBriefId > 0)
                {
                    //Lưu lại status và LoanBriefId vào bảng LoanBriefImportFileExcel
                    _unitOfWork.LoanBriefImportFileExcelRepository.Update(x => x.Id == entity.Id, x => new LoanBriefImportFileExcel()
                    {
                        Status = (int)EnumStatusLoanBriefImportFileExcel.LoanSuccess,
                        LoanBriefId = obj.LoanBriefId,
                        UpdateDate = DateTime.Now,
                        Note = "Tạo đơn thành công"
                    });
                    _unitOfWork.Save();
                }
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
