﻿using LOS.Common.Extensions;
using LOS.DAL.Dapper;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILoanBriefService
    {
        List<LoanBrief> WaitPushToHub();
        List<LoanBrief> HubDistributing();
        List<LoanBriefVerifyDetail> WaitGetInfoFromAI();
        List<LogRequestAi> AllRequestAI();
        bool ExcutedRequestAI(int id, int isExcuted = (int)EnumLogLoanInfoAiExcuted.HasResult);
        LoanBriefVerifyDetail LoanbriefInfoFromAI(int LoanbriefId);
        LoanBriefInfo LoanbriefInfo(int LoanbriefId);
        LoanBrief GetById(int LoanbriefId);
        LoanBriefInfoLog LoanbriefInfoPushLog(int LoanbriefId);
        bool PushToPipeline(int LoanBriefId, int ActionState);
        bool UpdateHub(int LoanBriefId, int HubId);
        bool UpdateResultRefphone(int LoanBriefId, string Phone, int call_rate, int call_duration);
        bool AddNote(LoanBriefNote entity);
        List<LoanBrief> WaitPushToTDHS();
        List<LoanBrief> WaitHubHandle();
        List<LoanBrief> WaitLenderCareHandle();
        List<LeadScoreInfoDetail> WaitGetLeadScore();
        List<LoanbriefDistributingTelelsale> WaitPushToTelesale();
        bool UpdateTDHS(int LoanBriefId, int userId);
        bool DistributingTelesale(int LoanBriefId, int telesaleId, int? TelesaleTeam = 0);
        void DoneSendRequestLocation(int LoanBriefId, string message);
        bool PushLoan(int LoanBriefId);
        bool CancelLoanPushPipeline(int LoanBriefId, int ReasonCancel, string Comment);
        bool CancelLoan(int LoanBriefId, int ReasonCancel, string Comment);
        bool UpdateResultRuleCheck(int LoanBriefId, string ResultRuleCheck);
        bool UpdateResultLocation(int LoanBriefId, string ResultLocationFinal);
        bool UpdateResultLocation(int LoanBriefId, string ResultLocationHome, string ResultLocationCompany);
        bool UpdateRangeScore(int LoanBriefId, int RangeScore);
        bool AddLogDistributingTelesales(TelesaleLoanbrief entity);
        List<LoanBrief> GetListLoanBriefOfFinanceNotSynchorize();
        ReasonCancel GetReasonNameLoanBrief(int reasonCancelId);
        bool UpdateSynchorizeFinance(short SynchorizeFinance, int LoanBriefId);
        bool DoneCreditScoring(int loanbriefId, decimal creditScoring, string labelScore);
        List<LoanBrief> GetListLoanOver(int day);
        TelesaleLoanbrief GetFirstDistributingTelesales(int loanbriefId);
        List<LoanBrief> GetLoanBriefReasonCancelIsPushToFinance();
        List<LoanBriefInfoTopup> ListLoanTopup();
        bool UpdateTypeRemarketing(int loanbriefId, int typeLoanSupport);
        public List<LoanBriefFileDetail> GetFilesByLoanId(int loanBriefId);
        public List<DocumentTypeDetail> GetDocumentTypeProductId(int productId);
        LogCallApi ListCallApiTopup();
        LoanBriefInfoTopup LoanbriefInfoTopup(int LoanbriefId);
        LoanBriefInfoTopup LoanbriefInfoByReMKTId(int LoanbriefId);
        bool UpdateLogCallApiTopup(int Id, int status);
        bool UpdateAllIsCanTopup();
        bool UpdateIsReborrow(int loanBriefId);
        List<LoanBrief> GetListLoanBriefNoCheckQlf();
        LoanBriefQuestionScript GetLoanBriefQuestionScript(int loanbriefId);
        LoanBriefResident GetLoanBriefResident(int loanbriefId);
        LoanBriefJob GetLoanBriefJob(int loanbriefId);
        District GetDistrictIsApply(int districtId);
        List<LoanBriefFiles> GetLoanBriefFiles(int loanbriefId);
        bool UpdateValueQualify(int loanBriefId, int valueCheckQualify);
        List<LoanBrief> WaitStaffHubHandle();
        LoanBriefJob GetLoanbriefJob(int loanbriefId);
        bool AddJsonInfoFamilyKalapa(int loanbriefId, string json);
        List<LoanBrief> GetListLoanFinish(string productIds, int limitedDay);
        bool LoanbriefHandle(string phone, string nationalCard);
        bool LoanbriefHandleCar(string phone, string nationalCard);
        int ReCreateLoanFinish(int LoanBriefId, bool isReborrow = false);
        int ReCreateLoanFinishCar(int LoanBriefId);
        List<LoanBrief> GetLoanDisbursementNotSynchronizeGoogleSheets();
        bool IsReLoanbrief(int loanbriefId);
        LogDistributionUser GetLastLogDistributionUser(int loanbriefId, int typeDistribution);
        List<LoanbriefDistributingTelelsale> WaitPushToTelesaleCTV();
        int ReCreateLoan(ReCreateLoanModel entity);
        LoanBrief LoanbriefHandleV2(string phone, string nationalCard);
        int ReCreateLoanFinish(ReCreateLoanModel entity);
        string GetVinNumber(int loanbriefId);
        TelesaleLoanbrief GetLastTelesaleLoanbrief(int loanbriefId, int telesaleId);
        List<Bank> GetBank(List<int> lstBankId);
        List<Bank> GetBankAll();
        Task<List<LoanDisbursed>> ListLoanDisbursedCar();
        Task<LoanBrief> GetLoanTopupHandle(int custommerId);
        Task<bool> RemoveIsCanTopup(int productId);
        List<LoanBrief> WaitingLendingOnline();
        List<LoanbriefDistributingTelelsale> WaitDistributingToTelesale();
        List<LoanbriefDistributingTelelsale> ListLoanbriefAdvice(int systeamUserId);
        List<LoanBrief> GetLoanGoogleClickNotSynchronizeGoogleSheets();
        LoanBriefBasicInfo GetLoanBriefBasic(int loanBriefId);
        bool CheckLoanBriefTopupProcessing(int loanBriefId);
        bool CheckLoanBriefDPDProcessing(int loanBriefId);
        int CreateLoanTopup(ReCreateLoanModel entity);
        bool UpdateLoanDevice(int loanBriefId, int deviceStatus, string contractGinno);
        LoanBrief GetAllById(int LoanbriefId);
        LoanBriefInfoTopup LoanbriefInfoTopupByLmsLoanId(int lmsLoanId);
        int GetLoanStatus(int loanbriefId);
        bool UpdateLoanTopup(int loanBriefId, decimal loanAmountExpertiseAi, decimal loanAmountExpertiseLast,
                                                 decimal loanAmount, int typeRemarketing);
        bool RemoveTopup(int loanBriefId);
        List<int> GetLoanBriefOtherLoanFast(List<int> lstLmsLoanId);
        bool UpdateAllIsCanDpd();
        LoanBriefInfoCreditScoring LoanbriefInfoCreditScoring(int LoanbriefId);
        List<LoanbriefDistributingTelelsale> WaitPushToTelesaleAdviceCTV();

        List<int> AllLoanBriefIdHasTopupProcessing();
        List<int> AllLoanBriefIdDPDProcessing();
        List<PushLoanCancelToPartnerDetail> ListLoanbriefCancel(int startLoanbriefId, List<int> listDistrictId);
        List<LoanBriefInfoForTopup> GetLoanbriefFromLmsLoanId(List<int> lmsLoanIds);
        List<string> ListNationalTopupDisbursed();
        bool UpdateSynchorizeFinance(short SynchorizeFinance, List<int> loanBriefIds);
        List<LoanBriefInfoForTopup> GetLoanCanTopupOnTopup();
        List<LoanBriefInfoCreditScoring> ListReGetCreditScoring();
    }
    public class LoanBriefServices : ILoanBriefService, IDisposable
    {
        private readonly IUnitOfWork _unitOfWork;
        protected IConfiguration _baseConfig;
        protected DbContextOptions<LOSContext> _options;
        public LoanBriefServices()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(_baseConfig.GetConnectionString("LOSDatabase"));
            _options = optionsBuilder.Options;
            var db = new LOSContext(_options);
            _unitOfWork = new UnitOfWork(db);
        }


        public LoanBriefJob GetLoanbriefJob(int loanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefJobRepository.GetById(loanbriefId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public LoanBriefResident GetLoanBriefResident(int loanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefResidentRepository.GetById(loanbriefId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LoanBrief> WaitPushToHub()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => x.Status == (int)EnumLoanStatus.HUB_LOAN_DISTRIBUTING && x.InProcess != (int)EnumInProcess.Process, null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LeadScoreInfoDetail> WaitGetLeadScore()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository
                    .Query(x => x.Status == (int)EnumLoanStatus.HUB_LOAN_DISTRIBUTING && x.InProcess != (int)EnumInProcess.Process, null, false, null)
                    .Select(LeadScoreInfoDetail.ProjectionDetail).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// lấy ra danh sách đơn chia về cho hub
        /// Hoặc đơn oto ở trạng thái chờ cht chia đơn và có cvkd
        /// </summary>
        /// <returns></returns>
        public List<LoanBrief> HubDistributing()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => (x.Status == (int)EnumLoanStatus.HUB_LOAN_DISTRIBUTING
                                    || (x.Status == (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING  //Lấy ra những đơn có trạng thái chờ cht chia đơn và có hubId
                                            && x.ProductId == (int)EnumProductCredit.OtoCreditType_CC && x.HubEmployeeId > 0))
                && (x.PipelineState == 30 || x.PipelineState == 20) && x.InProcess != (int)EnumInProcess.Process, null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LoanBriefVerifyDetail> WaitGetInfoFromAI()
        {
            try
            {
                //lấy ra d/s các đơn chờ xử lý
                //Chưa có bản ghi gọi sang bên AI
                return _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.WAIT_GET_RULE_INFO_AI && x.InProcess != (int)EnumInProcess.Process, null, false, x => x.LogLoanInfoAi)
                    .Where(x => !x.LogLoanInfoAi.Any(k => k.ServiceType == (int)ServiceTypeAI.RuleCheck))
                    .Select(LoanBriefVerifyDetail.ProjectionDetail).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public LoanBriefVerifyDetail LoanbriefInfoFromAI(int LoanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanbriefId && x.InProcess != (int)EnumInProcess.Process, null, false)
                    .Select(LoanBriefVerifyDetail.ProjectionDetail).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public LoanBriefInfo LoanbriefInfo(int LoanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanbriefId && x.InProcess != (int)EnumInProcess.Process, null, false).Select(LoanBriefInfo.ProjectionDetail).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public LoanBriefInfoCreditScoring LoanbriefInfoCreditScoring(int LoanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanbriefId && x.InProcess != (int)EnumInProcess.Process, null, false).Select(LoanBriefInfoCreditScoring.ProjectionDetail).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public LoanBrief GetById(int LoanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanbriefId && x.InProcess != (int)EnumInProcess.Process, null, false).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public LoanBriefInfoLog LoanbriefInfoPushLog(int LoanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanbriefId && x.InProcess != (int)EnumInProcess.Process, null, false).Select(LoanBriefInfoLog.ProjectionLogDetail).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LogRequestAi> AllRequestAI()
        {
            try
            {
                //lấy ra d/s các request chưa excuted và Type != LeadScore
                return _unitOfWork.LogRequestAiRepository.Query(x => x.IsExcuted != 1, null, false).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool ExcutedRequestAI(int id, int isExcuted = (int)EnumLogLoanInfoAiExcuted.HasResult)
        {
            try
            {
                var entity = _unitOfWork.LogRequestAiRepository.Update(x => x.Id == id, x => new LogRequestAi()
                {
                    IsExcuted = isExcuted
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception ExcutedRequestAI: ", ex.ToString());
            }
            return false;
        }
        public void DoneSendRequestLocation(int LoanBriefId, string message)
        {
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    LabelRequestLocation = message
                });
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception DoneSendRequestLocation: ", ex.ToString());
            }
        }

        public bool PushToPipeline(int LoanBriefId, int ActionState)
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    PipelineState = EnumPipelineState.AUTOMATIC_PROCESSED.GetHashCode(),
                    ActionState = ActionState,
                    InProcess = EnumInProcess.Process.GetHashCode(),
                    AddedToQueue = false
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool UpdateHub(int LoanBriefId, int HubId)
        {
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    HubId = HubId
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }
        public bool UpdateRangeScore(int LoanBriefId, int RangeScore)
        {
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    ScoreRange = RangeScore
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool UpdateResultRefphone(int LoanBriefId, string Phone, int call_rate, int call_duration)
        {
            try
            {
                if (_unitOfWork.LoanBriefRelationshipRepository.Any(x => x.LoanBriefId == LoanBriefId && x.Phone == Phone))
                {
                    _unitOfWork.LoanBriefRelationshipRepository.Update(x => x.LoanBriefId == LoanBriefId && x.Phone == Phone, x => new LoanBriefRelationship()
                    {
                        RefPhoneCallRate = call_rate,
                        RefPhoneDurationRate = call_duration
                    });
                    _unitOfWork.Save();
                    return true;
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool AddNote(LoanBriefNote entity)
        {
            try
            {
                _unitOfWork.LoanBriefNoteRepository.Insert(entity);
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;

        }

        public List<LoanBrief> WaitPushToTDHS()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_LOAN_DISTRIBUTING
                && x.InProcess != (int)EnumInProcess.Process, null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LoanBrief> WaitHubHandle()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => (x.Status == (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING
                || x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW) && !x.FirstTimeHubFeedBack.HasValue
                && x.InProcess != (int)EnumInProcess.Process, null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LoanBrief> WaitStaffHubHandle()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW && !x.FirstTimeStaffHubFeedback.HasValue
                && x.TelesalesPushAt.HasValue && DateTime.Now > x.TelesalesPushAt.Value.AddMinutes(15)
                && x.InProcess != (int)EnumInProcess.Process, null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public List<LoanBrief> WaitLenderCareHandle()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => x.Status == (int)EnumLoanStatus.LENDER_LOAN_DISTRIBUTING
                && x.InProcess != (int)EnumInProcess.Process, null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LoanbriefDistributingTelelsale> WaitPushToTelesale()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => x.ProvinceId > 0 && x.Status == (int)EnumLoanStatus.TELESALE_ADVICE
                   && (x.BoundTelesaleId == (int)EnumUser.spt_System)
                   && x.InProcess != (int)EnumInProcess.Process, null, false).Select(x => new LoanbriefDistributingTelelsale()
                   {
                       LoanBriefId = x.LoanBriefId,
                       CreatedTime = x.CreatedTime,
                       BoundTelesaleId = x.BoundTelesaleId,
                       CreateBy = x.CreateBy,
                       HubId = x.HubId,
                       Status = x.Status,
                       PlatformType = x.PlatformType,
                       ProvinceId = x.ProvinceId,
                       ProductId = x.ProductId,
                       DistrictId = x.DistrictId,
                       TeamTelesalesId = x.TeamTelesalesId
                   }).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateTDHS(int LoanBriefId, int userId)
        {
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    CoordinatorUserId = userId
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool DistributingTelesale(int LoanBriefId, int telesaleId, int? TelesaleTeam = 0)
        {
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    BoundTelesaleId = telesaleId,
                    TeamTelesalesId = TelesaleTeam
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool PushLoan(int LoanBriefId)
        {
            try
            {
                var entity = _unitOfWork.LoanBriefRepository.GetById(LoanBriefId);
                if (entity != null && entity.LoanBriefId > 0 && entity.InProcess != EnumInProcess.Process.GetHashCode())
                {
                    entity.InProcess = EnumInProcess.Process.GetHashCode();
                    entity.ActionState = EnumActionPush.Push.GetHashCode();
                    entity.PipelineState = EnumPipelineState.AUTOMATIC_PROCESSED.GetHashCode();
                    _unitOfWork.LoanBriefRepository.Update(entity);
                    _unitOfWork.Save();
                    return true;
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool CancelLoanPushPipeline(int LoanBriefId, int ReasonCancel, string Comment)
        {
            try
            {
                var entity = _unitOfWork.LoanBriefRepository.GetById(LoanBriefId);
                if (entity != null && entity.LoanBriefId > 0 && entity.InProcess != EnumInProcess.Process.GetHashCode())
                {
                    entity.InProcess = EnumInProcess.Process.GetHashCode();
                    entity.ActionState = EnumActionPush.Cancel.GetHashCode();
                    entity.PipelineState = EnumPipelineState.AUTOMATIC_PROCESSED.GetHashCode();
                    entity.ReasonCancel = ReasonCancel;
                    entity.LoanBriefComment = Comment;
                    entity.LoanBriefCancelAt = DateTime.Now;
                    _unitOfWork.LoanBriefRepository.Update(entity);
                    _unitOfWork.Save();
                    return true;
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool CancelLoan(int LoanBriefId, int ReasonCancel, string Comment)
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    Status = (int)EnumLoanStatus.CANCELED,
                    ReasonCancel = ReasonCancel,
                    LoanBriefCancelAt = DateTime.Now,
                    LoanBriefComment = Comment
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool UpdateResultRuleCheck(int LoanBriefId, string ResultRuleCheck)
        {
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    ResultRuleCheck = ResultRuleCheck
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool UpdateResultLocation(int LoanBriefId, string ResultLocationFinal)
        {
            try
            {
                var loanBrief = _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    ResultLocation = ResultLocationFinal
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool UpdateResultLocation(int LoanBriefId, string ResultLocationHome, string ResultLocationCompany)
        {
            try
            {
                var sql = new StringBuilder();
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                sql.AppendFormat("update LoanBriefResident set ResultLocation  = '{0}' where LoanBriefResidentId = {1} ", ResultLocationHome, LoanBriefId);
                sql.AppendFormat("update LoanBriefJob set ResultLocation  = '{0}' where LoanBriefJobId = {1} ", ResultLocationCompany, LoanBriefId);
                db.Execute(sql.ToString());
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }


        public bool AddLogDistributingTelesales(TelesaleLoanbrief entity)
        {
            try
            {
                _unitOfWork.TelesaleLoanbriefRepository.Insert(entity);
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;

        }

        public LogDistributionUser GetLastLogDistributionUser(int loanbriefId, int typeDistribution)
        {
            try
            {
                return _unitOfWork.LogDistributionUserRepository.Query(x => x.LoanbriefId == loanbriefId && x.TypeDistribution == typeDistribution, null, false).OrderByDescending(x => x.Id).FirstOrDefault();

            }
            catch (Exception ex)
            {
            }
            return null;

        }

        public List<LoanBrief> GetListLoanBriefOfFinanceNotSynchorize()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => x.FinanceLoanCreditId > 0
                && (x.SynchorizeFinance == (int)EnumSynchorizeFinance.Default
                || (x.SynchorizeFinance == (int)EnumSynchorizeFinance.LoanDisbursement && x.Status == (int)EnumLoanStatus.FINISH)), null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ReasonCancel GetReasonNameLoanBrief(int reasonCancelId)
        {
            try
            {
                return _unitOfWork.ReasonCancelRepository.Query(x => x.Id == reasonCancelId, null, false).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateSynchorizeFinance(short SynchorizeFinance, int LoanBriefId)
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == LoanBriefId, x => new LoanBrief()
                {
                    SynchorizeFinance = SynchorizeFinance
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }
        public bool DoneCreditScoring(int loanbriefId, decimal creditScoring, string labelScore)
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbriefId, x => new LoanBrief()
                {
                    LeadScore = creditScoring,
                    LabelScore = labelScore
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public List<LoanBrief> GetListLoanOver(int day)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => x.Status == (int)EnumLoanStatus.TELESALE_ADVICE
                && (x.ProductId == (int)EnumProductCredit.MotorCreditType_CC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                 || x.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                && x.BoundTelesaleId == 26695
                && (x.CreatedTime.Value.AddDays(day).Date <= DateTime.Now.Date) && x.InProcess != (int)EnumInProcess.Process, null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public TelesaleLoanbrief GetFirstDistributingTelesales(int loanbriefId)
        {
            try
            {
                return _unitOfWork.TelesaleLoanbriefRepository.Query(x => x.LoanBriefId == loanbriefId && x.TelesalesId != 26695, null, false).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }

        public List<LoanBrief> GetLoanBriefReasonCancelIsPushToFinance()
        {
            try
            {
                //Lấy đơn bắt đầu từ ngày 11/09/2020
                return _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.CANCELED
                && x.ReasonCancelNavigation.IsPushToFinance == true
                && (x.SynchorizeFinance != (int)EnumSynchorizeFinance.LoanCancelPushToFinance
                && x.SynchorizeFinance != (int)EnumSynchorizeFinance.LoanCancelPushToFinanceRefuse
                || x.SynchorizeFinance == null)
                && !x.FinanceLoanCreditId.HasValue
                && x.LoanBriefId > 998826, null, false).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LoanBriefInfoTopup> ListLoanTopup()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.DISBURSED
                && x.TypeLoanSupport == (int)EnumTypeLoanSupport.IsCanTopup, null, false).Select(LoanBriefInfoTopup.ProjectionDetail).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateTypeRemarketing(int loanbriefId, int typeLoanSupport)
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbriefId, x => new LoanBrief()
                {
                    TypeLoanSupport = typeLoanSupport
                });
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public LogCallApi ListCallApiTopup()
        {
            try
            {
                return _unitOfWork.LogCallApiRepository.Query(x => x.Status == 1 && x.ActionCallApi == 69, null, false).OrderByDescending(x => x.CreatedAt).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public LoanBriefInfoTopup LoanbriefInfoTopup(int LoanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanbriefId && x.InProcess != (int)EnumInProcess.Process, null, false).Select(LoanBriefInfoTopup.ProjectionDetail).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateLogCallApiTopup(int Id, int status)
        {
            try
            {
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == Id, x => new LogCallApi()
                {
                    Status = status
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool UpdateAllIsCanTopup()
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.Status == (int)EnumLoanStatus.DISBURSED && x.TypeLoanSupport == (int)EnumTypeLoanSupport.IsCanTopup, x => new LoanBrief()
                {
                    TypeLoanSupport = 0
                });
                return true;
            }

            catch (Exception ex)
            {
            }
            return false;
        }

        public LoanBriefInfoTopup LoanbriefInfoByReMKTId(int LoanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.ReMarketingLoanBriefId == LoanbriefId
                && x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp
                && x.Status != (int)EnumLoanStatus.CANCELED
                && x.Status != (int)EnumLoanStatus.FINISH, null, false).Select(LoanBriefInfoTopup.ProjectionDetail).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LoanBriefFileDetail> GetFilesByLoanId(int loanBriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanBriefId && x.Status == 1 && x.IsDeleted != 1, null, false).Select(LoanBriefFileDetail.ProjectionDetail).ToList();
            }
            catch (Exception ex)
            {

            }
            return new List<LoanBriefFileDetail>();
        }

        public List<DocumentTypeDetail> GetDocumentTypeProductId(int productId)
        {
            try
            {
                return _unitOfWork.DocumentTypeRepository.Query(x => x.ProductId == productId && x.IsEnable == 1, null, false).Select(DocumentTypeDetail.ProjectionDetail).ToList();
            }
            catch (Exception ex)
            {

            }
            return new List<DocumentTypeDetail>();
        }

        public bool UpdateIsReborrow(int loanBriefId)
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    IsReborrow = true
                });
                return true;
            }

            catch (Exception ex)
            {
            }
            return false;
        }

        public List<LoanBrief> GetListLoanBriefNoCheckQlf()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => (x.ProductId == 2 || x.ProductId == 5)
                && x.LoanBriefId >= 1051741 && (x.ValueCheckQualify == null || x.ValueCheckQualify == 0), null, false).ToList();
            }
            catch (Exception ex)
            {

            }
            return new List<LoanBrief>();
        }
        public LoanBriefQuestionScript GetLoanBriefQuestionScript(int loanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefQuestionScriptRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).FirstOrDefault();
            }
            catch (Exception ex)
            {
            }
            return new LoanBriefQuestionScript();
        }
        public LoanBriefJob GetLoanBriefJob(int loanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefJobRepository.Query(x => x.LoanBriefJobId == loanbriefId, null, false).FirstOrDefault();
            }
            catch (Exception ex)
            {
            }
            return new LoanBriefJob();
        }
        public District GetDistrictIsApply(int districtId)
        {
            try
            {
                return _unitOfWork.DistrictRepository.Query(x => x.DistrictId == districtId, null, false).FirstOrDefault();
            }
            catch (Exception ex)
            {
            }
            return new District();
        }
        public List<LoanBriefFiles> GetLoanBriefFiles(int loanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanbriefId && x.Status == 1 && x.IsDeleted != 1 && x.UserId == 0, null, false).ToList();
            }
            catch (Exception ex)
            {

            }
            return new List<LoanBriefFiles>();
        }
        public bool UpdateValueQualify(int loanBriefId, int valueCheckQualify)
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    ValueCheckQualify = valueCheckQualify
                });
                return true;
            }

            catch (Exception ex)
            {
            }
            return false;
        }

        public bool AddJsonInfoFamilyKalapa(int loanbriefId, string json)
        {
            try
            {
                _unitOfWork.LoanBriefHouseholdRepository.Update(x => x.LoanBriefHouseholdId == loanbriefId, x => new LoanBriefHousehold()
                {
                    JsonInfoFamilyKalapa = json
                });
                return true;
            }

            catch (Exception ex)
            {
            }
            return false;
        }
        /// <summary>
        /// Danh sách đơn vay xe máy đã tất toán
        /// </summary>
        /// <returns></returns>
        public List<LoanBrief> GetListLoanFinish(string productIds, int limitedDay)
        {
            try
            {
                var sql = new StringBuilder();
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                //sql.Append($"select LoanBriefId, NationalCard, Phone, FullName from LoanBrief(nolock) where Status = 110 and ProductId in ({productIds}) and ProvinceId in (1,79,27,30,26) and NationalCard is not null order by FinishAt desc");
                sql.AppendLine($"select l.LoanBriefId, l.NationalCard, l.Phone, l.FullName ");
                sql.AppendLine($"from LoanBrief(nolock) l ");
                sql.AppendLine($"inner join LoanBriefProperty(nolock) lp on lp.LoanBriefPropertyId = l.LoanBriefId ");
                sql.AppendLine($"where l.Status = 110 ");
                sql.AppendLine($"and l.ProductId in ({productIds}) ");
                sql.AppendLine($"and lp.ProductId > 0 ");
                sql.AppendLine($"and isnull(l.NationalCard, '') != '' ");
                sql.AppendLine($"and l.LoanBriefId not in (select LoanbriefId from LogReLoanbrief(nolock) where DATEDIFF(DAY, CreatedAt, GETDATE()) < {limitedDay} ) ");
                sql.AppendLine($"and l.NationalCard not in (select NationalCard from LoanBrief(nolock) where isnull(NationalCard, '') != '' and [Status] not in (99, 110, 100)) ");
                sql.AppendLine($"order by l.LoanBriefId ");
                return db.Query<LoanBrief>(sql.ToString());
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        //kiểm tra xem có đơn vay đang xử lý hay không
        public bool LoanbriefHandle(string phone, string nationalCard)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Any(x => x.Status != (int)EnumLoanStatus.FINISH
                  && x.Status != (int)EnumLoanStatus.CANCELED && (x.Phone == phone || (!string.IsNullOrEmpty(nationalCard) && x.NationalCard == nationalCard)));
            }
            catch
            {
                return false;
            }

        }



        public bool LoanbriefHandleCar(string phone, string nationalCard)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Any(x => x.Status != (int)EnumLoanStatus.FINISH
                  && x.Status != (int)EnumLoanStatus.CANCELED
                  && (x.ProductId == (int)EnumProductCredit.OtoCreditType_CC || x.ProductId == (int)EnumProductCredit.OtoCreditType_KCC
                  || x.ProductId == (int)EnumProductCredit.CamotoCreditType)
                  && (x.Phone == phone || (!string.IsNullOrEmpty(nationalCard) && x.NationalCard == nationalCard)));
            }
            catch
            {
                return false;
            }

        }

        public int ReCreateLoanFinish(int LoanBriefId, bool isReborrow = false)
        {
            //Get Info LoanOld
            try
            {
                var loanOld = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId, null, false).Select(LoanBriefInitDetail.ProjectionViewDetail).FirstOrDefault();
                if (loanOld != null)
                {

                    //Tạo đơn vay mới
                    var obj = new LoanBrief()
                    {
                        ProductId = loanOld.ProductId,
                        CustomerId = loanOld.CustomerId,
                        RateTypeId = loanOld.RateTypeId,
                        FullName = loanOld.FullName,
                        Phone = loanOld.Phone,
                        Dob = loanOld.Dob,
                        Gender = loanOld.Gender,
                        NationalCard = loanOld.NationalCard,
                        NationalCardDate = loanOld.NationalCardDate,
                        NationCardPlace = loanOld.NationCardPlace,
                        LoanAmount = loanOld.LoanAmount,
                        LoanAmountFirst = loanOld.LoanAmount,
                        LoanTime = 12,
                        FromDate = DateTime.Now,
                        ToDate = DateTime.Now.AddMonths(12),
                        LoanRate = loanOld.LoanRate,
                        ProvinceId = loanOld.ProvinceId,
                        DistrictId = loanOld.DistrictId,
                        WardId = loanOld.WardId,
                        UtmSource = "rmkt_loan_finish",
                        CreatedTime = DateTime.Now,
                        Status = (int)EnumLoanStatus.INIT,
                        BoundTelesaleId = (int)EnumUser.follow,
                        TeamTelesalesId = (int)EnumTeamTelesales.Other,
                        ReceivingMoneyType = loanOld.ReceivingMoneyType,
                        BankId = loanOld.BankId,
                        BankAccountName = loanOld.BankAccountName,
                        BankAccountNumber = loanOld.BankAccountNumber,
                        BankCardNumber = loanOld.BankCardNumber,
                        Frequency = loanOld.Frequency,
                        TypeLoanBrief = loanOld.TypeLoanBrief,
                        BuyInsurenceCustomer = loanOld.BuyInsurenceCustomer,
                        LoanPurpose = loanOld.LoanPurpose,
                        ReMarketingLoanBriefId = loanOld.LoanBriefId,
                        RateMoney = loanOld.RateMoney.HasValue ? loanOld.RateMoney.Value : 2700,
                        RatePercent = loanOld.RatePercent.HasValue ? loanOld.RatePercent.Value : 98.55,
                        IsReborrow = isReborrow,
                        Passport = loanOld.Passport,
                        TypeRemarketing = (int)EnumTypeRemarketing.IsRemarketing,
                        PlatformType = (int)EnumPlatformType.ReMarketing,
                        CreateBy = (int)EnumUser.follow,
                        IsHeadOffice = true
                    };
                    //insert to db
                    _unitOfWork.LoanBriefRepository.Insert(obj);
                    _unitOfWork.Save();
                    var LoanBriefIdNew = obj.LoanBriefId;
                    //Thêm thông tin địa chỉ đang ở vào bảng LoanBriefResident
                    if (loanOld.LoanBriefResident != null)
                    {
                        loanOld.LoanBriefResident.LoanBriefResidentId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefResidentRepository.Insert(loanOld.LoanBriefResident);
                    }
                    //Thêm thông tin địa chỉ hộ khẩu LoanBriefHousehold
                    if (loanOld.LoanBriefHousehold != null)
                    {
                        loanOld.LoanBriefHousehold.LoanBriefHouseholdId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefHouseholdRepository.Insert(loanOld.LoanBriefHousehold);
                    }
                    //Thêm thông tin công việc
                    if (loanOld.LoanBriefJob != null)
                    {
                        loanOld.LoanBriefJob.LoanBriefJobId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefJobRepository.Insert(loanOld.LoanBriefJob);
                    }
                    //Thêm thông tin tài sản
                    if (loanOld.LoanBriefProperty != null)
                    {
                        loanOld.LoanBriefProperty.LoanBriefPropertyId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefPropertyRepository.Insert(loanOld.LoanBriefProperty);
                    }

                    //Thêm tin đồng nghiệp
                    if (loanOld.LoanBriefRelationship != null && loanOld.LoanBriefRelationship.Count > 0)
                    {
                        foreach (var item in loanOld.LoanBriefRelationship)
                        {
                            item.Id = 0;
                            if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                            {
                                item.LoanBriefId = LoanBriefIdNew;
                                _unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                            }
                        }
                    }

                    _unitOfWork.Save();
                    return LoanBriefIdNew;
                }
                return 0;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public int ReCreateLoanFinishCar(int LoanBriefId)
        {
            //Get Info LoanOld
            try
            {
                var loanOld = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId, null, false).Select(LoanBriefInitDetail.ProjectionViewDetail).FirstOrDefault();
                if (loanOld != null)
                {

                    //Tạo đơn vay mới
                    var obj = new LoanBrief()
                    {
                        ProductId = (int)EnumProductCredit.OtoCreditType_CC,
                        CustomerId = loanOld.CustomerId,
                        RateTypeId = loanOld.RateTypeId,
                        FullName = loanOld.FullName,
                        Phone = loanOld.Phone,
                        Dob = loanOld.Dob,
                        Gender = loanOld.Gender,
                        NationalCard = loanOld.NationalCard,
                        NationalCardDate = loanOld.NationalCardDate,
                        NationCardPlace = loanOld.NationCardPlace,
                        LoanAmount = loanOld.LoanAmount,
                        LoanAmountFirst = loanOld.LoanAmount,
                        LoanTime = 12,
                        FromDate = DateTime.Now,
                        ToDate = DateTime.Now.AddMonths(12),
                        LoanRate = loanOld.LoanRate,
                        ProvinceId = loanOld.ProvinceId,
                        DistrictId = loanOld.DistrictId,
                        WardId = loanOld.WardId,
                        UtmSource = "rmkt_loan_finish",
                        CreatedTime = DateTime.Now,
                        Status = (int)EnumLoanStatus.INIT,
                        ReceivingMoneyType = loanOld.ReceivingMoneyType,
                        BankId = loanOld.BankId,
                        BankAccountName = loanOld.BankAccountName,
                        BankAccountNumber = loanOld.BankAccountNumber,
                        BankCardNumber = loanOld.BankCardNumber,
                        Frequency = loanOld.Frequency,
                        //IsHeadOffice = ,
                        TypeLoanBrief = loanOld.TypeLoanBrief,
                        BuyInsurenceCustomer = loanOld.BuyInsurenceCustomer,
                        LoanPurpose = loanOld.LoanPurpose,
                        ReMarketingLoanBriefId = loanOld.LoanBriefId,
                        RateMoney = loanOld.RateMoney.HasValue ? loanOld.RateMoney.Value : 2700,
                        RatePercent = loanOld.RatePercent.HasValue ? loanOld.RatePercent.Value : 98.55,
                        Passport = loanOld.Passport,
                        TypeRemarketing = (int)EnumTypeRemarketing.IsRemarketing,
                        PlatformType = (int)EnumPlatformType.ReMarketing,
                        CreateBy = (int)EnumUser.linhtq,
                    };
                    //insert to db
                    _unitOfWork.LoanBriefRepository.Insert(obj);
                    _unitOfWork.Save();
                    var LoanBriefIdNew = obj.LoanBriefId;
                    //Thêm thông tin địa chỉ đang ở vào bảng LoanBriefResident
                    if (loanOld.LoanBriefResident != null)
                    {
                        loanOld.LoanBriefResident.LoanBriefResidentId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefResidentRepository.Insert(loanOld.LoanBriefResident);
                    }
                    //Thêm thông tin địa chỉ hộ khẩu LoanBriefHousehold
                    if (loanOld.LoanBriefHousehold != null)
                    {
                        loanOld.LoanBriefHousehold.LoanBriefHouseholdId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefHouseholdRepository.Insert(loanOld.LoanBriefHousehold);
                    }
                    //Thêm thông tin công việc
                    if (loanOld.LoanBriefJob != null)
                    {
                        loanOld.LoanBriefJob.LoanBriefJobId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefJobRepository.Insert(loanOld.LoanBriefJob);
                    }
                    //Thêm thông tin tài sản
                    if (loanOld.LoanBriefProperty != null)
                    {
                        loanOld.LoanBriefProperty.LoanBriefPropertyId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefPropertyRepository.Insert(loanOld.LoanBriefProperty);
                    }

                    //Thêm tin đồng nghiệp
                    if (loanOld.LoanBriefRelationship != null && loanOld.LoanBriefRelationship.Count > 0)
                    {
                        foreach (var item in loanOld.LoanBriefRelationship)
                        {
                            item.Id = 0;
                            if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                            {
                                item.LoanBriefId = LoanBriefIdNew;
                                _unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                            }
                        }
                    }

                    _unitOfWork.Save();
                    return LoanBriefIdNew;
                }
                return 0;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public List<LoanBrief> GetLoanDisbursementNotSynchronizeGoogleSheets()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.SynchorizeFinance != (int)EnumSynchorizeFinance.SynchorizeLoanDisbursementToGoogleSheets
                && x.Status == (int)EnumLoanStatus.DISBURSED && x.GoogleClickId != null && x.GoogleClickId != "", null, false).ToList();
            }
            catch (Exception ex)
            {

            }
            return new List<LoanBrief>();
        }
        public bool IsReLoanbrief(int loanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Any(x => x.ReMarketingLoanBriefId == loanbriefId && x.UtmSource == "rmkt_loan_finish");
            }
            catch
            {
                return false;
            }

        }

        public List<LoanbriefDistributingTelelsale> WaitPushToTelesaleCTV()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => x.ProvinceId > 0 && (x.Status == (int)EnumLoanStatus.SYSTEM_TELESALE_DISTRIBUTING
                && (x.BoundTelesaleId == (int)EnumUser.ctv_autocall || x.BoundTelesaleId == (int)EnumUser.autocall_coldlead || x.BoundTelesaleId == (int)EnumUser.recare))
               && x.InProcess != (int)EnumInProcess.Process, null, false).Select(x => new LoanbriefDistributingTelelsale()
               {
                   LoanBriefId = x.LoanBriefId,
                   CreatedTime = x.CreatedTime,
                   BoundTelesaleId = x.BoundTelesaleId,
                   CreateBy = x.CreateBy,
                   HubId = x.HubId,
                   Status = x.Status,
                   PlatformType = x.PlatformType,
                   ProvinceId = x.ProvinceId,
                   ProductId = x.ProductId
               }).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LoanbriefDistributingTelelsale> WaitPushToTelesaleAdviceCTV()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => x.ProvinceId > 0 && (x.Status == (int)EnumLoanStatus.TELESALE_ADVICE
                && (x.BoundTelesaleId == (int)EnumUser.ctv_autocall || x.BoundTelesaleId == (int)EnumUser.autocall_coldlead || x.BoundTelesaleId == (int)EnumUser.recare))
               && x.InProcess != (int)EnumInProcess.Process, null, false).Select(x => new LoanbriefDistributingTelelsale()
               {
                   LoanBriefId = x.LoanBriefId,
                   CreatedTime = x.CreatedTime,
                   BoundTelesaleId = x.BoundTelesaleId,
                   CreateBy = x.CreateBy,
                   HubId = x.HubId,
                   Status = x.Status,
                   PlatformType = x.PlatformType,
                   ProvinceId = x.ProvinceId,
                   ProductId = x.ProductId
               }).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int ReCreateLoan(ReCreateLoanModel entity)
        {
            //Get Info LoanOld
            try
            {
                var loanOld = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(LoanBriefInitDetail.ProjectionViewDetail).FirstOrDefault();
                if (loanOld != null)
                {
                    var rateTypeId = loanOld.RateTypeId;
                    if (rateTypeId == (int)EnumRateType.RateMonthADay)
                        rateTypeId = (int)EnumRateType.TatToanCuoiKy;
                    else if (rateTypeId == (int)EnumRateType.RateAmortization30Days)
                        rateTypeId = (int)EnumRateType.DuNoGiamDan;
                    //Tạo đơn vay mới
                    var obj = new LoanBrief()
                    {
                        ProductId = loanOld.ProductId,
                        CustomerId = loanOld.CustomerId,
                        RateTypeId = rateTypeId,
                        FullName = loanOld.FullName,
                        Phone = loanOld.Phone,
                        Dob = loanOld.Dob,
                        Gender = loanOld.Gender,
                        NationalCard = loanOld.NationalCard,
                        NationalCardDate = loanOld.NationalCardDate,
                        NationCardPlace = loanOld.NationCardPlace,
                        LoanAmount = loanOld.LoanAmount,
                        LoanAmountFirst = loanOld.LoanAmount,
                        LoanTime = 12,
                        FromDate = DateTime.Now,
                        ToDate = DateTime.Now.AddMonths(12),
                        LoanRate = loanOld.LoanRate,
                        ProvinceId = loanOld.ProvinceId,
                        DistrictId = loanOld.DistrictId,
                        WardId = loanOld.WardId,
                        UtmSource = entity.UtmSource,
                        CreatedTime = DateTime.Now,
                        Status = (int)EnumLoanStatus.INIT,
                        ReceivingMoneyType = loanOld.ReceivingMoneyType,
                        BankId = loanOld.BankId,
                        BankAccountName = loanOld.BankAccountName,
                        BankAccountNumber = loanOld.BankAccountNumber,
                        BankCardNumber = loanOld.BankCardNumber,
                        Frequency = loanOld.Frequency,
                        //IsHeadOffice = ,
                        TypeLoanBrief = loanOld.TypeLoanBrief,
                        BuyInsurenceCustomer = loanOld.BuyInsurenceCustomer,
                        LoanPurpose = loanOld.LoanPurpose,
                        ReMarketingLoanBriefId = loanOld.LoanBriefId,
                        RateMoney = loanOld.RateMoney.HasValue ? loanOld.RateMoney.Value : 2700,
                        RatePercent = loanOld.RatePercent.HasValue ? loanOld.RatePercent.Value : 98.55,
                        //IsReborrow = true,
                        Passport = loanOld.Passport,
                        TypeRemarketing = entity.TypeRemarketing,
                        PlatformType = (int)EnumPlatformType.ReMarketing,
                        IsHeadOffice = !entity.HubId.HasValue,
                        BoundTelesaleId = entity.TelesaleId ?? 0,
                        HubId = entity.HubId,
                        CreateBy = entity.HubId > 0 ? entity.CreatedBy : 0,
                        TeamTelesalesId = entity.TeamTelesaleId
                    };
                    //if (obj.BoundTelesaleId == (int)EnumUser.recare) obj.BoundTelesaleId = 0;
                    //insert to db
                    _unitOfWork.LoanBriefRepository.Insert(obj);
                    _unitOfWork.Save();
                    var LoanBriefIdNew = obj.LoanBriefId;
                    //Thêm thông tin địa chỉ đang ở vào bảng LoanBriefResident
                    if (loanOld.LoanBriefResident != null)
                    {
                        loanOld.LoanBriefResident.LoanBriefResidentId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefResidentRepository.Insert(loanOld.LoanBriefResident);
                    }
                    //Thêm thông tin địa chỉ hộ khẩu LoanBriefHousehold
                    if (loanOld.LoanBriefHousehold != null)
                    {
                        loanOld.LoanBriefHousehold.LoanBriefHouseholdId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefHouseholdRepository.Insert(loanOld.LoanBriefHousehold);
                    }
                    //Thêm thông tin công việc
                    if (loanOld.LoanBriefJob != null)
                    {
                        loanOld.LoanBriefJob.LoanBriefJobId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefJobRepository.Insert(loanOld.LoanBriefJob);
                    }
                    //Thêm thông tin tài sản
                    if (loanOld.LoanBriefProperty != null)
                    {
                        loanOld.LoanBriefProperty.LoanBriefPropertyId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefPropertyRepository.Insert(loanOld.LoanBriefProperty);
                    }

                    //Thêm tin đồng nghiệp
                    if (loanOld.LoanBriefRelationship != null && loanOld.LoanBriefRelationship.Count > 0)
                    {
                        int dem = 0;
                        foreach (var item in loanOld.LoanBriefRelationship)
                        {
                            if (dem >= 2) continue;
                            item.Id = 0;
                            if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                            {
                                item.LoanBriefId = LoanBriefIdNew;
                                _unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                                dem++;
                            }
                        }
                    }

                    _unitOfWork.Save();
                    return LoanBriefIdNew;
                }
                return 0;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public LoanBrief LoanbriefHandleV2(string phone, string nationalCard)
        {
            try
            {
                var sql = new StringBuilder();
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                sql.AppendLine("select top 1 LoanBriefId, Phone, FullName, NationalCard from LoanBrief(nolock)  ");
                sql.AppendLine("where Status not in (99, 110) ");
                if (string.IsNullOrEmpty(nationalCard))
                    sql.AppendLine($"and (Phone = '{phone}')");
                else
                    sql.AppendLine($"and (Phone = '{phone}' or (isnull(NationalCard,'') != '' and NationalCard = '{nationalCard}'))");
                return db.QueryFirstOrDefault<LoanBrief>(sql.ToString());
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public int ReCreateLoanFinish(ReCreateLoanModel entity)
        {
            //Get Info LoanOld
            try
            {
                var loanOld = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false).Select(LoanBriefInitDetail.ProjectionViewDetail).FirstOrDefault();
                if (loanOld != null)
                {
                    var rateTypeId = loanOld.RateTypeId;
                    if (rateTypeId == (int)EnumRateType.RateMonthADay)
                        rateTypeId = (int)EnumRateType.TatToanCuoiKy;
                    else if (rateTypeId == (int)EnumRateType.RateAmortization30Days)
                        rateTypeId = (int)EnumRateType.DuNoGiamDan;
                    //Tạo đơn vay mới
                    var obj = new LoanBrief()
                    {
                        ProductId = loanOld.ProductId,
                        CustomerId = loanOld.CustomerId,
                        RateTypeId = rateTypeId,
                        FullName = loanOld.FullName,
                        Phone = loanOld.Phone,
                        Dob = loanOld.Dob,
                        Gender = loanOld.Gender,
                        NationalCard = loanOld.NationalCard,
                        NationalCardDate = loanOld.NationalCardDate,
                        NationCardPlace = loanOld.NationCardPlace,
                        LoanAmount = loanOld.LoanAmount.GetValueOrDefault(10000000),
                        LoanAmountFirst = loanOld.LoanAmount.GetValueOrDefault(10000000),
                        LoanTime = 12,
                        FromDate = DateTime.Now,
                        ToDate = DateTime.Now.AddMonths(12),
                        LoanRate = loanOld.LoanRate,
                        ProvinceId = loanOld.ProvinceId,
                        DistrictId = loanOld.DistrictId,
                        WardId = loanOld.WardId,
                        UtmSource = entity.UtmSource,
                        CreatedTime = DateTime.Now,
                        Status = (int)EnumLoanStatus.INIT,
                        BoundTelesaleId = entity.TelesaleId,
                        TeamTelesalesId = (int)EnumTeamTelesales.Other,
                        ReceivingMoneyType = loanOld.ReceivingMoneyType,
                        BankId = loanOld.BankId,
                        BankAccountName = loanOld.BankAccountName,
                        BankAccountNumber = loanOld.BankAccountNumber,
                        BankCardNumber = loanOld.BankCardNumber,
                        Frequency = loanOld.Frequency,
                        TypeLoanBrief = loanOld.TypeLoanBrief,
                        BuyInsurenceCustomer = loanOld.BuyInsurenceCustomer,
                        LoanPurpose = loanOld.LoanPurpose,
                        ReMarketingLoanBriefId = loanOld.LoanBriefId,
                        RateMoney = loanOld.RateMoney.HasValue ? loanOld.RateMoney.Value : 2700,
                        RatePercent = loanOld.RatePercent.HasValue ? loanOld.RatePercent.Value : 98.55,
                        IsReborrow = entity.ReBorrow,
                        Passport = loanOld.Passport,
                        TypeRemarketing = (int)EnumTypeRemarketing.IsRemarketing,
                        PlatformType = (int)EnumPlatformType.ReMarketing,
                        CreateBy = entity.CreatedBy,
                        IsHeadOffice = true
                    };
                    //insert to db
                    _unitOfWork.LoanBriefRepository.Insert(obj);
                    _unitOfWork.Save();
                    var LoanBriefIdNew = obj.LoanBriefId;
                    //Thêm thông tin địa chỉ đang ở vào bảng LoanBriefResident
                    if (loanOld.LoanBriefResident != null)
                    {
                        loanOld.LoanBriefResident.LoanBriefResidentId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefResidentRepository.Insert(loanOld.LoanBriefResident);
                    }
                    //Thêm thông tin địa chỉ hộ khẩu LoanBriefHousehold
                    if (loanOld.LoanBriefHousehold != null)
                    {
                        loanOld.LoanBriefHousehold.LoanBriefHouseholdId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefHouseholdRepository.Insert(loanOld.LoanBriefHousehold);
                    }
                    //Thêm thông tin công việc
                    if (loanOld.LoanBriefJob != null)
                    {
                        loanOld.LoanBriefJob.LoanBriefJobId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefJobRepository.Insert(loanOld.LoanBriefJob);
                    }
                    //Thêm thông tin tài sản
                    if (loanOld.LoanBriefProperty != null)
                    {
                        loanOld.LoanBriefProperty.LoanBriefPropertyId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefPropertyRepository.Insert(loanOld.LoanBriefProperty);
                    }

                    //Thêm tin đồng nghiệp
                    if (loanOld.LoanBriefRelationship != null && loanOld.LoanBriefRelationship.Count > 0)
                    {
                        int dem = 0;
                        foreach (var item in loanOld.LoanBriefRelationship.OrderByDescending(x => x.Id))
                        {
                            if (dem >= 2) continue;
                            item.Id = 0;
                            if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                            {
                                item.LoanBriefId = LoanBriefIdNew;
                                _unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                                dem++;
                            }
                        }
                    }

                    //xử lý Files
                    if (entity.ReBorrow.GetValueOrDefault(false) && loanOld.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || loanOld.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                    {

                        List<int> lstDocumentId = new List<int>();
                        lstDocumentId.Add((int)EnumDocumentType.CMND_CCCD);
                        lstDocumentId.Add((int)EnumDocumentType.Zalo);
                        lstDocumentId.Add((int)EnumDocumentType.HoChieu_CVKD_Chup);
                        lstDocumentId.Add((int)EnumDocumentType.SDT_ThamChieu);
                        lstDocumentId.Add((int)EnumDocumentType.Selfie);
                        lstDocumentId.Add((int)EnumDocumentType.Video_TinNhanLuong);
                        lstDocumentId.Add((int)EnumDocumentType.HDLD_CoDauDoCongTy);
                        var files = _unitOfWork.LoanBriefFileRepository.Query(x => lstDocumentId.Contains(x.TypeId.Value)
                                    && x.LoanBriefId == loanOld.LoanBriefId
                                    && x.Status == 1 && x.IsDeleted.GetValueOrDefault(0) == 0, null, false).ToList();
                        if (files != null && files.Count() > 0)
                        {
                            foreach (var file in files)
                            {
                                _unitOfWork.LoanBriefFileRepository.Insert(new LoanBriefFiles
                                {
                                    LoanBriefId = obj.LoanBriefId,
                                    CreateAt = DateTime.Now,
                                    FilePath = file.FilePath,
                                    UserId = file.UserId,
                                    Status = file.Status,
                                    TypeFile = file.TypeFile,
                                    S3status = file.S3status,
                                    TypeId = file.TypeId,
                                    IsDeleted = file.IsDeleted,
                                    MecashId = file.MecashId,
                                    FileThumb = file.FileThumb,
                                    SourceUpload = file.SourceUpload
                                });
                            }
                        }
                    }

                    _unitOfWork.Save();
                    return LoanBriefIdNew;
                }
                return 0;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public string GetVinNumber(int loanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefPropertyRepository.Query(x => x.LoanBriefPropertyId == loanbriefId, null, false).Select(x => x.Chassis).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public TelesaleLoanbrief GetLastTelesaleLoanbrief(int loanbriefId, int telesaleId)
        {
            try
            {
                return _unitOfWork.TelesaleLoanbriefRepository.Query(x => x.LoanBriefId == loanbriefId && x.TelesalesId == telesaleId, null, false).OrderByDescending(x => x.Id).FirstOrDefault();

            }
            catch (Exception ex)
            {
            }
            return null;

        }

        public List<Bank> GetBank(List<int> lstBankId)
        {
            try
            {
                var banks = _unitOfWork.BankRepository.Query(x => lstBankId.Contains(x.BankId), null, false).ToList();

                return banks;
            }
            catch (Exception ex)
            {

                return null;
            }

        }
        public List<Bank> GetBankAll()
        {
            try
            {
                var banks = _unitOfWork.BankRepository.All().ToList();

                return banks;
            }
            catch (Exception ex)
            {

                return null;
            }

        }

        public async Task<List<LoanDisbursed>> ListLoanDisbursedCar()
        {
            try
            {
                return await _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.DISBURSED
                    && x.ProductId == (int)EnumProductCredit.OtoCreditType_CC, null, false).OrderBy(x => x.CustomerId).Select(x => new LoanDisbursed
                    {
                        LoanbriefId = x.LoanBriefId,
                        FullName = x.FullName,
                        CustomerId = x.CustomerId.GetValueOrDefault(0),
                        Phone = x.Phone,
                        NationalCard = x.NationalCard,
                        TypeRemarketing = x.TypeRemarketing,
                        TypeLoanSupport = x.TypeLoanSupport,
                    }).ToListAsync();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<bool> RemoveIsCanTopup(int productId)
        {
            try
            {
                await _unitOfWork.LoanBriefRepository.UpdateAsync(x => x.ProductId == productId && x.Status == (int)EnumLoanStatus.DISBURSED && x.TypeLoanSupport == (int)EnumTypeLoanSupport.IsCanTopup, x => new LoanBrief()
                {
                    TypeLoanSupport = null
                });
                return true;

            }
            catch (Exception ex)
            {
                return true;
            }
        }


        //lấy đơn Topup oto đang xử lý
        public async Task<LoanBrief> GetLoanTopupHandle(int custommerId)
        {
            try
            {
                return await _unitOfWork.LoanBriefRepository.Query(x => x.CustomerId == custommerId
                    && x.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                    && x.Status != (int)EnumLoanStatus.CANCELED
                    && x.Status != (int)EnumLoanStatus.DISBURSED
                    && x.Status != (int)EnumLoanStatus.FINISH
                    && x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp
                    , null, false).OrderBy(x => x.CustomerId).FirstOrDefaultAsync();
            }
            catch
            {
                return null;
            }
        }
        //lấy ra d/s đơn lending online chưa đủ 7 step
        public List<LoanBrief> WaitingLendingOnline()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => (x.ProductDetailId == (int)Common.Extensions.InfomationProductDetail.LO_KT1_KSHK_5tr || x.ProductDetailId == (int)Common.Extensions.InfomationProductDetail.LO_KT3_KTN_5tr
                || x.ProductDetailId == (int)Common.Extensions.InfomationProductDetail.LO_KT1_12tr || x.ProductDetailId == (int)Common.Extensions.InfomationProductDetail.LO_KT1_KSHK_12tr) && x.Step < 7, null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LoanbriefDistributingTelelsale> WaitDistributingToTelesale()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => x.ProvinceId > 0 && x.Status == (int)EnumLoanStatus.SYSTEM_TELESALE_DISTRIBUTING
                 && (x.BoundTelesaleId != (int)EnumUser.ctv_autocall && x.BoundTelesaleId != (int)EnumUser.autocall_coldlead && x.BoundTelesaleId != (int)EnumUser.recare)
                && x.InProcess != (int)EnumInProcess.Process, null, false).Select(x => new LoanbriefDistributingTelelsale()
                {
                    LoanBriefId = x.LoanBriefId,
                    CreatedTime = x.CreatedTime,
                    BoundTelesaleId = x.BoundTelesaleId,
                    CreateBy = x.CreateBy,
                    HubId = x.HubId,
                    Status = x.Status,
                    PlatformType = x.PlatformType,
                    ProvinceId = x.ProvinceId,
                    ProductId = x.ProductId,
                    DistrictId = x.DistrictId
                }).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LoanbriefDistributingTelelsale> ListLoanbriefAdvice(int systeamUserId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Get(x => x.ProvinceId > 0 && x.Status == (int)EnumLoanStatus.TELESALE_ADVICE
                   && x.BoundTelesaleId == systeamUserId
                   && x.InProcess != (int)EnumInProcess.Process, null, false).Select(x => new LoanbriefDistributingTelelsale()
                   {
                       LoanBriefId = x.LoanBriefId,
                       CreatedTime = x.CreatedTime,
                       BoundTelesaleId = x.BoundTelesaleId,
                       CreateBy = x.CreateBy,
                       HubId = x.HubId,
                       Status = x.Status,
                       PlatformType = x.PlatformType,
                       ProvinceId = x.ProvinceId,
                       ProductId = x.ProductId,
                       DistrictId = x.DistrictId,
                       TeamTelesalesId = x.TeamTelesalesId
                   }).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LoanBrief> GetLoanGoogleClickNotSynchronizeGoogleSheets()
        {
            try
            {
                //lấy danh sách đơn bắt đầu từ tháng 7
                return _unitOfWork.LoanBriefRepository.Query(x => x.SynchorizeFinance != (int)EnumSynchorizeFinance.SynchorizeLoanGoogleClickToGoogleSheets
                && x.SynchorizeFinance != (int)EnumSynchorizeFinance.SynchorizeLoanDisbursementToGoogleSheets
                && x.GoogleClickId != null && x.GoogleClickId != "" && x.LoanBriefId >= 1770245
                && x.Status != (int)EnumLoanStatus.DISBURSED
                && x.Status != (int)EnumLoanStatus.FINISH
                && x.ProvinceId.GetValueOrDefault(0) > 0
                && x.DistrictId.GetValueOrDefault(0) > 0, null, false).Take(1000).ToList();
            }
            catch (Exception ex)
            {
            }
            return new List<LoanBrief>();
        }

        public LoanBriefBasicInfo GetLoanBriefBasic(int loanBriefId)
        {
            return _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanBriefId, null, false).
                Select(LoanBriefBasicInfo.ProjectionViewDetail).FirstOrDefault();
        }
        public bool CheckLoanBriefTopupProcessing(int loanBriefId)
        {
            try
            {
                using (var db = new LOSContext(_options))
                {
                    return db.LoanBrief.Count(x => x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp
                    && x.ReMarketingLoanBriefId > 0 && x.ReMarketingLoanBriefId == loanBriefId
                    && x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.FINISH) > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<int> AllLoanBriefIdHasTopupProcessing()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp
                && x.ReMarketingLoanBriefId > 0 && x.Status != (int)EnumLoanStatus.CANCELED
                && x.Status != (int)EnumLoanStatus.FINISH, null, false).Select(x => x.ReMarketingLoanBriefId.Value).ToList();

            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<string> ListNationalTopupDisbursed()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp
                && x.Status == (int)EnumLoanStatus.DISBURSED, null, false).Select(x => x.NationalCard).ToList();

            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public bool CheckLoanBriefDPDProcessing(int loanBriefId)
        {
            try
            {
                using (var db = new LOSContext(_options))
                {
                    return db.LoanBrief.Count(x => x.ReMarketingLoanBriefId > 0 && x.ReMarketingLoanBriefId == loanBriefId
                                && x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.FINISH && x.Status != (int)EnumLoanStatus.DISBURSED) > 0;
                }
            }
            catch
            {
                return false;
            }
        }

        public List<int> AllLoanBriefIdDPDProcessing()
        {
            return _unitOfWork.LoanBriefRepository.Query(x => x.ReMarketingLoanBriefId > 0
            && x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.FINISH
            && x.Status != (int)EnumLoanStatus.DISBURSED, null, false).Select(x => x.ReMarketingLoanBriefId.Value).ToList();
        }
        public int CreateLoanTopup(ReCreateLoanModel entity)
        {
            //Get Info LoanOld
            try
            {
                var loanOld = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanbriefId, null, false)
                    .Select(LoanBriefInitDetail.ProjectionViewDetail).FirstOrDefault();
                if (loanOld != null)
                {
                    var isLocate = (loanOld.IsLocate == true || !string.IsNullOrEmpty(loanOld.DeviceId));
                    //Mặc định đơn topup là vay 12 tháng
                    loanOld.LoanTime = 12;
                    //Tạo đơn vay mới
                    var obj = new LoanBrief()
                    {
                        TypeRemarketing = entity.TypeRemarketing,
                        ReMarketingLoanBriefId = loanOld.LoanBriefId,
                        LoanAmount = entity.LoanAmountNew,
                        LoanAmountFirst = entity.LoanAmountNew,
                        LoanTime = loanOld.LoanTime,
                        ProductId = loanOld.ProductId,
                        CreatedTime = DateTime.Now,
                        FullName = loanOld.FullName,
                        Phone = loanOld.Phone,
                        Dob = loanOld.Dob,
                        Gender = loanOld.Gender,
                        NationalCard = loanOld.NationalCard,
                        NationalCardDate = loanOld.NationalCardDate,
                        NationCardPlace = loanOld.NationCardPlace,
                        ProvinceId = loanOld.ProvinceId,
                        DistrictId = loanOld.DistrictId,
                        WardId = loanOld.WardId,
                        CustomerId = loanOld.CustomerId,
                        TypeLoanBrief = loanOld.TypeLoanBrief,
                        RateTypeId = loanOld.RateTypeId == (int)EnumRateType.RateMonthADay ? (int)EnumRateType.TatToanCuoiKy : loanOld.RateTypeId == (int)EnumRateType.RateAmortization30Days ? (int)EnumRateType.DuNoGiamDan : loanOld.RateTypeId,
                        FromDate = DateTime.Now,
                        ToDate = DateTime.Now.AddMonths((int)loanOld.LoanTime),
                        Status = loanOld.Status,
                        ReceivingMoneyType = loanOld.ReceivingMoneyType,
                        BankId = loanOld.BankId,
                        BankAccountName = loanOld.BankAccountName,
                        BankAccountNumber = loanOld.BankAccountNumber,
                        BankCardNumber = loanOld.BankCardNumber,
                        Frequency = loanOld.Frequency,
                        BuyInsurenceCustomer = loanOld.BuyInsurenceCustomer,
                        PlatformType = loanOld.PlatformType,
                        DeviceId = loanOld.DeviceId,
                        IsLocate = isLocate,
                        RateMoney = loanOld.RateMoney.HasValue ? loanOld.RateMoney.Value : 2700,
                        RatePercent = loanOld.RatePercent.HasValue ? loanOld.RatePercent.Value : 98.55,
                        LoanAmountExpertiseAi = entity.LoanAmountExpertiseNew,
                        LoanAmountExpertiseLast = entity.LoanAmountExpertiseLastNew,
                        IsTrackingLocation = loanOld.IsTrackingLocation,
                        CreateBy = entity.CreatedBy,
                        BoundTelesaleId = entity.TelesaleId,                        
                        UtmSource = entity.UtmSource,
                        IsHeadOffice = true,
                        //HubId = loanOld.HubId,
                        //HubEmployeeId = loanOld.HubEmployeeId,
                    };
                    //insert to db
                    _unitOfWork.LoanBriefRepository.Insert(obj);
                    _unitOfWork.Save();

                    var LoanBriefIdNew = obj.LoanBriefId;

                    //nếu là đơn doanh nghiệp => lưu thông tin doanh nghiệp
                    if (loanOld.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                    {
                        loanOld.LoanBriefCompany.LoanBriefId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefCompanyRepository.Insert(loanOld.LoanBriefCompany);

                    }

                    //Thêm thông tin địa chỉ đang ở vào bảng LoanBriefResident
                    if (loanOld.LoanBriefResident != null)
                    {
                        loanOld.LoanBriefResident.LoanBriefResidentId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefResidentRepository.Insert(loanOld.LoanBriefResident);
                    }

                    //Thêm thông tin địa chỉ hộ khẩu LoanBriefHousehold
                    if (loanOld.LoanBriefHousehold != null)
                    {
                        loanOld.LoanBriefHousehold.LoanBriefHouseholdId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefHouseholdRepository.Insert(loanOld.LoanBriefHousehold);
                    }

                    //Thêm thông tin công việc
                    if (loanOld.LoanBriefJob != null)
                    {
                        loanOld.LoanBriefJob.LoanBriefJobId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefJobRepository.Insert(loanOld.LoanBriefJob);
                    }

                    //Thêm thông tin tài sản
                    if (loanOld.LoanBriefProperty != null)
                    {
                        loanOld.LoanBriefProperty.LoanBriefPropertyId = LoanBriefIdNew;
                        _unitOfWork.LoanBriefPropertyRepository.Insert(loanOld.LoanBriefProperty);
                    }

                    //Thêm tin đồng nghiệp
                    if (loanOld.LoanBriefRelationship != null && loanOld.LoanBriefRelationship.Count > 0)
                    {
                        foreach (var item in loanOld.LoanBriefRelationship)
                        {
                            item.Id = 0;
                            if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                            {
                                item.LoanBriefId = LoanBriefIdNew;
                                _unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                            }
                        }
                    }

                    //update đơn gốc không cho tạo đơn topup nữa
                    _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == obj.ReMarketingLoanBriefId, x => new LoanBrief()
                    {
                        TypeLoanSupport = 0
                    });

                    //xử lý Files
                    if (loanOld.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || loanOld.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                    {

                        List<int> lstDocumentId = new List<int>();
                        lstDocumentId.Add((int)EnumDocumentType.CMND_CCCD);
                        lstDocumentId.Add((int)EnumDocumentType.Zalo);
                        lstDocumentId.Add((int)EnumDocumentType.HoChieu_CVKD_Chup);
                        lstDocumentId.Add((int)EnumDocumentType.SDT_ThamChieu);
                        lstDocumentId.Add((int)EnumDocumentType.Video_TinNhanLuong);
                        lstDocumentId.Add((int)EnumDocumentType.HDLD_CoDauDoCongTy);
                        lstDocumentId.Add((int)EnumDocumentType.Motorbike_Registration_Certificate);
                        var files = _unitOfWork.LoanBriefFileRepository.Query(x => lstDocumentId.Contains(x.TypeId.Value)
                                    && x.LoanBriefId == loanOld.LoanBriefId
                                    && x.Status == 1 && x.IsDeleted.GetValueOrDefault(0) == 0, null, false).ToList();
                        if (files != null && files.Count() > 0)
                        {
                            foreach (var file in files)
                            {
                                _unitOfWork.LoanBriefFileRepository.Insert(new LoanBriefFiles
                                {
                                    LoanBriefId = obj.LoanBriefId,
                                    CreateAt = DateTime.Now,
                                    FilePath = file.FilePath,
                                    UserId = file.UserId,
                                    Status = file.Status,
                                    TypeFile = file.TypeFile,
                                    S3status = file.S3status,
                                    TypeId = file.TypeId,
                                    IsDeleted = file.IsDeleted,
                                    MecashId = file.MecashId,
                                    FileThumb = file.FileThumb,
                                    SourceUpload = file.SourceUpload
                                });
                            }
                        }

                    }
                    _unitOfWork.Save();

                    return LoanBriefIdNew;
                }
                return 0;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public bool UpdateLoanDevice(int loanBriefId, int deviceStatus, string contractGinno)
        {
            if (loanBriefId > 0)
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    DeviceStatus = deviceStatus,
                    ContractGinno = contractGinno,
                    LastSendRequestActive = DateTime.Now,
                });
                return true;
            }
            return false;
        }

        public LoanBrief GetAllById(int LoanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanbriefId && x.InProcess != (int)EnumInProcess.Process, null, false, x => x.LoanBriefProperty, x => x.LoanBriefResident, x => x.LoanBriefJob).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public LoanBriefInfoTopup LoanbriefInfoTopupByLmsLoanId(int lmsLoanId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.LmsLoanId == lmsLoanId
                && x.InProcess != (int)EnumInProcess.Process, null, false).Select(LoanBriefInfoTopup.ProjectionDetail).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public int GetLoanStatus(int loanbriefId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false)
                    .Select(x => x.Status.Value).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public bool UpdateLoanTopup(int loanBriefId, decimal loanAmountExpertiseAi, decimal loanAmountExpertiseLast,
                                                 decimal loanAmount, int typeRemarketing)
        {
            if (loanBriefId > 0)
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    LoanAmountExpertiseAi = loanAmountExpertiseAi,
                    LoanAmountExpertiseLast = loanAmountExpertiseLast,
                    LoanAmount = loanAmount,
                    TypeRemarketing = typeRemarketing,
                });
                return true;
            }
            return false;
        }

        public bool RemoveTopup(int loanBriefId)
        {
            if (loanBriefId > 0)
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief()
                {
                    TypeRemarketing = null
                });
                return true;
            }
            return false;
        }

        public List<int> GetLoanBriefOtherLoanFast(List<int> lstLmsLoanId)
        {
            //danh sách các gói vay nhanh
            List<int> lstProductDetailFast = new List<int>()
            {
                (int)Common.Extensions.InfomationProductDetail.VayNhanh4tr,
                (int)Common.Extensions.InfomationProductDetail.KT1_KSHK_4tr,
                (int)Common.Extensions.InfomationProductDetail.KT1_KGT_4tr,
                (int)Common.Extensions.InfomationProductDetail.KT3_KTN_4tr,
                (int)Common.Extensions.InfomationProductDetail.XMCC_KT1_5tr,
                (int)Common.Extensions.InfomationProductDetail.XMCC_KT3_5tr
            };

            return _unitOfWork.LoanBriefRepository.Query(x => lstLmsLoanId.Contains(x.LmsLoanId.GetValueOrDefault(0))
            && !lstProductDetailFast.Contains(x.ProductDetailId.GetValueOrDefault(0)) && x.Status == (int)EnumLoanStatus.DISBURSED, null, false)
                .Select(x => x.LoanBriefId).ToList();
        }

        public bool UpdateAllIsCanDpd()
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => x.Status == (int)EnumLoanStatus.DISBURSED && x.TypeLoanSupport == (int)EnumTypeLoanSupport.DPD, x => new LoanBrief()
                {
                    TypeLoanSupport = 0
                });
                return true;
            }

            catch (Exception ex)
            {
            }
            return false;
        }

        public List<PushLoanCancelToPartnerDetail> ListLoanbriefCancel(int startLoanbriefId, List<int> listDistrictId)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId > startLoanbriefId &&
                x.Status == (int)EnumLoanStatus.CANCELED && x.DistrictId > 0
                && x.InProcess != (int)EnumInProcess.Process && listDistrictId.Contains(x.DistrictId.Value),
                null, false).OrderBy(x => x.LoanBriefId).Select(PushLoanCancelToPartnerDetail.ProjectionDetail).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LoanBriefInfoForTopup> GetLoanbriefFromLmsLoanId(List<int> lmsLoanIds)
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.LmsLoanId > 0 && lmsLoanIds.Contains(x.LmsLoanId.Value)
                && x.InProcess != (int)EnumInProcess.Process, null, false).Select(LoanBriefInfoForTopup.ProjectionDetail).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateSynchorizeFinance(short SynchorizeFinance, List<int> loanBriefIds)
        {
            try
            {
                _unitOfWork.LoanBriefRepository.Update(x => loanBriefIds.Contains(x.LoanBriefId), x => new LoanBrief()
                {
                    SynchorizeFinance = SynchorizeFinance
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public List<LoanBriefInfoForTopup> GetLoanCanTopupOnTopup()
        {
            try
            {
                var query = from l in _unitOfWork.GetContext().LoanBrief
                            join l1 in _unitOfWork.GetContext().LoanBrief on l.ReMarketingLoanBriefId equals l1.LoanBriefId
                            where l.Status == (int)EnumLoanStatus.DISBURSED && l1.Status == (int)EnumLoanStatus.FINISH
                            && l.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp
                            && (l.ProductId == (int)EnumProductCredit.MotorCreditType_CC || l.ProductId == (int)EnumProductCredit.MotorCreditType_KCC 
                                || l.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                            select new LoanBriefInfoForTopup()
                            {
                                LoanBriefId = l.LoanBriefId,
                                LmsLoanId = l.LmsLoanId.Value,
                                ProductId = l.ProductId.Value,
                                LoanBriefProperty = l.LoanBriefProperty ?? null,
                                ProductDetailId = l.ProductDetailId,
                                NationalCard = l.NationalCard,
                                TypeRemarketing = l.TypeRemarketing
                            };
                return query.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LoanBriefInfoCreditScoring> ListReGetCreditScoring()
        {
            try
            {
                return _unitOfWork.LoanBriefRepository.Query(x => x.LeadScore.GetValueOrDefault(0) == 0 
                    && (x.Status == 100 || x.Status == 110)
                    && (x.ProductId == 2 || x.ProductId == 5 || x.ProductId == 28)
                    && x.CreatedTime.Value.Year >= 2021, null, false).Select(LoanBriefInfoCreditScoring.ProjectionDetail).Take(2000).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void Dispose()
        {
            Console.WriteLine($" Dispose()");
        }
    }
}
