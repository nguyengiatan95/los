﻿using LOS.ThirdPartyProccesor.Objects;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILocationService
    {
        string GetNameAddress(decimal lat, decimal lng);
        string GetNameAddress(string latlng);
        ElementItem DistanceTwoPoint(string address, string latTo, string lngTo);
        ElementItem DistanceTwoPointGoogleMap(string fromAddress, string toAddress);
        double DistanceTwoPoint(double lat, double lon, double latTo, double lonTo);
        double DistanceTwoPoint(string fromAddress, string toAddress);
    }
    public class LocationService : ILocationService
    {
        private IConfiguration _configuration;
        public LocationService()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _configuration = builder.Build();
        }
        private GeocodingResult GetAddress(decimal lat, decimal lng)
        {
            var APIKEY = _configuration["AppSettings:APIKEY_GOOGLE_MAP"];
            var client = new RestClient("https://maps.googleapis.com");
            var request = new RestRequest("maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=" + APIKEY, Method.POST);
            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var strJson = response.Content;
                    var objResultGet =
                        JsonConvert.DeserializeObject<GeocodingResult>(strJson);
                    return objResultGet;
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        private GeocodingResult GetAddress(string latlng)
        {
            var APIKEY = _configuration["AppSettings:APIKEY_GOOGLE_MAP"];
            var client = new RestClient("https://maps.googleapis.com");
            var request = new RestRequest("maps/api/geocode/json?latlng=" + latlng + "&key=" + APIKEY, Method.POST);
            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var strJson = response.Content;
                    var objResultGet =
                        JsonConvert.DeserializeObject<GeocodingResult>(strJson);
                    return objResultGet;
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string GetNameAddress(string latlng)
        {
            try
            {
                var result = GetAddress(latlng);
                if (result != null && result.results != null && result.results.Count > 0)
                    return string.IsNullOrEmpty(result.results[0].formatted_address) ? null : result.results[0].formatted_address;
            }
            catch (Exception e)
            {
                return null;
            }

            return "Không xác định";
        }
        public string GetNameAddress(decimal lat, decimal lng)
        {
            try
            {
                var result = GetAddress(lat, lng);
                if (result != null && result.results != null && result.results.Count > 0)
                    return string.IsNullOrEmpty(result.results[0].formatted_address) ? null : result.results[0].formatted_address;
            }
            catch (Exception e)
            {
                return null;
            }

            return "Không xác định";
        }


        public ElementItem DistanceTwoPoint(string address, string latTo, string lngTo)
        {
            var APIKEY = _configuration["AppSettings:APIKEY_GOOGLE_MAP"];
            var client = new RestClient("https://maps.googleapis.com");
            var request = new RestRequest(string.Format("maps/api/distancematrix/json?origins={0}&destinations={1}, {2}&key={3}", address, latTo, lngTo, APIKEY), Method.POST);
            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var strJson = response.Content;
                    var objResultGet =
                        JsonConvert.DeserializeObject<DistancematrixResult>(strJson);
                    if (objResultGet != null && objResultGet.rows != null && objResultGet.rows.Count > 0
                        && objResultGet.rows[0] != null && objResultGet.rows[0].elements != null && objResultGet.rows[0].elements.Count > 0)
                        return objResultGet.rows[0].elements[0];
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public ElementItem DistanceTwoPointGoogleMap(string fromAddress, string toAddress)
        {
            var APIKEY = _configuration["AppSettings:APIKEY_GOOGLE_MAP"];
            var client = new RestClient("https://maps.googleapis.com");
            var request = new RestRequest($"maps/api/distancematrix/json?origins={fromAddress}&destinations={toAddress}&key={APIKEY}", Method.POST);
            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var strJson = response.Content;
                    var objResultGet =
                        JsonConvert.DeserializeObject<DistancematrixResult>(strJson);
                    if (objResultGet != null && objResultGet.rows != null && objResultGet.rows.Count > 0
                        && objResultGet.rows[0] != null && objResultGet.rows[0].elements != null && objResultGet.rows[0].elements.Count > 0)
                        return objResultGet.rows[0].elements[0];
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public double DistanceTwoPoint(string fromAddress, string toAddress)
        {

            try
            {
                double lat = 0l, latTo = 0l, lng = 0l, lngTo = 0l ;
                ConvertToPoint(fromAddress, ref lat, ref lng);
                ConvertToPoint(toAddress, ref latTo, ref lngTo);
                var R = 6371; //Km
                var dLat = Radians(latTo) - Radians(lat);
                var dLon = Radians(lngTo) - Radians(lng);
                var a = Math.Pow(Math.Sin(dLat / 2), 2) + (Math.Pow(Math.Sin(dLon / 2), 2) * Math.Cos(Radians(lat)) * Math.Cos(Radians(latTo)));
                var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
                return Math.Round(R * c * 1000, 0);
            }
            catch (Exception e)
            {
                return 0;
            }

        }
        public double DistanceTwoPoint(double lat, double lon, double latTo, double lonTo)
        {

            try
            {
                var R = 6371; //Km
                var dLat = Radians(latTo) - Radians(lat);
                var dLon = Radians(lonTo) - Radians(lon);
                var a = Math.Pow(Math.Sin(dLat / 2), 2) + (Math.Pow(Math.Sin(dLon / 2), 2) * Math.Cos(Radians(lat)) * Math.Cos(Radians(latTo)));
                var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
                return Math.Round(R * c * 1000, 0);
            }
            catch (Exception e)
            {
                return 0;
            }

        }


        public double Radians(double x)
        {
            return x * Math.PI / 180;
        }

        public void ConvertToPoint(string sLatLng, ref double lat, ref double lng)
        {
            try
            {
                if (sLatLng.Contains(","))
                {
                    var arr = sLatLng.Split(",");
                    if (arr.Length == 2)
                    {
                        lat = Convert.ToDouble(arr[0]);
                        lng = Convert.ToDouble(arr[1]);
                    }
                }
            }
            catch
            {

            }
        }


    }
}
