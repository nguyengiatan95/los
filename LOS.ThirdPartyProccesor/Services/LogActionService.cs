﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILogActionService
    {
        Task<int> AddLogAction(LogLoanAction entity);
    }
    public class LogActionService : ILogActionService
    {
        private readonly IUnitOfWork _unitOfWork;
        protected IConfiguration _baseConfig;
        public LogActionService()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(_baseConfig.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public async Task<int> AddLogAction(LogLoanAction entity)
        {
            try
            {
                _unitOfWork.LogLoanActionRepository.Insert(entity);
                await _unitOfWork.SaveAsync();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
