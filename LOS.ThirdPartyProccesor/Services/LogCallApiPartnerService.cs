﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILogCallApiPartnerService
    {
        int Insert(LogCallApiPartner entity);
        int Update(LogCallApiPartner entity);
    }
    public class LogCallApiPartnerService : ILogCallApiPartnerService
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogCallApiPartnerService()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public int Insert(LogCallApiPartner entity)
        {
            try
            {
                entity.CreateAt = DateTime.Now;
                _unitOfWork.LogCallApiPartnerRepository.Insert(entity);
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int Update(LogCallApiPartner entity)
        {
            try
            {
                var obj = _unitOfWork.LogCallApiRepository.GetById(entity.Id);
                if (obj != null && obj.Id > 0)
                {
                    obj.Status = entity.Status;
                    obj.Output = entity.Output;
                    obj.ModifyAt = DateTime.Now;
                    _unitOfWork.Save();
                }
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
