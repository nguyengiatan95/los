﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILogCallApiService
    {
        int Insert(LogCallApi entity);
        int Update(LogCallApi entity);
        List<LogCallApi> GetList(int loanbriefId, int typeCallApi, string department = "");
        LogCallApi GetLast(int loanbriefId, int typeCallApi);
        LogCallApi GetLastType(int typeCallApi);
    }
    public class LogCallApiService : ILogCallApiService
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogCallApiService()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public int Insert(LogCallApi entity)
        {
            try
            {
                entity.CreatedAt = DateTime.Now;
                _unitOfWork.LogCallApiRepository.Insert(entity);
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int Update(LogCallApi entity)
        {
            try
            {
                var obj = _unitOfWork.LogCallApiRepository.GetById(entity.Id);
                if (obj != null && obj.Id > 0)
                {
                    obj.Status = entity.Status;
                    obj.Output = entity.Output;
                    obj.ModifyAt = DateTime.Now;
                    _unitOfWork.Save();
                }
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public List<LogCallApi> GetList(int loanbriefId, int typeCallApi,string department = "")
        {
            try
            {
                return _unitOfWork.LogCallApiRepository.Query(x => x.LoanBriefId == loanbriefId && x.ActionCallApi == typeCallApi && (department == "" || x.TokenCallApi == department), null, false).ToList();
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public LogCallApi GetLast(int loanbriefId, int typeCallApi)
        {
            try
            {
                return _unitOfWork.LogCallApiRepository.Query(x => x.LoanBriefId == loanbriefId && x.ActionCallApi == typeCallApi && x.Status == 1, null, false).OrderByDescending(x=>x.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public LogCallApi GetLastType(int typeCallApi)
        {
            try
            {
                return _unitOfWork.LogCallApiRepository.Query(x => x.ActionCallApi == typeCallApi && x.Status == 1, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
