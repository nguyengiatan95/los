﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILogCancelLoanbrief
    {
        List<LogCancelLoanbrief> GetAllExcuted();
        void IsExcuted(int id);
    }
    public class LogCancelLoanbriefService : ILogCancelLoanbrief
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogCancelLoanbriefService()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public List<LogCancelLoanbrief> GetAllExcuted()
        {
            try
            {
                return _unitOfWork.LogCancelLoanbriefRepository.Query(x => x.IsExcuted.GetValueOrDefault(0) == 0, null, false).ToList();
            }catch(Exception ex)
            {
                return null;
            }
        }
        public void IsExcuted(int id)
        {
            try
            {
                _unitOfWork.LogCancelLoanbriefRepository.Update(x => x.Id == id, x => new LogCancelLoanbrief()
                {
                    IsExcuted = 1,
                    UpdatedAt = DateTime.Now
                });
            }
            catch (Exception ex)
            {
            }
        }
    }
}
