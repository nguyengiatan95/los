﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILogClickToCall
    {
        List<LogClickToCall> GetLogClickToCallMobile();
        bool DoneGetRecording(int id);
    }
    public sealed class LogClickToCallService : ILogClickToCall
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogClickToCallService()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }


        public List<LogClickToCall> GetLogClickToCallMobile()
        {
            try
            {
                return _unitOfWork.LogClickToCallRepository.Query(x => x.TypeCallService == (int)EnumTypeCallService.Metech
                 && !string.IsNullOrEmpty(x.CallId) && x.GetRecording.GetValueOrDefault() == 0, null, false).ToList();

            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public bool DoneGetRecording(int id)
        {
            try
            {
                _unitOfWork.LogClickToCallRepository.Update(x => x.Id == id, x => new LogClickToCall()
                {
                    GetRecording = 1
                });
                _unitOfWork.Save();
                return true; ;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
