﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILogLoanCheckMomoService
    {
        int AddLog(LogLoanInfoPartner entity);
    }

    public class LogLoanCheckMomoService : ILogLoanCheckMomoService
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogLoanCheckMomoService()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public int AddLog(LogLoanInfoPartner entity)
        {
            try
            {
                _unitOfWork.LogLoanInfoPartnerRepository.Insert(entity);
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


    }
}
