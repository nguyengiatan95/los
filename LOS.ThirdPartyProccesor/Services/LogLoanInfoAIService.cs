﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILogLoanInfoAIService
    {
        int AddLog(LogLoanInfoAi entity);
        int UpdateStatusCall(LogLoanInfoAi entity);
        List<LogLoanInfoAi> GetListResult();
        List<LogLoanInfoAi> GetListRequest();
        int InsertRequest(LogLoanInfoAi entity);
        int UpdateRequest(LogLoanInfoAi entity);
        int UpdateRefcode(LogLoanInfoAi entity, int status = 1);
        int UpdateResponseOfRequest(LogLoanInfoAi entity);
        int UpdateResponseOfRequest(LogLoanInfoAi entity, int IsExcuted);
        bool CancelRequest(int Id, string ResultFinal);
        LogLoanInfoAi GetById(int id);
        void CancelRequestBefore(int service, int loanbriefId, int LogLoanInfoAiId);
        int Done(LogLoanInfoAi entity);
        LogLoanInfoAi GetLastRequest(int loanbriefId, int serviceType);
        int UpdateResultRequest(int id, string final);
    }

    public class LogLoanInfoAIService : ILogLoanInfoAIService
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogLoanInfoAIService()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public int AddLog(LogLoanInfoAi entity)
        {
            try
            {
                entity.CreatedAt = DateTime.Now;
                _unitOfWork.LogLoanInfoAiRepository.Insert(entity);
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int InsertRequest(LogLoanInfoAi entity)
        {
            try
            {
                _unitOfWork.LogLoanInfoAiRepository.Insert(entity);
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int UpdateRequest(LogLoanInfoAi entity)
        {
            try
            {
                _unitOfWork.LogLoanInfoAiRepository.Update(x => x.Id == entity.Id, x => new LogLoanInfoAi()
                {
                    CreatedAt = DateTime.Now,
                    Request = entity.Request,
                    Token = entity.Token,
                    Url = entity.Url,
                    HomeNetwok = entity.HomeNetwok,
                    PushNextState = entity.PushNextState,
                    Retry = entity.Retry
                });
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int UpdateResponseOfRequest(LogLoanInfoAi entity)
        {
            try
            {
                _unitOfWork.LogLoanInfoAiRepository.Update(x => x.Id == entity.Id, x => new LogLoanInfoAi()
                {
                    Status = entity.Status,
                    ResponseRequest = entity.ResponseRequest,
                    UpdatedAt = DateTime.Now
                });
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int UpdateResponseOfRequest(LogLoanInfoAi entity, int IsExcuted)
        {
            try
            {
                _unitOfWork.LogLoanInfoAiRepository.Update(x => x.Id == entity.Id, x => new LogLoanInfoAi()
                {
                    Status = entity.Status,
                    ResponseRequest = entity.ResponseRequest,
                    IsExcuted = IsExcuted,
                    UpdatedAt = DateTime.Now
                });
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool CancelRequest(int Id, string ResultFinal)
        {
            try
            {
                _unitOfWork.LogLoanInfoAiRepository.Update(x => x.Id == Id, x => new LogLoanInfoAi()
                {
                    ResultFinal = ResultFinal,
                    IsCancel = 1
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        public int UpdateStatusCall(LogLoanInfoAi entity)
        {
            try
            {
                var logLoanInfoAi = _unitOfWork.LogLoanInfoAiRepository.GetById(entity.Id);
                if (logLoanInfoAi != null)
                    _unitOfWork.LogLoanInfoAiRepository.Update(x => x.Id == entity.Id, x => new LogLoanInfoAi()
                    {
                        Status = entity.Status,
                        ResponseRequest = entity.ResponseRequest
                    });
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int UpdateRefcode(LogLoanInfoAi entity, int status = 1)
        {
            try
            {
                _unitOfWork.LogLoanInfoAiRepository.Update(x => x.Id == entity.Id, x => new LogLoanInfoAi()
                {
                    RefCode = entity.RefCode,
                    Status = status
                });
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public void CancelRequestBefore(int service, int loanbriefId, int LogLoanInfoAiId)
        {
            try
            {
                var data = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.ServiceType == service && x.LoanbriefId == loanbriefId && x.Id < LogLoanInfoAiId && x.IsCancel != 1, null, false).ToList();
                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        _unitOfWork.LogLoanInfoAiRepository.Update(x => x.Id == item.Id, x => new LogLoanInfoAi()
                        {
                            IsCancel = 1
                        });
                    }
                    _unitOfWork.Save();
                }

            }
            catch (Exception ex)
            {

            }
        }

        public LogLoanInfoAi GetById(int id)
        {
            try
            {
                return _unitOfWork.LogLoanInfoAiRepository.GetById(id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LogLoanInfoAi> GetListResult()
        {
            try
            {
                return _unitOfWork.LogLoanInfoAiRepository.Query(x => x.ServiceType != (int)ServiceTypeAI.EkycMotorbikeRegistrationCertificate && (x.IsExcuted == (int)EnumLogLoanInfoAiExcuted.WaitHandle 
                || x.IsExcuted == (int)EnumLogLoanInfoAiExcuted.HasResult) && x.IsCancel != 1, null, false).OrderByDescending(x => x.Id).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LogLoanInfoAi> GetListRequest()
        {
            try
            {
                return _unitOfWork.LogLoanInfoAiRepository.Query(x => x.IsExcuted == (int)EnumLogLoanInfoAiExcuted.WaitRequest
               && x.IsCancel != 1, null, false).OrderByDescending(x => x.Id).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int Done(LogLoanInfoAi entity)
        {
            try
            {
                _unitOfWork.LogLoanInfoAiRepository.Update(x => x.Id == entity.Id, x => new LogLoanInfoAi()
                {
                    ResultFinal = entity.ResultFinal,
                    IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode()
                });
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("LogLoanInfoAi Done ERROR: {0} {1}", ex.ToString(), DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                return 0;
            }
        }

        public LogLoanInfoAi GetLastRequest(int loanbriefId, int serviceType)
        {
            try
            {
                return _unitOfWork.LogLoanInfoAiRepository.Query(x =>x.LoanbriefId == loanbriefId && x.ServiceType == serviceType && x.IsCancel != 1, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int UpdateResultRequest(int id, string final)
        {
            try
            {
                _unitOfWork.LogLoanInfoAiRepository.Update(x => x.Id == id, x => new LogLoanInfoAi()
                {
                    ResultFinal = final
                });
                return id;
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("UpdateResultRequest Done ERROR: {0} {1}", ex.ToString(), DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                return 0;
            }
        }

    }
}
