﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor
{
    public interface ILogPushNotificationService
    {
        LogPushNotification GetLast(int loanbriefId, int typeNotification, int actionPush);
    }
    public class LogPushNotificationService: ILogPushNotificationService
    {
        protected IConfiguration _baseConfig;
        private readonly IUnitOfWork _unitOfWork;
        public LogPushNotificationService()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public LogPushNotification GetLast(int loanbriefId, int typeNotification, int actionPush)
        {
            try
            {
                return _unitOfWork.LogPushNotificationRepository.Query(x => x.LoanbriefId == loanbriefId && x.TypeNotification == typeNotification
                 && x.ActionPush == actionPush && x.RequestStatus == (int)StatusCallApi.Success, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
