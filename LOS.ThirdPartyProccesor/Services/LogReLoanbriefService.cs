﻿using LOS.DAL.Dapper;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILogReLoanbrief
    {
        bool Update(int id, int isExcuted, string resultMessage, int? reLoanbriefId);
        bool Add(LogReLoanbrief entity);
        LogReLoanbrief GetLast(int loanbriefId);
        List<LogReLoanbrief> GetList(List<int> loanbriefIds);
        LogReLoanbrief GetLast(int loanbriefId, string nationalCard);
    }
    public class LogReLoanbriefService : ILogReLoanbrief
    {
        protected IConfiguration _baseConfig;
        private readonly IUnitOfWork _unitOfWork;
        public LogReLoanbriefService()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(_baseConfig.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }
        public bool Update(int id, int isExcuted, string resultMessage, int? reLoanbriefId)
        {
            try
            {
                _unitOfWork.LogReLoanbriefRepository.Update(x => x.Id == id, x => new LogReLoanbrief()
                {
                    UpdatedAt = DateTime.Now,
                    IsExcuted = isExcuted,
                    ResultMessage = resultMessage,
                    ReLoanbriefId = reLoanbriefId
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Add(LogReLoanbrief entity)
        {
            try
            {
                _unitOfWork.LogReLoanbriefRepository.Insert(entity);
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public LogReLoanbrief GetLast(int loanbriefId)
        {
            try
            {
                return _unitOfWork.LogReLoanbriefRepository.Query(x => x.LoanbriefId == loanbriefId && x.ReLoanbriefId > 0, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LogReLoanbrief> GetList(List<int> loanbriefIds)
        {
            try
            {
              return  _unitOfWork.LogReLoanbriefRepository.Query(x => loanbriefIds.Contains(x.LoanbriefId.Value) && x.ReLoanbriefId > 0, null, false).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public LogReLoanbrief GetLast(int loanbriefId, string nationalCard)
        {
            try
            {
                return _unitOfWork.LogReLoanbriefRepository.Query(x => x.ReLoanbriefId > 0 
                && (x.LoanbriefId == loanbriefId 
                    || (!string.IsNullOrEmpty(x.NationalCard) && x.NationalCard == nationalCard)), null, false).OrderByDescending(x => x.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
