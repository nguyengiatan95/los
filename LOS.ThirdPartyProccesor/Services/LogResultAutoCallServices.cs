﻿using LOS.Common.Extensions;
using LOS.DAL.Dapper;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILogResultAutoCallServices
    {
        List<LogResultAutoCall> GetListReLoanAutocall();
        List<LogResultAutoCall> GetListReLoanAutocallBN();
        int CreateLoanAutocall(int LoanBriefId);
        int CreateLoanAutocallBN(int LoanBriefId);
        List<LogResultAutoCall> GetListSendSMSQualified(int fromId);
        List<LogResultAutoCall> GetListSendZNS();
        bool UpdateSendSMS(int Id);
        LogSendSms GetLastSendSms(int TypeSendSms, int loanbriefId);
        LogResultAutoCall GetResult(int LoanbriefId);
    }
    public class LogResultAutoCallServices : ILogResultAutoCallServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        public LogResultAutoCallServices()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public List<LogResultAutoCall> GetListReLoanAutocall()
        {
            try
            {
                return _unitOfWork.LogResultAutoCallRepository.Get(x => x.AreaSupport.HasValue && !x.ResultCall.HasValue && x.CampaignId == (int)EnumCampaignAutocall.AutocallHuy
                && x.ReLoanbriefId.GetValueOrDefault(0) == (int)EnumCreateLoanAutocall.NotDone, null, false);

               // return _unitOfWork.LogResultAutoCallRepository.Get(x => x.BorrowMotobike == (int)EnumCreateLoanAutocall.Done && x.MotobikeCertificate == (int)EnumCreateLoanAutocall.Done
               //&& x.AreaSupport == (int)EnumCreateLoanAutocall.Done && x.CampaignId == (int)EnumCampaignAutocall.AutocallBN
               //&& x.ReLoanbriefId.GetValueOrDefault(0) == (int)EnumCreateLoanAutocall.NotDone, null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public LogResultAutoCall GetResult(int LoanbriefId)
        {
            try
            {
                return _unitOfWork.LogResultAutoCallRepository.Get(x => x.LoanbriefId == LoanbriefId && !string.IsNullOrEmpty(x.PhoneAgent), null, false).FirstOrDefault();

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LogResultAutoCall> GetListReLoanAutocallBN()
        {
            try
            {
                return _unitOfWork.LogResultAutoCallRepository.Get(x => x.BorrowMotobike == (int)EnumCreateLoanAutocall.Done
                && x.CampaignId == (int)EnumCampaignAutocall.AutocallBN && x.ReLoanbriefId.GetValueOrDefault(0) == (int)EnumCreateLoanAutocall.NotDone, null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int CreateLoanAutocall(int LoanBriefId)
        {
            //Get Info LoanOld
            try
            {
                var loanOld = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId, null, false).Select(LoanBriefInitDetail.ProjectionViewDetail).FirstOrDefault();
                if (loanOld != null)
                {
                    //Check xem có đơn nào đang xử lý hay không
                    var lstLoan = _unitOfWork.LoanBriefRepository.Query(x => x.Status != (int)EnumLoanStatus.FINISH
                    && x.Status != (int)EnumLoanStatus.CANCELED && (x.Phone == loanOld.Phone || (!string.IsNullOrEmpty(loanOld.NationalCard) && x.NationalCard == loanOld.NationalCard)), null, false).OrderByDescending(x => x.CreatedTime).Select(LoanBriefInfoTopup.ProjectionDetail).FirstOrDefault();
                    if (lstLoan == null)
                    {
                        //Tạo đơn vay mới
                        var obj = new LoanBrief()
                        {
                            TypeRemarketing = (int)EnumTypeRemarketing.IsCreateLoanAutocall,
                            ReMarketingLoanBriefId = loanOld.LoanBriefId,
                            LoanAmount = loanOld.LoanAmount,
                            LoanAmountFirst = loanOld.LoanAmount,
                            LoanTime = loanOld.LoanTime,
                            ProductId = loanOld.ProductId,
                            CreatedTime = DateTime.Now,
                            FullName = loanOld.Customer.FullName,
                            Phone = loanOld.Customer.Phone,
                            Dob = loanOld.Customer.Dob,
                            Gender = loanOld.Customer.Gender,
                            NationalCard = loanOld.Customer.NationalCard,
                            NationalCardDate = loanOld.Customer.NationalCardDate,
                            NationCardPlace = loanOld.Customer.NationCardPlace,
                            ProvinceId = loanOld.LoanBriefResident.ProvinceId,
                            DistrictId = loanOld.LoanBriefResident.DistrictId,
                            WardId = loanOld.LoanBriefResident.WardId,
                            CustomerId = loanOld.CustomerId,
                            TypeLoanBrief = loanOld.TypeLoanBrief,
                            RateTypeId = loanOld.RateTypeId,
                            FromDate = DateTime.Now,
                            Status = (int)EnumLoanStatus.INIT,
                            ReceivingMoneyType = loanOld.ReceivingMoneyType,
                            BankId = loanOld.BankId,
                            BankAccountName = loanOld.BankAccountName,
                            BankAccountNumber = loanOld.BankAccountNumber,
                            BankCardNumber = loanOld.BankCardNumber,
                            Frequency = loanOld.Frequency,
                            BuyInsurenceCustomer = loanOld.BuyInsurenceCustomer,
                            PlatformType = (int)EnumPlatformType.ReMarketing,
                            HubId = loanOld.HubId,
                            RateMoney = loanOld.RateMoney.HasValue ? loanOld.RateMoney.Value : 2700,
                            RatePercent = loanOld.RatePercent.HasValue ? loanOld.RatePercent.Value : 98.55,
                            LoanAmountExpertise = loanOld.LoanAmountExpertise,
                            LoanAmountExpertiseLast = loanOld.LoanAmountExpertiseLast,
                            IsTrackingLocation = loanOld.IsTrackingLocation,
                            IsLocate = loanOld.IsLocate,
                            Step = loanOld.Step,
                            UtmSource = "Autocall-Remarketing"
                        };
                        if (loanOld.LoanTime > 0)
                            obj.ToDate = DateTime.Now.AddMonths((int)loanOld.LoanTime);
                        //insert to db
                        _unitOfWork.LoanBriefRepository.Insert(obj);
                        _unitOfWork.Save();
                        var LoanBriefIdNew = obj.LoanBriefId;
                        //Update lại ReLoanBriefID = Mã đơn mới tạo
                        _unitOfWork.LogResultAutoCallRepository.Update(x => x.LoanbriefId == LoanBriefId, x => new LogResultAutoCall()
                        {
                            ReLoanbriefId = LoanBriefIdNew
                        });
                        //nếu là đơn doanh nghiệp => lưu thông tin doanh nghiệp
                        if (loanOld.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                        {
                            loanOld.LoanBriefCompany.LoanBriefId = LoanBriefIdNew;
                            _unitOfWork.LoanBriefCompanyRepository.Insert(loanOld.LoanBriefCompany);
                        }
                        //Thêm thông tin địa chỉ đang ở vào bảng LoanBriefResident
                        if (loanOld.LoanBriefResident != null)
                        {
                            loanOld.LoanBriefResident.LoanBriefResidentId = LoanBriefIdNew;
                            _unitOfWork.LoanBriefResidentRepository.Insert(loanOld.LoanBriefResident);
                        }
                        //Thêm thông tin địa chỉ hộ khẩu LoanBriefHousehold
                        if (loanOld.LoanBriefHousehold != null)
                        {
                            loanOld.LoanBriefHousehold.LoanBriefHouseholdId = LoanBriefIdNew;
                            _unitOfWork.LoanBriefHouseholdRepository.Insert(loanOld.LoanBriefHousehold);
                        }
                        //Thêm thông tin công việc
                        if (loanOld.LoanBriefJob != null)
                        {
                            loanOld.LoanBriefJob.LoanBriefJobId = LoanBriefIdNew;
                            _unitOfWork.LoanBriefJobRepository.Insert(loanOld.LoanBriefJob);
                        }
                        //Thêm thông tin tài sản
                        if (loanOld.LoanBriefProperty != null)
                        {
                            loanOld.LoanBriefProperty.LoanBriefPropertyId = LoanBriefIdNew;
                            _unitOfWork.LoanBriefPropertyRepository.Insert(loanOld.LoanBriefProperty);
                        }

                        //nếu là đơn doanh nghiệp => lưu thông tin doanh nghiệp
                        if (loanOld.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                        {
                            loanOld.LoanBriefCompany.LoanBriefId = LoanBriefIdNew;
                            _unitOfWork.LoanBriefCompanyRepository.Insert(loanOld.LoanBriefCompany);
                        }
                        //Thêm thông tin địa chỉ đang ở vào bảng LoanBriefResident
                        if (loanOld.LoanBriefResident != null)
                        {
                            loanOld.LoanBriefResident.LoanBriefResidentId = LoanBriefIdNew;
                            _unitOfWork.LoanBriefResidentRepository.Insert(loanOld.LoanBriefResident);
                        }
                        //Thêm thông tin địa chỉ hộ khẩu LoanBriefHousehold
                        if (loanOld.LoanBriefHousehold != null)
                        {
                            loanOld.LoanBriefHousehold.LoanBriefHouseholdId = LoanBriefIdNew;
                            _unitOfWork.LoanBriefHouseholdRepository.Insert(loanOld.LoanBriefHousehold);
                        }
                        //Thêm thông tin công việc
                        if (loanOld.LoanBriefJob != null)
                        {
                            loanOld.LoanBriefJob.LoanBriefJobId = LoanBriefIdNew;
                            _unitOfWork.LoanBriefJobRepository.Insert(loanOld.LoanBriefJob);
                        }
                        //Thêm thông tin tài sản
                        if (loanOld.LoanBriefProperty != null)
                        {
                            loanOld.LoanBriefProperty.LoanBriefPropertyId = LoanBriefIdNew;
                            _unitOfWork.LoanBriefPropertyRepository.Insert(loanOld.LoanBriefProperty);
                        }
                        //xử lý documment
                        if (loanOld.LoanBriefDocument != null && loanOld.LoanBriefDocument.Count > 0)
                        {

                            foreach (var item in loanOld.LoanBriefDocument.ToList())
                            {
                                if (item.LoanBriefDocumentId == 0)
                                    _unitOfWork.LoanBriefDocumentRepository.Insert(new LoanBriefDocument
                                    {
                                        LoanBriefId = LoanBriefIdNew,
                                        DocumentId = item.DocumentId
                                    });
                            }
                        }
                        //Thêm tin đồng nghiệp
                        if (loanOld.LoanBriefRelationship != null && loanOld.LoanBriefRelationship.Count > 0)
                        {
                            foreach (var item in loanOld.LoanBriefRelationship)
                            {
                                item.Id = 0;
                                if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                                {
                                    item.LoanBriefId = LoanBriefIdNew;
                                    _unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                                }
                            }
                        }

                        _unitOfWork.Save();
                        return LoanBriefIdNew;
                    }
                    else
                    {
                        //Update lại ReLoanBriefID = Mã đơn mới tạo
                        _unitOfWork.LogResultAutoCallRepository.Update(x => x.LoanbriefId == LoanBriefId, x => new LogResultAutoCall()
                        {
                            ReLoanbriefId = -1
                        });
                        _unitOfWork.Save();
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }

                return 0;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public int CreateLoanAutocallBN(int LoanBriefId)
        {
            //Get Info LoanOld
            try
            {
                var loanOld = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == LoanBriefId, null, false).Select(LoanBriefInitDetail.ProjectionViewDetail).FirstOrDefault();
                if (loanOld.ProvinceId == 27)
                {
                    if (loanOld != null)
                    {
                        //Check xem có đơn nào đang xử lý hay không
                        var lstLoan = _unitOfWork.LoanBriefRepository.Query(x => x.Status != (int)EnumLoanStatus.FINISH
                        && x.Status != (int)EnumLoanStatus.CANCELED && (x.Phone == loanOld.Phone || (!string.IsNullOrEmpty(loanOld.NationalCard) && x.NationalCard == loanOld.NationalCard)), null, false).OrderByDescending(x => x.CreatedTime).Select(LoanBriefInfoTopup.ProjectionDetail).FirstOrDefault();
                        if (lstLoan == null)
                        {
                            //Tạo đơn vay mới
                            var obj = new LoanBrief()
                            {
                                TypeRemarketing = (int)EnumTypeRemarketing.IsCreateLoanAutocall,
                                ReMarketingLoanBriefId = loanOld.LoanBriefId,
                                LoanAmount = loanOld.LoanAmount,
                                LoanAmountFirst = loanOld.LoanAmount,
                                LoanTime = loanOld.LoanTime,
                                ProductId = loanOld.ProductId,
                                CreatedTime = DateTime.Now,
                                FullName = loanOld.Customer.FullName,
                                Phone = loanOld.Customer.Phone,
                                Dob = loanOld.Customer.Dob,
                                Gender = loanOld.Customer.Gender,
                                NationalCard = loanOld.Customer.NationalCard,
                                NationalCardDate = loanOld.Customer.NationalCardDate,
                                NationCardPlace = loanOld.Customer.NationCardPlace,
                                ProvinceId = loanOld.LoanBriefResident.ProvinceId,
                                DistrictId = loanOld.LoanBriefResident.DistrictId,
                                WardId = loanOld.LoanBriefResident.WardId,
                                CustomerId = loanOld.CustomerId,
                                TypeLoanBrief = loanOld.TypeLoanBrief,
                                RateTypeId = loanOld.RateTypeId,
                                FromDate = DateTime.Now,
                                Status = (int)EnumLoanStatus.INIT,
                                ReceivingMoneyType = loanOld.ReceivingMoneyType,
                                BankId = loanOld.BankId,
                                BankAccountName = loanOld.BankAccountName,
                                BankAccountNumber = loanOld.BankAccountNumber,
                                BankCardNumber = loanOld.BankCardNumber,
                                Frequency = loanOld.Frequency,
                                BuyInsurenceCustomer = loanOld.BuyInsurenceCustomer,
                                PlatformType = (int)EnumPlatformType.ReMarketing,
                                HubId = loanOld.HubId,
                                RateMoney = loanOld.RateMoney.HasValue ? loanOld.RateMoney.Value : 2700,
                                RatePercent = loanOld.RatePercent.HasValue ? loanOld.RatePercent.Value : 98.55,
                                LoanAmountExpertise = loanOld.LoanAmountExpertise,
                                LoanAmountExpertiseLast = loanOld.LoanAmountExpertiseLast,
                                IsTrackingLocation = loanOld.IsTrackingLocation,
                                IsLocate = loanOld.IsLocate,
                                Step = loanOld.Step,
                                UtmSource = "Autocall-Remarketing"
                            };
                            if (loanOld.LoanTime > 0)
                                obj.ToDate = DateTime.Now.AddMonths((int)loanOld.LoanTime);
                            //insert to db
                            _unitOfWork.LoanBriefRepository.Insert(obj);
                            _unitOfWork.Save();
                            var LoanBriefIdNew = obj.LoanBriefId;
                            //Update lại ReLoanBriefID = Mã đơn mới tạo
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.LoanbriefId == LoanBriefId, x => new LogResultAutoCall()
                            {
                                ReLoanbriefId = LoanBriefIdNew
                            });
                            //nếu là đơn doanh nghiệp => lưu thông tin doanh nghiệp
                            if (loanOld.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                            {
                                loanOld.LoanBriefCompany.LoanBriefId = LoanBriefIdNew;
                                _unitOfWork.LoanBriefCompanyRepository.Insert(loanOld.LoanBriefCompany);
                            }
                            //Thêm thông tin địa chỉ đang ở vào bảng LoanBriefResident
                            if (loanOld.LoanBriefResident != null)
                            {
                                loanOld.LoanBriefResident.LoanBriefResidentId = LoanBriefIdNew;
                                _unitOfWork.LoanBriefResidentRepository.Insert(loanOld.LoanBriefResident);
                            }
                            //Thêm thông tin địa chỉ hộ khẩu LoanBriefHousehold
                            if (loanOld.LoanBriefHousehold != null)
                            {
                                loanOld.LoanBriefHousehold.LoanBriefHouseholdId = LoanBriefIdNew;
                                _unitOfWork.LoanBriefHouseholdRepository.Insert(loanOld.LoanBriefHousehold);
                            }
                            //Thêm thông tin công việc
                            if (loanOld.LoanBriefJob != null)
                            {
                                loanOld.LoanBriefJob.LoanBriefJobId = LoanBriefIdNew;
                                _unitOfWork.LoanBriefJobRepository.Insert(loanOld.LoanBriefJob);
                            }
                            //Thêm thông tin tài sản
                            if (loanOld.LoanBriefProperty != null)
                            {
                                loanOld.LoanBriefProperty.LoanBriefPropertyId = LoanBriefIdNew;
                                _unitOfWork.LoanBriefPropertyRepository.Insert(loanOld.LoanBriefProperty);
                            }

                            //nếu là đơn doanh nghiệp => lưu thông tin doanh nghiệp
                            if (loanOld.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                            {
                                loanOld.LoanBriefCompany.LoanBriefId = LoanBriefIdNew;
                                _unitOfWork.LoanBriefCompanyRepository.Insert(loanOld.LoanBriefCompany);
                            }                       
                            //Thêm tin đồng nghiệp
                            if (loanOld.LoanBriefRelationship != null && loanOld.LoanBriefRelationship.Count > 0)
                            {
                                foreach (var item in loanOld.LoanBriefRelationship)
                                {
                                    item.Id = 0;
                                    if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                                    {
                                        item.LoanBriefId = LoanBriefIdNew;
                                        _unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                                    }
                                }
                            }

                            _unitOfWork.Save();
                            return LoanBriefIdNew;
                        }
                        else
                        {
                            //Update lại ReLoanBriefID = Mã đơn mới tạo
                            _unitOfWork.LogResultAutoCallRepository.Update(x => x.LoanbriefId == LoanBriefId, x => new LogResultAutoCall()
                            {
                                ReLoanbriefId = -1
                            });
                            _unitOfWork.Save();
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }

                return 0;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public List<LogResultAutoCall> GetListSendSMSQualified(int fromId)
        {
            try
            {
                return _unitOfWork.LogResultAutoCallRepository.Get(x => x.BorrowMotobike == (int)EnumCreateLoanAutocall.Done && x.MotobikeCertificate == (int)EnumCreateLoanAutocall.Done
                && x.AreaSupport == (int)EnumCreateLoanAutocall.Done 
                && (x.CampaignId != (int)EnumCampaignAutocall.AutocallBN && x.CampaignId != (int)EnumCampaignAutocall.AutocallHuy) && x.SendSms != (int)EnumSendSMS.Done && x.Id >= fromId, null, false);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LogResultAutoCall> GetListSendZNS()
        {
            try
            {
                var db = new DapperHelper(_configuration.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select lr.* from   losreport.dbo.ccx1_autocall   ccx ");
                sql.AppendLine("inner join LOS.dbo.LogResultAutoCall lr on lr.LoanbriefId = ccx.loanbriefid ");
                sql.AppendLine("where DATEDIFF ( d, cast('2020-12-07 00:00:00.000' as date), dateadd ( hour,  7, ccx.startdatetime ))  >= 1  ");
                sql.AppendLine("and ccx.campaignid=  22  and    isnull (ccx.callresult, 0)  <>  1 ");
                sql.AppendLine("and isnull(lr.SendSMS,0) <> 1 ");
                var data = db.Query<LogResultAutoCall>(sql.ToString());
                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool UpdateSendSMS(int Id)
        {
            try
            {
                _unitOfWork.LogResultAutoCallRepository.Update(x => x.Id == Id, x => new LogResultAutoCall()
                {
                    SendSms = (int)EnumSendSMS.Done
                });
                _unitOfWork.Save();
                return true;
            }
            catch
            {

            }
            return false;

        }

        public LogSendSms GetLastSendSms(int TypeSendSms, int loanbriefId)
        {
            try
            {
                return _unitOfWork.LogSendSmsRepository.Get(x => x.TypeSendSms == TypeSendSms && x.LoanbriefId == loanbriefId, null, false).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
