﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ILogSendSmsService
    {
        List<LogSendSms> GetLogSendSms(int loanbriefId, int typeSendSms);
    }

    public class LogSendSmsService : ILogSendSmsService
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogSendSmsService(IUnitOfWork unitOfWork)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public List<LogSendSms> GetLogSendSms(int loanbriefId, int typeSendSms)
        {
            try
            {
                return _unitOfWork.LogSendSmsRepository.Query(x => x.LoanbriefId == loanbriefId && x.TypeSendSms == typeSendSms, null, false).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
