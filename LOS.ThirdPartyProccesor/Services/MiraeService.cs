﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IMiraeService
    {
        ResponceCreateLoan CreateLoan(RequestCreateLoan entity, int pushLoanToPartnerId);
    }
    public class MiraeService : IMiraeService
    {
        protected IConfiguration _baseConfig;
        private readonly ILogCallApiPartnerService _logCallApiPartnerService;
        public MiraeService(ILogCallApiPartnerService logCallApiPartnerService)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
            //var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            //optionsBuilder.UseSqlServer(_baseConfig.GetConnectionString("LOSDatabase"));
            //DbContextOptions<LOSContext> options = optionsBuilder.Options;
            //var db = new LOSContext(options);
            //_unitOfWork = new UnitOfWork(db);
            _logCallApiPartnerService = logCallApiPartnerService;
        }
        public ResponceCreateLoan CreateLoan(RequestCreateLoan entity, int pushLoanToPartnerId)
        {
            var url = _baseConfig["AppSettings:Mirae"] + Constants.CREATE_LOAN_MIRAE;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(entity);
            var request = new RestRequest(Method.POST);
            client.Authenticator = new HttpBasicAuthenticator("3p-lead", "1qaz@WSX");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            request.Timeout = 20000;
            try
            {
                //lưu log call api
                var objLogCallApi = new LogCallApiPartner
                {
                    Action = (int)ActionCallApiPartner.CreateLoanMirae,
                    Link = url,
                    Status = (int)StatusCallApi.CallApi,
                    Token = "Basic Auth - Username: 3p-lead - Password: 1qaz@WSX",
                    IdSelect = pushLoanToPartnerId,
                    Input = body,
                    CreateAt = DateTime.Now
                };
                objLogCallApi.Id = _logCallApiPartnerService.Insert(objLogCallApi);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = json;
                objLogCallApi.ModifyAt = DateTime.Now;
                _logCallApiPartnerService.Update(objLogCallApi);
                ResponceCreateLoan lst = JsonConvert.DeserializeObject<ResponceCreateLoan>(json);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Mirae/CreateLoan Exception");
                return null;
            }
        }
    }
}
