﻿using LOS.Common.Extensions;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface INetworkService
    {
        int CheckHomeNetwok(string phone);
    }
    public class NetworkService: INetworkService
    {
        private IConfiguration _configuration;
        public NetworkService()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _configuration = builder.Build();

        }
        public int CheckHomeNetwok(string phone)
        {
            try
            {
                var vina = _configuration["AppSettings:Vina"];
                var mobi = _configuration["AppSettings:Mobi"];
                var viettel = _configuration["AppSettings:Viettel"];
                string dauso = "";
                if (!string.IsNullOrEmpty(phone))
                {
                    if (phone.Length > 10)
                        dauso = phone.Substring(0, 4);
                    else if (phone.Length > 3)
                    {
                        dauso = phone.Substring(0, 3);
                    }
                }
                if (vina.Contains(dauso))
                    return (int)HomeNetWorkMobile.Vina;
                else if (mobi.Contains(dauso))
                    return (int)HomeNetWorkMobile.Mobi;
                else if (viettel.Contains(dauso))
                    return (int)HomeNetWorkMobile.Viettel;
                return (int)HomeNetWorkMobile.Other;
            }
            catch
            {
                return (int)HomeNetWorkMobile.Other;
            }
        }
    }
}
