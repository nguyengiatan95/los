﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IProductServices
    {
        ProductDetail GetProduct(int productId);
        BrandProductDetail GetBrand(int brandId);
        List<ProductDetail> GetAllProduct();
        List<BrandProductDetail> GetAllBrand();
        int AddBandProduct(BrandProduct entity);
        int AddProduct(Product entity);
        decimal GetPriceAI(int productId, int loanBriefId);

    }
    public class ProductServices : IProductServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISuggestionsInformation _suggestionsInformation;
        public ProductServices(ISuggestionsInformation suggestionsInformation)
        {
            _suggestionsInformation = suggestionsInformation;
            //create new unit of work instance
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public BrandProductDetail GetBrand(int brandId)
        {
            try
            {
                return _unitOfWork.BrandProductRepository.Query(x => x.Id == brandId, null, false).Select(BrandProductDetail.ProjectionDetail).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ProductDetail GetProduct(int productId)
        {
            try
            {
                return _unitOfWork.ProductRepository.Query(x => x.Id == productId, null, false).Select(ProductDetail.ProjectionDetail).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<BrandProductDetail> GetAllBrand()
        {
            try
            {
                return _unitOfWork.BrandProductRepository.All().Select(BrandProductDetail.ProjectionDetail).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ProductDetail> GetAllProduct()
        {
            try
            {
                return _unitOfWork.ProductRepository.All().Select(ProductDetail.ProjectionDetail).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public int AddBandProduct(BrandProduct entity)
        {
            try
            {
                _unitOfWork.BrandProductRepository.Insert(entity);
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int AddProduct(Product entity)
        {
            try
            {
                if (!_unitOfWork.ProductRepository.Any(x => x.ShortName.ToLower() == entity.ShortName && x.Year == entity.Year))
                {
                    _unitOfWork.ProductRepository.Insert(entity);
                    _unitOfWork.Save();
                    return entity.Id;
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public decimal GetPriceAI(int productId, int loanBriefId)
        {
            try
            {
                var productAiModel = PrepareProductAiModel(productId, loanBriefId);
                if (productAiModel != null)
                {
                    var data = _suggestionsInformation.GetMotoPrice(productAiModel);
                    if (data != null && data.predicted_price > 0)
                        return data.predicted_price * 1000000;
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public SuggestionsInformationInput PrepareProductAiModel(int productId, int loanBriefId)
        {
            try
            {
                var phanh = "";
                var vanh = "";
                var product = GetProduct(productId) ?? new ProductDetail();
                var productBrand = GetBrand(product.BrandProductId) ?? new BrandProductDetail();
                var brand = productBrand.Name;
                var models = product.ShortName;
                if (product.BrakeType != null && product.BrakeType > 0)
                    phanh = LOS.Common.Helpers.ExtentionHelper.GetName((BrakeType)product.BrakeType);
                if (product.RimType != null && product.RimType > 0)
                    vanh = LOS.Common.Helpers.ExtentionHelper.GetName((RimType)product.RimType);

                var regdate = product.Year ?? 0;
                return new SuggestionsInformationInput
                {
                    brand = brand,
                    model = models,
                    phanh = phanh,
                    vanh = vanh,
                    regdate = regdate.ToString(),
                    LoanBriefId = loanBriefId
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
