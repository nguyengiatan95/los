﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IPushLoanStatusToPartnerService
    {      
        List<LogPushToPartner> GetFailedPushLoan();
        List<PartnerPushLoanResult> GetChangedStatusLoan();
        List<PartnerPushLoanResult> GetChangedStatusLoanPartner();
        LoanBrief GetLoanBriefById(int id);
        Province GetProvinceById(int id);
        District GetDistrictById(int id);
        void UpdateStatus(string partnerId, int status);
        void IncreaseRetryCount(string partnerId);
    }
    public class PushLoanStatusToPartnerService : IPushLoanStatusToPartnerService
    {
        private readonly UnitOfWork _unitOfWork;
        protected IConfiguration _baseConfig;
        public PushLoanStatusToPartnerService()
        {
            // create new unit of work instance			
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(_baseConfig.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

		public List<PartnerPushLoanResult> GetChangedStatusLoan()
		{
            // chỉ lấy trong vòng 2 tuần 
            var result = (from logs in _unitOfWork.LogPushToPartnerRepository.All()
                        join loans in _unitOfWork.LoanBriefRepository.All() on logs.LoanBriefId equals loans.LoanBriefId
                        where logs.Status != loans.Status && (loans.Status == 99 || loans.Status == 100) && (logs.PartnerProcess == false || logs.PartnerProcess == null) && logs.RetryCount <= 2 && loans.CreatedTime >= DateTime.Now.AddDays(-14)
                        select new PartnerPushLoanResult()
                        {
                            customerId = loans.CustomerId.Value,
                            status = loans.Status.Value,
                            log = logs
                        }).ToList();
            return result;
        }

        public List<PartnerPushLoanResult> GetChangedStatusLoanPartner()
        {
            // chỉ lấy trong vòng 2 tuần 
            var result = (from logs in _unitOfWork.LogPushToPartnerRepository.All()
                          join loans in _unitOfWork.PushLoanToPartnerRepository.All() on logs.LoanBriefId equals loans.Id
                          where logs.Status != loans.Status && (loans.Status == 0 || loans.Status == 1) && logs.PartnerProcess == true && logs.RetryCount <= 2 && loans.CreateDate >= DateTime.Now.AddDays(-30)
                          select new PartnerPushLoanResult()
                          {
                              customerId = loans.LoanPartnerId.Value,
                              status = loans.Status.Value,
                              log = logs
                          }).ToList();
            return result;
        }

        public District GetDistrictById(int id)
		{
            return _unitOfWork.DistrictRepository.GetById(id);
		}

		public List<LogPushToPartner> GetFailedPushLoan()
        {
            return _unitOfWork.LogPushToPartnerRepository.Query(x => x.Status == 1 && x.PushStatus == 0 && x.RetryCount <= 2, null, false).ToList();
        }

		public LoanBrief GetLoanBriefById(int id)
		{
            return _unitOfWork.LoanBriefRepository.GetById(id);
		}

		public Province GetProvinceById(int id)
		{
            return _unitOfWork.ProvinceRepository.GetById(id);
        }

		public void IncreaseRetryCount(string partnerId)
		{
            _unitOfWork.LogPushToPartnerRepository.Update(x => x.PartnerId == partnerId, x => new LogPushToPartner()
            {
                RetryCount = x.RetryCount + 1,
                UpdatedTime = DateTime.Now                                
            });
        }

		public void UpdateStatus(string partnerId, int status)
		{         
            _unitOfWork.LogPushToPartnerRepository.Update(x => x.PartnerId == partnerId, x => new LogPushToPartner()
            {
                Status = status,
                PushStatus = 1,
                UpdatedTime = DateTime.Now,
                RetryCount = 0
            });                       
        }
	}
}
