﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IPushLoanToPartnerService
    {
        bool Add(PushLoanToPartner entity);
        List<PushLoanToPartnerDetail> GetLoanNewCreate();
        bool UpdatePartner(int id, int partner);
        bool UpdateCreateLoanPartner(int id, int? loanPartnerId, int pushAction, string comment, int status);
        PushLoanToPartner GetLastPushLoanCanel();
        PushLoanToPartner GetLastPush(string phone);
        List<int> GetAutoCancelLoanMirae();
        bool CancelLoan(int id, string note);
    }
    public class PushLoanToPartnerService : IPushLoanToPartnerService
    {
        private readonly IUnitOfWork _unitOfWork;
        public PushLoanToPartnerService()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public List<PushLoanToPartnerDetail> GetLoanNewCreate()
        {
            return _unitOfWork.PushLoanToPartnerRepository.Query(x => x.PushAction == (int)ActionPushLoanToPartner.CreateNew, null, false)
                .Select(PushLoanToPartnerDetail.ProjectionDetail).ToList();
        }
        public bool UpdatePartner(int id, int partner)
        {
            if (id > 0)
            {
                _unitOfWork.PushLoanToPartnerRepository.Update(x => x.Id == id, x => new PushLoanToPartner()
                {
                    Partner = partner
                });
                _unitOfWork.Save();
                return true;
            }
            return false;
        }
        public bool UpdateCreateLoanPartner(int id, int? loanPartnerId, int pushAction, string comment, int status)
        {
            if (id > 0)
            {
                _unitOfWork.PushLoanToPartnerRepository.Update(x => x.Id == id, x => new PushLoanToPartner()
                {
                    PushAction = pushAction,
                    Comment = comment,
                    PushDate = DateTime.Now,
                    LoanPartnerId = loanPartnerId,
                    Status = status,
                });
                _unitOfWork.Save();
                return true;
            }
            return false;
        }

        public bool Add(PushLoanToPartner entity)
        {
            try
            {
                _unitOfWork.PushLoanToPartnerRepository.Insert(entity);
                _unitOfWork.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public PushLoanToPartner GetLastPushLoanCanel()
        {
            try
            {
                return _unitOfWork.PushLoanToPartnerRepository.Query(x => x.LoanbriefId > 0, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }
        public PushLoanToPartner GetLastPush(string phone)
        {
            try
            {
                return _unitOfWork.PushLoanToPartnerRepository.Get(x => x.CustomerPhone == phone, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }
        public List<int> GetAutoCancelLoanMirae()
        {
            try
            {
                //lấy danh sách các đơn đã bán cho đối tác mà trong vòng 1 tháng chưa thay đổi trạng thái
                return _unitOfWork.PushLoanToPartnerRepository.Query(x => x.PushAction == (int)ActionPushLoanToPartner.CreatedSuccess
                && x.Status == (int)StatusPushLoanToPartner.Processing && (DateTime.Now - x.CreateDate.Value).Days >= 31 && 
                x.Partner == (int)PartnerPushLoanToPartner.Mirae, null, false)
                    .Select(x => x.Id).ToList();
            }
            catch
            {
                return null;
            }
        }
        public bool CancelLoan(int id, string note)
        {
            if (id > 0)
            {
                _unitOfWork.PushLoanToPartnerRepository.Update(x => x.Id == id, x => new PushLoanToPartner()
                {
                    SynchronizedDate = DateTime.Now,
                    Status = (int)StatusPushLoanToPartner.Cancel,
                    Comment = note
                });
                _unitOfWork.Save();
                return true;
            }
            else
                return false;
        }
    }
}
