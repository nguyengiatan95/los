﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IRequestEvent
    {
        List<RequestEvent> GetNotExcuted();
        bool Excuted(int id);
        bool Excuted(int id, DateTime requestAt);
    }
    public sealed class RequestEventService : IRequestEvent
    {
        private readonly IUnitOfWork _unitOfWork;
        public RequestEventService()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public List<RequestEvent> GetNotExcuted()
        {
            try
            {
                return _unitOfWork.RequestEventRepository.Query(x => x.IsExcuted.GetValueOrDefault() == 0, null, false).ToList();
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public bool Excuted(int id)
        {
            try
            {
                _unitOfWork.RequestEventRepository.Update(x => x.Id == id, x => new RequestEvent()
                {
                    IsExcuted = 1
                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }
        public bool Excuted(int id, DateTime requestAt)
        {
            try
            {
                _unitOfWork.RequestEventRepository.Update(x => x.Id == id, x => new RequestEvent()
                {
                    IsExcuted = 1,
                    RequestAt = requestAt

                });
                _unitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }
    }
}
