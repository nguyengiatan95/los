﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor
{
    public interface IRuleCheckLoanService
    {
        RuleCheckLoan GetByLoanbriefId(int loanbriefid);
    }
    public class RuleCheckLoanService: IRuleCheckLoanService
    {
        private readonly IUnitOfWork _unitOfWork;
        public RuleCheckLoanService()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public RuleCheckLoan GetByLoanbriefId(int loanbriefid)
        {
            try
            {
                return _unitOfWork.RuleCheckLoanRepository.Query(x => x.LoanbriefId == loanbriefid, null, false).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }
    }
}
