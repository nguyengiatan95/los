﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ISendMessage
    {
        void Send(string message, int loanbriefId);
        void SendNotifyToAppLender(NotifyLenderApp item, int LoanbriefId);
    }
    public class SendMessageService : ISendMessage
    {
        protected IConfiguration _baseConfig;
        private readonly ILogCallApiService _logCallApiService;
        public SendMessageService(ILogCallApiService logCallApiService)
        {
            _logCallApiService = logCallApiService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }
        public void Send(string message, int loanbriefId)
        {
            try
            {

                var username = _baseConfig["AppSettings:RabbitmqUser"];
                var Password = _baseConfig["AppSettings:RabbitmqPass"];
                var queue_notification = _baseConfig["AppSettings:QueueNotification"];
                var factory = new ConnectionFactory { HostName = _baseConfig["AppSettings:RabbitmqHost"], Port = 5673, UserName = username, Password = Password };
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.PushNotificationWeb,
                    LinkCallApi = queue_notification,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = "",
                    LoanBriefId = loanbriefId,
                    Input = message
                };
                objLogCallApi.Id = _logCallApiService.Insert(objLogCallApi);
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: queue_notification,
                                         durable: true,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);
                    var properties = channel.CreateBasicProperties();
                    properties.Persistent = true;
                    var body = Encoding.UTF8.GetBytes(message);
                    channel.BasicPublish(exchange: "",
                                         routingKey: queue_notification,
                                         basicProperties: properties,
                                         body: body);

                    objLogCallApi.Status = (int)StatusCallApi.Success;
                    objLogCallApi.Output = "OK";
                    _logCallApiService.Update(objLogCallApi);

                }

            }
            catch (Exception ex)
            {

            }
        }

        public void SendNotifyToAppLender(NotifyLenderApp item, int LoanbriefId)
        {
            try
            {
                try
                {
                    var domain = "http://103.35.65.122:7676/";
                        var url ="api/notifications/push";
                    var client = new RestClient(domain);
                    var request = new RestRequest(url);
                    request.Method = Method.POST;
                    request.AddHeader("Content-Type", "application/json");
                    var body = JsonConvert.SerializeObject(item);
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    var objLogCallApi = new LogCallApi
                    {
                        ActionCallApi = (int)ActionCallApi.PushToAppLender,
                        LinkCallApi = domain + url,
                        Status = (int)StatusCallApi.CallApi,
                        TokenCallApi = "",
                        LoanBriefId = LoanbriefId,
                        Input = body
                    };
                    objLogCallApi.Id = _logCallApiService.Insert(objLogCallApi);

                    var response = client.Execute(request);
                    var content = response.Content;
                    objLogCallApi.Status = (int)StatusCallApi.Success;
                    objLogCallApi.Output = content;
                    _logCallApiService.Update(objLogCallApi);
                }
                catch (Exception ex)
                {
                    Log.Error("SendNotifyToAppLender PostMessage " + ex.ToString());
                }
            }
            catch
            {

            }
        }
    }

}
