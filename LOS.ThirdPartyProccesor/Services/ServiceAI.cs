﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object;
using LOS.DAL.UnitOfWork;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Objects.ekyc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using static LOS.ThirdPartyProccesor.Objects.Ekyc;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IServiceAI
    {
        LocationAIRes VerifyAddress(LocationAIReq objData, int LogRequestId, bool reGetToken = false, int retry = 0);
        RefPhoneAIRes VerifyRefPhone(RefphoneReq objData, int LogRequestId, bool reGetToken = false, int retry = 0);
        RuleAIRes RegisterGetInfoAI(RuleAIReq objData, string url, int LogRequestId, int PushNextState = 0, bool reGetToken = false, int retry = 0);
        CreditScoringResult GetCreditScoring(string token, LoanBriefInfoCreditScoring entity, int LogRequestId);
        RangeCreditScoringRes RangeCreditScoring(string token, LoanBriefInfo entity, int LogRequestId);
        void PushLogLeadScore(LoanBriefInfoLog entity, int LogRequestId);
        List<ProductAiSupportItem> GetAlLProductSupport();
        InfoNetworkRes EkycInfoNetWork(InfoNetworkReq objData, int LogRequestId, bool reGetToken = false, int retry = 0);
        bool SendZns(SendZnsReq entity, int loanbriefId);
        FraudDetectionResponse RegisterFraudDetection(int loanbriefId, bool reGetToken = false, int retry = 0, int logRequestId = 0);
        InfoNetworkReq PrepareEkycInfoNetWorkModel(int loanbriefId, ref bool isCheck);
        EkycMotorbikeRegistrationCertificateRes EkycMotorbikeRegistrationCertificate(EkycMotorbikeRegistrationCertificateReq objData, int LogRequestId, bool reGetToken = false, int retry = 0);
        EkycMotorbikeRegistrationCertificateReq PrepareEkycMotorbikeRegistrationCertificateModel(int loanbriefId, ref bool isCheck);
        EkycInput PrepareEkycModel(int loanbriefId, ref bool IsCheck);
        EkycOutput EkycInfoNationalCard(EkycInput objData, int LogRequestId, bool reGetToken = false, int retry = 0);
        LocationAIReq PrepareCACModel(int loanbriefId, ref bool IsCheck);
        ParseVinNumberResponse ParseVinNumber(string token, string vinNumber, int logRequestId);
        CheckBank.OutPut CheckBank(int LoanBriefId, string STK, string BankId);
        FacebookInfoRes RegisterFacebook(string token, LoanBriefInfo entity, int LogRequestId);
        CheckLoanMomo.Output CheckLoanMomo(int loanBriefId, string nationalCard, string fromApp = "");
        OutputOpenContract OpenContract(string maHD, string deviceId, string shopid, int productId, string token, int loanbriefId, string contractType);
        bool CloseContract(string maHD, string deviceId, string token, int loanbriefId);
        LocationAIReq PrepareCACModelV2(LoanBriefVerifyDetail entity, ref bool isCheck);
        CreditScoringResult GetCreditScoring(string token, LoanBriefInfoCreditScoring entity);
    }
    public class ServiceAI : IServiceAI
    {
        protected IConfiguration _baseConfig;
        protected IAuthenticationAI _authenService;
        private ILogLoanInfoAIService _logLoanInfoAIService;
        private readonly ILogCallApiService _logCallApiService;
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IUnitOfWork _unitOfWork;
        private readonly INetworkService _networkService;
        private ILogLoanCheckMomoService _logLoanCheckMomoService;
        public ServiceAI(IAuthenticationAI authenService, ILogLoanInfoAIService logLoanInfoAIService, ILogCallApiService logCallApiService, ILoanBriefService loanBriefServices,
            INetworkService networkService, ILogLoanCheckMomoService logLoanCheckMomoService)
        {
            //_baseConfig = configuration;
            _authenService = authenService;
            _logLoanInfoAIService = logLoanInfoAIService;
            _logCallApiService = logCallApiService;
            _loanBriefServices = loanBriefServices;
            _networkService = networkService;
            _logLoanCheckMomoService = logLoanCheckMomoService;

            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(_baseConfig.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }
        private LocationAIRes api_verify_address(string token, LocationAIReq objData, int LogRequestId, int retry = 0)
        {
            try
            {
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.verify_address;
                var client = new RestClient(url);
                client.Timeout = 10000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    Id = LogRequestId,
                    Retry = retry,
                    ServiceType = ServiceTypeAI.Location.GetHashCode(),
                    Request = jsonInput,
                    Token = token,
                    Url = url,
                    HomeNetwok = objData.carrier
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.UpdateRequest(logLoanInfoAi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                logLoanInfoAi.Status = 1;//gọi thành công
                logLoanInfoAi.ResponseRequest = jsonResponse;
                _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.WaitHandle.GetHashCode());
                LocationAIRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<LocationAIRes>(jsonResponse);
                if (result != null)
                {
                    if (result.code != 0 && response.StatusCode != System.Net.HttpStatusCode.OK)
                        result.code = (int)response.StatusCode;

                    if (result.data != null && !string.IsNullOrEmpty(result.data.refCode))
                    {
                        logLoanInfoAi.RefCode = result.data.refCode;
                        _logLoanInfoAIService.UpdateRefcode(logLoanInfoAi);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor api_verify_address exception");
            }
            return null;
        }
        private RefPhoneAIRes api_verify_refphone(string token, RefphoneReq objData, int LogRequestId, int retry = 0)
        {
            try
            {
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.verify_refphone;
                var client = new RestClient(url);
                client.Timeout = 10000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    Id = LogRequestId,
                    ServiceType = ServiceTypeAI.RefPhone.GetHashCode(),
                    Request = jsonInput,
                    Token = token,
                    Url = url,
                    Retry = retry,
                    HomeNetwok = objData.carrier
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.UpdateRequest(logLoanInfoAi);
                if (logLoanInfoAi.Id > 0)
                {
                    IRestResponse response = client.Execute(request);
                    var jsonResponse = response.Content;
                    logLoanInfoAi.Status = 1;//gọi thành công
                    logLoanInfoAi.ResponseRequest = jsonResponse;
                    _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.WaitHandle.GetHashCode());
                    RefPhoneAIRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<RefPhoneAIRes>(jsonResponse);
                    if (result != null)
                    {
                        if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized || response.StatusCode == System.Net.HttpStatusCode.Forbidden)
                            result.code = 401;

                        if (result.data != null && !string.IsNullOrEmpty(result.data.refCode))
                        {
                            logLoanInfoAi.RefCode = result.data.refCode;
                            _logLoanInfoAIService.UpdateRefcode(logLoanInfoAi);
                        }
                    }
                    return result;
                }
                return null;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor api_verify_refphone exception");
            }
            return null;
        }
        private RuleAIRes api_register_rule_ai(string token, RuleAIReq entity, string urlCall, int LogRequestId, int PushNextState = 0, int retry = 0)
        {
            try
            {
                var environment = _baseConfig["Environments:RuleCheck"];
                var url = _baseConfig["AppSettings:AIAuthentication"] + string.Format(urlCall, environment);
                var body = JsonConvert.SerializeObject(entity);
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                request.AddParameter("application/json", body, ParameterType.RequestBody);

                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    Id = LogRequestId,
                    Request = body,
                    Token = token,
                    Url = url,
                    HomeNetwok = entity.phone.carrier,
                    PushNextState = PushNextState,
                    Retry = retry
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.UpdateRequest(logLoanInfoAi);
                if (logLoanInfoAi.Id > 0)
                {
                    IRestResponse response = client.Execute(request);
                    var jsonResponse = response.Content;
                    logLoanInfoAi.Status = 1;//gọi thành công
                    logLoanInfoAi.ResponseRequest = jsonResponse;
                    _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.WaitHandle.GetHashCode());
                    var result = JsonConvert.DeserializeObject<RuleAIRes>(jsonResponse);

                    if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized || response.StatusCode == System.Net.HttpStatusCode.Forbidden)
                        result.status = "401";
                    if (result != null && !string.IsNullOrEmpty(result.register_code))
                    {
                        logLoanInfoAi.RefCode = result.register_code;
                        _logLoanInfoAIService.UpdateRefcode(logLoanInfoAi);
                        //đăng ký request mới => hủy toàn bộ request chờ kết quả
                        // _logLoanInfoAIService.CancelRequestBefore(ServiceTypeAI.RuleCac.GetHashCode(), loanBriefId, logLoanInfoAi.Id);
                    }
                    return result;
                }
                return null;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor api_register_rule_ai exception");
                return null;
            }
        }

        private InfoNetworkRes api_ekyc_info_network(string token, InfoNetworkReq entity, int LogRequestId, int retry = 0)
        {
            try
            {
                var body = JsonConvert.SerializeObject(entity);
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.get_info_network;
                var client = new RestClient(url);
                client.Timeout = 15000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    Id = LogRequestId,
                    Retry = retry,
                    ServiceType = ServiceTypeAI.EkycGetInfoNetwork.GetHashCode(),
                    Request = body,
                    Token = token,
                    Url = url
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.UpdateRequest(logLoanInfoAi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                logLoanInfoAi.Status = 1;//gọi thành công
                logLoanInfoAi.ResponseRequest = jsonResponse;
                _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.Excuted.GetHashCode());
                InfoNetworkRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<InfoNetworkRes>(jsonResponse);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        private EkycMotorbikeRegistrationCertificateRes api_ekyc_motorbike_registration_certificate(string token, EkycMotorbikeRegistrationCertificateReq entity, int LogRequestId, int retry = 0)
        {
            try
            {
                var body = JsonConvert.SerializeObject(entity);
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.ekyc_motorbike_registration_certificate;
                var client = new RestClient(url);
                client.Timeout = 15000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    Id = LogRequestId,
                    Retry = retry,
                    ServiceType = ServiceTypeAI.EkycMotorbikeRegistrationCertificate.GetHashCode(),
                    Request = body,
                    Token = token,
                    Url = url
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.UpdateRequest(logLoanInfoAi);
                IRestResponse response = client.Execute(request);

                var jsonResponse = response.Content;
                logLoanInfoAi.Status = 1;//gọi thành công
                logLoanInfoAi.ResponseRequest = jsonResponse;
                _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.WaitHandle.GetHashCode());
                EkycMotorbikeRegistrationCertificateRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<EkycMotorbikeRegistrationCertificateRes>(jsonResponse);

                if (result.response_code == 200 && result.result != null && !string.IsNullOrEmpty(result.result.reference_code))
                {
                    logLoanInfoAi.RefCode = result.result.reference_code;
                    _logLoanInfoAIService.UpdateRefcode(logLoanInfoAi);
                }
                if (response.StatusCode != HttpStatusCode.OK)
                    result.response_code = (int)response.StatusCode;
                return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        private List<ProductAiSupportItem> api_get_all_product_support(string token)
        {
            try
            {
                var url = "https://gateway.tima-ai.dev/moto-price/get_list_supported_motor";
                var client = new RestClient(url);
                client.Timeout = 15000;
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                List<ProductAiSupportItem> result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProductAiSupportItem>>(jsonResponse);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        private bool api_send_zns(string token, SendZnsReq entity, int loanbriefId)
        {
            try
            {
                var url = "http://54.179.156.252:8800/chat-gateway/v1/ticket/send";
                var client = new RestClient(url);
                client.Timeout = 10000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(entity);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var logSendSms = new LogSendSms
                {
                    LoanbriefId = loanbriefId,
                    TypeSendSms = (int)EnumTypeSendSMS.SendZNS,
                    Url = url,
                    Smscontent = "",
                    Request = jsonInput,
                    Status = (int)StatusCallApi.Success,
                    CreatedAt = DateTime.Now
                };
                _unitOfWork.LogSendSmsRepository.Insert(logSendSms);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                _unitOfWork.LogSendSmsRepository.Update(x => x.Id == logSendSms.Id, x => new LogSendSms()
                {
                    Response = jsonResponse,
                    UpdatedAt = DateTime.Now
                });
                _unitOfWork.Save();
                SendZnsRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<SendZnsRes>(jsonResponse);
                return result.StatusCode == 0;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        private EkycOutput api_ekyc_info_national(string token, EkycInput entity, int LogRequestId, int retry = 0)
        {
            try
            {
                var body = JsonConvert.SerializeObject(entity);
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.GET_INFO_CHECK_EKYC;
                var client = new RestClient(url);
                client.Timeout = 15000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    Id = LogRequestId,
                    Retry = retry,
                    ServiceType = ServiceTypeAI.Ekyc.GetHashCode(),
                    Request = body,
                    Token = token,
                    Url = url
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.UpdateRequest(logLoanInfoAi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                logLoanInfoAi.Status = 1;//gọi thành công
                logLoanInfoAi.ResponseRequest = jsonResponse;
                _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.Excuted.GetHashCode());
                EkycOutput result = Newtonsoft.Json.JsonConvert.DeserializeObject<EkycOutput>(jsonResponse);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        private FraudDetectionResponse api_register_fraud_detection(string token, int loanbriefId, int retry = 0, int logRequestId = 0)
        {
            try
            {
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.register_fraud_detection;
                var client = new RestClient(url);
                client.Timeout = 10000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(new
                {
                    loan_brief_id = loanbriefId
                });
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    LoanbriefId = loanbriefId,
                    Retry = retry,
                    ServiceType = ServiceTypeAI.FraudDetection.GetHashCode(),
                    Request = jsonInput,
                    Token = token,
                    Url = url,
                    Status = 0,
                    CreatedAt = DateTime.Now,
                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.Excuted,
                    FromApp = "TDHS"
                };
                if (logRequestId > 0)
                {
                    logLoanInfoAi.Id = logRequestId;
                    logLoanInfoAi.Id = _logLoanInfoAIService.UpdateRequest(logLoanInfoAi);
                }
                else
                {
                    logLoanInfoAi.Id = _logLoanInfoAIService.InsertRequest(logLoanInfoAi);
                }

                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                logLoanInfoAi.Status = 0;//gọi thành công
                logLoanInfoAi.ResponseRequest = jsonResponse;
                _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, (int)EnumLogLoanInfoAiExcuted.Excuted);
                FraudDetectionResponse result = Newtonsoft.Json.JsonConvert.DeserializeObject<FraudDetectionResponse>(jsonResponse);
                if (result != null)
                {
                    if (result.response_code == 200 && result.result != null && !string.IsNullOrEmpty(result.result.reference_code))
                    {
                        logLoanInfoAi.RefCode = result.result.reference_code;
                        logLoanInfoAi.Status = 1;

                        _logLoanInfoAIService.UpdateRefcode(logLoanInfoAi);
                    }
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor api_register_fraud_detection exception");
            }
            return null;
        }

        public LocationAIRes VerifyAddress(LocationAIReq objData, int LogRequestId, bool reGetToken = false, int retry = 0)
        {
            var token = _authenService.GetToken(Constants.gps_device_tracking_app_id, Constants.gps_device_tracking_app_key, reGetToken);
            if (!string.IsNullOrEmpty(token))
            {
                var result = api_verify_address(token, objData, LogRequestId, retry);
                //nếu token hết hạn thì gọi lại
                if ((result.code == 401 || result.code == 503) && retry < 3)
                    VerifyAddress(objData, LogRequestId, true, retry++);
                return result;
            }
            return null;
        }
        public RuleAIRes RegisterGetInfoAI(RuleAIReq objData, string url, int LogRequestId, int PushNextState = 0, bool reGetToken = false, int retry = 0)
        {
            var token = _authenService.GetToken(Constants.gps_device_tracking_app_id, Constants.gps_device_tracking_app_key, reGetToken);
            if (!string.IsNullOrEmpty(token))
            {
                var result = api_register_rule_ai(token, objData, url, LogRequestId, PushNextState, retry);
                //nếu token hết hạn thì gọi lại
                if (result.status == "401" && retry < 3)
                    RegisterGetInfoAI(objData, url, LogRequestId, 0, true, retry++);
                return result;
            }
            return null;
        }
        public RefPhoneAIRes VerifyRefPhone(RefphoneReq objData, int LogRequestId, bool reGetToken = false, int retry = 0)
        {
            var token = _authenService.GetToken(Constants.gps_device_tracking_app_id, Constants.gps_device_tracking_app_key, reGetToken);
            if (!string.IsNullOrEmpty(token))
            {
                var result = api_verify_refphone(token, objData, LogRequestId, retry);
                //nếu token hết hạn thì gọi lại
                if (result.code == 401 && retry < 3)
                    VerifyRefPhone(objData, LogRequestId, true, retry++);
                return result;
            }
            return null;
        }

        public InfoNetworkRes EkycInfoNetWork(InfoNetworkReq objData, int LogRequestId, bool reGetToken = false, int retry = 0)
        {
            var token = _authenService.GetToken(Constants.gps_device_tracking_app_id, Constants.gps_device_tracking_app_key, reGetToken);
            if (!string.IsNullOrEmpty(token))
            {
                var result = api_ekyc_info_network(token, objData, LogRequestId, retry);
                //nếu token hết hạn thì gọi lại
                if ((result.response_code == "401" || result.mess == "503") && retry < 3)
                    EkycInfoNetWork(objData, LogRequestId, true, retry++);
                return result;
            }
            return null;
        }

        public FraudDetectionResponse RegisterFraudDetection(int loanbriefId, bool reGetToken = false, int retry = 0, int logRequestId = 0)
        {
            var token = _authenService.GetToken(Constants.gps_device_tracking_app_id, Constants.gps_device_tracking_app_key, reGetToken);
            if (!string.IsNullOrEmpty(token))
            {
                var result = api_register_fraud_detection(token, loanbriefId, retry, logRequestId);
                //nếu token hết hạn thì gọi lại
                if (result != null && (result.status == 503 || result.response_code == 401 || result.response_code == 503) && retry < 3)
                    RegisterFraudDetection(loanbriefId, true, retry == 0 ? 1 : retry + 1, logRequestId);
                return result;
            }
            return null;
        }

        public EkycMotorbikeRegistrationCertificateRes EkycMotorbikeRegistrationCertificate(EkycMotorbikeRegistrationCertificateReq objData, int LogRequestId, bool reGetToken = false, int retry = 0)
        {
            var token = _authenService.GetToken(Constants.gps_device_tracking_app_id, Constants.gps_device_tracking_app_key, reGetToken);
            if (!string.IsNullOrEmpty(token))
            {
                var result = api_ekyc_motorbike_registration_certificate(token, objData, LogRequestId, retry);
                //nếu token hết hạn thì gọi lại
                if ((result.response_code == 401 || result.mess == "503") && retry < 3)
                    EkycMotorbikeRegistrationCertificate(objData, LogRequestId, true, retry++);
                return result;
            }
            return null;
        }

        public ParseVinNumberResponse ParseVinNumber(string token, string vinNumber, int logRequestId)
        {
            try
            {
                var url = _baseConfig["AppSettings:AIAuthentication"] + string.Format(Constants.parse_vin_number, vinNumber);
                var client = new RestClient(url);
                client.Timeout = 15000;
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                // Lưu vào LogLoanInfoAI set IsExcuted = 2
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    Id = logRequestId,
                    ServiceType = ServiceTypeAI.ParseVinNumber.GetHashCode(),
                    Request = vinNumber,
                    Token = token,
                    Url = url,
                    CreatedAt = DateTime.Now
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.UpdateRequest(logLoanInfoAi);
                if (logLoanInfoAi.Id > 0)
                {
                    IRestResponse response = client.Execute(request);
                    var jsonResponse = response.Content;
                    logLoanInfoAi.Status = 1;//gọi thành công
                    logLoanInfoAi.ResponseRequest = jsonResponse;
                    //cập nhật luôn đã có kết quả
                    _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.Excuted.GetHashCode());
                    ParseVinNumberResponse result = Newtonsoft.Json.JsonConvert.DeserializeObject<ParseVinNumberResponse>(jsonResponse);
                    if (result.status_code == 200 && result.result != null && !string.IsNullOrEmpty(result.result.full_name))
                        logLoanInfoAi.ResultFinal = "SUCCESS";
                    else
                        logLoanInfoAi.ResultFinal = "ERROR";
                    _logLoanInfoAIService.Done(logLoanInfoAi);
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }


        public InfoNetworkReq PrepareEkycInfoNetWorkModel(int loanbriefId, ref bool isCheck)
        {
            isCheck = false;
            try
            {
                var loanbrief = _unitOfWork.LoanBriefRepository.GetById(loanbriefId);
                if (loanbrief != null)
                {
                    var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                    var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
                    var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];

                    var result = new InfoNetworkReq()
                    {
                        LoanbriefId = loanbrief.LoanBriefId,
                        BirthDay = loanbrief.Dob.HasValue ? loanbrief.Dob.Value.ToString("dd/MM/yyyy") : "",
                        FullName = loanbrief.FullName,
                        NationalCard = loanbrief.NationalCard
                    };
                    //Ảnh thông tin thuê bao
                    var fileTTTB = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanbrief.LoanBriefId && x.Status == 1 && x.IsDeleted != 1
                    && (x.TypeId == (int)EnumDocumentType.TTTB), null, false).FirstOrDefault();
                    if (fileTTTB != null && !string.IsNullOrEmpty(fileTTTB.FilePath))
                    {
                        if (!fileTTTB.FilePath.Contains("http") && (fileTTTB.MecashId == null || fileTTTB.MecashId == 0))
                            fileTTTB.FilePath = ServiceURL + fileTTTB.FilePath;
                        else if (fileTTTB.MecashId > 0 && (fileTTTB.S3status == 0 || fileTTTB.S3status == null))
                            fileTTTB.FilePath = ServiceURLAG + fileTTTB.FilePath;
                        else if (fileTTTB.MecashId > 0 && fileTTTB.S3status == 1)
                            fileTTTB.FilePath = ServiceURLFileTima + fileTTTB.FilePath;

                        result.ImageUrl = fileTTTB.FilePath;
                        isCheck = true;
                        return result;
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public List<ProductAiSupportItem> GetAlLProductSupport()
        {
            var token = _authenService.GetToken(Constants.gps_device_tracking_app_id, Constants.gps_device_tracking_app_key);
            if (!string.IsNullOrEmpty(token))
            {
                var result = api_get_all_product_support(token);
                return result;
            }
            return null;
        }

        public bool SendZns(SendZnsReq entity, int loanbriefId)
        {
            var token = _authenService.GetToken(Constants.chat_app_id, Constants.chat_app_key);
            if (!string.IsNullOrEmpty(token))
            {
                var result = api_send_zns(token, entity, loanbriefId);
                return result;
            }
            return false;

        }
        //public LeadScoreResult GetLeadScore(LoanCreditForLeadScoreItem entity, int loanbiefId)
        //{
        //    var token = _authenService.GetToken(Constants.gps_device_tracking_app_id, Constants.gps_device_tracking_app_key);
        //    if (!string.IsNullOrEmpty(token))
        //        return api_get_lead_score(token, entity, loanbiefId);
        //    return null;
        //}

        public RangeCreditScoringRes RangeCreditScoring(string token, LoanBriefInfo entity, int LogRequestId)
        {
            try
            {
                var body = JsonConvert.SerializeObject(entity);
                var environment = _baseConfig["Environments:CreditScoring"];
                var url = _baseConfig["AppSettings:AIAuthentication"] + string.Format(Constants.GET_RANGE_CREDIT_SCORE, environment);
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    Id = LogRequestId,
                    ServiceType = ServiceTypeAI.RangeCreditScoring.GetHashCode(),
                    Request = body,
                    Token = token,
                    Url = url
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.UpdateRequest(logLoanInfoAi);
                if (logLoanInfoAi.Id > 0)
                {
                    IRestResponse response = client.Execute(request);
                    var jsonResponse = response.Content;
                    logLoanInfoAi.Status = 1;//gọi thành công
                    logLoanInfoAi.ResponseRequest = jsonResponse;
                    _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.WaitHandle.GetHashCode());
                    RangeCreditScoringRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<RangeCreditScoringRes>(jsonResponse);
                    if (result != null)
                    {
                        if (result.StatusCode == 200 && !string.IsNullOrEmpty(result.RequestID))
                        {
                            logLoanInfoAi.RefCode = result.RequestID;
                            _logLoanInfoAIService.UpdateRefcode(logLoanInfoAi);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public CreditScoringResult GetCreditScoring(string token, LoanBriefInfoCreditScoring entity, int logRequestId)
        {
            try
            {
                var body = JsonConvert.SerializeObject(entity);
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.GET_CREDIT_SCORING;
                var client = new RestClient(url);
                client.Timeout = 15000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                // Lưu vào LogLoanInfoAI set IsExcuted = 2
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    Id = logRequestId,
                    ServiceType = ServiceTypeAI.CreditScoring.GetHashCode(),
                    Request = body,
                    Token = token,
                    Url = url
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.UpdateRequest(logLoanInfoAi);
                if (logLoanInfoAi.Id > 0)
                {
                    IRestResponse response = client.Execute(request);
                    var jsonResponse = response.Content;
                    logLoanInfoAi.Status = 0;
                    logLoanInfoAi.ResponseRequest = jsonResponse;
                    //cập nhật luôn đã có kết quả
                    _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.Excuted.GetHashCode());
                    CreditScoringResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<CreditScoringResult>(jsonResponse);
                    if(result != null && result.score > 0)
                    {
                        logLoanInfoAi.Status = 1;//gọi thành công
                        //cập nhật luôn đã có kết quả
                        _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.Excuted.GetHashCode());
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public CreditScoringResult GetCreditScoring(string token, LoanBriefInfoCreditScoring entity)
        {
            try
            {
                var body = JsonConvert.SerializeObject(entity);
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.GET_CREDIT_SCORING;
                var client = new RestClient(url);
                client.Timeout = 15000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    ServiceType = ServiceTypeAI.CreditScoring.GetHashCode(),
                    Request = body,
                    Token = token,
                    Url = url,
                    CreatedAt = DateTime.Now,
                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.Excuted,
                    //FromApp = "RE_GET",
                    LoanbriefId = entity.ID
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.InsertRequest(logLoanInfoAi);
                if (logLoanInfoAi.Id > 0)
                {
                    IRestResponse response = client.Execute(request);
                    var jsonResponse = response.Content;
                    logLoanInfoAi.Status = 0;
                    logLoanInfoAi.ResponseRequest = jsonResponse;
                    //cập nhật luôn đã có kết quả
                    _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.Excuted.GetHashCode());
                    CreditScoringResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<CreditScoringResult>(jsonResponse);
                    if (result != null && result.score > 0)
                    {
                        logLoanInfoAi.Status = 1;//gọi thành công
                        //cập nhật luôn đã có kết quả
                        _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.Excuted.GetHashCode());
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public void PushLogLeadScore(LoanBriefInfoLog entity, int LogRequestId)
        {
            try
            {
                var body = JsonConvert.SerializeObject(entity);
                var url = Constants.push_log_lead_score;
                var client = new RestClient(url);
                client.Timeout = 15000;
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    Id = LogRequestId,
                    ServiceType = ServiceTypeAI.PushLogLeadScore.GetHashCode(),
                    Request = body,
                    Token = "",
                    Url = url
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.UpdateRequest(logLoanInfoAi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                logLoanInfoAi.Status = 1;//gọi thành công
                logLoanInfoAi.ResponseRequest = jsonResponse;
                //cập nhật luôn đã có kết quả
                _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.Excuted.GetHashCode());
            }
            catch (Exception ex)
            {
            }
        }


        public EkycMotorbikeRegistrationCertificateReq PrepareEkycMotorbikeRegistrationCertificateModel(int loanbriefId, ref bool isCheck)
        {
            isCheck = false;
            try
            {
                var model = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(x => new EkycMotorbikeRegistrationCertificateReq()
                {
                    brand = x.LoanBriefProperty.Brand.Name,
                    LoanbriefId = x.LoanBriefId,
                    FullName = x.FullName,
                    Ownership = x.ProductId == (int)EnumProductCredit.MotorCreditType_CC || x.ProductId == (int)EnumProductCredit.LoanFastMoto_CC,
                    NumberPlate = x.LoanBriefProperty.PlateNumber
                }).FirstOrDefault();
                if (model != null && !string.IsNullOrEmpty(model.brand))
                {
                    var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                    var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
                    var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
                    //Giấy đăng ký xe
                    var listFiles = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == model.LoanbriefId && x.Status == 1 && x.IsDeleted != 1
                    && (x.TypeId == (int)EnumDocumentType.Motorbike_Registration_Certificate || x.TypeId == (int)EnumDocumentType.Motorbike_Registration_Certificate_Light), null, false).ToList();
                    if (listFiles != null && listFiles.Count > 0)
                    {
                        foreach (var item in listFiles)
                        {
                            if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                                item.FilePath = ServiceURL + item.FilePath;
                            else if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null))
                                item.FilePath = ServiceURLAG + item.FilePath;
                            else if (item.MecashId > 0 && item.S3status == 1)
                                item.FilePath = ServiceURLFileTima + item.FilePath;
                        }

                        model.images = listFiles.Select(x => x.FilePath).ToList();
                        isCheck = true;
                        return model;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public EkycInput PrepareEkycModel(int loanbriefId, ref bool IsCheck)
        {
            try
            {
                var item = _unitOfWork.LoanBriefRepository.GetById(loanbriefId);
                IsCheck = true;
                var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
                var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
                var listLoanBriefFile = _loanBriefServices.GetFilesByLoanId(item.LoanBriefId);
                var listDocument = _loanBriefServices.GetDocumentTypeProductId(item.ProductId.Value);
                if (item.Dob == null)
                {
                    IsCheck = false;
                }

                var lstCMND = listLoanBriefFile.Where(x => x.TypeId == (int)EnumDocumentType.CMND_CCCD).ToList();
                if (lstCMND.Count < 2)
                    IsCheck = false;
                if (!IsCheck)
                    return null;
                foreach (var f in lstCMND)
                {
                    if (!f.FilePath.Contains("http") && (f.MecashId == null || f.MecashId == 0))
                        f.FilePath = ServiceURL + f.FilePath;
                    if (f.MecashId > 0 && (f.S3status == 0 || f.S3status == null))
                        f.FilePath = ServiceURLAG + f.FilePath;
                    if (f.MecashId > 0 && f.S3status == 1)
                        f.FilePath = ServiceURLFileTima + f.FilePath;
                }
                var obj = new EkycInput
                {
                    FrontId = lstCMND[0].FilePath,
                    BackId = lstCMND[1].FilePath,
                    Selfie = "",
                    Fullname = item.FullName,
                    Birthday = item.Dob != null ? item.Dob.Value.ToString("dd-MM-yyyy") : "",
                    NationalId = item.NationalCard
                };
                return obj;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public EkycOutput EkycInfoNationalCard(EkycInput objData, int LogRequestId, bool reGetToken = false, int retry = 0)
        {
            var token = _authenService.GetToken(Constants.gps_device_tracking_app_id, Constants.gps_device_tracking_app_key, reGetToken);
            if (!string.IsNullOrEmpty(token))
            {
                var result = api_ekyc_info_national(token, objData, LogRequestId, retry);
                return result;
            }
            return null;
        }
        /// <summary>
        /// Tạo model request lấy dữ liệu nhà mạng CAC
        /// </summary>
        /// <param name="loanbriefId"></param>
        /// <param name="IsCheck">Mã lỗi trả ra: </param>
        /// <returns></returns>
        public LocationAIReq PrepareCACModel(int loanbriefId, ref bool isCheck)
        {
            try
            {
                var entity = _unitOfWork.LoanBriefRepository.GetById(loanbriefId);
                isCheck = false;
                if (entity.HomeNetwork.GetValueOrDefault(0) == 0)
                    entity.HomeNetwork = _networkService.CheckHomeNetwok(entity.Phone);
                string NetworkName = Enum.IsDefined(typeof(HomeNetWorkMobile), entity.HomeNetwork) ? ExtensionHelper.GetDescription((HomeNetWorkMobile)entity.HomeNetwork) : string.Empty;
                if (!string.IsNullOrEmpty(NetworkName))
                {
                    var webhook = string.Empty;
                    if (entity.HomeNetwork == HomeNetWorkMobile.Viettel.GetHashCode())
                        webhook = _baseConfig["AppSettings:DomainAPILOS"] + Constants.webhook_location_viettel;
                    else if (entity.HomeNetwork == HomeNetWorkMobile.Mobi.GetHashCode())
                        webhook = _baseConfig["AppSettings:DomainAPILOS"] + Constants.webhook_location_mobi;

                    int age = 0;
                    if (entity.Dob.HasValue)
                    {
                        age = DateTime.Now.Year - entity.Dob.Value.Year;
                        if (DateTime.Now.DayOfYear < entity.Dob.Value.DayOfYear)
                            age = age - 1;
                    }
                    var addressHome = GetLatLng((int)TypeGetLatLng.Home, entity.LoanBriefId);
                    var addressCompany = GetLatLng((int)TypeGetLatLng.Company, entity.LoanBriefId);

                    if (string.IsNullOrEmpty(addressHome) && string.IsNullOrEmpty(addressCompany))
                        return null;
                    //nếu có chứng từ nhà, k có chứng từ công ty thì gán
                    if (!string.IsNullOrEmpty(addressHome) && string.IsNullOrEmpty(addressCompany))
                        addressCompany = addressHome;
                    if (string.IsNullOrEmpty(addressHome) && !string.IsNullOrEmpty(addressCompany))
                        addressHome = addressCompany;
                    var objData = new LocationAIReq()
                    {
                        locationCodes = new LocationCode()
                        {
                            home = addressHome,
                            office = addressCompany,
                            other = string.Empty
                        },
                        phone = entity.Phone,
                        age = age,
                        url = webhook,
                        carrier = !string.IsNullOrEmpty(NetworkName) ? NetworkName.ToLower() : "",
                        office = true,
                        validateAddress = false
                    };
                    isCheck = true;
                    return objData;
                }

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public LocationAIReq PrepareCACModelV2(LoanBriefVerifyDetail entity, ref bool isCheck)
        {
            try
            {
                isCheck = false;
                if (entity.HomeNetwork == 0)
                    entity.HomeNetwork = _networkService.CheckHomeNetwok(entity.Phone);
                string NetworkName = Enum.IsDefined(typeof(HomeNetWorkMobile), entity.HomeNetwork) ? ExtensionHelper.GetDescription((HomeNetWorkMobile)entity.HomeNetwork) : string.Empty;
                if (!string.IsNullOrEmpty(NetworkName))
                {
                    var webhook = string.Empty;
                    if (entity.HomeNetwork == HomeNetWorkMobile.Viettel.GetHashCode())
                        webhook = _baseConfig["AppSettings:DomainAPILOS"] + Constants.webhook_location_viettel;
                    else if (entity.HomeNetwork == HomeNetWorkMobile.Mobi.GetHashCode())
                        webhook = _baseConfig["AppSettings:DomainAPILOS"] + Constants.webhook_location_mobi;

                    int age = 0;
                    if (entity.Dob.HasValue)
                    {
                        age = DateTime.Now.Year - entity.Dob.Value.Year;
                        if (DateTime.Now.DayOfYear < entity.Dob.Value.DayOfYear)
                            age = age - 1;
                    }
                    var addressHome = GetLatLng((int)TypeGetLatLng.Home, entity.LoanBriefId);
                    var addressCompany = GetLatLng((int)TypeGetLatLng.Company, entity.LoanBriefId);

                    if (string.IsNullOrEmpty(addressHome) && string.IsNullOrEmpty(addressCompany))
                        return null;
                    //nếu có chứng từ nhà, k có chứng từ công ty thì gán
                    if (!string.IsNullOrEmpty(addressHome) && string.IsNullOrEmpty(addressCompany))
                        addressCompany = addressHome;
                    if (string.IsNullOrEmpty(addressHome) && !string.IsNullOrEmpty(addressCompany))
                        addressHome = addressCompany;
                    var objData = new LocationAIReq()
                    {
                        locationCodes = new LocationCode()
                        {
                            home = addressHome,
                            office = addressCompany,
                            other = string.Empty
                        },
                        phone = entity.Phone,
                        age = age,
                        url = webhook,
                        carrier = !string.IsNullOrEmpty(NetworkName) ? NetworkName.ToLower() : "",
                        office = true,
                        validateAddress = false
                    };
                    isCheck = true;
                    return objData;
                }

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        private string GetLatLng(int typeGetLatLng, int loanbriefId)
        {
            try
            {
                if (typeGetLatLng == (int)TypeGetLatLng.Home)
                {
                    //Kiểm tra nếu có ảnh chụp trong nhà app tima care => lấy tọa độ
                    //Hoặc video do KH quay trong nhà
                    //var image = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanbriefId && !string.IsNullOrEmpty(x.LatLong)
                    ////&& x.SourceUpload == EnumSourceUpload.TimaCare.GetHashCode()
                    // && (x.TypeId == EnumDocumentType.CVKD_In_Home_Customer.GetHashCode()
                    //        || x.TypeId == EnumDocumentType.CVKD_CheckOut_Home_Customer.GetHashCode()
                    //           || x.TypeId == EnumDocumentType.Tima_Care_Document_In_Home.GetHashCode()
                    //        || x.TypeId == EnumDocumentType.Tima_Care_Video_In_Home.GetHashCode())

                    //&& x.IsDeleted != 1
                    //, null, false).OrderByDescending(x => x.TypeId).FirstOrDefault();
                    //if (image != null)
                    //    return image.LatLong;

                    //
                    var latLngHome = _unitOfWork.LoanBriefResidentRepository.Query(x => x.LoanBriefResidentId == loanbriefId, null, false).Select(x => new
                    {
                        AddressLatLng = x.AddressLatLng,
                        Address = x.Address
                    }).FirstOrDefault();
                    if (latLngHome != null)
                        if (!string.IsNullOrEmpty(latLngHome.AddressLatLng))
                            return latLngHome.AddressLatLng;
                        else return latLngHome.Address;
                    return string.Empty;
                }
                else if (typeGetLatLng == (int)TypeGetLatLng.Company)
                {
                    //Kiểm tra nếu có ảnh chụp trong công ty app tima care => lấy tọa độ
                    //Hoặc video do KH quay trong công ty
                    //var image = _unitOfWork.LoanBriefFileRepository.Get(x => x.LoanBriefId == loanbriefId && !string.IsNullOrEmpty(x.LatLong)
                    //// && x.SourceUpload == EnumSourceUpload.TimaCare.GetHashCode()
                    //&& (x.TypeId == (int)EnumDocumentType.CVKD_In_Company_Customer
                    //        || x.TypeId == (int)EnumDocumentType.CVKD_CheckOut_Company_Customer
                    //        || x.TypeId == EnumDocumentType.Tima_Care_Document_In_Company.GetHashCode()
                    //        || x.TypeId == (int)EnumDocumentType.Tima_Care_Video_In_Company)
                    //&& x.IsDeleted != 1
                    //, null, false).OrderByDescending(x => x.TypeId).FirstOrDefault();
                    //if (image != null)
                    //    return image.LatLong;

                    var latLngCompany = _unitOfWork.LoanBriefJobRepository.Query(x => x.LoanBriefJobId == loanbriefId, null, false).Select(x => new
                    {
                        CompanyAddressLatLng = x.CompanyAddressLatLng,
                        CompanyAddress = x.CompanyAddress
                    }).FirstOrDefault();
                    if (latLngCompany != null)
                        if (!string.IsNullOrEmpty(latLngCompany.CompanyAddressLatLng))
                            return latLngCompany.CompanyAddressLatLng;
                        else return latLngCompany.CompanyAddress;
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public CheckBank.OutPut CheckBank(int LoanBriefId, string STK, string BankId)
        {
            try
            {
                var url = _baseConfig["AppSettings:CheckBank"] + String.Format(Constants.CHECK_BANK_ENDPOINT, BankId, STK);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.CheckBankAI,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    Input = url,
                    CreatedAt = DateTime.Now,
                    LoanBriefId = LoanBriefId
                };
                _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                _unitOfWork.Save();
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == objLogCallApi.Id, x => new LogCallApi()
                {
                    Output = jsonResponse,
                    Status = (int)StatusCallApi.Success,
                    ModifyAt = DateTime.Now
                });
                _unitOfWork.Save();

                CheckBank.OutPut lst = Newtonsoft.Json.JsonConvert.DeserializeObject<CheckBank.OutPut>(jsonResponse);
                return lst;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public FacebookInfoRes RegisterFacebook(string token, LoanBriefInfo entity, int LogRequestId)
        {
            try
            {
                var url = _baseConfig["AppSettings:AIAuthentication"] + string.Format(Constants.FACEBOOK_PROFILE, entity.Phone);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    Id = LogRequestId,
                    ServiceType = ServiceTypeAI.FacebookInfo.GetHashCode(),
                    Request = string.Empty,
                    Token = token,
                    Url = url
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.UpdateRequest(logLoanInfoAi);
                if (logLoanInfoAi.Id > 0)
                {
                    IRestResponse response = client.Execute(request);
                    var jsonResponse = response.Content;
                    logLoanInfoAi.Status = 1;//gọi thành công
                    logLoanInfoAi.ResponseRequest = jsonResponse;
                    _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.Excuted.GetHashCode());
                    FacebookInfoRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<FacebookInfoRes>(jsonResponse);
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public CheckLoanMomo.Output CheckLoanMomo(int loanBriefId, string nationalCard, string fromApp = "")
        {
            try
            {
                var url = _baseConfig["AppSettings:LinkAILoans"] + String.Format(Constants.CHECK_LOAN_MOMO, nationalCard);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Timeout = 20000;
                request.AddHeader("Content-Type", "application/json");
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    ServiceType = ServiceTypeAI.CheckLoanMomo.GetHashCode(),
                    LoanbriefId = loanBriefId,
                    Url = url,
                    Request= nationalCard,
                    CreatedAt = DateTime.Now,
                    FromApp = fromApp ,
                    IsExcuted = (int) EnumLogLoanInfoAiExcuted.Excuted
                };
                logLoanInfoAi.Id = _logLoanInfoAIService.AddLog(logLoanInfoAi);
                if (logLoanInfoAi.Id > 0)
                {
                    IRestResponse response = client.Execute(request);
                    var jsonResponse = response.Content;
                    logLoanInfoAi.Status = 0;
                    logLoanInfoAi.ResponseRequest = jsonResponse;
                    _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi);
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<CheckLoanMomo.Output>(jsonResponse);
                    if (result != null && result.Status == 0)
                    {
                        logLoanInfoAi.Status = 1;
                        _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi);
                        if (result.Data.Bills.Count > 0)
                        {
                            foreach (var item in result.Data.Bills)
                            {
                                //Lưu log check Momo
                                var objLogCheck = new LogLoanInfoPartner
                                {
                                    LoanbriefId = loanBriefId,
                                    BillId = item.BillId,
                                    ServiceName = item.ServiceName,
                                    TotalAmount = item.TotalAmount,
                                    PartnerName = item.PartnerName,
                                    PartnerPhone = item.PartnerPhone,
                                    PartnerEmail = item.PartnerEmail,
                                    PartnerPersonalId = item.PartnerPersonalId,
                                    ExpiredDate = item.ExpiredDate,
                                    Status = item.Status,
                                    CreatedDate = DateTime.Now
                                };
                                _logLoanCheckMomoService.AddLog(objLogCheck);
                            }
                        }                       
                        return result;
                    }                   
                }

            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public OutputOpenContract OpenContract(string maHD, string deviceId, string shopid, int productId, string token, int loanbriefId, string contractType)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.open_contract;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new InputOpenContract()
                {
                    contractid = maHD,
                    imei = deviceId,
                    shopid = shopid,
                    vehicle_type = (productId == EnumProductCredit.CamotoCreditType.GetHashCode() || productId == EnumProductCredit.OtoCreditType_CC.GetHashCode()
                    || productId == EnumProductCredit.OtoCreditType_KCC.GetHashCode() || productId == EnumProductCredit.LoanFastCar_CC.GetHashCode()) ? "oto" : "xe may",
                    contract_type = contractType
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);

                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.OpenContract,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = token,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput,
                    CreatedAt = DateTime.Now
                };
                var insertLogCallApi = _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == insertLogCallApi.Id, x => new LogCallApi()
                {
                    Status = (int)StatusCallApi.Success,
                    ModifyAt = DateTime.Now,
                    Output = jsonResponse,
                });
                _unitOfWork.Save();
                OutputOpenContract result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputOpenContract>(jsonResponse);
                return result;// (result != null && result.StatusCode == 200);
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        public bool CloseContract(string maHD, string deviceId, string token, int loanbriefId)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.close_contract;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new InputCloseContract()
                {
                    contractid = maHD,
                    imei = deviceId
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.CloseContract,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = token,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput,
                    CreatedAt = DateTime.Now
                };
                var insertLogCallApi = _unitOfWork.LogCallApiRepository.Insert(objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                _unitOfWork.LogCallApiRepository.Update(x => x.Id == insertLogCallApi.Id, x => new LogCallApi()
                {
                    Status = (int)StatusCallApi.Success,
                    ModifyAt = DateTime.Now,
                    Output = jsonResponse,
                });
                _unitOfWork.Save();
                OutputCloseContract result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputCloseContract>(jsonResponse);
                return (result != null && result.StatusCode == 200);
            }
            catch (Exception ex)
            {

            }

            return false;
        }

    }

}
