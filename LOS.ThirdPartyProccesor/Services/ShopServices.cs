﻿using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IShopService
    {
        Shop GetHubByWard(int WardId, int productId);
        Shop GetHubByWardDV(int WardId, int productId);
        Shop GetHubByWardAll(int WardId, int productId);
        Shop GetHubByDistrict(int DistrictId, int productId);
        Shop GetHubByDistrictDV(int DistrictId, int productId);
        Shop GetHubByDistrictAll(int DistrictId, int productId);
        bool UpdateNumberReceived(int ShopId);
        Shop GetById(int ShopId);
        bool CheckAreaSupport(int shopId, int productId, int districtId);
        
    }
    public class ShopServices: IShopService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ShopServices()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }
        public Shop GetHubByWard(int WardId,int productId)
        {
            try
            {
                return _unitOfWork.ManagerAreaHubRepository.Query(x => x.WardId == WardId && x.Shop.Status == 1 && x.Shop.IsReceiveLoan == true && x.Shop.HubType == 2 && (x.ProductId == -1 || x.ProductId == productId),
                    x => x.OrderBy(k => k.Shop.Received), false, x => x.Shop).Select(x => x.Shop).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Shop GetHubByWardDV(int WardId,int productId)
        {
            try
            {
                return _unitOfWork.ManagerAreaHubRepository.Query(x => x.WardId == WardId && x.Shop.Status == 1 && x.Shop.IsReceiveLoan == true && x.Shop.HubType == 1 && (x.ProductId == -1 || x.ProductId == productId),
                    x => x.OrderBy(k => k.Shop.Received), false, x => x.Shop).Select(x => x.Shop).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Shop GetHubByWardAll(int WardId, int productId)
        {
            try
            {
                return _unitOfWork.ManagerAreaHubRepository.Query(x => x.WardId == WardId && x.Shop.Status == 1 && x.Shop.IsReceiveLoan == true && (x.ProductId == -1 || x.ProductId == productId),
                    x => x.OrderBy(k => k.Shop.Received), false, x => x.Shop).Select(x => x.Shop).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Shop GetHubByDistrict(int DistrictId,int productId)
        {
            try
            {
                return _unitOfWork.ManagerAreaHubRepository.Query(x => x.DistrictId == DistrictId && x.Shop.Status == 1 && x.Shop.IsReceiveLoan == true && x.Shop.HubType == 2 && (x.ProductId == -1 || x.ProductId == productId),
                    x => x.OrderBy(k => k.Shop.Received), false, x => x.Shop).Select(x => x.Shop).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Shop GetHubByDistrictDV(int DistrictId, int productId)
        {
            try
            {
                return _unitOfWork.ManagerAreaHubRepository.Query(x => x.DistrictId == DistrictId && x.Shop.Status == 1 && x.Shop.IsReceiveLoan == true && x.Shop.HubType == 1 && (x.ProductId == -1 || x.ProductId == productId),
                    x => x.OrderBy(k => k.Shop.Received), false, x => x.Shop).Select(x => x.Shop).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Shop GetHubByDistrictAll(int DistrictId, int productId)
        {
            try
            {
                return _unitOfWork.ManagerAreaHubRepository.Query(x => x.DistrictId == DistrictId && x.Shop.Status == 1 && x.Shop.IsReceiveLoan == true && (x.ProductId == -1 || x.ProductId == productId),
                    x => x.OrderBy(k => k.Shop.Received), false, x => x.Shop).Select(x => x.Shop).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateNumberReceived(int ShopId)
        {
            try
            {
                var entity = _unitOfWork.ShopRepository.GetById(ShopId);
                if (entity != null && entity.ShopId > 0)
                {
                    entity.Received += 1;
                    _unitOfWork.ShopRepository.Update(entity);
                    _unitOfWork.Save();
                    return true;
                }
            }
            catch (Exception ex)
            {
                
            }
            return false;
        }

        public Shop GetById(int ShopId)
        {
            try
            {
                return _unitOfWork.ShopRepository.GetById(ShopId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool CheckAreaSupport(int shopId, int productId, int districtId)
        {
            try
            {
                return _unitOfWork.ManagerAreaHubRepository.Any(x => x.ShopId == shopId && x.DistrictId == districtId
                && (x.ProductId == -1 || x.ProductId == productId));
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
