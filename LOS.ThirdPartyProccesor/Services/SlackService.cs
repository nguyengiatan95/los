﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ISlackService
    {
        void SendNotifyOfStack(string author_name, string colorText, string title, string text, string urlApi, int loanbriefId = 0, string department = "");
        List<LogCallApi> GetHistory(int loanbriefid, string department = "");
    }
    public class SlackService : ISlackService
    {
        protected IConfiguration _baseConfig;
        private readonly ILogCallApiService _logCallApiService;
        public SlackService(ILogCallApiService logCallApiService)
        {
            _logCallApiService = logCallApiService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }
        private void PostMessage(SlackSMS item)
        {
            try
            {
                var slackApi = _baseConfig["AppSettings:SlackAPIDomain"].ToString();
                var urlApi = _baseConfig["AppSettings:SlackAPIUrlMoneyNotiChannel"].ToString();
                var client = new RestClient(slackApi);
                var request = new RestRequest(urlApi);
                request.Method = Method.POST;
                request.AddHeader("Content-Type", "application/json");
                var body = JsonConvert.SerializeObject(item);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = client.Execute(request);
                var content = response.Content;
            }
            catch (Exception ex)
            {

            }
        }

        public List<LogCallApi> GetHistory(int loanbriefid, string department = "")
        {
            try
            {
               return _logCallApiService.GetList(loanbriefid, (int)ActionCallApi.CallSlack, department);
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        private void PostMessage(SlackSMS item, string urlApi, int LoanbriefId = 0, string department = "")
        {
            try
            {
                var slackApi = _baseConfig["AppSettings:SlackAPIDomain"].ToString();
                var client = new RestClient(slackApi);
                var request = new RestRequest(urlApi);
                request.Method = Method.POST;
                request.AddHeader("Content-Type", "application/json");
                var body = JsonConvert.SerializeObject(item);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.CallSlack,
                    LinkCallApi = slackApi + urlApi,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = department,
                    LoanBriefId = LoanbriefId,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                objLogCallApi.Id = _logCallApiService.Insert(objLogCallApi);

                var response = client.Execute(request);
                var content = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = content;
                _logCallApiService.Update(objLogCallApi);
            }
            catch (Exception ex)
            {
                Log.Error("Slack PostMessage " + ex.ToString());
            }
        }
        public void SendNotifyOfStack(string author_name, string colorText, string title, string text, string urlApi, int loanbriefId = 0, string department = "")
        {
            try
            {
                var objSms = new SlackSMS();
                objSms.mrkdwn = true;
                objSms.text = String.Empty;
                objSms.attachments = new List<SlackAttachments>();

                SlackAttachments itemAttack = new SlackAttachments()
                {
                    author_name = author_name,
                    color = colorText,
                    pretext = "",
                    title = title,
                    text = text
                };
                objSms.attachments.Add(itemAttack);
                PostMessage(objSms, urlApi, loanbriefId, department);
            }
            catch (Exception ex)
            {
                Log.Error("Slack SendNotifyOfStack " + ex.ToString());
            }
        }

    }
}
