﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ISuggestionsInformation
    {
        SuggestionsInformationResult GetMotoPrice(SuggestionsInformationInput suggestionsInformation);
        ResponSaveLogMotoPrice PushLogPriceAIMoto(RequestSaveLogMotoPrice entity, string access_token);
    }
    public class SuggestionsInformation : ISuggestionsInformation
    {
        protected IConfiguration _baseConfig;
        protected IAuthenticationAI _authenService;
        private ILogLoanInfoAIService _logLoanInfoAIService;
        public SuggestionsInformation(IConfiguration configuration, IAuthenticationAI authenService, ILogLoanInfoAIService logLoanInfoAIService)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
            _authenService = authenService;
            _logLoanInfoAIService = logLoanInfoAIService;
        }
        public SuggestionsInformationResult GetMotoPrice(SuggestionsInformationInput entity)
        {
            var body = JsonConvert.SerializeObject(new PriceMotobikeAiReqest()
            {
                brand = entity.brand,
                model = entity.model,
                regdate = entity.regdate,
                phanh = entity.phanh,
                vanh = entity.vanh
            });
            var token = _authenService.GetToken(Constants.APP_ID_TIMA_IT3, Constants.APP_KEY_TIMA_IT3);
            var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.GET_MOTO_PRICE;
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.Timeout = 60000;
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", token);
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    LoanbriefId = entity.LoanBriefId,
                    ServiceType = ServiceTypeAI.PriceAi.GetHashCode(),
                    Request = body,
                    Token = token,
                    Url = url,
                    CreatedAt = DateTime.Now,
                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.Excuted,
                    Status = 0
                };
                if (logLoanInfoAi.LoanbriefId > 0)
                    logLoanInfoAi.Id = _logLoanInfoAIService.AddLog(logLoanInfoAi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                logLoanInfoAi.Status = 1;//gọi thành công
                logLoanInfoAi.ResponseRequest = jsonResponse;
                _logLoanInfoAIService.UpdateResponseOfRequest(logLoanInfoAi, EnumLogLoanInfoAiExcuted.Excuted.GetHashCode());
                SuggestionsInformationResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<SuggestionsInformationResult>(jsonResponse);
                if (result != null)
                {
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public ResponSaveLogMotoPrice PushLogPriceAIMoto(RequestSaveLogMotoPrice entity, string access_token)
        {
            var body = JsonConvert.SerializeObject(entity);
            var token = _authenService.GetToken(Constants.APP_ID_TIMA_IT3, Constants.APP_KEY_TIMA_IT3);
            var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.SAVE_LOG_PRICE_MOTO_AI;
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", token);
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                var response = client.Execute<ResponSaveLogMotoPrice>(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponSaveLogMotoPrice>(response.Content);
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
