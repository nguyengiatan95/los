﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.ThirdPartyProccesor.Objects;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ITikiService
    {
        Task<CreateLeadRes> CreateLead(string customerId, string name, string phone, string address);
        Task<bool> UpdateLead(string tikiId, string status);
    }

    public class TikiService : ITikiService
    {
        private IConfiguration _configuration;
        private IUnitOfWork _unitOfWork;
        private string client_id;
        private string client_secret;
        private string tiki_url;
        public TikiService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
            client_id = _configuration["AppSettings:tiki_miniapp_client_id"].ToString();
            client_secret = _configuration["AppSettings:tiki_miniapp_secret"].ToString();
            tiki_url = _configuration["AppSettings:tiki_miniapp_url"].ToString();
        }

        public async Task<CreateLeadRes> CreateLead(string customerId, string name, string phone, string address)
		{
            var data = new
            {
                name = name,
                phone = phone,
                address = address
            };
            var req = new CreateLeadReq();
            req.customer_id = customerId.ToString();
            req.inputs = JsonConvert.SerializeObject(data);
            long timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            string payload = timestamp + "." + client_id + "." + JsonConvert.SerializeObject(req);
            string encoded_payload = Base64Encode(payload);
            string signature = CalculateHmac256(encoded_payload, client_secret);
            var logPartner = new LogPartner();        
            logPartner.Provider = "TIKI";
            logPartner.TransactionType = "PUSH_LOAN";
            logPartner.CreatedTime = DateTime.Now;
            logPartner.RequestUrl = tiki_url + "/lead/applications";
            logPartner.RequestContent = JsonConvert.SerializeObject(req);            
            using (var httpclient = new HttpClient())
			{
                httpclient.DefaultRequestHeaders.Add("X-Tiniapp-Timestamp", timestamp.ToString());
                httpclient.DefaultRequestHeaders.Add("X-Tiniapp-Client-Id", client_id);
                httpclient.DefaultRequestHeaders.Add("X-Tiniapp-Signature", signature);
                StringContent content = new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json");
                var response = await httpclient.PostAsync(tiki_url + "/lead/applications", content);
                if (response.IsSuccessStatusCode)
				{
                    var result = await response.Content.ReadAsStringAsync();
                    if (!String.IsNullOrEmpty(result))
					{
                        try
						{
                            logPartner.HttpStatusCode = (int)response.StatusCode;
                            logPartner.ResponseContent = result;
                            logPartner.ResponseCode = 0;
                            _unitOfWork.LogPartnerRepository.Insert(logPartner);
                            _unitOfWork.SaveChanges();
                            return JsonConvert.DeserializeObject<CreateLeadRes>(result);
						}
						catch { }
					}                        
				}
                logPartner.HttpStatusCode = (int)response.StatusCode;
                _unitOfWork.LogPartnerRepository.Insert(logPartner);
                _unitOfWork.SaveChanges();
            }
            return null;
		}

        public async Task<bool> UpdateLead(string tikiId, string status)
        {
            var req = new UpdateLeadReq()
            {
                status = status
            };
            long timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            string payload = timestamp + "." + client_id + "." + JsonConvert.SerializeObject(req);
            string encoded_payload = Base64Encode(payload);
            string signature = CalculateHmac256(encoded_payload, client_secret);
            var logPartner = new LogPartner();            
            logPartner.Provider = "TIKI";
            logPartner.TransactionType = "UPDATE_LOAN";
            logPartner.CreatedTime = DateTime.Now;
            logPartner.RequestUrl = tiki_url + "/lead/applications/" + tikiId;
            logPartner.RequestContent = JsonConvert.SerializeObject(req);
            using (var httpclient = new HttpClient())
            {
                httpclient.DefaultRequestHeaders.Add("X-Tiniapp-Timestamp", timestamp.ToString());
                httpclient.DefaultRequestHeaders.Add("X-Tiniapp-Client-Id", client_id);
                httpclient.DefaultRequestHeaders.Add("X-Tiniapp-Signature", signature);
                StringContent content = new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json");
                var response = await httpclient.PutAsync(tiki_url + "/lead/applications/" + tikiId, content);
                if ((int)response.StatusCode == 204)
                {
                    logPartner.HttpStatusCode = (int)response.StatusCode;                    
                    logPartner.ResponseCode = 0;
                    _unitOfWork.LogPartnerRepository.Insert(logPartner);
                    _unitOfWork.SaveChanges();
                    return true;
                }
                logPartner.HttpStatusCode = (int)response.StatusCode;
                _unitOfWork.LogPartnerRepository.Insert(logPartner);
                _unitOfWork.SaveChanges();
            }
            return false;
        }

        string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes).Replace("=", "").Replace("+", "-").Replace("/", "_");
        }

        public static string CalculateHmac256(string data, string secret)
        {
            byte[] key = Encoding.ASCII.GetBytes(secret);
            HMACSHA256 hmacsha256 = new HMACSHA256(key);
            byte[] byteArray = Encoding.ASCII.GetBytes(data);
            MemoryStream stream = new MemoryStream(byteArray);
            string result = hmacsha256.ComputeHash(stream).Aggregate("", (s, e) => s + String.Format("{0:x2}", e), s => s);            
            return result;
        }
    }
}
