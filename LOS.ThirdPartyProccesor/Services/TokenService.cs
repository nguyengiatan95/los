﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface ITokenService
    {
        AccessToken GetLastToken(string app_id);
        int AddToken(AccessToken entity);
    }
    public class TokenService : ITokenService
    {
        private readonly IUnitOfWork _unitOfWork;
        public TokenService() 
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public int AddToken(AccessToken entity)
        {
            try
            {
                entity.CreatedAt = DateTime.Now;
                _unitOfWork.AccessTokenRepository.Insert(entity);
                _unitOfWork.Save();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public AccessToken GetLastToken(string app_id)
        {
            try
            {
                return _unitOfWork.AccessTokenRepository.Query(x => x.AppId == app_id, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
