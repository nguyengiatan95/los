﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LOS.ThirdPartyProccesor.Services
{
    public interface IUserService
    {
        User GetCoordinatorUser(List<int> listUserId = null);
        User GetCoordinatorUser(int userId);
        bool UpdateNumberReceivedCoordinator(int userId);
        User GetHubEmployee(int userId);
        User GetTelesale(int userId);
        User GetLastUserTelesales(int productId, int ProvinceId = -1, int? telesaleShift = -1, int? telesaleTeamId = -1);

        bool UpdateLastReceivedTelesale(int telesaleId);

        List<TelesalesShift> ListTelesaleShift();
        int GetTeamManager(int userId);

        bool IsUserOfHub(int userId);

        User GetFieldHo(int provinceId);

        List<int> GetAllHub(int userId);

        List<int> GetUserStaffHub(List<int> listUserId);

        UserDetail GetById(int userId);
        User GetTelesaleByPhone(string phoneNumber);
        TeamTelesales GetTeamTelesale(int id);
        List<User> ListUser(List<int> userIds);


    }
    public class UserServices : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        public UserServices()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            var db = new LOSContext(options);
            _unitOfWork = new UnitOfWork(db);
        }

        public User GetCoordinatorUser(List<int> listUserId = null)
        {
            try
            {
                //trừ thẩm định td_sonnl xử lý đơn hàng hóa: 49500
                return _unitOfWork.UserRepository.Query(x => x.UserId != 49500
                && (listUserId == null || listUserId.Contains(x.UserId))
                && x.GroupId == (int)EnumGroupUser.ApproveEmp && x.Status == 1
                && x.IsDeleted != true && x.CoordinatorIsEnable == true,
                    x => x.OrderBy(k => k.CoordinateRecord), false).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public User GetCoordinatorUser(int userId)
        {
            try
            {
                return _unitOfWork.UserRepository.Query(x => x.UserId == userId && x.GroupId == (int)EnumGroupUser.ApproveEmp && x.Status == 1 && x.IsDeleted != true && x.CoordinatorIsEnable == true,
                    x => x.OrderBy(k => k.CoordinateRecord), false).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateNumberReceivedCoordinator(int userId)
        {
            try
            {
                var entity = _unitOfWork.UserRepository.GetById(userId);
                if (entity != null && entity.UserId > 0)
                {
                    entity.CoordinateRecord = entity.CoordinateRecord.GetValueOrDefault(0) + 1;
                    _unitOfWork.Save();
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public User GetTelesale(int userId)
        {
            try
            {
                return _unitOfWork.UserRepository.Query(x => x.UserId == userId
                && (x.GroupId == (int)EnumGroupUser.Telesale || x.GroupId == (int)EnumGroupUser.ManagerTelesales)
                && x.Status == 1 && x.IsDeleted != true, null, false).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<User> ListUser(List<int> userIds)
        {
            try
            {
                return _unitOfWork.UserRepository.Query(x => userIds.Contains(x.UserId)
                && x.Status == 1 && x.IsDeleted != true, null, false).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public User GetHubEmployee(int userId)
        {
            try
            {
                return _unitOfWork.UserRepository.Query(x => x.UserId == userId && x.GroupId == (int)EnumGroupUser.StaffHub && x.Status == 1 && x.IsDeleted != true, null, false).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateLastReceivedTelesale(int telesaleId)
        {
            try
            {
                var entity = _unitOfWork.UserRepository.GetById(telesaleId);
                if (entity != null && entity.UserId > 0)
                {
                    entity.LastReceived = DateTime.Now;
                    _unitOfWork.Save();
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public User GetLastUserTelesales(int productId, int ProvinceId = -1, int? telesaleShift = -1, int? telesaleTeamId = -1)
        {
            try
            {
                var product = _unitOfWork.LoanProductRepository.GetById(productId);
                if (product != null)
                {
                    //Nếu là sản phẩm nhỏ => lấy ra các user nhận sp nhỏ và user nhận cả 2
                    if (product.TypeProductId == EnumTypeProduct.SmallProduct.GetHashCode())
                    {
                        if (telesaleTeamId > 0 && 
                            (telesaleTeamId == EnumTeamTelesales.TeamXsell.GetHashCode()
                            || telesaleTeamId == EnumTeamTelesales.Xsell1.GetHashCode()
                            || telesaleTeamId == EnumTeamTelesales.Xsell2.GetHashCode()
                            || telesaleTeamId == EnumTeamTelesales.Xsell3.GetHashCode()
                            ))
                        {
                            return _unitOfWork.UserRepository.Query(x => x.GroupId == (int)EnumGroupUser.Telesale && x.Status == 1 && x.TelesaleRecievedLoan == true
                                && (x.TypeRecievedProduct == (int)EnumTypeRecievedProduct.SmallProduct || x.TypeRecievedProduct == (int)EnumTypeRecievedProduct.All)
                                && ((x.ValueTelesalesShift & telesaleShift) == telesaleShift || telesaleShift == -1)
                                && x.TeamTelesalesId == telesaleTeamId
                                && x.IsDeleted != true, x => x.OrderBy(k => k.LastReceived), false).FirstOrDefault();
                        }
                        else
                        {
                            //Nhận đơn hotlead trừ team xcell
                            return _unitOfWork.UserRepository.Query(x => x.GroupId == (int)EnumGroupUser.Telesale && x.Status == 1 && x.TelesaleRecievedLoan == true
                                && (x.TypeRecievedProduct == (int)EnumTypeRecievedProduct.SmallProduct || x.TypeRecievedProduct == (int)EnumTypeRecievedProduct.All)
                                && ((x.ValueTelesalesShift & telesaleShift) == telesaleShift || telesaleShift == -1)
                                && (x.TeamTelesalesId != EnumTeamTelesales.TeamXsell.GetHashCode() && x.TeamTelesalesId != EnumTeamTelesales.Xsell1.GetHashCode()
                                            && x.TeamTelesalesId != EnumTeamTelesales.Xsell2.GetHashCode()
                                            && x.TeamTelesalesId != EnumTeamTelesales.Xsell3.GetHashCode()
                                        && (x.TeamTelesalesId == telesaleTeamId || telesaleTeamId == -1))
                                && x.IsDeleted != true && (ProvinceId == -1 || x.CityId == ProvinceId),
                                            x => x.OrderBy(k => k.LastReceived), false).FirstOrDefault();
                        }

                    }
                    else if (product.TypeProductId == EnumTypeProduct.BigProduct.GetHashCode())
                    {
                        if (telesaleTeamId > 0 &&
                            (telesaleTeamId == EnumTeamTelesales.TeamXsell.GetHashCode()
                            || telesaleTeamId == EnumTeamTelesales.Xsell1.GetHashCode()
                            || telesaleTeamId == EnumTeamTelesales.Xsell2.GetHashCode()
                            || telesaleTeamId == EnumTeamTelesales.Xsell3.GetHashCode()
                            ))
                        {
                            return _unitOfWork.UserRepository.Query(x => x.GroupId == (int)EnumGroupUser.Telesale && x.Status == 1 && x.TelesaleRecievedLoan == true
                                && (x.TypeRecievedProduct == (int)EnumTypeRecievedProduct.BigProduct || x.TypeRecievedProduct == (int)EnumTypeRecievedProduct.All)
                                && ((x.ValueTelesalesShift & telesaleShift) == telesaleShift || telesaleShift == -1)
                                && x.TeamTelesalesId == telesaleTeamId
                                && x.IsDeleted != true, x => x.OrderBy(k => k.LastReceived), false).FirstOrDefault();
                        }
                        else
                        {
                            //Nhận đơn hotlead trừ team xcell
                            return _unitOfWork.UserRepository.Query(x => x.GroupId == (int)EnumGroupUser.Telesale && x.Status == 1 && x.TelesaleRecievedLoan == true
                               && (x.TypeRecievedProduct == (int)EnumTypeRecievedProduct.BigProduct || x.TypeRecievedProduct == (int)EnumTypeRecievedProduct.All)
                               && ((x.ValueTelesalesShift & telesaleShift) == telesaleShift || telesaleShift == -1)
                               && (x.TeamTelesalesId != EnumTeamTelesales.TeamXsell.GetHashCode() && x.TeamTelesalesId != EnumTeamTelesales.Xsell1.GetHashCode()
                                            && x.TeamTelesalesId != EnumTeamTelesales.Xsell2.GetHashCode()
                                            && x.TeamTelesalesId != EnumTeamTelesales.Xsell3.GetHashCode()
                                        && (x.TeamTelesalesId == telesaleTeamId || telesaleTeamId == -1))
                               && x.IsDeleted != true && (ProvinceId == -1 || x.CityId == ProvinceId),
                                               x => x.OrderBy(k => k.LastReceived), false).FirstOrDefault();
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<TelesalesShift> ListTelesaleShift()
        {
            try
            {
                return _unitOfWork.TelesalesShiftRepository.Query(x => x.IsEnable == true, null, false).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int GetTeamManager(int userId)
        {
            try
            {
                if (_unitOfWork.UserRepository.Any(x => x.Status == 1 && x.GroupId == (int)EnumGroupUser.ManagerTelesales && x.UserId == userId))
                {
                    var data = _unitOfWork.UserTeamTelesalesRepository.Query(x => x.UserId == userId && x.MainTeam == true, null, false).FirstOrDefault();
                    if (data != null)
                        return data.TeamTelesalesId;
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool IsUserOfHub(int userId)
        {
            try
            {
                return _unitOfWork.UserRepository.Any(x => x.Status == 1 && (x.GroupId == (int)EnumGroupUser.ManagerHub || x.GroupId == (int)EnumGroupUser.StaffHub) && x.UserId == userId);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public User GetFieldHo(int provinceId)
        {
            try
            {
                return _unitOfWork.UserRepository.Query(x => x.CityId == provinceId && x.GroupId == (int)EnumGroupUser.FieldSurvey
                && x.Status == 1 && x.IsDeleted != true, null, false).OrderBy(x => x.LastReceived).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<int> GetAllHub(int userId)
        {
            try
            {
                return _unitOfWork.UserShopRepository.Query(x => x.UserId == userId && x.ShopId > 0, null, false).Select(x => x.ShopId.Value).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<int> GetUserStaffHub(List<int> listUserId)
        {
            try
            {
                return _unitOfWork.UserRepository.Query(x => listUserId.Contains(x.UserId) && x.GroupId == (int)EnumGroupUser.StaffHub, null, false).Select(x => x.UserId).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public UserDetail GetById(int userId)
        {
            try
            {
                return _unitOfWork.UserRepository.Query(x => x.UserId == userId && x.Status == 1 && x.IsDeleted != true, null, false).Select(x => new UserDetail
                {
                    UserId = x.UserId,
                    FullName = x.FullName,
                    Username = x.Username,
                    Email = x.Email,
                    Phone = x.Phone,
                    GroupId = x.GroupId,
                    Status = x.Status,
                    Ipphone = x.Ipphone
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public User GetTelesaleByPhone(string phoneNumber)
        {
            try
            {
                return _unitOfWork.UserRepository.Query(x => x.Phone == phoneNumber
                && (x.GroupId == (int)EnumGroupUser.Telesale || x.GroupId == (int)EnumGroupUser.ManagerTelesales)
                && x.Status == 1 && x.IsDeleted != true, null, false).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public TeamTelesales GetTeamTelesale(int id)
        {
            try
            {
                return _unitOfWork.TeamTelesalesRepository.Query(x => x.TeamTelesalesId == id
                , null, false).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }
}
