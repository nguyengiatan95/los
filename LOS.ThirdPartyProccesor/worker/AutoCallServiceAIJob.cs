﻿using LOS.Common.Extensions;
using LOS.Common.Helpers;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoCallServiceAIJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly ILogLoanInfoAIService _logLoanInfoAIService;
        private readonly IServiceAI _serviceAI;
        private readonly INetworkService _networkService;
        protected readonly IConfiguration _baseConfig;
        protected IAuthenticationAI _authenService;
        protected ILocationService _locationService;
        //private Timer _timer;
        //private Timer _timerReGet;
        //private Thread _thread;
        private Thread _threadRequest;
        private readonly IHubDistributingService _hubDistributingService;
        public AutoCallServiceAIJob(ILoanBriefService loanBriefServices, IServiceAI serviceAI, INetworkService networkService, IAuthenticationAI authenService,
            ILogLoanInfoAIService logLoanInfoAIService, IHubDistributingService hubDistributingService, ILocationService locationService)
        {
            _loanBriefServices = loanBriefServices;
            _logLoanInfoAIService = logLoanInfoAIService;
            _serviceAI = serviceAI;
            _networkService = networkService;
            _authenService = authenService;
            _hubDistributingService = hubDistributingService;
            _locationService = locationService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            //_thread = new Thread(() => DoWork(null));
            //_thread.Start();

            _threadRequest = new Thread(() => DoWorkExcutedRequestAI(null));
            _threadRequest.Start();
            //_timer = new Timer(DoWork, null, TimeSpan.FromSeconds(20), TimeSpan.FromSeconds(300));
            return Task.CompletedTask;
        }
        private void DoWork(object state)
        {

            while (true)
            {
                //Console.WriteLine("****GetInfoFromRuleAI****" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                try
                {
                    //Lấy ra d/s đơn vay ở trạng thái chờ lấy dữ liệu AI
                    //Chưa có request thành công trong bảng LogLoanInfoAI
                    //var loanCredits = _loanBriefServices.WaitGetInfoFromAI();
                    //if (loanCredits != null && loanCredits.Count > 0)
                    //{
                    //    foreach (var item in loanCredits)
                    //    {
                    //        //nếu là danh sách chờ lấy dữ liệu từ AI thì mặc định phải để PushNextState = 1 => tự động đẩy lên state mới
                    //        RequestToRuleAI(item, 1);
                    //        if (item.IsTrackingLocation
                    //            && (_networkService.CheckHomeNetwok(item.Phone) == (int)HomeNetWorkMobile.Viettel || _networkService.CheckHomeNetwok(item.Phone) == (int)HomeNetWorkMobile.Mobi))
                    //        {
                    //            RequestToLocationAI(item);
                    //        }
                    //    }
                    //}
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor GetInfoFromRuleAI exception");
                }
                //Thread 1/2 minute
                Thread.Sleep(1000 * 30);
            }
        }

        private void DoWorkExcutedRequestAI(object state)
        {

            while (true)
            {
                //Console.WriteLine("****DoWorkExcutedRequestAI****" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                try
                {
                    //lấy ra danh sách các row chờ gửi request
                    var requests = _logLoanInfoAIService.GetListRequest();

                    //to do request credit scoring
                    //var loanInfo1 = _loanBriefServices.LoanbriefInfo(981313);
                    //RequestToCreditScoring(loanInfo1, 1222);
                    //RequestToLeadScore(loanInfo);

                    if (requests != null && requests.Count > 0)
                    {
                        foreach (var item in requests)
                        {
                            if (item.LoanbriefId > 0)
                            {
                                var loanBrief = new LoanBriefVerifyDetail();

                                if (item.ServiceType == (int)ServiceTypeAI.RuleCheck)
                                {
                                    loanBrief = _loanBriefServices.LoanbriefInfoFromAI(item.LoanbriefId.Value);
                                    if (loanBrief != null)
                                        RequestToRuleAI(loanBrief, item.Id);
                                }
                                else if (item.ServiceType == (int)ServiceTypeAI.Location)
                                {
                                    loanBrief = _loanBriefServices.LoanbriefInfoFromAI(item.LoanbriefId.Value);
                                    if (loanBrief != null
                                        && (
                                        loanBrief.HomeNetwork == (int)HomeNetWorkMobile.Viettel
                                        || loanBrief.HomeNetwork == (int)HomeNetWorkMobile.Mobi
                                        || _networkService.CheckHomeNetwok(loanBrief.Phone) == (int)HomeNetWorkMobile.Viettel
                                        || _networkService.CheckHomeNetwok(loanBrief.Phone) == (int)HomeNetWorkMobile.Mobi))
                                    {
                                        RequestToLocationAI(loanBrief, item.Id);
                                    }
                                    else
                                    {
                                        if (loanBrief != null)
                                        {
                                            _loanBriefServices.DoneSendRequestLocation(loanBrief.LoanBriefId, "Nhà mạng không hỗ trợ");
                                            _logLoanInfoAIService.CancelRequest(item.Id, "NOT_SUPPORT");
                                        }
                                    }
                                }
                                else if (item.ServiceType == (int)ServiceTypeAI.RefPhone)
                                {
                                    loanBrief = _loanBriefServices.LoanbriefInfoFromAI(item.LoanbriefId.Value);
                                    if (loanBrief != null)
                                        RequestToRefphoneAI(loanBrief, item.Id);
                                }
                                else if (item.ServiceType == (int)ServiceTypeAI.RangeCreditScoring)
                                {
                                    //to do request credit scoring
                                    var loanInfo = _loanBriefServices.LoanbriefInfo(item.LoanbriefId.Value);
                                    if (loanInfo != null)
                                        RequestToRangeCreditScoring(loanInfo, item.Id);
                                }
                                else if (item.ServiceType == (int)ServiceTypeAI.CreditScoring)
                                {
                                    var loanInfo = _loanBriefServices.LoanbriefInfoCreditScoring(item.LoanbriefId.Value);
                                    if (loanInfo != null)
                                        RequestToCreditScoring(loanInfo, item.Id);
                                }
                                else if (item.ServiceType == (int)ServiceTypeAI.PushLogLeadScore)
                                {
                                    var loanInfo = _loanBriefServices.LoanbriefInfoPushLog(item.LoanbriefId.Value);
                                    if (loanInfo != null)
                                        RequestPushLogToAI(loanInfo, item.Id);
                                }
                                else if (item.ServiceType == (int)ServiceTypeAI.EkycGetInfoNetwork)
                                {
                                    RequestToInfoNetwork(item);
                                }
                                else if (item.ServiceType == (int)ServiceTypeAI.EkycMotorbikeRegistrationCertificate)
                                {
                                    RequestEkycMotorbikeRegistrationCertificate(item);
                                }
                                else if (item.ServiceType == (int)ServiceTypeAI.Ekyc)
                                {
                                    RequestToInfoNationalCard(item);
                                }
                                else if (item.ServiceType == (int)ServiceTypeAI.FraudDetection)
                                {
                                    if (item.LoanbriefId > 0)
                                        SendRequestFraudDetection(item.LoanbriefId.Value, item.Id);
                                    // _loanBriefServices.ExcutedRequestAI(item.Id);
                                }
                                else if (item.ServiceType == (int)ServiceTypeAI.ParseVinNumber)
                                {
                                    if (item.LoanbriefId > 0)
                                    {
                                        //lấy thông tin số vin
                                        var vinNumber = _loanBriefServices.GetVinNumber(item.LoanbriefId.Value);
                                        if (!string.IsNullOrEmpty(vinNumber))
                                        {
                                            ParseVinNumber(item.LoanbriefId.Value, item.Id, vinNumber);
                                        }
                                        else
                                        {
                                            item.ResultFinal = "ERROR";
                                            _logLoanInfoAIService.Done(item);
                                        }
                                    }
                                    else
                                    {
                                        item.ResultFinal = "ERROR";
                                        _logLoanInfoAIService.Done(item);
                                    }
                                   
                                }
                                else if (item.ServiceType == (int)ServiceTypeAI.FacebookInfo)
                                {
                                    var loanInfo = _loanBriefServices.LoanbriefInfo(item.LoanbriefId.Value);
                                    if (loanInfo != null)
                                        SendRequestCrawlFB(loanInfo, item.Id);
                                }
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor GetInfoFromRuleAI exception");
                }
                //Thread 1/2 minute
                Thread.Sleep(1000 * 30);
            }
        }

        private void SendRequestFraudDetection(int loanbriefId, int logLoanInfoAI)
        {
            try
            {
                if (loanbriefId > 0)
                {
                    var lastRequest = _logLoanInfoAIService.GetLastRequest(loanbriefId, (int)ServiceTypeAI.FraudDetection);
                    if (lastRequest != null && lastRequest.Status == 1)
                        return;

                    var registerFraud = _serviceAI.RegisterFraudDetection(loanbriefId, false, 0, logLoanInfoAI);
                    if (registerFraud != null && registerFraud.response_code == 200)
                        Console.WriteLine(string.Format("TDHS: REQUEST FRAUD DETECTION HĐ-{0} {1}", loanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void RequestToRuleAI(LoanBriefVerifyDetail entity, int LogRequestId, int PushNextState = 0)
        {
            try
            {
                var url = Constants.register_get_infor_AI;
                if (entity.IsReborrow)
                    url = Constants.register_get_infor_taivay_AI;
                else if (entity.ProductId == EnumProductCredit.MotorCreditType_CC.GetHashCode() || entity.ProductId == EnumProductCredit.LoanFastMoto_CC.GetHashCode())
                    url = Constants.register_get_infor_xmcc_AI;
                else if (entity.ProductId == EnumProductCredit.MotorCreditType_KCC.GetHashCode() || entity.ProductId == EnumProductCredit.MotorCreditType_KGT.GetHashCode())
                    url = Constants.register_get_infor_xmkcc_AI;

                int age = 0;
                if (entity.Dob.HasValue)
                {
                    age = DateTime.Now.Year - entity.Dob.Value.Year;
                    if (DateTime.Now.DayOfYear < entity.Dob.Value.DayOfYear)
                        age = age - 1;
                }
                if (entity.HomeNetwork == 0)
                    entity.HomeNetwork = _networkService.CheckHomeNetwok(entity.Phone);
                string NetworkName = Enum.IsDefined(typeof(HomeNetWorkMobile), entity.HomeNetwork) ? ExtensionHelper.GetDescription((HomeNetWorkMobile)entity.HomeNetwork) : string.Empty;
                var objReq = new RuleAIReq
                {
                    name = entity.FullName,
                    age = age.ToString(),
                    national_id = entity.NationalCard,
                    phone = new PhoneCustomer
                    {
                        number = entity.Phone,
                        carrier = !string.IsNullOrEmpty(NetworkName) ? NetworkName.ToLower() : "",
                    },
                    city_name = entity.CityName,
                    address_home = entity.AddressHome,
                    address_office = entity.AddressOffice,
                    address_other = "",
                    reference_name = "",
                    reference_phone = "",
                    job = entity.JobName,
                    is_office = false
                };
                var result = _serviceAI.RegisterGetInfoAI(objReq, url, LogRequestId, PushNextState);
                if (result != null)
                    Console.WriteLine(string.Format("Request Rulecheck: {0} {1}", entity.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor RequestToRuleAI exception");
            }
        }

        private void RequestToLocationAI(LoanBriefVerifyDetail entity, int LogRequestId)
        {
            try
            {
                var lastRequest = _logLoanInfoAIService.GetLastRequest(entity.LoanBriefId, (int)ServiceTypeAI.Location);
                var isCheck = false;
                var model = _serviceAI.PrepareCACModelV2(entity, ref isCheck);
                if (!isCheck)
                {
                    _loanBriefServices.AddNote(new LoanBriefNote
                    {
                        LoanBriefId = entity.LoanBriefId,
                        Note = $"Tracking location: KH chưa chụp hồ sơ trên App Tima Care",
                        FullName = "Auto System",
                        Status = 1,
                        ActionComment = EnumActionComment.RequestLocation.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 1,
                        ShopId = EnumShop.Tima.GetHashCode(),
                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                    _logLoanInfoAIService.CancelRequest(LogRequestId, "REQUEST_ERROR");
                }
                if (lastRequest != null && lastRequest.Id != LogRequestId)
                {
                    if (isCheck && model != null)
                    {
                        if (String.Compare(lastRequest.Request.ToString(), JsonConvert.SerializeObject(model).ToString(), true) == 0)
                        {
                            isCheck = false;
                            _loanBriefServices.AddNote(new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = $"Tracking location: Đang có yêu cầu location khác đang được xử lý. Vui lòng chờ kết quả",
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.RequestLocation.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            });
                            _logLoanInfoAIService.CancelRequest(LogRequestId, "REQUEST_ERROR");
                        }
                    }
                }

                if (isCheck && model != null)
                {
                    var data = _serviceAI.VerifyAddress(model, LogRequestId);
                    if (data != null)
                    {
                        Console.WriteLine(string.Format("Request Tracking location: {0} {1}", entity.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        var strComment = new StringBuilder();
                        strComment.AppendLine("Tracking location: Đã gửi yêu cầu truy cập thông tin vị trí của Khách hàng với tọa độ");
                        strComment.AppendLine($"<br /> Nhà:{model.locationCodes.home}");

                        if (!string.IsNullOrEmpty(model.locationCodes.home))
                        {
                            var address = _locationService.GetNameAddress(model.locationCodes.home);
                            if (!string.IsNullOrEmpty(address))
                                strComment.AppendLine($"<br />({address})");
                        }

                        strComment.AppendLine($"<br /> Công ty:{model.locationCodes.office}");
                        if (!string.IsNullOrEmpty(model.locationCodes.office))
                        {
                            var address = _locationService.GetNameAddress(model.locationCodes.office);
                            if (!string.IsNullOrEmpty(address))
                                strComment.AppendLine($"<br />({address})");
                        }
                        strComment.AppendLine($"<br /> Nhà mạng: {model.carrier}");
                        if (data.code == 0 && data.data != null && !string.IsNullOrEmpty(data.data.refCode))
                        {
                            _loanBriefServices.DoneSendRequestLocation(entity.LoanBriefId, data.messages);
                            _loanBriefServices.AddNote(new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = strComment.ToString(),
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.RequestLocation.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            });
                        }
                        else if (data.code != 0 && !string.IsNullOrEmpty(data.messages))
                        {
                            if (string.IsNullOrEmpty(data.messages))
                            {
                                if (data.code == 503)
                                    data.messages = "503 Service Unavailable";
                            };
                            _loanBriefServices.DoneSendRequestLocation(entity.LoanBriefId, data.messages);

                            //hủy request do lỗi
                            _logLoanInfoAIService.CancelRequest(LogRequestId, "REQUEST_ERROR");
                        }
                        else
                        {
                            _logLoanInfoAIService.CancelRequest(LogRequestId, "REQUEST_ERROR");
                        }
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor RequestToRuleAI exception");
            }
        }

        private void RequestToRefphoneAI(LoanBriefVerifyDetail entity, int LogRequestId)
        {
            try
            {
                if (entity.Relationships != null && entity.Relationships.Any(x => !string.IsNullOrEmpty(x.Phone)))
                {
                    if (entity.HomeNetwork == 0)
                        entity.HomeNetwork = _networkService.CheckHomeNetwok(entity.Phone);
                    string NetworkName = Enum.IsDefined(typeof(HomeNetWorkMobile), entity.HomeNetwork) ? ExtensionHelper.GetDescription((HomeNetWorkMobile)entity.HomeNetwork) : string.Empty;

                    var webhook = string.Empty;
                    var PhoneNum1 = "";
                    var PhoneNum2 = "";
                    if (entity.HomeNetwork == HomeNetWorkMobile.Viettel.GetHashCode())
                    {
                        webhook = _baseConfig["AppSettings:DomainAPILOS"] + Constants.webhook_refphone_viettel;
                        PhoneNum1 = entity.Relationships != null && entity.Relationships.Count > 0 && !string.IsNullOrEmpty(entity.Relationships[0].Phone) ? entity.Relationships[0].Phone : "";
                        PhoneNum2 = entity.Relationships != null && entity.Relationships.Count > 1 && !string.IsNullOrEmpty(entity.Relationships[1].Phone) ? entity.Relationships[1].Phone : "";
                    }
                    else if (entity.HomeNetwork == HomeNetWorkMobile.Mobi.GetHashCode())
                    {
                        webhook = _baseConfig["AppSettings:DomainAPILOS"] + Constants.webhook_refphone_mobifone;
                        // nếu có sdt của ng hay liên hệ => gửi sdt đó check
                        if (entity.Relationships != null && entity.Relationships.Count > 0 && entity.Relationships.Any(x => x.RelationshipType == (int)EnumRelativeFamily.HayLienHe))
                        {
                            PhoneNum1 = entity.Relationships != null && entity.Relationships.Count > 0 && !string.IsNullOrEmpty(entity.Relationships.Where(x => x.RelationshipType == (int)EnumRelativeFamily.HayLienHe).FirstOrDefault().Phone) ? entity.Relationships.Where(x => x.RelationshipType == (int)EnumRelativeFamily.HayLienHe).FirstOrDefault().Phone : "";
                        }
                        else
                        {
                            //lấy 1 trong 2 sdt của người thân
                            if (entity.Relationships != null && entity.Relationships.Count > 0 && !string.IsNullOrEmpty(entity.Relationships[0].Phone))
                            {
                                PhoneNum1 = entity.Relationships[0].Phone;
                            }
                            else if (entity.Relationships != null && entity.Relationships.Count > 1 && !string.IsNullOrEmpty(entity.Relationships[1].Phone))
                            {
                                PhoneNum1 = entity.Relationships[1].Phone;
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(PhoneNum1) || !string.IsNullOrEmpty(PhoneNum2))
                    {
                        var objData = new RefphoneReq()
                        {
                            refPhones = new Refphone()
                            {
                                num1 = PhoneNum1,
                                num2 = PhoneNum2,
                                num3 = string.Empty
                            },
                            phone = entity.Phone,
                            url = webhook,
                            carrier = !string.IsNullOrEmpty(NetworkName) ? NetworkName.ToLower() : "",

                        };
                        var data = _serviceAI.VerifyRefPhone(objData, LogRequestId);
                        if (data != null)
                        {
                            Console.WriteLine(string.Format("Request Refphone: {0} {1}", entity.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            if (data.code == 0 && data.data != null && !string.IsNullOrEmpty(data.data.refCode))
                            {
                                _loanBriefServices.AddNote(new LoanBriefNote
                                {
                                    LoanBriefId = entity.LoanBriefId,
                                    Note = string.Format("RefPhone: Đã gửi yêu cầu truy cập thông tin sđt người thân của Khách hàng"),
                                    FullName = "Auto System",
                                    Status = 1,
                                    ActionComment = EnumActionComment.RequestRefphone.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = 1,
                                    ShopId = EnumShop.Tima.GetHashCode(),
                                    ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor RequestToRefphoneAI exception");
            }
        }

        private void RequestToRangeCreditScoring(LoanBriefInfo entity, int LogRequestId)
        {
            try
            {
                if (entity != null)
                {
                    var token = _authenService.GetToken(Constants.scoring_app_id, Constants.scoring_app_key);
                    if (!string.IsNullOrEmpty(token))
                    {
                        var data = _serviceAI.RangeCreditScoring(token, entity, LogRequestId);
                        if (data != null)
                        {
                            if (data.StatusCode == 200)
                            {
                                //Get scroring thành công! với mã đơn HĐ-122334
                                Console.WriteLine(string.Format("Request Range Credit Scoring done HĐ-: {0} {1}", entity.ID, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));

                                //_loanBriefServices.AddNote(new LoanBriefNote
                                //{
                                //    LoanBriefId = entity.ID,
                                //    Note = string.Format("Credit Scoring: Đăng ký lấy điểm credit scoring thành công!"),
                                //    FullName = entity.FullName,
                                //    Status = 1,
                                //    ActionComment = EnumActionComment.CreditScoring.GetHashCode(),
                                //    CreatedTime = DateTime.Now,
                                //    UserId = 1,
                                //    ShopId = EnumShop.Tima.GetHashCode(),
                                //    ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                //});
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor RequestToCreditScoring exception");
            }
        }

        private void RequestToCreditScoring(LoanBriefInfoCreditScoring entity, int LogRequestId)
        {
            try
            {
                if (entity != null)
                {
                    var token = _authenService.GetToken(Constants.scoring_app_id, Constants.scoring_app_key);
                    if (!string.IsNullOrEmpty(token))
                    {
                        var data = _serviceAI.GetCreditScoring(token, entity, LogRequestId);
                        if (data != null && data.score > 0)
                        {
                            _loanBriefServices.DoneCreditScoring(entity.ID, data.score, data.label);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor RequestToCreditScoring exception");
            }
        }

        private void RequestPushLogToAI(LoanBriefInfoLog entity, int LogRequestId)
        {
            try
            {
                if (entity != null)
                {
                    entity.IsPass = true;
                    _serviceAI.PushLogLeadScore(entity, LogRequestId);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor RequestToCreditScoring exception");
            }
        }

        private void RequestToInfoNetwork(LogLoanInfoAi entity)
        {
            try
            {
                entity.ResultFinal = "ERROR";
                if (entity.LoanbriefId > 0 && entity.Id > 0)
                {
                    var isCheck = false;
                    var model = _serviceAI.PrepareEkycInfoNetWorkModel(entity.LoanbriefId.Value, ref isCheck);
                    if (isCheck && model != null)
                    {
                        var result = _serviceAI.EkycInfoNetWork(model, entity.Id);
                        if (result.response_code == "200")
                        {
                            entity.ResultFinal = "SUCCESS";
                            Console.WriteLine(string.Format("Request Info TTTB: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        }
                        else
                            Console.WriteLine(string.Format("Request Info TTTB ERROR: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                    else
                    {
                        Console.WriteLine(string.Format("Request Info TTTB ERROR MODEL: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
                _logLoanInfoAIService.Done(entity);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor RequestToInfoNetwork exception");
            }
        }

        private void RequestEkycMotorbikeRegistrationCertificate(LogLoanInfoAi entity)
        {
            try
            {
                entity.ResultFinal = "ERROR";
                if (entity.LoanbriefId > 0 && entity.Id > 0)
                {
                    var isCheck = false;
                    var model = _serviceAI.PrepareEkycMotorbikeRegistrationCertificateModel(entity.LoanbriefId.Value, ref isCheck);
                    if (isCheck && model != null)
                    {
                        var result = _serviceAI.EkycMotorbikeRegistrationCertificate(model, entity.Id);
                        if (result.response_code == 200)
                        {
                            entity.ResultFinal = "";
                            Console.WriteLine(string.Format("Request Motorbike Registration Certificate: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        }
                        else
                            Console.WriteLine(string.Format("Request Motorbike Registration Certificate ERROR: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                    else
                    {
                        _logLoanInfoAIService.CancelRequest(entity.Id, entity.ResultFinal);
                        Console.WriteLine(string.Format("Request Motorbike Registration Certificate ERROR MODEL: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
                _logLoanInfoAIService.UpdateResultRequest(entity.Id, entity.ResultFinal);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor RequestToInfoNetwork exception");
            }
        }

        private void RequestToInfoNationalCard(LogLoanInfoAi entity)
        {
            try
            {
                entity.ResultFinal = "ERROR";
                if (entity.LoanbriefId > 0 && entity.Id > 0)
                {
                    var isCheck = false;
                    var model = _serviceAI.PrepareEkycModel(entity.LoanbriefId.Value, ref isCheck);
                    if (isCheck && model != null)
                    {
                        var result = _serviceAI.EkycInfoNationalCard(model, entity.Id);
                        if (result.StatusCode == 200)
                        {
                            entity.ResultFinal = "SUCCESS";
                            SaveResultEkyc(result, entity.LoanbriefId.Value);
                            Console.WriteLine(string.Format("Request Info National: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        }
                        else
                            Console.WriteLine(string.Format("Request Info National ERROR: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                    else
                    {
                        Console.WriteLine(string.Format("Request Info National ERROR MODEL: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
                _logLoanInfoAIService.Done(entity);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor RequestToInfoNationalCard exception");
            }
        }

        private void ParseVinNumber(int loanbriefId, int logRequestId, string vinNumber)
        {
            try
            {
                if (loanbriefId > 0 && logRequestId > 0 && !string.IsNullOrEmpty(vinNumber))
                {
                    var token = _authenService.GetToken(Constants.scoring_app_id, Constants.scoring_app_key);
                    if (!string.IsNullOrEmpty(token))
                    {
                        var data = _serviceAI.ParseVinNumber(token, vinNumber, logRequestId);
                        if (data != null)
                        {
                            if (data.status_code == 200)
                            {
                                Console.WriteLine(string.Format("Request vin  done HĐ-: {0} {1}", loanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                if (data.result != null && !string.IsNullOrEmpty(data.result.full_name))
                                    _loanBriefServices.AddNote(new LoanBriefNote
                                    {
                                        LoanBriefId = loanbriefId,
                                        Note = string.Format("Thông tin xe từ số VIN: <b>{0}</b>", data.result.full_name),
                                        FullName = "Auto System",
                                        Status = 1,
                                        ActionComment = EnumActionComment.CreditScoring.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = 1,
                                        ShopId = EnumShop.Tima.GetHashCode(),
                                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                    });
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void SaveResultEkyc(Ekyc.EkycOutput result, int loanbriefId)
        {
            try
            {
                //lưu dữ liệu vào bảng 
                var objEkyc = new DAL.EntityFramework.ResultEkyc()
                {
                    LoanbriefId = loanbriefId,
                    CreatedAt = DateTime.Now,
                    AddressValue = result.NationalId.Address.Value,
                    AddressCheck = result.NationalId.Address.Check,
                    IdValue = result.NationalId.Id.Value,
                    IdCheck = result.NationalId.Id.Check,
                    FullNameValue = result.NationalId.Fullname.Value,
                    FullNameCheck = result.NationalId.Fullname.Check,
                    BirthdayValue = result.NationalId.Birthday.Value,
                    BirthdayCheck = result.NationalId.Birthday.Check,
                    ExpiryValue = result.NationalId.Expiry.Value,
                    ExpiryCheck = result.NationalId.Expiry.Check,
                    GenderValue = result.NationalId.Gender.Value,
                    GenderCheck = result.NationalId.Gender.Check,
                    EthnicityValue = result.NationalId.Ethnicity.Value,
                    EthnicityCheck = result.NationalId.Ethnicity.Check,
                    IssueByValue = result.NationalId.IssueBy.Value,
                    IssueByCheck = result.NationalId.IssueBy.Check,
                    IssueDateValue = result.NationalId.IssueDate.Value,
                    IssueDateCheck = result.NationalId.IssueDate.Check,
                    ReligionValue = result.NationalId.Religion.Value,
                    ReligionCheck = result.NationalId.Religion.Check,
                    FaceCompareCode = result.FraudCheck.FaceCompare.Code,
                    FaceCompareMessage = result.FraudCheck.FaceCompare.Message,
                    FaceQueryCode = result.FraudCheck.FaceQuery.Code,
                    FaceQueryMessage = result.FraudCheck.FaceQuery.Message
                };
                _hubDistributingService.InsertEkycResult(objEkyc);
            }
            catch (Exception ex)
            {

            }
        }

        private void SendRequestCrawlFB(LoanBriefInfo entity, int LogRequestId)
        {
            try
            {
                if (entity != null)
                {
                    var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
                    if (!string.IsNullOrEmpty(token))
                    {
                        var data = _serviceAI.RegisterFacebook(token, entity, LogRequestId);
                        if (data != null)
                        {
                            if (data.status_code == 200)
                            {
                                //Get scroring thành công! với mã đơn HĐ-122334
                                Console.WriteLine(string.Format("Request Crawl FB done HĐ-: {0} {1}", entity.ID, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor SendRequestCrawlFB exception");
            }
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            //_timer?.Change(Timeout.Infinite, 0);
            //_timerReGet?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _threadRequest?.Abort();
            //_timerReGet?.Dispose();
        }


    }
}
