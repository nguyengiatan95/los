﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public sealed class AutoCancelLoanbriefJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly ILogActionService _logActionService;
        private readonly ILogCancelLoanbrief _logCancelLoanbrief;
        private Thread _thread;
        private int Day = DateTime.Now.Day;
        private bool isFirst = true;
        private readonly IConfiguration _configuration;
        public AutoCancelLoanbriefJob(ILoanBriefService loanBriefServices, ILogActionService logActionService, ILogCancelLoanbrief logCancelLoanbrief)
        {
            _loanBriefServices = loanBriefServices;
            _logActionService = logActionService;
            _logCancelLoanbrief = logCancelLoanbrief;

            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            this._configuration = builder.Build();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            while (true)
            {
                try
                {
                    if (IsRunning())
                    {
                        //var configDayover = int.Parse(_configuration["AppSettings:configDayover"]);
                        //var loanCredits = _loanBriefServices.GetListLoanOver(configDayover);ư
                        var listData = _logCancelLoanbrief.GetAllExcuted();
                        if (listData != null && listData.Count > 0)
                        {
                            foreach (var item in listData)
                            {
                                //var log = _loanBriefServices.GetFirstDistributingTelesales(item.LoanBriefId);
                                //if(log != null && ( DateTime.Now - log.CreatedAt.Value).TotalDays >= 4)
                                //{
                                if (item.LoanbriefId > 0)
                                    CancelLoanbrief(item);
                                //}

                            }
                        }
                        Day = DateTime.Now.Day;
                        if (isFirst)
                            isFirst = false;
                    }

                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor AutoCancelLoanbriefJob exception");
                }
                //Thread 10 minute
                Thread.Sleep(600000);
            }
        }

        private bool IsRunning()
        {
            //Thread.Sleep(1000 * 60 * 10);
            return true;
            //if (isFirst)
            //    return true;
            //if (!isFirst && Day != DateTime.Now.Day && DateTime.Now.Hour == 2)
            //    return true;
            //return false;
        }

        private async void CancelLoanbrief(LogCancelLoanbrief entity)
        {
            try
            {
                var loanbrief = _loanBriefServices.GetById(entity.LoanbriefId.Value);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (loanbrief.Status == (int)EnumLoanStatus.TELESALE_ADVICE
                        || loanbrief.Status == (int)EnumLoanStatus.SYSTEM_TELESALE_DISTRIBUTING
                        || loanbrief.Status == (int)EnumLoanStatus.INIT
                        || loanbrief.Status == (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN)
                    {
                        if (_loanBriefServices.CancelLoan(entity.LoanbriefId.Value, entity.ReasonCancel > 0 ? entity.ReasonCancel.Value : 478, string.IsNullOrEmpty(entity.ReasonComment) ? "Auto System: Hủy đơn do quá thời gian xử lý đơn" : entity.ReasonComment))
                        {
                            _loanBriefServices.AddNote(new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanbriefId,
                                Note = string.IsNullOrEmpty(entity.ReasonComment) ? "Auto System: Hủy đơn do quá thời gian xử lý đơn" : entity.ReasonComment,
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.AutoCancel.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            });
                            //Thêm log hủy đơn tự đông
                            await _logActionService.AddLogAction(new LogLoanAction
                            {
                                LoanbriefId = loanbrief.LoanBriefId,
                                ActionId = (int)EnumLogLoanAction.LoanCancel,
                                TypeAction = (int)EnumTypeAction.Manual,
                                LoanStatus = loanbrief.Status,
                                TlsLoanStatusDetail = loanbrief.DetailStatusTelesales,
                                HubLoanStatusDetail = loanbrief.LoanStatusDetailChild,
                                TelesaleId = loanbrief.BoundTelesaleId,
                                HubId = loanbrief.HubId,
                                HubEmployeeId = loanbrief.HubEmployeeId,
                                CoordinatorUserId = loanbrief.CoordinatorUserId,
                                UserActionId = 1,
                                GroupUserActionId = 1,
                                CreatedAt = DateTime.Now
                            });

                        }
                        Console.WriteLine(string.Format("Auto Cancel: Xu ly don HĐ-{0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
                _logCancelLoanbrief.IsExcuted(entity.Id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("CancelLoanbrief: EX {0} {1}", ex.StackTrace, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
