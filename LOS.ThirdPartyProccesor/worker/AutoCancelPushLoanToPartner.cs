﻿using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoCancelPushLoanToPartner : IHostedService, IDisposable
    {
        private readonly IPushLoanToPartnerService _pushLoanToPartnerService;


        private Thread _thread;
        private int Day = DateTime.Now.Day;
        private bool isFirst = true;
        public AutoCancelPushLoanToPartner(IPushLoanToPartnerService pushLoanToPartnerService)
        {
            _pushLoanToPartnerService = pushLoanToPartnerService;
        }
        private void DoWork(object state)
        {
            while (true)
            {
                if (IsRunning())
                {
                    Console.WriteLine($"Running AutoCancelPushLoanToPartner {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}");
                    var total = 0;
                    //lấy danh sách các đơn bán cho đối tác mà trong vòng 1 tháng chưa đổi trạng thái
                    var lstData = _pushLoanToPartnerService.GetAutoCancelLoanMirae();
                    if (lstData != null && lstData.Count > 0)
                    {
                        total = lstData.Count;
                        var index = 0;
                        foreach (var item in lstData)
                        {
                            index++;
                            Console.WriteLine($"Auto Cancel {index}/{total}");
                            //cập nhập lại trạng thái và note rõ lý do hủy
                            _pushLoanToPartnerService.CancelLoan(item, "Tima Auto hủy (30 ngày) ngày hủy: " + DateTime.Now.ToString("dd/MM/yyyy"));
                        }
                    }
                    Console.WriteLine($"Done {total} AutoCancelPushLoanToPartner {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}");
                    //thời gian chạy job
                    Day = DateTime.Now.Day;
                    if (isFirst)
                        isFirst = false;
                }
                //Sleep 15 minute
                Thread.Sleep(900000);
            }
        }
        private bool IsRunning()
        {
            //if (isFirst)
            //    return true;
            if (Day != DateTime.Now.Day && DateTime.Now.Hour == 1)
                return true;
            return false;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
