﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public sealed class AutoChangeDnsJob : IHostedService, IDisposable
    {
        private readonly ICloudFlareService _cloudFlareServices;
        private readonly IEmailService _emailServices;
        private Thread _thread;
        private readonly IConfiguration _configuration;

        public List<DomainObject> _listDomain;
        public AutoChangeDnsJob(ICloudFlareService cloudFlareServices, IEmailService emailServices)
        {
            _cloudFlareServices = cloudFlareServices;
            _emailServices = emailServices;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            this._configuration = builder.Build();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        public void InitData()
        {
            _listDomain = new List<DomainObject>();

            _listDomain.Add(new DomainObject
            {
                Url = "https://vaytien.tima.vn/",
                Domain = "vaytien.tima.vn",
                IpAddress = "103.35.65.57",
                IpAddressBackup = "52.74.73.109",
                CountDownTime = 0
            });
            _listDomain.Add(new DomainObject
            {
                Url = "https://vaymienbac.tima.vn/",
                Domain = "vaymienbac.tima.vn",
                IpAddress = "103.35.65.57",
                IpAddressBackup = "52.74.73.109",
                CountDownTime = 0
            });
            //_listDomain.Add(new DomainObject
            //{
            //    Url = "https://vaytima.com/",
            //    Domain = "vaytima.com",
            //    IpAddress = "103.35.65.57",
            //    IpAddressBackup = "52.74.73.109",
            //    CountDownTime = 0
            //});
            _listDomain.Add(new DomainObject
            {
                Url = "https://vayxemay.tima.vn/",
                Domain = "vayxemay.tima.vn",
                IpAddress = "103.35.65.57",
                IpAddressBackup = "52.74.73.109",
                CountDownTime = 0
            });
            _listDomain.Add(new DomainObject
            {
                Url = "https://vaynhanh.tima.vn/",
                Domain = "vaynhanh.tima.vn",
                IpAddress = "103.35.65.57",
                IpAddressBackup = "52.74.73.109",
                CountDownTime = 0
            });
            _listDomain.Add(new DomainObject
            {
                Url = "https://vaytienxemaynhanh.tima.vn/",
                Domain = "vaytienxemaynhanh.tima.vn",
                IpAddress = "103.35.65.57",
                IpAddressBackup = "52.74.73.109",
                CountDownTime = 0
            });
            //_listDomain.Add(new DomainObject
            //{
            //    Url = "https://vaytientima.com/",
            //    Domain = "vaytientima.com",
            //    IpAddress = "103.35.65.57",
            //    IpAddressBackup = "52.74.73.109",
            //    CountDownTime = 0
            //});
            _listDomain.Add(new DomainObject
            {
                Url = "https://tima.vn/",
                Domain = "tima.vn",
                IpAddress = "103.35.65.57",
                IpAddressBackup = "52.74.73.109",
                CountDownTime = 0
            });
            //_listDomain.Add(new DomainObject
            //{
            //    Url = "http://testlanding.tima.vn/",
            //    Domain = "testlanding.tima.vn",
            //    IpAddress = "52.74.73.109",
            //    IpAddressBackup = "52.221.50.99",
            //    CountDownTime = 0
            //});

        }
        private async void DoWork(object state)
        {
            InitData();
            while (true)
            {
                try
                {
                    if (IsRunning())
                    {
                        foreach (var domain in _listDomain)
                        {
                            bool isDownTime = await CheckDowntime(domain.Url);
                            //2p kiểm tra 1 lần. Nếu 3 lần liên tiếp domain chết =>
                            //Trong khung giờ làm việc: 9h => 17h30 => gửi email thông báo
                            //Ngoài khung giờ trên: gửi email thông báo và gọi đổi dns sang ip backup
                            if (isDownTime && !domain.IsChangeDns)
                            {
                                domain.CountDownTime++;
                                //Gửi email
                                var mailTo = _configuration["AppSettings:MailSendChangeDns"];
                                List<string> lstMailTo = mailTo.Split(',').ToList();
                                string emailTemplate = @$"<div style='font-family:Arial'>
                                                                  <div style='margin:0px;padding:10px;background:#cccccc;font-family:Arial;font-size:12px'>
                                                                        <div style='border-radius:6px;width:598px;margin:0px auto;padding:15px 0px;background:#ffffff;color:#4d4d4d'>
                                                                            <div style='width:546px;margin:0px auto;border:4px solid #007cc2'>
                                                                                <div style='padding:10px 20px'>
                                                                                    <div style='margin-bottom:0px'>
                                                                                        <p style='font-family:Arial'>Landingpage {domain.Url} bị chết sau khi kiểm tra tự động {domain.CountDownTime} lần</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>";
                                _emailServices.SendListMail(lstMailTo, string.Format("Landingpage {0} bị chết ngày {1}", domain.Url, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss fff")), emailTemplate);
                                Console.WriteLine(string.Format("SEND EMAIL LANDINGPAGE {0} DIE NGAY {1}", domain.Url, DateTime.Now.ToString("dd/MM/yyyy")));
                                if (domain.CountDownTime >= 3)
                                {
                                    //Nếu là ngày nghỉ: t7 cn
                                    //Hoặc ngoài khung giờ làm việc
                                    if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday
                                        || DateTime.Today.DayOfWeek == DayOfWeek.Sunday
                                        || DateTime.Now.Hour <= 8
                                        || DateTime.Now.Hour >= 18)
                                    {
                                        if (_configuration["ClouldFlare:AutoChange"] == "1")
                                            ChangeDns(domain);
                                    }
                                    domain.CountDownTime = 0;
                                    domain.IsChangeDns = true;
                                }
                            }
                            else
                                domain.CountDownTime = 0;
                        }
                        //Ngủ 2p
                        Thread.Sleep(120000);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor AutoChangeDnsJob exception");
                }
            }
        }
        public bool ChangeDns(DomainObject entity)
        {
            try
            {
                var listZone = _cloudFlareServices.GetAllZone();
                if (listZone != null && listZone.Count > 0)
                {
                    var listRecord = _cloudFlareServices.GetAllDnsRecord(listZone[0].id);
                    if (listRecord != null && listRecord.Count > 0)
                    {
                        foreach (var record in listRecord)
                        {
                            if (record.Name == entity.Domain && record.IpAddress == entity.IpAddress)
                            {
                                if (_cloudFlareServices.UpdateDnsRecord(record, entity.IpAddressBackup))
                                {
                                    //Cập nhật dns thành công
                                    return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine(string.Format("AutoChangeDnsJob: Khong tim thay list record {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
                else
                {
                    Console.WriteLine(string.Format("AutoChangeDnsJob: Khong tim thay zone {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
            }
            catch
            {

            }
            return false;
        }
        private async Task<bool> CheckDowntime(string url)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.Timeout = TimeSpan.FromSeconds(20);
                var checkingResponse = await client.GetAsync(url);
                if (checkingResponse != null && checkingResponse.IsSuccessStatusCode)
                    return false;
            }
            catch (Exception ex)
            {
            }
            return true;
        }
        private bool IsRunning()
        {
            return true;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
