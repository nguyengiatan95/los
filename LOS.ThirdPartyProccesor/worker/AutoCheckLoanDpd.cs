﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoCheckLoanDpd : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly ILMSService _lmsServices;
        //private Timer _timer;
        private Thread _thread;
        private int Day = DateTime.Now.Day;
        private bool isFirst = true;

        private List<int> listProductDetailFast = new List<int>()
            {
                (int)Common.Extensions.InfomationProductDetail.VayNhanh4tr,
                (int)Common.Extensions.InfomationProductDetail.KT1_KSHK_4tr,
                (int)Common.Extensions.InfomationProductDetail.KT1_KGT_4tr,
                (int)Common.Extensions.InfomationProductDetail.KT3_KTN_4tr,
                (int)Common.Extensions.InfomationProductDetail.XMCC_KT1_5tr,
                (int)Common.Extensions.InfomationProductDetail.XMCC_KT3_5tr
            };


        public AutoCheckLoanDpd(ILoanBriefService loanBriefServices, ILMSService lmsServices)
        {
            _loanBriefServices = loanBriefServices;
            _lmsServices = lmsServices;
        }
        private void DoWork(object state)
        {

            while (true)
            {
                try
                {
                    if (IsRunning())
                    {
                        var lstLoanMayTopupLms = GetLoanMayTopupLms();
                        if (lstLoanMayTopupLms != null)
                        {
                            // bỏ toàn bộ trạng thái có thể dpd của dữ liệu cũ
                            _loanBriefServices.UpdateAllIsCanDpd();
                            foreach (var item in lstLoanMayTopupLms)
                            {
                                try
                                {
                                    //lấy ra thông tin đơn vay dựa vào LmsLoanId
                                    var loanInfo = _loanBriefServices.LoanbriefInfoTopupByLmsLoanId(item.LmsLoanId);
                                    //var loanInfo = _loanBriefServices.LoanbriefInfoTopup(item.LoanbriefId);
                                    if (loanInfo == null 
                                        || loanInfo.LoanBriefId == 0 
                                        || loanInfo.TypeLoanSupport == (int)EnumTypeLoanSupport.IsCanTopup
                                        || loanInfo.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                                        continue;
                                    //Nếu là gói vay nhanh và xe không chính chủ => không cho tạo topup
                                    if (listProductDetailFast.Contains(loanInfo.ProductDetailId.GetValueOrDefault(0)) && loanInfo.ProductId != (int)EnumProductCredit.MotorCreditType_CC)
                                        continue;

                                    if (loanInfo.Status == (int)EnumLoanStatus.DISBURSED
                                        && (loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                            || loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                            || loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))
                                    {
                                        //Check đơn vay đã được tạo đơn Topup nào chưa
                                        var loanTopup = _loanBriefServices.LoanbriefInfoByReMKTId(loanInfo.LoanBriefId);
                                        if (loanTopup == null)
                                        {
                                            //check xem là loại đơn nào
                                            //nếu là có thể topup check sang lms
                                            //if (item.Type == (int)EnumTypeLoanSupport.IsCanTopup)
                                            //{
                                            //    //Call qua LMS để check xem đơn có đủ điều kiện Topup hay không
                                            //    long totalMoneyCurrent = 0;
                                            //    var lstPayment = _lmsServices.GetPaymentLoanById(item.LmsLoanId, loanInfo.LoanBriefId);
                                            //    if (lstPayment.Status == 1)
                                            //    {
                                            //        //kiểm tra xem có đơn đang xử lý hay không
                                            //        totalMoneyCurrent = lstPayment.Data.Loan.TotalMoneyCurrent;
                                            //        if (lstPayment.Data.Loan.TopUp == 1)
                                            //        {
                                            //            var priceTopup = GetMaxPriceProductTopup((int)loanInfo.LoanBriefProperty.ProductId, (int)loanInfo.ProductId, loanInfo.LoanBriefId, totalMoneyCurrent);
                                            //            var maxPrice = priceTopup.MaxPriceProduct;

                                            //            //Số tiền nhỏ hơn 1tr => không đạt yêu cầu => update TypeRemarketing = 0
                                            //            if (maxPrice >= 1000000)
                                            //            {
                                            //                _loanBriefServices.UpdateTypeRemarketing(loanInfo.LoanBriefId, (int)EnumTypeLoanSupport.IsCanTopup);
                                            //                Console.WriteLine(string.Format("DU DIEU KIEN TOPUP:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                            //            }
                                            //            else
                                            //            {
                                            //                _loanBriefServices.UpdateTypeRemarketing(loanInfo.LoanBriefId, 0);
                                            //                Console.WriteLine(string.Format("DON VAY KHONG DU DIEU KIEN TOPUP - LOANAMOUNT:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                            //            }
                                            //        }
                                            //        else
                                            //        {
                                            //            _loanBriefServices.UpdateTypeRemarketing(loanInfo.LoanBriefId, 0);
                                            //            Console.WriteLine(string.Format("DON VAY KHONG DU DIEU KIEN TOPUP - AG:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                            //        }
                                            //    }
                                            //    else
                                            //    {
                                            //        Console.WriteLine(string.Format("Co loi say ra khi call api check topup ag HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                            //    }
                                            //}
                                            if (item.Type == (int)EnumTypeLoanSupport.DPD)
                                            {
                                                _loanBriefServices.UpdateTypeRemarketing(loanInfo.LoanBriefId, (int)EnumTypeLoanSupport.DPD);
                                                Console.WriteLine(string.Format("[SuggestTopUp] DU DIEU KIEN TOPUP:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                            }
                                        }
                                        else
                                        {
                                            _loanBriefServices.UpdateTypeRemarketing(loanInfo.LoanBriefId, 0);
                                            Console.WriteLine(string.Format("DA CO DON VAY TOPUP:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                        }

                                    }
                                    else
                                    {
                                        //totalNotSupportProduct++; 
                                        Console.WriteLine(string.Format("DON VAY KHONG DU DIEU KIEN TOPUP - PRODUCT:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                        _loanBriefServices.UpdateTypeRemarketing(loanInfo.LoanBriefId, 0);
                                    }

                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                                }
                            }
                            Day = DateTime.Now.Day;
                            if (isFirst)
                                isFirst = false;
                        }                       
                        Day = DateTime.Now.Day;
                        if (isFirst)
                            isFirst = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                    Log.Error(ex, "LOS.ThirdPartyProccesor CheckLoanTopup exception");
                }
                //Sleep 15 minute
                Thread.Sleep(1000 * 60 * 15);
            }
        }
        private bool IsRunning()
        {
            if (Day != DateTime.Now.Day && DateTime.Now.Hour == 5)
                return true;
            return false;
        }
        private List<LoanMayTopupLms> GetLoanMayTopupLms()
        {
            var lstData = new List<LoanMayTopupLms>();
            //gọi api sang lms lấy những đơn vay sắp được topup
            var objListLoanSuggestTopUp = _lmsServices.GetListLoanSuggestTopUp();
            if (objListLoanSuggestTopUp.Status == 1 && objListLoanSuggestTopUp.Data != null && objListLoanSuggestTopUp.Data.Count > 0)
            {
                foreach (var item in objListLoanSuggestTopUp.Data)
                {
                    if (!lstData.Any(k => k.LmsLoanId == item))
                    {
                        var data = new LoanMayTopupLms()
                        {
                            LmsLoanId = item,
                            Type = (int)EnumTypeLoanSupport.DPD,
                        };
                        lstData.Add(data);
                    }
                }
                //add dữ liệu vào list
                //Parallel.ForEach(objListLoanSuggestTopUp.Data, item =>
                //{
                //    //kiểm tra xem trong list có loanId chưa có rồi thì bỏ qua
                //    if (!lstData.Any(x => x.LmsLoanId == item))
                //    {
                //        var data = new LoanMayTopupLms()
                //        {
                //            LmsLoanId = item,
                //            Type = (int)EnumTypeLoanSupport.DPD,
                //        };
                //        lstData.Add(data);
                //    }
                //});
            }
            return lstData;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
