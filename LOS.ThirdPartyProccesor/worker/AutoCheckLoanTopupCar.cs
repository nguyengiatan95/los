﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoCheckLoanTopupCar : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly ILMSService _lmsServices;
        //private Timer _timer;
        private Thread _threadCar;
        private bool isFirstCar = true;
        private int DayCar = DateTime.Now.Day;

        public AutoCheckLoanTopupCar(ILoanBriefService loanBriefServices, ILMSService lmsServices)
        {
            _loanBriefServices = loanBriefServices;
            _lmsServices = lmsServices;
           
        }

        private void DoWorkCar(object state)
        {

            while (true)
            {
                try
                {
                    if (IsRunningCar())
                    {
                        //lấy ra danh sách đơn đang vay ô tô sắp xếp theo mã KH
                        var taskLoanDisbursed = _loanBriefServices.ListLoanDisbursedCar();
                        var listLoanDisbursed = taskLoanDisbursed.Result;
                        //gom nhóm đơn vay theo khách hàng
                        var dicLoan = new Dictionary<int, List<LoanDisbursed>>();
                        if (listLoanDisbursed != null && listLoanDisbursed.Count > 0)
                        {
                            foreach (var item in listLoanDisbursed)
                            {
                                if (!dicLoan.ContainsKey(item.CustomerId))
                                    dicLoan[item.CustomerId] = new List<LoanDisbursed>();
                                dicLoan[item.CustomerId].Add(item);
                            }
                        }
                        if (dicLoan != null && dicLoan.Keys.Count > 0)
                        {
                            //bỏ toán bộ những đơn đang có thể topup
                            if (_loanBriefServices.RemoveIsCanTopup((int)EnumProductCredit.OtoCreditType_CC).Result)
                            {
                                foreach (var customerId in dicLoan.Keys)
                                {
                                    //kiểm tra xem KH có đơn topup đang vay hay không
                                    if (!dicLoan[customerId].Any(x => x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp))
                                    {
                                        //kiểm tra xem KH có đơn topup nào đang xử lý hay không
                                        var loanbriefTopup = _loanBriefServices.GetLoanTopupHandle(customerId);
                                        if (loanbriefTopup.Result != null && loanbriefTopup.Result.LoanBriefId > 0)
                                        {
                                            Console.WriteLine("KH đang có đơn topup đang được xử lý");
                                        }
                                        else
                                        {
                                            //kiểm tra xem KH có đủ điều kiện topup không
                                            var loanbriefFirst = dicLoan[customerId][0];
                                            if (loanbriefFirst != null && loanbriefFirst.LoanbriefId > 0
                                                && !string.IsNullOrEmpty(loanbriefFirst.FullName)
                                                && !string.IsNullOrEmpty(loanbriefFirst.NationalCard))
                                            {
                                                var req = new CheckReLoan.Input()
                                                {
                                                    CustomerName = loanbriefFirst.FullName,
                                                    NumberCard = loanbriefFirst.NationalCard,
                                                };
                                                var res = _lmsServices.CheckTopupOto(req, loanbriefFirst.LoanbriefId);
                                                if (res != null && res.IsAccept == 1)
                                                {
                                                    //lấy danh sách đơn vay của KH chuyển về đơn có thể topup
                                                    foreach (var item in dicLoan[customerId])
                                                    {
                                                        if (item.TypeLoanSupport != (int)EnumTypeLoanSupport.IsCanTopup)
                                                        {
                                                            Console.WriteLine(string.Format("[CAR] DU DIEU KIEN TOPUP:  HĐ-{0} {1}", item.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                                            _loanBriefServices.UpdateTypeRemarketing(item.LoanbriefId, (int)EnumTypeLoanSupport.IsCanTopup);
                                                        }                                                           
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        DayCar = DateTime.Now.Day;
                        if (isFirstCar)
                            isFirstCar = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                    Log.Error(ex, "LOS.ThirdPartyProccesor DoWorkCar exception");
                }
                //Sleep 15 minute
                Thread.Sleep(1000 * 60 * 15);
            }
        }

        private bool IsRunningCar()
        {
            if (isFirstCar)
                return true;
            if (!isFirstCar && DayCar != DateTime.Now.Day && DateTime.Now.Hour == 1)
                return true;
            return false;
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _threadCar = new Thread(() => DoWorkCar(null));
            _threadCar.Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _threadCar?.Abort();
        }
    }
}
