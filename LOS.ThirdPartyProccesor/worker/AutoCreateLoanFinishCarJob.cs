﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Objects.Finance;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoCreateLoanFinishCarJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly ILogResultAutoCallServices _logResultAutoCall;
        private readonly IConfiguration _baseConfig;
        private readonly ILMSService _lmsService;
        private readonly IErpService _erpService;

        private Thread _thread;
        public AutoCreateLoanFinishCarJob(ILogResultAutoCallServices logResultAutoCall, ILoanBriefService loanBriefServices, ILMSService lmsService, IErpService erpService)
        {
            _logResultAutoCall = logResultAutoCall;
            _loanBriefServices = loanBriefServices;
            _lmsService = lmsService;
            _erpService = erpService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        public void Dispose()
        {
            _thread?.Abort();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private void DoWork(object state)
        {
            while (true)
            {
                //lấy ra danh sách đơn vay ô tô đã tất toán
                var data = _loanBriefServices.GetListLoanFinish("6,8,11", 15);
                if (data != null && data.Count > 0)
                {
                    var total = data.Count;
                    var index = 0;
                    int totalHandle = 0;
                    int totalBlacklist = 0;
                    int totalLoan = 0;
                    int totalNotReborrow = 0;
                    foreach (var item in data)
                    {
                        try
                        {
                            index++;
                            Console.WriteLine($"{index}/{total} Xu ly don vay HĐ-{item.LoanBriefId}");

                            if (!string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.NationalCard))
                            {
                               
                                // Kiểm tra blackList
                                var ResultCheckBlackList = _lmsService.CheckBlackList(item.FullName, item.NationalCard, item.Phone, item.LoanBriefId);
                                if (ResultCheckBlackList != null && ResultCheckBlackList.Data == true && ResultCheckBlackList.Status == 1)
                                {
                                    totalBlacklist++;
                                    Console.WriteLine($"HĐ-{item.LoanBriefId} nam trong blackList");
                                    continue;
                                }

                                //kiểm tra nhân viên tima
                                var checkEmoloyeeTima = _erpService.CheckEmployeeTima(item.NationalCard, item.Phone, item.LoanBriefId);
                                if (checkEmoloyeeTima != null)
                                {
                                    Console.WriteLine($"HĐ-{item.LoanBriefId} la nhan vien Tima!");
                                    continue;
                                }

                                //Kiểmt ra đã có đơn xe máy đang xử lý không
                                if (_loanBriefServices.LoanbriefHandleCar(item.Phone, item.NationalCard))
                                {
                                    totalHandle++;
                                    //Có đơn vay đang được xử lý
                                    Console.WriteLine($"HĐ-{item.LoanBriefId} co don vay khac dang duoc xu ly");
                                    continue;
                                }

                                //KH đủ điều kiện tái vay
                                //tạo đơn tái vay cho KH
                                //Gán vào tài khoản follow
                                //utm_source: rmkt_loan_finish
                                var loanbriefId = _loanBriefServices.ReCreateLoanFinishCar(item.LoanBriefId);
                                if (loanbriefId > 0)
                                {
                                    totalLoan++;
                                    //tạo mới thành công
                                    Console.WriteLine($"HĐ-{loanbriefId} khoi tao tu don vay HĐ-{item.LoanBriefId}");
                                    var note = new LoanBriefNote
                                    {
                                        LoanBriefId = loanbriefId,
                                        Note = $"Khởi tạo đơn vay đã tất toán từ HĐ-{item.LoanBriefId}",
                                        FullName = "Auto System",
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = 1,
                                        ShopId = EnumShop.Tima.GetHashCode(),
                                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                    };
                                    _loanBriefServices.AddNote(note);
                                }
                                else
                                {
                                    //tạo mới thất bại
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    Console.WriteLine($"Khong du dieu kien tai vay {totalNotReborrow}/{total}");
                    Console.WriteLine($"Co don dang xu ly {totalHandle}/{total}");
                    Console.WriteLine($"Blacklist {totalBlacklist}/{total}");
                    Console.WriteLine($"Don vay duoc khoi tao {totalLoan}/{total}");
                }
                //lấy danh sách đơn đủ điều kiện khởi tạo lại
                Thread.Sleep(1000 * (60 * 10)); // nghỉ giảo lao 30 phút
            }


        }
    }
}
