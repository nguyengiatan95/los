﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Objects.Finance;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoCreateLoanFinishJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly ILMSService _lmsService;
        private readonly IErpService _erpService;
        private readonly ILogReLoanbrief _logReLoanbriefService;
        private int Day = DateTime.Now.Day;
        private bool isFirst = true;

        private Thread _thread;
        public AutoCreateLoanFinishJob(ILoanBriefService loanBriefServices, ILMSService lmsService,
            IErpService erpService, ILogReLoanbrief logReLoanbriefService)
        {
            _loanBriefServices = loanBriefServices;
            _lmsService = lmsService;
            _erpService = erpService;
            _logReLoanbriefService = logReLoanbriefService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
        }

        public void Dispose()
        {
            _thread?.Abort();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private bool IsRunning()
        {
            if (isFirst)
                return true;
            if (!isFirst && Day != DateTime.Now.Day && DateTime.Now.Hour == 3)
                return true;
            return false;
        }
        private void DoWork(object state)
        {
            while (true)
            {
                if (IsRunning())
                {
                    Console.WriteLine($"Running AutoCreateLoanFinishJob {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}");
                    //lấy ra danh sách đơn vay xe máy đã tất toán và chưa được khởi tạo trong 15 ngày
                    var data = _loanBriefServices.GetListLoanFinish(Constants.LIST_PRODUCT_MOTOBIKE, Constants.LIMITED_DAY_15);
                    var total = 0;
                    if (data != null && data.Count > 0)
                    {
                        total = data.Count;
                        //Lấy ra danh sách đơn đã từng được khởi tạo
                        foreach (var item in data)
                        {
                            try
                            {
                                
                                if (!string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.NationalCard))
                                {
                                    var utmSource = "rmkt_loan_finish";
                                    var lastLogReLoanbrief = _logReLoanbriefService.GetLast(item.LoanBriefId, item.NationalCard);
                                    if (lastLogReLoanbrief != null)
                                    {
                                        //nếu tạo đơn trong vòng 15 ngày thì gán nhãn đơn có thể Topup
                                        if (Convert.ToInt32((DateTime.Now - lastLogReLoanbrief.CreatedAt.Value).TotalDays) <= 15)
                                            continue;
                                        utmSource = "rmkt_loan_finish_renew";
                                    }

                                    //Kiểm tra điều kiện tái vay
                                    var obj = new CheckReLoan.Input
                                    {
                                        NumberCard = item.NationalCard,
                                        CustomerName = item.FullName
                                    };
                                    var checkReloan = _lmsService.CheckReBorrow(obj, item.LoanBriefId);
                                    if (checkReloan != null && checkReloan.Result == 1 && checkReloan.IsAccept == 1)
                                    {

                                        //Kiểmt ra đã có đơn xe máy đang xử lý không
                                        if (_loanBriefServices.LoanbriefHandle(item.Phone, item.NationalCard))
                                            continue;

                                        // Kiểm tra blackList
                                        var ResultCheckBlackList = _lmsService.CheckBlackList(item.FullName, item.NationalCard, item.Phone, item.LoanBriefId);
                                        if (ResultCheckBlackList != null && ResultCheckBlackList.Data == true && ResultCheckBlackList.Status == 1)
                                            continue;

                                        //kiểm tra nhân viên tima
                                        var checkEmoloyeeTima = _erpService.CheckEmployeeTima(item.NationalCard, item.Phone, item.LoanBriefId);
                                        if (checkEmoloyeeTima != null)
                                            continue;

                                        //thêm event log khởi tạo tái vay
                                        var logReLoanbrief = new LogReLoanbrief()
                                        {
                                            LoanbriefId = item.LoanBriefId,
                                            CreatedAt = DateTime.Now,
                                            CreatedBy = 1,
                                            TypeRemarketing = (int)EnumTypeRemarketing.IsRemarketing,
                                            IsExcuted = (int)LogReLoanbriefExcuted.NoExcuted,
                                            TypeReLoanbrief = (int)TypeReLoanbrief.FinishLoanbrief,
                                            UtmSource = utmSource,
                                            TelesaleId = (int)EnumUser.follow,
                                            NationalCard = item.NationalCard
                                        };
                                        _logReLoanbriefService.Add(logReLoanbrief);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine($"Exception {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} AutoCreateLoanFinishJob: {ex.ToString()}");
                            }
                        }
                    }
                    Day = DateTime.Now.Day;
                    if (isFirst)
                        isFirst = false;

                    Console.WriteLine($"Done {total} AutoCreateLoanFinishJob {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}");
                }
                Thread.Sleep(1800000); // sleep 30 minute
            }
        }
    }
}
