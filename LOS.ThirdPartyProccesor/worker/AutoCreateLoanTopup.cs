﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.Services.Services.Loanbrief;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoCreateLoanTopup : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly ILoanbriefV2Service _loanBriefV2Services;
        private readonly ILMSService _lmsServices;
        private readonly IProductServices _productServices;
        private readonly ILogReLoanbrief _logReLoanbrief;
        //private Timer _timer;
        private Thread _thread;
        private int Day = DateTime.Now.Day;
        private bool isFirst = true;

        private List<int> listProductDetailFast = new List<int>()
            {
                (int)Common.Extensions.InfomationProductDetail.VayNhanh4tr,
                (int)Common.Extensions.InfomationProductDetail.KT1_KSHK_4tr,
                (int)Common.Extensions.InfomationProductDetail.KT1_KGT_4tr,
                (int)Common.Extensions.InfomationProductDetail.KT3_KTN_4tr,
                (int)Common.Extensions.InfomationProductDetail.XMCC_KT1_5tr,
                (int)Common.Extensions.InfomationProductDetail.XMCC_KT3_5tr
            };

        public AutoCreateLoanTopup(ILoanBriefService loanBriefServices, ILMSService lmsServices, IProductServices productServices, ILogReLoanbrief logReLoanbrief,
            ILoanbriefV2Service loanBriefV2Services)
        {
            _loanBriefServices = loanBriefServices;
            _lmsServices = lmsServices;
            _productServices = productServices;
            _logReLoanbrief = logReLoanbrief;
            _loanBriefV2Services = loanBriefV2Services;
        }
        private async void DoWork(object state)
        {
            while (true)
            {
                try
                {
                    if (IsRunning())
                    {
                        Console.WriteLine($"Running AutoCreateLoanTopup {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}");
                        var total = 0;
                        var listData = GetAllData();
                        if (listData != null && listData.Count > 0)
                        {
                            total = listData.Count;
                            var index = 0;
                            //Lấy ra danh sách đơn vay đã có đơn topup đang xử lý
                            var listLoanbriefIdTopupProcessing = _loanBriefServices.AllLoanBriefIdHasTopupProcessing();
                            //Lấy ra danh sách đơn vay đã có đơn dpd đang xử lý
                            var listLoanbriefIdDpdProcessing = _loanBriefServices.AllLoanBriefIdDPDProcessing();
                            var listNationalTopupDisbursed = _loanBriefServices.ListNationalTopupDisbursed();
                            // bỏ toàn bộ trạng thái có thể Topup của dữ liệu cũ
                            _loanBriefServices.UpdateAllIsCanTopup();
                            foreach (var loanInfo in listData)
                            {
                                var isTopup = false;
                                index++;
                                Console.WriteLine($"Auto Topup {index}/{total} {loanInfo.LmsLoanId}-{loanInfo.LoanBriefId}");
                                try
                                {
                                    //Nếu là gói vay nhanh và xe không chính chủ => không cho tạo topup
                                    if (listProductDetailFast.Contains(loanInfo.ProductDetailId.GetValueOrDefault(0)) && loanInfo.ProductId != (int)EnumProductCredit.MotorCreditType_CC)
                                        continue;

                                    if (loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                            || loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                            || loanInfo.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                    {

                                        //Kiểm tra xem có đơn topup đang xử lý hay không
                                        if (listLoanbriefIdTopupProcessing != null && listLoanbriefIdTopupProcessing.Contains(loanInfo.LoanBriefId))
                                        {
                                            Console.WriteLine(string.Format("DA CO DON VAY TOPUP:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                            continue;
                                        }
                                        //kiểm tra xem có đơn DPD đang xử lý hay không
                                        if (listLoanbriefIdDpdProcessing != null && listLoanbriefIdDpdProcessing.Contains(loanInfo.LoanBriefId))
                                        {
                                            Console.WriteLine(string.Format("DA CO DON VAY DPD:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                            continue;
                                        }
                                        if (loanInfo.TypeRemarketing != (int)EnumTypeRemarketing.IsTopUp)
                                        {
                                            // //kiểm tra xem có đơn topup đã GN
                                            if (listNationalTopupDisbursed != null && listNationalTopupDisbursed.Contains(loanInfo.NationalCard))
                                            {
                                                Console.WriteLine(string.Format("DA co don topup dang vay:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                                continue;
                                            }
                                        }

                                        //Call qua LMS để check xem đơn có đủ điều kiện Topup hay không
                                        decimal totalMoneyCurrent = 0;
                                        var checkTopup = await _loanBriefV2Services.CheckCanTopup(loanInfo.LoanBriefId);
                                        if (checkTopup.IsCanTopup)
                                        {
                                            totalMoneyCurrent = checkTopup.CurrentDebt;
                                            isTopup = true;
                                            var priceTopup = GetMaxPriceProductTopup((int)loanInfo.LoanBriefProperty.ProductId, (int)loanInfo.ProductId, loanInfo.LoanBriefId, loanInfo.ResidentType, totalMoneyCurrent);
                                            var maxPrice = priceTopup.MaxPriceProduct;
                                            if(priceTopup.PriceAi == 0)
                                            {
                                                _loanBriefServices.UpdateTypeRemarketing(loanInfo.LoanBriefId, (int)EnumTypeLoanSupport.IsCanTopup);
                                                continue;
                                            }
                                            //Số tiền nhỏ hơn 3tr => không đạt yêu cầu => update TypeRemarketing = 0
                                            if (maxPrice >= 3000000)
                                            {
                                                var utmSource = "form_rmkt_topup";
                                                if(loanInfo.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                                                    utmSource = "form_rmkt_topup_2";
                                                //kiểm tra xem có tạo đơn trong vòng 15 ngày không
                                                //nếu tạo đơn trong vòng 15 ngày thì gán nhãn đơn có thể Topup
                                                var logReLoanbrief = _logReLoanbrief.GetLast(loanInfo.LoanBriefId, loanInfo.NationalCard);
                                                if (logReLoanbrief != null)
                                                {
                                                    //nếu tạo đơn trong vòng 15 ngày thì gán nhãn đơn có thể Topup
                                                    if (Convert.ToInt32((DateTime.Now - logReLoanbrief.CreatedAt.Value).TotalDays) <= 15)
                                                    {
                                                        if (_loanBriefServices.UpdateTypeRemarketing(loanInfo.LoanBriefId, (int)EnumTypeLoanSupport.IsCanTopup))
                                                        {
                                                            _loanBriefServices.AddNote(new LoanBriefNote
                                                            {
                                                                LoanBriefId = loanInfo.LoanBriefId,
                                                                Note = "Đơn vay đủ điều kiện tạo hợp đồng topup",
                                                                FullName = "Auto System",
                                                                Status = 0,
                                                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                                                CreatedTime = DateTime.Now,
                                                                UserId = 1,
                                                                ShopId = EnumShop.Tima.GetHashCode(),
                                                                ShopName = "Tima"
                                                            });
                                                        }
                                                        continue;
                                                    }
                                                    //nếu được tạo rồi thì đổi utmsource="form_rmkt_topup_renew"
                                                    utmSource = "form_rmkt_topup_renew";
                                                    if (loanInfo.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                                                        utmSource = "form_rmkt_topup_2_renew";
                                                }
                                                //Insert vào bảng LogReloanbrief để hệ thống tự động tạo đơn
                                                _logReLoanbrief.Add(new LogReLoanbrief()
                                                {
                                                    LoanbriefId = loanInfo.LoanBriefId,
                                                    CreatedAt = DateTime.Now,
                                                    CreatedBy = 1,
                                                    TypeRemarketing = EnumTypeRemarketing.IsTopUp.GetHashCode(),
                                                    UtmSource = utmSource,
                                                    IsExcuted = (int)LogReLoanbriefExcuted.NoExcuted,
                                                    TypeReLoanbrief = TypeReLoanbrief.LoanBriefTopup.GetHashCode(),
                                                    TelesaleId = (int)EnumUser.rmkt_topup,
                                                    NationalCard = loanInfo.NationalCard
                                                });
                                                Console.WriteLine(string.Format("DON VAY DUOC TAO TOPUP TU DONG:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                            }
                                            else
                                            {
                                                Console.WriteLine(string.Format("DON VAY KHONG DU DIEU KIEN TOPUP - LOANAMOUNT:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                                continue;
                                            }

                                        }
                                        else
                                        {
                                            Console.WriteLine(string.Format("DON VAY KHONG DU DIEU KIEN TOPUP - AG:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                            continue;
                                        }                                       
                                        #region Checktopup cũ
                                        ////Call qua LMS để check xem đơn có đủ điều kiện Topup hay không
                                        //long totalMoneyCurrent = 0;
                                        //var lstPayment = _lmsServices.GetPaymentLoanById(loanInfo.LmsLoanId.Value, loanInfo.LoanBriefId);
                                        //if (lstPayment.Status == 1)
                                        //{
                                        //    //kiểm tra xem có đơn đang xử lý hay không
                                        //    totalMoneyCurrent = lstPayment.Data.Loan.TotalMoneyCurrent;
                                        //    if (lstPayment.Data.Loan.TopUp == 1)
                                        //    {
                                        //        isTopup = true;
                                        //        var priceTopup = GetMaxPriceProductTopup((int)loanInfo.LoanBriefProperty.ProductId, (int)loanInfo.ProductId, loanInfo.LoanBriefId, totalMoneyCurrent);
                                        //        var maxPrice = priceTopup.MaxPriceProduct;

                                        //        //Số tiền nhỏ hơn 1tr => không đạt yêu cầu => update TypeRemarketing = 0
                                        //        if (maxPrice >= 1000000)
                                        //        {
                                        //            var utmSource = "form_rmkt_topup";
                                        //            //kiểm tra xem có tạo đơn trong vòng 15 ngày không
                                        //            //nếu tạo đơn trong vòng 15 ngày thì gán nhãn đơn có thể Topup
                                        //            var logReLoanbrief = _logReLoanbrief.GetLast(loanInfo.LoanBriefId);
                                        //            if (logReLoanbrief != null)
                                        //            {
                                        //                //nếu tạo đơn trong vòng 15 ngày thì gán nhãn đơn có thể Topup
                                        //                if (Convert.ToInt32((DateTime.Now - logReLoanbrief.CreatedAt.Value).TotalDays) <= 15)
                                        //                {
                                        //                    if (_loanBriefServices.UpdateTypeRemarketing(loanInfo.LoanBriefId, (int)EnumTypeLoanSupport.IsCanTopup))
                                        //                    {
                                        //                        _loanBriefServices.AddNote(new LoanBriefNote
                                        //                        {
                                        //                            LoanBriefId = loanInfo.LoanBriefId,
                                        //                            Note = "Đơn vay đủ điều kiện tạo hợp đồng topup",
                                        //                            FullName = "Auto System",
                                        //                            Status = 0,
                                        //                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        //                            CreatedTime = DateTime.Now,
                                        //                            UserId = 1,
                                        //                            ShopId = EnumShop.Tima.GetHashCode(),
                                        //                            ShopName = "Tima"
                                        //                        });
                                        //                    }
                                        //                    continue;
                                        //                }
                                        //                //nếu được tạo rồi thì đổi utmsource="form_rmkt_topup_renew"
                                        //                utmSource = "form_rmkt_topup_renew";
                                        //            }
                                        //            //Insert vào bảng LogReloanbrief để hệ thống tự động tạo đơn
                                        //            var objLogReLoanbrief = new LogReLoanbrief()
                                        //            {
                                        //                LoanbriefId = loanInfo.LoanBriefId,
                                        //                CreatedAt = DateTime.Now,
                                        //                CreatedBy = 1,
                                        //                TypeRemarketing = EnumTypeRemarketing.IsTopUp.GetHashCode(),
                                        //                UtmSource = utmSource,
                                        //                IsExcuted = (int)LogReLoanbriefExcuted.NoExcuted,
                                        //                TypeReLoanbrief = TypeReLoanbrief.LoanBriefTopup.GetHashCode(),
                                        //                TelesaleId = (int)EnumUser.rmkt_topup,
                                        //            };
                                        //            _logReLoanbrief.Add(objLogReLoanbrief);
                                        //            Console.WriteLine(string.Format("DON VAY DUOC TAO TOPUP TU DONG:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                        //        }
                                        //        else
                                        //        {
                                        //            Console.WriteLine(string.Format("DON VAY KHONG DU DIEU KIEN TOPUP - LOANAMOUNT:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                        //            continue;
                                        //        }
                                        //    }
                                        //    else
                                        //    {
                                        //        Console.WriteLine(string.Format("DON VAY KHONG DU DIEU KIEN TOPUP - AG:  HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                        //        continue;
                                        //    }
                                        //}
                                        //else
                                        //{
                                        //    Console.WriteLine(string.Format("Co loi say ra khi call api check topup ag HĐ-{0} {1}", loanInfo.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                        //    continue;
                                        //}
                                        #endregion

                                    }
                                    else continue;

                                }
                                catch (Exception ex)
                                {
                                    if (isTopup)
                                    {
                                        if (loanInfo != null && loanInfo.LoanBriefId > 0)
                                        {
                                            if (_loanBriefServices.UpdateTypeRemarketing(loanInfo.LoanBriefId, (int)EnumTypeLoanSupport.IsCanTopup))
                                            {
                                                _loanBriefServices.AddNote(new LoanBriefNote
                                                {
                                                    LoanBriefId = loanInfo.LoanBriefId,
                                                    Note = "Đơn vay đủ điều kiện tạo hợp đồng topup",
                                                    FullName = "Auto System",
                                                    Status = 0,
                                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                                    CreatedTime = DateTime.Now,
                                                    UserId = 1,
                                                    ShopId = EnumShop.Tima.GetHashCode(),
                                                    ShopName = "Tima"
                                                });
                                            }
                                        }
                                    }
                                    Console.WriteLine($"Exception {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} AutoCreateLoanTopup: {ex.ToString()}");
                                }
                            }
                        }
                        Console.WriteLine($"Done {total} AutoCreateLoanTopup {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}");
                        //thời gian chạy job
                        Day = DateTime.Now.Day;
                        if (isFirst)
                            isFirst = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                    Log.Error(ex, "LOS.ThirdPartyProccesor AutoCreateLoanTopup exception");
                }
                //Sleep 15 minute
                Thread.Sleep(900000);
            }
        }

        private List<LoanBriefInfoForTopup> GetAllData()
        {
            var result = new List<LoanBriefInfoForTopup>();
            try
            {
                var objListLoanTopUp = _lmsServices.GetListLoanTopUp();
               //var objListLoanTopUp = _lmsServices.GetListLoanTopUpLastDb();
                if (objListLoanTopUp.Status == 1 && objListLoanTopUp.Data != null && objListLoanTopUp.Data.Count > 0)
                {
                    var listData = _loanBriefServices.GetLoanbriefFromLmsLoanId(objListLoanTopUp.Data);
                    if (listData != null)
                        result.AddRange(listData);
                }
                var listLoanCanTopupOnTopup = _loanBriefServices.GetLoanCanTopupOnTopup();
                if (listLoanCanTopupOnTopup != null && listLoanCanTopupOnTopup.Count > 0)
                {
                    result.AddRange(listLoanCanTopupOnTopup);
                }
            }
            catch
            {

            }
            return result;
        }
        private PriceProductTopup GetMaxPriceProductTopup(int productId, int productCredit, int loanBriefId, int typeOfOwnerShip, decimal totalMoneyCurrent)
        {
            long originalCarPrice = 0;
            // lấy giá bên AI
            decimal priceCarAI = _productServices.GetPriceAI(productId, loanBriefId);
            if (priceCarAI > 0)
            {
                originalCarPrice = (long)priceCarAI;
            }
            else
            {
                var productPriceCurrent = _productServices.GetProduct(productId);
                if (productPriceCurrent != null && productPriceCurrent.PriceCurrent > 0)
                    originalCarPrice = productPriceCurrent.PriceCurrent ?? 0l;
            }
            //var originalCarPriceLast = originalCarPrice - TotalMoneyCurrent;
            var data = new PriceProductTopup
            {
                PriceAi = originalCarPrice,
                MaxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPriceTopup(productCredit, originalCarPrice, (long)totalMoneyCurrent, typeOfOwnerShip)
            };
            return data;
        }
        private bool IsRunning()
        {
            //if (isFirst)
            //    return true;
            if (Day != DateTime.Now.Day && DateTime.Now.Hour == 3)
                return true;
            return false;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
