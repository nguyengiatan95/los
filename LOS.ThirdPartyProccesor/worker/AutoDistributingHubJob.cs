﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object;
using LOS.Services.Services.Loanbrief;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static LOS.ThirdPartyProccesor.Objects.Ekyc;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoDistributingHubJob : IHostedService, IDisposable
    {
        private int executionCount = 0;
        //private Timer _timer;

        private Thread _thread;
        protected IConfiguration _baseConfig;
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IShopService _shopServices;
        private readonly IHubDistributingService _hubDistributingService;
        private readonly ILogPushHubService _logPushHubService;
        private readonly ISlackService _slackService;
        private readonly ISendMessage _sendNotification;
        private readonly IAuthenticationAI _authenticationAI;
        private readonly ILMSService _lmsService;
        private readonly IUserService _userServices;
        private readonly IErpService _erpServices;
        private readonly ILogLoanInfoAIService _logLoanInfoAIService;
        private readonly ILogCallApiService _logCallApiService;
        private readonly IFCMService _fCMService;
        private readonly ILogSendSmsService _logSendSmsService;
        private readonly ILogDistributionUserService _logDistributionUserService;
        private readonly ILogLoanInfoAIService _logLoaninfoAiServices;
        private readonly IServiceAI _serviceAI;
        private readonly ILMSService _lMSService;
        private readonly IRuleCheckLoanService _ruleCheckLoanService;
        private readonly ILoanbriefV2Service _loanBriefV2Services;

        private string environmentName = "";
        private int indexRecievedCar = 0;
        private List<UserShopItem> arrUserShopCar = new List<UserShopItem> {
            new UserShopItem() { UserId = (int)EnumUser.drs_anhnp, HubId = (int)EnumShop.HN_HUB004 } ,
             new UserShopItem() { UserId = (int)EnumUser.drs_thangdq , HubId = (int)EnumShop.HN_HUB005}
        };

        private List<UserShopItem> arrUserShopCarHCM = new List<UserShopItem> {
            new UserShopItem() { UserId = (int)EnumUser.hub_tuyenlm, HubId = (int)EnumShop.HCM_PDV2 }
        };
        private List<UserShopItem> arrShopLendingOnline = new List<UserShopItem> {
            new UserShopItem() { UserId = 0, HubId = (int)EnumShop.PDV_THANH_XUAN }
        };
        private readonly IProductServices _productService;

        Dictionary<int, Bank> dicBank = new Dictionary<int, Bank>();
        List<Bank> lstBank = new List<Bank>();

        public AutoDistributingHubJob(ILoanBriefService loanBriefServices, IShopService shopServices, IHubDistributingService hubDistributingService, ILogPushHubService logPushHubService,
            ISlackService slackService, ISendMessage sendNotification, IAuthenticationAI authenticationAI, ILMSService lmsService, IUserService userServices, IErpService erpServices,
            ILogLoanInfoAIService logLoanInfoAIService, ILogCallApiService logCallApiService, IFCMService fCMService, ILogSendSmsService logSendSmsService,
            ILogDistributionUserService logDistributionUserService, ILogLoanInfoAIService logLoaninfoAiServices, IServiceAI serviceAI, ILMSService lMSService,
            IRuleCheckLoanService ruleCheckLoanService, IProductServices productService, ILoanbriefV2Service loanBriefV2Services)
        {
            _loanBriefServices = loanBriefServices;
            _shopServices = shopServices;
            _hubDistributingService = hubDistributingService;
            _logPushHubService = logPushHubService;
            _slackService = slackService;
            _sendNotification = sendNotification;
            _authenticationAI = authenticationAI;
            _lmsService = lmsService;
            _userServices = userServices;
            _erpServices = erpServices;
            _logLoanInfoAIService = logLoanInfoAIService;
            _logCallApiService = logCallApiService;
            _fCMService = fCMService;
            _logSendSmsService = logSendSmsService;
            _logDistributionUserService = logDistributionUserService;
            _logLoaninfoAiServices = logLoaninfoAiServices;
            _serviceAI = serviceAI;
            _lMSService = lMSService;
            _ruleCheckLoanService = ruleCheckLoanService;
            _productService = productService;
            _loanBriefV2Services = loanBriefV2Services;
            environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            GetListBank();
            _thread = new Thread(() => DoWorkAsync(null));
            _thread.Start();
            return Task.CompletedTask;
        }
        private List<Bank> GetListBank()
        {
            try
            {
                lstBank = _loanBriefServices.GetBankAll();
            }
            catch (Exception ex)
            {

                lstBank = null;
            }
            return lstBank;
        }
        private async Task DoWorkAsync(object state)
        {
            while (true)
            {
                try
                {
                    var loanCredits = _loanBriefServices.HubDistributing();
                    if (loanCredits != null && loanCredits.Count > 0)
                    {
                        foreach (var item in loanCredits)
                        {
                            try
                            {
                                var configUserShop = new UserShopItem();
                                //kiểm tra xem có phải đơn DPD <= 8 không
                                if (item.UtmSource == "form_rmkt_dpd" || item.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                                {
                                    if (item.ReMarketingLoanBriefId > 0)
                                    {
                                        //kiểm tra đơn gốc xem là đơn đang vay hay là đơn đã tất toán
                                        var loanInfoOld = _loanBriefServices.GetAllById(item.ReMarketingLoanBriefId.Value);
                                        //nếu là đơn đã đang vay. Kiểm tra điều kiện topup
                                        if ((item.UtmSource == "form_rmkt_dpd" || item.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                                            && loanInfoOld.Status == (int)EnumLoanStatus.DISBURSED)
                                        {
                                            //Đối với gói vay xe máy
                                            if (item.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                                || item.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                                || item.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                            {
                                                //Kiểm tra xem đơn gốc đủ điều kiện topup không 
                                                decimal totalMoneyCurrent = 0;
                                                var checkTopup = await _loanBriefV2Services.CheckCanTopup(loanInfoOld.LoanBriefId);
                                                if (checkTopup.IsCanTopup)
                                                {
                                                    totalMoneyCurrent = checkTopup.CurrentDebt;
                                                    if (loanInfoOld.LoanBriefProperty == null
                                                                || loanInfoOld.LoanBriefProperty.ProductId == null)
                                                    {
                                                        BackLoanToTls("System: Không có thông tin tài sản", item.LoanBriefId);
                                                        continue;
                                                    }
                                                    var maxPrice = GetMaxPriceProductTopup((int)loanInfoOld.LoanBriefProperty.ProductId, (int)loanInfoOld.ProductId, item.LoanBriefId, (int)loanInfoOld.LoanBriefResident.ResidentType, (long)totalMoneyCurrent);
                                                    
                                                    if(maxPrice.PriceAi == 0)
                                                    {
                                                        BackLoanToTls("Không lấy được định giá xe từ AI", item.LoanBriefId);
                                                        continue;
                                                    }
                                                    var loanAmount = maxPrice.MaxPriceProduct;
                                                    loanAmount = Convert.ToInt64(LOS.Common.Utils.MathUtils.RoundMoney(loanAmount));
                                                    if (loanAmount < 3000000)
                                                    {
                                                        BackLoanToTls("Không đủ điều kiện Topup, số tiền < 3tr!", item.LoanBriefId);
                                                        continue;
                                                    }
                                                    if (item.UtmSource == "form_rmkt_dpd")
                                                    {
                                                        //cập nhập vào db
                                                        if (_loanBriefServices.UpdateLoanTopup(item.LoanBriefId, item.LoanAmountExpertiseAi.Value, item.LoanAmountExpertiseLast.Value,
                                                             item.LoanAmount.Value, (int)EnumTypeRemarketing.IsTopUp))
                                                        {
                                                            item.LoanAmountExpertiseAi = maxPrice.PriceAi;
                                                            item.LoanAmountExpertiseLast = maxPrice.MaxPriceProduct;
                                                            item.LoanAmount = loanAmount;
                                                            //Call api chuyển luồng
                                                            var result = _lmsService.ChangePipeline(new ChangePipelineReq.Input
                                                            {
                                                                loanBriefId = item.LoanBriefId,
                                                                productId = item.ProductId.Value,
                                                                status = item.Status.Value
                                                            });
                                                            if (result != null && result.Meta != null && result.Meta.ErrorCode == 200)
                                                            {
                                                                _loanBriefServices.AddNote(new LoanBriefNote
                                                                {
                                                                    LoanBriefId = item.LoanBriefId,
                                                                    Note = string.Format("System: HĐ-{0} đã được chuyển sang luồng Topup!", item.LoanBriefId),
                                                                    FullName = "Auto System",
                                                                    Status = 1,
                                                                    ActionComment = EnumActionComment.ChangePipe.GetHashCode(),
                                                                    CreatedTime = DateTime.Now,
                                                                    UserId = 1,
                                                                    ShopId = EnumShop.Tima.GetHashCode(),
                                                                    ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                                                });
                                                            }
                                                            item.TypeRemarketing = (int)EnumTypeRemarketing.IsTopUp;

                                                        }
                                                        else
                                                        {
                                                            BackLoanToTls("System: Không thay đổi được thông tin của đơn vay (số tiền (Topup))", item.LoanBriefId);
                                                            continue;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    BackLoanToTls("System: Đơn vay không đủ điều kiện vay Topup", item.LoanBriefId);
                                                    continue;
                                                }
                                            }
                                        }
                                        else if (loanInfoOld.Status == (int)EnumLoanStatus.FINISH && item.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                                        {
                                            //Đối với gói vay xe máy
                                            if (item.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                                || item.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                                || item.ProductId == (int)EnumProductCredit.MotorCreditType_KGT
                                                || item.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                                            {
                                                //Bỏ nhãn topup
                                                if (_loanBriefServices.RemoveTopup(item.LoanBriefId))
                                                {
                                                    _loanBriefServices.AddNote(new LoanBriefNote
                                                    {
                                                        LoanBriefId = item.LoanBriefId,
                                                        Note = string.Format("System: HĐ-{0} đã được bỏ nhãn Topup do đơn gốc đã Tất toán", item.LoanBriefId),
                                                        FullName = "Auto System",
                                                        Status = 1,
                                                        ActionComment = EnumActionComment.ChangePipe.GetHashCode(),
                                                        CreatedTime = DateTime.Now,
                                                        UserId = 1,
                                                        ShopId = EnumShop.Tima.GetHashCode(),
                                                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                                    });
                                                    item.TypeRemarketing = null;
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }
                                        }

                                        if(item.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                                        {
                                            if (!CheckBankAI(item))
                                                continue;
                                        }

                                    }
                                    else continue;
                                }
                                if (item.TypeRemarketing != (int)EnumTypeRemarketing.DebtRevolvingLoan 
                                    && item.TypeRemarketing != (int)EnumTypeRemarketing.IsTopUp)
                                {
                                    if (!item.ProvinceId.HasValue || !item.ProductId.HasValue) continue;
                                    //Nếu là đơn chờ cht chia đơn và có cvkd thì đẩy đơn lên bước tiếp theo
                                    if (item.Status == (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING
                                                   && item.ProductId == (int)EnumProductCredit.OtoCreditType_CC && item.HubEmployeeId > 0)
                                    {
                                        //đẩy đơn lên bước tiếp theo
                                        _loanBriefServices.PushToPipeline(item.LoanBriefId, EnumActionPush.Push.GetHashCode());
                                        continue;
                                    }

                                    //Nếu có passport
                                    //Nếu đơn từ telesale đẩy lên
                                    if (_baseConfig["AppSettings:FLAG_CHECK_EKYC"] != null && _baseConfig["AppSettings:FLAG_CHECK_EKYC"].Equals("1") && item.BoundTelesaleId > 0)
                                    {
                                        bool isBack = false;
                                        // check ekyc trước khi chưa cho hub
                                        CheckEkyc(item, ref isBack);
                                        if (isBack)
                                            continue;
                                    }

                                    //Xử lý check tái vay
                                    var isReborrow = false;
                                    long maxCountDayLate = 0;
                                    if (item.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                        || item.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                        || item.ProductId == (int)EnumProductCredit.MotorCreditType_KGT
                                        || item.ProductId == (int)EnumProductCredit.LoanFastMoto_CC
                                        || item.ProductId == (int)EnumProductCredit.LoanFastMoto_KCC)
                                    {
                                        var nationalCard = item.NationalCard;
                                        if (!string.IsNullOrEmpty(item.NationCardPlace))
                                            nationalCard = $"{item.NationalCard},{item.NationCardPlace}";
                                        if (!item.FullName.IsNormalized())
                                            item.FullName = item.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                                        isReborrow = CheckReBorrow(nationalCard, item.FullName, item.LoanBriefId, item.ProductId.Value, item.Status.Value, ref maxCountDayLate);
                                    }

                                    var ruleCheckLoan = _ruleCheckLoanService.GetByLoanbriefId(item.LoanBriefId);

                                    //chỉ check momo đối với đơn tái vay
                                    if (!isReborrow)
                                    {
                                        var isCheckMomo = true;
                                        if (ruleCheckLoan != null)
                                            isCheckMomo = ruleCheckLoan.IsCheckMomo.GetValueOrDefault(false);
                                        //Check Loan Momo
                                        if (isCheckMomo)
                                        {
                                            if (!CheckLoanInfoMomo(item))
                                                continue;
                                        }
                                    }

                                    //Kiểm tra số ngày trả muộn trong các kỳ thanh toán
                                    //Nếu số ngày trả muộn > cấu hình => trả lại tls 
                                    if (_baseConfig["AppSettings:MAX_COUNT_DAY_LATE"] != null && int.Parse(_baseConfig["AppSettings:MAX_COUNT_DAY_LATE"]) > 0
                                        && maxCountDayLate > 0 && maxCountDayLate > int.Parse(_baseConfig["AppSettings:MAX_COUNT_DAY_LATE"]))
                                    {
                                        bool isCheckDayLate = true;
                                        if (ruleCheckLoan != null)
                                            isCheckDayLate = ruleCheckLoan.IsCheckPayment.GetValueOrDefault(false);

                                        if (isCheckDayLate)
                                        {
                                            // đơn vay ko đủ thông tin check ekyc > trả lại    
                                            BackLoanToTls(string.Format("System: Đơn vay bị trả lại do KH có kỳ trả chậm {0} ngày", maxCountDayLate), item.LoanBriefId);
                                            continue;
                                        }

                                    }

                                    //Nếu là gói xe máy => gửi request kiểm tra thông tin xe từ số vin
                                    if (item.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                        || item.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                        || item.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                    {
                                        //Kiểm tra xem có số vin k
                                        var vinNumber = _loanBriefServices.GetVinNumber(item.LoanBriefId);
                                        if (!string.IsNullOrEmpty(vinNumber))
                                        {
                                            bool getVinInfo = true;
                                            //Kiểm tra xem số vin đó đã có kết quả chưa. Nếu chưa có thì mới get lại
                                            var logData = _logLoanInfoAIService.GetLastRequest(item.LoanBriefId, (int)ServiceTypeAI.ParseVinNumber);
                                            if (logData != null && logData.Status == 1
                                                && logData.ResultFinal == "SUCCESS"
                                                && !string.IsNullOrEmpty(logData.ResponseRequest)
                                                && String.Compare(logData.Request, vinNumber, true) == 0)
                                                getVinInfo = false;

                                            if (getVinInfo)
                                                _logLoaninfoAiServices.AddLog(new LogLoanInfoAi()
                                                {
                                                    LoanbriefId = item.LoanBriefId,
                                                    ServiceType = (int)ServiceTypeAI.ParseVinNumber,
                                                    IsExcuted = EnumLogLoanInfoAiExcuted.WaitRequest.GetHashCode(),
                                                    CreatedAt = DateTime.Now
                                                });
                                        }

                                    }

                                    if (item.HubId == 3703 || item.HubId == null) item.HubId = 0;

                                    //Nếu có hubId
                                    if (item.HubId > 0)
                                    {
                                        if (item.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                            || item.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                            || item.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                        {
                                            //nếu đơn do telesale tạo
                                            if (item.CreateBy > 0)
                                            {
                                                //Nếu là hub tạo thì để nguyên ở hub đó
                                                //Không phải hub tạo => kiểm tra có phải telesale k?
                                                if (!_userServices.IsUserOfHub(item.CreateBy.Value))
                                                {
                                                    //Nếu là đơn do telesale tạo => chia theo cấu hình khu vực
                                                    //xỷ lý TH tls cố tình đổi địa chỉ để chia về hub -> sau đó thì đổi lại
                                                    var userTelesale = _userServices.GetTelesale(item.CreateBy.Value);
                                                    if (userTelesale != null && userTelesale.UserId > 0)
                                                        item.HubId = 0;
                                                }
                                            }
                                            else
                                            {
                                                //Nếu không phải đơn tự tạo và không phải đơn aff
                                                //chia hub theo khu vực
                                                if (item.TypeRemarketing != (int)EnumTypeRemarketing.IsAff)
                                                    item.HubId = 0;
                                            }
                                        }

                                    }
                                    else
                                    {
                                        //Nếu là đơn oto
                                        //Nếu là đơn do mkt về hoặc đơn k phải do hub tạo
                                        //chia cho 2 bạn cvkd setup trước
                                        if (item.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                                        {
                                            if (item.CreateBy.GetValueOrDefault(0) == 0 || (item.CreateBy > 0 && !_userServices.IsUserOfHub(item.CreateBy.Value)))
                                            {
                                                if (item.ProvinceId == (int)EnumProvince.HaNoi)
                                                {
                                                    configUserShop = arrUserShopCar[indexRecievedCar];
                                                    indexRecievedCar++;
                                                    if (indexRecievedCar >= 2)
                                                        indexRecievedCar = 0;
                                                }
                                                else
                                                {
                                                    configUserShop = arrUserShopCarHCM[0];
                                                }
                                            }
                                        }

                                    }


                                    if (item.PlatformType == (int)EnumPlatformType.LendingOnlineApp
                                        || item.PlatformType == (int)EnumPlatformType.LendingOnlineWeb)
                                    {
                                        if (item.HubId.GetValueOrDefault(0) == 0)
                                            configUserShop = arrShopLendingOnline[0];
                                    }
                                }

                                if (item.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                                {
                                    if(item.TeamTelesalesId == (int)EnumTeamTelesales.HN1 || item.TeamTelesalesId == (int)EnumTeamTelesales.Xsell1)
                                        configUserShop.HubId = EnumShop.PDV_DE1.GetHashCode();
                                    else if (item.TeamTelesalesId == (int)EnumTeamTelesales.HN2 || item.TeamTelesalesId == (int)EnumTeamTelesales.Xsell2)
                                        configUserShop.HubId = EnumShop.PDV_DE2.GetHashCode();
                                    else if (item.TeamTelesalesId == (int)EnumTeamTelesales.HN3 || item.TeamTelesalesId == (int)EnumTeamTelesales.Xsell3)
                                        configUserShop.HubId = EnumShop.PDV_DE3.GetHashCode();
                                    else
                                        configUserShop.HubId = EnumShop.PDV_DE1.GetHashCode();
                                }
                                  
                                HubDistribution hubDistribution = _hubDistributingService.GetHubDistribution(item.ProvinceId.Value, item.ProductId.Value);
                                if (hubDistribution != null)
                                {
                                    //Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToHub: Start HĐ-{0}", item.LoanBriefId));
                                    Console.WriteLine(string.Format("PushToHub: Xu ly don HĐ-{0} {1}", item.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                    // calculate rate								
                                    // Reset theo từng ngày
                                    var applyTime = new DateTimeOffset(DateTimeOffset.Now.Year, DateTimeOffset.Now.Month, DateTimeOffset.Now.Day, 0, 0, 0, TimeSpan.FromHours(7));
                                    hubDistribution.ApplyTime = applyTime;

                                    //Mặc định chia theo khu vực
                                    // chia theo khu vực
                                    PushToHub(item, hubDistribution, configUserShop != null && configUserShop.HubId > 0 ? configUserShop : null);

                                    //Chia theo hub vật lý và hub dịch vụ
                                    //Loại hub nào nhận ít đơn hơn thì chia cho hub đó
                                    //Nếu k chia được cho hub vật lý(k hỗ trợ kv) => chia cho hub dịch vụ và ngược lại
                                    //Tuple<int, int> result = _hubDistributingService.GetLoanCountByHub(item.ProvinceId.Value, hubDistribution.ApplyTime.Value);
                                    //if (hubDistribution.ServiceHub > 0 || hubDistribution.PhysicalHub > 0)
                                    //{
                                    //    bool IsServiceDistribute = calculateHub(result, hubDistribution);
                                    //    if (!IsServiceDistribute)
                                    //    {
                                    //        // chia có hub vật lý
                                    //        PushToHub(item, false, hubDistribution, false);
                                    //    }
                                    //    else
                                    //    {
                                    //        PushToHub(item, true, hubDistribution, false);
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    // chia theo khu vực
                                    //    PushToHub(item, hubDistribution);
                                    //}
                                }
                                else
                                {
                                    Log.Error("LOS.ThirdPartyProccesor Hub Distribution null");
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor PushLoanCreditToHub exception");
                }
                //Thread 1 minute
                Thread.Sleep(3000);
            }
        }

        private void CheckEkyc(LoanBrief item, ref bool isBack)
        {
            try
            {
                bool flag = true;
                var ekycModel = _hubDistributingService.PrepareEkycModel(item, ref flag);
                if (flag && ekycModel != null)
                {
                    var logData = _logLoanInfoAIService.GetLastRequest(item.LoanBriefId, (int)ServiceTypeAI.Ekyc);
                    if (logData != null && !string.IsNullOrEmpty(logData.Request))
                    {
                        if (String.Compare(logData.Request.ToString(), JsonConvert.SerializeObject(ekycModel).ToString(), true) == 0)
                            flag = false;
                    }
                }
                if (flag)
                {
                    if (!GetInfoCheckEkyc(_authenticationAI.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH), ekycModel, item.LoanBriefId, 0))
                    {
                        // đơn vay ko đủ thông tin check ekyc > trả lại                                        
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = item.LoanBriefId,
                            Note = string.Format("System: Đơn vay bị trả lại do check Ekyc thất bại"),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        _loanBriefServices.AddNote(note);
                        // trả đơn lại cho telesale cập nhật
                        _loanBriefServices.PushToPipeline(item.LoanBriefId, EnumActionPush.Back.GetHashCode());

                        Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToHub: Back LoanBrief HĐ-{0}", item.LoanBriefId));
                    }
                }
                else
                {
                    //nếu k có passport thì kiểm báo => cần up
                    //Nếu có passport => bỏ qua
                    if (string.IsNullOrEmpty(item.Passport))
                    {
                        // đơn vay ko đủ thông tin check ekyc > trả lại                                    
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = item.LoanBriefId,
                            Note = string.Format("System: Đơn vay bị trả lại do không đủ thông tin để check Ekyc(CMND mặt trước, CMND mặt sau, Ngày sinh)"),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        _loanBriefServices.AddNote(note);
                        // trả đơn lại cho telesale cập nhật
                        _loanBriefServices.PushToPipeline(item.LoanBriefId, EnumActionPush.Back.GetHashCode());
                        isBack = true;
                        Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToHub: Back LoanBrief HĐ-{0}", item.LoanBriefId));
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        private bool GetInfoCheckEkyc(string token, EkycInput input, int loanBriefId, int retry = 0)
        {
            try
            {
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.GET_INFO_CHECK_EKYC;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var body = JsonConvert.SerializeObject(input);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    ServiceType = ServiceTypeAI.Ekyc.GetHashCode(),
                    LoanbriefId = loanBriefId,
                    Request = body,
                    Token = token,
                    Url = url,
                    Retry = retry,
                    CreatedAt = DateTime.Now
                };
                logLoanInfoAi = _hubDistributingService.InsertApiLog(logLoanInfoAi);
                if (logLoanInfoAi.Id > 0)
                {
                    IRestResponse response = client.Execute(request);
                    var jsonResponse = response.Content;
                    logLoanInfoAi.Status = 1;//gọi thành công
                    logLoanInfoAi.ResponseRequest = jsonResponse;
                    logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode();
                    _hubDistributingService.UpdateApiLog(logLoanInfoAi);
                    EkycOutput result = Newtonsoft.Json.JsonConvert.DeserializeObject<EkycOutput>(jsonResponse);
                    if (result.StatusCode == 200)
                    {
                        //Lưu vào bảng ResultEkyc
                        var objEkyc = new DAL.EntityFramework.ResultEkyc()
                        {
                            LoanbriefId = loanBriefId,
                            CreatedAt = DateTime.Now,
                            AddressValue = result.NationalId.Address.Value,
                            AddressCheck = result.NationalId.Address.Check,
                            IdValue = result.NationalId.Id.Value,
                            IdCheck = result.NationalId.Id.Check,
                            FullNameValue = result.NationalId.Fullname.Value,
                            FullNameCheck = result.NationalId.Fullname.Check,
                            BirthdayValue = result.NationalId.Birthday.Value,
                            BirthdayCheck = result.NationalId.Birthday.Check,
                            ExpiryValue = result.NationalId.Expiry.Value,
                            ExpiryCheck = result.NationalId.Expiry.Check,
                            GenderValue = result.NationalId.Gender.Value,
                            GenderCheck = result.NationalId.Gender.Check,
                            EthnicityValue = result.NationalId.Ethnicity.Value,
                            EthnicityCheck = result.NationalId.Ethnicity.Check,
                            IssueByValue = result.NationalId.IssueBy.Value,
                            IssueByCheck = result.NationalId.IssueBy.Check,
                            IssueDateValue = result.NationalId.IssueDate.Value,
                            IssueDateCheck = result.NationalId.IssueDate.Check,
                            ReligionValue = result.NationalId.Religion.Value,
                            ReligionCheck = result.NationalId.Religion.Check,
                            FaceCompareCode = result.FraudCheck.FaceCompare.Code,
                            FaceCompareMessage = result.FraudCheck.FaceCompare.Message,
                            FaceQueryCode = result.FraudCheck.FaceQuery.Code,
                            FaceQueryMessage = result.FraudCheck.FaceQuery.Message
                        };
                        _hubDistributingService.InsertEkycResult(objEkyc);
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }


        private void AddLogPush(LoanBrief entity)
        {
            try
            {
                var LogPushItem = new LogPushToHub()
                {
                    LoanbriefId = entity.LoanBriefId,
                    TelesaleId = entity.BoundTelesaleId,
                    CityId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    ProductId = entity.ProductId,
                    HubId = entity.HubId
                };
                _logPushHubService.Insert(LogPushItem);
            }
            catch
            {

            }
        }

        private void PushToHub(LoanBrief entity, bool IsServiceHub, HubDistribution hubDistribution, bool overlap)
        {
            try
            {
                if (entity.HubId > 0)
                {
                    //Lưu log push đơn 
                    AddLogPush(entity);
                    //Gửi notify qua slack
                    //var isPushSlack = _baseConfig["AppSettings:IsPushSlack"].ToString();
                    //if(isPushSlack == "1")
                    //{
                    if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_CC || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                        || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                    {
                        hubEmployeeDistribute(entity);
                    }

                    // đơn tạo bởi hub rồi thì ko tính
                    _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Push.GetHashCode());

                    var shop = _shopServices.GetById(entity.HubId.Value);
                    if (shop != null && !string.IsNullOrEmpty(shop.WebhookSlack))
                    {
                        var urlApi = shop.WebhookSlack;
                        if (!string.IsNullOrEmpty(urlApi))
                            _slackService.SendNotifyOfStack("Hệ thống", "Blue",
                            "HĐ-" + entity.LoanBriefId + " mới đẩy về"
                            , string.Format("HĐ-{0} {1} cần được xử lý", entity.LoanBriefId, entity.FullName)
                            , urlApi, entity.LoanBriefId, "hub");
                    }

                    var mess = string.Format("Có hợp đồng HĐ-{0} mới về cần được xử lý", entity.LoanBriefId);
                    SendNotification(entity.HubId.ToString(), mess, entity.LoanBriefId);
                    //}
                }
                else
                {
                    if (IsServiceHub)
                    {
                        var objHub = entity.WardId > 0 ? _shopServices.GetHubByWardDV(entity.WardId.Value, entity.ProductId.Value) : null;
                        if (objHub == null || objHub.ShopId == 0)
                        {
                            if (entity.DistrictId > 0)
                                objHub = _shopServices.GetHubByDistrictDV(entity.DistrictId.Value, entity.ProductId.Value);
                            if (objHub == null || objHub.ShopId == 0)
                            {
                                // nếu không chia đc cho dịch vụ thì chia cho vật lý
                                if (!overlap)
                                {
                                    PushToHub(entity, false, hubDistribution, true);
                                }
                                else
                                {
                                    //lưu comment trả lại đơn
                                    var note = new LoanBriefNote
                                    {
                                        LoanBriefId = entity.LoanBriefId,
                                        Note = string.Format("System: Đơn không được chia do chưa được cấu hình"),
                                        FullName = "Auto System",
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = 1,
                                        ShopId = EnumShop.Tima.GetHashCode(),
                                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                    };
                                    _loanBriefServices.AddNote(note);
                                    // trả đơn lại cho telesale cập nhật
                                    _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Back.GetHashCode());

                                    Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToHub: Back LoanBrief HĐ-{0}", entity.LoanBriefId));
                                }
                            }
                        }

                        if (objHub != null && objHub.ShopId > 0)
                        {
                            if (_hubDistributingService.UpdateHub(entity.LoanBriefId, objHub.ShopId, 2))
                            {
                                //Lưu log push đơn 
                                entity.HubId = objHub.ShopId;
                                AddLogPush(entity);

                                //lưu lại comment
                                var note = new LoanBriefNote
                                {
                                    LoanBriefId = entity.LoanBriefId,
                                    Note = string.Format("System: Hệ thống đã đẩy đơn về cho {0}", objHub.Name),
                                    FullName = "Auto System",
                                    Status = 1,
                                    ActionComment = EnumActionComment.DistributingLoanToHub.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = 1,
                                    ShopId = EnumShop.Tima.GetHashCode(),
                                    ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                };
                                _loanBriefServices.AddNote(note);
                                //tăng số đơn nhận của hub lên
                                _hubDistributingService.UpdateNumberReceived(objHub.ShopId, hubDistribution.ApplyTime.Value);
                                //_shopServices.UpdateNumberReceived(objHub.ShopId);
                                Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToHub: Done HĐ-{0}", entity.LoanBriefId));
                                if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                    || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                    || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT
                                    || entity.ProductId == (int)EnumProductCredit.LoanFastMoto_CC
                                    || entity.ProductId == (int)EnumProductCredit.LoanFastMoto_KCC)
                                {
                                    hubEmployeeDistribute(entity);
                                }

                                //đẩy đơn lên luồng xử lý
                                _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Push.GetHashCode());

                                //Gửi notify qua slack
                                //var isPushSlack = _baseConfig["AppSettings:IsPushSlack"].ToString();
                                //if (isPushSlack == "1")
                                //{
                                if (!string.IsNullOrEmpty(objHub.WebhookSlack))
                                {
                                    var urlApi = objHub.WebhookSlack;
                                    if (!string.IsNullOrEmpty(urlApi))
                                        _slackService.SendNotifyOfStack("Hệ thống", "Blue",
                                                    "HĐ-" + entity.LoanBriefId + " mới đẩy về"
                                                    , string.Format("HĐ-{0} {1} cần được xử lý", entity.LoanBriefId, entity.FullName)
                                                    , urlApi, entity.LoanBriefId, "hub");
                                }

                                //}

                                var mess = string.Format("Có hợp đồng HĐ-{0} mới về cần được xử lý", entity.LoanBriefId);
                                SendNotification(entity.HubId.ToString(), mess, entity.LoanBriefId);
                            }
                        }
                    }
                    else
                    {
                        //var objHub = entity.WardId > 0 ? _shopServices.GetHubByWard(entity.WardId.Value) : null;						
                        var objHub = entity.WardId > 0 ? _shopServices.GetHubByWard(entity.WardId.Value, entity.ProductId.Value) : null;
                        if (objHub == null || objHub.ShopId == 0)
                        {
                            if (entity.DistrictId > 0)
                                objHub = _shopServices.GetHubByDistrict(entity.DistrictId.Value, entity.ProductId.Value);
                            if (objHub == null || objHub.ShopId == 0)
                            {
                                // nếu không chia đc cho vật lý thì chia cho dịch vụ
                                if (!overlap)
                                {
                                    PushToHub(entity, true, hubDistribution, true);
                                }
                                else
                                {
                                    //lưu comment trả lại đơn
                                    var note = new LoanBriefNote
                                    {
                                        LoanBriefId = entity.LoanBriefId,
                                        Note = string.Format("System: Đơn không được chia do chưa được cấu hình"),
                                        FullName = "Auto System",
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = 1,
                                        ShopId = EnumShop.Tima.GetHashCode(),
                                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                    };
                                    _loanBriefServices.AddNote(note);
                                    // trả đơn lại cho telesale cập nhật
                                    _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Back.GetHashCode());
                                    Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToHub: Back LoanBrief HĐ-{0}", entity.LoanBriefId));
                                }
                            }
                        }

                        if (objHub != null && objHub.ShopId > 0)
                        {
                            if (_hubDistributingService.UpdateHub(entity.LoanBriefId, objHub.ShopId, 1))
                            {
                                //Lưu log push đơn 
                                entity.HubId = objHub.ShopId;
                                AddLogPush(entity);

                                //lưu lại comment
                                var note = new LoanBriefNote
                                {
                                    LoanBriefId = entity.LoanBriefId,
                                    Note = string.Format("System: Hệ thống đã đẩy đơn về cho {0}", objHub.Name),
                                    FullName = "Auto System",
                                    Status = 1,
                                    ActionComment = EnumActionComment.DistributingLoanToHub.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = 1,
                                    ShopId = EnumShop.Tima.GetHashCode(),
                                    ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                };
                                _loanBriefServices.AddNote(note);
                                //tăng số đơn nhận của hub lên
                                _hubDistributingService.UpdateNumberReceived(objHub.ShopId, hubDistribution.ApplyTime.Value);
                                //_shopServices.UpdateNumberReceived(objHub.ShopId);
                                //Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToHub: Done HĐ-{0}", entity.LoanBriefId));
                                if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                    || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                    || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT
                                    || entity.ProductId == (int)EnumProductCredit.LoanFastMoto_CC
                                    || entity.ProductId == (int)EnumProductCredit.LoanFastMoto_KCC)
                                {
                                    hubEmployeeDistribute(entity);
                                }

                                //đẩy đơn lên luồng xử lý
                                _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Push.GetHashCode());

                                //Gửi notify qua slack
                                //var isPushSlack = _baseConfig["AppSettings:IsPushSlack"].ToString();
                                //if(isPushSlack == "1")
                                //{
                                if (!string.IsNullOrEmpty(objHub.WebhookSlack))
                                {
                                    var urlApi = objHub.WebhookSlack;
                                    if (!string.IsNullOrEmpty(urlApi))
                                        _slackService.SendNotifyOfStack("Hệ thống", "Blue",
                                                "HĐ-" + entity.LoanBriefId + " mới đẩy về"
                                                , string.Format("HĐ-{0} {1} cần được xử lý", entity.LoanBriefId, entity.FullName)
                                                , urlApi, entity.LoanBriefId, "hub");
                                }
                                //}

                                var mess = string.Format("Có hợp đồng HĐ-{0} mới về cần được xử lý", entity.LoanBriefId);
                                SendNotification(entity.HubId.ToString(), mess, entity.LoanBriefId);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor PushToHub exception");
            }
        }

        private void PushToHub(LoanBrief entity, HubDistribution hubDistribution, UserShopItem configUserShopCar = null)
        {
            try
            {
                if (entity.HubId > 0 && entity.TypeRemarketing != (int)EnumTypeRemarketing.DebtRevolvingLoan)
                {
                    //Lưu log push đơn 
                    AddLogPush(entity);
                    //Nếu đơn cũ là không thẩm định HO => mới là thẩm định HO 
                    if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                        || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                         || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                    {
                        hubEmployeeDistribute(entity);
                    }
                    else
                    {
                        //Nếu là đơn oto
                        if (entity.ProductId == (int)EnumProductCredit.OtoCreditType_CC && configUserShopCar != null && configUserShopCar.UserId > 0)
                        {
                            hubEmployeeDistribute(entity, configUserShopCar.UserId);
                        }
                    }
                    // đơn tạo bởi hub rồi thì ko tính
                    _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Push.GetHashCode());

                    var shop = _shopServices.GetById(entity.HubId.Value);
                    if (shop != null && !string.IsNullOrEmpty(shop.WebhookSlack))
                    {
                        var urlApi = shop.WebhookSlack;
                        if (!string.IsNullOrEmpty(urlApi))
                            _slackService.SendNotifyOfStack("Hệ thống", "Blue",
                                    "HĐ-" + entity.LoanBriefId + " mới đẩy về"
                                    , string.Format("HĐ-{0} {1} cần được xử lý", entity.LoanBriefId, entity.FullName)
                                    , urlApi, entity.LoanBriefId, "hub");
                    }

                    var mess = string.Format("Có hợp đồng HĐ-{0} mới về cần được xử lý", entity.LoanBriefId);
                    SendNotification(entity.HubId.ToString(), mess, entity.LoanBriefId);
                }
                else
                {
                    var objHub = new Shop();
                    //Nếu có hub truyền từ ngoài => gán luôn shop đó
                    if (configUserShopCar != null && configUserShopCar.HubId > 0)
                        objHub = _shopServices.GetById(configUserShopCar.HubId);
                    //lấy hub theo khu vực đơn vay
                    if (objHub == null || objHub.ShopId == 0)
                        objHub = entity.WardId > 0 ? _shopServices.GetHubByWardAll(entity.WardId.Value, entity.ProductId.Value) : null;

                    if (objHub == null || objHub.ShopId == 0)
                    {
                        //TH không có phường xã chỉ chọn quận huyện
                        if (entity.DistrictId > 0 && entity.WardId.GetValueOrDefault(0) == 0)
                            objHub = _shopServices.GetHubByDistrictAll(entity.DistrictId.Value, entity.ProductId.Value);
                        //Nếu là đơn tái cấu trúc nợ mà không có khu vực nhận
                        //chia mặc định cho hub1
                        if (objHub == null || objHub.ShopId == 0)
                        {
                            if (entity.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan)
                            {
                                var hubId = (int)EnumShop.HCM_HUB001;
                                if (entity.ProvinceId != (int)EnumProvince.HCM)
                                    hubId = (int)EnumShop.HN_HUB001;
                                objHub = _shopServices.GetById(hubId);
                            }
                        }
                        if (objHub == null || objHub.ShopId == 0)
                        {
                            //lưu comment trả lại đơn
                            var note = new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = string.Format("System: Chưa có HUB nào nhận đơn ở khu vực của KH"),
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            };
                            _loanBriefServices.AddNote(note);
                            // trả đơn lại cho telesale cập nhật
                            _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Back.GetHashCode());

                            Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToHub: Back LoanBrief HĐ-{0}", entity.LoanBriefId));
                        }
                    }

                    if (objHub != null && objHub.ShopId > 0)
                    {
                        if (_hubDistributingService.UpdateHub(entity.LoanBriefId, objHub.ShopId, 2))
                        {
                            //Lưu log push đơn 
                            entity.HubId = objHub.ShopId;
                            AddLogPush(entity);

                            //lưu lại comment
                            var note = new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = string.Format("System: Hệ thống đã đẩy đơn về cho {0}", objHub.Name),
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.DistributingLoanToHub.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            };
                            _loanBriefServices.AddNote(note);
                            //tăng số đơn nhận của hub lên
                            _hubDistributingService.UpdateNumberReceived(objHub.ShopId, hubDistribution.ApplyTime.Value);
                            if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                 || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT
                                || entity.ProductId == (int)EnumProductCredit.LoanFastMoto_CC
                                || entity.ProductId == (int)EnumProductCredit.LoanFastMoto_KCC)
                            {
                                hubEmployeeDistribute(entity);
                            }
                            else
                            {
                                //Nếu là đơn oto
                                if (entity.ProductId == (int)EnumProductCredit.OtoCreditType_CC && configUserShopCar != null && configUserShopCar.UserId > 0)
                                {
                                    hubEmployeeDistribute(entity, configUserShopCar.UserId);
                                }
                            }

                            //đẩy đơn lên luồng xử lý
                            _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Push.GetHashCode());

                            if (!string.IsNullOrEmpty(objHub.WebhookSlack))
                            {
                                var urlApi = objHub.WebhookSlack;
                                if (!string.IsNullOrEmpty(urlApi))
                                    _slackService.SendNotifyOfStack("Hệ thống", "Blue",
                                         "HĐ-" + entity.LoanBriefId + " mới đẩy về"
                                         , string.Format("HĐ-{0} {1} cần được xử lý", entity.LoanBriefId, entity.FullName)
                                         , urlApi, entity.LoanBriefId, "hub");
                            }
                            Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToHub: Done HĐ-{0}", entity.LoanBriefId));
                            var mess = string.Format("Có hợp đồng HĐ-{0} mới về cần được xử lý", entity.LoanBriefId);
                            SendNotification(entity.HubId.ToString(), mess, entity.LoanBriefId);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor PushToHub exception");
            }
        }

        private bool calculateHub(Tuple<int, int> tuple, HubDistribution hubDistribution)
        {
            if (hubDistribution.ServiceHub > 0)
            {
                float ratioDistrubuting = (float)hubDistribution.PhysicalHub.Value / hubDistribution.ServiceHub.Value;
                if (tuple.Item2 > 0)
                {
                    float ratioRealDistributing = (float)tuple.Item1 / tuple.Item2;
                    if (ratioRealDistributing > ratioDistrubuting)
                    {
                        // hub vật lý đang nhiều hơn chia cho hub dịch vụ
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        private bool hubEmployeeDistribute(LoanBrief entity, int hubEmployeeId = 0)
        {
            if (entity.HubId > 0)
            {
                //Kiểm tra xem chia cho tdtd
                //if (distributingField(entity))
                //{
                //    var provinceId = EnumProvince.HaNoi.GetHashCode();
                //    if (entity.ProvinceId == EnumProvince.HCM.GetHashCode())
                //        provinceId = EnumProvince.HCM.GetHashCode();
                //    var userField = _userServices.GetFieldHo(provinceId);

                //    if (userField != null)
                //    {
                //        //Cập nhật hubEmployee
                //        //Câp nhật thẩm định nhà
                //        //Cập nhật thẩm định công ty
                //        var note = new LoanBriefNote
                //        {
                //            LoanBriefId = entity.LoanBriefId,
                //            Note = string.Format("System: Hệ thống đã đẩy đơn về cho TĐTĐ HO {0}", userField.FullName + "(" + userField.Username + ")"),
                //            FullName = "Auto System",
                //            Status = 1,
                //            ActionComment = EnumActionComment.DistributingLoanOfHub.GetHashCode(),
                //            CreatedTime = DateTime.Now,
                //            UserId = 1,
                //            ShopId = EnumShop.Tima.GetHashCode(),
                //            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                //        };
                //        _loanBriefServices.AddNote(note);
                //        // update loan brief
                //        _hubDistributingService.UpdateFieldHo(entity.LoanBriefId, userField.UserId);
                //        //insert log
                //        _hubDistributingService.AddLogDistributingHubEmployee(new LogDistributingHubEmployee()
                //        { LoanbriefId = entity.LoanBriefId, HubEmployeeId = userField.UserId, CreatedAt = DateTime.Now, HubId = entity.HubId });
                //    }
                //}
                //else
                //{
                //    //cập nhật field ho = null
                //    _hubDistributingService.RemoveFieldHo(entity.LoanBriefId);
                //}
                //kiểm tra user cũ có thuộc hub k
                if (entity.HubEmployeeId > 0)
                {
                    var allHub = _userServices.GetAllHub(entity.HubEmployeeId.Value);
                    if (allHub == null || allHub.Count == 0)
                        entity.HubEmployeeId = 0;
                    else if (allHub != null && allHub.Count > 0 && !allHub.Any(x => x == entity.HubId.Value))
                        entity.HubEmployeeId = 0;
                }

                //Nếu đã có HubEmployee => bỏ qua
                if (entity.HubEmployeeId == null || entity.HubEmployeeId == 0 || hubEmployeeId > 0)
                {
                    //Nếu môi trường dev thì k cần gọi sang erp
                    var listEmployeeId = new List<int>();
                    if (hubEmployeeId == 0 && environmentName != "Development")
                    {
                        var listInfoHubEmployee = _erpServices.GetInfoStaffHub(entity.HubId.Value, entity.LoanBriefId);
                        var comment = "Các nhân viên đã checkin ca làm việc: ";
                        if (listInfoHubEmployee != null && listInfoHubEmployee.Count > 0)
                        {
                            var listUserCheckIn = listInfoHubEmployee.Select(x => x.losId).ToList();
                            var listUserOfHub = _userServices.GetUserStaffHub(listUserCheckIn);
                            if (listUserOfHub != null && listUserOfHub.Count > 0)
                                listEmployeeId = listUserOfHub;
                            foreach (var item in listInfoHubEmployee)
                            {
                                if (!string.IsNullOrEmpty(item.fullname))
                                    comment += string.Format("<br /> + <b>{0}</b>", item.fullname);
                            }
                        }
                        else
                        {
                            comment = "Chưa có nhân viên nào checkin";
                        }
                        _loanBriefServices.AddNote(new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = comment,
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        });

                    }
                    var employee = new UserDetail();
                    if (hubEmployeeId > 0)
                    {
                        employee = _userServices.GetById(hubEmployeeId);
                    }
                    else
                    {
                        employee = _hubDistributingService.GetHubEmployeeLoanAsc(entity.HubId.Value, listEmployeeId != null && listEmployeeId.Count > 0 ? listEmployeeId : null);
                    }

                    if (employee != null && employee.UserId > 0)
                    {
                        var typeAppraiserName = "";
                        var typeAppraiser = ConvertAppraiserType(entity);
                        if (typeAppraiser > 0 && Enum.IsDefined(typeof(EnumAppraisal), typeAppraiser))
                        {
                            typeAppraiserName = ExtensionHelper.GetDescription((EnumAppraisal)typeAppraiser);
                        }
                        //lưu lại comment
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = string.Format("System: Hệ thống đã đẩy đơn về cho {0} ({1}) <br /> <b>{2}</b>", employee.FullName, employee.Username, typeAppraiserName),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.DistributingLoanOfHub.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        _loanBriefServices.AddNote(note);

                        //Kiểm tra xem trường HubEmployeeReceived có dữ liệu chưa
                        //nếu chưa có thì update ngày hiện tại
                        //có rồi thì bỏ qua

                        if (!entity.HubEmployeeReceived.HasValue)
                            _hubDistributingService.UpdateHubEmployeeReceived(entity.LoanBriefId);

                        // update nhân viên hub
                        _hubDistributingService.UpdateHubEmployee(entity.LoanBriefId, employee.UserId, typeAppraiser);
                        // increment
                        _hubDistributingService.UpdateNumberReceivedHubEmployee(entity.HubId.Value, employee.UserId);
                        //insert log
                        _hubDistributingService.AddLogDistributingHubEmployee(new LogDistributingHubEmployee()
                        { LoanbriefId = entity.LoanBriefId, HubEmployeeId = employee.UserId, CreatedAt = DateTime.Now, HubId = entity.HubId });

                        //Thêm log chia đơn cho cvkd
                        _logDistributionUserService.Insert(new LogDistributionUser()
                        {
                            UserId = employee.UserId,
                            LoanbriefId = entity.LoanBriefId,
                            CreatedAt = DateTime.Now,
                            HubId = entity.HubId,
                            TypeDistribution = (int)TypeDistributionLog.CVKD
                        });

                        // push notify
                        var mess = string.Format("Bạn có đơn HĐ-{0} cần xử lý, vui lòng kiểm tra.", entity.LoanBriefId);
                        _fCMService.PushNotification(Constants.TOKEN_FIREBASE_HUB, employee.Username, mess, (int)ActionPushNotification.PushAppCvkd, entity.LoanBriefId);

                        //Gửi sms cho KH
                        if (_baseConfig["AppSettings:FLAG_SEND_SMS_HUB"] != null && _baseConfig["AppSettings:FLAG_SEND_SMS_HUB"].Equals("1"))
                        {
                            if (entity.BoundTelesaleId > 0)
                                SendSmsToCustomer(entity.LoanBriefId, entity.Phone, employee.UserId);
                        }
                        return true;
                    }
                }
                else
                {
                    //Nếu là đơn hub tạo
                    //Thêm log chia đơn cho cvkd
                    if (entity.HubEmployeeId > 0 && entity.CreateBy == entity.HubEmployeeId)
                        _logDistributionUserService.Insert(new LogDistributionUser()
                        {
                            UserId = entity.HubEmployeeId,
                            LoanbriefId = entity.LoanBriefId,
                            CreatedAt = DateTime.Now,
                            HubId = entity.HubId,
                            TypeDistribution = (int)TypeDistributionLog.CVKD
                        });
                }

            }
            return false;
        }

        private bool SendSmsToCustomer(int loanbriefId, string phoneCustomer, int hubEmployeeId)
        {
            try
            {
                var logSendSms = _logSendSmsService.GetLogSendSms(loanbriefId, (int)EnumTypeSendSMS.SendSmsFormToHub);
                //chưa gửi sms
                if (logSendSms == null || logSendSms.Count == 0)
                {
                    //lấy thông tin sdt của cvkd và cht
                    var hubEmployeeInfo = _erpServices.GetHubEmployeeInfo(hubEmployeeId, loanbriefId);
                    if (hubEmployeeInfo != null)
                    {
                        if (!string.IsNullOrEmpty(hubEmployeeInfo.tel)
                            && hubEmployeeInfo.manager != null && !string.IsNullOrEmpty(hubEmployeeInfo.manager.tel))
                        {
                            var phoneCvkd = hubEmployeeInfo.tel;
                            var phoneCHT = hubEmployeeInfo.manager.tel;
                            var content = $"Tima: HS vay cua QK se duoc CVKD co SDT {phoneCvkd}/ {phoneCHT} LH de ky HD va giai ngan. QK vui long giu lien lac. Tran trong cam on QK!";
                            var result = _lmsService.SendSMS(phoneCustomer, content, loanbriefId, (int)EnumTypeSendSMS.SendSmsFormToHub);
                            if (result != null && result.Result == 1)
                            {
                                var note = new LoanBriefNote
                                {
                                    LoanBriefId = loanbriefId,
                                    Note = "Hệ thống gửi SMS thông báo cho khách hàng",
                                    FullName = "Auto System",
                                    Status = 1,
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = 1,
                                    ShopId = EnumShop.Tima.GetHashCode(),
                                    ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                };
                                _loanBriefServices.AddNote(note);
                                return true;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        private int ConvertAppraiserType(LoanBrief entity)
        {
            bool isThamDinhNha = true;
            bool isThamDinhCongTy = true;
            try
            {
                var loanbriefResident = _loanBriefServices.GetLoanBriefResident(entity.LoanBriefId);
                var TypeOfOwnerShip = loanbriefResident.ResidentType;
                //Nếu là nhà sở hữu
                //Miễn thẩm định nơi làm việc
                if (TypeOfOwnerShip == EnumTypeofownership.Sohuucanhan.GetHashCode() || TypeOfOwnerShip == EnumTypeofownership.Dongsohuu.GetHashCode()
                     || TypeOfOwnerShip == EnumTypeofownership.DungTenBoMe.GetHashCode() || TypeOfOwnerShip == EnumTypeofownership.DungTenVoChong.GetHashCode()
                     || TypeOfOwnerShip == EnumTypeofownership.NhaSoHuu.GetHashCode() || TypeOfOwnerShip == EnumTypeofownership.NhaSoHuuKhongTrungSHK.GetHashCode()
                     || TypeOfOwnerShip == EnumTypeofownership.NhaSoHuuTrungSHK.GetHashCode())
                {
                    #region Quy tắc thẩm định nơi ở

                    #endregion
                    //Nếu Location đạt => Miễn thẩm định nơi ở
                    //Không đạt => Thẩm định nơi ở
                    if (loanbriefResident != null && !string.IsNullOrEmpty(loanbriefResident.ResultLocation) && loanbriefResident.ResultLocation.Trim().ToUpper() == "YES")
                    {
                        //Miễn thẩm định nơi ở
                        isThamDinhNha = false;
                    }
                    isThamDinhCongTy = false;
                }
                //Nếu là nhà thuê
                else
                {
                    #region Quy tắc thẩm định nơi ở
                    //Nếu Location đạt => Miễn thẩm định nơi ở
                    //Nếu Location không đạt => thẩm định nơi ở
                    if (loanbriefResident != null && !string.IsNullOrEmpty(loanbriefResident.ResultLocation) && loanbriefResident.ResultLocation.Trim().ToUpper() == "YES")
                    {
                        //Cung cấp được chứng từ sổ tạm trú
                        //Số BHXH
                        //Sổ hộ khâu tại địa bàn tima hỗ trợ
                        // => Miễn thẩm định nơi ở
                        var loanbriefQuestion = _loanBriefServices.GetLoanBriefQuestionScript(entity.LoanBriefId);
                        if (loanbriefQuestion != null)
                        {
                            if (loanbriefQuestion.DocumentNoCoincideHouseHold > 0)
                            {
                                if (loanbriefQuestion.DocumentNoCoincideHouseHold == (int)EnumDocumentNoCoincideHouseHold.SoTamTruTren6thang
                                    || loanbriefQuestion.DocumentNoCoincideHouseHold == (int)EnumDocumentNoCoincideHouseHold.SoHKThuocKVHoTro
                                    || loanbriefQuestion.DocumentNoCoincideHouseHold == (int)EnumDocumentNoCoincideHouseHold.BHXH)
                                {
                                    //Miễn thẩm định nơi ở
                                    isThamDinhNha = false;
                                }
                            }

                        }

                    }
                    #endregion

                    #region Quy tắc thẩm định nơi làm việc
                    var loanbriefJob = _loanBriefServices.GetLoanbriefJob(entity.LoanBriefId);
                    if (loanbriefJob != null)
                    {
                        //Làm thuê lương chuyển khoản +
                        //Sao kê lương 

                        //=> miễn thẩm định nơi ở
                        if (loanbriefJob.JobId == (int)EnumJobTitle.LamHuongLuong)
                        {
                            if (loanbriefJob.ImcomeType == (int)EnumImcomeType.ChuyenKhoan)
                            {
                                //Nếu location đạt
                                if (loanbriefJob != null && !string.IsNullOrEmpty(loanbriefJob.ResultLocation) && loanbriefJob.ResultLocation.Trim().ToUpper() == "YES")
                                {
                                    var loanbriefQuestion = _loanBriefServices.GetLoanBriefQuestionScript(entity.LoanBriefId);
                                    if (loanbriefQuestion != null)
                                    {
                                        if (loanbriefQuestion.DocumentSalaryBankTransfer == (int)EnumDocumentSalaryBankTransfer.AnhSmsLuong
                                            || loanbriefQuestion.DocumentSalaryBankTransfer == (int)EnumDocumentSalaryBankTransfer.AnhBHYTBHXH)
                                            isThamDinhCongTy = false;
                                    }
                                }

                            }
                        }
                        //Làm tài xế công nghệ
                        else if (loanbriefJob.JobId == (int)EnumJobTitle.LamTaiXeCongNghe)
                        {
                            isThamDinhCongTy = false;
                        }
                    }
                    #endregion
                }

                if (isThamDinhCongTy && isThamDinhNha)
                    return EnumAppraisal.AppraisalHomeAndCompany.GetHashCode();
                else if (!isThamDinhCongTy && isThamDinhNha)
                    return EnumAppraisal.AppraisalHome.GetHashCode();
                else if (isThamDinhCongTy && !isThamDinhNha)
                    return EnumAppraisal.AppraisalCompany.GetHashCode();
                else if (!isThamDinhCongTy && !isThamDinhNha)
                    return EnumAppraisal.NoAppraisalHomeAndCompany.GetHashCode();
            }
            catch
            {
            }
            return EnumAppraisal.AppraisalHomeAndCompany.GetHashCode();
        }

        private void SendNotification(string user, string message, int loanbriefId)
        {
            try
            {
                var mess = new NotificationSystem();
                mess.UserName = user;
                mess.Message = message;
                mess.loanbriefId = loanbriefId;
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(mess);
                _sendNotification.Send(json, loanbriefId);
            }
            catch
            {
            }
        }

        private bool CheckReBorrow(string NumberCard, string CustomerName, int LoanbriefId, int ProductId, int Status, ref long maxCountDayLate)
        {
            try
            {
                var obj = new CheckReLoan.Input
                {
                    NumberCard = NumberCard,
                    CustomerName = CustomerName
                };
                var checkReloan = _lmsService.CheckReBorrow(obj, LoanbriefId);
                if (checkReloan != null)
                    maxCountDayLate = checkReloan.MaxCountDayLate;
                if (checkReloan != null && checkReloan.Result == 1 && checkReloan.IsAccept == 1)
                {
                    //update lại IsReborrow
                    var update = _loanBriefServices.UpdateIsReborrow(LoanbriefId);
                    if (update)
                    {
                        //Call api chuyển luồng
                        var objCall = new ChangePipelineReq.Input
                        {
                            loanBriefId = LoanbriefId,
                            productId = ProductId,
                            status = Status
                        };
                        var result = _lmsService.ChangePipeline(objCall);
                        if (result != null && result.Meta != null && result.Meta.ErrorCode == 200)
                        {
                            var note = new LoanBriefNote
                            {
                                LoanBriefId = objCall.loanBriefId,
                                Note = string.Format("System: HĐ-{0} đã được chuyển sang luồng tái vay!", objCall.loanBriefId),
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.ChangePipe.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            };
                            _loanBriefServices.AddNote(note);
                        }
                    }
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        private void KalapaGetInfoFamily(string nationalCard, int loanbriefId)
        {
            try
            {
                bool isCheck = false;
                //Kiểm tra đã gọi chưa. nếu chưa gọi thì gọi lại
                var lastCallApi = _logCallApiService.GetLast(loanbriefId, (int)ActionCallApi.KalapaFamily);
                if (lastCallApi != null)
                {
                    //kiểm tra xem data input có khác nhau k
                    if (!string.IsNullOrEmpty(lastCallApi.Input))
                    {
                        if (String.Compare(lastCallApi.Input, string.Format("{0} - {1}", nationalCard, loanbriefId), true) == 0)
                            isCheck = false;
                        else
                            isCheck = true;
                    }
                }
                else
                {
                    isCheck = true;
                }
                if (isCheck)
                {
                    var data = _lmsService.GetFamily(nationalCard, loanbriefId);
                    if (data != null)
                    {
                        var str = new StringBuilder();
                        str.AppendLine("<b>Thông tin hộ khẩu</b>");
                        if (!string.IsNullOrEmpty(data.houseOwner))
                        {
                            str.AppendFormat("<br />Chủ hộ: <b>{0}</b>:", data.houseOwner);
                        }
                        if (data.familyMember != null && data.familyMember.Count > 0)
                        {
                            foreach (var member in data.familyMember)
                            {
                                if (!string.IsNullOrEmpty(member.name))
                                    str.AppendFormat("<br /><b>{0}</b>", member.name);
                                if (!string.IsNullOrEmpty(member.gender))
                                    str.AppendFormat("<br />    + Giới tính: <b>{0}</b>", member.gender);
                                if (!string.IsNullOrEmpty(member.dateOfBirth))
                                    str.AppendFormat("<br />    + Ngày sinh: <b>{0}</b>", member.dateOfBirth);
                                if (!string.IsNullOrEmpty(member.address))
                                    str.AppendFormat("<br />    + Địa chỉ: <b>{0}</b>", member.address);
                                if (!string.IsNullOrEmpty(member.medicalInsurance))
                                    str.AppendFormat("<br />    + BHYT: <b>{0}</b>", member.medicalInsurance);
                            }
                        }
                        //Write thông tin ra comment
                        var message = str.ToString();
                        if (!string.IsNullOrEmpty(message))
                        {
                            // đơn vay ko đủ thông tin check ekyc > trả lại                                        
                            var note = new LoanBriefNote
                            {
                                LoanBriefId = loanbriefId,
                                Note = message,
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            };
                            _loanBriefServices.AddNote(note);

                            //cập nhật vào db
                            var json = JsonConvert.SerializeObject(data);
                            if (_loanBriefServices.AddJsonInfoFamilyKalapa(loanbriefId, json))
                                Console.WriteLine(string.Format("InfoFamily: Xu ly don HĐ-{0} {1}", loanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        }
                        else
                        {
                            var note = new LoanBriefNote
                            {
                                LoanBriefId = loanbriefId,
                                Note = "Không có thông tin sổ hộ khẩu",
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            };
                            _loanBriefServices.AddNote(note);
                        }
                    }
                    else
                    {
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = loanbriefId,
                            Note = "Không lấy được thông tin sổ hộ khẩu",
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        _loanBriefServices.AddNote(note);
                    }
                }

            }
            catch
            {

            }
        }

        private bool CheckLoanInfoMomo(LoanBrief entity)
        {
            var check = true;
            try
            {
                var logCheckMomo = _logLoanInfoAIService.GetLastRequest(entity.LoanBriefId, (int)ServiceTypeAI.CheckLoanMomo);
                if (logCheckMomo != null && logCheckMomo.Status == 1
                    && !string.IsNullOrEmpty(logCheckMomo.ResponseRequest)
                    && logCheckMomo.CreatedAt.Value.Day != DateTime.Now.Day
                    && String.Compare(logCheckMomo.Request, entity.NationalCard, true) == 0)
                {
                    return true;
                }

                var numBillBad = 0;
                var isCancel = false;
                var isBackList = false;
                var noteStr = "";
                if (!string.IsNullOrEmpty(entity.NationalCard))
                {
                    var result = _serviceAI.CheckLoanMomo(entity.LoanBriefId, entity.NationalCard, "HUB");
                    if (result != null)
                    {
                        var billsBad = result.Data.Bills.Where(x => x.Status == "BAD").Count();

                        // Nếu Khách hàng đang có 1 khoản vay là nợ xấu tại các tổ chức khác: Chặn đơn vay sản phẩm XMKCC, XMKGT.
                        if (billsBad == 1)
                        {
                            if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                            {
                                isCancel = true;
                                noteStr = "Auto System: Hủy đơn do khách hàng có 1 khoản vay nợ xấu ở tổ chức khác";
                            }
                            else
                                numBillBad = 1;

                        }

                        //Nếu Khách hàng đang có từ 2 khoản vay là nợ xấu tại các tổ chức khác trở lên: Chặn đơn vay và cho vào blacklist.
                        else if (billsBad >= 2)
                        {
                            isCancel = true;
                            isBackList = true;
                            noteStr = string.Format("Auto System: Hủy đơn do khách hàng có {0} khoản vay nợ xấu ở tổ chức khác", billsBad);
                        }
                    }
                }
                if (!isCancel && !string.IsNullOrEmpty(entity.NationCardPlace))
                {
                    var result2 = _serviceAI.CheckLoanMomo(entity.LoanBriefId, entity.NationCardPlace, "HUB");
                    if (result2 != null)
                    {
                        var billsBad = result2.Data.Bills.Where(x => x.Status == "BAD").Count();
                        // Nếu Khách hàng đang có 1 khoản vay là nợ xấu tại các tổ chức khác: Chặn đơn vay sản phẩm XMKCC, XMKGT.
                        if (billsBad == 1)
                        {
                            if (numBillBad > 0)
                            {
                                isCancel = true;
                                isBackList = true;
                                noteStr = string.Format("Auto System: Hủy đơn do khách hàng có {0} khoản vay nợ xấu ở tổ chức khác", billsBad + numBillBad);
                            }
                            else
                            {
                                if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                {
                                    isCancel = true;
                                    noteStr = "Auto System: Hủy đơn do khách hàng có 1 khoản vay nợ xấu ở tổ chức khác";
                                }
                            }
                        }
                        //Nếu Khách hàng đang có từ 2 khoản vay là nợ xấu tại các tổ chức khác trở lên: Chặn đơn vay và cho vào blacklist.
                        else if (billsBad >= 2)
                        {
                            isCancel = true;
                            isBackList = true;
                            noteStr = string.Format("Auto System: Hủy đơn do khách hàng có {0} khoản vay nợ xấu ở tổ chức khác", billsBad);
                        }
                    }
                }
                if (isCancel)
                {
                    if (_loanBriefServices.CancelLoan(entity.LoanBriefId, 759, "Auto System: Hệ thống hủy tự động do có nhiều khoản vay ở bên khác"))
                    {
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = noteStr,
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.AutoCancel.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        _loanBriefServices.AddNote(note);
                        //Nếu là đơn định vị => hủy hđ định vị
                        if (entity.IsLocate == true && !string.IsNullOrEmpty(entity.DeviceId))
                        {
                            var token = _authenticationAI.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
                            if (!string.IsNullOrEmpty(token))
                            {
                                var resultOpent = _serviceAI.CloseContract("HD-" + entity.LoanBriefId, entity.DeviceId, token, entity.LoanBriefId);
                                if (resultOpent)
                                {
                                    _loanBriefServices.AddNote(new LoanBriefNote()
                                    {
                                        LoanBriefId = entity.LoanBriefId,
                                        Note = string.Format("Đơn định vị: Đóng hợp đồng định vị với imei: {0}", entity.DeviceId),
                                        FullName = "Auto System",
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        ShopId = EnumShop.Tima.GetHashCode(),
                                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                    });

                                }
                            }
                        }
                        check = false;
                    }
                }
                if (isBackList)
                {
                    //Add BlackList
                    var objAddBlacklist = new RequestAddBlacklist()
                    {
                        CustomerCreditId = entity.CustomerId ?? 0,
                        FullName = entity.FullName,
                        NumberPhone = entity.Phone,
                        CardNumber = entity.NationalCard,
                        BirthDay = entity.Dob ?? null,
                        UserIdCreate = 1,
                        UserNameCreate = "Auto System",
                        FullNameCreate = "Auto System",
                        Note = noteStr,
                        LoanCreditId = entity.LoanBriefId
                    };
                    _lMSService.AddBlackList(objAddBlacklist);
                }
            }
            catch (Exception ex)
            {
                return check;
            }
            return check;

        }

        private void BackLoanToTls(string errorMessage, int loanbriefId)
        {
            _loanBriefServices.AddNote(new LoanBriefNote
            {
                LoanBriefId = loanbriefId,
                Note = errorMessage,
                FullName = "Auto System",
                Status = 1,
                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                CreatedTime = DateTime.Now,
                UserId = 1,
                ShopId = EnumShop.Tima.GetHashCode(),
                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
            });
            // trả đơn lại cho telesale cập nhật
            _loanBriefServices.PushToPipeline(loanbriefId, EnumActionPush.Back.GetHashCode());
        }

        private bool CheckBankAI(LoanBrief entity)
        {
            var check = true;
            try
            {
                if (lstBank != null && lstBank.Count > 0)
                {
                    if (dicBank.Count <= 0)
                    {
                        foreach (var item in lstBank)
                        {
                            if (!dicBank.ContainsKey(item.BankId))
                                dicBank[item.BankId] = new Bank();
                            dicBank[item.BankId] = item;
                        }
                    }

                }
                //var dicBank = new Dictionary<int, Bank>();
                if (dicBank.ContainsKey(entity.BankId.Value) && !string.IsNullOrEmpty(entity.FullName))
                {
                    var checkBank = _serviceAI.CheckBank(entity.LoanBriefId, entity.BankAccountNumber, dicBank[entity.BankId.Value].BankCode);
                    if (checkBank.Status == 0 && !string.IsNullOrEmpty(checkBank.Data.AccName))
                    {
                        //Xử lý so sánh
                        if (!entity.FullName.IsNormalized())
                            entity.FullName = entity.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                        entity.FullName = entity.FullName.ReduceWhitespace();
                        entity.FullName = entity.FullName.TitleCaseString();

                        entity.BankAccountName = checkBank.Data.AccName.ReduceWhitespace();
                        entity.BankAccountName = entity.BankAccountName.TitleCaseString();
                        entity.BankAccountName = ConvertExtensions.ConvertToUnSign(entity.BankAccountName);
                        if (String.Compare(ConvertExtensions.ConvertToUnSign(entity.FullName), entity.BankAccountName, false) != 0)
                        {
                            var note = new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = String.Format("System: Đơn vay bị trả lại do tên kh và tên chủ tk không khớp nhau - {0}", checkBank.Data.AccName),
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            };
                            _loanBriefServices.AddNote(note);
                            // trả đơn lại cho telesale cập nhật
                            _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Back.GetHashCode());
                            check = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return check;
            }
            return check;
        }

        private PriceProductTopup GetMaxPriceProductTopup(int productId, int productCredit, int loanBriefId, int typeOfOwnerShip, long totalMoneyCurrent)
        {
            long originalCarPrice = 0;
            // lấy giá bên AI
            decimal priceCarAI = _productService.GetPriceAI(productId, loanBriefId);
            if (priceCarAI > 0)
            {
                originalCarPrice = (long)priceCarAI;
            }
            else
            {
                //var productPriceCurrent = _productService.GetProduct(productId);
                //if (productPriceCurrent != null && productPriceCurrent.PriceCurrent > 0)
                //    originalCarPrice = productPriceCurrent.PriceCurrent ?? 0l;
            }
            //var originalCarPriceLast = originalCarPrice - TotalMoneyCurrent;
            var data = new PriceProductTopup
            {
                PriceAi = originalCarPrice,
                MaxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPriceTopup(productCredit, originalCarPrice, totalMoneyCurrent, typeOfOwnerShip)
            };
            return data;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            // _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
            //_timer?.Dispose();
        }

    }
}
