﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoDistributingLenderJob : IHostedService, IDisposable
    {
        private Thread _thread;
        protected IConfiguration _baseConfig;
        private ILenderAutoDistributeService _lenderService;
        private ILMSService _lmsService;
        private ISendMessage _sendMessage;
        private IConfiguration _configuration;

        public AutoDistributingLenderJob(ILenderAutoDistributeService lenderService, ISendMessage sendMessage, IConfiguration configuration, ILMSService lmsService)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
            _lenderService = lenderService;
            _lmsService = lmsService;
            _sendMessage = sendMessage;
        }
        private async void DoWork(object state)
        {
            while (true)
            {
                try
                {
                    if (!_lenderService.IsAutoDistributing())
                    {
                        Thread.Sleep(1000 * 30);
                    }
                    else
                    {
                        // Lấy đơn cần chia tự động
                        var loanCredits = _lenderService.GetWaitDistributingLoanBriefs();
                        if (loanCredits != null && loanCredits.Count > 0)
                        {
                            foreach (var item in loanCredits)
                            {
                                Log.Information("Bắt đầu chia đơn tự động :" + item.LoanBriefId + " - Level " + item.PriorityLevel);
                                try
                                {
                                    // check trạng thái đơn vay ??
                                    if (item.Status == (int)EnumLoanStatus.DISBURSED || item.Status == (int)EnumLoanStatus.WAIT_DISBURSE || item.IsLock == true)
                                    {
                                        continue;
                                    }
                                    // Get Lender Config by level
                                    int maxLevel = 3;
                                    bool isLender = true;
                                    var priorityLender = _lenderService.AutoDistributingLender();
                                    int currentLevel = item.PriorityLevel.HasValue ? item.PriorityLevel.Value : (priorityLender.Equals("DT") ? 101 : 1);
                                    if (currentLevel > 100) // level dt
                                    {
                                        currentLevel = currentLevel - 100;
                                        isLender = false;
                                    }
                                    List<LenderConfig> chosenLenders = new List<LenderConfig>();
                                    choose_lender:
                                    chosenLenders = new List<LenderConfig>();
                                    var lenders = _lenderService.GetLenderConfigs(currentLevel, isLender);
                                    if (lenders != null && lenders.Count > 0)
                                    {
                                        foreach (var lender in lenders)
                                        {
                                            if ((lender.CurrentMoney - lender.LockedMoney) >= item.LoanAmount)
                                            {
                                                // Lender đủ tiền để giải ngân
                                                chosenLenders.Add(lender);
                                            }
                                        }
                                        // kiểm tra xem có lender không
                                        if (chosenLenders.Count == 0)
                                        {
                                            // Không có ai >> next level
                                            currentLevel++;
                                            goto choose_lender;
                                        }
                                        else
                                        {
                                            // có lender
                                            goto chosen_lenders;
                                        }
                                    }
                                    else
                                    {
                                        // Kiểm tra nếu level bị skip thì chạy đến max level , DT thì không
                                        if (isLender)
                                        {
                                            if (currentLevel >= maxLevel)
                                            {
                                                if (priorityLender.Equals("NA"))
                                                {
                                                    currentLevel = 1;
                                                    isLender = false;
                                                    goto choose_lender;
                                                }
                                                else
                                                {
                                                    _lenderService.UpdateNotMet(item.LoanBriefId);
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                currentLevel++;
                                                goto choose_lender;
                                            }
                                        }
                                        else
                                        {
                                            if (currentLevel >= maxLevel)
                                            {
                                                // Không có nđt nào thỏa mãn điều kiện     
                                                if (priorityLender.Equals("DT"))
                                                {
                                                    // Bắt đầu bằng DT > Quay lại NA
                                                    currentLevel = 1;
                                                    isLender = true;
                                                    goto choose_lender;
                                                }
                                                else
                                                {
                                                    _lenderService.UpdateNotMet(item.LoanBriefId);
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                currentLevel++;
                                                goto choose_lender;
                                            }
                                        }
                                    }
                                    chosen_lenders:
                                    {
                                        if (chosenLenders.Count > 0)
                                        {                                            
                                            var esignLender = new List<int>();
                                            var noEsignLender = new List<int>();
                                            // Check xem đơn có phải esign ko 
                                            if (item.EsignState == (int)EsignState.BORROWER_SIGNED || item.EsignState == (int)EsignState.LENDER_SIGNED) // Ký lại
                                            {
                                                // Lấy thông tin esign
                                                foreach (var lender in chosenLenders)
                                                {
                                                    if (lender.Esign == null || (lender.EsignUpdatedTime.HasValue && lender.EsignUpdatedTime.Value.AddDays(-2) > DateTime.Now))
                                                    {
                                                        // Lấy thông tin esign
                                                        var shopInfo = await _lmsService.GetShopInfo(lender.LenderId.Value);
                                                        if (shopInfo.Status == 1)
                                                        {
                                                            if (shopInfo.Data.AgreementId != null)
                                                            {
                                                                esignLender.Add(lender.LenderId.Value);
                                                                lender.Esign = true;
                                                                // Có thông tin esign
                                                            }
                                                            else
                                                            {
                                                                // Không có thông tin esign
                                                                noEsignLender.Add(lender.LenderId.Value);
                                                                lender.Esign = false;
                                                            }
                                                        }
                                                    }
                                                }
                                                if (esignLender.Count > 0)
                                                {
                                                    // Update vào config
                                                    _lenderService.UpdateEsignLenderConfig(esignLender, true);

                                                }
                                                if (noEsignLender.Count > 0)
                                                {
                                                    // Update vào config
                                                    _lenderService.UpdateEsignLenderConfig(noEsignLender, false);
                                                }
                                                // Chỉ lấy nđt có esign
                                                chosenLenders.RemoveAll(x => x.Esign == null || !x.Esign.Value);
                                                if (chosenLenders.Count == 0) // Không có nđt đủ điều kiện
                                                {
                                                    currentLevel++;
                                                    goto choose_lender;
                                                }
                                            }
                                            Log.Information(item.LoanBriefId + "- Level :" + currentLevel + "- Lenders :" + String.Join(", ", chosenLenders.Select(x => x.LenderName).ToArray()));
                                            if (isLender)
                                            {
                                                // Update đã chia
                                                _lenderService.UpdateDistributed(item.LoanBriefId, isLender ? currentLevel : currentLevel + 100);
                                                // Insert vảo bảng lender loan brief
                                                // var lenderLoanBriefs = new List<LenderLoanBrief>();
                                                foreach (var cl in chosenLenders)
                                                {
                                                    try
                                                    {
                                                        LenderLoanBrief lenderLoanBrief = new LenderLoanBrief();
                                                        lenderLoanBrief.LenderId = cl.LenderId;
                                                        lenderLoanBrief.LenderName = cl.LenderName;
                                                        lenderLoanBrief.LoanBriefId = item.LoanBriefId;
                                                        lenderLoanBrief.PushedTime = DateTime.Now;
                                                        lenderLoanBrief.Status = (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED;
                                                        _lenderService.InsertLenderLoanBrief(lenderLoanBrief);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Log.Information("Error " + ex.Message + "\n" + ex.StackTrace);
                                                    }
                                                }
                                                // Insert batch                                       
                                                // _lenderService.InsertLenderLoanBrief(lenderLoanBriefs);
                                                // Received + 1;
                                                foreach (var cl in chosenLenders)
                                                {
                                                    var message = new NotifyLenderApp();
                                                    message.key = "tima@lender#234";
                                                    message.data = new List<DataItemNotify>();
                                                    message.data.Add(new DataItemNotify()
                                                    {
                                                        loanCreditId = item.LoanBriefId,
                                                        topic = cl.LenderId.ToString()
                                                    });
                                                    _sendMessage.SendNotifyToAppLender(message, item.LoanBriefId);
                                                    // update received
                                                    _lenderService.UpdateReceived(cl.LenderId.Value, 1);
                                                }
                                            }
                                            else
                                            {
                                                // Nếu chia cho DT                                            
                                                _lenderService.UpdateLockDistributed(item.LoanBriefId, isLender ? currentLevel : currentLevel + 100);
                                                // Insert vảo bảng lender loan brief
                                                // var lenderLoanBriefs = new List<LenderLoanBrief>();
                                                // Đẩy cho DT đầu tiên thôi
                                                foreach (var cl in chosenLenders)
                                                {
                                                    LenderLoanBrief lenderLoanBrief = new LenderLoanBrief();
                                                    lenderLoanBrief.LenderId = cl.LenderId;
                                                    lenderLoanBrief.LenderName = cl.LenderName;
                                                    lenderLoanBrief.LoanBriefId = item.LoanBriefId;
                                                    lenderLoanBrief.PushedTime = DateTime.Now;
                                                    lenderLoanBrief.Status = (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING;
                                                    _lenderService.InsertLenderLoanBrief(lenderLoanBrief);
                                                    _lenderService.UpdateReceivedLock(cl.LenderId.Value, item.LoanAmount.Value);
                                                    break;
                                                }
                                                // Insert batch                                       
                                                //_lenderService.InsertLenderLoanBriefs(lenderLoanBriefs);
                                                // gửi thông báo sang AG
                                                using (var httpClient = new HttpClient())
                                                {
                                                    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "8228459735");
                                                    var data = new
                                                    {
                                                        LoanBriefId = item.LoanBriefId,
                                                        LenderId = chosenLenders[0].LenderId.Value,
                                                        LenderName = chosenLenders[0].LenderName
                                                    };
                                                    var sContent = JsonConvert.SerializeObject(data);
                                                    Log.Information("/LoanCreditProfile/PushLoanBriefToLender Data:" + sContent);
                                                    var content = new StringContent(sContent, Encoding.UTF8, "application/json");
                                                    var url = _baseConfig["AppSettings:LMS"].ToString() + "/api/LoanCreditProfile/PushLoanBriefToLender";
                                                    var response = await httpClient.PostAsync(url, content);
                                                    var result = await response.Content.ReadAsStringAsync();
                                                    Log.Information("/LoanCreditProfile/PushLoanBriefToLender :" + result);
                                                    if (result != null)
                                                    {
                                                        var defaultResponse = JsonConvert.DeserializeObject<AGDefaultResponse>(result);
                                                        if (defaultResponse != null && defaultResponse.Status == 1)
                                                        {
                                                            // AG Nhận đơn thành công
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Error(ex, "LOS.ThirdPartyProccesor exception");
                                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                    Log.Error(ex, "LOS.ThirdPartyProccesor exception");
                }
                //Thread 
                Thread.Sleep(1000 * 30);
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
