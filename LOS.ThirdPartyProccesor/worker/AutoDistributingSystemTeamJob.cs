﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoDistributingSystemTeamJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IUserService _userServices;
        private readonly IShopService _shopService;
        private readonly ILogDistributionUserService _logDistributionUserService;
        //private Timer _timer;
        private Thread _thread;
        protected IConfiguration _baseConfig;
        private int indexRecieved = 0;
        public AutoDistributingSystemTeamJob(ILoanBriefService loanBriefServices, IUserService userServices, IShopService shopService, ILogDistributionUserService logDistributionUserService)
        {
            _loanBriefServices = loanBriefServices;
            _userServices = userServices;
            _shopService = shopService;
            _logDistributionUserService = logDistributionUserService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }
        //Chia đơn từ giỏ system ra cho tls
        private void DoWork(object state)
        {
            while (true)
            {
                try
                {
                    var loanCredits = _loanBriefServices.ListLoanbriefAdvice((int)EnumUser.system_team);
                    if (loanCredits != null && loanCredits.Count > 0)
                    {
                        foreach (var item in loanCredits)
                        {
                            try
                            {
                                #region Chờ tư vấn => chuyển giỏ cho tls từ các giỏ system team                               
                                var telesaleShiftNow = GetTelesaleShift(DateTime.Now);
                                //Nếu không có ca làm việc => bỏ qua
                                if (telesaleShiftNow == 0)
                                    continue;
                                //Nếu đơn ở giỏ system_team nhưng không có teamId => bỏ qua
                                if (item.BoundTelesaleId == (int)EnumUser.system_team && item.TeamTelesalesId.GetValueOrDefault(0) == 0)
                                    continue;

                                var boundTelesaleId = 0;
                                bool distributing = true;
                                if (distributing)
                                {
                                    if (item.ProvinceId > 0)
                                    {
                                        if (telesaleShiftNow > 0)
                                        {
                                            var telesaleTeam = item.TeamTelesalesId.Value;
                                            var telesale = GetTelesales(boundTelesaleId, item.ProductId.Value, telesaleShiftNow,
                                                telesaleTeam);
                                            if (telesale != null && telesale.UserId > 0)
                                            {
                                                PushToTelesale(item, telesale, false);
                                            }
                                            else
                                            {
                                                Console.WriteLine(string.Format("PushToTelesale: khong có telesale trong team {2} HĐ-{0} {1}", item.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), telesaleTeam));
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine(string.Format("PushToTelesale: KHONG TIM DUOC CA LAM VIEC HĐ-{0} {1}", item.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                        }
                                    }
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                            }
                        }
                        Thread.Sleep(3000); //sleep 3s
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                    Log.Error(ex, "LOS.ThirdPartyProccesor AutoDistributingSystemTeamJob exception");
                }
                //Thread 
                Thread.Sleep(3000);
            }
        }

        private int GetTelesaleShift(DateTime timeCheck)
        {

            var listData = _userServices.ListTelesaleShift();
            if (listData != null && listData.Count > 0)
            {
                foreach (var item in listData)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(item.StartTime) && !string.IsNullOrEmpty(item.EndTime))
                        {
                            var now = DateTime.Now;
                            var arrStart = item.StartTime.Split(':');
                            var arrEnd = item.EndTime.Split(':');
                            var start = new DateTime(now.Year, now.Month, now.Day, Int32.Parse(arrStart[0]), Int32.Parse(arrStart[1]), 0);
                            var end = new DateTime(now.Year, now.Month, now.Day, Int32.Parse(arrEnd[0]), Int32.Parse(arrEnd[1]), 0);
                            if (timeCheck >= start && timeCheck <= end)
                                return item.TelesalesShiftId;
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            return 0;
        }
        private void PushToTelesale(LoanbriefDistributingTelelsale entity, User telesale, bool pushPipeline = true, int logCiscoPushToTelesaleId = 0)
        {
            try
            {
                //Kiểm tra xem trước đó đã nhận line chưa
                var last = _loanBriefServices.GetLastLogDistributionUser(entity.LoanBriefId, (int)TypeDistributionLog.TELESALE);
                //TH mà tls đã nhận đơn line
                if (last != null && last.CreatedBy > 0 && last.UserId > 0
                    && last.CreatedBy == last.UserId)
                {
                    var user = _userServices.GetTelesale(last.UserId.Value);
                    if (user != null && user.TeamTelesalesId > 0)
                    {
                        _loanBriefServices.DistributingTelesale(entity.LoanBriefId, user.UserId, user.TeamTelesalesId);
                        return;
                    }
                }
                var loanbrief = _loanBriefServices.GetById(entity.LoanBriefId);
                if (loanbrief == null || loanbrief.LoanBriefId == 0)
                    return;

                if (loanbrief.BoundTelesaleId == telesale.UserId
                    || _loanBriefServices.DistributingTelesale(entity.LoanBriefId, telesale.UserId, telesale.TeamTelesalesId))
                {
                    if (loanbrief.BoundTelesaleId == telesale.UserId && loanbrief.TeamTelesalesId.GetValueOrDefault(0) == 0)
                    {
                        _loanBriefServices.DistributingTelesale(entity.LoanBriefId, telesale.UserId, telesale.TeamTelesalesId);
                    }
                    if (pushPipeline)
                    {
                        //đẩy đơn lên luồng xử lý
                        if (_loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Push.GetHashCode()))
                        {
                            //var taskRun = new List<Task>();
                            var teamName = string.Empty;
                            if (telesale.TeamTelesalesId > 0)
                            {
                                var team = _userServices.GetTeamTelesale(telesale.TeamTelesalesId.Value);
                                if (team != null && !string.IsNullOrEmpty(team.Name))
                                    teamName = team.Name;
                            }
                            //lưu lại comment
                            _loanBriefServices.AddNote(new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = string.Format("System: Hệ thống đã chia đơn về cho telesales {0}({1}) Team {2}", telesale.FullName, telesale.Username, teamName),
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.DistributingLoanOfTelesale.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            });
                            //cập nhật thời gian nhận đơn cuối cùng của telesales
                            _userServices.UpdateLastReceivedTelesale(telesale.UserId);
                            //Thêm log chia đơn cho tls
                            _logDistributionUserService.Insert(new LogDistributionUser()
                            {
                                UserId = telesale.UserId,
                                LoanbriefId = entity.LoanBriefId,
                                CreatedAt = DateTime.Now,
                                TypeDistribution = (int)TypeDistributionLog.TELESALE
                            });

                            Console.WriteLine(string.Format("PushToTelesale: Xu ly don HĐ-{0} {1} {2}", entity.LoanBriefId, telesale.UserId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        }
                    }
                    else
                    {
                        //var taskRun = new List<Task>();
                        //var taskRun = new List<Task>();
                        var teamName = string.Empty;
                        if (telesale.TeamTelesalesId > 0)
                        {
                            var team = _userServices.GetTeamTelesale(telesale.TeamTelesalesId.Value);
                            if (team != null && !string.IsNullOrEmpty(team.Name))
                                teamName = team.Name;
                        }
                        //lưu lại comment
                        _loanBriefServices.AddNote(new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = string.Format("System: Hệ thống đã chia đơn về cho telesales {0}({1}) Team {2}", telesale.FullName, telesale.Username, teamName),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.DistributingLoanOfTelesale.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        //Thêm log chia đơn cho tls
                        _logDistributionUserService.Insert(new LogDistributionUser()
                        {
                            UserId = telesale.UserId,
                            LoanbriefId = entity.LoanBriefId,
                            CreatedAt = DateTime.Now,
                            TypeDistribution = (int)TypeDistributionLog.TELESALE
                        });
                        //cập nhật thời gian nhận đơn cuối cùng của telesales
                        _userServices.UpdateLastReceivedTelesale(telesale.UserId);
                        Console.WriteLine(string.Format("PushToTelesale_Change : Xu ly don HĐ-{0} {1} {2}", entity.LoanBriefId, telesale.UserId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor PushToTelesale exception");
            }
        }
        private User GetTelesales(int? boundTelesaleId, int? productId, int? telesaleShift = -1, int? telesaleTeamId = -1)
        {
            //Lấy telesale nhận đơn cuối cùng
            try
            {
                if (boundTelesaleId > 0)
                    return _userServices.GetTelesale(boundTelesaleId.Value);
                else
                    return _userServices.GetLastUserTelesales(productId.Value, -1, telesaleShift, telesaleTeamId);
            }
            catch
            {
            }
            return null;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
