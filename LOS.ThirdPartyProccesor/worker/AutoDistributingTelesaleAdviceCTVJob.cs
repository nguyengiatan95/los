﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoDistributingTelesaleAdviceCTVJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IUserService _userServices;
        private readonly ILogResultAutoCallServices _logResultAutoCallServices;
        private readonly ILogCiscoPushToTelesaleService _logCiscoPushToTelesaleService;
        private readonly ILogDistributionUserService _logDistributionUserService;
        //private Timer _timer;
        private Thread _thread;
        protected IConfiguration _baseConfig;
        private int indexRecieved = 0;

        public AutoDistributingTelesaleAdviceCTVJob(ILoanBriefService loanBriefServices, IUserService userServices, ILogResultAutoCallServices logResultAutoCallServices, 
            ILogCiscoPushToTelesaleService logCiscoPushToTelesaleService, ILogDistributionUserService logDistributionUserService)
        {
            _loanBriefServices = loanBriefServices;
            _userServices = userServices;
            _logResultAutoCallServices = logResultAutoCallServices;
            _logCiscoPushToTelesaleService = logCiscoPushToTelesaleService;
            _logDistributionUserService = logDistributionUserService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }
        //Chia đơn CTV
        private void DoWork(object state)
        {

            while (true)
            {
                try
                {
                    //lấy ra các đơn của CTV cần xử lý
                    var loanCredits = _loanBriefServices.WaitPushToTelesaleAdviceCTV();
                    if (loanCredits != null && loanCredits.Count > 0)
                    {
                        foreach (var item in loanCredits)
                        {
                            try
                            {
                                if (item.BoundTelesaleId == (int)EnumUser.ctv_autocall
                                    || item.BoundTelesaleId == (int)EnumUser.autocall_coldlead
                                    ||  item.BoundTelesaleId == (int)EnumUser.recare)
                                {
                                    if(item.Status == (int)EnumLoanStatus.TELESALE_ADVICE)
                                    {
                                        var telesaleShiftNow = GetTelesaleShift(DateTime.Now);
                                        //Nếu không có ca làm việc => bỏ qua
                                        if (telesaleShiftNow == 0)
                                            continue;
                                        bool distributing = false;
                                        //Nếu gọi smartdailer không được -> chia cho teleslae
                                        int LogCiscoPushToTelesaleId = 0;
                                        if (_baseConfig["AppSettings:IsCheckLogLogCiscoPushToTelesale"] != null
                                            && _baseConfig["AppSettings:IsCheckLogLogCiscoPushToTelesale"].Equals("1"))
                                        {
                                            var logPushSmartDailer = _logCiscoPushToTelesaleService.Get(item.LoanBriefId);
                                            if (logPushSmartDailer != null && logPushSmartDailer.LogCiscoPushToTelesaleId > 0)
                                            {
                                                LogCiscoPushToTelesaleId = logPushSmartDailer.LogCiscoPushToTelesaleId;
                                                distributing = true;
                                            }
                                        }
                                        else
                                        {
                                            if (item.BoundTelesaleId == (int)EnumUser.ctv_autocall
                                                || item.BoundTelesaleId == (int)EnumUser.autocall_coldlead
                                                 || item.BoundTelesaleId == (int)EnumUser.recare)
                                            {
                                                var logPushSmartDailer = _logCiscoPushToTelesaleService.Get(item.LoanBriefId);
                                                if (logPushSmartDailer != null && logPushSmartDailer.LogCiscoPushToTelesaleId > 0)
                                                {
                                                    LogCiscoPushToTelesaleId = logPushSmartDailer.LogCiscoPushToTelesaleId;
                                                    if (item.BoundTelesaleId == (int)EnumUser.recare)
                                                    {
                                                        //lấy thời gian cuối cùng chuyển giỏ recare
                                                        var last = _loanBriefServices.GetLastTelesaleLoanbrief(item.LoanBriefId, (int)EnumUser.recare);
                                                        if (last != null && last.Id > 0)
                                                        {
                                                            //Thời gian chia đơn lớn hơn thời gian chuyển sang giỏ recare cuối cùng
                                                            if (logPushSmartDailer.CreateTime > last.CreatedAt)
                                                                distributing = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        distributing = true;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //Nếu k check auto call => chia đơn
                                                distributing = true;
                                            }

                                        }
                                        //chuyển giỏ cho telesale
                                        //Trong giờ hành chính thì chuyển về giỏ cho telesale
                                        if (distributing)
                                        {
                                            if (item.ProvinceId.HasValue && item.ProvinceId > 0)
                                            {
                                                if (telesaleShiftNow > 0)
                                                {
                                                    var telesaleTeam = -1;
                                                    if (item.CreateBy > 0)
                                                    {
                                                        var data = _userServices.GetTeamManager(item.CreateBy.Value);
                                                        if (data > 0)
                                                            telesaleTeam = data;
                                                    }
                                                    var telesale = GetTelesales(item.ProvinceId.Value, 0, item.ProductId.Value, telesaleShiftNow, telesaleTeam);
                                                    if (telesale != null && telesale.UserId > 0)
                                                    {
                                                        PushToTelesale(item, telesale, false, LogCiscoPushToTelesaleId);
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine(string.Format("PushToTelesale CTV: khong co cau hinh don HĐ-{0} {1}", item.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine(string.Format("PushToTelesale: KHONG TIM DUOC CA LAM VIEC HĐ-{0} {1}", item.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                                }

                                            }
                                        }
                                    }                                    
                                   
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                            }
                        }
                        Thread.Sleep(1000 * 3);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                    Log.Error(ex, "LOS.ThirdPartyProccesor PushLoanCreditToTelesales exception");
                }
                //Thread 
                Thread.Sleep(1000 * 3);
            }
        }

        private int GetTelesaleShift(DateTime timeCheck)
        {

            var listData = _userServices.ListTelesaleShift();
            if (listData != null && listData.Count > 0)
            {
                foreach (var item in listData)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(item.StartTime) && !string.IsNullOrEmpty(item.EndTime))
                        {
                            var now = DateTime.Now;
                            var arrStart = item.StartTime.Split(':');
                            var arrEnd = item.EndTime.Split(':');
                            var start = new DateTime(now.Year, now.Month, now.Day, Int32.Parse(arrStart[0]), Int32.Parse(arrStart[1]), 0);
                            var end = new DateTime(now.Year, now.Month, now.Day, Int32.Parse(arrEnd[0]), Int32.Parse(arrEnd[1]), 0);
                            if (timeCheck >= start && timeCheck <= end)
                                return item.TelesalesShiftId;
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            return 0;
        }
        public bool IsRunning(DateTime timeCheck)
        {
            var now = DateTime.Now;
            var startTime = new DateTime(now.Year, now.Month, now.Day, 08, 15, 00);
            var endTime = new DateTime(now.Year, now.Month, now.Day, 11, 55, 00);
            var startTime2 = new DateTime(now.Year, now.Month, now.Day, 13, 15, 00);
            var endTime2 = new DateTime(now.Year, now.Month, now.Day, 20, 20, 00);
            //Nếu thời gian là trong giờ hành chính thì chia
            if ((timeCheck >= startTime && timeCheck <= endTime)
                || (timeCheck >= startTime2 && timeCheck <= endTime2))
            {
                return true;
            }
            return false;
        }

        private void PushToTelesale(LoanbriefDistributingTelelsale entity, User telesale, bool pushPipeline = true, int logCiscoPushToTelesaleId = 0)
        {
            try
            {
                //Kiểm tra xem trước đó đã nhận line chưa
                var last = _loanBriefServices.GetLastLogDistributionUser(entity.LoanBriefId, (int)TypeDistributionLog.TELESALE);
                //TH mà tls đã nhận đơn line
                if (last != null && last.CreatedBy > 0 && last.UserId > 0
                    && last.CreatedBy == last.UserId)
                {
                    var user = _userServices.GetTelesale(last.UserId.Value);
                    if (user != null && user.TeamTelesalesId > 0)
                    {
                        _loanBriefServices.DistributingTelesale(entity.LoanBriefId, user.UserId, user.TeamTelesalesId);
                        return;
                    }
                }
                if (_loanBriefServices.DistributingTelesale(entity.LoanBriefId, telesale.UserId, telesale.TeamTelesalesId))
                {
                    if (pushPipeline)
                    {
                        //đẩy đơn lên luồng xử lý
                        if (_loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Push.GetHashCode()))
                        {
                            var taskRun = new List<Task>();
                            //lưu lại comment
                            var note = new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = string.Format("System: Hệ thống đã chia đơn về cho telesales {0}({1})", telesale.FullName, telesale.Username),
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.DistributingLoanOfTelesale.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            };
                            taskRun.Add(Task.Run(() => _loanBriefServices.AddNote(note)));
                            //cập nhật thời gian nhận đơn cuối cùng của telesales
                            taskRun.Add(Task.Run(() => _userServices.UpdateLastReceivedTelesale(telesale.UserId)));
                            var telesaleLoanbrief = new TelesaleLoanbrief
                            {
                                TelesalesId = telesale.UserId,
                                LoanBriefId = entity.LoanBriefId,
                                CreatedAt = DateTime.Now
                            };
                            taskRun.Add(Task.Run(() => _loanBriefServices.AddLogDistributingTelesales(telesaleLoanbrief)));
                            if (taskRun.Count > 0)
                                Task.WaitAll(taskRun.ToArray());
                            //Thêm log chia đơn cho tls
                            _logDistributionUserService.Insert(new LogDistributionUser()
                            {
                                UserId = telesale.UserId,
                                LoanbriefId = entity.LoanBriefId,
                                CreatedAt = DateTime.Now,
                                TypeDistribution = (int)TypeDistributionLog.TELESALE
                            });
                            //Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToTelesale: Done HĐ-{0}", entity.LoanBriefId));
                            if (logCiscoPushToTelesaleId > 0)
                                _logCiscoPushToTelesaleService.UpdatePushTelesale(logCiscoPushToTelesaleId);

                            Console.WriteLine(string.Format("PushToTelesale CTV: Xu ly don HĐ-{0} {1} {2}", entity.LoanBriefId, telesale.UserId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        }
                    }
                    else
                    {
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = string.Format("System: Hệ thống đã chia đơn về cho telesales {0}({1})", telesale.FullName, telesale.Username),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.DistributingLoanOfTelesale.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        _loanBriefServices.AddNote(note);
                        var telesaleLoanbrief = new TelesaleLoanbrief
                        {
                            TelesalesId = telesale.UserId,
                            LoanBriefId = entity.LoanBriefId,
                            CreatedAt = DateTime.Now
                        };
                       _loanBriefServices.AddLogDistributingTelesales(telesaleLoanbrief);
                        if (logCiscoPushToTelesaleId > 0)
                            _logCiscoPushToTelesaleService.UpdatePushTelesale(logCiscoPushToTelesaleId);
                        //Thêm log chia đơn cho tls
                        _logDistributionUserService.Insert(new LogDistributionUser()
                        {
                            UserId = telesale.UserId,
                            LoanbriefId = entity.LoanBriefId,
                            CreatedAt = DateTime.Now,
                            TypeDistribution = (int)TypeDistributionLog.TELESALE
                        });
                        //cập nhật thời gian nhận đơn cuối cùng của telesales
                        _userServices.UpdateLastReceivedTelesale(telesale.UserId);
                        Console.WriteLine(string.Format("PushToTelesale_Change CTV : Xu ly don HĐ-{0} {1} {2}", entity.LoanBriefId, telesale.UserId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor PushToTelesale exception");
            }
        }
        private User GetTelesales(int? ProvinceID, int? boundTelesaleId, int? productId, int? telesaleShift = -1, int? telesaleTeamId = -1)
        {
            //Lấy telesale nhận đơn cuối cùng
            try
            {
                //nếu đã có telesale => lấy thông tin tele đó
                //Update 11/10/2020 Chị Hoàn Request đơn về chia lần lượt cho telesale k phân biệt tỉnh
                if (boundTelesaleId > 0)
                {
                    return _userServices.GetTelesale(boundTelesaleId.Value);
                }
                else
                {
                    return _userServices.GetLastUserTelesales(productId.Value, -1, telesaleShift, telesaleTeamId);
                }
                //Nếu đơn là HN => chia cho telesale HN
                //if (ProvinceID == EnumProvince.HaNoi.GetHashCode() || ProvinceID == EnumProvince.BacNinh.GetHashCode())
                //{
                //    return _userServices.GetLastUserTelesales(productId.Value, EnumProvince.HaNoi.GetHashCode(), telesaleShift, telesaleTeamId);
                //}
                ////Nếu đơn ở HCM => chia all
                //else if (ProvinceID == EnumProvince.HCM.GetHashCode())
                //{
                //    return _userServices.GetLastUserTelesales(productId.Value, -1, telesaleShift, telesaleTeamId);
                //}
                //else
                //{
                //    return _userServices.GetLastUserTelesales(productId.Value, EnumProvince.HaNoi.GetHashCode(), telesaleShift, telesaleTeamId);
                //}
            }
            catch
            {
            }
            return null;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
