﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoDistributingTelesaleAdviceJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IUserService _userServices;
        private readonly ILogResultAutoCallServices _logResultAutoCallServices;
        private readonly ILogCiscoPushToTelesaleService _logCiscoPushToTelesaleService;
        private readonly IShopService _shopService;
        private readonly ILogDistributionUserService _logDistributionUserService;
        private Thread _thread;
        protected IConfiguration _baseConfig;
        public AutoDistributingTelesaleAdviceJob(ILoanBriefService loanBriefServices, IUserService userServices, ILogResultAutoCallServices logResultAutoCallServices,
            ILogCiscoPushToTelesaleService logCiscoPushToTelesaleService, IShopService shopService, ILogDistributionUserService logDistributionUserService)
        {
            _loanBriefServices = loanBriefServices;
            _userServices = userServices;
            _logResultAutoCallServices = logResultAutoCallServices;
            _logCiscoPushToTelesaleService = logCiscoPushToTelesaleService;
            _shopService = shopService;
            _logDistributionUserService = logDistributionUserService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }
        //Chia đơn từ giỏ system ra cho tls
        private void DoWork(object state)
        {
            while (true)
            {
                try
                {
                    //lấy ra d/s đơn chờ chia đơn cho CHT và đơn ở giỏ spt_system(26695) 
                    var loanCredits = _loanBriefServices.WaitPushToTelesale();
                    if (loanCredits != null && loanCredits.Count > 0)
                    {
                        foreach (var item in loanCredits)
                        {
                            try
                            {
                                #region Chờ tư vấn => chuyển giỏ cho tls từ các giỏ system
                                if ((item.BoundTelesaleId == (int)EnumUser.spt_System)
                                    && item.Status == (int)EnumLoanStatus.TELESALE_ADVICE)
                                {
                                    var telesaleShiftNow = GetTelesaleShift(DateTime.Now);
                                    //Nếu không có ca làm việc => bỏ qua
                                    if (telesaleShiftNow == 0)
                                        continue;
                                    var boundTelesaleId = 0;
                                    bool distributing = false;
                                    //Nếu đơn từ AI -> chia luôn cho telesale
                                    if (item.PlatformType == EnumPlatformType.Ai.GetHashCode())
                                        distributing = true;
                                    int LogCiscoPushToTelesaleId = 0;
                                    if (!distributing)
                                    {
                                        //Lẩy ra ca tạo đơn
                                        //Nếu đơn tạo vào ca tối => kiểm tra 
                                        var shiftCreateLoan = 0;
                                        if (item.CreatedTime.HasValue)
                                            shiftCreateLoan = GetTelesaleShift(item.CreatedTime.Value.DateTime);

                                        //Nếu gọi smartdailer không được -> chia cho teleslae
                                        if ((_baseConfig["AppSettings:IsCheckLogLogCiscoPushToTelesale"] != null
                                            && _baseConfig["AppSettings:IsCheckLogLogCiscoPushToTelesale"].Equals("1"))
                                            || (_baseConfig["AppSettings:CHECK_AUTOCALL_SHIFT"] != null
                                                    && int.Parse(_baseConfig["AppSettings:CHECK_AUTOCALL_SHIFT"]) > 0
                                                    && shiftCreateLoan == int.Parse(_baseConfig["AppSettings:CHECK_AUTOCALL_SHIFT"]))
                                            //Nếu đơn đó được tạo vào ca tối => kiểm tra log push cisco
                                            )
                                        {
                                            //Kiểm tra TH forword call 
                                            if (_baseConfig["AppSettings:CHECK_AUTOCALL_SHIFT"] != null
                                                    && int.Parse(_baseConfig["AppSettings:CHECK_AUTOCALL_SHIFT"]) > 0
                                                    && shiftCreateLoan == int.Parse(_baseConfig["AppSettings:CHECK_AUTOCALL_SHIFT"])
                                            )
                                            {
                                                var logResultAutocall = _logResultAutoCallServices.GetResult(item.LoanBriefId);
                                                if (logResultAutocall != null && !string.IsNullOrEmpty(logResultAutocall.PhoneAgent))
                                                {
                                                    var userTelesale = _userServices.GetTelesaleByPhone(logResultAutocall.PhoneAgent);
                                                    if (userTelesale != null && userTelesale.UserId > 0)
                                                    {
                                                        distributing = true;
                                                        boundTelesaleId = userTelesale.UserId;//gán cho chính tls đó nhận đơn
                                                    }
                                                }
                                                if (!distributing)
                                                {
                                                    //TH k có log autocall 
                                                    //từ lúc tạo đến hiện tại quá 15p
                                                    //quá 15p mặc định là chia đơn luôn
                                                    if (item.CreatedTime.HasValue && (DateTime.Now - item.CreatedTime.Value).TotalMinutes > 15)
                                                        distributing = true;
                                                }
                                            }
                                            if (!distributing)
                                            {
                                                var logPushSmartDailer = _logCiscoPushToTelesaleService.Get(item.LoanBriefId);
                                                if (logPushSmartDailer != null && logPushSmartDailer.LogCiscoPushToTelesaleId > 0)
                                                {
                                                    LogCiscoPushToTelesaleId = logPushSmartDailer.LogCiscoPushToTelesaleId;
                                                    distributing = true;
                                                }
                                            }

                                        }
                                        else
                                        {
                                            distributing = true;
                                        }
                                    }
                                    //chuyển giỏ cho telesale
                                    //Trong giờ hành chính thì chuyển về giỏ cho telesale
                                    if (distributing)
                                    {
                                        if (item.ProvinceId > 0)
                                        {
                                            if (telesaleShiftNow > 0)
                                            {
                                                var telesaleTeam = -1;
                                                if (item.CreateBy > 0)
                                                {
                                                    var teamTelesaleId = _userServices.GetTeamManager(item.CreateBy.Value);
                                                    if (teamTelesaleId > 0)
                                                    {
                                                        //Nếu là đơn remarketing do teamlead tạo => chia đều cho các team
                                                        if (item.PlatformType != (int)EnumPlatformType.ReMarketing)
                                                            telesaleTeam = teamTelesaleId;
                                                    }
                                                }
                                                var telesale = GetTelesales(boundTelesaleId, item.ProductId.Value, telesaleShiftNow,
                                                    telesaleTeam);
                                                if (telesale != null && telesale.UserId > 0)
                                                    PushToTelesale(item, telesale, false, LogCiscoPushToTelesaleId);
                                                else
                                                    Console.WriteLine(string.Format("PushToTelesale: khong có telesale trong team {2} HĐ-{0} {1}", item.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), telesaleTeam));
                                            }
                                            else
                                            {
                                                Console.WriteLine(string.Format("PushToTelesale: KHONG TIM DUOC CA LAM VIEC HĐ-{0} {1}", item.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                            }

                                        }
                                    }
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                            }
                        }
                        Thread.Sleep(3000);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                    Log.Error(ex, "LOS.ThirdPartyProccesor PushLoanCreditToTelesales exception");
                }
                //Thread 
                Thread.Sleep(3000);
            }
        }

        private int GetTelesaleShift(DateTime timeCheck)
        {

            var listData = _userServices.ListTelesaleShift();
            if (listData != null && listData.Count > 0)
            {
                foreach (var item in listData)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(item.StartTime) && !string.IsNullOrEmpty(item.EndTime))
                        {
                            var now = DateTime.Now;
                            var arrStart = item.StartTime.Split(':');
                            var arrEnd = item.EndTime.Split(':');
                            var start = new DateTime(now.Year, now.Month, now.Day, Int32.Parse(arrStart[0]), Int32.Parse(arrStart[1]), 0);
                            var end = new DateTime(now.Year, now.Month, now.Day, Int32.Parse(arrEnd[0]), Int32.Parse(arrEnd[1]), 0);
                            if (timeCheck >= start && timeCheck <= end)
                                return item.TelesalesShiftId;
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            return 0;
        }
        private void PushToTelesale(LoanbriefDistributingTelelsale entity, User telesale, bool pushPipeline = true, int logCiscoPushToTelesaleId = 0)
        {
            try
            {
                //Kiểm tra xem trước đó đã nhận line chưa
                var last = _loanBriefServices.GetLastLogDistributionUser(entity.LoanBriefId, (int)TypeDistributionLog.TELESALE);
                //TH mà tls đã nhận đơn line
                if (last != null && last.CreatedBy > 0 && last.UserId > 0
                    && last.CreatedBy == last.UserId)
                {
                    var user = _userServices.GetTelesale(last.UserId.Value);
                    if (user != null && user.TeamTelesalesId > 0)
                    {
                        _loanBriefServices.DistributingTelesale(entity.LoanBriefId, user.UserId, user.TeamTelesalesId);
                        return;
                    }
                }
                var loanbrief = _loanBriefServices.GetById(entity.LoanBriefId);
                if (loanbrief == null || loanbrief.LoanBriefId == 0)
                    return;

                if (loanbrief.BoundTelesaleId == telesale.UserId
                    || _loanBriefServices.DistributingTelesale(entity.LoanBriefId, telesale.UserId, telesale.TeamTelesalesId))
                {
                    if (loanbrief.BoundTelesaleId == telesale.UserId && loanbrief.TeamTelesalesId.GetValueOrDefault(0) == 0)
                    {
                        _loanBriefServices.DistributingTelesale(entity.LoanBriefId, telesale.UserId, telesale.TeamTelesalesId);
                    }
                    if (pushPipeline)
                    {
                        //đẩy đơn lên luồng xử lý
                        if (_loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Push.GetHashCode()))
                        {
                            //var taskRun = new List<Task>();
                            var teamName = string.Empty;
                            if (telesale.TeamTelesalesId > 0)
                            {
                                var team = _userServices.GetTeamTelesale(telesale.TeamTelesalesId.Value);
                                if (team != null && !string.IsNullOrEmpty(team.Name))
                                    teamName = team.Name;
                            }
                            //lưu lại comment
                            _loanBriefServices.AddNote(new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = string.Format("System: Hệ thống đã chia đơn về cho telesales {0}({1}) Team {2}", telesale.FullName, telesale.Username, teamName),
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.DistributingLoanOfTelesale.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            });
                            //cập nhật thời gian nhận đơn cuối cùng của telesales
                            _userServices.UpdateLastReceivedTelesale(telesale.UserId);
                            //Thêm log chia đơn cho tls
                            _logDistributionUserService.Insert(new LogDistributionUser()
                            {
                                UserId = telesale.UserId,
                                LoanbriefId = entity.LoanBriefId,
                                CreatedAt = DateTime.Now,
                                TypeDistribution = (int)TypeDistributionLog.TELESALE
                            });
                            if (logCiscoPushToTelesaleId > 0)
                                _logCiscoPushToTelesaleService.UpdatePushTelesale(logCiscoPushToTelesaleId);

                            Console.WriteLine(string.Format("PushToTelesale: Xu ly don HĐ-{0} {1} {2}", entity.LoanBriefId, telesale.UserId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        }
                    }
                    else
                    {
                        var teamName = string.Empty;
                        if (telesale.TeamTelesalesId > 0)
                        {
                            var team = _userServices.GetTeamTelesale(telesale.TeamTelesalesId.Value);
                            if (team != null && !string.IsNullOrEmpty(team.Name))
                                teamName = team.Name;
                        }
                        //lưu lại comment
                        _loanBriefServices.AddNote(new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = string.Format("System: Hệ thống đã chia đơn về cho telesales {0}({1}) Team {2}", telesale.FullName, telesale.Username, teamName),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.DistributingLoanOfTelesale.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        //Thêm log chia đơn cho tls
                        _logDistributionUserService.Insert(new LogDistributionUser()
                        {
                            UserId = telesale.UserId,
                            LoanbriefId = entity.LoanBriefId,
                            CreatedAt = DateTime.Now,
                            TypeDistribution = (int)TypeDistributionLog.TELESALE
                        });

                        if (logCiscoPushToTelesaleId > 0)
                            _logCiscoPushToTelesaleService.UpdatePushTelesale(logCiscoPushToTelesaleId);
                        //cập nhật thời gian nhận đơn cuối cùng của telesales
                        _userServices.UpdateLastReceivedTelesale(telesale.UserId);
                        Console.WriteLine(string.Format("PushToTelesale_Change : Xu ly don HĐ-{0} {1} {2}", entity.LoanBriefId, telesale.UserId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor PushToTelesale exception");
            }
        }


        private User GetTelesales(int? boundTelesaleId, int? productId, int? telesaleShift = -1, int? telesaleTeamId = -1)
        {
            //Lấy telesale nhận đơn cuối cùng
            try
            {
                //nếu đã có telesale => lấy thông tin tele đó
                //Update 11/10/2020 Chị Hoàn Request đơn về chia lần lượt cho telesale k phân biệt tỉnh
                if (boundTelesaleId > 0)
                    return _userServices.GetTelesale(boundTelesaleId.Value);
                else
                    return _userServices.GetLastUserTelesales(productId.Value, -1, telesaleShift, telesaleTeamId);
            }
            catch
            {
            }
            return null;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
