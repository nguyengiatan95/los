﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoDistributingTelesaleCTVJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IUserService _userServices;
        private readonly ILogDistributionUserService _logDistributionUserService;
        //private Timer _timer;
        private Thread _thread;
        protected IConfiguration _baseConfig;
        private Dictionary<int, User> _dicUser = new Dictionary<int, User>();

        public AutoDistributingTelesaleCTVJob(ILoanBriefService loanBriefServices, IUserService userServices,
            ILogDistributionUserService logDistributionUserService)
        {
            _loanBriefServices = loanBriefServices;
            _userServices = userServices;
            _logDistributionUserService = logDistributionUserService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        private void InitData()
        {
            //lấy ra danh sách user 
            var listUser = _userServices.ListUser(new List<int>() { (int)EnumUser.ctv_autocall, (int)EnumUser.autocall_coldlead, (int)EnumUser.recare });
            if (listUser != null && listUser.Count > 0)
            {
                foreach(var item in listUser)
                {
                    if (!_dicUser.ContainsKey(item.UserId))
                        _dicUser[item.UserId] = item;
                }
            }
        }

        //Chia đơn CTV
        private void DoWork(object state)
        {
            InitData();
            while (true)
            {
                try
                {
                    //lấy ra các đơn của CTV cần xử lý
                    var loanCredits = _loanBriefServices.WaitPushToTelesaleCTV();
                    if (loanCredits != null && loanCredits.Count > 0)
                    {
                      
                        foreach (var item in loanCredits)
                        {
                            try
                            {
                                if (item.BoundTelesaleId == (int)EnumUser.ctv_autocall
                                    || item.BoundTelesaleId == (int)EnumUser.autocall_coldlead
                                    || item.BoundTelesaleId == (int)EnumUser.recare)
                                {
                                    var telesale = new User();
                                    //lấy thông tin user
                                    if (item.BoundTelesaleId > 0 && _dicUser.Keys.Count > 0 && _dicUser.ContainsKey(item.BoundTelesaleId.Value))
                                        telesale = _dicUser[item.BoundTelesaleId.Value];
                                    else
                                        telesale = GetTelesales(item.BoundTelesaleId, item.ProductId);
                                   
                                    if (telesale != null && telesale.UserId > 0)
                                    {
                                        PushToTelesale(item, telesale);
                                    }
                                    else
                                    {
                                        Console.WriteLine(string.Format("PushToTelesale: khong co cau hinh don HĐ-{0} {1}", item.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                            }
                        }
                        Thread.Sleep(1000 * 3);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                    Log.Error(ex, "LOS.ThirdPartyProccesor PushLoanCreditToTelesales exception");
                }
                Thread.Sleep(1000 * 3);
            }
        }
        private void PushToTelesale(LoanbriefDistributingTelelsale entity, User telesale, bool pushPipeline = true)
        {
            try
            {
                if (_loanBriefServices.DistributingTelesale(entity.LoanBriefId, telesale.UserId, telesale.TeamTelesalesId))
                {
                    if (pushPipeline)
                    {
                        //đẩy đơn lên luồng xử lý
                        if (_loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Push.GetHashCode()))
                        {
                            _loanBriefServices.AddNote(new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = string.Format("System: Hệ thống đã chia đơn về cho telesales {0}({1})", telesale.FullName, telesale.Username),
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.DistributingLoanOfTelesale.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            });
                            //cập nhật thời gian nhận đơn cuối cùng của telesales
                           _userServices.UpdateLastReceivedTelesale(telesale.UserId);
                            //Thêm log chia đơn cho tls
                            _logDistributionUserService.Insert(new LogDistributionUser()
                            {
                                UserId = telesale.UserId,
                                LoanbriefId = entity.LoanBriefId,
                                CreatedAt = DateTime.Now,
                                TypeDistribution = (int)TypeDistributionLog.TELESALE
                            });
                            Console.WriteLine(string.Format("PushToTelesale CTV: Xu ly don HĐ-{0} {1} {2}", entity.LoanBriefId, telesale.UserId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        }
                    }
                    else
                    {
                        _loanBriefServices.AddNote(new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = string.Format("System: Hệ thống đã chia đơn về cho telesales {0}({1})", telesale.FullName, telesale.Username),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.DistributingLoanOfTelesale.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        //Thêm log chia đơn cho tls
                        _logDistributionUserService.Insert(new LogDistributionUser()
                        {
                            UserId = telesale.UserId,
                            LoanbriefId = entity.LoanBriefId,
                            CreatedAt = DateTime.Now,
                            TypeDistribution = (int)TypeDistributionLog.TELESALE
                        });
                        //cập nhật thời gian nhận đơn cuối cùng của telesales
                        _userServices.UpdateLastReceivedTelesale(telesale.UserId);
                        Console.WriteLine(string.Format("PushToTelesale_Change CTV : Xu ly don HĐ-{0} {1} {2}", entity.LoanBriefId, telesale.UserId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor PushToTelesale exception");
            }
        }
        private User GetTelesales(int? boundTelesaleId, int? productId, int? telesaleShift = -1, int? telesaleTeamId = -1)
        {
            //Lấy telesale nhận đơn cuối cùng
            try
            {
                if (boundTelesaleId > 0)
                {
                    return _userServices.GetTelesale(boundTelesaleId.Value);
                }
                else
                {
                    return _userServices.GetLastUserTelesales(productId.Value, -1, telesaleShift, telesaleTeamId);
                }
            }
            catch
            {
            }
            return null;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
