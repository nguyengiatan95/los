﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoDistributingTelesaleJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IUserService _userServices;
        private readonly ILogCiscoPushToTelesaleService _logCiscoPushToTelesaleService;
        private readonly ILogDistributionUserService _logDistributionUserService;
        private Thread _thread;
        protected IConfiguration _baseConfig;
        private int indexRecieved = 0;
        public AutoDistributingTelesaleJob(ILoanBriefService loanBriefServices, IUserService userServices,
            ILogCiscoPushToTelesaleService logCiscoPushToTelesaleService, ILogDistributionUserService logDistributionUserService)
        {
            _loanBriefServices = loanBriefServices;
            _userServices = userServices;
            _logCiscoPushToTelesaleService = logCiscoPushToTelesaleService;
            _logDistributionUserService = logDistributionUserService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        /// <summary>
        /// Chia đơn từ chờ chia cho tls sang chia đơn cho tls
        /// </summary>
        /// <param name="state"></param>
        private void DoWork(object state)
        {

            while (true)
            {
                try
                {
                    //lấy ra d/s đơn chờ chia đơn cho CHT và đơn ở giỏ spt_system(26695) 
                    var loanCredits = _loanBriefServices.WaitDistributingToTelesale();
                    if (loanCredits != null && loanCredits.Count > 0)
                    {
                        foreach (var item in loanCredits)
                        {
                            try
                            {
                                #region Chờ chia đơn cho TLS
                                if (item.Status == (int)EnumLoanStatus.SYSTEM_TELESALE_DISTRIBUTING)
                                {
                                    //Nếu đã có telsale => đẩy lên chờ tư vấn
                                    //TH đơn do TLS tự tạo
                                    if (item.BoundTelesaleId > 0)
                                    {
                                        //đẩy đơn lên bước tiếp theo
                                        var telesale = GetTelesales(item.BoundTelesaleId, item.ProductId);
                                        if (telesale != null && telesale.UserId > 0)
                                            PushToTelesale(item, telesale);
                                        else
                                            Console.WriteLine(string.Format("PushToTelesale: khong co cau hinh don HĐ-{0} {1}", item.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                    }
                                    else
                                    {
                                        var telesaleShiftNow = GetTelesaleShift(DateTime.Now);
                                        if (item.ProvinceId.GetValueOrDefault(0) == 0)
                                            continue;
                                        //kiểm tra có phải đơn tự tạo không
                                        var boundTelesaleId = 0;
                                        var telesaleTeam = -1;
                                        var isDistributing = false;
                                        if (item.CreateBy > 0)
                                        {
                                            var teamTelesale = _userServices.GetTeamManager(item.CreateBy.Value);
                                            if (teamTelesale > 0)
                                                telesaleTeam = teamTelesale;
                                            else
                                                isDistributing = true; // k thuộc team nào thì gán chia đơn 
                                        }
                                        else
                                        {
                                            //nếu đơn của AI tạo + trong giờ hành chính
                                            if (item.PlatformType == EnumPlatformType.Ai.GetHashCode())
                                            {
                                                //Nếu ngoài giờ hành chính => gán cho system
                                                if (telesaleShiftNow == 0)
                                                    boundTelesaleId = (int)EnumUser.spt_System;
                                                else
                                                    isDistributing = true;
                                            }
                                            else
                                            {
                                                //Nếu đơn từ mkt => gán về giỏ system
                                                boundTelesaleId = (int)EnumUser.spt_System;
                                            }
                                        }

                                        //Nếu là đơn do hub tạo hoặc do telesale tạo hoặc do manager telesale tạo
                                        //Chia đơn về cho team tls hoặc tls đó
                                        if (boundTelesaleId > 0 || telesaleTeam > 0 || isDistributing)
                                        {
                                            if (isDistributing)
                                                telesaleTeam = -1;
                                            //Lấy ra thông tin user telesale
                                            var telesale = GetTelesales(boundTelesaleId > 0 ? boundTelesaleId : item.BoundTelesaleId,
                                                item.ProductId.Value, telesaleShiftNow, telesaleTeam);
                                            if (telesale != null && telesale.UserId > 0)
                                                PushToTelesale(item, telesale);
                                            else
                                                Console.WriteLine(string.Format("PushToTelesale: khong co cau hinh don HĐ-{0} {1}", item.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                        }
                                    }
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                            }
                        }
                        Thread.Sleep(3000);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                    Log.Error(ex, "LOS.ThirdPartyProccesor PushLoanCreditToTelesales exception");
                }
                //Thread 
                Thread.Sleep(3000);
            }
        }
        private int GetTelesaleShift(DateTime timeCheck)
        {

            var listData = _userServices.ListTelesaleShift();
            if (listData != null && listData.Count > 0)
            {
                foreach (var item in listData)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(item.StartTime) && !string.IsNullOrEmpty(item.EndTime))
                        {
                            var now = DateTime.Now;
                            var arrStart = item.StartTime.Split(':');
                            var arrEnd = item.EndTime.Split(':');
                            var start = new DateTime(now.Year, now.Month, now.Day, Int32.Parse(arrStart[0]), Int32.Parse(arrStart[1]), 0);
                            var end = new DateTime(now.Year, now.Month, now.Day, Int32.Parse(arrEnd[0]), Int32.Parse(arrEnd[1]), 0);
                            if (timeCheck >= start && timeCheck <= end)
                                return item.TelesalesShiftId;
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            return 0;
        }
        private void PushToTelesale(LoanbriefDistributingTelelsale entity, User telesale, bool pushPipeline = true, int logCiscoPushToTelesaleId = 0)
        {
            try
            {
                //Kiểm tra xem trước đó đã nhận line chưa
                var last = _loanBriefServices.GetLastLogDistributionUser(entity.LoanBriefId, (int)TypeDistributionLog.TELESALE);
                //TH mà tls đã nhận đơn line => Nhận line thì gán luôn cho tls đó
                if (last != null && last.CreatedBy > 0 && last.UserId > 0
                    && last.CreatedBy == last.UserId)
                {
                    var user = _userServices.GetTelesale(last.UserId.Value);
                    if (user != null && user.TeamTelesalesId > 0)
                    {
                        _loanBriefServices.DistributingTelesale(entity.LoanBriefId, user.UserId, user.TeamTelesalesId);
                        return;
                    }
                }
                var loanbrief = _loanBriefServices.GetById(entity.LoanBriefId);
                if (loanbrief == null || loanbrief.LoanBriefId == 0)
                    return;

                if (loanbrief.BoundTelesaleId == telesale.UserId
                    || _loanBriefServices.DistributingTelesale(entity.LoanBriefId, telesale.UserId, telesale.TeamTelesalesId))
                {
                    if (loanbrief.BoundTelesaleId == telesale.UserId && loanbrief.TeamTelesalesId.GetValueOrDefault(0) == 0)
                    {
                        _loanBriefServices.DistributingTelesale(entity.LoanBriefId, telesale.UserId, telesale.TeamTelesalesId);
                    }
                    if (pushPipeline)
                    {
                        //đẩy đơn lên luồng xử lý
                        if (_loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Push.GetHashCode()))
                        {
                            //lưu lại comment
                            _loanBriefServices.AddNote(new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = string.Format("System: Hệ thống đã chia đơn về cho telesales {0}({1})", telesale.FullName, telesale.Username),
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.DistributingLoanOfTelesale.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            });

                            //cập nhật thời gian nhận đơn cuối cùng của telesales
                            _userServices.UpdateLastReceivedTelesale(telesale.UserId);
                            //Thêm log chia đơn cho tls
                            _logDistributionUserService.Insert(new LogDistributionUser()
                            {
                                UserId = telesale.UserId,
                                LoanbriefId = entity.LoanBriefId,
                                CreatedAt = DateTime.Now,
                                TypeDistribution = (int)TypeDistributionLog.TELESALE
                            });
                          
                            if (logCiscoPushToTelesaleId > 0)
                                _logCiscoPushToTelesaleService.UpdatePushTelesale(logCiscoPushToTelesaleId);

                            Console.WriteLine(string.Format("PushToTelesale: Xu ly don HĐ-{0} {1} {2}", entity.LoanBriefId, telesale.UserId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        }
                    }
                    else
                    {
                        _loanBriefServices.AddNote(new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = string.Format("System: Hệ thống đã chia đơn về cho telesales {0}({1})", telesale.FullName, telesale.Username),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.DistributingLoanOfTelesale.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        //Thêm log chia đơn cho tls
                        _logDistributionUserService.Insert(new LogDistributionUser()
                        {
                            UserId = telesale.UserId,
                            LoanbriefId = entity.LoanBriefId,
                            CreatedAt = DateTime.Now,
                            TypeDistribution = (int)TypeDistributionLog.TELESALE
                        });

                        if (logCiscoPushToTelesaleId > 0)
                            _logCiscoPushToTelesaleService.UpdatePushTelesale(logCiscoPushToTelesaleId);
                        //cập nhật thời gian nhận đơn cuối cùng của telesales
                        _userServices.UpdateLastReceivedTelesale(telesale.UserId);
                        Console.WriteLine(string.Format("PushToTelesale_Change : Xu ly don HĐ-{0} {1} {2}", entity.LoanBriefId, telesale.UserId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor PushToTelesale exception");
            }
        }
        private User GetTelesales(int? boundTelesaleId, int? productId, int? telesaleShift = -1, int? telesaleTeamId = -1)
        {
            try
            {
                //nếu đã có telesale => lấy thông tin tele đó
                if (boundTelesaleId > 0)
                    return _userServices.GetTelesale(boundTelesaleId.Value);
                else
                    return _userServices.GetLastUserTelesales(productId.Value, -1, telesaleShift, telesaleTeamId);
            }
            catch
            {
            }
            return null;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
