﻿using LOS.DAL.Dapper;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static LOS.ThirdPartyProccesor.Objects.Cisco;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoGetRecordingCiscoJob : IHostedService, IDisposable
    {
        private readonly ICiscoService _ciscoService;
        protected IConfiguration _baseConfig;
        private string startTime = "";
        private Thread _thread;
        private Thread _threadUpdateCallDuration;
        private string environmentName = "";
        private string CSRFTOKEN = "";
        private string JSESSIONID = "";
        public AutoGetRecordingCiscoJob(ICiscoService ciscoService, IConfiguration baseConfig)
        {
            _ciscoService = ciscoService;
            environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            if (environmentName != "Development")
            {
                _thread = new Thread(() => DoWork(null));
                _thread.Start();
                _threadUpdateCallDuration = new Thread(() => DoWorkUpdateCallDuration(null));
                _threadUpdateCallDuration.Start();
            }
            return Task.CompletedTask;

        }

        private void DoWork(object state)
        {
            while (true)
            {
                try
                {
                    AuthenCisco(true);
                    if (!string.IsNullOrEmpty(CSRFTOKEN) && !string.IsNullOrEmpty(JSESSIONID))
                    {
                        var lastCall = GetLastRecored();
                        if (lastCall != null)
                            startTime = lastCall.Value.ToString("yyyy-MM-dd HH:mm");
                        else
                            startTime = DateTime.Now.AddMinutes(-15).ToString("yyyy-MM-dd HH:mm");
                        var endTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
                        int logCallApiId = 0;
                        var dataRecording = _ciscoService.GetListByTime(startTime, endTime, CSRFTOKEN, JSESSIONID, ref logCallApiId);
                        if (dataRecording != null && dataRecording.Count > 0)
                        {
                            var data = dataRecording.OrderBy(x => x.StartTime).ToList();
                            InsertInBulk(data, logCallApiId);
                            //sleep 5'
                            Thread.Sleep(1000 * 60 * 5);
                        }
                    }

                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor AutoGetRecordingCiscoJob exception");
                    //Thread 1 minute
                    Thread.Sleep(1000 * 30);
                }
            }
        }

        private void AuthenCisco(bool isChange)
        {
            try
            {
                if (string.IsNullOrEmpty(CSRFTOKEN) || string.IsNullOrEmpty(JSESSIONID) || isChange)
                {
                    var authen = _ciscoService.Authorize();
                    if (authen != null && authen.Count > 0)
                    {
                        CSRFTOKEN = authen[0].CSRFTOKEN;
                        JSESSIONID = authen[0].JSESSIONID;
                    }
                }
            }
            catch
            {

            }
        }

        private void DoWorkUpdateCallDuration(object state)
        {
            while (true)
            {
                try
                {
                    var data = GetCallNonFinish();
                    if (data != null && data.Count > 0)
                    {
                        if (string.IsNullOrEmpty(CSRFTOKEN) || string.IsNullOrEmpty(JSESSIONID))
                        {
                            AuthenCisco(false);
                        }
                        if (!string.IsNullOrEmpty(CSRFTOKEN) && !string.IsNullOrEmpty(JSESSIONID))
                        {
                            foreach (var item in data)
                            {
                                var phone = item.PhoneNumber;
                                if (string.IsNullOrEmpty(phone))
                                {
                                    phone = item.Dnis;
                                    if (phone.Length < 10)
                                        phone = item.Ani;
                                    if (phone.Length == 13)
                                        phone = phone.Substring(3);
                                }

                                if (!string.IsNullOrEmpty(item.RecordingUrl))
                                {
                                    int callDuration = 0;
                                    var urlPlay = UrlRecording(item.RecordingUrl, ref callDuration);
                                    if (!string.IsNullOrEmpty(urlPlay) && callDuration > 0)
                                    {
                                        //cập nhật vào db
                                        UpdateRecording(item.Id, callDuration, phone, urlPlay);
                                    }
                                }
                            }
                        }
                           
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor DoWorkUpdateCallDuration exception");
                    //Thread 1 minute
                    Thread.Sleep(1000 * 30 * 2);
                }
            }
        }


        public void InsertInBulk(List<GetList> listRecording, int logCallApiId)
        {
            foreach (var item in listRecording)
            {
                var sql = "";
                try
                {
                    //Kiểm tra đã có cuộc chưa
                    if (!string.IsNullOrEmpty(item.IcmCallId))
                    {
                        var recording = GetRecording(item.IcmCallId);
                        if(recording == null || recording.Id == 0)
                        {
                            sql = GetSqlsInBatches(item, logCallApiId);
                            if (!string.IsNullOrEmpty(sql))
                            {
                                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSReport"));
                                db.Execute(sql);
                            }
                        }
                    }
                    
                }
                catch (Exception ex)
                {

                }

            }
        }

        private string GetSqlsInBatches(List<GetList> listRecording, int logCallApiId)
        {
            var insertSql = "INSERT INTO [LogRecording] (StartTime, CallDuration,Dnis, Ani, Line,RecordingType, IcmCallId, AssocCallId, ContactType, AgentUsername, RecordingUrl, CreatedAt) VALUES ";
            var sqlsToExecute = "";
            var valuesToInsert = new List<string>();
            foreach (var item in listRecording)
            {
                var sql = ConvertToValueInsert(item, logCallApiId);
                if (!string.IsNullOrEmpty(sql))
                    valuesToInsert.Add(sql);
            }
            sqlsToExecute = insertSql + string.Join(',', valuesToInsert);
            return sqlsToExecute;
        }

        public DateTime? GetLastRecored()
        {
            try
            {

                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSReport"));
                return db.QueryFirstOrDefault<DateTime>("select max(StartTime) from  [LogRecording] where ContactType = 'CALL'");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private string GetSqlsInBatches(GetList recording, int logCallApiId)
        {
            var insertSql = "INSERT INTO [LogRecording] (StartTime, CallDuration,Dnis, Ani, Line,RecordingType, IcmCallId, AssocCallId, ContactType, AgentUsername, RecordingUrl, CreatedAt, PhoneNumber, PlayUrl, LogCallApiId) VALUES ";
            return insertSql + ConvertToValueInsert(recording, logCallApiId);
        }

        public string ConvertToValueInsert(GetList item, int logCallApiId)
        {
            try
            {
                var phone = item.Dnis;
                if (phone.Length < 10)
                    phone = item.Ani;
                if (phone.Length == 13)
                    phone = phone.Substring(3);

                var playUrl = string.Empty;
                int duration = 0;
                if (!string.IsNullOrEmpty(item.RecordingUrl))
                    playUrl = UrlRecording(item.RecordingUrl, ref duration);

                if (duration == 0)
                    duration = item.CallDuration ?? 0;
                if (item.Agent != null)
                    return $"('{LOS.Common.Extensions.ConvertExtensions.UnixTimeStampToDateTime(item.StartTime)}', '{duration}', '{item.Dnis}', '{item.Ani}', '{item.Line}'," +
                           $"'{item.RecordingType}','{item.IcmCallId}','{item.AssocCallId}','{item.ContactType}','{item.Agent.Username}','{item.RecordingUrl}', GETDATE(), '{phone}', '{playUrl}', {logCallApiId} )";
                return $"('{LOS.Common.Extensions.ConvertExtensions.UnixTimeStampToDateTime(item.StartTime)}', '{duration}', '{item.Dnis}', {item.Ani}, {item.Line}," +
                    $"'{item.RecordingType}','{item.IcmCallId}','{item.AssocCallId}','{item.ContactType}','','{item.RecordingUrl}', GETDATE() , '{phone}', '{playUrl}', {logCallApiId} )";
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string UrlRecording(string recordingUrl, ref int duration)
        {
            try
            {
                var data = _ciscoService.GetPlayUrl(recordingUrl, CSRFTOKEN, JSESSIONID);
                if (data != null && data.wav != null && !string.IsNullOrEmpty(data.wav.playUrl))
                {
                    if (data.wav.duration > 0)
                        duration = data.wav.duration;
                    return data.wav.playUrl.Replace("//", "http:");
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            return string.Empty;
        }

        public List<LogRecording> GetCallNonFinish()
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSReport"));
                return db.Query<LogRecording>("select Id, Dnis, Ani, RecordingUrl,PhoneNumber  from   LogRecording where  ISNULL(CallDuration, 0) = 0 And ContactType = 'CALL' ");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public LogRecording GetRecording(string icmCallId)
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSReport"));
                return db.QueryFirstOrDefault<LogRecording>($"select Id, Dnis, Ani from LogRecording where IcmCallId = '{icmCallId}'");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void UpdateRecording(int id, int callDuration, string phoneNumber, string playUrl)
        {
            try
            {
                var sql = new StringBuilder();
                sql.AppendLine("update LogRecording ");
                sql.AppendLine($"set PhoneNumber = '{phoneNumber}', ");
                sql.AppendLine($"CallDuration = '{callDuration}', ");
                sql.AppendLine($"PlayUrl = '{playUrl}' ");
                sql.AppendLine($"where Id = {id} ");
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSReport"));
                db.Execute(sql.ToString());
            }
            catch (Exception ex)
            {
            }
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            //_timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            //_timer?.Dispose();
            _thread?.Abort();
        }
    }
}
