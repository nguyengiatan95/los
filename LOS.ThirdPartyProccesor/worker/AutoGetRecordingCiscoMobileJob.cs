﻿using LOS.DAL.Dapper;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Objects.Setttings;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static LOS.ThirdPartyProccesor.Objects.Cisco;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoGetRecordingCiscoMobileJob : IHostedService, IDisposable
    {
        private readonly ICiscoService _ciscoService;
        private readonly ILogClickToCall _logClickToCall;
        protected IConfiguration _baseConfig;
        private Thread _thread;
        private string environmentName = "";
        private readonly AppCiscoMobileSettings _appCiscoMobileSettings;
        public AutoGetRecordingCiscoMobileJob(ICiscoService ciscoService, ILogClickToCall logClickToCall, IOptions<AppCiscoMobileSettings> appCiscoMobileSettingsAccessor)
        {
            _ciscoService = ciscoService;
            environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
            _logClickToCall = logClickToCall;
            _appCiscoMobileSettings = appCiscoMobileSettingsAccessor.Value;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;

        }

        private void DoWork(object state)
        {
            while (true)
             {
                Console.WriteLine(string.Format("Run AutoGetRecordingCiscoMobileJob {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                try
                {
                    var data = _logClickToCall.GetLogClickToCallMobile();
                    if (data != null && data.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            try
                            {
                                var listRecording = _ciscoService.GetMobileRecording(item.CallId);
                                if (listRecording != null && listRecording.Count > 0)
                                {
                                    InsertInBulk(listRecording, item.LoanbriefId.GetValueOrDefault(0));
                                }
                                _logClickToCall.DoneGetRecording(item.Id);
                            }
                            catch(Exception ex)
                            {

                            }                           
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor AutoGetRecordingCiscoMobileJob exception");
                    //Thread 5 minute
                    Thread.Sleep(1000 * 30 * 5);
                }
                //ngủ 15p
                Thread.Sleep(450000);
            }
        }

        public void InsertInBulk(List<MobileRecordData> listRecording, int loanbriefId)
        {
           var sql = GetSqlsInBatches(listRecording, loanbriefId);
            if (!string.IsNullOrEmpty(sql))
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSReport"));
                db.Execute(sql);
            }
        }

        private string GetSqlsInBatches(List<MobileRecordData> listRecording, int loanbriefId)
        {
            var insertSql = "INSERT INTO [LogRecording](StartTime, CallDuration, Line, ContactType, RecordingUrl, CreatedAt, PhoneNumber, PlayUrl, AssocCallId, loanbriefid ) VALUES  ";
            var sqlsToExecute = "";
            var valuesToInsert = new List<string>();
            foreach (var item in listRecording)
            {
                var sql = ConvertToValueInsert(item, loanbriefId);
                if (!string.IsNullOrEmpty(sql))
                    valuesToInsert.Add(sql);
            }
            sqlsToExecute = insertSql + string.Join(',', valuesToInsert);
            return sqlsToExecute;
        }

        public string ConvertToValueInsert(MobileRecordData item, int loanbriefId)
        {
            try
            {
                var playUrl = _appCiscoMobileSettings.RecordBaseUrl + item.RecordPath;
                double duration = (item.EndCall - item.AnswerCall).TotalMilliseconds;
                return $"('{item.AnswerCall}', '{duration}', '{item.UserId}', 'CALL_MOBILE', '{item.RecordPath}',GETDATE(), '{item.PhoneNumber}','{playUrl}', '{item.CallId}', '{loanbriefId}')";
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        
        public Task StopAsync(CancellationToken cancellationToken)
        {
            //_timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            //_timer?.Dispose();
            _thread?.Abort();
        }
    }
}
