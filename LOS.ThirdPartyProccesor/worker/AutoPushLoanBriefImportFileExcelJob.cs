﻿using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoPushLoanBriefImportFileExcelJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefImportFileExcelServices _loanBriefImportFileExcelServices;
        private readonly IConfiguration _configuration;
        private string environmentName;

        private Thread _thread;
        public AutoPushLoanBriefImportFileExcelJob(ILoanBriefImportFileExcelServices loanBriefImportFileExcelServices)
        {
            _loanBriefImportFileExcelServices = loanBriefImportFileExcelServices;
            environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            this._configuration = builder.Build();
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            while (true)
            {
                //lấy danh sách đơn trong bảng LoanBriefImportFileExcel
                var lstLoanBriefImportFileExcel = _loanBriefImportFileExcelServices.GetLoanBriefImportFileExcelNoPush();
                if (lstLoanBriefImportFileExcel != null && lstLoanBriefImportFileExcel.Count > 0)
                {
                    foreach (var item in lstLoanBriefImportFileExcel)
                    {
                        var result = _loanBriefImportFileExcelServices.CreateLoanImportFileExcel(item);
                        if(result == 1)
                        {
                            Console.WriteLine("Tao don cho khach hang: " + item.FullName + " SDT: " + item.Phone + " thanh cong");
                        }
                        else
                        {
                            Console.WriteLine("Tao don cho khach hang: " + item.FullName + " SDT: " + item.Phone + " thất bại");
                        }
                        continue;
                    }
                }
                Thread.Sleep(10000); // nghỉ giảo lao 10  giây
            }
        }

        public void Dispose()
        {
            _thread?.Abort();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
