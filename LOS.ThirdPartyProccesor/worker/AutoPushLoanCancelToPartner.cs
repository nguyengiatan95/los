﻿using LOS.Common.Extensions;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoPushLoanCancelToPartner : IHostedService, IDisposable
    {
        private readonly IPushLoanToPartnerService _pushLoanToPartnerService;
        private readonly ILoanBriefService _loanBriefServices;

        private Thread _thread;
        private string environmentName = "";
        private List<int> _listDistrictId;

        public AutoPushLoanCancelToPartner(IPushLoanToPartnerService pushLoanToPartnerService, ILoanBriefService loanBriefServices)
        {
            environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            _pushLoanToPartnerService = pushLoanToPartnerService;
            _loanBriefServices = loanBriefServices;
            _listDistrictId = new List<int>()
            {
                (int)EnumDistrict.Ba_Vi,
                (int)EnumDistrict.Phuc_Tho,
                (int)EnumDistrict.Son_Tay,
                (int)EnumDistrict.Dan_Phuong,
                (int)EnumDistrict.Thach_That,
                (int)EnumDistrict.Hoai_Duc,
                (int)EnumDistrict.Quoc_Oai,
                (int)EnumDistrict.Chuong_My,
                (int)EnumDistrict.Thanh_Oai,
                (int)EnumDistrict.Thuong_Tin,
                (int)EnumDistrict.Phu_Xuyen,
                (int)EnumDistrict.Ung_Hoa,
                (int)EnumDistrict.My_Duc,
                (int)EnumDistrict.Di_An
            };
        }
        private void DoWork(object state)
        {
            while (true)
            {
                try
                {
                    var startLoanbriefId = 0;
                    //Lấy ra bản ghi cuối cùng đẩy
                    var lastPush = _pushLoanToPartnerService.GetLastPushLoanCanel();
                    if (lastPush != null && lastPush.LoanbriefId > 0)
                        startLoanbriefId = lastPush.LoanbriefId.Value;
                    if(startLoanbriefId == 0)
                    {
                        if (environmentName == "Production" || environmentName == "Staging")
                            startLoanbriefId = 2652961; //Id lớn nhất ngày 21/12
                        else
                            startLoanbriefId = 1041913;
                    }

                    //Lấy ra danh sách các đơn bị hủy của LOS
                    var listLoanbrief = _loanBriefServices.ListLoanbriefCancel(startLoanbriefId, _listDistrictId);
                    if (listLoanbrief != null && listLoanbrief.Count > 0)
                    {
                        var index = 1;
                        var total = listLoanbrief.Count;
                        foreach (var item in listLoanbrief)
                        {
                            Console.WriteLine(string.Format("AutoPushLoanCancelToPartner {0}/{1}", index, total));
                            //Kiểm tra đã từng tạo sang mirae chưa
                            var lastPushPhone = _pushLoanToPartnerService.GetLastPush(item.CustomerPhone);
                            if (lastPushPhone != null && lastPushPhone.Id > 0)
                                continue;
                            //Kiểm tra xem LOS có đơn đang xử lý hay không
                            var loanbrief = _loanBriefServices.LoanbriefHandleV2(item.CustomerPhone, item.NationalCard);
                            if (loanbrief != null && loanbrief.LoanBriefId > 0)
                                continue;

                            _pushLoanToPartnerService.Add(new DAL.EntityFramework.PushLoanToPartner()
                            {
                                CustomerName = item.CustomerName,
                                CustomerPhone = item.CustomerPhone,
                                ProvinceId = item.ProvinceId,
                                DistrictId = item.DistrictId,
                                ProvinceName = item.ProvinceName,
                                DistrictName = item.DistrictName,
                                UtmSource = item.UtmSource,
                                UtmMedium = item.UtmMedium,
                                UtmCampaign = item.UtmCampaign,
                                UtmContent = item.UtmContent,
                                UtmTerm = item.UtmTerm,
                                Domain = item.Domain,
                                //AffCode = item.AffCode,
                                //AffSid = item.sid,
                                //Tid = item.TId,
                                PushAction = (int)ActionPushLoanToPartner.CreateNew,
                                Status = (int)StatusPushLoanToPartner.Processing,
                                CreateDate = DateTime.Now,
                                LoanbriefId = item.LoanbriefId
                            });
                            index++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                    Log.Error(ex, "LOS.ThirdPartyProccesor AutoPushLoanCancelToPartner exception");
                }
                //Sleep 30 minute
                Thread.Sleep(1800000);
            }
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
