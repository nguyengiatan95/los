﻿using LOS.Common.Extensions;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoPushLoanStatusToPartner : IHostedService, IDisposable
    {
        private IPushLoanStatusToPartnerService _pushService;
        private ITikiService _tikiService;
        private Thread _thread;

        public AutoPushLoanStatusToPartner(IPushLoanStatusToPartnerService pushService, ITikiService tikiService)
        {
            _pushService = pushService;
            _tikiService = tikiService;
        }
        private async void DoWork(object state)
        {
            while (true)
            {                
                try
                {
                    // Đẩy lại các đơn thay đổi trạng thái
                    var logs = _pushService.GetChangedStatusLoan();
                    if (logs != null && logs.Count > 0)
					{
                        foreach (var log in logs)
						{
                            // Update lại trạng thái push                            
                            string status = "";
                            if (log.status == 100 || log.status == 110)
                                status = "disbursed";
                            else
                                status = "rejected";
                            var result = await _tikiService.UpdateLead(log.log.PartnerId, status);
                            if (result)
							{
                                _pushService.UpdateStatus(log.log.PartnerId, log.status);
							}                  
                            else
							{
                                _pushService.IncreaseRetryCount(log.log.PartnerId);
							}                                
						}                            
					}
                    logs = _pushService.GetChangedStatusLoanPartner();
                    if (logs != null && logs.Count > 0)
                    {
                        foreach (var log in logs)
                        {
                            // Update lại trạng thái push                            
                            string status = "";
                            if (log.status == 1)
                                status = "disbursed";
                            else
                                status = "rejected";
                            var result = await _tikiService.UpdateLead(log.log.PartnerId, status);
                            if (result)
                            {
                                _pushService.UpdateStatus(log.log.PartnerId, log.status);
                            }
                            else
                            {
                                _pushService.IncreaseRetryCount(log.log.PartnerId);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                    Log.Error(ex, "LOS.ThirdPartyProccesor AutoPushLoanStatusToPartner exception");
                }
                //Sleep 5 minute
                Thread.Sleep(300000);
            }
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
