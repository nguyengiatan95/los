﻿using LOS.Common.Extensions;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoPushLoanToPartner : IHostedService, IDisposable
    {
        private readonly IPushLoanToPartnerService _pushLoanToPartnerService;
        private readonly IMiraeService _miraeService;

        private Thread _thread;

        public AutoPushLoanToPartner(IPushLoanToPartnerService pushLoanToPartnerService, IMiraeService miraeService)
        {
            _pushLoanToPartnerService = pushLoanToPartnerService;
            _miraeService = miraeService;
        }
        private void DoWork(object state)
        {
            while (true)
            {
                //Console.WriteLine("****AutoPushLoanToPartner****");
                try
                {
                    //Lấy ra danh sách các đơn chưa tạo bên đối tác
                    var lstLoanNewCreate = _pushLoanToPartnerService.GetLoanNewCreate();
                    if (lstLoanNewCreate != null && lstLoanNewCreate.Count > 0)
                    {
                        var index = 1;
                        var total = lstLoanNewCreate.Count;
                        foreach (var item in lstLoanNewCreate)
                        {
                            Console.WriteLine(string.Format("AutoPushLoanToPartner {0}/{1}", index, total));
                            //cập nhập lại bán đơn cho Mirae
                            var updatePartner = _pushLoanToPartnerService.UpdatePartner(item.Id, (int)PartnerPushLoanToPartner.Mirae);
                            if (updatePartner)
                            {
                                //gọi api tạo đơn bên Mirae
                                var objCreateLoanMirae = new RequestCreateLoan()
                                {
                                    phoneNbr = item.CustomerPhone,
                                    currAddress = item.DistrictName + ", " + item.ProvinceName,
                                    priAddress = item.DistrictName + ", " + item.ProvinceName,
                                    income = "20000000",
                                    company = null,
                                    companyAddress = null,
                                    workingTime = null,
                                    nationalId = item.CustomerPhone.Remove(0, 1),
                                    vendorCode = "TIMA",
                                    createDatetime = DateTime.Now,
                                    typeCreate = "API",
                                    campaign = null,
                                    product = null,
                                    email = item.Id.ToString(),
                                    name = item.CustomerName,
                                    importId = null,
                                    age = "1990",
                                    allowQualified = "Y",
                                    loanAmt = "1000000",
                                    tenure = null,
                                    scoreMaketing = null,
                                    scoreIncome = null
                                };
                                var resultCreateLoanMirae = _miraeService.CreateLoan(objCreateLoanMirae, item.Id);
                                if (resultCreateLoanMirae != null)
                                {
                                    //tạo đơn thành công
                                    if (resultCreateLoanMirae.success)
                                    {
                                        //cập nhập lại trường pushAction, pushDate, comment
                                        _pushLoanToPartnerService.UpdateCreateLoanPartner(item.Id, Convert.ToInt32(resultCreateLoanMirae.data), (int)ActionPushLoanToPartner.CreatedSuccess, resultCreateLoanMirae.message, (int)StatusPushLoanToPartner.Processing);
                                        Console.WriteLine(string.Format("AutoPushLoanToPartner SUCCESS: {0} VOI MA HD {1}", item.CustomerName, resultCreateLoanMirae.data));
                                    }
                                    //CMND / Phone đã tồn tại trong Lead Portal, 
                                    //CMND / Phone vi phạm Rule của MAFC
                                    else
                                    {
                                        //cập nhập lại trường pushAction, pushDate, comment
                                        _pushLoanToPartnerService.UpdateCreateLoanPartner(item.Id, 0, (int)ActionPushLoanToPartner.CreatedError, resultCreateLoanMirae.message, (int)StatusPushLoanToPartner.Cancel);
                                        Console.WriteLine(string.Format("AutoPushLoanToPartner ERRROR: {0}, {1} ", item.CustomerName, resultCreateLoanMirae.message));
                                    }

                                }
                                //else
                                //    Console.WriteLine(string.Format("KHONG GOI DUOC API TAO DON BEN MIRAE {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm")));

                            }
                            //else
                            //    Console.WriteLine(string.Format("KHONG CAP NHAP DUOC PARTNER CHO MA ID: {0}", item.Id));
                            index++;
                        }
                    }
                    //else
                    //    Console.WriteLine(string.Format("KHONG CO DON VAY NAO BAN CHO DOI TAC {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm")));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
                    Log.Error(ex, "LOS.ThirdPartyProccesor AutoPushLoanToPartner exception");
                }
                //Sleep 30 minute
                Thread.Sleep(1800000);
            }
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
