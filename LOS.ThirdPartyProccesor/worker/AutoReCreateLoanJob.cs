﻿using LOS.Common.Extensions;
using LOS.DAL.Dapper;
using LOS.DAL.EntityFramework;
using LOS.Services.Services.Loanbrief;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Objects.Finance;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoReCreateLoanJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IConfiguration _baseConfig;
        private readonly ILMSService _lmsService;
        private readonly IErpService _erpService;
        private readonly ILogReLoanbrief _logReLoanbriefService;
        private readonly IAuthenticationAI _authenService;
        private readonly IServiceAI _aiService;
        private readonly IProductServices _productService;
        private readonly ILogReLoanbrief _logReLoanbrief;
        private readonly ILoanbriefV2Service _loanBriefV2Services;

        private Thread _thread;
        public AutoReCreateLoanJob(ILoanBriefService loanBriefServices, ILMSService lmsService, IErpService erpService, ILogReLoanbrief logReLoanbriefService,
            IAuthenticationAI authenService, IServiceAI aiService, IProductServices productService, ILogReLoanbrief logReLoanbrief,
            ILoanbriefV2Service loanBriefV2Services)
        {
            _loanBriefServices = loanBriefServices;
            _lmsService = lmsService;
            _erpService = erpService;
            _logReLoanbriefService = logReLoanbriefService;
            _authenService = authenService;
            _aiService = aiService;
            _productService = productService;
            _logReLoanbrief = logReLoanbrief;
            _loanBriefV2Services = loanBriefV2Services;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        public void Dispose()
        {
            _thread?.Abort();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private async void DoWork(object state)
        {
            while (true)
            {
                //lấy ra danh sách cần khởi tạo lại
                var listData = GetReLoanbrief();
                if (listData != null && listData.Count > 0)
                {
                    foreach (var item in listData)
                    {
                        if (!string.IsNullOrEmpty(item.Phone))
                        {
                            bool reBorrow = false;
                            int loanbriefId = 0;

                            //Nếu là tạo đơn topup
                            //kiểm tra xem có đơn topup nào đang xử lý không
                            if (item.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefTopup || item.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefDpd)
                            {
                                //Kiểm tra xem có đơn topup đang xử lý hay không
                                if (_loanBriefServices.CheckLoanBriefTopupProcessing(item.LoanbriefId))
                                {
                                    _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Có đơn Topup đang xử lý", 0);
                                    continue;
                                }
                                //kiểm tra xem có đơn DPD đang xử lý hay không
                                if (_loanBriefServices.CheckLoanBriefDPDProcessing(item.LoanbriefId))
                                {
                                    _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Có đơn đang xử lý", 0);
                                    continue;
                                }
                                //lấy ra thông tin loanbrief
                                var loanbrief = _loanBriefServices.GetAllById(item.LoanbriefId);
                                if (item.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefDpd
                                    && loanbrief.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                                {
                                    _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Không thể khởi tạo đơn DPD trên đơn Topup", 0);
                                    continue;
                                }
                                // Lấy dư nợ còn lại của đơn vay cũ
                                long totalMoneyCurrent = 0;
                                //Đối với gói vay xe máy
                                if (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                    || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                    || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                {
                                    //gọi api lms xem có đủ điều kiện Topup không
                                    var checkTopup = await _loanBriefV2Services.CheckCanTopup(loanbrief.LoanBriefId);
                                    if (!checkTopup.IsCanTopup)
                                    {
                                        if (item.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefTopup)
                                        {
                                            totalMoneyCurrent = (long)checkTopup.CurrentDebt;
                                            _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Đơn vay không đủ điều kiện vay Topup", 0);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        totalMoneyCurrent = (long)checkTopup.CurrentDebt;
                                    }
                                    if (loanbrief.LoanBriefProperty == null
                                        || loanbrief.LoanBriefProperty.ProductId == null)
                                    {
                                        _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Không lấy được định giá xe mới, không đủ điều kiện", 0);
                                        continue;
                                    }

                                    var maxPrice = GetMaxPriceProductTopup((int)loanbrief.LoanBriefProperty.ProductId, (int)loanbrief.ProductId, loanbrief.LoanBriefId, loanbrief.LoanBriefResident.ResidentType.Value, totalMoneyCurrent);
                                    if (maxPrice.PriceAi == 0)
                                    {
                                        //Không lấy được định giá AI thì chạy lại sau
                                        continue;
                                    }
                                    var loanAmount = maxPrice.MaxPriceProduct;
                                    //Làm tròn tiền xuống đơn vị triệu đồng
                                    loanAmount = Convert.ToInt64(LOS.Common.Utils.MathUtils.RoundMoney(loanAmount));
                                    if (loanAmount < 3000000)
                                    {
                                        _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Không đủ điều kiện Topup, số tiền < 3tr!", 0);
                                        continue;
                                    }
                                    item.LoanAmountExpertiseNew = maxPrice.PriceAi;
                                    item.LoanAmountExpertiseLastNew = maxPrice.MaxPriceProduct;
                                    item.LoanAmountNew = loanAmount;
                                }

                                else if (loanbrief.ProductId == (int)EnumProductCredit.OtoCreditType_KCC
                                    || loanbrief.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                                {
                                    //Check kiểm tra có đủ điều kiện topup oto hay không
                                    var nationalCard = loanbrief.NationalCard;
                                    if (!string.IsNullOrEmpty(loanbrief.NationCardPlace))
                                        nationalCard = $"{loanbrief.NationalCard},{loanbrief.NationCardPlace}";
                                    // Query api   	
                                    var data = _lmsService.CheckTopupOto(new CheckReLoan.Input()
                                    {
                                        CustomerName = loanbrief.FullName,
                                        NumberCard = nationalCard
                                    }, loanbrief.LoanBriefId);
                                    if (data.IsAccept == 1)
                                        item.LoanAmountExpertiseLastNew = data.TotalMoneyCurrent;
                                    else
                                    {
                                        _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, data.Message, 0);
                                        continue;
                                    }

                                    if ((loanbrief.LoanAmount + item.LoanAmountExpertiseLastNew) > 300000000)
                                    {
                                        _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Số tiền vượt quá hạn mức cho phép", 0);
                                        continue;
                                    }
                                    item.LoanAmountNew = 10000000; //để mặc định số tiền vay cho gói ô tô
                                }
                                else
                                {
                                    _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Đơn Topup chỉ hỗ trợ với gói vay xe máy hoặc ô tô", 0);
                                    continue;
                                }
                            }
                            else
                            {
                                //Nếu là khởi tạo lại đơn từ đơn tất toán
                                //kiểm tra xem có đủ điều kiện tái vay k
                                if (item.TypeReLoanbrief == (int)TypeReLoanbrief.FinishLoanbrief)
                                {
                                    if (!string.IsNullOrEmpty(item.NationalCard) && !string.IsNullOrEmpty(item.FullName))
                                    {
                                        //Kiểm tra điều kiện tái vay
                                        var checkReloan = _lmsService.CheckReBorrow(new CheckReLoan.Input
                                        {
                                            NumberCard = item.NationalCard,
                                            CustomerName = item.FullName
                                        }, item.LoanbriefId);
                                        if (checkReloan != null && checkReloan.Result == 1 && checkReloan.IsAccept == 1)
                                            reBorrow = true;
                                    }
                                }

                                //Kiểmt ra đã có đơn xe máy đang xử lý không
                                var loanbrief = _loanBriefServices.LoanbriefHandleV2(item.Phone, item.NationalCard);
                                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                                {
                                    _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Có đơn đang xử lý", 0);
                                    continue;
                                }
                                //kiểm tra nhân viên tima
                                var checkEmoloyeeTima = _erpService.CheckEmployeeTima(item.NationalCard, item.Phone, item.LoanbriefId);
                                if (checkEmoloyeeTima != null)
                                {
                                    _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Nhân viên Tima", 0);
                                    continue;
                                }
                            }

                            // Kiểm tra blackList
                            var resultCheckBlackList = _lmsService.CheckBlackList(item.FullName, item.NationalCard, item.Phone, item.LoanbriefId);
                            if (resultCheckBlackList != null && resultCheckBlackList.Data == true && resultCheckBlackList.Status == 1)
                            {
                                //KH nằm trong blacklist
                                _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "BlackList", 0);
                                continue;
                            }

                            if (item.TypeReLoanbrief == (int)TypeReLoanbrief.FinishLoanbrief)
                            {
                                item.ReBorrow = reBorrow;
                                loanbriefId = _loanBriefServices.ReCreateLoanFinish(item);
                            }
                            else if (item.TypeReLoanbrief == (int)TypeReLoanbrief.CancelLoanbrief)
                            {
                                loanbriefId = _loanBriefServices.ReCreateLoan(item);
                            }
                            else if (item.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefTopup || item.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefDpd)
                            {
                                if (item.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefTopup && item.UtmSource == "form_rmkt_topup")
                                {
                                    //kiểm tra xem đã có bản ghi được tạo chưa
                                    //nếu có rồi thì đổi đổi utmsource="form_rmkt_topup_renew"
                                    var logReLoanbrief = _logReLoanbrief.GetLast(item.LoanbriefId);
                                    if (logReLoanbrief != null)
                                        item.UtmSource = "form_rmkt_topup_renew";
                                }
                                if (item.LoanAmountNew >= 3000000)
                                {
                                    loanbriefId = _loanBriefServices.CreateLoanTopup(item);
                                }
                                else
                                {
                                    _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Không đủ điều kiện Topup, số tiền < 3tr!", 0);
                                    continue;
                                }
                            }

                            if (loanbriefId > 0)
                            {
                                //tạo mới thành công
                                var Comment = $"[{item.UtmSource}] Khởi tạo đơn remarketing từ đơn vay HĐ-{item.LoanbriefId}";
                                if (item.TypeReLoanbrief == (int)TypeReLoanbrief.FinishLoanbrief)
                                    Comment = $"Khởi tạo đơn vay từ HĐ đã tất toán HĐ-{item.LoanbriefId}";
                                else if (item.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefTopup)
                                    Comment = $"Auto System: Khởi tạo đơn vay Topup từ HĐ-{item.LoanbriefId}";
                                else if (item.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefDpd)
                                    Comment = $"Auto System: Khởi tạo đơn vay có DPD <= 8 từ HĐ-{item.LoanbriefId}";
                                _loanBriefServices.AddNote(new LoanBriefNote
                                {
                                    LoanBriefId = loanbriefId,
                                    Note = Comment,
                                    FullName = "Auto System",
                                    Status = 1,
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = item.CreatedBy,
                                    ShopId = EnumShop.Tima.GetHashCode(),
                                    ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                });
                                _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Success", loanbriefId);

                                if (item.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefTopup || item.TypeReLoanbrief == (int)TypeReLoanbrief.LoanBriefDpd)
                                {
                                    //lấy thông tin đỡ vừa tạo xong
                                    var loanbriefnew = _loanBriefServices.GetLoanBriefBasic(loanbriefId);
                                    //nếu là có gắn định vị thì gọi api mở HĐ
                                    if (loanbriefnew.IsLocate == true && !string.IsNullOrEmpty(loanbriefnew.DeviceId))
                                    {
                                        var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
                                        if (!string.IsNullOrEmpty(token))
                                        {
                                            var resultOpent = _aiService.OpenContract("HD-" + loanbriefnew.LoanBriefId, loanbriefnew.DeviceId, EnumShop.Tima.ToString(), loanbriefnew.ProductId.Value, token, loanbriefnew.LoanBriefId, "topup");
                                            if (resultOpent != null && (resultOpent.StatusCode == 200 || resultOpent.StatusCode == 3))
                                            {
                                                if (resultOpent.StatusCode == 200)
                                                {
                                                    _loanBriefServices.AddNote(new LoanBriefNote()
                                                    {
                                                        LoanBriefId = loanbriefId,
                                                        Note = string.Format("Đơn định vị: Tạo hợp đồng thành công với imei: {0}", loanbriefnew.DeviceId),
                                                        FullName = "Auto System",
                                                        Status = 1,
                                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                                        CreatedTime = DateTime.Now,
                                                        UserId = item.CreatedBy,
                                                        ShopId = EnumShop.Tima.GetHashCode(),
                                                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                                    });
                                                    _loanBriefServices.UpdateLoanDevice(loanbriefnew.LoanBriefId, (int)StatusOfDeviceID.OpenContract, $"HD-{loanbriefnew.LoanBriefId}");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                            _logReLoanbriefService.Update(item.Id, (int)LogReLoanbriefExcuted.IsExcuted, "Không có sdt khách hàng", 0);

                        Console.WriteLine(string.Format("ReLoanbrief: Done {0} {1}", item.Id, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
                //lấy danh sách đơn đủ điều kiện khởi tạo lại
                Thread.Sleep(1000 * 10); // nghỉ giảo lao 10s
            }


        }

        public List<ReCreateLoanModel> GetReLoanbrief()
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select lr.Id, l.LoanBriefId, l.Phone, l.FullName, l.NationalCard, lr.UtmSource, Lr.TypeRemarketing, lr.CreatedBy, lr.TelesaleId, lr.TypeReLoanbrief, lr.HubId, lr.TeamTelesaleId  ");
                sql.AppendLine("from LogReLoanbrief(nolock) lr ");
                sql.AppendLine("inner join LoanBrief(nolock) l on lr.loanbriefId = l.LoanBriefId ");
                sql.AppendLine("where ISNULL(lr.IsExcuted, 0) = 0 ");
                return db.Query<ReCreateLoanModel>(sql.ToString());
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private PriceProductTopup GetMaxPriceProductTopup(int productId, int productCredit, int loanBriefId, int typeOfOwnerShip, long totalMoneyCurrent)
        {
            long originalCarPrice = 0;
            // lấy giá bên AI
            decimal priceCarAI = _productService.GetPriceAI(productId, loanBriefId);
            if (priceCarAI > 0)
            {
                originalCarPrice = (long)priceCarAI;
            }
            else
            {
                //var productPriceCurrent = _productService.GetProduct(productId);
                //if (productPriceCurrent != null && productPriceCurrent.PriceCurrent > 0)
                //    originalCarPrice = productPriceCurrent.PriceCurrent ?? 0l;
            }
            //var originalCarPriceLast = originalCarPrice - TotalMoneyCurrent;
            var data = new PriceProductTopup
            {
                PriceAi = originalCarPrice,
                MaxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPriceTopup(productCredit, originalCarPrice, totalMoneyCurrent, typeOfOwnerShip)
            };
            return data;
        }

    }
}
