﻿using LOS.Common.Extensions;
using LOS.Common.Helpers;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoReGetCreditScoringJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IServiceAI _serviceAI;
        protected readonly IConfiguration _baseConfig;
        protected IAuthenticationAI _authenService;
        private Thread _threadRequest;
        public AutoReGetCreditScoringJob(ILoanBriefService loanBriefServices, IServiceAI serviceAI, IAuthenticationAI authenService)
        {
            _loanBriefServices = loanBriefServices;
            _authenService = authenService;
            _serviceAI = serviceAI;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            // create new unit of work instance			
            var builder = new ConfigurationBuilder()
              .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
              .AddJsonFile("appsettings.json", true)
              .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _threadRequest = new Thread(() => DoWork(null));
            _threadRequest.Start();
            return Task.CompletedTask;
        }
        private void DoWork(object state)
        {

            while (true)
            {
                try
                {
                    //lấy ra danh sách các row cần lấy lại điểm credit scoring
                    var requests = _loanBriefServices.ListReGetCreditScoring();
                    if (requests != null && requests.Count > 0)
                    {
                        var total = requests.Count;
                        var index = 0;
                        foreach (var item in requests)
                        {
                            index++;
                            Console.WriteLine($"{index}/{total}");
                            try
                            {
                                if (item.ID > 0)
                                {
                                    RequestToCreditScoring(item);
                                }
                            }
                            catch
                            {
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor ListReGetCreditScoring exception");
                }
                //Thread 1/2 minute
                Thread.Sleep(1000 * 30);
            }
        }
        private void RequestToCreditScoring(LoanBriefInfoCreditScoring entity)
        {
            try
            {
                if (entity != null)
                {
                    var token = _authenService.GetToken(Constants.scoring_app_id, Constants.scoring_app_key);
                    if (!string.IsNullOrEmpty(token))
                    {
                        var data = _serviceAI.GetCreditScoring(token, entity);
                        if (data != null && data.score > 0)
                        {
                            _loanBriefServices.DoneCreditScoring(entity.ID, data.score, data.label);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor RequestToCreditScoring exception");
            }
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            //_timer?.Change(Timeout.Infinite, 0);
            //_timerReGet?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _threadRequest?.Abort();
        }
    }
}
