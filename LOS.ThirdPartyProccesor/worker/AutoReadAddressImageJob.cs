﻿using LOS.Common.Helpers;
using LOS.DAL.Dapper;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoReadAddressImageJob : IHostedService, IDisposable
    {
        private Thread _thread;
        protected IConfiguration _baseConfig;
        protected ILocationService _locationService;
        private string environmentName = "";

        public AutoReadAddressImageJob(ILocationService locationService)
        {
            _locationService = locationService;
            environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {

            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }
        private void DoWork(object state)
        {
            while (true)
            {
                var data = ListFilesAsyn(environmentName == "Development" ? 9328169 : 9778731);
                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(item.LatLong))
                            {
                                var address = _locationService.GetNameAddress(item.LatLong);
                                if (!string.IsNullOrEmpty(address))
                                {
                                    if (UpdateAddress(item.ID, address) > 0)
                                        Console.WriteLine(string.Format("Done Read Address Image {0} {1}", item.ID, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                }
                                //upload từ app Tima Care
                                //if (item.SourceUpload == 3 && item.TypeId > 0 && item.LoanBriefId > 0)
                                //{
                                //    //Nếu loại chứng từ là chứng từ nhà
                                //    //tính khoảng cách tới nhà
                                //    if (Const.Document_Tham_Dinh_Nha_Tima_Care.Contains(item.TypeId.Value))
                                //    {
                                //        var addressHome = GetLocationHome(item.LoanBriefId.Value);
                                //        if (addressHome != null)
                                //        {
                                //            var distance = _locationService.DistanceTwoPoint(!string.IsNullOrEmpty(addressHome.LatLong) ? addressHome.LatLong : addressHome.Address, item.LatLong);
                                //            if (distance > 0)
                                //            {
                                //                //cập nhật khoảng cách tới nhà
                                //                UpdateDistance(item.ID, distance, 1);
                                //            }
                                //        }
                                //    }
                                //    else if (Const.Document_Tham_Dinh_Cty_Tima_Care.Contains(item.TypeId.Value))
                                //    {
                                //        var addressCompany = GetLocationCompany(item.LoanBriefId.Value);
                                //        if (addressCompany != null)
                                //        {
                                //            var distance = _locationService.DistanceTwoPoint(!string.IsNullOrEmpty(addressCompany.LatLong) ? addressCompany.LatLong : addressCompany.Address, item.LatLong);
                                //            if (distance > 0)
                                //            {
                                //                //cập nhật khoảng cách tới nhà
                                //                UpdateDistance(item.ID, distance, 2);
                                //            }
                                //        }
                                //    }
                                //}
                            }
                        }

                        catch (Exception ex)
                        {
                            Log.Error(ex, "LOS.ThirdPartyProccesor AutoReadAddressImageJob exception");
                        }
                    }
                }


                //Thread 30s
                Thread.Sleep(1000 * 30);
            }
        }


        private List<FileItem> ListFilesAsyn(int fromId)
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select ID, LatLong, SourceUpload, TypeId, LoanBriefId  from LoanBriefFiles(nolock) ");
                sql.AppendLine($"where ID > {fromId} and LatLong is not null and AddressUpload is null and Status = 1 and isnull(IsDeleted,0) = 0 ");
                var data = db.Query<FileItem>(sql.ToString());
                return data;
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        private AddressItem GetLocationHome(int LoanbriefId)
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var data = db.QueryFirstOrDefault<AddressItem>($"select AddressLatLng as 'LatLong', [Address] as 'Address' from LoanBriefResident(nolock) where LoanBriefResidentId = {LoanbriefId}");
                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private AddressItem GetLocationCompany(int LoanbriefId)
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var data = db.QueryFirstOrDefault<AddressItem>($"select CompanyAddressLatLng as 'LatLong' , CompanyAddress as 'Address' from LoanBriefJob where LoanBriefJobId = {LoanbriefId}");
                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private int UpdateAddress(int Id, string address)
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("update LoanBriefFiles ");
                sql.AppendFormat("set AddressUpload = N'{0}' ", address);
                sql.AppendFormat("where ID = {0}", Id);
                var data = db.Execute(sql.ToString());
                return data;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private int UpdateDistance(int Id, double distance, int from)
        {
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("update LoanBriefFiles ");
                //Khoảng cách tới nhà
                if (from == 1)
                    sql.AppendLine($"set DistanceAddressHomeGoogleMap =  {distance}");
                else
                    sql.AppendLine($"set DistanceAddressCompanyGoogleMap =  {distance}");
                sql.AppendLine($"where ID = {Id}");
                var data = db.Execute(sql.ToString());
                return data;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            //_timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            //_timer?.Dispose();
            _thread?.Abort();
        }
    }

    public class FileItem
    {
        public int ID { get; set; }
        public string LatLong { get; set; }
        public int? SourceUpload { get; set; }
        public int? TypeId { get; set; }
        public int? LoanBriefId { get; set; }
    }

    public class AddressItem
    {
        public string LatLong { get; set; }
        public string Address { get; set; }
    }
}
