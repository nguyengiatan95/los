﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
	public class AutoRetrievingLenderJob : IHostedService, IDisposable
	{
		private Thread _thread;
		protected IConfiguration _baseConfig;
		private ILenderAutoDistributeService _lenderService;
		private ISendMessage _sendMessage;

		public AutoRetrievingLenderJob(ILenderAutoDistributeService lenderService, ISendMessage sendMessage)
		{
			var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
			var builder = new ConfigurationBuilder()
			 .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
			 .AddJsonFile("appsettings.json", true)
			 .AddJsonFile($"appsettings.{environmentName}.json", true);
			_baseConfig = builder.Build();
			_lenderService = lenderService;
			_sendMessage = sendMessage;
		}
		private async void DoWork(object state)
		{
			while (true)
			{
				try
				{
					// lấy danh sách đơn đang chờ giải ngân và không bị khóa
					var loanBriefs = _lenderService.GetRetrieveableLoanBriefs();
					if (loanBriefs != null && loanBriefs.Count > 0)
					{
						foreach (var loanBrief in loanBriefs)
						{							
							// Nếu đã đẩy sang DT thì ko rút đơn về
							if (loanBrief.PriorityLevel < 100)
							{
								Log.Information("Rút đơn vay :" + loanBrief.LoanBriefId + " - LenderId:" + loanBrief.LenderId + " - Status :" + loanBrief.Status + " - State :" + loanBrief.AutoDistributeState + " - Level:" + loanBrief.PriorityLevel);
								// kiểm tra xem có ai nhận đơn không
								if (loanBrief.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING && loanBrief.Status == 101)
								{
									// kiểm tra người nhận đã nhận trong bao lâu
									var lenderLoanBrief = _lenderService.GetLenderLoanBriefByState(loanBrief.LoanBriefId, (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING);
									if (lenderLoanBrief != null && lenderLoanBrief.Count > 0)
									{
										if (lenderLoanBrief.Count == 1)
										{

											try
											{
												// Ok chỉ có 1 nđt khóa >> Kiểm tra thời gian nhận đơn
												if (lenderLoanBrief[0].UpdatedTime.Value.AddMinutes(10) < DateTime.Now)
												{
													bool hasPendingTransactions = false;
													// Kiểm tra xem có giao dịch nghi vấn hay không
													using (var httpClient = new HttpClient())
													{
														// Comment
														string url = _baseConfig["AppSettings:PaygateURL"] + "/api/loancredit/pending_transactions?loanBriefId=" + loanBrief.LoanBriefId + "&lenderId=" + lenderLoanBrief[0].LenderId;
														var response = await httpClient.GetAsync(url);
														if (response.IsSuccessStatusCode)
														{
															var result = await response.Content.ReadAsStringAsync();
															var obj = JsonConvert.DeserializeObject<PaygateDefaultResponse<bool>>(result);
															if (obj.meta.errCode == 200)
															{
																hasPendingTransactions = obj.data;
															}
														}
														else
														{
															// Ko request được >> mặc định ko cho rút về
															hasPendingTransactions = true;
														}
													}
													if (!hasPendingTransactions)
													{														
														// Quá thời gian >> Thu hồi đẩu xuống level tiếp theo
														_lenderService.UpdateLenderLoanBrief(loanBrief.LoanBriefId, lenderLoanBrief[0].LenderId.Value, (int)Common.Helpers.Const.AutoDistributeState.EXPIRED);
														// Chia tiếp
														_lenderService.UpdateLoanBriefRetrieve(loanBrief.LoanBriefId);
														//if (_lenderService.IsAutoDistributing())
														//{
														//	_lenderService.UpdateLoanBriefRetrieve(loanBrief.LoanBriefId);
														//}
														//else
														//{
														//	_lenderService.UpdateManualPush(loanBrief.LoanBriefId);
														//}
														//_lenderService.UpdateReceived(lenderLoanBrief[0].LenderId.Value, 4);			
														// Release tiền
														_lenderService.ReleaseLockedMoney(lenderLoanBrief[0].LenderId.Value, loanBrief.LoanAmount.Value);
													}
													else
													{
														Log.Information("Rút đơn vay :" + loanBrief.LoanBriefId + " , có giao dịch đang khởi tạo");
														// Có giao dịch nghi vấn hoặc đang khởi tạo
														Console.WriteLine("Có giao dịch nghi vấn hoặc vừa khởi tạo :" + loanBrief.LoanBriefId + "-" + loanBrief.LenderId);
													}
												}
											}
											catch (Exception ex)
											{
												// Continue
												Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
												Log.Error(ex, "LOS.ThirdPartyProccesor exception");
											}
										}
										else
										{
											// Có trên 2 nđt nhận đơn ?
											_lenderService.UpdateConflict(loanBrief.LoanBriefId);
											Console.WriteLine("Có trên 1 ndt nhận 1 đơn :" + loanBrief.LoanBriefId);
										}
									}
									// Đối với những đơn mà có nđt khóa thì vẫn cập nhật lại trạng thái đối với đon đã quá hạn ..
									var distributedLoanBriefs = _lenderService.GetLenderLoanBriefByState(loanBrief.LoanBriefId, (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED);
									if (distributedLoanBriefs != null && distributedLoanBriefs.Count > 0)
									{
										try
										{
											foreach (var lb in distributedLoanBriefs)
											{
												if (lb.UpdatedTime.Value.AddMinutes(10) < DateTime.Now)
												{
													_lenderService.UpdateLenderLoanBrief(loanBrief.LoanBriefId, lb.LenderId.Value, (int)Common.Helpers.Const.AutoDistributeState.EXPIRED);
													_lenderService.UpdateReceived(distributedLoanBriefs[0].LenderId.Value, 4);
												}
											}
										}
										catch (Exception ex)
										{
											// Continue
											Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
											Log.Error(ex, "LOS.ThirdPartyProccesor exception");
										}
									}
								}
								// kiểm tra xem có ai nhận đơn không
								else if (loanBrief.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.LOCKED_DISTRIBUTING && loanBrief.Status == 90)
								{
									Log.Error(loanBrief.LoanBriefId + " Sai trạng thái thu đơn!!!");
								}
								else if (loanBrief.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED || loanBrief.AutoDistributeState == (int)Common.Helpers.Const.AutoDistributeState.REQUIREMENT_NOT_MET) // Không có ai nhận đơn trong 10p được thì next level
								{
									var lenderLoanBriefs = _lenderService.GetLenderLoanBriefByState(loanBrief.LoanBriefId, (int)Common.Helpers.Const.AutoDistributeState.DISTRIBUTED);
									if (lenderLoanBriefs != null && lenderLoanBriefs.Count > 0)
									{
										// lấy bản ghi đầu tiên
										var lenderLoanBrief = lenderLoanBriefs[0];
										if (lenderLoanBrief.PushedTime.Value.AddMinutes(10) < DateTime.Now)
										{
											if (_lenderService.IsAutoDistributing())
											{
												// Quá thời gian >> Thu hồi đẩu xuống level tiếp theo																
												_lenderService.UpdateLoanBriefAllIgnore(loanBrief.LoanBriefId);
											}
											else
											{
												_lenderService.UpdateManualPush(loanBrief.LoanBriefId);
											}
										}
									}
									else
									{
										// Tất cả đã bỏ qua hoặc trả lại >> Thu hồi để chia lại
										if (_lenderService.IsAutoDistributing())
										{
											// Quá thời gian >> Thu hồi đẩu xuống level tiếp theo																
											_lenderService.UpdateLoanBriefAllIgnore(loanBrief.LoanBriefId);
										}
										else
										{
											_lenderService.UpdateManualPush(loanBrief.LoanBriefId);
										}
									}
								}
							}
						}
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine("Error " + ex.Message + "\n" + ex.StackTrace);
					Log.Error(ex, "LOS.ThirdPartyProccesor exception");
				}
				//Thread 
				Thread.Sleep(1000 * 30);
			}
		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			_thread = new Thread(() => DoWork(null));
			_thread.Start();
			return Task.CompletedTask;
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			return Task.CompletedTask;
		}

		public void Dispose()
		{
			_thread?.Abort();
		}
	}
}
