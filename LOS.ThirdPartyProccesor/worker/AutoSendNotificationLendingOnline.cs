﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoSendNotificationLendingOnline : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly ILogPushNotificationService _logPushNotificationService;
        private readonly IFCMService _fCMService;
        private Thread _thread;
        protected IConfiguration _baseConfig;

        private int _day = DateTime.Now.AddDays(-1).Day;
        private bool isFirst = true;
        private bool isSendNotify12Hour = false;
        private bool isSendNotify20Hour = false;
        public AutoSendNotificationLendingOnline(ILoanBriefService loanBriefServices, ILogPushNotificationService logPushNotificationService, IFCMService fCMService)
        {
            _loanBriefServices = loanBriefServices;
            _logPushNotificationService = logPushNotificationService;
            _fCMService = fCMService;

            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        private bool IsRunning()
        {
            if (isFirst && DateTime.Now.Hour == 10)
                return true;
            else if (_day != DateTime.Now.Day && DateTime.Now.Hour == 10)
                return true;
            return false;
        }

        private bool IsRunningHour()
        {
            if (DateTime.Now.Hour == 12 || DateTime.Now.Hour == 20)
            {
                //nếu là chạy lần đầu tiên 
                if (_day != DateTime.Now.Day && (!isSendNotify12Hour || !isSendNotify20Hour))
                {
                    return true;
                }
            }
            return false;
        }


        private void DoWork(object state)
        {

            while (true)
            {
                try
                {
                    //push 2 khung giờ 12h và 20
                    // 3 ngày đầu tiên push cả 2 khung giờ
                    // từ ngày thứ 4 trở đi chỉ push khung 12h
                    if (IsRunningHour())
                    {
                        var hourRun = DateTime.Now.Hour;
                        if (isFirst)
                        {
                            isFirst = false;
                            if (hourRun == 20)
                                isSendNotify12Hour = true;
                        }

                        if (hourRun == 12)
                        {
                            if (isSendNotify12Hour)
                            {
                                Thread.Sleep(1000 * 60 * 1);
                                continue;
                            }
                            isSendNotify12Hour = true;
                        }

                        else if (hourRun == 20)
                        {
                            if (isSendNotify20Hour)
                            {
                                Thread.Sleep(1000 * 60 * 1);
                                continue;
                            }
                            isSendNotify20Hour = true;
                        }

                        var loanCredits = _loanBriefServices.WaitingLendingOnline();
                        if (loanCredits != null && loanCredits.Count > 0)
                        {
                            foreach (var item in loanCredits)
                            {
                                try
                                {
                                    //tính số ngày tọa đơn so với hiện tại
                                    var pushNotification = true;
                                    var totalDayCreated = (int)(DateTime.Now - item.CreatedTime.Value).TotalDays;
                                    if (totalDayCreated > 3)
                                    {
                                        if (hourRun == 20)
                                            pushNotification = false;
                                    }
                                    if (pushNotification)
                                    {
                                        var isPush = true;
                                        var history = _logPushNotificationService.GetLast(item.LoanBriefId, (int)TypeNotification.FireBase, (int)ActionPushNotification.PushAppLendingOnline);
                                        if (history != null)
                                        {
                                            if (hourRun == 12)
                                            {
                                                //Lần gửi cuối cùng phải từ 1 ngày trước
                                                //Nếu ngày gửi = ngày hôm nay => không gửi lại nữa
                                                if (DateTime.Now.Day == history.ResponseAt.Value.Day)
                                                    isPush = false;
                                            }
                                            if (hourRun == 20)
                                            {
                                                //Lần cuối gửi phải trong khoảng 2h => job chạy lại ở khung giờ 20h
                                                if ((DateTime.Now - history.ResponseAt.Value).TotalHours <= 1)
                                                    isPush = false;
                                            }                                            
                                        }
                                        if (isPush)
                                        {
                                            var mess = "[Tima] Bạn có 1 đơn vay chưa hoàn thiện. Hãy hoàn thiện để được giải ngân.";
                                            _fCMService.PushNotification(Constants.TOKEN_FIREBASE_LENDING_ONLINE, item.Phone, mess, (int)ActionPushNotification.PushAppLendingOnline, item.LoanBriefId);
                                            Thread.Sleep(1000);
                                        }
                                    }
                                    //var history = _logPushNotificationService.GetLast(item.LoanBriefId, (int)TypeNotification.FireBase, (int)ActionPushNotification.PushAppLendingOnline);
                                    //if (history == null || (DateTime.Now - history.ResponseAt.Value).TotalDays > 1)
                                    //{
                                    //    var mess = "[Tima] Bạn có 1 đơn vay chưa hoàn thiện. Hãy hoàn thiện để được giải ngân.";
                                    //    _fCMService.PushNotification(Constants.TOKEN_FIREBASE_LENDING_ONLINE, item.Phone, mess, (int)ActionPushNotification.PushAppLendingOnline, item.LoanBriefId);
                                    //    Thread.Sleep(1000);
                                    //}
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }

                        if (isSendNotify12Hour && isSendNotify20Hour)
                        {
                            _day = DateTime.Now.Day;
                            isSendNotify12Hour = isSendNotify20Hour = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor AutoSendNotificationLendingOnline exception");
                }
                //Thread 1 minute
                Thread.Sleep(1000 * 60 * 1);
            }
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            //_timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            //_timer?.Dispose();
            _thread?.Abort();
        }
    }
}