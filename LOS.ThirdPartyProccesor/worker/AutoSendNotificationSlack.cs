﻿using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoSendNotificationSlack : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IShopService _shopServices;
        private readonly ISlackService _slackService;
        private Thread _thread;
        private Thread _threadLenderCare;
        protected IConfiguration _baseConfig;
        public AutoSendNotificationSlack(ILoanBriefService loanBriefServices, IShopService shopServices, ISlackService slackService)
        {
            _loanBriefServices = loanBriefServices;
            _shopServices = shopServices;
            _slackService = slackService;

            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();

            _threadLenderCare = new Thread(() => DoWorkLenderCare(null));
            _threadLenderCare.Start();
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {

            while (true)
            {
                try
                {
                    if(_baseConfig["AppSettings:IsPushSlack"] == "1")
                    {
                        var loanCredits = _loanBriefServices.WaitHubHandle();
                        if (loanCredits != null && loanCredits.Count > 0)
                        {
                            foreach (var item in loanCredits)
                            {
                                try
                                {
                                    //mỗi đơn gửi tối đa 10 lần
                                    var history = _slackService.GetHistory(item.LoanBriefId, "hub");
                                    if (history == null || history.Count <= 10)
                                    {
                                        if(history != null)
                                        {
                                            //3p gửi lại 1 lần
                                            var lastSend = history.Last().CreatedAt;
                                            if((DateTime.Now - lastSend.Value).TotalMinutes > 3)
                                            {
                                                SendNotificationToSlackHub(item);
                                            }
                                        }
                                        else
                                        {
                                            SendNotificationToSlackHub(item);
                                        }
                                        Thread.Sleep(1000);
                                    }
                                }
                                catch(Exception ex)
                                {
                                }
                            }
                        }

                        //push cảnh báo chuyên viên hub chưa xử lý đơn
                        var loanbriefs = _loanBriefServices.WaitStaffHubHandle();
                        if (loanbriefs != null && loanbriefs.Count > 0)
                        {
                            foreach (var item in loanbriefs)
                            {
                                try
                                {
                                    //mỗi đơn gửi tối đa 3 lần
                                    var history = _slackService.GetHistory(item.LoanBriefId, "staffhub");
                                    if (history == null || history.Count <= 3)
                                    {
                                        if (history != null)
                                        {
                                            //3p gửi lại 1 lần
                                            var lastSend = history.Last().CreatedAt;
                                            if ((DateTime.Now - lastSend.Value).TotalMinutes > 3)
                                            {
                                                SendNotificationToSlackStaffHub(item);
                                            }
                                        }
                                        else
                                        {
                                            SendNotificationToSlackStaffHub(item);
                                        }
                                        Thread.Sleep(1000);
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }
                    }                   
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor SendNotificationToSlack exception");
                }
                //Thread 2 minute
                Thread.Sleep(1000 * 60 * 2);
            }
        }

        private void DoWorkLenderCare(object state)
        {

            while (true)
            {
                try
                {
                    var isPushSlack = _baseConfig["AppSettings:IsPushSlack"].ToString();
                    if (isPushSlack == "1")
                    {
                        var loanCredits = _loanBriefServices.WaitLenderCareHandle();
                        if (loanCredits != null && loanCredits.Count > 0)
                        {
                            foreach (var item in loanCredits)
                            {
                                //mỗi đơn gửi tối đa 5 lần
                                var history = _slackService.GetHistory(item.LoanBriefId, "lendercare");
                                if (history == null || history.Count <= 5)
                                {
                                    if (history != null)
                                    {
                                        //3p gửi lại 1 lần
                                        var lastSend = history.Last().CreatedAt;
                                        if ((DateTime.Now - lastSend.Value).TotalMinutes > 3)
                                        {
                                            SendNotificationToSlackLenderCare(item);
                                        }
                                    }
                                    else
                                    {
                                        SendNotificationToSlackLenderCare(item);
                                    }
                                    Thread.Sleep(1000);
                                }
                                //SendNotificationToSlackLenderCare(item);
                                //Thread.Sleep(1000 * 2);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor DoWorkLenderCare SendNotificationToSlack exception");
                }
                //Thread 2 minute
                Thread.Sleep(1000 * 60 * 2);
            }
        }

        private void SendNotificationToSlackHub(LoanBrief entity)
        {
            try
            {
                var shop = _shopServices.GetById(entity.HubId.Value);
                if (shop != null && !string.IsNullOrEmpty(shop.WebhookSlack))
                {
                    var urlApi = shop.WebhookSlack;
                    if (!string.IsNullOrEmpty(urlApi))
                        _slackService.SendNotifyOfStack("Hệ thống", "Blue",
                            "HĐ-" + entity.LoanBriefId + " mới đẩy về"
                            , string.Format("RePush: HĐ-{0} {1} cần được xử lý", entity.LoanBriefId, entity.FullName)
                            , urlApi, entity.LoanBriefId, "hub");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor SendNotificationToSlackHub exception");
            }
        }

        private void SendNotificationToSlackStaffHub(LoanBrief entity)
        {
            try
            {
                var shop = _shopServices.GetById(entity.HubId.Value);
                if (shop != null && !string.IsNullOrEmpty(shop.WebhookSlack))
                {
                    var urlApi = shop.WebhookSlack;
                    if (!string.IsNullOrEmpty(urlApi))
                        _slackService.SendNotifyOfStack("Hệ thống", "Blue",
                            "HĐ-" + entity.LoanBriefId + " chưa được xử lý"
                            , string.Format("HĐ-{0} {1} cần được CVKD xử lý ngay", entity.LoanBriefId, entity.FullName)
                            , urlApi, entity.LoanBriefId, "staffhub");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor SendNotificationToSlackHub exception");
            }
        }
        private void SendNotificationToSlackLenderCare(LoanBrief entity)
        {
            try
            {
                var urlApi = _baseConfig["AppSettings:SlackAPIUrlMoneyNotiChannelOfLenderCare"].ToString();
                if (!string.IsNullOrEmpty(urlApi))
                {
                    _slackService.SendNotifyOfStack("Hệ thống", "Blue", "Phòng nguồn vốn có đơn HĐ-" + entity.LoanBriefId + " mới đẩy về"
                       , string.Format("RePush: HĐ-{0} {1} cần được xử lý", entity.LoanBriefId, entity.FullName)
                        , urlApi, entity.LoanBriefId, "lendercare");
                    Console.WriteLine(string.Format("PushNoti Lender Care DONE: {0} {1}", entity.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor SendNotificationToSlackLenderCare exception");
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            //_timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            //_timer?.Dispose();
            _thread?.Abort();
            _threadLenderCare?.Abort();
        }
    }
}