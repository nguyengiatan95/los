﻿using LOS.Common.Extensions;
using LOS.Common.Utils;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public sealed class AutoSendSMSJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly ILogResultAutoCallServices _logResultAutoCallServices;
        private readonly ILMSService _lmsService;
        private Thread _thread;
        private Thread _threadSendZNS;
        protected IConfiguration _baseConfig;
        protected IServiceAI _aiService;
        public AutoSendSMSJob(ILoanBriefService loanBriefServices, ILogResultAutoCallServices logResultAutoCallServices, ILMSService lmsService, IServiceAI aiService)
        {
            _loanBriefServices = loanBriefServices;
            _logResultAutoCallServices = logResultAutoCallServices;
            _lmsService = lmsService;
            _aiService = aiService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {

            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            _threadSendZNS = new Thread(() => DoWorkSendZNS(null));
            _threadSendZNS.Start();
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            while (true)
            {
                try
                {
                    if (_baseConfig["AppSettings:IsSendSmsQualified"] == "1")
                    {
                        var loanCredits = _logResultAutoCallServices.GetListSendSMSQualified(47585);
                        if (loanCredits != null && loanCredits.Count > 0)
                        {
                            foreach (var item in loanCredits)
                            {
                                if (item.LoanbriefId.HasValue)
                                {
                                    var loanbrief = _loanBriefServices.GetById(item.LoanbriefId.Value);
                                    if (loanbrief != null && !string.IsNullOrEmpty(loanbrief.Phone) && (loanbrief.Status == (int)EnumLoanStatus.INIT || loanbrief.Status == (int)EnumLoanStatus.TELESALE_ADVICE))
                                    {
                                        var logSendSms = _logResultAutoCallServices.GetLastSendSms((int)EnumTypeSendSMS.QualifiedAutoCall, loanbrief.LoanBriefId);
                                        if (logSendSms == null)
                                            SendSmsQualified(loanbrief.LoanBriefId, loanbrief.Phone);
                                    }
                                }
                                _logResultAutoCallServices.UpdateSendSMS(item.Id);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor GetListSendSMSQualified exception");
                }
                //Thread 1 minute
                Thread.Sleep(1000 * 30);
            }
        }

        private void DoWorkSendZNS(object state)
        {
            while (true)
            {
                try
                {
                    if (_baseConfig["AppSettings:IsSendZnsQualified"] == "1")
                    {
                        var loanCredits = _logResultAutoCallServices.GetListSendZNS();
                        if (loanCredits != null && loanCredits.Count > 0)
                        {
                            foreach (var item in loanCredits)
                            {
                                if (item.LoanbriefId.HasValue)
                                {
                                    var loanbrief = _loanBriefServices.GetById(item.LoanbriefId.Value);
                                    if (loanbrief != null && !string.IsNullOrEmpty(loanbrief.Phone) 
                                        && (loanbrief.Status == (int)EnumLoanStatus.INIT || loanbrief.Status == (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN 
                                        || loanbrief.Status == (int)EnumLoanStatus.TELESALE_ADVICE))
                                    {
                                        var logSendSms = _logResultAutoCallServices.GetLastSendSms((int)EnumTypeSendSMS.SendZNS, loanbrief.LoanBriefId);
                                        if (logSendSms == null)
                                            SendZNS(loanbrief);
                                    }
                                }
                                _logResultAutoCallServices.UpdateSendSMS(item.Id);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor GetListSendSMSQualified exception");
                }
                //Thread 1 minute
                Thread.Sleep(1000 * 30);
            }
        }

        private void SendSmsQualified(int loanbriefId, string phone)
        {
            try
            {
                var content = _baseConfig["AppSettings:ContentSmsQualified"].ToString();
                if (!string.IsNullOrEmpty(content))
                {
                    var url = string.Format(LOS.Common.Helpers.Const.url_tra_cuu, EncryptUtils.Encrypt(loanbriefId.ToString(), false));
                    content = string.Format(content, url);
                    var result = _lmsService.SendSMS(phone, content, loanbriefId, (int)EnumTypeSendSMS.QualifiedAutoCall);
                    if (result != null && result.Result == 1)
                        Console.WriteLine(string.Format("SMS QUALIFIED AUTOCALL: {0} {1}", loanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    else
                        Console.WriteLine(string.Format("SMS QUALIFIED AUTOCALL ERROR: {0} {1}", loanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void SendZNS(LoanBrief entity)
        {
            try
            {
                var objData = new SendZnsReq()
                {
                    customer_id = entity.Phone,
                    template_data = new TemplateDataZns()
                    {
                        customer_name = entity.FullName,
                        loan_id = string.Format("HĐ-{0}", entity.LoanBriefId),
                        ngay_dang_ky = entity.CreatedTime.Value.ToString("yyyy-MM-dd")
                    }
                };
                if (_aiService.SendZns(objData, entity.LoanBriefId))
                    Console.WriteLine(string.Format("ZNS Zalo AUTOCALL: {0} {1}", entity.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                else
                    Console.WriteLine(string.Format("ZNS Zalo AUTOCALL ERROR: {0} {1}", entity.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                Thread.Sleep(1000 * 5);
            }
            catch (Exception ex)
            {

            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            //_timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            //_timer?.Dispose();
            _thread?.Abort();
        }
    }
}
