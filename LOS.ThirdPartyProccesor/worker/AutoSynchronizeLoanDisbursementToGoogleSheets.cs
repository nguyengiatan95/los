﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using LOS.Common.Extensions;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoSynchronizeLoanDisbursementToGoogleSheets : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefService;
        private readonly IConfiguration _configuration;
        private string environmentName;
        private Thread _thread;

        static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
        static readonly string ApplicationName = "IT_Auto";
        private static readonly string SpreadsheetId = "1zZSn5bOF9fYHqkhBR86s2G3ntX92KDkFZK1OnTrNKII";
        private static readonly string sheet = "Giải ngân v2";
        private static SheetsService service;

        public AutoSynchronizeLoanDisbursementToGoogleSheets(ILoanBriefService loanBriefService)
        {
            _loanBriefService = loanBriefService;
            environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            this._configuration = builder.Build();
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            while (true)
            {
                Console.WriteLine("****SynchronizeLoanDisbursementToGoogleSheets****");
                //lấy ra danh sách các đơn có GoogleClickId đã giải ngân và chưa đồng bộ lên GoogleSheets
                var loanbrief = _loanBriefService.GetLoanDisbursementNotSynchronizeGoogleSheets();
                if (loanbrief != null && loanbrief.Count > 0)
                {
                    GoogleCredential credential;
                    using (var stream = new FileStream("googlesheets.json", FileMode.Open, FileAccess.Read))
                    {
                        credential = GoogleCredential.FromStream(stream).CreateScoped(Scopes);
                    }
                    service = new SheetsService(new Google.Apis.Services.BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = ApplicationName,
                    });

                    var lstData = new List<LoanDisbursementGoogleSheets>();
                    foreach (var item in loanbrief)
                    {
                        var data = new LoanDisbursementGoogleSheets()
                        {
                            GoogleClickId = item.GoogleClickId,
                            ConversionName = "Giải Ngân",
                            ConversionTime = item.DisbursementAt.Value.ToString("yyyy-MM-dd HH: mm: ss")
                        };
                        lstData.Add(data);
                        //update lại SynchorizeFinance
                        _loanBriefService.UpdateSynchorizeFinance((int)EnumSynchorizeFinance.SynchorizeLoanDisbursementToGoogleSheets, item.LoanBriefId);
                    }
                    CreateGoogleSheets(lstData);
                    Console.WriteLine("Cap nhap thanh cong {0} ho so len GoogleSheets", loanbrief.Count);
                }
                else
                    Console.WriteLine("Khong co don nao can dong bo");

                Console.WriteLine("**** Nghi giao lao 2 gio ^^! ****");
                Thread.Sleep(1000 * (2 * 60 * 30)); // nghỉ giảo lao 2 gio
            }

        }

        private void CreateGoogleSheets(List<LoanDisbursementGoogleSheets> entity)
        {
            var range = $"{sheet}!A:C";
            var valueRange = new ValueRange();
            var lstResult = new List<IList<object>>();
            foreach (var item in entity)
            {
                var objList = new List<object>()
                {
                    $"{item.GoogleClickId}", $"{item.ConversionName}", $"{item.ConversionTime}"
                };
                lstResult.Add(objList);
            }
            valueRange.Values = lstResult;
            var appendRequest = service.Spreadsheets.Values.Append(valueRange, SpreadsheetId, range);
            appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            appendRequest.Execute();
        }

        public void Dispose()
        {
            _thread?.Abort();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
