﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using LOS.Common.Extensions;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace LOS.ThirdPartyProccesor.worker
{
    public class AutoSynchronizeLoanGoogleClickToGoogleSheets : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefService;
        private readonly IConfiguration _configuration;
        private string environmentName;
        private Thread _thread;

        static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
        static readonly string ApplicationName = "IT_Auto";
        private static readonly string SpreadsheetId = "1T3X6au9gYN-yMEzBrLxHSsFnH6QTVQctVcsv33tp0bc";
        private static readonly string sheet = "Form GGads";
        private static SheetsService service;

        public AutoSynchronizeLoanGoogleClickToGoogleSheets(ILoanBriefService loanBriefService)
        {
            _loanBriefService = loanBriefService;
            environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            this._configuration = builder.Build();
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            while (true)
            {
                Console.WriteLine("****SynchronizeLoanGoogleClickToGoogleSheets****");
                //lấy ra danh sách các đơn có GoogleClickId và chưa đồng bộ lên GoogleSheets
                var loanbrief = _loanBriefService.GetLoanGoogleClickNotSynchronizeGoogleSheets();
                if (loanbrief != null && loanbrief.Count > 0)
                {
                    GoogleCredential credential;
                    using (var stream = new FileStream("googlesheets.json", FileMode.Open, FileAccess.Read))
                    {
                        credential = GoogleCredential.FromStream(stream).CreateScoped(Scopes);
                    }
                    service = new SheetsService(new Google.Apis.Services.BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = ApplicationName,
                    });

                    var lstData = new List<LoanDisbursementGoogleSheets>();
                    var listLoanbriefId = new List<int>();
                    foreach (var item in loanbrief)
                    {
                        if (!string.IsNullOrEmpty(item.GoogleClickId))
                        {
                            lstData.Add(new LoanDisbursementGoogleSheets()
                            {
                                GoogleClickId = item.GoogleClickId,
                                ConversionName = "Form GGads",
                                ConversionTime = item.CreatedTime.Value.ToString("yyyy-MM-dd HH: mm: ss")
                            });
                        }                     
                    }
                    if (CreateGoogleSheets(lstData))
                    {
                        //update lại SynchorizeFinance
                        _loanBriefService.UpdateSynchorizeFinance((int)EnumSynchorizeFinance.SynchorizeLoanGoogleClickToGoogleSheets, listLoanbriefId);
                    }
                    Console.WriteLine("Cap nhap thanh cong {0} ho so len GoogleSheets", loanbrief.Count);
                }
                else
                    Console.WriteLine("Khong co don nao can dong bo");

                Console.WriteLine("**** Nghi giao lao 30 phút ^^! ****");
                Thread.Sleep(1000 * 30 * 60); // nghỉ giải lao 30 phút
            }

        }

        private bool CreateGoogleSheets(List<LoanDisbursementGoogleSheets> entity)
        {
            try
            {
                var range = $"{sheet}!A:C";
                var valueRange = new ValueRange();
                var lstResult = new List<IList<object>>();
                foreach (var item in entity)
                {
                    var objList = new List<object>()
                {
                    $"{item.GoogleClickId}", $"{item.ConversionName}", $"{item.ConversionTime}"
                };
                    lstResult.Add(objList);
                }
                valueRange.Values = lstResult;
                var appendRequest = service.Spreadsheets.Values.Append(valueRange, SpreadsheetId, range);
                appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
                appendRequest.Execute();
                return true;
            }catch(Exception ex)
            {
                return false;
            }            
        }

        public void Dispose()
        {
            _thread?.Abort();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
