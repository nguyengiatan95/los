﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects.Finance;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class CreateLoanAutocall : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly ILogResultAutoCallServices _logResultAutoCall;
        private readonly IConfiguration _baseConfig;

        private Thread _thread;
        public CreateLoanAutocall(ILogResultAutoCallServices logResultAutoCall, ILoanBriefService loanBriefServices)
        {
            _logResultAutoCall = logResultAutoCall;
            _loanBriefServices = loanBriefServices;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }
        private void DoWork(object state)
        {
            while (true)
            {
                if (_baseConfig["AppSettings:IsCreateLoanFromAutoCall"] != null && _baseConfig["AppSettings:IsCreateLoanFromAutoCall"].Equals("1"))
                {
                    //lấy danh sách đơn đủ điều kiện khởi tạo lại
                    var lstData = _logResultAutoCall.GetListReLoanAutocall();
                    if (lstData != null && lstData.Count > 0)
                    {
                        foreach (var item in lstData)
                        {
                            try
                            {
                                //Call Service CreateLoanAutocall
                                var result = _logResultAutoCall.CreateLoanAutocall(item.LoanbriefId.Value);
                                if (result > 0)
                                {
                                    Console.WriteLine("RE CREATE LOANBRIEF HĐ-" + result + " FROM " + item.LoanbriefId);
                                    var note = new LoanBriefNote
                                    {
                                        LoanBriefId = result,
                                        Note = "Khởi tạo đơn vay Autocall đơn hủy với KH có xe máy",
                                        FullName = "Auto System",
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = 1,
                                        ShopId = EnumShop.Tima.GetHashCode(),
                                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                    };
                                    _loanBriefServices.AddNote(note);
                                }
                                else
                                {
                                    Console.WriteLine("Khong the khoi tao don vay Autocall!");
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                    Console.WriteLine("**** Nghi giao lao 10 phut! ****");
                }
                  
                Thread.Sleep(1000 * (60 * 10)); // nghỉ giảo lao 30 phút
            }
        }

        public void Dispose()
        {
            _thread?.Abort();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
