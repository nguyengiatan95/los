﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor
{
    public sealed class EventRequestJob : IHostedService, IDisposable
    {
        private readonly IRequestEvent _requestEventServices;
        private readonly ILogRequestAi _logRequestAiServices;
        private readonly ILogLoanInfoAIService _logLoaninfoAiServices;
        private readonly ISlackService _slackService;
        private readonly ISendMessage _sendMessageService;
        protected IConfiguration _baseConfig;
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IShopService _shopServices;
        private Thread _thread;
        public EventRequestJob(IRequestEvent requestEventServices,
            ILogRequestAi logRequestAiServices, ILogLoanInfoAIService logLoaninfoAiServices, ISlackService slackService,
            ILoanBriefService loanBriefServices, ISendMessage sendMessageService, IShopService shopServices)
        {
            _requestEventServices = requestEventServices;
            _logRequestAiServices = logRequestAiServices;
            _logLoaninfoAiServices = logLoaninfoAiServices;
            _slackService = slackService;
            _loanBriefServices = loanBriefServices;
            _sendMessageService = sendMessageService;
            _shopServices = shopServices;

            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _baseConfig = builder.Build();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {

            while (true)
            {
                try
                {
                    var allEvents = _requestEventServices.GetNotExcuted();
                    if (allEvents != null && allEvents.Count > 0)
                    {
                        foreach (var item in allEvents)
                        {
                            Console.WriteLine(string.Format("Read Event Request: {0} {1} {2}", item.Id, item.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            //lấy điểm credit scoring
                            if (item.EventId == 1)
                            {
                                //Thêm 1 row vào bảng LogLoanInfoAI
                                ExcutedCreditScoring(item);
                                if (_requestEventServices.Excuted(item.Id, DateTime.Now))
                                    Console.WriteLine(string.Format("Done Event Request: {0} {1} {2}", item.Id, item.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            }
                            //push log lead score to AI
                            else if (item.EventId == 2)
                            {
                                ExcutedPushLogLeadScore(item);
                                if (_requestEventServices.Excuted(item.Id))
                                    Console.WriteLine(string.Format("Done Event Request: {0} {1} {2}", item.Id, item.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            }
                            //lấy điểm range credit scoring
                            else if (item.EventId == 3)
                            {
                                //Thêm 1 row vào bảng 
                                ExcutedRangeCreditScoring(item);
                                if (_requestEventServices.Excuted(item.Id, DateTime.Now))
                                    Console.WriteLine(string.Format("Done Event Request: {0} {1} {2}", item.Id, item.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            }
                            //Send notify to Lender Care
                            else if (item.EventId == 4)
                            {
                                //Thêm 1 row vào bảng LogRequestAI
                                SendNotifyToLenderCare(item);
                                if (_requestEventServices.Excuted(item.Id, DateTime.Now))
                                    Console.WriteLine(string.Format("Done Event Request: {0} {1} {2}", item.Id, item.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            }
                            //Send totify to Lender APP
                            else if (item.EventId == 5)
                            {
                                //Gọi api 
                                SendNotifyToLenderApp(item);
                                if (_requestEventServices.Excuted(item.Id, DateTime.Now))
                                    Console.WriteLine(string.Format("Done Event Request: {0} {1} {2}", item.Id, item.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            }
                            //Đơn vay do TĐHS trả về
                            else if (item.EventId == 6)
                            {
                                SendNotifyTDHSBackToHub(item);
                                if (_requestEventServices.Excuted(item.Id, DateTime.Now))
                                    Console.WriteLine(string.Format("Done Send notify tdhs back hub: {0} {1} {2}", item.Id, item.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            }
                            else if (item.EventId == 7)
                            {
                                ExcutedRegisterCrawlFacebook(item);
                                _requestEventServices.Excuted(item.Id, DateTime.Now);
                            }
                            else if (item.EventId == 8)
                            {
                                SendNotifyApproveGroup(item);
                                if (_requestEventServices.Excuted(item.Id, DateTime.Now))
                                    Console.WriteLine(string.Format("Done Send notify pd: {0} {1} {2}", item.Id, item.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            }
                            else if (item.EventId == 9)
                            {
                                SendNotifyBODGroup(item);
                                if (_requestEventServices.Excuted(item.Id, DateTime.Now))
                                    Console.WriteLine(string.Format("Done Send notify pd bod: {0} {1} {2}", item.Id, item.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            }                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor EventRequestJob exception");
                }
                //Thread 1 minute
                Thread.Sleep(1000 * 30);
            }
        }

        private void ExcutedCreditScoring(RequestEvent entity)
        {
            try
            {
                var result = _logLoaninfoAiServices.AddLog(new LogLoanInfoAi()
                {
                    LoanbriefId = entity.LoanbriefId,
                    ServiceType = (int)ServiceTypeAI.CreditScoring,
                    IsExcuted = EnumLogLoanInfoAiExcuted.WaitRequest.GetHashCode(),
                    CreatedAt = DateTime.Now
                });
                if (result > 0)
                    Console.WriteLine(string.Format("Excuted CreditScoring DONE: {0} {1}", result, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                else
                    Console.WriteLine(string.Format("Excuted CreditScoring ERROR: {0} {1}", result, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("ExcutedLeadScore EX: {0} {1}", ex.ToString(), DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
        }

        private void ExcutedRangeCreditScoring(RequestEvent entity)
        {
            try
            {
                var result = _logLoaninfoAiServices.AddLog(new LogLoanInfoAi()
                {
                    LoanbriefId = entity.LoanbriefId,
                    ServiceType = (int)ServiceTypeAI.RangeCreditScoring,
                    IsExcuted = EnumLogLoanInfoAiExcuted.WaitRequest.GetHashCode(),
                    CreatedAt = DateTime.Now
                });
                if (result > 0)
                    Console.WriteLine(string.Format("ExcutedCreditScoring DONE: {0} {1}", result, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                else
                    Console.WriteLine(string.Format("ExcutedCreditScoring ERROR: {0} {1}", result, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("ExcutedCreditScoring EX: {0} {1}", ex.ToString(), DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
        }

        private void ExcutedPushLogLeadScore(RequestEvent entity)
        {
            try
            {
                var result = _logLoaninfoAiServices.AddLog(new LogLoanInfoAi()
                {
                    LoanbriefId = entity.LoanbriefId,
                    ServiceType = (int)ServiceTypeAI.PushLogLeadScore,
                    IsExcuted = EnumLogLoanInfoAiExcuted.WaitRequest.GetHashCode(),
                    CreatedAt = DateTime.Now
                });
                if (result > 0)
                    Console.WriteLine(string.Format("ExcutedPushLogLeadScore DONE: {0} {1}", result, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                else
                    Console.WriteLine(string.Format("ExcutedPushLogLeadScore ERROR: {0} {1}", result, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("ExcutedPushLogLeadScore EX: {0} {1}", ex.ToString(), DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
        }

        private void SendNotifyToLenderCare(RequestEvent entity)
        {
            try
            {
                var isPushSlack = _baseConfig["AppSettings:IsPushSlack"].ToString();
                if (isPushSlack == "1")
                {
                    var urlApi = _baseConfig["AppSettings:SlackAPIUrlMoneyNotiChannelOfLenderCare"].ToString();
                    if (!string.IsNullOrEmpty(urlApi))
                    {
                        _slackService.SendNotifyOfStack("Hệ thống", "Blue", "Phòng nguồn vốn có đơn HĐ-" + entity.LoanbriefId + " mới đẩy về",
                            "HD-" + entity.LoanbriefId + " cần được xử lý"
                            , urlApi, entity.LoanbriefId.Value, "lendercare");
                        Console.WriteLine(string.Format("PushNoti Lender Care DONE: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("SendNotifyToLenderCare EX: {0} {1}", ex.ToString(), DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
        }
        private void SendNotifyToLenderApp(RequestEvent entity)
        {
            try
            {
                var loanbrief = _loanBriefServices.GetById(entity.LoanbriefId.Value);
                if (loanbrief != null && loanbrief.TypeDisbursement == (int)EnumDisbursement.DISBURSE_LEND)
                {
                    var message = new NotifyLenderApp();
                    message.key = "tima@lender#234";
                    message.data = new List<DataItemNotify>();
                    message.data.Add(new DataItemNotify()
                    {
                        loanCreditId = loanbrief.LoanBriefId,
                        topic = loanbrief.LenderId.ToString()

                    });
                    _sendMessageService.SendNotifyToAppLender(message, loanbrief.LoanBriefId);
                    Console.WriteLine(string.Format("PushNoti Lender App DONE: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("SendNotifyToLenderApp EX: {0} {1}", ex.ToString(), DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
        }

        private void SendNotifyTDHSBackToHub(RequestEvent entity)
        {
            try
            {
                var loanbrief = _loanBriefServices.GetById(entity.LoanbriefId.Value);
                if (loanbrief != null && loanbrief.HubId != null)
                {
                    var objHub = _shopServices.GetById(loanbrief.HubId.Value);
                    if (!string.IsNullOrEmpty(objHub.WebhookSlack))
                    {
                        var urlApi = objHub.WebhookSlack;
                        if (!string.IsNullOrEmpty(urlApi))
                            _slackService.SendNotifyOfStack("Hệ thống", "Blue",
                                    "HĐ-" + loanbrief.LoanBriefId + " bị TĐHS trả về"
                                    , string.Format("HĐ-{0} {1} cần được xử lý luôn", loanbrief.LoanBriefId, loanbrief.FullName)
                                    , urlApi, loanbrief.LoanBriefId, "tdhs_back_hub");
                    }
                    Console.WriteLine(string.Format("PushNoti TĐHS Back DONE: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("SendNotifyTDHSBackToHub EX: {0} {1}", ex.ToString(), DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
        }

        private void ExcutedRegisterCrawlFacebook(RequestEvent entity)
        {
            try
            {
                var result = _logLoaninfoAiServices.AddLog(new LogLoanInfoAi()
                {
                    LoanbriefId = entity.LoanbriefId,
                    ServiceType = (int)ServiceTypeAI.FacebookInfo,
                    IsExcuted = EnumLogLoanInfoAiExcuted.WaitRequest.GetHashCode(),
                    CreatedAt = DateTime.Now
                });
                if (result > 0)
                    Console.WriteLine(string.Format("ExcutedRegisterCrawlFacebook DONE: {0} {1}", result, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                else
                    Console.WriteLine(string.Format("ExcutedRegisterCrawlFacebook ERROR: {0} {1}", result, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("ExcutedRegisterCrawlFacebook EX: {0} {1}", ex.ToString(), DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
        }

        private void SendNotifyApproveGroup(RequestEvent entity)
        {
            try
            {
                var webhookSlack = "services/T019B14UGQ1/B02GH3CT0JW/dAQA6INhvxDRyW8sz1ILWWsx";
                var loanbrief = _loanBriefServices.GetById(entity.LoanbriefId.Value);
                if (loanbrief != null && loanbrief.HubId != null)
                {
                    if (!string.IsNullOrEmpty(webhookSlack))
                        _slackService.SendNotifyOfStack("Hệ thống", "Blue",
                                "HĐ-" + loanbrief.LoanBriefId + " trình ngoại lệ"
                                , string.Format("HĐ-{0} {1} cần được phê duyệt ngoại lệ", loanbrief.LoanBriefId, loanbrief.FullName)
                                , webhookSlack, loanbrief.LoanBriefId, "pd_ngoai_le");
                    Console.WriteLine(string.Format("PushNoti PDNL Back DONE: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("SendNotifyApproveGroup EX: {0} {1}", ex.ToString(), DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
        }

        private void SendNotifyBODGroup(RequestEvent entity)
        {
            try
            {
                var webhookSlack = "services/T019B14UGQ1/B02SKM4HT6W/nCtjxNAsS74FqxvuYjLM4ap2";
                var loanbrief = _loanBriefServices.GetById(entity.LoanbriefId.Value);
                if (loanbrief != null && loanbrief.HubId != null)
                {
                    if (!string.IsNullOrEmpty(webhookSlack))
                        _slackService.SendNotifyOfStack("Hệ thống", "Blue",
                                "HĐ-" + loanbrief.LoanBriefId + " trình phê duyệt"
                                , string.Format("HĐ-{0} {1} cần được phê duyệt", loanbrief.LoanBriefId, loanbrief.FullName)
                                , webhookSlack, loanbrief.LoanBriefId, "pd_ngoai_le");
                    Console.WriteLine(string.Format("PushNoti PDNL BOD Back DONE: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("SendNotifyBODGroup EX: {0} {1}", ex.ToString(), DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            //_timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            //_timer?.Dispose();
            _thread?.Abort();
        }
    }
}
