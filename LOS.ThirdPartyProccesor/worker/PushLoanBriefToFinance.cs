﻿using LOS.Common.Extensions;
using LOS.ThirdPartyProccesor.Objects.Finance;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class PushLoanBriefToFinance : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IFinanceService _financeService;
        private readonly IConfiguration _configuration;

        private string environmentName;
        private Thread _thread;
        public PushLoanBriefToFinance(ILoanBriefService loanBriefServices, IFinanceService financeService)
        {
            _loanBriefServices = loanBriefServices;
            _financeService = financeService;
            environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            this._configuration = builder.Build();
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            if(environmentName == "Production")
            {
                _thread = new Thread(() => DoWork(null));
                _thread.Start();
            }
            return Task.CompletedTask;
        }
        private void DoWork(object state)
        {
            while (true)
            {
                Console.WriteLine("****PushLoanBriefToFinance****");
                //lấy danh sách đơn bị hủy của LOS phù hợp để bắn về sàn
                var lstData = _loanBriefServices.GetLoanBriefReasonCancelIsPushToFinance();
                if (lstData != null && lstData.Count > 0)
                {
                    foreach (var item in lstData)
                    {
                        // đồng bộ lại khoản vay với bên Sàn
                        var productId = 2;
                        if (item.ProductId == 8)
                        {
                            productId = 7;
                        }
                        else if (item.ProductId == 27)
                        {
                            productId = 14;
                        }
                        var objCreateLoan = new RequestApiCreateLoanCreditFromCancelLOS()
                        {
                            Phone = item.Phone,
                            ProductId = productId,
                            TotalMoney = item.LoanAmount.HasValue ? (long)item.LoanAmount : 0,
                            LoanTime = item.LoanTime.HasValue ? item.LoanTime.Value : 12,
                            TypeTime = 2,
                            FullName = item.FullName,
                            CityId = item.ProvinceId.HasValue ? item.ProvinceId.Value : 0,
                            DistrictId = item.DistrictId.HasValue ? item.DistrictId.Value : 0,
                            AffCode = item.AffCode,
                            CardNumber = item.NationalCard,
                            UtmSource = item.UtmSource,
                            UtmMedium = item.UtmMedium,
                            UtmCampaign = item.UtmCampaign,
                            UtmContent = item.UtmContent,
                            UtmTerm = item.UtmTerm,
                            Aff_Sid = item.AffSid,
                            TId = item.Tid,
                            DomainName = "los.tima.vn"
                        };
                        //Gọi API tạo đơn bên Sàn
                        var result = _financeService.CreateLoanCreditFromCancelLOS(objCreateLoan);
                        //tạo đơn không thành công bên sàn
                        if(result == null)
                        {
                            Console.WriteLine("Khong goi duoc api tao don ben san");
                        }
                        else
                        {
                            if(result.Status == 1)
                            {
                                Console.WriteLine("HĐ-"+ item.LoanBriefId +" Tao don thanh cong ben san");
                                //Cập nhập lại trường SynchorizeFinance trong bảng LoanBrief
                                _loanBriefServices.UpdateSynchorizeFinance((int)EnumSynchorizeFinance.LoanCancelPushToFinance, item.LoanBriefId);
                            }
                            else
                            {
                                Console.WriteLine("HĐ-" + item.LoanBriefId + result.Messages[0]);
                                //Cập nhập lại trường SynchorizeFinance trong bảng LoanBrief
                                _loanBriefServices.UpdateSynchorizeFinance((int)EnumSynchorizeFinance.LoanCancelPushToFinanceRefuse, item.LoanBriefId);
                            }
                        }     
                    }
                }
                Console.WriteLine("**** Nghi giao lao 30 phut ^^! ****");
                Thread.Sleep(1000 * (60 * 30)); // nghỉ giảo lao 30 phút
            }
        }

        public void Dispose()
        {
            _thread?.Abort();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
