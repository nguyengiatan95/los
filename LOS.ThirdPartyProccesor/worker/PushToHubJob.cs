﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class PushToHubJob : IHostedService, IDisposable
    {
        private int executionCount = 0;
        //private Timer _timer;

        private Thread _thread;

        private readonly ILoanBriefService _loanBriefServices;
        private readonly IShopService _shopServices;
        public PushToHubJob(ILoanBriefService loanBriefServices, IShopService shopServices)
        {
            _loanBriefServices = loanBriefServices;
            _shopServices = shopServices;
        }
       
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            //_timer = new Timer(DoWork, null, TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(300));
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            
            while (true)
            {
                //Timed Hosted Service is working
                //var count = Interlocked.Increment(ref executionCount);
                Console.WriteLine("****PushToHub****" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                // Log.Information("LOS.ThirdPartyProccesor  PushToHub: Start App");
                try
                {
                    var loanCredits = _loanBriefServices.WaitPushToHub();
                    if (loanCredits != null && loanCredits.Count > 0)
                    {
                        //Parallel.ForEach(loanCredits, t => PushToHub(t));
                        foreach (var item in loanCredits)
                        {
                            //Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToHub: Start HĐ-{0}", item.LoanBriefId));
                            Console.WriteLine(string.Format("PushToHub: Xu ly don HĐ-{0} {1}", item.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            PushToHub(item);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor PushLoanCreditToHub exception");
                }
                //Thread 1 minute
                Thread.Sleep(1000 * 30);
            }         
        }

        private void PushToHub(LoanBrief entity)
        {
            try
            {
                //var objHub = entity.WardId > 0 ? _shopServices.GetHubByWard(entity.WardId.Value) : null;
                //fix hub001
                var objHub = entity.WardId > 0 ? _shopServices.GetHubByWard(7403, entity.ProductId.Value) : null;
                if (objHub == null || objHub.ShopId == 0)
                {
                    if (entity.DistrictId > 0)
                        objHub = _shopServices.GetHubByDistrict(entity.DistrictId.Value, entity.ProductId.Value);
                    if (objHub == null || objHub.ShopId == 0)
                    {

                        //lưu comment trả lại đơn
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = string.Format("System: Trả lại đơn do khu vực chưa được cấu hình"),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        _loanBriefServices.AddNote(note);
                        //trả đơn lại cho telesale cập nhật
                        _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Back.GetHashCode());

                        //Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToHub: Back LoanBrief HĐ-{0}", entity.LoanBriefId));

                    }
                }

                if (objHub != null && objHub.ShopId > 0)
                {
                    if (_loanBriefServices.UpdateHub(entity.LoanBriefId, objHub.ShopId))
                    {
                        //đẩy đơn lên luồng xử lý
                        _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Push.GetHashCode());
                        //lưu lại comment
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = string.Format("System: Hệ thống đã đẩy đơn về cho {0}", objHub.Name),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.DistributingLoanToHub.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        _loanBriefServices.AddNote(note);
                        //tăng số đơn nhận của hub lên
                        _shopServices.UpdateNumberReceived(objHub.ShopId);
                        //Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToHub: Done HĐ-{0}", entity.LoanBriefId));
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor PushToHub exception");
            }
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            // _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
            //_timer?.Dispose();
        }

    }
}
