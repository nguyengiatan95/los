﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class PushToTDHSJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly IUserService _userServices;
        private readonly IServiceAI _serviceAI;
        private ILogLoanInfoAIService _logLoanInfoAIService;
        private readonly IErpService _erpServices;
        private readonly ILogDistributionUserService _logDistributionUserService;
        private readonly ILogLoanInfoAIService _logLoaninfoAiServices;
        private readonly ILMSService _lMSService;
        private readonly IRuleCheckLoanService _ruleCheckLoanService;
        private readonly IAuthenticationAI _authenticationAI;
        //private Timer _timer;
        private Thread _thread;
        Dictionary<int, Bank> dicBank = new Dictionary<int, Bank>();
        List<Bank> lstBank = new List<Bank>();
        public PushToTDHSJob(ILoanBriefService loanBriefServices, IUserService userServices, IServiceAI serviceAI,
            ILogLoanInfoAIService logLoanInfoAIService, IErpService erpServices, ILogDistributionUserService logDistributionUserService,
            ILogLoanInfoAIService logLoaninfoAiServices, ILMSService lMSService, IRuleCheckLoanService ruleCheckLoanService,
            IAuthenticationAI authenticationAI)
        {
            _loanBriefServices = loanBriefServices;
            _userServices = userServices;
            _serviceAI = serviceAI;
            _logLoanInfoAIService = logLoanInfoAIService;
            _erpServices = erpServices;
            _logDistributionUserService = logDistributionUserService;
            _logLoaninfoAiServices = logLoaninfoAiServices;
            _lMSService = lMSService;
            _ruleCheckLoanService = ruleCheckLoanService;
            _authenticationAI = authenticationAI;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            GetListBank();
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        private List<Bank> GetListBank()
        {
            try
            {
                lstBank = _loanBriefServices.GetBankAll();
            }
            catch (Exception ex)
            {

                lstBank = null;
            }
            return lstBank;
        }
        private void DoWork(object state)
        {
            while (true)
            {
                try
                {
                    var loanCredits = _loanBriefServices.WaitPushToTDHS();
                    if (loanCredits != null && loanCredits.Count > 0)
                    {
                        foreach (var item in loanCredits)
                        {
                            //Check nợ xấu Momo
                            //bỏ qua đơn cấu trúc lại nợ
                            if (item.TypeRemarketing != (int)EnumTypeRemarketing.DebtRevolvingLoan && item.TypeRemarketing != (int)EnumTypeRemarketing.IsTopUp)
                            {
                                //Không kiểm tra đơn tái vay
                                if (!item.IsReborrow.GetValueOrDefault(false))
                                {
                                    var ruleCheckLoan = _ruleCheckLoanService.GetByLoanbriefId(item.LoanBriefId);
                                    var isCheckMomo = true;
                                    if (ruleCheckLoan != null)
                                        isCheckMomo = ruleCheckLoan.IsCheckMomo.GetValueOrDefault(false);
                                    //Check Loan Momo
                                    if (isCheckMomo)
                                    {
                                        if (!CheckLoanInfoMomo(item))
                                            continue;
                                    }
                                }
                                if (!CheckBankAI(item))
                                    continue;

                                //Gửi request lấy TTTB
                                SendRequestTTTB(item);
                                //Gửi request lấy thông tin ảnh đăng ký xe
                                SendRequestMotorbikeRegistrationCertificate(item);
                                //Gửi request lấy thông tin cmnd
                                SendRequestEkycNationalCard(item);
                                //Log.Information(string.Format("LOS.ThirdPartyProccesor  PushToTDHS: Start HĐ-{0}", item.LoanBriefId));
                                SendRequestFraudDetection(item);

                                ParseVinNumber(item);
                            }
                            PushToTDHS(item);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor PushLoanCreditToTDHS exception");
                }
                //Thread 1 minute
                Thread.Sleep(1000 * 3);
            }
        }

        public void ParseVinNumber(LoanBrief entity)
        {
            try
            {
                //Nếu là gói xe máy => gửi request kiểm tra thông tin xe từ số vin
                if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                    || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                    || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                {
                    //Kiểm tra xem có số vin k
                    var vinNumber = _loanBriefServices.GetVinNumber(entity.LoanBriefId);
                    if (!string.IsNullOrEmpty(vinNumber))
                    {
                        bool getVinInfo = true;
                        //Kiểm tra xem số vin đó đã có kết quả chưa. Nếu chưa có thì mới get lại
                        var logData = _logLoanInfoAIService.GetLastRequest(entity.LoanBriefId, (int)ServiceTypeAI.ParseVinNumber);
                        if (logData != null && logData.Status == 1
                            && logData.ResultFinal == "SUCCESS"
                            && !string.IsNullOrEmpty(logData.ResponseRequest)
                            && String.Compare(logData.Request, vinNumber, true) == 0)
                            getVinInfo = false;

                        if (getVinInfo)
                            _logLoaninfoAiServices.AddLog(new LogLoanInfoAi()
                            {
                                LoanbriefId = entity.LoanBriefId,
                                ServiceType = (int)ServiceTypeAI.ParseVinNumber,
                                IsExcuted = EnumLogLoanInfoAiExcuted.WaitRequest.GetHashCode(),
                                CreatedAt = DateTime.Now
                            });
                    }

                }
            }
            catch
            {

            }

        }

        private void PushToTDHS(LoanBrief entity)
        {
            try
            {

                bool saveLogDistributed = false;
                bool countRecord = true;
                var coordinatorUser = new User();
                if (entity.CoordinatorUserId > 0)
                {
                    coordinatorUser = _userServices.GetCoordinatorUser(entity.CoordinatorUserId.Value);
                    if (coordinatorUser != null && coordinatorUser.UserId > 0)
                        countRecord = false;
                }
                if (countRecord || entity.CoordinatorUserId == 0 || entity.CoordinatorUserId == null)
                {
                    //Nếu là gói cầm cố hàng hóa thì chia mặc định cho td_sonml : 49500
                    if (entity.ProductId == EnumProductCredit.CamCoHangHoa.GetHashCode())
                    {
                        coordinatorUser.UserId = 49500;
                    }
                    else
                    {
                        //kiểm tra danh sách user thẩm định đã checkin
                        var listUserId = new List<int>();
                        //var listUserCoordinator = _erpServices.GetUserCoordinatorCheckIn(entity.LoanBriefId);
                        //var comment = "Các nhân viên thẩm định trong ca làm việc: ";
                        //if (listUserCoordinator != null && listUserCoordinator.Count > 0)
                        //{
                        //    listUserId = listUserCoordinator.Select(x => x.losId).ToList();
                        //    foreach (var item in listUserCoordinator)
                        //    {
                        //        if (!string.IsNullOrEmpty(item.fullname))
                        //            comment += string.Format("<br /> + <b>{0}</b>", item.fullname);
                        //    }
                        //}
                        //else
                        //{
                        //    comment = "Chưa có nhân viên thẩm định nào checkin";
                        //}
                        //_loanBriefServices.AddNote(new LoanBriefNote
                        //{
                        //    LoanBriefId = entity.LoanBriefId,
                        //    Note = comment,
                        //    FullName = "Auto System",
                        //    Status = 1,
                        //    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        //    CreatedTime = DateTime.Now,
                        //    UserId = 1,
                        //    ShopId = EnumShop.Tima.GetHashCode(),
                        //    ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        //});
                        coordinatorUser = _userServices.GetCoordinatorUser(listUserId != null && listUserId.Count > 0 ? listUserId : null);
                    }
                    saveLogDistributed = true;
                }
                if (coordinatorUser != null && coordinatorUser.UserId > 0)
                {
                    if (_loanBriefServices.UpdateTDHS(entity.LoanBriefId, coordinatorUser.UserId))
                    {
                        //đẩy đơn lên luồng xử lý
                        _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Push.GetHashCode());
                        //lưu lại comment
                        _loanBriefServices.AddNote(new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = string.Format("System: Hệ thống đã đẩy đơn về cho TĐHS {0}", coordinatorUser.FullName),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.DistributingLoanTDHS.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        //tăng số đơn nhận của hub lên
                        if (countRecord)
                            _userServices.UpdateNumberReceivedCoordinator(coordinatorUser.UserId);

                        //Thêm log chia đơn cho tdhs
                        if (saveLogDistributed)
                            _logDistributionUserService.Insert(new LogDistributionUser()
                            {
                                UserId = coordinatorUser.UserId,
                                LoanbriefId = entity.LoanBriefId,
                                CreatedAt = DateTime.Now,
                                TypeDistribution = (int)TypeDistributionLog.TDHS
                            });

                        Console.WriteLine(string.Format("PushToTDHS: Xu ly don HĐ-{0} {1}", entity.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("LOS.ThirdPartyProccesor PushToTDHS exception: {0}", ex.ToString()));
                Log.Error(ex, "LOS.ThirdPartyProccesor PushToTDHS exception");
            }
        }

        private void SendRequestTTTB(LoanBrief entity)
        {
            try
            {
                if (entity.LoanBriefId > 0)
                {
                    var lastRequest = _logLoanInfoAIService.GetLastRequest(entity.LoanBriefId, (int)ServiceTypeAI.EkycGetInfoNetwork);
                    var isCheck = false;
                    var modelEkycNetwork = _serviceAI.PrepareEkycInfoNetWorkModel(entity.LoanBriefId, ref isCheck);
                    if (lastRequest != null)
                    {
                        if (isCheck && modelEkycNetwork != null)
                        {
                            if (String.Compare(lastRequest.Request.ToString(), JsonConvert.SerializeObject(modelEkycNetwork).ToString(), true) == 0)
                                isCheck = false;
                        }
                    }
                    if (isCheck && modelEkycNetwork != null)
                    {
                        //Thêm request lấy thông tin ảnh TTTB
                        _logLoanInfoAIService.AddLog(new LogLoanInfoAi()
                        {
                            LoanbriefId = entity.LoanBriefId,
                            ServiceType = (int)ServiceTypeAI.EkycGetInfoNetwork,
                            CreatedAt = DateTime.Now,
                            IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest,
                            FromApp = "TDHS"
                        });
                        Console.WriteLine(string.Format("TDHS: REQUEST EKYC NETWORK HĐ-{0} {1}", entity.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void SendRequestEkycNationalCard(LoanBrief entity)
        {
            try
            {
                if (entity.LoanBriefId > 0)
                {
                    var lastRequest = _logLoanInfoAIService.GetLastRequest(entity.LoanBriefId, (int)ServiceTypeAI.Ekyc);
                    var isCheck = false;
                    var modelEkycNationalCard = _serviceAI.PrepareEkycModel(entity.LoanBriefId, ref isCheck);
                    if (lastRequest != null)
                    {
                        if (isCheck && modelEkycNationalCard != null && !string.IsNullOrEmpty(lastRequest.Request))
                        {
                            if (String.Compare(lastRequest.Request.ToString(), JsonConvert.SerializeObject(modelEkycNationalCard).ToString(), true) == 0)
                                isCheck = false;
                        }
                    }
                    if (isCheck && modelEkycNationalCard != null)
                    {
                        //Thêm request lấy thông tin ảnh TTTB
                        _logLoanInfoAIService.AddLog(new LogLoanInfoAi()
                        {
                            LoanbriefId = entity.LoanBriefId,
                            ServiceType = (int)ServiceTypeAI.Ekyc,
                            CreatedAt = DateTime.Now,
                            IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest,
                            FromApp = "TDHS"
                        });
                        Console.WriteLine(string.Format("TDHS: REQUEST EKYC National HĐ-{0} {1}", entity.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void SendRequestMotorbikeRegistrationCertificate(LoanBrief entity)
        {
            try
            {
                if (entity.LoanBriefId > 0)
                {
                    var lastRequest = _logLoanInfoAIService.GetLastRequest(entity.LoanBriefId, (int)ServiceTypeAI.EkycMotorbikeRegistrationCertificate);
                    var isCheck = false;
                    var modelEkycMotorbikeRegistrationCertificate = _serviceAI.PrepareEkycMotorbikeRegistrationCertificateModel(entity.LoanBriefId, ref isCheck);
                    if (lastRequest != null)
                    {
                        if (isCheck && modelEkycMotorbikeRegistrationCertificate != null)
                        {
                            if (String.Compare(lastRequest.Request.ToString(), JsonConvert.SerializeObject(modelEkycMotorbikeRegistrationCertificate).ToString(), true) == 0)
                                isCheck = false;
                        }
                    }
                    if (isCheck && modelEkycMotorbikeRegistrationCertificate != null)
                    {
                        //Thêm request lấy thông tin ảnh đăng ký xe
                        _logLoanInfoAIService.AddLog(new LogLoanInfoAi()
                        {
                            LoanbriefId = entity.LoanBriefId,
                            ServiceType = (int)ServiceTypeAI.EkycMotorbikeRegistrationCertificate,
                            CreatedAt = DateTime.Now,
                            IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest,
                            FromApp = "TDHS"
                        });
                        Console.WriteLine(string.Format("TDHS: REQUEST EKYC Motorbike Registration Certificate HĐ-{0} {1}", entity.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void SendRequestFraudDetection(LoanBrief entity)
        {
            try
            {
                if (entity.LoanBriefId > 0)
                {
                    var lastRequest = _logLoanInfoAIService.GetLastRequest(entity.LoanBriefId, (int)ServiceTypeAI.FraudDetection);
                    if (lastRequest != null && lastRequest.Status == 1)
                        return;

                    var registerFraud = _serviceAI.RegisterFraudDetection(entity.LoanBriefId);
                    if (registerFraud != null && registerFraud.response_code == 200)
                        Console.WriteLine(string.Format("TDHS: REQUEST FRAUD DETECTION HĐ-{0} {1}", entity.LoanBriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
            }
            catch (Exception ex)
            {
            }
        }

        private bool CheckBankAI(LoanBrief entity)
        {
            var check = true;
            try
            {
                if (lstBank != null && lstBank.Count > 0)
                {
                    if (dicBank.Count <= 0)
                    {
                        foreach (var item in lstBank)
                        {
                            if (!dicBank.ContainsKey(item.BankId))
                                dicBank[item.BankId] = new Bank();
                            dicBank[item.BankId] = item;
                        }
                    }

                }
                //var dicBank = new Dictionary<int, Bank>();
                if (dicBank.ContainsKey(entity.BankId.Value) && !string.IsNullOrEmpty(entity.FullName))
                {
                    var checkBank = _serviceAI.CheckBank(entity.LoanBriefId, entity.BankAccountNumber, dicBank[entity.BankId.Value].BankCode);
                    if (checkBank.Status == 0 && !string.IsNullOrEmpty(checkBank.Data.AccName))
                    {
                        //Xử lý so sánh
                        if (!entity.FullName.IsNormalized())
                            entity.FullName = entity.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                        entity.FullName = entity.FullName.ReduceWhitespace();
                        entity.FullName = entity.FullName.TitleCaseString();

                        entity.BankAccountName = checkBank.Data.AccName.ReduceWhitespace();
                        entity.BankAccountName = entity.BankAccountName.TitleCaseString();
                        entity.BankAccountName = ConvertExtensions.ConvertToUnSign(entity.BankAccountName);
                        if (String.Compare(ConvertExtensions.ConvertToUnSign(entity.FullName), entity.BankAccountName, false) != 0)
                        {
                            var note = new LoanBriefNote
                            {
                                LoanBriefId = entity.LoanBriefId,
                                Note = String.Format("System: Đơn vay bị trả lại do tên kh và tên chủ tk không khớp nhau - {0}", checkBank.Data.AccName),
                                FullName = "Auto System",
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = 1,
                                ShopId = EnumShop.Tima.GetHashCode(),
                                ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                            };
                            _loanBriefServices.AddNote(note);
                            // trả đơn lại cho telesale cập nhật
                            _loanBriefServices.PushToPipeline(entity.LoanBriefId, EnumActionPush.Back.GetHashCode());
                            check = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return check;
            }
            return check;
        }

        private bool CheckLoanInfoMomo(LoanBrief entity)
        {
            var check = true;
            try
            {
                var logCheckMomo = _logLoanInfoAIService.GetLastRequest(entity.LoanBriefId, (int)ServiceTypeAI.CheckLoanMomo);
                if (logCheckMomo != null && logCheckMomo.Status == 1
                    && !string.IsNullOrEmpty(logCheckMomo.ResponseRequest)
                    && logCheckMomo.CreatedAt.Value.Day != DateTime.Now.Day
                    && String.Compare(logCheckMomo.Request, entity.NationalCard, true) == 0)
                {
                    return true;
                }
                var numBillBad = 0;
                var isCancel = false;
                var isBackList = false;
                var noteStr = "";
                var result = _serviceAI.CheckLoanMomo(entity.LoanBriefId, entity.NationalCard, "TDHS");
                if (result != null)
                {
                    var billsBad = result.Data.Bills.Where(x => x.Status == "BAD").Count();

                    // Nếu Khách hàng đang có 1 khoản vay là nợ xấu tại các tổ chức khác: Chặn đơn vay sản phẩm XMKCC, XMKGT.
                    if (billsBad == 1)
                    {
                        if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                        {
                            isCancel = true;
                            noteStr = "Auto System: Hủy đơn do khách hàng có 1 khoản vay nợ xấu ở tổ chức khác";
                        }
                        else
                            numBillBad = 1;
                    }
                    //Nếu Khách hàng đang có từ 2 khoản vay là nợ xấu tại các tổ chức khác trở lên: Chặn đơn vay và cho vào blacklist.
                    else if (billsBad >= 2)
                    {
                        isCancel = true;
                        isBackList = true;
                        noteStr = string.Format("Auto System: Hủy đơn do khách hàng có {0} khoản vay nợ xấu ở tổ chức khác", billsBad);
                    }
                }

                if (!isCancel && !string.IsNullOrEmpty(entity.NationCardPlace))
                {
                    var result2 = _serviceAI.CheckLoanMomo(entity.LoanBriefId, entity.NationCardPlace, "TDHS");
                    if (result2 != null)
                    {
                        var billsBad = result2.Data.Bills.Where(x => x.Status == "BAD").Count();
                        // Nếu Khách hàng đang có 1 khoản vay là nợ xấu tại các tổ chức khác: Chặn đơn vay sản phẩm XMKCC, XMKGT.
                        if (billsBad == 1)
                        {
                            if (numBillBad > 0)
                            {
                                isCancel = true;
                                isBackList = true;
                                noteStr = string.Format("Auto System: Hủy đơn do khách hàng có {0} khoản vay nợ xấu ở tổ chức khác", billsBad + numBillBad);
                            }
                            else
                            {
                                if (entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                {
                                    isCancel = true;
                                    noteStr = "Auto System: Hủy đơn do khách hàng có 1 khoản vay nợ xấu ở tổ chức khác";
                                }
                            }
                        }
                        //Nếu Khách hàng đang có từ 2 khoản vay là nợ xấu tại các tổ chức khác trở lên: Chặn đơn vay và cho vào blacklist.
                        else if (billsBad >= 2)
                        {
                            isCancel = true;
                            isBackList = true;
                            noteStr = string.Format("Auto System: Hủy đơn do khách hàng có {0} khoản vay nợ xấu ở tổ chức khác", billsBad);
                        }
                    }
                }
                if (isCancel)
                {
                    if (_loanBriefServices.CancelLoan(entity.LoanBriefId, 759, "Auto System: Hệ thống hủy tự động do có nhiều khoản vay ở bên khác"))
                    {
                        _loanBriefServices.AddNote(new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanBriefId,
                            Note = noteStr,
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.AutoCancel.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        });

                        //Nếu là đơn định vị => hủy hđ định vị
                        if (entity.IsLocate == true && !string.IsNullOrEmpty(entity.DeviceId))
                        {
                            var token = _authenticationAI.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH);
                            if (!string.IsNullOrEmpty(token))
                            {
                                var resultOpent = _serviceAI.CloseContract("HD-" + entity.LoanBriefId, entity.DeviceId, token, entity.LoanBriefId);
                                if (resultOpent)
                                {
                                    _loanBriefServices.AddNote(new LoanBriefNote()
                                    {
                                        LoanBriefId = entity.LoanBriefId,
                                        Note = string.Format("Đơn định vị: Đóng hợp đồng định vị với imei: {0}", entity.DeviceId),
                                        FullName = "Auto System",
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        ShopId = EnumShop.Tima.GetHashCode(),
                                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                    });

                                }
                            }
                        }

                        check = false;
                    }
                }
                if (isBackList)
                {
                    //Add BlackList
                    _lMSService.AddBlackList(new RequestAddBlacklist()
                    {
                        CustomerCreditId = entity.CustomerId ?? 0,
                        FullName = entity.FullName,
                        NumberPhone = entity.Phone,
                        CardNumber = entity.NationalCard,
                        BirthDay = entity.Dob ?? null,
                        UserIdCreate = 1,
                        UserNameCreate = "Auto System",
                        FullNameCreate = "Auto System",
                        Note = noteStr,
                        LoanCreditId = entity.LoanBriefId
                    });
                }
            }
            catch (Exception ex)
            {
                return check;
            }
            return check;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
        }
    }
}
