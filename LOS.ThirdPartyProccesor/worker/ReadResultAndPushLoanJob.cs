﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.ThirdPartyProccesor.Helper;
using LOS.ThirdPartyProccesor.Objects;
using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.ThirdPartyProccesor.worker
{
    public class ReadResultAndPushLoanJob : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefServices;
        private readonly ILocationService _locationService;
        private readonly IServiceAI _serviceAI;
        private readonly INetworkService _networkService;
        private readonly ILogLoanInfoAIService _logLoanInfoAIService;
        private readonly IConfiguration _configuration;
        private CancellationTokenSource _cts;
        // private Timer _timer;
        private Thread _thread;
        public ReadResultAndPushLoanJob(ILoanBriefService loanBriefServices, ILocationService locationService, IServiceAI serviceAI, INetworkService networkService, ILogLoanInfoAIService logLoanInfoAIService)
        {
            _loanBriefServices = loanBriefServices;
            _locationService = locationService;
            _serviceAI = serviceAI;
            _networkService = networkService;
            _logLoanInfoAIService = logLoanInfoAIService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _configuration = builder.Build();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            //_timer = new Timer(DoWork, null, TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(300));
            _cts = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {

            while (true)
            {
                var configTimeoutCac = int.Parse(_configuration["AppSettings:configTimeoutCac"]);
                var configTimeoutLocation = int.Parse(_configuration["AppSettings:configTimeoutLocation"]);
                try
                {
                    var dicData = new Dictionary<int, List<LogLoanInfoAi>>();
                    var listData = _logLoanInfoAIService.GetListResult();
                    if (listData != null && listData.Count > 0)
                    {
                        foreach (var item in listData)
                        {
                            if (item.ServiceType == (int)ServiceTypeAI.RuleCheck && item.IsExcuted == (int)EnumLogLoanInfoAiExcuted.WaitHandle && (DateTime.Now - item.CreatedAt.Value).TotalMinutes < configTimeoutCac)
                                continue;
                            if (item.ServiceType == (int)ServiceTypeAI.Location && item.IsExcuted == (int)EnumLogLoanInfoAiExcuted.WaitHandle && (DateTime.Now - item.CreatedAt.Value).TotalMinutes < configTimeoutLocation)
                                continue;
                            if (item.ServiceType == (int)ServiceTypeAI.RangeCreditScoring && item.IsExcuted == (int)EnumLogLoanInfoAiExcuted.WaitHandle && (DateTime.Now - item.CreatedAt.Value).TotalMinutes < configTimeoutCac)
                                continue;
                            if (item.ServiceType == (int)ServiceTypeAI.RefPhone && item.IsExcuted == (int)EnumLogLoanInfoAiExcuted.WaitHandle && (DateTime.Now - item.CreatedAt.Value).TotalMinutes < configTimeoutLocation)
                                continue;
                            if (item.ServiceType == (int)ServiceTypeAI.EkycMotorbikeRegistrationCertificate)
                                continue;
                            if (!dicData.ContainsKey(item.LoanbriefId.Value))
                                dicData[item.LoanbriefId.Value] = new List<LogLoanInfoAi>();
                            dicData[item.LoanbriefId.Value].Add(item);
                        }
                    }
                    if (dicData != null && dicData.Keys.Count > 0)
                    {
                        foreach (var loanBriefId in dicData.Keys)
                        {
                            //kiểm tra có log kết quả location
                            if (dicData[loanBriefId].Any(x => x.ServiceType == (int)ServiceTypeAI.Location))
                            {
                                //nếu có log => lấy row cuối cùng ra xử lý, bỏ qua các row trước đó
                                var lastLogLocation = dicData[loanBriefId].Where(x => x.ServiceType == (int)ServiceTypeAI.Location && !string.IsNullOrEmpty(x.RefCode)).OrderByDescending(x => x.Id).FirstOrDefault();
                                if (lastLogLocation == null || lastLogLocation.Id == 0) continue;
                                //hủy tất cả các request trước đó
                                _logLoanInfoAIService.CancelRequestBefore((int)ServiceTypeAI.Location, lastLogLocation.LoanbriefId.Value, lastLogLocation.Id);
                                if (lastLogLocation.IsExcuted == (int)EnumLogLoanInfoAiExcuted.HasResult && !string.IsNullOrEmpty(lastLogLocation.HomeNetwok))
                                {
                                    Console.WriteLine(string.Format("Read location: {0} {1}", lastLogLocation.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                    if (lastLogLocation.HomeNetwok.Trim() == "viettel")
                                    {
                                        ReadLocationViettel(lastLogLocation);
                                    }
                                    else if (lastLogLocation.HomeNetwok.Trim() == "mobifone")
                                    {
                                        ReadLocationMobi(lastLogLocation);
                                    }
                                }
                                //Quá thời hạn đọc dữ liệu => đánh dấu hoàn thành 
                                else if (lastLogLocation.IsExcuted == (int)EnumLogLoanInfoAiExcuted.WaitHandle && (DateTime.Now - lastLogLocation.CreatedAt.Value).TotalMinutes > configTimeoutLocation)
                                {
                                    lastLogLocation.ResultFinal = "OverTime";
                                    if (_logLoanInfoAIService.Done(lastLogLocation) > 0)
                                        Console.WriteLine(string.Format("Read location Done: {0} {1} {2}", lastLogLocation.LoanbriefId, lastLogLocation.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                    else
                                        Console.WriteLine(string.Format("Read location ERROR: {0} {1} {2}", lastLogLocation.LoanbriefId, lastLogLocation.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                }
                                // var logLocation = dicData[loanBriefId].Where(x => x.ServiceType == (int)ServiceTypeAI.Location).OrderByDescending(x => x.Id).FirstOrDefault();
                            }
                            //kiểm tra nếu đã có kết quả CAC hoặc quá thời gian timeount
                            //=> đọc kết quả 
                            //=> Đẩy đơn lên bước tiếp theo
                            if (dicData[loanBriefId].Any(x => x.ServiceType == (int)ServiceTypeAI.RuleCheck))
                            {
                                var LogLoanInfoAiItem = dicData[loanBriefId].Where(x => x.ServiceType == (int)ServiceTypeAI.RuleCheck).OrderByDescending(x => x.Id).FirstOrDefault();
                                //hủy tất cả các request trước đó
                                _logLoanInfoAIService.CancelRequestBefore((int)ServiceTypeAI.RuleCheck, LogLoanInfoAiItem.LoanbriefId.Value, LogLoanInfoAiItem.Id);
                                //nếu đã có kết quả
                                if (LogLoanInfoAiItem.IsExcuted == (int)EnumLogLoanInfoAiExcuted.HasResult)
                                {
                                    Console.WriteLine(string.Format("Read Rucheck: {0} {1}", LogLoanInfoAiItem.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                    if (!string.IsNullOrEmpty(LogLoanInfoAiItem.Response))
                                    {
                                        var finalResult = string.Empty;
                                        ReadRuleCheck(LogLoanInfoAiItem, ref finalResult);
                                        //hủy đơn
                                        if (!string.IsNullOrEmpty(finalResult))
                                        {
                                            _loanBriefServices.UpdateResultRuleCheck(LogLoanInfoAiItem.LoanbriefId.Value, finalResult);
                                            //if (finalResult == "NO_PASS")
                                            //{
                                            //    _loanBriefServices.AddNote(new LoanBriefNote
                                            //    {
                                            //        LoanBriefId = LogLoanInfoAiItem.LoanbriefId,
                                            //        Note = string.Format("Rule Cac: Hủy đơn"),
                                            //        FullName = "Auto System",
                                            //        Status = 1,
                                            //        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                            //        CreatedTime = DateTime.Now,
                                            //        UserId = 1,
                                            //        ShopId = EnumShop.Tima.GetHashCode(),
                                            //        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                            //    });
                                            //    _loanBriefServices.CancelLoan(LogLoanInfoAiItem.LoanbriefId.Value, StateDetailsReason.CheckCic.GetHashCode(), "Hệ thống tự động hủy NO_PASS: Rule CAC AI");
                                            //}
                                            //else
                                            //{
                                            //đẩy đơn lên bước tiếp theo
                                            if (LogLoanInfoAiItem.PushNextState == 1)
                                            {
                                                _loanBriefServices.PushLoan(LogLoanInfoAiItem.LoanbriefId.Value);
                                            }

                                            //}
                                        }

                                    }
                                }
                                //nếu chưa có kết quả + quá thời hạn => đẩy đơn đi
                                else if (LogLoanInfoAiItem.IsExcuted == (int)EnumLogLoanInfoAiExcuted.WaitHandle && (DateTime.Now - LogLoanInfoAiItem.CreatedAt.Value).TotalMinutes > configTimeoutCac)
                                {
                                    _loanBriefServices.AddNote(new LoanBriefNote
                                    {
                                        LoanBriefId = LogLoanInfoAiItem.LoanbriefId,
                                        Note = string.Format("Quá {0} phút không lấy được thông tin Rule Cac. Hệ thống tự động đẩy đơn đi tiếp", configTimeoutCac),
                                        FullName = "Auto System",
                                        Status = 1,
                                        ActionComment = EnumActionComment.ResultRuleCheck.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = 1,
                                        ShopId = EnumShop.Tima.GetHashCode(),
                                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                    });
                                    //đánh dấu đã xử lý
                                    LogLoanInfoAiItem.ResultFinal = "OverTime";
                                    if (_logLoanInfoAIService.Done(LogLoanInfoAiItem) > 0)
                                        Console.WriteLine(string.Format("Read Rucheck Done: {0} {1} {2}", LogLoanInfoAiItem.LoanbriefId, LogLoanInfoAiItem.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                    else
                                        Console.WriteLine(string.Format("Read Rucheck ERROR: {0} {1} {2}", LogLoanInfoAiItem.LoanbriefId, LogLoanInfoAiItem.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                    //đẩy đơn lên bước tiếp theo
                                    if (LogLoanInfoAiItem.PushNextState == 1)
                                    {
                                        _loanBriefServices.PushLoan(LogLoanInfoAiItem.LoanbriefId.Value);
                                    }
                                }
                            }

                            //Đọc dữ liệu kết quả RefPhone
                            if (dicData[loanBriefId].Any(x => x.ServiceType == (int)ServiceTypeAI.RefPhone))
                            {
                                var LogLoanInfoAiItem = dicData[loanBriefId].Where(x => x.ServiceType == (int)ServiceTypeAI.RefPhone).OrderByDescending(x => x.Id).FirstOrDefault();
                                //hủy tất cả các request trước đó
                                _logLoanInfoAIService.CancelRequestBefore((int)ServiceTypeAI.RefPhone, LogLoanInfoAiItem.LoanbriefId.Value, LogLoanInfoAiItem.Id);
                                //nếu đã có kết quả
                                if (LogLoanInfoAiItem.IsExcuted == (int)EnumLogLoanInfoAiExcuted.HasResult)
                                {
                                    Console.WriteLine(string.Format("Read Refphone: {0} {1}", LogLoanInfoAiItem.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                    if (!string.IsNullOrEmpty(LogLoanInfoAiItem.Response))
                                    {
                                        if (LogLoanInfoAiItem.HomeNetwok == "viettel")
                                        {
                                            ReadRefPhone(LogLoanInfoAiItem);
                                        }
                                        else if (LogLoanInfoAiItem.HomeNetwok == "mobifone")
                                        {
                                            ReadRefPhoneMobi(LogLoanInfoAiItem);
                                        }

                                    }
                                    else
                                    {
                                        LogLoanInfoAiItem.ResultFinal = "NOT_VALUE";
                                        //đánh dấu đọc xong dữ liệu
                                        _logLoanInfoAIService.Done(LogLoanInfoAiItem);
                                    }
                                    
                                }
                                else if (LogLoanInfoAiItem.IsExcuted == (int)EnumLogLoanInfoAiExcuted.WaitHandle && (DateTime.Now - LogLoanInfoAiItem.CreatedAt.Value).TotalMinutes > configTimeoutLocation)
                                {
                                    //đánh dấu đọc xong dữ liệu
                                    LogLoanInfoAiItem.ResultFinal = "OverTime";
                                    //đánh dấu đọc xong dữ liệu
                                    if (_logLoanInfoAIService.Done(LogLoanInfoAiItem) > 0)
                                        Console.WriteLine(string.Format("Read Refphone OverTime Done: {0} {1} {2}", LogLoanInfoAiItem.LoanbriefId, LogLoanInfoAiItem.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                    else
                                        Console.WriteLine(string.Format("Read Refphone OverTime ERROR: {0} {1} {2}", LogLoanInfoAiItem.LoanbriefId, LogLoanInfoAiItem.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                }
                            }

                            //Đọc dữ liệu kết quả credit scoring
                            if (dicData[loanBriefId].Any(x => x.ServiceType == (int)ServiceTypeAI.RangeCreditScoring))
                            {
                                var LogLoanInfoAiItem = dicData[loanBriefId].Where(x => x.ServiceType == (int)ServiceTypeAI.RangeCreditScoring).OrderByDescending(x => x.Id).FirstOrDefault();
                                //hủy tất cả các request trước đó
                                _logLoanInfoAIService.CancelRequestBefore((int)ServiceTypeAI.RangeCreditScoring, LogLoanInfoAiItem.LoanbriefId.Value, LogLoanInfoAiItem.Id);
                                //nếu đã có kết quả
                                if (LogLoanInfoAiItem.IsExcuted == (int)EnumLogLoanInfoAiExcuted.HasResult)
                                {
                                    Console.WriteLine(string.Format("Read CreditScoring: {0} {1}", LogLoanInfoAiItem.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                    if (!string.IsNullOrEmpty(LogLoanInfoAiItem.Response))
                                    {
                                        //Read response credit scoring
                                        ReadRangeCreditScoring(LogLoanInfoAiItem);
                                    }
                                }
                                //nếu chưa có kết quả + quá thời hạn => đẩy đơn đi
                                else if (LogLoanInfoAiItem.IsExcuted == (int)EnumLogLoanInfoAiExcuted.WaitHandle && (DateTime.Now - LogLoanInfoAiItem.CreatedAt.Value).TotalMinutes > configTimeoutCac)
                                {
                                    //đánh dấu đọc xong dữ liệu
                                    LogLoanInfoAiItem.ResultFinal = "OverTime";
                                    if (_logLoanInfoAIService.Done(LogLoanInfoAiItem) > 0)
                                        Console.WriteLine(string.Format("Read CreditScoring Done: {0} {1} {2}", LogLoanInfoAiItem.LoanbriefId, LogLoanInfoAiItem.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                    else
                                        Console.WriteLine(string.Format("Read CreditScoring ERROR: {0} {1} {2}", LogLoanInfoAiItem.LoanbriefId, LogLoanInfoAiItem.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                                }
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LOS.ThirdPartyProccesor GetInfoFromRuleAI exception");
                }
                //Thread 1 minute
                Thread.Sleep(1000 * 30);
            }
        }
        private void ReadLocationViettel(LogLoanInfoAi entity)
        {
            try
            {
                var str = new StringBuilder();
                LocationViettelRes dataresult = Newtonsoft.Json.JsonConvert.DeserializeObject<LocationViettelRes>(entity.Response);
                if (dataresult != null && dataresult.status != 0)
                {
                    //lưu thông tin xuống comment
                    //lưu comment trả lại đơn
                    var note = new LoanBriefNote
                    {
                        LoanBriefId = entity.LoanbriefId,
                        Note = string.Format("Tracking location Viettel: {0}", dataresult.desc),
                        FullName = "Auto System",
                        Status = 1,
                        ActionComment = EnumActionComment.ResultLocationViettel.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 1,
                        ShopId = EnumShop.Tima.GetHashCode(),
                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                    };
                    _loanBriefServices.AddNote(note);
                    _loanBriefServices.UpdateResultLocation(entity.LoanbriefId.Value, dataresult.desc);
                    entity.ResultFinal = "NOT_VALUE";
                    if (_logLoanInfoAIService.Done(entity) > 0)
                        Console.WriteLine(string.Format("Read location VT Done: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    else
                        Console.WriteLine(string.Format("Read location VT ERROR: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
                if (dataresult != null && dataresult.status == 0)
                {
                    if (dataresult.data != null && dataresult.data.home != null && dataresult.data.office != null)
                    {
                        str.AppendFormat("<br /> - Địa chỉ nhà KH: {0} có số điểm: {1}<br />", dataresult.data.home.result, dataresult.data.home.point);
                        str.AppendFormat(" - Địa chỉ công ty KH: {0} có số điểm: {1}<br />", dataresult.data.office.result, dataresult.data.office.point);
                        //câp nhật kết quả location nhà và công ty
                        _loanBriefServices.UpdateResultLocation(entity.LoanbriefId.Value, dataresult.data.home.result, dataresult.data.office.result);
                    }
                    var locationInfo = dataresult.locations;
                    if (locationInfo.home != null)
                    {
                        var home = _locationService.GetNameAddress(decimal.Parse(locationInfo.home.latitude), decimal.Parse(locationInfo.home.longitude));
                        if (home == null || home == "")
                            home = string.Format("lat: {0} lng: {1}", locationInfo.home.latitude, locationInfo.home.longitude);
                        str.AppendFormat("Địa chỉ nhà: {0}({1},{2}) có tỉ lệ xuất hiện 7 ngày, 30 ngày, 90 ngày lần lượt là: {3} <br />", home, locationInfo.home.latitude, locationInfo.home.longitude, string.Join(", ", locationInfo.home.rates.Select(x => string.Format("{0}%", x))));
                        //var loanbriefResident = _loanBriefServices.GetLoanBriefResident(entity.LoanbriefId.Value);
                        //if(loanbriefResident != null && !string.IsNullOrEmpty(loanbriefResident.Address))
                        //{
                        //    var distance = _locationService.DistanceTwoPoint(loanbriefResident.Address, locationInfo.home.latitude, locationInfo.home.longitude);
                        //    if(distance != null && distance.distance != null)
                        //    {
                        //        if(distance.distance.value > 0 || !string.IsNullOrEmpty(distance.distance.text))
                        //        {
                        //            str.AppendFormat("Khoảng cách tới địa chỉ nơi ở({0}) là: {1}m({2})<br />", loanbriefResident.Address, distance.distance.value, distance.distance.text);
                        //        }
                        //    }
                        //}
                    
                    }

                    if (locationInfo.office != null)
                    {
                        var office = _locationService.GetNameAddress(decimal.Parse(locationInfo.office.latitude), decimal.Parse(locationInfo.office.longitude));
                        if (office == null || office == "")
                            office = string.Format("lat: {0} lng: {1}", locationInfo.office.latitude, locationInfo.office.longitude);
                        str.AppendFormat("Địa chỉ nơi làm việc: {0}({1},{2}) có tỉ lệ xuất hiện 7 ngày, 30 ngày, 90 ngày lần lượt là: {3} <br />", office, locationInfo.office.latitude, locationInfo.office.longitude, string.Join(", ", locationInfo.office.rates.Select(x => string.Format("{0}%", x))));

                        //var loanbriefJob = _loanBriefServices.GetLoanBriefJob(entity.LoanbriefId.Value);
                        //if (loanbriefJob != null && !string.IsNullOrEmpty(loanbriefJob.CompanyAddress))
                        //{
                        //    var distance = _locationService.DistanceTwoPoint(loanbriefJob.CompanyAddress, locationInfo.office.latitude, locationInfo.office.longitude);
                        //    if (distance != null && distance.distance != null)
                        //    {
                        //        if (distance.distance.value > 0 || !string.IsNullOrEmpty(distance.distance.text))
                        //        {
                        //            str.AppendFormat("Khoảng cách tới địa chỉ công ty ở({0}) là: {1}m({2})<br />", loanbriefJob.CompanyAddress, distance.distance.value, distance.distance.text);
                        //        }
                        //    }
                        //}
                    }

                    //lưu thông tin xuống comment
                    _loanBriefServices.AddNote(new LoanBriefNote
                    {
                        LoanBriefId = entity.LoanbriefId,
                        Note = string.Format("<b class='required-red'>[VIETTEL] KẾT QUẢ LOCATION NHÀ MẠNG </b>: {0}", str.ToString()),
                        FullName = "Auto System",
                        Status = 1,
                        ActionComment = EnumActionComment.ResultLocationViettel.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 1,
                        ShopId = EnumShop.Tima.GetHashCode(),
                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                    //chuyển trạng thái đã lấy đơn xong

                    //var message = string.Empty;
                    //if (dataresult.finalResult == "YES")
                    //{
                    //    message = "Thu  hồ sơ theo  checklist gói 1";
                    //}
                    //else if (dataresult.finalResult == "NO" || dataresult.finalResult == "UNKNOWN")
                    //{
                    //    message = "Thu  hồ sơ theo  checklist gói 2";
                    //}
                    //Cập nhật giá trị final + Đánh dấu đã xử lý bản ghi log để k xử lý lại
                    if (!string.IsNullOrEmpty(dataresult.finalResult))
                    {
                        _loanBriefServices.UpdateResultLocation(entity.LoanbriefId.Value, dataresult.finalResult);
                        entity.ResultFinal = dataresult.finalResult;
                        if (_logLoanInfoAIService.Done(entity) > 0)
                            Console.WriteLine(string.Format("Read location VT Done: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        else
                            Console.WriteLine(string.Format("Read location VT ERROR: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    }

                    //if (!string.IsNullOrEmpty(message))
                    //{
                    //    //lưu thông tin gợi ý gói vay
                    //    _loanBriefServices.AddNote(new LoanBriefNote
                    //    {
                    //        LoanBriefId = entity.LoanbriefId,
                    //        Note = message,
                    //        FullName = "Auto System",
                    //        Status = 1,
                    //        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    //        CreatedTime = DateTime.Now,
                    //        UserId = 1,
                    //        ShopId = EnumShop.Tima.GetHashCode(),
                    //        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                    //    });
                    //}
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor ReadLocationViettel exception");
            }
        }

        private void ReadLocationMobi(LogLoanInfoAi entity)
        {
            try
            {
                var str = new StringBuilder();
                LocationMobiRes dataresult = Newtonsoft.Json.JsonConvert.DeserializeObject<LocationMobiRes>(entity.Response);
                if (dataresult != null && dataresult.status != 0)
                {
                    
                    //lưu thông tin xuống comment
                    _loanBriefServices.AddNote(new LoanBriefNote
                    {
                        LoanBriefId = entity.LoanbriefId,
                        Note = string.Format("Tracking location Mobifone: {0}", dataresult.desc),
                        FullName = "Auto System",
                        Status = 1,
                        ActionComment = EnumActionComment.ResultLocationMobifone.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 1,
                        ShopId = EnumShop.Tima.GetHashCode(),
                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                    if (_logLoanInfoAIService.Done(entity) > 0)
                        Console.WriteLine(string.Format("Read location Mobi Done: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    else
                        Console.WriteLine(string.Format("Read location Mobi ERROR: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
                if (dataresult.data != null && dataresult.status == EnumRefCodeStatus.Success.GetHashCode())
                {
                    if (dataresult.result != null && dataresult.result.data2 != null)
                    {
                        if (dataresult.data != null && dataresult.data.home != null && dataresult.data.office != null)
                        {
                            str.AppendFormat("<br /> - Địa chỉ nhà KH: {0} có số điểm: {1}<br />", dataresult.data.home.result, dataresult.data.home.point);
                            str.AppendFormat(" - Địa chỉ công ty KH: {0} có số điểm: {1}<br />", dataresult.data.office.result, dataresult.data.office.point);
                            //câp nhật kết quả location nhà và công ty
                            _loanBriefServices.UpdateResultLocation(entity.LoanbriefId.Value, dataresult.data.home.result, dataresult.data.office.result);
                        }

                        if (dataresult.result != null && dataresult.result.data2 != null && dataresult.result.data2.Count > 0)
                        {
                            var locationInfo = dataresult.result.data2;
                            str.Append("3 địa chỉ xuất hiện nhiều nhất của KH: <br />");
                            if (locationInfo[0] != null)
                            {
                                var firstLocation = _locationService.GetNameAddress(locationInfo[0].lat, locationInfo[0].lng);
                                if (firstLocation == null || firstLocation == "")
                                    firstLocation = string.Format("lat: {0} lng: {1}", locationInfo[0].lat, locationInfo[0].lng);
                                str.AppendFormat("- Địa chỉ thứ nhất: {0}({1},{2}) <br /> + Tỉ lệ xuất hiện là: {3}% {4} {5} <br />", firstLocation, locationInfo[0].lat, locationInfo[0].lng, locationInfo[0].percent,
                                 (dataresult.distance != null && dataresult.distance.homeDist != null && dataresult.distance.homeDist[0] > 0) ? string.Format("<br />+ Cách nhà {0}m", Math.Round(dataresult.distance.homeDist[0] * 1000, 0)) : "",
                                 (dataresult.distance != null && dataresult.distance.officeDist != null && dataresult.distance.officeDist[0] > 0) ? string.Format("<br />+ Cách công ty {0}m", Math.Round(dataresult.distance.officeDist[0] * 1000, 0)) : ""
                                 );
                            }
                            if (locationInfo.Count >= 2 && locationInfo[1] != null)
                            {
                                var secondLocation = _locationService.GetNameAddress(locationInfo[1].lat, locationInfo[1].lng);
                                if (secondLocation == null || secondLocation == "")
                                    secondLocation = string.Format("lat: {0} lng: {1}", locationInfo[1].lat, locationInfo[1].lng);
                                str.AppendFormat("- Địa chỉ thứ hai: {0}({1},{2}) <br /> + Tỉ lệ xuất hiện là: {3}% {4} {5}<br />", secondLocation, locationInfo[1].lat, locationInfo[1].lng, locationInfo[1].percent,
                                         (dataresult.distance != null && dataresult.distance.homeDist != null && dataresult.distance.homeDist[1] > 0) ? string.Format("<br />+ Cách nhà {0}m", Math.Round(dataresult.distance.homeDist[1] * 1000, 0)) : "",
                                        (dataresult.distance != null && dataresult.distance.officeDist != null && dataresult.distance.officeDist[1] > 0) ? string.Format("<br />+ Cách công ty {0}m", Math.Round(dataresult.distance.officeDist[1] * 1000, 0)) : ""
                                        );
                            }


                            if (locationInfo.Count >= 3 && locationInfo[2] != null)
                            {
                                var thirdLocation = _locationService.GetNameAddress(locationInfo[2].lat, locationInfo[2].lng);
                                if (thirdLocation == null || thirdLocation == "")
                                    thirdLocation = string.Format("lat: {0} lng: {1}", locationInfo[2].lat, locationInfo[2].lng);
                                str.AppendFormat("- Địa chỉ thứ ba: {0}({1},{2}) <br /> + Tỉ lệ xuất hiện là: {3}% {4} {5}<br />", thirdLocation, locationInfo[2].lat, locationInfo[2].lng, locationInfo[2].percent,
                                        (dataresult.distance != null && dataresult.distance.homeDist != null && dataresult.distance.homeDist[2] > 0) ? string.Format("<br />+ Cách nhà {0}m", Math.Round(dataresult.distance.homeDist[2] * 1000, 0)) : "",
                                      (dataresult.distance != null && dataresult.distance.officeDist != null && dataresult.distance.officeDist[2] > 0) ? string.Format("<br />+ Cách công ty {0}m", Math.Round(dataresult.distance.officeDist[2] * 1000, 0)) : ""
                                      );
                            }

                        }
                        //lưu thông tin xuống comment
                        _loanBriefServices.AddNote(new LoanBriefNote
                        {
                            LoanBriefId = entity.LoanbriefId,
                            Note = string.Format("<b class='required-red'>[MOBIFONE] KẾT QUẢ LOCATION NHÀ MẠNG </b>: {0}", str.ToString()),
                            FullName = "Auto System",
                            Status = 1,
                            ActionComment = EnumActionComment.ResultLocationMobifone.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = 1,
                            ShopId = EnumShop.Tima.GetHashCode(),
                            ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        // update lại trạng thái
                        //var message = string.Empty;
                        //if (dataresult.finalResult == "YES")
                        //{
                        //    message = "Thu  hồ sơ theo  checklist gói 1";
                        //}
                        //else if (dataresult.finalResult == "NO" || dataresult.finalResult == "UNKNOWN")
                        //{
                        //    message = "Thu  hồ sơ theo  checklist gói 2";
                        //}
                        //Cập nhật giá trị final + Đánh dấu đã xử lý bản ghi log để k xử lý lại
                        if (!string.IsNullOrEmpty(dataresult.finalResult))
                        {
                            _loanBriefServices.UpdateResultLocation(entity.LoanbriefId.Value, dataresult.finalResult);
                            entity.ResultFinal = dataresult.finalResult;
                            if (_logLoanInfoAIService.Done(entity) > 0)
                                Console.WriteLine(string.Format("Read location Mobi Done: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            else
                                Console.WriteLine(string.Format("Read location Mobi ERROR: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        }

                        //if (!string.IsNullOrEmpty(message))
                        //{
                        //    //lưu thông tin gợi ý gói vay
                        //    _loanBriefServices.AddNote(new LoanBriefNote
                        //    {
                        //        LoanBriefId = entity.LoanbriefId,
                        //        Note = message,
                        //        FullName = "Auto System",
                        //        Status = 1,
                        //        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        //        CreatedTime = DateTime.Now,
                        //        UserId = 1,
                        //        ShopId = EnumShop.Tima.GetHashCode(),
                        //        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                        //    });
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor ReadLocationMobi exception");
            }
        }

        private void ReadRuleCheck(LogLoanInfoAi entity, ref string resultFinal)
        {
            try
            {
                var str = string.Empty;
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseLogLoanAi>(entity.Response);
                resultFinal = result.result;
                if (result.result == "PASS")
                {
                    str = "PASS CIC " + string.Join("<br />", result.messages);
                }
                else if (result.result == "NO_PASS")
                {
                    str = "NO PAS CIC " + string.Join("<br />", result.messages);
                }
                else if (result.result == "UNKNOWN")
                {
                    str = "UNKNOWN CIC " + string.Join("<br />", result.messages);
                }
                if (!string.IsNullOrEmpty(str.ToString()))
                {
                    _loanBriefServices.AddNote(new LoanBriefNote
                    {
                        LoanBriefId = entity.LoanbriefId,
                        Note = string.Format("Rule Cac: {0}", str.ToString()),
                        FullName = "Auto System",
                        Status = 1,
                        ActionComment = EnumActionComment.ResultRuleCheck.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 1,
                        ShopId = EnumShop.Tima.GetHashCode(),
                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                }
                //Cập nhật giá trị final + Đánh dấu đã xử lý bản ghi log để k xử lý lại
                //if (!string.IsNullOrEmpty(resultFinal))
                //{
                entity.ResultFinal = resultFinal;
                if (_logLoanInfoAIService.Done(entity) > 0)
                    Console.WriteLine(string.Format("Read Rulecheck Done: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                else
                    Console.WriteLine(string.Format("Read Rulecheck ERROR: {0} {1}", entity.LoanbriefId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                //}
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor ReadRuleCac exception");
            }
        }

        private void ReadRefPhone(LogLoanInfoAi entity)
        {
            try
            {

                RefphoneRes dataresult = Newtonsoft.Json.JsonConvert.DeserializeObject<RefphoneRes>(entity.Response);
                if (dataresult != null && dataresult.status != 0)
                {
                    //lưu thông tin xuống comment
                    _loanBriefServices.AddNote(new LoanBriefNote
                    {
                        LoanBriefId = entity.LoanbriefId,
                        Note = string.Format("Refphone: {0}", dataresult.desc),
                        FullName = "Auto System",
                        Status = 1,
                        ActionComment = EnumActionComment.ResultRefPhone.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 1,
                        ShopId = EnumShop.Tima.GetHashCode(),
                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                    entity.ResultFinal = "NOT_VALUE";
                    //đánh dấu đọc xong dữ liệu
                    if (_logLoanInfoAIService.Done(entity) > 0)
                        Console.WriteLine(string.Format("Read Refphone VT Done: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    else
                        Console.WriteLine(string.Format("Read Refphone VT ERROR: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
                else if (dataresult.data != null && dataresult.status == EnumRefCodeStatus.Success.GetHashCode())
                {
                    if (dataresult.refPhones != null)
                    {
                        var str = new StringBuilder();
                        var listData = new List<DataRefphoneItem>();
                        if (dataresult.refPhones.num1 != null && !string.IsNullOrEmpty(dataresult.refPhones.num1.value))
                            listData.Add(dataresult.refPhones.num1);
                        if (dataresult.refPhones.num2 != null && !string.IsNullOrEmpty(dataresult.refPhones.num2.value))
                            listData.Add(dataresult.refPhones.num2);
                        if (dataresult.refPhones.num3 != null && !string.IsNullOrEmpty(dataresult.refPhones.num3.value))
                            listData.Add(dataresult.refPhones.num3);
                        if (listData != null && listData.Count > 0)
                        {
                            foreach (var item in listData)
                            {
                                try
                                {
                                    var phone = item.value;
                                    if (phone.StartsWith("84"))
                                        phone = string.Format("0{0}", phone.Remove(0, 2));
                                    var CallRate = (int)item.call_rate;
                                    var CallDuration = (int)item.call_rate;
                                    _loanBriefServices.UpdateResultRefphone(entity.LoanbriefId.Value, phone, (int)item.call_rate, (int)item.duration_rate);
                                    str.AppendFormat("KQ refphone KH: {0} có tần suất call {1} với thời lượng {2}", item.value,
                                        Enum.IsDefined(typeof(EnumCallRate), CallRate) ? ExtensionHelper.GetDescription((EnumCallRate)CallRate) : CallRate.ToString(),
                                        Enum.IsDefined(typeof(CallDuration), CallDuration) ? ExtensionHelper.GetDescription((CallDuration)CallDuration) : CallDuration.ToString());
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                            var msg = str.ToString();
                            if (!string.IsNullOrEmpty(msg))
                            {
                                //lưu thông tin xuống comment
                                _loanBriefServices.AddNote(new LoanBriefNote
                                {
                                    LoanBriefId = entity.LoanbriefId,
                                    Note = str.ToString(),
                                    FullName = "Auto System",
                                    Status = 1,
                                    ActionComment = EnumActionComment.ResultRefPhone.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = 1,
                                    ShopId = EnumShop.Tima.GetHashCode(),
                                    ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                });
                            }

                            entity.ResultFinal = dataresult.finalResult;
                            //đánh dấu đọc xong dữ liệu
                            if (_logLoanInfoAIService.Done(entity) > 0)
                                Console.WriteLine(string.Format("Read Refphone VT Done: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            else
                                Console.WriteLine(string.Format("Read Refphone VT ERROR: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor ReadRefPhone exception");
            }
        }

        private void ReadRefPhoneMobi(LogLoanInfoAi entity)
        {
            try
            {

                RefphoneMobiReq dataresult = Newtonsoft.Json.JsonConvert.DeserializeObject<RefphoneMobiReq>(entity.Response);
                if (dataresult != null && dataresult.status != 0)
                {
                    //lưu thông tin xuống comment
                    _loanBriefServices.AddNote(new LoanBriefNote
                    {
                        LoanBriefId = entity.LoanbriefId,
                        Note = string.Format("Refphone: {0}", dataresult.desc),
                        FullName = "Auto System",
                        Status = 1,
                        ActionComment = EnumActionComment.ResultRefPhone.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 1,
                        ShopId = EnumShop.Tima.GetHashCode(),
                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                    entity.ResultFinal = "NOT_VALUE";
                    //đánh dấu đọc xong dữ liệu
                    if (_logLoanInfoAIService.Done(entity) > 0)
                        Console.WriteLine(string.Format("Read Refphone Mobi Done: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    else
                        Console.WriteLine(string.Format("Read Refphone Mobi ERROR: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                }
                else if (dataresult.data != null && dataresult.status == EnumRefCodeStatus.Success.GetHashCode())
                {
                    if (dataresult.refPhones != null)
                    {
                        var refphone = dataresult.refPhones;
                        var phone = refphone.phone;
                        if (phone.StartsWith("84"))
                            phone = string.Format("0{0}", phone.Remove(0, 2));

                        if (!string.IsNullOrEmpty(refphone.result))
                        {
                            var callRate = int.Parse(refphone.result);
                            var str = string.Format("KQ refphone KH: {0} có tần suất call {1}", phone,
                                            Enum.IsDefined(typeof(EnumCallRateMobifone), callRate) ? ExtensionHelper.GetDescription((EnumCallRateMobifone)callRate) : callRate.ToString());
                            _loanBriefServices.UpdateResultRefphone(entity.LoanbriefId.Value, phone, callRate, 0);
                            if (!string.IsNullOrEmpty(str))
                            {
                                //lưu thông tin xuống comment
                                _loanBriefServices.AddNote(new LoanBriefNote
                                {
                                    LoanBriefId = entity.LoanbriefId,
                                    Note = str.ToString(),
                                    FullName = "Auto System",
                                    Status = 1,
                                    ActionComment = EnumActionComment.ResultRefPhone.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = 1,
                                    ShopId = EnumShop.Tima.GetHashCode(),
                                    ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                                });
                            }

                            entity.ResultFinal = dataresult.finalResult;
                            //đánh dấu đọc xong dữ liệu
                            if (_logLoanInfoAIService.Done(entity) > 0)
                                Console.WriteLine(string.Format("Read Refphone Mobi Done: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                            else
                                Console.WriteLine(string.Format("Read Refphone Mobi ERROR: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor ReadRefPhoneMobi exception");
            }
        }

        private void ReadRangeCreditScoring(LogLoanInfoAi entity)
        {
            try
            {

                CreditScoringReq dataresult = Newtonsoft.Json.JsonConvert.DeserializeObject<CreditScoringReq>(entity.Response);
                if (dataresult != null && !string.IsNullOrEmpty(dataresult.RequestID) && dataresult.ScoreRange > 0)
                {
                    Console.WriteLine(string.Format("Result Range CreditScoring: {0} {1} {2}", dataresult.RequestID, dataresult.ScoreRange, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    //lưu thông tin xuống comment
                    _loanBriefServices.AddNote(new LoanBriefNote
                    {
                        LoanBriefId = entity.LoanbriefId,
                        Note = string.Format("Credit Scoring: KH được phân loại theo điểm score: {0}", dataresult.ScoreRange),
                        FullName = "Auto System",
                        Status = 1,
                        ActionComment = EnumActionComment.ResultCreditScoring.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = 1,
                        ShopId = EnumShop.Tima.GetHashCode(),
                        ShopName = ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                    entity.ResultFinal = string.Format("{0}", dataresult.ScoreRange);
                    //đánh dấu đọc xong dữ liệu
                    if (_logLoanInfoAIService.Done(entity) > 0)
                        Console.WriteLine(string.Format("Read Range CreditScoring Done: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    else
                        Console.WriteLine(string.Format("Read Range CreditScoring ERROR: {0} {1} {2}", entity.LoanbriefId, entity.ResultFinal, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                    _loanBriefServices.UpdateRangeScore(entity.LoanbriefId.Value, dataresult.ScoreRange);

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LOS.ThirdPartyProccesor ReadCreditScoring exception");
            }
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            // _timer?.Change(Timeout.Infinite, 0);
            // Signal cancellation to the executing method
            _cts.Cancel();
            cancellationToken.ThrowIfCancellationRequested();
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _thread?.Abort();
            //_timer?.Dispose();
        }
    }
}
