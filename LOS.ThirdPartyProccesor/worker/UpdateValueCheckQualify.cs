﻿using LOS.ThirdPartyProccesor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LOS.Common.Extensions;

namespace LOS.ThirdPartyProccesor.worker
{
    public class UpdateValueCheckQualify : IHostedService, IDisposable
    {
        private readonly ILoanBriefService _loanBriefService;
        private readonly IConfiguration _configuration;
        private string environmentName;
        private Thread _thread;
        public UpdateValueCheckQualify(ILoanBriefService loanBriefService)
        {
            _loanBriefService = loanBriefService;
            environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            this._configuration = builder.Build();
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _thread = new Thread(() => DoWork(null));
            _thread.Start();
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            //Lấy danh sách các đơn chưa được check qlf
            var lstData = _loanBriefService.GetListLoanBriefNoCheckQlf();
            if (lstData != null && lstData.Count > 0)
            {
                Console.WriteLine("Lay duoc " + lstData.Count + "ban ghi");
                foreach (var item in lstData)
                {
                    var loanbriefQuestionScript = _loanBriefService.GetLoanBriefQuestionScript(item.LoanBriefId);
                    if (loanbriefQuestionScript != null)
                    {
                        Console.WriteLine("Bat dau cap nhap value qlf HĐ " + item.LoanBriefId);
                        int valueCheckQualify = 0;
                        //có nhu cầu vay
                        if (loanbriefQuestionScript.QuestionBorrow == true)
                            valueCheckQualify += (int)EnumCheckQualify.NeedLoan;
                        //có xe máy
                        else if (loanbriefQuestionScript.QuestionUseMotobikeGo == true)
                            valueCheckQualify += (int)EnumCheckQualify.HaveMotobike;
                        //có đăng ký xe bản gốc
                        else if (loanbriefQuestionScript.QuestionMotobikeCertificate == true)
                            valueCheckQualify += (int)EnumCheckQualify.HaveOriginalVehicleRegistration;
                        //Trong độ tuổi cho vay (20 - 60 tuổi)
                        if (item.Dob != null)
                        {
                            var dateNowYear = DateTime.Now.Year;
                            var dobYear = item.Dob.Value.Year;
                            var yearOld = dateNowYear - dobYear;
                            if (yearOld >= 20 && yearOld <= 60)
                            {
                                valueCheckQualify += (int)EnumCheckQualify.InAge;
                            }
                        }
                        //Trong khu vực hỗ trợ
                        if(item.ProvinceId == 1 || item.ProvinceId == 79)
                        {
                            if(item.DistrictId > 0)
                            {
                                var district = _loanBriefService.GetDistrictIsApply(item.DistrictId.Value);
                                if (district != null && district.IsApply == 1)
                                {
                                    valueCheckQualify += (int)EnumCheckQualify.InAreaSupport;
                                }
                            }
                        }
                        //Upload chứng từ
                        var loanbriefFiles = _loanBriefService.GetLoanBriefFiles(item.LoanBriefId);
                        if(loanbriefFiles != null && loanbriefFiles.Count >= 4)
                        {
                            valueCheckQualify += (int)EnumCheckQualify.UploadImage;
                        }

                        //Cập nhập lại vào db

                       var result = _loanBriefService.UpdateValueQualify(item.LoanBriefId, valueCheckQualify);
                        if (result)
                            Console.WriteLine("Cap nhap value qlf HĐ " + item.LoanBriefId + " thanh cong");
                        else
                            Console.WriteLine("Cap nhap value qlf HĐ " + item.LoanBriefId + " that bai");
                    }
                }
            }
        }

        public void Dispose()
        {
            _thread?.Abort();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
