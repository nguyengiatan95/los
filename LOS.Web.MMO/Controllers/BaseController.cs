﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.Web.MMO.Helpers;
using LOS.Web.MMO.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;

namespace LOS.Web.MMO.Controllers
{
    public class BaseController : Controller
    {
        protected IConfiguration _baseConfig;
        public BaseController(IConfiguration configuration)
        {
            _baseConfig = configuration;

        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var user = GetUserGlobal();
            if (user == null || string.IsNullOrEmpty(user.Token))
                filterContext.Result = new RedirectToActionResult("Index", "Login", null);
            //check permission
            var controller = filterContext.RouteData.Values["Controller"].ToString();
            var action = filterContext.RouteData.Values["Action"].ToString();
            if (!string.IsNullOrEmpty(controller) && !string.IsNullOrEmpty(action))
            {
                //var module = _moduleService.Get(user.Token, controller.AsToLower(), action.AsToLower());
                // if(module.ModuleId > 0)
                // {
                //     if(!user.Modules.Any(x=>x.ModuleId == module.ModuleId))
                //         filterContext.Result = new RedirectResult("/error.html");
                // }
            }
            base.OnActionExecuted(filterContext);
        }
        protected string GetToken()
        {
            return HttpContext.Session.GetObjectFromJson<UserMMODetail>("USER_DATA").Token;
        }

        protected UserMMODetail GetUserGlobal()
        {
            return HttpContext.Session.GetObjectFromJson<UserMMODetail>("USER_DATA");
        }
    }
}