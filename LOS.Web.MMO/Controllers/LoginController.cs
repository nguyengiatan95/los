﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.Web.MMO.Helpers;
using LOS.Web.MMO.Models.Login;
using LOS.Web.MMO.Models.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace LOS.Web.MMO.Controllers
{
    public class LoginController : Controller
    {
        private IConfiguration _baseConfig;
        public LoginController(IConfiguration configuration)
        {
            _baseConfig = configuration;
        }
        [Route("login.html")]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> PostLogin(LoginReq obj)
        {
            try
            {
                var httpClient = new HttpClient();
                var loginReq = new LoginReq()
                {
                    Email = obj.Email,
                    Password = Security.GetMD5Hash(obj.Password)
                };
                var response = await httpClient.PostAsJsonAsync(_baseConfig["Configuration:WebApiUrl"] + Constants.USERSMMO_LOGIN_ENDPOINT, loginReq);
                if (response.IsSuccessStatusCode)
                {
                    var sResult = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<DefaultResponse<UserMMODetail>>(sResult);
                    if (result.meta.errorCode == 200)
                    {
                        if (result.data.IsActive == false)// tk bị khóa
                        {
                            return Json(new { status = 0, message = "Tài khoản đã bị khóa" });
                        }
                        else
                            HttpContext.Session.SetObjectAsJson("USER_DATA", result.data);
                        //return Redirect("~/home.html");
                        return Json(new { status = 1, message = "home.html" });
                        // save access token					
                    }
                }
                return Json(new { status = 0, message = "Thông tin tài khoản không đúng" });
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Lỗi liên hệ kỹ thuật." });
            }
        }

        [Route("logout.html")]
        public IActionResult Logout()
        {
            HttpContext.Session.SetObjectAsJson("USER_DATA", null);
            return Redirect("/login.html");
        }
    }
}