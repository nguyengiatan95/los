﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.Web.MMO.Helpers;
using LOS.Web.MMO.Models;
using LOS.Web.MMO.Services.Report;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LOS.Web.MMO.Controllers
{
    public class ReportController : BaseController
    {
        private IReportService _reportService;
        private IConfiguration _baseConfig;
        private readonly IMapper _mapper;
        public ReportController(IReportService reportService, IConfiguration configuration,IMapper mapper) : base(configuration)
        {
            _reportService = reportService;
            _baseConfig = configuration;
            _mapper = mapper;
        }
        [Route("usermmo-ReportLoanBriefSuccessfullySold.html")]
        public IActionResult ReportLoanBriefSuccessfullySold()
        {
            return View();
        }
        public async Task<IActionResult> GetReportLoanBriefSuccessfullySold()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new ReportLoanBriefByUserMmo(HttpContext.Request.Form);
                string[] arrdates = null;
                DateTime fromDate = DateTime.Now;
                DateTime toDate = DateTime.Now;
                DateTime datenow = DateTime.Now;
                if (!string.IsNullOrEmpty(modal.DateRanger))
                {
                    arrdates = modal.DateRanger.Split('-');
                }
                else
                {
                    // get date on week
                    fromDate = Lib.FirstDayOfWeek(datenow).AddDays(-1);
                    toDate = Lib.LastDayOfWeek(datenow).AddDays(+1);
                }
                if (arrdates != null)
                {
                    fromDate = DateTimeUtility.ConvertStringToDate(arrdates[0].Trim(), DateTimeUtility.DATE_FORMAT).AddDays(-1);
                    toDate = DateTimeUtility.ConvertStringToDate(arrdates[1].Trim(), DateTimeUtility.DATE_FORMAT).AddDays(+1);
                }
                int recordsTotal = 0;
                modal.Affcode = user.UserId;
                modal.fromDate = fromDate;
                modal.toDate = toDate;
                modal.affstatus = StatusAff.SuccessfulSale.GetHashCode();
                // Query api   	
                List<LoanBriefSuccessFullySoldDetail> data = new List<LoanBriefSuccessFullySoldDetail>();
                data = _reportService.ReporLoanSuccessFullySoldMMO(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });

            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("ReporLoanRegisterOfUserMmo.html")]
        public IActionResult ReporLoanRegisterOfUserMmo()
        {
            return View();
        }
        public async Task<IActionResult> GetReporLoanRegisterOfUserMmo()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new ReportLoanBriefByUserMmo(HttpContext.Request.Form);
                string[] arrdates = null;
                DateTime fromDate = DateTime.Now;
                DateTime toDate = DateTime.Now;
                DateTime datenow = DateTime.Now;
                if (!string.IsNullOrEmpty(modal.DateRanger))
                {
                    arrdates = modal.DateRanger.Split('-');
                }
                else
                {
                    // get date on week
                    fromDate = Lib.FirstDayOfWeek(datenow).AddDays(-1);
                    toDate = Lib.LastDayOfWeek(datenow).AddDays(+1);
                }
                if (arrdates != null)
                {
                    fromDate = DateTimeUtility.ConvertStringToDate(arrdates[0].Trim(), DateTimeUtility.DATE_FORMAT).AddDays(-1);
                    toDate = DateTimeUtility.ConvertStringToDate(arrdates[1].Trim(), DateTimeUtility.DATE_FORMAT).AddDays(+1);
                }
                int recordsTotal = 0;
                modal.Affcode = user.UserId;
                modal.fromDate = fromDate;
                modal.toDate = toDate;
                modal.affstatus = StatusAff.Introduced.GetHashCode();
                // Query api   	
                List<LoanBriefDetail> data = new List<LoanBriefDetail>();
                data = _reportService.ReporLoanRegisterOfUserMmo(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });

            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}