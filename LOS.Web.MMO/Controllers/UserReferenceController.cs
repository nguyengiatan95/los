﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LOS.DAL.DTOs;
using LOS.Web.MMO.Helpers;
using LOS.Web.MMO.Models;
using LOS.Web.MMO.Services.UserMMO;
using LOS.Web.MMO.Services.UserReference;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LOS.Web.MMO.Controllers
{
    public class UserReferenceController : BaseController
    {
        private IConfiguration _baseConfig;
        private IUserMMOService _userMmoServive;
        private IUserReferenceService _userReferenceServive;
        private readonly IMapper _mapper;

        public UserReferenceController(IConfiguration configuration, IUserMMOService userMmoServive, IMapper mapper, IUserReferenceService userReferenceServive) : base(configuration)
        {
            _userMmoServive = userMmoServive;
            _baseConfig = configuration;
            _mapper = mapper;
            _userReferenceServive = userReferenceServive;
        }

        [Route("usermmo-links.html")]
        public async Task<IActionResult> LinksUserMMO()
        {
            return View();
        }
        public async Task<IActionResult> LoadDataLinksUserMMO()
        {
            try
            {
                var user = HttpContext.Session.GetObjectFromJson<UserMMODetail>("USER_DATA");
                var modal = new UserReferenceDatatable(HttpContext.Request.Form);
                int recordsTotal = 0;
                modal.UserId = user.UserId;
                // Query api   	
                List<UserReferenceDetail> data = new List<UserReferenceDetail>();
                data = _userReferenceServive.GetByLinksUser(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //modal.total = data.
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}