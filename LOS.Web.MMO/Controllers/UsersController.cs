﻿using AutoMapper;
using LOS.DAL.DTOs;
using LOS.Web.MMO.Helpers;
using LOS.Web.MMO.Models;
using LOS.Web.MMO.Services.UserMMO;
using LOS.Web.MMO.Services.UserReference;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Controllers
{
    public class UsersController : Controller
    {
        private IConfiguration _baseConfig;
        private IUserMMOService _userMmoServive;
        private IUserReferenceService _userReferenceServive;
        private readonly IMapper _mapper;

        public UsersController(IConfiguration configuration, IUserMMOService userMmoServive, IMapper mapper, IUserReferenceService userReferenceServive)
        {
            _userMmoServive = userMmoServive;
            _baseConfig = configuration;
            _mapper = mapper;
            _userReferenceServive = userReferenceServive;
        }
        [Route("user-register.html")]
        public IActionResult Register(int rf)
        {

            ViewBag.rf = rf;
            return View();
        }
        public async Task<IActionResult> CreateUser(UsersRegister entity)
        {
            try
            {
                if (entity.Password != entity.rPassword)
                {
                    return Json(new { status = 0, message = "mật khẩu không giống nhau" });
                }
                var objuser = _userMmoServive.GetByUser(entity.Email.Trim());
                if (objuser.UserId > 0)
                {
                    return Json(new { status = 0, message = "Email đã tồn tại." });
                }
                var CheckObjPhone = _userMmoServive.GetByPhone(entity.Phone.Trim());
                if (CheckObjPhone.UserId > 0)
                {
                    return Json(new { status = 0, message = "Số điện thoại đã tồn tại." });
                }
                var obj = _mapper.Map<UserMMODTO>(entity);
                obj.CreateDate = DateTime.Now;
                obj.Password = Security.GetMD5Hash(entity.Password);
                obj.IsActive = false;

                var guid = Guid.NewGuid().ToString();
                obj.Giud = guid;
                var result = _userMmoServive.Create(obj);
                if (result > 0) // gưi mail xác nhận
                {

                    string htmlString = @"<html>
                      <body>
                      <p>Dear Fullname</p>
                      <p>Cảm ơn bạn đã đăng ký tài khoản MMO của chúng tôi.</p></br>
                      <p>Bạn vui lòng click vào link bên dưới  xác nhận mail này là của bạn</p><br />
                      <a target='_blank' href='https://localhost:44300/Users/ConfirmMail?={0}'>Xác nhận thông tin tài khoản MMO </a>
                      <p>Trân trọng.!<br></br></p>
                      </body>
                      </html>
                     ";
                    htmlString = string.Format(htmlString, guid);
                    htmlString = htmlString.Replace("Fullname", entity.FullName);
                    var sendmail = SendEmail("Xác nhận mail đăng ký tài khoản MMO", "Xác nhận đăng ký tài khoản MMO", entity.Email, htmlString);
                    if (sendmail == true)
                    {
                        obj.AffCode = "tima" + result;
                        obj.UserId = result;
                        _userMmoServive.Update(result, obj);
                        if (entity.rf > 0) // check thêm tài khoản liên kết
                        {
                            var UserReferenceobj = new UserReferenceItem();
                            UserReferenceobj.UserId = result;
                            UserReferenceobj.UserReferenceId = entity.rf;
                            UserReferenceobj.UserCode = obj.AffCode;
                            int createUserRf = CreateUserRf(UserReferenceobj);
                            if (createUserRf > 0)
                            {
                                return Json(new { status = 1, message = "Thêm mới tài khoản thành công", });
                            }
                            return Json(new { status = 0, message = "Xảy ra lỗi khi thêm mới tài khoản. Vui lòng liên hệ quản trị viên!", });
                        }
                        return Json(new { status = 1, message = "Thêm mới tài khoản thành công", });
                    }
                }
                return Json(new { status = 0, message = "Xảy ra lỗi khi thêm mới tài khoản. Vui lòng liên hệ quản trị viên!" });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public int CreateUserRf(UserReferenceItem entity)
        {
            int result = 0;
            var LstUserjoin = _userReferenceServive.GetByUserId((int)entity.UserReferenceId);
            if (LstUserjoin != null)
            {
                if (LstUserjoin.Count >= 4)
                {
                    var itemToRemove = LstUserjoin.Single(r => r.Level == 4);
                    LstUserjoin.Remove(itemToRemove);
                }
            }


            var userObj = _userMmoServive.GetById((int)entity.UserReferenceId);
            var obj = _mapper.Map<UserReferenceDTO>(entity);
            obj.UserReferenceId = userObj.UserId;
            obj.AffReferenceCode = userObj.AffCode;
            obj.Level = 1;
            result = _userReferenceServive.Create(obj);
            if (LstUserjoin != null)
            {
                foreach (var item in LstUserjoin)
                {
                    obj.UserReferenceId = item.UserReferenceId;
                    obj.AffReferenceCode = item.AffReferenceCode;
                    obj.Level = 1 + item.Level;
                    result = _userReferenceServive.Create(obj);
                    if (result > 0)
                    {
                        return result;
                    }
                    result = 0;
                }
            }
            if (result > 0)
            {
                return result;
            }
            result = 0;
            return result;
        }

        [Route("user-forgotpassword.html")]
        public IActionResult ForgotPassword()
        {
            return View();
        }
        public IActionResult SendMailRestPassword(string email)
        {
            var objuser = _userMmoServive.GetByUser(email.Trim());
            if (objuser.UserId > 0)
            {
                var guid = UpdateGiud(objuser.UserId);
                string htmlString = @"<html>
                      <body>
                      <p>Dear Fullname</p>
                      <p>Chúng tôi nhận thấy yêu cầu bạn bị quên mật khẩu và muốn lấy lại mật khẩu.</p></br>
                      <p>Bạn vui lòng click vào link bên dưới  để được cấp lại mật khẩu. nếu không phải thì xin vui lòng bỏ qua</p><br />
                      <a target='_blank' href='https://localhost:44300/Users/Resetpassword?={0}'>Cấp lại mật khẩu</a>
                      <p>Trân trọng.!<br></br></p>
                      </body>
                      </html>
                     ";
                htmlString = string.Format(htmlString, guid);
                htmlString = htmlString.Replace("Fullname", objuser.FullName);
                var result = SendEmail("Quên mật khẩu tài khoản MMO", "Xác nhận quên tài khoản MMO", email, htmlString);
                if (result == true)
                {
                    return Json(new { status = 1, message = "Bạn vui lòng vào mail xác nhận lại thông tin" });
                }
                return Json(new { status = 0, message = "Lỗi liên hệ kỹ thuật viên" });
            }
            return Json(new { status = 0, message = "Email không tồn tại trong hệ thống" });
        }
        public IActionResult Resetpassword(string guid)
        {
            var objuser = _userMmoServive.GetByGuid(guid);
            if (objuser != null)
            {
                UserMMODTO obj = new UserMMODTO();
                obj.UserId = objuser.UserId;
                var newpas = _baseConfig["Configuration:NewPassUser"];
                obj.Password = Security.GetMD5Hash(newpas);
                obj.UpdateDate = DateTime.Now;
                var result = _userMmoServive.Update(objuser.UserId, obj);
                if (result == true)
                {
                    string htmlString = @"<html>
                      <body>
                      <p>Dear Fullname</p>
                      <p>Mật khẩu mới của bạn là:  newpass</p></br>
                      <p>Trân trọng.!<br></br></p>
                      </body>
                      </html>
                     ";
                    htmlString = htmlString.Replace("Fullname", objuser.FullName);
                    htmlString = htmlString.Replace("newpass", newpas);

                    SendEmail("Cấp lại mật khẩu tài khoản MMO", "Cấp lại mật khẩu tài khoản MMO", objuser.Email, htmlString);
                    return Redirect("~/login.html");
                }
            }
            return Redirect("~/user-error.html");
        }
        public IActionResult ConfirmMail(string guid)
        {
            var objuser = _userMmoServive.GetByGuid(guid);
            if (objuser != null)
            {
                UserMMODTO obj = new UserMMODTO();
                obj.UserId = objuser.UserId;
                obj.IsActive = true;
                var result = _userMmoServive.Update(objuser.UserId, obj);
                if (result)
                {
                    string htmlString = @"<html>
                      <body>
                      <p>Dear Fullname</p>
                      <p>Chúc mừng bạn đã kích hoạt tài khoản thành công</p></br>
                      <p>Trân trọng.!<br></br></p>
                      </body>
                      </html>
                     ";
                    htmlString = htmlString.Replace("Fullname", objuser.FullName);
                  SendEmail("Kích hoạt tài khoản MMO", "Kích hoạt tài khoản MMO", objuser.Email, htmlString);
                    return Redirect("~/login.html");
                }
            }
            return Redirect("~/user-error.html");
           
        }
        public bool SendEmail(string subject, string message, string receiver, string strbody)
        {
            bool result = false;
            try
            {
                var senderEmail = new MailAddress(_baseConfig["Configuration:Email"], "Tima");
                var receiverEmail = new MailAddress(receiver, "Receiver");
                var password = _baseConfig["Configuration:EmailPass"];
                var sub = subject;
                //var body = message;

                //string  url = "<a target="'_blank'" href=\"https://localhost:44300/Users/ConfirmMail?=" + UserId+">Xác nhận thông tin tài khoản MMO </a>";
                string body = strbody;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail.Address, password)
                };
                using (var mess = new MailMessage(senderEmail, receiverEmail)
                {
                    Subject = subject,
                    IsBodyHtml = true,
                    Body = body
                })
                {
                    smtp.Send(mess);
                }
                result = true;
            }
            catch (Exception)
            {
                result = false;
                throw;
            }
            return result;
        }
        [Route("user-error.html")]
        public IActionResult Error()
        {
            ViewBag.Error = "Lỗi liên hệ kỹ thuật viên";
            return View();
        }

        [Route("user-linkrefer.html")]
        public IActionResult LinkRefer()
        {
            var model = new LinkReferModel();
            var user = HttpContext.Session.GetObjectFromJson<UserMMODetail>("USER_DATA");
            var gettoken = HttpContext.Session.GetObjectFromJson<UserMMODetail>("USER_DATA").Token;

            var obj = _userMmoServive.GetById(user.UserId);

            model.ReferralsLink = _baseConfig["Configuration:ReferralsLink"] + obj.UserId;
            model.AffiliateLink = _baseConfig["Configuration:AffiliateLink"] + obj.UserId;
            model.Url = _baseConfig["Configuration:UrlBanner"];
            return View(model);
        }

        [Route("user-Infor.html")]
        public IActionResult InformationUser()
        {
            return View();
        }

        public async Task<IActionResult> LoadDataInformationUser()
        {
            try
            {
                var user = HttpContext.Session.GetObjectFromJson<UserMMODetail>("USER_DATA");
                //var modal = new UserMmoDatatable(HttpContext.Request.Form);
                //int recordsTotal = 0;
                //// Query api   	
                List<UserMMODetail> data = new List<UserMMODetail>();
                var obj = _userMmoServive.GetById(user.UserId);
                data.Add(obj);
                //modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = data, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult CreateUserMmoModal(int Id)
        {
            var entity = new UserMmoItem();
            // Query api   
            var user = _userMmoServive.GetById(Id);
            entity = _mapper.Map<UserMmoItem>(user);
            //return View("/Views/Users/Partial/_CreateUserModalPartial.cshtml", entity);
            return View(entity);

        }
        [HttpPost]
        public async Task<IActionResult> EditUser(UserMmoItem entity)
        {
            try
            {
                var gettoken = HttpContext.Session.GetObjectFromJson<UserMMODetail>("USER_DATA").Token;
                if (gettoken == null)
                {
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật tài khoản. Vui lòng liên hệ quản trị viên!" });
                }
                var ImageFacedeCmt = UploadFile(entity.ImageFacedeCmt);
                if (ImageFacedeCmt == "1")
                {
                    return Json(new { status = 0, message = "Định dạng file tải lên không đúng" });
                }
                var ImageBacksideCmt = UploadFile(entity.ImageBacksideCmt);
                if (ImageBacksideCmt == "1")
                {
                    return Json(new { status = 0, message = "Định dạng file tải lên không đúng" });
                }
                entity.FacedeCmt = ImageFacedeCmt;
                entity.BacksideCmt = ImageBacksideCmt;

                if (entity.UserId > 0)
                {
                    var obj = _mapper.Map<UserMMODTO>(entity);
                    obj.UpdateDate = DateTime.Now;
                    var result = _userMmoServive.Update(entity.UserId, obj);
                    if (result)
                        return Json(new { status = 1, message = "Cập nhật tài khoản thành công" });
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật thông tin tài khoản. Vui lòng liên hệ quản trị viên!" });
                }
                return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật tài khoản. Vui lòng liên hệ quản trị viên!" });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public string UploadFile(IFormFile file)
        {
            string str = null;
            if (file != null)
            {
                string path = _baseConfig["Configuration:UrlUserMMo"];

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                var extension = Path.GetExtension(file.FileName);
                var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                if (allowedExtensions.Contains(extension.ToLower()))
                {
                    //do some logic here because the data is smaller than 5mb   
                    string ImageName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                    //Get url To Save

                    string SavePath = Path.Combine(Directory.GetCurrentDirectory(), path, ImageName);
                    using (var stream = new FileStream(SavePath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    SavePath = SavePath.Replace(Directory.GetCurrentDirectory(), "");
                    str = SavePath;
                }
                else
                {
                    str = "1";
                }

            }
            return str;
        }

        [Route("user-changepassword.html")]
        public IActionResult ChangePassword()
        {

            return View();
        }
        public async Task<IActionResult> EditPass(string CurrentPassword, string NewPassword, string ReNewPassword)
        {
            try
            {
                var user = HttpContext.Session.GetObjectFromJson<UserMMODetail>("USER_DATA");
                if (user == null)
                {
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật tài khoản. Vui lòng liên hệ quản trị viên!" });
                }
                var gettoken = HttpContext.Session.GetObjectFromJson<UserMMODetail>("USER_DATA").Token;
                if (gettoken == null)
                {
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật tài khoản. Vui lòng liên hệ quản trị viên!" });
                }
                CurrentPassword = Security.GetMD5Hash(CurrentPassword);
                var obj = _userMmoServive.GetById(user.UserId);
                if (CurrentPassword != obj.Password)
                {
                    return Json(new { status = 0, message = "Mật khẩu cũ không đúng" });
                }
                if (NewPassword != ReNewPassword)
                {
                    return Json(new { status = 0, message = "Mật khẩu mới không giống nhau" });
                }
                NewPassword = Security.GetMD5Hash(NewPassword);
                obj.Password = NewPassword;
                var result = _userMmoServive.Update(user.UserId, obj);
                if (result)
                {
                    return Json(new { status = 1, message = "Cập nhật mật khẩu thành công.!" });
                }
                return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật tài khoản. Vui lòng liên hệ quản trị viên!" });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string UpdateGiud(int id)
        {

            var guid = Guid.NewGuid().ToString();
            var obj = new UserMMODTO();
            obj.Giud = guid;
            obj.UserId = id;
            var result = _userMmoServive.Update(id, obj);
            return guid;
        }
    }
}