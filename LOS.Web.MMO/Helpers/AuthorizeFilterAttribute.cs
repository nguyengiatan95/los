﻿using LOS.DAL.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Helpers
{
    public class AuthorizeFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var session = filterContext.HttpContext.Session;
            if (session == null || session.GetObjectFromJson<UserMMODetail>("USER_DATA") == null)
            {
                filterContext.Result = new RedirectToActionResult("Index", "Login", null);
            }
        }
    }
}
