﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Helpers
{
	public class Constants
	{
        public static string WEB_API_AUTHEN_URL { get; set; }
		public static string WEB_API_URL { get; set; }
		public static string LOGIN_ENDPOINT = "security/login";
        public static string USERS_ENDPOINT = "users";
        public static string USERS_GET_ALL = "users/getall";
        public static string USERS_GET_ENDPOINT = "users/{0}";
        public static string USERS_GET_USERNAME_ENDPOINT = "users/getuser?username={0}";
        public static string USERS_UPDATE_ENDPOINT = "users/{0}";
        public static string USERS_DELETE_ENDPOINT = "users/{0}";
        public static string GET_PROVINCES_ENPOINT = "/Dictionary/provinces";
        public static string GET_DISTRICTS_ENPOINT = "/Dictionary/districts?id={0}";
        public static string GET_WARDS_ENPOINT = "/Dictionary/wards?id={0}";


        public static string USERSMMO_LOGIN_ENDPOINT = "UserMMO/login";
        public static string USERSMMO_ENDPOINT = "UserMMO";
        public static string USERSMMO_GET_ENDPOINT = "UserMMO/{0}";
        public static string USERSMMO_GET_GUID_ENDPOINT = "UserMMO/getbyguid?guid={0}";
        public static string USERSMMO_GET_USERNAME_ENDPOINT = "UserMMO/getuser?username={0}";
        public static string USERSMMO_GET_PHONE_ENDPOINT = "UserMMO/GetByPhoneUser?phone={0}";
        public static string USERSMMO_UPDATE_ENDPOINT = "UserMMO/{0}";
        public static string USERSMMO_DELETE_ENDPOINT = "UserMMO/{0}";

        public static string USERREFERENCE_ENDPOINT = "UserReference";
        public static string USERREFERENCE_LINKS_ENDPOINT = "UserReference/GetByLinksUser";
        public static string USERREFERENCE_GET_LIST = "UserReference/GetListByUserId?id={0}";

        //Report
        public static string REPORT_LOANBRIEF_REGISTER_BY_USERMMO_ENDPOINT = "Report/ReporLoanRegisterOfUserMmo";
        public static string REPORT_LOANBRIEF_SUCCESSFULLSOLD_BY_USERMMO_ENDPOINT = "Report/ReporLoanSuccessFullySoldMMO";


        public static string GET_ALL_JOB_ENDPOINT { get; set; }
		public static string GET_JOB_ENDPOINT { get; set; }

        public static string GET_ALL_JOBTITLE_ENDPOINT { get; set; }
        public static string GET_JOBTITLE_ENDPOINT { get; set; }

        public static string GET_ALL_POSITION_ENDPOINT { get; set; }
        public static string GET_POSITION_ENDPOINT { get; set; }

        public static string GET_MKTREPORT_ENDPOINT { get; set; }

        public static void init()
		{
			GET_ALL_JOB_ENDPOINT = WEB_API_URL + "job";
			GET_JOB_ENDPOINT = WEB_API_URL + "job/{0}";
            GET_ALL_JOBTITLE_ENDPOINT = WEB_API_URL + "jobTitle";
            GET_JOBTITLE_ENDPOINT = WEB_API_URL + "jobTitle/{0}";
            GET_ALL_POSITION_ENDPOINT = WEB_API_URL + "position";
            GET_POSITION_ENDPOINT = WEB_API_URL + "position/{0}";
            GET_MKTREPORT_ENDPOINT = WEB_API_URL + "marketing/report";
        }


        
    }
}
