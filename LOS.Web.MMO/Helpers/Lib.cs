﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Helpers
{
    public class Lib
    {
        public static DateTime FirstDayOfWeek(DateTime dt)
        {
            // set monday is day start of week
            DayOfWeek startOfWeek = DayOfWeek.Monday;
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }
        public static DateTime LastDayOfWeek(DateTime date)
        {
            DateTime ldowDate = FirstDayOfWeek(date).AddDays(6);
            return ldowDate;
        }
    }
}
