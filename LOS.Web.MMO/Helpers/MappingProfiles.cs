﻿using AutoMapper;
using LOS.DAL.DTOs;
using LOS.Web.MMO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Helpers
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<UserMMODTO, UsersRegister>().ReverseMap();
            CreateMap<UserMMODetail, UsersRegister>().ReverseMap();
            CreateMap<UserMMODetail, UserMmoItem>().ReverseMap();
            CreateMap<UserMMODTO, UserMmoItem>().ReverseMap();
        }
    }
}
