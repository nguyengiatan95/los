﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Models
{
    public class ReportLoanBriefByUserMmo : DatatableBase
    {
        public ReportLoanBriefByUserMmo(IFormCollection form) : base(form)
        {
            name = form["query[filterName]"].FirstOrDefault();
            DateRanger = form["query[filtercreateTime]"].FirstOrDefault();
            
        }

        public string DateRanger { get; set; }
        public string name { get; set; }
        public int Affcode { get; set; }
        public int affstatus { get; set; }
      
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
    }
}
