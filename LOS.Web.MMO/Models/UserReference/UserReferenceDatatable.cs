﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Models
{
    public class UserReferenceDatatable : DatatableBase
    {
        public UserReferenceDatatable(IFormCollection form) : base(form)
        {
            isActive = form["query[isActive]"].FirstOrDefault();
            name = form["query[filterName]"].FirstOrDefault();
        }
        public string isActive { get; set; }
        public string name { get; set; }
        public int UserId { get; set; }
      
    }
}
