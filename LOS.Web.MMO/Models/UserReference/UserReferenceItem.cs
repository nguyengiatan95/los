﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Models
{
    public class UserReferenceItem
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? UserReferenceId { get; set; }
        public string AffReferenceCode { get; set; }
        public int? Level { get; set; }
        public string UserCode { get; set; }
    }
}
