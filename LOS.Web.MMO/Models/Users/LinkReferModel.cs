﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Models
{
    public class LinkReferModel
    {
        public string ReferralsLink { get; set; }
        public string AffiliateLink { get; set; }
        public string Url { get; set; }
    }
}
