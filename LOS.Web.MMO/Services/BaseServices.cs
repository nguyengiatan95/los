﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Core;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
using LOS.DAL.DTOs;
using Microsoft.AspNetCore.Mvc;
using LOS.Web.MMO.Helpers;

namespace LOS.Web.MMO.Services
{
    public class BaseServices
    {
        protected IConfiguration _baseConfig;
        public BaseServices(IConfiguration configuration)
        {
            _baseConfig = configuration;
        }
        protected async System.Threading.Tasks.Task<T> GetAsyncNotToken<T>(string uri,string baseUri = "")
        {
            string responseJson = string.Empty;
            using (var client = new HttpClient())
            {
                if (string.IsNullOrEmpty(baseUri))
                    baseUri = _baseConfig["Configuration:WebApiUrl"];
                client.BaseAddress = new System.Uri(baseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
                var response = await client.GetAsync(baseUri + uri);
                if (response.IsSuccessStatusCode)
                {
                    responseJson = await response.Content.ReadAsStringAsync();
                }
            }
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseJson);
        }
        protected async System.Threading.Tasks.Task<T> GetAsync<T>(string uri, string access_token , string baseUri = "")
        {
            string responseJson = string.Empty;
            using (var client = new HttpClient())
            {
                if (string.IsNullOrEmpty(baseUri))
                    baseUri = _baseConfig["Configuration:WebApiUrl"];
                client.BaseAddress = new System.Uri(baseUri);
                client.DefaultRequestHeaders.Accept.Clear();
               client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
                var response = await client.GetAsync(baseUri + uri);
                if (response.IsSuccessStatusCode)
                {
                    responseJson = await response.Content.ReadAsStringAsync();
                }
            }
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseJson);
        }
        protected async System.Threading.Tasks.Task<T> PostAsyncNotToken<T>(string uri,StringContent data, string baseUri = "")
        {
            string responseJson = string.Empty;
            using (var client = new HttpClient())
            {
                if (string.IsNullOrEmpty(baseUri))
                    baseUri = _baseConfig["Configuration:ApiUrl"];
                client.BaseAddress = new System.Uri(baseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
                var response = await client.PostAsync(baseUri + uri, data);
                if (response.IsSuccessStatusCode)
                {
                    responseJson = await response.Content.ReadAsStringAsync();
                }
            }

            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseJson);
        }

        protected async System.Threading.Tasks.Task<T> PostAsync<T>(string uri, string access_token, StringContent data, string baseUri = "")
        {
            string responseJson = string.Empty;
            using (var client = new HttpClient())
            {
                if (string.IsNullOrEmpty(baseUri))
                    baseUri = _baseConfig["Configuration:ApiUrl"];
                client.BaseAddress = new System.Uri(baseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
                var response = await client.PostAsync(baseUri + uri, data);
                if (response.IsSuccessStatusCode)
                {
                    responseJson = await response.Content.ReadAsStringAsync();
                }
            }

            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseJson);
        }
        protected async System.Threading.Tasks.Task<T> PutAsyncNotToken<T>(string uri, StringContent data, string baseUri = "")
        {
            string responseJson = string.Empty;
            using (var client = new HttpClient())
            {
                if (string.IsNullOrEmpty(baseUri))
                    baseUri = _baseConfig["Configuration:ApiUrl"];
                client.BaseAddress = new System.Uri(baseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
                var response = await client.PutAsync(baseUri + uri, data);
                if (response.IsSuccessStatusCode)
                {
                    responseJson = await response.Content.ReadAsStringAsync();
                }
            }

            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseJson);
        }
        protected async System.Threading.Tasks.Task<T> PutAsync<T>(string uri, string access_token, StringContent data, string baseUri = "")
        {
            string responseJson = string.Empty;
            using (var client = new HttpClient())
            {
                if (string.IsNullOrEmpty(baseUri))
                    baseUri = _baseConfig["Configuration:ApiUrl"];
                client.BaseAddress = new System.Uri(baseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
                var response = await client.PutAsync(baseUri + uri, data);
                if (response.IsSuccessStatusCode)
                {
                    responseJson = await response.Content.ReadAsStringAsync();
                }
            }

            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseJson);
        }

        protected async System.Threading.Tasks.Task<T> DeleteAsync<T>(string uri, string access_token, string baseUri = "")
        {
            string responseJson = string.Empty;
            using (var client = new HttpClient())
            {
                if (string.IsNullOrEmpty(baseUri))
                    baseUri = _baseConfig["Configuration:ApiUrl"];
                client.BaseAddress = new System.Uri(baseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
                var response = await client.DeleteAsync(baseUri + uri);
                if (response.IsSuccessStatusCode)
                {
                    responseJson = await response.Content.ReadAsStringAsync();
                }
            }

            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseJson);
        }

    }
}
