﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Services.Report
{
    public interface IReportService
    {
        List<LoanBriefDetail> ReporLoanRegisterOfUserMmo(string access_token, string param, ref int recordTotals);

        List<LoanBriefSuccessFullySoldDetail> ReporLoanSuccessFullySoldMMO(string access_token, string param, ref int recordTotals);
    }
}
