﻿using LOS.DAL.DTOs;
using LOS.Web.MMO.Helpers;
using LOS.Web.MMO.Models.Response;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Services.Report
{
    public class ReportService : BaseServices, IReportService
    {
        protected IConfiguration _baseConfig;
        public ReportService(IConfiguration configuration) : base(configuration)
        {
            _baseConfig = configuration;
        }
        public List<LoanBriefDetail> ReporLoanRegisterOfUserMmo(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefDetail> data = new List<LoanBriefDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>>(Constants.REPORT_LOANBRIEF_REGISTER_BY_USERMMO_ENDPOINT + param, access_token, _baseConfig["Configuration:WebApiUrl"]);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public List<LoanBriefSuccessFullySoldDetail> ReporLoanSuccessFullySoldMMO(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefSuccessFullySoldDetail> data = new List<LoanBriefSuccessFullySoldDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefSuccessFullySoldDetail>>>(Constants.REPORT_LOANBRIEF_SUCCESSFULLSOLD_BY_USERMMO_ENDPOINT + param, access_token, _baseConfig["Configuration:WebApiUrl"]);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }
    }
}
