﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Services.UserMMO
{
    public interface IUserMMOService
    {
        List<UserMMODetail> Search(string access_token, string param, ref int recordTotals);
        UserMMODetail GetById(int id);
        UserMMODetail GetByUser(string email);
        UserMMODetail GetByPhone(string phone);
        bool Update(int id, UserMMODTO entity);
        int Create(UserMMODTO entity);
        bool Delete(string access_token, int id);
        UserMMODetail GetByGuid(string guid);
    }
}
