﻿
using LOS.DAL.DTOs;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LOS.Web.MMO.Models.Response;
using LOS.Web.MMO.Helpers;

namespace LOS.Web.MMO.Services.UserMMO
{
    public class UserMMOService : BaseServices, IUserMMOService
    {
        protected IConfiguration _baseConfig;
        public UserMMOService(IConfiguration configuration) : base(configuration)
        {
            _baseConfig = configuration;
        }

        public UserMMODetail GetById(int id)
        {
            var result = new UserMMODetail();
            try
            {
                var response = GetAsyncNotToken<DefaultResponse<Meta, UserMMODetail>>(string.Format(Constants.USERSMMO_GET_ENDPOINT, id), _baseConfig["Configuration:WebApiUrl"]);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        public UserMMODetail GetByGuid(string guid)
        {
            var result = new UserMMODetail();
            try
            {
                var response = GetAsyncNotToken<DefaultResponse<Meta, UserMMODetail>>(string.Format(Constants.USERSMMO_GET_GUID_ENDPOINT, guid), _baseConfig["Configuration:WebApiUrl"]);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public UserMMODetail GetByUser(string email)
        {
            var result = new UserMMODetail();
            try
            {
                var response = GetAsyncNotToken<DefaultResponse<Meta, UserMMODetail>>(string.Format(Constants.USERSMMO_GET_USERNAME_ENDPOINT, email), _baseConfig["Configuration:WebApiUrl"]);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public UserMMODetail GetByPhone(string phone)
        {
            var result = new UserMMODetail();
            try
            {
                var response = GetAsyncNotToken<DefaultResponse<Meta, UserMMODetail>>(string.Format(Constants.USERSMMO_GET_PHONE_ENDPOINT, phone), _baseConfig["Configuration:WebApiUrl"]);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public List<UserMMODetail> Search(string access_token, string param, ref int recordsTotal)
        {
            List<UserMMODetail> data = new List<UserMMODetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<UserMMODetail>>>(Constants.USERSMMO_ENDPOINT + param, access_token, _baseConfig["Configuration:WebApiUrl"]);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordsTotal = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public bool Update(int id, UserMMODTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PutAsyncNotToken<DefaultResponse<object>>(string.Format(Constants.USERSMMO_UPDATE_ENDPOINT, id), stringContent, _baseConfig["Configuration:WebApiUrl"]);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int Create(UserMMODTO entity)
        {
            int Result = 0;
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsyncNotToken<DefaultResponse<dynamic>>(Constants.USERSMMO_ENDPOINT, stringContent, _baseConfig["Configuration:WebApiUrl"]);
                if (response.Result.meta.errorCode == 200)
                    Result = (int)response.Result.data;
            }
            catch (Exception ex)
            {
                return Result;
            }
            return Result;
        }

        public bool Delete(string access_token, int id)
        {
            try
            {
                var response = DeleteAsync<DefaultResponse<object>>(string.Format(Constants.USERSMMO_DELETE_ENDPOINT, id), access_token, _baseConfig["Configuration:WebApiUrl"]);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
