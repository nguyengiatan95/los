﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Services.UserReference
{
    public interface IUserReferenceService
    {
        List<UserReferenceDTO> GetByUserId(int Id);
        List<UserReferenceDetail> GetByLinksUser(string access_token, string param, ref int recordTotals);
        UserReferenceDetail GetById(int id);
        int Create(UserReferenceDTO entity);

    }
}
