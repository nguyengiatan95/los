﻿using Microsoft.Extensions.Configuration;
using LOS.DAL.DTOs;
using LOS.Web.MMO.Helpers;
using LOS.Web.MMO.Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.Web.MMO.Services.UserReference
{
     public class UserReferenceService : BaseServices, IUserReferenceService
    {
        protected IConfiguration _baseConfig;
        public UserReferenceService(IConfiguration configuration) : base(configuration)
        {
            _baseConfig = configuration;
        }

        public List<UserReferenceDetail> GetByLinksUser(string access_token, string param, ref int recordTotals)
        {
            List<UserReferenceDetail> data = new List<UserReferenceDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<UserReferenceDetail>>>(Constants.USERREFERENCE_LINKS_ENDPOINT + param, access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public List<UserReferenceDTO> GetByUserId(int Id)
        {
            var data = new List<UserReferenceDTO>();
            try
            {
                var response = GetAsyncNotToken<DefaultResponse<Meta, List<UserReferenceDTO>>>(string.Format(Constants.USERREFERENCE_GET_LIST, Id), _baseConfig["Configuration:WebApiUrl"]);
                if (response.Result.meta.errorCode == 200)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public UserReferenceDetail GetById(int id)
        {
            var result = new UserReferenceDetail();
            try
            {
                var response = GetAsyncNotToken<DefaultResponse<Meta, UserReferenceDetail>>(string.Format(Constants.USERSMMO_GET_ENDPOINT, id), _baseConfig["Configuration:WebApiUrl"]);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        public int Create(UserReferenceDTO entity)
        {
            int Result = 0;
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsyncNotToken<DefaultResponse<dynamic>>(Constants.USERREFERENCE_ENDPOINT, stringContent, _baseConfig["Configuration:WebApiUrl"]);
                if (response.Result.meta.errorCode == 200)
                    Result = (int)response.Result.data;
            }
            catch (Exception ex)
            {
                return Result;
            }
            return Result;
        }
       
    }
}
