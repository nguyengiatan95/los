﻿var App = new function () {

	var access_token = "";

	var SetToken = function (token) {
		this.access_token = token;
	}

	var Init = function () {
		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		};

		var placeholderElement = $('#modal-placeholder');

		$(document).on('click', 'button[data-toggle="ajax-modal"]', function (event) {
			var url = $(this).data('url');
			placeholderElement.remove();
			$.get(url).done(function (data) {
				placeholderElement.html(data);
				placeholderElement.find('.modal').modal('show');
			});
		});

		$(document).on('click', 'a[data-toggle="ajax-modal"]', function (event) {
			var url = $(this).data('url');
			placeholderElement.remove();
			$.get(url).done(function (data) {
				placeholderElement.html(data);
				placeholderElement.find('.modal').modal('show');
			});
        });

        $('#filtercreateTime').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: moment().startOf('isoweek').toDate(),
            endDate: moment().endOf('isoweek').toDate(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY',
                firstDay: 1,
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7",
                ],
                "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ],
            },

        });
	}

	var ShowInfo = function (message) {
		toastr.info(message);
	}
    var ShowSuccessNotLoad = function (message) {
        toastr.success(message);
	}
	var ShowErrorNotLoad = function (message) {
		toastr.error(message);
	}
    var ShowSuccess = function (message) {
        setTimeout(function () { 
            location.reload()
        }, 1000);
		toastr.success(message);
    }
    var ShowSuccessRedirect = function (message,url) {
        setTimeout(function () {
            window.location.href =url;
        }, 1000);
        toastr.success(message);
    }

	var ShowWarning = function (message) {
		toastr.warning(message);
	}

	var ShowError = function (message) {
		toastr.error(message);
	}

	var Ajax = function (method, endpoint, data) {
		return $.ajax({
			type: method,
			url: endpoint,
			data: JSON.stringify(data),
			dataType: "json",
			contentType: "application/json; charset=utf-8",
			headers: {
				"Authorization": "bearer " + this.access_token
			},
		});
	}

	

	return {
		access_token: access_token,
		ShowInfo: ShowInfo,
        ShowSuccess: ShowSuccess,
        ShowSuccessRedirect: ShowSuccessRedirect,
		ShowWarning: ShowWarning,
		ShowError: ShowError,
		Ajax: Ajax,
		SetToken: SetToken,
		Init: function () {
			Init();
		}
	}
}

$(document).ready(function () {
	App.Init();
});