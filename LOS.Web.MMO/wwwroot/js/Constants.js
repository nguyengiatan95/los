﻿var Constants = new function () {
	return {
		LOGIN_ERROR_MESSAGE: "Đăng nhập thất bại",
		LOGIN_WRONG_PASSWORD_MESSAGE: "Đăng nhập thất bại, sai mật khẩu",
		LOGIN_SUCCESS_MESSAGE: "Đăng nhập thành công",
	}
}