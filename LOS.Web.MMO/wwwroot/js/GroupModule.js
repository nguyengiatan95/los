﻿var GroupModule = new function () {

    var Init = function () {

        var datatable = $('.m-datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Group/LoadData'
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                scroll: true,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'groupName',
                    title: 'Nhóm người dùng',
                    sortable: false
                },
                {
                    field: 'description',
                    title: 'Mô tả',
                    sortable: false
                },
                {
                    field: 'createdAt',
                    title: 'Thời gian tạo',
                    sortable: false,
                    template: function (row, index, datatable) {
                        return formattedDateHourMinutes(row.createdAt)
                    }
                },
                //{
                //    field: 'updatedAt',
                //    title: 'Thời gian sửa',
                //    sortable: false,
                //    template: function (row, index, datatable) {
                //        return formattedDateHourMinutes(row.updatedAt)
                //    }
                //},
                {
                    field: 'defaultPath',
                    title: 'Đường dẫn',
                    sortable: false
                }
                , {
                    field: 'Action',
                    title: 'Action',
                    sortable: false,
                    width: 100,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return '\
							<a href="/group?id='+ row.groupId + '" class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only" >\
                                <i class="la la-edit"></i></a>\
	                <a href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only"  onclick="GroupModule.DeleteGroup('+ row.groupId + ',\'' + row.groupName + '\')" >\
                                <i class="la la-times"></i></a>';
                    }
                }
            ]
        });
      
        $("#filterName").on('keydown', function (e) {
            if (e.which == 13) {
                datatable.API.params.pagination.page = 1
                datatable.search($(this).val().toLowerCase(), 'filterName');
                return false;
            }
        });
    };

    var AddGroup = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            rules: {
                GroupName: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                Description: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                }
            },
            messages: {
                GroupName: {
                    required: "Bạn chưa nhập nhóm tài khoản",
                },
                Description: {
                    required: "Bạn chưa nhập mô tả",
                }
            }
        });

        if (!form.valid()) {
            return;
        }
        form.ajaxSubmit({
            url: '/Group/CreateGroup',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccessRedirect(data.message,'/group-manager.html');
                    }
                    else {
                        App.ShowError(data.message);
                    }
                }
                else {
                    App.ShowError(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };

    var ShowModel = function (id) {
        $('#modelGroup').html('');
        $.ajax({
            type: "POST",
            url: "/Group/CreateGroupModalPartial",
            data: { Id: id },
            success: function (data) {
                $('#modelGroup').html(data);
                $('#m_modal_Group').modal('show');
            },
            traditional: true
        });
    }

    var DeleteGroup = function (id, username) {
        swal({
            title: 'Cảnh báo',
            text: "Bạn có chắc chắn muốn  xóa tài khoản " + username + "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xoá'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Group/DeleteGroup",
                    data: { Id: id },
                    success: function (data) {
                        if (data.status == 1) {
                            location.reload();
                            swal(
                                'Xóa nhóm tài khoản thành công!'
                            )

                        } else {
                            App.ShowError(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var GetData = function (id) {
        $.ajax({
            async: true,
            type: "POST",
            url: '/Module/GetDataModule',
            data: { Id: id },
            beforeSend: function () {
                mApp.block('#m_tree_3', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'success',
                    message: 'Please wait...'
                });
            },
            dataType: "json",
            success: function (data) {
                loadData(data.data);
                mApp.unblock('#m_tree_3');
            }
        });
    }

    var loadData = function (jsondata) {
        $("#m_tree_3").jstree('destroy');
        $('#m_tree_3').on('changed.jstree', function (e, data) {

            var arr = [];
            var nodes = $('#m_tree_3').jstree('get_selected', true);
            $.each(nodes, function (index, item) {
                arr.push(item.id);
                arr.push(item.parent);
            });
            var removeItem = "#";
            arr = jQuery.grep(arr, function (value) {
                return value != removeItem;
            });
            var unique = arr.filter(function (itm, i, arr) {
                return i == arr.indexOf(itm);
            });
            $('#lstMenu').val(unique);
            console.log($('#lstMenu').val())
          
        }).jstree({
            'plugins': ["wholerow", "checkbox", "types"],
            'core': {
                "check_callback": false,
                "themes": {
                    "responsive": false
                },
                'data': jsondata
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder m--font-success"
                },
                "file": {
                    "icon": "fa fa-file  m--font-warning"
                }
            },
        });
    }
    return {
        Init: Init,
        AddGroup: AddGroup,
        ShowModel: ShowModel,
        DeleteGroup: DeleteGroup,
        GetData: GetData
    };
}
