﻿var JobModule = new function () {
    var Init = function () {
        var datatable = $('.m-datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Job/LoadData'
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                scroll: true,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                input: $('#filterName'),
            },
            columns: [
                //{
                //    //field: 'jobId',
                //    //title: '#',
                //    //sortable: false,
                //    //width: 20,
                //    //selector: { class: 'm-checkbox--solid m-checkbox--brand' },
                //},
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'name',
                    title: 'Tên',
                    sortable: false
                },
                {
                    field: 'priority',
                    title: 'Độ ưu tiên',
                    sortable: false
                },
                {
                    field: 'action',
                    title: 'Action',
                    sortable: false,
                    template: function (row, index, datatable) {
                        return '\
							<a href="#" class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only" onclick="JobModule.ShowUpdate('+ row.jobId + ')">\
                                <i class="la la-edit"></i></a>\
	                        <a href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only"  onclick="JobModule.DeleteJob('+ row.jobId + ',\'' + row.name + '\')" >\
                                <i class="la la-times"></i></a>';
                    }
                },

            ]
        });

        $('#filterStatus').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'status');
        });
    };

    var ShowModel = function () {
        $('#modelJob').html('');
        $.ajax({
            type: "POST",
            url: "/Job/CreateJobModalPatial",
            success: function (data) {
                $('#modelJob').html(data);
                $('#m_modal_4').modal('show');
            },
            traditional: true
        });
    };

    var ShowUpdate = function (id) {
        $('#modelJob').html('');
        $.ajax({
            type: "POST",
            url: "/Job/UpdateJobModalPartial",
            data: { Id: id },
            success: function (data) {
                $('#modelJob').html(data);
                $('#m_modal_4').modal('show');
            },
            traditional: true
        });
    }

    var AddJob = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            rules: {
                name: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                priority: {
                    required: true,
                    number: true
                }
            },
            messages: {
                name: {
                    required: "Bạn chưa nhập tên công việc",
                },
                priority: {
                    required: "Bạn chưa nhập mức độ ưu tiên",
                    number: "Mức độ ưu tiên nhập vào phải là số"
                }
            }
        });

        if (!form.valid()) {
            return;
        }
        form.ajaxSubmit({
            url: '/Job/CreateJob',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccess(data.message);
                        location.reload();
                    }
                    else {
                        App.ShowError(data.message);
                    }
                }
                else {
                    App.ShowError(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };

    var DeleteJob = function (id, name) {
        swal({
            title: 'Cảnh báo',
            text: "Bạn có chắc chắn muốn  xóa " + name + "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xoá'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Job/DeleteJob",
                    data: { Id: id },
                    success: function (data) {
                        if (data.status == 1) {
                            location.reload();
                            swal(
                                'Xóa thành công!'
                            )
                        } else {
                            App.ShowError(data.message);
                        }
                    },
                    traditional: true
                });
            }
        });
    }

    var UpdateJob = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            rules: {
                name: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                priority: {
                    required: true,
                    number: true
                }
            },
            messages: {
                name: {
                    required: "Bạn chưa nhập tên công việc",
                },
                priority: {
                    required: "Bạn chưa nhập mức độ ưu tiên",
                    number: "Mức độ ưu tiên nhập vào phải là số"
                }
            }
        });

        if (!form.valid()) {
            return;
        }
        form.ajaxSubmit({
            url: '/Job/UpdateJob',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccess(data.message);
                        location.reload();
                    }
                    else {
                        App.ShowError(data.message);
                    }
                }
                else {
                    App.ShowError(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };

    return {
        Init: Init,
        ShowModel: ShowModel,
        AddJob: AddJob,
        DeleteJob: DeleteJob,
        ShowUpdate: ShowUpdate,
        UpdateJob: UpdateJob
    };
}

$(document).ready(function () {
    JobModule.Init();
});