﻿var UserReferenceModule = new function () {
    var LinksUserMMO = function () {
        var datatable = $('.m-datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/UserReference/LoadDataLinksUserMMO'
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: '',
                scroll: true,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'userMMO.fullName',
                    title: 'Tên thành viên',
                    sortable: false
                },
                {
                    field: 'userMMO.email',
                    title: 'Email',
                    sortable: false
                },
                {
                    field: 'userMMO.phone',
                    title: 'Số điện thoại',
                    sortable: false
                },
                {
                    field: 'userMMO.address',
                    title: 'Địa chỉ',
                    sortable: false
                },
                {
                    field: 'level',
                    title: 'Level',
                    sortable: false
                },
                {
                    field: 'userMMO.createDate',
                    title: 'Ngày tạo',
                    sortable: false,
                    template: function (row, index, datatable) {
                        return formattedDateHourMinutes(row.userMMO.createDate)
                    }
                },
                {
                    field: 'userMMO.isActive',
                    title: 'Trạng thái',
                    sortable: false,
                    template: function (row) {
                        if (row.userMMO.isActive == true) {
                            return '<span class="m-badge m-badge--success m-badge--wide"> Hoạt động </span>';
                        } else {
                            return '<span class="m-badge m-badge--metal m-badge--wide"> Khóa </span>';
                        }

                    }
                }
            ]
        });

        $('#filterIsActive').on('change', function () {
            datatable.API.params.pagination.page = 1;
            datatable.search($(this).val().toLowerCase(), 'isActive');
        });

        $("#filterName").on('keydown', function (e) {
            if (e.which == 13) {
                datatable.API.params.pagination.page = 1;
                datatable.search($(this).val().toLowerCase(), 'filterName');
                return false;
            }
        });
        $('#filterIsActive').selectpicker();
    };

    return {
        LinksUserMMO, LinksUserMMO
    };
}
