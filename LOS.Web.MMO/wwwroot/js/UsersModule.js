﻿var UsersModule = new function () {
    var InformationUser = function () {
        var datatable = $('.m-datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Users/LoadDataInformationUser'
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: '',
                scroll: true,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Họ tên',
                    sortable: false
                },
                {
                    field: 'email',
                    title: 'Email',
                    sortable: false
                },
                {
                    field: 'phone',
                    title: 'Số điện thoại',
                    sortable: false
                },
                {
                    field: 'address',
                    title: 'Địa chỉ',
                    sortable: false
                },
                {
                    field: 'affCode',
                    title: 'Mã liên kết',
                    sortable: false
                },
                {
                    field: 'createDate',
                    title: 'Ngày tạo',
                    sortable: false,
                    template: function (row, index, datatable) {
                        return formattedDateHourMinutes(row.createDate)
                    }
                },
                {
                    field: 'Action',
                    title: 'Action',
                    sortable: false,
                    width: 100,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return '\
							<a href="#" class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only" onclick="UsersModule.ShowModelUserMmo('+ row.userId + ')">\
                                <i class="la la-edit"></i></a>';
                    }
                }
            ]
        });

    };
    var AddUserMmo = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            focusInvalid: false,
            rules: {
                FullName: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                Email: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    },
                    email: true
                },
                Phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 11,
                },
                Cmt: {
                    required: true,
                }
            },
            messages: {
                FullName: {
                    required: "Bạn chưa nhập họ tên",

                },
                Email: {
                    required: "Bạn chưa nhập email",
                    email: "Định dạng email không đúng"
                },
                Phone: {
                    number: "Số điện thoại không hợp lệ",
                    minlength: "Số điện thoại không hợp lệ ",
                    maxlength: "Số điện thoại không hợp lệ",
                }, Cmt: {
                    required: "Bạn chưa nhập số CMND/Thẻ căn cước",
                }
            },

        });
        if (!form.valid()) {
            return;
        }
        form.ajaxSubmit({
            url: '/Users/EditUser',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccess(data.message);
                        $('#create-user').modal('toggle');
                    }
                    else {
                        App.ShowError(data.message);
                    }
                }
                else {
                    App.ShowError(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };
    var ChangePassword = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            focusInvalid: false,
            rules: {
                CurrentPassword: {
                    required: true,
                },
                NewPassword: {
                    required: true,
                },
                ReNewPassword: {
                    required: true,
                    equalTo: "#NewPassword"
                }
            },
            messages: {
                CurrentPassword: {
                    required: "Bạn chưa nhập mật khẩu cũ",
                },
                NewPassword: {
                    required: "Bạn chưa nhập mật khẩu mới",
                },
                ReNewPassword: {
                    required: "Bạn chưa nhập lại mật khẩu mới",
                    equalTo : "Mật khẩu không giống nhau"
                }
            },

        });
        if (!form.valid()) {
            return;
        }
        form.ajaxSubmit({
            url: '/Users/EditPass',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccess(data.message);
                        $('#create-user').modal('toggle');
                    }
                    else {
                        App.ShowError(data.message);
                    }
                }
                else {
                    App.ShowError(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };
    var ShowModelUserMmo = function (id) {
        $('#modelUserMmo').html('');
        $.ajax({
            type: "POST",
            url: "/Users/CreateUserMmoModal",
            data: { Id: id },
            success: function (data) {
                $('#modelInfor').html(data);
                $('#m_modal_4').modal('show');
            },
            traditional: true
        });
    }
    var AddUsers = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            rules: {
                FullName: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                Email: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    },
                    email: true
                },
                Password: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                rPassword: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                Phone: {
                    required: true,
                    //matches: "^(\\d|\\s)+$",
                    number: true,
                    minlength: 10,
                    maxlength: 11,
                },
                Agree: {
                    required: true
                }
            },
            messages: {
                FullName: {
                    required: "Bạn chưa nhập họ tên",
                    class: "has-danger"
                },
                Email: {
                    required: "Bạn chưa nhập email",
                    email: "Định dạng email không đúng",
                    class: "has-danger"
                },
                Password: {
                    required: "Bạn chưa nhập mật khẩu",
                    class: "has-danger"
                },
                rPassword: {
                    required: "Bạn chưa nhập lại mật khẩu",
                    class: "has-danger"
                },
                Phone: {
                    required: "Bạn chưa nhập số điện thoại",
                    // matches: "^(\\d|\\s)+$",
                    number: "Số điện thoại không hợp lệ",
                    minlength: "Số điện thoại không hợp lệ ",
                    maxlength: "Số điện thoại không hợp lệ",
                    class: "has-danger"
                },
                Agree: {
                    required: "Bạn chưa chọn đồng ý các điều khoản và điều lệ"
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        $(btn).addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);				
        form.ajaxSubmit({
            url: '/Users/CreateUser',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        App.ShowSuccessRedirect(data.message, '/login.html');
                        //$('#create-user').modal('toggle');
                    }
                    else {
                        App.ShowError(data.message);
                    }
                }
                else {
                    App.ShowError(data.message);
                }
            }
        });
    };
    var ForgotPassword = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            rules: {
                Email: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    },
                    email: true
                }
            },
            messages: {
                Email: {
                    required: "Bạn chưa nhập email",
                    email: "Định dạng email không đúng",
                    class: "has-danger"
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
        form.ajaxSubmit({
            url: '/Users/SendMailRestPassword',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                  
                    if (data.status == 1) {
                        $('#txtError').text(data.message)
                        $('#txtError').removeAttr('disabled');
                        App.ShowSuccessNotLoad(data.message);
                    }
                    else {
                        $('#txtError').text(data.message)
                        $('#txtError').removeAttr('disabled');
                        App.ShowError(data.message);
                    }
                }
                else {
                    App.ShowError(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };
    return {
        InformationUser: InformationUser,
        AddUserMmo: AddUserMmo,
        ChangePassword: ChangePassword,
        ShowModelUserMmo: ShowModelUserMmo,
        AddUsers: AddUsers,
        ForgotPassword: ForgotPassword
    };
}
