﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class AccessTokenController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        public AccessTokenController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

       
        [HttpGet]
        [Route("get_last_token")]
        public ActionResult<DefaultResponse<TokenDetail>> GetLastToken(string app_id)
        {
            var def = new DefaultResponse<TokenDetail>();
            var data = unitOfWork.AccessTokenRepository.Query(x => x.AppId == app_id, null, false).OrderByDescending(x=>x.Id).Select(TokenDetail.ProjectionDetail).FirstOrDefault();
            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }
       
        [HttpPost]
        [Route("add_token")]
        public ActionResult<DefaultResponse<object>> AddToken([FromBody]AccessToken entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                entity.CreatedAt = DateTime.Now;
                var result = unitOfWork.AccessTokenRepository.Insert(entity);
                unitOfWork.Save();
                if (result.Id > 0)
                {
                    def.data = result.Id;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Token/AddToken Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


    }
}