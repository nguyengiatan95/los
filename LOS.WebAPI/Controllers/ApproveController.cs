﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Utils;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using LOS.Common.Models.Request;
using LOS.Common.Extensions;
using Newtonsoft.Json;
using System.Security.Claims;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ApproveController : BaseController
    {
        private IUnitOfWork unitOfWork;
        public ApproveController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("search")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>> Search([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] int loanBriefId = -1, [FromQuery] string search = null, [FromQuery] int hubId = -1, [FromQuery] int status = -1, [FromQuery] int coordinatorUserId = -1)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefDetail>>();
            try
            {
                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done && (x.LoanBriefId == loanBriefId || loanBriefId == -1)
                        && ((x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null) || (x.FullName.Contains(search) || search == null))
                        && (x.Status == status || status == -1)
                        && (x.CoordinatorUserId == coordinatorUserId || coordinatorUserId == -1)
                        && (x.HubId == hubId || hubId == -1));
                //filter by user
                var filterService = new FilterServices<LoanBrief>(unitOfWork);
                filterService.GetFilter(GetUserId(), ref filter, !(status > 0));

                var query = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefDetail.ProjectionDetail);
                var totalRecords = query.Count();
                List<LoanBriefDetail> data;
                if (skip == -1)
                    data = query.OrderByDescending(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                {
                    data = query.Skip(skip).Take(take).ToList();
                    pageSize = take;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Approved/Search Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [Route("search_emp")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>> SearchEmp([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] int loanBriefId = -1, [FromQuery] string search = null, [FromQuery] int status = -1, [FromQuery] int productId = -1,
            [FromQuery] int provinceId = -1, [FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null, [FromQuery] string shopName = null, [FromQuery] int coordinatorUserId = -1)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefDetail>>();
            try
            {
                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done && (x.LoanBriefId == loanBriefId || loanBriefId == -1)
                        && ((x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null) || (x.FullName.Contains(search) || search == null))
                        && (x.Status == status || status == -1)
                        && (x.ProductId == productId || productId == -1)
                        && (x.ProvinceId == provinceId || provinceId == -1)
                        && (x.CreatedTime >= fromDate || fromDate == null)
                        && (x.CreatedTime <= toDate || toDate == null)
                        && (x.CoordinatorUserId == coordinatorUserId || coordinatorUserId == -1)
                        && ((x.Hub.Name.Contains(shopName) || shopName == null)));
                //filter by user
                var filterService = new FilterServices<LoanBrief>(unitOfWork);
                filterService.GetFilter(GetUserId(), ref filter, !(status > 0));

                var query = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefDetail.ProjectionDetail);

                var totalRecords = query.Count();
                List<LoanBriefDetail> data;
                if (skip == -1)
                    data = query.OrderByDescending(x => x.HubPushAt).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                {
                    data = query.Skip(skip).Take(take).ToList();
                    pageSize = take;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Approved/Search Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("disbursement_after")]
        public ActionResult<DefaultResponse<SummaryMeta, GetDisbursementAfter>> DisbursementAfter([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] int loanBriefId = -1, [FromQuery] string search = null, [FromQuery] int provinceId = -1, [FromQuery] int status = -1,
            [FromQuery] int productId = -1, [FromQuery] int locate = -1, [FromQuery] int hubId = -1, [FromQuery] int coordinatorUserId = -1, [FromQuery] int export = 0,
            [FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null, [FromQuery] int borrowCavet = -1, [FromQuery] int typeSearch = -1, 
            [FromQuery] int topup = -1,[FromQuery] int empHubId = -1, [FromQuery] int telesaleId = -1)
        {
            var def = new DefaultResponse<SummaryMeta, GetDisbursementAfter>();
            try
            {

                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0)
                == (int)EnumInProcess.Done &&
                        (x.Status == EnumLoanStatus.DISBURSED.GetHashCode())
                        && (x.LoanBriefId == loanBriefId || loanBriefId == -1)
                        && ((x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null) || (x.FullName.Contains(search) || search == null))
                        && (x.ProvinceId == provinceId || provinceId == -1)
                        && (x.ProductId == productId || productId == -1)
                        && (x.HubId == hubId || hubId == -1)
                        && (x.CoordinatorUserId == coordinatorUserId || coordinatorUserId == -1)
                        && (x.TypeRemarketing == topup || topup == -1)
                        && (x.HubEmployeeId == empHubId || empHubId == -1)
                        && (x.BoundTelesaleId == telesaleId || telesaleId == -1)
                        );
                if(borrowCavet == (int)EnumReCareLoanbrief.BorrowCavet)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ReCareLoanbrief == (int)EnumReCareLoanbrief.BorrowCavet));
                else if (borrowCavet == (int)EnumReCareLoanbrief.NotBorrowCavet)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ReCareLoanbrief != (int)EnumReCareLoanbrief.BorrowCavet));
                else if (borrowCavet == (int)EnumReCareLoanbrief.PostRegistration)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LoanBriefProperty.PostRegistration == true));
                if (typeSearch == -1 && topup == -1)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.DisbursementAt >= fromDate && x.DisbursementAt < toDate));
                if (typeSearch == 1)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ToDate >= fromDate && x.ToDate < toDate));
                //filter by user
                var filterService = new FilterServices<LoanBrief>(unitOfWork);
                filterService.GetFilter(GetUserId(), ref filter, false);

                var query = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefDetail.ProjectionDetail);
                if (status == 1)
                    query = query.Where(x => x.IsUploadState == true);
                if (status == 0)
                    query = query.Where(x => x.IsUploadState == false);
                if (locate == 1)
                    query = query.Where(x => x.IsLocate == true);
                if (locate == 0)
                    query = query.Where(x => x.IsLocate == false);
                var totalRecords = query.Count();
                decimal totalMoney = 0;
                if (totalRecords > 0)
                    totalMoney = query.Sum(x => x.LoanAmount.Value);
                List<LoanBriefDetail> data;
                if (export > 0)
                    data = query.OrderByDescending(x => x.DisbursementAt).ToList();
                else
                {
                    if (skip == -1)
                        data = query.OrderByDescending(x => x.DisbursementAt).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    else
                    {
                        data = query.Skip(skip).Take(take).ToList();
                        pageSize = take;
                    }
                }
                
                
                var objData = new GetDisbursementAfter
                {
                    TotalMoney = totalMoney,
                    lstLoan = data
                };
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = objData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Approved/Search Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("confirm_loan_money")]
        public ActionResult<DefaultResponse<object>> ConfirmLoanMoney(LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                var userId = int.Parse(identity.Claims.Where(c => c.Type == "Id").Select(c => c.Value).SingleOrDefault());

                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                var loanbriefHistory = new LoanBriefHistory()
                {
                    LoanBriefId = entity.LoanBriefId,
                    OldValue = JsonConvert.SerializeObject(loanbrief, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    }),
                    ActorId = EnumActor.TDHSComfirm.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    UserId = userId
                };
                loanbrief.ActionState = EnumActionPush.Push.GetHashCode();
                loanbrief.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                loanbrief.InProcess = EnumInProcess.Process.GetHashCode();
                loanbrief.LoanAmount = entity.LoanAmount;
                loanbrief.RateTypeId = entity.RateTypeId;
                loanbrief.LoanTime = entity.LoanTime;
                loanbrief.ReceivingMoneyType = entity.ReceivingMoneyType;
                loanbrief.BankId = entity.BankId;
                loanbrief.BankAccountName = entity.BankAccountName;
                loanbrief.BankAccountNumber = entity.BankAccountNumber;
                loanbrief.BankCardNumber = entity.BankCardNumber;
                loanbrief.FeeInsuranceOfCustomer = entity.FeeInsuranceOfCustomer;
                //loanbrief.BuyInsurenceCustomer = entity.BuyInsurenceCustomer;
                loanbrief.CoordinatorPushAt = DateTime.Now;
                //loanbrief.LoanBriefComment = entity
                unitOfWork.LoanBriefRepository.Update(loanbrief);
                //lưu thông tin log
                loanbriefHistory.NewValue = JsonConvert.SerializeObject(loanbrief, Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                });
                unitOfWork.LoanBriefHistoryRepository.Insert(loanbriefHistory);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Approve/ConfirmLoanMoney POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}