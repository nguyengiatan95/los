﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Utils;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using LOS.Common.Models.Request;
using LOS.Common.Extensions;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Authorization;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class BODController : BaseController
    {
        private IUnitOfWork unitOfWork;
        public BODController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("search")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefSearchDetail>>> Search([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] int loanBriefId = -1, [FromQuery] string search = null, [FromQuery] int typeLoan = -1)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefSearchDetail>>();
            try
            {

                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done
                && (x.LoanBriefId == loanBriefId || loanBriefId == -1)
                && ((x.FullName.Contains(search) || search == null) || (x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null)));
                if (typeLoan == (int)EnumTypeLoanSearchBOD.Duyet_huy_don)
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.BOD_APPROVE_CANCEL
                    || x.Status == (int)EnumLoanStatus.APPROVAL_LEADER_APPROVE_CANCEL));
                }
                else if (typeLoan == (int)EnumTypeLoanSearchBOD.Xe_may_ngoai_le)
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.BOD_REVIEW && x.IsExceptions.GetValueOrDefault(0) == 1
                    && (x.ProductId == (int)EnumProductCredit.MotorCreditType_CC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)));
                }
                else if (typeLoan == (int)EnumTypeLoanSearchBOD.Oto)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.BOD_REVIEW
                        && (x.ProductId == (int)EnumProductCredit.OtoCreditType_CC || x.ProductId == (int)EnumProductCredit.OtoCreditType_KCC)
                    ));


                var query = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefSearchDetail.ProjectionDetail);
                var totalRecords = query.Count();
                List<LoanBriefSearchDetail> data;
                if (skip == -1)
                    data = query.OrderByDescending(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                {
                    data = query.Skip(skip).Take(take).ToList();
                    pageSize = take;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "BOD/Search Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("business_manager_search")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefSearchDetail>>> BusinessManagerSearch([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] int loanBriefId = -1, [FromQuery] string search = null, [FromQuery] int typeLoan = -1, [FromQuery] string shops = "")
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefSearchDetail>>();
            try
            {

                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done
                && x.Status == (int)EnumLoanStatus.WAIT_CCO_APPROVE
                && (x.LoanBriefId == loanBriefId || loanBriefId == -1)
                && ((x.FullName.Contains(search) || search == null) || (x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null)));

                if (typeLoan == (int)EnumBusinessFilter.XM)
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.IsExceptions.GetValueOrDefault(0) == 1
                    && (x.ProductId == (int)EnumProductCredit.MotorCreditType_CC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)));
                }
                else if (typeLoan == (int)EnumBusinessFilter.Oto)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ProductId == (int)EnumProductCredit.OtoCreditType_CC || x.ProductId == (int)EnumProductCredit.OtoCreditType_KCC));

                if (!string.IsNullOrEmpty(shops))
                {
                    List<int> lstShops = shops.Split(",").Select<string, int>(int.Parse).ToList();
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => lstShops.Contains(x.HubId.Value)));
                }


                var query = unitOfWork.LoanBriefRepository
                .Query(filter.Apply(), null, false).Select(LoanBriefSearchDetail.ProjectionDetail);
                var totalRecords = query.Count();
                List<LoanBriefSearchDetail> data;
                if (skip == -1)
                    data = query.OrderByDescending(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                {
                    data = query.Skip(skip).Take(take).ToList();
                    pageSize = take;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "BOD/BusinessManagerSearch Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

    }
}