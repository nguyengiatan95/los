﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace LOS.WebAPI.Controllers
{
    public class BaseController : ControllerBase
    {
        protected int GetUserId()
        {
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                return int.Parse(identity.Claims.Where(c => c.Type == "Id").Select(c => c.Value).SingleOrDefault());
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
    }
}