﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
using LOS.Common.Utils;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using LOS.DAL.EntityFramework;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class ContactCustomerController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;

        public ContactCustomerController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
        }

        [HttpGet]
        [Route("GetAllContactCustomer")]
        public ActionResult<DefaultResponse<SummaryMeta, List<ContactCustomerDetails>>> GetAllContactCustomer([FromQuery] int page = 1, [FromQuery] int pageSize = 10,
            [FromQuery] int createBy = 0, [FromQuery] string phone = null, [FromQuery] string name = null, [FromQuery] int province = 0,
            [FromQuery] string sortBy = "CreateDate", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<ContactCustomerDetails>>();
            try
            {
                var query = _unitOfWork.ContactCustomerRepository.Query(x => x.CreateBy == createBy && x.Status != 0, null, false);
                if (name != null)
                {
                    query = query.Where(x => x.FullName.Contains(name));
                }
                if (phone != null)
                {
                    query = query.Where(x => x.Phone.Contains(phone));
                }
                if (province != 0)
                {
                    query = query.Where(x => x.ProvinceId == province);
                }
                var totalRecords = query.Count();
                List<ContactCustomerDetails> data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).Select(ContactCustomerDetails.ProjectionDetail).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ContactCustomer/GetAllContactCustomer Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_contactcustomer")]
        public ActionResult<DefaultResponse<object>> AddContact([FromBody] ContactCustomer entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (_unitOfWork.ContactCustomerRepository.Any(x => x.Phone == entity.Phone))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại đã được tạo  thông tin liên hệ trước đó!");
                    return Ok(def);
                }
                if (_unitOfWork.LoanBriefRepository.Any(x => x.Phone == entity.Phone))
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Số điện thoại đã được tạo  thông tin liên hệ trước đó!");
                    return Ok(def);
                }

                _unitOfWork.ContactCustomerRepository.Insert(entity);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.ContactId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ContactCustomer/AddNote Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("getcontactcustomer")]
        [HttpGet]
        public ActionResult<DefaultResponse<ContactCustomer>> GetContactCustomer(int id)
        {
            var contact = _unitOfWork.ContactCustomerRepository.Query(x => x.ContactId == id, null, false).FirstOrDefault();
            var def = new DefaultResponse<ContactCustomer>();
            def.meta = new Meta(200, "success");
            def.data = contact;
            return Ok(def);
        }

        [Route("getcontactcustomerbyphone")]
        [HttpGet]
        public ActionResult<DefaultResponse<ContactCustomer>> GetContactCustomerByPhone(string phone)
        {
            var contact = _unitOfWork.ContactCustomerRepository.Query(x => x.Phone == phone && x.Status !=0, null, false).FirstOrDefault();
            var def = new DefaultResponse<ContactCustomer>();
            def.meta = new Meta(200, "success");
            def.data = contact;
            return Ok(def);
        }

        [HttpPut]
        [Route("{id}")]
        public ActionResult<DefaultResponse<object>> PutContact(int id, [FromBody] ContactCustomerDTO contact)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != contact.ContactId)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var entityOld = _unitOfWork.ContactCustomerRepository.GetById(contact.ContactId);
                entityOld.ProvinceId = contact.ProvinceId;
                entityOld.DistrictId = contact.DistrictId;
                entityOld.WardId = contact.WardId;
                entityOld.JobId = contact.JobId;
                entityOld.HaveCarsOrNot = contact.HaveCarsOrNot;
                entityOld.BrandProductId = contact.BrandProductId;
                entityOld.ProductId = contact.ProductId;
                entityOld.Income = contact.Income;
                entityOld.ModifyDate = contact.ModifyDate;
                entityOld.LoanBriefId = contact.LoanBriefId;
                entityOld.Status = contact.Status;
                //Ultility.CopyObject(contact.Mapping(), ref entityOld, true);
                _unitOfWork.ContactCustomerRepository.Update(entityOld);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "ContactCustomer/PutContact Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}