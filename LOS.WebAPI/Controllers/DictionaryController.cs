﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Request;
using LOS.DAL.Dapper;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using static LOS.DAL.Object.Tool.ToolReq;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    //[Authorize]
    public class DictionaryController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        protected IConfiguration _baseConfig;
        public DictionaryController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            _baseConfig = configuration;
        }

        [Route("provinces")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<ProvinceDetail>>> GetProvinces()
        {
            var provinces = unitOfWork.ProvinceRepository.All().Select(ProvinceDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<ProvinceDetail>>();
            def.meta = new Meta(200, "success");
            def.data = provinces;
            return Ok(def);
        }

        [Route("districts")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<DistrictDetail>>> GetDistricts([FromQuery] int province_id)
        {
            var def = new DefaultResponse<List<DistrictDetail>>();
            if (province_id > 0)
            {
                var districts = unitOfWork.DistrictRepository.Query(x => x.ProvinceId == province_id, null, false).Select(DistrictDetail.ProjectionDetail).ToList();
                def.meta = new Meta(200, "success");
                def.data = districts;
            }
            else
            {
                var districts = unitOfWork.DistrictRepository.All().Select(DistrictDetail.ProjectionDetail).ToList();
                def.meta = new Meta(200, "success");
                def.data = districts;
            }
            return Ok(def);
        }

        [Route("wards")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<WardDetail>>> GetWards([FromQuery] int district_id)
        {
            var wards = unitOfWork.WardRepository.Query(x => x.DistrictId == district_id, null, false).Select(WardDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<WardDetail>>();
            def.meta = new Meta(200, "success");
            def.data = wards;
            return Ok(def);
        }

        [Route("banks")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<BankDetail>>> GetBanks()
        {
            var banks = unitOfWork.BankRepository.All().Select(BankDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<BankDetail>>();
            def.meta = new Meta(200, "success");
            def.data = banks;
            return Ok(def);
        }

        [Route("jobs")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<JobDetail>>> GetJobs()
        {
            var jobs = unitOfWork.JobRepository.All().Select(JobDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<JobDetail>>();
            def.meta = new Meta(200, "success");
            def.data = jobs;
            return Ok(def);
        }

        [Route("loan_status")]
        public ActionResult<DefaultResponse<List<LoanStatus>>> GetLoanStatus()
        {
            var values = unitOfWork.LoanStatusRepository.All().ToList();
            var def = new DefaultResponse<List<LoanStatus>>();
            def.meta = new Meta(200, "success");
            def.data = values;
            return Ok(def);
        }

        [Route("pipeline_state")]
        public ActionResult<DefaultResponse<List<PipelineState>>> GetPipelineState()
        {
            var values = unitOfWork.PipelineStateRepository.All().ToList();
            var def = new DefaultResponse<List<PipelineState>>();
            def.meta = new Meta(200, "success");
            def.data = values;
            return Ok(def);
        }

        [Route("loan_product")]
        public ActionResult<DefaultResponse<List<LoanProduct>>> GetLoanProduct()
        {
            var values = unitOfWork.LoanProductRepository.Query(x => x.Status == 1, null, false).ToList();
            var def = new DefaultResponse<List<LoanProduct>>();
            def.meta = new Meta(200, "success");
            def.data = values;
            return Ok(def);
        }


        [Route("utm_source")]
        public ActionResult<DefaultResponse<List<UtmSource>>> GetUtmSource()
        {
            var values = unitOfWork.UtmSourceRepository.All().ToList();
            var def = new DefaultResponse<List<UtmSource>>();
            def.meta = new Meta(200, "success");
            def.data = values;
            return Ok(def);
        }
        [Route("brand_product")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<BrandProductDetail>>> GetBrandProduct()
        {
            var brandProducts = unitOfWork.BrandProductRepository.Query(x => x.IsEnable == true, null, false).Select(BrandProductDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<BrandProductDetail>>();
            def.meta = new Meta(200, "success");
            def.data = brandProducts;
            return Ok(def);
        }

        [Route("list_product")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<ProductDetail>>> GetProduct(int brandId)
        {
            var products = unitOfWork.ProductRepository.Query(x => x.BrandProductId == brandId && x.IsEnable == true, null, false).Select(ProductDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<ProductDetail>>();
            def.meta = new Meta(200, "success");
            def.data = products;
            return Ok(def);
        }

        [Route("list_platform")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<PlatformTypeDetail>>> GetPlatformType()
        {
            var platformtypes = unitOfWork.PlatformTypeRepository.All().Select(PlatformTypeDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<PlatformTypeDetail>>();
            def.meta = new Meta(200, "success");
            def.data = platformtypes;
            return Ok(def);
        }
        [Route("list_userbygroup")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<UserDetail>>> GetUserByGroupId(int groupId)
        {
            var query = unitOfWork.UserRepository.Query(x => x.GroupId == groupId && x.IsDeleted != true, null, false).Select(UserDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<UserDetail>>();
            def.meta = new Meta(200, "success");
            def.data = query;
            return Ok(def);
        }

        [Route("relative_family")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<RelativeFamilyDetail>>> GetRelativeFamily()
        {
            var relativeFamilys = unitOfWork.RelativeFamilyRepository.Query(x => x.IsEnable == true, null, false).Select(RelativeFamilyDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<RelativeFamilyDetail>>();
            def.meta = new Meta(200, "success");
            def.data = relativeFamilys;
            return Ok(def);
        }

        [Route("script")]
        [HttpGet]
        public ActionResult<DefaultResponse<LoanBriefScriptDetail>> GetScript(int ProductId)
        {
            var script = unitOfWork.LoanbriefScriptRepository.Query(x => x.ProductId == ProductId == true, null, false).Select(LoanBriefScriptDetail.ProjectionDetail).FirstOrDefault();
            var def = new DefaultResponse<LoanBriefScriptDetail>();
            def.meta = new Meta(200, "success");
            def.data = script;
            return Ok(def);
        }


        [Route("reason_cancel")]
        public ActionResult<DefaultResponse<List<ReasonCancel>>> GetReasonCancel()
        {
            var values = unitOfWork.ReasonCancelRepository.Query(x => x.IsEnable == 1, null, false).ToList();
            var def = new DefaultResponse<List<ReasonCancel>>();
            def.meta = new Meta(200, "success");
            def.data = values;
            return Ok(def);
        }

        [Route("shops")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<ShopDetail>>> GetShop()
        {
            var def = new DefaultResponse<List<ShopDetail>>();
            var shop = unitOfWork.ShopRepository.Query(x => x.Status == 1, null, false).Select(ShopDetail.ProjectionDetail).ToList();
            def.meta = new Meta(200, "success");
            def.data = shop;
            return Ok(def);
        }
        [Route("shops_cityid")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<DistrictDetail>>> GetShopByCityId([FromQuery] int cityId)
        {
            var def = new DefaultResponse<List<ShopDetail>>();
            var shop = unitOfWork.ShopRepository.Query(x => x.Status == 1 && x.CityId == cityId, null, false).Select(ShopDetail.ProjectionDetail).ToList();
            def.meta = new Meta(200, "success");
            def.data = shop;
            return Ok(def);
        }
        [Route("document")]
        [HttpGet]
        public ActionResult<DefaultResponse<DocumentTypeDetail>> GetDocument(int ProductId)
        {
            var def = new DefaultResponse<List<DocumentTypeDetail>>();
            var documents = unitOfWork.DocumentTypeRepository.Query(x => x.ProductId == ProductId && x.IsEnable == 1, null, false).Select(DocumentTypeDetail.ProjectionDetail).ToList();
            def.meta = new Meta(200, "success");
            def.data = documents;
            return Ok(def);
        }

        [Route("document_all")]
        [HttpGet]
        public ActionResult<DefaultResponse<DocumentTypeDetail>> GetDocumentAll()
        {
            var def = new DefaultResponse<List<DocumentTypeDetail>>();
            var documents = unitOfWork.DocumentTypeRepository.Query(x => x.IsEnable == 1, null, false).Select(DocumentTypeDetail.ProjectionDetail).ToList();
            def.meta = new Meta(200, "success");
            def.data = documents;
            return Ok(def);
        }

        //[Route("lender_search")]
        //[HttpGet]
        //public ActionResult<DefaultResponse<List<ShopDetail>>> LenderSearch([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
        //    [FromQuery] int take = 20, [FromQuery] int lenderId = -1, [FromQuery] string lenderCode = null,[FromQuery] int loanAmountFinal = 0)
        //{
        //    var def = new DefaultResponse<SummaryMeta, List<ShopDetail>>();
        //    List<ShopDetail> data;

        //    data = unitOfWork.ShopRepository.Query(x => (x.Status == 1)
        //                                          && (x.ShopId == lenderId || lenderId == -1)
        //                                          && (x.Name.StartsWith(lenderCode) || lenderCode == null)
        //                                          && (x.SelfEmployed == 4 || x.SelfEmployed == 5)
        //                                          && (x.TotalMoney >= loanAmountFinal), null, false).Select(ShopDetail.ProjectionDetail).ToList();
        //    var totalRecords = data.Count();
        //    if (skip == -1)
        //        data = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();
        //    else
        //    {
        //        data = data.Skip(skip).Take(take).ToList();
        //        pageSize = take;
        //    }
        //    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
        //    def.data = data;
        //    return Ok(def);
        //}

        [HttpPost]
        [Route("add_log_api")]
        public ActionResult<DefaultResponse<object>> AddLogApi([FromBody] LogCallApi entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LogCallApiRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogCallApi/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_log_api")]
        public ActionResult<DefaultResponse<object>> UpdateLogApi([FromBody] LogCallApi entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var obj = unitOfWork.LogCallApiRepository.GetById(entity.Id);
                if (obj != null && obj.Id > 0)
                {
                    obj.Status = entity.Status;
                    obj.Output = entity.Output;
                    obj.ModifyAt = DateTime.Now;
                    unitOfWork.Save();
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogCallApi/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("hubtypes")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<HubType>>> GetHubType()
        {
            var hubTypes = new List<HubType>();
            hubTypes.Add(new HubType()
            {
                HubTypeId = 1,
                Name = "Hub dịch vụ"
            });
            hubTypes.Add(new HubType()
            {
                HubTypeId = 2,
                Name = "Hub vật lý"
            });
            var def = new DefaultResponse<List<HubType>>();
            def.meta = new Meta(200, "success");
            def.data = hubTypes;
            return Ok(def);
        }

        [Route("testing")]
        [HttpGet]
        public IActionResult Testing()
        {
            LoanBrief loanBrief = unitOfWork.LoanBriefRepository.GetById(10316);
            DatabaseHelper<LoanBrief> databaseHelper = new DatabaseHelper<LoanBrief>();
            var result = databaseHelper.CheckFieldOperator(loanBrief, "IsReborrow", "EQUAL", "1", "Boolean");
            return Ok();
        }

        [HttpPost]
        [Route("add_log_click_to_call")]
        public ActionResult<DefaultResponse<object>> AddLogClickToCal([FromBody] LogClickToCall entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LogClickToCallRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogClickToCall/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //Hình thức sở hữu nhà
        [Route("type_ownership")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<TypeOwnerShip>>> GetTypeOwnerShip()
        {
            var ownership = unitOfWork.TypeOwnerShipRepository.Query(x => x.IsEnable == true, null, false).ToList();
            var def = new DefaultResponse<List<TypeOwnerShip>>();
            def.meta = new Meta(200, "success");
            def.data = ownership;
            return Ok(def);
        }

        [HttpPost]
        [Route("add_log_ai")]
        public ActionResult<DefaultResponse<object>> AddLogAi([FromBody] LogLoanInfoAi entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LogLoanInfoAiRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogCallAi/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpPost]
        [Route("update_log_ai")]
        public ActionResult<DefaultResponse<object>> UpdateLogAi([FromBody] LogLoanInfoAi entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var obj = unitOfWork.LogLoanInfoAiRepository.GetById(entity.Id);
                if (obj != null && obj.Id > 0)
                {
                    if (entity.IsExcuted.HasValue)
                        obj.IsExcuted = entity.IsExcuted;
                    obj.Status = entity.Status;
                    obj.ResponseRequest = entity.ResponseRequest;
                    unitOfWork.LogLoanInfoAiRepository.Update(obj);
                    unitOfWork.Save();
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogCallApi/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("check_logai/{id}")]
        public ActionResult<DefaultResponse<Meta, LogLoanInfoAi>> CheckLogAI(int id)
        {
            var def = new DefaultResponse<Meta, LogLoanInfoAi>();
            try
            {
                var data = unitOfWork.LogLoanInfoAiRepository.Query(x => x.ServiceType == 6 && x.LoanbriefId == id, null, false).OrderByDescending(s => s.Id).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("event_config")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<EventConfig>>> GetEventConfig()
        {
            var eventConfigs = unitOfWork.EventConfigRepository.Query(x => x.IsEnable == true, null, false).ToList();
            var def = new DefaultResponse<List<EventConfig>>();
            def.meta = new Meta(200, "success");
            def.data = eventConfigs;
            return Ok(def);
        }

        [HttpGet]
        [Route("loanbrief_lender/{id}")]
        public ActionResult<DefaultResponse<Meta, LoanbriefLender>> LoanBriefLender(int id)
        {
            var def = new DefaultResponse<Meta, LoanbriefLender>();
            try
            {
                var data = unitOfWork.LoanBriefLenderRepository.Query(x => x.LoanbriefId == id, null, false).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_district_by_id/{id}")]
        public ActionResult<DefaultResponse<Meta, District>> GetDistrictById(int id)
        {
            var def = new DefaultResponse<Meta, District>();
            try
            {
                var data = unitOfWork.DistrictRepository.Query(x => x.DistrictId == id, null, false).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetDistrictById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("check_log_send_sms")]
        public ActionResult<DefaultResponse<Meta, LogSendSms>> CheckLogSendSms(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, LogSendSms>();
            try
            {
                var data = unitOfWork.LogSendSmsRepository.Query(x => x.LoanbriefId == loanbriefId, null, false).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/CheckLogSendSms Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_log_send_sms")]
        public ActionResult<DefaultResponse<object>> AddLogSendSms([FromBody] LogSendSms entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LogSendSmsRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "AddLogSendSms/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_log_send_sms")]
        public ActionResult<DefaultResponse<object>> UpdateLogSendSms([FromBody] LogSendSms entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var data = unitOfWork.LogSendSmsRepository.Query(x => x.LoanbriefId == entity.LoanbriefId, null, false).FirstOrDefault();
                if (data != null && data.Id > 0)
                {
                    data.UpdatedAt = entity.UpdatedAt;
                    data.Response = entity.Response;
                    unitOfWork.LogSendSmsRepository.Update(data);
                    unitOfWork.Save();
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogSendSms/Update Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("type_remarketing")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<TypeRemarketing>>> GetTypeRemarketing()
        {
            var types = unitOfWork.TypeRemarketingRepository.Query(x => x.IsEnable == 1, null, false).ToList();
            var def = new DefaultResponse<List<TypeRemarketing>>();
            def.meta = new Meta(200, "success");
            def.data = types;
            return Ok(def);
        }

        [HttpGet]
        [Route("get_provinceid_by_name")]
        public ActionResult<DefaultResponse<Meta, ProvinceDetail>> GetProvinceIdByName(string name)
        {
            var def = new DefaultResponse<Meta, ProvinceDetail>();
            try
            {
                var data = unitOfWork.ProvinceRepository.Query(x => x.Name.Contains(name), null, false).Select(ProvinceDetail.ProjectionDetail).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetProvinceIdByName Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_districtid_by_name")]
        public ActionResult<DefaultResponse<Meta, DistrictDetail>> GetDistrictIdByName(string name)
        {
            var def = new DefaultResponse<Meta, DistrictDetail>();
            try
            {
                var data = unitOfWork.DistrictRepository.Query(x => x.Name.Contains(name), null, false).Select(DistrictDetail.ProjectionDetail).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetDistrictIdByName Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_productid_by_name")]
        public ActionResult<DefaultResponse<Meta, LoanProduct>> GetProductIdByName(string name)
        {
            var def = new DefaultResponse<Meta, LoanProduct>();
            try
            {
                var data = unitOfWork.LoanProductRepository.Query(x => x.Name.Contains(name), null, false).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetProductIdByName Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("get_log_result_auto_call/{id}")]
        public ActionResult<DefaultResponse<Meta, LogResultAutoCall>> GetLogResultAutoCall(int id)
        {
            var def = new DefaultResponse<Meta, LogResultAutoCall>();
            try
            {
                var data = unitOfWork.LogResultAutoCallRepository.Query(x => x.LoanbriefId == id, null, false).OrderByDescending(s => s.Id).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionary/GetLogResultAutoCall Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_checkloan_information")]
        public ActionResult<DefaultResponse<object>> AddCheckImage([FromBody] CheckImageReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                foreach (var item in entity.LstCheckImage)
                {
                    var infocheck = unitOfWork.CheckImageRepository.Query(x => x.LoanBriefId == entity.LoanBriefId && x.TypeCheck == entity.TypeCheck && x.DocumentTypeId == item.DocumentType, null, false).FirstOrDefault();
                    if (infocheck != null && infocheck.ResultCheck != item.ResultCheck)
                    {
                        //Update data
                        unitOfWork.CheckImageRepository.Update(x => x.LoanBriefId == entity.LoanBriefId && x.DocumentTypeId == item.DocumentType, x => new CheckLoanInformation()
                        {
                            ResultCheck = item.ResultCheck,
                            UpdateAt = DateTime.Now
                        });
                    }
                    else if (infocheck == null)
                    {
                        //Insert bản ghi mới
                        unitOfWork.CheckImageRepository.Insert(new CheckLoanInformation()
                        {
                            LoanBriefId = entity.LoanBriefId,
                            DocumentTypeId = item.DocumentType,
                            ResultCheck = item.ResultCheck,
                            CreatedTime = DateTime.Now,
                            UserId = entity.UserId,
                            TypeCheck = entity.TypeCheck,
                            Status = 1
                        });
                    }
                }

                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CheckImage/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("get_checkimage_by_loanBriefId/{id}")]
        public ActionResult<DefaultResponse<Meta, List<CheckLoanInformation>>> GetCheckImageByLoanBriefId(int id)
        {
            var def = new DefaultResponse<Meta, List<CheckLoanInformation>>();
            try
            {
                var data = unitOfWork.CheckImageRepository.Query(x => x.LoanBriefId == id, null, false).OrderBy(s => s.DocumentTypeId).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionary/GetCheckImageByLoanBriefId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_documenttype_image")]
        public ActionResult<DefaultResponse<object>> AddDocumentTypeImage([FromBody] DocumentTypeReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                foreach (var item in entity.LstFileId)
                {
                    if (unitOfWork.LoanBriefFileRepository.Any(x => x.Id == item.FileId))
                    {
                        //Update data
                        unitOfWork.LoanBriefFileRepository.Update(x => x.Id == item.FileId, x => new LoanBriefFiles()
                        {
                            TypeId = entity.DocumentId,
                            ModifyAt = DateTime.Now
                        });
                    }
                }

                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CheckImage/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_reason_coordinator")]
        public ActionResult<DefaultResponse<object>> AddReasonCoordinator([FromBody] ReturnLoanReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                foreach (var item in entity.LstReason)
                {

                    //Insert
                    unitOfWork.ReasonCoordinatorRepository.Insert(new ReasonCoordinator()
                    {
                        LoanBriefId = entity.LoanBriefId,
                        UserId = entity.UserId,
                        ReasonId = item.ReasonId,
                        Reason = item.ReasonName,
                        CreateAt = DateTime.Now,
                        GroupId = entity.GroupId
                    });
                }

                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ReasonCoordinator/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("get_job_parentId")]
        public ActionResult<DefaultResponse<Meta, List<JobDetail>>> GetJobByParentId([FromQuery] int parentId)
        {
            var def = new DefaultResponse<Meta, List<JobDetail>>();
            try
            {
                var lstData = unitOfWork.JobRepository.Query(x => x.ParentId == parentId, null, false).Select(JobDetail.ProjectionDetail).OrderBy(x => x.Name).ToList();
                def.meta = new Meta(200, "success");
                def.data = lstData;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "GetJobByParentId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_job_by_id")]
        public ActionResult<DefaultResponse<Meta, JobDetail>> GetJobById([FromQuery] int id)
        {
            var def = new DefaultResponse<Meta, JobDetail>();
            try
            {
                var data = unitOfWork.JobRepository.Query(x => x.JobId == id, null, false).Select(JobDetail.ProjectionDetail).OrderBy(x => x.Name).FirstOrDefault();
                def.meta = new Meta(200, "success");
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "GetJobById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_product_reviewresult")]
        public ActionResult<DefaultResponse<Meta, List<ProductReviewResultDetail>>> GetProductReviewDetail([FromQuery] int id)
        {
            var def = new DefaultResponse<Meta, List<ProductReviewResultDetail>>();
            try
            {
                var data = unitOfWork.ProductReviewResultDetailRepository.Query(x => x.LoanBriefId == id, null, false).OrderByDescending(x => x.Id).ToList();
                def.meta = new Meta(200, "success");
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "GetProductReviewDetail Exception");
            }
            return Ok(def);
        }

        [Route("get_province_by_id/{id}")]
        public ActionResult<DefaultResponse<Meta, Province>> GetProvinceById(int id)
        {
            var def = new DefaultResponse<Meta, Province>();
            try
            {
                var data = unitOfWork.ProvinceRepository.Query(x => x.ProvinceId == id, null, false).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetProvinceById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_config_document")]
        public ActionResult<DefaultResponse<Meta, List<ConfigDocument>>> GetConfigDocument([FromQuery] int productid, [FromQuery] int typeownership)
        {
            var def = new DefaultResponse<Meta, List<ConfigDocument>>();
            try
            {
                var data = unitOfWork.ConfigDocumentRepository.Query(x => x.ProductId == productid && x.TypeOwnerShip == typeownership && x.IsEnable == true, null, false).ToList();
                def.meta = new Meta(200, "success");
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "GetConfigDocument Exception");
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_config_document")]
        public ActionResult<DefaultResponse<object>> AddConfigDocument([FromBody] ConfigDocument entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //Đã tồn tại
                if (entity.Id > 0)
                {
                    var infoConfig = unitOfWork.ConfigDocumentRepository.Query(x => x.Id == entity.Id, null, false).FirstOrDefault();
                    if (infoConfig != null)
                    {
                        //if (infoConfig.AllowFromUpload == null) infoConfig.AllowFromUpload = "";
                        //if (infoConfig.GroupJobId == null) infoConfig.GroupJobId = "";
                        var checkChange = infoConfig.MinImage.GetValueOrDefault(0).Equals(entity.MinImage.GetValueOrDefault(0)) &&
                                          infoConfig.Required.GetValueOrDefault(false).Equals(entity.Required.GetValueOrDefault(false)) &&
                                          infoConfig.TypeRequired.GetValueOrDefault(0).Equals(entity.TypeRequired.GetValueOrDefault(0)) &&
                                          infoConfig.IsEnable.GetValueOrDefault(false).Equals(entity.IsEnable.GetValueOrDefault(false)) &&
                                          infoConfig.Sort.GetValueOrDefault(0).Equals(entity.Sort.GetValueOrDefault(0));
                        if (checkChange == true)
                        {
                            if (infoConfig.AllowFromUpload != entity.AllowFromUpload)
                                checkChange = false;
                            if (infoConfig.GroupJobId != entity.GroupJobId)
                                checkChange = false;
                        }
                        //Nếu kiểm tra có thay đổi thì update
                        if (checkChange == false)
                        {
                            unitOfWork.ConfigDocumentRepository.Update(x => x.Id == infoConfig.Id, x => new ConfigDocument()
                            {
                                MinImage = entity.MinImage,
                                Required = entity.Required,
                                TypeRequired = entity.TypeRequired,
                                AllowFromUpload = entity.AllowFromUpload,
                                GroupJobId = entity.GroupJobId,
                                IsEnable = entity.IsEnable,
                                Sort = entity.Sort
                            });
                        }
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = infoConfig.Id;
                    }
                }
                else
                {
                    //Insert
                    unitOfWork.ConfigDocumentRepository.Insert(entity);
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = entity.Id;
                }


                unitOfWork.Save();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ReasonCoordinator/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpGet]
        [Route("get_docment_type_v2")]
        public ActionResult<DefaultResponse<Meta, List<DocumentMapInfo>>> GetDocumentTypeV2([FromQuery] int productid, [FromQuery] int typeownership)
        {
            var def = new DefaultResponse<Meta, List<DocumentMapInfo>>();
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select dt.Id as 'Id', dt.Name as 'Name', dt.Guide, dt.ParentId, cd.ProductId, cd.MinImage as 'NumberImage', cd.Required, cd.TypeRequired , cd.AllowFromUpload, cd. GroupJobId ");
                sql.AppendLine("from DocumentType(nolock) dt ");
                sql.AppendLine("inner join ConfigDocument(nolock) cd on dt.Id = cd.DocumentTypeId ");
                sql.AppendLine("where dt.IsEnable = 1 ");
                sql.AppendLine($"and cd.IsEnable = 1  and cd.ProductId = {productid} and cd.TypeOwnerShip = {typeownership} ");
                sql.AppendLine("order by dt.Id ");
                var result = db.Query<DocumentMapInfo>(sql.ToString());
                def.data = result;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "GetDocumentTypeV2 Exception");
            }
            return Ok(def);
        }

        [Route("jobs_v2")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<JobDetail>>> GetJobsV2()
        {
            var jobs = unitOfWork.JobRepository.Query(x => x.ParentId == (int)Common.Extensions.EnumJobTitle.LamHuongLuong
            || x.ParentId == (int)Common.Extensions.EnumJobTitle.LamTuDo
            || x.ParentId == (int)Common.Extensions.EnumJobTitle.TuDoanh, null, false).Select(JobDetail.ProjectionDetail).ToList();
            var def = new DefaultResponse<List<JobDetail>>();
            def.meta = new Meta(200, "success");
            def.data = jobs;
            return Ok(def);
        }

        [Route("get_loan_config_step")]
        [HttpGet]
        public ActionResult<DefaultResponse<LoanConfigStep>> GetLoanConfigStep([FromQuery] int id)
        {
            var data = unitOfWork.LoanConfigStepRepository.Query(x => x.LoanStatusDetailId == id && x.IsEnable == true, null, false).FirstOrDefault();
            var def = new DefaultResponse<LoanConfigStep>();
            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }

        [Route("get_all_loan_config_step")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<LoanConfigStep>>> GetAllLoanConfigStep()
        {
            var data = unitOfWork.LoanConfigStepRepository.Query(x => x.IsEnable == true, null, false).ToList();
            var def = new DefaultResponse<List<LoanConfigStep>>();
            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }

        [Route("get_all_infomation_product_detail")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<InfomationProductDetailDTO>>> GetAllInfomationProductDetail()
        {
            var data = unitOfWork.InfomationProductDetailRepository.Query(x => x.IsEnable == true, null, false).Select(InfomationProductDetailDTO.ProjectionViewDetail).ToList();
            var def = new DefaultResponse<List<InfomationProductDetailDTO>>();
            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }

        [Route("get_infomation_product_detail_by_id")]
        [HttpGet]
        public ActionResult<DefaultResponse<InfomationProductDetail>> GetInfomationProductDetailById([FromQuery] int id)
        {
            var data = unitOfWork.InfomationProductDetailRepository.Query(x => x.IsEnable == true && x.Id == id, null, false).FirstOrDefault();
            var def = new DefaultResponse<InfomationProductDetail>();
            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }

        [Route("get_infomation_product_detail_by_type_ownership")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<InfomationProductDetail>>> GetInfomationProductDetailByTypeOwnership([FromQuery] int typeOwnership)
        {
            var data = unitOfWork.InfomationProductDetailRepository.Query(x => x.IsEnable == true
            && (x.TypeOwnership == typeOwnership || x.TypeOwnership == 0), null, false).OrderBy(x => x.Priority).ToList();
            var def = new DefaultResponse<List<InfomationProductDetail>>();
            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }

        [Route("get_infomation_product_detail_by_type_ownership_and_product")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<ProductDetailModel>>> GetInfomationProductDetailByTypeOwnershipAndProduct([FromQuery] int typeOwnership, [FromQuery] int productId, [FromQuery] int typeLoanBrief, [FromQuery] int groupId)
        {
            var data = new List<ProductDetailModel>();

            if (groupId == (int)Common.Extensions.EnumGroupUser.ApproveEmp || groupId == (int)Common.Extensions.EnumGroupUser.ApproveLeader)
            {
                if (typeLoanBrief == (int)Common.Extensions.TypeLoanBrief.DOANHNGHIEP)
                    data = unitOfWork.ConfigProductDetailRepository.Query(x => x.ProductId == productId && x.TypeLoanBrief == typeLoanBrief, null, false).Select(ProductDetailModel.ProjectionViewDetail).ToList();
                else
                    data = unitOfWork.ConfigProductDetailRepository.Query(x => x.TypeOwnership == typeOwnership && x.ProductId == productId, null, false).Select(ProductDetailModel.ProjectionViewDetail).ToList();
            }
            else
            {
                if (typeLoanBrief == (int)Common.Extensions.TypeLoanBrief.DOANHNGHIEP)
                    data = unitOfWork.ConfigProductDetailRepository.Query(x => x.ProductId == productId && x.TypeLoanBrief == typeLoanBrief && x.IsEnable == true, null, false).Select(ProductDetailModel.ProjectionViewDetail).ToList();
                else
                    data = unitOfWork.ConfigProductDetailRepository.Query(x => x.TypeOwnership == typeOwnership && x.ProductId == productId && x.IsEnable == true, null, false).Select(ProductDetailModel.ProjectionViewDetail).ToList();
            }


            var def = new DefaultResponse<List<ProductDetailModel>>();
            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }

        [Route("get_infomation_product_detail_by_product_detail_and_product")]
        [HttpGet]
        public ActionResult<DefaultResponse<ProductDetailModel>> GetInfomationProductDetailByProductDetailAndProduct([FromQuery] int productDetailId, [FromQuery] int productId)
        {
            var data = unitOfWork.ConfigProductDetailRepository.Query(x => x.InfomationProductDetailId == productDetailId && x.ProductId == productId && x.IsEnable == true, null, false).Select(ProductDetailModel.ProjectionViewDetail).FirstOrDefault();
            var def = new DefaultResponse<ProductDetailModel>();
            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }

        [Route("get_config_department_status")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<ConfigDepartmentStatus>>> GetDepartmentStatus([FromQuery] int departmentId)
        {
            var data = unitOfWork.ConfigDepartmentStatusRepository.Query(x => x.IsEnable == true && x.DepartmentId == departmentId, null, false).ToList();
            var def = new DefaultResponse<List<ConfigDepartmentStatus>>();
            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }

        [HttpPost]
        [Route("add_district")]
        public ActionResult<DefaultResponse<object>> AddDistrict([FromBody] District entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //kiểm tra xem đã tồn tại quận/huyện chưa
                if (unitOfWork.DistrictRepository.Any(x => x.Name.ToLower() == entity.Name.ToLower() && x.ProvinceId == entity.ProvinceId))
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = -1;
                }
                else
                {
                    unitOfWork.DistrictRepository.Insert(entity);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = entity.DistrictId;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "District/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_ward")]
        public ActionResult<DefaultResponse<object>> AddWard([FromBody] Ward entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //kiểm tra xem Phường/Xã đã tồn tại chưa
                if (unitOfWork.WardRepository.Any(x => x.Name.ToLower() == entity.Name.ToLower() && x.DistrictId == entity.DistrictId))
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = -1;
                }
                else
                {
                    unitOfWork.WardRepository.Insert(entity);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = entity.WardId;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Ward/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }       

        [HttpPost]
        [Route("change_phone")]
        public ActionResult<DefaultResponse<object>> ChangePhone([FromBody] ChangePhoneReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.LoanBriefId <= 0 || string.IsNullOrEmpty(entity.Phone))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (loanbrief != null)
                {
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                    {
                        PhoneOther = loanbrief.Phone,
                        Phone = entity.Phone

                    });
                    unitOfWork.CustomerRepository.Update(x => x.CustomerId == loanbrief.CustomerId, x => new Customer()
                    {
                        Phone = entity.Phone
                    });
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Tool/ChangePhone Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("get_relationship_info")]
        [HttpGet]
        public ActionResult<DefaultResponse<LoanRelationshipDTO>> GetRelationshipInfo([FromQuery] int loanBriefId, [FromQuery] int relationshipId)
        {
            var def = new DefaultResponse<Meta, LoanRelationshipDTO>();
            try
            {
                var data = unitOfWork.LoanBriefRelationshipRepository.Query(x => x.LoanBriefId == loanBriefId && x.Id == relationshipId, null, false).Select(LoanRelationshipDetail.ProjectionDetail).FirstOrDefault();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanRelationship/Detail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }

            return Ok(def);
        }

        [HttpPost]
        [Route("save_compare_document")]
        public ActionResult<DefaultResponse<object>> SaveCompareDocument([FromBody] CompareDocument entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.CompareDocumentRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CompareDocument/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("get_last_compare")]
        [HttpGet]
        public ActionResult<DefaultResponse<CompareDocument>> GetLastCompare([FromQuery] int loanBriefId)
        {
            var def = new DefaultResponse<CompareDocument>();
            var data = unitOfWork.CompareDocumentRepository.Query(x => x.LoanBriefId == loanBriefId && x.Status == 1, null, false).OrderByDescending(x => x.Tab).FirstOrDefault();

            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }

        [Route("check_compare_finish")]
        [HttpGet]
        public ActionResult<DefaultResponse<object>> CheckCompareFinish(int loanBriefId, int groupId)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (loanBriefId <= 0 || groupId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var data = unitOfWork.CompareDocumentRepository.Any(x => x.LoanBriefId == loanBriefId && x.GroupId == groupId && x.IsFinish == true);
                if (data)
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = true;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                    def.data = false;
                }

            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("get_log_send_otp")]
        public async Task<ActionResult<DefaultResponse<object>>> GetLogSendOTP([FromQuery] string search, int type)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(search) || type <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }

                var data = await unitOfWork.LogSendOtpRepository.Query(x => x.Phone == search && x.TypeOtp == type, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();
                if (data != null)
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = $"{data.Otp}-{ data.ExpiredVerifyOtp.Value.ToString("dd/MM/yyyy HH:mm:ss")}-{data.Phone}";
                }
                else
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Không tìm thấy OTP hợp lệ");

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Tool/GetOTP Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("get_document_exception")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<DocumentException>>> GetDocumentException()
        {
            var data = unitOfWork.DocumentExceptionRepository.Query(x => x.IsEnable == true, null, false).ToList();
            var def = new DefaultResponse<List<DocumentException>>();
            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }

        [HttpPost]
        [Route("add_log_momo_info")]
        public ActionResult<DefaultResponse<object>> AddLogMomoInfo([FromBody] LogLoanInfoPartner entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LogLoanInfoPartnerRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
 [HttpPost]
        [Route("save_log_change_phone")]
        public ActionResult<DefaultResponse<object>> SaveLogChangePhone([FromBody] LogChangePhone entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LogChangePhoneRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogChangePhone/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_log_transaction_secured")]
        public ActionResult<DefaultResponse<object>> AddLogTransactionSecured([FromBody] LogSecuredTransaction entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //Insert Log
                unitOfWork.LogSecuredTransactionRepository.Insert(entity);
                //Update Lại TransactionState trong LoanBrief
                unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId,x => new LoanBrief()
                {
                    TransactionState = (int)Common.Extensions.EnumTransactionState.DangChoDuyet,
                    IsTransactionsSecured = true
                });
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("get_last_log_secured_transaction")]
        [HttpGet]
        public ActionResult<DefaultResponse<LogSecuredTransaction>> GetLastLogSecuredTransaction([FromQuery] int loanBriefId)
        {
            var def = new DefaultResponse<LogSecuredTransaction>();
            var data = unitOfWork.LogSecuredTransactionRepository.Query(x => x.LoanBriefId == loanBriefId, null, false).OrderByDescending(x => x.CreatedAt).FirstOrDefault();

            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }
    }
}