﻿using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.Dapper;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class DocumentTypeController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;

        public DocumentTypeController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
        }

        [HttpGet]
        [Route("get_all")]
        public ActionResult<DefaultResponse<SummaryMeta, List<DocumentTypeDetail>>> GetAll([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] string name = null,
            [FromQuery] int? productId = -1, [FromQuery] byte? isEnable = null)
        {
            var def = new DefaultResponse<SummaryMeta, List<DocumentTypeDetail>>();
            try
            {
                var query = _unitOfWork.DocumentTypeRepository.Query(x => (x.ProductId == productId || productId == -1) && (x.Name.Contains(name) || name == null)
                && (x.IsEnable == isEnable || isEnable == null), null, false)
                    .Select(DocumentTypeDetail.ProjectionDetail);
                var totalRecords = query.Count();
                var skip = (page - 1) * pageSize;
                var data = query.OrderByDescending(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DocumentType/GetAll Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }


        [HttpGet]
        [Route("get_parent")]
        public ActionResult<DefaultResponse<Meta, List<DocumentTypeDetail>>> GetDocumentTypeParent(int productId)
        {
            var def = new DefaultResponse<Meta, List<DocumentTypeDetail>>();
            try
            {
                var query = _unitOfWork.DocumentTypeRepository.Query(x => x.ParentId.GetValueOrDefault(0) == 0 && x.ProductId == productId && x.IsEnable == 1, null, false).Select(DocumentTypeDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DocumentType/GetDocumentTypeParent Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<DefaultResponse<Meta, List<DocumentTypeDetail>>> GetById(int id)
        {
            var def = new DefaultResponse<Meta, DocumentTypeDetail>();
            try
            {
                var query = _unitOfWork.DocumentTypeRepository.Query(x => x.Id == id, null, false).Select(DocumentTypeDetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DocumentType/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        public ActionResult<DefaultResponse<object>> PostDocumentType([FromBody] DocumentType entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                entity.CreatedTime = DateTime.Now;
                entity.Version = 2;
                var result = _unitOfWork.DocumentTypeRepository.Insert(entity);
                _unitOfWork.Save();
                if (result.Id > 0)
                {
                    def.data = result.Id;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DocumentType/CreateDocumentType Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut]
        [Route("{id}")]
        public ActionResult<DefaultResponse<object>> PutDocumentType(int id, [FromBody] DocumentType entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != entity.Id)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //cập nhật User
                _unitOfWork.DocumentTypeRepository.Update(entity);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DocumentType/UpdateDocumentType Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_all_enable")]
        public ActionResult<DefaultResponse<Meta, List<DocumentTypeDetail>>> GetDocumentTypeEnable()
        {
            var def = new DefaultResponse<Meta, List<DocumentTypeDetail>>();
            try
            {
                var query = _unitOfWork.DocumentTypeRepository.Query(x => x.IsEnable == 1, null, false).Select(DocumentTypeDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DocumentType/GetDocumentTypeEnable Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get")]
        public ActionResult<DefaultResponse<Meta, List<DocumentMapInfo>>> GetDocumentType(int productId, int typeOwnerShip)
        {
            var def = new DefaultResponse<Meta, List<DocumentMapInfo>>();
            try
            {
                var db = new DapperHelper(_configuration.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select dt.Id as 'Id', dt.Name as 'Name', dt.Guide, dt.ParentId, cd.ProductId, cd.MinImage as 'NumberImage', cd.Required, cd.TypeRequired , cd.AllowFromUpload, cd. GroupJobId, cd.Sort ");
                sql.AppendLine("from DocumentType(nolock) dt ");
                sql.AppendLine("inner join ConfigDocument(nolock) cd on dt.Id = cd.DocumentTypeId ");              
                sql.AppendLine("where dt.IsEnable = 1 ");
                sql.AppendLine($"and cd.IsEnable = 1  and cd.ProductId = {productId} and cd.TypeOwnerShip = {typeOwnerShip} ");
                sql.AppendLine(" order by dt.Id ");
                var data = db.Query<DocumentMapInfo>(sql.ToString());
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "DocumentType/GetDocumentType Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
