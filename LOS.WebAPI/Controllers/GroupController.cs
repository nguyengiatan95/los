﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq.Dynamic.Core;
using Serilog;
using LOS.DAL.EntityFramework;
using LOS.Common.Utils;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class GroupController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;

        public GroupController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
        }

        [HttpGet]
        public ActionResult<DefaultResponse<SummaryMeta, List<GroupDetail>>> GetAllGroup([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] string name = null, [FromQuery] string sortBy = "GroupId", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<GroupDetail>>();
            try
            {
                var query = _unitOfWork.GroupRepository.Query(x => x.IsDeleted != true && (x.GroupName.Contains(name) || name == null), null, false)
                    .Include(x => x.GroupModule)
                    .Select(GroupDetail.ProjectionDetail);
                var totalRecords = query.Count();
                var skip = (page - 1) * pageSize;
                var data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Group/GetAllGroup Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("getall")]
        public ActionResult<DefaultResponse<Meta, List<Group>>> GetAll()
        {
            var def = new DefaultResponse<Meta, List<Group>>();
            try
            {
                var query = _unitOfWork.GroupRepository.Query(x => x.IsDeleted != true, null, false).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Group/GetAll Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<DefaultResponse<Meta, GroupDetail>> GetById(int id)
        {
            var def = new DefaultResponse<Meta, GroupDetail>();
            try
            {
                var query = _unitOfWork.GroupRepository.Query(x => x.GroupId == id, null,
                    false).Include(x => x.GroupModule).Select(GroupDetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Group/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("count_user")]
        public ActionResult<DefaultResponse<object>> NumberUserInGroup(int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var query = _unitOfWork.UserRepository.Count(x => x.GroupId == id);
                def.data = query;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "NumberUserInGroup/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpGet]
        [Route("check_delete")]
        public ActionResult<DefaultResponse<object>> CheckDelete(int id)
        {
            var def = new DefaultResponse<object>();
            def.data = 0;
            try
            {
                var query = _unitOfWork.UserRepository.Any(x => x.GroupId == id && x.IsDeleted != true);
                if (query)
                    def.data = 1;
                else
                {
                    var anyGroup = _unitOfWork.GroupModuleRepository.Any(x => x.GroupId == id);
                    if (anyGroup)
                        def.data = 2;
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CheckDelete/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        public ActionResult<DefaultResponse<object>> Post([FromBody]GroupDTO entity)
        {
            var def = new DefaultResponse<object>();
            try

            {
                _unitOfWork.BeginTransaction();
                var result = _unitOfWork.GroupRepository.Insert(entity.Mapping());
                _unitOfWork.Save();
                //insert Group Module
                if (entity.ListGroupModule != null && entity.ListGroupModule.Count > 0)
                {
                    var lstGropuModule = entity.ListGroupModule.Where(x => x.ModuleId > 0).ToList();
                    lstGropuModule.ForEach(x => x.GroupId = result.GroupId);
                    _unitOfWork.GroupModuleRepository.Insert(lstGropuModule);
                }
                _unitOfWork.Save();

                if (result.GroupId > 0)
                {
                    _unitOfWork.CommitTransaction();
                    def.data = result.GroupId;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    _unitOfWork.RollbackTransaction();
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "Group/Post Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut]
        [Route("{id}")]
        public ActionResult<DefaultResponse<object>> Put(int id, [FromBody]GroupDTO entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != entity.GroupId)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                _unitOfWork.BeginTransaction();
                var lstGropuModule = entity.ListGroupModule;
                var entityOld = _unitOfWork.GroupRepository.GetById(entity.GroupId);
                Ultility.CopyObject(entity.Mapping(), ref entityOld, true);
                entityOld.UpdatedAt = DateTime.Now;
                //cập nhật Group
                _unitOfWork.GroupRepository.Update(entityOld);
                //xóa toàn bộ dữ liệu cũ Group-Module
                _unitOfWork.GroupModuleRepository.Delete(x => x.GroupId == entity.GroupId && x.ApplicationId.GetValueOrDefault((int)Common.Extensions.EnumApplication.WebLOS) == (int)Common.Extensions.EnumApplication.WebLOS);
                //Thêm dữ liệu mới Group-Module
                _unitOfWork.GroupModuleRepository.Insert(lstGropuModule);

                _unitOfWork.Save();
                _unitOfWork.CommitTransaction();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "Group/Put Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpDelete]
        [Route("{id}")]
        public ActionResult<DefaultResponse<object>> Delete(int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.GroupRepository.SoftDelete(id);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Group/Delete Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_group_permission")]
        public ActionResult<DefaultResponse<SummaryMeta, GroupDetail>> GetPermissionGroup(int groupId)
        {
            var def = new DefaultResponse<Meta, GroupDetail>();
            try
            {
                var query = _unitOfWork.GroupRepository.Query(x => x.GroupId == groupId, null, false)
                    .Select(GroupDetail.ProjectionPermissionDetail);
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query.FirstOrDefault();
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Group/GetPermissionGroup Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_permission")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> UpdatePermissionGroup(PermissionGroupDTO entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                //xóa dữ liệu cũ
                if (_unitOfWork.GroupConditionRepository.Any(x => x.GroupId == entity.GroupId))
                    _unitOfWork.GroupConditionRepository.Delete(x => x.GroupId == entity.GroupId);
                if (_unitOfWork.GroupActionPermissionRepository.Any(x => x.GroupId == entity.GroupId))
                    _unitOfWork.GroupActionPermissionRepository.Delete(x => x.GroupId == entity.GroupId);
                //insert dữ liệu mới
                if (entity.GroupConditions != null && entity.GroupConditions.Count > 0)
                {
                    foreach (var item in entity.GroupConditions)
                        _unitOfWork.GroupConditionRepository.Insert(new LOS.DAL.EntityFramework.GroupCondition()
                        {
                            GroupId = entity.GroupId,
                            PropertyId = item.PropertyId,
                            Operator = item.Operator,
                            CreatetAt = DateTime.Now,
                            CreatedBy = item.CreatedBy,
                            Value = item.Value
                        });
                }

                if (entity.GroupActionPermissions != null && entity.GroupActionPermissions.Count > 0)
                {
                    foreach (var item in entity.GroupActionPermissions)
                        _unitOfWork.GroupActionPermissionRepository.Insert(new LOS.DAL.EntityFramework.GroupActionPermission()
                        {
                            GroupId = entity.GroupId,
                            CreatedBy = item.CreatedBy,
                            CreatedAt = DateTime.Now,
                            LoanStatus = item.LoanStatus,
                            Value = item.Value
                        });
                }

                _unitOfWork.Save();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.GroupId;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Group/UpdatePermissionGroup Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_all_not_administrator")]
        public ActionResult<DefaultResponse<Meta, List<Group>>> GetAllNotAdministrator()
        {
            var def = new DefaultResponse<Meta, List<Group>>();
            try
            {
                //lấy ra danh sách group trừ Administrator
                var query = _unitOfWork.GroupRepository.Query(x => x.IsDeleted != true && x.GroupId != 1, null, false).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Group/GetAll Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
