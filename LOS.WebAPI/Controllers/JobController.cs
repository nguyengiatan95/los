﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.WebAPI.Controllers
{

    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class JobController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        private IConfiguration configuration;

        public JobController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            this.configuration = configuration;
        }


        [HttpGet]
        [Authorize]
        public ActionResult<DefaultResponse<SummaryMeta, List<JobDetail>>> GetAllJobs([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1, [FromQuery] int take = 10, [FromQuery] string name = null, [FromQuery] string sortBy = "JobTitleId", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<JobDetail>>();

            try
            {
                var query = unitOfWork.JobRepository.Query(x => (x.Name.Contains(name) || (x.Priority.ToString().Contains(name)) || name == null), null, false).Select(JobDetail.ProjectionDetail);

                var totalRecords = query.Count();
                List<JobDetail> data;
                if (skip >= 0)
                {
                    //data = query.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    data = query.ToList();
                }
                else
                {
                    data = query.Skip(skip).Take(take).ToList();
                }

                def.meta = new SummaryMeta(200, "success", page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Jobs/GetAllJobs Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }



        [HttpGet]
        [Route("{id}")]
        [Authorize]
        public ActionResult<DefaultResponse<Meta,JobDTO>> GetJob(int Id)
        {
            var def = new DefaultResponse<Meta, JobDTO>();
            try
            {
                var job = unitOfWork.JobRepository.GetById(Id).Mapping();
                def.data = job;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Jobs/GetAllJobs Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Authorize]
        public ActionResult<DefaultResponse<object>> PostJob([FromBody] JobDTO job)
        {
            var def = new DefaultResponse<object>();

            try
            {
                var result = unitOfWork.JobRepository.Insert(job.Mapping());
                unitOfWork.Save();

                if (result.JobId > 0)
                {
                    def.data = result.JobId;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Job/CreateJob Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }

            return Ok();
        }


        [HttpPut]
        [Route("{id}")]
        [Authorize]
        public ActionResult<DefaultResponse<object>> PutJob(int id, [FromBody] JobDTO job)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != job.JobId)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                unitOfWork.JobRepository.Update(job.Mapping());
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Job/Update Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok();
        }


        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        public ActionResult<DefaultResponse<object>> DeleteJob(int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.JobRepository.Delete(id);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Job/Delete Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok();
        }
    }
}