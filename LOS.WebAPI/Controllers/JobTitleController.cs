﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class JobTitleController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        private IConfiguration configuration;

        public JobTitleController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            this.configuration = configuration;
        }


        [HttpGet]
        [Authorize]
        public ActionResult<DefaultResponse<SummaryMeta, List<JobTitleDetail>>> GetAllJobTitles([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1, [FromQuery] int take = 1, [FromQuery] string name = null, [FromQuery] string sortBy = "JobId", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<JobTitleDetail>>();

            try
            {
                var query = unitOfWork.JobTitleRepository.Query(x => (x.Name.Contains(name) ||(x.Priority.Contains(name)) || name == null), null, false).Select(JobTitleDetail.ProjectionDetail);

                var totalRecords = query.Count();
                List<JobTitleDetail> data;
                if (skip >= 0)
                {
                    //data = query.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    data = query.ToList();
                }
                else
                {
                    data = query.Skip(skip).Take(take).ToList();
                }

                def.meta = new SummaryMeta(200, "success", page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "JobTitle/GetAllJobTitile Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }



        [HttpGet]
        [Route("{id}")]
        [Authorize]
        public ActionResult<DefaultResponse<Meta, JobTitleDTO>> GetJobTitle(int Id)
        {
            var def = new DefaultResponse<Meta, JobTitleDTO>();
            try
            {
                var jobTitle = unitOfWork.JobTitleRepository.GetById(Id).Mapping();
                def.data = jobTitle;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "JobTitle/GetAllJobTitle Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Authorize]
        public ActionResult<DefaultResponse<object>> PostJob([FromBody] JobTitleDTO jobTitle)
        {
            var def = new DefaultResponse<object>();

            try
            {
                var result = unitOfWork.JobTitleRepository.Insert(jobTitle.Mapping());
                unitOfWork.Save();

                if (result.JobTitleId > 0)
                {
                    def.data = result.JobTitleId;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Job/CreateJobTitle Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }

            return Ok();
        }


        [HttpPut]
        [Route("{id}")]
        [Authorize]
        public ActionResult<DefaultResponse<object>> PutJobTitle(int id, [FromBody] JobTitleDTO jobTitle)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != jobTitle.JobTitleId)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                unitOfWork.JobTitleRepository.Update(jobTitle.Mapping());
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "JobTitle/Update Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok();
        }


        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        public ActionResult<DefaultResponse<object>> DeleteJobTitle(int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.JobTitleRepository.Delete(id);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "JobTitle/Delete Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok();
        }
    }
}