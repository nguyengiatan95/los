﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Request;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
namespace LOS.WebAPI.Controllers
{
	[ApiVersion("1.0")]
	[Route("api/v{version:apiVersion}/[controller]")]
	[ApiController]
	public class LeadController : ControllerBase
    {
		private IUnitOfWork unitOfWork;
		private IConfiguration configuration;

		public LeadController(IUnitOfWork unitOfWork, IConfiguration configuration)
		{
			this.unitOfWork = unitOfWork;
			this.configuration = configuration;
		}

		[HttpPost]
		[Route("init_loancredit")]
		public ActionResult<DefaultResponse<object>> InitLoanCredit([FromBody] LeadReq req)
		{
			var def = new DefaultResponse<object>();
			try
			{
				// validate input
				if (String.IsNullOrEmpty(req.name) || String.IsNullOrEmpty(req.phone) || req.loanAmount <= 0 || req.loanTime <= 0 || req.cityId <= 0 || req.districtId <= 0 || req.loanProduct <= 0)
				{
					def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
					return Ok(def);
				}

				// check exists
				var exist = unitOfWork.LoanBriefRepository.Any(x => x.Phone == req.phone && x.Status < 100);
				if (exist)
				{
					def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
					return Ok(def);
				}

				// create loan credit
				var loanCredit = new DAL.EntityFramework.LoanBrief();
				loanCredit.CreatedTime = DateTimeOffset.Now;
				loanCredit.FullName = req.name;
				loanCredit.UtmSource = req.utmSource;
				loanCredit.UtmCampaign = req.utmCampaign;
				loanCredit.UtmContent = req.utmContent;
				loanCredit.UtmTerm = req.utmTerm;
				loanCredit.UtmMedium = req.utmMedium;
				loanCredit.Phone = req.phone;				
				loanCredit.ProductId = req.loanProduct;
				loanCredit.Status = 0;
				if (req.loanTime > 0)
				{
					loanCredit.LoanTime = req.loanTime;
					loanCredit.FromDate = DateTime.Now;
					loanCredit.ToDate = DateTime.Now.AddMonths(req.loanTime);
				}
				loanCredit.LoanAmount = req.loanAmount;
				unitOfWork.LoanBriefRepository.Insert(loanCredit);
				unitOfWork.Save();
				def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
				return Ok(def);
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Lead/InitLoanCredit Exception");
				def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
				return Ok(def); 
			}
		}
    }
}