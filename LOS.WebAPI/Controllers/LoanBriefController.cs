﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Utils;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using LOS.Common.Models.Request;
using LOS.Common.Extensions;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Authorization;
using LOS.WebAPI.Models.Request;
using System.Linq.Dynamic.Core;
using System.Security.Cryptography.X509Certificates;
using System.Security.Claims;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using LOS.DAL.Helpers;
using LOS.DAL.Object;
using LOS.DAL.Dapper;
using Microsoft.Extensions.Configuration;
using System.Text;
using static LOS.Common.Models.Request.LoanBriefReq;
using static LOS.DAL.Models.Request.LoanBrief.LoanBrief;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class LoanBriefController : BaseController
    {
        private IUnitOfWork unitOfWork;
        private IConfiguration _baseConfig;
        public LoanBriefController(IUnitOfWork unitOfWork, IConfiguration baseConfig)
        {
            this.unitOfWork = unitOfWork;
            this._baseConfig = baseConfig;
        }

        //[HttpGet]
        //[Route("search")]
        //public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>> Search([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
        //    [FromQuery] int take = 20, [FromQuery] int loanBriefId = -1, [FromQuery] string search = null, [FromQuery] int productId = -1, [FromQuery] int status = -1,
        //    [FromQuery] int platformType = -1, [FromQuery] int provinceId = -1, [FromQuery] int boundTelesaleId = -1, [FromQuery] DateTime? fromDate = null,
        //    [FromQuery] DateTime? toDate = null, [FromQuery] int countCall = -1, [FromQuery] int filterClickTop = -1, [FromQuery] int detailStatusTelesales = -1,
        //    [FromQuery] int filterStaffTelesales = -1, [FromQuery] int statusTelesales = 0, [FromQuery] string teamTelesales = null, [FromQuery] int team = 0,
        //    [FromQuery] DateTime? fromDateLastChangeStatus = null, [FromQuery] DateTime? toDateLastChangeStatus = null, [FromForm] int groupId = 0, [FromQuery] string utmSource = null,
        //    [FromQuery] string field = "", [FromQuery] string sort = "")
        //{
        //    var def = new DefaultResponse<SummaryMeta, List<LoanBriefDetail>>();
        //    var now = DateTimeOffset.Now;
        //    var startDay = new DateTimeOffset(now.Year, now.Month, now.Day, 00, 00, 01, new TimeSpan(7, 0, 0));
        //    var startTime = new DateTimeOffset(now.Year, now.Month, now.Day, 08, 30, 00, new TimeSpan(7, 0, 0));
        //    var endTime = new DateTimeOffset(now.Year, now.Month, now.Day, 11, 55, 00, new TimeSpan(7, 0, 0));
        //    var startTime2 = new DateTimeOffset(now.Year, now.Month, now.Day, 13, 15, 00, new TimeSpan(7, 0, 0));
        //    var endTime2 = new DateTimeOffset(now.Year, now.Month, now.Day, 17, 30, 00, new TimeSpan(7, 0, 0));
        //    //var startTime3 = new DateTimeOffset(now.Year, now.Month, now.Day, 18, 00, 00, new TimeSpan(7, 0, 0));
        //    //var endTime3 = new DateTimeOffset(now.Year, now.Month, now.Day, 20, 15, 00, new TimeSpan(7, 0, 0));
        //    try
        //    {
        //        List<int> lstTeamTelesalesId = new List<int>();
        //        if (teamTelesales != null)
        //            lstTeamTelesalesId = teamTelesales.Split(',').Select(Int32.Parse).ToList();

        //        //Hiển thị tất cả các đơn OTO đã GN không theo telesale
        //        if (statusTelesales == (int)EnumStatusTelesales.DISBURSED
        //            || statusTelesales == (int)EnumStatusTelesales.FINISH)
        //            boundTelesaleId = -1;
        //        var isCheckStatus = true;
        //        var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done
        //            && (x.BoundTelesaleId == boundTelesaleId || boundTelesaleId == -1));
        //        if (filterClickTop == EnumFilterClickTop.ADVICE_LOAN.GetHashCode())
        //        {
        //            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()
        //            && !x.StatusTelesales.HasValue));
        //            isCheckStatus = false;
        //        }
        //        else if (filterClickTop == EnumFilterClickTop.PREPARE_LOAN.GetHashCode())
        //        {
        //            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode()));
        //            isCheckStatus = false;
        //        }

        //        else if (filterClickTop == EnumFilterClickTop.CANCEL_LOAN.GetHashCode())
        //        {
        //            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.CANCELED.GetHashCode() && x.CreatedTime >= startDay));
        //            isCheckStatus = false;
        //        }
        //        else if (filterClickTop == EnumFilterClickTop.HOTLEAD_NORESPONSE.GetHashCode())
        //        {
        //            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode() && x.FirstProcessingTime == null
        //            && ((x.CreatedTime >= startTime && x.CreatedTime < endTime) || (x.CreatedTime >= startTime2 && x.CreatedTime < endTime2)
        //            //|| (x.CreatedTime >= startTime3 && x.CreatedTime < endTime3)
        //            )));
        //            isCheckStatus = false;
        //        }
        //        else if (filterClickTop == EnumFilterClickTop.HOTLEAD_RESPONDED.GetHashCode())
        //        {
        //            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode() && x.FirstProcessingTime != null
        //            && ((x.CreatedTime >= startTime && x.CreatedTime < endTime) || (x.CreatedTime >= startTime2 && x.CreatedTime < endTime2)
        //            //|| (x.CreatedTime >= startTime3 && x.CreatedTime < endTime3)
        //            )));
        //            isCheckStatus = false;
        //        }
        //        else if (filterClickTop == EnumFilterClickTop.AUTOCALL.GetHashCode())
        //        {
        //            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Step > 0 && x.PlatformType != (int)EnumPlatformType.Ai && x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()));
        //            isCheckStatus = false;
        //            field = "Step";
        //            sort = "desc";
        //        }
        //        else if (filterClickTop == EnumFilterClickTop.CHATBOT.GetHashCode())
        //        {
        //            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.PlatformType == (int)EnumPlatformType.Ai && x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()));
        //            isCheckStatus = false;
        //            field = "Step";
        //            sort = "desc";
        //        }
        //        else
        //        {
        //            if (fromDateLastChangeStatus != null && toDateLastChangeStatus != null)
        //                filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LastChangeStatusOfTelesale >= fromDateLastChangeStatus && x.LastChangeStatusOfTelesale <= toDateLastChangeStatus));

        //            if (statusTelesales == (int)EnumStatusTelesales.NotContact || statusTelesales == (int)EnumStatusTelesales.LoanCancel
        //                || statusTelesales == (int)EnumStatusTelesales.DISBURSED || statusTelesales == (int)EnumStatusTelesales.FINISH)
        //            {
        //                status = statusTelesales;
        //                if (statusTelesales == (int)EnumStatusTelesales.NotContact)
        //                {
        //                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.LoanBriefId == loanBriefId || loanBriefId == -1) && ((x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null) || (x.FullName.Contains(search) || search == null))
        //                   && (x.ProductId == productId || productId == -1) && (x.Status == status || x.Status == -1)
        //                   && (x.PlatformType == platformType || platformType == -1)
        //                   && (x.ProvinceId == provinceId || provinceId == -1)
        //                   && (!x.DetailStatusTelesales.HasValue)
        //                   && (x.BoundTelesaleId == filterStaffTelesales || filterStaffTelesales == -1)
        //                   && (x.CreatedTime > fromDate && x.CreatedTime < toDate)
        //                   && (x.TeamTelesalesId == team || team == 0)
        //                   && (x.UtmSource.Contains(utmSource) || utmSource == null)
        //                   && (lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value))));
        //                }
        //                //chỉ mở sản phẩm oto
        //                else if (statusTelesales == (int)EnumStatusTelesales.DISBURSED || statusTelesales == (int)EnumStatusTelesales.FINISH)
        //                {
        //                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.LoanBriefId == loanBriefId || loanBriefId == -1) && ((x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null) || (x.FullName.Contains(search) || search == null))
        //                     && (x.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
        //                     && (x.Status == status || x.Status == -1)
        //                     && (x.ProvinceId == provinceId || provinceId == -1)
        //                     && (x.UtmSource.Contains(utmSource) || utmSource == null)
        //                     && (x.CreatedTime > fromDate && x.CreatedTime < toDate)
        //                     ));
        //                }
        //                else
        //                {
        //                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.LoanBriefId == loanBriefId || loanBriefId == -1) && ((x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null) || (x.FullName.Contains(search) || search == null))
        //                 && (x.ProductId == productId || productId == -1) && (x.Status == status || x.Status == -1)
        //                 && (x.PlatformType == platformType || platformType == -1)
        //                 && (x.ProvinceId == provinceId || provinceId == -1)
        //                 && (x.DetailStatusTelesales == detailStatusTelesales || detailStatusTelesales == -1)
        //                 && (x.BoundTelesaleId == filterStaffTelesales || filterStaffTelesales == -1)
        //                 && (x.UtmSource.Contains(utmSource) || utmSource == null)
        //                 && (x.CreatedTime > fromDate && x.CreatedTime < toDate)
        //                 && (x.TeamTelesalesId == team || team == 0)
        //                 && (lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value))));
        //                }

        //            }
        //            else if (statusTelesales == (int)EnumStatusTelesales.HubBack)
        //            {
        //                filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.LoanBriefId == loanBriefId || loanBriefId == -1) && ((x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null) || (x.FullName.Contains(search) || search == null))
        //                   && (x.ProductId == productId || productId == -1)
        //                   && x.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode()
        //                   && (x.PlatformType == platformType || platformType == -1)
        //                   && (x.ProvinceId == provinceId || provinceId == -1)
        //                   && (x.TelesalesPushAt.HasValue)
        //                   && (x.BoundTelesaleId == filterStaffTelesales || filterStaffTelesales == -1)
        //                   && (x.CreatedTime > fromDate && x.CreatedTime < toDate)
        //                   && (x.CountCall == countCall || countCall == -1)
        //                   && (x.UtmSource.Contains(utmSource) || utmSource == null)
        //                   && (x.TeamTelesalesId == team || team == 0)
        //                   && (lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value))));
        //                isCheckStatus = false;
        //            }
        //            else if (statusTelesales == (int)EnumStatusTelesales.PushHub)
        //            {
        //                filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.LoanBriefId == loanBriefId || loanBriefId == -1) && ((x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null) || (x.FullName.Contains(search) || search == null))
        //                   && (x.ProductId == productId || productId == -1)
        //                   && (x.Status == EnumLoanStatus.APPRAISER_REVIEW.GetHashCode() || x.Status == EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING.GetHashCode()
        //                        || x.Status == EnumLoanStatus.HUB_CHT_APPROVE.GetHashCode())
        //                   && (x.PlatformType == platformType || platformType == -1)
        //                   && (x.ProvinceId == provinceId || provinceId == -1)
        //                   && (x.TelesalesPushAt.HasValue)
        //                   && (x.BoundTelesaleId == filterStaffTelesales || filterStaffTelesales == -1)
        //                   && (x.CreatedTime > fromDate && x.CreatedTime < toDate)
        //                   && (x.CountCall == countCall || countCall == -1)
        //                   && (x.TeamTelesalesId == team || team == 0)
        //                   && (x.UtmSource.Contains(utmSource) || utmSource == null)
        //                   && (lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value))));
        //                isCheckStatus = false;
        //            }
        //            else if (statusTelesales == (int)EnumStatusTelesales.WaitTDHSHandle)
        //            {
        //                filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.LoanBriefId == loanBriefId || loanBriefId == -1) && ((x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null) || (x.FullName.Contains(search) || search == null))
        //                   && (x.ProductId == productId || productId == -1)
        //                   && (x.Status == EnumLoanStatus.BRIEF_APPRAISER_LOAN_DISTRIBUTING.GetHashCode()
        //                        || x.Status == EnumLoanStatus.BRIEF_APPRAISER_REVIEW.GetHashCode()
        //                        || x.Status == EnumLoanStatus.BRIEF_APPRAISER_APPROVE_PROPOSION.GetHashCode()
        //                        || x.Status == EnumLoanStatus.BRIEF_APPRAISER_APPROVE_CANCEL.GetHashCode())
        //                   && (x.PlatformType == platformType || platformType == -1)
        //                   && (x.ProvinceId == provinceId || provinceId == -1)
        //                   && (x.TelesalesPushAt.HasValue)
        //                   && (x.BoundTelesaleId == filterStaffTelesales || filterStaffTelesales == -1)
        //                   && (x.CreatedTime > fromDate && x.CreatedTime < toDate)
        //                   && (x.CountCall == countCall || countCall == -1)
        //                   && (x.TeamTelesalesId == team || team == 0)
        //                   && (x.UtmSource.Contains(utmSource) || utmSource == null)
        //                   && (lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value))));
        //                isCheckStatus = false;
        //            }
        //            else
        //            {
        //                filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.LoanBriefId == loanBriefId || loanBriefId == -1)
        //                   && ((x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null) || (x.FullName.Contains(search) || search == null))
        //                   && (x.ProductId == productId || productId == -1)
        //                   && (x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode() || x.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode())
        //                   && (x.PlatformType == platformType || platformType == -1)
        //                   && (x.ProvinceId == provinceId || provinceId == -1)
        //                   && (x.StatusTelesales == statusTelesales || statusTelesales == 0)
        //                   && (x.DetailStatusTelesales == detailStatusTelesales || detailStatusTelesales == -1)
        //                   && (x.BoundTelesaleId == filterStaffTelesales || filterStaffTelesales == -1)
        //                   && (x.CreatedTime > fromDate && x.CreatedTime < toDate)
        //                   && (x.CountCall == countCall || countCall == -1)
        //                   && (x.TeamTelesalesId == team || team == 0) && (x.UtmSource.Contains(utmSource) || utmSource == null)
        //                   && (lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value))));
        //                isCheckStatus = false;
        //            }
        //        }

        //        if (status > 0)
        //            isCheckStatus = false;

        //        //filter by user
        //        var filterService = new FilterServices<LoanBrief>(unitOfWork);
        //        filterService.GetFilter(GetUserId(), ref filter, isCheckStatus);

        //        var query = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefDetail.ProjectionDetail);
        //        var totalRecords = query.Count();
        //        List<LoanBriefDetail> data;
        //        if (skip == -1)
        //        {
        //            if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(sort))
        //            {
        //                data = query.OrderBy(field + " " + sort).Skip((page - 1) * pageSize).Take(pageSize).ToList();
        //            }
        //            else
        //            {
        //                data = query.OrderByDescending(x => x.UtmSource == "chatbot")
        //                  .ThenByDescending(x => x.TelesalesPushAt.HasValue)
        //                  .ThenByDescending(x => x.ScheduleTime.HasValue)
        //                  .ThenByDescending(x => x.ScheduleTime)
        //                  .ThenByDescending(x => x.LoanBriefId)
        //                  .Skip((page - 1) * pageSize).Take(pageSize).ToList();

        //                //data = query.OrderByDescending(x => x.UtmSource == "chatbot")
        //                // .ThenByDescending(x => x.TelesalesPushAt.HasValue ? x.TelesalesPushAt.Value : DateTime.MinValue)
        //                // .ThenByDescending(x => x.LoanBriefId)
        //                // .Skip((page - 1) * pageSize).Take(pageSize).ToList();
        //            }
        //        }
        //        else
        //        {
        //            if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(sort))
        //            {
        //                data = query.OrderBy(field + " " + sort).Skip(skip).Take(take).ToList();
        //                pageSize = take;
        //            }
        //            else
        //            {
        //                data = query.OrderByDescending(x => x.UtmSource == "chatbot")
        //                .ThenByDescending(x => x.TelesalesPushAt.HasValue)
        //                  .ThenByDescending(x => x.LoanBriefId)
        //                  .Skip((page - 1) * pageSize).Take(pageSize).ToList();
        //                pageSize = take;
        //            }
        //        }
        //        def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
        //        def.data = data;
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex, "LoanBrief/Search Exception");
        //        def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
        //    }
        //    return Ok(def);
        //}

        [HttpGet]
        [Route("search")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>> Search([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] int loanBriefId = -1, [FromQuery] string search = null, [FromQuery] int productId = -1, [FromQuery] int status = -1,
            [FromQuery] int filterSource = -1, [FromQuery] int provinceId = -1, [FromQuery] int boundTelesaleId = -1, [FromQuery] DateTime? fromDate = null,
            [FromQuery] DateTime? toDate = null, [FromQuery] int countCall = -1, [FromQuery] int detailStatusTelesales = -1,
            [FromQuery] int statusTelesales = 0, [FromQuery] string teamTelesales = null, [FromQuery] int team = 0,
            [FromQuery] DateTime? fromDateLastChangeStatus = null, [FromQuery] DateTime? toDateLastChangeStatus = null, [FromQuery] int groupId = 0, [FromQuery] string utmSource = null,
            [FromQuery] int userId = 0, [FromQuery] string field = "", [FromQuery] string sort = "")
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefForBrower>>();
            try
            {
                List<int> lstTeamTelesalesId = new List<int>();
                if (teamTelesales != null)
                    lstTeamTelesalesId = teamTelesales.Split(',').Select(Int32.Parse).ToList();

                //Hiển thị tất cả các đơn OTO đã GN không theo telesale
                if (statusTelesales == (int)EnumStatusTelesales.DISBURSED
                    || statusTelesales == (int)EnumStatusTelesales.FINISH)
                    boundTelesaleId = -1;
                bool isCheckTeam = false;
                var lstUserId = new List<int>();
                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done);
                //Kiểm tra theo boundtelesale
                if (boundTelesaleId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.BoundTelesaleId == boundTelesaleId));
                if (loanBriefId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LoanBriefId == loanBriefId));

                if (team > 0)
                {
                    if (boundTelesaleId > 0)
                    {
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.TeamTelesalesId == team));
                    }
                    else
                    {
                        //lấy ra danh sách userId của team system
                        lstUserId = unitOfWork.UserRepository.Query(x => x.TeamTelesalesId == (int)EnumTeamTelesales.SystemTeam.GetHashCode(), null, false).Select(x => x.UserId).ToList();
                        //kiểm tra xem có chọn là team system team
                        if (team == (int)EnumTeamTelesales.SystemTeam.GetHashCode())
                        {
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => lstUserId.Contains(x.BoundTelesaleId.Value) && lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value)));
                        }
                        else
                        {
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.TeamTelesalesId == team && !lstUserId.Contains(x.BoundTelesaleId.Value)));
                        }
                    }
                    isCheckTeam = true;

                }
                if (!isCheckTeam && lstTeamTelesalesId.Count > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value)));

                if (fromDateLastChangeStatus != null && toDateLastChangeStatus != null)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LastChangeStatusOfTelesale >= fromDateLastChangeStatus
                    && x.LastChangeStatusOfTelesale <= toDateLastChangeStatus));

                if (productId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ProductId == productId));

                if (provinceId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ProvinceId == provinceId));

                if (!string.IsNullOrEmpty(search))
                {
                    var textSearch = search.Trim();
                    if (ConvertExtensions.IsNumber(textSearch)) //Nếu là số
                    {
                        if (textSearch.Length == (int)NationalCardLength.CMND_QD || textSearch.Length == (int)NationalCardLength.CMND || textSearch.Length == (int)NationalCardLength.CCCD)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.NationalCard == textSearch || x.NationCardPlace == textSearch)));
                        else//search phone
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Phone == textSearch || x.PhoneOther == textSearch)));
                    }
                    else//nếu là text =>search tên
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.FullName.Contains(search)));
                }

                //if (team > 0)
                //    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.TeamTelesalesId == team));

                if (!string.IsNullOrEmpty(utmSource))
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.UtmSource.Contains(utmSource)));

                if (statusTelesales != (int)EnumStatusTelesales.NotContact && statusTelesales != (int)EnumStatusTelesales.LoanCancel
                    && statusTelesales != (int)EnumStatusTelesales.DISBURSED && statusTelesales != (int)EnumStatusTelesales.FINISH
                    && countCall > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.CountCall == countCall));
                if (filterSource > 0)
                {
                    if (filterSource == (int)FilterSourceTLS.form_cancel_rmkt)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.UtmSource == LOS.Common.Helpers.ExtentionHelper.GetName((FilterSourceTLS)filterSource)));
                    else if (filterSource == (int)FilterSourceTLS.rmkt_loan_finish)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.IsReborrow == true && x.UtmSource.Contains(LOS.Common.Helpers.ExtentionHelper.GetName((FilterSourceTLS)filterSource))));
                    else if (filterSource == (int)FilterSourceTLS.rmkt_loan_finish_not_reborrow)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.IsReborrow != true && x.UtmSource.Contains(LOS.Common.Helpers.ExtentionHelper.GetName((FilterSourceTLS)filterSource))));
                    else if (filterSource == (int)FilterSourceTLS.SourceSelfCreated)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.CreateBy == userId));
                    else if (filterSource == (int)FilterSourceTLS.SourceFormNewMKT)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.CreateBy.GetValueOrDefault(0) == 0 && x.ReMarketingLoanBriefId.GetValueOrDefault(0) == 0));
                }

                if (statusTelesales == (int)EnumStatusTelesales.NotContact || statusTelesales == (int)EnumStatusTelesales.LoanCancel
                    || statusTelesales == (int)EnumStatusTelesales.DISBURSED || statusTelesales == (int)EnumStatusTelesales.FINISH)
                {
                    status = statusTelesales;
                    if (status > 0)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == status));

                    if (statusTelesales == (int)EnumStatusTelesales.NotContact)
                    {
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => (!x.DetailStatusTelesales.HasValue)
                       && x.CreatedTime > fromDate && x.CreatedTime < toDate));
                    }
                    //chỉ mở sản phẩm oto
                    else if (statusTelesales == (int)EnumStatusTelesales.DISBURSED || statusTelesales == (int)EnumStatusTelesales.FINISH)
                    {
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                         && x.CreatedTime > fromDate && x.CreatedTime < toDate));
                    }
                    else
                    {
                        if (detailStatusTelesales > 0)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.DetailStatusTelesales == detailStatusTelesales));

                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.CreatedTime > fromDate && x.CreatedTime < toDate));
                    }
                }
                else if (statusTelesales == (int)EnumStatusTelesales.HubBack)
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode()
                       && x.TelesalesPushAt.HasValue
                       && x.CreatedTime > fromDate && x.CreatedTime < toDate));
                }
                else if (statusTelesales == (int)EnumStatusTelesales.PushHub)
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Status == EnumLoanStatus.APPRAISER_REVIEW.GetHashCode() || x.Status == EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING.GetHashCode()
                            || x.Status == EnumLoanStatus.HUB_CHT_APPROVE.GetHashCode())
                       && x.TelesalesPushAt.HasValue
                       && x.CreatedTime > fromDate && x.CreatedTime < toDate));
                }
                else if (statusTelesales == (int)EnumStatusTelesales.WaitTDHSHandle)
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Status == EnumLoanStatus.BRIEF_APPRAISER_LOAN_DISTRIBUTING.GetHashCode()
                            || x.Status == EnumLoanStatus.BRIEF_APPRAISER_REVIEW.GetHashCode()
                            || x.Status == EnumLoanStatus.BRIEF_APPRAISER_APPROVE_PROPOSION.GetHashCode()
                            || x.Status == EnumLoanStatus.BRIEF_APPRAISER_APPROVE_CANCEL.GetHashCode())
                       && x.TelesalesPushAt.HasValue
                       && x.CreatedTime > fromDate && x.CreatedTime < toDate));
                }
                else
                {
                    if (statusTelesales > 0)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.StatusTelesales == statusTelesales));
                    if (detailStatusTelesales > 0)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.DetailStatusTelesales == detailStatusTelesales));

                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode() || x.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode())
                       && x.CreatedTime > fromDate && x.CreatedTime < toDate));
                }

                var query = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefForBrower.ProjectionViewDetail);
                var totalRecords = query.Count();
                List<LoanBriefForBrower> data;
                if (skip == -1)
                {
                    if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(sort))
                    {
                        data = query.OrderBy(field + " " + sort).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    }
                    else
                    {
                        data = query.OrderByDescending(x => x.UtmSource == "chatbot")
                         .ThenByDescending(x => x.TelesalesPushAt.HasValue)
                         .ThenByDescending(x => x.ScheduleTime.HasValue)
                         .ThenByDescending(x => x.ScheduleTime)
                         .ThenByDescending(x => x.LoanBriefId)
                         .Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(sort))
                    {
                        data = query.OrderBy(field + " " + sort).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                        pageSize = take;
                    }
                    else
                    {
                        data = query.OrderByDescending(x => x.UtmSource == "chatbot")
                        .ThenByDescending(x => x.TelesalesPushAt.HasValue)
                          .ThenByDescending(x => x.LoanBriefId)
                          .Skip((page - 1) * pageSize).Take(pageSize).ToList();
                        pageSize = take;
                    }
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/Search Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("remarketing_search")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>> RemarketingSearch([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] int productId = -1, [FromQuery] int provinceId = 1, [FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null, [FromQuery] int actionId = -1)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefDetail>>();
            try
            {

                var query = unitOfWork.LoanBriefRepository
                    .Query(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done && (x.ProductId == productId || productId == -1) && (x.ProvinceId == provinceId) && (x.CreatedTime >= fromDate && x.CreatedTime <= toDate) && (x.ReasonCancel == actionId || actionId == -1), null, false).Select(LoanBriefDetail.ProjectionDetail);
                var totalRecords = query.Count();
                List<LoanBriefDetail> data;
                if (skip == -1)
                    data = query.OrderByDescending(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                {
                    data = query.Skip(skip).Take(take).ToList();
                    pageSize = take;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/RemarketingSearch Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("search_loan_all")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefSearchDetail>>> SearchLoanAll([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] int loanBriefId = -1, [FromQuery] string search = null, [FromQuery] string phone = null, [FromQuery] string nationalCard = null)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefSearchDetail>>();
            try
            {
                List<LoanBriefSearchDetail> data;
                if (loanBriefId != -1 || !string.IsNullOrEmpty(search) || !string.IsNullOrEmpty(phone) || !string.IsNullOrEmpty(nationalCard))
                {
                    var query = unitOfWork.LoanBriefRepository
                    .Query(x => (loanBriefId == -1 || x.LoanBriefId == loanBriefId)
                    && (string.IsNullOrEmpty(phone)
                        || (!string.IsNullOrEmpty(phone) && x.Phone == phone)
                        || (!string.IsNullOrEmpty(x.PhoneOther) && x.PhoneOther == phone)
                        )
                    && (string.IsNullOrEmpty(nationalCard) || x.NationalCard == nationalCard
                        || (!string.IsNullOrEmpty(x.NationCardPlace) && x.NationCardPlace == nationalCard)
                        )
                    && (string.IsNullOrEmpty(search)
                        || (x.FullName.Trim().ToLower() == search.ToLower()))
                    , null, false).Select(LoanBriefSearchDetail.ProjectionDetail);

                    var totalRecords = query.Count();
                    if (skip == -1)
                        data = query.OrderByDescending(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    else
                    {
                        data = query.Skip(skip).Take(take).ToList();
                        pageSize = take;
                    }
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                    def.data = data;
                }
                else
                {
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, 0, 0, 0);
                    def.data = null;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/SearchLoanAll Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("loancancel_search")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefCancell>>> LoanCancelSearch([FromQuery] int HubId, [FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] int productId = -1, [FromQuery] int provinceId = -1, [FromQuery] int districtId = -1, [FromQuery] int actionId = -1,
            [FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefCancell>>();
            try
            {
                var listResultCancel = new List<int>() { 130, 269, 310, 378, 491, 664, 690, 270, 384, 492, 593, 495, 496, 319, 379, 501 };
                var listAreaOfHub = unitOfWork.ManagerAreaHubRepository.Query(x => x.ShopId == HubId && x.ProductId == productId, null, false)
                    .Select(x => x.WardId).Distinct().ToList();
                if (actionId == EnumFilterReasonCancel.CheLaiCao.GetHashCode())
                {
                    listResultCancel = new List<int>() { 130, 269, 310, 378, 491, 664, 690 };
                }
                else if (actionId == EnumFilterReasonCancel.KhongMuonTDNha.GetHashCode() || actionId == EnumFilterReasonCancel.KhongMuonTDCty.GetHashCode())
                {
                    listResultCancel = new List<int>() { 270, 384, 492, 593 };
                }
                else if (actionId == EnumFilterReasonCancel.NhuCauVay.GetHashCode())
                {
                    listResultCancel = new List<int>() { 495, 496 };
                }
                else if (actionId == EnumFilterReasonCancel.CongViec.GetHashCode())
                {
                    listResultCancel = new List<int>() { 319, 379, 501 };
                }
                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done && x.Status == (int)EnumLoanStatus.CANCELED
                   && (x.ProductId == productId || productId == -1) && (x.ProvinceId == provinceId || provinceId == -1)
                   && (x.DistrictId == districtId || districtId == -1)
                   && listAreaOfHub.Contains(x.WardId.Value)
                   && (listResultCancel.Contains(x.ReasonCancel.Value) || actionId == -1)
                   && (!x.LoanBriefCancelAt.HasValue || (x.LoanBriefCancelAt.HasValue && DateTime.Now > x.LoanBriefCancelAt.Value.AddDays(7)))
                   && (!x.UpdatedTime.HasValue || (x.UpdatedTime.HasValue && DateTime.Now > x.UpdatedTime.Value.AddDays(30))));
                var query = unitOfWork.LoanBriefRepository
                    .Query(filter.Apply(), null, false).Select(LoanBriefCancell.ProjectionDetail);
                var totalRecords = query.Count();
                List<LoanBriefCancell> data;
                if (skip == -1)
                    data = query.OrderByDescending(x => x.LoanBriefCancelAt).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                {
                    data = query.Skip(skip).Take(take).ToList();
                    pageSize = take;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/LoanCancellSearch Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_loanbrief")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefDetail>>> GetLoanBrief(string Phone)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefDetail>>();
            try
            {
                var data = unitOfWork.LoanBriefRepository.Query(x => x.Phone == Phone, null, false).Select(LoanBriefDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanBriefInitDetail>>> GetById(int id)
        {
            var def = new DefaultResponse<Meta, LoanBriefInitDetail>();
            try
            {
                var data = await unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == id, null, false).Select(LoanBriefInitDetail.ProjectionViewDetail).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("init_loanbrief")]
        public ActionResult<DefaultResponse<object>> InitLoanBrief(LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.BeginTransaction();
                //xử lý thông tin customer
                var customerOld = unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == entity.Customer.Phone);
                if (customerOld != null && customerOld.CustomerId > 0)
                {
                    //cập nhật thông tin customer
                    entity.CustomerId = customerOld.CustomerId;
                    Ultility.CopyObject(entity.Customer, ref customerOld, true);
                    customerOld.UpdatedAt = DateTimeOffset.Now;
                    //customerOld.CustomerId = entity.CustomerId;
                    //cập nhật User
                    entity.Customer = customerOld;
                    entity.Customer.CustomerId = entity.CustomerId ?? 0;
                    unitOfWork.CustomerRepository.Update(customerOld);
                    unitOfWork.Save();
                }
                else
                {
                    entity.Customer.CreatedAt = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);
                    entity.Customer.JobId = entity.LoanBriefJob.JobId;
                    entity.Customer.CompanyName = entity.LoanBriefJob.CompanyName;
                    entity.Customer.CompanyAddress = entity.LoanBriefJob.CompanyAddress;
                    entity.Customer.ProvinceId = entity.LoanBriefResident.ProvinceId;
                    entity.Customer.DistrictId = entity.LoanBriefResident.DistrictId;
                    entity.Customer.WardId = entity.LoanBriefResident.WardId;
                    entity.Customer.Address = entity.LoanBriefResident.Address;
                    unitOfWork.CustomerRepository.Insert(entity.Customer);
                    unitOfWork.Save();
                    entity.CustomerId = entity.Customer.CustomerId;
                }
                #region Xử lý map + insert LoanBrief
                entity.CreatedTime = DateTimeOffset.Now;
                entity.FullName = entity.Customer.FullName;
                entity.Phone = entity.Customer.Phone;
                entity.Dob = entity.Customer.Dob;
                entity.Gender = entity.Customer.Gender;
                entity.NationalCard = entity.Customer.NationalCard;
                entity.Passport = entity.Customer.Passport;
                entity.NationalCardDate = entity.Customer.NationalCardDate;
                entity.NationCardPlace = entity.Customer.NationCardPlace;
                entity.ProvinceId = entity.LoanBriefResident.ProvinceId;
                entity.DistrictId = entity.LoanBriefResident.DistrictId;
                entity.WardId = entity.LoanBriefResident.WardId;
                entity.LoanAmountFirst = entity.LoanAmount;
                //insert to db
                unitOfWork.LoanBriefRepository.Insert(entity);
                unitOfWork.Save();
                #endregion
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
                unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBrief/InitLoanBrief POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("create_remarketing")]
        public ActionResult<DefaultResponse<object>> CloneLoanBrief([FromBody] LoanBrief loan)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var customerOld = unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.CustomerId == loan.CustomerId);
                if (customerOld != null && customerOld.CustomerId > 0)
                {
                    //cập nhật thông tin customer
                    loan.CustomerId = customerOld.CustomerId;
                    Ultility.CopyObject(loan.Customer, ref customerOld, true);
                    customerOld.UpdatedAt = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);
                    //customerOld.CustomerId = entity.CustomerId;
                    //cập nhật User
                    loan.Customer = customerOld;
                    loan.Customer.CustomerId = loan.CustomerId ?? 0;
                    unitOfWork.CustomerRepository.Update(customerOld);
                    unitOfWork.Save();
                }

                var result = unitOfWork.LoanBriefRepository.Insert(loan);
                unitOfWork.Save();
                if (result.LoanBriefId > 0)
                {
                    def.data = result.LoanBriefId;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Loanbrief/CloneLoanBrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("init_loanbrief/{id}")]
        public ActionResult<DefaultResponse<object>> InitLoanBrief(int id, [FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                var userId = int.Parse(identity.Claims.Where(c => c.Type == "Id").Select(c => c.Value).SingleOrDefault());

                if (id != entity.LoanBriefId || entity.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanbrief = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false, x => x.Customer, x => x.LoanBriefCompany, x => x.LoanBriefHousehold
                , x => x.LoanBriefJob, x => x.LoanBriefProperty, x => x.LoanBriefResident, x => x.LoanBriefRelationship).FirstOrDefault();
                var loanbriefHistory = new LoanBriefHistory()
                {
                    LoanBriefId = entity.LoanBriefId,
                    OldValue = JsonConvert.SerializeObject(loanbrief, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    }),
                    ActorId = EnumActor.UpdateLoanbrief.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    UserId = userId
                };

                unitOfWork.BeginTransaction();
                //Check nếu là đơn topup cho update thông tin số tiền vay, thông tin đồng nghiệp, thông tin ngân hàng
                if (entity.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                {
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                    {
                        LoanAmount = entity.LoanAmount,
                        ReceivingMoneyType = entity.ReceivingMoneyType,
                        BankAccountName = entity.BankAccountName,
                        BankAccountNumber = entity.BankAccountNumber,
                        BankCardNumber = entity.BankCardNumber,
                        BankId = entity.BankId,
                        IsCheckBank = entity.IsCheckBank
                    });

                    //Lưu thông tin đồng nghiệp
                    if (loanbrief.LoanBriefRelationship != null && loanbrief.LoanBriefRelationship.Count > 0)
                    {
                        foreach (var item in entity.LoanBriefRelationship)
                        {
                            if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                            {
                                item.LoanBriefId = entity.LoanBriefId;
                                if (item.Id > 0)
                                    unitOfWork.LoanBriefRelationshipRepository.Update(item);
                                else
                                    unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                            }
                        }
                    }

                }
                else
                {
                    loanbrief.Customer = entity.Customer;
                    loanbrief.LoanBriefCompany = entity.LoanBriefCompany;
                    loanbrief.LoanBriefResident = entity.LoanBriefResident;
                    loanbrief.LoanBriefHousehold = entity.LoanBriefHousehold;
                    loanbrief.LoanBriefRelationship = entity.LoanBriefRelationship;
                    loanbrief.LoanBriefProperty = entity.LoanBriefProperty;
                    loanbrief.LoanBriefJob = entity.LoanBriefJob;
                    loanbrief.LoanBriefDocument = entity.LoanBriefDocument;

                    //xử lý thông tin customer
                    var customerOld = unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.CustomerId == entity.Customer.CustomerId);
                    if (customerOld != null && customerOld.CustomerId > 0)
                    {
                        //cập nhật thông tin customer
                        Ultility.CopyObject(loanbrief.Customer, ref customerOld, true);
                        customerOld.UpdatedAt = DateTimeOffset.Now;
                        customerOld.JobId = entity.LoanBriefJob.JobId;
                        customerOld.CompanyName = entity.LoanBriefJob.CompanyName;
                        customerOld.CompanyAddress = entity.LoanBriefJob.CompanyAddress;
                        customerOld.ProvinceId = entity.LoanBriefResident.ProvinceId;
                        customerOld.DistrictId = entity.LoanBriefResident.DistrictId;
                        customerOld.WardId = entity.LoanBriefResident.WardId;
                        customerOld.Address = entity.LoanBriefResident.Address;
                        entity.Customer = customerOld;
                        unitOfWork.CustomerRepository.Update(customerOld);
                    }

                    //nếu là đơn doanh nghiệp => lưu thông tin doanh nghiệp
                    if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                    {
                        if (loanbrief.LoanBriefCompany.LoanBriefId > 0)
                        {
                            unitOfWork.LoanBriefCompanyRepository.Update(loanbrief.LoanBriefCompany);
                        }
                        else
                        {
                            loanbrief.LoanBriefCompany.LoanBriefId = entity.LoanBriefId;
                            unitOfWork.LoanBriefCompanyRepository.Insert(loanbrief.LoanBriefCompany);
                        }
                    }
                    //Cập nhật thông tin địa chỉ đang ở vào bảng LoanBriefResident
                    if (loanbrief.LoanBriefResident.LoanBriefResidentId > 0
                        && unitOfWork.LoanBriefResidentRepository.Any(x => x.LoanBriefResidentId == loanbrief.LoanBriefResident.LoanBriefResidentId))
                    {
                        unitOfWork.LoanBriefResidentRepository.Update(loanbrief.LoanBriefResident);
                    }
                    else
                    {
                        loanbrief.LoanBriefResident.LoanBriefResidentId = loanbrief.LoanBriefId;
                        unitOfWork.LoanBriefResidentRepository.Insert(loanbrief.LoanBriefResident);
                    }

                    ////Cập nhật thông tin địa chỉ hộ khẩu LoanBriefHousehold
                    if (loanbrief.LoanBriefHousehold.LoanBriefHouseholdId > 0
                        && unitOfWork.LoanBriefHouseholdRepository.Any(x => x.LoanBriefHouseholdId == loanbrief.LoanBriefHousehold.LoanBriefHouseholdId))
                    {
                        unitOfWork.LoanBriefHouseholdRepository.Update(loanbrief.LoanBriefHousehold);
                    }
                    else
                    {
                        loanbrief.LoanBriefHousehold.LoanBriefHouseholdId = loanbrief.LoanBriefId;
                        unitOfWork.LoanBriefHouseholdRepository.Insert(loanbrief.LoanBriefHousehold);
                    }

                    ////cập nhật thông tin công việc
                    if (loanbrief.LoanBriefJob.LoanBriefJobId > 0
                        && unitOfWork.LoanBriefJobRepository.Any(x => x.LoanBriefJobId == loanbrief.LoanBriefJob.LoanBriefJobId))
                        unitOfWork.LoanBriefJobRepository.Update(loanbrief.LoanBriefJob);
                    else
                    {
                        loanbrief.LoanBriefJob.LoanBriefJobId = loanbrief.LoanBriefId;
                        unitOfWork.LoanBriefJobRepository.Insert(loanbrief.LoanBriefJob);
                    }
                    //cập nhật thông tin tài sản
                    if (loanbrief.LoanBriefProperty.LoanBriefPropertyId > 0
                         && unitOfWork.LoanBriefPropertyRepository.Any(x => x.LoanBriefPropertyId == loanbrief.LoanBriefProperty.LoanBriefPropertyId))
                        unitOfWork.LoanBriefPropertyRepository.Update(loanbrief.LoanBriefProperty);
                    else
                    {
                        loanbrief.LoanBriefProperty.LoanBriefPropertyId = loanbrief.LoanBriefId;
                        unitOfWork.LoanBriefPropertyRepository.Insert(loanbrief.LoanBriefProperty);
                    }
                    //xử lý documment
                    if (entity.LoanBriefDocument != null && entity.LoanBriefDocument.Count > 0)
                    {
                        var oldData = unitOfWork.LoanBriefDocumentRepository.Query(x => x.LoanBriefId == loanbrief.LoanBriefId, null, false).ToList();
                        if (oldData != null && oldData.Count > 0)
                        {
                            foreach (var oldItem in oldData)
                                unitOfWork.LoanBriefDocumentRepository.Delete(oldItem.LoanBriefDocumentId);
                            unitOfWork.Save();
                        }

                        foreach (var item in entity.LoanBriefDocument.ToList())
                        {
                            if (item.LoanBriefDocumentId == 0)
                                unitOfWork.LoanBriefDocumentRepository.Insert(new LoanBriefDocument
                                {
                                    LoanBriefId = loanbrief.LoanBriefId,
                                    DocumentId = item.DocumentId
                                });
                        }
                    }
                    //Lưu thông tin đồng nghiệp
                    if (loanbrief.LoanBriefRelationship != null && loanbrief.LoanBriefRelationship.Count > 0)
                    {
                        foreach (var item in entity.LoanBriefRelationship)
                        {
                            if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                            {
                                item.LoanBriefId = entity.LoanBriefId;
                                if (item.Id > 0)
                                    unitOfWork.LoanBriefRelationshipRepository.Update(item);
                                else
                                    unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                            }
                        }
                    }

                    #region Xử lý map + insert LoanBrief
                    entity.FullName = entity.Customer.FullName;
                    entity.Dob = entity.Customer.Dob;
                    entity.Gender = entity.Customer.Gender;
                    entity.NationalCard = entity.Customer.NationalCard;
                    entity.Passport = entity.Customer.Passport;
                    entity.NationalCardDate = entity.Customer.NationalCardDate;
                    entity.NationCardPlace = entity.Customer.NationCardPlace;
                    entity.ProvinceId = entity.LoanBriefResident.ProvinceId;
                    entity.DistrictId = entity.LoanBriefResident.DistrictId;
                    entity.WardId = entity.LoanBriefResident.WardId;
                    entity.UpdatedTime = DateTimeOffset.Now;
                    Ultility.CopyObject(entity, ref loanbrief, false);
                    loanbrief.UpdatedTime = DateTime.Now;
                    if (loanbrief.LoanTime > 0)
                        loanbrief.ToDate = loanbrief.FromDate.Value.AddMonths((int)loanbrief.LoanTime);

                    //check nếu đơn chưa xử lý lần đầu add thời gian xử lý
                    if (loanbrief.FirstProcessingTime == null)
                        loanbrief.FirstProcessingTime = DateTime.Now;
                    //Check nếu có mua bảo hiểm thì gán đơn vị bảo hiểm Bảo Minh type = 2
                    if (loanbrief.BuyInsurenceCustomer == true)
                        loanbrief.TypeInsurence = 2;
                    if (loanbrief.BuyInsurenceCustomer == false)
                        loanbrief.TypeInsurence = null;
                    //Save
                    unitOfWork.LoanBriefRepository.Update(loanbrief);
                    unitOfWork.Save();
                }


                //lưu thông tin log
                loanbriefHistory.NewValue = JsonConvert.SerializeObject(loanbrief, Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                });
                unitOfWork.LoanBriefHistoryRepository.Insert(loanbriefHistory);
                unitOfWork.Save();

                #endregion
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
                unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBrief/InitLoanBrief PUT Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("update_loanbrief_script/{id}")]
        public ActionResult<DefaultResponse<object>> UpdateLoanBriefScript(int id, [FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                var userId = int.Parse(identity.Claims.Where(c => c.Type == "Id").Select(c => c.Value).SingleOrDefault());


                if (id != entity.LoanBriefId || entity.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanbrief = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false, x => x.Customer, x => x.LoanBriefCompany, x => x.LoanBriefHousehold
               , x => x.LoanBriefJob, x => x.LoanBriefProperty, x => x.LoanBriefResident, x => x.LoanBriefRelationship).FirstOrDefault();
                var loanbriefHistory = new LoanBriefHistory()
                {
                    LoanBriefId = entity.LoanBriefId,
                    OldValue = JsonConvert.SerializeObject(loanbrief, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    }),
                    ActorId = EnumActor.UpdateScript.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    UserId = userId
                };
                loanbrief.Customer = entity.Customer;
                loanbrief.LoanBriefCompany = entity.LoanBriefCompany;
                loanbrief.LoanBriefResident = entity.LoanBriefResident;
                loanbrief.LoanBriefHousehold = entity.LoanBriefHousehold;
                loanbrief.LoanBriefRelationship = entity.LoanBriefRelationship;
                loanbrief.LoanBriefProperty = entity.LoanBriefProperty;
                loanbrief.LoanBriefJob = entity.LoanBriefJob;
                loanbrief.LoanBriefDocument = entity.LoanBriefDocument;
                unitOfWork.BeginTransaction();
                //xử lý thông tin customer
                var customerOld = unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.CustomerId == entity.Customer.CustomerId);
                if (customerOld != null && customerOld.CustomerId > 0)
                {
                    //cập nhật thông tin customer
                    Ultility.CopyObject(loanbrief.Customer, ref customerOld, true);
                    customerOld.UpdatedAt = DateTimeOffset.Now;
                    customerOld.JobId = entity.LoanBriefJob.JobId;
                    customerOld.CompanyName = entity.LoanBriefJob.CompanyName;
                    customerOld.CompanyAddress = entity.LoanBriefJob.CompanyAddress;
                    customerOld.ProvinceId = entity.LoanBriefResident.ProvinceId;
                    customerOld.DistrictId = entity.LoanBriefResident.DistrictId;
                    customerOld.WardId = entity.LoanBriefResident.WardId;
                    customerOld.Address = entity.LoanBriefResident.Address;
                    entity.Customer = customerOld;
                    unitOfWork.CustomerRepository.Update(customerOld);
                }

                //nếu là đơn doanh nghiệp => lưu thông tin doanh nghiệp
                if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    if (loanbrief.LoanBriefCompany.LoanBriefId > 0)
                    {
                        unitOfWork.LoanBriefCompanyRepository.Update(loanbrief.LoanBriefCompany);
                    }
                    else
                    {
                        loanbrief.LoanBriefCompany.LoanBriefId = entity.LoanBriefId;
                        unitOfWork.LoanBriefCompanyRepository.Insert(loanbrief.LoanBriefCompany);
                    }
                }
                //Cập nhật thông tin địa chỉ đang ở vào bảng LoanBriefResident
                if (loanbrief.LoanBriefResident.LoanBriefResidentId > 0
                    && unitOfWork.LoanBriefResidentRepository.Any(x => x.LoanBriefResidentId == loanbrief.LoanBriefResident.LoanBriefResidentId))
                {
                    unitOfWork.LoanBriefResidentRepository.Update(loanbrief.LoanBriefResident);
                }
                else
                {
                    loanbrief.LoanBriefResident.LoanBriefResidentId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefResidentRepository.Insert(loanbrief.LoanBriefResident);
                }

                ////Cập nhật thông tin địa chỉ hộ khẩu LoanBriefHousehold
                if (loanbrief.LoanBriefHousehold.LoanBriefHouseholdId > 0
                    && unitOfWork.LoanBriefHouseholdRepository.Any(x => x.LoanBriefHouseholdId == loanbrief.LoanBriefHousehold.LoanBriefHouseholdId))
                {
                    unitOfWork.LoanBriefHouseholdRepository.Update(loanbrief.LoanBriefHousehold);
                }
                else
                {
                    loanbrief.LoanBriefHousehold.LoanBriefHouseholdId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefHouseholdRepository.Insert(loanbrief.LoanBriefHousehold);
                }

                ////cập nhật thông tin công việc
                if (loanbrief.LoanBriefJob.LoanBriefJobId > 0
                    && unitOfWork.LoanBriefJobRepository.Any(x => x.LoanBriefJobId == loanbrief.LoanBriefJob.LoanBriefJobId))
                    unitOfWork.LoanBriefJobRepository.Update(loanbrief.LoanBriefJob);
                else
                {
                    loanbrief.LoanBriefJob.LoanBriefJobId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefJobRepository.Insert(loanbrief.LoanBriefJob);
                }
                //cập nhật thông tin tài sản
                if (loanbrief.LoanBriefProperty.LoanBriefPropertyId > 0
                     && unitOfWork.LoanBriefPropertyRepository.Any(x => x.LoanBriefPropertyId == loanbrief.LoanBriefProperty.LoanBriefPropertyId))
                    unitOfWork.LoanBriefPropertyRepository.Update(loanbrief.LoanBriefProperty);
                else
                {
                    loanbrief.LoanBriefProperty.LoanBriefPropertyId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefPropertyRepository.Insert(loanbrief.LoanBriefProperty);
                }
                //xử lý documment
                if (entity.LoanBriefDocument != null && entity.LoanBriefDocument.Count > 0)
                {
                    var oldData = unitOfWork.LoanBriefDocumentRepository.Query(x => x.LoanBriefId == loanbrief.LoanBriefId, null, false).ToList();
                    if (oldData != null && oldData.Count > 0)
                    {
                        foreach (var oldItem in oldData)
                            unitOfWork.LoanBriefDocumentRepository.Delete(oldItem.LoanBriefDocumentId);
                    }

                    foreach (var item in entity.LoanBriefDocument.ToList())
                    {
                        if (item.LoanBriefDocumentId == 0)
                            unitOfWork.LoanBriefDocumentRepository.Insert(new LoanBriefDocument
                            {
                                LoanBriefId = loanbrief.LoanBriefId,
                                DocumentId = item.DocumentId
                            });
                    }
                }
                //Lưu thông tin đồng nghiệp
                if (loanbrief.LoanBriefRelationship != null && loanbrief.LoanBriefRelationship.Count > 0)
                {
                    foreach (var item in entity.LoanBriefRelationship)
                    {
                        if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                        {
                            item.LoanBriefId = entity.LoanBriefId;
                            if (item.Id > 0)
                                unitOfWork.LoanBriefRelationshipRepository.Update(item);
                            else
                                unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                        }
                    }
                }

                if (unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == loanbrief.LoanBriefId))
                {
                    //xóa dữ liệu cũ
                    unitOfWork.LoanBriefQuestionScriptRepository.Delete(x => x.LoanBriefId == loanbrief.LoanBriefId);
                }
                unitOfWork.LoanBriefQuestionScriptRepository.Insert(entity.LoanBriefQuestionScript);

                #region Xử lý map + insert LoanBrief
                entity.FullName = entity.Customer.FullName;
                entity.Dob = entity.Customer.Dob;
                entity.Gender = entity.Customer.Gender;
                entity.NationalCard = entity.Customer.NationalCard;
                entity.Passport = entity.Customer.Passport;
                entity.NationalCardDate = entity.Customer.NationalCardDate;
                entity.NationCardPlace = entity.Customer.NationCardPlace;
                entity.ProvinceId = entity.LoanBriefResident.ProvinceId;
                entity.DistrictId = entity.LoanBriefResident.DistrictId;
                entity.WardId = entity.LoanBriefResident.WardId;
                entity.UpdatedTime = DateTimeOffset.Now;
                Ultility.CopyObject(entity, ref loanbrief, false);
                loanbrief.UpdatedTime = DateTime.Now;
                if (loanbrief.LoanTime > 0)
                    loanbrief.ToDate = loanbrief.FromDate.Value.AddMonths((int)loanbrief.LoanTime);

                //check nếu đơn chưa xử lý lần đầu add thời gian xử lý
                if (loanbrief.FirstProcessingTime == null)
                    loanbrief.FirstProcessingTime = DateTime.Now;
                unitOfWork.LoanBriefRepository.Update(loanbrief);
                unitOfWork.Save();
                //lưu thông tin log
                loanbriefHistory.NewValue = JsonConvert.SerializeObject(loanbrief, Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                });
                unitOfWork.LoanBriefHistoryRepository.Insert(loanbriefHistory);
                unitOfWork.Save();

                #endregion
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
                unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBrief/UpdateLoanBriefScript Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_status")]
        public ActionResult<DefaultResponse<object>> UpdateStatusLoanBrief([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                loanbrief.Status = entity.Status;
                unitOfWork.LoanBriefRepository.Update(loanbrief);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = loanbrief.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/InitLoanBrief PUT Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_note")]
        public ActionResult<DefaultResponse<object>> AddNote([FromBody] LoanBriefNote entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LoanBriefNoteRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefNoteId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddNote Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_log")]
        public ActionResult<DefaultResponse<object>> AddLog([FromBody] LoanBriefLog entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LoanBriefLogRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddLog Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        //[HttpPost]
        //[Route("update_userid")]
        //public ActionResult<DefaultResponse<Meta>> UpdateBoundTelesale([FromBody] BoundTelesaleReq req)
        //{
        //    var def = new DefaultResponse<Meta>();
        //    try
        //    {
        //        if (req.loanBriefId <= 0 || req.userId <= 0)
        //        {
        //            def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
        //            return Ok(def);
        //        }
        //        // check if loan action exists
        //        var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.loanBriefId);
        //        if (loanInfo != null)
        //        {
        //            //nếu đã có telesales rồi => đang chuyển telesales khác => không cần đẩy lên luồng
        //            //mở thêm những đơn do telesale tạo => salesadmin được đẩy đúng về teles đó
        //            if (loanInfo.BoundTelesaleId == 0 || loanInfo.BoundTelesaleId == null || loanInfo.BoundTelesaleId == req.userId)
        //            {
        //                if (req.ActionState > 0 && req.PipelineState > 0)
        //                {
        //                    loanInfo.ActionState = req.ActionState;
        //                    loanInfo.PipelineState = req.PipelineState;
        //                    loanInfo.InProcess = EnumInProcess.Process.GetHashCode();
        //                }
        //            }
        //            var user = unitOfWork.UserRepository.GetById(req.userId);
        //            if (user != null && user.TeamTelesalesId > 0)
        //                loanInfo.TeamTelesalesId = user.TeamTelesalesId;
        //            loanInfo.BoundTelesaleId = req.userId;
        //            //cập nhật thời gian nhận đơn cuối cùng của telesale
        //            unitOfWork.UserRepository.Update(x => x.UserId == req.userId, x => new DAL.EntityFramework.User()
        //            {
        //                LastReceived = DateTime.Now
        //            });
        //            unitOfWork.LoanBriefRepository.Update(loanInfo);
        //            unitOfWork.Save();
        //            def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
        //            return Ok(def);
        //        }
        //        else
        //        {
        //            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
        //            return Ok(def);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex, "Loanbrief/UpdateBoundTelesale Exception");
        //        def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
        //    }
        //    return Ok(def);
        //}

        [HttpPost]
        [Route("update_userid")]
        public ActionResult<DefaultResponse<Meta>> UpdateBoundTelesale([FromBody] BoundTelesaleReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.loanBriefId <= 0 || req.userId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.loanBriefId);
                if (loanInfo != null)
                {

                    var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                    var sql = new StringBuilder();
                    sql.AppendLine("update LoanBrief set ");
                    //nếu đã có telesales rồi => đang chuyển telesales khác => không cần đẩy lên luồng
                    //mở thêm những đơn do telesale tạo => salesadmin được đẩy đúng về teles đó
                    if (loanInfo.BoundTelesaleId == 0 || loanInfo.BoundTelesaleId == null || loanInfo.BoundTelesaleId == req.userId)
                    {
                        if (req.ActionState > 0 && req.PipelineState > 0)
                        {
                            sql.AppendFormat("ActionState = {0}, ", req.ActionState);
                            sql.AppendFormat("PipelineState = {0}, ", req.PipelineState);
                            sql.AppendFormat("InProcess = {0}, ", EnumInProcess.Process.GetHashCode());
                        }
                    }
                    if (req.TeamTelesaleId > 0)
                        sql.AppendFormat("TeamTelesalesId = {0}, ", req.TeamTelesaleId);
                    sql.AppendFormat("BoundTelesaleId = {0} ", req.userId);
                    sql.AppendFormat("where LoanBriefId = {0}", loanInfo.LoanBriefId);
                    db.Execute(sql.ToString());
                    //cập nhật thời gian nhận đơn cuối cùng của telesale
                    db.Execute(string.Format("update [User] set LastReceived = GETDATE() where UserId = {0}", req.userId));
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Loanbrief/UpdateBoundTelesale Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_approveid")]
        public ActionResult<DefaultResponse<Meta>> UpdateApprove([FromBody] BoundTelesaleReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.loanBriefId <= 0 || req.userId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.loanBriefId);
                if (loanInfo != null)
                {
                    // update loan CoordinatorUserId
                    loanInfo.CoordinatorUserId = req.userId;
                    unitOfWork.LoanBriefRepository.Update(loanInfo);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateApprove CoordinatorUserId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_lenderid")]
        public ActionResult<DefaultResponse<Meta>> UpdateLender([FromBody] LenderReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.loanBriefId <= 0 || req.lenderId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.loanBriefId);
                if (loanInfo != null)
                {
                    // update loan CoordinatorUserId
                    loanInfo.LenderId = req.lenderId;
                    loanInfo.ActionState = EnumActionPush.Push.GetHashCode();
                    loanInfo.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                    unitOfWork.LoanBriefRepository.Update(loanInfo);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateLender Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_isupload")]
        public ActionResult<DefaultResponse<Meta>> UpdateIsUpload([FromBody] IsUploadReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.loanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.loanbriefId);
                if (loanInfo != null)
                {

                    loanInfo.IsUploadState = req.status;
                    unitOfWork.LoanBriefRepository.Update(loanInfo);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateIsUpload Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_insurence")]
        public ActionResult<DefaultResponse<Meta>> UpdateInsurence([FromBody] LenderReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.loanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.loanBriefId);
                if (loanInfo != null)
                {
                    // update loan CoordinatorUserId
                    loanInfo.BuyInsurenceCustomer = true;
                    unitOfWork.LoanBriefRepository.Update(loanInfo);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateInsurence Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("get_telesaleid")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefDetail>>> GetLoanTelesaleId(int TelesaleId)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefDetail>>();
            try
            {
                var data = unitOfWork.LoanBriefRepository.Query(x => x.BoundTelesaleId == TelesaleId, null, false).Select(LoanBriefDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanTelesaleId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #region push loanbrief
        [HttpPost]
        [Route("push")]
        public ActionResult<DefaultResponse<object>> PushLoanBrief([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                loanbrief.PipelineState = entity.PipelineState;
                loanbrief.ActionState = entity.ActionState;
                loanbrief.InProcess = EnumInProcess.Process.GetHashCode();
                if (entity.LoanAmount.HasValue)
                    loanbrief.LoanAmount = entity.LoanAmount;
                if (entity.TelesalesPushAt.HasValue)
                    loanbrief.TelesalesPushAt = entity.TelesalesPushAt;
                if (entity.HubPushAt.HasValue)
                    loanbrief.HubPushAt = entity.HubPushAt;
                if (entity.CoordinatorPushAt.HasValue)
                    loanbrief.CoordinatorPushAt = entity.CoordinatorPushAt;
                if (entity.HubId > 0)
                    loanbrief.HubId = entity.HubId;
                if (entity.ApproverStatusDetail.HasValue)
                    loanbrief.ApproverStatusDetail = entity.ApproverStatusDetail;
                //if (entity.LoanAmountFinal.HasValue)
                //    loanbrief.LoanAmountFinal = entity.LoanAmount;
                unitOfWork.LoanBriefRepository.Update(loanbrief);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = loanbrief.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/PushLoanBrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Return loanbrief
        [HttpPost]
        [Route("return")]
        public ActionResult<DefaultResponse<object>> ReturnBrief([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                {
                    InProcess = (int)EnumInProcess.Process,
                    ActionState = entity.ActionState,
                    PipelineState = entity.PipelineState,
                    ReasonCoordinatorBack = entity.ReasonCoordinatorBack
                });
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ReturnBrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        [HttpPost]
        [Route("cancel")]
        public ActionResult<DefaultResponse<object>> CancelLoanBrief([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                loanbrief.PipelineState = entity.PipelineState;
                loanbrief.ActionState = entity.ActionState;
                loanbrief.InProcess = EnumInProcess.Process.GetHashCode();
                //loanbrief.
                if (entity.FirstProcessingTime != null || entity.FirstProcessingTime != DateTime.MinValue)
                    loanbrief.FirstProcessingTime = entity.FirstProcessingTime;
                if (entity.ReasonCancel > 0)
                {
                    loanbrief.ReasonCancel = entity.ReasonCancel;

                    if (loanbrief.ValueCheckQualify.HasValue && (loanbrief.ValueCheckQualify == 248 || loanbrief.ValueCheckQualify == 252))
                    {
                        //lý do hủy nằm trong các lý do sau đây
                        //2.3. Thay đổi nhu cầu không vay nữa 
                        //7.6.Cân nhắc mãi nhưng khách không vay
                        //1.1.Không liên hệ được 
                        //659 1.2.Không có nhu cầu vay
                        if (entity.ReasonCancel == 666 || entity.ReasonCancel == 706 || entity.ReasonCancel == 658 || entity.ReasonCancel == 659)
                        {
                            loanbrief.ValueCheckQualify = loanbrief.ValueCheckQualify - (int)EnumCheckQualify.NeedLoan;
                        }

                        //660 1.3.Không thuộc khu vực hỗ trợ
                        if (entity.ReasonCancel == 660)
                        {
                            loanbrief.ValueCheckQualify = loanbrief.ValueCheckQualify - (int)EnumCheckQualify.InAreaSupport;
                        }

                        //661 1.4.Không có xe máy
                        if (entity.ReasonCancel == 661)
                        {
                            loanbrief.ValueCheckQualify = loanbrief.ValueCheckQualify - (int)EnumCheckQualify.HaveMotobike;
                        }

                        //662 1.5.Không có đăng ký xe bản gốc
                        if (entity.ReasonCancel == 662)
                        {
                            loanbrief.ValueCheckQualify = loanbrief.ValueCheckQualify - (int)EnumCheckQualify.HaveOriginalVehicleRegistration;
                        }

                        //663 1.6.Không thuộc độ tuổi quy định
                        if (entity.ReasonCancel == 663)
                        {
                            loanbrief.ValueCheckQualify = loanbrief.ValueCheckQualify - (int)EnumCheckQualify.InAge;
                        }

                    }

                }
                if (!string.IsNullOrEmpty(entity.LoanBriefComment))
                    loanbrief.LoanBriefComment = entity.LoanBriefComment;
                if (entity.LoanBriefCancelAt.HasValue)
                    loanbrief.LoanBriefCancelAt = entity.LoanBriefCancelAt;
                if (entity.LoanBriefCancelBy > 0)
                    loanbrief.LoanBriefCancelBy = entity.LoanBriefCancelBy;
                unitOfWork.LoanBriefRepository.Update(loanbrief);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = loanbrief.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/CancelLoanBrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #region dashboad telesale
        [HttpGet]
        [Route("get_loanbrief_schedule")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefDetail>>> GetLoanBriefSchedule(int telesale, DateTime dateTime)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefDetail>>();
            try
            {
                var query = unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.INIT && x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done
                        && x.BoundTelesaleId == telesale && x.ScheduleTime.HasValue && DateTime.Compare(x.ScheduleTime.Value.Date, dateTime.Date) <= 0, null, false)
                            .Select(LoanBriefDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBriefSchedule Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_loanbrief_handle")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefDetail>>> GetLoanBriefHandle(int telesale, DateTime dateTime)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefDetail>>();
            try
            {
                var query = unitOfWork.LoanBriefRepository.Query(x => x.BoundTelesaleId == telesale && x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done &&
                        ((x.LoanBriefCancelAt.HasValue && x.LoanBriefCancelBy == telesale && x.Status == (int)EnumLoanStatus.CANCELED
                            && DateTime.Compare(x.LoanBriefCancelAt.Value.Date, dateTime.Date) == 0)
                        || (x.TelesalesPushAt.HasValue && x.BoundTelesaleId == telesale && x.Status != (int)EnumLoanStatus.CANCELED
                                && x.Status != (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN && x.Status != (int)EnumLoanStatus.TELESALE_ADVICE
                                && DateTime.Compare(x.TelesalesPushAt.Value.Date, dateTime.Date) == 0)), null, false)
                            .Select(LoanBriefDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBriefHandle Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_loanbrief_processing_by_phone")]
        public ActionResult<DefaultResponse<Meta, LoanBriefDetail>> GetLoanBriefByPhone(string Phone)
        {
            var def = new DefaultResponse<Meta, LoanBriefDetail>();
            try
            {
                var data = unitOfWork.LoanBriefRepository.Query(x => (x.Status == (int)EnumLoanStatus.TELESALE_ADVICE
                || x.Status == (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN) && x.Phone == Phone, null, false).Select(LoanBriefDetail.ProjectionDetail).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBriefByPhone Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("get_loanbrief_processing")]
        public ActionResult<DefaultResponse<Meta, LoanBriefDetail>> GetLoanBriefProcessinge(string Phone, string NationalCard, int LoanBriefId)
        {
            var def = new DefaultResponse<Meta, LoanBriefDetail>();
            try
            {
                if (LoanBriefId > 0)
                {
                    var data = unitOfWork.LoanBriefRepository.Query(x => x.Status != (int)EnumLoanStatus.FINISH
                && x.Status != (int)EnumLoanStatus.CANCELED && (x.Phone == Phone || (!string.IsNullOrEmpty(NationalCard) && x.NationalCard == NationalCard)) && x.LoanBriefId != LoanBriefId, null, false).OrderByDescending(x => x.CreatedTime).Select(LoanBriefDetail.ProjectionDetail).FirstOrDefault();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = data;
                }
                else
                {
                    var data = unitOfWork.LoanBriefRepository.Query(x => x.Status != (int)EnumLoanStatus.FINISH
                && x.Status != (int)EnumLoanStatus.CANCELED && (x.Phone == Phone || (!string.IsNullOrEmpty(NationalCard) && x.NationalCard == NationalCard)), null, false).OrderByDescending(x => x.CreatedTime).Select(LoanBriefDetail.ProjectionDetail).FirstOrDefault();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = data;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBriefProcessinge Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region for hub
        [HttpGet]
        [Route("get_data_hub")]
        public async Task<ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>>> GetDataForHub([FromQuery] int HubId = -1, [FromQuery] int page = 1,
            [FromQuery] int pageSize = 20, [FromQuery] int skip = -1, [FromQuery] int take = 20, [FromQuery] int LoanBriefId = -1, [FromQuery] string SearchName = null,
            [FromQuery] int HubEmployeeId = -1, [FromQuery] int LoanStatus = -1, [FromQuery] string field = "", [FromQuery] string sort = "",
            [FromQuery] string ListLoanStatus = "", [FromQuery] int HubLoanStatusChild = -1, [FromQuery] int LoanStatusDetail = -1)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefDetail>>();
            try
            {
                List<int> ListStatus = null;
                if (!string.IsNullOrEmpty(ListLoanStatus))
                    ListStatus = ListLoanStatus.Split(",").Select<string, int>(int.Parse).ToList();

                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done);
                if (HubId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.HubId == HubId));
                if (LoanBriefId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LoanBriefId == LoanBriefId));
                if (HubEmployeeId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.HubEmployeeId == HubEmployeeId));

                if (!string.IsNullOrEmpty(SearchName) && LoanBriefId <= 0)
                {
                    var textSearch = SearchName.Trim();
                    if (Common.Extensions.ConvertExtensions.IsNumber(textSearch)) //Nếu là số
                    {
                        if (textSearch.Length == (int)NationalCardLength.CMND_QD || textSearch.Length == (int)NationalCardLength.CMND || textSearch.Length == (int)NationalCardLength.CCCD)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.NationalCard == textSearch || x.NationCardPlace == textSearch)));
                        else//search phone
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Phone == textSearch || x.PhoneOther == textSearch)));
                    }
                    else//nếu là text =>search tên
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.FullName.Contains(SearchName)));
                }

                //D/S đơn remarketing autocall
                if (ListLoanStatus == "200")
                {
                    //lấy ra các đơn chứa UtmSource = form_remkt_hub
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW
                    || x.Status == (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING || x.Status == (int)EnumLoanStatus.HUB_LOAN_DISTRIBUTING
                    || x.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE)
                    && x.UtmSource == "form_remkt_hub"));
                }
                //D/s đơn  chưa xử lý của chuyên viên hub
                else if (ListLoanStatus == "-40")
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => ((ListStatus.Contains(x.Status.Value) || ListStatus == null)
                    && (x.TelesalesPushAt.HasValue && !x.FirstTimeStaffHubFeedback.HasValue && DateTime.Now > x.TelesalesPushAt.Value.AddMinutes(15)))));
                }
                //DS đơn ở trạng thái chờ thẩm định
                else if (LoanStatus == (int)EnumLoanStatus.APPRAISER_REVIEW)
                {
                    if (LoanStatusDetail == EnumLoanStatusDetailHub.LeadCLH.GetHashCode())
                    {
                        //Lấy ra tất cả các đơn không có chứa UtmSource = form_remkt_hub
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW
                            && x.LoanStatusDetail.GetValueOrDefault(0) == 0 && x.UtmSource != "form_remkt_hub"));
                    }
                    else
                    {
                        if (LoanStatusDetail > 0)
                        {
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW
                              && x.LoanStatusDetail.GetValueOrDefault(0) == LoanStatusDetail));
                            if (HubLoanStatusChild > 0)
                                filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LoanStatusDetailChild.GetValueOrDefault(0) == HubLoanStatusChild));
                        }
                        else
                        {
                            //chờ cvkd xử lý
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == (int)EnumLoanStatus.APPRAISER_REVIEW && x.UtmSource != "form_remkt_hub"));
                        }
                    }
                }
                else
                {
                    //Search tất cả của ManageHub
                    //Lấy ra tất cả các đơn không có chứa UtmSource = form_remkt_hub
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (ListStatus.Contains(x.Status.Value) || ListStatus == null) && x.UtmSource != "form_remkt_hub"));
                }
                //filter data by user              
                //var isCheckStatus = true;
                //if (ListStatus != null && ListStatus.Count > 0)
                //    isCheckStatus = false;
                //var filterService = new FilterServices<LoanBrief>(unitOfWork);
                //filterService.GetFilter(GetUserId(), ref filter, isCheckStatus);

                var query = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefDetail.ProjectionHubDetail);
                var totalRecords = await query.CountAsync();
                List<LoanBriefDetail> data;
                if (skip == -1)
                {
                    data = await query.OrderByDescending(x => x.TelesalesPushAt).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                    //if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(sort))
                    //{
                    //    data = await query.OrderBy(field + " " + sort).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                    //}
                    //else
                    //{
                    //    data = await query.OrderByDescending(x => x.TelesalesPushAt)
                    //      //.ThenBy(x => x.LoanAmount)
                    //      //.ThenByDescending(x => x.FirstProcessingTime ?? DateTime.MaxValue)
                    //      .Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                    //}
                }

                else
                {
                    data = await query.OrderByDescending(x => x.TelesalesPushAt).Skip(skip).Take(take).ToListAsync();
                    pageSize = take;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetDataForHub Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("update_hub")]
        public ActionResult<DefaultResponse<Meta>> UpdateShopId([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.LoanBriefId <= 0 || entity.HubId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (loanInfo != null)
                {
                    loanInfo.HubId = entity.HubId;
                    unitOfWork.LoanBriefRepository.Update(loanInfo);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = loanInfo.LoanBriefId;
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateShopId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_file")]
        public ActionResult<DefaultResponse<object>> AddLoanBriefFile([FromBody] LoanBriefFiles entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LoanBriefFileRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddLoanBriefFile Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_file")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefFileDetail>>> GetLoanBriefFile(int LoanBriefId, int documentType)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefFileDetail>>();
            try
            {
                var data = unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == LoanBriefId && x.TypeId == documentType && x.Status == 1 && x.IsDeleted != 1, null, false)
                    .Select(LoanBriefFileDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBriefFile Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("file_by_id")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefFileDetail>>> GetFileById(int loanBriefId)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefFileDetail>>();
            try
            {
                var data = unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanBriefId && x.Status == 1 && x.IsDeleted != 1, null, false).Select(LoanBriefFileDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefFile/GetFileById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpDelete]
        [Route("delete_file/{id}")]
        public ActionResult<DefaultResponse<dynamic>> DeleteLoanBriefFile(int LoanBriefFileId)
        {
            var def = new DefaultResponse<dynamic>();
            try
            {
                var data = unitOfWork.LoanBriefFileRepository.GetById(LoanBriefFileId);
                if (data != null)
                {
                    data.IsDeleted = 1;
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = data.Id;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/DeleteLoanBriefFile Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("distributing_staff_hub")]
        public ActionResult<DefaultResponse<object>> DistributingForStaffHub([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                loanbrief.PipelineState = entity.PipelineState;
                loanbrief.ActionState = entity.ActionState;
                loanbrief.InProcess = EnumInProcess.Process.GetHashCode();
                //loanbrief.
                loanbrief.HubEmployeeId = entity.HubEmployeeId;
                if (entity.EvaluationHomeUserId > 0)
                    loanbrief.EvaluationHomeUserId = entity.EvaluationHomeUserId;
                loanbrief.EvaluationHomeState = entity.EvaluationHomeState;
                if (entity.EvaluationCompanyUserId > 0)
                    loanbrief.EvaluationCompanyUserId = entity.EvaluationCompanyUserId;
                loanbrief.EvaluationCompanyState = entity.EvaluationCompanyState;
                unitOfWork.LoanBriefRepository.Update(loanbrief);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = loanbrief.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/DistributingForStaffHub Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        #endregion

        #region add device

        [HttpPost]
        [Route("update_device")]
        public ActionResult<DefaultResponse<Meta>> UpdateDevice([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.LoanBriefId <= 0 || string.IsNullOrEmpty(entity.DeviceId))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (loanInfo != null)
                {
                    loanInfo.DeviceId = entity.DeviceId;
                    loanInfo.DeviceStatus = entity.DeviceStatus;
                    if (entity.LastSendRequestActive.HasValue)
                        loanInfo.LastSendRequestActive = entity.LastSendRequestActive;
                    if (!string.IsNullOrEmpty(loanInfo.ContractGinno) && !string.IsNullOrEmpty(entity.ContractGinno))
                        loanInfo.ContractGinno = entity.ContractGinno;
                    unitOfWork.LoanBriefRepository.Update(loanInfo);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = loanInfo.LoanBriefId;
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateDevice Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);

        }
        [HttpPost]
        [Route("re_active_device")]
        public ActionResult<DefaultResponse<Meta>> ReActiveDevice([FromBody] ReActiveReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.LoanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (unitOfWork.LoanBriefRepository.Any(x => x.LoanBriefId == entity.LoanbriefId))
                {
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanbriefId, x => new LoanBrief()
                    {
                        DeviceStatus = (int)StatusOfDeviceID.OpenContract
                    });
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ReActiveDevice Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        [HttpPost]
        [Route("re_get_location")]
        public ActionResult<DefaultResponse<object>> ReGetLocationSim([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var objRequest = new LogLoanInfoAi()
                {
                    LoanbriefId = entity.LoanBriefId,
                    ServiceType = ServiceTypeAI.Location.GetHashCode(),
                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest,
                    CreatedAt = DateTime.Now
                };
                unitOfWork.LogLoanInfoAiRepository.Insert(objRequest);
                unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                {
                    ResultLocation = string.Empty
                });
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = objRequest.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ReGetLocationSim POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        //[Authorize]
        [Route("add_schedule_time")]
        public ActionResult<DefaultResponse<Meta>> AddScheduleTime([FromBody] ScheduleTimeReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LoanBriefId <= 0 || req.LoanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.LoanBriefId);
                if (loanInfo != null)
                {
                    // update to Time call
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief()
                    {
                        ScheduleTime = DateTime.ParseExact(req.DateTimeCall, "dd/MM/yyyy HH:mm", null),
                        LoanBriefComment = loanInfo.LoanBriefComment + Environment.NewLine + req.Comment + ":" + req.DateTimeCall
                    });

                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Loanbrief/AddScheduleTime Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_reloan")]
        public ActionResult<DefaultResponse<object>> AddReLoan([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {

                var obj = new LoanBrief()
                {
                    IsReborrow = true,
                    ReMarketingLoanBriefId = entity.LoanBriefId,
                    LoanAmount = entity.LoanAmount,
                    LoanAmountFirst = entity.LoanAmount,
                    LoanTime = entity.LoanTime,
                    ProductId = entity.ProductId,
                    CreatedTime = DateTime.Now,
                    FullName = entity.Customer.FullName,
                    Phone = entity.Customer.Phone,
                    Dob = entity.Customer.Dob,
                    Gender = entity.Customer.Gender,
                    NationalCard = entity.Customer.NationalCard,
                    NationalCardDate = entity.Customer.NationalCardDate,
                    NationCardPlace = entity.Customer.NationCardPlace,
                    ProvinceId = entity.LoanBriefResident.ProvinceId,
                    DistrictId = entity.LoanBriefResident.DistrictId,
                    WardId = entity.LoanBriefResident.WardId,
                    CustomerId = entity.Customer.CustomerId,
                    TypeLoanBrief = entity.TypeLoanBrief,
                    CreateBy = entity.CreateBy,
                    HubId = entity.HubId,
                    BoundTelesaleId = entity.BoundTelesaleId,
                    IsHeadOffice = entity.IsHeadOffice,
                    Status = EnumLoanStatus.INIT.GetHashCode()
                };
                //insert to db
                unitOfWork.LoanBriefRepository.Insert(obj);
                unitOfWork.Save();
                entity.LoanBriefId = obj.LoanBriefId;

                //xử lý thông tin customer
                var customerOld = unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.CustomerId == entity.Customer.CustomerId);
                if (customerOld != null && customerOld.CustomerId > 0)
                {
                    //cập nhật thông tin customer
                    Ultility.CopyObject(entity.Customer, ref customerOld, true);
                    customerOld.UpdatedAt = DateTime.Now;
                    customerOld.JobId = entity.LoanBriefJob.JobId;
                    customerOld.CompanyName = entity.LoanBriefJob.CompanyName;
                    customerOld.CompanyAddress = entity.LoanBriefJob.CompanyAddress;
                    customerOld.ProvinceId = entity.LoanBriefResident.ProvinceId;
                    customerOld.DistrictId = entity.LoanBriefResident.DistrictId;
                    customerOld.WardId = entity.LoanBriefResident.WardId;
                    customerOld.Address = entity.LoanBriefResident.Address;
                    entity.Customer = customerOld;
                    unitOfWork.CustomerRepository.Update(customerOld);
                }

                //nếu là đơn doanh nghiệp => lưu thông tin doanh nghiệp
                if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    entity.LoanBriefCompany.LoanBriefId = entity.LoanBriefId;
                    unitOfWork.LoanBriefCompanyRepository.Insert(entity.LoanBriefCompany);

                }
                //Thêm thông tin địa chỉ đang ở vào bảng LoanBriefResident

                if (entity.LoanBriefResident != null)
                {
                    entity.LoanBriefResident.LoanBriefResidentId = entity.LoanBriefId;
                    unitOfWork.LoanBriefResidentRepository.Insert(entity.LoanBriefResident);
                }

                //Thêm thông tin địa chỉ hộ khẩu LoanBriefHousehold

                if (entity.LoanBriefHousehold != null)
                {
                    entity.LoanBriefHousehold.LoanBriefHouseholdId = entity.LoanBriefId;
                    unitOfWork.LoanBriefHouseholdRepository.Insert(entity.LoanBriefHousehold);
                }

                //Thêm thông tin công việc

                if (entity.LoanBriefJob != null)
                {
                    entity.LoanBriefJob.LoanBriefJobId = entity.LoanBriefId;
                    unitOfWork.LoanBriefJobRepository.Insert(entity.LoanBriefJob);
                }

                //Thêm thông tin tài sản

                if (entity.LoanBriefProperty != null)
                {
                    entity.LoanBriefProperty.LoanBriefPropertyId = entity.LoanBriefId;
                    unitOfWork.LoanBriefPropertyRepository.Insert(entity.LoanBriefProperty);
                }

                //xử lý documment
                if (entity.LoanBriefDocument != null && entity.LoanBriefDocument.Count > 0)
                {
                    var oldData = unitOfWork.LoanBriefDocumentRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false).ToList();
                    if (oldData != null && oldData.Count > 0)
                    {
                        foreach (var oldItem in oldData)
                            unitOfWork.LoanBriefDocumentRepository.Delete(oldItem.LoanBriefDocumentId);
                        unitOfWork.Save();
                    }

                    foreach (var item in entity.LoanBriefDocument.ToList())
                    {
                        if (item.LoanBriefDocumentId == 0)
                            unitOfWork.LoanBriefDocumentRepository.Insert(new LoanBriefDocument
                            {
                                LoanBriefId = entity.LoanBriefId,
                                DocumentId = item.DocumentId
                            });
                    }
                }
                //Thêm tin đồng nghiệp
                if (entity.LoanBriefRelationship != null && entity.LoanBriefRelationship.Count > 0)
                {
                    foreach (var item in entity.LoanBriefRelationship)
                    {
                        if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                        {
                            item.LoanBriefId = entity.LoanBriefId;
                            if (item.Id > 0)
                                unitOfWork.LoanBriefRelationshipRepository.Update(item);
                            else
                                unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                        }
                    }
                }

                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
            }
            catch (Exception ex)
            {
                unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBrief/AddReLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("save_request_ai/{id}")]
        public ActionResult<DefaultResponse<object>> SaveAndRequestAI(int id, [FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != entity.LoanBriefId || entity.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                loanbrief.Customer = entity.Customer;
                loanbrief.LoanBriefCompany = entity.LoanBriefCompany;
                loanbrief.LoanBriefResident = entity.LoanBriefResident;
                loanbrief.LoanBriefHousehold = entity.LoanBriefHousehold;
                loanbrief.LoanBriefRelationship = entity.LoanBriefRelationship;
                loanbrief.LoanBriefProperty = entity.LoanBriefProperty;
                loanbrief.LoanBriefJob = entity.LoanBriefJob;
                loanbrief.LoanBriefDocument = entity.LoanBriefDocument;
                unitOfWork.BeginTransaction();
                //xử lý thông tin customer
                if (unitOfWork.CustomerRepository.Any(x => x.CustomerId == entity.Customer.CustomerId))
                {
                    //cập nhật thông tin customer
                    unitOfWork.CustomerRepository.Update(x => x.CustomerId == entity.Customer.CustomerId, x => new Customer()
                    {
                        UpdatedAt = DateTime.Now,
                        JobId = entity.LoanBriefJob.JobId,
                        CompanyName = entity.LoanBriefJob.CompanyName,
                        CompanyAddress = entity.LoanBriefJob.CompanyAddress,
                        ProvinceId = entity.LoanBriefResident.ProvinceId,
                        DistrictId = entity.LoanBriefResident.DistrictId,
                        WardId = entity.LoanBriefResident.WardId,
                        Address = entity.LoanBriefResident.Address
                    });
                }

                //Cập nhật thông tin địa chỉ đang ở vào bảng LoanBriefResident
                if (loanbrief.LoanBriefResident.LoanBriefResidentId > 0
                    && unitOfWork.LoanBriefResidentRepository.Any(x => x.LoanBriefResidentId == loanbrief.LoanBriefResident.LoanBriefResidentId))
                {
                    unitOfWork.LoanBriefResidentRepository.Update(loanbrief.LoanBriefResident);
                }
                else
                {
                    loanbrief.LoanBriefResident.LoanBriefResidentId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefResidentRepository.Insert(loanbrief.LoanBriefResident);
                }

                ////Cập nhật thông tin địa chỉ hộ khẩu LoanBriefHousehold
                if (loanbrief.LoanBriefHousehold.LoanBriefHouseholdId > 0
                    && unitOfWork.LoanBriefHouseholdRepository.Any(x => x.LoanBriefHouseholdId == loanbrief.LoanBriefHousehold.LoanBriefHouseholdId))
                {
                    unitOfWork.LoanBriefHouseholdRepository.Update(loanbrief.LoanBriefHousehold);
                }
                else
                {
                    loanbrief.LoanBriefHousehold.LoanBriefHouseholdId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefHouseholdRepository.Insert(loanbrief.LoanBriefHousehold);
                }

                ////cập nhật thông tin công việc
                if (loanbrief.LoanBriefJob.LoanBriefJobId > 0
                    && unitOfWork.LoanBriefJobRepository.Any(x => x.LoanBriefJobId == loanbrief.LoanBriefJob.LoanBriefJobId))
                    unitOfWork.LoanBriefJobRepository.Update(loanbrief.LoanBriefJob);
                else
                {
                    loanbrief.LoanBriefJob.LoanBriefJobId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefJobRepository.Insert(loanbrief.LoanBriefJob);
                }
                if (loanbrief.LoanBriefProperty.LoanBriefPropertyId > 0
                    && unitOfWork.LoanBriefPropertyRepository.Any(x => x.LoanBriefPropertyId == loanbrief.LoanBriefProperty.LoanBriefPropertyId))
                    unitOfWork.LoanBriefPropertyRepository.Update(loanbrief.LoanBriefProperty);
                else
                {
                    loanbrief.LoanBriefProperty.LoanBriefPropertyId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefPropertyRepository.Insert(loanbrief.LoanBriefProperty);
                }
                //xử lý documment
                if (entity.LoanBriefDocument != null && entity.LoanBriefDocument.Count > 0)
                {
                    var oldData = unitOfWork.LoanBriefDocumentRepository.Query(x => x.LoanBriefId == loanbrief.LoanBriefId, null, false).ToList();
                    if (oldData != null && oldData.Count > 0)
                    {
                        foreach (var oldItem in oldData)
                            unitOfWork.LoanBriefDocumentRepository.Delete(oldItem.LoanBriefDocumentId);
                        unitOfWork.Save();
                    }

                    foreach (var item in entity.LoanBriefDocument.ToList())
                    {
                        if (item.LoanBriefDocumentId == 0)
                            unitOfWork.LoanBriefDocumentRepository.Insert(new LoanBriefDocument
                            {
                                LoanBriefId = loanbrief.LoanBriefId,
                                DocumentId = item.DocumentId
                            });
                    }
                }
                //Lưu thông tin đồng nghiệp
                if (loanbrief.LoanBriefRelationship != null && loanbrief.LoanBriefRelationship.Count > 0)
                {
                    foreach (var item in entity.LoanBriefRelationship)
                    {
                        if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                        {
                            item.LoanBriefId = entity.LoanBriefId;
                            if (item.Id > 0)
                                unitOfWork.LoanBriefRelationshipRepository.Update(item);
                            else
                                unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                        }
                    }
                }

                if (unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == loanbrief.LoanBriefId))
                {
                    //xóa dữ liệu cũ
                    unitOfWork.LoanBriefQuestionScriptRepository.Delete(x => x.LoanBriefId == loanbrief.LoanBriefId);
                }
                unitOfWork.LoanBriefQuestionScriptRepository.Insert(entity.LoanBriefQuestionScript);


                #region Xử lý map + insert LoanBrief
                entity.FullName = entity.Customer.FullName;
                entity.Dob = entity.Customer.Dob;
                entity.Gender = entity.Customer.Gender;
                entity.NationalCard = entity.Customer.NationalCard;
                entity.Passport = entity.Customer.Passport;
                entity.NationalCardDate = entity.Customer.NationalCardDate;
                entity.NationCardPlace = entity.Customer.NationCardPlace;
                entity.ProvinceId = entity.LoanBriefResident.ProvinceId;
                entity.DistrictId = entity.LoanBriefResident.DistrictId;
                entity.WardId = entity.LoanBriefResident.WardId;
                entity.UpdatedTime = DateTime.Now;
                Ultility.CopyObject(entity, ref loanbrief, false);
                loanbrief.UpdatedTime = DateTime.Now;
                if (loanbrief.LoanTime > 0)
                    loanbrief.ToDate = loanbrief.FromDate.Value.AddMonths((int)loanbrief.LoanTime);
                //var context = unitOfWork.LoanBriefRepository.GetConnect();
                unitOfWork.LoanBriefRepository.Update(loanbrief);
                unitOfWork.SaveChanges();
                #endregion
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
                unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBrief/SaveAndRequestAI PUT Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPut, HttpPost]
        [Route("update_first_hub_feedback")]
        public ActionResult<DefaultResponse<object>> UpdateFirstHubFeedback([FromBody] HubFeedbackReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (!loanbrief.FirstTimeHubFeedBack.HasValue)
                    {
                        var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                        var sql = new StringBuilder();
                        sql.AppendFormat("update LoanBrief set FirstTimeHubFeedBack = '{0}' ", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt"));
                        sql.AppendFormat("where LoanBriefId ={0}", entity.LoanBriefId);
                        db.Execute(sql.ToString());
                        //unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                        //{
                        //    FirstTimeHubFeedBack = DateTime.Now
                        //});
                    }
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = loanbrief.LoanBriefId;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateFirstHubFeedback PUT Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("update_first_telesale_feedback")]
        public ActionResult<DefaultResponse<object>> UpdateFirstTelesaleFeedback([FromBody] HubFeedbackReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                    var sql = new StringBuilder();
                    sql.AppendFormat("update LoanBrief set First_ProcessingTime = '{0}' ", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt"));
                    sql.AppendFormat("where LoanBriefId ={0}", entity.LoanBriefId);
                    db.Execute(sql.ToString());

                    //unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                    //{
                    //    FirstProcessingTime = DateTime.Now
                    //});
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = loanbrief.LoanBriefId;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateFirstTelesaleFeedback PUT Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("update_last_telesale_feedback")]
        public ActionResult<DefaultResponse<object>> UpdateLastTelesaleFeedback([FromBody] HubFeedbackReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                    var sql = new StringBuilder();
                    sql.AppendFormat("update LoanBrief set LastChangeStatusOfTelesale = '{0}' ", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt"));
                    sql.AppendFormat("where LoanBriefId ={0}", entity.LoanBriefId);
                    db.Execute(sql.ToString());

                    //unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                    //{
                    //    LastChangeStatusOfTelesale = DateTime.Now
                    //});
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = loanbrief.LoanBriefId;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateLastTelesaleFeedback PUT Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("update_first_staff_hub_feedback")]
        public ActionResult<DefaultResponse<object>> UpdateFirstStaffHubFeedback([FromBody] HubFeedbackReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendFormat("update LoanBrief set FirstTimeStaffHubFeedback = '{0}' ", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt"));
                sql.AppendFormat("where LoanBriefId ={0}", entity.LoanBriefId);
                db.Execute(sql.ToString());
                //unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                //{
                //    FirstTimeStaffHubFeedback = DateTime.Now
                //});
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateFirstStaffHubFeedback PUT Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("save_refphone_isnull_ai")]
        public ActionResult<DefaultResponse<object>> SaveRefPhoneIsNullRequestAI([FromBody] Models.Request.UpdateRefPhoneLoanBriefRelationship entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (entity.LoanBriefRelationshipId == 0)
                    {
                        unitOfWork.LoanBriefRelationshipRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBriefRelationship()
                        {
                            RefPhoneCallRate = null,
                            RefPhoneDurationRate = null
                        });
                        unitOfWork.Save();
                    }
                    else
                    {
                        unitOfWork.LoanBriefRelationshipRepository.Update(x => x.LoanBriefId == entity.LoanBriefId && x.Id == entity.LoanBriefRelationshipId, x => new LoanBriefRelationship()
                        {
                            RefPhoneCallRate = null,
                            RefPhoneDurationRate = null
                        });
                        unitOfWork.Save();
                    }
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = entity.LoanBriefId;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/SaveRefPhoneIsNullRequestAI PUT Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("update_countcall")]
        public ActionResult<DefaultResponse<object>> UpdateCountCall(int loanBriefId)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //var callcount = 1;
                //var loanbrief = unitOfWork.LoanBriefRepository.GetById(loanBriefId);
                //if (loanbrief.CountCall > 0)
                //    callcount = loanbrief.CountCall.Value + 1;

                unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanBriefId, x => new LoanBrief
                {
                    CountCall = x.CountCall.HasValue ? x.CountCall.Value + 1 : 1
                });
                //unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = loanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateCountCall Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("count_loan_tls")]
        public ActionResult<DefaultResponse<Meta, CountLoanTLSDetail>> CountLoanTLS(int TelesaleId, string teamTelesales)
        {
            var def = new DefaultResponse<Meta, CountLoanTLSDetail>();
            try
            {
                var now = DateTimeOffset.Now;
                var startDay = new DateTimeOffset(now.Year, now.Month, now.Day, 00, 00, 01, new TimeSpan(7, 0, 0));
                var startTime = new DateTimeOffset(now.Year, now.Month, now.Day, 08, 30, 00, new TimeSpan(7, 0, 0));
                var endTime = new DateTimeOffset(now.Year, now.Month, now.Day, 11, 55, 00, new TimeSpan(7, 0, 0));
                var startTime2 = new DateTimeOffset(now.Year, now.Month, now.Day, 13, 15, 00, new TimeSpan(7, 0, 0));
                var endTime2 = new DateTimeOffset(now.Year, now.Month, now.Day, 17, 30, 00, new TimeSpan(7, 0, 0));
                var startTime3 = new DateTimeOffset(now.Year, now.Month, now.Day, 18, 00, 00, new TimeSpan(7, 0, 0));
                var endTime3 = new DateTimeOffset(now.Year, now.Month, now.Day, 20, 15, 00, new TimeSpan(7, 0, 0));

                List<int> lstTeamTelesalesId = new List<int>();
                if (!string.IsNullOrEmpty(teamTelesales))
                    lstTeamTelesalesId = teamTelesales.Split(',').Select(Int32.Parse).ToList();

                var lstInDay = unitOfWork.LoanBriefRepository
                    .Query(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done &&
                     lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value) && (x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()
                    || x.Status == EnumLoanStatus.CANCELED.GetHashCode()) && (x.BoundTelesaleId == TelesaleId || TelesaleId == -1) && x.CreatedTime > startDay, null, false).Select(LoanBriefDetail.ProjectionDetail);

                var lstByStatus = unitOfWork.LoanBriefRepository
                    .Query(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done
                    && (x.BoundTelesaleId == TelesaleId || TelesaleId == -1)
                    && lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value)
                    && (x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()
                    || x.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode()), null, false).Select(LoanBriefDetail.ProjectionDetail);

                var obj = new CountLoanTLSDetail
                {
                    TLS_WaitAdvice = lstByStatus.Where(x => !x.StatusTelesales.HasValue && x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()).Count(),
                    TLS_WaitPREPARE = lstByStatus.Where(x => x.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode()).Count(),
                    TLS_Cancel = lstInDay.Where(x => x.Status == EnumLoanStatus.CANCELED.GetHashCode()).Count(),
                    TotalLoanbrief = lstByStatus.Count(),
                    AutoCall = lstByStatus.Count(x => x.Step > 0 && x.PlatformType != (int)EnumPlatformType.Ai && x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()),
                    Chatbot = lstByStatus.Count(x => x.PlatformType == (int)EnumPlatformType.Ai && x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()),
                    HotLeadNoResponse = lstInDay.Where(x => x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode() && !x.FirstProcessingTime.HasValue
                    && ((x.CreatedTime >= startTime && x.CreatedTime < endTime) || (x.CreatedTime >= startTime2 && x.CreatedTime < endTime2) || (x.CreatedTime >= startTime3 && x.CreatedTime < endTime3))).Count(),
                    HotLeadResponded = lstInDay.Where(x => x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode() && x.FirstProcessingTime.HasValue
                    && ((x.CreatedTime >= startTime && x.CreatedTime < endTime) || (x.CreatedTime >= startTime2 && x.CreatedTime < endTime2) || (x.CreatedTime >= startTime3 && x.CreatedTime < endTime3))).Count()
                };
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/CountLoanTLS Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpGet]
        [Route("detail/{id}")]
        public ActionResult<DefaultResponse<Meta, LoanBriefDetailNew>> GetByIdNew(int id)
        {
            var def = new DefaultResponse<Meta, LoanBriefDetailNew>();
            try
            {
                var data = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == id, null, false).Select(LoanBriefDetailNew.ProjectionViewDetail).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetByIdNew Exception");
            }
            return Ok(def);
        }
        [HttpPost]
        [Route("update_status_device")]
        public ActionResult<DefaultResponse<Meta>> UpdateStatusDevice([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.LoanBriefId <= 0 || string.IsNullOrEmpty(entity.DeviceId))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (loanInfo != null)
                {
                    loanInfo.DeviceStatus = entity.DeviceStatus;
                    unitOfWork.LoanBriefRepository.Update(loanInfo);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = loanInfo.LoanBriefId;
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateStatusDevice Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("delete_file")]
        public ActionResult<DefaultResponse<Meta>> DeleteFile([FromBody] RemoveFileItem entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.loanbriefId <= 0 || entity.fileId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                unitOfWork.LoanBriefFileRepository.Update(x => x.Id == entity.fileId && x.LoanBriefId == entity.loanbriefId, x => new LoanBriefFiles
                {
                    IsDeleted = 1,
                    DeletedBy = entity.userRemove,
                    ModifyAt = DateTime.Now
                });
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/DeleteFile Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("update_loanbrief_scriptv2/{id}")]
        public ActionResult<DefaultResponse<object>> UpdateLoanBriefScriptV2(int id, [FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                var userId = int.Parse(identity.Claims.Where(c => c.Type == "Id").Select(c => c.Value).SingleOrDefault());


                if (id != entity.LoanBriefId || entity.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanbrief = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false, x => x.Customer, x => x.LoanBriefCompany, x => x.LoanBriefHousehold
               , x => x.LoanBriefJob, x => x.LoanBriefProperty, x => x.LoanBriefResident, x => x.LoanBriefRelationship).FirstOrDefault();
                var loanbriefHistory = new LoanBriefHistory()
                {
                    LoanBriefId = entity.LoanBriefId,
                    OldValue = JsonConvert.SerializeObject(loanbrief, Formatting.Indented, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    }),
                    ActorId = EnumActor.UpdateScript.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    UserId = userId
                };
                loanbrief.Customer = entity.Customer;
                loanbrief.LoanBriefCompany = entity.LoanBriefCompany;
                loanbrief.LoanBriefResident = entity.LoanBriefResident;
                loanbrief.LoanBriefHousehold = entity.LoanBriefHousehold;
                loanbrief.LoanBriefRelationship = entity.LoanBriefRelationship;
                loanbrief.LoanBriefProperty = entity.LoanBriefProperty;
                loanbrief.LoanBriefJob = entity.LoanBriefJob;
                loanbrief.LoanBriefDocument = entity.LoanBriefDocument;
                loanbrief.LoanBriefQuestionScript = entity.LoanBriefQuestionScript;
                unitOfWork.BeginTransaction();
                //xử lý thông tin customer
                var customerOld = unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.CustomerId == entity.Customer.CustomerId);
                if (customerOld != null && customerOld.CustomerId > 0)
                {
                    //cập nhật thông tin customer
                    Ultility.CopyObject(loanbrief.Customer, ref customerOld, true);
                    customerOld.UpdatedAt = DateTimeOffset.Now;
                    customerOld.JobId = entity.LoanBriefJob.JobId;
                    customerOld.CompanyName = entity.LoanBriefJob.CompanyName;
                    customerOld.CompanyAddress = entity.LoanBriefJob.CompanyAddress;
                    customerOld.ProvinceId = entity.LoanBriefResident.ProvinceId;
                    customerOld.DistrictId = entity.LoanBriefResident.DistrictId;
                    customerOld.WardId = entity.LoanBriefResident.WardId;
                    customerOld.Address = entity.LoanBriefResident.Address;
                    entity.Customer = customerOld;
                    unitOfWork.CustomerRepository.Update(customerOld);
                }

                //nếu là đơn doanh nghiệp => lưu thông tin doanh nghiệp
                if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    if (loanbrief.LoanBriefCompany.LoanBriefId > 0)
                    {
                        loanbrief.LoanBriefCompany.LoanBriefId = entity.LoanBriefId;
                        unitOfWork.LoanBriefCompanyRepository.Update(loanbrief.LoanBriefCompany);
                    }
                    else
                    {
                        loanbrief.LoanBriefCompany.LoanBriefId = entity.LoanBriefId;
                        unitOfWork.LoanBriefCompanyRepository.Insert(loanbrief.LoanBriefCompany);
                    }
                }
                //Cập nhật thông tin địa chỉ đang ở vào bảng LoanBriefResident
                if (loanbrief.LoanBriefId > 0
                    && unitOfWork.LoanBriefResidentRepository.Any(x => x.LoanBriefResidentId == loanbrief.LoanBriefId))
                {
                    loanbrief.LoanBriefResident.LoanBriefResidentId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefResidentRepository.Update(loanbrief.LoanBriefResident);
                }
                else
                {
                    loanbrief.LoanBriefResident.LoanBriefResidentId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefResidentRepository.Insert(loanbrief.LoanBriefResident);
                }

                ////Cập nhật thông tin địa chỉ hộ khẩu LoanBriefHousehold
                if (loanbrief.LoanBriefId > 0
                    && unitOfWork.LoanBriefHouseholdRepository.Any(x => x.LoanBriefHouseholdId == loanbrief.LoanBriefId))
                {
                    loanbrief.LoanBriefHousehold.LoanBriefHouseholdId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefHouseholdRepository.Update(loanbrief.LoanBriefHousehold);
                }
                else
                {
                    loanbrief.LoanBriefHousehold.LoanBriefHouseholdId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefHouseholdRepository.Insert(loanbrief.LoanBriefHousehold);
                }

                ////cập nhật thông tin công việc
                if (loanbrief.LoanBriefId > 0
                    && unitOfWork.LoanBriefJobRepository.Any(x => x.LoanBriefJobId == loanbrief.LoanBriefId))
                {
                    loanbrief.LoanBriefJob.LoanBriefJobId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefJobRepository.Update(loanbrief.LoanBriefJob);
                }
                else
                {
                    loanbrief.LoanBriefJob.LoanBriefJobId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefJobRepository.Insert(loanbrief.LoanBriefJob);
                }
                //cập nhật thông tin tài sản
                if (loanbrief.LoanBriefId > 0
                     && unitOfWork.LoanBriefPropertyRepository.Any(x => x.LoanBriefPropertyId == loanbrief.LoanBriefId))
                {
                    loanbrief.LoanBriefProperty.LoanBriefPropertyId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefPropertyRepository.Update(loanbrief.LoanBriefProperty);
                }

                else
                {
                    loanbrief.LoanBriefProperty.LoanBriefPropertyId = loanbrief.LoanBriefId;
                    unitOfWork.LoanBriefPropertyRepository.Insert(loanbrief.LoanBriefProperty);
                }
                //xử lý documment
                if (entity.LoanBriefDocument != null && entity.LoanBriefDocument.Count > 0)
                {
                    var oldData = unitOfWork.LoanBriefDocumentRepository.Any(x => x.LoanBriefId == loanbrief.LoanBriefId);
                    if (oldData)
                    {
                        unitOfWork.LoanBriefDocumentRepository.Delete(x => x.LoanBriefId == loanbrief.LoanBriefId);
                        unitOfWork.Save();
                    }

                    foreach (var item in entity.LoanBriefDocument.ToList())
                    {
                        if (item.LoanBriefDocumentId == 0)
                            unitOfWork.LoanBriefDocumentRepository.Insert(new LoanBriefDocument
                            {
                                LoanBriefId = loanbrief.LoanBriefId,
                                DocumentId = item.DocumentId
                            });
                    }
                }
                //Lưu thông tin đồng nghiệp
                if (loanbrief.LoanBriefRelationship != null && loanbrief.LoanBriefRelationship.Count > 0)
                {
                    foreach (var item in entity.LoanBriefRelationship)
                    {
                        if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                        {
                            item.LoanBriefId = entity.LoanBriefId;
                            if (item.Id > 0)
                                unitOfWork.LoanBriefRelationshipRepository.Update(item);
                            else
                                unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                        }
                    }
                }

                //xử lý thông tin của bảng kịch bản
                if (loanbrief.LoanBriefQuestionScript != null)
                {
                    if (unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == loanbrief.LoanBriefId))
                    {
                        //xóa dữ liệu cũ
                        unitOfWork.LoanBriefQuestionScriptRepository.Delete(x => x.LoanBriefId == loanbrief.LoanBriefId);
                        unitOfWork.Save();
                    }
                    unitOfWork.LoanBriefQuestionScriptRepository.Insert(entity.LoanBriefQuestionScript);
                }

                #region Xử lý map + insert LoanBrief
                entity.FullName = entity.Customer.FullName;
                entity.Dob = entity.Customer.Dob;
                entity.Gender = entity.Customer.Gender;
                entity.NationalCard = entity.Customer.NationalCard;
                entity.Passport = entity.Customer.Passport;
                entity.NationalCardDate = entity.Customer.NationalCardDate;
                entity.NationCardPlace = entity.Customer.NationCardPlace;
                entity.ProvinceId = entity.LoanBriefResident.ProvinceId;
                entity.DistrictId = entity.LoanBriefResident.DistrictId;
                entity.WardId = entity.LoanBriefResident.WardId;
                entity.UpdatedTime = DateTimeOffset.Now;
                Ultility.CopyObject(entity, ref loanbrief, false);
                loanbrief.UpdatedTime = DateTime.Now;
                if (loanbrief.LoanTime > 0)
                    loanbrief.ToDate = loanbrief.FromDate.Value.AddMonths((int)loanbrief.LoanTime);

                //check nếu đơn chưa xử lý lần đầu add thời gian xử lý
                if (loanbrief.FirstProcessingTime == null)
                    loanbrief.FirstProcessingTime = DateTime.Now;
                unitOfWork.LoanBriefRepository.Update(loanbrief);
                unitOfWork.Save();
                //lưu thông tin log
                loanbriefHistory.NewValue = JsonConvert.SerializeObject(loanbrief, Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                });
                unitOfWork.LoanBriefHistoryRepository.Insert(loanbriefHistory);
                unitOfWork.Save();

                #endregion
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
                unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBrief/UpdateLoanBriefScriptV2 Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("save_status_telesales")]
        public ActionResult<DefaultResponse<object>> SaveStatusTelesales([FromBody] DAL.Object.RequestSaveStatusTelesales entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (entity.LoanBriefId > 0)
                {
                    //cập nhập status telesales
                    var firstProcessingTime = loanbrief.FirstProcessingTime.HasValue ? loanbrief.FirstProcessingTime : DateTime.Now;
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                    {
                        StatusTelesales = entity.StatusTelesales,
                        DetailStatusTelesales = entity.StatusTelesalesDetail,
                        FirstProcessingTime = firstProcessingTime,
                        LastChangeStatusOfTelesale = DateTime.Now
                    });
                    unitOfWork.Save();

                    //Xử lý đơn vay
                    if (entity.StatusTelesales == (int)EnumStatusTelesales.WaitPrepareFile && loanbrief.Status == (int)EnumLoanStatus.TELESALE_ADVICE)
                    {
                        unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                        {
                            PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode(),
                            ActionState = EnumActionPush.Push.GetHashCode(),
                            InProcess = EnumInProcess.Process.GetHashCode()
                        });
                        unitOfWork.Save();
                    }

                    if (entity.CommentStatusTelesales != null)
                    {
                        // cập nhập comment vào bản loanbriefquestionscript
                        unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBriefQuestionScript()
                        {
                            CommentStatusTelesales = entity.CommentStatusTelesales
                        });
                        unitOfWork.Save();
                    }

                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
            }

            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/SaveStatusTelesales Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("update_cavet")]
        public ActionResult<DefaultResponse<object>> UpdateCavet(int loanbriefId)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (loanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(loanbriefId);
                if (loanInfo != null)
                {
                    if (loanInfo.ReCareLoanbrief == null)
                    {
                        unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbriefId, x => new LoanBrief()
                        {
                            ReCareLoanbrief = (int)EnumReCareLoanbrief.BorrowCavet
                        });
                    }
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateCavet Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_loan_topup")]
        public ActionResult<DefaultResponse<object>> AddLoanTopup(LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var obj = new LoanBrief()
                {
                    TypeRemarketing = (int)EnumTypeRemarketing.IsTopUp,
                    ReMarketingLoanBriefId = entity.LoanBriefId,
                    LoanAmount = entity.LoanAmount,
                    LoanAmountFirst = entity.LoanAmount,
                    LoanTime = entity.LoanTime,
                    ProductId = entity.ProductId,
                    CreatedTime = DateTime.Now,
                    FullName = entity.Customer.FullName,
                    Phone = entity.Customer.Phone,
                    Dob = entity.Customer.Dob,
                    Gender = entity.Customer.Gender,
                    NationalCard = entity.Customer.NationalCard,
                    NationalCardDate = entity.Customer.NationalCardDate,
                    NationCardPlace = entity.Customer.NationCardPlace,
                    ProvinceId = entity.LoanBriefResident.ProvinceId,
                    DistrictId = entity.LoanBriefResident.DistrictId,
                    WardId = entity.LoanBriefResident.WardId,
                    CustomerId = entity.Customer.CustomerId,
                    TypeLoanBrief = entity.TypeLoanBrief,
                    RateTypeId = entity.RateTypeId,
                    FromDate = entity.FromDate,
                    ToDate = entity.ToDate,
                    Status = entity.Status,
                    ReceivingMoneyType = entity.ReceivingMoneyType,
                    BankId = entity.BankId,
                    BankAccountName = entity.BankAccountName,
                    BankAccountNumber = entity.BankAccountNumber,
                    BankCardNumber = entity.BankCardNumber,
                    Frequency = entity.Frequency,
                    BuyInsurenceCustomer = entity.BuyInsurenceCustomer,
                    PlatformType = entity.PlatformType,
                    DeviceId = entity.DeviceId,
                    HubId = entity.HubId,
                    HubEmployeeId = entity.HubEmployeeId,
                    RateMoney = entity.RateMoney.HasValue ? entity.RateMoney.Value : 2700,
                    RatePercent = entity.RatePercent.HasValue ? entity.RatePercent.Value : 98.55,
                    LoanAmountExpertise = entity.LoanAmountExpertise,
                    LoanAmountExpertiseLast = entity.LoanAmountExpertiseLast,
                    IsTrackingLocation = entity.IsTrackingLocation,
                    CreateBy = entity.CreateBy,
                    BoundTelesaleId = entity.BoundTelesaleId,
                    IsLocate = entity.IsLocate,
                    IsHeadOffice = entity.IsHeadOffice,
                };

                //insert to db
                unitOfWork.LoanBriefRepository.Insert(obj);
                unitOfWork.Save();
                entity.LoanBriefId = obj.LoanBriefId;
                //nếu là đơn doanh nghiệp => lưu thông tin doanh nghiệp
                if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    entity.LoanBriefCompany.LoanBriefId = entity.LoanBriefId;
                    unitOfWork.LoanBriefCompanyRepository.Insert(entity.LoanBriefCompany);

                }
                //Thêm thông tin địa chỉ đang ở vào bảng LoanBriefResident
                if (entity.LoanBriefResident != null)
                {
                    entity.LoanBriefResident.LoanBriefResidentId = entity.LoanBriefId;
                    unitOfWork.LoanBriefResidentRepository.Insert(entity.LoanBriefResident);
                }
                //Thêm thông tin địa chỉ hộ khẩu LoanBriefHousehold
                if (entity.LoanBriefHousehold != null)
                {
                    entity.LoanBriefHousehold.LoanBriefHouseholdId = entity.LoanBriefId;
                    unitOfWork.LoanBriefHouseholdRepository.Insert(entity.LoanBriefHousehold);
                }
                //Thêm thông tin công việc
                if (entity.LoanBriefJob != null)
                {
                    entity.LoanBriefJob.LoanBriefJobId = entity.LoanBriefId;
                    unitOfWork.LoanBriefJobRepository.Insert(entity.LoanBriefJob);
                }
                //Thêm thông tin tài sản
                if (entity.LoanBriefProperty != null)
                {
                    entity.LoanBriefProperty.LoanBriefPropertyId = entity.LoanBriefId;
                    unitOfWork.LoanBriefPropertyRepository.Insert(entity.LoanBriefProperty);
                }
                //xử lý Files
                if (obj.ReMarketingLoanBriefId > 0 && (entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))
                {

                    List<int> lstDocumentId = new List<int>();
                    lstDocumentId.Add((int)EnumDocumentType.CMND_CCCD);
                    lstDocumentId.Add((int)EnumDocumentType.Zalo);
                    lstDocumentId.Add((int)EnumDocumentType.HoChieu_CVKD_Chup);
                    lstDocumentId.Add((int)EnumDocumentType.SDT_ThamChieu);
                    lstDocumentId.Add((int)EnumDocumentType.Video_TinNhanLuong);
                    lstDocumentId.Add((int)EnumDocumentType.HDLD_CoDauDoCongTy);
                    lstDocumentId.Add((int)EnumDocumentType.Motorbike_Registration_Certificate);
                    var files = unitOfWork.LoanBriefFileRepository.Query(x => lstDocumentId.Contains(x.TypeId.Value)
                                && x.LoanBriefId == obj.ReMarketingLoanBriefId
                                && x.Status == 1 && x.IsDeleted.GetValueOrDefault(0) == 0, null, false).ToList();
                    if (files != null && files.Count() > 0)
                    {
                        foreach (var file in files)
                        {
                            unitOfWork.LoanBriefFileRepository.Insert(new LoanBriefFiles
                            {
                                LoanBriefId = obj.LoanBriefId,
                                CreateAt = DateTime.Now,
                                FilePath = file.FilePath,
                                UserId = file.UserId,
                                Status = file.Status,
                                TypeFile = file.TypeFile,
                                S3status = file.S3status,
                                TypeId = file.TypeId,
                                IsDeleted = file.IsDeleted,
                                MecashId = file.MecashId,
                                FileThumb = file.FileThumb,
                                SourceUpload = file.SourceUpload
                            });
                        }
                    }

                }
                //Thêm tin đồng nghiệp
                if (entity.LoanBriefRelationship != null && entity.LoanBriefRelationship.Count > 0)
                {
                    foreach (var item in entity.LoanBriefRelationship)
                    {
                        item.Id = 0;
                        if (item.RelationshipType > 0 && !string.IsNullOrEmpty(item.Phone) && !string.IsNullOrEmpty(item.FullName))
                        {
                            item.LoanBriefId = entity.LoanBriefId;
                            unitOfWork.LoanBriefRelationshipRepository.Insert(item);
                        }
                    }
                }

                //update đơn gốc không cho tạo đơn topup nữa
                unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == obj.ReMarketingLoanBriefId, x => new LoanBrief()
                {
                    TypeLoanSupport = 0
                });
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddLoanTopup POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_loanbrief_by_codeid")]
        public ActionResult<DefaultResponse<Meta, LoanBrief>> GetLoanBriefByCodeId(int codeId)
        {
            var def = new DefaultResponse<Meta, LoanBrief>();
            try
            {
                var data = unitOfWork.LoanBriefRepository.Query(x => x.CodeId == codeId, null, false).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("re_init_loanbrief")]
        public ActionResult<DefaultResponse<object>> ReInitLoanBrief(LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.BeginTransaction();
                //xử lý thông tin customer
                var customerOld = unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == entity.Customer.Phone);
                if (customerOld != null && customerOld.CustomerId > 0)
                {
                    //cập nhật thông tin customer
                    entity.CustomerId = customerOld.CustomerId;
                    Ultility.CopyObject(entity.Customer, ref customerOld, true);
                    customerOld.UpdatedAt = DateTimeOffset.Now;
                    //customerOld.CustomerId = entity.CustomerId;
                    //cập nhật User
                    entity.Customer = customerOld;
                    entity.Customer.CustomerId = entity.CustomerId ?? 0;
                    unitOfWork.CustomerRepository.Update(customerOld);
                    unitOfWork.Save();
                }
                else
                {
                    entity.Customer.CreatedAt = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);
                    entity.Customer.JobId = entity.LoanBriefJob.JobId;
                    entity.Customer.CompanyName = entity.LoanBriefJob.CompanyName;
                    entity.Customer.CompanyAddress = entity.LoanBriefJob.CompanyAddress;
                    entity.Customer.ProvinceId = entity.LoanBriefResident.ProvinceId;
                    entity.Customer.DistrictId = entity.LoanBriefResident.DistrictId;
                    entity.Customer.WardId = entity.LoanBriefResident.WardId;
                    entity.Customer.Address = entity.LoanBriefResident.Address;
                    unitOfWork.CustomerRepository.Insert(entity.Customer);
                    unitOfWork.Save();
                    entity.CustomerId = entity.Customer.CustomerId;
                }
                #region Xử lý map + insert LoanBrief
                entity.CreatedTime = DateTimeOffset.Now;
                entity.FullName = entity.Customer.FullName;
                entity.Phone = entity.Customer.Phone;
                entity.Dob = entity.Customer.Dob;
                entity.Gender = entity.Customer.Gender;
                entity.NationalCard = entity.Customer.NationalCard;
                entity.Passport = entity.Customer.Passport;
                entity.NationalCardDate = entity.Customer.NationalCardDate;
                entity.NationCardPlace = entity.Customer.NationCardPlace;
                entity.ProvinceId = entity.LoanBriefResident.ProvinceId;
                entity.DistrictId = entity.LoanBriefResident.DistrictId;
                entity.WardId = entity.LoanBriefResident.WardId;
                entity.LoanAmountFirst = entity.LoanAmount;
                //insert to db
                unitOfWork.LoanBriefRepository.Insert(entity);

                //cập nhật thời gian update đơn cũ
                //unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.ReMarketingLoanBriefId, x => new LoanBrief()
                //{
                //    UpdatedTime = DateTime.Now
                //});
                unitOfWork.Save();
                #endregion
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
                unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBrief/ReInitLoanBrief POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("file_by_list_id")]
        public async Task<ActionResult<DefaultResponse<Meta, List<int>>>> GetFileByListId(string loanbriefId)
        {
            var def = new DefaultResponse<Meta, List<int>>();
            try
            {
                if (!string.IsNullOrEmpty(loanbriefId))
                {
                    var lstLoanBriefId = loanbriefId.Split(',').Select(Int32.Parse).ToList();
                    var data = await unitOfWork.LoanBriefFileRepository.Query(x => lstLoanBriefId.Contains(x.LoanBriefId.Value), null, false).Select(x => x.LoanBriefId.Value).ToListAsync();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = data;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefFile/GetFileByListId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_and_edit_schedule")]
        public ActionResult<DefaultResponse<Meta>> AddAndEditSchedule([FromBody] ScheduleTimeHubReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.LoanBriefId <= 0 || req.LoanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //Thêm mới
                if (req.TypeSubmit == 1 && req.ScheduleId == 0)
                {
                    unitOfWork.ScheduleTimeRepository.Insert(new ScheduleTime()
                    {
                        LoanBriefId = req.LoanBriefId,
                        UserId = req.UserId,
                        HubId = req.HubId,
                        CreatedTime = DateTime.Now,
                        UpdateAt = DateTime.Now,
                        StartTime = DateTime.ParseExact(req.StartTime, "dd/MM/yyyy HH:mm", null),
                        EndTime = DateTime.ParseExact(req.EndTime, "dd/MM/yyyy HH:mm", null),
                        Status = 1,
                        Note = req.Comment,
                        CusName = req.CusName,
                        UserFullName = req.UserFullName
                    });
                    //Update vào trường ScheduleTime trong LoanBrief
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == req.LoanBriefId, x => new LoanBrief()
                    {
                        ScheduleTime = DateTime.ParseExact(req.StartTime, "dd/MM/yyyy HH:mm", null)
                    });
                }
                //Cập nhật
                if (req.TypeSubmit == 2 && req.ScheduleId > 0)
                {
                    unitOfWork.ScheduleTimeRepository.Update(x => x.ScheduleId == req.ScheduleId, x => new ScheduleTime()
                    {
                        StartTime = DateTime.ParseExact(req.StartTime, "dd/MM/yyyy HH:mm", null),
                        EndTime = DateTime.ParseExact(req.EndTime, "dd/MM/yyyy HH:mm", null),
                        UpdateAt = DateTime.Now,
                        Note = req.Comment
                    });
                }
                //Hoàn thành
                if (req.TypeSubmit == 3 && req.ScheduleId > 0)
                {
                    unitOfWork.ScheduleTimeRepository.Update(x => x.ScheduleId == req.ScheduleId, x => new ScheduleTime()
                    {

                        UpdateAt = DateTime.Now,
                        Status = 2
                    });
                }

                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Loanbrief/AddAndEditSchedule Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("countloan_approve")]
        public ActionResult<DefaultResponse<object>> CountLoanByApprove(int approveId)
        {
            var def = new DefaultResponse<object>();
            try
            {

                var count = unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_REVIEW && x.CoordinatorUserId == approveId, null, false).Count();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = count;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/CountLoanByApprove Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_label_reborrow")]
        public ActionResult<DefaultResponse<object>> UpdateLabelReBorrow([FromBody] UpdateReBorrow entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                {
                    IsReborrow = entity.IsReBorrow
                });
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/RemoveLabelReBorrow Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("search_schedule")]
        public ActionResult<DefaultResponse<Meta, List<ScheduleTime>>> SearchSchedule([FromBody] GetListScheduleTimeHubReq req)
        {
            var def = new DefaultResponse<Meta, List<ScheduleTime>>();
            try
            {
                var start = DateTime.ParseExact(req.StartTime, "dd/MM/yyyy HH:mm", null);
                var end = DateTime.ParseExact(req.EndTime, "dd/MM/yyyy HH:mm", null);

                //var data = unitOfWork.ScheduleTimeRepository.Query(x => (x.HubId == req.HubId || req.HubId == -1) && (x.UserId == req.HubEmpId || req.HubEmpId == -1)
                //&& x.StartTime >= start && x.EndTime <= end, null, false).ToList();
                //def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                //def.data = data;


                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select st.LoanBriefId, st.UserId, st.Note, st.HubId, st.CreatedTime, st.UpdateAt, st.StartTime, st.EndTime , st.Status, st. ScheduleId, st.CusName, st.UserFullName ");
                sql.AppendLine("from LoanBrief(nolock) l ");
                sql.AppendLine("inner join ScheduleTime(nolock) st on l.LoanBriefId = st.LoanBriefId ");
                sql.AppendLine($"where st.StartTime >= '{start}' ");
                sql.AppendLine($"and st.EndTime <= '{end}' ");

                if (req.GroupId == (int)EnumGroupUser.Telesale && req.FilterGroupId == (int)EnumGroupUser.Telesale)
                {
                    sql.AppendLine($"and st.UserId = {req.UserId}");
                    sql.AppendLine($"and (l.Status = {(int)EnumLoanStatus.TELESALE_ADVICE}  or l.Status = {(int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN})");
                }
                if (req.GroupId == (int)EnumGroupUser.Telesale && req.FilterGroupId == (int)EnumGroupUser.StaffHub)
                    sql.AppendLine($"and l.Status = {(int)EnumLoanStatus.APPRAISER_REVIEW} and l.BoundTelesaleId = {req.UserId}");
                if (req.GroupId == (int)EnumGroupUser.StaffHub && req.FilterGroupId == (int)EnumGroupUser.StaffHub)
                    sql.AppendLine($"and st.UserId = {req.UserId} and l.Status = {(int)EnumLoanStatus.APPRAISER_REVIEW}");
                if (req.GroupId == (int)EnumGroupUser.StaffHub && req.FilterGroupId == (int)EnumGroupUser.Telesale)
                    sql.AppendLine($"and l.Status = {(int)EnumLoanStatus.APPRAISER_REVIEW} and l.HubEmployeeId = {req.UserId}");

                var data = db.Query<ScheduleTime>(sql.ToString());
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Loanbrief/GetScheduleTimeHub Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_schedule_by_loan/{loanBriefId}")]
        public ActionResult<DefaultResponse<Meta, List<ScheduleTime>>> GetScheduleByLoan(int loanBriefId)
        {
            var def = new DefaultResponse<Meta, List<ScheduleTime>>();
            try
            {

                var data = unitOfWork.ScheduleTimeRepository.Query(x => x.LoanBriefId == loanBriefId, null, false).OrderByDescending(x => x.CreatedTime).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetScheduleByLoan Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_json_info_familykalapa/{loanbriefId}")]
        public ActionResult<DefaultResponse<object>> UpdateJsonInfoFamilyKalapa(int loanbriefId, [FromBody] InfomationFamilyKalapa entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LoanBriefHouseholdRepository.Update(x => x.LoanBriefHouseholdId == loanbriefId, x => new LoanBriefHousehold()
                {
                    JsonInfoFamilyKalapa = JsonConvert.SerializeObject(entity)
                });
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateJsonInfoFamilyKalapa POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("SearchRemarketingCar")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>> SearchRemarketingCar([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefDetail>>();
            try
            {

                var query = unitOfWork.LoanBriefRepository
                    .Query(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done
                    && x.TypeRemarketing == (int)EnumTypeRemarketing.IsRemarketing
                    && x.UtmSource == "rmkt_loan_finish"
                    && x.PlatformType == (int)EnumPlatformType.ReMarketing
                    && x.CreateBy == (int)EnumUser.linhtq
                    && x.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                    && x.HubId.GetValueOrDefault(0) == 0
                    && x.Status == (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING, null, false).Select(LoanBriefDetail.ProjectionDetail);
                var totalRecords = query.Count();
                List<LoanBriefDetail> data;
                if (skip == -1)
                    data = query.OrderByDescending(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                {
                    data = query.Skip(skip).Take(take).ToList();
                    pageSize = take;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/SearchRemarketingCar Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_hubid")]
        public ActionResult<DefaultResponse<Meta>> UpdateHubId([FromBody] BoundTelesaleReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.loanBriefId <= 0 || req.userId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanInfo = unitOfWork.LoanBriefRepository.GetById(req.loanBriefId);
                if (loanInfo != null)
                {
                    // update hub
                    loanInfo.HubId = req.userId;
                    unitOfWork.LoanBriefRepository.Update(loanInfo);

                    //Insert LogPushToHub
                    var LogPushItem = new LogPushToHub()
                    {
                        LoanbriefId = loanInfo.LoanBriefId,
                        CityId = loanInfo.ProvinceId,
                        DistrictId = loanInfo.DistrictId,
                        WardId = loanInfo.WardId,
                        ProductId = loanInfo.ProductId,
                        HubId = req.userId,
                        CreatedAt = DateTime.Now
                    };
                    unitOfWork.LogPushToHubRepository.Insert(LogPushItem);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateApprove CoordinatorUserId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_hub_employee_call_first")]
        public ActionResult<DefaultResponse<object>> UpdateHubEmployeeCallFirst([FromBody] HubFeedbackReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                //if (loanbrief != null && loanbrief.LoanBriefId > 0)
                //{
                // update to new status
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("update LoanBrief ");
                sql.AppendLine("set HubEmployeeCallFirst = GETDATE() ");
                sql.AppendLine($"where LoanBriefId = {entity.LoanBriefId} ");
                db.Execute(sql.ToString());
                //unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                //{
                //    HubEmployeeCallFirst = DateTime.Now,
                //});
                //unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
                //}
                //else
                //{
                //    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                //    return Ok(def);
                //}
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateHubEmployeeCallFirst Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("calculate_time_processing_loanbrief")]
        public async Task<ActionResult<DefaultResponse<Meta, List<TimeProcessingLoanBrief>>>> CalculateTimeProcessingLoanBrief(string loanbriefId, int groupId)
        {
            var def = new DefaultResponse<Meta, List<TimeProcessingLoanBrief>>();
            try
            {
                if (!string.IsNullOrEmpty(loanbriefId))
                {
                    var lstLoanBriefId = loanbriefId.Split(',').Select(Int32.Parse).ToList();
                    var lstData = new List<TimeProcessingLoanBrief>();
                    if (lstLoanBriefId != null && lstLoanBriefId.Count > 0)
                    {
                        //lấy ra bảng loanbrief
                        var lstLoanBrief = await unitOfWork.LoanBriefRepository.Query(x => lstLoanBriefId.Contains(x.LoanBriefId), null, false).Select(x => new
                        {
                            LoanBriefId = x.LoanBriefId,
                            FirstProcessingTime = x.FirstProcessingTime,
                            HubEmployeeReceived = x.HubEmployeeReceived,
                            HubEmployeeCallFirst = x.HubEmployeeCallFirst
                        }).ToListAsync();
                        foreach (var item in lstLoanBrief)
                        {
                            var timeProcessing = 0;
                            bool isFeedBack = false;
                            //Telesales
                            if (groupId == (int)EnumGroupUser.Telesale || groupId == (int)EnumGroupUser.ManagerTelesales)
                            {
                                //lấy ra bản ghi đầu tiên trong bảng TelesalesLoanBrief không phải là của spt_System 
                                //UserId của spt_System: 26695
                                var telesalesLoanBrief = await unitOfWork.LogDistributionUserRepository.Query(x => x.LoanbriefId == item.LoanBriefId && x.UserId != 26695, null, false).Select(x => new
                                {
                                    CreatedAt = x.CreatedAt
                                }).OrderBy(x => x.CreatedAt).FirstOrDefaultAsync();
                                //không tồn tại thì lấy không thì bỏ qua
                                if (telesalesLoanBrief != null)
                                {
                                    if (item.FirstProcessingTime.HasValue)
                                    {
                                        timeProcessing = (int)item.FirstProcessingTime.Value.Subtract(telesalesLoanBrief.CreatedAt.Value).TotalMinutes;
                                        isFeedBack = true;
                                    }
                                    else
                                        timeProcessing = (int)DateTime.Now.Subtract(telesalesLoanBrief.CreatedAt.Value).TotalMinutes;
                                }
                                else continue;
                            }
                            //Hub
                            else if (groupId == (int)EnumGroupUser.StaffHub || groupId == (int)EnumGroupUser.ManagerHub)
                            {
                                if (item.HubEmployeeReceived.HasValue)
                                {
                                    if (item.HubEmployeeCallFirst.HasValue)
                                    {
                                        timeProcessing = (int)item.HubEmployeeCallFirst.Value.Subtract(item.HubEmployeeReceived.Value).TotalMinutes;
                                        isFeedBack = true;
                                    }
                                    else
                                        timeProcessing = (int)DateTime.Now.Subtract(item.HubEmployeeReceived.Value).TotalMinutes;
                                }
                                else continue;
                            }

                            var data = new TimeProcessingLoanBrief()
                            {
                                LoanBriefId = item.LoanBriefId,
                                TimeProcessing = timeProcessing,
                                IsFeedBack = isFeedBack
                            };
                            lstData.Add(data);
                        }
                    }
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = lstData;
                }
                else
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefFile/GetFileByListId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_telesales_first_processing_time")]
        public ActionResult<DefaultResponse<object>> UpdateTelesalesFirstProcessingTime([FromBody] HubFeedbackReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("update LoanBrief ");
                sql.AppendLine("set First_ProcessingTime = GETDATE() ");
                sql.AppendLine($"where LoanBriefId  = {entity.LoanBriefId} ");
                db.Execute(sql.ToString());
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateTelesalesFirstProcessingTime Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("save_next_step")]
        public ActionResult<DefaultResponse<object>> SaveNextStep([FromBody] DAL.Object.LoanStatusDetail.NextStepLoanHub entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (entity.LoanBriefId > 0)
                {
                    //save status detail
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                    {
                        LoanStatusDetail = entity.StatusDetail,
                        LoanStatusDetailChild = entity.StatusDetailChild
                    });
                    unitOfWork.Save();

                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
            }

            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/SaveNextStep Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("save_change_status_approver")]
        public ActionResult<DefaultResponse<object>> SaveChangeStatusApprover([FromBody] DAL.Object.LoanStatusDetail.NextStepLoanHub entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (entity.LoanBriefId > 0)
                {
                    //save status detail
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                    {
                        ApproverStatusDetail = entity.StatusDetail
                    });
                    unitOfWork.Save();

                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
            }

            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/SaveChangeStatusApprover Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("list_loan_by_codeid")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefDetailDebt>>> GetListLoanByCodeId([FromBody] GetLoanByCode entity)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefDetailDebt>>();
            try
            {
                //var arrCodeId = lstId.Split(',').Select(x => int.Parse(x));
                var data = unitOfWork.LoanBriefRepository.Query(x => entity.lstId.Contains(x.LmsLoanId.Value), null, false).Select(LoanBriefDetailDebt.ProjectionDetailDebt).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetListLoanByCodeId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_loan_department")]
        public async Task<ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>>> GetLoanDepartment([FromQuery] int page = 1,
            [FromQuery] int pageSize = 20, [FromQuery] int skip = -1, [FromQuery] int take = 20, [FromQuery] int loanBriefId = -1, [FromQuery] int hubId = -1,
            [FromQuery] string status = "", [FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefDetail>>();
            try
            {
                List<int> ListStatus = null;
                if (!string.IsNullOrEmpty(status))
                {
                    ListStatus = status.Split(",").Select<string, int>(int.Parse).ToList();
                }
                var query = unitOfWork.LoanBriefRepository.Query(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done
                                                                      && (x.LoanBriefId == loanBriefId || loanBriefId == -1)
                                                                      && (x.HubId == hubId || hubId == -1)
                                                                      && (x.CreatedTime >= fromDate && x.CreatedTime < toDate)
                                                                      && (ListStatus.Contains(x.Status.Value) || ListStatus == null)
                                                                    , null, false).Select(LoanBriefDetail.ProjectionHubDetail);

                var totalRecords = query.Count();
                List<LoanBriefDetail> data;
                if (skip == -1)
                {
                    data = await query.OrderByDescending(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                }

                else
                {
                    data = query.OrderByDescending(x => x.LoanBriefId).Skip(skip).Take(take).ToList();
                    pageSize = take;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanDepartment Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("tool_change_pipeline")]
        public ActionResult<DefaultResponse<Meta>> ToolChangePipeline([FromBody] ToolChangePipelineModel entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.LoanbriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (Enum.IsDefined(typeof(ActionChangePipeline), entity.ActionPipeline))
                {
                    if (entity.ActionPipeline == (int)ActionChangePipeline.Next)
                    {
                        unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanbriefId, x => new LoanBrief()
                        {
                            InProcess = EnumInProcess.Process.GetHashCode(),
                            ActionState = EnumActionPush.Push.GetHashCode(),
                            PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode(),
                            AddedToQueue = false
                        });
                        unitOfWork.Save();
                    }
                    else if (entity.ActionPipeline == (int)ActionChangePipeline.Back)
                    {
                        unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanbriefId, x => new LoanBrief()
                        {
                            InProcess = EnumInProcess.Process.GetHashCode(),
                            ActionState = EnumActionPush.Back.GetHashCode(),
                            PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode(),
                            AddedToQueue = false
                        });
                        unitOfWork.Save();

                    }
                    else if (entity.ActionPipeline == (int)ActionChangePipeline.Init)
                    {
                        var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                        var sql = new StringBuilder();
                        sql.AppendLine("update LoanBrief ");
                        sql.AppendLine("set Status = 1, CurrentPipelineId  = null, PipelineState = 0, AddedToQueue = 0 ");
                        sql.AppendLine($"where LoanBriefId  = {entity.LoanbriefId} ");
                        db.Execute(sql.ToString());
                    }
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ToolChangePipeline Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);

        }

        [HttpPost]
        [Route("update_exceptions")]
        public ActionResult<DefaultResponse<object>> UpdateExceptions([FromBody] DAL.Object.Loanbrief.ProposeExceptions entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.LoanBriefId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(entity.LoanBriefId);
                if (loanbrief != null)
                {
                    if (entity.IsPushPipeline)
                    {
                        if (entity.LoanAmount > 0)
                            loanbrief.LoanAmount = entity.LoanAmount;
                        if (loanbrief.HubPushAt != null)
                            loanbrief.ApproverStatusDetail = (int)EnumLoanStatusDetailApprover.BoSungGiayTo;
                        loanbrief.HubPushAt = DateTime.Now;
                        loanbrief.IsExceptions = entity.Status;
                        loanbrief.ActionState = EnumActionPush.Push.GetHashCode();
                        loanbrief.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                        loanbrief.InProcess = EnumInProcess.Process.GetHashCode();
                        loanbrief.ExceptionsId = entity.DocId;
                    }
                    else
                    {
                        loanbrief.IsExceptions = entity.Status;
                    }
                    unitOfWork.LoanBriefRepository.Update(loanbrief);
                    unitOfWork.Save();
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }


                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateExceptions Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_feedback")]
        public async Task<ActionResult<DefaultResponse<object>>> UpdateLoanBriefClickToCall([FromBody] DAL.Object.LoanBriefFeedback entity)
        {
            var def = new DefaultResponse<object>();
            var optionsBuilder = new DbContextOptionsBuilder<LOSContext>();
            optionsBuilder.UseSqlServer(_baseConfig.GetConnectionString("LOSDatabase"));
            DbContextOptions<LOSContext> options = optionsBuilder.Options;
            using (var db = new LOSContext(options))
            {
                try
                {
                    var loanbrief = await db.LoanBrief.FindAsync(entity.LoanBriefId);
                    if (loanbrief != null && loanbrief.LoanBriefId > 0)
                    {
                        if (entity.CountCall > 0)
                            loanbrief.CountCall = entity.CountCall;
                        if (entity.FirstProcessingTime.HasValue)
                            loanbrief.FirstProcessingTime = entity.FirstProcessingTime;
                        if (entity.LastProcessingTime.HasValue)
                            loanbrief.LastChangeStatusOfTelesale = entity.LastProcessingTime;
                        if (entity.HubEmployeeCallFirst.HasValue)
                            loanbrief.HubEmployeeCallFirst = entity.HubEmployeeCallFirst;
                        if (entity.FirstTimeHubFeedBack.HasValue)
                            loanbrief.FirstTimeHubFeedBack = entity.FirstTimeHubFeedBack;
                        if (entity.FirstTimeStaffHubFeedback.HasValue)
                            loanbrief.FirstTimeStaffHubFeedback = entity.FirstTimeStaffHubFeedback;
                        db.Entry(loanbrief).State = EntityState.Modified;
                        db.SaveChanges();
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "LoanBrief/update_feedback Exception");
                    def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                }
            };
            return Ok(def);
        }

        [HttpGet]
        [Route("get_loanbrief_by_phone_nationalcard")]
        public ActionResult<DefaultResponse<Meta, LoanBriefPrematureInterest>> GetLoanBriefByPhoneOrNationalCard(string text_search)
        {
            var def = new DefaultResponse<Meta, LoanBriefPrematureInterest>();
            try
            {
                var data = new LoanBriefPrematureInterest();
                //tìm theo sđt
                if (text_search.Length == 10)
                    data = unitOfWork.LoanBriefRepository.Query(x => x.Phone == text_search && (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH), null, false).OrderByDescending(x => x.LoanBriefId).Select(LoanBriefPrematureInterest.ProjectionDetail).FirstOrDefault();
                //tìm theo số cmnd
                else
                    data = unitOfWork.LoanBriefRepository.Query(x => x.NationalCard == text_search && (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH), null, false).OrderByDescending(x => x.LoanBriefId).Select(LoanBriefPrematureInterest.ProjectionDetail).FirstOrDefault();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBriefByPhoneOrNationalCard Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("internalControl_search")]
        public async Task<ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>>> InternalControlSearch([FromQuery] int loanBriefId = -1, [FromQuery] string search = null,
            [FromQuery] int status = -1, [FromQuery] int hubId = -1, [FromQuery] int locate = -1, [FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null, [FromQuery] int export = 0)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefDetail>>();
            try
            {

                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done
                && (x.LoanBriefId == loanBriefId || loanBriefId == -1)
                && ((x.Phone.Contains(search) || search == null) || (x.NationalCard.Contains(search) || search == null) || (x.FullName.Contains(search) || search == null))
                && x.Status == status
                && (x.DisbursementAt >= fromDate && x.DisbursementAt < toDate)
                && x.HubId == hubId);
                //Có lắp định vị
                if (locate == 1)
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.IsLocate == true));
                }
                //Không lắp định vị
                else if (locate == 0)
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.IsLocate == false));
                }

                var filterService = new FilterServices<LoanBrief>(unitOfWork);
                filterService.GetFilter(GetUserId(), ref filter, false);

                var query = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefDetail.ProjectionDetail);
                var totalRecords = query.Count();
                List<LoanBriefDetail> data;
                if (export > 0)
                    data = query.OrderByDescending(x => x.DisbursementAt).ToList();
                else
                {
                    if (skip == -1)
                    {
                        data = await query.OrderByDescending(x => x.DisbursementAt).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                    }

                    else
                    {
                        data = query.OrderByDescending(x => x.DisbursementAt).Skip(skip).Take(take).ToList();
                        pageSize = take;
                    }
                }

                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/InternalControlSearch Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("for_detail/{id}")]
        public ActionResult<DefaultResponse<Meta, LoanBriefForDetail>> GetForDetail(int id)
        {
            var def = new DefaultResponse<Meta, LoanBriefForDetail>();
            try
            {
                var data = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == id, null, false).Select(LoanBriefForDetail.ProjectionViewDetail).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetForDetail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("check_loan_topup")]
        public ActionResult<DefaultResponse<Meta, dynamic>> CheckLoanTopup(string nationalCard, string phone, int loanbriefId)
        {
            var def = new DefaultResponse<Meta, dynamic>();
            try
            {
                var lstLoanBrief = unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId != loanbriefId &&
                x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp
                && (x.Phone == phone || (!string.IsNullOrEmpty(nationalCard) && x.NationalCard == nationalCard)), null, false).Count();
                if (lstLoanBrief > 0)
                {
                    def.data = false;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/CheckLoanTopup Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        /// <summary>
        /// Lấy ra đơn vay đang xử lý
        /// </summary>
        /// <param name="nationalCard"></param>
        /// <param name="phone"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("loanbrief_get_borrowing")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanBriefDetail>>> GetLoanBriefBorrowing(string nationalCard, string phone)
        {
            var def = new DefaultResponse<Meta, LoanBriefDetail>();
            try
            {
                var data = await unitOfWork.LoanBriefRepository.Query(x => x.Status != (int)EnumLoanStatus.DISBURSED
                    && x.Status != (int)EnumLoanStatus.CANCELED
                    && x.Status != (int)EnumLoanStatus.FINISH
                    && (x.Phone == phone || (!string.IsNullOrEmpty(nationalCard)
                    && x.NationalCard == nationalCard)), null, false).OrderByDescending(x => x.LoanBriefId).Select(LoanBriefDetail.ProjectionDetail).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBriefBorrowing Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("loanbrief_get_list_topup_car")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LoanBriefDetail>>>> GetLoanBriefListTopupCar(int customerId)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefDetail>>();
            try
            {
                var data = await unitOfWork.LoanBriefRepository.Query(x => x.Status != (int)EnumLoanStatus.DISBURSED
                && x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.FINISH
                && x.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                && x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp && x.PlatformType == (int)EnumPlatformType.ReMarketing
                && x.CustomerId == customerId, null, false).Select(LoanBriefDetail.ProjectionDetail).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBriefListTopupCar Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("loanbrief_get_click_top_tls")]
        public async Task<ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefForBrower>>>> GetLoanBriefClickTopTls([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] int filterClickTop = -1, [FromQuery] string field = "", [FromQuery] string sort = "", [FromQuery] int boundTelesaleId = -1, [FromQuery] string teamTelesales = null)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefForBrower>>();
            var now = DateTimeOffset.Now;
            var startDay = new DateTimeOffset(now.Year, now.Month, now.Day, 00, 00, 01, new TimeSpan(7, 0, 0));
            var startTime = new DateTimeOffset(now.Year, now.Month, now.Day, 08, 30, 00, new TimeSpan(7, 0, 0));
            var endTime = new DateTimeOffset(now.Year, now.Month, now.Day, 11, 55, 00, new TimeSpan(7, 0, 0));
            var startTime2 = new DateTimeOffset(now.Year, now.Month, now.Day, 13, 15, 00, new TimeSpan(7, 0, 0));
            var endTime2 = new DateTimeOffset(now.Year, now.Month, now.Day, 17, 30, 00, new TimeSpan(7, 0, 0));
            try
            {
                List<int> lstTeamTelesalesId = new List<int>();
                if (teamTelesales != null)
                    lstTeamTelesalesId = teamTelesales.Split(',').Select(Int32.Parse).ToList();

                var isCheckStatus = true;
                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done
                    && (x.BoundTelesaleId == boundTelesaleId || boundTelesaleId == -1));

                if (lstTeamTelesalesId.Count > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value)));

                if (filterClickTop == EnumFilterClickTop.ADVICE_LOAN.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()
                    && !x.StatusTelesales.HasValue));
                    isCheckStatus = false;
                }
                else if (filterClickTop == EnumFilterClickTop.PREPARE_LOAN.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode()));
                    isCheckStatus = false;
                }

                else if (filterClickTop == EnumFilterClickTop.CANCEL_LOAN.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.CANCELED.GetHashCode() && x.CreatedTime >= startDay));
                    isCheckStatus = false;
                }
                else if (filterClickTop == EnumFilterClickTop.HOTLEAD_NORESPONSE.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode() && x.FirstProcessingTime == null
                    && ((x.CreatedTime >= startTime && x.CreatedTime < endTime) || (x.CreatedTime >= startTime2 && x.CreatedTime < endTime2)
                    //|| (x.CreatedTime >= startTime3 && x.CreatedTime < endTime3)
                    )));
                    isCheckStatus = false;
                }
                else if (filterClickTop == EnumFilterClickTop.HOTLEAD_RESPONDED.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode() && x.FirstProcessingTime != null
                    && ((x.CreatedTime >= startTime && x.CreatedTime < endTime) || (x.CreatedTime >= startTime2 && x.CreatedTime < endTime2)
                    //|| (x.CreatedTime >= startTime3 && x.CreatedTime < endTime3)
                    )));
                    isCheckStatus = false;
                }
                else if (filterClickTop == EnumFilterClickTop.AUTOCALL.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Step > 0 && x.PlatformType != (int)EnumPlatformType.Ai && x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()));
                    isCheckStatus = false;
                    field = "Step";
                    sort = "desc";
                }
                else if (filterClickTop == EnumFilterClickTop.CHATBOT.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.PlatformType == (int)EnumPlatformType.Ai && x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()));
                    isCheckStatus = false;
                    field = "Step";
                    sort = "desc";
                }
                var filterService = new FilterServices<LoanBrief>(unitOfWork);
                filterService.GetFilter(GetUserId(), ref filter, isCheckStatus);

                var query = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefForBrower.ProjectionViewDetail);
                var totalRecords = query.Count();
                List<LoanBriefForBrower> data;
                if (skip == -1)
                {
                    if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(sort))
                    {
                        data = await query.OrderBy(field + " " + sort).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                    }
                    else
                    {
                        data = query.OrderByDescending(x => x.UtmSource == "chatbot")
                         .ThenByDescending(x => x.TelesalesPushAt.HasValue)
                         .ThenByDescending(x => x.ScheduleTime.HasValue)
                         .ThenByDescending(x => x.ScheduleTime)
                         .ThenByDescending(x => x.LoanBriefId)
                         .Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(sort))
                    {
                        data = await query.OrderBy(field + " " + sort).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                        pageSize = take;
                    }
                    else
                    {
                        data = query.OrderByDescending(x => x.UtmSource == "chatbot")
                        .ThenByDescending(x => x.TelesalesPushAt.HasValue)
                          .ThenByDescending(x => x.LoanBriefId)
                          .Skip((page - 1) * pageSize).Take(pageSize).ToList();
                        pageSize = take;
                    }
                }

                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBriefClickTopTls Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("loanbrief_get_by_national_card")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanBriefDetail>>> GetLoanBriefByNationalCard(string nationalCard, int loanbriefId)
        {
            var def = new DefaultResponse<Meta, LoanBriefDetail>();
            try
            {
                var data = await unitOfWork.LoanBriefRepository.Query(x => x.NationalCard == nationalCard 
                && x.Status != (int)EnumLoanStatus.DISBURSED && x.Status != (int)EnumLoanStatus.FINISH
                && x.Status != (int)EnumLoanStatus.CANCELED && x.LoanBriefId != loanbriefId, null, false).OrderByDescending(x => x.LoanBriefId).Select(LoanBriefDetail.ProjectionDetail).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBriefByNationalCard Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpPost]
        [Route("get_log_distribution_user")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LogDistributionUser>>>> GetLogDistributionUser(GetLogDistributionUserModel entity)
        {
            var def = new DefaultResponse<Meta, List<LogDistributionUser>>();
            try
            {

                var data = await unitOfWork.LogDistributionUserRepository.Query(x => entity.LstLoanBriefId.Contains(x.LoanbriefId.Value)
                    && x.TypeDistribution == entity.TypeDistribution, null, false).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLogDistributionUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("compare_info")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanInfoCompare>>> GetCompareInfo(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, LoanInfoCompare>();
            try
            {
                var data = await unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(LoanInfoCompare.ProjectionViewDetail).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetCompareInfo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_file_by_document_multi")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LoanBriefFileDetail>>>> GetFileByDocumentMulti(int loanbriefId, string documentId)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefFileDetail>>();
            try
            {
                if (!string.IsNullOrEmpty(documentId))
                {
                    var lstDocumentId = documentId.Split(',').Select(Int32.Parse).ToList();

                    var data = await unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanbriefId && x.Status == 1 && x.IsDeleted != 1 && lstDocumentId.Contains(x.TypeId.Value), null, false).Select(LoanBriefFileDetail.ProjectionDetail).ToListAsync();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = data;

                }
                else
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefFile/GetFileByDocumentMulti Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("loanbrief_disbursed")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanBriefDetail>>> GetLoanBriefDisbursed(string nationalCard, string phone)
        {
            var def = new DefaultResponse<Meta, LoanBriefDetail>();
            try
            {
                var data = await unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.DISBURSED
                && (x.Phone == phone || (!string.IsNullOrEmpty(nationalCard)
                && x.NationalCard == nationalCard)), null, false).OrderByDescending(x => x.LoanBriefId).Select(LoanBriefDetail.ProjectionDetail).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBriefDisbursed Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("loanbrief_change_support_all_click_top_tls")]
        public ActionResult<DefaultResponse<Meta, List<int>>> LoanBriefChangeSupportAllClickTopTls([FromQuery] int filterClickTop = -1,
            [FromQuery] int boundTelesaleId = -1, [FromQuery] string teamTelesales = null)
        {
            var def = new DefaultResponse<Meta, List<int>>();
            var now = DateTimeOffset.Now;
            var startDay = new DateTimeOffset(now.Year, now.Month, now.Day, 00, 00, 01, new TimeSpan(7, 0, 0));
            var startTime = new DateTimeOffset(now.Year, now.Month, now.Day, 08, 30, 00, new TimeSpan(7, 0, 0));
            var endTime = new DateTimeOffset(now.Year, now.Month, now.Day, 11, 55, 00, new TimeSpan(7, 0, 0));
            var startTime2 = new DateTimeOffset(now.Year, now.Month, now.Day, 13, 15, 00, new TimeSpan(7, 0, 0));
            var endTime2 = new DateTimeOffset(now.Year, now.Month, now.Day, 17, 30, 00, new TimeSpan(7, 0, 0));
            try
            {
                List<int> lstTeamTelesalesId = new List<int>();
                if (teamTelesales != null)
                    lstTeamTelesalesId = teamTelesales.Split(',').Select(Int32.Parse).ToList();

                var isCheckStatus = true;
                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done
                    && (x.BoundTelesaleId == boundTelesaleId || boundTelesaleId == -1));

                if (lstTeamTelesalesId.Count > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value)));

                if (filterClickTop == EnumFilterClickTop.ADVICE_LOAN.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()
                    && !x.StatusTelesales.HasValue));
                    isCheckStatus = false;
                }
                else if (filterClickTop == EnumFilterClickTop.PREPARE_LOAN.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode()));
                    isCheckStatus = false;
                }

                else if (filterClickTop == EnumFilterClickTop.CANCEL_LOAN.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.CANCELED.GetHashCode() && x.CreatedTime >= startDay));
                    isCheckStatus = false;
                }
                else if (filterClickTop == EnumFilterClickTop.HOTLEAD_NORESPONSE.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode() && x.FirstProcessingTime == null
                    && ((x.CreatedTime >= startTime && x.CreatedTime < endTime) || (x.CreatedTime >= startTime2 && x.CreatedTime < endTime2)
                    )));
                    isCheckStatus = false;
                }
                else if (filterClickTop == EnumFilterClickTop.HOTLEAD_RESPONDED.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode() && x.FirstProcessingTime != null
                    && ((x.CreatedTime >= startTime && x.CreatedTime < endTime) || (x.CreatedTime >= startTime2 && x.CreatedTime < endTime2)
                    )));
                    isCheckStatus = false;
                }
                else if (filterClickTop == EnumFilterClickTop.AUTOCALL.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Step > 0 && x.PlatformType != (int)EnumPlatformType.Ai && x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()));
                    isCheckStatus = false;
                }
                else if (filterClickTop == EnumFilterClickTop.CHATBOT.GetHashCode())
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.PlatformType == (int)EnumPlatformType.Ai && x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()));
                    isCheckStatus = false;
                }
                var filterService = new FilterServices<LoanBrief>(unitOfWork);
                filterService.GetFilter(GetUserId(), ref filter, isCheckStatus);

                var data = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(x => x.LoanBriefId).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/LoanBriefChangeSupportAllClickTopTls Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("loanbrief_change_support_all")]
        public ActionResult<DefaultResponse<Meta, List<int>>> LoanBriefChangeSupportAll([FromQuery] int loanBriefId = -1, [FromQuery] string search = null, [FromQuery] int productId = -1, [FromQuery] int status = -1,
            [FromQuery] int filterSource = -1, [FromQuery] int provinceId = -1, [FromQuery] int boundTelesaleId = -1, [FromQuery] DateTime? fromDate = null,
            [FromQuery] DateTime? toDate = null, [FromQuery] int countCall = -1, [FromQuery] int detailStatusTelesales = -1,
            [FromQuery] int filterStaffTelesales = -1, [FromQuery] int statusTelesales = 0, [FromQuery] string teamTelesales = null, [FromQuery] int team = 0,
            [FromQuery] DateTime? fromDateLastChangeStatus = null, [FromQuery] DateTime? toDateLastChangeStatus = null, [FromForm] int groupId = 0, [FromQuery] string utmSource = null,
            [FromQuery] int userId = 0)
        {
            var def = new DefaultResponse<Meta, List<int>>();
            try
            {
                List<int> lstTeamTelesalesId = new List<int>();
                if (teamTelesales != null)
                    lstTeamTelesalesId = teamTelesales.Split(',').Select(Int32.Parse).ToList();

                //Hiển thị tất cả các đơn OTO đã GN không theo telesale
                if (statusTelesales == (int)EnumStatusTelesales.DISBURSED
                    || statusTelesales == (int)EnumStatusTelesales.FINISH)
                    boundTelesaleId = -1;
                bool isCheckTeam = false;
                var filter = FilterHelper<LoanBrief>.Create(x => x.InProcess.GetValueOrDefault(0) == (int)EnumInProcess.Done);
                //Kiểm tra theo boundtelesale
                if (boundTelesaleId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.BoundTelesaleId == boundTelesaleId));
                if (loanBriefId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LoanBriefId == loanBriefId));

                if (team > 0)
                {
                    //kiểm tra xem có chọn là team system team
                    if (team == (int)EnumTeamTelesales.SystemTeam.GetHashCode())
                    {
                        //lấy ra danh sách userId của team system
                        var lstUserId = unitOfWork.UserRepository.Query(x => x.TeamTelesalesId == (int)EnumTeamTelesales.SystemTeam.GetHashCode(), null, false).Select(x => x.UserId).ToList();
                        if (lstUserId != null && lstUserId.Count > 0)
                        {
                            //lấy những đơn mà của system team mà do team mình quản lý
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => lstUserId.Contains(x.BoundTelesaleId.Value) && lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value)));
                        }
                    }
                    else
                    {
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.TeamTelesalesId == team));
                        isCheckTeam = true;
                    }
                }
                if (!isCheckTeam && lstTeamTelesalesId.Count > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value)));

                if (fromDateLastChangeStatus != null && toDateLastChangeStatus != null)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.LastChangeStatusOfTelesale >= fromDateLastChangeStatus
                    && x.LastChangeStatusOfTelesale <= toDateLastChangeStatus));

                if (productId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ProductId == productId));

                if (provinceId > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.ProvinceId == provinceId));

                if (filterStaffTelesales > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.BoundTelesaleId == filterStaffTelesales));

                if (!string.IsNullOrEmpty(search))
                {
                    var textSearch = search.Trim();
                    if (ConvertExtensions.IsNumber(textSearch)) //Nếu là số
                    {
                        if (textSearch.Length == (int)NationalCardLength.CMND_QD || textSearch.Length == (int)NationalCardLength.CMND || textSearch.Length == (int)NationalCardLength.CCCD)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.NationalCard == textSearch || x.NationCardPlace == textSearch)));
                        else//search phone
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Phone == textSearch || x.PhoneOther == textSearch)));
                    }
                    else//nếu là text =>search tên
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.FullName.Contains(search)));
                }

                //if (team > 0)
                //    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.TeamTelesalesId == team));

                if (!string.IsNullOrEmpty(utmSource))
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.UtmSource.Contains(utmSource)));

                if (statusTelesales != (int)EnumStatusTelesales.NotContact && statusTelesales != (int)EnumStatusTelesales.LoanCancel
                    && statusTelesales != (int)EnumStatusTelesales.DISBURSED && statusTelesales != (int)EnumStatusTelesales.FINISH
                    && countCall > 0)
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.CountCall == countCall));
                if (filterSource > 0)
                {
                    if (filterSource == (int)FilterSourceTLS.form_cancel_rmkt || filterSource == (int)FilterSourceTLS.rmkt_loan_finish)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.UtmSource == LOS.Common.Helpers.ExtentionHelper.GetName((FilterSourceTLS)filterSource)));
                    else if (filterSource == (int)FilterSourceTLS.SourceSelfCreated)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.CreateBy == userId));
                    else if (filterSource == (int)FilterSourceTLS.SourceFormNewMKT)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.CreateBy.GetValueOrDefault(0) == 0 && x.ReMarketingLoanBriefId.GetValueOrDefault(0) == 0));
                }

                if (statusTelesales == (int)EnumStatusTelesales.NotContact || statusTelesales == (int)EnumStatusTelesales.LoanCancel
                    || statusTelesales == (int)EnumStatusTelesales.DISBURSED || statusTelesales == (int)EnumStatusTelesales.FINISH)
                {
                    status = statusTelesales;
                    if (status > 0)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == status));

                    if (statusTelesales == (int)EnumStatusTelesales.NotContact)
                    {
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => (!x.DetailStatusTelesales.HasValue)
                       && x.CreatedTime > fromDate && x.CreatedTime < toDate));
                    }
                    //chỉ mở sản phẩm oto
                    else if (statusTelesales == (int)EnumStatusTelesales.DISBURSED || statusTelesales == (int)EnumStatusTelesales.FINISH)
                    {
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                         && x.CreatedTime > fromDate && x.CreatedTime < toDate));
                    }
                    else
                    {
                        if (detailStatusTelesales > 0)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.DetailStatusTelesales == detailStatusTelesales));

                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.CreatedTime > fromDate && x.CreatedTime < toDate));
                    }
                }
                else if (statusTelesales == (int)EnumStatusTelesales.HubBack)
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode()
                       && x.TelesalesPushAt.HasValue
                       && x.CreatedTime > fromDate && x.CreatedTime < toDate));
                }
                else if (statusTelesales == (int)EnumStatusTelesales.PushHub)
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Status == EnumLoanStatus.APPRAISER_REVIEW.GetHashCode() || x.Status == EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING.GetHashCode()
                            || x.Status == EnumLoanStatus.HUB_CHT_APPROVE.GetHashCode())
                       && x.TelesalesPushAt.HasValue
                       && x.CreatedTime > fromDate && x.CreatedTime < toDate));
                }
                else if (statusTelesales == (int)EnumStatusTelesales.WaitTDHSHandle)
                {
                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Status == EnumLoanStatus.BRIEF_APPRAISER_LOAN_DISTRIBUTING.GetHashCode()
                            || x.Status == EnumLoanStatus.BRIEF_APPRAISER_REVIEW.GetHashCode()
                            || x.Status == EnumLoanStatus.BRIEF_APPRAISER_APPROVE_PROPOSION.GetHashCode()
                            || x.Status == EnumLoanStatus.BRIEF_APPRAISER_APPROVE_CANCEL.GetHashCode())
                       && x.TelesalesPushAt.HasValue
                       && x.CreatedTime > fromDate && x.CreatedTime < toDate));
                }
                else
                {
                    if (statusTelesales > 0)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.StatusTelesales == statusTelesales));
                    if (detailStatusTelesales > 0)
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.DetailStatusTelesales == detailStatusTelesales));

                    filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode() || x.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode())
                       && x.CreatedTime > fromDate && x.CreatedTime < toDate));
                }

                var data = unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(x => x.LoanBriefId).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/LoanBriefChangeSupportAll Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_range_note")]
        public ActionResult<DefaultResponse<object>> AddRangeNote([FromBody] List<LoanBriefNote> entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LoanBriefNoteRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddRangeNote Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_range_telesale")]
        public ActionResult<DefaultResponse<Meta>> UpdateRangeBoundTelesale([FromBody] RangeBoundTelesaleReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.loanBriefIds == null || req.loanBriefIds.Count <= 0 || req.userId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                if (req.TeamTelesaleId > 0)
                {
                    unitOfWork.LoanBriefRepository.Update(x => req.loanBriefIds.Contains(x.LoanBriefId), x => new LoanBrief()
                    {
                        BoundTelesaleId = req.userId,
                        TeamTelesalesId = req.TeamTelesaleId
                    });
                }
                else
                {
                    unitOfWork.LoanBriefRepository.Update(x => req.loanBriefIds.Contains(x.LoanBriefId), x => new LoanBrief()
                    {
                        BoundTelesaleId = req.userId,
                    });
                }
                //cập nhật thời gian nhận đơn cuối cùng của telesale
                unitOfWork.UserRepository.Update(x => x.UserId == req.userId, x => new User()
                {
                    LastReceived = DateTime.Now
                });
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Loanbrief/UpdateRangeBoundTelesale Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #region Exceptions
        [HttpGet]
        [Route("get_list_exception")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DocumentException>>>> GetListException(int parentId)
        {
            var def = new DefaultResponse<Meta, List<DocumentException>>();
            try
            {
                var data = await unitOfWork.DocumentExceptionRepository.Query(x => x.IsEnable == true
                && x.ParentId == parentId, null, false).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetListException Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_propose_exception")]
        public async Task<ActionResult<DefaultResponse<Meta, ProposeExceptions>>> GetProposeException(int loanBriefId)
        {
            var def = new DefaultResponse<Meta, ProposeExceptions>();
            try
            {
                var data = await unitOfWork.ProposeExceptionsRepository.Query(x => x.LoanBriefId == loanBriefId, null, false).OrderByDescending(x => x.Id).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetProposeException Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_propose_exceptions")]
        public ActionResult<DefaultResponse<object>> AddProposeExceptions([FromBody] ProposeExceptionReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {

                //Insert
                unitOfWork.ProposeExceptionsRepository.Insert(new ProposeExceptions()
                {
                    LoanBriefId = entity.LoanBriefId,
                    UserId = entity.UserId,
                    DocId = entity.DocId,
                    DocName = entity.DocName,
                    ChildDocId = entity.ChildDocId,
                    ChildDocName = entity.ChildDocName,
                    Money = entity.Money,
                    Note = entity.Note,
                    CreateAt = DateTime.Now
                });

                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddProposeExceptions Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        #region Get list Loan Cancel by Momo
        [HttpPost]
        [Route("get_loan_cancel_momo")]
        public async Task<ActionResult<DefaultResponse<Meta, List<DocumentException>>>> GetLoanCancelMomo([FromBody] LoanBriefForDetail entity)
        {
            var def = new DefaultResponse<Meta, List<LoanCancelMomo>>();
            try
            {
                //var loanCurent = await unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanBriefId,null,false).Select(LoanCancelMomo.ProjectionViewDetail).FirstOrDefaultAsync();
                //if(loanCurent != null)
                //{
                var data = await unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.CANCELED
                && x.ReasonCancel == 759
                && x.LoanBriefCancelAt.Value.AddMonths(1) >= entity.CreatedTime
                && ((x.FullName == entity.FullName && x.Dob == entity.Dob) || x.NationalCard == entity.NationalCard)
                , null, false).Select(LoanCancelMomo.ProjectionViewDetail).ToListAsync();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
                //}                   

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanCancelMomo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        [HttpGet]
        [Route("get_loan_transaction_secured")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanTransactionsSecured>>> GetLoanTransactionSecured(int loanBriefId)
        {
            var def = new DefaultResponse<Meta, LoanTransactionsSecured>();
            try
            {
                var data = await unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanBriefId, null, false).Select(LoanTransactionsSecured.ProjectionViewDetail).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanTransactionSecured Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_total_loan_amount_secured")]
        public ActionResult<DefaultResponse<object>> GetTotalLoanAmountSecured(int customerId)
        {
            var def = new DefaultResponse<object>();
            try
            {

                var total = unitOfWork.LoanBriefRepository.Query(x => x.CustomerId == customerId
                && x.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                           && x.Status != (int)EnumLoanStatus.FINISH
                           && x.Status != (int)EnumLoanStatus.CANCELED
                           , null,false).Sum(x => x.LoanAmount);

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = total;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetTotalLoanAmountSecured Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_transaction_state")]
        public ActionResult<DefaultResponse<object>> UpdateTransactionState([FromBody] UpdateTransactionStateReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {

                unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief
                {
                    TransactionState = entity.Status
                });
                //unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateTransactionState Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}