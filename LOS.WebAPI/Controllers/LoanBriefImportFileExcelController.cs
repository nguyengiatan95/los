﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class LoanBriefImportFileExcelController : BaseController
    {
        private IUnitOfWork unitOfWork;
        public LoanBriefImportFileExcelController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpPost]
        [Route("add")]
        public ActionResult<DefaultResponse<object>> Add([FromBody] List<LoanBriefImportFileExcel> entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.LoanBriefImportFileExcelRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = 1;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefImportFileExcel/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                def.data = 0;
            }
            return Ok(def);
        }


        [HttpGet]
        [Route("search")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefImportFileExcelDetail>>> Search([FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefImportFileExcelDetail>>();
            try
            {
                var data = unitOfWork.LoanBriefImportFileExcelRepository.Query(x => x.CreateDate >= fromDate && x.CreateDate <= toDate, null, false).Select(LoanBriefImportFileExcelDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefImportFileExcel/Search Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_detail")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefImportFileExcelDetail>>> GetDetail([FromQuery] string? importFileId = null, [FromQuery] int? status = null)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefImportFileExcelDetail>>();
            try
            {
                var data = unitOfWork.LoanBriefImportFileExcelRepository.Query(x => x.FileImportId == importFileId && x.Status == status, null, false).Select(LoanBriefImportFileExcelDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefImportFileExcel/GetDetail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}