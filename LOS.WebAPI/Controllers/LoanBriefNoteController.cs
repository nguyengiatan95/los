﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class LoanBriefNoteController : ControllerBase
    {
        private IUnitOfWork unitOfWork;

        public LoanBriefNoteController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("get_notebyloanid")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefNoteDetail>>> GetNoteByLoanID(int loanBriefId)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefNoteDetail>>();
            try
            {
                var data = unitOfWork.LoanBriefNoteRepository.Query(x => x.LoanBriefId == loanBriefId && x.Status == 1, null, false).Select(LoanBriefNoteDetail.ProjectionDetail).ToList();
                data = data.OrderByDescending(x => x.LoanBriefNoteId).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefNote/GetNoteByLoanID Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("postloannote")]
        public ActionResult<DefaultResponse<object>> PostLoanNote([FromBody] LoanBriefNoteDTO note)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var result = unitOfWork.LoanBriefNoteRepository.Insert(note.Mapping());
                unitOfWork.Save();
                if (result.LoanBriefNoteId > 0)
                {
                    def.data = result.LoanBriefNoteId;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UsersMMO/PostUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}