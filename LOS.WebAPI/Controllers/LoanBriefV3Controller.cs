﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.Dapper;
using LOS.DAL.DTOs;
using LOS.DAL.DTOs.Loanbrief;
using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers.FilterHelper;
using LOS.DAL.Object;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using static LOS.DAL.Object.Tool.ToolReq;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class LoanBriefV3Controller : BaseController
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _baseConfig;
        public LoanBriefV3Controller(IUnitOfWork unitOfWork, IConfiguration baseConfig)
        {
            this._unitOfWork = unitOfWork;
            this._baseConfig = baseConfig;
        }

        [HttpPost]
        [Route("request_ai_v3/{id}")]
        public ActionResult<DefaultResponse<object>> RequestAIV3(int id, [FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != entity.LoanBriefId || entity.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }

                #region cập nhật thông tin KH
                _unitOfWork.CustomerRepository.Update(x => x.CustomerId == entity.CustomerId, x => new Customer()
                {
                    FullName = entity.FullName,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    Address = entity.LoanBriefResident.Address,
                    UpdatedAt = DateTime.Now,
                    Dob = entity.Dob,
                    Gender = entity.Gender,
                    NationalCard = entity.NationalCard,
                    AddressLatLong = entity.LoanBriefResident.AddressLatLng,
                    CompanyAddressLatLong = entity.LoanBriefJob.CompanyAddressLatLng

                });
                #endregion

                #region LoanBriefResident
                _unitOfWork.LoanBriefResidentRepository.Update(x => x.LoanBriefResidentId == entity.LoanBriefId, x => new LoanBriefResident()
                {
                    LoanBriefResidentId = entity.LoanBriefId,
                    Address = entity.LoanBriefResident != null ? entity.LoanBriefResident.Address : "",
                    AddressLatLng = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressLatLng : "",
                    AddressGoogleMap = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressGoogleMap : "",
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId
                });

                #endregion

                #region Loanbrief
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                {
                    FullName = entity.FullName,
                    NationalCard = entity.NationalCard,
                    Dob = entity.Dob,
                    Gender = entity.Gender,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                });
                #endregion

                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3/RequestAIV3 PUT Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("request_refphone_v3/{id}")]
        public ActionResult<DefaultResponse<object>> RequestRefphoneV3(int id, [FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != entity.LoanBriefId || entity.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                #region LoanBriefRelationship
                if (entity.LoanBriefRelationship != null && entity.LoanBriefRelationship.Count > 0)
                {
                    if (_unitOfWork.LoanBriefRelationshipRepository.Any(x => x.LoanBriefId == entity.LoanBriefId))
                        _unitOfWork.LoanBriefRelationshipRepository.Delete(x => x.LoanBriefId == entity.LoanBriefId);
                    foreach (var relationship in entity.LoanBriefRelationship)
                    {
                        if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                        {
                            _unitOfWork.LoanBriefRelationshipRepository.Insert(new LoanBriefRelationship()
                            {
                                LoanBriefId = entity.LoanBriefId,
                                RelationshipType = relationship.RelationshipType,
                                FullName = relationship.FullName,
                                Phone = relationship.Phone,
                                CreatedDate = DateTime.Now,
                                Address = relationship.Address
                            });
                        }
                    }
                }
                #endregion

                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;

            }
            catch (Exception ex)
            {
                //_unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBriefV3/RequestRefphoneV3 PUT Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("script_loanbrief/{id}")]
        public ActionResult<DefaultResponse<object>> UpdateScript(int id, [FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != entity.LoanBriefId || entity.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                #region cập nhật thông tin KH
                _unitOfWork.CustomerRepository.Update(x => x.CustomerId == entity.CustomerId, x => new Customer()
                {
                    FullName = entity.FullName,
                    JobId = entity.LoanBriefJob.JobId,
                    CompanyName = entity.LoanBriefJob.CompanyName,
                    CompanyAddress = entity.LoanBriefJob.CompanyAddress,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    Address = entity.LoanBriefResident.Address,
                    UpdatedAt = DateTime.Now,
                    //FacebookAddress = entity.Customer.FacebookAddress,
                    //InsurenceNumber = entity.Customer.InsurenceNumber,
                    //InsurenceNote = entity.Customer.InsurenceNote,
                    //IsMerried = entity.Customer.IsMerried,
                    //NumberBaby = entity.Customer.NumberBaby,
                    Dob = entity.Dob,
                    Gender = entity.Gender,
                    NationalCard = entity.NationalCard,
                    NationCardPlace = entity.NationCardPlace,
                    //Passport = entity.Passport,
                    AddressLatLong = entity.LoanBriefResident.AddressLatLng,
                    CompanyAddressLatLong = entity.LoanBriefJob.CompanyAddressLatLng

                });
                #endregion

                #region LoanBriefResident
                if (_unitOfWork.LoanBriefResidentRepository.Any(x => x.LoanBriefResidentId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefResidentRepository.Update(x => x.LoanBriefResidentId == entity.LoanBriefId, x => new LoanBriefResident()
                    {
                        LoanBriefResidentId = entity.LoanBriefId,
                        //LivingTime = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingTime : 0,
                        ResidentType = entity.LoanBriefResident != null ? entity.LoanBriefResident.ResidentType : 0,
                        //LivingWith = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingWith : 0,
                        Address = entity.LoanBriefResident != null ? entity.LoanBriefResident.Address : "",
                        AddressLatLng = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressLatLng : "",
                        AddressGoogleMap = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressGoogleMap : "",
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId,
                        BillElectricityId = entity.LoanBriefResident.BillElectricityId,
                        BillWaterId = entity.LoanBriefResident.BillWaterId,
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefResidentRepository.Insert(new LoanBriefResident()
                    {
                        LoanBriefResidentId = entity.LoanBriefId,
                        //LivingTime = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingTime : 0,
                        ResidentType = entity.LoanBriefResident != null ? entity.LoanBriefResident.ResidentType : 0,
                        //LivingWith = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingWith : 0,
                        Address = entity.LoanBriefResident != null ? entity.LoanBriefResident.Address : "",
                        AddressLatLng = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressLatLng : "",
                        AddressGoogleMap = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressGoogleMap : "",
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId,
                        BillElectricityId = entity.LoanBriefResident.BillElectricityId,
                        BillWaterId = entity.LoanBriefResident.BillWaterId,
                    });
                }
                #endregion

                #region LoanBriefProperty
                if (_unitOfWork.LoanBriefPropertyRepository.Any(x => x.LoanBriefPropertyId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefPropertyRepository.Update(x => x.LoanBriefPropertyId == entity.LoanBriefId, x => new LoanBriefProperty()
                    {
                        LoanBriefPropertyId = entity.LoanBriefId,
                        BrandId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BrandId : 0,
                        ProductId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.ProductId : 0,
                        PlateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumber : "",
                        UpdatedTime = DateTime.Now,
                        CarManufacturer = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarManufacturer : "",
                        CarName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarName : "",
                        CarColor = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarColor : "",
                        PlateNumberCar = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumberCar : "",
                        BuyInsuranceProperty = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BuyInsuranceProperty : false
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefPropertyRepository.Insert(new LoanBriefProperty()
                    {
                        LoanBriefPropertyId = entity.LoanBriefId,
                        BrandId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BrandId : 0,
                        ProductId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.ProductId : 0,
                        PlateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumber : "",
                        CreatedTime = DateTime.Now,
                        CarManufacturer = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarManufacturer : "",
                        CarName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarName : "",
                        CarColor = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarColor : "",
                        PlateNumberCar = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumberCar : "",
                        BuyInsuranceProperty = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BuyInsuranceProperty : false
                    });
                }

                #endregion

                #region LoanBriefHousehold
                if (_unitOfWork.LoanBriefHouseholdRepository.Any(x => x.LoanBriefHouseholdId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefHouseholdRepository.Update(x => x.LoanBriefHouseholdId == entity.LoanBriefId, x => new LoanBriefHousehold()
                    {
                        LoanBriefHouseholdId = entity.LoanBriefId,
                        Address = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.Address : "",
                        ProvinceId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.ProvinceId : 0,
                        DistrictId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.DistrictId : 0,
                        WardId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.WardId : 0,
                        FullNameHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.FullNameHouseOwner : null,
                        RelationshipHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.RelationshipHouseOwner : null,
                        BirdayHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.BirdayHouseOwner : null,
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefHouseholdRepository.Insert(new LoanBriefHousehold()
                    {
                        LoanBriefHouseholdId = entity.LoanBriefId,
                        Address = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.Address : "",
                        ProvinceId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.ProvinceId : 0,
                        DistrictId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.DistrictId : 0,
                        WardId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.WardId : 0,
                        FullNameHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.FullNameHouseOwner : null,
                        RelationshipHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.RelationshipHouseOwner : null,
                        BirdayHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.BirdayHouseOwner : null,
                    });
                }

                #endregion

                #region LoanBriefJob
                if (_unitOfWork.LoanBriefJobRepository.Any(x => x.LoanBriefJobId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefJobRepository.Update(x => x.LoanBriefJobId == entity.LoanBriefId, x => new LoanBriefJob()
                    {
                        LoanBriefJobId = entity.LoanBriefId,
                        JobId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobId : 0,
                        CompanyName = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyName : "",
                        //CompanyPhone = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyPhone : "",
                        //CompanyTaxCode = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyTaxCode : "",
                        CompanyProvinceId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyProvinceId : 0,
                        CompanyDistrictId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyDistrictId : 0,
                        CompanyWardId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyWardId : 0,
                        CompanyAddress = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddress : "",
                        //CompanyAddressGoogleMap = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressGoogleMap : "",
                        //CompanyAddressLatLng = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressLatLng : "",
                        TotalIncome = entity.LoanBriefJob != null ? entity.LoanBriefJob.TotalIncome : 0,
                        ImcomeType = entity.LoanBriefJob != null ? entity.LoanBriefJob.ImcomeType : 0,
                        Description = entity.LoanBriefJob != null ? entity.LoanBriefJob.Description : "",
                        CompanyInsurance = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyInsurance : null,
                        BusinessPapers = entity.LoanBriefJob != null ? entity.LoanBriefJob.BusinessPapers : null,
                        WorkLocation = entity.LoanBriefJob != null ? entity.LoanBriefJob.WorkLocation : null,
                        JobDescriptionId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobDescriptionId : null,
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefJobRepository.Insert(new LoanBriefJob()
                    {
                        LoanBriefJobId = entity.LoanBriefId,
                        JobId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobId : 0,
                        CompanyName = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyName : "",
                        //CompanyPhone = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyPhone : "",
                        //CompanyTaxCode = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyTaxCode : "",
                        CompanyProvinceId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyProvinceId : 0,
                        CompanyDistrictId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyDistrictId : 0,
                        CompanyWardId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyWardId : 0,
                        CompanyAddress = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddress : "",
                        TotalIncome = entity.LoanBriefJob != null ? entity.LoanBriefJob.TotalIncome : 0,
                        ImcomeType = entity.LoanBriefJob != null ? entity.LoanBriefJob.ImcomeType : 0,
                        Description = entity.LoanBriefJob != null ? entity.LoanBriefJob.Description : "",
                        //CompanyAddressGoogleMap = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressGoogleMap : "",
                        //CompanyAddressLatLng = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressLatLng : "",
                        CompanyInsurance = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyInsurance : null,
                        BusinessPapers = entity.LoanBriefJob != null ? entity.LoanBriefJob.BusinessPapers : null,
                        WorkLocation = entity.LoanBriefJob != null ? entity.LoanBriefJob.WorkLocation : null,
                        JobDescriptionId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobDescriptionId : null,
                    });
                }
                #endregion

                #region LoanBriefCompany
                if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    if (_unitOfWork.LoanBriefCompanyRepository.Any(x => x.LoanBriefId == entity.LoanBriefId))
                    {
                        _unitOfWork.LoanBriefCompanyRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBriefCompany()
                        {
                            LoanBriefId = entity.LoanBriefId,
                            CompanyName = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyName : "",
                            CareerBusiness = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CareerBusiness : "",
                            BusinessCertificationAddress = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.BusinessCertificationAddress : "",
                            HeadOffice = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.HeadOffice : "",
                            Address = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.Address : "",
                            CompanyShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyShareholder : "",
                            CardNumberShareholderDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.CardNumberShareholderDate.HasValue
                                    ? entity.LoanBriefCompany.CardNumberShareholderDate : null,
                            BusinessCertificationDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BusinessCertificationDate.HasValue
                                    ? entity.LoanBriefCompany.BusinessCertificationDate : null,
                            BirthdayShareholder = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BirthdayShareholder.HasValue
                                    ? entity.LoanBriefCompany.BirthdayShareholder : null,
                            CardNumberShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CardNumberShareholder : "",
                            PlaceOfBirthShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.PlaceOfBirthShareholder : "",
                            UpdatedDate = DateTime.Now
                        });
                    }
                    else
                    {
                        _unitOfWork.LoanBriefCompanyRepository.Insert(new LoanBriefCompany()
                        {
                            LoanBriefId = entity.LoanBriefId,
                            CompanyName = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyName : "",
                            CareerBusiness = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CareerBusiness : "",
                            BusinessCertificationAddress = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.BusinessCertificationAddress : "",
                            HeadOffice = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.HeadOffice : "",
                            Address = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.Address : "",
                            CompanyShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyShareholder : "",
                            CardNumberShareholderDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.CardNumberShareholderDate.HasValue
                                    ? entity.LoanBriefCompany.CardNumberShareholderDate : null,
                            BusinessCertificationDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BusinessCertificationDate.HasValue
                                    ? entity.LoanBriefCompany.BusinessCertificationDate : null,
                            BirthdayShareholder = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BirthdayShareholder.HasValue
                                    ? entity.LoanBriefCompany.BirthdayShareholder : null,
                            CardNumberShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CardNumberShareholder : "",
                            PlaceOfBirthShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.PlaceOfBirthShareholder : "",
                            CreatedDate = DateTime.Now
                        });
                    }

                }
                #endregion

                #region LoanBriefRelationship
                //if (entity.LoanBriefRelationship != null && entity.LoanBriefRelationship.Count > 0)
                //{
                //    var listRelationshipId = new List<int>();
                //    foreach (var relationship in entity.LoanBriefRelationship)
                //    {

                //        if (relationship.Id > 0)
                //        {
                //            listRelationshipId.Add(relationship.Id);
                //            _unitOfWork.LoanBriefRelationshipRepository.Update(x => x.Id == relationship.Id, x => new LoanBriefRelationship()
                //            {
                //                LoanBriefId = entity.LoanBriefId,
                //                RelationshipType = relationship.RelationshipType,
                //                FullName = relationship.FullName,
                //                Phone = relationship.Phone,
                //                //Address = relationship.Address,
                //                //RefPhoneCallRate = relationship.RefPhoneCallRate,
                //                UpdatedDate = DateTime.Now
                //            });
                //        }
                //        else
                //        {
                //            //kiểm tra xe có bản ghi nào không
                //            var relationshipRerord = _unitOfWork.LoanBriefRelationshipRepository.Query(x => x.LoanBriefId == entity.LoanBriefId
                //            && (listRelationshipId == null || listRelationshipId.Count == 0 || !listRelationshipId.Contains(x.Id)), null, false).FirstOrDefault();
                //            if (relationshipRerord != null && relationshipRerord.Id > 0)
                //            {
                //                listRelationshipId.Add(relationshipRerord.Id);
                //                _unitOfWork.LoanBriefRelationshipRepository.Update(x => x.Id == relationshipRerord.Id, x => new LoanBriefRelationship()
                //                {
                //                    LoanBriefId = entity.LoanBriefId,
                //                    RelationshipType = relationship.RelationshipType,
                //                    FullName = relationship.FullName,
                //                    Phone = relationship.Phone,
                //                    //Address = relationship.Address,
                //                    //RefPhoneCallRate = relationship.RefPhoneCallRate,
                //                    UpdatedDate = DateTime.Now
                //                });
                //            }
                //            else
                //            {
                //                var objRelationShip = new LoanBriefRelationship()
                //                {
                //                    LoanBriefId = entity.LoanBriefId,
                //                    RelationshipType = relationship.RelationshipType,
                //                    FullName = relationship.FullName,
                //                    Phone = relationship.Phone,
                //                    Address = relationship.Address,
                //                    RefPhoneCallRate = relationship.RefPhoneCallRate,
                //                    CreatedDate = DateTime.Now
                //                };
                //                _unitOfWork.LoanBriefRelationshipRepository.Insert(objRelationShip);
                //                _unitOfWork.Save();
                //                listRelationshipId.Add(objRelationShip.Id);
                //            }
                //        }

                //    }
                //    if (entity.LoanBriefRelationship.Count == 1)
                //    {
                //        _unitOfWork.LoanBriefRelationshipRepository.Insert(new LoanBriefRelationship()
                //        {
                //            LoanBriefId = entity.LoanBriefId
                //        });
                //    }
                //}
                //else
                //{
                //    _unitOfWork.LoanBriefRelationshipRepository.Insert(new LoanBriefRelationship()
                //    {
                //        LoanBriefId = entity.LoanBriefId
                //    });
                //    _unitOfWork.LoanBriefRelationshipRepository.Insert(new LoanBriefRelationship()
                //    {
                //        LoanBriefId = entity.LoanBriefId
                //    });
                //}
                #endregion

                #region LoanBriefQuestionScript
                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBriefQuestionScript()
                    {
                        QuestionUseMotobikeGo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionUseMotobikeGo : null,
                        QuestionMotobikeCertificate = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionMotobikeCertificate : null,
                        QuestionAccountBank = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionAccountBank : null,
                        TypeJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeJob : null,
                        AdvisoryDeductInsurance = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisoryDeductInsurance : null,
                        AdvisoryKeepRegisteredMotobike = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisoryKeepRegisteredMotobike : null,
                        AdvisorySettingGps = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisorySettingGps : null,
                        ChooseCareer = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.ChooseCareer : null,
                        Signs = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.Signs : null,
                        AddressBusiness = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AddressBusiness : null,
                        WorkingTime = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.WorkingTime : null,
                        DriverOfCompany = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DriverOfCompany : null,
                        AccountDriver = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AccountDriver : null,
                        QuestionDriverGo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionDriverGo : null,
                        TimeDriver = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TimeDriver : null,
                        ContractInShop = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.ContractInShop : null,
                        PhotographShop = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.PhotographShop : null,
                        CommentStatusTelesales = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.CommentStatusTelesales : null,
                        QuestionBorrow = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionBorrow : null,
                        SigningAlaborContract = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.SigningAlaborContract : null,
                        TypeWork = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeWork : null,
                        LatestPayday = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.LatestPayday : null,
                        NameWeb = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.NameWeb : null,
                        QuestionWarehouse = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionWarehouse : null,
                        QuestionImageGoods = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageGoods : null,
                        DetailJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DetailJob : null,
                        TimeSellWeb = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TimeSellWeb : null,
                        QuestionCarOwnership = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionCarOwnership : null,
                        QuestionChangeColorMotobike = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionChangeColorMotobike : null,
                        QuestionUseMotobikeGoNormal = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionUseMotobikeGoNormal : null,
                        QuestionAddress = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionAddress : null,
                        QuestionCheckCurrentStatusMotobike = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionCheckCurrentStatusMotobike : null,
                        TypeOwnerShip = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeOwnerShip : null,
                        DocumentCoincideHouseHold = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentCoincideHouseHold : null,
                        DocumentNoCoincideHouseHold = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentNoCoincideHouseHold : null,
                        StartWokingJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.StartWokingJob : null,
                        QuestionNowInWorkingJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionNowInWorkingJob : null,
                        QuestionImageWorkplace = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageWorkplace : null,
                        DocumentSalaryBankTransfer = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentSalaryBankTransfer : null,
                        DocumetSalaryCash = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumetSalaryCash : null,
                        QuestionLicenseDkkd = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionLicenseDkkd : null,
                        CommentJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.CommentJob : null,
                        DocmentRelationship = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocmentRelationship : null,
                        QuestionBlurredSignCertificate = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionBlurredSignCertificate : null,
                        HourOpenDoor = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.HourOpenDoor : null,
                        QuestionProvidedImageOrVideo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionProvidedImageOrVideo : null,
                        WaterSupplier = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.WaterSupplier : null,
                        RemindCustomerDocument = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.RemindCustomerDocument : null,
                        QuestionHouseOwner = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionHouseOwner : null,
                        DocumentFormsOfResidence = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentFormsOfResidence : null,
                        BusinessStatus = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.BusinessStatus : null,
                        QuestionBusinessLocation = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionBusinessLocation : null,
                        QuestionImageAppAccountRevenue = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageAppAccountRevenue : null,
                        DocumentBusiness = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentBusiness : null,
                        QuestionAddressCoincideAreaSupport = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionAddressCoincideAreaSupport : null,
                        QuestionImageRegistrationBook = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageRegistrationBook : null,
                        Appraiser = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.Appraiser : null,
                        MaxPrice = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.MaxPrice : null,
                        QuestionOriginCardNumber = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionOriginCardNumber : null,
                        QuestionOriginCavet = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionOriginCavet : null,
                        QuestionMotobikeNearby = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionMotobikeNearby : null,
                        QuestionDocumentJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionDocumentJob : null,
                        QuestionCustomerBorrowProductFast4tr = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionCustomerBorrowProductFast4tr : null,
                        LoanAmountFirst = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.LoanAmountFirst : null,
                        QuestionSupportArea = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionSupportArea : null,
                        OwnerCar = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.OwnerCar : null,
                        QuestionSaleContractCar = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionSaleContractCar : null,
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                    {
                        LoanBriefId = entity.LoanBriefId,
                        QuestionUseMotobikeGo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionUseMotobikeGo : null,
                        QuestionMotobikeCertificate = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionMotobikeCertificate : null,
                        QuestionAccountBank = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionAccountBank : null,
                        TypeJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeJob : null,
                        AdvisoryDeductInsurance = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisoryDeductInsurance : null,
                        AdvisoryKeepRegisteredMotobike = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisoryKeepRegisteredMotobike : null,
                        AdvisorySettingGps = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisorySettingGps : null,
                        ChooseCareer = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.ChooseCareer : null,
                        Signs = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.Signs : null,
                        AddressBusiness = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AddressBusiness : null,
                        WorkingTime = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.WorkingTime : null,
                        DriverOfCompany = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DriverOfCompany : null,
                        AccountDriver = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AccountDriver : null,
                        QuestionDriverGo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionDriverGo : null,
                        TimeDriver = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TimeDriver : null,
                        ContractInShop = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.ContractInShop : null,
                        PhotographShop = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.PhotographShop : null,
                        CommentStatusTelesales = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.CommentStatusTelesales : null,
                        QuestionBorrow = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionBorrow : null,
                        SigningAlaborContract = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.SigningAlaborContract : null,
                        TypeWork = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeWork : null,
                        LatestPayday = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.LatestPayday : null,
                        NameWeb = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.NameWeb : null,
                        QuestionWarehouse = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionWarehouse : null,
                        QuestionImageGoods = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageGoods : null,
                        DetailJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DetailJob : null,
                        TimeSellWeb = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TimeSellWeb : null,
                        QuestionCarOwnership = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionCarOwnership : null,
                        QuestionChangeColorMotobike = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionChangeColorMotobike : null,
                        QuestionUseMotobikeGoNormal = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionUseMotobikeGoNormal : null,
                        QuestionAddress = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionAddress : null,
                        QuestionCheckCurrentStatusMotobike = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionCheckCurrentStatusMotobike : null,
                        TypeOwnerShip = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeOwnerShip : null,
                        DocumentCoincideHouseHold = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentCoincideHouseHold : null,
                        DocumentNoCoincideHouseHold = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentNoCoincideHouseHold : null,
                        StartWokingJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.StartWokingJob : null,
                        QuestionNowInWorkingJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionNowInWorkingJob : null,
                        QuestionImageWorkplace = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageWorkplace : null,
                        DocumentSalaryBankTransfer = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentSalaryBankTransfer : null,
                        DocumetSalaryCash = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumetSalaryCash : null,
                        QuestionLicenseDkkd = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionLicenseDkkd : null,
                        CommentJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.CommentJob : null,
                        DocmentRelationship = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocmentRelationship : null,
                        QuestionBlurredSignCertificate = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionBlurredSignCertificate : null,
                        HourOpenDoor = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.HourOpenDoor : null,
                        QuestionProvidedImageOrVideo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionProvidedImageOrVideo : null,
                        WaterSupplier = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.WaterSupplier : null,
                        RemindCustomerDocument = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.RemindCustomerDocument : null,
                        QuestionHouseOwner = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionHouseOwner : null,
                        DocumentFormsOfResidence = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentFormsOfResidence : null,
                        BusinessStatus = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.BusinessStatus : null,
                        QuestionBusinessLocation = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionBusinessLocation : null,
                        QuestionImageAppAccountRevenue = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageAppAccountRevenue : null,
                        DocumentBusiness = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentBusiness : null,
                        QuestionAddressCoincideAreaSupport = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionAddressCoincideAreaSupport : null,
                        QuestionImageRegistrationBook = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageRegistrationBook : null,
                        Appraiser = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.Appraiser : null,
                        MaxPrice = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.MaxPrice : null,
                        QuestionOriginCardNumber = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionOriginCardNumber : null,
                        QuestionOriginCavet = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionOriginCavet : null,
                        QuestionMotobikeNearby = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionMotobikeNearby : null,
                        QuestionDocumentJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionDocumentJob : null,
                        QuestionCustomerBorrowProductFast4tr = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionCustomerBorrowProductFast4tr : null,
                        LoanAmountFirst = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.LoanAmountFirst : null,
                        QuestionSupportArea = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionSupportArea : null,
                        OwnerCar = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.OwnerCar : null,
                        QuestionSaleContractCar = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionSaleContractCar : null,
                    });
                }
                #endregion

                #region Loanbrief
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                {
                    //PlatformType = entity.PlatformType.GetValueOrDefault(),
                    //TypeLoanBrief = entity.TypeLoanBrief.GetValueOrDefault(),
                    FullName = entity.FullName,
                    //Phone = entity.Phone,
                    NationalCard = entity.NationalCard,
                    NationCardPlace = entity.NationCardPlace,
                    IsTrackingLocation = entity.IsTrackingLocation,
                    Dob = entity.Dob,
                    Passport = entity.Passport,
                    Gender = entity.Gender,
                    NumberCall = entity.NumberCall,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    ProductId = entity.ProductId.GetValueOrDefault(),
                    RateTypeId = entity.RateTypeId.GetValueOrDefault(),
                    LoanAmount = entity.LoanAmount.GetValueOrDefault(),
                    BuyInsurenceCustomer = entity.BuyInsurenceCustomer,
                    LoanTime = entity.LoanTime,
                    Frequency = entity.Frequency,
                    RateMoney = entity.RateMoney,
                    RatePercent = entity.RatePercent,
                    //ReceivingMoneyType = entity.ReceivingMoneyType,
                    //BankId = entity.BankId,
                    //BankAccountNumber = entity.BankAccountNumber,
                    //BankCardNumber = entity.BankCardNumber,
                    //BankAccountName = entity.BankAccountName,
                    IsLocate = entity.IsLocate,
                    //TypeRemarketing = entity.TypeRemarketing,
                    FromDate = entity.FromDate,
                    ToDate = entity.ToDate,
                    //CreatedTime = entity.CreatedTime.HasValue ? entity.CreatedTime.Value.DateTime : DateTime.MinValue,
                    //IsHeadOffice = entity.IsHeadOffice,
                    //HubId = entity.HubId,
                    // CreateBy = entity.CreateBy,
                    //BoundTelesaleId = entity.BoundTelesaleId,
                    //HubEmployeeId = entity.HubEmployeeId,
                    PresenterCode = entity.PresenterCode,
                    LoanAmountFirst = entity.LoanAmountFirst,
                    LoanAmountExpertise = entity.LoanAmountExpertise,
                    LoanAmountExpertiseLast = entity.LoanAmountExpertiseLast,
                    LoanAmountExpertiseAi = entity.LoanAmountExpertiseAi,
                    FirstProcessingTime = entity.FirstProcessingTime,
                    LoanPurpose = entity.LoanPurpose,
                    ValueCheckQualify = entity.ValueCheckQualify,
                    ProductDetailId = entity.ProductDetailId,
                    FeePaymentBeforeLoan = entity.FeePaymentBeforeLoan
                });
                #endregion
                _unitOfWork.Save();
                //_unitOfWork.CommitTransaction();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;

            }
            catch (Exception ex)
            {
                //_unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBriefV3/UpdateScript PUT Exception");
                Log.Information("Info UpdateScript: " + Newtonsoft.Json.JsonConvert.SerializeObject(entity));
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpPost]
        [Route("create_loanbrief_script")]
        public ActionResult<DefaultResponse<object>> CreateLoanbriefScript([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.BeginTransaction();
                #region customer               
                var customerOld = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == entity.Phone);
                if (customerOld != null && customerOld.CustomerId > 0)
                {
                    _unitOfWork.CustomerRepository.Update(x => x.CustomerId == customerOld.CustomerId, x => new Customer()
                    {
                        FullName = entity.FullName,
                        Phone = entity.Phone,
                        JobId = entity.LoanBriefJob.JobId,
                        CompanyName = entity.LoanBriefJob.CompanyName,
                        CompanyAddress = entity.LoanBriefJob.CompanyAddress,
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId,
                        Address = entity.LoanBriefResident.Address,
                        UpdatedAt = DateTime.Now,
                        InsurenceNumber = entity.Customer.InsurenceNumber,
                        InsurenceNote = entity.Customer.InsurenceNote,
                        IsMerried = entity.Customer.IsMerried,
                        NumberBaby = entity.Customer.NumberBaby,
                        Dob = entity.Dob,
                        Gender = entity.Gender,
                        NationalCard = entity.NationalCard,
                        NationCardPlace = entity.NationCardPlace,
                        Passport = entity.Passport
                    });
                    entity.CustomerId = customerOld.CustomerId;
                }
                else
                {
                    var customer = new Customer()
                    {
                        FullName = entity.FullName,
                        JobId = entity.LoanBriefJob.JobId,
                        CompanyName = entity.LoanBriefJob.CompanyName,
                        CompanyAddress = entity.LoanBriefJob.CompanyAddress,
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId,
                        Address = entity.LoanBriefResident.Address,
                        CreatedAt = DateTime.Now,
                        InsurenceNumber = entity.Customer.InsurenceNumber,
                        InsurenceNote = entity.Customer.InsurenceNote,
                        IsMerried = entity.Customer.IsMerried,
                        NumberBaby = entity.Customer.NumberBaby,
                        Phone = entity.Phone,
                        Dob = entity.Dob,
                        Gender = entity.Gender,
                        NationalCard = entity.NationalCard,
                        NationCardPlace = entity.NationCardPlace,
                        Passport = entity.Passport
                    };
                    _unitOfWork.CustomerRepository.Insert(customer);
                    _unitOfWork.Save();
                    entity.CustomerId = customer.CustomerId;
                }
                #endregion

                #region Loanbrief
                var loanbrief = new LoanBrief()
                {
                    PlatformType = entity.PlatformType.GetValueOrDefault(),
                    TypeLoanBrief = entity.TypeLoanBrief.GetValueOrDefault(),
                    FullName = entity.FullName,
                    Phone = entity.Phone,
                    NationalCard = entity.NationalCard,
                    NationCardPlace = entity.NationCardPlace,
                    IsTrackingLocation = entity.IsTrackingLocation,
                    Dob = entity.Dob,
                    Passport = entity.Passport,
                    Gender = entity.Gender,
                    NumberCall = entity.NumberCall,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    ProductId = entity.ProductId.GetValueOrDefault(),
                    RateTypeId = entity.RateTypeId.GetValueOrDefault(),
                    LoanAmount = entity.LoanAmount.GetValueOrDefault(),
                    BuyInsurenceCustomer = entity.BuyInsurenceCustomer,
                    LoanTime = entity.LoanTime,
                    Frequency = entity.Frequency,
                    RateMoney = entity.RateMoney,
                    RatePercent = entity.RatePercent,
                    ReceivingMoneyType = entity.ReceivingMoneyType,
                    BankId = entity.BankId,
                    BankAccountNumber = entity.BankAccountNumber,
                    BankCardNumber = entity.BankCardNumber,
                    BankAccountName = entity.BankAccountName,
                    IsLocate = entity.IsLocate,
                    FromDate = entity.FromDate,
                    ToDate = entity.ToDate,
                    CreatedTime = entity.CreatedTime.HasValue ? entity.CreatedTime.Value.DateTime : DateTime.Now,
                    IsHeadOffice = entity.IsHeadOffice,
                    HubId = entity.HubId,
                    CreateBy = entity.CreateBy,
                    BoundTelesaleId = entity.BoundTelesaleId,
                    HubEmployeeId = entity.HubEmployeeId,
                    PresenterCode = entity.PresenterCode,
                    LoanAmountFirst = entity.LoanAmountFirst,
                    LoanAmountExpertise = entity.LoanAmountExpertise,
                    LoanAmountExpertiseLast = entity.LoanAmountExpertiseLast,
                    LoanAmountExpertiseAi = entity.LoanAmountExpertiseAi,
                    FirstProcessingTime = entity.FirstProcessingTime,
                    LoanPurpose = entity.LoanPurpose,
                    ValueCheckQualify = entity.ValueCheckQualify,
                    ReMarketingLoanBriefId = entity.ReMarketingLoanBriefId,
                    Status = EnumLoanStatus.INIT.GetHashCode(),
                    CustomerId = entity.CustomerId,
                    UtmSource = entity.UtmSource,
                    TeamTelesalesId = entity.TeamTelesalesId,
                };
                _unitOfWork.LoanBriefRepository.Insert(loanbrief);
                _unitOfWork.Save();
                entity.LoanBriefId = loanbrief.LoanBriefId;
                #endregion

                #region LoanBriefResident
                _unitOfWork.LoanBriefResidentRepository.Insert(new LoanBriefResident()
                {
                    LoanBriefResidentId = entity.LoanBriefId,
                    LivingTime = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingTime : 0,
                    ResidentType = entity.LoanBriefResident != null ? entity.LoanBriefResident.ResidentType : 0,
                    LivingWith = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingWith : 0,
                    Address = entity.LoanBriefResident != null ? entity.LoanBriefResident.Address : "",
                    AddressLatLng = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressLatLng : "",
                    AddressGoogleMap = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressGoogleMap : "",
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    BillElectricityId = entity.LoanBriefResident.BillElectricityId,
                    BillWaterId = entity.LoanBriefResident.BillWaterId,
                });
                #endregion

                #region LoanBriefProperty
                _unitOfWork.LoanBriefPropertyRepository.Insert(new LoanBriefProperty()
                {
                    LoanBriefPropertyId = entity.LoanBriefId,
                    BrandId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BrandId : 0,
                    ProductId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.ProductId : 0,
                    PlateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumber : "",
                    CreatedTime = DateTime.Now,
                    CarManufacturer = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarManufacturer : "",
                    CarName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarName : "",
                    CarColor = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarColor : "",
                    PlateNumberCar = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumberCar : ""
                });

                #endregion

                #region LoanBriefHousehold
                _unitOfWork.LoanBriefHouseholdRepository.Insert(new LoanBriefHousehold()
                {
                    LoanBriefHouseholdId = entity.LoanBriefId,
                    Address = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.Address : "",
                    ProvinceId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.ProvinceId : 0,
                    DistrictId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.DistrictId : 0,
                    WardId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.WardId : 0,
                });

                #endregion

                #region LoanBriefJob
                _unitOfWork.LoanBriefJobRepository.Insert(new LoanBriefJob()
                {
                    LoanBriefJobId = entity.LoanBriefId,
                    JobId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobId : 0,
                    CompanyName = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyName : "",
                    CompanyPhone = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyPhone : "",
                    CompanyTaxCode = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyTaxCode : "",
                    CompanyProvinceId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyProvinceId : 0,
                    CompanyDistrictId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyDistrictId : 0,
                    CompanyWardId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyWardId : 0,
                    CompanyAddress = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddress : "",
                    TotalIncome = entity.LoanBriefJob != null ? entity.LoanBriefJob.TotalIncome : 0,
                    ImcomeType = entity.LoanBriefJob != null ? entity.LoanBriefJob.ImcomeType : 0,
                    Description = entity.LoanBriefJob != null ? entity.LoanBriefJob.Description : "",
                    CompanyAddressGoogleMap = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressGoogleMap : "",
                    CompanyAddressLatLng = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressLatLng : "",
                    CompanyInsurance = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyInsurance : null,
                    BusinessPapers = entity.LoanBriefJob != null ? entity.LoanBriefJob.BusinessPapers : null,
                    WorkLocation = entity.LoanBriefJob != null ? entity.LoanBriefJob.WorkLocation : null,
                    JobDescriptionId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobDescriptionId : null,
                });
                #endregion

                #region LoanBriefCompany
                if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    _unitOfWork.LoanBriefCompanyRepository.Insert(new LoanBriefCompany()
                    {
                        LoanBriefId = entity.LoanBriefId,
                        CompanyName = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyName : "",
                        CareerBusiness = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CareerBusiness : "",
                        BusinessCertificationAddress = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.BusinessCertificationAddress : "",
                        HeadOffice = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.HeadOffice : "",
                        Address = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.Address : "",
                        CompanyShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyShareholder : "",
                        CardNumberShareholderDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.CardNumberShareholderDate.HasValue
                                   ? entity.LoanBriefCompany.CardNumberShareholderDate : null,
                        BusinessCertificationDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BusinessCertificationDate.HasValue
                                   ? entity.LoanBriefCompany.BusinessCertificationDate : null,
                        BirthdayShareholder = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BirthdayShareholder.HasValue
                                   ? entity.LoanBriefCompany.BirthdayShareholder : null,
                        CardNumberShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CardNumberShareholder : "",
                        PlaceOfBirthShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.PlaceOfBirthShareholder : "",
                        CreatedDate = DateTime.Now
                    });

                }
                #endregion

                #region LoanBriefRelationship
                if (entity.LoanBriefRelationship != null && entity.LoanBriefRelationship.Count > 0)
                {
                    foreach (var relationship in entity.LoanBriefRelationship)
                    {
                        if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                            _unitOfWork.LoanBriefRelationshipRepository.Insert(new LoanBriefRelationship()
                            {
                                LoanBriefId = entity.LoanBriefId,
                                RelationshipType = relationship.RelationshipType,
                                FullName = relationship.FullName,
                                Phone = relationship.Phone,
                                Address = relationship.Address,
                                RefPhoneCallRate = relationship.RefPhoneCallRate,
                                CreatedDate = DateTime.Now
                            });
                    }

                }

                #endregion

                #region LoanBriefQuestionScript
                _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                {
                    LoanBriefId = entity.LoanBriefId,
                    QuestionUseMotobikeGo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionUseMotobikeGo : null,
                    QuestionMotobikeCertificate = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionMotobikeCertificate : null,
                    QuestionAccountBank = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionAccountBank : null,
                    TypeJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeJob : null,
                    AdvisoryDeductInsurance = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisoryDeductInsurance : null,
                    AdvisoryKeepRegisteredMotobike = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisoryKeepRegisteredMotobike : null,
                    AdvisorySettingGps = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisorySettingGps : null,
                    ChooseCareer = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.ChooseCareer : null,
                    Signs = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.Signs : null,
                    AddressBusiness = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AddressBusiness : null,
                    WorkingTime = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.WorkingTime : null,
                    DriverOfCompany = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DriverOfCompany : null,
                    AccountDriver = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AccountDriver : null,
                    QuestionDriverGo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionDriverGo : null,
                    TimeDriver = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TimeDriver : null,
                    ContractInShop = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.ContractInShop : null,
                    PhotographShop = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.PhotographShop : null,
                    CommentStatusTelesales = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.CommentStatusTelesales : null,
                    QuestionBorrow = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionBorrow : null,
                    SigningAlaborContract = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.SigningAlaborContract : null,
                    TypeWork = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeWork : null,
                    LatestPayday = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.LatestPayday : null,
                    NameWeb = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.NameWeb : null,
                    QuestionWarehouse = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionWarehouse : null,
                    QuestionImageGoods = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageGoods : null,
                    DetailJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DetailJob : null,
                    TimeSellWeb = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TimeSellWeb : null,
                    QuestionCarOwnership = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionCarOwnership : null,
                    QuestionChangeColorMotobike = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionChangeColorMotobike : null,
                    QuestionUseMotobikeGoNormal = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionUseMotobikeGoNormal : null,
                    QuestionAddress = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionAddress : null,
                    QuestionCheckCurrentStatusMotobike = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionCheckCurrentStatusMotobike : null,
                    TypeOwnerShip = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeOwnerShip : null,
                    DocumentCoincideHouseHold = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentCoincideHouseHold : null,
                    DocumentNoCoincideHouseHold = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentNoCoincideHouseHold : null,
                    StartWokingJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.StartWokingJob : null,
                    QuestionNowInWorkingJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionNowInWorkingJob : null,
                    QuestionImageWorkplace = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageWorkplace : null,
                    DocumentSalaryBankTransfer = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentSalaryBankTransfer : null,
                    DocumetSalaryCash = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumetSalaryCash : null,
                    QuestionLicenseDkkd = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionLicenseDkkd : null,
                    CommentJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.CommentJob : null,
                    DocmentRelationship = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocmentRelationship : null,
                    QuestionBlurredSignCertificate = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionBlurredSignCertificate : null,
                    HourOpenDoor = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.HourOpenDoor : null,
                    QuestionProvidedImageOrVideo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionProvidedImageOrVideo : null,
                    WaterSupplier = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.WaterSupplier : null,
                    RemindCustomerDocument = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.RemindCustomerDocument : null,
                });
                #endregion

                _unitOfWork.CommitTransaction();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;

            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBriefV3/CreateLoanbriefScript POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("getlatlng")]
        public ActionResult<DefaultResponse<Meta, string>> GetLatLng(int loanbriefId, int typeGetLatLng)
        {
            var def = new DefaultResponse<Meta, string>();
            try
            {
                if (typeGetLatLng == (int)TypeGetLatLng.Home)
                {
                    //Kiểm tra nếu có ảnh chụp trong nhà app tima care => lấy tọa độ
                    //Hoặc video do KH quay trong nhà
                    var image = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == loanbriefId && !string.IsNullOrEmpty(x.LatLong)
                    //&& x.SourceUpload == EnumSourceUpload.TimaCare.GetHashCode()
                    && (x.TypeId == EnumDocumentType.CVKD_In_Home_Customer.GetHashCode()
                            || x.TypeId == EnumDocumentType.Tima_Care_Document_In_Home.GetHashCode()
                            || x.TypeId == EnumDocumentType.CVKD_CheckOut_Home_Customer.GetHashCode()
                            || x.TypeId == EnumDocumentType.Tima_Care_Video_In_Home.GetHashCode())
                    && x.IsDeleted != 1
                    , null, false).OrderByDescending(x => x.TypeId).FirstOrDefault();
                    if (image != null)
                    {
                        def.data = image.LatLong;
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                }
                else if (typeGetLatLng == (int)TypeGetLatLng.Company)
                {
                    //Kiểm tra nếu có ảnh chụp trong công ty app tima care => lấy tọa độ
                    //Hoặc video do KH quay trong công ty
                    var image = _unitOfWork.LoanBriefFileRepository.Get(x => x.LoanBriefId == loanbriefId && !string.IsNullOrEmpty(x.LatLong)
                    // && x.SourceUpload == EnumSourceUpload.TimaCare.GetHashCode()
                    && (x.TypeId == (int)EnumDocumentType.CVKD_In_Company_Customer
                            || x.TypeId == (int)EnumDocumentType.CVKD_CheckOut_Company_Customer
                            || x.TypeId == EnumDocumentType.Tima_Care_Document_In_Company.GetHashCode()
                            || x.TypeId == (int)EnumDocumentType.Tima_Care_Video_In_Company)
                    && x.IsDeleted != 1
                    , null, false).OrderByDescending(x => x.TypeId).FirstOrDefault();
                    if (image != null)
                    {
                        def.data = image.LatLong;
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3/GetLatLng Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("loanbrief_search")]
        public async Task<ActionResult<DefaultResponse<SummaryMeta, List<LoanbriefSearch>>>> SearchLoanAll([FromQuery] int loanBriefId = -1, [FromQuery] int codeId = -1, [FromQuery] string search = "")
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanbriefSearch>>();
            try
            {
                if (string.IsNullOrEmpty(search) && loanBriefId == -1)
                {
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, 0, 0, 0);
                    def.data = null;
                }
                var sql = new StringBuilder();
                sql.AppendLine("select l.LoanBriefId, l.ProductId, l.FullName, l.Gender,l.HubId, ");
                sql.AppendLine("l.Phone, l.LoanAmount, l.LoanTime, l.ProvinceId, l.DistrictId, ");
                sql.AppendLine("l.WardId, l.UtmSource, l.CreatedTime, l.Status, l.IsLocate, l.CoordinatorUserId, ");
                sql.AppendLine("l.HubEmployeeId, l.BoundTelesaleId, l.InProcess, l.IsReborrow, l.IsTrackingLocation, ");
                sql.AppendLine("l.ScheduleTime, l.StatusTelesales, l.DetailStatusTelesales, l.CodeId, l.LenderName, l.TypeRemarketing, l.DeviceId ");
                sql.AppendLine("from LoanBrief(nolock) l ");
                if (loanBriefId > 0)
                    sql.AppendLine($"where l.LoanBriefid = {loanBriefId}");
                else if (codeId > 0)
                    sql.AppendLine($"where l.CodeId = {codeId}");
                else
                {
                    var textSearch = search.Trim();
                    //Nếu là số
                    if (Common.Extensions.ConvertExtensions.IsNumber(textSearch))
                    {
                        if (textSearch.Length == (int)NationalCardLength.CMND_QD || textSearch.Length == (int)NationalCardLength.CMND || textSearch.Length == (int)NationalCardLength.CCCD)
                            sql.AppendLine($"where NationalCard = '{textSearch}'  or NationCardPlace = '{textSearch}' ");
                        else//search phone
                            sql.AppendLine($"where Phone = '{textSearch}' or PhoneOther = '{textSearch}'");
                    }
                    //nếu là text =>search tên
                    else
                        sql.AppendLine($"where FullName = N'{textSearch}'");
                }
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var data = db.Query<LoanbriefSearch>(sql.ToString());
                if (data != null && data.Count > 0)
                {
                    var totalRecords = data.Count();
                    var dicCity = _unitOfWork.ProvinceRepository.Get(null, null, false).ToDictionary(x => x.ProvinceId, x => x.Name);
                    var dicDistrict = _unitOfWork.DistrictRepository.Get(null, null, false).ToDictionary(x => x.DistrictId, x => x.Name);
                    var dicShop = _unitOfWork.ShopRepository.Get(null, null, false).ToDictionary(x => x.ShopId, x => x.Name);
                    //var dicWard = _unitOfWork.WardRepository.Get(null, null, false).ToDictionary(x => x.WardId, x => x.Name);
                    var dicUser = _unitOfWork.UserRepository.Query(x => (x.GroupId == (int)EnumGroupUser.StaffHub
                    || x.GroupId == (int)EnumGroupUser.Telesale || x.GroupId == (int)EnumGroupUser.ApproveEmp), null, false)
                        .ToDictionary(x => x.UserId, x => new
                        {
                            FullName = x.FullName,
                            UserName = x.Username
                        });
                    await Task.Run(() => Parallel.ForEach(data, item =>
                    {
                        if (item.ProductId > 0 && Enum.IsDefined(typeof(EnumProductCredit), item.ProductId))
                        {
                            item.LoanProductName = ExtentionHelper.GetDescription((EnumProductCredit)item.ProductId);
                        }
                        if (item.ProvinceId > 0 && dicCity.ContainsKey(item.ProvinceId.Value))
                        {
                            item.ProvinceName = dicCity[item.ProvinceId.Value];
                        }
                        if (item.DistrictId > 0 && dicDistrict.ContainsKey(item.DistrictId.Value))
                        {
                            item.DistrictName = dicDistrict[item.DistrictId.Value];
                        }

                        if (item.HubEmployeeId > 0 && dicUser.ContainsKey(item.HubEmployeeId.Value))
                        {
                            item.HubStaffFullName = dicUser[item.HubEmployeeId.Value].FullName;
                            item.HubStaffUserName = dicUser[item.HubEmployeeId.Value].UserName;
                        }
                        if (item.CoordinatorUserId > 0 && dicUser.ContainsKey(item.CoordinatorUserId.Value))
                        {
                            item.CoordinatorFullName = dicUser[item.CoordinatorUserId.Value].FullName;
                            item.CoordinatorUserName = dicUser[item.CoordinatorUserId.Value].UserName;
                        }
                        if (item.BoundTelesaleId > 0 && dicUser.ContainsKey(item.BoundTelesaleId.Value))
                        {
                            item.TelesaleFullName = dicUser[item.BoundTelesaleId.Value].FullName;
                            item.TelesaleUserName = dicUser[item.BoundTelesaleId.Value].UserName;
                        }
                        if (item.HubId > 0 && dicShop.ContainsKey(item.HubId.Value))
                        {
                            item.HubName = dicShop[item.HubId.Value];
                        }

                    }));
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, 0, 0, totalRecords);
                    def.data = data;
                }
                else
                {
                    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, 0, 0, 0);
                    def.data = null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3/SearchLoanAll Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("create_loanbrief_child")]
        public ActionResult<DefaultResponse<object>> CreateLoanBriefChild([FromBody] CreateLoanBriefChild entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //lấy ra danh sách file mà chưa bị xóa
                var lstImage = new List<LoanBriefFiles>();
                if (entity.LoanBriefId > 0)
                    lstImage = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == entity.LoanBriefId && x.Status == 1 && x.IsDeleted != 1, null, false).ToList();
                _unitOfWork.BeginTransaction();

                #region Loanbrief
                var loanbrief = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false).FirstOrDefault();
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    // update to new status
                    _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanbrief.LoanBriefId, x => new LoanBrief()
                    {
                        ReMarketingLoanBriefId = entity.LoanBriefId,
                    });
                }
                loanbrief.LoanBriefId = 0;
                loanbrief.CreatedTime = DateTime.Now;
                loanbrief.LoanAmount = entity.LoanAmount;
                loanbrief.LoanAmountFirst = entity.LoanAmount;
                loanbrief.UtmCampaign = null;
                loanbrief.UtmContent = null;
                loanbrief.UtmMedium = null;
                loanbrief.UtmSource = null;
                loanbrief.UtmTerm = null;
                loanbrief.Tid = null;
                loanbrief.PlatformType = (int)EnumPlatformType.LoanCarChild;
                loanbrief.ReMarketingLoanBriefId = entity.LoanBriefId;
                loanbrief.CreateBy = null;
                loanbrief.BoundTelesaleId = null;
                _unitOfWork.LoanBriefRepository.Insert(loanbrief);
                _unitOfWork.Save();
                #endregion
                var loanbriefrelationshipall = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == entity.LoanBriefId, null, false).Select(LoanBriefRelationshipAll.ProjectionDetail).FirstOrDefault();
                #region LoanBriefResident
                if (loanbriefrelationshipall.LoanBriefResident != null)
                {
                    loanbriefrelationshipall.LoanBriefResident.LoanBriefResidentId = loanbrief.LoanBriefId;
                    _unitOfWork.LoanBriefResidentRepository.Insert(loanbriefrelationshipall.LoanBriefResident);
                }
                #endregion

                #region LoanBriefProperty
                if (loanbriefrelationshipall.LoanBriefProperty != null)
                {
                    loanbriefrelationshipall.LoanBriefProperty.LoanBriefPropertyId = loanbrief.LoanBriefId;
                    loanbriefrelationshipall.LoanBriefProperty.CreatedTime = DateTime.Now;
                    _unitOfWork.LoanBriefPropertyRepository.Insert(loanbriefrelationshipall.LoanBriefProperty);
                }
                #endregion

                #region LoanBriefHousehold
                if (loanbriefrelationshipall.LoanBriefHousehold != null)
                {
                    loanbriefrelationshipall.LoanBriefHousehold.LoanBriefHouseholdId = loanbrief.LoanBriefId;
                    _unitOfWork.LoanBriefHouseholdRepository.Insert(loanbriefrelationshipall.LoanBriefHousehold);
                }
                #endregion

                #region LoanBriefJob
                if (loanbriefrelationshipall.LoanBriefJob != null)
                {
                    loanbriefrelationshipall.LoanBriefJob.LoanBriefJobId = loanbrief.LoanBriefId;
                    _unitOfWork.LoanBriefJobRepository.Insert(loanbriefrelationshipall.LoanBriefJob);
                }
                #endregion

                #region LoanBriefRelationship
                if (loanbriefrelationshipall.LoanBriefRelationship != null && loanbriefrelationshipall.LoanBriefRelationship.Count > 0)
                {
                    foreach (var item in loanbriefrelationshipall.LoanBriefRelationship)
                    {
                        item.Id = 0;
                        item.LoanBriefId = loanbrief.LoanBriefId;
                        item.CreatedDate = DateTime.Now;
                    }
                    _unitOfWork.LoanBriefRelationshipRepository.Insert(loanbriefrelationshipall.LoanBriefRelationship.ToList());
                }

                #endregion

                #region LoanBriefImage
                if (lstImage != null && lstImage.Count > 0)
                {
                    foreach (var item in lstImage)
                    {
                        item.Id = 0;
                        item.LoanBriefId = loanbrief.LoanBriefId;
                        item.CreateAt = DateTime.Now;
                    }
                    _unitOfWork.LoanBriefFileRepository.Insert(lstImage);
                }
                #endregion

                _unitOfWork.Save();
                _unitOfWork.CommitTransaction();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = loanbrief.LoanBriefId;

            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBriefV3/CreateLoanBriefChild POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("basic_info")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanBriefBasicInfo>>> GetBasicInfo(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, LoanBriefBasicInfo>();
            try
            {
                var data = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(LoanBriefBasicInfo.ProjectionViewDetail).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3/GetBasicInfo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("total_loan_disbursement_in_month/{coordinatorUserId}")]
        public async Task<ActionResult<DefaultResponse<Meta, dynamic>>> TotalLoanDisbursementInMonth(int coordinatorUserId)
        {
            var def = new DefaultResponse<Meta, dynamic>();
            try
            {
                var fromDate = ExtentionHelper.FirstDayOfMonth(DateTime.Now);
                var toDate = ExtentionHelper.LastDayOfMonth(DateTime.Now);
                var data = await _unitOfWork.LoanBriefRepository.Query(x => x.CoordinatorUserId == coordinatorUserId
                            && (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH)
                            && x.DisbursementAt >= fromDate && x.DisbursementAt <= toDate, null, false).CountAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3/TotalLoanDisbursementInMonth Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("detail_for_appraiser/{id}")]
        public ActionResult<DefaultResponse<Meta, LoanBriefForAppraiser>> GetForAppraiser(int id)
        {
            var def = new DefaultResponse<Meta, LoanBriefForAppraiser>();
            try
            {
                var data = _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == id, null, false).Select(LoanBriefForAppraiser.ProjectionViewDetail).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3/GetForAppraiser Exception");
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("loanbrief_get_contract_financial_agreement")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanBriefContractFinancialAgreement>>> GetContractFinancialAgreement(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, LoanBriefContractFinancialAgreement>();
            try
            {
                var data = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(LoanBriefContractFinancialAgreement.ProjectionViewDetail).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3/GetContractFinancialAgreement Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("loanbrief_get_committed_paper")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanBriefCommittedPaper>>> GetCommittedPaper(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, LoanBriefCommittedPaper>();
            try
            {
                var data = await _unitOfWork.LoanBriefRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).Select(LoanBriefCommittedPaper.ProjectionViewDetail).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3/GetCommittedPaper Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("init_debtRevolving_loan")]
        public ActionResult<DefaultResponse<object>> InitDebtRevolvingLoan(LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.BeginTransaction();

                var files = _unitOfWork.LoanBriefFileRepository.Query(x => x.LoanBriefId == entity.ReMarketingLoanBriefId && x.Status == 1 && x.IsDeleted != 1, null, false).ToList();

                #region Loanbrief
                var loanbrief = new LoanBrief()
                {
                    LoanBriefId = entity.LoanBriefId,
                    CustomerId = entity.CustomerId,
                    PlatformType = entity.PlatformType.GetValueOrDefault(),
                    TypeLoanBrief = entity.TypeLoanBrief.GetValueOrDefault(),
                    FullName = entity.FullName,
                    Phone = entity.Phone,
                    NationalCard = entity.NationalCard,
                    NationCardPlace = entity.NationCardPlace,
                    IsTrackingLocation = entity.IsTrackingLocation,
                    Dob = entity.Dob,
                    Passport = entity.Passport,
                    Gender = entity.Gender,
                    NumberCall = entity.NumberCall,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    ProductId = entity.ProductId.GetValueOrDefault(),
                    RateTypeId = entity.RateTypeId.GetValueOrDefault(),
                    LoanAmount = entity.LoanAmount.GetValueOrDefault(),
                    BuyInsurenceCustomer = entity.BuyInsurenceCustomer,
                    LoanTime = entity.LoanTime,
                    Frequency = entity.Frequency,
                    RateMoney = entity.RateMoney,
                    RatePercent = entity.RatePercent,
                    ReceivingMoneyType = entity.ReceivingMoneyType,
                    BankId = entity.BankId,
                    BankAccountNumber = entity.BankAccountNumber,
                    BankCardNumber = entity.BankCardNumber,
                    BankAccountName = entity.BankAccountName,
                    IsLocate = entity.IsLocate,
                    TypeRemarketing = entity.TypeRemarketing,
                    FromDate = entity.FromDate,
                    ToDate = entity.ToDate,
                    CreatedTime = entity.CreatedTime.HasValue ? entity.CreatedTime.Value.DateTime : DateTime.Now,
                    IsHeadOffice = entity.IsHeadOffice,
                    HubId = entity.HubId,
                    CreateBy = entity.CreateBy,
                    Status = EnumLoanStatus.INIT.GetHashCode(),
                    PipelineState = -1,

                    PresenterCode = entity.PresenterCode,
                    LoanAmountFirst = entity.LoanAmountFirst,
                    LoanAmountExpertise = entity.LoanAmountExpertise,
                    LoanAmountExpertiseLast = entity.LoanAmountExpertiseLast,
                    LoanAmountExpertiseAi = entity.LoanAmountExpertiseAi,
                    PhoneOther = entity.PhoneOther,
                    BankBranch = entity.BankBranch,

                    ProductDetailId = entity.ProductDetailId,
                    IsSim = entity.IsSim,
                    IsReborrow = entity.IsReborrow,
                    ReMarketingLoanBriefId = entity.ReMarketingLoanBriefId,
                    LoanPurpose = entity.LoanPurpose,
                    FeePaymentBeforeLoan = entity.FeePaymentBeforeLoan
                };
                _unitOfWork.LoanBriefRepository.Insert(loanbrief);
                _unitOfWork.Save();
                #endregion

                #region LoanBriefResident
                _unitOfWork.LoanBriefResidentRepository.Insert(new LoanBriefResident()
                {
                    LoanBriefResidentId = loanbrief.LoanBriefId,
                    LivingTime = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingTime : 0,
                    ResidentType = entity.LoanBriefResident != null ? entity.LoanBriefResident.ResidentType : 0,
                    LivingWith = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingWith : 0,
                    Address = entity.LoanBriefResident != null ? entity.LoanBriefResident.Address : "",
                    AddressGoogleMap = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressGoogleMap : "",
                    AddressLatLng = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressLatLng : "",
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    BillElectricityId = entity.LoanBriefResident.BillElectricityId,
                    BillWaterId = entity.LoanBriefResident.BillWaterId,
                    AddressNationalCard = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressNationalCard : "",
                    CustomerShareLocation = entity.LoanBriefResident != null ? entity.LoanBriefResident.CustomerShareLocation : "",
                });
                #endregion

                #region LoanBriefProperty
                _unitOfWork.LoanBriefPropertyRepository.Insert(new LoanBriefProperty()
                {
                    LoanBriefPropertyId = loanbrief.LoanBriefId,
                    BrandId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BrandId : 0,
                    ProductId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.ProductId : 0,
                    PlateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumber : "",
                    CreatedTime = DateTime.Now,
                    CarManufacturer = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarManufacturer : "",
                    CarName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarName : "",
                    CarColor = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarColor : "",
                    PlateNumberCar = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumberCar : "",
                    Chassis = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.Chassis : "",
                    Engine = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.Engine : "",
                    BuyInsuranceProperty = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BuyInsuranceProperty : false,
                    OwnerFullName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.OwnerFullName : "",
                    MotobikeCertificateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.MotobikeCertificateNumber : "",
                    MotobikeCertificateDate = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.MotobikeCertificateDate : "",
                    MotobikeCertificateAddress = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.MotobikeCertificateAddress : "",
                });
                #endregion

                #region LoanBriefHousehold
                _unitOfWork.LoanBriefHouseholdRepository.Insert(new LoanBriefHousehold()
                {
                    LoanBriefHouseholdId = loanbrief.LoanBriefId,
                    Address = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.Address : "",
                    ProvinceId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.ProvinceId : 0,
                    DistrictId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.DistrictId : 0,
                    WardId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.WardId : 0,
                    FullNameHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.FullNameHouseOwner : null,
                    RelationshipHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.RelationshipHouseOwner : null,
                    BirdayHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.BirdayHouseOwner : null,
                });
                #endregion

                #region LoanBriefJob
                _unitOfWork.LoanBriefJobRepository.Insert(new LoanBriefJob()
                {
                    LoanBriefJobId = loanbrief.LoanBriefId,
                    JobId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobId : 0,
                    CompanyName = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyName : "",
                    CompanyPhone = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyPhone : "",
                    CompanyTaxCode = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyTaxCode : "",
                    CompanyProvinceId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyProvinceId : 0,
                    CompanyDistrictId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyDistrictId : 0,
                    CompanyWardId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyWardId : 0,
                    CompanyAddress = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddress : "",
                    TotalIncome = entity.LoanBriefJob != null ? entity.LoanBriefJob.TotalIncome : 0,
                    ImcomeType = entity.LoanBriefJob != null ? entity.LoanBriefJob.ImcomeType : 0,
                    Description = entity.LoanBriefJob != null ? entity.LoanBriefJob.Description : "",
                    CompanyAddressGoogleMap = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressGoogleMap : "",
                    CompanyAddressLatLng = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressLatLng : "",
                    CompanyInsurance = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyInsurance : null,
                    WorkLocation = entity.LoanBriefJob != null ? entity.LoanBriefJob.WorkLocation : null,
                    BusinessPapers = entity.LoanBriefJob != null ? entity.LoanBriefJob.BusinessPapers : null,
                    JobDescriptionId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobDescriptionId : null,
                });
                #endregion

                #region LoanBriefCompany
                if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    _unitOfWork.LoanBriefCompanyRepository.Insert(new LoanBriefCompany()
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        CompanyName = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyName : "",
                        CareerBusiness = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CareerBusiness : "",
                        BusinessCertificationAddress = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.BusinessCertificationAddress : "",
                        HeadOffice = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.HeadOffice : "",
                        Address = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.Address : "",
                        CompanyShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyShareholder : "",
                        CardNumberShareholderDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.CardNumberShareholderDate.HasValue
                        ? entity.LoanBriefCompany.CardNumberShareholderDate : null,
                        BusinessCertificationDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BusinessCertificationDate.HasValue
                        ? entity.LoanBriefCompany.BusinessCertificationDate : null,
                        BirthdayShareholder = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BirthdayShareholder.HasValue
                        ? entity.LoanBriefCompany.BirthdayShareholder : null,
                        CardNumberShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CardNumberShareholder : "",
                        PlaceOfBirthShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.PlaceOfBirthShareholder : "",
                        CreatedDate = DateTime.Now
                    });
                }
                #endregion

                #region LoanBriefRelationship
                if (entity.LoanBriefRelationship != null && entity.LoanBriefRelationship.Count > 0)
                {
                    foreach (var relationship in entity.LoanBriefRelationship)
                    {
                        if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                        {
                            _unitOfWork.LoanBriefRelationshipRepository.Insert(new LoanBriefRelationship()
                            {
                                LoanBriefId = loanbrief.LoanBriefId,
                                RelationshipType = relationship.RelationshipType,
                                FullName = relationship.FullName,
                                Phone = relationship.Phone,
                                CreatedDate = DateTime.Now,
                                Address = relationship.Address
                            });
                        }
                    }
                }
                #endregion

                #region LoanBriefFiles
                if (files != null && files.Count > 0)
                {
                    foreach (var item in files)
                    {
                        _unitOfWork.LoanBriefFileRepository.Insert(new LoanBriefFiles()
                        {
                            LoanBriefId = loanbrief.LoanBriefId,
                            FilePath = item.FilePath,
                            FileThumb = item.FileThumb,
                            TypeId = item.TypeId,
                            UserId = item.UserId,
                            CreateAt = item.CreateAt,
                            MecashId = item.MecashId,
                            S3status = item.S3status,
                            SourceUpload = item.SourceUpload,
                            Status = item.Status,
                            SubTypeId = item.SubTypeId,
                            TypeFile = item.TypeFile
                        });
                    }
                }
                #endregion

                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = loanbrief.LoanBriefId;
                _unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanbriefV3/InitDebtRevolvingLoan POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("check_debt_revolving")]
        public async Task<ActionResult<DefaultResponse<dynamic>>> CheckDebtRevolving(int loanBriefId)
        {
            var def = new DefaultResponse<dynamic>();
            try
            {
                def.data = true;
                var loanBrief = await _unitOfWork.LoanBriefRepository.Query(x => x.ReMarketingLoanBriefId == loanBriefId && x.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan, null, false).FirstOrDefaultAsync();
                if (loanBrief != null)
                {
                    def.data = false;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3/CheckDebtRevolving Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("re_esign_customer")]
        public ActionResult<DefaultResponse<object>> ReEsignCustomer([FromBody] ReEsignReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.LoanBriefId <= 0 || entity.EsignNumber < 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //kiểm tra số lần ký
                var numberEsign = _unitOfWork.EsignContractRepository.Count(x => x.LoanBriefId == entity.LoanBriefId && x.TypeEsign == (int)TypeEsign.Customer);
                if (numberEsign >= entity.EsignNumber)
                {
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, "Đã ký quá số lần quy định");
                    return Ok(def);
                }
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                {
                    EsignState = 0,
                    EsignBorrowerContract = null

                });
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanbriefV3/ReEsignCustomer Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("check_loan_processing")]
        public async Task<ActionResult<DefaultResponse<dynamic>>> CheckLoanProcessing(string phone)
        {
            var def = new DefaultResponse<dynamic>();
            try
            {
                def.data = false;
                var loanBrief = await _unitOfWork.LoanBriefRepository.Query(x => x.Phone == phone && (x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.FINISH), null, false).FirstOrDefaultAsync();
                if (loanBrief != null)
                {
                    def.data = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3/CheckDebtRevolving Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_loan_by_phone_change")]
        public ActionResult<DefaultResponse<Meta, List<LoanByPhoneChange>>> GetLoanByPhoneChange(string phone)
        {
            var def = new DefaultResponse<Meta, List<LoanByPhoneChange>>();
            try
            {
                var data = _unitOfWork.LoanBriefRepository.Query(x => x.Phone == phone || x.LoanBriefRelationship.Any(k => k.Phone == phone), null, false).Select(x => new
                {
                    LoanbriefId = x.LoanBriefId,
                    FulllName = x.FullName,
                    Phone = x.Phone,
                    LoanBriefRelationship = x.LoanBriefRelationship

                }).ToList();
                var lst = new List<LoanByPhoneChange>();
                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        var rela = 0;
                        if (item.Phone != phone)
                        {
                            var item2 = item.LoanBriefRelationship.Where(x => x.Phone == phone).FirstOrDefault();
                            rela = item2.RelationshipType.Value;
                        }

                        lst.Add(new LoanByPhoneChange
                        {
                            LoanBriefId = item.LoanbriefId,
                            Phone = item.Phone,
                            FullName = item.FulllName,
                            RelationshipType = rela
                        });
                    }
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_loan_tool_check_cavet")]
        public ActionResult<DefaultResponse<Meta, List<LoanBriefCheckCavetDetail>>> GetLoanToolCheckCavet(string search)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefCheckCavetDetail>>();
            try
            {
                var filter = FilterHelper<LoanBrief>.Create(x => (x.Status == (int)EnumLoanStatus.DISBURSED || x.Status == (int)EnumLoanStatus.FINISH));

                if (!string.IsNullOrEmpty(search))
                {
                    var textSearch = search.Trim();
                    if (ConvertExtensions.IsNumber(textSearch)) //Nếu là số
                    {
                        if (textSearch.Length == (int)NationalCardLength.CMND_QD || textSearch.Length == (int)NationalCardLength.CMND || textSearch.Length == (int)NationalCardLength.CCCD)
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.NationalCard == textSearch || x.NationCardPlace == textSearch)));
                        else//search phone
                            filter = filter.And(FilterHelper<LoanBrief>.Create(x => (x.Phone == textSearch || x.PhoneOther == textSearch)));
                    }
                    else//nếu là text =>search tên
                        filter = filter.And(FilterHelper<LoanBrief>.Create(x => x.FullName.Contains(search)));
                }
                var data = _unitOfWork.LoanBriefRepository.Query(filter.Apply(), null, false).Select(LoanBriefCheckCavetDetail.Projection).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanToolCheckCavet Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("check_loan_display_doc_hub_emp")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanDisplayDocHub>>> CheckLoanDisplayDocHubEmp(string phone, string nationalCard)
        {
            var def = new DefaultResponse<Meta, LoanDisplayDocHub>();
            try
            {

                var data = await _unitOfWork.LoanBriefRepository.Query(x => (x.Phone == phone || x.NationalCard == nationalCard)
                    && (x.Status != (int)EnumLoanStatus.CANCELED && x.Status != (int)EnumLoanStatus.DISBURSED && x.Status != (int)EnumLoanStatus.FINISH)
                    && (x.IsReborrow == true || (x.TypeRemarketing > 0 && x.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)),null, false).Select(LoanDisplayDocHub.ProjectionViewDetail).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;


            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3/CheckLoanDisplayDocHubEmp Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("check_loan_assign")]
        public async Task<ActionResult<DefaultResponse<dynamic>>> CheckLoanAssign(int loanBriefId, string phone,int userId)
        {
            var def = new DefaultResponse<dynamic>();
            try
            {
                def.data = false;
                if(loanBriefId > 0)
                {
                    if(await _unitOfWork.LoanBriefRepository.AnyAsync(x => x.LoanBriefId == loanBriefId && x.BoundTelesaleId == userId))
                        def.data = true;
                }
                if (!String.IsNullOrEmpty(phone))
                {
                    if (await _unitOfWork.LoanBriefRepository.AnyAsync(x => x.Phone == phone && x.BoundTelesaleId == userId))
                        def.data = true;
                }
                

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3/CheckLoanAssign Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        #region Push LoanTo Cisco
        [HttpGet]
        [Route("push_to_cisco_search")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LogPushToCisco>>> PushToCiscoSearch([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] string search = null, [FromQuery] int status = -1)
        {
            var def = new DefaultResponse<SummaryMeta, List<LogPushToCisco>>();
            try
            {
                var query = _unitOfWork.LogPushToCiscoRepository.Query(x => x.Phone == search || search == null && x.Status == status || status == -1 , null,false).ToList();
               
                var totalRecords = query.Count();
                List<LogPushToCisco> data;
                if (skip == -1)
                    data = query.OrderByDescending(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                {
                    data = query.Skip(skip).Take(take).ToList();
                    pageSize = take;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/PushToCiscoSearch Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("push_to_cisco_create")]
        public ActionResult<DefaultResponse<object>> AddLogPushToCisco([FromBody] LogPushToCisco entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //Insert Log
                _unitOfWork.LogPushToCiscoRepository.Insert(entity);

                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion
    }
}