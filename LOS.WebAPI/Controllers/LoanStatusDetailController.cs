﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class LoanStatusDetail : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        public LoanStatusDetail(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("list_status_detail")]
        public ActionResult<DefaultResponse<Meta, List<DAL.EntityFramework.LoanStatusDetail>>> GetStatusTelesales(int typeId)
        {
            var def = new DefaultResponse<Meta, List<DAL.EntityFramework.LoanStatusDetail>>();
            try
            {
                var data = unitOfWork.LoanStatusDetailRepository.Query(x => x.Type == typeId && x.IsEnable == true && x.ParentId.GetValueOrDefault() == 0, null, false).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanStatusDetail/list_status_detail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("list_status_child")]
        public ActionResult<DefaultResponse<Meta, List<DAL.EntityFramework.LoanStatusDetail>>> GetStatusTelesalesDetail(int parentId)
        {
            var def = new DefaultResponse<Meta, List<DAL.EntityFramework.LoanStatusDetail>>();
            try
            {
                var data = unitOfWork.LoanStatusDetailRepository.Query(x => x.IsEnable == true && x.ParentId == parentId, null, false).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanStatusDetail/list_status_child Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("list_status_telesales_detail_all")]
        public ActionResult<DefaultResponse<Meta, List<DAL.EntityFramework.LoanStatusDetail>>> GetStatusTelesalesDetailAll()
        {
            var def = new DefaultResponse<Meta, List<DAL.EntityFramework.LoanStatusDetail>>();
            try
            {
                var data = unitOfWork.LoanStatusDetailRepository.Query(x => x.ParentId != 0, null, false).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "StatusTelesales/list_status_telesales_detail_all Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
