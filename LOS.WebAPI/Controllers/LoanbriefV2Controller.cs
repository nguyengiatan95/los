﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.Dapper;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class LoanbriefV2Controller : BaseController
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _baseConfig;
        public LoanbriefV2Controller(IUnitOfWork unitOfWork, IConfiguration baseConfig)
        {
            this._unitOfWork = unitOfWork;
            this._baseConfig = baseConfig;
        }

        [HttpPost]
        [Route("init_loanbrief")]
        public ActionResult<DefaultResponse<object>> InitLoanBrief(LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.BeginTransaction();
                #region customer               
                var customerOld = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == entity.Phone || x.NationalCard == entity.NationalCard);
                if (customerOld != null && customerOld.CustomerId > 0)
                {
                    _unitOfWork.CustomerRepository.Update(x => x.CustomerId == customerOld.CustomerId, x => new Customer()
                    {
                        FullName = entity.FullName,
                        JobId = entity.LoanBriefJob.JobId,
                        Phone = entity.Phone,
                        CompanyName = entity.LoanBriefJob.CompanyName,
                        CompanyAddress = entity.LoanBriefJob.CompanyAddress,
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId,
                        Address = entity.LoanBriefResident.Address,
                        UpdatedAt = DateTime.Now,
                        InsurenceNumber = entity.Customer.InsurenceNumber,
                        InsurenceNote = entity.Customer.InsurenceNote,
                        IsMerried = entity.Customer.IsMerried,
                        NumberBaby = entity.Customer.NumberBaby,
                        Dob = entity.Dob,
                        Gender = entity.Gender,
                        NationalCard = !string.IsNullOrEmpty(entity.NationalCard) ? entity.NationalCard : customerOld.NationalCard,
                        NationCardPlace = entity.NationCardPlace,
                        Passport = entity.Passport,
                        FacebookAddress = entity.Customer.FacebookAddress,
                    });
                    entity.CustomerId = customerOld.CustomerId;
                }
                else
                {
                    var customer = new Customer()
                    {
                        FullName = entity.FullName,
                        JobId = entity.LoanBriefJob.JobId,
                        CompanyName = entity.LoanBriefJob.CompanyName,
                        CompanyAddress = entity.LoanBriefJob.CompanyAddress,
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId,
                        Address = entity.LoanBriefResident.Address,
                        CreatedAt = DateTime.Now,
                        InsurenceNumber = entity.Customer.InsurenceNumber,
                        InsurenceNote = entity.Customer.InsurenceNote,
                        IsMerried = entity.Customer.IsMerried,
                        NumberBaby = entity.Customer.NumberBaby,
                        Phone = entity.Phone,
                        Dob = entity.Dob,
                        Gender = entity.Gender,
                        NationalCard = entity.NationalCard,
                        NationCardPlace = entity.NationCardPlace,
                        Passport = entity.Passport,
                        FacebookAddress = entity.Customer.FacebookAddress,
                    };
                    _unitOfWork.CustomerRepository.Insert(customer);
                    _unitOfWork.Save();
                    entity.CustomerId = customer.CustomerId;
                }
                #endregion

                #region Loanbrief
                var loanbrief = new LoanBrief()
                {
                    LoanBriefId = entity.LoanBriefId,
                    CustomerId = entity.CustomerId,
                    PlatformType = entity.PlatformType.GetValueOrDefault(),
                    TypeLoanBrief = entity.TypeLoanBrief.GetValueOrDefault(),
                    FullName = entity.FullName,
                    Phone = entity.Phone,
                    NationalCard = entity.NationalCard,
                    NationCardPlace = entity.NationCardPlace,
                    IsTrackingLocation = entity.IsTrackingLocation,
                    Dob = entity.Dob,
                    Passport = entity.Passport,
                    Gender = entity.Gender,
                    NumberCall = entity.NumberCall,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    ProductId = entity.ProductId.GetValueOrDefault(),
                    RateTypeId = entity.RateTypeId.GetValueOrDefault(),
                    LoanAmount = entity.LoanAmount.GetValueOrDefault(),
                    BuyInsurenceCustomer = entity.BuyInsurenceCustomer,
                    LoanTime = entity.LoanTime,
                    Frequency = entity.Frequency,
                    RateMoney = entity.RateMoney,
                    RatePercent = entity.RatePercent,
                    ReceivingMoneyType = entity.ReceivingMoneyType,
                    BankId = entity.BankId,
                    BankAccountNumber = entity.BankAccountNumber,
                    BankCardNumber = entity.BankCardNumber,
                    BankAccountName = entity.BankAccountName,
                    IsLocate = entity.IsLocate,
                    TypeRemarketing = entity.TypeRemarketing,
                    FromDate = entity.FromDate,
                    ToDate = entity.ToDate,
                    CreatedTime = entity.CreatedTime.HasValue ? entity.CreatedTime.Value.DateTime : DateTime.Now,
                    IsHeadOffice = entity.IsHeadOffice,
                    HubId = entity.HubId,
                    CreateBy = entity.CreateBy,
                    Status = EnumLoanStatus.INIT.GetHashCode(),
                    PipelineState = EnumPipelineState.WAITING_FOR_PIPELINE.GetHashCode(),
                    BoundTelesaleId = entity.BoundTelesaleId,
                    HubEmployeeId = entity.HubEmployeeId,
                    PresenterCode = entity.PresenterCode,
                    LoanAmountFirst = entity.LoanAmountFirst,
                    LoanAmountExpertise = entity.LoanAmountExpertise,
                    LoanAmountExpertiseLast = entity.LoanAmountExpertiseLast,
                    LoanAmountExpertiseAi = entity.LoanAmountExpertiseAi,
                    PhoneOther = entity.PhoneOther,
                    BankBranch = entity.BankBranch,
                    TeamTelesalesId = entity.TeamTelesalesId,
                    ProductDetailId = entity.ProductDetailId,
                    IsSim = entity.IsSim,
                    IsReborrow = entity.IsReborrow,
                    ReMarketingLoanBriefId = entity.ReMarketingLoanBriefId,
                    LoanPurpose = entity.LoanPurpose,
                    FeePaymentBeforeLoan = entity.FeePaymentBeforeLoan,
                    Email = entity.Email,
                    FirstTimeHubFeedBack = entity.FirstTimeHubFeedBack,
                    FirstTimeStaffHubFeedback = entity.FirstTimeStaffHubFeedback,
                    LoanStatusDetail = entity.LoanStatusDetail,
                    LoanStatusDetailChild = entity.LoanStatusDetailChild,
                    Ltv = entity.Ltv,
                };
                _unitOfWork.LoanBriefRepository.Insert(loanbrief);
                _unitOfWork.Save();
                #endregion

                #region LoanBriefResident
                _unitOfWork.LoanBriefResidentRepository.Insert(new LoanBriefResident()
                {
                    LoanBriefResidentId = loanbrief.LoanBriefId,
                    LivingTime = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingTime : 0,
                    ResidentType = entity.LoanBriefResident != null ? entity.LoanBriefResident.ResidentType : 0,
                    LivingWith = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingWith : 0,
                    Address = entity.LoanBriefResident != null ? entity.LoanBriefResident.Address : "",
                    AddressGoogleMap = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressGoogleMap : "",
                    AddressLatLng = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressLatLng : "",
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    BillElectricityId = entity.LoanBriefResident.BillElectricityId,
                    BillWaterId = entity.LoanBriefResident.BillWaterId,
                    AddressNationalCard = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressNationalCard : "",
                    CustomerShareLocation = entity.LoanBriefResident != null ? entity.LoanBriefResident.CustomerShareLocation : "",
                });
                #endregion

                #region LoanBriefProperty
                _unitOfWork.LoanBriefPropertyRepository.Insert(new LoanBriefProperty()
                {
                    LoanBriefPropertyId = loanbrief.LoanBriefId,
                    BrandId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BrandId : 0,
                    ProductId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.ProductId : 0,
                    PlateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumber : "",
                    CreatedTime = DateTime.Now,
                    CarManufacturer = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarManufacturer : "",
                    CarName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarName : "",
                    CarColor = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarColor : "",
                    PlateNumberCar = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumberCar : "",
                    Chassis = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.Chassis : "",
                    Engine = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.Engine : "",
                    BuyInsuranceProperty = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BuyInsuranceProperty : false,
                    OwnerFullName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.OwnerFullName : "",
                    MotobikeCertificateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.MotobikeCertificateNumber : "",
                    MotobikeCertificateDate = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.MotobikeCertificateDate : "",
                    MotobikeCertificateAddress = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.MotobikeCertificateAddress : "",
                    PostRegistration = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PostRegistration : false,
                    Description = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.Description : null,
                });
                #endregion

                #region LoanBriefHousehold
                _unitOfWork.LoanBriefHouseholdRepository.Insert(new LoanBriefHousehold()
                {
                    LoanBriefHouseholdId = loanbrief.LoanBriefId,
                    Address = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.Address : "",
                    ProvinceId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.ProvinceId : 0,
                    DistrictId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.DistrictId : 0,
                    WardId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.WardId : 0,
                    FullNameHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.FullNameHouseOwner : null,
                    RelationshipHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.RelationshipHouseOwner : null,
                    BirdayHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.BirdayHouseOwner : null,
                });
                #endregion

                #region LoanBriefJob
                _unitOfWork.LoanBriefJobRepository.Insert(new LoanBriefJob()
                {
                    LoanBriefJobId = loanbrief.LoanBriefId,
                    JobId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobId : 0,
                    CompanyName = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyName : "",
                    CompanyPhone = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyPhone : "",
                    CompanyTaxCode = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyTaxCode : "",
                    CompanyProvinceId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyProvinceId : 0,
                    CompanyDistrictId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyDistrictId : 0,
                    CompanyWardId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyWardId : 0,
                    CompanyAddress = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddress : "",
                    TotalIncome = entity.LoanBriefJob != null ? entity.LoanBriefJob.TotalIncome : 0,
                    ImcomeType = entity.LoanBriefJob != null ? entity.LoanBriefJob.ImcomeType : 0,
                    Description = entity.LoanBriefJob != null ? entity.LoanBriefJob.Description : "",
                    CompanyAddressGoogleMap = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressGoogleMap : "",
                    CompanyAddressLatLng = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressLatLng : "",
                    CompanyInsurance = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyInsurance : null,
                    WorkLocation = entity.LoanBriefJob != null ? entity.LoanBriefJob.WorkLocation : null,
                    BusinessPapers = entity.LoanBriefJob != null ? entity.LoanBriefJob.BusinessPapers : null,
                    JobDescriptionId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobDescriptionId : null,
                });
                #endregion

                #region LoanBriefCompany
                if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    _unitOfWork.LoanBriefCompanyRepository.Insert(new LoanBriefCompany()
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        CompanyName = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyName : "",
                        CareerBusiness = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CareerBusiness : "",
                        BusinessCertificationAddress = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.BusinessCertificationAddress : "",
                        HeadOffice = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.HeadOffice : "",
                        Address = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.Address : "",
                        CompanyShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyShareholder : "",
                        CardNumberShareholderDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.CardNumberShareholderDate.HasValue
                        ? entity.LoanBriefCompany.CardNumberShareholderDate : null,
                        BusinessCertificationDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BusinessCertificationDate.HasValue
                        ? entity.LoanBriefCompany.BusinessCertificationDate : null,
                        BirthdayShareholder = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BirthdayShareholder.HasValue
                        ? entity.LoanBriefCompany.BirthdayShareholder : null,
                        CardNumberShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CardNumberShareholder : "",
                        PlaceOfBirthShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.PlaceOfBirthShareholder : "",
                        CreatedDate = DateTime.Now
                    });
                }
                #endregion

                #region LoanBriefRelationship
                if (entity.LoanBriefRelationship != null && entity.LoanBriefRelationship.Count > 0)
                {
                    foreach (var relationship in entity.LoanBriefRelationship)
                    {
                        if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                        {
                            _unitOfWork.LoanBriefRelationshipRepository.Insert(new LoanBriefRelationship()
                            {
                                LoanBriefId = loanbrief.LoanBriefId,
                                RelationshipType = relationship.RelationshipType,
                                FullName = relationship.FullName,
                                Phone = relationship.Phone,
                                CreatedDate = DateTime.Now,
                                Address = relationship.Address
                            });
                        }
                    }
                }
                #endregion

                #region LoanBriefQuestionScript
                _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                {
                    LoanBriefId = loanbrief.LoanBriefId,
                    WaterSupplier = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.WaterSupplier : null,
                    QuestionHouseOwner = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionHouseOwner : null,
                    Appraiser = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.Appraiser : null,
                    DocumentBusiness = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentBusiness : null,
                    MaxPrice = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.MaxPrice : null,
                    QuestionAddressCoincideAreaSupport = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionAddressCoincideAreaSupport : null,
                });
                #endregion

                #region Nếu là đơn tái vay và gói vay xe máy, xử lý clone chứng từ
                if(entity.IsReborrow.GetValueOrDefault(false) && entity.ReMarketingLoanBriefId > 0 && (entity.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || entity.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))
                {
                    List<int> lstDocumentId = new List<int>();
                    lstDocumentId.Add((int)EnumDocumentType.CMND_CCCD);
                    lstDocumentId.Add((int)EnumDocumentType.Zalo);
                    lstDocumentId.Add((int)EnumDocumentType.HoChieu_CVKD_Chup);
                    lstDocumentId.Add((int)EnumDocumentType.SDT_ThamChieu);
                    lstDocumentId.Add((int)EnumDocumentType.Selfie);
                    lstDocumentId.Add((int)EnumDocumentType.Video_TinNhanLuong);
                    lstDocumentId.Add((int)EnumDocumentType.HDLD_CoDauDoCongTy);
                    var files = _unitOfWork.LoanBriefFileRepository.Query(x => lstDocumentId.Contains(x.TypeId.Value) 
                                && x.LoanBriefId == entity.ReMarketingLoanBriefId
                                && x.Status == 1 && x.IsDeleted.GetValueOrDefault(0) == 0,null,false).ToList();
                    if(files != null && files.Count() > 0)
                    {
                        foreach(var file in files)
                        {
                            _unitOfWork.LoanBriefFileRepository.Insert(new LoanBriefFiles
                            {
                                LoanBriefId = loanbrief.LoanBriefId,
                                CreateAt = DateTime.Now,
                                FilePath = file.FilePath,
                                UserId = file.UserId,
                                Status = file.Status,
                                TypeFile = file.TypeFile,
                                S3status = file.S3status,
                                TypeId = file.TypeId,
                                IsDeleted= file.IsDeleted,
                                MecashId = file.MecashId,
                                FileThumb = file.FileThumb,
                                SourceUpload =  file.SourceUpload
                            });
                        }    
                    }    
                    
                }    
                #endregion

                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = loanbrief.LoanBriefId;
                _unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanbriefV2/InitLoanBrief POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("init_loanbrief/{id}")]
        public ActionResult<DefaultResponse<object>> InitLoanBrief(int id, [FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != entity.LoanBriefId || entity.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //_unitOfWork.BeginTransaction();
                //Check nếu là đơn topup cho update thông tin số tiền vay, thông tin đồng nghiệp, thông tin ngân hàng

                #region cập nhật thông tin KH
                _unitOfWork.CustomerRepository.Update(x => x.CustomerId == entity.CustomerId, x => new Customer()
                {
                    FullName = entity.FullName,
                    JobId = entity.LoanBriefJob.JobId,
                    CompanyName = entity.LoanBriefJob.CompanyName,
                    CompanyAddress = entity.LoanBriefJob.CompanyAddress,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    Address = entity.LoanBriefResident.Address,
                    UpdatedAt = DateTime.Now,
                    FacebookAddress = entity.Customer.FacebookAddress,
                    InsurenceNumber = entity.Customer.InsurenceNumber,
                    InsurenceNote = entity.Customer.InsurenceNote,
                    IsMerried = entity.Customer.IsMerried,
                    NumberBaby = entity.Customer.NumberBaby,
                    Dob = entity.Dob,
                    Gender = entity.Gender,
                    NationalCard = entity.NationalCard,
                    NationCardPlace = entity.NationCardPlace,
                    Passport = entity.Passport

                });
                #endregion

                #region LoanBriefResident
                if (_unitOfWork.LoanBriefResidentRepository.Any(x => x.LoanBriefResidentId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefResidentRepository.Update(x => x.LoanBriefResidentId == entity.LoanBriefId, x => new LoanBriefResident()
                    {
                        LoanBriefResidentId = entity.LoanBriefId,
                        LivingTime = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingTime : 0,
                        ResidentType = entity.LoanBriefResident != null ? entity.LoanBriefResident.ResidentType : 0,
                        LivingWith = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingWith : 0,
                        Address = entity.LoanBriefResident != null ? entity.LoanBriefResident.Address : "",
                        AddressGoogleMap = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressGoogleMap : "",
                        AddressLatLng = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressLatLng : "",
                        AddressNationalCard = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressNationalCard : "",
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId,
                        BillElectricityId = entity.LoanBriefResident.BillElectricityId,
                        BillWaterId = entity.LoanBriefResident.BillWaterId,
                        CustomerShareLocation = entity.LoanBriefResident != null ? entity.LoanBriefResident.CustomerShareLocation : "",
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefResidentRepository.Insert(new LoanBriefResident()
                    {
                        LoanBriefResidentId = entity.LoanBriefId,
                        LivingTime = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingTime : 0,
                        ResidentType = entity.LoanBriefResident != null ? entity.LoanBriefResident.ResidentType : 0,
                        LivingWith = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingWith : 0,
                        Address = entity.LoanBriefResident != null ? entity.LoanBriefResident.Address : "",
                        //AddressGoogleMap = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressGoogleMap : "",
                        //AddressLatLng = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressLatLng : "",
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId,
                        BillElectricityId = entity.LoanBriefResident.BillElectricityId,
                        BillWaterId = entity.LoanBriefResident.BillWaterId,
                        AddressNationalCard = entity.LoanBriefResident.AddressNationalCard,
                        CustomerShareLocation = entity.LoanBriefResident != null ? entity.LoanBriefResident.CustomerShareLocation : "",
                    });
                }
                #endregion

                #region LoanBriefProperty
                if (_unitOfWork.LoanBriefPropertyRepository.Any(x => x.LoanBriefPropertyId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefPropertyRepository.Update(x => x.LoanBriefPropertyId == entity.LoanBriefId, x => new LoanBriefProperty()
                    {
                        LoanBriefPropertyId = entity.LoanBriefId,
                        BrandId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BrandId : 0,
                        ProductId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.ProductId : 0,
                        PlateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumber : "",
                        UpdatedTime = DateTime.Now,
                        CarManufacturer = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarManufacturer : "",
                        CarName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarName : "",
                        CarColor = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarColor : "",
                        PlateNumberCar = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumberCar : "",
                        Chassis = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.Chassis : "",
                        Engine = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.Engine : "",
                        BuyInsuranceProperty = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BuyInsuranceProperty : false,
                        OwnerFullName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.OwnerFullName : "",
                        MotobikeCertificateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.MotobikeCertificateNumber : "",
                        MotobikeCertificateDate = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.MotobikeCertificateDate : "",
                        MotobikeCertificateAddress = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.MotobikeCertificateAddress : "",
                        PostRegistration = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PostRegistration : false,
                        Description = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.Description : null,
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefPropertyRepository.Insert(new LoanBriefProperty()
                    {
                        LoanBriefPropertyId = entity.LoanBriefId,
                        BrandId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BrandId : 0,
                        ProductId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.ProductId : 0,
                        PlateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumber : "",
                        CreatedTime = DateTime.Now,
                        CarManufacturer = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarManufacturer : "",
                        CarName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarName : "",
                        CarColor = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarColor : "",
                        PlateNumberCar = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumberCar : "",
                        Chassis = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.Chassis : "",
                        Engine = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.Engine : "",
                        BuyInsuranceProperty = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BuyInsuranceProperty : false,
                        OwnerFullName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.OwnerFullName : "",
                        MotobikeCertificateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.MotobikeCertificateNumber : "",
                        MotobikeCertificateDate = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.MotobikeCertificateDate : "",
                        MotobikeCertificateAddress = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.MotobikeCertificateAddress : "",
                        Description = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.Description : null,
                    });
                }

                #endregion

                #region LoanBriefHousehold
                if (_unitOfWork.LoanBriefHouseholdRepository.Any(x => x.LoanBriefHouseholdId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefHouseholdRepository.Update(x => x.LoanBriefHouseholdId == entity.LoanBriefId, x => new LoanBriefHousehold()
                    {
                        LoanBriefHouseholdId = entity.LoanBriefId,
                        Address = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.Address : "",
                        ProvinceId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.ProvinceId : 0,
                        DistrictId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.DistrictId : 0,
                        WardId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.WardId : 0,
                        FullNameHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.FullNameHouseOwner : null,
                        RelationshipHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.RelationshipHouseOwner : null,
                        BirdayHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.BirdayHouseOwner : null,
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefHouseholdRepository.Insert(new LoanBriefHousehold()
                    {
                        LoanBriefHouseholdId = entity.LoanBriefId,
                        Address = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.Address : "",
                        ProvinceId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.ProvinceId : 0,
                        DistrictId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.DistrictId : 0,
                        WardId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.WardId : 0,
                        FullNameHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.FullNameHouseOwner : null,
                        RelationshipHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.RelationshipHouseOwner : null,
                        BirdayHouseOwner = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.BirdayHouseOwner : null,
                    });
                }

                #endregion

                #region LoanBriefJob
                if (_unitOfWork.LoanBriefJobRepository.Any(x => x.LoanBriefJobId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefJobRepository.Update(x => x.LoanBriefJobId == entity.LoanBriefId, x => new LoanBriefJob()
                    {
                        LoanBriefJobId = entity.LoanBriefId,
                        JobId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobId : 0,
                        CompanyName = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyName : "",
                        CompanyPhone = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyPhone : "",
                        CompanyTaxCode = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyTaxCode : "",
                        CompanyProvinceId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyProvinceId : 0,
                        CompanyDistrictId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyDistrictId : 0,
                        CompanyWardId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyWardId : 0,
                        CompanyAddress = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddress : "",
                        TotalIncome = entity.LoanBriefJob != null ? entity.LoanBriefJob.TotalIncome : 0,
                        ImcomeType = entity.LoanBriefJob != null ? entity.LoanBriefJob.ImcomeType : 0,
                        Description = entity.LoanBriefJob != null ? entity.LoanBriefJob.Description : "",
                        CompanyAddressGoogleMap = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressGoogleMap : "",
                        CompanyAddressLatLng = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressLatLng : "",
                        CompanyInsurance = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyInsurance : null,
                        WorkLocation = entity.LoanBriefJob != null ? entity.LoanBriefJob.WorkLocation : null,
                        BusinessPapers = entity.LoanBriefJob != null ? entity.LoanBriefJob.BusinessPapers : null,
                        JobDescriptionId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobDescriptionId : null,
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefJobRepository.Insert(new LoanBriefJob()
                    {
                        LoanBriefJobId = entity.LoanBriefId,
                        JobId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobId : 0,
                        CompanyName = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyName : "",
                        CompanyPhone = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyPhone : "",
                        CompanyTaxCode = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyTaxCode : "",
                        CompanyProvinceId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyProvinceId : 0,
                        CompanyDistrictId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyDistrictId : 0,
                        CompanyWardId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyWardId : 0,
                        CompanyAddress = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddress : "",
                        TotalIncome = entity.LoanBriefJob != null ? entity.LoanBriefJob.TotalIncome : 0,
                        ImcomeType = entity.LoanBriefJob != null ? entity.LoanBriefJob.ImcomeType : 0,
                        Description = entity.LoanBriefJob != null ? entity.LoanBriefJob.Description : "",
                        //CompanyAddressGoogleMap = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressGoogleMap : "",
                        //CompanyAddressLatLng = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressLatLng : "",
                        CompanyInsurance = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyInsurance : null,
                        WorkLocation = entity.LoanBriefJob != null ? entity.LoanBriefJob.WorkLocation : null,
                        BusinessPapers = entity.LoanBriefJob != null ? entity.LoanBriefJob.BusinessPapers : null,
                        JobDescriptionId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobDescriptionId : null,
                    });
                }
                #endregion

                #region LoanBriefCompany
                if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    if (_unitOfWork.LoanBriefCompanyRepository.Any(x => x.LoanBriefId == entity.LoanBriefId))
                    {
                        _unitOfWork.LoanBriefCompanyRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBriefCompany()
                        {
                            LoanBriefId = entity.LoanBriefId,
                            CompanyName = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyName : "",
                            CareerBusiness = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CareerBusiness : "",
                            BusinessCertificationAddress = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.BusinessCertificationAddress : "",
                            HeadOffice = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.HeadOffice : "",
                            Address = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.Address : "",
                            CompanyShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyShareholder : "",
                            CardNumberShareholderDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.CardNumberShareholderDate.HasValue
                                    ? entity.LoanBriefCompany.CardNumberShareholderDate : null,
                            BusinessCertificationDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BusinessCertificationDate.HasValue
                                    ? entity.LoanBriefCompany.BusinessCertificationDate : null,
                            BirthdayShareholder = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BirthdayShareholder.HasValue
                                    ? entity.LoanBriefCompany.BirthdayShareholder : null,
                            CardNumberShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CardNumberShareholder : "",
                            PlaceOfBirthShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.PlaceOfBirthShareholder : "",
                            UpdatedDate = DateTime.Now
                        });
                    }
                    else
                    {
                        _unitOfWork.LoanBriefCompanyRepository.Insert(new LoanBriefCompany()
                        {
                            LoanBriefId = entity.LoanBriefId,
                            CompanyName = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyName : "",
                            CareerBusiness = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CareerBusiness : "",
                            BusinessCertificationAddress = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.BusinessCertificationAddress : "",
                            HeadOffice = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.HeadOffice : "",
                            Address = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.Address : "",
                            CompanyShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyShareholder : "",
                            CardNumberShareholderDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.CardNumberShareholderDate.HasValue
                                    ? entity.LoanBriefCompany.CardNumberShareholderDate : null,
                            BusinessCertificationDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BusinessCertificationDate.HasValue
                                    ? entity.LoanBriefCompany.BusinessCertificationDate : null,
                            BirthdayShareholder = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BirthdayShareholder.HasValue
                                    ? entity.LoanBriefCompany.BirthdayShareholder : null,
                            CardNumberShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CardNumberShareholder : "",
                            PlaceOfBirthShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.PlaceOfBirthShareholder : "",
                            CreatedDate = DateTime.Now
                        });
                    }

                }
                #endregion

                #region LoanBriefRelationship
                if (entity.LoanBriefRelationship != null && entity.LoanBriefRelationship.Count > 0)
                {
                    if (_unitOfWork.LoanBriefRelationshipRepository.Any(x => x.LoanBriefId == entity.LoanBriefId))
                        _unitOfWork.LoanBriefRelationshipRepository.Delete(x => x.LoanBriefId == entity.LoanBriefId);
                    foreach (var relationship in entity.LoanBriefRelationship)
                    {
                        if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                        {
                            _unitOfWork.LoanBriefRelationshipRepository.Insert(new LoanBriefRelationship()
                            {
                                LoanBriefId = entity.LoanBriefId,
                                RelationshipType = relationship.RelationshipType,
                                FullName = relationship.FullName,
                                Phone = relationship.Phone,
                                CreatedDate = DateTime.Now,
                                Address = relationship.Address
                            });
                        }
                    }
                }
                #endregion

                #region Loanbrief
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                {
                    PlatformType = entity.PlatformType.GetValueOrDefault(),
                    TypeLoanBrief = entity.TypeLoanBrief.GetValueOrDefault(),
                    FullName = entity.FullName,
                    //Phone = entity.Phone,
                    NationalCard = entity.NationalCard,
                    NationCardPlace = entity.NationCardPlace,
                    IsTrackingLocation = entity.IsTrackingLocation,
                    Dob = entity.Dob,
                    Passport = entity.Passport,
                    Gender = entity.Gender,
                    NumberCall = entity.NumberCall,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    ProductId = entity.ProductId.GetValueOrDefault(),
                    RateTypeId = entity.RateTypeId.GetValueOrDefault(),
                    LoanAmount = entity.LoanAmount.GetValueOrDefault(),
                    BuyInsurenceCustomer = entity.BuyInsurenceCustomer,
                    LoanTime = entity.LoanTime,
                    Frequency = entity.Frequency,
                    RateMoney = entity.RateMoney,
                    RatePercent = entity.RatePercent,
                    ReceivingMoneyType = entity.ReceivingMoneyType,
                    BankId = entity.BankId,
                    BankAccountNumber = entity.BankAccountNumber,
                    BankCardNumber = entity.BankCardNumber,
                    BankAccountName = entity.BankAccountName,
                    IsLocate = entity.IsLocate,
                    //TypeRemarketing = entity.TypeRemarketing,
                    FromDate = entity.FromDate,
                    ToDate = entity.ToDate,
                    //CreatedTime = entity.CreatedTime.HasValue ? entity.CreatedTime.Value.DateTime : DateTime.MinValue,
                    //IsHeadOffice = entity.IsHeadOffice,
                    //HubId = entity.HubId,
                    // CreateBy = entity.CreateBy,
                    //BoundTelesaleId = entity.BoundTelesaleId,
                    //HubEmployeeId = entity.HubEmployeeId,
                    PresenterCode = entity.PresenterCode,
                    LoanAmountFirst = entity.LoanAmountFirst,
                    LoanAmountExpertise = entity.LoanAmountExpertise,
                    LoanAmountExpertiseLast = entity.LoanAmountExpertiseLast,
                    LoanAmountExpertiseAi = entity.LoanAmountExpertiseAi,
                    FirstProcessingTime = entity.FirstProcessingTime,
                    IsCheckBank = entity.IsCheckBank,
                    HomeNetwork = entity.HomeNetwork,
                    PhoneOther = entity.PhoneOther,
                    BankBranch = entity.BankBranch,
                    ProductDetailId = entity.ProductDetailId,
                    IsSim = entity.IsSim,
                    LoanPurpose = entity.LoanPurpose,
                    FeePaymentBeforeLoan = entity.FeePaymentBeforeLoan,
                    Email = entity.Email,
                    IsTransactionsSecured = entity.IsTransactionsSecured,
                    Ltv = entity.Ltv,
                });
                #endregion

                #region LoanBriefQuestionScript
                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBriefQuestionScript()
                    {
                        WaterSupplier = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.WaterSupplier : null,
                        QuestionHouseOwner = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionHouseOwner : null,
                        Appraiser = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.Appraiser : null,
                        DocumentBusiness = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentBusiness : null,
                        MaxPrice = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.MaxPrice : null,
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                    {
                        LoanBriefId = entity.LoanBriefId,
                        WaterSupplier = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.WaterSupplier : null,
                        QuestionHouseOwner = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionHouseOwner : null,
                        Appraiser = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.Appraiser : null,
                        DocumentBusiness = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DocumentBusiness : null,
                        MaxPrice = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.MaxPrice : null,
                    });
                }
                #endregion

                _unitOfWork.Save();
                //_unitOfWork.CommitTransaction();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;

            }
            catch (Exception ex)
            {
                // _unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBriefV2/InitLoanBrief PUT Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("property")]
        public async Task<ActionResult<DefaultResponse<Meta, LoanBriefPropertyDTO>>> GetProperty(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, LoanBriefPropertyDTO>();
            try
            {
                var data = _unitOfWork.LoanBriefPropertyRepository.Query(x => x.LoanBriefPropertyId == loanbriefId, null, false).Select(x => new LoanBriefPropertyDTO()
                {
                    BrandId = x.BrandId,
                    ProductId = x.ProductId
                }).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = await data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV2/GetProperty Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("add_loanbrief_history")]
        public ActionResult<DefaultResponse<object>> AddLoanbriefHistory(LoanBriefHistory entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.LoanBriefHistoryRepository.Insert(entity);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefHistoryId;
            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanbriefV2/AddLoanbriefHistory POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("script_loanbrief/{id}")]
        public ActionResult<DefaultResponse<object>> UpdateScript(int id, [FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != entity.LoanBriefId || entity.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // _unitOfWork.BeginTransaction();

                #region cập nhật thông tin KH
                _unitOfWork.CustomerRepository.Update(x => x.CustomerId == entity.CustomerId, x => new Customer()
                {
                    FullName = entity.FullName,
                    JobId = entity.LoanBriefJob.JobId,
                    CompanyName = entity.LoanBriefJob.CompanyName,
                    CompanyAddress = entity.LoanBriefJob.CompanyAddress,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    Address = entity.LoanBriefResident.Address,
                    UpdatedAt = DateTime.Now,
                    FacebookAddress = entity.Customer.FacebookAddress,
                    InsurenceNumber = entity.Customer.InsurenceNumber,
                    InsurenceNote = entity.Customer.InsurenceNote,
                    IsMerried = entity.Customer.IsMerried,
                    NumberBaby = entity.Customer.NumberBaby,
                    Dob = entity.Dob,
                    Gender = entity.Gender,
                    NationalCard = entity.NationalCard,
                    NationCardPlace = entity.NationCardPlace,
                    Passport = entity.Passport,
                    AddressLatLong = entity.LoanBriefResident.AddressLatLng,
                    CompanyAddressLatLong = entity.LoanBriefJob.CompanyAddressLatLng

                });
                #endregion

                #region LoanBriefResident
                if (_unitOfWork.LoanBriefResidentRepository.Any(x => x.LoanBriefResidentId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefResidentRepository.Update(x => x.LoanBriefResidentId == entity.LoanBriefId, x => new LoanBriefResident()
                    {
                        LoanBriefResidentId = entity.LoanBriefId,
                        LivingTime = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingTime : 0,
                        ResidentType = entity.LoanBriefResident != null ? entity.LoanBriefResident.ResidentType : 0,
                        LivingWith = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingWith : 0,
                        Address = entity.LoanBriefResident != null ? entity.LoanBriefResident.Address : "",
                        AddressLatLng = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressLatLng : "",
                        AddressGoogleMap = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressGoogleMap : "",
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefResidentRepository.Insert(new LoanBriefResident()
                    {
                        LoanBriefResidentId = entity.LoanBriefId,
                        LivingTime = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingTime : 0,
                        ResidentType = entity.LoanBriefResident != null ? entity.LoanBriefResident.ResidentType : 0,
                        LivingWith = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingWith : 0,
                        Address = entity.LoanBriefResident != null ? entity.LoanBriefResident.Address : "",
                        AddressLatLng = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressLatLng : "",
                        AddressGoogleMap = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressGoogleMap : "",
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId
                    });
                }
                #endregion

                #region LoanBriefProperty
                if (_unitOfWork.LoanBriefPropertyRepository.Any(x => x.LoanBriefPropertyId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefPropertyRepository.Update(x => x.LoanBriefPropertyId == entity.LoanBriefId, x => new LoanBriefProperty()
                    {
                        LoanBriefPropertyId = entity.LoanBriefId,
                        BrandId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BrandId : 0,
                        ProductId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.ProductId : 0,
                        PlateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumber : "",
                        UpdatedTime = DateTime.Now,
                        CarManufacturer = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarManufacturer : "",
                        CarName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarName : "",
                        CarColor = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarColor : "",
                        PlateNumberCar = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumberCar : ""
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefPropertyRepository.Insert(new LoanBriefProperty()
                    {
                        LoanBriefPropertyId = entity.LoanBriefId,
                        BrandId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BrandId : 0,
                        ProductId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.ProductId : 0,
                        PlateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumber : "",
                        CreatedTime = DateTime.Now,
                        CarManufacturer = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarManufacturer : "",
                        CarName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarName : "",
                        CarColor = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarColor : "",
                        PlateNumberCar = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumberCar : ""
                    });
                }

                #endregion

                #region LoanBriefHousehold
                if (_unitOfWork.LoanBriefHouseholdRepository.Any(x => x.LoanBriefHouseholdId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefHouseholdRepository.Update(x => x.LoanBriefHouseholdId == entity.LoanBriefId, x => new LoanBriefHousehold()
                    {
                        LoanBriefHouseholdId = entity.LoanBriefId,
                        Address = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.Address : "",
                        ProvinceId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.ProvinceId : 0,
                        DistrictId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.DistrictId : 0,
                        WardId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.WardId : 0,
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefHouseholdRepository.Insert(new LoanBriefHousehold()
                    {
                        LoanBriefHouseholdId = entity.LoanBriefId,
                        Address = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.Address : "",
                        ProvinceId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.ProvinceId : 0,
                        DistrictId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.DistrictId : 0,
                        WardId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.WardId : 0,
                    });
                }

                #endregion

                #region LoanBriefJob
                if (_unitOfWork.LoanBriefJobRepository.Any(x => x.LoanBriefJobId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefJobRepository.Update(x => x.LoanBriefJobId == entity.LoanBriefId, x => new LoanBriefJob()
                    {
                        LoanBriefJobId = entity.LoanBriefId,
                        JobId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobId : 0,
                        CompanyName = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyName : "",
                        CompanyPhone = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyPhone : "",
                        CompanyTaxCode = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyTaxCode : "",
                        CompanyProvinceId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyProvinceId : 0,
                        CompanyDistrictId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyDistrictId : 0,
                        CompanyWardId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyWardId : 0,
                        CompanyAddress = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddress : "",
                        CompanyAddressGoogleMap = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressGoogleMap : "",
                        CompanyAddressLatLng = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressLatLng : "",
                        TotalIncome = entity.LoanBriefJob != null ? entity.LoanBriefJob.TotalIncome : 0,
                        ImcomeType = entity.LoanBriefJob != null ? entity.LoanBriefJob.ImcomeType : 0,
                        Description = entity.LoanBriefJob != null ? entity.LoanBriefJob.Description : ""
                    });
                }
                else
                {
                    _unitOfWork.LoanBriefJobRepository.Insert(new LoanBriefJob()
                    {
                        LoanBriefJobId = entity.LoanBriefId,
                        JobId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobId : 0,
                        CompanyName = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyName : "",
                        CompanyPhone = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyPhone : "",
                        CompanyTaxCode = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyTaxCode : "",
                        CompanyProvinceId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyProvinceId : 0,
                        CompanyDistrictId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyDistrictId : 0,
                        CompanyWardId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyWardId : 0,
                        CompanyAddress = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddress : "",
                        TotalIncome = entity.LoanBriefJob != null ? entity.LoanBriefJob.TotalIncome : 0,
                        ImcomeType = entity.LoanBriefJob != null ? entity.LoanBriefJob.ImcomeType : 0,
                        Description = entity.LoanBriefJob != null ? entity.LoanBriefJob.Description : "",
                        CompanyAddressGoogleMap = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressGoogleMap : "",
                        CompanyAddressLatLng = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressLatLng : "",
                    });
                }
                #endregion

                #region LoanBriefCompany
                if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    if (_unitOfWork.LoanBriefCompanyRepository.Any(x => x.LoanBriefId == entity.LoanBriefId))
                    {
                        _unitOfWork.LoanBriefCompanyRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBriefCompany()
                        {
                            LoanBriefId = entity.LoanBriefId,
                            CompanyName = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyName : "",
                            CareerBusiness = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CareerBusiness : "",
                            BusinessCertificationAddress = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.BusinessCertificationAddress : "",
                            HeadOffice = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.HeadOffice : "",
                            Address = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.Address : "",
                            CompanyShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyShareholder : "",
                            CardNumberShareholderDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.CardNumberShareholderDate.HasValue
                                    ? entity.LoanBriefCompany.CardNumberShareholderDate : null,
                            BusinessCertificationDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BusinessCertificationDate.HasValue
                                    ? entity.LoanBriefCompany.BusinessCertificationDate : null,
                            BirthdayShareholder = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BirthdayShareholder.HasValue
                                    ? entity.LoanBriefCompany.BirthdayShareholder : null,
                            CardNumberShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CardNumberShareholder : "",
                            PlaceOfBirthShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.PlaceOfBirthShareholder : "",
                            UpdatedDate = DateTime.Now
                        });
                    }
                    else
                    {
                        _unitOfWork.LoanBriefCompanyRepository.Insert(new LoanBriefCompany()
                        {
                            LoanBriefId = entity.LoanBriefId,
                            CompanyName = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyName : "",
                            CareerBusiness = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CareerBusiness : "",
                            BusinessCertificationAddress = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.BusinessCertificationAddress : "",
                            HeadOffice = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.HeadOffice : "",
                            Address = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.Address : "",
                            CompanyShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyShareholder : "",
                            CardNumberShareholderDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.CardNumberShareholderDate.HasValue
                                    ? entity.LoanBriefCompany.CardNumberShareholderDate : null,
                            BusinessCertificationDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BusinessCertificationDate.HasValue
                                    ? entity.LoanBriefCompany.BusinessCertificationDate : null,
                            BirthdayShareholder = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BirthdayShareholder.HasValue
                                    ? entity.LoanBriefCompany.BirthdayShareholder : null,
                            CardNumberShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CardNumberShareholder : "",
                            PlaceOfBirthShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.PlaceOfBirthShareholder : "",
                            CreatedDate = DateTime.Now
                        });
                    }

                }
                #endregion

                #region LoanBriefRelationship
                
                #endregion

                #region LoanBriefQuestionScript
                if (_unitOfWork.LoanBriefQuestionScriptRepository.Any(x => x.LoanBriefId == entity.LoanBriefId))
                {
                    _unitOfWork.LoanBriefQuestionScriptRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBriefQuestionScript()
                    {
                        QuestionUseMotobikeGo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionUseMotobikeGo : null,
                        QuestionMotobikeCertificate = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionMotobikeCertificate : null,
                        QuestionAccountBank = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionAccountBank : null,
                        TypeJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeJob : null,
                        AdvisoryDeductInsurance = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisoryDeductInsurance : null,
                        AdvisoryKeepRegisteredMotobike = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisoryKeepRegisteredMotobike : null,
                        AdvisorySettingGps = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisorySettingGps : null,
                        ChooseCareer = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.ChooseCareer : null,
                        Signs = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.Signs : null,
                        AddressBusiness = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AddressBusiness : null,
                        WorkingTime = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.WorkingTime : null,
                        DriverOfCompany = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DriverOfCompany : null,
                        AccountDriver = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AccountDriver : null,
                        QuestionDriverGo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionDriverGo : null,
                        TimeDriver = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TimeDriver : null,
                        ContractInShop = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.ContractInShop : null,
                        PhotographShop = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.PhotographShop : null,
                        CommentStatusTelesales = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.CommentStatusTelesales : null,
                        QuestionBorrow = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionBorrow : null,
                        SigningAlaborContract = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.SigningAlaborContract : null,
                        TypeWork = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeWork : null,
                        LatestPayday = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.LatestPayday : null,
                        NameWeb = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.NameWeb : null,
                        QuestionWarehouse = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionWarehouse : null,
                        QuestionImageGoods = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageGoods : null,
                        DetailJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DetailJob : null,
                        TimeSellWeb = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TimeSellWeb : null,
                        QuestionCarOwnership = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionCarOwnership : null

                    });
                }
                else
                {
                    _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                    {
                        LoanBriefId = entity.LoanBriefId,
                        QuestionUseMotobikeGo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionUseMotobikeGo : null,
                        QuestionMotobikeCertificate = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionMotobikeCertificate : null,
                        QuestionAccountBank = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionAccountBank : null,
                        TypeJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeJob : null,
                        AdvisoryDeductInsurance = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisoryDeductInsurance : null,
                        AdvisoryKeepRegisteredMotobike = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisoryKeepRegisteredMotobike : null,
                        AdvisorySettingGps = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisorySettingGps : null,
                        ChooseCareer = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.ChooseCareer : null,
                        Signs = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.Signs : null,
                        AddressBusiness = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AddressBusiness : null,
                        WorkingTime = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.WorkingTime : null,
                        DriverOfCompany = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DriverOfCompany : null,
                        AccountDriver = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AccountDriver : null,
                        QuestionDriverGo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionDriverGo : null,
                        TimeDriver = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TimeDriver : null,
                        ContractInShop = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.ContractInShop : null,
                        PhotographShop = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.PhotographShop : null,
                        CommentStatusTelesales = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.CommentStatusTelesales : null,
                        QuestionBorrow = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionBorrow : null,
                        SigningAlaborContract = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.SigningAlaborContract : null,
                        TypeWork = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeWork : null,
                        LatestPayday = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.LatestPayday : null,
                        NameWeb = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.NameWeb : null,
                        QuestionWarehouse = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionWarehouse : null,
                        QuestionImageGoods = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageGoods : null,
                        DetailJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DetailJob : null,
                        TimeSellWeb = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TimeSellWeb : null,
                        QuestionCarOwnership = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionCarOwnership : null
                    });
                }
                #endregion

                #region Loanbrief
                _unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.LoanBriefId, x => new LoanBrief()
                {
                    //PlatformType = entity.PlatformType.GetValueOrDefault(),
                    //TypeLoanBrief = entity.TypeLoanBrief.GetValueOrDefault(),
                    FullName = entity.FullName,
                    //Phone = entity.Phone,
                    NationalCard = entity.NationalCard,
                    NationCardPlace = entity.NationCardPlace,
                    IsTrackingLocation = entity.IsTrackingLocation,
                    Dob = entity.Dob,
                    Passport = entity.Passport,
                    Gender = entity.Gender,
                    NumberCall = entity.NumberCall,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    ProductId = entity.ProductId.GetValueOrDefault(),
                    RateTypeId = entity.RateTypeId.GetValueOrDefault(),
                    LoanAmount = entity.LoanAmount.GetValueOrDefault(),
                    BuyInsurenceCustomer = entity.BuyInsurenceCustomer,
                    LoanTime = entity.LoanTime,
                    Frequency = entity.Frequency,
                    RateMoney = entity.RateMoney,
                    RatePercent = entity.RatePercent,
                    ReceivingMoneyType = entity.ReceivingMoneyType,
                    BankId = entity.BankId,
                    BankAccountNumber = entity.BankAccountNumber,
                    BankCardNumber = entity.BankCardNumber,
                    BankAccountName = entity.BankAccountName,
                    IsLocate = entity.IsLocate,
                    //TypeRemarketing = entity.TypeRemarketing,
                    FromDate = entity.FromDate,
                    ToDate = entity.ToDate,
                    //CreatedTime = entity.CreatedTime.HasValue ? entity.CreatedTime.Value.DateTime : DateTime.MinValue,
                    //IsHeadOffice = entity.IsHeadOffice,
                    //HubId = entity.HubId,
                    // CreateBy = entity.CreateBy,
                    //BoundTelesaleId = entity.BoundTelesaleId,
                    //HubEmployeeId = entity.HubEmployeeId,
                    PresenterCode = entity.PresenterCode,
                    LoanAmountFirst = entity.LoanAmountFirst,
                    LoanAmountExpertise = entity.LoanAmountExpertise,
                    LoanAmountExpertiseLast = entity.LoanAmountExpertiseLast,
                    LoanAmountExpertiseAi = entity.LoanAmountExpertiseAi,
                    FirstProcessingTime = entity.FirstProcessingTime,
                    LoanPurpose = entity.LoanPurpose,
                    ValueCheckQualify = entity.ValueCheckQualify

                });
                #endregion
                _unitOfWork.Save();
                //_unitOfWork.CommitTransaction();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;

            }
            catch (Exception ex)
            {
                //_unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBriefV2/UpdateScript PUT Exception");
                Log.Information("Info UpdateScript: " + Newtonsoft.Json.JsonConvert.SerializeObject(entity));
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("create_loanbrief_script")]
        public ActionResult<DefaultResponse<object>> CreateLoanbriefScript([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.BeginTransaction();
                #region customer               
                var customerOld = _unitOfWork.CustomerRepository.GetFirstOrDefault(x => x.Phone == entity.Phone);
                if (customerOld != null && customerOld.CustomerId > 0)
                {
                    _unitOfWork.CustomerRepository.Update(x => x.CustomerId == customerOld.CustomerId, x => new Customer()
                    {
                        FullName = entity.FullName,
                        Phone = entity.Phone,
                        JobId = entity.LoanBriefJob.JobId,
                        CompanyName = entity.LoanBriefJob.CompanyName,
                        CompanyAddress = entity.LoanBriefJob.CompanyAddress,
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId,
                        Address = entity.LoanBriefResident.Address,
                        UpdatedAt = DateTime.Now,
                        InsurenceNumber = entity.Customer.InsurenceNumber,
                        InsurenceNote = entity.Customer.InsurenceNote,
                        IsMerried = entity.Customer.IsMerried,
                        NumberBaby = entity.Customer.NumberBaby,
                        Dob = entity.Dob,
                        Gender = entity.Gender,
                        NationalCard = entity.NationalCard,
                        NationCardPlace = entity.NationCardPlace,
                        Passport = entity.Passport
                    });
                    entity.CustomerId = customerOld.CustomerId;
                }
                else
                {
                    var customer = new Customer()
                    {
                        FullName = entity.FullName,
                        JobId = entity.LoanBriefJob.JobId,
                        CompanyName = entity.LoanBriefJob.CompanyName,
                        CompanyAddress = entity.LoanBriefJob.CompanyAddress,
                        ProvinceId = entity.ProvinceId,
                        DistrictId = entity.DistrictId,
                        WardId = entity.WardId,
                        Address = entity.LoanBriefResident.Address,
                        CreatedAt = DateTime.Now,
                        InsurenceNumber = entity.Customer.InsurenceNumber,
                        InsurenceNote = entity.Customer.InsurenceNote,
                        IsMerried = entity.Customer.IsMerried,
                        NumberBaby = entity.Customer.NumberBaby,
                        Phone = entity.Phone,
                        Dob = entity.Dob,
                        Gender = entity.Gender,
                        NationalCard = entity.NationalCard,
                        NationCardPlace = entity.NationCardPlace,
                        Passport = entity.Passport
                    };
                    _unitOfWork.CustomerRepository.Insert(customer);
                    _unitOfWork.Save();
                    entity.CustomerId = customer.CustomerId;
                }
                #endregion

                #region Loanbrief
                var loanbrief = new LoanBrief()
                {
                    PlatformType = entity.PlatformType.GetValueOrDefault(),
                    TypeLoanBrief = entity.TypeLoanBrief.GetValueOrDefault(),
                    FullName = entity.FullName,
                    Phone = entity.Phone,
                    NationalCard = entity.NationalCard,
                    NationCardPlace = entity.NationCardPlace,
                    IsTrackingLocation = entity.IsTrackingLocation,
                    Dob = entity.Dob,
                    Passport = entity.Passport,
                    Gender = entity.Gender,
                    NumberCall = entity.NumberCall,
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId,
                    ProductId = entity.ProductId.GetValueOrDefault(),
                    RateTypeId = entity.RateTypeId.GetValueOrDefault(),
                    LoanAmount = entity.LoanAmount.GetValueOrDefault(),
                    BuyInsurenceCustomer = entity.BuyInsurenceCustomer,
                    LoanTime = entity.LoanTime,
                    Frequency = entity.Frequency,
                    RateMoney = entity.RateMoney,
                    RatePercent = entity.RatePercent,
                    ReceivingMoneyType = entity.ReceivingMoneyType,
                    BankId = entity.BankId,
                    BankAccountNumber = entity.BankAccountNumber,
                    BankCardNumber = entity.BankCardNumber,
                    BankAccountName = entity.BankAccountName,
                    IsLocate = entity.IsLocate,
                    //TypeRemarketing = entity.TypeRemarketing,
                    FromDate = entity.FromDate,
                    ToDate = entity.ToDate,
                    CreatedTime = entity.CreatedTime.HasValue ? entity.CreatedTime.Value.DateTime : DateTime.Now,
                    IsHeadOffice = entity.IsHeadOffice,
                    //HubId = entity.HubId,
                    CreateBy = entity.CreateBy,
                    BoundTelesaleId = entity.BoundTelesaleId,
                    //HubEmployeeId = entity.HubEmployeeId,
                    PresenterCode = entity.PresenterCode,
                    LoanAmountFirst = entity.LoanAmountFirst,
                    LoanAmountExpertise = entity.LoanAmountExpertise,
                    LoanAmountExpertiseLast = entity.LoanAmountExpertiseLast,
                    LoanAmountExpertiseAi = entity.LoanAmountExpertiseAi,
                    FirstProcessingTime = entity.FirstProcessingTime,
                    LoanPurpose = entity.LoanPurpose,
                    ValueCheckQualify = entity.ValueCheckQualify,
                    ReMarketingLoanBriefId = entity.ReMarketingLoanBriefId,
                    Status = EnumLoanStatus.INIT.GetHashCode(),
                    CustomerId = entity.CustomerId,
                    UtmSource = entity.UtmSource
                };
                _unitOfWork.LoanBriefRepository.Insert(loanbrief);
                _unitOfWork.Save();
                entity.LoanBriefId = loanbrief.LoanBriefId;
                #endregion

                #region LoanBriefResident
                _unitOfWork.LoanBriefResidentRepository.Insert(new LoanBriefResident()
                {
                    LoanBriefResidentId = entity.LoanBriefId,
                    LivingTime = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingTime : 0,
                    ResidentType = entity.LoanBriefResident != null ? entity.LoanBriefResident.ResidentType : 0,
                    LivingWith = entity.LoanBriefResident != null ? entity.LoanBriefResident.LivingWith : 0,
                    Address = entity.LoanBriefResident != null ? entity.LoanBriefResident.Address : "",
                    AddressLatLng = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressLatLng : "",
                    AddressGoogleMap = entity.LoanBriefResident != null ? entity.LoanBriefResident.AddressGoogleMap : "",
                    ProvinceId = entity.ProvinceId,
                    DistrictId = entity.DistrictId,
                    WardId = entity.WardId
                });
                #endregion

                #region LoanBriefProperty
                _unitOfWork.LoanBriefPropertyRepository.Insert(new LoanBriefProperty()
                {
                    LoanBriefPropertyId = entity.LoanBriefId,
                    BrandId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.BrandId : 0,
                    ProductId = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.ProductId : 0,
                    PlateNumber = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumber : "",
                    CreatedTime = DateTime.Now,
                    CarManufacturer = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarManufacturer : "",
                    CarName = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarName : "",
                    CarColor = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.CarColor : "",
                    PlateNumberCar = entity.LoanBriefProperty != null ? entity.LoanBriefProperty.PlateNumberCar : ""
                });

                #endregion

                #region LoanBriefHousehold
                _unitOfWork.LoanBriefHouseholdRepository.Insert(new LoanBriefHousehold()
                {
                    LoanBriefHouseholdId = entity.LoanBriefId,
                    Address = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.Address : "",
                    ProvinceId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.ProvinceId : 0,
                    DistrictId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.DistrictId : 0,
                    WardId = entity.LoanBriefHousehold != null ? entity.LoanBriefHousehold.WardId : 0,
                });

                #endregion

                #region LoanBriefJob
                _unitOfWork.LoanBriefJobRepository.Insert(new LoanBriefJob()
                {
                    LoanBriefJobId = entity.LoanBriefId,
                    JobId = entity.LoanBriefJob != null ? entity.LoanBriefJob.JobId : 0,
                    CompanyName = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyName : "",
                    CompanyPhone = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyPhone : "",
                    CompanyTaxCode = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyTaxCode : "",
                    CompanyProvinceId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyProvinceId : 0,
                    CompanyDistrictId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyDistrictId : 0,
                    CompanyWardId = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyWardId : 0,
                    CompanyAddress = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddress : "",
                    TotalIncome = entity.LoanBriefJob != null ? entity.LoanBriefJob.TotalIncome : 0,
                    ImcomeType = entity.LoanBriefJob != null ? entity.LoanBriefJob.ImcomeType : 0,
                    Description = entity.LoanBriefJob != null ? entity.LoanBriefJob.Description : "",
                    CompanyAddressGoogleMap = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressGoogleMap : "",
                    CompanyAddressLatLng = entity.LoanBriefJob != null ? entity.LoanBriefJob.CompanyAddressLatLng : "",
                });
                #endregion

                #region LoanBriefCompany
                if (entity.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    _unitOfWork.LoanBriefCompanyRepository.Insert(new LoanBriefCompany()
                    {
                        LoanBriefId = entity.LoanBriefId,
                        CompanyName = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyName : "",
                        CareerBusiness = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CareerBusiness : "",
                        BusinessCertificationAddress = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.BusinessCertificationAddress : "",
                        HeadOffice = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.HeadOffice : "",
                        Address = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.Address : "",
                        CompanyShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CompanyShareholder : "",
                        CardNumberShareholderDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.CardNumberShareholderDate.HasValue
                                   ? entity.LoanBriefCompany.CardNumberShareholderDate : null,
                        BusinessCertificationDate = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BusinessCertificationDate.HasValue
                                   ? entity.LoanBriefCompany.BusinessCertificationDate : null,
                        BirthdayShareholder = entity.LoanBriefCompany != null && entity.LoanBriefCompany.BirthdayShareholder.HasValue
                                   ? entity.LoanBriefCompany.BirthdayShareholder : null,
                        CardNumberShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.CardNumberShareholder : "",
                        PlaceOfBirthShareholder = entity.LoanBriefCompany != null ? entity.LoanBriefCompany.PlaceOfBirthShareholder : "",
                        CreatedDate = DateTime.Now
                    });

                }
                #endregion

                #region LoanBriefRelationship
                if (entity.LoanBriefRelationship != null && entity.LoanBriefRelationship.Count > 0)
                {
                    foreach (var relationship in entity.LoanBriefRelationship)
                    {
                        if(!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                        _unitOfWork.LoanBriefRelationshipRepository.Insert(new LoanBriefRelationship()
                        {
                            LoanBriefId = entity.LoanBriefId,
                            RelationshipType = relationship.RelationshipType,
                            FullName = relationship.FullName,
                            Phone = relationship.Phone,
                            Address = relationship.Address,
                            RefPhoneCallRate = relationship.RefPhoneCallRate,
                            CreatedDate = DateTime.Now
                        });
                    }                    
                }                
                #endregion

                #region LoanBriefQuestionScript
                _unitOfWork.LoanBriefQuestionScriptRepository.Insert(new LoanBriefQuestionScript()
                {
                    LoanBriefId = entity.LoanBriefId,
                    QuestionUseMotobikeGo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionUseMotobikeGo : null,
                    QuestionMotobikeCertificate = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionMotobikeCertificate : null,
                    QuestionAccountBank = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionAccountBank : null,
                    TypeJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeJob : null,
                    AdvisoryDeductInsurance = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisoryDeductInsurance : null,
                    AdvisoryKeepRegisteredMotobike = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisoryKeepRegisteredMotobike : null,
                    AdvisorySettingGps = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AdvisorySettingGps : null,
                    ChooseCareer = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.ChooseCareer : null,
                    Signs = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.Signs : null,
                    AddressBusiness = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AddressBusiness : null,
                    WorkingTime = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.WorkingTime : null,
                    DriverOfCompany = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DriverOfCompany : null,
                    AccountDriver = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.AccountDriver : null,
                    QuestionDriverGo = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionDriverGo : null,
                    TimeDriver = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TimeDriver : null,
                    ContractInShop = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.ContractInShop : null,
                    PhotographShop = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.PhotographShop : null,
                    CommentStatusTelesales = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.CommentStatusTelesales : null,
                    QuestionBorrow = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionBorrow : null,
                    SigningAlaborContract = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.SigningAlaborContract : null,
                    TypeWork = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TypeWork : null,
                    LatestPayday = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.LatestPayday : null,
                    NameWeb = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.NameWeb : null,
                    QuestionWarehouse = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionWarehouse : null,
                    QuestionImageGoods = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionImageGoods : null,
                    DetailJob = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.DetailJob : null,
                    TimeSellWeb = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.TimeSellWeb : null,
                    QuestionCarOwnership = entity.LoanBriefQuestionScript != null ? entity.LoanBriefQuestionScript.QuestionCarOwnership : null
                });
                #endregion

                _unitOfWork.CommitTransaction();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;

            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "LoanBriefV2/CreateLoanbriefScript POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("hub_employee_change")]
        public ActionResult<DefaultResponse<object>> HubEmployeeChange([FromBody] LoanBrief entity)
        {
            var def = new DefaultResponse<object>();
            var sql = new StringBuilder();
            try
            {
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                sql.AppendFormat("update LoanBrief set HubEmployeeId = {0} ", entity.HubEmployeeId);
                if (entity.EvaluationHomeUserId > 0)
                    sql.AppendFormat(", EvaluationHomeUserId = {0} ", entity.EvaluationHomeUserId);
                else
                    sql.AppendLine(", EvaluationHomeUserId = null ");
                if (entity.EvaluationHomeState > 0)
                    sql.AppendFormat(", EvaluationHomeState = {0} ", entity.EvaluationHomeState);
                else
                    sql.AppendLine(", EvaluationHomeState = null ");
                if (entity.EvaluationCompanyUserId > 0)
                    sql.AppendFormat(", EvaluationCompanyUserId = {0} ", entity.EvaluationCompanyUserId);
                else
                    sql.AppendLine(", EvaluationCompanyUserId = null ");
                if (entity.EvaluationCompanyState > 0)
                    sql.AppendFormat(", EvaluationCompanyState = {0} ", entity.EvaluationCompanyState);
                else
                    sql.AppendLine(", EvaluationCompanyState = null ");
                sql.AppendFormat("where LoanBriefId = {0}", entity.LoanBriefId);
                db.Execute(sql.ToString());
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.LoanBriefId;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV2/HubEmployeeChange Exception");
                Log.Information("HubEmployeeChange: " + sql.ToString());
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpGet]
        [Route("list_loan_from_id_status")]
        public ActionResult<DefaultResponse<Meta, List<LoanBrief>>> GetListLoanFromIdAndStatus(string loanbriefIds, int Status)
        {
            var def = new DefaultResponse<Meta, List<LoanBrief>>();
            try
            {
                var arrloanbrief = loanbriefIds.Split(',').Select(x => int.Parse(x));
                var data = _unitOfWork.LoanBriefRepository.Query(x => arrloanbrief.Contains(x.LoanBriefId) && x.Status == Status, null, false).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV2/GetListLoanFromIdAndStatus Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("change_hub")]
        public ActionResult<DefaultResponse<Meta>> ChangeHub([FromBody] ChangeHubSupportItem entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.loanbriefIds == null || entity.loanbriefIds.Count == 0 || entity.HubId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                _unitOfWork.LoanBriefRepository.Update(x => entity.loanbriefIds.Contains(x.LoanBriefId), x => new LoanBrief()
                {
                    HubId = entity.HubId
                });
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV2/ChangeHub Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("change_hub_employee")]
        public ActionResult<DefaultResponse<Meta>> ChangeHubAndEmployee([FromBody] ChangeHubSupportItem entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.loanbriefIds == null || entity.loanbriefIds.Count == 0 || entity.HubId == 0 || entity.HubEmployee == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //_unitOfWork.LoanBriefRepository.Update(x => entity.loanbriefIds.Contains(x.LoanBriefId), x => new LoanBrief()
                //{
                //    HubId = entity.HubId,
                //    HubEmployeeId = entity.HubEmployee
                //});
                //_unitOfWork.Save();
                var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendFormat("update LoanBrief set HubId = {0}, ", entity.HubId);
                sql.AppendFormat("HubEmployeeId = {0} ", entity.HubEmployee);
                sql.AppendLine(", EvaluationCompanyUserId = null ");
                sql.AppendLine(", EvaluationHomeUserId = null ");
                sql.AppendFormat("where LoanBriefId in ({0})", string.Join(",", entity.loanbriefIds));
                db.Execute(sql.ToString());

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV2/ChangeHubAndEmployee Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_countcall")]
        public ActionResult<DefaultResponse<object>> UpdateCountCall([FromBody] UpdateCountCallReq entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.loanbriefId > 0)
                {
                    //_unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == entity.loanbriefId, x => new LoanBrief
                    //{
                    //    CountCall = entity.countCall
                    //});
                    //_unitOfWork.Save();
                    var db = new DapperHelper(_baseConfig.GetConnectionString("LOSDatabase"));
                    var sql = new StringBuilder();
                    sql.AppendFormat("update LoanBrief set CountCall = {0} ", entity.countCall);
                    sql.AppendFormat("where LoanBriefId ={0}", entity.loanbriefId);
                    db.Execute(sql.ToString());

                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = entity.loanbriefId;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV2/UpdateCountCall Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("add_log_change_boundtelesale")]
        public ActionResult<DefaultResponse<object>> AddLogChangeBoundTelesale([FromBody] TelesaleLoanbrief entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.TelesaleLoanbriefRepository.Insert(entity);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV2/AddLogChangeBoundTelesale POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("relationship")]
        public async Task<ActionResult<DefaultResponse<Meta, List<LoanBriefRelationship>>>> GetRelationship(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, List<LoanBriefRelationship>>();
            try
            {
                var data = await _unitOfWork.LoanBriefRelationshipRepository.Query(x => x.LoanBriefId == loanbriefId, null, false).ToListAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV2/GetRelationship Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_log_distribution_user")]
        public ActionResult<DefaultResponse<object>> AddLogDistributionUser([FromBody] LogDistributionUser entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.LogDistributionUserRepository.Insert(entity);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV2/AddLogDistributionUser POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_range_log_distribution_user")]
        public ActionResult<DefaultResponse<object>> AddRangeLogDistributionUser([FromBody] List<LogDistributionUser> entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.LogDistributionUserRepository.Insert(entity);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV2/AddRangeLogDistributionUser POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


    }
}
