﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class LogCallApiController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;

        public LogCallApiController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
        }

        [HttpGet]
        [Route("search")]
        public ActionResult<DefaultResponse<Meta, List<LogCallApi>>> SearchLog(int loanbriefId, int actionCallApi)
        {
            var def = new DefaultResponse<Meta, List<LogCallApi>>();
            try
            {
                var query = _unitOfWork.LogCallApiRepository.Query(x => x.LoanBriefId == loanbriefId && (x.ActionCallApi == actionCallApi || actionCallApi == -1), null, false).OrderByDescending(x => x.Id).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogCallApi/SearchLog Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get")]
        public ActionResult<DefaultResponse<Meta, LogCallApi>> GetById(int Id)
        {
            var def = new DefaultResponse<Meta, LogCallApi>();
            try
            {
                var query = _unitOfWork.LogCallApiRepository.GetById(Id);
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogCallApi/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_last")]
        public ActionResult<DefaultResponse<Meta, LogCallApi>> GetLast(int loanbriefId, int actionCallApi)
        {
            var def = new DefaultResponse<Meta, LogCallApi>();
            try
            {
                var query = _unitOfWork.LogCallApiRepository.Query(x => x.LoanBriefId == loanbriefId && (x.ActionCallApi == actionCallApi || actionCallApi == -1), null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogCallApi/GetLast Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}