﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class LogLoanActionController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;

        public LogLoanActionController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
        }

        [HttpPost]
        [Route("add")]
        public async Task<ActionResult<DefaultResponse<object>>> AddLogAction([FromBody] LogLoanAction entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                await _unitOfWork.LogLoanActionRepository.InsertAsync(entity);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogLoanAction/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_log_restruct")]
        public async Task<ActionResult<DefaultResponse<object>>> AddLogRestruct([FromBody] LogActionRestruct entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                await _unitOfWork.LogActionRestructRepository.InsertAsync(entity);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogActionRestruct/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}