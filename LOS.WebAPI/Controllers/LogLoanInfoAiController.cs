﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class LogLoanInfoAiController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;

        public LogLoanInfoAiController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
        }

        [HttpGet]
        [Route("search")]
        public ActionResult<DefaultResponse<Meta, List<LogLoanInfoAi>>> SearchLog(int loanbriefId, int serviceType)
        {
            var def = new DefaultResponse<Meta, List<LogLoanInfoAi>>();
            try
            {
                var query = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.LoanbriefId == loanbriefId && (x.ServiceType == serviceType || serviceType == -1), null, false).OrderByDescending(x => x.Id).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogLoanInfoAi/SearchLog Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get")]
        public ActionResult<DefaultResponse<Meta, LogLoanInfoAi>> GetById(int Id)
        {
            var def = new DefaultResponse<Meta, LogLoanInfoAi>();
            try
            {
                var query = _unitOfWork.LogLoanInfoAiRepository.GetById(Id);
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogLoanInfoAi/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("getbyservicetype")]
        public ActionResult<DefaultResponse<Meta, LogLoanInfoAi>> GetByServiceType(int loanbriefId, int serviceType)
        {
            var def = new DefaultResponse<Meta, LogLoanInfoAi>();
            try
            {
                var query = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.LoanbriefId == loanbriefId && x.ServiceType == serviceType, null, false).OrderByDescending(x => x.Id).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogLoanInfoAi/GetByServiceType Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("get_by_list_servicetype")]
        public ActionResult<DefaultResponse<Meta, List<LogLoanInfoAi>>> GetByListServiceType(int loanbriefId, string serviceType)
        {
            var def = new DefaultResponse<Meta, List<LogLoanInfoAi>>();
            try
            {

                var lstserviceType = serviceType.Split(',').Select(Int32.Parse).ToList();
                var data = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.LoanbriefId == loanbriefId && lstserviceType.Contains(x.ServiceType. Value), null, false).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;


            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogLoanInfoAi/GetByListServiceType Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
