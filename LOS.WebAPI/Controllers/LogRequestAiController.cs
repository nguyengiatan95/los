﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class LogRequestAiController : BaseController
    {
        private IUnitOfWork _unitOfWork;
        public LogRequestAiController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        [HttpPost]
        public ActionResult<DefaultResponse<object>> Post([FromBody]LogLoanInfoAi entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var result = _unitOfWork.LogLoanInfoAiRepository.Insert(entity);
                _unitOfWork.Save();
                def.data = result.Id;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogRequestAi/Post Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update")]
        public ActionResult<DefaultResponse<object>> Update([FromBody]LogLoanInfoAi entity, int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var result = _unitOfWork.LogLoanInfoAiRepository.Update(x => x.Id == id, x => new LogLoanInfoAi()
                {
                    IsExcuted = entity.IsExcuted,
                    Response = entity.Response,
                    UpdatedAt = entity.UpdatedAt
                });
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogRequestAi/Update Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpPost]
        [Route("adds")]
        public ActionResult<DefaultResponse<object>> Adds([FromBody] List<LogLoanInfoAi> entities)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var result = _unitOfWork.LogLoanInfoAiRepository.Insert(entities);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogRequestAi/Adds Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpGet]
        [Route("get_by_loanbrief")]
        public ActionResult<DefaultResponse<Meta, List<LogLoanInfoAi>>> GetByLoanbrief(int loanbriefId)
        {
            var def = new DefaultResponse<Meta, List<LogLoanInfoAi>>();
            try
            {
                var query = _unitOfWork.LogLoanInfoAiRepository.Query(x => x.LoanbriefId == loanbriefId, null, false).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogRequestAi/GetByLoanbrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("create_v2")]
        public ActionResult<DefaultResponse<object>> CreateV2([FromBody]LogLoanInfoAi entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var result = _unitOfWork.LogLoanInfoAiRepository.Insert(entity);
                _unitOfWork.Save();
                def.data = result.Id;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogRequestAi/CreateV2 Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

    }
}