﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class ManagerAreaHubController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;
        public ManagerAreaHubController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
        }

        [HttpGet]
        [Route("GetListManagerAreaHub")]
        public ActionResult<DefaultResponse<SummaryMeta, List<ManagerAreaHub>>> GetListManagerAreaHub(int cityid, int hubid, int procductid)
        {
            var def = new DefaultResponse<Meta, List<ManagerAreaHub>>();
            try
            {
                var query = _unitOfWork.ManagerAreaHubRepository.Query(x => x.ProvinceId == cityid && x.ShopId == hubid && x.ProductId == procductid
                , null, false).ToList();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ManagerAreaHub/GetListManagerAreaHub Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        public ActionResult<DefaultResponse<object>> PostManagerAreaHub([FromBody] ManagerAreaHubSaveItem entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                System.Data.DataTable dt = new System.Data.DataTable("ManagerAreaHub");
                dt.Columns.Add("ShopId", typeof(Int32));
                dt.Columns.Add("DistrictId", typeof(Int32));
                dt.Columns.Add("WardId", typeof(Int32));
                dt.Columns.Add("CreateDate", typeof(DateTime));
                dt.Columns.Add("ProvinceId", typeof(Int32));
                dt.Columns.Add("ProductId", typeof(Int32));

                if (entity.CityId == -1 && entity.ProductId == -1) // Xóa tất cả
                {
                    _unitOfWork.ManagerAreaHubRepository.Delete(x => x.ShopId == entity.Shopid);
                    _unitOfWork.Save();
                }
                else
                    _unitOfWork.ManagerAreaHubRepository.Delete(x => x.ShopId == entity.Shopid && x.ProductId == entity.ProductId  && x.ProvinceId == entity.CityId);
                _unitOfWork.Save();
                //mainSql = "delete from tblManageSpices where ShopId=" + AgencyId + " and (ProductId=" + ProductId + " or 0=" + ProductId + ") and (CityId=" + CityId + " or 0=" + CityId + ")";

                //string[] ArrID = DistrictId.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var district = entity.ListWardId.Select(x => x.DistrictID).Distinct().ToList();
                foreach (var item in district)
                {
                    if (entity.ListWardId != null)
                    {
                        if (entity.ListWardId.Any(x => x.DistrictID == item))
                        {
                            foreach (var jtem in entity.ListWardId)
                            {
                                if (item == jtem.DistrictID)
                                {
                                    System.Data.DataRow row = dt.NewRow();
                                    row["ShopId"] = entity.Shopid;
                                    row["ProductId"] = entity.ProductId;
                                    row["DistrictId"] = item;
                                    row["WardId"] = jtem.WardID;
                                    row["ProvinceId"] = entity.CityId;
                                    row["CreateDate"] = DateTime.Now;
                                    dt.Rows.Add(row);
                                }

                            }
                        }
                        else
                        {
                            System.Data.DataRow row = dt.NewRow();
                            row["ShopId"] = entity.Shopid;
                            row["ProductId"] = entity.ProductId;
                            row["DistrictId"] = item;
                            row["WardId"] = 0;
                            row["ProvinceId"] = entity.CityId;
                            row["CreateDate"] = DateTime.Now;
                            dt.Rows.Add(row);
                        }
                    }
                }

                using (SqlConnection sqlconn = new SqlConnection(_configuration["ConnectionStrings:LOSDatabase"]))
                {
                    sqlconn.Open();
                    using (System.Data.SqlClient.SqlBulkCopy s = new System.Data.SqlClient.SqlBulkCopy(sqlconn))
                    {
                        s.DestinationTableName = dt.TableName;

                        foreach (var column in dt.Columns)
                            s.ColumnMappings.Add(column.ToString(), column.ToString());
                        s.WriteToServer(dt);
                    }
                    sqlconn.Close();
                    sqlconn.Dispose();
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "ManagerAreaHub/PostManagerAreaHub Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}