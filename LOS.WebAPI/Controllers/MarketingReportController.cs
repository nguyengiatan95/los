﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class MarketingController : Controller
    {
        private IUnitOfWork unitOfWork;
        private IConfiguration configuration;

        public MarketingController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            this.configuration = configuration;
        }


        [HttpGet]
        //[Authorize]
        [Route("report")]
        public ActionResult<DefaultResponse<SummaryMeta, List<MarketingReportUtmDTO>>> GetReports([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1, [FromQuery] int take = 10, [FromQuery] string name = null, [FromQuery] DateTime? fromDate = null , [FromQuery] DateTime? toDate = null , [FromQuery] string sortBy = "JobTitleId", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<MarketingReportUtmDTO>>();

            try
            {
                var query = unitOfWork.LoanBriefRepository.Query(x => (x.UtmSource.Contains(name) || name == null),null,false).GroupBy(x => x.UtmSource).Select(x => new MarketingReportUtmDTO
                {
                    UtmSource = x.First().UtmSource,
                    Status = x.First().Status.Value,
                    CountLoanBrief = x.Count(),
                    CountWaitTelesale = x.Count(m => m.Status == 10),
                    CountStatusDisbured = x.Count(m => m.Status == 50),
                    CountStatusCanceled = x.Count(m => m.Status == 99),
                    RateDisbured = 100 * x.Count(m => m.Status == 50) / x.Count(),
                    RateCanceled = 100 * x.Count(m => m.Status == 99) / x.Count(),
                });

                var query1 = unitOfWork.LoanBriefRepository.Query(x => (x.UtmSource.Contains(name) || name == null), null, false).Where(x => (x.CreatedTime >= fromDate || fromDate == null) && (x.CreatedTime <= toDate || toDate == null)).GroupBy(x => x.UtmSource).Select(x => new MarketingReportUtmDTO
                {
                    UtmSource = x.First().UtmSource,
                    Status = x.First().Status.Value,
                    CountLoanBrief = x.Count(),
                    CountWaitTelesale = x.Count(m => m.Status == 10),
                    CountStatusDisbured = x.Count(m => m.Status == 50),
                    CountStatusCanceled = x.Count(m => m.Status == 99),
                    RateDisbured = 100 * x.Count(m => m.Status == 50) / x.Count(),
                    RateCanceled = 100 * x.Count(m => m.Status == 99) / x.Count(),
                });

                var totalRecords = query1.Count();

                List<MarketingReportUtmDTO> list;
                list = query1.ToList();

                def.meta = new SummaryMeta(200,"success",page,pageSize,totalRecords);
                def.data = list;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Jobs/GetAllJobs Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE,0,0,0);
            }
            return Ok(def);
        }
    }
}