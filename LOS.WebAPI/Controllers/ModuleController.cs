﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.Common.Utils;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using System.Text.RegularExpressions;
using LOS.DAL.EntityFramework;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class ModuleController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;

        public ModuleController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
        }

        [HttpGet]
        public ActionResult<DefaultResponse<SummaryMeta, List<ModuleDetail>>> GetAllModule([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int status = -1, [FromQuery] int ismenu = -1, [FromQuery] int applicationId = -1, [FromQuery] string name = null, [FromQuery] string sortBy = "ModuleId", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<ModuleDetail>>();
            try
            {
                var query = _unitOfWork.ModulesRepository.Query(x => x.IsDeleted != true
                    && (x.Name.Contains(name) || name == null)
                    && (status == -1 || x.Status == status)
                    && (applicationId == -1 || x.ApplicationId.GetValueOrDefault((int)Common.Extensions.EnumApplication.WebLOS) == applicationId)
                    && (ismenu == -1 || x.IsMenu == (ismenu == 1))
                , null, false).Select(ModuleDetail.ProjectionDetail);
                var totalRecords = query.Count();
                var skip = (page - 1) * pageSize;
                var data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Module/GetAllModule Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("getall")]
        public ActionResult<DefaultResponse<Meta, List<Module>>> GetAll()
        {
            var def = new DefaultResponse<Meta, List<Module>>();
            try
            {
                var query = _unitOfWork.ModulesRepository.Query(x => x.IsDeleted != true, null, false).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Module/GetAll Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<DefaultResponse<Meta, ModuleDetail>> GetById(int id)
        {
            var def = new DefaultResponse<Meta, ModuleDetail>();
            try
            {
                var query = _unitOfWork.ModulesRepository.Query(x => x.ModuleId == id, null,
                    false).Select(ModuleDetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Module/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get")]
        public ActionResult<DefaultResponse<Meta, ModuleDetail>> GetBy(string controller, string action)
        {
            var def = new DefaultResponse<Meta, ModuleDetail>();
            try
            {
                var query = _unitOfWork.ModulesRepository.Query(x => x.Controller.ToLower() == controller && x.Action.ToLower() == action, null, false)
                    .Select(ModuleDetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Module/GetBy Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        public ActionResult<DefaultResponse<object>> Post([FromBody]ModuleDTO entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var result = _unitOfWork.ModulesRepository.Insert(entity.Mapping());
                _unitOfWork.Save();
                if (result.ModuleId > 0)
                {
                    def.data = result.ModuleId;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Module/Post Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut]
        [Route("{id}")]
        public ActionResult<DefaultResponse<object>> Put(int id, [FromBody]ModuleDTO entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != entity.ModuleId)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var entityOld = _unitOfWork.ModulesRepository.Query(x => x.ModuleId == entity.ModuleId, null,
                    false).Select(ModuleDetail.ProjectionDetail).FirstOrDefault().Mapping();
                Ultility.CopyObject(entity.Mapping(), ref entityOld, true);
                _unitOfWork.ModulesRepository.Update(entityOld);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Module/Put Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpDelete]
        [Route("{id}")]
        public ActionResult<DefaultResponse<object>> Delete(int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.ModulesRepository.SoftDelete(id);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Module/Delete Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("count_user")]
        public ActionResult<DefaultResponse<object>> NumberUserInModule(int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var query = _unitOfWork.UserModuleRepository.Count(x => x.ModuleId == id);
                def.data = query;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "NumberUserInModule Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("count_child")]
        public ActionResult<DefaultResponse<object>> NumberChild(int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var query = _unitOfWork.ModulesRepository.Count(x => x.ParentId == id);
                def.data = query;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "NumberChild Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("check_delete")]
        public ActionResult<DefaultResponse<object>> CheckDelete(int id)
        {
            var def = new DefaultResponse<object>();
            def.data = 0;
            try
            {
                var anyUser = _unitOfWork.UserModuleRepository.Any(x => x.ModuleId == id && x.User.IsDeleted != true);
                if (anyUser)
                    def.data = 1;
                else
                {
                    var anyChild = _unitOfWork.ModulesRepository.Any(x => x.ParentId == id && x.IsDeleted != true);
                    if (anyChild)
                        def.data = 2;
                    else
                    {
                        var anyGroupModule = _unitOfWork.GroupModuleRepository.Any(x => x.ModuleId == id);
                        if (anyGroupModule)
                            def.data = 3;
                    }
                }
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "NumberUserInModule Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_group")]
        public ActionResult<DefaultResponse<Meta, List<ModuleDetail>>> GetByGroupId(int groupId, int applicationId)
        {
            var def = new DefaultResponse<Meta, List<ModuleDetail>>();
            try
            {
                var query = _unitOfWork.GroupModuleRepository.Query(x => x.GroupId == groupId && x.ApplicationId.GetValueOrDefault((int)Common.Extensions.EnumApplication.WebLOS) == applicationId, null, false).Select(x => new ModuleDetail()
                {
                    ModuleId = x.Module.ModuleId,
                    Code = x.Module.Code,
                    Name = x.Module.Name,
                    Path = x.Module.Path,
                    Description = x.Module.Description,
                    Status = x.Module.Status,
                    ParentId = x.Module.ParentId,
                    Priority = x.Module.Priority,
                    IsMenu = x.Module.IsMenu.GetValueOrDefault(),
                    Controller = x.Module.Controller,
                    Action = x.Module.Action,
                }).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Module/GetByGroupId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_by_type")]
        public ActionResult<DefaultResponse<Meta, List<Module>>> GetAllByType(bool isMenu, int applicationId)
        {
            var def = new DefaultResponse<Meta, List<Module>>();
            try
            {
                var query = _unitOfWork.ModulesRepository.Query(x => x.IsMenu == isMenu && x.IsDeleted != true && x.ApplicationId.GetValueOrDefault((int)Common.Extensions.EnumApplication.WebLOS) == applicationId, null, false).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Module/GetAllByType Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("save_permission_user_api/{userId}")]
        public ActionResult<DefaultResponse<object>> SavePermissionUserApi(int userId, [FromBody] List<UserModule> entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //xóa toàn bộ dữ liệu cũ User-Module
                _unitOfWork.UserModuleRepository.Delete(x => x.UserId == userId && x.ApplicationId.GetValueOrDefault((int)Common.Extensions.EnumApplication.WebLOS) == (int)Common.Extensions.EnumApplication.ApiLOS);
                //Thêm dữ liệu mới User-Module
                _unitOfWork.UserModuleRepository.Insert(entity);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Module/SavePermissionUserApi Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut, HttpPost]
        [Route("save_permission_group_api/{groupId}")]
        public ActionResult<DefaultResponse<object>> SavePermissionGroupApi(int groupId, [FromBody] List<GroupModule> entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                //xóa toàn bộ dữ liệu cũ User-Module
                _unitOfWork.GroupModuleRepository.Delete(x => x.GroupId == groupId && x.ApplicationId.GetValueOrDefault((int)Common.Extensions.EnumApplication.WebLOS) == (int)Common.Extensions.EnumApplication.ApiLOS);
                //Thêm dữ liệu mới User-Module
                _unitOfWork.GroupModuleRepository.Insert(entity);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Module/SavePermissionGroupApi Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
