﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Helpers;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Models.Request;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbitMQ.Client;
using Serilog;

namespace LOS.WebAPI.Controllers
{

    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    //[Authorize]
    public class PipelineController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        private IConfiguration configuration;

        public PipelineController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            this.configuration = configuration;
        }

        [HttpGet]
        public ActionResult<DefaultResponse<Meta>> GetAll()
        {
            DefaultResponse<object> def = new DefaultResponse<object>();
            def.meta = ResponseHelper.SuccessMeta;
            var pipelines = unitOfWork.PipelineRepository.Query(x => x.Status != 99, x => x.OrderBy(x1 => x1.Priority), false)
                    //.Include("Section").Include("Section.SectionDetail")					
                    //.Include("Section.SectionDetail.SectionAction").Include("Section.SectionDetail.SectionAction.Property")
                    //.Include("Section.SectionDetail.SectionApproveApprove").Include("Section.SectionDetail.SectionApproveApprove.SectionCondition").Include("Section.SectionDetail.SectionApproveApprove.SectionCondition.Property")
                    .Select(x => new PipelineDTO()
                    {
                        CreatedAt = x.CreatedAt,
                        CreatorId = x.CreatorId,
                        ModifiedAt = x.ModifiedAt,
                        Name = x.Name,
                        PipelineId = x.PipelineId,
                        Priority = x.Priority,
                        Status = x.Status,
                        Sections = x.Section.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Order).Select(x1 => new SectionDTO()
                        {
                            Name = x1.Name,
                            Order = x1.Order,
                            SectionGroupId = x1.SectionGroupId,
                            SectionId = x1.SectionId,
                            IsShow = x1.IsShow,
                            PipelineId = x1.PipelineId,
                            Details = x1.SectionDetail.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Step).Select(x2 => new SectionDetailDTO()
                            {
                                Mode = x2.Mode,
                                Name = x2.Name,
                                SectionDetailId = x2.SectionDetailId,
                                SectionId = x2.SectionId,
                                Step = x2.Step,
                                IsShow = x2.IsShow,
                                DisapproveId = x2.DisapproveId,
                                ReturnId = x2.ReturnId,
                                Approves = x2.SectionApproveSectionDetail.Where(x => x.Type == 1).Select(x3 => new SectionApproveDTO()
                                {
                                    Name = x3.Name,
                                    Type = x3.Type,
                                    Approve = x3.Approve != null ? new SectionDetailDTO()
                                    {
                                        Name = x3.Approve.Name,
                                        SectionDetailId = x3.Approve.SectionDetailId,
                                        SectionId = x3.Approve.SectionId,
                                        Step = x3.Approve.Step
                                    } : null,
                                    SectionApproveId = x3.SectionApproveId,
                                    SectionDetailId = x3.SectionDetailId,
                                    ApproveId = x3.ApproveId,
                                    Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
                                    {
                                        Operator = x4.Operator,
                                        PropertyId = x4.PropertyId,
                                        SectionConditionId = x4.SectionConditionId,
                                        SectionApproveId = x4.SectionApproveId,
                                        Value = x4.Value,
                                        Property = x4.Property != null ? new PropertyDTO()
                                        {
                                            Object = x4.Property.Object,
                                            FieldName = x4.Property.FieldName,
                                            Priority = x4.Property.Priority,
                                            Regex = x4.Property.Regex,
                                            Type = x4.Property.Type,
                                            PropertyId = x4.Property.PropertyId,
                                            Name = x4.Property.Name,
                                            LinkObject = x4.Property.LinkObject,
                                            MultipleSelect = x4.Property.MultipleSelect
                                        } : null
                                    })
                                }),
                                Disapproves = x2.SectionApproveSectionDetail.Where(x => x.Type == 2).Select(x3 => new SectionApproveDTO()
                                {
                                    Name = x3.Name,
                                    Type = x3.Type,
                                    Approve = x3.Approve != null ? new SectionDetailDTO()
                                    {
                                        Name = x3.Approve.Name,
                                        SectionDetailId = x3.Approve.SectionDetailId,
                                        SectionId = x3.Approve.SectionId,
                                        Step = x3.Approve.Step
                                    } : null,
                                    SectionApproveId = x3.SectionApproveId,
                                    SectionDetailId = x3.SectionDetailId,
                                    ApproveId = x3.ApproveId,
                                    Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
                                    {
                                        Operator = x4.Operator,
                                        PropertyId = x4.PropertyId,
                                        SectionConditionId = x4.SectionConditionId,
                                        SectionApproveId = x4.SectionApproveId,
                                        Value = x4.Value,
                                        Property = x4.Property != null ? new PropertyDTO()
                                        {
                                            Object = x4.Property.Object,
                                            FieldName = x4.Property.FieldName,
                                            Priority = x4.Property.Priority,
                                            Regex = x4.Property.Regex,
                                            Type = x4.Property.Type,
                                            PropertyId = x4.Property.PropertyId,
                                            Name = x4.Property.Name,
                                            LinkObject = x4.Property.LinkObject,
                                            MultipleSelect = x4.Property.MultipleSelect
                                        } : null
                                    })
                                }),                               
                                Return = x2.Return != null ? new SectionDetailDTO()
                                {
                                    Name = x2.Return.Name,
                                    SectionDetailId = x2.Return.SectionDetailId,
                                    SectionId = x2.Return.SectionId,
                                    Step = x2.Return.Step
                                } : null,
                                Actions = x2.SectionAction.Select(x3 => new SectionActionDTO()
                                {
                                    PropertyId = x3.PropertyId,
                                    SectionActionId = x3.SectionActionId,
                                    SectionDetailId = x3.SectionDetailId,
                                    Value = x3.Value,
                                    Type = x3.Type,
                                    Property = x3.Property != null ? new PropertyDTO()
                                    {
                                        Object = x3.Property.Object,
                                        FieldName = x3.Property.FieldName,
                                        Priority = x3.Property.Priority,
                                        Regex = x3.Property.Regex,
                                        Type = x3.Property.Type,
                                        PropertyId = x3.Property.PropertyId,
                                        Name = x3.Property.Name,
                                        LinkObject = x3.Property.LinkObject,
                                        MultipleSelect = x3.Property.MultipleSelect
                                    } : null
                                })
                            })
                        })
                    }).ToList();
            def.data = pipelines;
            return Ok(def);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<DefaultResponse<Meta>> GetById(int id)
        {
            DefaultResponse<object> def = new DefaultResponse<object>();
            def.meta = ResponseHelper.SuccessMeta;
            var pipeline = unitOfWork.PipelineRepository.Query(x => x.Status != 0 && x.PipelineId == id && x.Status != 99, x => x.OrderBy(x1 => x1.Priority), false)
                    //.Include("Section").Include("Section.SectionDetail")					
                    //.Include("Section.SectionDetail.SectionAction").Include("Section.SectionDetail.SectionAction.Property")
                    //.Include("Section.SectionDetail.SectionApproveSectionDetail").Include("Section.SectionDetail.SectionApproveSectionDetail.SectionCondition").Include("Section.SectionDetail.SectionApproveSectionDetail.SectionCondition.Property")
                    .Select(x => new PipelineDTO()
                    {
                        CreatedAt = x.CreatedAt,
                        CreatorId = x.CreatorId,
                        ModifiedAt = x.ModifiedAt,
                        Name = x.Name,
                        PipelineId = x.PipelineId,
                        Priority = x.Priority,
                        Status = x.Status,
                        Sections = x.Section.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Order).Select(x1 => new SectionDTO()
                        {
                            Name = x1.Name,
                            Order = x1.Order,
                            SectionGroupId = x1.SectionGroupId,
                            SectionId = x1.SectionId,
                            IsShow = x1.IsShow,
                            PipelineId = x1.PipelineId,
                            Details = x1.SectionDetail.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Step).Select(x2 => new SectionDetailDTO()
                            {
                                Mode = x2.Mode,
                                Name = x2.Name,
                                SectionDetailId = x2.SectionDetailId,
                                SectionId = x2.SectionId,
                                Step = x2.Step,
                                IsShow = x2.IsShow,
                                DisapproveId = x2.DisapproveId,
                                ReturnId = x2.ReturnId,
                                Approves = x2.SectionApproveSectionDetail.Where(x => x.Type == 1).Select(x3 => new SectionApproveDTO()
                                {
                                    Name = x3.Name,
                                    Type = x3.Type,
                                    Approve = x3.Approve != null ? new SectionDetailDTO()
                                    {
                                        Name = x3.Approve.Name,
                                        SectionDetailId = x3.Approve.SectionDetailId,
                                        SectionId = x3.Approve.SectionId,
                                        Step = x3.Approve.Step
                                    } : null,
                                    SectionApproveId = x3.SectionApproveId,
                                    SectionDetailId = x3.SectionDetailId,
                                    ApproveId = x3.ApproveId,
                                    Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
                                    {
                                        Operator = x4.Operator,
                                        PropertyId = x4.PropertyId,
                                        SectionConditionId = x4.SectionConditionId,
                                        SectionApproveId = x4.SectionApproveId,
                                        Value = x4.Value,
                                        Property = x4.Property != null ? new PropertyDTO()
                                        {
                                            Object = x4.Property.Object,
                                            FieldName = x4.Property.FieldName,
                                            Priority = x4.Property.Priority,
                                            Regex = x4.Property.Regex,
                                            Type = x4.Property.Type,
                                            PropertyId = x4.Property.PropertyId,
                                            Name = x4.Property.Name,
                                            LinkObject = x4.Property.LinkObject,
                                            MultipleSelect = x4.Property.MultipleSelect
                                        } : null
                                    })
                                }),
                                Disapproves = x2.SectionApproveSectionDetail.Where(x => x.Type == 2).Select(x3 => new SectionApproveDTO()
                                {
                                    Name = x3.Name,
                                    Type = x3.Type,
                                    Approve = x3.Approve != null ? new SectionDetailDTO()
                                    {
                                        Name = x3.Approve.Name,
                                        SectionDetailId = x3.Approve.SectionDetailId,
                                        SectionId = x3.Approve.SectionId,
                                        Step = x3.Approve.Step
                                    } : null,
                                    SectionApproveId = x3.SectionApproveId,
                                    SectionDetailId = x3.SectionDetailId,
                                    ApproveId = x3.ApproveId,
                                    Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
                                    {
                                        Operator = x4.Operator,
                                        PropertyId = x4.PropertyId,
                                        SectionConditionId = x4.SectionConditionId,
                                        SectionApproveId = x4.SectionApproveId,
                                        Value = x4.Value,
                                        Property = x4.Property != null ? new PropertyDTO()
                                        {
                                            Object = x4.Property.Object,
                                            FieldName = x4.Property.FieldName,
                                            Priority = x4.Property.Priority,
                                            Regex = x4.Property.Regex,
                                            Type = x4.Property.Type,
                                            PropertyId = x4.Property.PropertyId,
                                            Name = x4.Property.Name,
                                            LinkObject = x4.Property.LinkObject,
                                            MultipleSelect = x4.Property.MultipleSelect
                                        } : null
                                    })
                                }),                               
                                Return = x2.Return != null ? new SectionDetailDTO()
                                {
                                    Name = x2.Return.Name,
                                    SectionDetailId = x2.Return.SectionDetailId,
                                    SectionId = x2.Return.SectionId,
                                    Step = x2.Return.Step
                                } : null,
                                Actions = x2.SectionAction.Select(x3 => new SectionActionDTO()
                                {
                                    PropertyId = x3.PropertyId,
                                    SectionActionId = x3.SectionActionId,
                                    SectionDetailId = x3.SectionDetailId,
                                    Value = x3.Value,
                                    Type = x3.Type,
                                    Property = x3.Property != null ? new PropertyDTO()
                                    {
                                        Object = x3.Property.Object,
                                        FieldName = x3.Property.FieldName,
                                        Priority = x3.Property.Priority,
                                        Regex = x3.Property.Regex,
                                        Type = x3.Property.Type,
                                        PropertyId = x3.Property.PropertyId,
                                        Name = x3.Property.Name,
                                        LinkObject = x3.Property.LinkObject,
                                        MultipleSelect = x3.Property.MultipleSelect
                                    } : null
                                })
                            })
                        })
                    }).FirstOrDefault();
            def.data = pipeline;
            return Ok(def);
        }

        [HttpPost]
        public ActionResult<DefaultResponse<Meta>> CreatePipeline([FromBody] PipelineDTO dto)
        {
            DefaultResponse<object> def = new DefaultResponse<object>();
            Pipeline pipeline = new Pipeline();
            pipeline.Name = dto.Name;
            pipeline.CreatedAt = DateTime.Now;
            pipeline.Status = 1;
            pipeline.CreatorId = dto.CreatorId;
            pipeline.Priority = 99;
            pipeline.Note = dto.Note;
            unitOfWork.PipelineRepository.Insert(pipeline);
            unitOfWork.Save();
            // Tạo sectioh mặc định hủy
            var section = new Section();
            section.IsShow = false;
            section.Name = "Hủy";
            section.PipelineId = pipeline.PipelineId;
            section.Status = 0;
            section.Order = 999;
            unitOfWork.SectionRepository.Insert(section);
            unitOfWork.Save();
            // create section detail
            var sectionDetail = new SectionDetail();
            sectionDetail.SectionId = section.SectionId;
            sectionDetail.Name = "Hủy";
            sectionDetail.Step = 1;
            sectionDetail.Mode = "AUTOMATIC";
            sectionDetail.IsShow = false;
            sectionDetail.Status = 0;
            unitOfWork.SectionDetailRepository.Insert(sectionDetail);
            unitOfWork.Save();
            // tạo action cho detail
            var action = new SectionAction();
            action.PropertyId = 4;
            action.SectionDetailId = sectionDetail.SectionDetailId;
            action.Value = "99";
            action.Type = 1;
            unitOfWork.SectionActionRepository.Insert(action);
            unitOfWork.Save();
            def.meta = ResponseHelper.SuccessMeta;
            return Ok(def);
        }

        [HttpPut]
        [Route("{id}")]
        public ActionResult<DefaultResponse<Meta>> UpdatePipeline(int id, [FromBody] PipelineDTO dto)
        {
            DefaultResponse<object> def = new DefaultResponse<object>();
            var pipeline = unitOfWork.PipelineRepository.GetById(id);
            if (pipeline != null)
            {
                pipeline.Name = dto.Name;
                pipeline.Note = dto.Note;
                unitOfWork.Save();
                def.meta = ResponseHelper.SuccessMeta;
                return Ok(def);
            }
            else
            {
                def.meta = ResponseHelper.NotFoundMeta;
            }
            return Ok(def);
        }

        [HttpDelete]
        [Route("{id}")]
        public ActionResult<DefaultResponse<Meta>> DeletePipeline(int id)
        {
            DefaultResponse<object> def = new DefaultResponse<object>();
            var pipeline = unitOfWork.PipelineRepository.GetById(id);
            if (pipeline != null)
            {
                pipeline.Status = 99;
                unitOfWork.Save();
                def.meta = ResponseHelper.SuccessMeta;
            }
            else
            {
                def.meta = ResponseHelper.NotFoundMeta;
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("{id}/change_status")]
        public ActionResult<DefaultResponse<Meta>> ChangeStatus(int id)
        {
            DefaultResponse<object> def = new DefaultResponse<object>();
            var pipeline = unitOfWork.PipelineRepository.GetById(id);
            if (pipeline != null)
            {
                if (pipeline.Status == 1)
                    pipeline.Status = 0;
                else
                    pipeline.Status = 1;
                unitOfWork.Save();
                def.meta = ResponseHelper.SuccessMeta;
            }
            else
            {
                def.meta = ResponseHelper.NotFoundMeta;
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("orders")]
        public ActionResult<DefaultResponse<Meta>> OrderSections([FromBody] UpdateSectionOrderReq req)
        {
            DefaultResponse<object> def = new DefaultResponse<object>();
            if (req.data != null && req.data.Count > 0)
            {
                foreach (var d in req.data)
                {
                    if (d.id > 0 && d.order > 0)
                    {
                        unitOfWork.SectionRepository.Update(x => x.SectionId == d.id, x => new Section()
                        {
                            Order = d.order
                        });
                    }
                }
                def.meta = ResponseHelper.SuccessMeta;
                return Ok(def);
            }
            def.data = ResponseHelper.BadInputMeta;
            return Ok(def);
        }

        [HttpPost]
        [Route("copy")]
        public ActionResult<DefaultResponse<Meta>> CopyPipeline([FromBody] CopyPipelineReq req)
        {
            DefaultResponse<object> def = new DefaultResponse<object>();
            var pipeline = unitOfWork.PipelineRepository.GetById(req.id);
            if (pipeline != null)
            {
                try
                {
                    unitOfWork.BeginTransaction();
                    var p = new Pipeline();
                    p.CreatedAt = DateTime.Now;
                    p.CreatorId = req.creatorId;
                    p.Name = req.name;
                    p.Priority = 1;
                    p.Status = 1;
                    unitOfWork.PipelineRepository.Insert(p);
                    unitOfWork.Save();
                    if (p.PipelineId > 0)
                    {
                        var sections = unitOfWork.SectionRepository.Query(x => x.PipelineId == pipeline.PipelineId, null, false).Include("SectionDetail").Include("SectionDetail.SectionAction").Include("SectionDetail.Disapprove").Include("SectionDetail.Return").ToList();
                        foreach (var section in sections)
                        {
                            var s = new Section();
                            s.IsShow = section.IsShow;
                            s.Name = section.Name;
                            s.Order = section.Order;
                            s.PipelineId = p.PipelineId;
                            s.Status = section.Status;
                            unitOfWork.SectionRepository.Insert(s);
                            unitOfWork.Save();
                            foreach (var detail in section.SectionDetail)
                            {
                                var sd = new SectionDetail();
                                sd.IsShow = detail.IsShow;
                                sd.Mode = detail.Mode;
                                sd.Name = detail.Name;
                                sd.SectionId = s.SectionId;
                                sd.Status = detail.Status;
                                sd.Step = detail.Step;
                                unitOfWork.SectionDetailRepository.Insert(sd);
                                unitOfWork.Save();
                                foreach (var action in detail.SectionAction)
                                {
                                    var a = new SectionAction();
                                    a.PropertyId = action.PropertyId;
                                    a.SectionDetailId = sd.SectionDetailId;
                                    a.Type = action.Type;
                                    a.Value = action.Value;
                                    unitOfWork.SectionActionRepository.Insert(a);
                                    unitOfWork.Save();
                                }
                            }
                        }
                        foreach (var section in sections)
                        {
                            foreach (var detail in section.SectionDetail)
                            {
                                if (detail.DisapproveId > 0)
                                {
                                    unitOfWork.SectionDetailRepository.Update(x => x.Name == detail.Name, x => new SectionDetail()
                                    {
                                        DisapproveId = unitOfWork.SectionDetailRepository.Query(x1 => x1.Name == detail.Disapprove.Name, null, false).First().SectionDetailId
                                    });
                                    unitOfWork.Save();
                                }
                                if (detail.ReturnId > 0)
                                {
                                    unitOfWork.SectionDetailRepository.Update(x => x.Name == detail.Name, x => new SectionDetail()
                                    {
                                        ReturnId = unitOfWork.SectionDetailRepository.Query(x1 => x1.Name == detail.Return.Name, null, false).First().SectionDetailId
                                    });
                                    unitOfWork.Save();
                                }
                            }
                        }
                        unitOfWork.CommitTransaction();
                        def.meta = ResponseHelper.SuccessMeta;
                    }
                    else
                    {
                        def.meta = ResponseHelper.FaileMeta;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Section Detail Exception");
                    unitOfWork.RollbackTransaction();
                    def.meta = ResponseHelper.InternalErrorMeta;
                }
            }
            else
            {
                def.meta = ResponseHelper.NotFoundMeta;
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("fix_disapprove")]
        public IActionResult FixDisapprove()
		{
            var sectionDetails = unitOfWork.SectionDetailRepository.Query(x => x.Status == 0 && x.DisapproveId >0, null, false).ToList();
            if (sectionDetails != null && sectionDetails.Count > 0)
            {
                // Check exists
                List<SectionApprove> disapproves = new List<SectionApprove>();
                foreach (var sectionDetail in sectionDetails)
                {
                    if (!unitOfWork.SectionApproveRepository.Any(x => x.SectionDetailId == sectionDetail.SectionDetailId && x.Type == 2 && x.ApproveId == sectionDetail.DisapproveId))
                    {
                        SectionApprove sectionApprove = new SectionApprove();
                        sectionApprove.ApproveId = sectionDetail.DisapproveId;
                        sectionApprove.SectionDetailId = sectionDetail.SectionDetailId;
                        sectionApprove.Type = 2;
                        disapproves.Add(sectionApprove);
                    }
                }
                unitOfWork.SectionApproveRepository.Insert(disapproves);
                unitOfWork.SaveChanges();
            }
            return Ok("Ok");
		}

        [HttpPost]
        [Route("change_pipeline")]
        public ActionResult<DefaultResponse<object>> ChangePipeline([FromBody] ChangePipelineReq req)
        {
            DefaultResponse<object> def = new DefaultResponse<object>();
            try
            {
                var loanBrief = unitOfWork.LoanBriefRepository.GetById(req.loanBriefId);
                var result = SimulatePipeline(loanBrief, req.productId, req.status);
                if (result == 1)
                {
                    def.meta = new Meta(200, "success");
                }
                else if (result == -1)
                {
                    def.meta = new Meta(222, "status not match");
                }
                else if (result == -2)
                {
                    def.meta = new Meta(223, "pipeline not match");
                }
                else
                {
                    def.meta = new Meta(201, "fail");
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Information("ChangePipeline Exception:" + ex.Message + "\n" + ex.StackTrace);
                def.meta = new Meta(500, "internal server error");
                return Ok(def);
            }
        }

        int SimulatePipeline(LoanBrief loanBrief, int newProductId, int newStatus)
        {
            try
            {
                var pipelines = getPipeline();
                bool IsOk = false;
                if (pipelines != null && pipelines.Count > 0)
                {
                    DatabaseHelper<LoanBrief> helper = new DatabaseHelper<LoanBrief>();
                    // gán lại productId
                    loanBrief.ProductId = newProductId;
                    //if (loanBrief.CurrentPipelineId != null)
                    //{
                    PipelineDTO newPipeline = null;
                    foreach (var pipeline in pipelines)
                    {
                        var sections = pipeline.Sections;
                        if (sections != null && sections.Count() > 0)
                        {
                            SectionDetailDTO lastAutomaticSuccess = null;
                            int indexApproveSucess = -1;
                            foreach (var section in sections)
                            {
                                // for each detail	
                                int detailSucess = 0;
                                int detailCount = section.Details != null ? section.Details.Count() : 0;
                                if (detailCount == 0)
                                {
                                    goto gotonextPipeline;
                                }
                                foreach (var detail in section.Details)
                                {
                                    if (detail.Mode == "AUTOMATIC")
                                    {
                                        if (detail.Approves != null && detail.Approves.Count() > 0)
                                        {
                                            bool IsOneSuccess = false;
                                            int index = 0;
                                            foreach (var approve in detail.Approves)
                                            {
                                                IsOneSuccess = false;
                                                int conditionSuccess = 0;
                                                int conditionCount = approve.Conditions != null ? approve.Conditions.Count() : 0;
                                                if (conditionCount <= 0)
                                                {
                                                    goto gotonextPipeline;
                                                }
                                                foreach (var condition in approve.Conditions)
                                                {
                                                    if (helper.CheckFieldOperator(loanBrief, condition.Property.FieldName, condition.Operator, condition.Value, condition.Property.Type))
                                                    {
                                                        indexApproveSucess = index;
                                                        Console.WriteLine("Condition successed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                        conditionSuccess++;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("Condition failed:" + loanBrief.LoanBriefId + ":" + condition.Property.FieldName + ":" + condition.Operator + ":" + condition.Value + ":" + condition.Property.Type);
                                                    }
                                                }
                                                // nếu điều kiện thành công hết thì chuyển đến aprrove section
                                                if (conditionSuccess == conditionCount)
                                                {
                                                    // detail success
                                                    detailSucess++;
                                                    lastAutomaticSuccess = detail;
                                                    IsOneSuccess = true;
                                                    break;
                                                }
                                                index++;
                                            }
                                            if (!IsOneSuccess)
                                            {
                                                goto gotonextPipeline;
                                            }
                                            else
                                            {
                                                IsOk = true;
                                                newPipeline = pipeline;
                                                goto successed;
                                            }
                                        }
                                        else
                                        {
                                            return 0;
                                        }
                                    }
                                    else
                                    {
                                        // kiêm tra xem có đúng section sau khi kiểm tra không
                                        if (lastAutomaticSuccess != null)
                                        {
                                            if (indexApproveSucess >= 0)
                                            {
                                                if (lastAutomaticSuccess.Approves.ToList()[indexApproveSucess].ApproveId == detail.SectionDetailId)
                                                {
                                                    IsOk = true;
                                                    newPipeline = pipeline;
                                                    goto successed;
                                                }
                                                else
                                                {
                                                    goto gotonextPipeline;
                                                }
                                            }
                                            else
                                            {
                                                goto gotonextPipeline;
                                            }
                                        }
                                        else
                                        {
                                            goto gotonextPipeline;
                                        }
                                    }
                                }
                            }
                        }
                    gotonextPipeline:
                        {
                            Console.WriteLine("Go to next pipeline, Current pipeline : " + pipeline.Name);
                        }
                    }
                successed:
                    {
                        if (IsOk)
                        {
                            if (newPipeline != null)
                            {
                                // tìm xem action có status tương ứng
                                var sectionAction = unitOfWork.SectionActionRepository.Query(x => x.PropertyId == 4 && x.Value == newStatus.ToString() && x.SectionDetail.Status != 99 && x.SectionDetail.Section.PipelineId == newPipeline.PipelineId, null, false).FirstOrDefault();
                                if (sectionAction != null)
                                {
                                    var sectionDetail = unitOfWork.SectionDetailRepository.GetById(sectionAction.SectionDetailId);
                                    // gán lại pipeline đơn vay
                                    if (sectionDetail != null)
                                    {
                                        //save log
                                        var currentHistory = new PipelineHistory();
                                        currentHistory.ActionState = null;
                                        currentHistory.BeginSectionDetailId = loanBrief.CurrentSectionDetailId;
                                        currentHistory.BeginSectionId = loanBrief.CurrentSectionId;
                                        currentHistory.BeginStatus = loanBrief.Status;
                                        currentHistory.BeginTime = DateTimeOffset.Now;
                                        currentHistory.CreatedDate = DateTimeOffset.Now;
                                        currentHistory.Data = JsonConvert.SerializeObject(loanBrief);
                                        currentHistory.LoanBriefId = loanBrief.LoanBriefId;
                                        currentHistory.PipelineId = newPipeline.PipelineId;
                                        currentHistory.EndSectionDetailId = sectionDetail.SectionDetailId;
                                        currentHistory.EndSectionId = sectionDetail.SectionId;
                                        if (sectionDetail.Mode.Equals("MANUAL"))
                                            currentHistory.EndPipelineState = 20;
                                        else
                                            currentHistory.EndPipelineState = 30;
                                        currentHistory.EndStatus = newStatus;
                                        currentHistory.ExecuteTime = 0;
                                        unitOfWork.PipelineHistoryRepository.Insert(currentHistory);
                                        loanBrief.CurrentPipelineId = newPipeline.PipelineId;
                                        loanBrief.CurrentSectionId = sectionDetail.SectionId;
                                        loanBrief.CurrentSectionDetailId = sectionDetail.SectionDetailId;
                                        loanBrief.Status = newStatus;
                                        if (sectionDetail.Mode.Equals("MANUAL"))
                                            loanBrief.PipelineState = 20;
                                        else
                                            loanBrief.PipelineState = 30;
                                        loanBrief.AddedToQueue = false;
                                        loanBrief.InProcess = 0;
                                        loanBrief.ActionState = null;
                                        unitOfWork.LoanBriefRepository.Update(loanBrief);
                                        unitOfWork.SaveChanges();
                                        return 1;
                                    }
                                }
                                else
                                {
                                    return -1;
                                }
                                Console.WriteLine("Pipeline Successed:" + loanBrief.LoanBriefId);
                            }
                        }
                        else
                        {
                            return -2;
                        }
                    }
                    //}
                    //else
                    //{
                    //	return 0;
                    //}
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Pipeline Exception:" + ex.Message);
                Console.WriteLine("Pipeline Exception:" + ex.StackTrace);
            }
            return 0;
        }

        bool CheckValidStatus(PipelineDTO pipeline, int status)
        {
            return false;
        }

        private List<PipelineDTO> getPipeline()
        {
            try
            {
                return unitOfWork.PipelineRepository.Query(x => x.Status != 99 && x.Status != 0, x => x.OrderBy(x1 => x1.Priority), false)
                    .Select(x => new PipelineDTO()
                    {
                        CreatedAt = x.CreatedAt,
                        CreatorId = x.CreatorId,
                        ModifiedAt = x.ModifiedAt,
                        Name = x.Name,
                        PipelineId = x.PipelineId,
                        Priority = x.Priority,
                        Status = x.Status,
                        Sections = x.Section.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Order).Select(x1 => new SectionDTO()
                        {
                            Name = x1.Name,
                            Order = x1.Order,
                            SectionGroupId = x1.SectionGroupId,
                            SectionId = x1.SectionId,
                            IsShow = x1.IsShow,
                            PipelineId = x1.PipelineId,
                            Details = x1.SectionDetail.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Step).Select(x2 => new SectionDetailDTO()
                            {
                                Mode = x2.Mode,
                                Name = x2.Name,
                                SectionDetailId = x2.SectionDetailId,
                                SectionId = x2.SectionId,
                                Step = x2.Step,
                                IsShow = x2.IsShow,
                                Approves = x2.SectionApproveSectionDetail.Select(x3 => new SectionApproveDTO()
                                {
                                    Name = x3.Name,
                                    Approve = x3.Approve != null ? new SectionDetailDTO()
                                    {
                                        Name = x3.Approve.Name,
                                        SectionDetailId = x3.Approve.SectionDetailId,
                                        SectionId = x3.Approve.SectionId,
                                        Step = x3.Approve.Step
                                    } : null,
                                    SectionApproveId = x3.SectionApproveId,
                                    SectionDetailId = x3.SectionDetailId,
                                    ApproveId = x3.ApproveId,
                                    Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
                                    {
                                        Operator = x4.Operator,
                                        PropertyId = x4.PropertyId,
                                        SectionConditionId = x4.SectionConditionId,
                                        SectionApproveId = x4.SectionApproveId,
                                        Value = x4.Value,
                                        Property = x4.Property != null ? new PropertyDTO()
                                        {
                                            Object = x4.Property.Object,
                                            FieldName = x4.Property.FieldName,
                                            Priority = x4.Property.Priority,
                                            Regex = x4.Property.Regex,
                                            Type = x4.Property.Type,
                                            PropertyId = x4.Property.PropertyId,
                                            Name = x4.Property.Name,
                                            LinkObject = x4.Property.LinkObject
                                        } : null
                                    })
                                }),
                                DisapproveId = x2.DisapproveId,
                                Disapprove = x2.Disapprove != null ? new SectionDetailDTO()
                                {
                                    Name = x2.Disapprove.Name,
                                    SectionDetailId = x2.Disapprove.SectionDetailId,
                                    SectionId = x2.Disapprove.SectionId,
                                    Step = x2.Disapprove.Step
                                } : null,
                                ReturnId = x2.ReturnId,
                                Return = x2.Return != null ? new SectionDetailDTO()
                                {
                                    Name = x2.Return.Name,
                                    SectionDetailId = x2.Return.SectionDetailId,
                                    SectionId = x2.Return.SectionId,
                                    Step = x2.Return.Step
                                } : null,
                                Actions = x2.SectionAction.Select(x3 => new SectionActionDTO()
                                {
                                    PropertyId = x3.PropertyId,
                                    SectionActionId = x3.SectionActionId,
                                    SectionDetailId = x3.SectionDetailId,
                                    Value = x3.Value,
                                    Type = x3.Type,
                                    Property = x3.Property != null ? new PropertyDTO()
                                    {
                                        Object = x3.Property.Object,
                                        FieldName = x3.Property.FieldName,
                                        Priority = x3.Property.Priority,
                                        Regex = x3.Property.Regex,
                                        Type = x3.Property.Type,
                                        PropertyId = x3.Property.PropertyId,
                                        Name = x3.Property.Name,
                                        LinkObject = x3.Property.LinkObject
                                    } : null
                                })
                            })
                        })
                    }).ToList();
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        //[HttpPost]
        //[Route("add_to_queue")]
        //public ActionResult<DefaultResponse<Meta>> AddToQueue([FromBody] AddToQueueReq req)
        //{
        //	DefaultResponse<object> def = new DefaultResponse<object>();
        //	if (req.id > 0 && req.state > 0)
        //	{
        //		// check forceRefresh
        //		var forceRefresh = false;
        //		// end check				
        //		// kiểm tra xem có action nào trong 5s không
        //		var now = DateTimeOffset.Now.AddSeconds(-5);
        //		var log = unitOfWork.QueueLogRepository.Query(x => x.LoanBriefId == req.id
        //			&& x.PushBy == req.pushBy && x.PushAt >= now, null, false).FirstOrDefault();
        //		var brief = unitOfWork.LoanBriefRepository.GetById(req.id);
        //		if (log == null && brief != null)
        //		{
        //			// insert to logs
        //			var ql = new QueueLog()
        //			{
        //				LoanBriefId = req.id,
        //				PushAt = DateTimeOffset.Now,
        //				PipelineState = req.state,
        //				PushBy = req.pushBy
        //			};
        //			unitOfWork.QueueLogRepository.SingleInsert(ql);					
        //			if (ql.QueueLogId > 0)
        //			{
        //				// added to rabbitmq									
        //				var factory = new ConnectionFactory
        //				{
        //					HostName = configuration["AppSettings:RabbitmqHost"],
        //					Port = Int32.Parse(configuration["AppSettings:RabbitmqPort"]),
        //					UserName = configuration["AppSettings:RabbitmqUsername"],
        //					Password = configuration["AppSettings:RabbitmqPassword"]
        //				};
        //				using ( var connection = factory.CreateConnection())
        //				using (var channel = connection.CreateModel())
        //				{
        //					try
        //					{
        //						//channel.ExchangeDeclare("los.exchange", ExchangeType.Direct);
        //						channel.QueueDeclare(queue: "los.normal_loan_queue",
        //											 durable: true,
        //											 exclusive: false,
        //											 autoDelete: false,
        //											 arguments: null);								

        //						var properties = channel.CreateBasicProperties();
        //						properties.Persistent = true;
        //						IdOnlyReq idReq = new IdOnlyReq();
        //						idReq.id = brief.LoanBriefId;
        //						idReq.forceRefresh = forceRefresh;
        //						var message = JsonConvert.SerializeObject(idReq);
        //						var body = Encoding.UTF8.GetBytes(message);
        //						channel.BasicPublish(exchange: "",
        //											 routingKey: "los.normal_loan_queue",
        //											 basicProperties: properties,
        //											 body: body);
        //						// update added to queue
        //						unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == brief.LoanBriefId, x => new LoanBrief()
        //						{
        //							AddedToQueue = true,
        //							InProcess = 1,
        //							PipelineState = req.state
        //						});
        //						unitOfWork.Save();
        //						Log.Information("Pipeline: LoanBriefID: " + idReq.id + " added to queue by " + req.pushBy + " with " + req.state);
        //						def.meta = ResponseHelper.SuccessMeta;
        //						return Ok(def);
        //					}
        //					catch (Exception ex)
        //					{
        //						def.meta = ResponseHelper.FaileMeta;
        //						return Ok(def);
        //					}
        //				}
        //			}
        //			else
        //			{
        //				def.meta = ResponseHelper.FaileMeta;
        //				return Ok(def);
        //			}
        //		}
        //		else
        //		{
        //			if (log != null)
        //			{
        //				def.meta = new Meta(222, "duplicated action in 5s");
        //				return Ok(def);
        //			}
        //			else
        //			{
        //				def.meta = ResponseHelper.BadInputMeta;
        //				return Ok(def);
        //			}	
        //		}
        //	}
        //	else
        //	{
        //		def.meta = ResponseHelper.BadInputMeta;
        //		return Ok(def);
        //	}
        //}
    }
}