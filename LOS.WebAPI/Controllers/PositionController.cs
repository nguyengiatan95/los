﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class PositionController : Controller
    {
        private IUnitOfWork unitOfWork;
        private IConfiguration configuration;

        public PositionController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            this.configuration = configuration;
        }


        [HttpGet]
        [Authorize]
        public ActionResult<DefaultResponse<SummaryMeta, List<PositionDetail>>> GetAllPosition([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1, [FromQuery] int take = 10, [FromQuery] int status = -1, [FromQuery] string name = null, [FromQuery] string sortBy = "PositionId", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<PositionDetail>>();

            try
            {
                var query = unitOfWork.PositionRepository.Query(x => (x.Status == status || status == -1) && (x.Name.Contains(name) || name == null), null, false).Select(PositionDetail.ProjectionDetail);

                var totalRecords = query.Count();
                List<PositionDetail> data;
                if (skip >= 0)
                {
                    //data = query.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    data = query.ToList();
                }
                else
                {
                    data = query.Skip(skip).Take(take).ToList();
                }

                def.meta = new SummaryMeta(200, "success", page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Position/GetAllPosition Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }



        [HttpGet]
        [Route("{id}")]
        [Authorize]
        public ActionResult<DefaultResponse<Meta, PositionDTO>> GetPosition(int Id)
        {
            var def = new DefaultResponse<Meta, PositionDTO>();
            try
            {
                var position = unitOfWork.PositionRepository.GetById(Id).Mapping();
                def.data = position;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Position/GetPositionById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Authorize]
        public ActionResult<DefaultResponse<object>> PostPosition([FromBody] PositionDTO position)
        {
            var def = new DefaultResponse<object>();

            try
            {
                var result = unitOfWork.PositionRepository.Insert(position.Mapping());
                unitOfWork.Save();

                if (result.PositionId > 0)
                {
                    def.data = result.PositionId;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Position/CreatePositon Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }

            return Ok();
        }


        [HttpPut]
        [Route("{id}")]
        [Authorize]
        public ActionResult<DefaultResponse<object>> PutPosition(int id, [FromBody] PositionDTO position)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != position.PositionId)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                unitOfWork.PositionRepository.Update(position.Mapping());
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Position/Update Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok();
        }


        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        public ActionResult<DefaultResponse<object>> DeletePosition(int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.PositionRepository.Delete(id);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Position/Delete Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok();
        }
    }
}