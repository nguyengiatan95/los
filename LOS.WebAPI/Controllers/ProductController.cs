﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Linq.Dynamic.Core;
using LOS.Common.Utils;
using LOS.DAL.EntityFramework;
using LOS.DAL.Dapper;
using Microsoft.Extensions.Configuration;
using System.Text;
using LOS.Common.Extensions;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class ProductController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        private IConfiguration _configuration;
        public ProductController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            this._configuration = configuration;
        }

        [Route("get_product")]
        [HttpGet]
        public ActionResult<DefaultResponse<ProductDetail>> GetProduct(int productId)
        {
            var product = unitOfWork.ProductRepository.Query(x => x.Id == productId, null, false).Select(ProductDetail.ProjectionDetail).FirstOrDefault();
            var def = new DefaultResponse<ProductDetail>();
            def.meta = new Meta(200, "success");
            def.data = product;
            return Ok(def);
        }

        [Route("current_price")]
        [HttpGet]
        public ActionResult<DefaultResponse<ProductDetail>> GetCurrentPrice(int id)
        {
            var product = unitOfWork.ProductRepository.Query(x => x.Id == id, null, false).Select(ProductDetail.ProjectionDetail).FirstOrDefault();
            if (product != null)
            {
                var countYear = DateTime.Now.Year - product.Year.GetValueOrDefault();
                if (countYear == 0) countYear++;
                else if (countYear >= 11) countYear = 11;

                var productPercentReduction = unitOfWork.ProductPercentReductionRepository.Query(x => x.BrandProductId == product.BrandProductId
                && x.ProductTypeId == product.ProductTypeId && x.CountYear == countYear, null, false).Select(ProductPercentReductionDetail.ProjectionDetail).FirstOrDefault();
                if (productPercentReduction != null && productPercentReduction.PercentReduction > 0)
                    product.PriceCurrent = (product.Price - (product.Price * productPercentReduction.PercentReduction / 100));
            }
            var def = new DefaultResponse<ProductDetail>();
            def.meta = new Meta(200, "success");
            def.data = product;
            return Ok(def);
        }
        #region DuyNV 
        [HttpGet]
        [Route("get_product_by_brand")]
        public ActionResult<DefaultResponse<SummaryMeta, List<ProductDetail>>> GetProductByBrand([FromQuery] int brandId)
        {
            var def = new DefaultResponse<SummaryMeta, List<ProductDetail>>();
            List<ProductDetail> data;
            try
            {
                data = unitOfWork.ProductRepository.Query(x => x.BrandProductId == brandId, null, false).Select(ProductDetail.ProjectionDetail).ToList();

                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, 0, 0, 0);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Product/GetProductByBrand Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }
        [Route("get_brand")]
        [HttpGet]
        public ActionResult<DefaultResponse<BrandProductDetail>> GetBrand(int brandId)
        {
            var def = new DefaultResponse<SummaryMeta, BrandProductDetail>();
            try
            {
                var product = unitOfWork.BrandProductRepository.Query(x => x.Id == brandId, null, false).Select(BrandProductDetail.ProjectionDetail).FirstOrDefault();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, 0, 0, 0);
                def.data = product;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Product/get_brand Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("get_product_review")]
        public ActionResult<DefaultResponse<SummaryMeta, List<ProductReviewDetailAPI>>> GetProductReview([FromQuery] int loanbriefId, [FromQuery] int productId, [FromQuery] int IdType = 0, [FromQuery] decimal price = 0)
        {
            var def = new DefaultResponse<SummaryMeta, List<ProductReviewDetailAPI>>();
            try
            {
                var query = unitOfWork.ProductReviewRepository.Query(x => x.Status > 0, null, false, x => x.ProductReviewResult)
                     .OrderBy(x => x.OrderNo).Include(x => x.ProductReviewResult).Select(x => new ProductReviewDetailAPI()
                     {
                         Id = x.Id,
                         Name = x.Name,
                         ParentId = x.ParentId,
                         Status = x.Status,
                         State = x.State,
                         IsCancel = x.IsCancel,
                         IdType = x.ProductReviewDetail.FirstOrDefault(k => k.ProductTypeId == IdType) != null ? x.ProductReviewDetail.FirstOrDefault(k => k.ProductTypeId == IdType).ProductTypeId : 0,
                         IsCheck = x.ProductReviewResult.FirstOrDefault(k => k.LoanBriefId == loanbriefId && k.ProductId == productId) != null ? x.ProductReviewResult.FirstOrDefault(k => k.LoanBriefId == loanbriefId && k.ProductId == productId).IsCheck : null,
                         MoneyDiscount = (x.ProductReviewDetail.FirstOrDefault(k => k.ProductTypeId == IdType) != null
                                           && x.ProductReviewDetail.FirstOrDefault(k => k.ProductTypeId == IdType).MoneyDiscount > 0)
                           ? x.ProductReviewDetail.FirstOrDefault(k => k.ProductTypeId == IdType).MoneyDiscount
                           : (price > 0)
                                ? (long)(x.ProductReviewDetail.FirstOrDefault(k => k.ProductTypeId == IdType).PecentDiscount.Value * price / 100)
                                : 0
                     });
                def.data = query.ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, 0, 0, 0);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Product/GetProductReview Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }
        [HttpPost]
        [Route("insert_product_review_result")]
        public ActionResult<DefaultResponse<object>> InsertProductReviewResult([FromBody] ProductReviewResultObject productReviewResults)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (productReviewResults == null || productReviewResults.LoanBriefId == 0
                    || productReviewResults.ProductReviewResults == null || productReviewResults.ProductReviewResults.Count == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var loanbrief = unitOfWork.LoanBriefRepository.GetById(productReviewResults.LoanBriefId);
                if (loanbrief == null || loanbrief.LoanBriefId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // 1: insert update ProductReviewResult
                // 2: insert tblProductReviewResultDetail
                // 3:   UPDATE tblLoanCredit SET TotalMoney = @TotalMoney, Totalmoneyexpertise = @Totalmoneyexpertise, TotalMoneyExpertiseLast = @TotalMoney WHERE ID = @LoanCreditId
                //xóa toàn bộ dữ liệu cũ ProductReviewResultRepository
                unitOfWork.ProductReviewResultRepository.Delete(x => x.LoanBriefId == productReviewResults.LoanBriefId);
                // 1:
                foreach (var item in productReviewResults.ProductReviewResults)
                {
                    unitOfWork.ProductReviewResultRepository.Insert(new ProductReviewResult()
                    {
                        LoanBriefId = productReviewResults.LoanBriefId,
                        ProductId = item.ProductId,
                        ProductReviewId = item.ProductReviewId,
                        IsCheck = item.IsCheck,
                        CreateDate = DateTime.Now
                    });
                }
                // 2:  
                if (productReviewResults.ProductReviewResultDetail != null)
                {
                    var reviewResult = productReviewResults.ProductReviewResultDetail;
                    unitOfWork.ProductReviewResultDetailRepository.Insert(new ProductReviewResultDetail()
                    {
                        LoanBriefId = productReviewResults.LoanBriefId,
                        ProductId = reviewResult.ProductId,
                        UserId = reviewResult.UserId,
                        GroupUserId = reviewResult.GroupUserId,
                        CreateDate = DateTime.Now,
                        PriceReview = reviewResult.PriceReview
                    });
                }
                // 3:
                if(loanbrief.TypeRemarketing != (int)EnumTypeRemarketing.DebtRevolvingLoan)
                {
                    var totalMoney = loanbrief.LoanAmount;
                    if (totalMoney > productReviewResults.TotalMoney)
                        totalMoney = productReviewResults.TotalMoney;
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == productReviewResults.LoanBriefId, x => new LoanBrief()
                    {
                        LoanAmountExpertise = productReviewResults.TotalMoneyExpertise,
                        LoanAmountExpertiseLast = productReviewResults.TotalMoney,
                        LoanAmount = totalMoney
                    });
                }
                else
                {
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == productReviewResults.LoanBriefId, x => new LoanBrief()
                    {
                        LoanAmountExpertise = productReviewResults.TotalMoneyExpertise,
                        LoanAmountExpertiseLast = productReviewResults.TotalMoney
                    });
                }
               
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Product/InsertProductReviewResult Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        #endregion

        [HttpGet]
        [Route("get_product_all")]
        public ActionResult<DefaultResponse<SummaryMeta, List<ProductDetail>>> GetProduct([FromQuery] int page = 1, [FromQuery] int pageSize = 25, [FromQuery] string name = null, [FromQuery] string sortBy = "Id", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<ProductDetail>>();
            try
            {
                var query = unitOfWork.ProductRepository.Query(x => (name == null
                || x.Name.ToLower().Contains(name.ToLower())
                || x.FullName.ToLower().Contains(name.ToLower())), null, false).Select(ProductDetail.ProjectionDetail);
                var totalRecords = query.Count();

                List<ProductDetail> data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Product/GetProduct Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpPut]
        [Route("{id}")]
        public ActionResult<DefaultResponse<object>> PutProduct(int id, [FromBody] Product product)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != product.Id)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                unitOfWork.ProductRepository.Update(x => x.Id == id, x => new Product()
                {
                    BrandProductId = product.BrandProductId,
                    ProductTypeId = product.ProductTypeId,
                    Name = product.Name,
                    FullName = product.FullName,
                    Year = product.Year,
                    BrakeType = product.BrakeType,
                    RimType = product.RimType,
                    ShortName = product.ShortName,
                    UpdateTime = product.UpdateTime,
                    UpdateBy = product.UpdateBy
                });
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                unitOfWork.RollbackTransaction();
                Log.Error(ex, "Product/PutProduct Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        public ActionResult<DefaultResponse<object>> PostProduct([FromBody] Product product)
        {
            var def = new DefaultResponse<object>();
            try
            {
                product.IsEnable = true;
                var result = unitOfWork.ProductRepository.Insert(product);
                unitOfWork.Save();
                if (result.Id > 0)
                {
                    def.data = result.Id;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Product/PostProduct Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [Route("check_fullname")]
        [HttpGet]
        public ActionResult<DefaultResponse<ProductDetail>> CheckFullNameProduct(string fullname)
        {
            var def = new DefaultResponse<ProductDetail>();
            try
            {
                var product = unitOfWork.ProductRepository.Query(x => fullname.Contains(x.FullName), null, false).Select(ProductDetail.ProjectionDetail).FirstOrDefault();
                def.meta = new Meta(200, "success");
                def.data = product;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Product/CheckFullNameProduct Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_product_review_v2")]
        public ActionResult<DefaultResponse<SummaryMeta, List<ProductReviewDetailAPI>>> GetProductReviewV2([FromQuery] int loanbriefId, [FromQuery] int productId
           , [FromQuery] int productTypeId = 0, [FromQuery] decimal price = 0)
        {
            var def = new DefaultResponse<SummaryMeta, List<ProductReviewDetailAPI>>();
            try
            {

                var db = new DapperHelper(_configuration.GetConnectionString("LOSDatabase"));
                var sql = new StringBuilder();
                sql.AppendLine("select pr.Id, pr.Name, pr.ParentId, pr.Status, pr.State,pr.IsCancel, prr.IsCheck, prd.MoneyDiscount, prd.PecentDiscount ");
                sql.AppendLine("from ProductReview(nolock) pr ");
                sql.AppendLine($"left join ProductReviewDetail(nolock) prd on prd.ProductReviewId = pr.Id and prd.ProductTypeId = {productTypeId} ");
                sql.AppendLine("left join ");
                sql.AppendLine($"(select * from ProductReviewResult(nolock)  where Id in (select MAX(Id) from ProductReviewResult(nolock) prr where  prr.LoanBriefId ={loanbriefId} group by ProductReviewId))   prr on prr.ProductReviewId = pr.Id ");
                sql.AppendLine("where pr.Status = 1 ");
                var data = db.Query<ProductReviewDetailAPI>(sql.ToString());
                if (data != null)
                {
                    if (price > 0)
                    {
                        foreach (var item in data)
                        {
                            if (item.PecentDiscount > 0 && !item.MoneyDiscount.HasValue)
                            {
                                var moneyDiscount = price * item.PecentDiscount.Value / 100;
                                item.MoneyDiscount = (long)Math.Round(moneyDiscount, 0);
                            }

                        }
                    }
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, 0, 0, 0);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Product/GetProductReviewV2 Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }
    }
}