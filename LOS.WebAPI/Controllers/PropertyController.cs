﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.WebAPI.Controllers
{
	[ApiVersion("1.0")]
	[Route("api/v{version:apiVersion}/[controller]")]
	[ApiController]
    [Authorize]
    public class PropertyController : ControllerBase
    {
		private IUnitOfWork unitOfWork;
		private IConfiguration configuration;

		public PropertyController(IUnitOfWork unitOfWork, IConfiguration configuration)
		{
			this.unitOfWork = unitOfWork;
			this.configuration = configuration;
		}

		[HttpGet]
		//[Authorize]
		public ActionResult<DefaultResponse<SummaryMeta, List<PropertyDetail>>> GetAllProperties([FromQuery] int page = 1, [FromQuery] int pageSize = 999, [FromQuery] int skip = -1, 
			[FromQuery] int take = 999,[FromQuery] int status = -1, [FromQuery] string sortBy = "Priority", [FromQuery] string sortOrder = "ASC")
		{
			var def = new DefaultResponse<SummaryMeta, List<PropertyDetail>>();
			try
			{
				var query = unitOfWork.PropertyRepository.Query(x => (x.Status == status || status == -1), null, page, pageSize,
					false).Select(PropertyDetail.ProjectionDetail);
				var totalRecords = query.Count();
				List<PropertyDetail> data;
				if (skip >= 0)
					data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
				else
					data = query.OrderBy(sortBy + " " + sortOrder).Skip(0).Take(take).ToList();
				def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
				def.data = data;
				return Ok(def);
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Property/Get Exception");
				def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
			}
			return Ok(def);
		}

		[HttpGet]
		//[Authorize]
		[Route("{id}")]
		public ActionResult<DefaultResponse<PropertyDetail>> GetById(int id)
		{
			var def = new DefaultResponse<Meta, PropertyDetail>();
			try
			{
				var data = unitOfWork.PropertyRepository.Query(x => x.PropertyId == id, null, false).Select(PropertyDetail.ProjectionDetail).FirstOrDefault();
				def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
				def.data = data;
				return Ok(def);
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Property/Get Exception");
				def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
			}
			return Ok(def);
		}
	}
}