﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class ReasonController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        public ReasonController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("list_reason_group")]
        public ActionResult<DefaultResponse<Meta, List<ReasonCancelDetail>>> GetReasonGroup(int reasonType)
        {
            var def = new DefaultResponse<Meta, List<ReasonCancelDetail>>();
            try
            {
                var data = unitOfWork.ReasonCancelRepository.Query(x => (x.Type == reasonType && x.IsEnable == 1 && x.ParentId.GetValueOrDefault() == 0) 
                || x.Id == (int)EnumReasonCancel.OverDay, null, false).Select(ReasonCancelDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Reason/list_reason_group Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("list_reason_detail")]
        public ActionResult<DefaultResponse<Meta, List<ReasonCancelDetail>>> GetReasonDetail(int parentId)
        {
            var def = new DefaultResponse<Meta, List<ReasonCancelDetail>>();
            try
            {
                var data = unitOfWork.ReasonCancelRepository.Query(x => x.IsEnable == 1 && x.ParentId.GetValueOrDefault(0) == parentId, null, false).Select(ReasonCancelDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Reason/list_reason_detail Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("get_day_in_black_list")]
        public async Task<ActionResult<DefaultResponse<Meta, ReasonCancelDetail>>> GetDayInBlackList(int id)
        {
            var def = new DefaultResponse<Meta, ReasonCancelDetail>();
            try
            {
                var data = await unitOfWork.ReasonCancelRepository.Query(x => x.Id == id, null, false).Select(ReasonCancelDetail.ProjectionDetail).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Reason/GetDayInBlackList Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

    }
}