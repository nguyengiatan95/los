﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class RemarketingController : BaseController
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _baseConfig;
        public RemarketingController(IUnitOfWork unitOfWork, IConfiguration baseConfig)
        {
            this._unitOfWork = unitOfWork;
            this._baseConfig = baseConfig;
        }

        [HttpGet]
        [Route("get_data")]
        public ActionResult<DefaultResponse<SummaryMeta, List<ReLoanbriefDetail>>> GetData([FromQuery] int reasonCancel = -1,
            [FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1, [FromQuery] int groupId = -1,
            [FromQuery] int take = 20, [FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null,
            [FromQuery] DateTime? fromCancelDate = null, [FromQuery] DateTime? toCancelDate = null, [FromQuery] int detailReason = -1,
            [FromQuery] int status = -1, [FromQuery] int source = -1, [FromQuery] string textSearch = null, [FromQuery] string utmSource = null,
            [FromQuery] int provinceId = -1, [FromQuery] int shopId = -1)
        {
            var def = new DefaultResponse<SummaryMeta, List<ReLoanbriefDetail>>();
            try
            {
                var lstData = new List<ReLoanbriefDetail>();
                var dayRecreate = 30;
                if (status == (int)EnumLoanStatus.FINISH)
                    dayRecreate = 15;
                var loanbriefId = 0;
                var phone = "";
                var utm_source = "";
                var createBy = -1;
                if (!string.IsNullOrEmpty(textSearch))
                {
                    if (textSearch.Length == 10 && textSearch.ValidPhone())
                    {
                        phone = textSearch;
                    }
                    else if (ConvertExtensions.IsNumber(textSearch))
                    {
                        loanbriefId = Convert.ToInt32(textSearch);
                    }
                }
                if (source > 0)
                {
                    if (source == (int)FilterSourceTLS.form_cancel_rmkt || source == (int)FilterSourceTLS.rmkt_loan_finish)
                        utm_source = LOS.Common.Helpers.ExtentionHelper.GetName((FilterSourceTLS)source);
                    else if (source == (int)FilterSourceTLS.SourceFormNewMKT)
                        createBy = 0;
                }
                //danh sách các đơn vay đã tạo trong vòng 1 tháng vừa qua

                //lấy danh sách các lstDistrictId của Shop hỗ trợ
                var lstDistrictId = new List<int>();
                if (shopId > 0)
                {
                    //list quận/huyện
                    lstDistrictId = _unitOfWork.ManagerAreaHubRepository.Query(x => x.ShopId == shopId, null, false)
                   .Select(x => x.DistrictId).Distinct().ToList();
                }
                var listLoanbriefCreated = _unitOfWork.LogReLoanbriefRepository.Query(x => x.LoanbriefId > 0
                    && x.CreatedAt.Value.AddDays(dayRecreate) >= DateTime.Now && x.TypeReLoanbrief != (int)TypeReLoanbrief.LoanBriefTopup.GetHashCode(), null, false).Select(x => x.LoanbriefId.Value);
                if (status == (int)EnumLoanStatus.CANCELED)
                {
                    if (reasonCancel == (int)EnumReasonCancel.OverDay)
                    {
                        lstData = _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.CANCELED
                           && (x.CreateBy.GetValueOrDefault(0) == 0)
                           && (x.ReMarketingLoanBriefId.GetValueOrDefault(0) == 0)
                           && x.LoanBriefCancelAt.HasValue
                           && x.ReasonCancel == (int)EnumReasonCancel.OverDay
                           && x.BoundTelesaleId == (int)EnumUser.recare
                           && !listLoanbriefCreated.Contains(x.LoanBriefId)
                           && x.CreatedTime.HasValue
                           && (fromDate == null || (fromDate != null && x.CreatedTime.Value >= fromDate.Value))
                           && (toDate == null || (toDate != null && x.CreatedTime.Value < toDate.Value))
                           && (fromCancelDate == null || (fromCancelDate != null && x.LoanBriefCancelAt >= fromCancelDate.Value))
                           && (toCancelDate == null || (toCancelDate != null && x.LoanBriefCancelAt < toCancelDate.Value))
                           && (x.Phone == phone || phone == "")
                           && (x.LoanBriefId == loanbriefId || loanbriefId == 0)
                           && (x.UtmSource == utm_source || utm_source == "")
                           && (x.UtmSource == utmSource || utmSource == null)
                           && (x.ProvinceId == provinceId || provinceId == -1)
                           && (lstDistrictId.Contains(x.DistrictId.Value) || lstDistrictId.Count == 0)
                       , null, false).Select(ReLoanbriefDetail.ProjectionDetail).ToList();
                    }
                    else
                    {
                        var listParent = new List<int>();
                        //lấy d/s lý do hủy
                        //Loại bỏ lý do hủy do k có xe máy và không thuộc hỗ trợ
                        //lấy danh sách đơn hủy không phải của hub
                        if (reasonCancel != -2)
                            listParent = _unitOfWork.ReasonCancelRepository.Query(x => x.IsEnable == 1 && x.Id != 660 && x.Id != 661 && x.Type == EnumTypeReason.TeleSalesCancel.GetHashCode(), null, false).Select(x => x.Id).ToList();
                        else if (reasonCancel == (int)EnumReasonCancel.OverDay)
                            detailReason = (int)EnumReasonCancel.OverDay;
                        else
                            listParent.Add(reasonCancel);

                        var listReasonCancelId = new List<int>();
                        if (detailReason == -1)
                        {
                            //lấy danh sách đơn hủy của cả hub và tls
                            if (reasonCancel == -1 && groupId == (int)EnumGroupUser.ManagerHub)
                            {
                                //danh sách tls
                                listReasonCancelId = _unitOfWork.ReasonCancelRepository.Query(x => x.IsEnable == 1
                            && x.Id != 660 && x.Id != 661 && x.ParentId > 0
                            && listParent.Contains(x.ParentId.Value), null, false).Select(x => x.Id).ToList();

                                //danh sách hub
                                listReasonCancelId.AddRange(_unitOfWork.ReasonCancelRepository.Query(x => x.IsEnable == 1 && x.Type == EnumTypeReason.HubCancel.GetHashCode(), null, false).Select(x => x.Id).ToList());
                            }


                            //lấy danh sách đơn hủy của tls
                            else if (reasonCancel == -1 && groupId != (int)EnumGroupUser.ManagerHub)
                                listReasonCancelId = _unitOfWork.ReasonCancelRepository.Query(x => x.IsEnable == 1
                           && x.Id != 660 && x.Id != 661 && x.ParentId > 0
                           && listParent.Contains(x.ParentId.Value), null, false).Select(x => x.Id).ToList();

                            //lấy danh sách đơn hủy của của hub
                            else if (reasonCancel == -2 && groupId == (int)EnumGroupUser.ManagerHub)
                                listReasonCancelId = _unitOfWork.ReasonCancelRepository.Query(x => x.IsEnable == 1 && x.Type == EnumTypeReason.HubCancel.GetHashCode(), null, false).Select(x => x.Id).ToList();
                        }

                        else
                            listReasonCancelId.Add(detailReason);
                        //query dữ liêu đơn cần khởi tạo lại
                        lstData = _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.CANCELED
                           && (x.CreateBy.GetValueOrDefault(0) == 0)
                           && (x.ReMarketingLoanBriefId.GetValueOrDefault(0) == 0)
                           && x.LoanBriefCancelAt.HasValue
                           && x.ReasonCancel > 0 && listReasonCancelId.Contains(x.ReasonCancel.Value)
                           && !listLoanbriefCreated.Contains(x.LoanBriefId)
                           && x.CreatedTime.HasValue
                           && (fromDate == null || (fromDate != null && x.CreatedTime.Value >= fromDate.Value))
                           && (toDate == null || (toDate != null && x.CreatedTime.Value < toDate.Value))
                           && (fromCancelDate == null || (fromCancelDate != null && x.LoanBriefCancelAt >= fromCancelDate.Value))
                           && (toCancelDate == null || (toCancelDate != null && x.LoanBriefCancelAt < toCancelDate.Value))
                           && (x.Phone == phone || phone == "")
                           && (x.LoanBriefId == loanbriefId || loanbriefId == 0)
                           && (x.UtmSource == utm_source || utm_source == "")
                           && (x.UtmSource == utmSource || utmSource == null)
                           && (x.ProvinceId == provinceId || provinceId == -1)
                           && (lstDistrictId.Contains(x.DistrictId.Value) || lstDistrictId.Count == 0)
                       , null, false).Select(ReLoanbriefDetail.ProjectionDetail).ToList();
                    }
                }
                else if (status == (int)EnumLoanStatus.FINISH && fromDate != null && toDate != null)
                {
                    //query dữ liêu đơn cần khởi tạo lại
                    lstData = _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.FINISH
                       && (x.BoundTelesaleId.GetValueOrDefault(0) > 0)
                       && (x.ReMarketingLoanBriefId.GetValueOrDefault(0) == 0)
                       && !listLoanbriefCreated.Contains(x.LoanBriefId)
                       && (x.ProductId == (int)EnumProductCredit.MotorCreditType_CC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                       && x.FinishAt >= fromDate.Value
                       && x.FinishAt < toDate.Value
                       && (x.Phone == phone || phone == "")
                       && (x.LoanBriefId == loanbriefId || loanbriefId == 0)
                       && (x.UtmSource == utm_source || utm_source == "")
                       //&& (x.CreateBy.GetValueOrDefault(0) == createBy || createBy == -1)
                       && (x.UtmSource == utmSource || utmSource == null)
                       , null, false).Select(ReLoanbriefDetail.ProjectionDetail).ToList();
                }
                var totalRecords = lstData.Count();
                List<ReLoanbriefDetail> data;
                if (skip == -1)
                    data = lstData.OrderByDescending(x => x.LoanBriefId).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                {
                    data = lstData.Skip(skip).Take(take).ToList();
                    pageSize = take;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Remarketing/GetData Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("remarketing_loanbrief")]
        public async Task<ActionResult<DefaultResponse<object>>> RemarketingLoanbrief(RemarketingLoanModel entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (entity.ListLoanbrief != null && entity.ListLoanbrief.Count > 0 && entity.TypeRemarketing > 0)
                {
                    var checkLoanBriefReason = new List<int>();
                    //kiểm tra xem phải đơn hủy mà telesales khởi tạo lại
                    if (entity.TypeReLoanbrief == TypeReLoanbrief.CancelLoanbrief.GetHashCode() && entity.TelesalesId.HasValue)
                    {
                        //kiểm tra xem lý do hủy đơn có thuộc những lý do này không
                        //1.1.Không liên hệ được
                        //1.3.Không thuộc khu vực hỗ trợ
                        //1.4.Không có xe máy
                        //8.Hủy do hết thời gian quy định
                        //nếu thuộc những lý do trên thì chuyển vào giỏ recare
                        checkLoanBriefReason = await _unitOfWork.LoanBriefRepository.Query(x => entity.ListLoanbrief.Contains(x.LoanBriefId)
                        && (x.ReasonCancel == (int)EnumReasonCancel.NoAreaSupport || x.ReasonCancel == (int)EnumReasonCancel.OverDay
                        || x.ReasonCancel == (int)EnumReasonCancel.NoContact || x.ReasonCancel == (int)EnumReasonCancel.NoMotobike)
                        , null, false).Select(x => x.LoanBriefId).ToListAsync();

                    }
                    foreach (var item in entity.ListLoanbrief)
                    {
                        if (checkLoanBriefReason != null && checkLoanBriefReason.Count > 0)
                        {
                            if (checkLoanBriefReason.Any(x => x == item))
                                entity.TelesalesId = (int)EnumUser.recare;
                        }

                        var LoanbriefRemarketingItem = new LogReLoanbrief()
                        {
                            LoanbriefId = item,
                            CreatedAt = DateTime.Now,
                            CreatedBy = entity.UserId,
                            TypeRemarketing = entity.TypeRemarketing,
                            UtmSource = entity.UtmSource,
                            IsExcuted = (int)LogReLoanbriefExcuted.NoExcuted,
                            TypeReLoanbrief = entity.TypeReLoanbrief,
                            TelesaleId = entity.TelesalesId,
                            HubId = entity.HubId,
                        };
                        _unitOfWork.LogReLoanbriefRepository.Insert(LoanbriefRemarketingItem);
                    }
                    _unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Remarketing/RemarketingLoanbrief POST Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("get_data_all")]
        public ActionResult<DefaultResponse<Meta, List<int>>> GetDataAll([FromQuery] int status = -1, [FromQuery] int groupId = -1,
          [FromQuery] int reasonCancel = -1, [FromQuery] int detailReason = -1, [FromQuery] DateTime? fromCreateDate = null,
          [FromQuery] DateTime? toCreateDate = null, [FromQuery] DateTime? fromCancelTime = null, [FromQuery] DateTime? toCancelTime = null,
          [FromQuery] string textSearch = null, [FromQuery] int source = -1, [FromQuery] string utmSource = null, [FromQuery] int provinceId = -1)
        {
            var def = new DefaultResponse<Meta, List<int>>();
            try
            {
                var lstData = new List<int>();
                var dayRecreate = 30;
                if (status == (int)EnumLoanStatus.FINISH)
                    dayRecreate = 15;
                var loanbriefId = 0;
                var phone = "";
                var utm_source = "";
                var createBy = -1;
                if (!string.IsNullOrEmpty(textSearch))
                {
                    if (textSearch.Length == 10 && textSearch.ValidPhone())
                    {
                        phone = textSearch;
                    }
                    else if (ConvertExtensions.IsNumber(textSearch))
                    {
                        loanbriefId = Convert.ToInt32(textSearch);
                    }
                }
                if (source > 0)
                {
                    if (source == (int)FilterSourceTLS.form_cancel_rmkt || source == (int)FilterSourceTLS.rmkt_loan_finish)
                        utm_source = LOS.Common.Helpers.ExtentionHelper.GetName((FilterSourceTLS)source);
                    else if (source == (int)FilterSourceTLS.SourceFormNewMKT)
                        createBy = 0;
                }
                //danh sách các đơn vay đã tạo trong vòng 1 tháng vừa qua
                var listLoanbriefCreated = _unitOfWork.LogReLoanbriefRepository.Query(x => x.LoanbriefId > 0
                    && x.CreatedAt.Value.AddDays(dayRecreate) >= DateTime.Now, null, false).Select(x => x.LoanbriefId.Value);
                if (status == (int)EnumLoanStatus.CANCELED)
                {
                    if (reasonCancel == (int)EnumReasonCancel.OverDay)
                    {
                        lstData = _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.CANCELED
                           && (x.CreateBy.GetValueOrDefault(0) == 0)
                           && (x.ReMarketingLoanBriefId.GetValueOrDefault(0) == 0)
                           && x.LoanBriefCancelAt.HasValue
                           && x.ReasonCancel == (int)EnumReasonCancel.OverDay
                           && x.BoundTelesaleId == (int)EnumUser.recare
                           && !listLoanbriefCreated.Contains(x.LoanBriefId)
                           && x.CreatedTime.HasValue
                           && (fromCreateDate == null || (fromCreateDate != null && x.CreatedTime.Value >= fromCreateDate.Value))
                           && (toCreateDate == null || (toCreateDate != null && x.CreatedTime.Value < toCreateDate.Value))
                           && (fromCancelTime == null || (fromCancelTime != null && x.LoanBriefCancelAt >= fromCancelTime.Value))
                           && (toCancelTime == null || (toCancelTime != null && x.LoanBriefCancelAt < toCancelTime.Value))
                           && (x.Phone == phone || phone == "")
                           && (x.LoanBriefId == loanbriefId || loanbriefId == 0)
                           && (x.UtmSource == utm_source || utm_source == "")
                           && (x.UtmSource == utmSource || utmSource == null)
                           && (x.ProvinceId == provinceId || provinceId == -1)
                       , null, false).Select(x => x.LoanBriefId).ToList();
                    }
                    else
                    {
                        var listParent = new List<int>();
                        //lấy d/s lý do hủy
                        //Loại bỏ lý do hủy do k có xe máy và không thuộc hỗ trợ
                        //lấy danh sách đơn hủy không phải của hub
                        if (reasonCancel != -2)
                            listParent = _unitOfWork.ReasonCancelRepository.Query(x => x.IsEnable == 1 && x.Id != 660 && x.Id != 661 && x.Type == EnumTypeReason.TeleSalesCancel.GetHashCode(), null, false).Select(x => x.Id).ToList();
                        else if (reasonCancel == (int)EnumReasonCancel.OverDay)
                            detailReason = (int)EnumReasonCancel.OverDay;
                        else
                            listParent.Add(reasonCancel);

                        var listReasonCancelId = new List<int>();
                        if (detailReason == -1)
                        {
                            //lấy danh sách đơn hủy của cả hub và tls
                            if (reasonCancel == -1 && groupId == (int)EnumGroupUser.ManagerHub)
                            {
                                //danh sách tls
                                listReasonCancelId = _unitOfWork.ReasonCancelRepository.Query(x => x.IsEnable == 1
                            && x.Id != 660 && x.Id != 661 && x.ParentId > 0
                            && listParent.Contains(x.ParentId.Value), null, false).Select(x => x.Id).ToList();
                                //danh sách hub
                                listReasonCancelId.AddRange(_unitOfWork.ReasonCancelRepository.Query(x => x.IsEnable == 1 && x.Type == EnumTypeReason.HubCancel.GetHashCode(), null, false).Select(x => x.Id).ToList());
                            }

                            //lấy danh sách đơn hủy của tls
                            else if (reasonCancel == -1 && groupId != (int)EnumGroupUser.ManagerHub)
                                listReasonCancelId = _unitOfWork.ReasonCancelRepository.Query(x => x.IsEnable == 1
                           && x.Id != 660 && x.Id != 661 && x.ParentId > 0
                           && listParent.Contains(x.ParentId.Value), null, false).Select(x => x.Id).ToList();

                            //lấy danh sách đơn hủy của của hub
                            else if (reasonCancel == -2 && groupId == (int)EnumGroupUser.ManagerHub)
                                listReasonCancelId = _unitOfWork.ReasonCancelRepository.Query(x => x.IsEnable == 1 && x.Type == EnumTypeReason.HubCancel.GetHashCode(), null, false).Select(x => x.Id).ToList();
                        }

                        else
                            listReasonCancelId.Add(detailReason);
                        //query dữ liêu đơn cần khởi tạo lại
                        lstData = _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.CANCELED
                           && (x.CreateBy.GetValueOrDefault(0) == 0)
                           && (x.ReMarketingLoanBriefId.GetValueOrDefault(0) == 0)
                           && x.LoanBriefCancelAt.HasValue
                           && x.ReasonCancel > 0 && listReasonCancelId.Contains(x.ReasonCancel.Value)
                           && !listLoanbriefCreated.Contains(x.LoanBriefId)
                           && x.CreatedTime.HasValue
                           && (fromCreateDate == null || (fromCreateDate != null && x.CreatedTime.Value >= fromCreateDate.Value))
                           && (toCreateDate == null || (toCreateDate != null && x.CreatedTime.Value < toCreateDate.Value))
                           && (fromCancelTime == null || (fromCancelTime != null && x.LoanBriefCancelAt >= fromCancelTime.Value))
                           && (toCancelTime == null || (toCancelTime != null && x.LoanBriefCancelAt < toCancelTime.Value))
                           && (x.Phone == phone || phone == "")
                           && (x.LoanBriefId == loanbriefId || loanbriefId == 0)
                           && (x.UtmSource == utm_source || utm_source == "")
                           && (x.UtmSource == utmSource || utmSource == null)
                           && (x.ProvinceId == provinceId || provinceId == -1)
                       , null, false).Select(x => x.LoanBriefId).ToList();
                    }
                }
                else if (status == (int)EnumLoanStatus.FINISH && fromCreateDate != null && toCreateDate != null)
                {
                    //query dữ liêu đơn cần khởi tạo lại
                    lstData = _unitOfWork.LoanBriefRepository.Query(x => x.Status == (int)EnumLoanStatus.FINISH
                       && (x.BoundTelesaleId.GetValueOrDefault(0) > 0)
                       && (x.ReMarketingLoanBriefId.GetValueOrDefault(0) == 0)
                       && !listLoanbriefCreated.Contains(x.LoanBriefId)
                       && (x.ProductId == (int)EnumProductCredit.MotorCreditType_CC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || x.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                       && x.FinishAt >= fromCreateDate.Value
                       && x.FinishAt < toCreateDate.Value
                       && (x.Phone == phone || phone == "")
                       && (x.LoanBriefId == loanbriefId || loanbriefId == 0)
                       && (x.UtmSource == utm_source || utm_source == "")
                       && (x.CreateBy.GetValueOrDefault(0) == createBy || createBy == -1)
                       && (x.UtmSource == utmSource || utmSource == null)
                       , null, false).Select(x => x.LoanBriefId).ToList();
                }

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Remarketing/GetDataAll Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
