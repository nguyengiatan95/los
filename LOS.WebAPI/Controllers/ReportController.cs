﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
using LOS.Common.Utils;
using static LOS.DAL.Object.Report.Report;
using LOS.DAL.Dapper;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        private IConfiguration configuration;
        public ReportController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            this.configuration = configuration;
        }
        [HttpGet]
        //[Authorize]
        [Route("ReporLoanRegisterOfUserMmo")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>> ReporLoanRegisterOfUserMmo([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1, [FromQuery] int take = 10, [FromQuery] int affstatus = -1, [FromQuery] string name = null, int Affcode = -1, [FromQuery] string sortBy = "CreatedTime", [FromQuery] string sortOrder = "DESC", [FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefDetail>>();
            try
            {
                var query = unitOfWork.LoanBriefRepository.Query(x => (x.FullName.Contains(name) || name == null)
                && (x.CreatedTime >= fromDate && x.CreatedTime <= toDate)
                , null, false).Select(LoanBriefDetail.ProjectionDetail);
                if (Affcode != -1)
                {
                    query = query.Where(x => x.AffCode == Affcode);
                }
                if (affstatus != -1)
                {
                    query = query.Where(x => x.AffStatus == affstatus);
                }

                var totalRecords = query.Count();
                List<LoanBriefDetail> data;
                if (skip >= 0)
                    data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                    data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Report/ReporLoanRegisterOfUserMmo Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("ReporLoanSuccessFullySoldMMO")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefSuccessFullySoldDetail>>> ReporLoanSuccessFullySoldMMO([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1, [FromQuery] int take = 10, [FromQuery] int affstatus = -1, [FromQuery] string name = null, int Affcode = -1, [FromQuery] string sortBy = "CreatedDate", [FromQuery] string sortOrder = "DESC", [FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefSuccessFullySoldDetail>>();
            //try
            //{
            //    var query = unitOfWork.TransactionsRepository.Query(x => (string.IsNullOrEmpty(name) || x.LoanBrief.FullName.Contains(name) || x.LoanBriefId.ToString().Contains(name))
            //    && (Affcode == -1 )
            //     && (affstatus == -1 || x.LoanBrief.AffStatus == affstatus)
            //    && (x.CreatedDate >= fromDate && x.CreatedDate <= toDate)
            //    , null, false).Select(LoanBriefSuccessFullySoldDetail.ProjectionDetail);
            //    var totalRecords = query.Count();
            //    List<LoanBriefSuccessFullySoldDetail> data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            //    def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
            //    def.data = data;
            //    return Ok(def);
            //}
            //catch (Exception ex)
            //{
            //    Log.Error(ex, "Report/ReporLoanSuccessFullySoldMMO Exception");
            //    def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            //}
            return Ok(def);
        }

        [HttpGet]
        [Route("get_report_approve_employee")]
        public ActionResult<DefaultResponse<Meta, List<ReportApproveEmployee>>> ReportApproveEmployee([FromQuery] int month)
        {
            var def = new DefaultResponse<Meta, List<ReportApproveEmployee>>();
            try
            {
                var now = DateTime.Now;
                var date = new DateTime(now.Year,month,now.Day);
                var db = new DapperHelper(configuration.GetConnectionString("LOSDatabase"));
                var sql = new System.Text.StringBuilder();
                sql.AppendLine("select u.UserId,u.FullName, SUM(CASE WHEN (l.Status=100 or l.Status=110) THEN 1 END) as Disbursement,SUM(CASE WHEN l.Status not in (100, 110) THEN 1 END) as 'Other', COUNT(l.LoanBriefId) as TotalPush ");
                sql.AppendLine("from LoanBrief(nolock) l ");
                sql.AppendLine("left join [User](nolock) u on l.CoordinatorUserId = u.UserId ");
                sql.AppendLine($"where (l.Status in (100, 110) and  DATEDIFF(MONTH, DisbursementAt,'{date}') = 0) or (DATEDIFF(MONTH, CoordinatorPushAt,'{date}') = 0 and l.CurrentPipelineId != 31 and l.Status not in (100, 110)) ");
                sql.AppendLine("group by u.UserId, u.FullName ");
                var result = db.Query<ReportApproveEmployee>(sql.ToString());
                def.data = result;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Report/ReportApproveEmployee Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_report_esign_customer")]
        public ActionResult<DefaultResponse<Meta, List<ReportEsignCustomer>>> ReportEsignCustomer([FromQuery] DateTime? fromDate = null, [FromQuery] DateTime? toDate = null)
        {
            var def = new DefaultResponse<Meta, List<ReportEsignCustomer>>();
            try
            {
                
                //var now = DateTime.Now;
                
                //var start = new DateTime(now.Year, monthStart, 1);
                //var lastDayOfMonth = DateTime.DaysInMonth(now.Year, monthEnd);
                //var end = new DateTime(now.Year, monthEnd, lastDayOfMonth);
                //if (monthStart > monthEnd)
                //    end = new DateTime(now.Year + 1, monthEnd, lastDayOfMonth);
                var db = new DapperHelper(configuration.GetConnectionString("LOSDatabase"));
                var sql = new System.Text.StringBuilder();
                sql.AppendLine("select e.AgreementId,e.CreatedAt,e.LoanBriefId,l.FullName,l.Phone ");
                sql.AppendLine("from EsignContract(nolock) e");
                sql.AppendLine("left join [LoanBrief](nolock) l on e.LoanBriefId = l.LoanBriefId ");
                sql.AppendLine($"where TypeEsign = 1 and CreatedAt >= '{fromDate}' and CreatedAt <= '{toDate}' ");
                sql.AppendLine("group by e.CreatedAt,e.LoanBriefId,e.AgreementId,l.FullName,l.Phone ");
                sql.AppendLine("order by e.LoanBriefId ");
                var result = db.Query<ReportEsignCustomer>(sql.ToString());
                if(result != null && result.Count > 0)
                {
                    foreach (var item in result)
                    {
                        item.Phone = item.Phone.Remove(4, 4).Insert(4, "****");
                    }
                }    
                def.data = result;
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);

                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Report/ReportApproveEmployee Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}