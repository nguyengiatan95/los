﻿using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ReportUtmGroupController : Controller
    {
        private IUnitOfWork unitOfWork;
        private IConfiguration configuration;

        public ReportUtmGroupController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            this.configuration = configuration;
        }

        [HttpGet]
        //[Authorize]
        [Route("report")]
        public ActionResult<DefaultResponse<Meta, List<ReportUtmGroupDTO>>> GetReports()
        {
            var def = new DefaultResponse<Meta, List<ReportUtmGroupDTO>>();

            try
            {
                //var query = unitOfWork.LoanBriefRepository.Query().GroupBy(x => x.UtmSource).Select(x => new ReportUtmGroupDTO
                //{
                //    UtmSource = x.First().UtmSource,
                //    Status = x.First().Status.Value,
                //    CountLoanBrief = x.Count(),
                //    CountStatusDisbured = x.Count(m => m.Status == 50),
                //    RateDisbured = 100 * x.Count(m => m.Status == 50) / x.Count(),
                //});

                var query1 = unitOfWork.LoanBriefRepository.All()
                    .Join(
                            unitOfWork.UtmSourceRepository.All(),
                            m => m.UtmSource,
                            y => y.UtmSourceName,
                            (m, y) => new { m, y })
                     .Join(
                            unitOfWork.GroupSourceRepository.All(),
                            utm => utm.m.UtmSource,
                            gr => gr.GroupSourceName,
                            (utm,gr) => new {utm, gr })
                    .GroupBy(x=> x.utm.m.UtmSource)
                    .Select(x => new ReportUtmGroupDTO
                    {
                        UtmSource = x.First().utm.m.UtmSource,
                        Status = x.First().utm.m.Status.Value,
                        GroupName = x.First().gr.GroupSourceName,
                        CountLoanBrief = x.Count()
                        //CountStatusDisbured = x.Count(u =>u.m.Status ==50),

                    });

                var query2 = unitOfWork.LoanBriefRepository.All()
                    .GroupBy(x => x.UtmSource)
                    .Select(x => new ReportUtmGroupDTO
                    {
                        UtmSource = x.First().UtmSource,
                        Status = x.First().Status.Value,
                        //GroupName = x.First().y.GroupSourceName,
                        CountLoanBrief = x.Count(),
                        CountStatusDisbured = x.Count(u => u.Status == 50),

                    });

                var totalRecords = query2.Count();

                List<ReportUtmGroupDTO> list;
                list = query2.ToList();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = list;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Jobs/GetAllJobs Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}