﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Utils;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using LOS.Common.Models.Request;
using LOS.Common.Extensions;
using Newtonsoft.Json;
using System.Security.Claims;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ResultEkycController : BaseController
    {
        private IUnitOfWork unitOfWork;
        public ResultEkycController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("get_ekyc_by_loanbrief")]
        public ActionResult<DefaultResponse<Meta, ResultEkyc>> GetEkycByLoanBrief(int LoanBriefId)
        {
            var def = new DefaultResponse<Meta, ResultEkyc>();
            try
            {
                var data = unitOfWork.ResultEkycRepository.Query(x => x.LoanbriefId == LoanBriefId, null, false).ToList();
                var lst = new ResultEkyc();
                if(data != null && data.Count > 0)
                {
                    lst = data.OrderByDescending(x => x.Id).FirstOrDefault();
                }    
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanBrief Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<DefaultResponse<Meta, ResultEkyc>> GetById(int id)
        {
            var def = new DefaultResponse<Meta, ResultEkyc>();
            try
            {
                var data = unitOfWork.ResultEkycRepository.Query(x => x.Id == id, null, false).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ResultEkyc/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add")]
        public ActionResult<DefaultResponse<object>> Add([FromBody] ResultEkyc entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.ResultEkycRepository.Insert(entity);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ResultEkyc/Add Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}