﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Security.Claims;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using ScriptDetail = LOS.DAL.DTOs.ScriptDetail;

namespace LOS.WebAPI.Controllers
{
	[ApiVersion("1.0")]
	[Route("api/v{version:apiVersion}/[controller]")]
	[ApiController]
    [Authorize]
    public class ScriptController : ControllerBase
	{
		private IUnitOfWork unitOfWork;
		private IConfiguration configuration;

		public ScriptController(IUnitOfWork unitOfWork, IConfiguration configuration)
		{
			this.unitOfWork = unitOfWork;
			this.configuration = configuration;
		}

		[HttpGet]
		[Authorize]
		public ActionResult<DefaultResponse<SummaryMeta, List<ScriptDetail>>> GetAllScripts([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
			[FromQuery] int take = 10, [FromQuery] int status = -1, [FromQuery] string sortBy = "Priority", [FromQuery] string sortOrder = "DESC")
		{
			var def = new DefaultResponse<SummaryMeta, List<ScriptDetail>>();
			try
			{
				var query = unitOfWork.ScriptRepository.Query(x => (x.Status == status || status == -1) && x.IsDeleted == false, null, page, pageSize,
					false).Select(ScriptDetail.ProjectionDetail);
				var totalRecords = query.Count();
				List<ScriptDetail> data;
				if (skip >= 0)
					data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
				else
					data = query.OrderBy(sortBy + " " + sortOrder).Skip(skip).Take(take).ToList();
				def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
				def.data = data;
				return Ok(def);
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Script/Get Exception");
				def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
			}
			return Ok(def);
		}

		[HttpGet]
		[Authorize]
		[Route("{id}")]
		public ActionResult<DefaultResponse<Meta, ScriptDetail>> GetScriptById(int id)
		{
			var def = new DefaultResponse<Meta, ScriptDetail>();
			try
			{
				var script = unitOfWork.ScriptRepository.Query(x => x.ScriptId == id, null, false).Include(x => x.ScriptDetail)
					.ThenInclude(x => x.Property).Select(ScriptDetail.ProjectionDetail).FirstOrDefault();
				if (script != null)
				{
					def.data = script;
					def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
				}
				else
				{
					def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
				}
				return Ok(def);
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Script/Get Exception");
				def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
			}
			return Ok(def);
		}

		[HttpPost]
		[Authorize]
		[Route("{id}")]
		public ActionResult<DefaultResponse<Meta, ScriptDetail>> PostScript([FromBody] ScriptDetail script)
		{
			var def = new DefaultResponse<Meta, ScriptDetail>();
			try
			{
				var identity = (ClaimsIdentity)User.Identity;
				int UserId = int.Parse(identity.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault());				
				// valid date input
				if (script == null || String.IsNullOrEmpty(script.Name))
				{
					// add script
					var s = new Script();
					s.CreatedTime = DateTimeOffset.Now;					
					s.Name = script.Name;
					s.Status = 0; // Inactive
					s.Type = script.Type;
					s = unitOfWork.ScriptRepository.Insert(s);
					unitOfWork.Save();
					if (s != null)
					{
						// create property for each script detail
						foreach (var detail in script.Details)
						{
							if (detail.PropertyId != null)
							{
								var sd = new LOS.DAL.EntityFramework.ScriptDetail();							
								sd.Priority = detail.Priority;
								sd.PropertyId = detail.PropertyId;
								sd.Question = detail.Question;
								sd.ScriptId = s.ScriptId;
								sd.ValidRequired = detail.ValidRequired;
							}
						}
					}
					{
						def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
						return Ok(def);
					}
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Script/Get Exception");
				def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
			}
			return Ok(def);
		}

		[HttpPut]
		[Authorize]
		[Route("{id}")]
		public ActionResult<DefaultResponse<object>> PutScript(int id, [FromBody] ScriptDetail script)
		{
			var def = new DefaultResponse<object>();
			try
			{
				var identity = (ClaimsIdentity)User.Identity;
				int UserId = int.Parse(identity.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault());
				// valid date input
				var s = unitOfWork.ScriptRepository.GetById(id);		
				if (s != null)
				{
					// update script
					s.Name = script.Name;
					s.UpdatedTime = DateTimeOffset.Now;
					unitOfWork.ScriptRepository.Update(s);
					unitOfWork.Save();
					// foreach detail in detail
					foreach (var detail in script.Details)
					{
						if (detail.Action == "ADD")
						{
							//check exists
							if (!unitOfWork.ScriptDetailRepository.Query(x => x.PropertyId == detail.PropertyId,null,false).Any())
							{
								// script new detail
								var sd = new LOS.DAL.EntityFramework.ScriptDetail();
								sd.Priority = detail.Priority;
								sd.PropertyId = detail.PropertyId;
								sd.Question = detail.Question;
								sd.ScriptId = s.ScriptId;
								sd.ValidRequired = detail.ValidRequired;
								unitOfWork.ScriptDetailRepository.Insert(sd);
							}
							else
							{
								// update
								var sd = unitOfWork.ScriptDetailRepository.GetById(detail.PropertyId);
								sd.Question = detail.Question;
								sd.Priority = detail.Priority;
								sd.ValidRequired = detail.ValidRequired;
								unitOfWork.ScriptDetailRepository.Update(sd);
							}
						}
						else if (detail.Action == "UPDATE")
						{
							var sd = unitOfWork.ScriptDetailRepository.GetById(detail.PropertyId);
							if (sd != null)
							{
								sd.Question = detail.Question;
								sd.Priority = detail.Priority;
								sd.ValidRequired = detail.ValidRequired;
								unitOfWork.ScriptDetailRepository.Update(sd);
							}
							else
							{
								// insert new script detail
								// script new detail
								sd = new LOS.DAL.EntityFramework.ScriptDetail();
								sd.Priority = detail.Priority;
								sd.PropertyId = detail.PropertyId;
								sd.Question = detail.Question;
								sd.ScriptId = s.ScriptId;
								sd.ValidRequired = detail.ValidRequired;
								unitOfWork.ScriptDetailRepository.Insert(sd);
							}
						}
						else if (detail.Action == "DELETE")
						{
							unitOfWork.ScriptDetailRepository.Delete(detail.ScriptDetailId);
						}
						else
						{
							continue;
						}
					}
					// save changed 
					unitOfWork.Save();
					def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
					return Ok(def);
				}
				else
				{
					def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
					return Ok(def);
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Script/Get Exception");
				def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
			}
			return Ok(def);
		}

		[HttpDelete]
		[Authorize]
		[Route("{id}")]
		public ActionResult<DefaultResponse<Meta>> DeleteScript(int id)
		{
			var def = new DefaultResponse<Meta>();
			try
			{
				var identity = (ClaimsIdentity)User.Identity;
				int UserId = int.Parse(identity.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault());
				// valid date input
				var s = unitOfWork.ScriptRepository.GetById(id);
				if (s != null)
				{
					// soft deleted
					unitOfWork.ScriptRepository.SoftDelete(id);
					unitOfWork.Save();
					def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
					return Ok(def);
				}
				else
				{
					def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
					return Ok(def);
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Script/Get Exception");
				def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
			}
			return Ok(def);
		}
	}
}