﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace LOS.WebAPI.Controllers
{
	[ApiVersion("1.0")]
	[Route("api/v{version:apiVersion}/[controller]")]
	[ApiController]
    [Authorize]
    public class SectionController : ControllerBase
    {
		private IUnitOfWork unitOfWork;
		public SectionController(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

		[HttpGet]		
		public ActionResult<DefaultResponse<Meta>> GetAllSections()
		{
			DefaultResponse<object> def = new DefaultResponse<object>();
			def.meta = ResponseHelper.SuccessMeta;
			var pipelines = unitOfWork.SectionRepository.Query(x => x.SectionId > 0 && x.Status != 99, x => x.OrderBy(x1 => x1.SectionId), false)					
					//.Include("SectionDetail.SectionApproveApprove.SectionCondition").Include("SectionDetail.SectionApproveApprove.SectionCondition.Property")
					//.Include("SectionDetail.SectionAction").Include("SectionDetail.SectionAction.Property")
					.Select(x1 => new SectionDTO()
					{
						Name = x1.Name,
						Order = x1.Order,
						SectionGroupId = x1.SectionGroupId,
						SectionId = x1.SectionId,
						IsShow = x1.IsShow,
						PipelineId = x1.PipelineId,
						Details = x1.SectionDetail.Where(x1 => x1.Status != 99).OrderBy(x1 => x1.Step).Select(x2 => new SectionDetailDTO()
						{
							Mode = x2.Mode,
							Name = x2.Name,
							SectionDetailId = x2.SectionDetailId,
							SectionId = x2.SectionId,
							Step = x2.Step,
							IsShow = x2.IsShow,
							DisapproveId = x2.DisapproveId,
							ReturnId = x2.ReturnId,
							Approves = x2.SectionApproveSectionDetail.Where(x => x.Type == 1).Select(x3 => new SectionApproveDTO()
							{
								Name = x3.Name,
								Approve = x3.Approve != null ? new SectionDetailDTO()
								{
									Name = x3.Approve.Name,
									SectionDetailId = x3.Approve.SectionDetailId,
									SectionId = x3.Approve.SectionId,
									Step = x3.Approve.Step
								} : null,
								SectionApproveId = x3.SectionApproveId,
								SectionDetailId = x3.SectionDetailId,
								Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
								{
									Operator = x4.Operator,
									PropertyId = x4.PropertyId,
									SectionConditionId = x4.SectionConditionId,
									SectionApproveId = x4.SectionApproveId,
									Value = x4.Value,
									Property = x4.Property != null ? new PropertyDTO()
									{
										Object = x4.Property.Object,
										FieldName = x4.Property.FieldName,
										Priority = x4.Property.Priority,
										Regex = x4.Property.Regex,
										Type = x4.Property.Type,
										PropertyId = x4.Property.PropertyId,
										Name = x4.Property.Name,
										LinkObject = x4.Property.LinkObject,
										MultipleSelect = x4.Property.MultipleSelect
									} : null
								})
							}),
							Disapproves = x2.SectionApproveSectionDetail.Where(x => x.Type == 2).Select(x3 => new SectionApproveDTO()
							{
								Name = x3.Name,
								Approve = x3.Approve != null ? new SectionDetailDTO()
								{
									Name = x3.Approve.Name,
									SectionDetailId = x3.Approve.SectionDetailId,
									SectionId = x3.Approve.SectionId,
									Step = x3.Approve.Step
								} : null,
								SectionApproveId = x3.SectionApproveId,
								SectionDetailId = x3.SectionDetailId,
								Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
								{
									Operator = x4.Operator,
									PropertyId = x4.PropertyId,
									SectionConditionId = x4.SectionConditionId,
									SectionApproveId = x4.SectionApproveId,
									Value = x4.Value,
									Property = x4.Property != null ? new PropertyDTO()
									{
										Object = x4.Property.Object,
										FieldName = x4.Property.FieldName,
										Priority = x4.Property.Priority,
										Regex = x4.Property.Regex,
										Type = x4.Property.Type,
										PropertyId = x4.Property.PropertyId,
										Name = x4.Property.Name,
										LinkObject = x4.Property.LinkObject,
										MultipleSelect = x4.Property.MultipleSelect
									} : null
								})
							}),							
							Return = x2.Return != null ? new SectionDetailDTO()
							{
								Name = x2.Return.Name,
								SectionDetailId = x2.Return.SectionDetailId,
								SectionId = x2.Return.SectionId,
								Step = x2.Return.Step
							} : null,
							Actions = x2.SectionAction.Select(x3 => new SectionActionDTO()
							{
								PropertyId = x3.PropertyId,
								SectionActionId = x3.SectionActionId,
								SectionDetailId = x3.SectionDetailId,
								Value = x3.Value,
								Type = x3.Type,
								Property = x3.Property != null ? new PropertyDTO()
								{
									Object = x3.Property.Object,
									FieldName = x3.Property.FieldName,
									Priority = x3.Property.Priority,
									Regex = x3.Property.Regex,
									Type = x3.Property.Type,
									PropertyId = x3.Property.PropertyId,
									Name = x3.Property.Name,
									LinkObject = x3.Property.LinkObject,
									MultipleSelect = x3.Property.MultipleSelect
								} : null
							})
						})
					}).ToList();
			def.data = pipelines;
			return Ok(def);
		}

		[HttpGet]
		[Route("{id}")]
		public ActionResult<DefaultResponse<Meta>> GetById(int id)
		{
			DefaultResponse<object> def = new DefaultResponse<object>();
			def.meta = ResponseHelper.SuccessMeta;
			var pipeline = unitOfWork.SectionRepository.Query(x => x.SectionId > 0 && x.SectionId == id && x.Status != 99, x => x.OrderBy(x1 => x1.SectionId), false)
					//.Include("SectionDetail.SectionApproveApprove.SectionCondition").Include("SectionDetail.SectionApproveApprove.SectionCondition.Property")
					//.Include("SectionDetail.SectionAction").Include("SectionDetail.SectionAction.Property")
					.Select(x1 => new SectionDTO()
					{
						Name = x1.Name,
						Order = x1.Order,
						SectionGroupId = x1.SectionGroupId,
						SectionId = x1.SectionId,
						IsShow = x1.IsShow,
						PipelineId = x1.PipelineId,
						Details = x1.SectionDetail.OrderBy(x1 => x1.Step).Select(x2 => new SectionDetailDTO()
						{
							Mode = x2.Mode,
							Name = x2.Name,
							SectionDetailId = x2.SectionDetailId,
							SectionId = x2.SectionId,
							Step = x2.Step,
							IsShow = x2.IsShow,
							DisapproveId = x2.DisapproveId,
							ReturnId = x2.ReturnId,
							Approves = x2.SectionApproveSectionDetail.Where(x => x.Type == 1).Select(x3 => new SectionApproveDTO()
							{
								Name = x3.Name,
								Type = x3.Type,
								Approve = x3.Approve != null ? new SectionDetailDTO()
								{
									Name = x3.Approve.Name,
									SectionDetailId = x3.Approve.SectionDetailId,
									SectionId = x3.Approve.SectionId,
									Step = x3.Approve.Step
								} : null,
								SectionApproveId = x3.SectionApproveId,
								SectionDetailId = x3.SectionDetailId,
								ApproveId = x3.ApproveId,
								Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
								{
									Operator = x4.Operator,
									PropertyId = x4.PropertyId,
									SectionConditionId = x4.SectionConditionId,
									SectionApproveId = x4.SectionApproveId,
									Value = x4.Value,
									Property = x4.Property != null ? new PropertyDTO()
									{
										Object = x4.Property.Object,
										FieldName = x4.Property.FieldName,
										Priority = x4.Property.Priority,
										Regex = x4.Property.Regex,
										Type = x4.Property.Type,
										PropertyId = x4.Property.PropertyId,
										Name = x4.Property.Name,
										LinkObject = x4.Property.LinkObject,
										MultipleSelect = x4.Property.MultipleSelect
									} : null
								})
							}),
							Disapproves = x2.SectionApproveSectionDetail.Where(x => x.Type == 2).Select(x3 => new SectionApproveDTO()
							{
								Name = x3.Name,
								Type = x3.Type,
								Approve = x3.Approve != null ? new SectionDetailDTO()
								{
									Name = x3.Approve.Name,
									SectionDetailId = x3.Approve.SectionDetailId,
									SectionId = x3.Approve.SectionId,
									Step = x3.Approve.Step
								} : null,
								SectionApproveId = x3.SectionApproveId,
								SectionDetailId = x3.SectionDetailId,
								ApproveId = x3.ApproveId,
								Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
								{
									Operator = x4.Operator,
									PropertyId = x4.PropertyId,
									SectionConditionId = x4.SectionConditionId,
									SectionApproveId = x4.SectionApproveId,
									Value = x4.Value,
									Property = x4.Property != null ? new PropertyDTO()
									{
										Object = x4.Property.Object,
										FieldName = x4.Property.FieldName,
										Priority = x4.Property.Priority,
										Regex = x4.Property.Regex,
										Type = x4.Property.Type,
										PropertyId = x4.Property.PropertyId,
										Name = x4.Property.Name,
										LinkObject = x4.Property.LinkObject,
										MultipleSelect = x4.Property.MultipleSelect
									} : null
								})
							}),							
							Return = x2.Return != null ? new SectionDetailDTO()
							{
								Name = x2.Return.Name,
								SectionDetailId = x2.Return.SectionDetailId,
								SectionId = x2.Return.SectionId,
								Step = x2.Return.Step
							} : null,
							Actions = x2.SectionAction.Select(x3 => new SectionActionDTO()
							{
								PropertyId = x3.PropertyId,
								SectionActionId = x3.SectionActionId,
								SectionDetailId = x3.SectionDetailId,
								Value = x3.Value,
								Type = x3.Type,
								Property = x3.Property != null ? new PropertyDTO()
								{
									Object = x3.Property.Object,
									FieldName = x3.Property.FieldName,
									Priority = x3.Property.Priority,
									Regex = x3.Property.Regex,
									Type = x3.Property.Type,
									PropertyId = x3.Property.PropertyId,
									Name = x3.Property.Name,
									LinkObject = x3.Property.LinkObject,
									MultipleSelect = x3.Property.MultipleSelect
								} : null
							})
						})
					});
			def.data = pipeline.FirstOrDefault();
			return Ok(def);
		}

		[HttpGet]
		[Route("detail/{id}")]
		public ActionResult<DefaultResponse<Meta>> GetSectionDetailById(int id)
		{
			DefaultResponse<object> def = new DefaultResponse<object>();
			def.meta = ResponseHelper.SuccessMeta;
			var pipeline = unitOfWork.SectionDetailRepository.Query(x => x.SectionId > 0 && x.SectionDetailId == id && x.Status != 99, x => x.OrderBy(x1 => x1.SectionDetailId), false)
					.Select(x2 => new SectionDetailDTO()
					{
						Mode = x2.Mode,
						Name = x2.Name,
						SectionDetailId = x2.SectionDetailId,
						SectionId = x2.SectionId,
						Step = x2.Step,
						IsShow = x2.IsShow,
						PipelineId = x2.Section.PipelineId,
						Approves = x2.SectionApproveSectionDetail.Where(x => x.Type == 1).Select(x3 => new SectionApproveDTO()
						{
							Name = x3.Name,
							Type = x3.Type,
							Approve = x3.Approve != null ? new SectionDetailDTO()
							{
								Name = x3.Approve.Name,
								SectionDetailId = x3.Approve.SectionDetailId,
								SectionId = x3.Approve.SectionId,
								Step = x3.Approve.Step
							} : null,
							SectionApproveId = x3.SectionApproveId,
							SectionDetailId = x3.SectionDetailId,
							ApproveId = x3.ApproveId,
							Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
							{
								Operator = x4.Operator,
								PropertyId = x4.PropertyId,
								SectionConditionId = x4.SectionConditionId,
								SectionApproveId = x4.SectionApproveId,
								Value = x4.Value,
								Property = x4.Property != null ? new PropertyDTO()
								{
									Object = x4.Property.Object,
									FieldName = x4.Property.FieldName,
									Priority = x4.Property.Priority,
									Regex = x4.Property.Regex,
									Type = x4.Property.Type,
									PropertyId = x4.Property.PropertyId,
									Name = x4.Property.Name,
									LinkObject = x4.Property.LinkObject,
									MultipleSelect = x4.Property.MultipleSelect
								} : null
							})
						}),
						Disapproves = x2.SectionApproveSectionDetail.Where(x => x.Type == 2).Select(x3 => new SectionApproveDTO()
						{
							Name = x3.Name,
							Type = x3.Type,
							Approve = x3.Approve != null ? new SectionDetailDTO()
							{
								Name = x3.Approve.Name,
								SectionDetailId = x3.Approve.SectionDetailId,
								SectionId = x3.Approve.SectionId,
								Step = x3.Approve.Step
							} : null,
							SectionApproveId = x3.SectionApproveId,
							SectionDetailId = x3.SectionDetailId,
							ApproveId = x3.ApproveId,
							Conditions = x3.SectionCondition.Select(x4 => new SectionConditionDTO()
							{
								Operator = x4.Operator,
								PropertyId = x4.PropertyId,
								SectionConditionId = x4.SectionConditionId,
								SectionApproveId = x4.SectionApproveId,
								Value = x4.Value,
								Property = x4.Property != null ? new PropertyDTO()
								{
									Object = x4.Property.Object,
									FieldName = x4.Property.FieldName,
									Priority = x4.Property.Priority,
									Regex = x4.Property.Regex,
									Type = x4.Property.Type,
									PropertyId = x4.Property.PropertyId,
									Name = x4.Property.Name,
									LinkObject = x4.Property.LinkObject,
									MultipleSelect = x4.Property.MultipleSelect
								} : null
							})
						}),						
						Return = x2.Return != null ? new SectionDetailDTO()
						{
							Name = x2.Return.Name,
							SectionDetailId = x2.Return.SectionDetailId,
							SectionId = x2.Return.SectionId,
							Step = x2.Return.Step
						} : null,
						Actions = x2.SectionAction.Select(x3 => new SectionActionDTO()
						{
							PropertyId = x3.PropertyId,
							SectionActionId = x3.SectionActionId,
							SectionDetailId = x3.SectionDetailId,
							Value = x3.Value,
							Type = x3.Type,
							Property = x3.Property != null ? new PropertyDTO()
							{
								Object = x3.Property.Object,
								FieldName = x3.Property.FieldName,
								Priority = x3.Property.Priority,
								Regex = x3.Property.Regex,
								Type = x3.Property.Type,
								PropertyId = x3.Property.PropertyId,
								Name = x3.Property.Name,
								LinkObject = x3.Property.LinkObject,
								MultipleSelect = x3.Property.MultipleSelect
							} : null
						})
					});			
			def.data = pipeline.FirstOrDefault();
			return Ok(def);
		}

		[HttpPost]
		public ActionResult<DefaultResponse<Meta>> PostSection([FromBody] SectionDTO dto)
		{
			Section section = new Section();
			section.Name = dto.Name;
			section.PipelineId = dto.PipelineId;
			section.IsShow = true;
			section.Status = 0;
			var order = unitOfWork.SectionRepository.Count(x => x.PipelineId == dto.PipelineId && x.IsShow == true);
			section.Order = order + 1;
			unitOfWork.SectionRepository.Insert(section);
			unitOfWork.Save();
			DefaultResponse<object> def = new DefaultResponse<object>();
			def.meta = ResponseHelper.SuccessMeta;
			return Ok(def);
		}

		[HttpPut]
		[Route("{id}")]
		public ActionResult<DefaultResponse<Meta>> PutSection(int id,[FromBody] SectionDTO dto)
		{
			DefaultResponse<object> def = new DefaultResponse<object>();
			var section = unitOfWork.SectionRepository.GetById(id);
			if (section != null)
			{
				section.Name = dto.Name;
				unitOfWork.Save();			
				def.meta = ResponseHelper.SuccessMeta;
				return Ok(def);
			}
			else
			{
				def.meta = ResponseHelper.NotFoundMeta;
				return Ok(def);
			}			
		}

		[HttpDelete]
		[Route("{id}")]
		public ActionResult<DefaultResponse<Meta>> DeleteSection(int id)
		{
			DefaultResponse<object> def = new DefaultResponse<object>();
			var section = unitOfWork.SectionRepository.GetById(id);
			if (section != null)
			{
				section.Status = 99;
				unitOfWork.Save();
				def.meta = ResponseHelper.SuccessMeta;
				return Ok(def);
			}
			else
			{
				def.meta = ResponseHelper.NotFoundMeta;
				return Ok(def);
			}
		}

		[HttpPost]
		[Route("{id}/details")]
		public ActionResult<DefaultResponse<Meta>> PostSectionDetail(int id, [FromBody] SectionDetailDTO dto)
		{
			//validate input
			DefaultResponse<Meta> def = new DefaultResponse<Meta>();
			if (dto.SectionId == null || dto.Name == null)
			{
				def.meta = ResponseHelper.BadInputMeta;
				return Ok(def);
			}
			unitOfWork.BeginTransaction();
			try
			{
				var sectionDetail = new SectionDetail();
				sectionDetail.ReturnId = dto.ReturnId;
				sectionDetail.DisapproveId = dto.DisapproveId;
				sectionDetail.IsShow = true;
				sectionDetail.Mode = dto.Mode;
				sectionDetail.Name = dto.Name;
				sectionDetail.SectionId = dto.SectionId;
				sectionDetail.Status = 0;
				// count step			
				var step = unitOfWork.SectionDetailRepository.Count(x => x.SectionId == dto.SectionId);
				sectionDetail.Step = step + 1;
				unitOfWork.SectionDetailRepository.Insert(sectionDetail);
				unitOfWork.Save();
				if (sectionDetail.SectionDetailId > 0)
				{
					// create section action
					foreach (var action in dto.Actions)
					{
						if (action.PropertyId > 0)
						{
							var sectionAction = new SectionAction();
							sectionAction.PropertyId = action.PropertyId;
							sectionAction.SectionDetailId = sectionDetail.SectionDetailId;
							sectionAction.Value = action.Value;
							sectionAction.Type = action.Type;
							unitOfWork.SectionActionRepository.Insert(sectionAction);
						}
					}
					unitOfWork.Save();
					// create section condition
					if (dto.Approves != null &&  dto.Approves.Count() > 0)
					{
						foreach (var approve in dto.Approves)
						{
							if (approve.ApproveId > 0)
							{
								var sectionApprove = new SectionApprove();
								sectionApprove.SectionDetailId = sectionDetail.SectionDetailId;
								sectionApprove.ApproveId = approve.ApproveId;
								sectionApprove.Name = "";
								sectionApprove.Type = 1;
								unitOfWork.SectionApproveRepository.Insert(sectionApprove);
								unitOfWork.Save();
								if (sectionApprove.SectionApproveId > 0)
								{
									if (approve.Conditions != null && approve.Conditions.Count() > 0)
									{
										foreach (var condition in approve.Conditions)
										{
											if (condition.PropertyId > 0 && condition.Value != null && condition.Value != "" && condition.Operator != null && condition.Operator != "")
											{
												var sectionCondition = new SectionCondition();
												sectionCondition.Operator = condition.Operator;
												sectionCondition.PropertyId = condition.PropertyId;
												sectionCondition.SectionApproveId = sectionApprove.SectionApproveId;
												sectionCondition.Value = condition.Value;
												unitOfWork.SectionConditionRepository.Insert(sectionCondition);
											}
										}
										unitOfWork.Save();
									}
								}
							}
						}
					}
					// create section condition
					if (dto.Disapproves != null && dto.Disapproves.Count() > 0)
					{
						foreach (var disapprove in dto.Disapproves)
						{
							if (disapprove.ApproveId > 0)
							{
								var sectionApprove = new SectionApprove();
								sectionApprove.SectionDetailId = sectionDetail.SectionDetailId;
								sectionApprove.ApproveId = disapprove.ApproveId;
								sectionApprove.Name = "";
								sectionApprove.Type = 2;
								unitOfWork.SectionApproveRepository.Insert(sectionApprove);
								unitOfWork.Save();
								if (sectionApprove.SectionApproveId > 0)
								{
									if (disapprove.Conditions != null && disapprove.Conditions.Count() > 0)
									{
										foreach (var condition in disapprove.Conditions)
										{
											if (condition.PropertyId > 0 && condition.Value != null && condition.Value != "" && condition.Operator != null && condition.Operator != "")
											{
												var sectionCondition = new SectionCondition();
												sectionCondition.Operator = condition.Operator;
												sectionCondition.PropertyId = condition.PropertyId;
												sectionCondition.SectionApproveId = sectionApprove.SectionApproveId;
												sectionCondition.Value = condition.Value;
												unitOfWork.SectionConditionRepository.Insert(sectionCondition);
											}
										}
										unitOfWork.Save();
									}
								}
							}
						}
					}
					unitOfWork.CommitTransaction();
					def.meta = ResponseHelper.SuccessMeta;
				}
				else
				{
					unitOfWork.RollbackTransaction();
					def.meta = ResponseHelper.InternalErrorMeta;
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Section Detail Exception");
				def.meta = ResponseHelper.InternalErrorMeta;
			}
			return Ok(def);
		}

		[HttpPut]
		[Route("{id}/details/{did}")]
		public ActionResult<DefaultResponse<Meta>> PutSectionDetail(int id,int did, [FromBody] SectionDetailDTO dto)
		{
			//validate input
			DefaultResponse<Meta> def = new DefaultResponse<Meta>();
			if (dto.SectionId == null || dto.Name == null)
			{
				def.meta = ResponseHelper.BadInputMeta;
				return Ok(def);
			}
			unitOfWork.BeginTransaction();
			try
			{
				var sectionDetail = unitOfWork.SectionDetailRepository.GetById(did);
				if (sectionDetail == null)
				{
					def.meta = ResponseHelper.NotFoundMeta;
					return Ok(def);
				}
				sectionDetail.ReturnId = dto.ReturnId;
				sectionDetail.DisapproveId = dto.DisapproveId;				
				sectionDetail.Mode = dto.Mode;
				sectionDetail.Name = dto.Name;								
				unitOfWork.SectionDetailRepository.Update(sectionDetail);
				unitOfWork.Save();
				if (sectionDetail.SectionDetailId > 0)
				{
					// remove all actions					
					unitOfWork.SectionActionRepository.Delete(x => x.SectionDetailId == sectionDetail.SectionDetailId);
					// create section action
					foreach (var action in dto.Actions)
					{
						if (action.PropertyId > 0)
						{
							var sectionAction = new SectionAction();
							sectionAction.PropertyId = action.PropertyId;
							sectionAction.SectionDetailId = sectionDetail.SectionDetailId;
							sectionAction.Value = action.Value;
							sectionAction.Type = action.Type;
							unitOfWork.SectionActionRepository.Insert(sectionAction);
						}
					}
					unitOfWork.Save();
					// create section condition
					if (dto.Approves != null && dto.Approves.Count() > 0)
					{
						// remove all approves
						var approves = unitOfWork.SectionApproveRepository.Query(x => x.SectionDetailId == sectionDetail.SectionDetailId && x.Type == 1, null, false).ToList();
						if (approves != null)
						{
							foreach (var app in approves)
							{
								unitOfWork.SectionConditionRepository.Delete(x => x.SectionApproveId == app.SectionApproveId);
							}
							unitOfWork.SectionApproveRepository.Delete(x => x.SectionDetailId == sectionDetail.SectionDetailId && x.Type == 1);
						}
						foreach (var approve in dto.Approves)
						{
							if (approve.ApproveId > 0)
							{
								var sectionApprove = new SectionApprove();
								sectionApprove.SectionDetailId = sectionDetail.SectionDetailId;
								sectionApprove.ApproveId = approve.ApproveId;
								sectionApprove.Name = "";
								sectionApprove.Type = 1;
								unitOfWork.SectionApproveRepository.Insert(sectionApprove);
								unitOfWork.Save();
								if (sectionApprove.SectionApproveId > 0)
								{
									if (approve.Conditions != null && approve.Conditions.Count() > 0)
									{
										foreach (var condition in approve.Conditions)
										{
											if (condition.PropertyId > 0 && condition.Value != null && condition.Value != "" && condition.Operator != null && condition.Operator != "")
											{
												var sectionCondition = new SectionCondition();
												sectionCondition.Operator = condition.Operator;
												sectionCondition.PropertyId = condition.PropertyId;
												sectionCondition.SectionApproveId = sectionApprove.SectionApproveId;
												sectionCondition.Value = condition.Value;
												unitOfWork.SectionConditionRepository.Insert(sectionCondition);
											}
										}
										unitOfWork.Save();
									}
								}
							}
						}
					}
					// create section condition
					if (dto.Disapproves != null && dto.Disapproves.Count() > 0)
					{
						// remove all approves
						var approves = unitOfWork.SectionApproveRepository.Query(x => x.SectionDetailId == sectionDetail.SectionDetailId && x.Type == 2, null, false).ToList();
						if (approves != null)
						{
							foreach (var app in approves)
							{
								unitOfWork.SectionConditionRepository.Delete(x => x.SectionApproveId == app.SectionApproveId);
							}
							unitOfWork.SectionApproveRepository.Delete(x => x.SectionDetailId == sectionDetail.SectionDetailId && x.Type == 2);
						}
						foreach (var disapprove in dto.Disapproves)
						{
							if (disapprove.ApproveId > 0)
							{
								var sectionApprove = new SectionApprove();
								sectionApprove.SectionDetailId = sectionDetail.SectionDetailId;
								sectionApprove.ApproveId = disapprove.ApproveId;
								sectionApprove.Name = "";
								sectionApprove.Type = 2;
								unitOfWork.SectionApproveRepository.Insert(sectionApprove);
								unitOfWork.Save();
								if (sectionApprove.SectionApproveId > 0)
								{
									if (disapprove.Conditions != null && disapprove.Conditions.Count() > 0)
									{
										foreach (var condition in disapprove.Conditions)
										{
											if (condition.PropertyId > 0 && condition.Value != null && condition.Value != "" && condition.Operator != null && condition.Operator != "")
											{
												var sectionCondition = new SectionCondition();
												sectionCondition.Operator = condition.Operator;
												sectionCondition.PropertyId = condition.PropertyId;
												sectionCondition.SectionApproveId = sectionApprove.SectionApproveId;
												sectionCondition.Value = condition.Value;
												unitOfWork.SectionConditionRepository.Insert(sectionCondition);
											}
										}
										unitOfWork.Save();
									}
								}
							}
						}
					}
					unitOfWork.CommitTransaction();
					def.meta = ResponseHelper.SuccessMeta;
				}
				else
				{
					unitOfWork.RollbackTransaction();
					def.meta = ResponseHelper.InternalErrorMeta;
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Section Detail Exception");
				def.meta = ResponseHelper.InternalErrorMeta;
			}
			return Ok(def);
		}

		[HttpDelete]
		[Route("{id}/details/{did}")]
		public ActionResult<DefaultResponse<Meta>> DeleteSectionDetail(int id,int did)
		{
			//validate input
			DefaultResponse<Meta> def = new DefaultResponse<Meta>();
			var sectionDetail = unitOfWork.SectionDetailRepository.GetById(did);
			if (sectionDetail != null)
			{
				sectionDetail.Status = 99;
				def.meta = ResponseHelper.SuccessMeta;
				unitOfWork.Save();
			}
			else
			{
				def.meta = ResponseHelper.NotFoundMeta;
				return Ok(def);
			}
			return Ok(def);
		}
	}
}