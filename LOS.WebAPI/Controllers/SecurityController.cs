﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Request;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration configuration;

        public SecurityController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this.configuration = configuration;
        }

        [HttpPost]
        [EnableCors("AllowAll")]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<DefaultResponse<UserDetail>>> Login([FromBody] LoginReq req)
        {
            var def = new DefaultResponse<UserDetail>();
            try
            {
                req.password = req.password.ToLower();
                var data = await _unitOfWork.UserRepository.Query(x => x.Username == req.username
                                        && x.Password == req.password, null, false, x => x.Company, x => x.UserTeamTelesales)
                                        .Include(x => x.UserModule).Select(UserDetail.ProjectionDetail).FirstOrDefaultAsync();
                if (data != null)
                {
                    #region insert into log

                    #endregion
                    #region generate token
                    var claims = new[]{
                                                    new Claim(ClaimTypes.NameIdentifier, data.UserId.ToString()),
                                                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                                    new Claim("Id", data.UserId.ToString()),
                                                    new Claim("CompanyId" , data.Company != null ? data.Company?.CompanyId.ToString() : ""),
                                                    new Claim("DepartmentId" , data.Department != null ? data.Department?.DepartmentId.ToString() : ""),
                                                    new Claim("PositionId" , data.Position != null ? data.Position?.PositionId.ToString() : "")
                                                };

                    var token = new JwtSecurityToken
                    (
                        issuer: configuration["JWT:Issuer"],
                        audience: configuration["JWT:Audience"],
                        claims: claims,
                        expires: DateTime.UtcNow.AddDays(90),
                        notBefore: DateTime.UtcNow,
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:SigningKey"])),
                                SecurityAlgorithms.HmacSha256)
                    );

                    data.Token = new JwtSecurityTokenHandler().WriteToken(token);
                    #endregion
                    #region get user permission					
                    var group = _unitOfWork.UserRepository.Query(x => x.UserId == data.UserId, null, false)
                        .Include(x => x.Group).ThenInclude(x => x.GroupModule)
                          //.ThenInclude(x => x.GroupModulePermission).ThenInclude(x => x.Permission)
                          .Select(x => x.Group).Select(GroupDetail.ProjectionDetail).FirstOrDefault();
                    data.Group = group;

                    #endregion
                    def.data = data;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    if (_unitOfWork.UserRepository.Any(x => x.Username == req.username))
                    {
                        def.meta = new Meta(ResponseHelper.USER_WRONG_PASSWORD_CODE, "Sai thông tin tài khoản");
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Không tồn tại tài khoản trong hệ thống");
                    }
                }
                if (def.data != null)
                    def.data.Password = string.Empty;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Security/Login exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("redirect_login")]
        [AllowAnonymous]
        public async Task<ActionResult<DefaultResponse<UserDetail>>> RedirectLogin([FromBody] InfoLoginReq req)
        {
            var def = new DefaultResponse<UserDetail>();
            try
            {
                var data = await _unitOfWork.UserRepository.Query(x => x.UserIdSso == req.UserId, null, false, x => x.Company)
                                        .Include(x => x.UserModule).Select(UserDetail.ProjectionDetail).FirstOrDefaultAsync();
                if (data != null)
                {

                    #region generate token
                    var claims = new[]{
                                                    new Claim(ClaimTypes.NameIdentifier, data.UserId.ToString()),
                                                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                                    new Claim("Id", data.UserId.ToString()),
                                                    new Claim("CompanyId" , data.Company != null ? data.Company?.CompanyId.ToString() : ""),
                                                    new Claim("DepartmentId" , data.Department != null ? data.Department?.DepartmentId.ToString() : ""),
                                                    new Claim("PositionId" , data.Position != null ? data.Position?.PositionId.ToString() : "")
                                                };

                    var token = new JwtSecurityToken
                    (
                        issuer: configuration["JWT:Issuer"],
                        audience: configuration["JWT:Audience"],
                        claims: claims,
                        expires: DateTime.UtcNow.AddDays(90),
                        notBefore: DateTime.UtcNow,
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:SigningKey"])),
                                SecurityAlgorithms.HmacSha256)
                    );

                    data.Token = new JwtSecurityTokenHandler().WriteToken(token);
                    #endregion
                    #region get user permission					
                    var group = _unitOfWork.UserRepository.Query(x => x.UserId == data.UserId, null, false)
                        .Include(x => x.Group).ThenInclude(x => x.GroupModule)
                          //.ThenInclude(x => x.GroupModulePermission).ThenInclude(x => x.Permission)
                          .Select(x => x.Group).Select(GroupDetail.ProjectionDetail).FirstOrDefault();
                    data.Group = group;

                    //d/s shop
                    var shops = _unitOfWork.UserShopRepository.Query(x => x.UserId == data.UserId && x.Status == true, null, false).Include(x => x.Shop).Select(x => new ShopDetail
                    {
                        Name = x.Shop.Name,
                        ShopId = x.Shop.ShopId,
                        CityId = x.Shop.CityId
                    }).ToList();
                    data.ListShop = shops;

                    #endregion
                    def.data = data;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {

                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
                def.data.Password = string.Empty;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Security/Info_Login exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("get_user_detail")]
        [Authorize]
        public ActionResult<DefaultResponse<object>> GetUserDetail()
        {
            return Ok();
        }
    }
}