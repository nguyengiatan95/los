﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class ShopController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        public ShopController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        [Route("get_hubs")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<ShopDetail>>> GetAllHub()
        {
            var def = new DefaultResponse<List<ShopDetail>>();
            var shop = unitOfWork.ShopRepository.Query(x => x.Status == 1, null, false).Select(ShopDetail.ProjectionDetail).ToList();
            def.meta = new Meta(200, "success");
            def.data = shop;
            return Ok(def);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<DefaultResponse<ShopDetail>> GetById(int id)
        {
            var def = new DefaultResponse<ShopDetail>();
            var shop = unitOfWork.ShopRepository.Query(x => x.ShopId == id, null, false).Select(ShopDetail.ProjectionDetail).FirstOrDefault();
            def.meta = new Meta(200, "success");
            def.data = shop;
            return Ok(def);
        }
    }
}