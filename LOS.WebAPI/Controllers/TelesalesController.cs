﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using LOS.Common.Extensions;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class TelesalesController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        public TelesalesController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpPost]
        //[Authorize]
        [Route("update_call_status")]
        public ActionResult<DefaultResponse<Meta>> UpdateCallStatus([FromBody] UpdateCallStatusReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.loanActionId <= 0 || req.actionStatusId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanAction = unitOfWork.LoanActionRepository.GetById(req.loanActionId);
                if (loanAction != null)
                {
                    // update to new status
                    unitOfWork.LoanActionRepository.Update(x => x.LoanActionId == req.loanActionId, x => new LoanAction()
                    {
                        ActionStatusId = req.actionStatusId,
                        LastActionStatusId = loanAction.ActionStatusId.Value
                    });
                    if (req.actionStatusId >= 14)
                    {
                        var loanBrief = unitOfWork.LoanBriefRepository.GetById(loanAction.LoanBriefId);
                        // kiểm tra nếu về trạng thái finish
                        if (loanBrief != null)
                        {
                            if (loanBrief.Status == 11)
                            {
                                loanBrief.Status = 10;
                                loanBrief.UpdatedTime = DateTimeOffset.Now;
                            }
                            else if (loanBrief.Status == 14) // Schedule call
                            {
                                // nếu khách hàng nghe máy ?
                                if (req.actionStatusId == 14)
                                {
                                    loanBrief.Status = 15;
                                    loanBrief.UpdatedTime = DateTimeOffset.Now;
                                    // update schedule status
                                    unitOfWork.ScheduleRepository.Update(x => x.LoanBriefId == loanBrief.LoanBriefId, x => new Schedule()
                                    {
                                        Status = 10
                                    });
                                }
                                else
                                {
                                    loanBrief.Status = 14;
                                    loanBrief.UpdatedTime = DateTimeOffset.Now;
                                    // update schedule status
                                    unitOfWork.ScheduleRepository.Update(x => x.LoanBriefId == loanBrief.LoanBriefId, x => new Schedule()
                                    {
                                        Status = 0
                                    });
                                }
                            }
                            else
                            {
                                loanBrief.Status = 15;
                                loanBrief.UpdatedTime = DateTimeOffset.Now;
                            }
                            unitOfWork.LoanBriefRepository.Update(loanBrief);
                            unitOfWork.Save();
                        }
                        else
                        {
                            def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                            return Ok(def);
                        }
                    }
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Telesales/Update Call Status Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        //[Authorize]
        [Route("update_schedule_call")]
        public ActionResult<DefaultResponse<Meta>> UpdateScheduleCall([FromBody] UpdateScheduleCallReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.loanActionId <= 0 || req.actionStatusId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanAction = unitOfWork.LoanActionRepository.GetById(req.loanActionId);
                if (loanAction != null)
                {
                    // update to new status
                    unitOfWork.LoanActionRepository.Update(x => x.LoanActionId == req.loanActionId, x => new LoanAction()
                    {
                        ActionStatusId = req.actionStatusId,
                        LastActionStatusId = loanAction.ActionStatusId.Value
                    });
                    // update loan status
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanAction.LoanBriefId, x => new LoanBrief()
                    {
                        Status = 14,
                        BoundTelesaleId = loanAction.ActorId
                    });
                    // set schedule 
                    unitOfWork.ScheduleRepository.Insert(new Schedule()
                    {
                        CreatedAt = DateTimeOffset.Now,
                        LoanBriefId = loanAction.LoanBriefId,
                        ScheduleTime = DateTimeOffset.ParseExact(req.scheduleTime, "dd/MM/yyyy HH:mm", null),
                        Status = 0,
                        UserId = loanAction.ActorId
                    });
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Telesales/Update Call Status Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        //[Authorize]
        [Route("get_missed_calls")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefTelesale>>> GetMissedCalls([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 10, [FromQuery] int userId = -1)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefTelesale>>();
            try
            {
                var query = (from loan in unitOfWork.LoanBriefRepository.All()
                             join district in unitOfWork.DistrictRepository.All() on loan.DistrictId equals district.DistrictId into LoanDistricts
                             from LoanDistrict in LoanDistricts.DefaultIfEmpty()
                             join province in unitOfWork.ProvinceRepository.All() on loan.ProvinceId equals province.ProvinceId into LoanProvinces
                             from LoanProvince in LoanProvinces.DefaultIfEmpty()
                             join product in unitOfWork.LoanProductRepository.All() on loan.ProductId equals product.LoanProductId into LoanProducts
                             from LoanProduct in LoanProducts.DefaultIfEmpty()
                             join loanAction in unitOfWork.LoanActionRepository.All() on loan.LoanBriefId equals loanAction.LoanBriefId
                             where loanAction.ActionStatusId == 6 && loanAction.ActorId == userId // missed call
                             orderby loanAction.StartTime descending
                             select new LoanBriefTelesale()
                             {
                                 CreatedTime = loan.CreatedTime,
                                 DistrictId = loan.DistrictId,
                                 District = LoanDistrict != null ? new DistrictDTO()
                                 {
                                     DistrictId = LoanDistrict.DistrictId,
                                     Name = LoanDistrict.Name
                                 } : null,
                                 FullName = loan.FullName,
                                 LoanAmount = loan.LoanAmount,
                                 IsRetrieved = true,
                                 LoanActionId = loanAction.LoanActionId,
                                 LoanBriefId = loan.LoanBriefId,
                                 LoanTime = loan.LoanTime,
                                 Phone = loan.Phone,
                                 Product = LoanProduct != null ? new LoanProductDTO()
                                 {
                                     LoanProductId = LoanProduct.LoanProductId,
                                     Name = LoanProduct.Name
                                 } : null,
                                 ProductId = loan.ProductId,
                                 Province = LoanProvince != null ? new ProvinceDTO()
                                 {
                                     ProvinceId = LoanProvince.ProvinceId,
                                     Name = LoanProvince.Name
                                 } : null,
                                 ProvinceId = loan.ProvinceId,
                                 RateTypeId = loan.RateTypeId,
                                 StartTime = loanAction.StartTime
                             });
                var totalRecords = query.Count();
                List<LoanBriefTelesale> data;
                if (skip == -1)
                    data = query.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                    data = query.Skip(skip).Take(take).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Telesales/Get Missed Calls Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        //[Authorize]
        [Route("get_scheduled_calls")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefTelesale>>> GetScheduledCalls([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 10, [FromQuery] int userId = -1)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefTelesale>>();
            try
            {
                var query = (from loan in unitOfWork.LoanBriefRepository.All()
                             join district in unitOfWork.DistrictRepository.All() on loan.DistrictId equals district.DistrictId into LoanDistricts
                             from LoanDistrict in LoanDistricts.DefaultIfEmpty()
                             join province in unitOfWork.ProvinceRepository.All() on loan.ProvinceId equals province.ProvinceId into LoanProvinces
                             from LoanProvince in LoanProvinces.DefaultIfEmpty()
                             join product in unitOfWork.LoanProductRepository.All() on loan.ProductId equals product.LoanProductId into LoanProducts
                             from LoanProduct in LoanProducts.DefaultIfEmpty()
                             join schedule in unitOfWork.ScheduleRepository.All() on loan.LoanBriefId equals schedule.LoanBriefId
                             where loan.Status == 14 // scheduled 
                             orderby schedule.ScheduleTime descending
                             select new LoanBriefTelesale()
                             {
                                 CreatedTime = loan.CreatedTime,
                                 DistrictId = loan.DistrictId,
                                 District = LoanDistrict != null ? new DistrictDTO()
                                 {
                                     DistrictId = LoanDistrict.DistrictId,
                                     Name = LoanDistrict.Name
                                 } : null,
                                 FullName = loan.FullName,
                                 LoanAmount = loan.LoanAmount,
                                 IsRetrieved = true,
                                 LoanBriefId = loan.LoanBriefId,
                                 LoanTime = loan.LoanTime,
                                 Phone = loan.Phone,
                                 Product = LoanProduct != null ? new LoanProductDTO()
                                 {
                                     LoanProductId = LoanProduct.LoanProductId,
                                     Name = LoanProduct.Name
                                 } : null,
                                 ProductId = loan.ProductId,
                                 Province = LoanProvince != null ? new ProvinceDTO()
                                 {
                                     ProvinceId = LoanProvince.ProvinceId,
                                     Name = LoanProvince.Name
                                 } : null,
                                 ProvinceId = loan.ProvinceId,
                                 RateTypeId = loan.RateTypeId,
                                 ScheduleTime = schedule.ScheduleTime.Value,
                                 ScheduleStatus = schedule.Status
                             });
                var totalRecords = query.Count();
                List<LoanBriefTelesale> data;
                if (skip == -1)
                    data = query.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                    data = query.Skip(skip).Take(take).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Telesales/Get Missed Calls Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        //[Authorize]
        [Route("get_called")]
        public ActionResult<DefaultResponse<SummaryMeta, List<LoanBriefTelesale>>> GetCalled([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1,
            [FromQuery] int take = 10, [FromQuery] int userId = -1)
        {
            var def = new DefaultResponse<SummaryMeta, List<LoanBriefTelesale>>();
            try
            {
                var calledStatus = unitOfWork.ActionStatusRepository.Query(x => x.ActionId == 3, null, false).Select(x => x.ActionStatusId).ToList();
                var query = (from loan in unitOfWork.LoanBriefRepository.All()
                             join district in unitOfWork.DistrictRepository.All() on loan.DistrictId equals district.DistrictId into LoanDistricts
                             from LoanDistrict in LoanDistricts.DefaultIfEmpty()
                             join province in unitOfWork.ProvinceRepository.All() on loan.ProvinceId equals province.ProvinceId into LoanProvinces
                             from LoanProvince in LoanProvinces.DefaultIfEmpty()
                             join product in unitOfWork.LoanProductRepository.All() on loan.ProductId equals product.LoanProductId into LoanProducts
                             from LoanProduct in LoanProducts.DefaultIfEmpty()
                             join loanAction in unitOfWork.LoanActionRepository.Query(x => calledStatus.Contains(x.ActionStatusId.Value) && x.ActorId == userId, x => x.OrderByDescending(x1 => x1.StartTime), 1, 1, false) on loan.LoanBriefId equals loanAction.LoanBriefId
                             orderby loan.UpdatedTime descending
                             select new LoanBriefTelesale()
                             {
                                 CreatedTime = loan.CreatedTime,
                                 DistrictId = loan.DistrictId,
                                 District = LoanDistrict != null ? new DistrictDTO()
                                 {
                                     DistrictId = LoanDistrict.DistrictId,
                                     Name = LoanDistrict.Name
                                 } : null,
                                 FullName = loan.FullName,
                                 LoanAmount = loan.LoanAmount,
                                 IsRetrieved = true,
                                 LoanBriefId = loan.LoanBriefId,
                                 LoanTime = loan.LoanTime,
                                 Phone = loan.Phone,
                                 Product = LoanProduct != null ? new LoanProductDTO()
                                 {
                                     LoanProductId = LoanProduct.LoanProductId,
                                     Name = LoanProduct.Name
                                 } : null,
                                 ProductId = loan.ProductId,
                                 Province = LoanProvince != null ? new ProvinceDTO()
                                 {
                                     ProvinceId = LoanProvince.ProvinceId,
                                     Name = LoanProvince.Name
                                 } : null,
                                 ProvinceId = loan.ProvinceId,
                                 RateTypeId = loan.RateTypeId,
                                 Status = loan.Status,
                                 LoanAction = new LoanActionDetail()
                                 {
                                     LoanActionId = loanAction.LoanActionId,
                                     StartTime = loanAction.StartTime,
                                     FinishTime = loanAction.FinishTime,
                                     Note = loanAction.Note,
                                     ActionStatusText = "",
                                     ActorId = loanAction.ActorId
                                 }
                             });
                var totalRecords = query.Count();
                List<LoanBriefTelesale> data;
                if (skip == -1)
                    data = query.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                    data = query.Skip(skip).Take(take).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Telesales/Get Missed Calls Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpPost]
        //[Authorize]
        [Route("update_missed_call")]
        public ActionResult<DefaultResponse<Meta>> UpdateMissCall([FromBody] UpdateCallStatusReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.loanActionId <= 0 || req.actionStatusId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanAction = unitOfWork.LoanActionRepository.GetById(req.loanActionId);
                if (loanAction != null)
                {
                    // update to new status
                    unitOfWork.LoanActionRepository.Update(x => x.LoanActionId == req.loanActionId, x => new LoanAction()
                    {
                        ActionStatusId = req.actionStatusId,
                        LastActionStatusId = loanAction.ActionStatusId.Value
                    });
                    // kiểm tra nếu về trạng thái finish
                    unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanAction.LoanBriefId, x => new LoanBrief()
                    {
                        Status = 15,
                        UpdatedTime = DateTimeOffset.Now
                    });
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Telesales/Update missed call  Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        //[Authorize]
        [Route("bound_telesale")]
        public ActionResult<DefaultResponse<Meta>> BoundTelesale([FromBody] BoundTelesaleReq req)
        {
            var def = new DefaultResponse<Meta>();
            try
            {
                if (req.loanBriefId <= 0 || req.userId <= 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                // check if loan action exists
                var loanBrief = unitOfWork.LoanBriefRepository.GetById(req.loanBriefId);
                if (loanBrief != null)
                {
                    if (loanBrief.BoundTelesaleId != req.userId && loanBrief.BoundTelesaleId != null)
                    {
                        def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                        return Ok(def);
                    }
                    if (loanBrief.Status == 15)
                    {
                        // bound
                        loanBrief.BoundTelesaleId = req.userId;
                        loanBrief.Status = 16; // Bound to telesale;
                        loanBrief.UpdatedTime = DateTimeOffset.Now;
                    }
                    else
                    {
                        // unbound
                        loanBrief.BoundTelesaleId = null;
                        loanBrief.Status = 15; // Bound to telesale;
                        loanBrief.UpdatedTime = DateTimeOffset.Now;
                    }
                    unitOfWork.LoanBriefRepository.Update(loanBrief);
                    unitOfWork.Save();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Telesales/Update missed call  Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        //[Authorize]
        [Route("get_missed_call_reasons")]
        public ActionResult<DefaultResponse<Meta, List<ActionStatusDTO>>> GetMissedCallReasons()
        {
            var def = new DefaultResponse<Meta, List<ActionStatusDTO>>();
            try
            {
                var reasons = (from actionStatus in unitOfWork.ActionStatusRepository.All()
                               join action in unitOfWork.ActionRepository.All() on actionStatus.ActionId equals action.ActionId
                               where action.ActionCode == "MISSED_CALL_REASONS"
                               select new ActionStatusDTO()
                               {
                                   Action = actionStatus.Action,
                                   ActionStatusId = actionStatus.ActionStatusId,
                                   Description = actionStatus.Description
                               }).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = reasons;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Telesales/Get Missed Calls Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("statistics/get_calls_by_customer_status")]
        public ActionResult<DefaultResponse<Meta, object>> GetCallsByCustomerStatus()
        {
            return Ok();
        }

        [HttpGet]
        [Route("get_staff")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetListStaff([FromQuery] int page = 1, [FromQuery] int pageSize = 25,
            [FromQuery] int status = -1, [FromQuery] string name = null, [FromQuery] int team = 0,
            [FromQuery] int typeRecievedProduct = 0, [FromQuery] string teamTelesales = null,
            [FromQuery] string sortBy = "CityId", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<UserDetail>>();
            try
            {
                //List<int> lstTeamTelesalesId = new List<int>();
                //if (teamTelesales != null)
                //{
                //    lstTeamTelesalesId = teamTelesales.Split(',').Select(Int32.Parse).ToList();
                //}

                var query = unitOfWork.UserRepository.Query(x => x.IsDeleted != true
            && (x.Status == status || status == -1)
            && (x.GroupId == 2) 
            && (x.TeamTelesalesId == team || team == 0)
            && (x.TypeRecievedProduct == typeRecievedProduct || typeRecievedProduct == 0)
            //&& (lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value))
            && (name == null || x.Username.ToLower().Contains(name.ToLower()) || x.FullName.ToLower().Contains(name.ToLower()))
            , null, false, x => x.Company, x => x.Group).Select(UserDetail.ProjectionDetail);
                var totalRecords = query.Count();
                List<UserDetail> data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Telesale/GetListStaff Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }
        [Route("get_list_telesales_shift")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<TelesalesShift>>> GetListTelesalesShift()
        {
            var def = new DefaultResponse<List<TelesalesShift>>();
            var data = unitOfWork.TelesalesShiftRepository.Query(x => x.IsEnable == true, null, false).ToList();
            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }
        [Route("get_list_team_telesales")]
        [HttpGet]
        public ActionResult<DefaultResponse<List<TeamTelesales>>> GetListTeamTelesales()
        {
            var def = new DefaultResponse<List<TeamTelesales>>();
            var data = unitOfWork.TeamTelesalesRepository.Query(x => x.IsEnable == true, null, false).ToList();
            def.meta = new Meta(200, "success");
            def.data = data;
            return Ok(def);
        }

        [Route("get_telesales_by_team")]
        [HttpGet]
        public ActionResult<DefaultResponse<Meta, List<UserDetail>>> GetTelesalesByTeam(string teamTelesales)
        {
            var def = new DefaultResponse<Meta, List<UserDetail>>();
            try
            {
                List<int> lstTeamTelesalesId = new List<int>();
                if (!string.IsNullOrEmpty(teamTelesales))
                    lstTeamTelesalesId = teamTelesales.Split(',').Select(Int32.Parse).ToList();
                var lstData = unitOfWork.UserRepository.Query(x=> lstTeamTelesalesId.Contains(x.TeamTelesalesId.Value)
                 && x.IsDeleted != true && x.Status == (int)EnumStatusBase.Active, null, false).Select(UserDetail.ProjectionDetail).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch(Exception ex)
            {
                Log.Error(ex, "Telesale/GetTelesalesByTeam Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        
        [HttpGet]
        [Route("get_team")]
        public async Task< ActionResult<DefaultResponse<Meta, TeamTelesales>>> GetTelesalesTeam(int teamTelesaleId)
        {
            var def = new DefaultResponse<Meta, TeamTelesales>();
            try
            {
               
                var lstData = await unitOfWork.TeamTelesalesRepository.Query(x => x.TeamTelesalesId == teamTelesaleId, null, false).FirstOrDefaultAsync();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = lstData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Telesale/GetTelesalesTeam Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}