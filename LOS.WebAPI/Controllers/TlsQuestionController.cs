﻿using LOS.Common.Models.Request;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
	[Route("api/v{version:apiVersion}/[controller]")]
	[ApiController]
	public class TlsQuestionController : LosControllerBase<TlsQuestion, ApiPagingPostModel>
    {
		private IUnitOfWork _unitOfWork;
		public TlsQuestionController(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.TlsQuestionRepository)
		{
            _unitOfWork = unitOfWork;
		}
        public override async Task<Expression<Func<TlsQuestion, bool>>> GetAllCondition(ApiPagingPostModel input)
        {
            if (!string.IsNullOrEmpty(input.Keyword))
            {
                Expression<Func<TlsQuestion, bool>> condition = x => x.Question.Contains(input.Keyword) || x.JsonOptions.Contains(input.Keyword);
                return condition;
            }
            return null;
            
        }
    }
}