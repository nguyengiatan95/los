﻿using LOS.Common.Helpers;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
	[Route("api/v{version:apiVersion}/[controller]")]
	[ApiController]
	public class TlsScenarioController : LosControllerBase<TlsScenario, ApiPagingPostModel>
    {
		private IUnitOfWork _unitOfWork;
		public TlsScenarioController(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.TlsScenarioRepository)
		{
            _unitOfWork = unitOfWork;
		}
        public override async Task<Expression<Func<TlsScenario, bool>>> GetAllCondition(ApiPagingPostModel input)
        {
            if (!string.IsNullOrEmpty(input.Keyword))
            {
                Expression<Func<TlsScenario, bool>> condition = x => x.ConfigName.Contains(input.Keyword);
                return condition;
            }
            return null;
        }
        public override async Task<DefaultResponse<bool>> DeleteByIdAsync(int id)
        {
            var output = await base.DeleteByIdAsync(id);
            if (output.meta.errorCode == ResponseHelper.SUCCESS_CODE)
            {
                var questions = _unitOfWork.TlsScenarioQuestionRepository.All().Where(x => x.ScenarioId == id).ToList();
                if (questions.Count > 0)
                {
                    foreach (var item in questions)
                    {
                        _unitOfWork.TlsScenarioQuestionRepository.Delete(item.Id);
                    }
                }
                _unitOfWork.Save();
            }
            return output;
        }
    }
}