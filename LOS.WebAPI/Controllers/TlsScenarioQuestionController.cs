﻿using LOS.Common.Helpers;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Models.Request.TlsScenarioQuestion;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class TlsScenarioQuestionController : LosControllerBase<TlsScenarioQuestion, ApiPagingPostModel>
    {
        private IUnitOfWork _unitOfWork;
        public TlsScenarioQuestionController(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.TlsScenarioQuestionRepository)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("GetQuestionIdsByScenarioIdAsync/{id}")]
        public async Task<DefaultResponse<IEnumerable<int>>> GetQuestionIdsByScenarioIdAsync(int id)
        {
            var questions = _unitOfWork.TlsScenarioQuestionRepository.All().Where(x => x.ScenarioId == id).OrderBy(x => x.Priority).Select(x => x.QuestionId).ToList();
            return new DefaultResponse<IEnumerable<int>> { meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE), data = questions };
        }

        [HttpPatch]
        [Route("CreateMappingQuestionWithScenarioAsync")]
        public async Task<DefaultResponse<bool>> CreateMappingQuestionWithScenarioAsync(CreateMappingQuestionWithScenario_InputModel input)
        {
            var output = new DefaultResponse<bool>();
            var scenario = _unitOfWork.TlsScenarioRepository.All().Where(x => x.Id == input.ScenarioId);
            if (scenario.Count()  == 0)
            {
                output.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                return output;
            }
            try
            {
                var questions = _unitOfWork.TlsScenarioQuestionRepository.All().Where(x => x.ScenarioId == input.ScenarioId).ToList();
                if (questions.Count > 0)
                {
                    foreach (var item in questions)
                    {
                        _unitOfWork.TlsScenarioQuestionRepository.Delete(item.Id);
                    }
                }
                var priority = 0;
                foreach (var item in input.QuestionIds)
                {
                    _unitOfWork.TlsScenarioQuestionRepository.Insert(new TlsScenarioQuestion { ScenarioId = input.ScenarioId, QuestionId = item, Priority = priority });
                    priority++;
                }
                _unitOfWork.Save();
                output.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
            }
            catch (System.Exception ex)
            {
                string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
                string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                Log.Error(ex, $"{controllerName}/{actionName}");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output.data = false;
            }
            return output;
        }
    }
}