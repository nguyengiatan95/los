﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class TokenSmartDailerController : Controller
    {
        private IUnitOfWork unitOfWork;

        public TokenSmartDailerController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("get_last_token")]
        public ActionResult<DefaultResponse<Meta, TokenSmartDailerDetail>> GetLastToken(int userId)
        {
            var def = new DefaultResponse<Meta, TokenSmartDailerDetail>();
            try
            {
                var data = unitOfWork.TokenSmartDailerRepository.Query(x => x.UserId == userId, null, false).OrderByDescending(x=>x.Id).Select(TokenSmartDailerDetail.ProjectionDetail).FirstOrDefault();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "TokenSmartDailer/GetLastToken Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add")]
        public ActionResult<DefaultResponse<object>> AddToken([FromBody] TokenSmartDailer entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var result = unitOfWork.TokenSmartDailerRepository.Insert(entity);
                unitOfWork.Save();
                if (result.Id > 0)
                {
                    def.data = result.Id;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "TokenSmartDailer/AddToken Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

    }
}