﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
using LOS.Common.Utils;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class UserMMOController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        private IConfiguration configuration;

        public UserMMOController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            this.configuration = configuration;
        }

        [HttpGet]
        //[Authorize]
        //[Route("{GetAllUsers}")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserMMODetail>>> GetAllUsers([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int skip = -1, [FromQuery] int take = 10, [FromQuery] string name = null, [FromQuery] int isActive = -1, [FromQuery] string sortBy = "UserId", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<UserMMODetail>>();
            try
            {
                var query = unitOfWork.UserMMORepository.Query(x => (x.FullName.Contains(name) || name == null), null, false).Select(UserMMODetail.ProjectionDetail);
                if (isActive == 1)
                {
                    query = query.Where(x => x.IsActive == true);
                }
                if (isActive == 0)
                {
                    query = query.Where(x => x.IsActive == false);
                }

                var totalRecords = query.Count();
                List<UserMMODetail> data;
                if (skip >= 0)
                    data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                    data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //data = query.OrderBy(sortBy + " " + sortOrder).Skip(page - 1).Take(take).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UsersMMO/GetAllUsers Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserMMODetail>>> GetById(int id)
        {
            var def = new DefaultResponse<Meta, UserMMODetail>();
            try
            {
                var query = unitOfWork.UserMMORepository.Query(x => x.UserId == id
                , null, false).Select(UserMMODetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserMmo/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("getbyguid")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserMMODetail>>> GetByGuid(string guid)
        {
            var def = new DefaultResponse<Meta, UserMMODetail>();
            try
            {
                var query = unitOfWork.UserMMORepository.Query(x => x.Giud.ToLower() == guid.Trim().ToLower()
                , null, false).Select(UserMMODetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserMMO/GetByGuid Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("getuser")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserMMODetail>>> GetByUserName(string username)
        {
            var def = new DefaultResponse<Meta, UserMMODetail>();
            try
            {
                var query = unitOfWork.UserMMORepository.Query(x => x.Email.ToLower() == username.Trim().ToLower()
                , null, false).Select(UserMMODetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserMMO/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("GetByPhoneUser")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserMMODetail>>> GetByPhoneUser(string phone)
        {
            var def = new DefaultResponse<Meta, UserMMODetail>();
            try
            {
                var query = unitOfWork.UserMMORepository.Query(x => x.Phone.ToLower() == phone.Trim().ToLower()
                , null, false).Select(UserMMODetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        //[Authorize]
        public ActionResult<DefaultResponse<object>> PostUser([FromBody] UserMMODTO user)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var result = unitOfWork.UserMMORepository.Insert(user.Mapping());
                unitOfWork.Save();
                if (result.UserId > 0)
                {
                    def.data = result.UserId;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UsersMMO/PostUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPut]
        [Route("{id}")]
        public ActionResult<DefaultResponse<object>> PutUser(int id, [FromBody]UserMMODTO entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != entity.UserId)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var entityOld = unitOfWork.UserMMORepository.Query(x => x.UserId == entity.UserId, null,
                    false).Select(UserMMODetail.ProjectionDetail).FirstOrDefault().Mapping();
                Ultility.CopyObject(entity.Mapping(), ref entityOld, true);
                unitOfWork.UserMMORepository.Update(entityOld);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserMMO/Put Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        public ActionResult<DefaultResponse<object>> DeleteUser(int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                unitOfWork.UserMMORepository.Delete(id);
                unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UsersMMO/DeleteUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public ActionResult<DefaultResponse<UserDetail>> Login([FromBody]UserMMODTO req)
        {
            var def = new DefaultResponse<UserMMODetail>();
            try
            {
                req.Password = req.Password.ToLower();
                var data = unitOfWork.UserMMORepository.Query(x => x.Email == req.Email
                                        && x.Password == req.Password, null, false)
                                        .Include(x => x.UserId).Select(UserMMODetail.ProjectionDetail).FirstOrDefault();

                if (data != null)
                {
                    #region insert into log

                    #endregion
                    #region generate token
                    var claims = new[]{
                                                    new Claim(ClaimTypes.NameIdentifier, data.UserId.ToString()),
                                                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                                    new Claim("UserId", data.UserId.ToString()),
                                                      new Claim("Email", data.Email)
                                                };

                    var token = new JwtSecurityToken
                    (
                        issuer: configuration["JWT:Issuer"],
                        audience: configuration["JWT:Audience"],
                        claims: claims,
                        expires: DateTime.UtcNow.AddDays(90),
                        notBefore: DateTime.UtcNow,
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:SigningKey"])),
                                SecurityAlgorithms.HmacSha256)
                    );

                    data.Token = new JwtSecurityTokenHandler().WriteToken(token);
                    #endregion
                    def.data = data;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }

                else
                {
                    if (unitOfWork.UserMMORepository.Any(x => x.Email == req.Email))
                    {
                        def.meta = new Meta(ResponseHelper.USER_WRONG_PASSWORD_CODE, ResponseHelper.USER_WRONG_PASSWORD_MESSAGE);
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    }
                }
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserMMO/Login exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return Ok(def);
            }
        }
    }
}