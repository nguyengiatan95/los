﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
using LOS.Common.Utils;
using Microsoft.EntityFrameworkCore;
using LOS.DAL.EntityFramework;

namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class UserReferenceController : ControllerBase
    {
        private IUnitOfWork unitOfWork;
        private IConfiguration configuration;

        public UserReferenceController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this.unitOfWork = unitOfWork;
            this.configuration = configuration;
        }
        [HttpGet]
        [Route("{id}")]
        public ActionResult<DefaultResponse<Meta, UserReferenceDetail>> GetById(int id)
        {
            var def = new DefaultResponse<Meta, UserReferenceDetail>();
            try
            {
                var query = unitOfWork.UserReferenceRepository.Query(x => x.UserReferenceId == id, null,
                    false).Select(UserReferenceDetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserReference/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("GetListByUserId")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserReference>>> GetListByUserId(int Id)
        {
            var def = new DefaultResponse<Meta, List<UserReference>>();
            try
            {
                var query = unitOfWork.UserReferenceRepository.Query(x => x.UserId == Id, null, false).ToList();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserReference/GetListByUserId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        //[Authorize]
        public ActionResult<DefaultResponse<object>> PostUser([FromBody] UserReferenceDTO user)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var result = unitOfWork.UserReferenceRepository.Insert(user.Mapping());
                unitOfWork.Save();
                if (result.Id > 0)
                {
                    def.data = result.Id;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserReference/PostUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("GetByLinksUser")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserReferenceDetail>>> GetByLinksUser([FromQuery] int page = 1, [FromQuery] int pageSize = 20, [FromQuery] int isActive = -1, [FromQuery] string name = null, [FromQuery] int UserId = -1, [FromQuery] string sortBy = "UserId", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<UserReferenceDetail>>();
            try
            {
                var query = unitOfWork.UserReferenceRepository.Query(x => x.UserReferenceId == UserId 
                && (name == null || x.User.FullName.Contains(name))
                , null, false)
                    .Select(UserReferenceDetail.ProjectionDetail);
                if (isActive != -1)
                    query = query.Where(x => x.UserMMO.IsActive == (isActive == 1));
                var totalRecords = query.Count();
                List<UserReferenceDetail> data;
                var skip = (page - 1) * pageSize;
                data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UserReference/GetByLinksUser Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }
    }
}