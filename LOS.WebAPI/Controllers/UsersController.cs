﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.Common.Utils;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using LOS.WebAPI.Models.Request;
using LOS.WebAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;


namespace LOS.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;

        public UsersController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._configuration = configuration;
        }

        [HttpGet]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetAllUsers([FromQuery] int page = 1, [FromQuery] int pageSize = 10, [FromQuery] int groupId = -1, [FromQuery] int status = -1, [FromQuery] string name = null, [FromQuery] string sortBy = "UserId", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<UserDetail>>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => x.IsDeleted != true
                && (x.GroupId == groupId || groupId == -1)
                && (x.Status == status || status == -1)
                && (name == null || x.Username.ToLower().Contains(name.ToLower()) || x.FullName.ToLower().Contains(name.ToLower()))
                , null, false, x => x.Company, x => x.Group).Select(UserDetail.ProjectionDetail);
                var totalRecords = query.Count();
                List<UserDetail> data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Users/GetAllUsers Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetById(int id)
        {
            var def = new DefaultResponse<Meta, UserDetail>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => x.UserId == id
                , null, false, x => x.Company).Select(UserDetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("getuser")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetByUserName(string username)
        {
            var def = new DefaultResponse<Meta, UserDetail>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => x.Username.ToLower() == username.Trim().ToLower()
                , null, false, x => x.Company).Select(UserDetail.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        public ActionResult<DefaultResponse<object>> PostUser([FromBody] UserDTO user)
        {
            var def = new DefaultResponse<object>();
            try
            {
                var result = _unitOfWork.UserRepository.Insert(user.Mapping());
                _unitOfWork.Save();
                //insert Group Module
                if (user.ListUserModule != null && user.ListUserModule.Count > 0)
                {
                    var lstUserModule = user.ListUserModule.Where(x => x.ModuleId > 0).ToList();
                    lstUserModule.ForEach(x => x.UserId = result.UserId);
                    _unitOfWork.UserModuleRepository.Insert(lstUserModule);
                }
                //them moi UserShop
                if (user.HubIds != null && user.HubIds.Count > 0)
                {
                    foreach (var item in user.HubIds)
                    {
                        _unitOfWork.UserShopRepository.Insert(new DAL.EntityFramework.UserShop()
                        {
                            UserId = result.UserId,
                            ShopId = item,
                            CreatedAt = DateTime.Now,
                            Status = true
                        });
                    }
                }

                //thêm mới userteamtelesales
                if (user.TeamTelesalesId > 0)
                {
                    _unitOfWork.UserTeamTelesalesRepository.Insert(new DAL.EntityFramework.UserTeamTelesales()
                    {
                        UserId = result.UserId,
                        TeamTelesalesId = user.TeamTelesalesId.Value
                    });
                }

                if (result.UserId > 0)
                {
                    def.data = result.UserId;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                }
                else
                {
                    def.data = 0;
                    def.meta = new Meta(ResponseHelper.FAIL_CODE, ResponseHelper.FAIL_MESSAGE);
                }
                _unitOfWork.Save();
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Users/CreateUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPut]
        [Route("{id}")]
        public ActionResult<DefaultResponse<object>> PutUser(int id, [FromBody] UserDTO user)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (id != user.UserId)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                var lstUserModule = user.ListUserModule;
                var entityOld = _unitOfWork.UserRepository.GetById(user.UserId);
                Ultility.CopyObject(user.Mapping(), ref entityOld, true);
                entityOld.UpdatedTime = DateTime.Now;
                entityOld.Ipphone = user.Ipphone;
                _unitOfWork.BeginTransaction();
                //cập nhật User
                _unitOfWork.UserRepository.Update(entityOld);
                //xóa toàn bộ dữ liệu cũ User-Module
                _unitOfWork.UserModuleRepository.Delete(x => x.UserId == user.UserId && x.ApplicationId.GetValueOrDefault((int)EnumApplication.WebLOS) == (int)EnumApplication.WebLOS);
                //Thêm dữ liệu mới User-Module
                _unitOfWork.UserModuleRepository.Insert(lstUserModule);

                //xóa toàn bộ usershop cũ
                _unitOfWork.UserShopRepository.Delete(x => x.UserId == id);
                //them moi
                if (user.HubIds != null && user.HubIds.Count > 0)
                {
                    foreach (var item in user.HubIds)
                    {
                        _unitOfWork.UserShopRepository.Insert(new DAL.EntityFramework.UserShop()
                        {
                            UserId = id,
                            ShopId = item,
                            CreatedAt = DateTime.Now,
                            Status = true
                        });
                    }
                }

                //thêm mới userteamtelesales
                if (user.TeamTelesalesId > 0)
                {
                    //xóa toàn bộ userteamtelesales cũ
                    _unitOfWork.UserTeamTelesalesRepository.Delete(x => x.UserId == id);
                    _unitOfWork.UserTeamTelesalesRepository.Insert(new DAL.EntityFramework.UserTeamTelesales()
                    {
                        UserId = id,
                        TeamTelesalesId = user.TeamTelesalesId.Value
                    });

                    //chuyển đơn của các bạn sang team mới
                    _unitOfWork.LoanBriefRepository.Update(x => x.BoundTelesaleId == id
                    && (x.Status == (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN || x.Status == (int)EnumLoanStatus.TELESALE_ADVICE), x => new LoanBrief()
                    {
                        TeamTelesalesId = user.TeamTelesalesId.Value
                    });
                }
                _unitOfWork.Save();
                _unitOfWork.CommitTransaction();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "Users/PutUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpDelete]
        [Route("{id}")]
        public ActionResult<DefaultResponse<object>> DeleteUser(int id)
        {
            var def = new DefaultResponse<object>();
            try
            {
                _unitOfWork.UserRepository.SoftDelete(id);
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Users/DeleteUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_staff_hub")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetStaffOfHub(int hubId)
        {
            var def = new DefaultResponse<Meta, List<UserDetail>>();
            try
            {
                var query = _unitOfWork.UserShopRepository.Query(x => x.ShopId == hubId
                , null, false, x => x.User).Where(x => x.User.GroupId == (int)EnumGroupUser.StaffHub && x.User.Status == 1 && x.User.IsDeleted != true).Select(x => new UserDetail
                {
                    UserId = x.User.UserId,
                    FullName = x.User.FullName,
                    Username = x.User.Username,
                    Email = x.User.Email,
                    Phone = x.User.Phone,
                    GroupId = x.User.GroupId,
                    Status = x.User.Status,
                    Ipphone = x.User.Ipphone
                }).ToList();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetStaffOfHub Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_manager_hub")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetManagerOfHub(int hubId)
        {
            var def = new DefaultResponse<Meta, List<UserDetail>>();
            try
            {
                var query = _unitOfWork.UserShopRepository.Query(x => x.ShopId == hubId && x.Status == true
                , null, false, x => x.User).Where(x => x.User.GroupId == (int)EnumGroupUser.ManagerHub && x.User.Status == 1 && x.User.IsDeleted != true).Select(x => new UserDetail
                {
                    UserId = x.User.UserId,
                    FullName = x.User.FullName,
                    Username = x.User.Username,
                    Email = x.User.Email,
                    Phone = x.User.Phone,
                    GroupId = x.User.GroupId,
                    Status = x.User.Status,
                    Ipphone = x.User.Ipphone
                }).ToList();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetManagerOfHub Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_user_permission")]
        public ActionResult<DefaultResponse<SummaryMeta, UserDetail>> GetPermissionUser(int userId)
        {
            var def = new DefaultResponse<Meta, UserDetail>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => x.UserId == userId, null, false, x => x.Group).Select(UserDetail.ProjectionPermissionDetail);
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query.FirstOrDefault();
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetPermissionUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("update_permission")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> UpdatePermissionUser(PermissionUserDTO entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                //xóa dữ liệu cũ
                if (_unitOfWork.UserConditionRepository.Any(x => x.UserId == entity.UserId))
                    _unitOfWork.UserConditionRepository.Delete(x => x.UserId == entity.UserId);
                if (_unitOfWork.UserActionPermissionRepository.Any(x => x.UserId == entity.UserId))
                    _unitOfWork.UserActionPermissionRepository.Delete(x => x.UserId == entity.UserId);
                //insert dữ liệu mới
                if (entity.UserConditions != null && entity.UserConditions.Count > 0)
                {
                    foreach (var item in entity.UserConditions)
                        _unitOfWork.UserConditionRepository.Insert(new LOS.DAL.EntityFramework.UserCondition()
                        {
                            UserId = entity.UserId,
                            PropertyId = item.PropertyId,
                            Operator = item.Operator,
                            CreatetAt = DateTime.Now,
                            CreatedBy = item.CreatedBy,
                            Value = item.Value
                        });
                }

                if (entity.UserActionPermissions != null && entity.UserActionPermissions.Count > 0)
                {
                    foreach (var item in entity.UserActionPermissions)
                        _unitOfWork.UserActionPermissionRepository.Insert(new LOS.DAL.EntityFramework.UserActionPermission()
                        {
                            UserId = entity.UserId,
                            CreatedBy = item.CreatedBy,
                            CreatedAt = DateTime.Now,
                            LoanStatus = item.LoanStatus,
                            Value = item.Value
                        });
                }

                _unitOfWork.Save();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = entity.UserId;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/UpdatePermissionUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("permission")]
        public ActionResult<DefaultResponse<object>> IsPermission(int userId, int moduleId)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (_unitOfWork.UserModuleRepository.Any(x => x.UserId == userId && x.ModuleId == moduleId))
                {
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    return Ok(def);
                }
                else
                {
                    //neu k cos caaus hinfh 
                    if (!_unitOfWork.UserModuleRepository.Any(x => x.UserId == userId))
                    {
                        //kiểm tra quyền của group
                        var user = _unitOfWork.UserRepository.GetById(userId);

                    }
                }

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                _unitOfWork.RollbackTransaction();
                Log.Error(ex, "Users/PutUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_password")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserPassword>>> GetPassword(int userId)
        {
            var def = new DefaultResponse<Meta, UserPassword>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => x.UserId == userId
                , null, false, x => x.Company).Select(UserPassword.ProjectionDetail).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetById Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("getlistaccountoftima")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetListAccountOfTima([FromQuery] int page = 1, [FromQuery] int pageSize = 50, [FromQuery] int groupId = -1, [FromQuery] int status = -1, [FromQuery] string name = null, [FromQuery] string sortBy = "UserId", [FromQuery] string sortOrder = "DESC")
        {
            var def = new DefaultResponse<SummaryMeta, List<UserDetail>>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => x.IsDeleted != true
                && (x.GroupId == groupId || groupId == -1)
                && (x.Status == status || status == -1)
                && (x.GroupId != null)
                && (x.GroupId != 1)
                && (name == null || x.Username.ToLower().Contains(name.ToLower()) || x.FullName.ToLower().Contains(name.ToLower()))
                , null, false, x => x.Company, x => x.Group).Select(UserDetail.ProjectionDetail);
                var totalRecords = query.Count();
                List<UserDetail> data = query.OrderBy(sortBy + " " + sortOrder).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Users/GetAllUsers Exception");
                def.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("get_list_user_by_ipphone")]
        public ActionResult<DefaultResponse<Meta, List<UserDetail>>> GetListUserByIpPhone([FromBody] GetUserReq req)
        {
            var def = new DefaultResponse<Meta, List<UserDetail>>();
            try
            {
                //Ipphone
                if (req.Type == 1)
                {
                    var query = _unitOfWork.UserRepository.Query(x => req.lstIpphone.Contains(x.Ipphone.Value), null, false).Select(UserDetail.ProjectionDetail).ToList();
                    if (query != null)
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    else
                        def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                    def.data = query;
                    return Ok(def);
                }
                else if (req.Type == 2)
                {
                    var query = _unitOfWork.UserRepository.Query(x => req.lstIpphone.Contains(Convert.ToInt32(x.CiscoExtension)), null, false).Select(UserDetail.ProjectionDetail).ToList();
                    if (query != null)
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    else
                        def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                    def.data = query;
                    return Ok(def);
                }
                else
                {//Cisco mobile
                    var dicUserMobile = _unitOfWork.UserMobileCiscoRepository.Query(x =>
                        req.lstIpphone.Contains(Convert.ToInt32(x.MbCiscoExtension)), null, false).ToDictionary(x => x.UserId, x => x.MbCiscoExtension);
                    var listData = _unitOfWork.UserRepository.Query(x => dicUserMobile.Keys.Contains(x.UserId), null, false).Select(UserDetail.ProjectionDetail).ToList();
                    if (listData != null && listData.Count > 0)
                    {
                        foreach (var item in listData)
                        {
                            if (dicUserMobile.ContainsKey(item.UserId))
                                item.CiscoExtension = dicUserMobile[item.UserId];
                        }
                        def.data = listData;
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    }
                    else
                        def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);

                    return Ok(def);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Users/GetListUserByIpPhone Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("update_password")]
        public ActionResult<DefaultResponse<object>> UpdatePassword(int id, [FromBody] UserDTO user)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (user.UserId == 0)
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                _unitOfWork.UserRepository.Update(x => x.UserId == user.UserId, x => new DAL.EntityFramework.User()
                {
                    Password = user.Password,
                    UpdatedTime = DateTime.Now
                });
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Users/UpdatePassword Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpGet]
        [Route("get_staff_hub_loan_asc")]
        public ActionResult<DefaultResponse<SummaryMeta, UserDetail>> GetStaffOfHubLoanAsc(int hubId)
        {
            var def = new DefaultResponse<Meta, UserDetail>();
            try
            {
                var query = _unitOfWork.UserShopRepository.Query(x => x.ShopId == hubId
                , null, false, x => x.User).Where(x => x.User.GroupId == (int)EnumGroupUser.StaffHub && x.User.Status == 1 && x.User.IsDeleted != true)
                .OrderBy(k => k.User.LoanBriefHubEmployee.Count(k => k.Status == (int)EnumLoanStatus.APPRAISER_REVIEW)).Select(x => new UserDetail
                {
                    UserId = x.User.UserId,
                    FullName = x.User.FullName,
                    Username = x.User.Username,
                    Email = x.User.Email,
                    Phone = x.User.Phone,
                    GroupId = x.User.GroupId,
                    Status = x.User.Status,
                    Ipphone = x.User.Ipphone
                }).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetStaffOfHubLoanAsc Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("chang_information")]
        public ActionResult<DefaultResponse<SummaryMeta, object>> ChangeInformation(User entity)
        {
            var def = new DefaultResponse<Meta, object>();
            try
            {
                _unitOfWork.UserRepository.Update(x => x.UserId == entity.UserId, x => new User()
                {
                    FullName = entity.FullName,
                    Phone = entity.Phone,
                    Email = entity.Email,
                    TypeCallService = entity.TypeCallService,
                    Ipphone = entity.Ipphone,
                    CiscoUsername = entity.CiscoUsername,
                    CiscoPassword = entity.CiscoPassword,
                    CiscoExtension = entity.CiscoExtension
                });
                _unitOfWork.Save();
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/ChangeInformation Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("staff_hub_loan_asc")]
        public ActionResult<DefaultResponse<SummaryMeta, UserDetail>> GetStaffOfHubLoanAsc(HubEmployeeReq entity)
        {
            var def = new DefaultResponse<Meta, UserDetail>();
            try
            {
                List<int>? hubEmployees = null;
                if (entity.userIds != null && entity.userIds.Count > 0)
                    hubEmployees = entity.userIds;
                var query = _unitOfWork.UserShopRepository.Query(x => x.ShopId == entity.hubId && (hubEmployees == null || hubEmployees.Contains(x.UserId.Value))
                , null, false, x => x.User).Where(x => x.User.GroupId == (int)EnumGroupUser.StaffHub && x.User.Status == 1 && x.User.IsDeleted != true)
                .OrderBy(k => k.User.LoanBriefHubEmployee.Count(k => k.Status == (int)EnumLoanStatus.APPRAISER_REVIEW)).Select(x => new UserDetail
                {
                    UserId = x.User.UserId,
                    FullName = x.User.FullName,
                    Username = x.User.Username,
                    Email = x.User.Email,
                    Phone = x.User.Phone,
                    GroupId = x.User.GroupId,
                    Status = x.User.Status,
                    Ipphone = x.User.Ipphone
                }).FirstOrDefault();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetStaffOfHubLoanAsc Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("get_field_ho")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetFieldHo(int provinceId)
        {
            var def = new DefaultResponse<Meta, List<UserDetail>>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => x.Status == 1 && x.IsDeleted != true && x.GroupId == (int)EnumGroupUser.FieldSurvey
                && (x.CityId == provinceId)
                , null, false).Select(x => new UserDetail
                {
                    UserId = x.UserId,
                    FullName = x.FullName,
                    Username = x.Username,
                    Email = x.Email,
                    Phone = x.Phone,
                    GroupId = x.GroupId,
                    Status = x.Status,
                    Ipphone = x.Ipphone
                }).ToList();
                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetFieldHo Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_multi_user")]
        public ActionResult<DefaultResponse<SummaryMeta, List<UserDetail>>> GetMultiUser(int group1, int group2)
        {
            var def = new DefaultResponse<Meta, List<UserDetail>>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => x.IsDeleted != true && x.Status == 1 && (x.GroupId == group1 || x.GroupId == group2), null, false).Select(UserDetail.ProjectionDetail).ToList();

                if (query != null)
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                else
                    def.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                def.data = query;
                return Ok(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "User/GetMultiUser Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
        [HttpGet]
        [Route("get_user_by_list_userid")]
        public ActionResult<DefaultResponse<Meta, List<UserGroup>>> GetUserByListUserId(string userId)
        {
            var def = new DefaultResponse<Meta, List<UserGroup>>();
            try
            {
                if (!string.IsNullOrEmpty(userId))
                {
                    var lstUserId = userId.Split(',').Select(Int32.Parse).ToList();
                    var lstData = _unitOfWork.UserRepository.Query(x => lstUserId.Contains(x.UserId) && x.Status == (int)EnumStatusBase.Active, null, false).Select(UserGroup.ProjectionDetail).ToList();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = lstData;
                }
                else
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Users/GetUserByListUserId Exception");
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
