﻿using LOS.Common.Models.Response;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebAPI.Helpers
{
    public class CustomResponseMiddleware
    {
        private readonly RequestDelegate _next;
        public CustomResponseMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext context)
        {
            var statusCodeFeature = new StatusCodePagesFeature();
            context.Features.Set<IStatusCodePagesFeature>(statusCodeFeature);

            await _next(context);

            if (!statusCodeFeature.Enabled)
            {
                // Check if the feature is still available because other middleware (such as a web API written in MVC) could
                // have disabled the feature to prevent HTML status code responses from showing up to an API client.
                return;
            }

            // Do nothing if a response body has already been provided.
            if (context.Response.HasStarted
                || context.Response.StatusCode < 400
                || context.Response.StatusCode >= 600
                || context.Response.ContentLength.HasValue
                || !string.IsNullOrEmpty(context.Response.ContentType))
            {
                return;
            }
            var message = "Error";
            if (context.Response.StatusCode == StatusCodes.Status404NotFound)
                message = "Not Found";
            if (context.Response.StatusCode == 401)
                message = "Unauthorized";
            if (context.Response.StatusCode == 500)
                message = "Server Internal Error";
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(new DefaultResponse<Meta>() { meta = new Meta(context.Response.StatusCode, message) });
            await context.Response.WriteAsync(json);
        }
    }
}
