﻿using LOS.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace LOS.WebAPI.Helpers
{
    public class ExpressionBuilder
    {
        public static Expression<Func<T, bool>> Build<T>(string propertyName, EnumOperationType operationType,
           object value)
        {
            //Create parameter expression "o"
            var parameter = Expression.Parameter(typeof(T), "o");

            //Search the member- field or property. You can also pass it BindingFlags.IgnoreCase to do case insensitive search.
            var memberInfo = typeof(T).GetMember(propertyName, MemberTypes.Property | MemberTypes.Field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).FirstOrDefault();
            if (memberInfo == null)
            {
                throw new ArgumentException(string.Format("Can not find the property or field by name {0} on type {1}", propertyName, typeof(T).Name));
            }
            //Create o.MemberName expression
            var property = Expression.MakeMemberAccess(parameter, memberInfo);

            var targetType = GetTargetType(memberInfo);

            targetType = Nullable.GetUnderlyingType(targetType) ?? targetType;

            //Change the provided _value to target type.
            var targetValue = Convert.ChangeType(value, targetType);

            //Create expression for targetValue
            var valueExpression = Expression.Constant(targetValue);

            //Combin property and value expression using operation type e.g.  o.PropertyName >= targetValue
            //var filterExpression = CombineExpression(property, valueExpression, operationType);
            var filterExpression = CombineExpression(property, Expression.Convert(valueExpression, property.Type) , operationType);

            //Generate lamda expression e.g. o=>o.PropertyName >= targetValue
            return Expression.Lambda<Func<T, bool>>(filterExpression, parameter);
        }

        private static Type GetTargetType(MemberInfo memberInfo)
        {
            if (memberInfo.MemberType == MemberTypes.Field)
            {
                return ((FieldInfo)memberInfo).FieldType;
            }
            if (memberInfo.MemberType == MemberTypes.Property)
            {
                return ((PropertyInfo)memberInfo).PropertyType;
            }

            throw new NotSupportedException(string.Format("Does not support the member type {0}", memberInfo.MemberType));
        }

        private static Expression CombineExpression(Expression left, Expression right, EnumOperationType operationType)
        {
            switch (operationType)
            {
                case EnumOperationType.EqualTo:
                    return Expression.Equal(left, right);
                case EnumOperationType.NotEqualTo:
                    return Expression.NotEqual(left, right);
                case EnumOperationType.GreaterThan:
                    return Expression.GreaterThan(left, right);
                case EnumOperationType.GreaterThanEqualTo:
                    return Expression.GreaterThanOrEqual(left, right);
                case EnumOperationType.LessThan:
                    return Expression.LessThan(left, right);
                case EnumOperationType.LessThanEqualTo:
                    return Expression.LessThanOrEqual(left, right);
                case EnumOperationType.Contains:
                    return CallMethodOnString(left, "Contains", right);
                case EnumOperationType.StartsWith:
                    return CallMethodOnString(left, "StartsWith", right);
                case EnumOperationType.EndsWith:
                    return CallMethodOnString(left, "EndsWith", right);
            }
            throw new NotSupportedException();
        }

        private static Expression CallMethodOnString(Expression instance, string methodName, Expression parameter)
        {
            var method = typeof(string).GetMethods().Single(m => m.Name == methodName && m.GetParameters().Count() == 1);
            return Expression.Call(instance, method, parameter);
        }
    }
}
