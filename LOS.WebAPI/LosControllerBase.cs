﻿using LOS.Common.Helpers;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.Respository;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LOS.WebAPI
{
    //TODO  Uncomment this one
    //[Authorize]
    public class LosControllerBase<Entity, ApiPagingModel> : ControllerBase where Entity : class where ApiPagingModel : ApiPagingPostModel
    {
        private IGenericRepository<Entity> _repository;
        private IUnitOfWork _unitOfWork;
        public LosControllerBase(IUnitOfWork unitOfWork, IGenericRepository<Entity> repository)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        [HttpGet]
        [Route("{id}")]
        public virtual async Task<DefaultResponse<Entity>> GetByIdAsync(int id)
        {
            var output = new DefaultResponse<Entity>() { meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE), data = null };
            try
            {
                var data = _repository.GetById(id);
                if (data == null)
                {
                    output.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, ResponseHelper.NOT_FOUND_MESSAGE);
                    output.data = null;
                }
                output.data = data;
            }
            catch (Exception ex)
            {
                string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
                string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                Log.Error(ex, $"{controllerName}/{actionName}");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output.data = null;
            }
            return output;
        }


        [HttpPatch]
        [Route("UpdateAsync")]
        public virtual async Task<DefaultResponse<bool>> UpdateAsync(Entity entity)
        {
            var output = new DefaultResponse<bool>();
            try
            {
                _repository.Update(entity);
                _unitOfWork.Save();
                output.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                output.data = true;
            }
            catch (System.Exception ex)
            {
                string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
                string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                Log.Error(ex, $"{controllerName}/{actionName}");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output.data = false;
            }
            return output;
        }


        [HttpPost]
        [Route("InsertAsync")]
        public virtual async Task<DefaultResponse<Entity>> InsertAsync(Entity entity)
        {
            var output = new DefaultResponse<Entity>() { meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE) };
            try
            {
                output.data = _repository.Insert(entity);
                _unitOfWork.Save();
            }
            catch (System.Exception ex)
            {
                string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
                string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                Log.Error(ex, $"{controllerName}/{actionName}");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output = null;
            }
            return output;
        }


        [HttpDelete]
        [Route("{id}")]
        public virtual async Task<DefaultResponse<bool>> DeleteByIdAsync(int id)
        {
            var output = new DefaultResponse<bool>() { data = true, meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE) };
            try
            {
                _repository.Delete(id);
                _unitOfWork.Save();
            }
            catch (System.Exception ex)
            {
                string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
                string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                Log.Error(ex, $"{controllerName}/{actionName}");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output.data = false;
            }
            return output;
        }

        [HttpGet]
        [Route("GetAll")]
        public virtual async Task<DefaultResponse<SummaryMeta, IEnumerable<Entity>>> GetAll([FromQuery]ApiPagingModel input)
        {
            var output = new DefaultResponse<SummaryMeta, IEnumerable<Entity>>();
            try
            {
                var query = _repository.All();
                var condition = await GetAllCondition(input);
                if (condition != null)
                {
                    query = query.Where(condition);
                }
                var pagedQuery = ApplyPagingAndSorting<Entity>(query, input);
                var totalRecords = query.Count();
                output.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, input.CurrentPage, input.PageSize, totalRecords);
                output.data = pagedQuery.ToList();
            }
            catch (Exception ex)
            {
                string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
                string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                Log.Error(ex, $"{controllerName}/{actionName}");
                output.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
            }
            return output;
        }

        public virtual async Task<Expression<Func<Entity, bool>>> GetAllCondition(ApiPagingModel input)
        {
            return null;
        }
        protected IQueryable<T> ApplyPagingAndSorting<T>(IQueryable<T> query, ApiPagingPostModel pagingModel)
        {
            var propertyInfo = typeof(T).GetProperty(pagingModel.SortColumn);
            if (propertyInfo == null)
            {
                if (typeof(T).GetProperties().Count() > 0)
                {
                    propertyInfo = typeof(T).GetProperties()[0];
                }
            }
            if (propertyInfo != null)
            {
                if (pagingModel.IsSortByAsc)
                    query = query.OrderBy(x => propertyInfo.GetValue(x, null));
                else
                    query = query.OrderByDescending(x => propertyInfo.GetValue(x, null));
            }

            query = query.Skip((pagingModel.CurrentPage - 1) * pagingModel.PageSize).Take(pagingModel.PageSize);
            return query;
        }

    }
}
