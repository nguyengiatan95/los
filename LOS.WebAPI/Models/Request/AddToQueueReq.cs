﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebAPI.Models.Request
{
	public class AddToQueueReq
	{
		public int id { get; set; }
		public int state { get; set; }
		public int pushBy { get; set; }
	}
}
