﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.WebAPI.Models.Request
{
	public class ChangePipelineReq
	{
		public int loanBriefId { get; set; }
		public int productId { get; set; }
		public int status { get; set; }
	}
}
