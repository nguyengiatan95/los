﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebAPI.Models
{
    public class CreateLoanBriefChild
    {
        public int LoanBriefId { get; set; }
        public long LoanAmount { get; set; }
    }
}
