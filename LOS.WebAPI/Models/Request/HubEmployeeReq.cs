﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebAPI.Models.Request
{
    public class HubEmployeeReq
    {
        public int hubId { get; set; }
        public List<int>? userIds { get; set; }
    }
}
