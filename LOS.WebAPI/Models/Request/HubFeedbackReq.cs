﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebAPI.Models.Request
{
    public class HubFeedbackReq
    {
        public int LoanBriefId { get; set; }
    }
}
