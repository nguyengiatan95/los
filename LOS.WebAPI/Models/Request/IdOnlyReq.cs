﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOS.WebAPI.Models.Request
{
	class IdOnlyReq
	{
		public int id { get; set; }
		public bool forceRefresh { get; set; }
	}
}
