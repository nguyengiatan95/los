﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebAPI.Models.Request
{
	public class InfoLoginReq
	{
		public int UserId { get; set; }
	}
}
