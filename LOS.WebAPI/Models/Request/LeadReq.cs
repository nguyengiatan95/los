﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebAPI.Models.Request
{
	public class LeadReq
	{
		public string name { get; set; }
		public string phone { get; set; }
		public long loanAmount { get; set; }
		public int loanProduct { get; set; }
		public int loanTime { get; set; }
		public int cityId { get; set; }
		public int districtId { get; set; }
		public string utmSource { get; set; }
		public string utmMedium { get; set; }
		public string utmContent { get; set; }
		public string utmCampaign { get; set; }
		public string utmTerm { get; set; }
	}
}
