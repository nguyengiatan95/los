﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebAPI.Models.Request
{
	public class LoginReq
	{
		public string username { get; set; }
		public string password { get; set; }
	}
}
