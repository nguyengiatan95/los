﻿using System.ComponentModel.DataAnnotations;

namespace LOS.WebAPI.Models.Request.TlsScenarioQuestion
{
    public class CreateMappingQuestionWithScenario_InputModel
    {
        [Required]
        public int ScenarioId { get; set; }

        [Required]
        public int[] QuestionIds { get; set; }
    }
}
