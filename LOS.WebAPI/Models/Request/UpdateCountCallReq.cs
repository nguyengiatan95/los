﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebAPI.Models
{
    public class UpdateCountCallReq
    {
        public int loanbriefId { get; set; }
        public int countCall { get; set; }
    }
}
