﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebAPI.Models.Response
{
	public class LoginResponse
	{
		public int UserId { get; set; }
		public string Username { get; set; }
		public string FullName { get; set; }
	}
}
