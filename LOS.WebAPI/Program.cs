using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Compact;

namespace LOS.WebAPI
{
	public class Program
	{
		public static void Main(string[] args)
		{
			Log.Logger = new LoggerConfiguration()
						.MinimumLevel.Override("Microsoft", LogEventLevel.Fatal)
						.Enrich.FromLogContext()
						.WriteTo.File(new CompactJsonFormatter(), "logs/tima.log", rollOnFileSizeLimit: true, fileSizeLimitBytes: 10_000_000, rollingInterval: RollingInterval.Day)
						.WriteTo.Seq("http://seq:5341")
						.CreateLogger();
			var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
			Console.WriteLine("ASPNETCORE_ENVIRONMENT: " + environmentName);
			Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", environmentName);
			
			CreateWebHostBuilder(args).Build().Run();
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)				
				.UseSetting("detailedErrors", "true")

                .UseKestrel(serverOptions =>
                {
                    serverOptions.Limits.MaxConcurrentConnections = 1000;
                    serverOptions.Limits.MaxConcurrentUpgradedConnections = 1000;
                    serverOptions.Limits.KeepAliveTimeout =
                        TimeSpan.FromMinutes(5);
                    serverOptions.Limits.RequestHeadersTimeout =
                        TimeSpan.FromMinutes(5);
                })
                .UseUrls("http://0.0.0.0:80", "http://0.0.0.0:7777")
                .UseStartup<Startup>();

	}
}
