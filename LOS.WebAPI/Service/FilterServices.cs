﻿using LOS.Common.Extensions;
using LOS.DAL.UnitOfWork;
using LOS.WebAPI.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LOS.WebAPI
{
    public class FilterServices<T>
    {
        private IUnitOfWork _unitOfWork;
        public FilterServices(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public void GetFilter(int userId,ref FilterHelper<T> filter, bool IsCheckStatus = true)
        {
            //lấy thông tin permission user
            if(userId > 0)
            {
                var user = _unitOfWork.UserRepository.GetById(userId);
                if (user != null && user.UserId > 0)
                {
                    var Conditions = _unitOfWork.UserConditionRepository.Query(x => x.UserId == userId, null, false).ToList();
                    //nếu user k có row thì lấy group của user đó
                    if (Conditions == null || Conditions.Count == 0)
                    {
                        var GroupConditions = _unitOfWork.GroupConditionRepository.Query(x => x.GroupId == user.GroupId, null, false).ToList();
                        if (GroupConditions != null && GroupConditions.Count > 0)
                        {
                            Conditions = GroupConditions.Select(x => new DAL.EntityFramework.UserCondition()
                            {
                                PropertyId = x.PropertyId,
                                Operator = x.Operator,
                                Value = x.Value,
                                UserId = userId
                            }).ToList();
                        }
                    }

                    if (Conditions != null && Conditions.Count > 0)
                    {
                        foreach (var item in Conditions)
                        {
                            var property = _unitOfWork.PropertyRepository.GetById(item.PropertyId);
                            if (property != null && !string.IsNullOrEmpty(item.Value))
                            {
                                //nếu input là multipe select : IN or NOT_IN
                                if (property.MultipleSelect == true)
                                {
                                    //Nếu k check trạng thái + property là status => bỏ qua
                                    if (!IsCheckStatus && !string.IsNullOrEmpty(property.FieldName) && property.FieldName.ToUpper() == "STATUS")
                                        continue;
                                    //lấy danh sách giá trị cấu hình
                                    var arrValue = item.Value.Split(",").ToList();
                                    if (arrValue != null && arrValue.Count > 0)
                                    {
                                        //tạo filter với giá trị đầu tiên
                                        FilterHelper<T> filterIn = FilterHelper<T>.Create(property.FieldName, EnumOperationType.EqualTo, arrValue[0]);
                                        if (item.Operator == "NOT_IN")
                                            filterIn = FilterHelper<T>.Create(property.FieldName, EnumOperationType.NotEqualTo, arrValue[0]);
                                        var isFirst = true;
                                        foreach (var value in arrValue)
                                        {
                                            if (item.Operator == "IN" || item.Operator == "NOT_IN")
                                            {
                                                //Nếu là IN  thì bỏ qua giá trị đầu tiên arrValue[0]
                                                if (isFirst)
                                                {
                                                    isFirst = false;
                                                    continue;
                                                }
                                                if (item.Operator == "IN")
                                                    filterIn = filterIn.OrProperty(property.FieldName, EnumOperationType.EqualTo, value);
                                                else
                                                    filter = filter.AndProperty(property.FieldName, EnumOperationType.NotEqualTo, value);
                                            }
                                        }
                                        if (!isFirst)
                                            filter = filter.And(filterIn);
                                    }
                                }
                                else
                                {
                                    filter = filter.AndProperty(property.FieldName, ConverseOperationType(item.Operator.Trim().ToUpper()), item.Value);
                                }
                            }
                        }
                    }
                }
            }
        }

        private EnumOperationType ConverseOperationType(string OperationType)
        {
            switch (OperationType)
            {
                case "EQUAL":
                    return EnumOperationType.EqualTo;
                case "GREATHER_THAN":
                    return EnumOperationType.GreaterThan;
                case "GREATHER_THAN_EQUAL":
                    return EnumOperationType.GreaterThanEqualTo;
                case "LESS_THAN":
                    return EnumOperationType.LessThan;
                case "LESS_THAN_EQUAL":
                    return EnumOperationType.LessThanEqualTo;
                case "IN":
                    return EnumOperationType.Contains;
                case "NOT_IN":
                    return EnumOperationType.NotEqualTo;
            }
            return EnumOperationType.EqualTo;
        }
    }
}
