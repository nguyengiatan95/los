using System.Collections.Generic;
using LOS.WebApp.VueJS.Models;

namespace LOS.WebApp.VueJS.Providers
{
    public interface IWeatherProvider
    {
        List<WeatherForecast> GetForecasts();
    }
}
