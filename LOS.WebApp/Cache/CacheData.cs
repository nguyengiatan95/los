﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Cache
{
    public class CacheData
    {
        public static List<ProvinceDetail> ListProvince;
        public static List<JobDetail> ListJobs;
        public static List<RelativeFamilyDetail> ListRelativeFamilys;
        public static List<BrandProductDetail> ListBrandProduct;
        public static List<LoanProduct> ListLoanProduct;
        public static List<PlatformTypeDetail> ListPlatformType;
        public static List<BankDetail> ListBank;
        public static List<TypeOwnerShip> ListOwnerShip;
        public static List<InfomationProductDetailDTO> ListInfomationProductDetail;
        public static void InitData()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
                .AddJsonFile("appsettings.json", true)
                .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
            var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
            var _dictionaryService = new DictionaryService(configuration, httpClientFactory);

            CacheData.ListProvince = _dictionaryService.GetProvince("");
            CacheData.ListJobs = _dictionaryService.GetJobV2("");
            CacheData.ListRelativeFamilys = _dictionaryService.GetRelativeFamily("");
            CacheData.ListBrandProduct = _dictionaryService.GetBrandProduct("");
            var product = _dictionaryService.GetLoanProduct("");
            if (product != null && product.Count > 0)
                CacheData.ListLoanProduct = product.Where(x => x.Status == 1).ToList();
            CacheData.ListPlatformType = _dictionaryService.GetPlatformType("");
            CacheData.ListBank = _dictionaryService.GetBank("");
            CacheData.ListOwnerShip = _dictionaryService.GetTypeOwnerShip("");
            //CacheData.ListInfomationProductDetail = _dictionaryService.GetAllInfomationProductDetail("");
        }
    }
}
