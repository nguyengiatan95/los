﻿using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Components
{
	public class BreadcrumbViewComponent : ViewComponent
	{
		public async Task<IViewComponentResult> InvokeAsync(string ModuleName)
		{
			ViewBag.ModuleName = ModuleName;
			var model = await HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA");
			return View(model.Group);
		}
	}
}
