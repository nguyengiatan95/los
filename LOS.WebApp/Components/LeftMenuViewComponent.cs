﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Components
{
    public class LeftMenuViewComponent : ViewComponent
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public LeftMenuViewComponent(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
         
            var userData = await HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA");
            var menu = new List<ModuleDetail>();
            var dicMenuChild = new Dictionary<int, List<ModuleDetail>>();
            var modules = new List<ModuleDetail>();
            //xử lý data cho menu
            if (userData.Modules != null && userData.Modules.Count > 0)
                modules = userData.Modules;
            else if (userData.Group != null && userData.Group.Modules != null && userData.Group.Modules.Count > 0)
                modules = userData.Group.Modules;

            if(modules != null && modules.Count > 0)
            {
                var controller = _httpContextAccessor.HttpContext.Request.RouteValues["controller"].ToString();
                var action = _httpContextAccessor.HttpContext.Request.RouteValues["action"].ToString();
                modules = modules.OrderBy(x => x.Priority).ToList();
                foreach (var item in modules)
                {
                    if (item.IsMenu)
                    {
                        if (item.ParentId.GetValueOrDefault(0) == 0)
                            menu.Add(item);
                        else
                        {
                            if (item.Controller.AsToLower() == controller.ToLower() && item.Action.AsToLower() == action.ToLower())
                                item.Selected = true;
                            if (!dicMenuChild.ContainsKey(item.ParentId.Value))
                                dicMenuChild[item.ParentId.Value] = new List<ModuleDetail>();
                            dicMenuChild[item.ParentId.Value].Add(item);
                        }
                    }
                }

                if(menu != null && menu.Count > 0 && dicMenuChild.Keys.Count > 0)
                {
                    foreach(var item in menu)
                    {
                        if (dicMenuChild.ContainsKey(item.ModuleId))
                        {
                            item.Modules = dicMenuChild[item.ModuleId];
                            if (item.Modules.Any(x => x.Controller.AsToLower() == controller.ToLower() && x.Action.AsToLower() == action.ToLower()))
                                item.Selected = true;
                        }
                    }
                }
            }
            
            return View(menu);
        }
    }
}
