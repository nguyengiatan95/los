﻿using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Components
{
	public class PropertyViewComponent : ViewComponent
	{
        private IPipelineService _pipelineService;
        private IHttpClientFactory clientFactory;
        public PropertyViewComponent(IPipelineService pipelineService, IHttpClientFactory clientFactory)
        {
            this._pipelineService = pipelineService;
            this.clientFactory = clientFactory;
        }
        public async Task<IViewComponentResult> InvokeAsync(int PropertyId, object value = null)
		{
            var token = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result.Token;
            var data = _pipelineService.GetPropertyById(token, PropertyId);
            if (data.LinkObject != null)
            {
                // Get Link Object
                Ultility utils = new Ultility(clientFactory);
                var objects = utils.GetLinkObject(data.LinkObject, token);
                ViewBag.LinkObjects = objects;
            }
            if (data.MultipleSelect == true)
            {
                ViewBag.Data = value.ToString().Split(",").ToList();
            }
            else
            {
                if (data.LinkObject == null && Microsoft.VisualBasic.Information.IsNumeric(value))
                {
                    decimal valuedecimal = decimal.Parse(value.ToString(), System.Globalization.NumberStyles.AllowDecimalPoint);
                    ViewBag.Data = valuedecimal.ToString("##,#");
                }
                else
                {
                    ViewBag.Data = value;
                }
                
            }
            return View("~/Views/Shared/Components/Property/GetPropertyPartial.cshtml", data);
        }
	}
}
