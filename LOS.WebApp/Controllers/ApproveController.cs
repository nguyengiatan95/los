﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Models.Response;
using LOS.Services.Services.Approve;
using LOS.WebApp.Cache;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.LMS;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Services;
using LOS.WebApp.Services.AIService;
using LOS.WebApp.Services.ResultEkyc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using static LOS.DAL.Models.Request.LoanBrief.LoanBrief;
using static LOS.WebApp.Models.Compare.Compare;
using static LOS.WebApp.Models.Ekyc.Ekyc;
using static LOS.WebApp.Models.Report;

namespace LOS.WebApp.Controllers
{
    // [AuthorizeFilter]
    public class ApproveController : BaseController
    {
        private IApproveV2Service _approveV2Service;

        private IApproveService approveService;
        private ILoanBriefService loanBriefService;
        private IDictionaryService _dictionaryService;
        private IReasonService _reasonService;
        private readonly IMapper _mapper;
        private IConfiguration _configuration;
        private ILMSService _lmsService;
        private ICompositeViewEngine _viewEngine;
        private ILoanStatusDetailService _loanStatusDetailService;
        private ILogActionService _logActionService;
        private IProductService _productService;
        private readonly IPipelineService _pipelineService;
        private ITrackDeviceService _trackDeviceService;
        private ISecuredTransaction _securedTransaction;

        public ApproveController(IConfiguration configuration, IApproveService services, ILoanBriefService loanService, IDictionaryService dictionaryService, IReasonService reationService, IMapper mapper, ILMSService lmsService, ICompositeViewEngine viewEngine, IHttpClientFactory clientFactory,
            ILoanStatusDetailService loanStatusDetailService, ILogActionService logActionService, IProductService productService,
            IPipelineService pipelineService, ITrackDeviceService trackDeviceService, IApproveV2Service approveV2Service, ISecuredTransaction securedTransaction) : base(configuration: configuration, clientFactory)
        {
            this.approveService = services;
            this.loanBriefService = loanService;
            this._dictionaryService = dictionaryService;
            this._reasonService = reationService;
            this._mapper = mapper;
            this._configuration = configuration;
            _lmsService = lmsService;
            this._viewEngine = viewEngine;
            this._loanStatusDetailService = loanStatusDetailService;
            _logActionService = logActionService;
            this._productService = productService;
            _pipelineService = pipelineService;
            _trackDeviceService = trackDeviceService;

            this._approveV2Service = approveV2Service;
            _securedTransaction = securedTransaction;
        }

        [Route("/approve/leader.html")]
        [PermissionFilter]
        public IActionResult Leader()
        {
            return View();

        }

        [Route("/approve/employee.html")]
        [PermissionFilter]
        public async Task<IActionResult> Employee()
        {
            var user = GetUserGlobal();
            ViewBag.TotalLoanDisbursement = await loanBriefService.TotalLoanBriefDisbursementInMonth(user.Token, user.UserId);
            ViewBag.User = user;
            return View();
        }

        [Route("/approve/disbursementafter.html")]
        [PermissionFilter]

        public IActionResult DisbursementAfter()
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            ViewBag.GroupId = user.GroupId;
            return View();
        }
        [PermissionFilter]
        public IActionResult Search()
        {
            try
            {
                var user = GetUserGlobal();
                var model = new ApproveDatatable(HttpContext.Request.Form);
                //model.userId = user.UserId;
                model.coordinatorUserId = -1;
                int recordsTotal = 0;
                // Query api   	
                var data = approveService.Search(GetToken(), model.ToQueryObject(), ref recordsTotal);
                model.total = recordsTotal.ToString();
                //set permission row
                if (user.ListPermissionAction != null && user.ListPermissionAction.Count > 0)
                {
                    foreach (var item in data)
                    {
                        if (item.Status != EnumLoanStatus.CANCELED.GetHashCode() && user.ListPermissionAction.Any(x => x.LoanStatus == item.Status))
                        {
                            var permission = user.ListPermissionAction.FirstOrDefault(x => x.LoanStatus == item.Status);
                            if (permission != null && permission.Value > 0)
                            {
                                if ((EnumActionUser.Edit.GetHashCode() & permission.Value) == EnumActionUser.Edit.GetHashCode())
                                    item.IsEdit = true;
                                if ((EnumActionUser.Script.GetHashCode() & permission.Value) == EnumActionUser.Script.GetHashCode())
                                    item.IsScript = true;
                                if ((EnumActionUser.PushLoan.GetHashCode() & permission.Value) == EnumActionUser.PushLoan.GetHashCode())
                                    item.IsPush = true;
                                if ((EnumActionUser.Cancel.GetHashCode() & permission.Value) == EnumActionUser.Cancel.GetHashCode())
                                    item.IsCancel = true;
                                if ((EnumActionUser.Upload.GetHashCode() & permission.Value) == EnumActionUser.Upload.GetHashCode())
                                    item.IsUpload = true;
                                if ((EnumActionUser.ComfirmCancel.GetHashCode() & permission.Value) == EnumActionUser.ComfirmCancel.GetHashCode())
                                    item.IsConfirmCancel = true;
                                if ((EnumActionUser.BackLoan.GetHashCode() & permission.Value) == EnumActionUser.BackLoan.GetHashCode())
                                    item.IsBack = true;
                            }
                        }
                    }
                }
                //Returning Json Data  
                return Json(new { meta = model, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
        [PermissionFilter]
        public IActionResult SearchEmp()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new ApproveDatatable(HttpContext.Request.Form);

                DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
                DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
                if (!string.IsNullOrEmpty(modal.DateRanger))
                    DateTimeUtility.ConvertDateRanger(modal.DateRanger, ref fromDate, ref toDate);

                //modal.FromDate = fromDate.ToString("yyyy/MM/dd HH:mm:ss");
                //modal.ToDate = toDate.ToString("yyyy/MM/dd HH:mm:ss");
                if (modal.provinceId == null)
                    modal.provinceId = "-1";

                //modal.userId = user.UserId;
                //mine
                if (modal.status == "0")
                {
                    modal.coordinatorUserId = user.UserId;
                    modal.status = EnumLoanStatus.BRIEF_APPRAISER_REVIEW.GetHashCode().ToString();
                }
                else
                    modal.coordinatorUserId = -1;
                int recordsTotal = 0;
                var data = approveService.SearchEmp(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //set permission row
                if (user.ListPermissionAction != null && user.ListPermissionAction.Count > 0)
                {
                    foreach (var item in data)
                    {
                        if (item.TransactionState.GetValueOrDefault(2) == (int)EnumTransactionState.Huy && (item.ProductId == (int)EnumProductCredit.OtoCreditType_CC || item.ProductId == (int)EnumProductCredit.OtoCreditType_KCC))
                            item.IsTransactionsSecured = true;
                        if (item.Status == EnumLoanStatus.WAIT_APPRAISER_CONFIRM_CUSTOMER.GetHashCode())
                            item.IsConfirmAndPush = true;
                        if (item.Status != EnumLoanStatus.CANCELED.GetHashCode() && user.ListPermissionAction.Any(x => x.LoanStatus == item.Status))
                        {
                            var permission = user.ListPermissionAction.FirstOrDefault(x => x.LoanStatus == item.Status);
                            if (permission != null && permission.Value > 0)
                            {
                                if ((EnumActionUser.Edit.GetHashCode() & permission.Value) == EnumActionUser.Edit.GetHashCode())
                                    item.IsEdit = true;
                                if ((EnumActionUser.Script.GetHashCode() & permission.Value) == EnumActionUser.Script.GetHashCode())
                                    item.IsScript = true;
                                if ((EnumActionUser.PushLoan.GetHashCode() & permission.Value) == EnumActionUser.PushLoan.GetHashCode())
                                    item.IsPush = true;
                                if ((EnumActionUser.Cancel.GetHashCode() & permission.Value) == EnumActionUser.Cancel.GetHashCode())
                                    item.IsCancel = true;
                                if ((EnumActionUser.Upload.GetHashCode() & permission.Value) == EnumActionUser.Upload.GetHashCode())
                                    item.IsUpload = true;
                                if ((EnumActionUser.ComfirmCancel.GetHashCode() & permission.Value) == EnumActionUser.ComfirmCancel.GetHashCode())
                                    item.IsConfirmCancel = true;
                                if ((EnumActionUser.BackLoan.GetHashCode() & permission.Value) == EnumActionUser.BackLoan.GetHashCode())
                                    item.IsBack = true;
                            }
                        }

                        //Kiểm tra thời gian đẩy lên và chưa xử lý
                        if (item.HubPushAt != null && item.ApproverStatusDetail.GetValueOrDefault(0) == 0)
                        {
                            TimeSpan span = DateTime.Now.Subtract(item.HubPushAt.Value);
                            item.ApproverTimeProcessing = Convert.ToInt32(span.TotalMinutes);

                            DateTime dt = new DateTime(item.HubPushAt.Value.Year, item.HubPushAt.Value.Month, item.HubPushAt.Value.Day, 08, 15, 00);
                            DateTime dt2 = new DateTime(item.HubPushAt.Value.Year, item.HubPushAt.Value.Month, item.HubPushAt.Value.Day, 21, 00, 00);
                            if (item.HubPushAt.Value < dt || item.HubPushAt.Value > dt2)
                                item.ApproverStatusDetail = (int)EnumLoanStatusDetailApprover.NgoaiGioLamViec;

                        }


                    }
                }
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult SearchLender(LenderParam.Input param)
        {
            try
            {
                if (param.LenderCode == null)
                    param.LenderCode = "";

                if (param.PageIndex == 0)
                    param.PageIndex = 1;
                if (param.PageSize == 0)
                    param.PageSize = 10;
                var data = _lmsService.SearchLender(GetToken(), param);
                return Json(new { Status = 1, Lst = data.Data.LstLender, TotalRow = data.Data.TotalRow });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult SearchDisbursementAfter()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new DisbursementAfterDatatable(HttpContext.Request.Form);

                if (user.GroupId == (int)EnumGroupUser.Telesale)
                    modal.telesaleId = user.UserId.ToString();

                if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode() || user.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    modal.hubId = user.ShopGlobal.ToString();

                DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
                DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
                if (!string.IsNullOrEmpty(modal.DateRanger))
                    DateTimeUtility.ConvertDateRangerV2(modal.DateRanger, ref fromDate, ref toDate);

                modal.FromDate = fromDate.ToString("yyyy/MM/dd");
                modal.ToDate = toDate.ToString("yyyy/MM/dd");

                #region Gán giá trị mặc định cho param

                if (string.IsNullOrEmpty(modal.provinceId))
                    modal.provinceId = "1";

                if (string.IsNullOrEmpty(modal.loanBriefId))
                    modal.loanBriefId = "-1";

                if (string.IsNullOrEmpty(modal.status))
                    modal.status = "-1";

                if (string.IsNullOrEmpty(modal.productId))
                    modal.productId = "-1";

                if (string.IsNullOrEmpty(modal.locate))
                    modal.locate = "-1";

                if (string.IsNullOrEmpty(modal.hubId))
                    modal.hubId = "-1";

                if (string.IsNullOrEmpty(modal.borrowCavet))
                    modal.borrowCavet = "-1";

                if (string.IsNullOrEmpty(modal.typeSearch))
                    modal.typeSearch = "-1";

                if (string.IsNullOrEmpty(modal.topup))
                    modal.topup = "-1";

                if (string.IsNullOrEmpty(modal.telesaleId))
                    modal.telesaleId = "-1";

                if (string.IsNullOrEmpty(modal.empHubId))
                    modal.empHubId = "-1";

                if (string.IsNullOrEmpty(modal.coordinatorUserId))
                    modal.coordinatorUserId = "-1";
                #endregion

                int recordsTotal = 0;
                // Query api   	
                //var data = approveService.DisbursementAfter(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                var data = _approveV2Service.GetDisbursementAfter(int.Parse(modal.telesaleId), int.Parse(modal.hubId), int.Parse(modal.provinceId),
                    int.Parse(modal.loanBriefId), int.Parse(modal.status), int.Parse(modal.productId), int.Parse(modal.locate), int.Parse(modal.borrowCavet),
                    int.Parse(modal.typeSearch), int.Parse(modal.topup), fromDate, toDate, modal.search, int.Parse(modal.empHubId), int.Parse(modal.coordinatorUserId), int.Parse(modal.page), int.Parse(modal.perpage), ref recordsTotal);

                modal.total = recordsTotal.ToString();
                return Json(new { meta = modal, data = data.lstLoan, refData = data.TotalMoney });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [PermissionFilter]
        public IActionResult ChangeSupport(string[] ArrId, int ApproveId, string ApproveName)
        {
            var user = GetUserGlobal();
            for (int i = 0; i < ArrId.Length; i++)
            {
                var req = new BoundTelesaleReq();
                req.userId = ApproveId;
                req.loanBriefId = Convert.ToInt32(ArrId[i]);

                //var re = JsonConvert.SerializeObject(req);
                var result = loanBriefService.UpdateApprove(GetToken(), req);
                if (result.meta.errorCode != 200)
                {
                    return Json(new { status = 0, message = "Xảy ra lỗi khi chuyển hỗ trợ. Vui lòng thử lại sau!" });
                }
                var note = new LoanBriefNote
                {
                    LoanBriefId = Convert.ToInt32(ArrId[i]),
                    Note = "Đơn vay có mã HĐ-" + ArrId[i] + " đã được " + user.Username + " chuyển cho " + ApproveName,
                    FullName = user.FullName,
                    UserId = user.UserId,
                    Status = 1,
                    ActionComment = EnumActionComment.ApproveChange.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                };
                loanBriefService.AddLoanBriefNote(GetToken(), note);
                //log chuyển đơn qua tdhs khác
                loanBriefService.AddLogDistributionUser(GetToken(), new LogDistributionUser()
                {
                    LoanbriefId = req.loanBriefId,
                    UserId = user.UserId,
                    TypeDistribution = (int)TypeDistributionLog.TDHS,
                    CreatedBy = user.UserId,
                    CreatedAt = DateTime.Now
                });
            }
            return Json(new { status = 1, message = "Chuyển hỗ trợ thành công" });
        }

        [HttpPost]
        public IActionResult CICCardNumber(int LoanBriefId, string FullName)
        {
            ViewBag.LoanID = LoanBriefId;
            ViewBag.FullName = FullName;
            var lstData = _dictionaryService.GetRelativeFamily(GetToken());
            return View(lstData);
        }
        [HttpPost]
        [PermissionFilter]
        public IActionResult CICCardNumberPost(int LoanBriefId, string Type, string Name, string CardNumber)
        {
            var user = GetUserGlobal();
            var result = _lmsService.CICCardNumber(CardNumber);

            if (result.Result == 1 && result.Message.Contains("thành công"))
            {
                //Thêm note khi check thành công
                var note = new LoanBriefNote
                {
                    LoanBriefId = LoanBriefId,
                    Note = Name + " ,CMT: " + CardNumber + " Mối quan hệ: " + Type + " kết quả CIC:" + result.Data.CreditInfo,
                    FullName = user.FullName,
                    UserId = user.UserId,
                    Status = 1,
                    ActionComment = EnumActionComment.ResultCIC.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                };
                loanBriefService.AddLoanBriefNote(GetToken(), note);

                return Json(new { Status = 1, Message = "Check CIC thành công" });
            }
            else
            {
                return Json(new { Status = 0, Message = "Check CIC thất bại" });
            }
        }


        public IActionResult PushLoanLender(int LoanBriefId, int LoanAmountFinal)
        {
            ViewBag.LoanID = LoanBriefId;
            ViewBag.LoanAmountFinal = LoanAmountFinal;
            return View();
        }
        [HttpPost]
        public IActionResult PushLoanToLender(int LoanBriefId, int LenderId)
        {
            var user = GetUserGlobal();
            var req = new LenderReq
            {
                lenderId = LenderId,
                loanBriefId = LoanBriefId
            };

            var result = loanBriefService.UpdateLender(GetToken(), req);

            if (result.meta.errorCode == 200)
            {
                //Thêm note khi check thành công
                var note = new LoanBriefNote
                {
                    LoanBriefId = LoanBriefId,
                    Note = string.Format("Đẩy đơn cho lender :{0}", LenderId),
                    FullName = user.FullName,
                    UserId = user.UserId,
                    Status = 1,
                    ActionComment = EnumActionComment.PushLoanToLender.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                };
                loanBriefService.AddLoanBriefNote(GetToken(), note);

                return Json(new { Status = 1, Message = "Đẩy đơn cho lender thành công" });
            }
            else
            {
                return Json(new { Status = 0, Message = "Đẩy đơn thất bại" });
            }
        }

        [HttpPost]
        [PermissionFilter]
        public IActionResult ConfirmInsurence(int LoanBriefId)
        {
            var user = GetUserGlobal();
            var req = new LenderReq
            {
                loanBriefId = LoanBriefId
            };
            var result = loanBriefService.UpdateInsurence(GetToken(), req);

            if (result.meta.errorCode == 200)
            {
                //Thêm note khi check thành công
                var note = new LoanBriefNote
                {
                    LoanBriefId = LoanBriefId,
                    Note = string.Format("Xác nhận mua bảo hiểm HĐ :{0}", LoanBriefId),
                    FullName = user.FullName,
                    UserId = user.UserId,
                    Status = 1,
                    ActionComment = EnumActionComment.ConfirmInsurence.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                };
                loanBriefService.AddLoanBriefNote(GetToken(), note);

                return Json(new { Status = 1, Message = "Xác nhận mua bảo hiểm thành công" });
            }
            else
            {
                return Json(new { Status = 0, Message = "Xác nhận mua bảo hiểm thất bại" });
            }
        }

        #region ReturnLoan TĐTĐ
        [HttpPost]
        public IActionResult ReturnLoanBrief(int Type, int LoanBriefId, string FullName)
        {
            var user = GetUserGlobal();
            var typeReason = 0;
            if (user.GroupId == EnumGroupUser.ApproveLeader.GetHashCode() || user.GroupId == EnumGroupUser.ApproveEmp.GetHashCode())
                typeReason = EnumTypeReasonReturn.Approve.GetHashCode();
            var reationList = _reasonService.GetReasonGroup(GetToken(), typeReason);
            ViewBag.LoanID = LoanBriefId;
            ViewBag.FullName = FullName;
            ViewBag.Type = Type;
            return View(reationList);
        }

        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> ReturnLoanBriefPost([FromBody] ReturnLoanReq entity)
        {
            try
            {
                var user = GetUserGlobal();
                var loanbrief = await loanBriefService.GetLoanBriefById(GetToken(), entity.LoanBriefId);
                if (loanbrief != null)
                {
                    if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        return Json(new { status = 0, message = "Đơn vay được hủy trước đó!" });

                    //if (loanbrief.Status == EnumLoanStatus.WAIT_APPRAISER_CONFIRM_CUSTOMER.GetHashCode())
                    //    return Json(new { status = 0, message = "Đơn vay đã được đẩy trước đó!" });

                    if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
                        return Json(new { status = 0, message = "Đơn đang được xử lý. Vui lòng thử lại sau!" });

                    if (user.GroupId == EnumGroupUser.ApproveLeader.GetHashCode()
                        || user.GroupId == EnumGroupUser.ApproveEmp.GetHashCode()
                        || user.GroupId == EnumGroupUser.BOD.GetHashCode()
                        || user.GroupId == EnumGroupUser.BusinessManagement.GetHashCode()
                        || user.GroupId == EnumGroupUser.TP_KSNB.GetHashCode())
                    {
                        //Kiêm tra quyền tương tác dữ liệu
                        if (!CheckPermissionAction(user.ListPermissionAction, loanbrief.Status.Value, EnumActionUser.BackLoan.GetHashCode()))
                        {
                            if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                                return Json(GetBaseObjectResult(false, string.Format("Bạn không có quyền trả lại đơn đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)), null));
                            else
                                return Json(GetBaseObjectResult(false, "Bạn không có quyền trả lại đơn!", null));
                        }
                        //loanbrief.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                        //loanbrief.ReasonCoordinatorBack = ReasonId;
                    }
                    loanbrief.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                    loanbrief.ActionState = EnumActionPush.Back.GetHashCode();
                    var result = loanBriefService.ReturnLoanBrief(GetToken(), loanbrief);
                    if (result > 0)
                    {
                        var taskRun = new List<Task>();
                        var ActionCmt = 0;
                        if (user.GroupId == EnumGroupUser.BOD.GetHashCode())
                        {
                            ActionCmt = EnumActionComment.BODReturn.GetHashCode();
                        }
                        else if (user.GroupId == EnumGroupUser.ApproveEmp.GetHashCode() || user.GroupId == EnumGroupUser.ApproveLeader.GetHashCode())
                        {
                            ActionCmt = EnumActionComment.ApproveReturn.GetHashCode();
                        }
                        else if (user.GroupId == (int)EnumGroupUser.BusinessManagement)
                        {
                            ActionCmt = EnumActionComment.BusinessManageBack.GetHashCode();
                        }
                        else if (user.GroupId == (int)EnumGroupUser.TP_KSNB)
                        {
                            ActionCmt = EnumActionComment.TTP_KSNB_Return.GetHashCode();
                        }
                        var mess = "";
                        if (entity.LstReason != null && entity.LstReason.Count > 0)
                        {
                            foreach (var item in entity.LstReason)
                            {
                                mess += "<br />- " + item.ReasonName;
                            }
                        }
                        else
                        {
                            mess = entity.Comment;
                        }

                        //Thêm note
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = result,
                            Note = string.Format("Hợp đồng HĐ-{0} đã được {1} trả lại với lý do : {2}. <br /> Comment : {3}", entity.LoanBriefId, user.Group.GroupName, mess, entity.Comment),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = ActionCmt,
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        var taskLog = Task.Run(() => loanBriefService.AddLoanBriefNote(GetToken(), note));
                        taskRun.Add(taskLog);

                        //Add ReasonCoordinator
                        entity.UserId = user.UserId;
                        var taskReason = Task.Run(() => _dictionaryService.AddReasonCoordinator(GetToken(), entity));
                        taskRun.Add(taskReason);
                        //Log Action
                        var taskLogAction = Task.Run(() => _logActionService.AddLogAction(user.Token, new LogLoanAction
                        {
                            LoanbriefId = loanbrief.LoanBriefId,
                            ActionId = (int)EnumLogLoanAction.LoanReturn,
                            TypeAction = (int)EnumTypeAction.Manual,
                            LoanStatus = loanbrief.Status,
                            TlsLoanStatusDetail = loanbrief.DetailStatusTelesales,
                            HubLoanStatusDetail = loanbrief.LoanStatusDetailChild,
                            TelesaleId = loanbrief.BoundTelesaleId,
                            HubId = loanbrief.HubId,
                            HubEmployeeId = loanbrief.HubEmployeeId,
                            CoordinatorUserId = loanbrief.CoordinatorUserId,
                            UserActionId = user.UserId,
                            GroupUserActionId = user.GroupId,
                            CreatedAt = DateTime.Now
                        })); ;
                        taskRun.Add(taskLogAction);
                        //End
                        if (taskRun != null && taskRun.Count > 0)
                            Task.WaitAll(taskRun.ToArray());
                        return Json(new { status = 1, message = "Trả lại hợp đồng thành công" });
                    }
                    else
                        return Json(new { status = 0, message = "Xảy ra lỗi khi trả lại đơn vay. Vui lòng thử lại sau!" });
                }
                else
                {
                    return Json(new { status = 0, message = "Không tìm thấy thông tin khoản vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> ConfirmLoanMoney(int LoanBriefId)
        {
            var user = GetUserGlobal();
            var model = new LoanBriefInit();
            var loanBrief = await loanBriefService.GetLoanBriefById(GetToken(), LoanBriefId);

            if (loanBrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                return Json(GetBaseObjectResult(false, "Đơn vay đã được hủy trước đó.", null));

            if (loanBrief.InProcess == EnumInProcess.Process.GetHashCode())
                return Json(GetBaseObjectResult(false, "Đơn đang được xử lý. Vui lòng thử lại sau!", null));

            if (loanBrief.Status != EnumLoanStatus.WAIT_APPRAISER_CONFIRM_CUSTOMER.GetHashCode())
                return Json(GetBaseObjectResult(false, "Đơn vay đã được chuyển sang bước khác.", null));

            //Add config 
            if (_baseConfig["AppSettings:TransactionSecured_Check"] != null && _baseConfig["AppSettings:TransactionSecured_Check"] == "1")
            {

                //Check kiểm tra đơn vay là gói oto => đã ký giao dịch đảm bảo ?
                if (loanBrief.ProductId == (int)EnumProductCredit.OtoCreditType_CC || loanBrief.ProductId == (int)EnumProductCredit.OtoCreditType_KCC)
                {
                    if(loanBrief.IsTransactionsSecured.GetValueOrDefault(false) == false)
                    {
                        if (loanBrief.TransactionState != null)
                        {
                            if (loanBrief.TransactionState != (int)EnumTransactionState.DaDuyet)
                                return Json(GetBaseObjectResult(false, "Đơn vay chưa được tạo giao dịch đảm bảo thành công, vui lòng thử lại", null));
                        }
                        else
                            return Json(GetBaseObjectResult(false, "Đơn vay phải được tạo giao dịch đảm bảo trước khi đẩy", null));
                    }    
                    
                    //Get last log transaction
                    //var logTran = _dictionaryService.GetLastLogSecuredTransaction(user.Token, LoanBriefId);
                    //if (logTran != null)
                    //{
                    //    //Kiểm tra: ngày thường(t2 - t6) => status: 1 => đẩy đơn. 2,3,4 => không cho đẩy
                    //    var dayOfWeek = logTran.CreatedAt.Value.DayOfWeek;
                    //    //Kiểm tra nếu là trong tuần thì chỉ cho phép đẩy đơn ở status = 1
                    //    if (dayOfWeek.ToString() == "Saturday" || dayOfWeek.ToString() == "Sunday")
                    //    {
                    //        //Cho phép đẩy đơn ở status cả 1 và 4
                    //    }
                    //    else
                    //    {
                    //        if (loanBrief.TransactionState != (int)EnumTransactionState.DaDuyet)
                    //            return Json(GetBaseObjectResult(false, "Đơn vay chưa được tạo giao dịch đảm bảo thành công, vui lòng thử lại", null));
                    //    }

                    //}
                    //else
                    //    return Json(GetBaseObjectResult(false, "Đơn vay phải được tạo giao dịch đảm bảo trước khi đẩy", null));

                }
            }
            model = loanBrief.MapToLoanBriefViewModel();
            model.ListBank = _dictionaryService.GetBank(GetToken());
            ViewBag.TypeProduct = _dictionaryService.GetLoanProduct(GetToken()).Where(x => x.LoanProductId == loanBrief.ProductId).FirstOrDefault().TypeProductId;
            ViewBag.ProductDetailId = loanBrief.ProductDetailId;

            //Check nếu có mua bảo hiểm mới call tính tiền
            if (loanBrief.BuyInsurenceCustomer.GetValueOrDefault(false) == true)
            {
                //Call api tính bảo hiểm   
                var obj = new InterestParam.Input
                {
                    LoanBriefId = loanBrief.LoanBriefId,
                    TotalMoneyDisbursement = Convert.ToInt64(loanBrief.LoanAmount),
                    NotSchedule = 0,
                    LoanTime = Convert.ToInt32(loanBrief.LoanTime) * 30,
                    Frequency = 30,
                    RateType = Convert.ToInt64(loanBrief.RateTypeId),
                    Rate = Convert.ToDecimal(loanBrief.RatePercent != null ? loanBrief.RatePercent : 98.55)
                };
                var result = _lmsService.InterestAndInsurence(GetToken(), obj);
                if (result != null && result.Result == 1 && result.Data != null)
                {
                    model.FeeInsuranceOfCustomer = result.Data.FeeInsurrance;
                    model.FeeInsuranceMaterialCovered = result.Data.FeeInsuranceMaterialCovered;
                }
                else
                {
                    model.FeeInsuranceOfCustomer = loanBrief.FeeInsuranceOfCustomer;
                    model.FeeInsuranceMaterialCovered = loanBrief.FeeInsuranceOfProperty;
                }
            }

            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "ConfirmLoanMoney")));
        }
        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> ConfirmLoanMoneyPost(LoanBriefInit model)
        {
            var user = GetUserGlobal();
            if (model.LoanBriefId == 0)
                return Json(new { Status = 0, Message = "Đơn vay không tồn tại" });
            if (model.ReceivingMoneyType == EnumReceivingMoneyType.ChuyenKhoan.GetHashCode())
            {
                if ((model.BankAccountName == null || model.BankAccountName == "") || (model.BankAccountNumber == null || model.BankAccountNumber == "") || (model.BankId == null || model.BankId == 0))
                {
                    return Json(new { Status = 0, Message = "Bạn phải nhập đầy đủ thông tin ngân hàng!" });
                }
            }
            if (model.ReceivingMoneyType == EnumReceivingMoneyType.SoThe.GetHashCode())
            {
                if ((model.BankCardNumber == null || model.BankCardNumber == "") || (model.BankId == null || model.BankId == 0))
                {
                    return Json(new { Status = 0, Message = "Bạn phải nhập đầy đủ thông tin ngân hàng!" });
                }
            }
            var loanBrief = await loanBriefService.GetLoanBriefById(GetToken(), model.LoanBriefId);
            if (loanBrief != null && loanBrief.LoanBriefId > 0)
            {
                if (loanBrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                    return Json(new { Status = 0, Message = "Đơn vay đã được hủy trước đó." });

                if (loanBrief.InProcess == EnumInProcess.Process.GetHashCode())
                    return Json(new { Status = 0, Message = "Đơn đang được xử lý. Vui lòng thử lại sau!" });

                if (loanBrief.Status != EnumLoanStatus.WAIT_APPRAISER_CONFIRM_CUSTOMER.GetHashCode())
                    return Json(new { Status = 0, Message = "Đơn vay đã được chuyển sang bước khác." });

                model.BuyInsurenceCustomer = true;
                var result = approveService.ConfirmLoanMoney(GetToken(), model);
                //Check xem có thay đổi số tiền vay không
                if (loanBrief.LoanAmount != model.LoanAmount)
                {
                    var note = new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = user.FullName + " thay đổi số tiền từ " + Convert.ToInt64(loanBrief.LoanAmount).ToString("##,#") + " VNĐ -> " + Convert.ToInt64(model.LoanAmount).ToString("##,#") + " VNĐ",
                        FullName = user.FullName,
                        UserId = user.UserId,
                        Status = 1,
                        ActionComment = EnumActionComment.ConfirmLoanMoney.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    };
                    loanBriefService.AddLoanBriefNote(GetToken(), note);
                }

                if (result.meta.errorCode == 200)
                {

                    var note = new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = "Chốt hợp đồng HĐ-" + model.LoanBriefId + " với số tiền là: " + Convert.ToInt64(model.LoanAmount).ToString("##,#") + "VNĐ",
                        FullName = user.FullName,
                        UserId = user.UserId,
                        Status = 1,
                        ActionComment = EnumActionComment.ConfirmLoanMoney.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    };
                    loanBriefService.AddLoanBriefNote(GetToken(), note);

                    return Json(new { Status = 1, Message = "Chốt hợp đồng và đẩy đơn thành công" });
                }
                else
                {
                    return Json(new { Status = 0, Message = "Có lỗi xảy ra, vui lòng liên hệ kỹ thuật" });
                }
            }
            else
            {
                return Json(new { Status = 0, Message = "Đơn vay không tồn tại trong hệ thống." });
            }
        }

        [HttpPost]
        public IActionResult CalculatorMoneyInsurance(CalculatorMoneyInsurance.InPut param)
        {
            param.LoanTime = Convert.ToInt32(param.LoanTime) * 30;

            var result = _lmsService.CalculatorMoneyInsurance(GetToken(), param);
            if (result.Status == 1)
            {
                //Thêm LogCall khi call thành công
                return Json(new { Status = 1, Data = result.Data.FeeInsuranceOfCustomer });
            }
            else
            {
                return Json(new { Status = 0 });
            }
        }

        [HttpPost]
        public IActionResult ChangeIsUpload(int LoanBriefId, bool Status)
        {


            var user = GetUserGlobal();
            var req = new IsUploadReq
            {
                loanbriefId = LoanBriefId,
                status = Status
            };
            var noteMess = "";
            int action = 0;
            if (Status == true)
            {
                noteMess = "Hoàn thành upload bổ sung chứng từ";
                action = EnumActionComment.UploadCompleted.GetHashCode();
            }
            else
            {
                noteMess = "Trả lại trạng thái chưa hoàn thành";
                action = EnumActionComment.UploadNotCompleted.GetHashCode();
            }
            var result = loanBriefService.UpdateIsUpload(GetToken(), req);

            if (result.meta.errorCode == 200)
            {
                var note = new LoanBriefNote
                {
                    LoanBriefId = LoanBriefId,
                    Note = string.Format(noteMess + " HĐ :{0}", LoanBriefId),
                    FullName = user.FullName,
                    UserId = user.UserId,
                    Status = 1,
                    ActionComment = action,
                    CreatedTime = DateTime.Now,
                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                };
                loanBriefService.AddLoanBriefNote(GetToken(), note);

                return Json(new { Status = 1, Message = noteMess });
            }
            else
            {
                return Json(new { Status = 0, Message = "Có lỗi xảy ra, vui lòng thử lại sau" });
            }
        }

        [HttpPost]
        public IActionResult GetNoteByLoanID([FromQuery] int loanBriefId)
        {
            try
            {
                //loanBriefId = 10020;
                // Query api   	
                var data = loanBriefService.GetNoteByLoanID(GetToken(), loanBriefId);
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public IActionResult GetNoteDetail([FromQuery] int loanBriefId)
        {
            try
            {
                //loanBriefId = 10020;
                // Query api   	
                var data = loanBriefService.GetNoteByLoanID(GetToken(), loanBriefId);
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        public async Task<IActionResult> ShowNote(int loanBriefId)
        {
            var user = GetUserGlobal();
            ViewBag.LoanBriefId = loanBriefId;
            var loan = await loanBriefService.GetLoanBriefById(GetToken(), loanBriefId);
            var checkLocate = loan.IsLocate;
            ViewBag.Locate = checkLocate;
            return View(user);
        }


        [HttpPost]
        public async Task<IActionResult> CreateLoanNote(int LoanBriefId, string Content, int Action)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var taskRun = new List<Task>();
                int UserId = (await HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA")).UserId;
                string FullName = (await HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA")).FullName;
                var ActionComment = 1;
                if (Action == EnumActionComment.Locate.GetHashCode())
                    ActionComment = EnumActionComment.Locate.GetHashCode();
                if (Action == EnumActionComment.NotLocate.GetHashCode())
                    ActionComment = EnumActionComment.NotLocate.GetHashCode();

                var loanbrief = await loanBriefService.GetBasicInfo(access_token, LoanBriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (user.GroupId == EnumGroupUser.ManagerHub.GetHashCode() || user.GroupId == EnumGroupUser.StaffHub.GetHashCode()
                    || user.GroupId == EnumGroupUser.Telesale.GetHashCode() || user.GroupId == EnumGroupUser.ManagerTelesales.GetHashCode())
                    {
                        var obj = new DAL.Object.LoanBriefFeedback();
                        obj.LoanBriefId = loanbrief.LoanBriefId;
                        obj.CountCall = loanbrief.CountCall.GetValueOrDefault(0);
                        var timeNow = DateTime.Now;
                        if (user.GroupId == EnumGroupUser.ManagerHub.GetHashCode() || user.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                        {
                            if (!loanbrief.FirstTimeHubFeedBack.HasValue)
                                obj.FirstTimeHubFeedBack = timeNow;
                            if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode() && !loanbrief.FirstTimeStaffHubFeedback.HasValue)
                                obj.FirstTimeStaffHubFeedback = timeNow;
                        }
                        if (user.GroupId == EnumGroupUser.Telesale.GetHashCode() || user.GroupId == EnumGroupUser.ManagerTelesales.GetHashCode())
                        {
                            if (!loanbrief.FirstProcessingTime.HasValue)
                                obj.FirstProcessingTime = timeNow;

                            if (user.GroupId == EnumGroupUser.Telesale.GetHashCode())
                                obj.LastProcessingTime = timeNow;

                            if (loanbrief.Status != (int)EnumLoanStatus.TELESALE_ADVICE && loanbrief.Status != (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN)
                                return Json(new { status = 0, message = "Đơn vay đã được đẩy đi không thể comment" });
                        }

                        loanBriefService.UpdateFeedback(access_token, obj);
                    }
                }
                else
                    return Json(new { status = 0, message = "Không tồn tại đơn vay trong hệ thống" });

                //Thêm note khi check thành công
                var taskNote = Task.Run(() => loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                {
                    LoanBriefId = LoanBriefId,
                    Note = Content,
                    FullName = FullName,
                    UserId = UserId,
                    Status = 1,
                    ActionComment = ActionComment,
                    CreatedTime = DateTime.Now,
                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                }));
                taskRun.Add(taskNote);

                //Log Action
                var taskLogAction = Task.Run(() => _logActionService.AddLogAction(access_token, new LogLoanAction
                {
                    LoanbriefId = loanbrief.LoanBriefId,
                    ActionId = (int)EnumLogLoanAction.Comment,
                    TypeAction = (int)EnumTypeAction.Manual,
                    LoanStatus = loanbrief.Status,
                    TlsLoanStatusDetail = loanbrief.DetailStatusTelesales,
                    HubLoanStatusDetail = loanbrief.LoanStatusChild,
                    NewValues = Content,
                    TelesaleId = loanbrief.BoundTelesaleId,
                    HubId = loanbrief.HubId,
                    HubEmployeeId = loanbrief.HubEmployeeId,
                    CoordinatorUserId = loanbrief.CoordinatorUserId,
                    UserActionId = user.UserId,
                    GroupUserActionId = user.GroupId,
                    CreatedAt = DateTime.Now
                })); ;
                taskRun.Add(taskLogAction);
                //End

                Task.WaitAll(taskRun.ToArray());
                var result = taskNote.Result;
                //var result = loanBriefService.AddLoanBriefNote(GetToken(), note);
                if (result > 0)
                    return Json(new { status = 1, message = "Thêm note thành công" });
                return Json(new { status = 0, message = "Xảy ra lỗi khi thêm note. Vui lòng liên hệ quản trị viên!" });

            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Lỗi liên hệ kĩ thuật" });
            }
        }

        [Produces("application/json")]
        public IActionResult ExportExcelDis(string loanBriefId, string search, string provinceId, string status, string productId, string hubId, string locate, string filtercreateTime, string hubEmployee)
        {
            var user = GetUserGlobal();
            var modal = new ReportDisberment();

            modal.loanBriefId = loanBriefId;
            modal.search = search;
            modal.provinceId = provinceId;
            modal.export = 1;
            if (status != "undefined")
                modal.status = status;
            modal.productId = productId;
            if (hubId != "undefined")
                modal.hubId = hubId;
            if (modal.productId == "undefined")
                modal.productId = "-1";
            if (hubEmployee != "undefined")
                modal.empHubId = hubEmployee;
            if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode() || user.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                modal.hubId = user.ShopGlobal.ToString();
            modal.Locate = locate;
            modal.DateRanger = filtercreateTime;

            DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
            DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
            if (!string.IsNullOrEmpty(modal.DateRanger))
                DateTimeUtility.ConvertDateRangerV2(modal.DateRanger, ref fromDate, ref toDate);

            modal.FromDate = fromDate.ToString("yyyy/MM/dd");
            modal.ToDate = toDate.ToString("yyyy/MM/dd");
            if (modal.provinceId == null)
                modal.provinceId = "1";
            //modal.userId = user.UserId;
            int recordsTotal = 0;
            // Query api   	
            var objData = approveService.DisbursementAfter(GetToken(), modal.ToQueryObject(), ref recordsTotal);
            var data = objData.lstLoan;
            List<string> lstHeaderLoan = new List<string> { "STT", "Mã HĐ", "Khách hàng", "Thành phố", "Đơn vị bán(HUB/PDV)", "Sản phẩm vay", "Tình trạng nhà ở", "Số tiền vay", "Thời gian vay", "Ngày giải ngân", "Số kỳ đã đóng", "Dư nợ hiện tại", "Số ngày chậm thanh toán", "Trạng thái" };
            List<int> lstWidthLoan = new List<int> { 10, 20, 30, 15, 15, 30, 20, 20, 20, 20, 20, 20, 20, 25 };
            List<ExcelHorizontalAlignment> lstAlign = new List<ExcelHorizontalAlignment> { ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Right,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left
                                                                                           };

            List<System.Drawing.Color> lstColor = new List<System.Drawing.Color> { Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black
                                                                                  };

            byte[] fileContents;
            ExcelPackage package = new ExcelPackage();

            var ws = package.Workbook.Worksheets.Add("Kiểm soát sau giải ngân");
            // Set Font
            ws.Cells.Style.Font.Name = "Calibri";
            ws.Cells.Style.Font.Size = 11;

            // Format All Cells
            ws.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            // Format All Column
            for (int i = 1; i <= lstHeaderLoan.Count; i++)
            {
                ws.Column(i).Width = lstWidthLoan[i - 1];
                ws.Column(i).Style.HorizontalAlignment = lstAlign[i - 1];
                ws.Column(i).Style.Font.Color.SetColor(lstColor[i - 1]);
            }

            int iPosHeader = 1;
            // mapping title header
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(77, 119, 204));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Font.Bold = true;
            ws.Row(iPosHeader).Height = 16;
            for (int i = 1; i <= lstHeaderLoan.Count; i++)
            {
                ws.Cells[iPosHeader, i].Value = lstHeaderLoan[i - 1];
            }

            // SET VALUE
            iPosHeader++;


            int j = 1;
            var statuStr = "Chưa hoàn thành";
            for (int i = 0; i < data.Count(); i++)
            {
                if (data[i].Status == 1)
                    statuStr = "Đã hoàn thành";
                var Reborrow = "";
                if (data[i].IsReborrow == true)
                    Reborrow = "(Tái vay)";
                ws.Cells[iPosHeader, 1].Value = j;
                ws.Cells[iPosHeader, 2].Value = "HĐ" + data[i].LoanBriefId + Reborrow;
                ws.Cells[iPosHeader, 3].Value = data[i].FullName;
                ws.Cells[iPosHeader, 4].Value = data[i].Province.Name;
                ws.Cells[iPosHeader, 5].Value = data[i].Hub.Name;
                ws.Cells[iPosHeader, 6].Value = data[i].LoanProduct.Name;
                ws.Cells[iPosHeader, 7].Value = Description.GetDescription((EnumTypeofownership)data[i].LoanBriefResident.ResidentType);
                ws.Cells[iPosHeader, 8].Value = Convert.ToInt64(data[i].LoanAmount).ToString("#,##");
                ws.Cells[iPosHeader, 9].Value = data[i].LoanTime + " tháng";
                ws.Cells[iPosHeader, 10].Value = Convert.ToDateTime(data[i].DisbursementAt.ToString()).ToString("dd/MM/yyyy");
                ws.Cells[iPosHeader, 11].Value = "";
                ws.Cells[iPosHeader, 12].Value = "";
                ws.Cells[iPosHeader, 13].Value = "";
                ws.Cells[iPosHeader, 14].Value = statuStr;
                iPosHeader++;
                j++;
            }
            fileContents = package.GetAsByteArray();
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: "kiem-soat-sau-giai-ngan.xlsx"
            );
            //Response.ContentType = "application/vnd.ms-excel";
            //HttpContext.Response.Headers.Add("content-disposition", "attachment;  filename=" + string.Format("kiem-soat-sau-giai-ngan.xls"));


            //return new EmptyResult();
        }

        [HttpPost]
        public IActionResult InterestTool(int LoanBriefId)
        {
            var data = loanBriefService.GetLoanBriefById(GetToken(), LoanBriefId);
            return View(data);
        }

        [HttpPost]
        [PermissionFilter]
        public IActionResult InterestSubmit(InterestParam.Input param)
        {
            //param.TotalRate = param.RateInterest + param.RateConsultant;
            param.LoanTime = param.LoanTime * 30;
            param.NotSchedule = 0;
            param.Frequency = 30;
            var data = _lmsService.InterestAndInsurence(GetToken(), param);
            if (data != null && data.Result == 1)
            {
                //Thêm LogCall khi call thành công
                return Json(new { Status = 1, Data = data.Data });
            }
            else
            {
                return Json(new { Status = 0, Message = data != null ? data.Message : "Xảy ra lỗi khi gọi api" });
            }
        }

        [HttpPost]
        public IActionResult ViewIntersestSubmit(InterestParam.SubmitFormIntersest entity)
        {
            var strView = string.Empty;
            var dataOften = new InterestParam.Output();
            var dataTopup = new InterestParam.ViewDataTopUp();
            var data = new object();
            // Tính lãi phí thường
            if (entity.CalculationInterestOften == true)
            {
                strView = "_viewIntersestOftenSubmit";
                entity.LoanTime = entity.LoanTime * 30;
                var request = new InterestParam.Input
                {
                    TotalMoneyDisbursement = entity.TotalMoneyDisbursement,
                    LoanTime = entity.LoanTime,
                    RateType = entity.RateType,
                    Frequency = entity.Frequency,
                    Rate = entity.Rate,
                    NotSchedule = 0,
                    LoanBriefId = entity.LoanBriefId
                };
                dataOften = _lmsService.InterestAndInsurence(GetToken(), request);
                data = dataOften;
            }
            else
            {
                strView = "_viewIntersestTopupSubmit";
                //Lấy ra trường LMS_LoanId
                var loanBrief = loanBriefService.GetLoanBriefByCodeId(GetToken(), entity.CodeId);
                if (loanBrief != null)
                {
                    if (loanBrief.Status == (int)EnumLoanStatus.FINISH)
                        return Json(GetBaseObjectResult(false, "Đơn vay đã tất toán.", null, null));

                    var request = new InterestParam.InputTopup
                    {
                        LoanId = loanBrief.LmsLoanId.Value,
                        TotalMoneyDisbursement = entity.TotalMoneyDisbursementTopup,
                        LoanTime = entity.LoanTimeTopup,
                        LoanBriefId = loanBrief.LoanBriefId
                    };
                    //Tính lãi phí Topup
                    dataTopup = _lmsService.GenCalendarPayment(GetToken(), request);
                    data = dataTopup;
                }
                else
                    return Json(GetBaseObjectResult(false, "Không tìm thấy đơn vay. Vui lòng kiểm tra lại.", null, null));
            }
            ViewBag.RateType = entity.RateType;
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(data, strView)));
        }


        #region DuyNV
        public async Task<IActionResult> DeviceLocation(int loanbriefId)
        {
            if (loanbriefId > 0)
            {
                var loanbrief = await loanBriefService.GetLoanBriefById(GetToken(), loanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    var urlEmbedMapTracking = _baseConfig["AppSettings:UrlEmbedMapTracking"];

                    return Json(GetBaseObjectResult(true, "Success", null, urlEmbedMapTracking + loanbriefId));
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống", null));
                }
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
            }
        }
        #endregion

        private string RenderViewAsString(object model, string viewName = null)
        {
            viewName = viewName ?? ControllerContext.ActionDescriptor.ActionName;
            ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                IView view = _viewEngine.FindView(ControllerContext, viewName, true).View;
                ViewContext viewContext = new ViewContext(ControllerContext, view, ViewData, TempData, sw, new HtmlHelperOptions());
                view.RenderAsync(viewContext).Wait();
                return sw.GetStringBuilder().ToString();
            }
        }

        private bool CheckPermissionAction(List<UserActionPermission> ListPermissionAction, int LoanStatus, int Action)
        {
            //Kiêm tra quyền tương tác dữ liệu
            if (ListPermissionAction != null && ListPermissionAction.Any(x => x.LoanStatus == LoanStatus))
            {
                var permission = ListPermissionAction.FirstOrDefault(x => x.LoanStatus == LoanStatus);
                if ((Action & permission.Value) == Action)
                    return true;
            }
            return false;
        }

        [HttpPost]
        public async Task<IActionResult> BorrowCavet(string[] ArrId)
        {
            var user = GetUserGlobal();
            for (int i = 0; i < ArrId.Length; i++)
            {

                var loanbrief = await loanBriefService.GetLoanBriefById(GetToken(), Convert.ToInt32(ArrId[i]));
                if (loanbrief != null)
                {
                    if (loanbrief.Status == EnumLoanStatus.DISBURSED.GetHashCode() && loanbrief.ReCareLoanbrief == null)
                    {
                        var result = loanBriefService.UpdateBorrowCavet(GetToken(), Convert.ToInt32(ArrId[i]));
                        if (result.meta.errorCode != 200)
                            return Json(new { status = 0, message = "Có lỗi xảy ra, vui lòng thử lại sau!" });
                    }
                    //lưu log comment
                    loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        Note = string.Format("Hợp đồng HĐ-{0} đã được {1} chuyển qua mượn cavet", loanbrief.LoanBriefId, user.Username),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.BorrowCavet.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                }

            }
            return Json(new { status = 1, message = "Chuyển trạng thái mượn cavet thành công" });

        }

        [HttpGet]
        public IActionResult ChangeStatus(int loanBriefId)
        {
            var user = GetUserGlobal();
            ViewBag.LoanBriefId = loanBriefId;

            var result = _loanStatusDetailService.GetLoanStatusDetail(user.Token, (int)EnumLoanStatusDetail.TDHS);
            if (result != null && result.Count > 0)
                result = result.Where(x => x.Id == (int)EnumLoanStatusDetailApprover.DangXuLy).ToList();
            return View(result);
        }
        [HttpPost]
        public IActionResult SaveChangeStatus(DAL.Object.LoanStatusDetail.NextStepLoanHub entity)
        {
            var user = GetUserGlobal();
            var access_token = GetToken();
            //Cập nhập trạng thái telesales vào bảng loanbrief
            var result = loanBriefService.SaveChangeStatusApprover(access_token, entity);
            if (result)
            {
                //lưu lại vào bảng comment
                if (entity.NoteLoanStatusDetail != null)
                {
                    var loanBriefNote = new LoanBriefNote()
                    {
                        LoanBriefId = entity.LoanBriefId,
                        UserId = user.UserId,
                        FullName = user.FullName,
                        ActionComment = (int)EnumActionComment.ApproverNextStep,
                        Note = entity.NoteLoanStatusDetail,
                        Status = 1,
                        CreatedTime = DateTime.Now,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    };
                    loanBriefService.AddLoanBriefNote(access_token, loanBriefNote);

                }
                return Json(new { status = 1, message = "Chuyển trạng thái đơn thành công" });

            }
            else
                return Json(new { status = 1, message = "Có lỗi xảy ra trong quá trình cập nhập. Vui lòng báo lại với phòng kĩ thuật" });
        }

        #region Compare Loan
        public async Task<IActionResult> CompareLoan(int id = 0)
        {
            var user = GetUserGlobal();
            var access_token = user.Token;

            var loanBrief = await loanBriefService.GetLoanInfoCompare(access_token, id);
            if (loanBrief == null || loanBrief.LoanBriefId == 0)
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
            //var isSaveLog = 0;
            //if (user.GroupId == (int)EnumGroupUser.ManagerHub || user.GroupId == (int)EnumGroupUser.StaffHub)
            //    isSaveLog = 1;
            //ViewBag.IsSaveLog = isSaveLog;

            var logCompare = _dictionaryService.GetLastCompareDocument(access_token, id);
            var tabLog = 0;
            if (logCompare != null && logCompare.Tab.GetValueOrDefault(0) > 0)
                tabLog = logCompare.Tab.Value;
            ViewBag.TabLog = tabLog;

            //ViewBag.lstFileCMND = loanBriefService.GetLoanBriefFile(access_token, id, (int)EnumDocumentType.CMND_CCCD).ToList();
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(loanBrief, "CompareLoan")));
        }

        public async Task<IActionResult> LoadFileRoot(int id = 0, int documentId = 0)
        {
            var user = GetUserGlobal();
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var ServiceURLLocal = _baseConfig["AppSettings:ServiceURLLocal"];
            var data = new List<LoanBriefFileDetail>();
            if (id > 0 && documentId > 0)
            {
                var access_token = user.Token;
                var loanBrief = await loanBriefService.GetLoanInfoCompare(access_token, id);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));

                data = loanBriefService.GetLoanBriefFile(access_token, id, documentId).ToList();

                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        if (!string.IsNullOrEmpty(item.FileThumb))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                            else
                                item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                        }
                        if (string.IsNullOrEmpty(item.FilePath)) continue;
                        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                            item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                            item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                            else
                                item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                        }
                    }
                }


            }

            return Json(new { data = data });

        }

        public async Task<IActionResult> LoadDisplayStep(int loanBriefId = 0, int tab = 0)
        {
            var data = new LoanInfoCompare();
            var view = "";
            if (loanBriefId > 0 && tab > 0)
            {
                data.LoanBriefId = loanBriefId;
                if (tab == 1)
                    view = "_tab1";
                else if (tab == 2)
                    view = "_tab2";
                else if (tab == 3)
                    view = "_tab3";
                else if (tab == 4)
                    view = "_tab4";
                else if (tab == 5)
                    view = "_tab5";
            }
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(data, "Compare/DisplayStep/" + view)));

        }
        [HttpPost]
        public async Task<IActionResult> LoadDataCMND([FromBody] LoadDataCMNDReq req)
        {
            var user = GetUserGlobal();
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var ServiceURLLocal = _baseConfig["AppSettings:ServiceURLLocal"];
            var data = new CMND();
            var view = "";
            if (req.LoanBriefId > 0 && req.Step > 0)
            {
                var access_token = user.Token;
                var loanBrief = await loanBriefService.GetLoanInfoCompare(access_token, req.LoanBriefId);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                bool fileFromRequest = false;
                if (req.LstFiles != null && req.LstFiles.Count > 0)
                {
                    data.lstFilesRoot = req.LstFiles;
                    fileFromRequest = true;
                }
                else
                    data.lstFilesRoot = loanBriefService.GetLoanBriefFile(access_token, req.LoanBriefId, (int)EnumDocumentType.CMND_CCCD).ToList();
                //CMND với thông tin trên LOS
                if (req.Step == 1)
                {
                    data.LoanInfo = loanBrief;
                    view = "_CMND_Step1";
                }
                else if (req.Step == 2)
                {
                    data.lstFilesCompare = loanBriefService.GetLoanBriefFile(access_token, req.LoanBriefId, (int)EnumDocumentType.Motorbike_Registration_Certificate).ToList();
                    view = "_CMND_Step2";
                }
                else if (req.Step == 3)
                {
                    data.lstFilesCompare = loanBriefService.GetLoanBriefFile(access_token, req.LoanBriefId, (int)EnumDocumentType.SHK).ToList();
                    view = "_CMND_Step3";
                }
                else if (req.Step == 4)
                {
                    data.lstFilesCompare = loanBriefService.GetLoanBriefFile(access_token, req.LoanBriefId, (int)EnumDocumentType.CTKK).ToList();
                    view = "_CMND_Step4";
                }
                if (!fileFromRequest && data.lstFilesRoot != null && data.lstFilesRoot.Count > 0)
                {
                    foreach (var item in data.lstFilesRoot)
                    {
                        if (!string.IsNullOrEmpty(item.FileThumb))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                            else
                                item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                        }
                        if (string.IsNullOrEmpty(item.FilePath)) continue;
                        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                            item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                            item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                            else
                                item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                        }
                    }
                }
                if (data.lstFilesCompare != null && data.lstFilesCompare.Count > 0)
                {
                    foreach (var item in data.lstFilesCompare)
                    {
                        if (!string.IsNullOrEmpty(item.FileThumb))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                            else
                                item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                        }
                        if (string.IsNullOrEmpty(item.FilePath)) continue;
                        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                            item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                            item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                            else
                                item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                        }
                    }
                }


            }
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(data, "Compare/" + view)));

        }

        [HttpPost]
        public async Task<IActionResult> LoadDataDKX([FromBody] LoadDataCMNDReq req)
        {
            var user = GetUserGlobal();
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var ServiceURLLocal = _baseConfig["AppSettings:ServiceURLLocal"];
            var data = new CMND();
            var view = "";
            if (req.LoanBriefId > 0 && req.Step > 0)
            {
                var access_token = user.Token;
                var loanBrief = await loanBriefService.GetLoanInfoCompare(access_token, req.LoanBriefId);

                ViewBag.AppraiserMotoPrice = null;
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                bool fileFromRequest = false;
                if (req.LstFiles != null && req.LstFiles.Count > 0)
                {
                    data.lstFilesRoot = req.LstFiles;
                    fileFromRequest = true;
                }
                else
                    data.lstFilesRoot = loanBriefService.GetLoanBriefFile(access_token, req.LoanBriefId, (int)EnumDocumentType.Motorbike_Registration_Certificate).ToList();

                if (req.Step == 1)
                {
                    data.lstFilesCompare = loanBriefService.GetLoanBriefFile(access_token, req.LoanBriefId, (int)EnumDocumentType.AnhThucTeXe).ToList();
                    view = "_DKX_Step1";
                }
                else if (req.Step == 2)
                {
                    data.lstFilesCompare = loanBriefService.GetLoanBriefFile(access_token, req.LoanBriefId, (int)EnumDocumentType.AnhSoKhungSoMay).ToList();
                    view = "_DKX_Step2";
                }
                else if (req.Step == 3)
                {
                    data.lstFilesCompare = loanBriefService.GetLoanBriefFile(access_token, req.LoanBriefId, (int)EnumDocumentType.Motorbike_Registration_Certificate_Light).ToList();
                    view = "_DKX_Step3";
                }
                else if (req.Step == 4)
                {
                    data.lstFilesCompare = loanBriefService.GetLoanBriefFile(access_token, req.LoanBriefId, (int)EnumDocumentType.SHK).ToList();
                    view = "_DKX_Step4";
                }
                else if (req.Step == 5)
                {
                    data.lstFilesCompare = loanBriefService.GetLoanBriefFile(access_token, req.LoanBriefId, (int)EnumDocumentType.CTKK).ToList();
                    view = "_DKX_Step5";
                }
                else if (req.Step == 6)
                {
                    try
                    {
                        var loanbriefModel = loanBriefService.GetLoanBriefForAppraiser(access_token, req.LoanBriefId);
                        if (loanbriefModel != null)
                        {
                            if (loanbriefModel.ProductPropertyId > 0)
                            {
                                var appraiserMotoPriceViewModel = new AppraiserMotoPriceViewModel();

                                var product = _productService.GetProduct(user.Token, loanbriefModel.ProductPropertyId.Value);
                                //định giá xe
                                var maxPriceProduct = 0L;
                                decimal priceCarAI = 0;
                                // lấy giá bên AI
                                decimal priceCurrent = 0L;
                                if (loanbriefModel.LoanAmountExpertiseAi > 0)
                                    priceCurrent = loanbriefModel.LoanAmountExpertiseAi.Value;
                                else
                                {
                                    priceCarAI = _productService.GetPriceAI(access_token, loanbriefModel.ProductPropertyId.Value, 0);
                                    if (priceCarAI > 0)
                                        priceCurrent = (long)priceCarAI;
                                }
                                if (priceCurrent > 0)
                                {
                                    appraiserMotoPriceViewModel.CurrentPrice = priceCurrent;
                                    if (!loanbriefModel.TypeOwnerShipId.HasValue)
                                        return Json(GetBaseObjectResult(false, "Vui lòng chọn hình thức sở hữu nhà"));

                                    appraiserMotoPriceViewModel.TypeOwnerShipId = loanbriefModel.TypeOwnerShipId.Value;
                                    var productDetailId = loanbriefModel.ProductDetailId;
                                    if (productDetailId.GetValueOrDefault(0) == 0)
                                        productDetailId = Common.Utils.ProductPriceUtils.ConvertProductDetail(loanbriefModel.TypeOwnerShipId.Value);
                                    var typeRemarketingLtv = 0;
                                    if (loanbriefModel.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                                        typeRemarketingLtv = (int)TypeRemarketingLtv.TopUp;
                                    else if (loanbriefModel.IsReborrow.GetValueOrDefault(false))
                                        typeRemarketingLtv = (int)TypeRemarketingLtv.Reborrow;
                                    maxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPriceV3(loanbriefModel.ProductCreditId.Value, priceCurrent,
                                              loanbriefModel.TypeOwnerShipId.Value, productDetailId.Value, 0, 0, typeRemarketingLtv);
                                    appraiserMotoPriceViewModel.ProductDetailId = productDetailId.Value;
                                }
                                appraiserMotoPriceViewModel.MaxPriceProduct = maxPriceProduct;
                                appraiserMotoPriceViewModel.ProductCreditId = loanbriefModel.ProductCreditId.Value;
                                appraiserMotoPriceViewModel.ProductId = product.Id;
                                appraiserMotoPriceViewModel.ProductReview = _productService.GetProductReviewV2(GetToken(), loanbriefModel.LoanBriefId, loanbriefModel.ProductPropertyId.Value,
                                    product.ProductTypeId, priceCurrent) ?? new List<Models.ProductReview>();
                                appraiserMotoPriceViewModel.UserDetail = GetUserGlobal();
                                appraiserMotoPriceViewModel.LoanBriefId = loanbriefModel.LoanBriefId;
                                appraiserMotoPriceViewModel.Status = loanbriefModel.Status ?? 0;

                                #region lấy dữ liệu cũ
                                var olDataInfomationAppraiserMoto = new OldInfomationAppraiserMoto();
                                if (loanbriefModel.ProductCreditId == (int)EnumProductCredit.OtoCreditType_CC
                                   || loanbriefModel.ProductCreditId == (int)EnumProductCredit.CamotoCreditType
                                   || loanbriefModel.ProductCreditId == (int)EnumProductCredit.OtoCreditType_KCC)
                                {
                                    if (!string.IsNullOrEmpty(loanbriefModel.CarManufacturer))
                                    {
                                        olDataInfomationAppraiserMoto.ProductName = loanbriefModel.CarManufacturer;
                                        olDataInfomationAppraiserMoto.BrandName = loanbriefModel.CarName;
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(loanbriefModel.ProductPropertyName))
                                    {
                                        olDataInfomationAppraiserMoto.ProductName = loanbriefModel.ProductPropertyName;
                                        olDataInfomationAppraiserMoto.BrandName = loanbriefModel.BrandPropertyName;
                                    }
                                }
                                olDataInfomationAppraiserMoto.ProductId = loanbriefModel.ProductCreditId.Value;
                                olDataInfomationAppraiserMoto.ProductDetailName = Common.Helpers.ExtentionHelper.GetDescription((Common.Extensions.InfomationProductDetail)appraiserMotoPriceViewModel.ProductDetailId);
                                if (loanbriefModel.ProductCreditId.HasValue)
                                    olDataInfomationAppraiserMoto.OwnerProductName = Common.Helpers.ExtentionHelper.GetDescription((Common.Extensions.EnumProductCredit)loanbriefModel.ProductCreditId);
                                if (loanbriefModel.TypeOwnerShipId.HasValue)
                                    olDataInfomationAppraiserMoto.TypeOwnerShipName = Common.Helpers.ExtentionHelper.GetDescription((EnumTypeofownership)loanbriefModel.TypeOwnerShipId.Value);
                                appraiserMotoPriceViewModel.OldInfomationAppraiserMoto = olDataInfomationAppraiserMoto;
                                #endregion
                                ViewBag.AppraiserMotoPrice = appraiserMotoPriceViewModel;
                            }
                            else
                                return Json(GetBaseObjectResult(false, "Đơn vay chưa có đủ thông tin xe máy. Không thể thẩm định xe."));
                        }
                        else
                            return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống"));
                    }
                    catch (Exception ex)
                    {
                        return Json(GetBaseObjectResult(false, "Xảy ra lỗi khi lấy thông tin thẩm định"));
                    }

                    view = "_DKX_Step6";
                }
                if (!fileFromRequest && data.lstFilesRoot != null && data.lstFilesRoot.Count > 0)
                {
                    foreach (var item in data.lstFilesRoot)
                    {
                        if (!string.IsNullOrEmpty(item.FileThumb))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                            else
                                item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                        }
                        if (string.IsNullOrEmpty(item.FilePath)) continue;
                        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                            item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                            item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                            else
                                item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                        }
                    }
                }
                if (data.lstFilesCompare != null && data.lstFilesCompare.Count > 0)
                {
                    foreach (var item in data.lstFilesCompare)
                    {
                        if (!string.IsNullOrEmpty(item.FileThumb))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                            else
                                item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                        }
                        if (string.IsNullOrEmpty(item.FilePath)) continue;
                        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                            item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                            item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                            else
                                item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                        }
                    }
                }


            }
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(data, "Compare/" + view)));

        }

        [HttpPost]
        public async Task<IActionResult> LoadDataBCT([FromBody] LoadDataCMNDReq req)
        {
            var user = GetUserGlobal();
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var ServiceURLLocal = _baseConfig["AppSettings:ServiceURLLocal"];
            var data = new CMND();
            var view = "";
            if (req.LoanBriefId > 0 && req.Step > 0)
            {
                var access_token = user.Token;
                var loanBrief = await loanBriefService.GetLoanInfoCompare(access_token, req.LoanBriefId);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                bool fileFromRequest = false;
                if (req.LstFiles != null && req.LstFiles.Count > 0)
                {
                    data.lstFilesRoot = req.LstFiles;
                    fileFromRequest = true;
                }
                else
                    data.lstFilesRoot = loanBriefService.GetLoanBriefFile(access_token, req.LoanBriefId, (int)EnumDocumentType.CTKK).ToList();

                if (req.Step == 1)
                {
                    data.LoanInfo = loanBrief;
                    view = "_BCT_Step1";
                }
                if (!fileFromRequest && data.lstFilesRoot != null && data.lstFilesRoot.Count > 0)
                {
                    foreach (var item in data.lstFilesRoot)
                    {
                        if (!string.IsNullOrEmpty(item.FileThumb))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                            else
                                item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                        }
                        if (string.IsNullOrEmpty(item.FilePath)) continue;
                        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                            item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                            item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                            else
                                item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                        }
                    }
                }


            }
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(data, "Compare/" + view)));

        }

        [HttpPost]
        public async Task<IActionResult> LoadDataZaloFace([FromBody] LoadDataCMNDReq req)
        {
            var user = GetUserGlobal();
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var ServiceURLLocal = _baseConfig["AppSettings:ServiceURLLocal"];
            var data = new CMND();
            var view = "";
            if (req.LoanBriefId > 0 && req.Step > 0)
            {
                var access_token = user.Token;
                var loanBrief = await loanBriefService.GetLoanInfoCompare(access_token, req.LoanBriefId);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                if (req.LstFiles != null && req.LstFiles.Count > 0)
                    data.lstFilesRoot = req.LstFiles;
                else
                {
                    var lstDocument = new List<int>();
                    lstDocument.Add((int)EnumDocumentType.Zalo);
                    lstDocument.Add((int)EnumDocumentType.DanhBa);
                    data.lstFilesRoot = await loanBriefService.GetLoanBriefFileByDocumentMulti(access_token, req.LoanBriefId, lstDocument);
                }


                if (req.Step == 1)
                {
                    data.LoanInfo = loanBrief;
                    data.ListRelativeFamilys = CacheData.ListRelativeFamilys;
                    view = "_ZaloFace_Step1";
                }
                if (data.lstFilesRoot != null && data.lstFilesRoot.Count > 0)
                {
                    foreach (var item in data.lstFilesRoot)
                    {
                        if (!string.IsNullOrEmpty(item.FileThumb))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                            else
                                item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                        }
                        if (string.IsNullOrEmpty(item.FilePath)) continue;
                        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                            item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                            item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                            else
                                item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                        }
                    }
                }


            }
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(data, "Compare/" + view)));

        }
        [HttpPost]
        public async Task<IActionResult> LoadDataApprove([FromBody] LoadDataCMNDReq req)
        {
            var user = GetUserGlobal();
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var ServiceURLLocal = _baseConfig["AppSettings:ServiceURLLocal"];
            var data = new CMND();
            var view = "";
            if (req.LoanBriefId > 0 && req.Step > 0)
            {
                var access_token = user.Token;
                var loanBrief = await loanBriefService.GetLoanInfoCompare(access_token, req.LoanBriefId);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                if (req.LstFiles != null && req.LstFiles.Count > 0)
                    data.lstFilesRoot = req.LstFiles;
                else
                {
                    var lstDocument = new List<int>();
                    lstDocument.Add((int)EnumDocumentType.ChanDungKH);
                    lstDocument.Add((int)EnumDocumentType.CVKD_In_Home_Customer_1);
                    lstDocument.Add((int)EnumDocumentType.CVKD_In_Home_Customer);
                    lstDocument.Add((int)EnumDocumentType.CVKD_CheckOut_Home_Customer);
                    data.lstFilesRoot = await loanBriefService.GetLoanBriefFileByDocumentMulti(access_token, req.LoanBriefId, lstDocument);
                }


                if (req.Step == 1)
                {
                    data.lstFilesCompare = loanBriefService.GetLoanBriefFile(access_token, req.LoanBriefId, (int)EnumDocumentType.CMND_CCCD).ToList();
                    view = "_ThamDinh_Step1";
                }
                if (data.lstFilesRoot != null && data.lstFilesRoot.Count > 0)
                {
                    foreach (var item in data.lstFilesRoot)
                    {
                        if (!string.IsNullOrEmpty(item.FileThumb))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                            else
                                item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                        }
                        if (string.IsNullOrEmpty(item.FilePath)) continue;
                        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                            item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                            item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                            else
                                item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                        }
                    }
                }
                if (data.lstFilesCompare != null && data.lstFilesCompare.Count > 0)
                {
                    foreach (var item in data.lstFilesCompare)
                    {
                        if (!string.IsNullOrEmpty(item.FileThumb))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                            else
                                item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                        }
                        if (string.IsNullOrEmpty(item.FilePath)) continue;
                        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                            item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                            item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                            else
                                item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                        }
                    }
                }

            }
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(data, "Compare/" + view)));

        }
        #endregion
        #region LogCompare
        public async Task<IActionResult> SaveLogCompare(int loanBriefId = 0, int tab = 0, bool isFinish = false)
        {
            var user = GetUserGlobal();
            var obj = new CompareDocument
            {
                LoanBriefId = loanBriefId,
                Tab = tab,
                UserId = user.UserId,
                UserName = user.Username,
                GroupId = user.GroupId ?? 0,
                Status = 1,
                CreatedAt = DateTime.Now,
                IsFinish = isFinish
            };
            var result = await _dictionaryService.SaveLogCompare(GetToken(), obj);
            if (result > 0)
                return Json(new { Status = 1 });
            else
                return Json(new { Status = 0 });
        }
        #endregion

        #region Tái cơ cấu lịch trả nợ
        [HttpGet]
        public IActionResult DebtRestructuring(int loanBriefId, int loanId)
        {
            //loanBriefId = 1040329;
            //loanId = 147974;
            ViewBag.LoanBriefId = loanBriefId;
            ViewBag.LoanId = loanId;

            return View();
        }
        [HttpPost]
        public IActionResult DebtRestructuring(DebtRestructuring.Input req)
        {
            var user = GetUserGlobal();
            var access_token = GetToken();
            req.UserId = user.UserId;
            req.UserName = user.Username;
            req.FullName = user.FullName;
            if (!string.IsNullOrEmpty(req.AppointmentDate))
                req.AppointmentDate = req.AppointmentDate.ToString().Replace("/", "-");
            if (req.TypeDebtRestructuring != 5)
                req.PercentDiscount = 0;
            //Cập nhập trạng thái telesales vào bảng loanbrief
            var result = _lmsService.DebtRestructuring(access_token, req);
            if (result != null)
            {
                if (result.Status == 1 && result.Data)
                {
                    //lưu lại vào bảng comment

                    var loanBriefNote = new LoanBriefNote()
                    {
                        LoanBriefId = (int)req.LoanBriefId,
                        UserId = user.UserId,
                        FullName = user.FullName,
                        ActionComment = (int)EnumActionComment.DebtRestructuring,
                        Note = string.Format("HĐ-{0} đã được tái cơ cấu lịch trả nợ", req.LoanBriefId),
                        Status = 1,
                        CreatedTime = DateTime.Now,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    };
                    loanBriefService.AddLoanBriefNote(access_token, loanBriefNote);

                    //Lưu ActionLog Restruct
                    _logActionService.AddLogActionRestruct(access_token, new LogActionRestruct
                    {
                        LoanbriefId = (int)req.LoanBriefId,
                        TypeAction = (int)req.TypeDebtRestructuring,
                        LoanTime = (int)req.NumberOfRepaymentPeriod,
                        FirstPayment = req.AppointmentDate,
                        PercenInterest = req.PercentDiscount,
                        CreatedAt = DateTime.Now,
                        Status = 1,
                        CreatedBy = user.UserId
                    });

                    return Json(new { status = 1, message = "Tái cơ cấu lịch trả nợ thành công" });
                }
                else
                    return Json(new { status = 0, message = result.Message });


            }
            else
                return Json(new { status = 0, message = "Có lỗi xảy ra, Vui lòng thử lại sau" });
        }

        [HttpPost]
        public async Task<IActionResult> InitDebtRevolvingLoan(int loanBriefId = 0, decimal money = 0, int rateTypeId = 0)
        {
            var user = GetUserGlobal();
            var access_token = user.Token;


            var loanBrief = await loanBriefService.GetLoanBriefById(access_token, loanBriefId);
            if (loanBrief == null || loanBrief.LoanBriefId == 0)
                return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay" });
            if (money <= 0)
                return Json(new { status = 0, message = "Số tiền đăng ký đơn vay phải lớn hơn 0đ" });

            //Kiểm tra đã có đơn đảo nợ chưa
            var check = await loanBriefService.CheckDebtRevolvingLoan(access_token, loanBriefId);
            if (!check)
                return Json(new { status = 0, message = "Đã có đơn tái cấu trúc khoản vay trên HĐ này!" });

            var model = loanBrief.MapToLoanbriefDebtRevolving();
            model.TypeRemarketing = (int)EnumTypeRemarketing.DebtRevolvingLoan;
            model.ReMarketingLoanBriefId = loanBriefId;
            model.LoanAmount = money;
            model.LoanBriefId = 0;
            model.CreatedTime = DateTime.Now;
            model.FromDate = DateTime.Now;
            model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);
            model.ReceivingMoneyType = (int)EnumReceivingMoneyType.TienMat;
            model.LoanAmountFirst = model.LoanAmount;
            model.CreateBy = user.UserId;
            model.RateTypeId = rateTypeId;
            if (model.LoanTime.GetValueOrDefault(0) == 0)
                model.LoanTime = 12;
            //if (model.RateTypeId == (int)EnumRateType.RateMonthADay)
            //    model.RateTypeId = (int)EnumRateType.TatToanCuoiKy;
            //else if (model.RateTypeId == (int)EnumRateType.RateAmortization30Days)
            //    model.RateTypeId = (int)EnumRateType.DuNoGiamDan;
            //Check rate
            if (loanBrief.RatePercent > 0 && loanBrief.RateMoney > 0)
            {
                if (model.ProductId == EnumProductCredit.OtoCreditType_CC.GetHashCode())
                {
                    var rate = Common.Utils.ProductPriceUtils.GetCalculatorRate(model.ProductId.Value, model.LoanAmount, model.LoanTime, model.RateTypeId);
                    model.RateMoney = rate.Item1;
                    model.RatePercent = rate.Item2;
                }
                else
                {
                    model.RateMoney = loanBrief.RateMoney;
                    model.RatePercent = loanBrief.RatePercent;
                }
            }
            else
            {
                if (model.ProductId > 0)
                {
                    var rate = Common.Utils.ProductPriceUtils.GetCalculatorRate(model.ProductId.Value, model.LoanAmount, model.LoanTime, model.RateTypeId);
                    model.RateMoney = rate.Item1;
                    model.RatePercent = rate.Item2;
                }
            }

            //mặc định không mua bảo hiểm
            model.BuyInsurenceCustomer = false;
            model.LoanBriefProperty.BuyInsuranceProperty = false;
            //Tính phí tất toán sớm
            model.FeePaymentBeforeLoan = Common.Utils.ProductPriceUtils.GetFeePaymentBeforeLoan(model.ProductId.Value, model.LoanTime);


            var result = await loanBriefService.InitDebtRevolvingLoan(access_token, model);
            if (result > 0)
            {
                //Lưu ActionLog Restruct
                await _logActionService.AddLogActionRestruct(access_token, new LogActionRestruct
                {
                    LoanbriefId = result,
                    LoanbriefParentId = loanBriefId,
                    TypeAction = ActionLogRestruct.CauTrucLaiKhoanVay.GetHashCode(),
                    LoanAmount = money,
                    CreatedAt = DateTime.Now,
                    Status = 1,
                    CreatedBy = user.UserId
                });
                //Thêm note khởi tạo đơn vay
                var note = new LoanBriefNote
                {
                    LoanBriefId = result,
                    Note = string.Format("{0}: Khởi tạo đơn cấu trúc lại khoản vay từ HĐ-{1} với hình thức tính lãi: {2}", user.FullName, loanBriefId, Description.GetDescription((EnumRateType)model.RateTypeId.Value)),
                    FullName = user.FullName,
                    UserId = user.UserId,
                    Status = 1,
                    ActionComment = EnumActionComment.InitDebtRevolvingLoan.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                };

                loanBriefService.AddLoanBriefNote(access_token, note);

                if (_pipelineService.ChangePipeline(access_token, new ChangePipelineReq() { loanBriefId = result, productId = model.ProductId.Value, status = (int)EnumLoanStatus.BRIEF_APPRAISER_LOAN_DISTRIBUTING }))
                {
                    var taskRun = new List<Task>();
                    var task1 = Task.Run(() => loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = string.Format("HĐ-{0} đã được chuyển lên TĐHS", loanBriefId),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    }));
                    taskRun.Add(task1);
                }

                //Định vị
                if (model.IsLocate.GetValueOrDefault(false) && !string.IsNullOrEmpty(model.DeviceId))
                {
                    var loanBriefNew = await loanBriefService.GetLoanBriefById(access_token, result);
                    //gọi api mở hợp đồng
                    var resultOpent = _trackDeviceService.OpenContract(access_token, "HD-" + result, model.DeviceId, user.ShopGlobal.ToString(), model.ProductId.Value, result, "topup");
                    //StatusCode == 3 => mã hợp đồng đã tồn tại
                    if (resultOpent != null && (resultOpent.StatusCode == 200 || resultOpent.StatusCode == 3))
                    {
                        if (resultOpent.StatusCode == 200)
                        {
                            loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                            {
                                LoanBriefId = result,
                                Note = string.Format("Đơn định vị: Tạo hợp đồng thành công với imei: {0}", model.DeviceId),
                                FullName = user.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = user.UserId,
                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            });
                            loanBriefNew.DeviceStatus = StatusOfDeviceID.OpenContract.GetHashCode();
                        }

                        loanBriefNew.ContractGinno = "HD-" + loanBriefNew.LoanBriefId;
                        //cập nhật db
                        var resultUpdate = loanBriefService.UpdateDevice(access_token, loanBriefNew);

                    }
                }

                return Json(new { status = 1, message = string.Format("Tạo đơn đảo nợ thành công HĐ-{0}", result) });
            }
            else
                return Json(new { status = 0, message = "Xảy ra lỗi khi tạo mới đơn vay. Vui lòng thử lại sau!" });

        }
        #endregion

        #region Tạo giao dịch đảm bảo
        [HttpPost]
        public async Task<IActionResult> TransactionsSecured(int loanBriefId)
        {
            try
            {
                var user = GetUserGlobal();
                var loan = await loanBriefService.GetLoanTransactionsSecured(user.Token, loanBriefId);
                if (loan != null)
                {
                    //Validate
                    var validMess = ValidTransactionSecered(loan);
                    if (!string.IsNullOrEmpty(validMess))
                        return Json(new { status = 0, message = validMess });
                    //Xử lý check giá trị khoản vay

                    //var loanAmount = await loanBriefService.GetTotalLoanAmountTranSactionSecured(user.Token, loan.CustomerId.Value);
                    var loanAmount = loan.LoanAmount;

                    //Thông tin người bảo đảm
                    var cus = new SecuringPartyItem
                    {
                        Name = loan.FullName,
                        NationalNumber = loan.NationalCard,
                        CountryId = "VN",
                        CityId = loan.ProvinceMap,
                        DistrictId = loan.DistrictMap,
                        Address = loan.Address

                    };
                    var lstCus = new List<SecuringPartyItem>();
                    lstCus.Add(cus);
                    var obj = new SercuredTransactionReq
                    {
                        ContractNumber = loan.LoanBriefId.ToString(),
                        ContractingDay = DateTime.Now.ToString("dd-MM-yyyy"),
                        LoanAmount = Convert.ToInt32(loanAmount),
                        SecuringParties = lstCus,
                        Description = loan.Description,
                        ExtraData = new ExtraData
                        {
                            VehiclePlate = loan.PlateNumberCar,
                            VehicleEngine = loan.Engine,
                            VehicleVin = loan.Chassis
                        }
                    };
                    var result = _securedTransaction.RegisterSecuredTransaction(obj, user.Token, loanBriefId);
                    if (result != null)
                    {
                        if ((result.Status == 1 || result.Status == 4) && result.Data != null)
                        {
                            //Lưu comment
                            var loanBriefNote = new LoanBriefNote()
                            {
                                LoanBriefId = loanBriefId,
                                UserId = user.UserId,
                                FullName = user.FullName,
                                ActionComment = (int)EnumActionComment.TransactionSecured,
                                Note = string.Format("HĐ-{0} đã được tạo giao dịch đảm bảo", loanBriefId),
                                Status = 1,
                                CreatedTime = DateTime.Now,
                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            };
                            loanBriefService.AddLoanBriefNote(user.Token, loanBriefNote);

                            //Lưu vào LogSecuredTransaction
                            var log = _dictionaryService.AddLogSecuredTransaction(user.Token, new LogSecuredTransaction
                            {
                                LoanBriefId = loanBriefId,
                                CreatedAt = DateTime.Now,
                                NoticeNumber = result.Data.NoticeNumber,
                                Pin = result.Data.Pin,
                                LoanAmount = Convert.ToDecimal(loanAmount),
                                Status = result.Status,
                                UserId = user.UserId
                            });
                            return Json(new { status = 1, message = "Đơn vay đã được tạo giao dịch đảm bảo!" });
                        }
                        else
                            return Json(new { status = 0, message = result.Message });

                    }
                    else
                        return Json(new { status = 0, message = "Có lỗi xảy ra trong quá trình tạo giao dịch. Vui lòng liên hệ kỹ thuật!" });
                }
                else
                {
                    return Json(new { status = 0, message = "Không tìm thấy thông tin khoản vay. Vui lòng thử lại sau!" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        public string ValidTransactionSecered(LoanTransactionsSecured entity)
        {
            var mess = "";
            if(string.IsNullOrEmpty(entity.FullName))
            {
                mess = "Chưa có thông tin tên khách hàng";
                return mess;
            }
            if (string.IsNullOrEmpty(entity.NationalCard))
            {
                mess = "Chưa có thông tin CMND khách hàng";
                return mess;
            }
            if (string.IsNullOrEmpty(entity.ProvinceMap) || string.IsNullOrEmpty(entity.DistrictMap) || string.IsNullOrEmpty(entity.Address))
            {
                mess = "Chưa có thông tin địa chỉ nơi ở khách hàng";
                return mess;
            }            
            if (string.IsNullOrEmpty(entity.PlateNumberCar))
            {
                mess = "Chưa có thông tin biển số xe";
                return mess;
            }
            if (string.IsNullOrEmpty(entity.Chassis))
            {
                mess = "Chưa có thông tin số khung xe";
                return mess;
            }
            if (string.IsNullOrEmpty(entity.Engine))
            {
                mess = "Chưa có thông tin số máy xe";
                return mess;
            }
            if (string.IsNullOrEmpty(entity.Description))
            {
                mess = "Chưa có thông tin mô tả xe";
                return mess;
            }
            
            return mess;
        }

        public async Task<IActionResult> CheckStatusTransactionSecured(int loanBriefId)
        {
            var user = GetUserGlobal();
            var log = _dictionaryService.GetLastLogSecuredTransaction(user.Token, loanBriefId);
            if (log != null)
            {
                var result = _securedTransaction.CheckStatusSecuredTransaction(log.NoticeNumber, user.Token, loanBriefId);
                if (result != null && result.Status == 1)
                {
                    //Kiểm tra nếu trạng thái đã tạo thành công thì update lại
                    if (result.Data.StatusCode > 0)
                    {
                        var obj = new UpdateTransactionStateReq
                        {
                            LoanBriefId = loanBriefId,
                            Status = result.Data.StatusCode
                        };
                        loanBriefService.UpdateTransactionState(user.Token, obj);
                    }

                    return Json(new { status = 1, message = result.Data.Status });
                }
                else
                    return Json(new { status = 0, message = "Có lỗi xảy ra, vui lòng thử lại sau!" });

            }
            else
                return Json(new { status = 0, message = "" });

        }
        #endregion
    }
}