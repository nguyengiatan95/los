﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using static LOS.WebApp.Models.Report;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class BODController : BaseController
    {

        private ILoanBriefService _loanBriefService;
        private IBODService _bodService;
        private IDictionaryService _dictionaryService;
        private IConfiguration _configuration;
        private IHttpClientFactory clientFactory;
        private IShopServices _shopServices;

        public BODController(IConfiguration configuration, ILoanBriefService loanService, IDictionaryService dictionaryService, IBODService bodService,
            IHttpClientFactory clientFactory, IShopServices shopServices) : base(configuration: configuration, clientFactory)
        {
            _loanBriefService = loanService;
            _dictionaryService = dictionaryService;
            _configuration = configuration;
            _bodService = bodService;
            this.clientFactory = clientFactory;
            _shopServices = shopServices;
        }

        [Route("/bod/index.html")]
        [PermissionFilter]
        public IActionResult Index()
        {
            var user = GetUserGlobal();
            return View(user);

        }

        public IActionResult Search()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new BodDatatable(HttpContext.Request.Form);
                int recordsTotal = 0;
                // Query api   	
                var data = _bodService.Search(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Check quyền tương tác data
                if(data != null && data.Count > 0)
                {
                    if(user != null && user.GroupId != (int)EnumGroupUser.TP_KSNB)
                    {
                        foreach (var item in data)
                        {
                            item.IsBack = true;
                            item.IsCancel = true;
                            item.IsPush = true;
                        }
                        
                    }    
                }    
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult FormConfirm(int LoanBriefId, long LoanAmount)
        {
            ViewBag.LoanBriefId = LoanBriefId;
            ViewBag.LoanAmount = LoanAmount;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ConfirmLoanBrief(int LoanBriefId, decimal LoanAmountEdit)
        {
            try
            {
                var user = GetUserGlobal();
                var loanbrief = await _loanBriefService.GetLoanBriefById(GetToken(), LoanBriefId);
                if (loanbrief != null)
                {
                    if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn vay đã được hủy trước đó.", null));

                    if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
                        return Json(new { status = 0, message = "Đơn đang được xử lý. Vui lòng thử lại sau!" });
                    //Check số tiền thay đổi thì update lại
                    if (loanbrief.LoanAmount != LoanAmountEdit)
                    {
                        loanbrief.LoanAmount = LoanAmountEdit;
                    }

                    loanbrief.ActionState = EnumActionPush.Push.GetHashCode();
                    loanbrief.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                    var resultPush = _loanBriefService.PushLoanBrief(GetToken(), loanbrief);
                    if (resultPush > 0)
                    {
                        var action = EnumActionComment.BusinessManagePush.GetHashCode();
                        if (user.GroupId == (int)EnumGroupUser.BOD)
                            action = EnumActionComment.BODPush.GetHashCode();
						else if (user.GroupId == (int)EnumGroupUser.TP_KSNB)
                            action = EnumActionComment.TP_KSNB_Push.GetHashCode();
                        else if (user.GroupId == (int)EnumGroupUser.ManagerHub)
                            action = EnumActionComment.QLKV_Push.GetHashCode();

                        //Thêm note
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = resultPush,
                            Note = string.Format("Hợp đồng HĐ-{0} đã được {1} duyệt đơn với số tiền là: {2}", LoanBriefId, user.GroupId == (int)EnumGroupUser.ManagerHub ? "Quản lý khu vực" : user.Group.GroupName, Convert.ToInt64(LoanAmountEdit).ToString("##,#")),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = action,
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        _loanBriefService.AddLoanBriefNote(GetToken(), note);
                        //ThreadPool.QueueUserWorkItem(item => _loanBriefService.AddLoanBriefNote(GetToken(), note));

                        return Json(new { status = 1, message = "Đẩy hợp đồng thành công" });
                    }
                    else
                        return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });
                }
                else
                {
                    return Json(new { status = 0, message = "Không tìm thấy thông tin khoản vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }


        [Route("/businessmanager/index.html")]
        [PermissionFilter]
        public IActionResult BusinessManagerIndex()
        {
            var user = GetUserGlobal();
            return View(user);

        }

        public IActionResult BusinessManagerSearch()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new BusinessDatatable(HttpContext.Request.Form);
                int recordsTotal = 0;

                if (user.GroupId == (int)EnumGroupUser.ManagerHub && user != null && user.ListShop.Count > 0)
                    modal.shops = string.Join(",", user.ListShop.Select(c => c.ShopId.ToString()).ToArray<string>());
                // Query api   	
                var data = _bodService.BusinessManagerSearch(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();

                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region KSNB
        [Route("/internalcontrol/index.html")]
        [PermissionFilter]
        public IActionResult InternalControlIndex()
        {
            var user = GetUserGlobal();
            var access_token = user.Token;
            var listHub = _shopServices.GetAllHub(access_token);
            return View(listHub);

        }

        public IActionResult InternalControlSearch()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new InternalControlDatatable(HttpContext.Request.Form);
                int recordsTotal = 0;
                modal.status = EnumLoanStatus.DISBURSED.GetHashCode().ToString();
                DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
                DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
                if (!string.IsNullOrEmpty(modal.DateRanger))
                    DateTimeUtility.ConvertDateRangerV2(modal.DateRanger, ref fromDate, ref toDate);

                modal.FromDate = fromDate.ToString("yyyy/MM/dd");
                modal.ToDate = toDate.ToString("yyyy/MM/dd");
                // Query api   	
                var data = _bodService.InternalControlSearch(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();

                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Produces("application/json")]
        public IActionResult ExportInternalControll(string loanBriefId, string search, string hubId, string locate, string filtercreateTime)
        {
            var user = GetUserGlobal();
            var modal = new ReportInternalControll();

            modal.loanBriefId = loanBriefId;
            modal.search = search;
            modal.status = EnumLoanStatus.DISBURSED.GetHashCode().ToString();
            modal.hubId = hubId;
            modal.Locate = locate;
            modal.DateRanger = filtercreateTime;
            modal.export = 1;

            DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
            DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
            if (!string.IsNullOrEmpty(modal.DateRanger))
                DateTimeUtility.ConvertDateRangerV2(modal.DateRanger, ref fromDate, ref toDate);

            modal.FromDate = fromDate.ToString("yyyy/MM/dd");
            modal.ToDate = toDate.ToString("yyyy/MM/dd");
            int recordsTotal = 0;
            // Query api   	
            var data = _bodService.InternalControlSearch(GetToken(), modal.ToQueryObject(), ref recordsTotal);
            List<string> lstHeaderLoan = new List<string> { "STT", "Mã HĐ", "Khách hàng", "Quận/Huyện", "Thời gian GN", "Tiền vay", "Gói vay", "Thời gian vay", "Trạng thái" };
            List<int> lstWidthLoan = new List<int> { 10, 20, 30, 15, 15, 30, 30, 20, 20 };
            List<ExcelHorizontalAlignment> lstAlign = new List<ExcelHorizontalAlignment> { ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Right,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left
                                                                                           };

            List<System.Drawing.Color> lstColor = new List<System.Drawing.Color> { Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black
                                                                                  };

            byte[] fileContents;
            ExcelPackage package = new ExcelPackage();

            var ws = package.Workbook.Worksheets.Add("Kiểm soát sau giải ngân");
            // Set Font
            ws.Cells.Style.Font.Name = "Calibri";
            ws.Cells.Style.Font.Size = 11;

            // Format All Cells
            ws.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            // Format All Column
            for (int i = 1; i <= lstHeaderLoan.Count; i++)
            {
                ws.Column(i).Width = lstWidthLoan[i - 1];
                ws.Column(i).Style.HorizontalAlignment = lstAlign[i - 1];
                ws.Column(i).Style.Font.Color.SetColor(lstColor[i - 1]);
            }

            int iPosHeader = 1;
            // mapping title header
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(77, 119, 204));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Font.Bold = true;
            ws.Row(iPosHeader).Height = 16;
            for (int i = 1; i <= lstHeaderLoan.Count; i++)
            {
                ws.Cells[iPosHeader, i].Value = lstHeaderLoan[i - 1];
            }

            // SET VALUE
            iPosHeader++;


            int j = 1;
            var statuStr = "Đã giải ngân";
            for (int i = 0; i < data.Count(); i++)
            {
                var Reborrow = "";
                if (data[i].IsReborrow == true)
                    Reborrow = "(Tái vay)";
                ws.Cells[iPosHeader, 1].Value = j;
                ws.Cells[iPosHeader, 2].Value = "HĐ" + data[i].LoanBriefId + Reborrow;
                ws.Cells[iPosHeader, 3].Value = data[i].FullName;
                ws.Cells[iPosHeader, 4].Value = data[i].District.Name + "/" + data[i].Province.Name;
                ws.Cells[iPosHeader, 5].Value = Convert.ToDateTime(data[i].DisbursementAt.ToString()).ToString("dd/MM/yyyy");
                ws.Cells[iPosHeader, 6].Value = Convert.ToInt64(data[i].LoanAmount).ToString("#,##");
                ws.Cells[iPosHeader, 7].Value = data[i].LoanProduct.Name;
                ws.Cells[iPosHeader, 8].Value = data[i].LoanTime + " tháng";
                ws.Cells[iPosHeader, 9].Value = statuStr;
                iPosHeader++;
                j++;
            }
            fileContents = package.GetAsByteArray();
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: "kiem-soat-sau-giai-ngan.xlsx"
            );
            //Response.ContentType = "application/vnd.ms-excel";
            //HttpContext.Response.Headers.Add("content-disposition", "attachment;  filename=" + string.Format("kiem-soat-sau-giai-ngan.xls"));


            //return new EmptyResult();
        }
        #endregion
    }
}