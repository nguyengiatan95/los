﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.ViewEngines;

namespace LOS.WebApp.Controllers
{
    public class BaseController : Controller
    {
        private IModuleService _moduleService;
        protected IConfiguration _baseConfig;
        // protected ICompositeViewEngine viewEngine;
        public BaseController(IConfiguration configuration, IHttpClientFactory clientFactory)
        {
            _moduleService = new ModuleService(configuration, clientFactory);
            _baseConfig = configuration;
            //this.viewEngine = viewEngine;

        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var user = GetUserGlobal();
            if (user == null || string.IsNullOrEmpty(user.Token))
                filterContext.Result = new RedirectResult(_baseConfig["AppSettings:SSO"]);
                //filterContext.Result = new RedirectResult(_baseConfig["AppSettings:SSO"]);          
                
            //else
            //{
            //    var controller = filterContext.RouteData.Values["Controller"].ToString();
            //    var action = filterContext.RouteData.Values["Action"].ToString();
            //    if (!string.IsNullOrEmpty(controller) && !string.IsNullOrEmpty(action))
            //    {
            //        var module = _moduleService.Get(user.Token, controller.AsToLower(), action.AsToLower());
            //        if (module.ModuleId > 0)
            //        {
            //            if (!user.Modules.Any(x => x.ModuleId == module.ModuleId))
            //                filterContext.Result = new RedirectResult("/error.html");
            //        }
            //    }
            //}
            //check permission

            base.OnActionExecuted(filterContext);
        }
        protected string GetToken()
        {
            return HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result.Token;
        }
        protected UserDetail GetUserGlobal()
        {
            return HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result;
        }

        public virtual object GetBaseObjectResult(bool isSuccess = true, string msg = "", object data = null, string html = "")
        {
            return new
            {
                IsSuccess = isSuccess,
                Message = msg,
                Data = data,
                Html = html
            };
        }
        //protected string RenderViewAsString(object model, string viewName = null)
        //{
        //    viewName = viewName ?? ControllerContext.ActionDescriptor.ActionName;
        //    ViewData.Model = model;

        //    using (StringWriter sw = new StringWriter())
        //    {
        //        IView view = viewEngine.FindView(ControllerContext, viewName, true).View;
        //        ViewContext viewContext = new ViewContext(ControllerContext, view, ViewData, TempData, sw, new HtmlHelperOptions());

        //        view.RenderAsync(viewContext).Wait();

        //        return sw.GetStringBuilder().ToString();
        //    }
        //}


    }
}