﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;

namespace LOS.WebApp.Controllers
{
    public class BaseV2Controller : Controller
    {
        private IModuleService _moduleService;
        protected IConfiguration _baseConfig;
        protected ICompositeViewEngine _viewEngine;
        public BaseV2Controller(IConfiguration configuration, IHttpClientFactory clientFactory, ICompositeViewEngine viewEngine)
        {
            _moduleService = new ModuleService(configuration, clientFactory);
            _baseConfig = configuration;
            this._viewEngine = viewEngine;

        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var user = GetUserGlobal();
            if (user == null || string.IsNullOrEmpty(user.Token))
                filterContext.Result = new RedirectResult(_baseConfig["AppSettings:SSO"]);
            base.OnActionExecuted(filterContext);
        }
        protected string GetToken()
        {
            return HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result.Token;
        }
        protected UserDetail GetUserGlobal()
        {
            return HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result;
        }

        public virtual object GetBaseObjectResult(bool isSuccess = true, string msg = "", object data = null, string html = "")
        {
            return new
            {
                IsSuccess = isSuccess,
                Message = msg,
                Data = data,
                Html = html
            };
        }
        protected string RenderViewAsString(object model, string viewName = null)
        {
            viewName = viewName ?? ControllerContext.ActionDescriptor.ActionName;
            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                IView view = this._viewEngine.FindView(ControllerContext, viewName, true).View;
                ViewContext viewContext = new ViewContext(ControllerContext, view, ViewData, TempData, sw, new HtmlHelperOptions());

                view.RenderAsync(viewContext).Wait();

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}
