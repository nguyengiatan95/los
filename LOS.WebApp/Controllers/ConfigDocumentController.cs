﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using LOS.WebApp.Services;
using System.Net.Http;
using LOS.WebApp.Cache;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.DAL.EntityFramework;

namespace LOS.WebApp.Controllers
{
    public class ConfigDocumentController : BaseController
    {
        private IConfiguration _baseConfig;
        private IDictionaryService _dictionaryService;
        private IHttpClientFactory _clientFactory;
        public ConfigDocumentController(IConfiguration configuration, IDictionaryService dictionaryService, IHttpClientFactory clientFactory)
            : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
            _dictionaryService = dictionaryService;
            _clientFactory = clientFactory;
        }

        [Route("config-document.html")]
        [PermissionFilter]
        //[PermissionFilter(ModuleCode = "USER_MANAGER", Permission = "SEARCH")]
        public async Task<IActionResult> Index()
        {
            var lstProduct = CacheData.ListLoanProduct;
            return View(lstProduct);
        }

        public async Task<IActionResult> GetData(int ProductId, int TypeOwnerShip)
        {
            var user = GetUserGlobal();
            ViewBag.ProductId = ProductId;
            ViewBag.TypeOwnerShip = TypeOwnerShip;
            var lstDocument = _dictionaryService.GetDocumentAll(user.Token);
            var lstConfig = _dictionaryService.GetConfigDocument(user.Token, ProductId, TypeOwnerShip);

            var lst = Ultility.ConvertDocumentTree(lstDocument, lstConfig, ProductId, TypeOwnerShip);

            var lstProduct = CacheData.ListLoanProduct;
            ViewBag.ListProduct = lstProduct;
            ViewBag.ListData = lst;
            return View();

        }
        public async Task<IActionResult> GetDocByProduct(int ProductId)
        {
            var user = GetUserGlobal();
            var lst = new List<DocumentTypeNew>();
            var lstDocument = _dictionaryService.GetDocumentType(user.Token, ProductId);
            if (lstDocument != null && lstDocument.Count > 0)
                lst = Ultility.GetListDocumentNew(lstDocument);
            return View(lst);
        }

        [HttpPost]
        public IActionResult AddConfig(List<LOS.DAL.DTOs.DocumentMapConfig> listDataForm)
        {
            try
            {
                var user = GetUserGlobal();
                if (listDataForm != null && listDataForm.Count > 0)
                {
                    //listDataForm = listDataForm.Where(x => x.IsChecked == true).ToList();
                    if (listDataForm != null && listDataForm.Count > 0)
                    {
                        var result = 0;
                        foreach (var item in listDataForm)
                        {
                            var obj = new ConfigDocument
                            {
                                Id = item.ConfigDocument.Id,
                                DocumentTypeId = item.Id,
                                ProductId = item.ConfigDocument.ProductId,
                                MinImage = item.ConfigDocument.MinImage,
                                Required = item.ConfigDocument.Required,
                                TypeRequired = item.ConfigDocument.TypeRequired,
                                AllowFromUpload = item.ListAllowFromUpload != null ? string.Join(",", item.ListAllowFromUpload) : null,
                                GroupJobId = item.ListGroupJobId != null ? string.Join(",", item.ListGroupJobId) : null,
                                TypeOwnerShip = item.ConfigDocument.TypeOwnerShip,
                                ParentId = item.ParentId,
                                IsEnable = item.IsChecked != null && item.IsChecked == true ? true : false,
                                CreatedBy = user.UserId,
                                CreatedAt = DateTime.Now,
                                Sort = item.ConfigDocument.Sort
                            };
                            result = _dictionaryService.AddConfigDocument(user.Token, obj);

                        }
                        return Json(new { status = 1, message = "Cập nhật thành công!" });
                    }
                }

                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng liên hệ kỹ thuật!" });
            }

            catch (Exception ex)
            {
                throw;
            }
        }

    }
}