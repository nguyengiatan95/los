﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using LOS.WebApp.Services;
using AutoMapper;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Models;
using System.Reflection;
using LOS.DAL.EntityFramework;
using LOS.Common.Utils;
using LOS.WebApp.Models.Request;
using System.Net.Http;
using LOS.WebApp.Services.ContactCustomer;
using LOS.WebApp.Models.ContactCustomer;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using LOS.WebApp.Cache;
using LOS.Common.Extensions;
using Serilog;

namespace LOS.WebApp.Controllers
{
    public class ContactCustomerController : BaseV2Controller
    {

        private IContactCustomerService _iContactCustomerService;
        private ILoanBriefService _iLoanBriefService;
        private IDictionaryService _dictionaryService;
        private IMapper _mapper;
        private ICompositeViewEngine _viewEngine;
        private IHttpClientFactory _clientFactory;
        private IConfiguration _configuration;
        private IHttpClientFactory clientFactory;


        public ContactCustomerController(
            IContactCustomerService iContactCustomerService,
            ILoanBriefService iLoanBriefService,
            IDictionaryService dictionaryService,
            IConfiguration configuration,
            IHttpClientFactory clientFactory,
            ICompositeViewEngine viewEngine
                      ) : base(configuration, clientFactory, viewEngine)
        {
            this._iContactCustomerService = iContactCustomerService;
            this._iLoanBriefService = iLoanBriefService;
            this._clientFactory = clientFactory;
            this._dictionaryService = dictionaryService;

        }
        [PermissionFilter]
        [Route("/contact/index.html")]
        public IActionResult Index()
        {
            var user = GetUserGlobal();
            ViewBag.ListProvince = _dictionaryService.GetProvince(GetToken());
            ViewBag.User = user;
            return View();
        }

        public IActionResult GetDataContactCustomer()
        {
            var user = GetUserGlobal();
            var modal = new ContactCustomerDatatable(HttpContext.Request.Form);
            modal.createBy = user.UserId;
            int recordsTotal = 0;
            // Query api   	
            List<ContactCustomerDetails> data = new List<ContactCustomerDetails>();
            data = _iContactCustomerService.Search(GetToken(), modal.ToQueryObject(), ref recordsTotal);
            modal.total = recordsTotal.ToString();
            return Json(new { meta = modal, data = data });
        }

        [PermissionFilter]
        public async Task<IActionResult> Init(int id = 0)
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            var model = new ContactCustomerViewModel();
            if (id > 0)
            {
                var result = await _iContactCustomerService.GetContactCustomerById(GetToken(), id);
                model = MapperExtentions.MapContactCustomerToView(result);
            }
            ViewBag.ListProvince = CacheData.ListProvince;
            ViewBag.ListJobs = CacheData.ListJobs;
            ViewBag.ListBrandProduct = CacheData.ListBrandProduct;
            ViewBag.ListLoanProduct = CacheData.ListLoanProduct;
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "Init")));
        }
        public async Task<IActionResult> InitContactCustomer(ContactCustomerViewModel model)
        {
            try
            {
                var user = GetUserGlobal();
                var obj = MapperExtentions.MapToContactCustomer(model);
                obj.Status = 1;
                if (model.ContactId > 0)
                {
                    obj.ModifyDate = DateTime.Now;
                    var result = _iContactCustomerService.Update(GetToken(), obj.ContactId, obj);
                    if (result)
                    {
                        return Json(new { status = 1, message = "Thành công!" });
                    }
                }
                else
                {
                    var checkPhoneExist = await _iContactCustomerService.GetContactCustomerByPhone(GetToken(), model.Phone);
                    if (checkPhoneExist != null && checkPhoneExist.ContactId > 0)
                    {
                        return Json(new { status = 0, message = "Số điện thoại đã được tạo liên hệ" });
                    }
                    var checkPhoneExistLoanBrief = _iLoanBriefService.GetLoanBriefByPhone(GetToken(), model.Phone);
                    if (checkPhoneExistLoanBrief != null && checkPhoneExistLoanBrief.Count > 0)
                    {
                        return Json(new { status = 0, message = "Số điện thoại đã được tạo liên hệ" });
                    }
                    obj.CreateBy = user.UserId;
                    obj.CreateDate = DateTime.Now;
                    var result = _iContactCustomerService.AddContactCustomer(GetToken(), obj);
                    if (result > 0)
                    {
                        return Json(new { status = 1, message = "Thành công!" });
                    }
                }

                return Json(new { status = 0, message = "Xảy ra lỗi khi lưu .Vui lòng thử lại sau!" });
            }
            catch (Exception ex)
            {
                //Log.Error(ex, "Web LoanbriefV2/InitLoanBrief Exception");
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        public async Task<IActionResult> CreateLoanBrief(int ContactId)
        {
            var user = GetUserGlobal();
            var access_token = user.Token;
            var model = new LoanbriefViewModel();
            if (user == null)
                return Json(new { status = 0, message = "Lỗi. Vui lòng đăng nhập lại!" });
            try
            {
                #region Thêm mới thông tin đơn vay
                model.CreateBy = user.UserId;
                //nếu tạo đơn + user group telesales
                if (model.LoanBriefId == 0 && user.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                {
                    model.HubId = user.ShopGlobal;
                    model.IsHeadOffice = false;
                    model.HubEmployeeId = user.UserId;
                }
                else if (model.LoanBriefId == 0 && user.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                {
                    model.IsHeadOffice = false;
                    model.HubId = user.ShopGlobal;
                    model.HubEmployeeId = user.UserId;
                }
                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                var entity = model.MapToLoanbrief();
                entity.UtmSource = user.Username;


                var objcontact = await _iContactCustomerService.GetContactCustomerById(GetToken(), ContactId);
                if (objcontact != null && objcontact.ContactId > 0)
                {
                    var checkPhoneExistLoanBrief = _iLoanBriefService.GetLoanBriefByPhone(GetToken(), objcontact.Phone);
                    if (checkPhoneExistLoanBrief != null && checkPhoneExistLoanBrief.Count > 0)
                    {
                        return Json(new { status = 0, message = "Số điện thoại đã được tạo liên hệ" });
                    }
                    //map info contact
                    entity = MapperExtentions.MapContactCustomerToLoanbrief(objcontact, entity);
                    //insert loanbrief

                    var result = await _iLoanBriefService.InitLoanBriefV2(access_token, entity);
                    if (result > 0)
                    {
                        objcontact.LoanBriefId = result;
                        //update loanbrief => contact
                        var updateContact = _iContactCustomerService.Update(GetToken(), ContactId, objcontact);
                        //Thêm note khởi tạo đơn vay
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = result,
                            Note = string.Format("{0}: Khởi tạo đơn vay từ contact", user.FullName),
                            FullName = user.FullName,
                            UserId = user.UserId,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        if (!string.IsNullOrEmpty(model.Comment))
                            note.Note += "<br />" + model.Comment;
                        _iLoanBriefService.AddLoanBriefNote(access_token, note);
                        return Json(new { status = 1, message = string.Format("Tạo thành công đơn vay HĐ-{0}", result) });
                    }
                    else
                        return Json(new { status = 0, message = "Xảy ra lỗi khi tạo mới đơn vay. Vui lòng thử lại sau!" });
                }
                return Json(new { status = 0, message = "Thông tin liên hệ không đúng. Vui lòng thử lại!" });
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Web ContactCustomer/CreateLoanBrief Exception");
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        public async Task<IActionResult> DeleteContactCustomer(int ContactId)
        {
            try
            {
                var user = GetUserGlobal();
                var objcontact = await _iContactCustomerService.GetContactCustomerById(GetToken(), ContactId);
                objcontact.Status = 0;
                objcontact.ModifyDate = DateTime.Now;
                var result = _iContactCustomerService.Update(GetToken(), objcontact.ContactId, objcontact);
                if (result)
                {
                    return Json(new { status = 1, message = "Thành công!" });
                }
                return Json(new { status = 0, message = "Xảy ra lỗi khi lưu .Vui lòng thử lại sau!" });
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
    }
}