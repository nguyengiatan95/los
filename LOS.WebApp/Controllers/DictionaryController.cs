﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.Models.LMS;
using LOS.Services.Services.District;
using LOS.Services.Services.LMS;
using LOS.Services.Services.Province;
using LOS.Services.Services.Ward;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Dictionary;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class DictionaryController : BaseController
    {
        private IProvinceService _provinceService;
        private IDistrictService _districtService;
        private IWardService _wardService;
        private ILMSV2Service _lmsV2Service;

        private IDictionaryService dictionaryService;
        private ILoanBriefService loanBriefService;
        private IHttpClientFactory clientFactory;
        private ILoanStatusDetailService _loanStatusDetailService;

        public DictionaryController(IConfiguration configuration, IDictionaryService services, ILoanBriefService loanServices,
            IHttpClientFactory clientFactory, ILoanStatusDetailService loanStatusDetailService, IProvinceService provinceService,
            IDistrictService districtService, IWardService wardService, ILMSV2Service lmsV2Service) : base(configuration: configuration, clientFactory)
        {
            this.dictionaryService = services;
            this.loanBriefService = loanServices;
            this.clientFactory = clientFactory;
            _loanStatusDetailService = loanStatusDetailService;
            this._provinceService = provinceService;
            this._districtService = districtService;
            this._wardService = wardService;
            this._lmsV2Service = lmsV2Service;
        }

        public IActionResult GetJob()
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetJob(GetToken());
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetProvince()
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetProvince(GetToken());
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetDistrict([FromQuery] int id)
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetDistrict(GetToken(), id);
                if (data != null && data.Count > 0)
                    data = data.OrderBy(x => x.Name).ToList();
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetWard([FromQuery] int districtId)
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetWard(GetToken(), districtId);
                if (data != null && data.Count > 0)
                    data = data.OrderBy(x => x.Name).ToList();
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetProduct([FromQuery] int brandId)
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetProduct(GetToken(), brandId);
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult GetLoanProduct()
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetLoanProduct(GetToken());
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult GetStatus()
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetLoanStatus(GetToken());
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetUtmSource()
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetUtmSource(GetToken());
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetPlatformType()
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetPlatformType(GetToken());
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IActionResult> GetUserByGroupId([FromQuery] int groupId)
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetUserByGroupId(GetToken(), groupId);
                //DiepNH check nếu là nhóm telesale thì count số đơn vay theo từng tele
                int count = 0;
                if (groupId == Common.Extensions.EnumGroupUser.ApproveEmp.GetHashCode())
                {
                    for (int i = 0; i < data.Count; i++)
                    {

                        count = await loanBriefService.CountLoanByApproveId(GetToken(), data[i].UserId);
                        data[i].CountLoan = count;
                        //int count = data.Count();
                    }
                }
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetReasonCancel()
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetReasonCancel(GetToken());
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetShop()
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetShop(GetToken());
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult GetShopByCityId([FromQuery] int cityId)
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetShopByCityId(GetToken(), cityId);
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetLoanStatusDetailChild([FromQuery] int id)
        {
            try
            {
                var data = _loanStatusDetailService.GetLoanStatusDetailChild(GetToken(), id);

                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult GetInfomationProductDetailById([FromQuery] int Id)
        {
            try
            {
                var data = dictionaryService.GetInfomationProductDetailById(GetToken(), Id);
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetInfomationProductDetailByTypeOwnership([FromQuery] int TypeOwnership)
        {
            try
            {
                var data = dictionaryService.GetInfomationProductDetailByTypeOwnership(GetToken(), TypeOwnership);
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetInfomationProductDetailByTypeOwnershipAndProduct(int TypeOwnership, int ProductId, int TypeLoanBrief)
        {
            try
            {
                var data = dictionaryService.GetInfomationProductDetailByTypeOwnershipAndProduct(GetToken(), TypeOwnership, ProductId, TypeLoanBrief);
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetInfomationProductDetailByProductDetailIdAndProduct(int ProductdetailId, int ProductId)
        {
            try
            {
                var data = dictionaryService.GetInfomationProductDetailByProductDetailIdAndProduct(GetToken(), ProductdetailId, ProductId);
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult GetConfigDepartmentStatus([FromQuery] int DepartmentId)
        {
            try
            {
                // Query api   	
                var data = dictionaryService.GetConfigDepartmentStatus(GetToken(), DepartmentId);
                //Returning Json Data  
                return Json(new { status = 1, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Quản lý Tỉnh/Thành phố
        [Route("/dictionary/manage-province.html")]
        [PermissionFilter]
        public IActionResult ManageProvince()
        {
            return View();
        }

        [HttpPost]
        public IActionResult LoadDataProvince()
        {
            try
            {
                var modal = new ProvinceDatatable(HttpContext.Request.Form);
                #region Gán giá trị mặc định cho param
                if (string.IsNullOrEmpty(modal.status))
                    modal.status = "1";
                #endregion

                int recordsTotal = 0;
                // Query api   
                var data = _provinceService.Search(modal.searchName, int.Parse(modal.status), int.Parse(modal.page), int.Parse(modal.perpage), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateIsApplyProvince(int provinceId, int isApply)
        {
            var result = await _provinceService.UpdateIsApplyProvince(provinceId, isApply);
            if (result)
                return Json(new { status = 1, message = "Cập nhập thành công" });
            else
                return Json(new { status = 0, message = "Cập nhập thất bại. Vui lòng liên hệ phòng kĩ thuật" });
        }

        #endregion

        #region Quản lý Quận/Huyện
        [Route("/dictionary/manage-district.html")]
        [PermissionFilter]
        public async Task<IActionResult> ManageDistrict()
        {
            //lấy danh sách tất cả thành phố
            ViewBag.ListProvince = await _provinceService.GetAll();
            return View();
        }

        [HttpPost]
        public IActionResult LoadDataDistrict()
        {
            try
            {
                var modal = new DistrictDatatable(HttpContext.Request.Form);
                #region Gán giá trị mặc định cho param
                if (string.IsNullOrEmpty(modal.status))
                    modal.status = "1";
                if (string.IsNullOrEmpty(modal.provinceId))
                    modal.provinceId = "1";
                #endregion
                int recordsTotal = 0;
                // Query api   
                var data = _districtService.Search(modal.searchName, int.Parse(modal.provinceId), int.Parse(modal.status), int.Parse(modal.page), int.Parse(modal.perpage), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateIsApplyDistrict(int districtId, int isApply)
        {
            var result = await _districtService.UpdateIsApplyDistrict(districtId, isApply);
            if (result)
                return Json(new { status = 1, message = "Cập nhập thành công" });
            else
                return Json(new { status = 0, message = "Cập nhập thất bại. Vui lòng liên hệ phòng kĩ thuật" });
        }

        [HttpPost]
        public async Task<IActionResult> AddOrUpdateDistrict(DAL.EntityFramework.District entity)
        {
            //Cập nhập
            if (entity.DistrictId > 0)
            {
                var update = await _districtService.Update(entity);
                if (update)
                    return Json(new { status = 1, message = "Cập nhập Quận/Huyện thành công" });
                else
                    return Json(new { status = 1, message = "Cập nhập Quận/Huyện thất bại. Vui lòng liên hệ phòng kỹ thuật" });
            }
            //Thêm mới
            else
            {
                //kiểm tra xem trong db có chưa
                //chưa có mới cho tạo mới
                var checkDistrict = await _districtService.CheckDistrict(entity.Name, entity.ProvinceId.Value);
                if (checkDistrict)
                {
                    //gọi api thêm mới của LMS
                    var objCreateDistrict = new CreateDistrict()
                    {
                        CityId = entity.ProvinceId.Value,
                        Name = entity.Name,
                        TypeDistrict = Description.GetDescription((TypeDistrict)entity.Type)
                    };
                    var create = _lmsV2Service.CreateDistrict(objCreateDistrict);
                    if (create != null && create.Data > 0 && create.Status == 1)
                    {
                        //tạo thành công bên LMS thì tạo xuống LOS dùng ID của LMS làm ID của LOS
                        entity.DistrictId = create.Data;
                        entity.MecashId = create.Data;
                        var insert = await _districtService.Add(entity);
                        if (insert)
                            return Json(new { status = 1, message = "Tạo mới Quận/Huyện thành công" });
                        else
                            return Json(new { status = 1, message = "Tạo mới Quận/Huyện thất bại. Vui lòng liên hệ phòng kỹ thuật" });
                    }
                    else
                        return Json(new { status = 0, message = "Lỗi. Gọi Api tạo mới bên LMS" });
                }
                else
                    return Json(new { status = 0, message = "Quận/Huyện đã tồn tại" });
            }
        }

        public async Task<IActionResult> GetDistrictByProvinceId(int provinceId)
        {
            var lstData = await _districtService.GetDistrictByProvinceId(provinceId);
            return Json(new { data = lstData });
        }

        public async Task<IActionResult> GetProvinceIdByDistrictId(int districtId)
        {
            var provinceId = await _districtService.GetProvinceIdByDistrictId(districtId);
            return Json(new { data = provinceId });
        }
        #endregion

        #region Quản lý Phường/Xã
        [Route("/dictionary/manage-ward.html")]
        [PermissionFilter]
        public async Task<IActionResult> ManageWard()
        {
            //lấy danh sách tất cả thành phố
            ViewBag.ListProvince = await _provinceService.GetAll();
            return View();
        }

        [HttpPost]
        public IActionResult LoadDataWard()
        {
            try
            {
                var modal = new WardDatatable(HttpContext.Request.Form);
                #region Gán giá trị mặc định cho param
                if (string.IsNullOrEmpty(modal.districtId))
                    modal.districtId = "1";
                #endregion
                int recordsTotal = 0;
                // Query api   
                var data = _wardService.Search(modal.searchName, int.Parse(modal.districtId), int.Parse(modal.page), int.Parse(modal.perpage), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddOrUpdateWard(DAL.EntityFramework.Ward entity)
        {
            //Cập nhập
            if (entity.WardId > 0)
            {
                var update = await _wardService.Update(entity);
                if (update)
                    return Json(new { status = 1, message = "Cập nhập Phường/Xã thành công" });
                else
                    return Json(new { status = 1, message = "Cập nhập Phường/Xã thất bại. Vui lòng liên hệ phòng kỹ thuật" });
            }
            //Thêm mới
            else
            {
                //kiểm tra xem trong db có chưa
                //chưa có mới cho tạo mới
                var checkWard = await _wardService.CheckWard(entity.Name, entity.DistrictId.Value);
                if (checkWard)
                {
                    //gọi api thêm mới của LMS
                    var objCreateWard = new CreateWard()
                    {
                        DistrictID = entity.DistrictId.Value,
                        Name = entity.Name,
                        TypeWard = Description.GetDescription((TypeWard)entity.Type)
                    };
                    var create = _lmsV2Service.CreateWard(objCreateWard);
                    if (create != null && create.Data > 0 && create.Status == 1)
                    {
                        //tạo thành công bên LMS thì tạo xuống LOS dùng ID của LMS làm ID của LOS
                        entity.WardId = create.Data;
                        entity.MecashId = create.Data;
                        var insert = await _wardService.Add(entity);
                        if (insert)
                            return Json(new { status = 1, message = "Tạo mới Phường/Xã thành công" });
                        else
                            return Json(new { status = 1, message = "Tạo mới Phường/Xã thất bại. Vui lòng liên hệ phòng kỹ thuật" });
                    }
                    else
                        return Json(new { status = 0, message = "Lỗi. Gọi Api tạo mới bên LMS" });
                }
                else
                    return Json(new { status = 0, message = "Phường/Xã đã tồn tại" });
            }
        }
        #endregion

    }
}