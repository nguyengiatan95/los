﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LOS.WebApp.Controllers
{
    public class DocumentTypeController : BaseController
    {
        private IHttpClientFactory clientFactory;
        private IConfiguration _baseConfig;
        private IDocumentType _documentTypeService;
        public DocumentTypeController(IConfiguration configuration, IHttpClientFactory clientFactory, IDocumentType documentTypeService)
          : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
            this.clientFactory = clientFactory;
            this._documentTypeService = documentTypeService;
        }

        [Route("document-manager.html")]
        [PermissionFilter]
        public IActionResult Index()
        {
            return View();
        }


        public List<DocumentTypeDetail> ConvertDocumentTree(List<DocumentTypeDetail> listData)
        {
            var result = new List<DocumentTypeDetail>();
            var dicData = new Dictionary<int, List<DocumentTypeDetail>>();
            foreach (var item in listData)
            {
                if (item.ParentId > 0)
                {
                    var key = item.ParentId.Value;
                    if (!dicData.ContainsKey(key))
                        dicData[key] = new List<DocumentTypeDetail>();
                    dicData[key].Add(item);
                }
            }
            foreach (var item in listData)
            {
                if (item.ParentId.GetValueOrDefault(0) == 0)
                {
                    result.Add(item);
                    var key = item.Id;
                    if (dicData.ContainsKey(item.Id))
                    {
                        var childs = dicData[item.Id];
                        foreach (var child in childs)
                        {
                            result.Add(child);
                            if (dicData.ContainsKey(child.Id))
                            {
                                result.AddRange(dicData[child.Id].OrderBy(x=>x.Id));
                            }
                        }
                    }
                }
            }
            return result;
        }
        public async Task<IActionResult> LoadData()
        {
            try
            {

                var modal = new DocumentTypeDatable(HttpContext.Request.Form);
                int recordsTotal = 0;
                // Query api   	
                List<DocumentTypeDetail> data = new List<DocumentTypeDetail>();
                data = _documentTypeService.GetAll(GetToken(), modal.ToQueryObject(), ref recordsTotal);
               
                
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ActionResult GetDocumentTypeParent(int ProductId)
        {
            var lstData = new List<DocumentTypeDetail>();
            lstData = _documentTypeService.GetDocumentTypeEnable(GetToken());
            if (lstData != null && lstData.Count > 0)
                return Json(new { status = 1, data = lstData });
            else
                return Json(new { status = 0});
        }

        public ActionResult GetById(int Id)
        {
            var data = _documentTypeService.GetById(GetToken(), Id);
            if(data != null)
                return Json(new { status = 1 , data = data });
            else
                return Json(new { status = 0 });
        }

        [HttpPost]
        public ActionResult AddOrUpdate(DocumentTypeDTO entity)
        {
            //cập nhập
            if (entity != null && entity.Id > 0)
            {
                var result = _documentTypeService.Update(GetToken(), entity.MapToDocumentType(), entity.Id);
                if (result)
                    return Json(new { status = 1, message = "Cập nhật danh mục chứng từ thành công" });
                return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật danh mục chứng từ. Vui lòng liên hệ quản trị viên!" });
            }
            //Tạo mới
            else
            {
                var result = _documentTypeService.Create(GetToken(), entity.MapToDocumentType());

                if (result)
                    return Json(new { status = 1, message = "Thêm mới danh mục chứng từ thành công" });
                return Json(new { status = 0, message = "Xảy ra lỗi khi thêm mới danh mục chứng từ. Vui lòng liên hệ quản trị viên!" });
            }
        }

    }
}
