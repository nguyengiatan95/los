﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.LMS;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Services;
using LOS.WebApp.Services.AIService;
using LOS.WebApp.Services.ResultEkyc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using static LOS.WebApp.Models.Ekyc.Ekyc;

namespace LOS.WebApp.Controllers
{
    // [AuthorizeFilter]
    public class ExportController : BaseController
    {
        private ILoanBriefService loanBriefService;
        private IDictionaryService _dictionaryService;
        private readonly IMapper _mapper;
        private IConfiguration _configuration;
        private IHttpClientFactory clientFactory;
        private ILMSService _lmsService;

        public ExportController(IConfiguration configuration, ILoanBriefService loanService, IDictionaryService dictionaryService,
            IHttpClientFactory clientFactory, ILMSService lmsService) : base(configuration, clientFactory)
        {
            this.loanBriefService = loanService;
            this._dictionaryService = dictionaryService;
            this._configuration = configuration;
            this.clientFactory = clientFactory;
            this._lmsService = lmsService;
        }


        [Produces("application/json")]
        public IActionResult ExportExcelTLS(string loanBriefId, string search, string provinceId, string status, string productId, string support, string filtercreateTime, string filterTeam, string page, string pageSize, string filterActionTime)
        {
            var user = GetUserGlobal();
            var modal = new ExportTLS();
            modal.loanBriefId = loanBriefId;
            modal.search = search;
            modal.provinceId = provinceId;
            if (status != "undefined")
                modal.statusTelesales = status;
            modal.productId = productId;
            if (modal.productId == "undefined")
                modal.productId = "-1";
            if (modal.filterStaffTelesales != "undefined")
                modal.filterStaffTelesales = support;
            modal.DateRanger = filtercreateTime;
            modal.page = page;
            modal.pageSize = "100000";

            modal.team = filterTeam;
            if (filterTeam == "undefined")
                modal.team = "0";
            DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
            DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
            if (!string.IsNullOrEmpty(modal.DateRanger))
                DateTimeUtility.ConvertDateRanger(modal.DateRanger, ref fromDate, ref toDate);

            modal.FromDate = fromDate.ToString("yyyy/MM/dd");
            modal.ToDate = toDate.ToString("yyyy/MM/dd");

            if (!string.IsNullOrEmpty(filterActionTime))
            {
                DateTime fromDateLastChangeStatus = DateTimeUtility.FromeDate(DateTime.Now);
                DateTime toDateDateLastChangeStatus = DateTimeUtility.ToDate(DateTime.Now);
                DateTimeUtility.ConvertDateRanger(filterActionTime, ref fromDateLastChangeStatus, ref toDateDateLastChangeStatus);
                modal.FromDateLastChangeStatus = fromDateLastChangeStatus.ToString("yyyy/MM/dd");
                modal.ToDateLastChangeStatus = toDateDateLastChangeStatus.ToString("yyyy/MM/dd");
            }

            if (modal.provinceId == null)
                modal.provinceId = "-1";
            //modal.userId = user.UserId;
            if (user.ListUserTeamTelesales != null && user.ListUserTeamTelesales.Count > 0)
            {
                modal.teamTelesales = string.Join(",", user.ListUserTeamTelesales.Select(x => x.TeamTelesalesId).ToList());
            }
            //tài khoản qc_phuongnt
            if (user.UserId == 55764)
            {
                modal.teamTelesales = "1,2,3,4";
            }
            int recordsTotal = 0;
            // Query api   	
            var data = loanBriefService.Search(GetToken(), modal.ToQueryObject(), ref recordsTotal);
            List<string> lstHeaderLoan = new List<string> { "STT", "Mã HĐ", "Nguồn(utm_source)", "Hub", "Khách hàng", "Thành phố", "Quận/Huyện", "Thời gian tạo", "Tiền vay", "Gói vay", "Trạng thái", "Trạng thái liên hệ", "Nhân viên", "Lịch hẹn", "Thời gian tác động", "Rule Check", "Location Sim" };
            List<int> lstWidthLoan = new List<int> { 10, 15, 20, 10, 30, 25, 25, 25, 20, 25, 20, 30, 25, 20, 20, 20, 20 };
            List<ExcelHorizontalAlignment> lstAlign = new List<ExcelHorizontalAlignment> { ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left
                                                                                           };

            List<System.Drawing.Color> lstColor = new List<System.Drawing.Color> { Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black
                                                                                  };

            byte[] fileContents;
            ExcelPackage package = new ExcelPackage();

            var ws = package.Workbook.Worksheets.Add("Kiểm soát sau giải ngân");
            // Set Font
            ws.Cells.Style.Font.Name = "Calibri";
            ws.Cells.Style.Font.Size = 11;

            // Format All Cells
            ws.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            // Format All Column
            for (int i = 1; i <= lstHeaderLoan.Count; i++)
            {
                ws.Column(i).Width = lstWidthLoan[i - 1];
                ws.Column(i).Style.HorizontalAlignment = lstAlign[i - 1];
                ws.Column(i).Style.Font.Color.SetColor(lstColor[i - 1]);
            }

            int iPosHeader = 1;
            // mapping title header
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(77, 119, 204));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Font.Bold = true;
            ws.Row(iPosHeader).Height = 16;
            for (int i = 1; i <= lstHeaderLoan.Count; i++)
            {
                ws.Cells[iPosHeader, i].Value = lstHeaderLoan[i - 1];
            }

            // SET VALUE
            iPosHeader++;


            int j = 1;
            var statusStr = "Không xác định";
            for (int i = 0; i < data.Count(); i++)
            {
                if (data[i].TypeHotLead == 1)
                    statusStr = "Chưa liên hệ";
                if (data[i].Status == 20 && data[i].StatusTelesales == null)
                    statusStr = "Chưa liên hệ";
                if (data[i].Status == 16 && data[i].StatusTelesales == null)
                    statusStr = "Chờ chuẩn bị hồ sơ";
                if (data[i].Status == 99)
                    statusStr = "Đơn đã hủy";
                if (data[i].StatusTelesales != null)
                {
                    if (data[i].StatusTelesales == 1)
                        statusStr = "Không liên hệ được";
                    if (data[i].StatusTelesales == 2)
                        statusStr = "Cân nhắc";
                    if (data[i].StatusTelesales == 3)
                        statusStr = "Chờ chuẩn bị hồ sơ";
                    if (data[i].StatusTelesales == 33)
                        statusStr = "Hẹn gọi lại";
                }

                ws.Cells[iPosHeader, 1].Value = j;
                ws.Cells[iPosHeader, 2].Value = "HĐ" + data[i].LoanBriefId;
                ws.Cells[iPosHeader, 3].Value = data[i].UtmSource;
                ws.Cells[iPosHeader, 4].Value = data[i].HubName;
                ws.Cells[iPosHeader, 5].Value = data[i].FullName;
                ws.Cells[iPosHeader, 6].Value = data[i].ProvinceName;
                ws.Cells[iPosHeader, 7].Value = data[i].DistrictName;
                ws.Cells[iPosHeader, 8].Value = Convert.ToDateTime(data[i].CreatedTime.ToString()).ToString("dd/MM/yyyy hh:MM:ss");
                ws.Cells[iPosHeader, 9].Value = Convert.ToInt64(data[i].LoanAmount).ToString("#,##");
                ws.Cells[iPosHeader, 10].Value = data[i].ProvinceName;
                ws.Cells[iPosHeader, 11].Value = statusStr;
                ws.Cells[iPosHeader, 12].Value = data[i].DetailStatusTelesales != null ? Common.Helpers.ExtentionHelper.GetDescription((EnumDetailStatusTelesales)data[i].DetailStatusTelesales) : "";
                ws.Cells[iPosHeader, 13].Value = data[i].UserTelesales;
                ws.Cells[iPosHeader, 14].Value = data[i].ScheduleTime != null ? Convert.ToDateTime(data[i].ScheduleTime.ToString()).ToString("dd/MM/yyyy hh:MM") : "";
                ws.Cells[iPosHeader, 15].Value = data[i].LastChangeStatusTelesale != null ? Convert.ToDateTime(data[i].LastChangeStatusTelesale.ToString()).ToString("dd/MM/yyyy hh:MM") : "";
                ws.Cells[iPosHeader, 16].Value = String.IsNullOrEmpty(data[i].ResultRuleCheck) ? data[i].ResultRuleCheck : "";
                ws.Cells[iPosHeader, 17].Value = String.IsNullOrEmpty(data[i].ResultLocation) ? data[i].ResultLocation : "Chưa có KQ";
                iPosHeader++;
                j++;
            }
            fileContents = package.GetAsByteArray();
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: "danh-sach-don-vay.xlsx"
            );
            //Response.ContentType = "application/vnd.ms-excel";
            //HttpContext.Response.Headers.Add("content-disposition", "attachment;  filename=" + string.Format("kiem-soat-sau-giai-ngan.xls"));


            //return new EmptyResult();
        }

        public IActionResult ExportExcelReportDebt(RequestReportDebt entity)
        {
            var lstData = new List<ReportDebt>();
            if (!string.IsNullOrEmpty(entity.date))
            {
                entity.date = entity.date.Replace("/", "-");
                var result = _lmsService.GetReportDebt(GetToken(), entity);
                if (result != null && result.Data != null && result.Data.Count > 0)
                {
                    lstData = result.Data;
                }
            }
            List<string> lstHeaderLoan = new List<string> { "STT", "Tên HUB", "Dư nợ đầu kỳ", "Dư nợ đầu kỳ DPD < 10", "Dư nợ GN mới", "Dư nợ hiện tại", "Dư nợ hiện tại DPD < 10", "Tốt Tăng Net", "Tỷ lệ hoàn thành nợ QH 10+ của tháng T-3", "Tỷ lệ hoàn thành nợ QH 10+ của tháng T-2", "Tỷ lệ hoàn thành nợ QH 10+ của tháng T-1", "Tỷ lệ hoàn thành nợ QH 10+ của 3 tháng giải ngân gần nhất" };
            List<int> lstWidthLoan = new List<int> { 10, 20, 20, 40, 20, 20, 20, 20, 40, 40, 40, 40 };
            List<ExcelHorizontalAlignment> lstAlign = new List<ExcelHorizontalAlignment> { ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Right,
                                                                                           ExcelHorizontalAlignment.Right,
                                                                                           ExcelHorizontalAlignment.Right,
                                                                                           ExcelHorizontalAlignment.Right,
                                                                                           ExcelHorizontalAlignment.Right,
                                                                                           ExcelHorizontalAlignment.Right,
                                                                                           ExcelHorizontalAlignment.Right,
                                                                                           ExcelHorizontalAlignment.Right,
                                                                                           ExcelHorizontalAlignment.Right,
                                                                                           ExcelHorizontalAlignment.Right,
                                                                                           };

            List<System.Drawing.Color> lstColor = new List<System.Drawing.Color> { Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                  };

            byte[] fileContents;
            ExcelPackage package = new ExcelPackage();

            var ws = package.Workbook.Worksheets.Add("Báo cáo dư nợ HUB");
            // Set Font
            ws.Cells.Style.Font.Name = "Calibri";
            ws.Cells.Style.Font.Size = 11;

            // Format All Cells
            ws.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            // Format All Column
            for (int i = 1; i <= lstHeaderLoan.Count; i++)
            {
                ws.Column(i).Width = lstWidthLoan[i - 1];
                ws.Column(i).Style.HorizontalAlignment = lstAlign[i - 1];
                ws.Column(i).Style.Font.Color.SetColor(lstColor[i - 1]);
            }

            int iPosHeader = 1;
            // mapping title header
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(77, 119, 204));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Font.Bold = true;
            ws.Row(iPosHeader).Height = 16;
            for (int i = 1; i <= lstHeaderLoan.Count; i++)
            {
                ws.Cells[iPosHeader, i].Value = lstHeaderLoan[i - 1];
            }

            // SET VALUE
            iPosHeader++;
            int j = 1;
            for (int i = 0; i < lstData.Count(); i++)
            {
                ws.Cells[iPosHeader, 1].Value = j;
                ws.Cells[iPosHeader, 2].Value = lstData[i].HubName;
                ws.Cells[iPosHeader, 3].Value = lstData[i].TotalMoneyCurrentBegin.ToString("###,0");
                ws.Cells[iPosHeader, 4].Value = lstData[i].TotalMoneyCurrentBegin_10PDP.ToString("###,0");
                ws.Cells[iPosHeader, 5].Value = lstData[i].TotalMoneyDisbursementNew.ToString("###,0");
                ws.Cells[iPosHeader, 6].Value = lstData[i].TotalMoneyCurrent.ToString("###,0");
                ws.Cells[iPosHeader, 7].Value = lstData[i].TotalMoneyCurrent_10DPD.ToString("###,0");
                ws.Cells[iPosHeader, 8].Value = lstData[i].NetAugment.ToString("###,0");
                ws.Cells[iPosHeader, 9].Value = lstData[i].CompletionRateT_3;
                ws.Cells[iPosHeader, 10].Value = lstData[i].CompletionRateT_2;
                ws.Cells[iPosHeader, 11].Value = lstData[i].CompletionRateT_1;
                ws.Cells[iPosHeader, 12].Value = lstData[i].CompletionRate3Month;

                iPosHeader++;
                j++;
            }
            fileContents = package.GetAsByteArray();
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: "bao-cao-du-no-hub.xlsx"
            );

        }
       
    }
}