﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Groups;
using LOS.WebApp.Models.Request;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class GroupController : BaseController
    {
        private IConfiguration _baseConfig;
        private IGroupServices _groupServices;
        private readonly IMapper _mapper;
        private IPipelineService _pipelineService;
        private IDictionaryService _dictionaryService;
        private IModuleService _moduleService;
        private IHttpClientFactory clientFactory;

        public GroupController(IGroupServices groupServices, IConfiguration configuration, IMapper mapper, IPipelineService pipelineService, IDictionaryService dictionaryService, IModuleService moduleService, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _groupServices = groupServices;
            _baseConfig = configuration;
            _mapper = mapper;
            this._pipelineService = pipelineService;
            this._dictionaryService = dictionaryService;
            this._moduleService = moduleService;
            this.clientFactory = clientFactory;
        }
        [Route("group-manager.html")]
        [PermissionFilter]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> LoadData()
        {
            try
            {
                var modal = new GroupDatatable(HttpContext.Request.Form);
                int recordsTotal = 0;
                // Query api   	
                List<GroupDetail> data = new List<GroupDetail>();
                data = _groupServices.Search(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });

            }
            catch (Exception)
            {
                return Json(new { status = 0, message = "Lỗi liên hệ kĩ thuật" });
            }
        }

        [Route("group")]
        public IActionResult CreateGroupModalPartial(int Id)
        {
            var entity = new GroupItem();
            if (Id == 0)
            {
                return View(entity);
            }
            else
            {
                var group = _groupServices.GetById(GetToken(), Id);
                entity = _mapper.Map<GroupItem>(group);
                // entity.ListGroupModule = group.Modules;
                return View(entity);
            }
        }

        public async Task<IActionResult> CreateGroup(GroupItem entity)
        {
            try
            {
                List<string> stringList = new List<string>();
                if (entity.lstMenu[0] != null)
                {
                    stringList = entity.lstMenu[0].Split(',').ToList();
                    stringList.Select(int.Parse).ToList();
                }
                if (entity.GroupId > 0)
                {
                    List<GroupModule> lstmodule = new List<GroupModule>();
                    foreach (var item in stringList)
                    {
                        GroupModule objmodule = new GroupModule();
                        objmodule.ModuleId = Convert.ToInt32(item);
                        objmodule.GroupId = entity.GroupId;
                        lstmodule.Add(objmodule);
                    }
                    //xử lý action phân quyền
                    if (entity.lstAction != null && entity.lstAction.Count > 0)
                    {
                        foreach (var item in entity.lstAction)
                        {
                            GroupModule objmodule = new GroupModule();
                            objmodule.ModuleId = Convert.ToInt32(item);
                            objmodule.GroupId = entity.GroupId;
                            objmodule.ApplicationId = (int)Common.Extensions.EnumApplication.WebLOS;
                            lstmodule.Add(objmodule);
                        }
                    }
                    entity.ListGroupModule = lstmodule;
                    var obj = _mapper.Map<GroupDTO>(entity);
                    obj.UpdatedAt = DateTime.Now;
                    var result = _groupServices.Update(GetToken(), entity.GroupId, obj);
                    if (result)
                        return Json(new { status = 1, message = "Cập nhập thông tin nhóm người dùng thành công" });
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhập thông tin nhóm người dùng. Vui lòng liên hệ quản trị viên!" });
                }
                else
                {
                    entity.Status = 1;
                    entity.CreatedAt = DateTime.Now;
                    List<GroupModule> lstmodule = new List<GroupModule>();
                    foreach (var item in stringList)
                    {
                        GroupModule objmodule = new GroupModule();
                        objmodule.ModuleId = Convert.ToInt32(item);
                        lstmodule.Add(objmodule);
                    }
                    //xử lý action phân quyền
                    if (entity.lstAction != null && entity.lstAction.Count > 0)
                    {
                        foreach (var item in entity.lstAction)
                        {
                            GroupModule objmodule = new GroupModule();
                            objmodule.ModuleId = Convert.ToInt32(item);
                            objmodule.GroupId = entity.GroupId;
                            objmodule.ApplicationId = (int)Common.Extensions.EnumApplication.WebLOS;
                            lstmodule.Add(objmodule);
                        }
                    }
                    entity.ListGroupModule = lstmodule;
                    var obj = _mapper.Map<GroupDTO>(entity);

                    obj.CreatedAt = DateTime.Now;
                    var result = _groupServices.Create(GetToken(), obj);
                    if (result)
                        return Json(new { status = 1, message = "Thêm mới nhóm người dùng thành công" });
                    return Json(new { status = 0, message = "Xảy ra lỗi khi thêm mới nhóm người dùng. Vui lòng liên hệ quản trị viên!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Lỗi liên hệ kĩ thuật" });
            }
        }
        public IActionResult DeleteGroup(int Id)
        {
            try
            {
                var checkdelete = _groupServices.CheckDelete(GetToken(), Id);
                if (checkdelete == 1)
                    return Json(new { status = 0, message = "Nhóm tài khoản đã có người dùng không thể xóa" });
                else if (checkdelete == 2)
                {
                    return Json(new { status = 0, message = "Nhóm tài khoản đã phân quyền không thể xóa" });
                }
                else
                {
                    var result = _groupServices.Delete(GetToken(), Id);
                    if (result)
                        return Json(new { status = 1, message = "Xóa nhóm người dùng thành công" });
                    return Json(new { status = 0, message = "Xảy ra lỗi khi xóa nhóm người dùng. Vui lòng liên hệ quản trị viên!" });
                }
                return Json(new { status = 0, message = "Xảy ra lỗi khi xóa nhóm người dùng. Vui lòng liên hệ quản trị viên!" });
                // Query api
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Lỗi liên hệ kĩ thuật" });
            }
        }

        #region Permission
        public async Task<IActionResult> GetPermissionData(int Id)
        {
            var groupModel = new GroupDetail();
            ViewBag.Properties = _pipelineService.GetAllProperty(GetToken());
            ViewBag.LoanStatus = _dictionaryService.GetLoanStatus(GetToken());
            if (Id > 0)
                groupModel = _groupServices.GetPermission(GetToken(), Id);
            return View("~/Views/Group/Partial/GroupPermissionPartial.cshtml", groupModel);
        }

        public IActionResult ActionPropertyPartial()
        {
            ViewBag.LoanStatus = _dictionaryService.GetLoanStatus(GetToken());
            return View("~/Views/Group/Partial/ActionPropertyPartial.cshtml", new GroupDetail());
        }
        public IActionResult ConditionValuePropertyPartial()
        {
            var data = _pipelineService.GetAllProperty(GetToken());
            ViewBag.Properties = data;
            return View("~/Views/Group/Partial/ConditionValuePropertyPartial.cshtml", new GroupDetail());
        }

        public IActionResult GetPropertyPartial([FromBody] IdOnlyReq req)
        {
            try
            {
                var data = _pipelineService.GetPropertyById(GetToken(), req.id);

                if (data.LinkObject != null)
                {
                    // Get Link Object
                    Helpers.Ultility util = new Helpers.Ultility(clientFactory);
                    var objects = util.GetLinkObject(data.LinkObject, GetToken());
                    ViewBag.LinkObjects = objects;
                }
                return View("~/Views/Group/Partial/GetPropertyPartial.cshtml", data);
            }
            catch (Exception)
            {
                return View("~/Views/Group/Partial/GetPropertyPartial.cshtml");
            }
        }
        [HttpPost]
        public IActionResult UpdatePermissionGroup([FromBody]PermissionGroupItem data)
        {
            if (data.GroupId > 0)
            {
                var user = GetUserGlobal();
                if (data.ConditionGroups != null && data.ConditionGroups.Count > 0)
                {
                    var model = new PermissionGroupDTO()
                    {
                        GroupId = data.GroupId,
                        GroupConditions = (data.ConditionGroups != null && data.ConditionGroups.Count > 0) ?
                                data.ConditionGroups.Select(x => new GroupCondition
                                {
                                    GroupId = data.GroupId,
                                    PropertyId = x.PropertyId,
                                    Operator = x.Operator.Trim().ToUpper(),
                                    Value = x.Value,
                                    CreatedBy = user.UserId
                                }).ToList() : new List<GroupCondition>(),
                        GroupActionPermissions = data.ActionGroups != null && data.ActionGroups.Count > 0 ?
                            data.ActionGroups.Select(x => new GroupActionPermission()
                            {
                                //UserId = data.UserId,
                                LoanStatus = x.PropertyId,
                                Value = x.Value,
                                CreatedBy = user.UserId
                            }).ToList() : new List<GroupActionPermission>()
                    };

                    if (_groupServices.UpdatePermission(GetToken(), model))
                        return Json(new { isSuccess = 1, message = "Phân quyền thành công" });
                    return Json(new { isSuccess = 0, message = "Xảy ra lỗi khi phân quyền dữ liệu user. Vui lòng thử lại" });
                }
                return Json(new { isSuccess = 0, message = "Vui lòng phân quyền dữ liệu cho user" });
            }
            else
            {
                return Json(new { isSuccess = 0, message = "Không tồn tại thông tin user" });
            }
        }
        #endregion
        public async Task<IActionResult> GetActionPermisson(int Id)
        {
            var ListActionPermisson = _moduleService.GetAllAction(GetToken(), (int)Common.Extensions.EnumApplication.WebLOS);
            var listPermission = _moduleService.GetByGroupId(GetToken(), Id, (int)Common.Extensions.EnumApplication.WebLOS);
            var ListResult = new List<ActionPermissionItem>();
            if (ListActionPermisson != null && ListActionPermisson.Count > 0)
            {
                foreach (var item in ListActionPermisson)
                {
                    var obj = new ActionPermissionItem() { Name = item.Name, ModuleId = item.ModuleId, Controller = item.Controller, Action = item.Action, Selected = false };

                    if (listPermission.Any(x => x.ModuleId == item.ModuleId))
                    {
                        obj.Selected = true;
                    }
                    ListResult.Add(obj);
                }
            }
            return View("/Views/Group/Partial/_ActionPermisson.cshtml", ListResult);
        }
        public async Task<IActionResult> GetMenuPermisson(int Id)
        {
            List<JSTreeModel> lstPermission = new List<JSTreeModel>();

            //if (Id == 0)
            //    return Json(new { Data = lstPermission, status = 1, message = "Lây thông tin thành công" });

            var _listModule = _moduleService.GetAllMenu(GetToken(), (int)Common.Extensions.EnumApplication.WebLOS);
            _listModule = _listModule.OrderBy(m => m.ParentId).ToList();
            var listPermission = _moduleService.GetByGroupId(GetToken(), Id, (int)Common.Extensions.EnumApplication.WebLOS);

            foreach (var item in _listModule)
            {
                JSTreeModel objCurrentNode = new JSTreeModel();
                objCurrentNode.id = item.ModuleId.ToString();
                objCurrentNode.text = item.Name.ToString();
                objCurrentNode.ControllerName = item.Controller == null ? item.Controller : "";
                objCurrentNode.ActionName = item.Action == null ? item.Action : "";
                if (Id > 0)
                {
                    if (listPermission.Any(x => x.ModuleId == item.ModuleId && x.ParentId != null) || listPermission.Any(x => x.ModuleId == item.ModuleId && item.IsMenu != true))
                        objCurrentNode.state.selected = true;
                }
                if (item.ParentId == null)
                    lstPermission.Add(objCurrentNode);
                else
                    AddCurrentNodeToParent(lstPermission, objCurrentNode, item.ParentId.ToString());
            }
            return Json(new { Data = lstPermission, status = 1, message = "Lây thông tin thành công" });
        }
        private void AddCurrentNodeToParent(List<JSTreeModel> lstData, JSTreeModel objCurrentNode, string strParentID)
        {
            if (lstData != null && lstData.Count > 0)
            {
                for (int i = 0; i < lstData.Count; i++)
                {
                    if (lstData[i].id == strParentID)
                    {
                        if (lstData[i].children == null) lstData[i].children = new List<JSTreeModel>();
                        lstData[i].children.Add(objCurrentNode);
                        return;
                    }
                }
            }
        }
        public IActionResult GetPermissionApi(int groupId)
        {
            var ListActionPermisson = _moduleService.GetAllAction(GetToken(), (int)Common.Extensions.EnumApplication.ApiLOS);
            var listPermission = _moduleService.GetByGroupId(GetToken(), groupId, (int)Common.Extensions.EnumApplication.ApiLOS);
            var ListResult = new List<ActionPermissionItem>();
            if (ListActionPermisson != null && ListActionPermisson.Count > 0)
            {
                foreach (var item in ListActionPermisson)
                {
                    var obj = new ActionPermissionItem() { Name = item.Name, ModuleId = item.ModuleId, Controller = item.Controller, Action = item.Action, Selected = false, Url = item.Path };

                    if (listPermission.Any(x => x.ModuleId == item.ModuleId))
                    {
                        obj.Selected = true;
                    }
                    ListResult.Add(obj);
                }
            }
            return View("/Views/Group/Partial/GroupPermissionApi.cshtml", ListResult);
        }
        [HttpPost]
        public IActionResult SaveActionPermissonApi(int GroupId, List<int> lstAction)
        {
            List<GroupModule> lstModule = new List<GroupModule>();
            //xử lý action phân quyền
            if (lstAction != null && lstAction.Count > 0)
            {
                foreach (var item in lstAction)
                {
                    GroupModule objmodule = new GroupModule();
                    objmodule.ModuleId = Convert.ToInt32(item);
                    objmodule.GroupId = GroupId;
                    objmodule.ApplicationId = (int)Common.Extensions.EnumApplication.ApiLOS;
                    lstModule.Add(objmodule);
                }
            }
            var result = _moduleService.SaveActionPermissonApiGroup(GetToken(), GroupId, lstModule);
            if (result)
                return Json(new { status = 1, message = "Lưu lại thành công" });
            else
                return Json(new { status = 0, message = "Lưu lại không thành công" });
        }
    }
}
