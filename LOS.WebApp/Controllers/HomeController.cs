﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LOS.WebApp.Models;
using LOS.WebApp.Helpers;

namespace LOS.WebApp.Controllers
{
	public class HomeController : Controller
	{
		[AuthorizeFilter]
		[Route("home.html")]
		[Route("/")]
		public IActionResult Index()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		[Route("error.html")]
		public IActionResult PermissionDenied()
		{
			// Retry CI / CD
			return View();
		}

		[Route("login-error.html")]
		public IActionResult LoginErr()
		{
			return View();
		}
	}
}
