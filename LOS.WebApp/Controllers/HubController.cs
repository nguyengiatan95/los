﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object.Loanbrief;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using static LOS.Common.Models.Request.LoanBriefReq;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class HubController : BaseController
    {
        private ILoanBriefService loanBriefService;
        private IDictionaryService _dictionaryService;
        private IReasonService _reasonService;
        private readonly IMapper _mapper;
        private IConfiguration _configuration;
        private ILMSService _lmsService;
        private IHttpClientFactory clientFactory;
        private IUserServices _userServices;
        private IShopServices _shopServices;
        private IDocumentType _documentService;
        public HubController(IConfiguration configuration, ILoanBriefService loanService, IDictionaryService dictionaryService,
            IReasonService reationService, IMapper mapper, ILMSService lmsService, IHttpClientFactory clientFactory,
            IUserServices userServices, IShopServices shopServices, IDocumentType documentService) : base(configuration: configuration, clientFactory)
        {
            this.loanBriefService = loanService;
            this._dictionaryService = dictionaryService;
            this._reasonService = reationService;
            this._mapper = mapper;
            this._configuration = configuration;
            _lmsService = lmsService;
            this.clientFactory = clientFactory;
            _userServices = userServices;
            _shopServices = shopServices;
            _documentService = documentService;
        }

        [Route("/hub/invoice.html")]
        [PermissionFilter]
        public IActionResult Invoice()
        {
            return View();

        }

        public IActionResult SearchInvoice()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new ApproveDatatable(HttpContext.Request.Form);

                DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
                DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
                if (!string.IsNullOrEmpty(modal.DateRanger))
                    DateTimeUtility.ConvertDateRanger(modal.DateRanger, ref fromDate, ref toDate);


                //modal.userId = user.UserId;
                //mine
                if (modal.status == "-1" || modal.status == null)
                    modal.coordinatorUserId = user.UserId;
                else
                    modal.coordinatorUserId = -1;
                int recordsTotal = 0;
                //var data = approveService.SearchEmp(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //set permission row
                //Returning Json Data  
                return Json(new { meta = modal, data = 1 });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult CreateFromInvoice(int TypeInvoice)
        {
            ViewBag.Type = TypeInvoice;
            return View();
        }


        [HttpGet]
        [Route("/hub/reportdebt.html")]
        [PermissionFilter]
        public IActionResult ReportHubDebt()
        {
            ViewBag.LstHub = _shopServices.GetAllHub(GetToken());
            return View();
        }

        [HttpPost]
        public IActionResult GetReportDebt()
        {
            var modal = new ReportDebtDatatable(HttpContext.Request.Form);
            var lstData = new List<ReportDebt>();
            if (!string.IsNullOrEmpty(modal.date))
            {
                var entity = new RequestReportDebt()
                {
                    date = modal.date.Replace("/", "-"),
                    hubid = Convert.ToInt32(modal.hubId)
                };
                var result = _lmsService.GetReportDebt(GetToken(), entity);
                if (result != null && result.Data != null && result.Data.Count > 0)
                {
                    lstData = result.Data;
                }
            }
            return Json(new { meta = modal, data = lstData });
        }

        //Danh sách màn hình theo dõi nợ cho hub
        [HttpGet]
        [Route("/hub/listloandebt.html")]
        [PermissionFilter]
        public IActionResult ListLoanDebt()
        {
            var user = GetUserGlobal();
            var lstShop = user.ListShop;
            return View(lstShop);
        }

        [HttpPost]
        public IActionResult GetListLoanDebt()
        {
            var user = GetUserGlobal();
            var model = new ListLoanDebtDatatable(HttpContext.Request.Form);
            model.Date = DateTime.Now.ToString("dd-MM-yyyy");
            int recordsTotal = 0;
            var lstData = new List<ListLoanDebt.lstData>();
            if (model.HubId != null && model.HubId.Length > 0)
            {
                var obj = new ListLoanDebt.RequestListLoanDebt()
                {
                    Date = model.Date,
                    LstHubId = Array.ConvertAll(model.HubId, int.Parse)
                };
                var result = _lmsService.GetListLoanDebt(GetToken(), obj);
                if (result != null && result.Code == 200)
                {
                    //Xử lý map data
                    var dicLoanBrief = new Dictionary<int, List<LoanBriefDetailDebt>>();
                    var lstLoanId = result.Data.Select(x => x.LoanId).Distinct().ToList();
                    if (lstLoanId != null && lstLoanId.Count > 0)
                    {
                        var obj2 = new GetLoanByCode()
                        {
                            lstId = lstLoanId
                        };
                        var lstLoan = loanBriefService.ListLoanBriefByCodeId(user.Token, obj2);
                        if (lstLoan != null && lstLoan.Count > 0)
                        {
                            var lmsData = result.Data.Where(x => lstLoan.Select(x => x.LmsLoanId).Distinct().ToList().Contains(x.LoanId)).ToList();
                            foreach (var item in lstLoan)
                            {
                                var lms = lmsData.Where(x => x.LoanId == item.LmsLoanId).FirstOrDefault();
                                lstData.Add(new Models.ListLoanDebt.lstData
                                {
                                    LMSData = lms,
                                    LOSData = item
                                });
                                //if (!dicLoanBrief.ContainsKey(item.LmsLoanId.Value))
                                //    dicLoanBrief[item.LmsLoanId.Value] = new List<LoanBriefDetailDebt>();
                                //dicLoanBrief[item.LmsLoanId.Value].Add(item);
                            }
                        }
                    }
                }
            }
            model.total = lstData.Count().ToString();
            lstData = lstData.Skip((Convert.ToInt32(model.page) - 1) * Convert.ToInt32(model.perpage)).Take(Convert.ToInt32(model.perpage)).ToList();
            return Json(new { meta = model, data = lstData });
        }

        #region ProposeExceptions

        [HttpGet]
        public async Task<IActionResult> ProposeExceptions(int loanBriefId)
        {
            var user = GetUserGlobal();
            var model = new ProposeExceptionsInit();
            var lstDocExceptions = await loanBriefService.GetDocumentException(user.Token, 0);
            var proposeException = await loanBriefService.GetProposeExceptions(user.Token, loanBriefId);

            model.LoanBriefId = loanBriefId;
            model.ProposeExceptions = proposeException;
            model.DocumentException = lstDocExceptions;

            return View(model);
        }
        [HttpGet]
        public async Task<IActionResult> GetListDocumentExceptions(int parentId)
        {
            var user = GetUserGlobal();
            var lstException = await loanBriefService.GetDocumentException(user.Token, parentId);

            return Json(new { status = 1, data = lstException });
        }

        [HttpPost]
        public async Task<IActionResult> ProposeExceptionsPost([FromBody] ProposeExceptionReq entity)
        {
            try
            {
                var user = GetUserGlobal();
                var loanbrief = await loanBriefService.GetLoanBriefById(GetToken(), entity.LoanBriefId);
                if (loanbrief != null)
                {
                    if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        return Json(new { status = 0, message = "Đơn vay được hủy trước đó!" });
                    if (loanbrief.Status == (int)EnumLoanStatus.WAIT_APPRAISER_CONFIRM_CUSTOMER
                        || loanbrief.Status == (int)EnumLoanStatus.BRIEF_APPRAISER_LOAN_DISTRIBUTING)
                        return Json(new { status = 0, message = "Đơn vay đã được đẩy trước đó!" });
                    if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
                        return Json(new { status = 0, message = "Đơn đang được xử lý. Vui lòng thử lại sau!" });

                    //if (loanbrief.Status != (int)EnumLoanStatus.HUB_CHT_APPROVE)
                    //    return Json(new { status = 0, message = "Vui lòng chuyển đơn về trạng thái Chờ CHT duyệt để đề xuất ngoại lệ" });


                    var obj = new DAL.Object.Loanbrief.ProposeExceptions();
                    if (user.GroupId == (int)EnumGroupUser.ManagerHub || user.GroupId == (int)EnumGroupUser.StaffHub)
                    {
                        obj.Status = 1;
                        obj.LoanBriefId = loanbrief.LoanBriefId;
                        obj.IsPushPipeline = true;
                        obj.DocId = entity.DocId;
                        obj.LoanAmount = entity.Money;
                        var result = loanBriefService.ProposeExceptions(GetToken(), obj);
                        if (result > 0)
                        {
                            var comment = $"<b>Trình phê duyệt ngoại lệ </b> hợp đồng HĐ-{entity.LoanBriefId} <br />Lý do: {entity.DocName} ";
                            if (!string.IsNullOrEmpty(entity.ChildDocName))
                                comment += $"<br />Lý do chi tiết: {entity.ChildDocName}";
                            if (entity.Money > 0)
                                comment = $"<b>Trình phê duyệt ngoại lệ </b> hợp đồng HĐ-{entity.LoanBriefId} với số tiền là: {entity.Money.ToString("n0")}";

                            if (!string.IsNullOrEmpty(entity.Note))
                                comment += $"<br /> Nội dung: {entity.Note}";
                            //Thêm note
                            var note = new LoanBriefNote
                            {
                                LoanBriefId = result,
                                Note = comment,
                                FullName = user.FullName,
                                Status = 1,
                                ActionComment = (int)EnumActionComment.ProposeExceptions,
                                CreatedTime = DateTime.Now,
                                UserId = user.UserId,
                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            };
                            loanBriefService.AddLoanBriefNote(GetToken(), note);

                            entity.UserId = user.UserId;
                            loanBriefService.AddProposeExceptions(GetToken(), entity);

                            return Json(new { status = 1, message = "Đề xuất phê duyệt ngoại lệ thành công" });
                        }
                        else
                            return Json(new { status = 0, message = "Xảy ra lỗi khi đề xuất phê duyệt ngoại lệ. Vui lòng thử lại sau!" });

                    }
                    return Json(new { status = 0, message = "Bạn không có quyền trình phê duyệt ngoại lệ!" });

                }
                else
                {
                    return Json(new { status = 0, message = "Không tìm thấy thông tin khoản vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion
    }
}