﻿using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.Response;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class JobTitleController : Controller
    {
        [Route("jobTitle-manager.html")]
        [PermissionFilter]
        //[PermissionFilter(ModuleCode = "JOB_MANAGER", Permission = "SEARCH")]
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                NameValueCollection query = new NameValueCollection();
                // Skip number of Rows count
                var start = Request.Form["pagination[page]"].FirstOrDefault();
                if (start == "1")
                    start = "0";
                query.Add("skip", start);
                // Paging Length 10,20
                var length = Request.Form["pagination[perpage]"].FirstOrDefault();
                query.Add("take", length);
                // Sort Column Name
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                if (!String.IsNullOrEmpty(sortColumn))
                {
                    query.Add("sortBy", sortColumn);
                }
                // Sort Column Direction (asc, desc)
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                if (!String.IsNullOrEmpty(sortColumnDirection))
                {
                    query.Add("sortOrder", sortColumnDirection);
                }
                // Search Value from (Search box)
                var searchValue = Request.Form["query[filterName]"].FirstOrDefault();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    query.Add("name", searchValue);
                }

                //Paging Size (10, 20, 50,100)
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Query api
                var httpClient = new HttpClient();
                string access_token = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result.Token;
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
                string url = Constants.GET_ALL_JOBTITLE_ENDPOINT + query.ToQueryString();
                var response = await httpClient.GetAsync(url);
                List<JobTitleDetail> data = new List<JobTitleDetail>();
                if (response.IsSuccessStatusCode)
                {
                    var sResult = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<DefaultResponse<SummaryMeta, List<JobTitleDetail>>>(sResult);
                    if (result.meta.errorCode == 200)
                    {
                        if (result.data.Count > 0)
                        {
                            recordsTotal = result.meta.totalRecords;
                            data = result.data;
                        }
                    }
                }

                //Returning Json Data
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult CreateJobTitleModalPartial()
        {
            var obj = new JobTitleDTO();
            return View("/Views/JobTitle/Partial/_CreateJobTitleModalPartial.cshtml", obj);
        }


        [HttpPost]
        public async Task<IActionResult> CreateJobTitle(JobTitleDTO jobTitle)
        {
            try
            {
                var obj = new JobTitleDTO();
                obj.JobTitleId = jobTitle.JobTitleId;
                obj.Name = jobTitle.Name;
                obj.Priority = jobTitle.Priority;

                //Query api
                var httpClient = new HttpClient();
                string access_token = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result.Token;
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
                string url = Constants.GET_ALL_JOBTITLE_ENDPOINT;

                var response = await httpClient.PostAsJsonAsync(url, jobTitle);

                if (response.IsSuccessStatusCode)
                {
                    return Json(new { status = 1, message = "Thành công" });
                }

                return Json(new { status = 0, message = "Tạo mới thất bại" });
            }
            catch (Exception)
            {
                return Json(new { status = 0, message = "Lỗi liên hệ kĩ thuật" });
            }

        }


        public async Task<IActionResult> UpdateJobTitleModalPartial(int Id)
        {
            var obj = new JobTitleDTO();

            // Query api
            var httpClient = new HttpClient();
            string access_token = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result.Token;
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
            string url = Constants.GET_ALL_JOBTITLE_ENDPOINT + "/";

            var response = await httpClient.GetAsync(url + Id);

            var data = new JobTitleDetail();
            if (response.IsSuccessStatusCode)
            {
                var sResult = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<DefaultResponse<Meta, JobTitleDetail>>(sResult);
                if (result.meta.errorCode == 200)
                {
                    data = result.data;
                    obj.JobTitleId = data.JobTitleId;
                    obj.Name = data.Name;
                    obj.Priority = data.Priority;
                }
            }

            return View("/Views/JobTitle/Partial/_UpdateJobTitleModalPartial.cshtml", obj);
        }



        public async Task<IActionResult> UpdateJobTitle(JobTitleDTO jobTitle)
        {
            try
            {
                var obj = new JobTitleDTO();
                obj.JobTitleId = jobTitle.JobTitleId;
                obj.Name = jobTitle.Name;
                obj.Priority = jobTitle.Priority;

                //Query api
                var httpClient = new HttpClient();
                string access_token = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result.Token;
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
                string url = Constants.GET_ALL_JOBTITLE_ENDPOINT + "/";

                var response = await httpClient.PutAsJsonAsync(url + jobTitle.JobTitleId, jobTitle);

                if (response.IsSuccessStatusCode)
                {
                    return Json(new { status = 1, message = "Thành công" });
                }

                return Json(new { status = 0, message = "Cập nhật thất bại" });
            }
            catch (Exception)
            {
                return Json(new { status = 0, message = "Lỗi liên hệ kĩ thuật" });
            }
        }



        [HttpPost]
        public async Task<IActionResult> DeleteJobTitle(int Id)
        {
            try
            {
                var client = new HttpClient();
                string access_token = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result.Token;
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
                string url = Constants.GET_ALL_JOBTITLE_ENDPOINT + "/";

                var response = await client.DeleteAsync(url + Id);

                if (response.IsSuccessStatusCode)
                {
                    return Json(new { status = 1, message = "Thành công" });
                }

                return Json(new { status = 0, message = "Xoá không thành công" });
            }
            catch (Exception)
            {
                return Json(new { status = 0, message = "Lỗi liên hệ kĩ thuật" });
            }
        }


    }
}