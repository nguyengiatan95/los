﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using AutoMapper;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Services;
using LOS.WebApp.Services.AIService;
using LOS.WebApp.Services.ResultEkyc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using static LOS.WebApp.Models.Ekyc.Ekyc;
using Serilog;
using LOS.WebApp.Cache;
using LOS.WebApp.Helpers;
using System.Net.Http;
using Amazon.Runtime;
using LOS.WebApp.Services.ProxyService;
using LOS.WebApp.Services.CiscoService;
using LOS.WebApp.Models.Cisco;
using RestSharp;
using System.Net.Http.Headers;
using OfficeOpenXml;
using System.Text.RegularExpressions;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using System.Drawing.Imaging;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Advanced;
using LOS.WebApp.Models.Ekyc;
using LOS.WebApp.Services.Location;
using System.Text;
using LOS.Common.Helpers;
using LOS.WebApp.Factories;
using LOS.DAL.Models.Request.DrawImage;
using LOS.Services.Services.DrawImage;
using Microsoft.AspNetCore.Hosting;
using LOS.Services.Services.Loanbrief;
using LOS.Services.Services.Telesales;
using LOS.Services.Services.ConfigProductDetail;
using LOS.Services.Services;

namespace LOS.WebApp.Controllers
{
    //[AuthorizeFilter]
    public class LoanBriefController : BaseController
    {
        private ILoanBriefService loanBriefService;
        private IDictionaryService _dictionaryService;
        private ICompositeViewEngine viewEngine;
        private IProductService _productService;
        private IMapper _mapper;
        private IReasonService _reasonService;
        private IUserServices _userServices;
        private IShopServices _shopServices;
        private ITrackDeviceService _trackDeviceService;
        private IDetectFace _detectFaceServices;
        private IConfiguration _configuration;
        private ICareSoftService _careSoftService;
        private ILMSService _lmsService;
        private ILogReqestAiService _logReqestAiService;
        private readonly INetworkService _networkService;
        private IResultEkycService _resultEkycService;
        private IEkycService _ekyc;
        private IPriceMotor _priceMotorService;
        private IErp _erpService;
        private IHttpClientFactory clientFactory;
        private ICheckBankService _checkBankService;
        private ILoanStatusDetailService _loanStatusDetailService;
        private ISendSmsBrandName _sendSmsBrandName;
        private ICiscoService _ciscoService;
        private ILoanBriefImportFileExcelServices _loanBriefImportFileExcelServices;
        private ITelesalesServices _telesalesServices;
        private ILogLoanInfoAi _logLoanInfoAi;
        private ILocationService _location;
        private ILogCallApi _logCallApi;
        private IProxyTimaService _proxyTimaService;
        private IDocumentType _documentService;
        private ILosService _losService;
        private ILogActionService _logActionService;
        private readonly ILoanbriefModelFactory _loanbriefModelFactory;
        private IDrawImageService _drawService;

        private ILoanbriefV2Service _loanbriefV2Service;
        private ITelesalesV2Service _telesalesV2Service;
        protected readonly IHostingEnvironment _environment;
        private IConfigProductDetailService _configProductDetailService;
        private ILogCallApiCiscoService _logCallApiCiscoService;
        private IShopV2Service _shopV2Service;
        public LoanBriefController(IConfiguration configuration, ILoanBriefService services, IDictionaryService dictionaryService, ICompositeViewEngine viewEngine, IProductService productService
            , IMapper mapper, IReasonService reasonService, IUserServices userServices, IShopServices shopServices, ITrackDeviceService trackDeviceService, IDetectFace detectFaceServices
            , ICareSoftService careSoftService, ILMSService lmsService, ILogReqestAiService logReqestAiService, INetworkService networkService, IResultEkycService resultEkycService, IEkycService ekyc,
            IPriceMotor priceMotorService, IErp erpService, IHttpClientFactory clientFactory, ICheckBankService checkBankService, ILoanStatusDetailService loanStatusDetailService,
            ISendSmsBrandName sendSmsBrandName, ILoanBriefImportFileExcelServices loanBriefImportFileExcelServices, ICiscoService ciscoService, ITelesalesServices telesalesServices,
            ILogLoanInfoAi logLoanInfoAi, ILocationService location, ILogCallApi logCallApi, IProxyTimaService proxyTimaService, IDocumentType documentService, ILosService losService,
            ILogActionService logActionService, IDrawImageService drawService, ILoanbriefModelFactory loanbriefModelFactory, IHostingEnvironment environment,
            ILoanbriefV2Service loanbriefV2Service, ITelesalesV2Service telesalesV2Service, IConfigProductDetailService configProductDetailService,
            ILogCallApiCiscoService logCallApiCiscoService, IShopV2Service shopV2Service)
            : base(configuration, clientFactory)
        {
            this.loanBriefService = services;
            this._dictionaryService = dictionaryService;
            this.viewEngine = viewEngine;
            this._productService = productService;
            this._mapper = mapper;
            this._reasonService = reasonService;
            this._userServices = userServices;
            this._shopServices = shopServices;
            this._trackDeviceService = trackDeviceService;
            _detectFaceServices = detectFaceServices;
            _configuration = configuration;
            _careSoftService = careSoftService;
            _lmsService = lmsService;
            _logReqestAiService = logReqestAiService;
            _networkService = networkService;
            _resultEkycService = resultEkycService;
            _ekyc = ekyc;
            _priceMotorService = priceMotorService;
            this._erpService = erpService;
            this.clientFactory = clientFactory;
            _checkBankService = checkBankService;
            _loanStatusDetailService = loanStatusDetailService;
            _sendSmsBrandName = sendSmsBrandName;
            _ciscoService = ciscoService;
            _loanBriefImportFileExcelServices = loanBriefImportFileExcelServices;
            _telesalesServices = telesalesServices;
            _logLoanInfoAi = logLoanInfoAi;
            _location = location;
            _logCallApi = logCallApi;
            _proxyTimaService = proxyTimaService;
            _documentService = documentService;
            _losService = losService;
            _logActionService = logActionService;
            _loanbriefModelFactory = loanbriefModelFactory;
            _drawService = drawService;
            _environment = environment;
            _loanbriefV2Service = loanbriefV2Service;
            _telesalesV2Service = telesalesV2Service;
            _configProductDetailService = configProductDetailService;
            _logCallApiCiscoService = logCallApiCiscoService;
            _shopV2Service = shopV2Service;
        }

        [Route("/loanbrief/index.html")]
        [PermissionFilter]
        public IActionResult Index()
        {
            var user = GetUserGlobal();
            var lstTeamTelesales = new List<TeamTelesales>();
            if (user.GroupId == EnumGroupUser.ManagerTelesales.GetHashCode())
            {
                var lstTeamTelesalesId = user.ListUserTeamTelesales.Select(x => x.TeamTelesalesId).ToList();
                var allTeamTelesale = _telesalesServices.GetListTeamTelesales(GetToken());
                lstTeamTelesales = allTeamTelesale.Where(x => x.TeamTelesalesId > 0 && lstTeamTelesalesId.Contains(x.TeamTelesalesId)).ToList();
            }
            ViewBag.User = user;
            ViewBag.LstTeamTelesales = lstTeamTelesales;
            ViewBag.ListTeamTelesale = lstTeamTelesales.Where(x => x.IsTelesaleTeam == true).ToList(); ;
            return View();
        }
        [PermissionFilter]
        public async Task<IActionResult> Search()
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var modal = new LoanBriefDatatable(HttpContext.Request.Form);
                DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
                DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
                if (!string.IsNullOrEmpty(modal.DateRanger))
                    DateTimeUtility.ConvertDateRanger(modal.DateRanger, ref fromDate, ref toDate);
                modal.FromDate = fromDate.ToString("yyyy/MM/dd");
                modal.ToDate = toDate.ToString("yyyy/MM/dd");
                if (user.GroupId == EnumGroupUser.Telesale.GetHashCode())
                {
                    modal.boundTelesaleId = user.UserId;
                    if (Convert.ToInt32(modal.filterRecare) != -1)
                    {
                        modal.boundTelesaleId = Convert.ToInt32(modal.filterRecare);
                        modal.teamTelesales = $"{user.TeamTelesalesId}";
                    }
                    else
                    {
                        modal.teamTelesales = string.Empty;
                    }

                }
                else if (user.GroupId == (int)EnumGroupUser.ManagerTelesales)
                {
                    if (user.ListUserTeamTelesales != null && user.ListUserTeamTelesales.Count > 0)
                        modal.teamTelesales = string.Join(",", user.ListUserTeamTelesales.Select(x => x.TeamTelesalesId).ToList());
                    if (!string.IsNullOrEmpty(modal.filterStaffTelesales))
                        modal.boundTelesaleId = Convert.ToInt32(modal.filterStaffTelesales);
                }
                else if (user.UserId == (int)EnumUser.qc_phuongnt)
                {
                    modal.teamTelesales = "1,2,3,4";
                }
                int recordsTotal = 0;
                if (!string.IsNullOrEmpty(modal.dateRangerLastChangeStatus) && !string.IsNullOrEmpty(modal.dateRangerLastChangeStatus))
                {
                    DateTime fromDateLastChangeStatus = DateTimeUtility.FromeDate(DateTime.Now);
                    DateTime toDateDateLastChangeStatus = DateTimeUtility.ToDate(DateTime.Now);
                    DateTimeUtility.ConvertDateRanger(modal.dateRangerLastChangeStatus, ref fromDateLastChangeStatus, ref toDateDateLastChangeStatus);
                    modal.FromDateLastChangeStatus = fromDateLastChangeStatus.ToString("yyyy/MM/dd");
                    modal.ToDateLastChangeStatus = toDateDateLastChangeStatus.ToString("yyyy/MM/dd");
                }
                modal.groupId = user.GroupId.Value;
                modal.userId = user.UserId;
                // Query api  
                // Query theo click ở menu
                var endPrepare = DateTime.Now;
                var lstData = new List<LoanBriefForBrower>();
                if (!string.IsNullOrEmpty(modal.filterClickTop) && Convert.ToInt32(modal.filterClickTop) > 0)
                {
                    lstData = loanBriefService.SearchClickTopTls(access_token, modal.ToQueryObject(), ref recordsTotal);
                }
                //Query theo tìm kiếm
                else
                {
                    lstData = loanBriefService.Search(access_token, modal.ToQueryObject(), ref recordsTotal);
                }
                //Lấy ra những đơn có chứng từ
                var lstLoanBriefId = lstData.Select(x => x.LoanBriefId).ToList();
                var resultImage = await loanBriefService.GetLoanBriefFileByLoanId(access_token, lstLoanBriefId);

                //lấy ra danh sách log chia đơn cho telesales
                var startTimeAM = new TimeSpan(8, 20, 0);
                var endTimePM = new TimeSpan(17, 30, 0);

                var listLoanbriefIdHotlead = lstData.Where(x => x.CreatedTime.Value.TimeOfDay >= startTimeAM
                        && x.CreatedTime.Value.TimeOfDay <= endTimePM).Select(x => x.LoanBriefId).ToList();

                var logDistributionUser = await loanBriefService.GetLogDistributionUser(access_token, new DAL.Object.GetLogDistributionUserModel()
                {
                    LstLoanBriefId = listLoanbriefIdHotlead,
                    TypeDistribution = (int)TypeDistributionLog.TELESALE
                });
                var dicLogDistributionUser = new Dictionary<string, LogDistributionUser>();
                if (logDistributionUser != null && logDistributionUser.Count > 0)
                {
                    foreach (var item in logDistributionUser)
                    {
                        if (item.UserId != (int)EnumUser.spt_System && item.UserId != (int)EnumUser.ctv_autocall
                            && item.UserId != (int)EnumUser.follow && item.UserId != (int)EnumUser.autocall_coldlead
                            && item.LoanbriefId > 0 && item.UserId > 0)
                        {
                            var key = $"{item.LoanbriefId}-{item.UserId}";
                            if (!dicLogDistributionUser.ContainsKey(key))
                                dicLogDistributionUser[key] = item;
                        }
                    }
                }
                //set permission row
                if (user.ListPermissionAction != null && user.ListPermissionAction.Count > 0)
                {
                    var now = DateTime.Now;
                    var startAM = new DateTimeOffset(now.Year, now.Month, now.Day, 08, 20, 00, new TimeSpan(7, 0, 0));
                    var endAM = new DateTimeOffset(now.Year, now.Month, now.Day, 11, 55, 00, new TimeSpan(7, 0, 0));
                    var startPM = new DateTimeOffset(now.Year, now.Month, now.Day, 13, 15, 00, new TimeSpan(7, 0, 0));
                    var endPM = new DateTimeOffset(now.Year, now.Month, now.Day, 17, 30, 00, new TimeSpan(7, 0, 0));
                    var task = Task.Run(() => Parallel.ForEach(lstData, item =>
                      {
                          //Kiểm tra đơn có đủ điều kiện check Momo
                          if (!string.IsNullOrEmpty(item.FullName) && !string.IsNullOrEmpty(item.Dob.ToString()) && !string.IsNullOrEmpty(item.NationalCard))
                              item.IsCheckMomo = true;
                          //xử lý nhãn hot lead
                          if (item.TypeRemarketing != (int)EnumTypeRemarketing.IsRemarketing)
                          {
                              if (item.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()
                              && ((item.CreatedTime >= startAM && item.CreatedTime <= endAM)
                                || (item.CreatedTime >= startPM && item.CreatedTime <= endPM)
                                ))
                              {
                                  if (item.FirstProcessingTime.HasValue)
                                      item.TypeHotLead = EnumHotLead.Responsed.GetHashCode();
                                  else
                                      item.TypeHotLead = EnumHotLead.NotResponse.GetHashCode();
                              }
                              //tính thời gian phản hồi của tls cho đơn                                  
                              if (item.BoundTelesaleId > 0)
                              {
                                  var key = $"{item.LoanBriefId}-{item.BoundTelesaleId}";
                                  if (dicLogDistributionUser.ContainsKey(key))
                                  {
                                      var dataResponseTelesalse = new LOS.DAL.Object.TimeProcessingLoanBrief()
                                      {
                                          LoanBriefId = item.LoanBriefId,
                                          IsFeedBack = false
                                      };
                                      var logDistributionUser = dicLogDistributionUser[key];
                                      if (item.FirstProcessingTime.HasValue)
                                      {
                                          dataResponseTelesalse.TimeProcessing = (int)item.FirstProcessingTime.Value.Subtract(logDistributionUser.CreatedAt.Value).TotalMinutes;
                                          dataResponseTelesalse.IsFeedBack = true;
                                      }
                                      else
                                      {
                                          dataResponseTelesalse.TimeProcessing = (int)DateTime.Now.Subtract(logDistributionUser.CreatedAt.Value).TotalMinutes;
                                      }
                                      item.TimeProcessingLoan = dataResponseTelesalse;
                                  }
                              }
                          }
                          if (item.Status != EnumLoanStatus.CANCELED.GetHashCode() && user.ListPermissionAction.Any(x => x.LoanStatus == item.Status))
                          {
                              var permission = user.ListPermissionAction.FirstOrDefault(x => x.LoanStatus == item.Status);
                              if (permission != null && permission.Value > 0)
                              {
                                  if ((EnumActionUser.Edit.GetHashCode() & permission.Value) == EnumActionUser.Edit.GetHashCode())
                                      item.IsEdit = true;
                                  if ((EnumActionUser.Script.GetHashCode() & permission.Value) == EnumActionUser.Script.GetHashCode())
                                      item.IsScript = true;
                                  if ((EnumActionUser.PushLoan.GetHashCode() & permission.Value) == EnumActionUser.PushLoan.GetHashCode())
                                      item.IsPush = true;
                                  if ((EnumActionUser.Cancel.GetHashCode() & permission.Value) == EnumActionUser.Cancel.GetHashCode())
                                      item.IsCancel = true;
                                  if ((EnumActionUser.Upload.GetHashCode() & permission.Value) == EnumActionUser.Upload.GetHashCode())
                                      item.IsUpload = true;
                                  if ((EnumActionUser.ComfirmCancel.GetHashCode() & permission.Value) == EnumActionUser.ComfirmCancel.GetHashCode())
                                      item.IsConfirmCancel = true;
                              }
                          }
                          //phần xử lý những đơn có chứng từ
                          item.UploadImage = false;
                          if (resultImage != null && resultImage.Count > 0)
                          {
                              if (resultImage.Any(x => x == item.LoanBriefId))
                                  item.UploadImage = true;
                          }
                      }));
                    Task.WaitAll(task);
                }

                modal.total = recordsTotal.ToString();

                //Returning Json Data  
                return Json(new { meta = modal, data = lstData });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult FilterLoanTop()
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            var TeamTelesales = "";
            if (user.UserId == (int)EnumUser.qc_phuongnt)
                TeamTelesales = "1,2,3,4";
            else if (user.ListUserTeamTelesales != null && user.ListUserTeamTelesales.Count > 0)
                TeamTelesales = string.Join(",", user.ListUserTeamTelesales.Select(x => x.TeamTelesalesId).ToList());
            int BoundTelesaleId = -1;
            if (user.GroupId == EnumGroupUser.Telesale.GetHashCode())
                BoundTelesaleId = user.UserId;
            var data = loanBriefService.CountLoanTLS(GetToken(), BoundTelesaleId, TeamTelesales);
            return View(data);
        }

        public IActionResult FilterStaffTLS()
        {
            var user = GetUserGlobal();

            var access_token = GetToken();
            var taskRun = new List<Task>();
            var recordingByUser = Task.Run(() => _losService.GetRecordingByUser(access_token, DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"), user.CiscoExtension));
            taskRun.Add(recordingByUser);
            var f2LTelesales = Task.Run(() => _losService.GetF2LTelesales(access_token, user.UserId));
            taskRun.Add(f2LTelesales);
            if (taskRun.Count > 0)
                Task.WaitAll(taskRun.ToArray());

            var modal = new FilterStaffTLSViewModel();
            if (recordingByUser != null && recordingByUser.Result != null && recordingByUser.Result.Count > 0)
                modal.TotalMinutesCall = Convert.ToInt32(recordingByUser.Result.Sum(x => x.callDuration).Value / 1000 / 60);
            if (f2LTelesales != null && f2LTelesales.Result != null)
            {
                modal.Form = f2LTelesales.Result.form ?? 0;
                modal.Lead = f2LTelesales.Result.lead ?? 0;
                if (modal.Form > 0 && modal.Lead > 0)
                    modal.PercentFormToLead = Math.Round((decimal)f2LTelesales.Result.lead / (decimal)f2LTelesales.Result.form, 2);
                else
                    modal.PercentFormToLead = 0;
            }
            return View(modal);
        }

        #region script telesales
        [PermissionFilter]
        public async Task<IActionResult> ScriptTelesalse(int loanBriefid, int? ProductId = 0)
        {
            var model = new LoanBriefScriptItem();
            var user = GetUserGlobal();
            ViewBag.User = user;
            if (loanBriefid > 0)
            {
                var access_token = user.Token;
                var loanBrief = await loanBriefService.GetLoanBriefById(access_token, loanBriefid);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                if (ProductId > 0)
                    loanBrief.ProductId = ProductId;
                if (loanBrief.ProductId > 0)
                {
                    var script = _dictionaryService.GetScript(access_token, (int)loanBrief.ProductId);
                    if (script == null || string.IsNullOrEmpty(script.ScriptView))
                    {
                        return Json(GetBaseObjectResult(false, "Gói sản phẩm chưa được cấu hình kịch bản"));
                    }
                    //if(user.UserId == 26464)
                    //{
                    var modelView = new LoanbriefScriptViewModel();
                    modelView.IsChangeInsurance = true;
                    modelView = loanBrief.MapToLoanbriefScriptViewModelV2();
                    //Nếu là đơn đã hủy
                    if (loanBrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                    {
                        modelView.ReMarketingLoanBriefId = loanBrief.LoanBriefId;
                        modelView.LoanBriefId = 0;
                        modelView.LoanBriefResidentModel.LoanBriefResidentId = 0;
                        modelView.LoanBriefQuestionScriptModel.LoanBriefId = 0;
                        modelView.LoanBriefPropertyModel.LoanBriefPropertyId = 0;
                        modelView.LoanBriefJobModel.LoanBriefJobId = 0;
                        if (modelView.LoanBriefRelationshipModels != null)
                            modelView.LoanBriefRelationshipModels.ForEach(x => x.Id = 0);
                        modelView.LoanAutoCall = true;
                    }
                    else
                    {
                        var objAutoCall = _dictionaryService.GetLogResultAutoCall(access_token, loanBrief.LoanBriefId);
                        if (objAutoCall != null && loanBrief.ValueCheckQualify == null)
                            modelView.LoanAutoCall = true;
                        else
                            modelView.LoanAutoCall = false;
                    }
                    //đơn tạo sau ngày 04/06/2021 => bắt buộc mua bảo hiểm tài sản
                    //if (loanBrief.CreatedTime >= new DateTime(2021, 06, 04))
                    //    modelView.IsChangeInsurance = true;
                    ViewBag.HomeNetwork = _networkService.CheckHomeNetwok(loanBrief.Phone);
                    ViewBag.ListProvince = _dictionaryService.GetProvince(GetToken());
                    ViewBag.ListJobs = CacheData.ListJobs;// _dictionaryService.GetJob(GetToken());
                    ViewBag.ListRelativeFamilys = CacheData.ListRelativeFamilys;// _dictionaryService.GetRelativeFamily(GetToken());
                    ViewBag.ListBrandProduct = CacheData.ListBrandProduct;// _dictionaryService.GetBrandProduct(GetToken());
                    ViewBag.ListLoanProduct = CacheData.ListLoanProduct;// _dictionaryService.GetLoanProduct(GetToken()).Where(x => x.Status == 1).ToList();
                    ViewBag.ListPlatformType = CacheData.ListPlatformType;// _dictionaryService.GetPlatformType(GetToken());
                    ViewBag.InfomationProductDetail = CacheData.ListPlatformType;
                    ViewBag.ListLoanPurpose = ExtensionHelper.GetEnumToList(typeof(EnumLoadLoanPurpose));
                    ViewBag.ListLogRequestAi = _logReqestAiService.GetRequest(access_token, loanBrief.LoanBriefId);
                    //ViewBag.ListInfomationProductDetail = CacheData.ListInfomationProductDetail;
                    ViewBag.ListInfomationProductDetail = null;
                    if (loanBrief.ProductId.HasValue && loanBrief.LoanBriefResident != null
                        && loanBrief.LoanBriefResident.ResidentType.HasValue)
                    {
                        ViewBag.ListInfomationProductDetail = _dictionaryService.GetInfomationProductDetailByTypeOwnershipAndProduct(access_token, loanBrief.LoanBriefResident.ResidentType.Value, loanBrief.ProductId.Value);
                    }
                    return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(modelView, "PartialScript/" + script.ScriptView)));
                    //return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(modelView, "PartialScript/ScriptMotobike_v4")));
                    //}

                    //MapperExtentions.MapToLoanBriefScriptItem(loanBrief, ref model);
                    //model.HomeNetwork = _networkService.CheckHomeNetwok(loanBrief.Phone);
                    //model.ListProvince = _dictionaryService.GetProvince(GetToken());
                    //model.ListJobs = CacheData.ListJobs;// _dictionaryService.GetJob(GetToken());
                    //model.ListRelativeFamilys = CacheData.ListRelativeFamilys;// _dictionaryService.GetRelativeFamily(GetToken());
                    //model.ListBrandProduct = CacheData.ListBrandProduct;// _dictionaryService.GetBrandProduct(GetToken());
                    //model.ListLoanProduct = CacheData.ListLoanProduct;// _dictionaryService.GetLoanProduct(GetToken()).Where(x => x.Status == 1).ToList();
                    //model.ListPlatformType = CacheData.ListPlatformType;// _dictionaryService.GetPlatformType(GetToken());
                    //model.ListLoanPurpose = ExtensionHelper.GetEnumToList(typeof(EnumLoadLoanPurpose));
                    //model.ListLogRequestAi = _logReqestAiService.GetRequest(access_token, loanBrief.LoanBriefId);
                    //model.LoanBriefQuestionScript = loanBrief.LoanBriefQuestionScript;

                    ////Tài khoản telesales test kịch bản mới : anhdt, phuongntt, phuongntm, tvv_test
                    //if (user.UserId == 26844 || user.UserId == 54577 || user.UserId == 44298 || user.UserId == 26464)
                    //    return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "PartialScript/ScriptMotobike_v2")));
                    //else
                    //return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "PartialScript/" + script.ScriptView)));
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay chưa có gói sản phẩm. Vui lòng cập nhật đơn vay"));
                }

            }
            else
            {
                return Json(GetBaseObjectResult(false, "Xảy ra lỗi khi lấy thông tin khoản vay. Vui lòng thử lại"));
            }

        }

        public async Task<IActionResult> ScriptCreateLoan(string phone)
        {
            var model = new LoanBriefScriptItem();
            var user = GetUserGlobal();
            ViewBag.User = user;
            var modelView = new LoanbriefScriptViewModel();
            modelView.LoanAutoCall = false;
            modelView.Phone = phone;
            modelView.UtmSource = "hotline";
            modelView.sBirthDay = DateTime.Now.AddYears(-18).ToString("dd/MM/yyyy");
            ViewBag.HomeNetwork = _networkService.CheckHomeNetwok(phone);
            ViewBag.ListProvince = _dictionaryService.GetProvince(GetToken());
            ViewBag.ListJobs = CacheData.ListJobs;// _dictionaryService.GetJob(GetToken());
            ViewBag.ListRelativeFamilys = CacheData.ListRelativeFamilys;// _dictionaryService.GetRelativeFamily(GetToken());
            ViewBag.ListBrandProduct = CacheData.ListBrandProduct;// _dictionaryService.GetBrandProduct(GetToken());
            ViewBag.ListLoanProduct = CacheData.ListLoanProduct;// _dictionaryService.GetLoanProduct(GetToken()).Where(x => x.Status == 1).ToList();
            ViewBag.ListPlatformType = CacheData.ListPlatformType;// _dictionaryService.GetPlatformType(GetToken());
            ViewBag.ListLoanPurpose = ExtensionHelper.GetEnumToList(typeof(EnumLoadLoanPurpose));
            ViewBag.ListLogRequestAi = null;
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(modelView, "PartialScript/ScriptMotobike_v6")));

        }

        [PermissionFilter]
        public IActionResult ListLoanBriefSearch(string phone)
        {
            if (string.IsNullOrEmpty(phone))
                return Json(GetBaseObjectResult(false, "Vui lòng nhập số điện thoại"));
            var loanBrief = loanBriefService.GetLoanBriefByPhone(GetToken(), phone);
            if (loanBrief != null && loanBrief.Count > 0)
                loanBrief = loanBrief.OrderByDescending(x => x.LoanBriefId).ToList();
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(loanBrief, "PartialScript/_ListLoanBriefSearch")));
        }
        public IActionResult ProductCurrentPrice(int productId, int typeOfOwnerShip, int productCredit, int loanBriefId)
        {
            if (productId > 0 && typeOfOwnerShip > 0)
            {
                var priceAG = 0l;
                decimal priceAI = 0l;
                var price = GetMaxPriceProduct(productId, typeOfOwnerShip, productCredit, loanBriefId, ref priceAG, ref priceAI);
                return Json(GetBaseObjectResult(true, "Success", price));
            }
            return Json(GetBaseObjectResult(true, "Xảy ra lỗi định giá xe"), 0);
        }

        #endregion

        #region SmartDailer Accept Call
        public async Task<IActionResult> AcceptCall(string phone, int? loanbriefId = 0)
        {
            var model = new LoanBriefScriptItem();
            var user = GetUserGlobal();
            ViewBag.User = user;
            try
            {
                if (!string.IsNullOrEmpty(phone) || loanbriefId > 0)
                {
                    if (loanbriefId > 0)
                    {
                        var loanBrief = await loanBriefService.GetLoanBriefById(GetToken(), loanbriefId.Value);
                        if (loanBrief != null && loanBrief.LoanBriefId > 0)
                        {
                            //cập nhật tele xử lý đơn
                            //Nếu đơn đang ở trạng thái của telesale thì => gán cho telesale đó
                            if (loanBrief.Status == EnumLoanStatus.TELESALE_ADVICE.GetHashCode()
                                || loanBrief.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode()
                                || loanBrief.Status == EnumLoanStatus.INIT.GetHashCode()
                                || loanBrief.Status == EnumLoanStatus.SALEADMIN_LOAN_DISTRIBUTING.GetHashCode()
                                || loanBrief.Status == EnumLoanStatus.SYSTEM_TELESALE_DISTRIBUTING.GetHashCode()
                                || loanBrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                            {
                                if (loanBrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                                    return await ScriptTelesalse(loanBrief.LoanBriefId, (int)EnumProductCredit.MotorCreditType_CC);

                                if (loanBrief.Status != EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode()
                                    && loanBrief.StatusTelesales != 2)//cân nhắc
                                {
                                    var taskRun = new List<Task>();

                                    //loanBriefService.UpdateBoundTelesale(GetToken(), new BoundTelesaleReq()
                                    //{
                                    //    userId = user.UserId,
                                    //    loanBriefId = loanBrief.LoanBriefId
                                    //});
                                    //taskRun.Add(loanBriefService.AddLogDistributionUser(GetToken(), new LogDistributionUser()
                                    //{
                                    //    LoanbriefId = loanBrief.LoanBriefId,
                                    //    UserId = user.UserId,
                                    //    TypeDistribution = (int)TypeDistributionLog.TELESALE,
                                    //    CreatedBy = user.UserId,
                                    //    CreatedAt = DateTime.Now
                                    //}));
                                    //var tAddNote = Task.Run(() => loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                                    //{
                                    //    LoanBriefId = loanBrief.LoanBriefId,
                                    //    Note = string.Format("Line: {0} nhận đơn", user.FullName),
                                    //    FullName = user.FullName,
                                    //    UserId = user.UserId,
                                    //    Status = 1,
                                    //    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    //    CreatedTime = DateTime.Now,
                                    //    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                    //    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                    //}));
                                    //taskRun.Add(tAddNote);

                                    //if (taskRun.Count > 0)
                                    //    Task.WaitAll(taskRun.ToArray());

                                    loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                                    {
                                        LoanBriefId = loanBrief.LoanBriefId,
                                        Note = string.Format("Line: {0} nhận đơn", user.FullName),
                                        FullName = user.FullName,
                                        UserId = user.UserId,
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                    });

                                }
                                return await ScriptTelesalse(loanBrief.LoanBriefId);
                            }
                            else
                            {
                                return Json(GetBaseObjectResult(false, "Đơn vay đang không tồn tại ở phòng TLS"));
                            }

                        }
                        else
                        {
                            return Json(GetBaseObjectResult(false, "Số điện thoại không tồn tại trong hệ thống"));
                        }
                    }
                    else
                    {
                        var loanBrief = loanBriefService.GetLoanBriefProcessingByPhone(GetToken(), phone);
                        if (loanBrief != null && loanBrief.LoanBriefId > 0)
                        {
                            var changeTelesale = true;
                            //if (loanBrief.BoundTelesaleId > 0 && loanBrief.BoundTelesaleId != (int)EnumUser.recare)
                            //{
                            //    if (loanBrief.StatusTelesales == (int)EnumStatusTelesales.NotContact // chưa liên hệ
                            //        || loanBrief.StatusTelesales.GetValueOrDefault(0) == 0 //Chưa liên hệ được
                            //    || loanBrief.StatusTelesales == (int)EnumStatusTelesales.NoContact) // Không liên hệ được
                            //    {
                            //        var logDistributionUser = await loanBriefService.GetLogDistributionUser(user.Token, new DAL.Object.GetLogDistributionUserModel()
                            //        {
                            //            LstLoanBriefId = new List<int> { loanBrief.LoanBriefId },
                            //            TypeDistribution = (int)TypeDistributionLog.TELESALE
                            //        });

                            //        if (logDistributionUser != null && logDistributionUser.Count > 0)
                            //        {
                            //            var firstRecieved = logDistributionUser.Where(x => x.UserId == loanBrief.BoundTelesaleId).FirstOrDefault();
                            //            if (firstRecieved != null)
                            //            {
                            //                var timeRecieved = (int)DateTime.Now.Subtract(firstRecieved.CreatedAt.Value).TotalMinutes;
                            //                if (loanBrief.StatusTelesales == (int)EnumStatusTelesales.NotContact
                            //                    || loanBrief.StatusTelesales.GetValueOrDefault(0) == 0)
                            //                {
                            //                    //Tls nhận đơn trong khoảng 30p thì k chia sang giỏ khác
                            //                    if (timeRecieved <= 30)
                            //                        changeTelesale = false;
                            //                }
                            //                else
                            //                {
                            //                    //tls nhận trong khoảng 2h thì không chia
                            //                    if (timeRecieved <= 120)
                            //                        changeTelesale = false;
                            //                }
                            //            }
                            //        }
                            //    }
                            //    // không chuyển giỏ tls
                            //    else if (loanBrief.Status == EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN.GetHashCode()
                            //        || loanBrief.StatusTelesales == (int)EnumStatusTelesales.Consider//cân nhắc
                            //        || loanBrief.StatusTelesales == (int)EnumStatusTelesales.SeeLater// hẹn gọi lại
                            //        )
                            //    {
                            //        changeTelesale = false;
                            //    }
                            //}
                            ////cập nhật tele xử lý đơn
                            //if (changeTelesale)
                            //{
                            //    var teles = _userServices.GetById(GetToken(), user.UserId);
                            //    if (teles == null || teles.UserId == 0)
                            //        return Json(new { status = 0, message = "Telesales không tồn tại trong hệ thống!" });
                            //    var taskRun = new List<Task>();
                            //    var request = new RangeBoundTelesaleReq()
                            //    {
                            //        userId = teles.UserId,
                            //        loanBriefIds = new List<int>() { loanBrief.LoanBriefId },
                            //        TeamTelesaleId = teles.TeamTelesalesId
                            //    };
                            //    var result = loanBriefService.UpdateRangeBoundTelesale(GetToken(), request);
                            //    if (result.meta.errorCode == 200)
                            //    {
                            //        var tAddNote = Task.Run(() => loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                            //        {
                            //            LoanBriefId = loanBrief.LoanBriefId,
                            //            Note = string.Format("Line: {0} nhận đơn", user.FullName),
                            //            FullName = user.FullName,
                            //            UserId = user.UserId,
                            //            Status = 1,
                            //            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            //            CreatedTime = DateTime.Now,
                            //            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            //            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            //        }));
                            //        taskRun.Add(tAddNote);
                            //        taskRun.Add(loanBriefService.AddLogDistributionUser(GetToken(), new LogDistributionUser()
                            //        {
                            //            LoanbriefId = loanBrief.LoanBriefId,
                            //            UserId = user.UserId,
                            //            TypeDistribution = (int)TypeDistributionLog.TELESALE,
                            //            CreatedBy = user.UserId,
                            //            CreatedAt = DateTime.Now
                            //        }));
                            //        if (taskRun.Count > 0)
                            //            Task.WaitAll(taskRun.ToArray());
                            //    }

                            //}

                            loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                            {
                                LoanBriefId = loanBrief.LoanBriefId,
                                Note = string.Format("Line: {0} nhận đơn", user.FullName),
                                FullName = user.FullName,
                                UserId = user.UserId,
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            });
                            return await ScriptTelesalse(loanBrief.LoanBriefId);
                        }
                        else
                        {
                            return await ScriptCreateLoan(phone);
                            //return Json(GetBaseObjectResult(false, "Số điện thoại không tồn tại trong hệ thống"));
                        }
                    }
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Không có số điện thoại"));
                }
            }
            catch (Exception ex)
            {
                return Json(GetBaseObjectResult(false, "Xảy ra lỗi không mong muốn. Vui lòng thử lại"));
            }
        }
        #endregion

        #region Confirm LoanBrief (PUSH Loanbrief)

        [HttpPost]
        [PermissionFilter]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> ConfirmLoanBrief(int LoanBriefId, int HubId = 0)
        {
            try
            {
                var user = GetUserGlobal();
                var loanbrief = await loanBriefService.GetLoanBriefById(GetToken(), LoanBriefId);
                if (loanbrief != null)
                {
                    var access_token = GetToken();
                    if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn vay đã được hủy trước đó.", null));

                    if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn đang được xử lý. Vui lòng thử lại sau!", null));
                    //Kiêm tra quyền tương tác dữ liệu
                    if (!CheckPermissionAction(user.ListPermissionAction, loanbrief.Status.Value, EnumActionUser.PushLoan.GetHashCode()))
                    {
                        if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                            return Json(GetBaseObjectResult(false, string.Format("Bạn không có quyền đẩy đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)), null));
                        else
                            return Json(GetBaseObjectResult(false, "Bạn không có quyền đẩy đơn!", null));
                    }
                    var ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode();
                    if (user.GroupId == EnumGroupUser.Telesale.GetHashCode() || user.GroupId == EnumGroupUser.ManagerTelesales.GetHashCode())
                    {
                        var message = string.Empty;

                        //kiểm tra quận huyên/ số tiền vay của KH
                        if (loanbrief.ProvinceId == 0 || loanbrief.DistrictId == 0)
                            message += "Vui lòng cập nhật thông tin địa chỉ Khách hàng.<br /> ";
                        if (loanbrief.LoanAmount <= 0)
                            message += "Vui lòng cập nhật số tiền khách hàng cần vay.<br /> ";
                        //Kiểm tra địa chỉ sổ hộ khẩu
                        if (loanbrief.LoanBriefHousehold == null || !loanbrief.LoanBriefHousehold.ProvinceId.HasValue
                            || loanbrief.LoanBriefHousehold.ProvinceId == 0 || !loanbrief.LoanBriefHousehold.DistrictId.HasValue
                            || loanbrief.LoanBriefHousehold.DistrictId == 0)
                            message += "Vui lòng cập thông tin địa chỉ sổ hộ khẩu.<br /> ";
                        if (loanbrief.LoanBriefQuestionScript == null || loanbrief.LoanBriefQuestionScript.QuestionMotobikeCertificate == null)
                            message += "Vui lòng cập thông tin (Khách hàng có bản gốc ĐKX/Cavet không?).<br /> ";
                        if (!string.IsNullOrEmpty(message))
                            return Json(new { status = 0, message = message });



                        if (loanbrief.Status == (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN)
                        {
                            loanbrief.TelesalesPushAt = DateTime.Now;

                            //Kiểm tra định giá của xe
                            if (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                  || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                  || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                            {
                                if (loanbrief.LoanAmountExpertiseLast.GetValueOrDefault(0) <= 0)
                                    return Json(GetBaseObjectResult(false, "Chưa có giá xe sau khi thẩm định. Vui lòng cập nhật thông tin xe máy", null));

                                if (loanbrief.LoanAmountExpertiseLast.GetValueOrDefault(0) < 3000000)
                                    return Json(GetBaseObjectResult(false, "Định giá xe của KH sau khi thẩm định nhỏ hơn 3 triệu", null));
                            }

                            //kiểm tra trường bản gốc ĐKX/Cavet
                            if (loanbrief.LoanBriefQuestionScript.QuestionMotobikeCertificate == false)
                                return Json(GetBaseObjectResult(false, "Không có bản gốc ĐKX/Cavet không đẩy được đơn. Vui lòng kiểm tra lại", null));
                        }

                        ActionComment = EnumActionComment.TelesalesPush.GetHashCode();
                    }

                    else if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode() || user.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        //loanbrief.HubPushAt = DateTime.Now;
                        //Kiểm tra đã thẩm định xong chưa. Nếu chưa thì k cho đẩy lên
                        if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                        {

                            ActionComment = EnumActionComment.StaffHubPush.GetHashCode();

                            //Nếu là nhân viên hub + đơn xe máy + Trạng thái chờ thẩm định đẩy lên CHT => gửi request lấy thông tin TTTB
                            if (loanbrief.Status == (int)EnumLoanStatus.APPRAISER_REVIEW
                                && (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))
                            {
                                if ((loanbrief.EvaluationCompanyUserId > 0 && loanbrief.EvaluationCompanyState != EnumAppraisalState.Appraised.GetHashCode())
                                || (loanbrief.EvaluationHomeUserId > 0 && loanbrief.EvaluationHomeState != EnumAppraisalState.Appraised.GetHashCode()))
                                {
                                    return Json(new { status = 0, message = "Vui lòng thẩm định trước khi đẩy lên!" });
                                }
                                var listRequest = new List<LogLoanInfoAi>();
                                // Thêm yêu cầu lấy dữ liệu TTTB
                                listRequest.Add(new LogLoanInfoAi()
                                {
                                    LoanbriefId = loanbrief.LoanBriefId,
                                    ServiceType = (int)ServiceTypeAI.EkycGetInfoNetwork,
                                    CreatedAt = DateTime.Now,
                                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest
                                });
                                // Thêm yêu cầu lấy dữ liệu đăng ký xe
                                listRequest.Add(new LogLoanInfoAi()
                                {
                                    LoanbriefId = loanbrief.LoanBriefId,
                                    ServiceType = (int)ServiceTypeAI.EkycMotorbikeRegistrationCertificate,
                                    CreatedAt = DateTime.Now,
                                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest
                                });

                                _logReqestAiService.Create(access_token, listRequest);
                            }

                        }
                        else
                        {
                            if (loanbrief.Status == (int)EnumLoanStatus.APPRAISER_REVIEW || loanbrief.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE)
                            {
                                //Check đối với gói vay xe máy => kiểm tra xe đã được thẩm đinh hay chưa số tiền sau thẩm định phải >= 3tr
                                if (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                    || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                    || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                {
                                    //Call qua ProductReviewResultDetail lấy kết quả mới nhất
                                    var productReview = _dictionaryService.GetProductReviewDetail(access_token, loanbrief.LoanBriefId);
                                    if (productReview != null && productReview.Count > 0)
                                    {
                                        if (loanbrief.TypeRemarketing != (int)EnumTypeRemarketing.DebtRevolvingLoan &&
                                            productReview[0].PriceReview.GetValueOrDefault(0) < 3000000)
                                            return Json(new { status = 0, message = "Số tiền sau thẩm định xe < 3tr!" });
                                    }
                                    else
                                    {
                                        return Json(new { status = 0, message = "Đơn vay chưa được thẩm định xe!" });
                                    }

                                    if (loanbrief.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE)
                                    {
                                        if (loanbrief.RatePercent.GetValueOrDefault(0) <= 0)
                                            return Json(new { status = 0, message = "Chưa có lãi suất. Vui lòng cập nhật đơn vay" });
                                        //Kiểm tra đơn vay đã compare hay chưa
                                        if (_baseConfig["AppSettings:Compare_Check"] != null && _baseConfig["AppSettings:Compare_Check"] == "1")
                                        {
                                            var checkCompare = _dictionaryService.CheckCompareFinish(access_token, loanbrief.LoanBriefId, (int)EnumGroupUser.StaffHub).Result;
                                            if (!checkCompare)
                                                return Json(new { status = 0, message = "Bạn phải hoàn thành kiểm tra hồ sơ trước khi đẩy đơn vay!" });
                                        }

                                        //Nếu là đơn topup và của hub DE thì kiểm tra ký hd điện tử chưa
                                        if (loanbrief.TypeRemarketing == EnumTypeRemarketing.IsTopUp.GetHashCode()
                                            && (user.ShopGlobal == (int)EnumShop.PDV_DE1
                                                || user.ShopGlobal == (int)EnumShop.PDV_DE2
                                                || user.ShopGlobal == (int)EnumShop.PDV_DE3))
                                        {
                                            if (loanbrief.EsignState != (int)EsignState.BORROWER_SIGNED && loanbrief.EsignState != (int)EsignState.LENDER_SIGNED)
                                                return Json(new { status = 0, message = "Vui lòng ký Esign hoặc chuyển sang PDV ký giấy" });

                                            if (loanbrief.ReMarketingLoanBriefId > 0)
                                            {
                                                var resultCheckTopup = await _loanbriefV2Service.CheckCanTopup(loanbrief.ReMarketingLoanBriefId.Value);
                                                if (resultCheckTopup.IsCanTopup)
                                                {
                                                    if (!resultCheckTopup.IsTopupOnTopup)
                                                    {
                                                        var typeOwnerShip = loanbrief.LoanBriefResident.ResidentType;
                                                        if (typeOwnerShip != (int)EnumTypeofownership.KT1 && typeOwnerShip != (int)EnumTypeofownership.KT3)
                                                            typeOwnerShip = loanbrief.LoanBriefResident.ResidentType;
                                                        //Kiểm tra điều kiện goi vay
                                                        if (typeOwnerShip > 0)
                                                        {
                                                            var loanbriefParent = await _loanbriefV2Service.GetBasicInfo(loanbrief.ReMarketingLoanBriefId.Value);
                                                            var productMaxMoney = 0l;
                                                            if (loanbriefParent.ProductDetailId > 0)
                                                            {
                                                                var productDetail = await _configProductDetailService.GetConfigProductCreditDetail(loanbriefParent.ProductId.Value,
                                                                        loanbriefParent.ProductDetailId.Value, typeOwnerShip.Value);
                                                                if (productDetail != null && productDetail.MaxMoney > 0)
                                                                    productMaxMoney = productDetail.MaxMoney.Value;
                                                            }
                                                            else
                                                            {
                                                                //Nếu đơn cũ không có gói chi tiết thì chọn theo giá trị vay
                                                                var listProductDetaiNext = await _configProductDetailService.GetConfigProductCreditDetailNext(loanbriefParent.ProductId.Value,
                                                                             typeOwnerShip.Value, (long)loanbriefParent.LoanAmount);
                                                                if (listProductDetaiNext != null && listProductDetaiNext.Count > 0)
                                                                {
                                                                    var productDetailNext = listProductDetaiNext[0];
                                                                    if (listProductDetaiNext.Count > 1)
                                                                        productDetailNext = listProductDetaiNext[1];
                                                                    if (loanbrief.ProductDetailId.GetValueOrDefault(0) != productDetailNext.ProductDetailId
                                                                            && listProductDetaiNext.Any(x => x.ProductDetailId == loanbrief.ProductDetailId))
                                                                        return Json(new { status = 0, message = $"Vui lòng chọn gói vay chi tiết: {productDetailNext.Name} hoặc gói thấp hơn" });
                                                                }
                                                            }

                                                            if (productMaxMoney > 0)
                                                            {
                                                                var listProductDetaiNext = await _configProductDetailService.GetConfigProductCreditDetailNext(loanbriefParent.ProductId.Value,
                                                                             typeOwnerShip.Value, productMaxMoney);
                                                                if (listProductDetaiNext != null && listProductDetaiNext.Count > 0)
                                                                {
                                                                    var productDetailNext = listProductDetaiNext.First();
                                                                    if (loanbrief.ProductDetailId.GetValueOrDefault(0) != productDetailNext.ProductDetailId
                                                                            && listProductDetaiNext.Any(x => x.ProductDetailId == loanbrief.ProductDetailId))
                                                                        return Json(new { status = 0, message = $"Vui lòng chọn gói vay chi tiết: {productDetailNext.Name} hoặc gói thấp hơn" });
                                                                }
                                                            }
                                                        }
                                                    }

                                                    var limitedLoanAmount = Common.Utils.ProductPriceUtils.LimitedLoanAmount(loanbrief.ProductId.Value, loanbrief.LoanBriefResident.ResidentType.Value);
                                                    if ((loanbrief.LoanAmount + resultCheckTopup.CurrentDebt) > limitedLoanAmount)
                                                        return Json(new { status = 0, message = string.Format("Số tiền cho vay không được lớn hơn {0}đ", limitedLoanAmount - resultCheckTopup.CurrentDebt) });
                                                }
                                                else
                                                {
                                                    //Đơn gốc không đủ điều kiện topup
                                                    return Json(new { status = 0, message = "Đơn vay không đủ điều kiện topup" });
                                                }
                                            }
                                            else
                                            {
                                                return Json(new { status = 0, message = "Không có thông tin đơn gốc của đơn Topup" });
                                            }
                                        }

                                    }
                                }
                            }
                            //Check gói vay ô tô hoặc xe máy bắt buộc nhập đủ thông tin biển số xe
                            if (loanbrief.ProductId == (int)EnumProductCredit.CamotoCreditType || loanbrief.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                                || loanbrief.ProductId == (int)EnumProductCredit.OtoCreditType_KCC || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                            {
                                if (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                {
                                    if (String.IsNullOrEmpty(loanbrief.LoanBriefProperty.PlateNumber))
                                        return Json(new { status = 0, message = "Vui lòng nhập đủ thông tin biển số xe!" });
                                }
                                else
                                {
                                    if (loanbrief.LoanBriefProperty == null || String.IsNullOrEmpty(loanbrief.LoanBriefProperty.PlateNumberCar))
                                        return Json(new { status = 0, message = "Vui lòng nhập đủ thông tin biển số xe!" });
                                }

                            }
                        }

                        //Check chứng từ
                        //Chỉ kiểm tra đối với những đơn tạo mới
                        // Đơn đang ở trạng thái chờ CHT duyệt đơn đẩy lên TĐHS
                        if (loanbrief.Status == EnumLoanStatus.HUB_CHT_APPROVE.GetHashCode() &&
                            loanbrief.IsReMarketing.GetValueOrDefault(false) == false
                            && loanbrief.IsReborrow.GetValueOrDefault(false) == false
                            && loanbrief.TypeRemarketing != EnumTypeRemarketing.IsTopUp.GetHashCode()
                            && (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT
                                || loanbrief.ProductId == (int)EnumProductCredit.OtoCreditType_CC))
                        {

                            //Kiểm tra số lượng chứng từ
                            if (_baseConfig["AppSettings:Flag_CheckList"] != null && _baseConfig["AppSettings:Flag_CheckList"] == "1")
                            {
                                if (loanbrief.LoanBriefResident == null || !loanbrief.LoanBriefResident.ResidentType.HasValue)
                                    return Json(new { status = 0, message = "Vui lòng nhập thông tin hình thức sở hữu nhà!" });
                                if (!loanbrief.LoanBriefJob.JobId.HasValue)
                                    return Json(new { status = 0, message = "Chưa có thông tin công việc của KH" });
                                var listDocument = _documentService.GetDocument(access_token, loanbrief.ProductId.Value, Common.Utils.ProductPriceUtils.ConvertEnumTypeOfOwnerShipDocument(loanbrief.LoanBriefResident.ResidentType.Value));
                                if (listDocument != null && listDocument.Count > 0)
                                {
                                    var groupJobId = 0;
                                    if (loanbrief.LoanBriefJob != null && loanbrief.LoanBriefJob.JobId.GetValueOrDefault(0) > 0)
                                    {
                                        groupJobId = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(loanbrief.LoanBriefJob.JobId.Value, loanbrief.LoanBriefJob != null && loanbrief.LoanBriefJob.ImcomeType != null ? loanbrief.LoanBriefJob.ImcomeType.Value : 0
                                                    , loanbrief.LoanBriefJob.CompanyInsurance, loanbrief.LoanBriefJob.BusinessPapers);
                                    }


                                    var listFiles = loanBriefService.GetLoanBriefFileByLoanId(access_token, LoanBriefId);

                                    var resultDocuments = LOS.WebApp.Helpers.Ultility.ConvertDocumentTree(listDocument, listFiles, 0, groupJobId);

                                    #region Kiểm tra chứng từư

                                    var listDocumentBreack = new List<int>();
                                    //Nếu nhóm công việc là Grab => bỏ qua thẩm định nơi làm việc
                                    if (loanbrief.LoanBriefJob.JobId.Value == EnumJobTitle.LamTaiXeCongNghe.GetHashCode())
                                        listDocumentBreack.Add((int)EnumDocumentType.ThamDinhNoiLamViec);

                                    var documentInvalid = LOS.WebApp.Helpers.Ultility.ValidRequiredDocument(resultDocuments, listDocumentBreack);
                                    if (documentInvalid != null)
                                    {
                                        return Json(new { status = 0, message = "Chứng từ " + documentInvalid.Name + " chưa đủ điều kiện!" });
                                    }
                                    #endregion

                                }
                            }
                        }
                        if (loanbrief.Status == EnumLoanStatus.HUB_CHT_APPROVE.GetHashCode())
                        {
                            ActionComment = EnumActionComment.HubPush.GetHashCode();
                            //Kiểm tra đơn đã được đẩy lên lần nào chưa
                            if (loanbrief.HubPushAt != null)
                                loanbrief.ApproverStatusDetail = (int)EnumLoanStatusDetailApprover.BoSungGiayTo;
                            loanbrief.HubPushAt = DateTime.Now;
                        }
                        //kiểm tra xem phải đơn định vị không
                        //nếu phải kiểm tra xem có imei chưa
                        //chưa có bắt nhập
                        if (loanbrief.IsLocate == true && string.IsNullOrEmpty(loanbrief.DeviceId))
                        {
                            return Json(new { status = 0, message = "Vui lòng nhập thông tin imei của định vị!" });
                        }
                    }
                    else if (user.GroupId == EnumGroupUser.ApproveEmp.GetHashCode() && loanbrief.TypeRemarketing != (int)EnumTypeRemarketing.DebtRevolvingLoan)
                    {
                        //Check xác nhận check bank
                        if (loanbrief.IsCheckBank == false || loanbrief.IsCheckBank == null)
                        {
                            //Kiểm tra có chứng từ check bank hay không
                            var documentType = _dictionaryService.GetDocumentType(GetToken(), loanbrief.ProductId.Value);
                            var typeBank = documentType.Where(x => x.Name == "CheckBank").FirstOrDefault();
                            if (typeBank != null)
                            {
                                var file = loanBriefService.GetLoanBriefFileByLoanId(GetToken(), loanbrief.LoanBriefId);
                                var fileBank = file.Where(x => x.TypeId == typeBank.Id).FirstOrDefault();
                                if (fileBank == null)
                                    return Json(new { status = 0, message = "Đơn vay chưa được cung cấp chứng từ check bank" });
                            }
                        }
                        loanbrief.CoordinatorPushAt = DateTime.Now;
                        ActionComment = EnumActionComment.ApprovePush.GetHashCode();
                    }
                    else if (user.GroupId == EnumGroupUser.ApproveLeader.GetHashCode())
                    {
                        loanbrief.CoordinatorPushAt = DateTime.Now;
                    }
                    if (user.GroupId == EnumGroupUser.BOD.GetHashCode())
                    {
                        ActionComment = EnumActionComment.BODPush.GetHashCode();
                    }

                    loanbrief.ActionState = EnumActionPush.Push.GetHashCode();
                    loanbrief.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                    if (HubId > 0)
                        loanbrief.HubId = HubId;
                    var resultPush = loanBriefService.PushLoanBrief(GetToken(), loanbrief);
                    if (resultPush > 0)
                    {
                        var taskRun = new List<Task>();
                        //Thêm note
                        var taskNote = Task.Run(() => loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = resultPush,
                            Note = string.Format("Hợp đồng HĐ-{0} đã được {1} đẩy đi", LoanBriefId, user.Group.GroupName),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = ActionComment,
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(taskNote);

                        //Log Action
                        var taskLogAction = Task.Run(() => _logActionService.AddLogAction(access_token, new LogLoanAction
                        {
                            LoanbriefId = loanbrief.LoanBriefId,
                            ActionId = (int)EnumLogLoanAction.LoanPush,
                            TypeAction = (int)EnumTypeAction.Manual,
                            LoanStatus = loanbrief.Status,
                            TlsLoanStatusDetail = loanbrief.DetailStatusTelesales,
                            HubLoanStatusDetail = loanbrief.LoanStatusDetailChild,
                            TelesaleId = loanbrief.BoundTelesaleId,
                            HubId = loanbrief.HubId,
                            HubEmployeeId = loanbrief.HubEmployeeId,
                            CoordinatorUserId = loanbrief.CoordinatorUserId,
                            UserActionId = user.UserId,
                            GroupUserActionId = user.GroupId,
                            CreatedAt = DateTime.Now
                        }));
                        taskRun.Add(taskLogAction);
                        Task.WaitAll(taskRun.ToArray());
                        //End
                        return Json(new { status = 1, message = "Đẩy hợp đồng thành công" });
                    }
                    else
                        return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });
                }
                else
                {
                    return Json(new { status = 0, message = "Không tìm thấy thông tin khoản vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        public IActionResult GetLoanByTelesale(int TelesaleId)
        {
            try
            {
                // Query api   	
                var data = loanBriefService.GetLoanTelesaleId(GetToken(), TelesaleId);
                int count = data.Count();
                //Returning Json Data  
                return Json(new { data = data, count = count });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> ChangeSupport(string[] ArrId, int TelesaleId, int? teamTelesaleId = 0)
        {
            var user = GetUserGlobal();
            var teles = _userServices.GetById(GetToken(), TelesaleId);
            if (teles == null || teles.UserId == 0)
                return Json(new { status = 0, message = "Telesales không tồn tại trong hệ thống!" });
            var teamTelesale = teles.TeamTelesalesId;
            //Nếu user tls nhận đơn thuộc team System => bắt buộc phải có teamId
            if (teles.TeamTelesalesId == (int)EnumTeamTelesales.SystemTeam)
            {

                teamTelesale = 0;
                //Nếu là tls chuyển đơn về giỏ recare_team => không thay đổi team của đơn vay
                if (user.GroupId == (int)EnumGroupUser.Telesale && teles.UserId == (int)EnumUser.recare_team)
                {
                    teamTelesale = user.TeamTelesalesId;
                }
                else
                {
                    if (teamTelesaleId > 0 && user.ListUserTeamTelesales != null
                    && user.ListUserTeamTelesales.Count > 1
                    && user.ListUserTeamTelesales.Any(x => x.TeamTelesalesId == teamTelesaleId))
                        teamTelesale = teamTelesaleId;
                }
            }
            var request = new RangeBoundTelesaleReq()
            {
                userId = teles.UserId,
                loanBriefIds = new List<int>(),
                TeamTelesaleId = teamTelesale
            };
            var teamName = string.Empty;
            var teamId = request.TeamTelesaleId;
            if (teamId.GetValueOrDefault(0) == 0)
                teamId = teles.TeamTelesalesId;
            if (teamId > 0)
            {
                if (teamId == (int)EnumTeamTelesales.SystemTeam
                    && request.TeamTelesaleId.GetValueOrDefault(0) == 0)
                {
                    teamId = user.TeamTelesalesId.Value;
                }
                var team = _telesalesServices.GetTelesalesTeam(GetToken(), teamId.Value);
                if (team != null && !string.IsNullOrEmpty(team.Name))
                    teamName = team.Name;
            }
            var requestLoanbriefNote = new List<LoanBriefNote>();
            var requestLogDistributionUser = new List<LogDistributionUser>();
            for (int i = 0; i < ArrId.Length; i++)
            {
                var req = new BoundTelesaleReq();
                req.userId = TelesaleId;
                req.loanBriefId = Convert.ToInt32(ArrId[i]);
                //kiểm tra trạng thái
                var loanbrief = await loanBriefService.GetBasicInfo(GetToken(), req.loanBriefId);
                if (loanbrief != null)
                {
                    //kiểm tra trạng thái đơn đang ở tls mới cho chuyển
                    if (loanbrief.Status == (int)EnumLoanStatus.INIT
                        || loanbrief.Status == (int)EnumLoanStatus.WAIT_CUSTOMER_PREPARE_LOAN
                        || loanbrief.Status == (int)EnumLoanStatus.TELESALE_ADVICE)
                    {
                        request.loanBriefIds.Add(loanbrief.LoanBriefId);

                        requestLoanbriefNote.Add(new LoanBriefNote
                        {
                            LoanBriefId = loanbrief.LoanBriefId,
                            Note = string.Format("Hợp đồng HĐ-{0} đã được {1} chuyển cho telesalse {2}({3}) hỗ trợ", loanbrief.LoanBriefId, user.Username, teles.Username, teamName),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.SaleAdminSplitLoan.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        requestLogDistributionUser.Add(new LogDistributionUser()
                        {
                            LoanbriefId = loanbrief.LoanBriefId,
                            UserId = TelesaleId,
                            TypeDistribution = (int)TypeDistributionLog.TELESALE,
                            CreatedBy = user.UserId,
                            CreatedAt = DateTime.Now
                        });
                    }
                }
            }

            if (request.loanBriefIds != null && request.loanBriefIds.Count > 0)
            {
                var result = loanBriefService.UpdateRangeBoundTelesale(GetToken(), request);
                if (result.meta.errorCode == 200)
                {
                    var taskRun = new List<Task>();
                    taskRun.Add(loanBriefService.AddRangeLoanBriefNote(GetToken(), requestLoanbriefNote));
                    taskRun.Add(loanBriefService.AddRangeLogDistributionUser(GetToken(), requestLogDistributionUser));
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());

                    return Json(new { status = 1, message = "Chuyển telesale hỗ trợ thành công" });
                }
                else
                {
                    return Json(new { status = 0, message = "Xảy ra lỗi khi chuyển hỗ trợ. Vui lòng thử lại sau!" });
                }

            }
            else
            {
                return Json(new { status = 0, message = "Không có thông tin đơn vay trong hệ thống!" });
            }
        }

        #region cancel loabrief
        [HttpGet]
        public IActionResult GetReasonDetail(int groupId)
        {
            // Query api   	
            var data = _reasonService.GetReasonDetail(GetToken(), groupId);
            //Returning Json Data  
            return Json(new { data = data });
        }
        [HttpGet]
        [PermissionFilter]
        public async Task<IActionResult> CancelLoanBrief(int loanbriefId, int questionQlf = 0)
        {
            var user = GetUserGlobal();
            if (loanbriefId > 0)
            {
                var access_token = GetToken();
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, loanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn vay đã được hủy trước đó.", null));

                    if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn đang được xử lý. Vui lòng thử lại sau!", null));

                    //Kiêm tra quyền tương tác dữ liệu
                    if (!CheckPermissionAction(user.ListPermissionAction, loanbrief.Status.Value, EnumActionUser.Cancel.GetHashCode()))
                    {
                        if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                            return Json(GetBaseObjectResult(false, string.Format("Bạn không có quyền hủy đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)), null));
                        else
                            return Json(GetBaseObjectResult(false, "Bạn không có quyền hủy đơn"));
                    }

                    var model = new LoanBriefCancelItem();
                    model.LoanbriefId = loanbriefId;
                    if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode() || user.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        model.HasGroupReason = false;
                        model.ListReasonDetail = _reasonService.GetReasonGroup(access_token, EnumTypeReason.HubCancel.GetHashCode());
                    }
                    else if (user.GroupId == EnumGroupUser.Telesale.GetHashCode() || user.GroupId == EnumGroupUser.ManagerTelesales.GetHashCode())
                    {
                        //kiểm tra xem đơn có trong giỏ của tls không
                        if (user.GroupId == EnumGroupUser.Telesale.GetHashCode() && user.UserId != loanbrief.BoundTelesaleId)
                            return Json(GetBaseObjectResult(false, "Đơn không phải của bạn. Bạn không có quyền hủy ", null));

                        //nếu là ô tô thì show ra lý do hủy của ô tô
                        if (loanbrief.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                        {
                            model.HasGroupReason = false;
                            model.ListReasonDetail = _reasonService.GetReasonGroup(access_token, EnumTypeReason.LoanCarTeleSalesCancel.GetHashCode()).ToList();
                        }
                        else
                        {
                            if (questionQlf == 4 && loanbrief.ProvinceId != null
                                && (loanbrief.ProvinceId == (int)EnumProvince.HaNoi || loanbrief.ProvinceId == (int)EnumProvince.HCM
                                || loanbrief.ProvinceId == (int)EnumProvince.BacNinh || loanbrief.ProvinceId == (int)EnumProvince.VinhPhuc
                                || loanbrief.ProvinceId == (int)EnumProvince.HaiDuong || loanbrief.ProvinceId == (int)EnumProvince.BinhDuong
                                || loanbrief.ProvinceId == (int)EnumProvince.BacGiang))
                            {
                                model.ListReasonGroup = _reasonService.GetReasonGroup(access_token, EnumTypeReason.TeleSalesCancel.GetHashCode()).Where(x => x.Id != 651).ToList();
                            }
                            else
                                model.ListReasonGroup = _reasonService.GetReasonGroup(access_token, EnumTypeReason.TeleSalesCancel.GetHashCode()).Where(x => x.Id == 651).ToList();

                            ////đơn của đối tác mmo và aff
                            //if (!string.IsNullOrEmpty(loanbrief.Tid))
                            //    model.ListReasonGroup = _reasonService.GetReasonGroup(access_token, EnumTypeReason.TeleSalesCancel.GetHashCode());
                            //else
                            ////đơn inhouse
                            //{
                            //    if (questionQlf == 4 && loanbrief.ProvinceId != null && (loanbrief.ProvinceId == (int)EnumProvince.HaNoi || loanbrief.ProvinceId == (int)EnumProvince.HCM || loanbrief.ProvinceId == (int)EnumProvince.BacNinh || loanbrief.ProvinceId == (int)EnumProvince.VinhPhuc || loanbrief.ProvinceId == (int)EnumProvince.HaiDuong))
                            //    {
                            //        model.ListReasonGroup = _reasonService.GetReasonGroup(access_token, EnumTypeReason.TeleSalesCancel.GetHashCode()).Where(x => x.Id != 651).ToList();
                            //    }
                            //    else
                            //        model.ListReasonGroup = _reasonService.GetReasonGroup(access_token, EnumTypeReason.TeleSalesCancel.GetHashCode()).Where(x => x.Id == 651).ToList();
                            //}
                        }
                    }
                    else if (user.GroupId == EnumGroupUser.ApproveLeader.GetHashCode() || user.GroupId == EnumGroupUser.ApproveEmp.GetHashCode())
                    {
                        model.ListReasonGroup = _reasonService.GetReasonGroup(access_token, EnumTypeReason.Tdhs2Cancel.GetHashCode());
                    }
                    else
                    {
                        return Json(GetBaseObjectResult(false, "Nhóm của bạn không có quyền hủy đơn", null));
                    }
                    return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "_CancelLoanBrief")));
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống", null));
                }
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
            }

        }
        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> CancelLoanBrief(int LoanBriefId, int ReasonDetail, string CommentCancel)
        {
            var user = GetUserGlobal();
            if (LoanBriefId > 0)
            {
                var access_token = GetToken();
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, LoanBriefId);
                if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                    return Json(GetBaseObjectResult(false, "Đơn vay đã được hủy trước đó.", null));

                if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
                    return Json(GetBaseObjectResult(false, "Đơn đang được xử lý. Vui lòng thử lại sau!", null));

                //Kiêm tra quyền tương tác dữ liệu
                if (!CheckPermissionAction(user.ListPermissionAction, loanbrief.Status.Value, EnumActionUser.Cancel.GetHashCode()))
                {
                    if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                        return Json(GetBaseObjectResult(false, string.Format("Bạn không có quyền hủy đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)), null));
                    else
                        return Json(GetBaseObjectResult(false, "Bạn không có quyền hủy đơn", null));
                }

                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    var actionComment = EnumActionComment.CommentLoanBrief.GetHashCode();
                    var derpartment = "";
                    if (user.GroupId == EnumGroupUser.Telesale.GetHashCode() || user.GroupId == EnumGroupUser.ManagerTelesales.GetHashCode())
                    {
                        if (loanbrief.FirstProcessingTime == null)
                            loanbrief.FirstProcessingTime = DateTime.Now;
                        derpartment = "Telesale";
                        actionComment = EnumActionComment.TelesaleCancel.GetHashCode();
                    }
                    else if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode() || user.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        if (!string.IsNullOrEmpty(loanbrief.DeviceId))
                        {
                            //gọi sang erp check thiết bị
                            var checkDevice = _erpService.CheckStatusDevice(access_token, loanbrief.DeviceId);
                            if (checkDevice != null && checkDevice.serial == loanbrief.DeviceId && checkDevice.status == "Active")
                                return Json(new { status = 0, message = "Phải gỡ thiết bị mới được hủy đơn" });
                        }

                        derpartment = "Hub";
                        actionComment = EnumActionComment.HubCancel.GetHashCode();
                    }
                    else if (user.GroupId == EnumGroupUser.ApproveEmp.GetHashCode() || user.GroupId == EnumGroupUser.ApproveLeader.GetHashCode())
                    {
                        derpartment = "TĐHS";
                        actionComment = EnumActionComment.TĐHSCancel.GetHashCode();

                    }
                    loanbrief.ReasonCancel = ReasonDetail;
                    loanbrief.LoanBriefComment = string.Format("{0} {1} hủy đơn: {2}", derpartment, user.FullName, CommentCancel);
                    loanbrief.ActionState = EnumActionPush.Cancel.GetHashCode();
                    loanbrief.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                    loanbrief.InProcess = EnumInProcess.Process.GetHashCode();
                    loanbrief.LoanBriefCancelAt = DateTime.Now;
                    loanbrief.LoanBriefCancelBy = user.UserId;


                    if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode() || user.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                       || user.GroupId == EnumGroupUser.ApproveEmp.GetHashCode() || user.GroupId == EnumGroupUser.ApproveLeader.GetHashCode())
                    {
                        var reason = await _reasonService.GetDayInBlacklist(access_token, ReasonDetail);
                        if (reason != null && reason.DayInBlacklist.HasValue && reason.DayInBlacklist > 0)
                        {
                            //gọi api insert black list sang lms
                            var objAddBlacklist = new RequestAddBlacklist()
                            {
                                CustomerCreditId = loanbrief.CustomerId ?? 0,
                                FullName = loanbrief.FullName,
                                NumberPhone = loanbrief.Phone,
                                CardNumber = loanbrief.NationalCard,
                                BirthDay = loanbrief.Dob ?? null,
                                UserIdCreate = user.UserId,
                                UserNameCreate = user.Username,
                                FullNameCreate = user.FullName,
                                Note = CommentCancel,
                                NumberDay = reason.DayInBlacklist.Value,
                                LoanCreditId = loanbrief.LoanBriefId
                            };
                            _lmsService.AddBlackList(access_token, objAddBlacklist);
                        }
                    }

                    var result = loanBriefService.CancelLoanBrief(access_token, loanbrief);
                    if (result > 0)
                    {
                        var listTask = new List<Task>();
                        //Thêm note
                        Task task = Task.Run(() => loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                        {
                            LoanBriefId = result,
                            Note = CommentCancel,
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = actionComment,
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        listTask.Add(task);
                        //Gọi sang bên AI hủy đơn định vị
                        if (loanbrief.IsLocate == true || !string.IsNullOrEmpty(loanbrief.DeviceId))
                        {
                            if (_trackDeviceService.CloseContract(access_token, string.Format("HD-{0}", loanbrief.LoanBriefId), loanbrief.DeviceId, loanbrief.LoanBriefId))
                            {
                                loanbrief.DeviceStatus = StatusOfDeviceID.Close.GetHashCode();
                                loanBriefService.UpdateStatusDevice(access_token, loanbrief);
                                Task t1 = Task.Run(() => loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                                {
                                    LoanBriefId = result,
                                    Note = string.Format("Đóng hợp đồng HD-{0} bên ginno", loanbrief.LoanBriefId),
                                    FullName = user.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.CloseContractGinno.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = user.UserId,
                                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                }));
                                listTask.Add(t1);
                                //đóng hợp đồng bên ERP
                                Task t2 = Task.Run(() => CloseDeviceToERP(new PushDeviceReq()
                                {
                                    loanId = loanbrief.LoanBriefId,
                                    serial = loanbrief.DeviceId,
                                    status = (int)StatusOfDeviceID.Close
                                }));
                                listTask.Add(t2);

                            }
                        }

                        //Log Action
                        var taskLogAction = Task.Run(() => _logActionService.AddLogAction(access_token, new LogLoanAction
                        {
                            LoanbriefId = loanbrief.LoanBriefId,
                            ActionId = (int)EnumLogLoanAction.LoanCancel,
                            TypeAction = (int)EnumTypeAction.Manual,
                            LoanStatus = loanbrief.Status,
                            TlsLoanStatusDetail = loanbrief.DetailStatusTelesales,
                            HubLoanStatusDetail = loanbrief.LoanStatusDetailChild,
                            TelesaleId = loanbrief.BoundTelesaleId,
                            HubId = loanbrief.HubId,
                            HubEmployeeId = loanbrief.HubEmployeeId,
                            CoordinatorUserId = loanbrief.CoordinatorUserId,
                            UserActionId = user.UserId,
                            GroupUserActionId = user.GroupId,
                            CreatedAt = DateTime.Now
                        }));
                        listTask.Add(taskLogAction);
                        //End

                        Task.WaitAll(listTask.ToArray());
                        return Json(new { status = 1, message = "Hủy hợp đồng thành công" });
                    }
                    else
                    {
                        return Json(new { status = 0, message = "Xảy ra lỗi khi hủy đơn" });
                    }
                }
                else
                {
                    return Json(new { status = 0, message = "Không tìm thấy thông tin khoản vay" });
                }
            }
            else
            {
                return Json(new { status = 0, message = "Không có mã đơn" });
            }
        }

        private void CloseDeviceToERP(PushDeviceReq req)
        {
            try
            {
                var access_token = GetToken();
                if (_erpService.UpdateDeviceStatus(access_token, req))
                {
                    var user = GetUserGlobal();
                    //Thêm comment
                    loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = req.loanId,
                        Note = string.Format("Gọi đóng hoạt thiết bị sang ERP"),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CloseContractERP.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "CloseDeviceToERP Exception");
            }
        }

        [HttpPost]
        public async Task<IActionResult> ConfirmCancelLoanBrief(int LoanBriefId)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, LoanBriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn vay đã được hủy trước đó.", null));

                    if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn đang được xử lý. Vui lòng thử lại sau!", null));

                    //Kiêm tra quyền tương tác dữ liệu
                    if (!CheckPermissionAction(user.ListPermissionAction, loanbrief.Status.Value, EnumActionUser.ComfirmCancel.GetHashCode()))
                    {
                        if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                            return Json(GetBaseObjectResult(false, string.Format("Bạn không có quyền duyệt hủy đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)), null));
                        else
                            return Json(GetBaseObjectResult(false, "Bạn không có quyền duyệt hủy đơn!", null));
                    }

                    loanbrief.ActionState = EnumActionPush.Cancel.GetHashCode();
                    loanbrief.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                    loanbrief.InProcess = EnumInProcess.Process.GetHashCode();
                    var result = loanBriefService.CancelLoanBrief(access_token, loanbrief);
                    if (result > 0)
                    {
                        var actionComment = EnumActionComment.TelesaleCancel.GetHashCode();
                        if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                            actionComment = EnumActionComment.HubCancel.GetHashCode();
                        if (user.GroupId == EnumGroupUser.BOD.GetHashCode())
                            actionComment = EnumActionComment.BODCancel.GetHashCode();
                        //Thêm note
                        var listTask = new List<Task>();
                        Task t1 = Task.Run(() => loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                        {
                            LoanBriefId = result,
                            Note = string.Format("{0} phê duyệt hủy đơn.", user.FullName),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = actionComment,
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        listTask.Add(t1);
                        //Gọi sang bên AI hủy đơn định vị
                        if (loanbrief.IsLocate == true)
                        {
                            if (_trackDeviceService.CloseContract(access_token, string.Format("HD-{0}", loanbrief.LoanBriefId), loanbrief.DeviceId, loanbrief.LoanBriefId))
                            {
                                loanbrief.Status = EnumDeviceStatusGinno.InStock.GetHashCode();
                                loanBriefService.UpdateStatusDevice(access_token, loanbrief);
                                Task t2 = Task.Run(() => loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                                {
                                    LoanBriefId = result,
                                    Note = string.Format("Đóng hợp đồng HD-{0} bên ginno", loanbrief.LoanBriefId),
                                    FullName = user.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.CloseContractGinno.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = user.UserId,
                                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                }));
                                listTask.Add(t2);
                                //đóng hợp đồng bên ERP
                                Task t3 = Task.Run(() => CloseDeviceToERP(new PushDeviceReq()
                                {
                                    loanId = loanbrief.LoanBriefId,
                                    serial = loanbrief.DeviceId,
                                    status = (int)StatusOfDeviceID.Close
                                }));
                                listTask.Add(t3);
                            }
                        }
                        Task.WaitAll(listTask.ToArray());
                        return Json(new { status = 1, message = "Hủy hợp đồng thành công" });
                    }
                    else
                    {
                        return Json(new { status = 0, message = "Xảy ra lỗi khi hủy đơn" });
                    }
                }
                else
                {
                    return Json(new { status = 0, message = "Không tìm thấy thông tin khoản vay" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region for Hub
        [Route("/loanbrief/manager-hub.html")]
        [PermissionFilter]
        public IActionResult ManagerHub()
        {
            var user = GetUserGlobal();
            var access_token = user.Token;
            ViewBag.User = user;
            var model = new LoanbriefHubViewModel();
            if (user.ShopGlobal > 0)
                model.ListUserOfHub = _userServices.GetStaff(access_token, user.ShopGlobal);
            model.ListHub = _shopServices.GetAllHub(access_token);
            if (user.ShopGlobal > 0)
                model.ListHub = model.ListHub.Where(x => x.ShopId != user.ShopGlobal).ToList();

            var lstStatusDetail = _loanStatusDetailService.GetLoanStatusDetail(user.Token, (int)EnumLoanStatusDetail.CVKD);
            ViewBag.ListStatusDetail = lstStatusDetail;
            return View(model);
        }

        [Route("/loanbrief/staff-hub.html")]
        [PermissionFilter]
        public IActionResult StaffHub()
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            var lstStatusDetail = _loanStatusDetailService.GetLoanStatusDetail(user.Token, (int)EnumLoanStatusDetail.CVKD);
            return View(lstStatusDetail);
        }
        [PermissionFilter]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetDataHub()
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var model = new LoanBriefHubDatatable(HttpContext.Request.Form);
                var requestModel = await _loanbriefModelFactory.PrepareHubRequestModels(model, user);
                int recordsTotal = 0;
                var data = loanBriefService.GetDataForHub(access_token, requestModel.ToQueryObject(), ref recordsTotal);
                var dataView = new List<LoanBriefDetail>();
                if (data != null && data.Count > 0)
                    dataView = await _loanbriefModelFactory.PrepareLoanbriefHubOverviewModels(data, user);
                model.total = recordsTotal.ToString();
                return Json(new { meta = model, data = dataView });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [PermissionFilter]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetDataStaffHub()
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var model = new LoanBriefHubDatatable(HttpContext.Request.Form);
                var requestModel = await _loanbriefModelFactory.PrepareStaffHubRequestModels(model, user);
                int recordsTotal = 0;
                var data = loanBriefService.GetDataForHub(access_token, requestModel.ToQueryObject(), ref recordsTotal);
                var dataView = new List<LoanBriefDetail>();
                if (data != null && data.Count > 0)
                    dataView = await _loanbriefModelFactory.PrepareLoanbriefStaffHubOverviewModels(data, user);
                model.total = recordsTotal.ToString();
                return Json(new
                {
                    meta = model,
                    data = dataView
                });
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Loanbrief/GetDataStaffHub");
                throw;
            }
        }

        [HttpPost]
        [PermissionFilter]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> TranferLoanForHub(int hubId, List<int> loanBriefs)
        {
            try
            {
                if (hubId > 0)
                {
                    var user = GetUserGlobal();
                    var access_token = user.Token;
                    if (user.GroupId != EnumGroupUser.ManagerHub.GetHashCode())
                        return Json(new { isSuccess = 0, message = "Bạn k có quyền chuyển cho hub khác hỗ trợ." });
                    if (loanBriefs == null || loanBriefs.Count == 0)
                        return Json(new { isSuccess = 0, message = "Vui lòng chọn danh sách đơn vay" });
                    var shop = _shopServices.GetById(access_token, hubId);
                    if (shop == null || shop.ShopId == 0)
                        return Json(new { isSuccess = 0, message = "Hub chuyển đến không tồn tại trong hệ thống" });

                    var listWaitCHTDistributing = await loanBriefService.GetListLoanFromIdAndStatus(access_token, loanBriefs, (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING);
                    var listWaitAppraiser = await loanBriefService.GetListLoanFromIdAndStatus(access_token, loanBriefs, (int)EnumLoanStatus.APPRAISER_REVIEW);
                    var taskRun = new List<Task>();
                    if (listWaitCHTDistributing != null && listWaitCHTDistributing.Count > 0)
                    {
                        if (user.ShopGlobal == (int)EnumShop.PDV_DE1 || user.ShopGlobal == (int)EnumShop.PDV_DE2 || user.ShopGlobal == (int)EnumShop.PDV_DE3)
                        {
                            foreach (var item in listWaitCHTDistributing)
                            {
                                var shopSupport = await _shopV2Service.GetShopSupport(item.ProductId.Value, item.DistrictId.Value, item.WardId);
                                if (shopSupport == null || shopSupport.ShopId == 0)
                                    shopSupport = shop;
                                if (shopSupport == null || shopSupport.ShopId == 0)
                                    continue;
                                var isChangeHub = await loanBriefService.ChangeHub(access_token, new ChangeHubSupportItem() { HubId = shopSupport.ShopId, loanbriefIds = new List<int> { item.LoanBriefId } });
                                if (isChangeHub)
                                {
                                    taskRun.Add(Task.Run(() => loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                                    {
                                        LoanBriefId = item.LoanBriefId,
                                        Note = string.Format("Hợp đồng HĐ-{0} đã được {1} chuyển sang {2} hỗ trợ", item, user.Username, shopSupport.Name),
                                        FullName = user.FullName,
                                        Status = 1,
                                        ActionComment = EnumActionComment.TranferHub.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = user.UserId,
                                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                    })));
                                }
                            }
                        }
                        else
                        {
                            var loanbriefIds = listWaitCHTDistributing.Select(x => x.LoanBriefId).ToList();
                            var result = loanBriefService.ChangeHub(access_token, new ChangeHubSupportItem() { HubId = shop.ShopId, loanbriefIds = loanbriefIds });
                            if (result.Result)
                            {
                                foreach (var item in loanbriefIds)
                                {
                                    var task = Task.Run(() => loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                                    {
                                        LoanBriefId = item,
                                        Note = string.Format("Hợp đồng HĐ-{0} đã được {1} chuyển sang {2} hỗ trợ", item, user.Username, shop.Name),
                                        FullName = user.FullName,
                                        Status = 1,
                                        ActionComment = EnumActionComment.TranferHub.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = user.UserId,
                                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                    }));
                                    taskRun.Add(task);
                                }
                            }
                        }
                    }

                    if (listWaitAppraiser != null && listWaitAppraiser.Count > 0)
                    {
                        foreach (var item in listWaitAppraiser)
                        {
                            if (user.ShopGlobal == (int)EnumShop.PDV_DE1
                                || user.ShopGlobal == (int)EnumShop.PDV_DE2
                                || user.ShopGlobal == (int)EnumShop.PDV_DE3)
                            {
                                var shopSupport = await _shopV2Service.GetShopSupport(item.ProductId.Value, item.DistrictId.Value, item.WardId);
                                if (shopSupport != null && shopSupport.ShopId > 0)
                                    shop = shopSupport;
                            }
                            var listHubEmployee = _erpService.GetStaffHub(access_token, shop.ShopId, item.LoanBriefId);
                            var userHub = _userServices.UserLoanAsc(access_token, new HubEmployeeReq()
                            {
                                hubId = shop.ShopId,
                                userIds = listHubEmployee != null && listHubEmployee.Count > 0 ? listHubEmployee : null
                            });
                            if (userHub != null && userHub.UserId > 0)
                            {
                                var result = await loanBriefService.ChangeHubAndEmployee(access_token, new ChangeHubSupportItem()
                                { HubId = shop.ShopId, loanbriefIds = new List<int> { item.LoanBriefId }, HubEmployee = userHub.UserId });
                                if (result)
                                {
                                    taskRun.Add(Task.Run(() => loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                                    {
                                        LoanBriefId = item.LoanBriefId,
                                        Note = string.Format("Hợp đồng HĐ-{0} đã được {1} chuyển sang {2} hỗ trợ", item.LoanBriefId, user.Username, shop.Name),
                                        FullName = user.FullName,
                                        Status = 1,
                                        ActionComment = EnumActionComment.TranferHub.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = user.UserId,
                                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                    })));
                                    taskRun.Add(Task.Run(() => loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                                    {
                                        LoanBriefId = item.LoanBriefId,
                                        Note = string.Format("Hợp đồng HĐ-{0} đã được chia cho CVKD {1}", item.LoanBriefId, userHub.Username),
                                        FullName = "Auto System",
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = user.UserId,
                                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                    })));

                                    //Thêm log chuyển cvkd 
                                    taskRun.Add(loanBriefService.AddLogDistributionUser(GetToken(), new LogDistributionUser()
                                    {
                                        LoanbriefId = item.LoanBriefId,
                                        UserId = userHub.UserId,
                                        HubId = shop.ShopId,
                                        TypeDistribution = (int)TypeDistributionLog.CVKD,
                                        CreatedBy = user.UserId,
                                        CreatedAt = DateTime.Now
                                    }));
                                }
                            }

                        }
                    }

                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    return Json(new { isSuccess = 1, message = "Chuyển hỗ trợ thành công đơn vay cho " + shop.Name });
                }
                return Json(new { isSuccess = 0, message = "Vui lòng chọn hub hỗ trợ" });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [PermissionFilter]
        public async Task<IActionResult> DistributingLoanOfHub(int loanbriefId)
        {
            var user = GetUserGlobal();
            if (loanbriefId > 0)
            {
                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, loanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn vay đã được hủy trước đó.", null));

                    if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn đang được xử lý. Vui lòng thử lại sau!", null));

                    //Kiêm tra quyền tương tác dữ liệu
                    if (!CheckPermissionAction(user.ListPermissionAction, loanbrief.Status.Value, EnumActionUser.PushLoan.GetHashCode()))
                    {
                        if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                            return Json(new { status = 0, message = string.Format("Bạn không có quyền đẩy đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)) });
                        else
                            return Json(new { status = 0, message = "Bạn không có quyền đẩy đơn" });
                    }

                    if (loanbrief.Status != EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING.GetHashCode())
                    {
                        if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                            return Json(new { status = 0, message = string.Format("Bạn không có quyền đẩy đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)) });
                        else
                            return Json(new { status = 0, message = "Bạn không có quyền đẩy đơn" });
                    }

                    var model = new DistributingLoanOfHubItem();
                    model.LoanBriefId = loanbriefId;
                    if (user.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        if (user.ShopGlobal > 0)
                            model.ListUserOfHub = _userServices.GetStaff(access_token, user.ShopGlobal);
                    }
                    else
                    {
                        return Json(GetBaseObjectResult(false, "Nhóm của bạn không có quyền hủy đơn", null));
                    }
                    return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "_DistributingLoanOfHub")));
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống", null));
                }
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
            }

        }

        [HttpPost]
        [PermissionFilter]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> DistributingLoanOfHub(int LoanBriefId, int StaffHub, int PlaceOfAppraisal)
        {
            var user = GetUserGlobal();
            if (LoanBriefId > 0)
            {
                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, LoanBriefId);
                if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                    return Json(GetBaseObjectResult(false, "Đơn vay đã được hủy trước đó.", null));

                if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
                    return Json(GetBaseObjectResult(false, "Đơn đang được xử lý. Vui lòng thử lại sau!", null));

                //Kiêm tra quyền tương tác dữ liệu
                if (!CheckPermissionAction(user.ListPermissionAction, loanbrief.Status.Value, EnumActionUser.PushLoan.GetHashCode()))
                {
                    if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                        return Json(new { status = 0, message = string.Format("Bạn không có quyền đẩy đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)) });
                    else
                        return Json(new { status = 0, message = "Bạn không có quyền đẩy đơn" });
                }

                if (loanbrief.Status != EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING.GetHashCode())
                {
                    if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                        return Json(new { status = 0, message = string.Format("Bạn không có quyền đẩy đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)) });
                    else
                        return Json(new { status = 0, message = "Bạn không có quyền đẩy đơn" });
                }
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    var userStaff = _userServices.GetById(access_token, StaffHub);

                    if (userStaff == null || userStaff.UserId == 0)
                        return Json(GetBaseObjectResult(false, "Nhân viên thẩm định không tồn tại trong hệ thống!", null));

                    var sNote = string.Empty;
                    loanbrief.HubEmployeeId = StaffHub;
                    if (PlaceOfAppraisal == EnumAppraisal.AppraisalHome.GetHashCode())
                    {
                        loanbrief.EvaluationHomeUserId = StaffHub;
                        loanbrief.EvaluationHomeState = EnumFieldSurveyState.NotAppraised.GetHashCode();
                        sNote = string.Format("{0} phải đi thẩm định nhà", userStaff.FullName);
                    }
                    else if (PlaceOfAppraisal == EnumAppraisal.AppraisalCompany.GetHashCode())
                    {
                        loanbrief.EvaluationCompanyUserId = StaffHub;
                        loanbrief.EvaluationCompanyState = EnumFieldSurveyState.NotAppraised.GetHashCode();
                        sNote = string.Format("{0} phải đi thẩm định công ty", userStaff.FullName);
                    }
                    else if (PlaceOfAppraisal == EnumAppraisal.AppraisalHomeAndCompany.GetHashCode())
                    {
                        loanbrief.EvaluationHomeUserId = StaffHub;
                        loanbrief.EvaluationHomeState = EnumFieldSurveyState.NotAppraised.GetHashCode();
                        loanbrief.EvaluationCompanyUserId = StaffHub;
                        loanbrief.EvaluationCompanyState = EnumFieldSurveyState.NotAppraised.GetHashCode();
                        sNote = string.Format("{0} phải đi thẩm định nhà và công ty", userStaff.FullName);
                    }
                    else
                    {
                        sNote = string.Format("{0} phải thẩm định xe. Không phải đi thẩm định nhà và công ty", userStaff.FullName);
                    }
                    loanbrief.ActionState = EnumActionPush.Push.GetHashCode();
                    loanbrief.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();

                    var result = loanBriefService.DistributingStaffHub(access_token, loanbrief);
                    if (result > 0)
                    {
                        //Thêm note
                        loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                        {
                            LoanBriefId = result,
                            Note = sNote,
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.DistributingLoanOfHub.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        return Json(GetBaseObjectResult(true, "Phân đơn cho nhân viên xử lý thành công", null));
                    }
                    else
                    {
                        return Json(GetBaseObjectResult(false, "Xảy ra lỗi khi phân đơn cho nhân viên!", null));
                    }
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống!", null));
                }
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không có mã đơn vay", null));
            }
        }


        [HttpGet]
        public IActionResult BackToStaffHubInit(int LoanBriefId)
        {
            var user = GetUserGlobal();
            var reationList = _reasonService.GetReasonGroup(user.Token, (int)EnumTypeReason.HubCancel);
            ViewBag.LoanID = LoanBriefId;

            return View(reationList);
        }
        [HttpPost]
        [PermissionFilter]
        // [ValidateAntiForgeryToken]

        public async Task<IActionResult> BackToStaffHub([FromBody] ReturnLoanReq entity)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, entity.LoanBriefId);
                if (loanbrief != null)
                {
                    if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        return Json(new { status = 0, message = "Đơn vay đã được hủy trước đó." });

                    if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
                        return Json(new { status = 0, message = "Đơn đang được xử lý. Vui lòng thử lại sau!" });

                    if (loanbrief.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                    {
                        if (loanbrief.Status == (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING && loanbrief.BoundTelesaleId.GetValueOrDefault(0) == 0)
                            return Json(new { status = 0, message = "Bạn không có quyền trả lại đơn oto về bước bước" });
                    }

                    if (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                        || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                        || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                    {
                        if ((loanbrief.Status == (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING || loanbrief.Status == (int)EnumLoanStatus.APPRAISER_REVIEW)
                            && loanbrief.BoundTelesaleId.GetValueOrDefault(0) == 0)
                            return Json(new { status = 0, message = "Bạn không có quyền trả lại đơn xe máy tự tạo" });
                    }
                    if (user.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        //Kiêm tra quyền tương tác dữ liệu
                        if (!CheckPermissionAction(user.ListPermissionAction, loanbrief.Status.Value, EnumActionUser.BackLoan.GetHashCode()))
                        {
                            if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                                return Json(GetBaseObjectResult(false, string.Format("Bạn không có quyền trả lại đơn đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)), null));
                            else
                                return Json(GetBaseObjectResult(false, "Bạn không có quyền trả lại đơn!", null));
                        }

                        loanbrief.ActionState = EnumActionPush.Back.GetHashCode();
                        loanbrief.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                        var resultPush = loanBriefService.PushLoanBrief(access_token, loanbrief);
                        if (resultPush > 0)
                        {
                            var sNote = string.Format("Hợp đồng HĐ-{0} đã được {1} trả lại với lý do:{2}.<br /> Comment: {3}", entity.LoanBriefId, user.FullName, entity.LstReason[0].ReasonName, entity.Comment);
                            if (loanbrief.Status == EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING.GetHashCode())
                                sNote = string.Format("Hợp đồng HĐ-{0} đã được {1} trả lại", entity.LoanBriefId, user.FullName);
                            //Thêm note
                            loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                            {
                                LoanBriefId = resultPush,
                                Note = sNote,
                                FullName = user.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.ManagerHubBack.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = user.UserId,
                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            });
                            //Add ReasonCoordinator
                            entity.UserId = user.UserId;
                            entity.GroupId = 2;
                            _dictionaryService.AddReasonCoordinator(GetToken(), entity);
                            return Json(new { status = 1, message = "Trả lại hợp đồng thành công." });
                        }
                        else
                            return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });
                    }
                    else
                    {
                        return Json(new { status = 0, message = "Bạn không có quyền trả lại đơn vay này" });
                    }
                }
                else
                {
                    return Json(new { status = 0, message = "Không tìm thấy thông tin khoản vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        //Mở cho KH ký lại hợp đồng
        [HttpPost]
        public async Task<IActionResult> ReEsign(int loanbriefId)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetBasicInfo(access_token, loanbriefId);
                if (loanbrief != null)
                {
                    if (loanbrief.Status != (int)EnumLoanStatus.HUB_CHT_APPROVE && loanbrief.Status != (int)EnumLoanStatus.APPRAISER_REVIEW)
                        return Json(new { status = 0, message = "Đơn vay không còn ở hub xử lý" });

                    if (loanbrief.EsignState != (int)EsignState.BORROWER_SIGNED && loanbrief.EsignState != (int)EsignState.LENDER_SIGNED)
                        return Json(new { status = 0, message = "KH chưa ký hợp đồng điện tử" });

                    var response = await loanBriefService.ReEsignCustomer(GetToken(), new DAL.Object.Tool.ToolReq.ReEsignReq()
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        EsignNumber = 2
                    });
                    if (response.Success)
                    {
                        //Thêm note
                        loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = loanbrief.LoanBriefId,
                            Note = "Hợp đồng đã được mở cho Khách hàng ký lại hợp đồng điện tử",
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = (int)EnumActionComment.CommentLoanBrief,
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        return Json(new { status = 1, message = "Mở cho KH ký lại thành công" });
                    }
                    else
                        return Json(new { status = 0, message = response.Message });
                }
                else
                {
                    return Json(new { status = 0, message = "Không tìm thấy thông tin khoản vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region Upload file
        [HttpGet]
        [PermissionFilter]
        public async Task<IActionResult> UploadLoanBrief(int loanbriefId)
        {
            var user = GetUserGlobal();
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var model = new LoanBriefFileViewModel();
            if (loanbriefId > 0)
            {
                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, loanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    model.LoanBriefId = loanbrief.LoanBriefId;
                    if (loanbrief.ProductId > 0)
                    {
                        if (loanbrief.LoanBriefResident == null || !loanbrief.LoanBriefResident.ResidentType.HasValue)
                            return Json(GetBaseObjectResult(false, "Chưa có thông tin Hình thức sở hữu nhà của KH"));
                        if (loanbrief.LoanBriefJob == null || !loanbrief.LoanBriefJob.JobId.HasValue)
                            return Json(GetBaseObjectResult(false, "Chưa có thông tin công việc của KH"));
                        //Check gói sản phẩm khác xe máy
                        //if (loanbrief.ProductId == (int)EnumProductCredit.OtoCreditType_CC || loanbrief.ProductId == (int)EnumProductCredit.OtoCreditType_KCC)
                        //{
                        //    var ListDocument = _dictionaryService.GetDocumentType(access_token, loanbrief.ProductId.Value);
                        //    if (ListDocument != null && ListDocument.Count > 0)
                        //    {
                        //        var listFiles = loanBriefService.GetLoanBriefFileByLoanId(access_token, loanbriefId);
                        //        model.ListDocumentV2 = Ultility.ConvertDocumentOto(ListDocument, listFiles);
                        //    }
                        //}
                        //else
                        //{
                        var listDocument = _dictionaryService.GetDocumentTypeV2(GetToken(), loanbrief.ProductId.Value, Common.Utils.ProductPriceUtils.ConvertEnumTypeOfOwnerShipDocument(loanbrief.LoanBriefResident.ResidentType.Value));
                        if (listDocument != null && listDocument.Count > 0)
                        {
                            var imcomeType = 0;
                            bool? companyInsurance = null;
                            bool? businessPapers = null;
                            if (loanbrief.LoanBriefJob != null)
                            {
                                imcomeType = loanbrief.LoanBriefJob.ImcomeType.HasValue ? loanbrief.LoanBriefJob.ImcomeType.Value : 0;
                                companyInsurance = loanbrief.LoanBriefJob.CompanyInsurance != null ? loanbrief.LoanBriefJob.CompanyInsurance : null;
                                businessPapers = loanbrief.LoanBriefJob.BusinessPapers != null ? loanbrief.LoanBriefJob.BusinessPapers : null;
                            }
                            var groupJobId = 0;
                            if (loanbrief.LoanBriefJob != null && loanbrief.LoanBriefJob.JobId.GetValueOrDefault(0) > 0)
                                groupJobId = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(loanbrief.LoanBriefJob.JobId.Value, imcomeType, companyInsurance, businessPapers);
                            var listFiles = loanBriefService.GetLoanBriefFileByLoanId(access_token, loanbriefId);
                            model.ListDocumentV2 = Ultility.ConvertDocumentTree(listDocument, listFiles, 0, groupJobId);
                        }
                        else
                            return Json(GetBaseObjectResult(false, "Không lấy được danh mục chứng từ"));
                        //}


                        //if (!loanbrief.LoanBriefJob.JobId.HasValue)
                        //    return Json(GetBaseObjectResult(false, "Chưa có thông tin công việc của KH"));
                        //model.LoanBriefId = loanbrief.LoanBriefId;
                        //var ListDocument = _dictionaryService.GetDocumentType(access_token, loanbrief.ProductId.Value);
                        //if (ListDocument != null && ListDocument.Count > 0)
                        //{
                        //    model.ListDocument = ListDocument;

                        //    if (loanbrief.ProductId != (int)EnumProductCredit.OtoCreditType_CC && loanbrief.ProductId != (int)EnumProductCredit.OtoCreditType_KCC)
                        //    {
                        //        var doc = ListDocument.Where(x => x.Version == 2).ToList();
                        //        var workRuleType = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(loanbrief.LoanBriefJob.JobId.Value, loanbrief.LoanBriefJob != null && loanbrief.LoanBriefJob.ImcomeType != null ? loanbrief.LoanBriefJob.ImcomeType.Value : 0
                        //                    , loanbrief.LoanBriefJob.CompanyInsurance, loanbrief.LoanBriefJob.BusinessPapers);

                        //        model.ListDocumentNew = Ultility.GetListDocumentNewV2(doc, loanbrief.LoanBriefResident.ResidentType.Value, workRuleType
                        //                            , !String.IsNullOrEmpty(loanbrief.LoanBriefResident.ResultLocation) ? loanbrief.LoanBriefResident.ResultLocation : "");
                        //        model.DocumentType = model.ListDocument.First().Id;

                        //    }
                        //    //model.ListLoanBriefFile = loanBriefService.GetLoanBriefFile(access_token, loanbriefId, model.DocumentType);
                        //    model.ListLoanBriefFile = loanBriefService.GetLoanBriefFileByLoanId(access_token, loanbriefId);
                        //    if (model.ListLoanBriefFile != null && model.ListLoanBriefFile.Count > 0)
                        //    {
                        //        foreach (var item in model.ListLoanBriefFile)
                        //        {
                        //            if (string.IsNullOrEmpty(item.FilePath)) continue;
                        //            if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                        //                item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                        //            if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                        //                item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                        //            if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        //                item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                        //        }
                        //    }
                        //}
                        return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "_UploadLoanBriefFile")));
                    }
                    else
                        return Json(GetBaseObjectResult(false, "Đơn vay chưa có gói sản phẩm", null));
                }
                else
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống", null));
            }
            else
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
        }


        //public async Task<IActionResult> UploadFile(IFormFile file, int documentId, int loanBriefId)
        //{
        //    if (file != null)
        //    {
        //        var user = GetUserGlobal();

        //        var accessKey = _configuration["AppSettings:AwsAccessKeyId"];
        //        var accessSecret = _configuration["AppSettings:AwsSecretAccessKey"];
        //        var bucket = "Tima";
        //        var extension = Path.GetExtension(file.FileName);
        //        var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
        //        if (allowedExtensions.Contains(extension.ToLower()))
        //        {
        //            string ImageName = Guid.NewGuid().ToString() + extension;

        //            var client = new AmazonS3Client(accessKey, accessSecret, Amazon.RegionEndpoint.EUCentral1);

        //            // get the file and convert it to the byte[]
        //            byte[] fileBytes = new Byte[file.Length];
        //            file.OpenReadStream().Read(fileBytes, 0, Int32.Parse(file.Length.ToString()));

        //            // create unique file name for prevent the mess
        //            var fileName = Guid.NewGuid() + file.FileName;

        //            PutObjectResponse response = null;

        //            using (var stream = new MemoryStream(fileBytes))
        //            {
        //                var request = new PutObjectRequest
        //                {
        //                    BucketName = bucket,
        //                    Key = fileName,
        //                    InputStream = stream,
        //                    ContentType = file.ContentType,
        //                    CannedACL = S3CannedACL.PublicRead
        //                };
        //                try
        //                {
        //                    response = await client.PutObjectAsync(request);
        //                }
        //                catch(Exception ex)
        //                {

        //                }

        //            };
        //            if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
        //            {
        //                // this model is up to you, in my case I have to use it following;
        //                var result = loanBriefService.AddLoanBriefFile(GetToken(), new LoanBriefFiles()
        //                {
        //                    LoanBriefId = loanBriefId,
        //                    CreateAt = DateTime.Now,
        //                    Status = 1,
        //                    UserId = user.UserId,
        //                    FilePath = ImageName,
        //                    TypeId = documentId
        //                });
        //                if (result > 0)
        //                    return Json(new { status = 1, message = "Upload ảnh thành công" });
        //                else
        //                    return Json(new { status = 0, message = "Xảy ra lỗi khi upload ảnh" });
        //            }
        //            else
        //            {
        //                return Json(new { status = 0, message = "Xảy ra lỗi khi upload ảnh" });
        //            }
        //            //using (var client = new AmazonS3Client(accessKey, accessSecret, RegionEndpoint.USEast1))
        //            //{
        //            //    using (var newMemoryStream = new MemoryStream())
        //            //    {
        //            //        file.CopyTo(newMemoryStream);

        //            //        var uploadRequest = new TransferUtilityUploadRequest
        //            //        {
        //            //            InputStream = newMemoryStream,
        //            //            Key = ImageName,
        //            //            BucketName = "Image",
        //            //            CannedACL = S3CannedACL.PublicRead
        //            //        };

        //            //        var fileTransferUtility = new TransferUtility(client);
        //            //       await fileTransferUtility.Upload(uploadRequest);
        //            //    }
        //            //}

        //            //insert to db
        //            //var result = loanBriefService.AddLoanBriefFile(GetToken(), new LoanBriefFiles()
        //            //{
        //            //    LoanBriefId = loanBriefId,
        //            //    CreateAt = DateTime.Now,
        //            //    Status = 1,
        //            //    UserId = user.UserId,
        //            //    FilePath = ImageName,
        //            //    TypeId = documentId
        //            //});
        //            //if (result > 0)
        //            //    return Json(new { status = 1, message = "Upload ảnh thành công" });
        //            //else
        //            //    return Json(new { status = 0, message = "Xảy ra lỗi khi upload ảnh" });
        //        }
        //        else
        //        {
        //            return Json(new { status = 0, message = "Vui lòng chọn đúng định dạng ảnh" });
        //        }
        //    }
        //    else
        //    {
        //        return Json(new { status = 0, message = "Không tồn tại file hình ảnh" });
        //    }
        //}
        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> UploadFile(List<IFormFile> files, int documentId, int loanBriefId, string documentName)
        {
            if (files != null)
            {
                var typeUpload = 1;
                var result = 0;
                var user = GetUserGlobal();
                var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                var BucketName = _baseConfig["AppSettings:BucketName"];
                var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
                {
                    RegionEndpoint = RegionEndpoint.APSoutheast1
                });
                var folder = "";
                if (documentName.ToLower().Contains("video"))
                    folder = BucketName + "/uploads/LOS/video/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
                else
                    folder = BucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
                foreach (var file in files)
                {
                    var link = "";
                    var linkThumb = "";
                    var extension = Path.GetExtension(file.FileName);
                    string[] allowedExtensions;
                    if (documentName.ToLower().Contains("video"))
                        allowedExtensions = new[] { ".mp4", ".webm", ".ogg", ".mov" };
                    else
                        allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                    if (allowedExtensions.Contains(extension.ToLower()))
                    {
                        string img = Guid.NewGuid().ToString();
                        string ImageName = img + extension;
                        string ImageThumb = img + "-thumb" + extension;
                        using (var stream = new MemoryStream())
                        {
                            file.CopyTo(stream);

                            PutObjectRequest req = new PutObjectRequest()
                            {
                                InputStream = stream,
                                BucketName = folder,
                                Key = ImageName,
                                CannedACL = S3CannedACL.PublicRead
                            };
                            await client.PutObjectAsync(req);
                            if (documentName.ToLower().Contains("video"))
                            {
                                link = "/uploads/LOS/video/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                                typeUpload = 2;
                            }
                            else
                            {
                                //Crop image
                                using var image = Image.Load(file.OpenReadStream());
                                if (image.Width > 500)
                                {
                                    image.Mutate(x => x.Resize(200, image.Height / (image.Width / 200)));

                                    var outputStream = new MemoryStream();
                                    image.Save(outputStream, new JpegEncoder());
                                    outputStream.Seek(0, 0);
                                    //Upload Thumb
                                    PutObjectRequest reqThumb = new PutObjectRequest()
                                    {
                                        InputStream = outputStream,
                                        BucketName = folder,
                                        Key = ImageThumb,
                                        CannedACL = S3CannedACL.PublicRead
                                    };
                                    await client.PutObjectAsync(reqThumb);
                                    linkThumb = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + ImageThumb;
                                    //image.Save(file.FileName);
                                    //return Ok();
                                }
                                //End
                                link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                            }


                            //insert to db
                            result = loanBriefService.AddLoanBriefFile(GetToken(), new LoanBriefFiles()
                            {
                                LoanBriefId = loanBriefId,
                                CreateAt = DateTime.Now,
                                Status = 1,
                                UserId = user.UserId,
                                FilePath = link,
                                TypeId = documentId,
                                S3status = 1,
                                MecashId = 1000000000,
                                FileThumb = linkThumb,
                                SourceUpload = (int)EnumSourceUpload.Web
                            });
                        }

                    }
                    else
                    {
                        return Json(new { status = 0, message = "Vui lòng chọn đúng định dạng" });
                    }
                }
                if (result > 0)
                    return Json(new { status = 1, message = "Upload ảnh thành công" });
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi khi upload ảnh" });
            }
            else
            {
                return Json(new { status = 0, message = "Không tồn tại file hình ảnh" });
            }
        }
        [HttpGet]
        public async Task<IActionResult> GetDocumetView(int loanbriefId, int documentId)
        {
            var user = GetUserGlobal();
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var model = new LoanBriefFileViewModel();
            if (loanbriefId > 0)
            {
                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, loanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    var IsDelete = true;
                    if ((loanbrief.Status == (int)EnumLoanStatus.DISBURSED || loanbrief.Status == (int)EnumLoanStatus.FINISH) && user.GroupId == (int)EnumGroupUser.Telesale)
                        IsDelete = false;
                    model.IsDelete = IsDelete;
                    if (loanbrief.ProductId > 0)
                    {
                        model.LoanBriefId = loanbrief.LoanBriefId;
                        model.DocumentType = documentId;
                        model.ListLoanBriefFile = loanBriefService.GetLoanBriefFile(access_token, loanbriefId, documentId);
                        if (model.ListLoanBriefFile != null && model.ListLoanBriefFile.Count > 0)
                        {
                            foreach (var item in model.ListLoanBriefFile)
                            {
                                if (string.IsNullOrEmpty(item.FilePath)) continue;
                                if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                                    item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                                if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                                    item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                                if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                                    item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                            }
                        }
                        return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "_ViewFile")));
                    }
                    else
                    {
                        return Json(GetBaseObjectResult(false, "Đơn vay chưa có gói sản phẩm", null));
                    }
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống", null));
                }
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
            }

        }

        [HttpGet]
        public async Task<IActionResult> GetUploadMulti(int loanbriefId)
        {
            var user = GetUserGlobal();
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var model = new LoanBriefFileViewModel();
            if (loanbriefId > 0)
            {

                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, loanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    var IsDelete = true;
                    if ((loanbrief.Status == (int)EnumLoanStatus.DISBURSED || loanbrief.Status == (int)EnumLoanStatus.FINISH) && user.GroupId == (int)EnumGroupUser.Telesale)
                        IsDelete = false;
                    model.IsDelete = IsDelete;
                    if (loanbrief.ProductId > 0)
                    {
                        if (!loanbrief.LoanBriefJob.JobId.HasValue)
                            return Json(GetBaseObjectResult(false, "Chưa có thông tin công việc của KH"));
                        if (loanbrief.LoanBriefResident == null || !loanbrief.LoanBriefResident.ResidentType.HasValue)
                            return Json(new { status = 0, message = "Vui lòng nhập thông tin hình thức sở hữu nhà!" });
                        if (!loanbrief.LoanBriefJob.JobId.HasValue)
                            return Json(new { status = 0, message = "Chưa có thông tin công việc của KH" });
                        model.LoanBriefId = loanbrief.LoanBriefId;
                        var allFiles = loanBriefService.GetLoanBriefFileByLoanId(access_token, loanbriefId);
                        var listLoanFiles = new List<LoanBriefFileDetail>();
                        //lấy ra d/s các chứng từ không nằm trong danh mục
                        if (allFiles != null && allFiles.Count > 0)
                        {
                            var listDocumentDetail = new List<DocumentMapInfo>();
                            if (loanbrief.ProductId.Value == (int)EnumProductCredit.OtoCreditType_CC)
                                allFiles = allFiles.Where(x => x.TypeId.GetValueOrDefault(0) == 0).ToList();
                            var listDocument = _documentService.GetDocument(access_token, loanbrief.ProductId.Value, Common.Utils.ProductPriceUtils.ConvertEnumTypeOfOwnerShipDocument(loanbrief.LoanBriefResident.ResidentType.Value));
                            if (listDocument != null && listDocument.Count > 0)
                            {
                                var imcomeType = 0;
                                bool? companyInsurance = null;
                                bool? businessPapers = null;
                                if (loanbrief.LoanBriefJob != null)
                                {
                                    imcomeType = loanbrief.LoanBriefJob.ImcomeType.HasValue ? loanbrief.LoanBriefJob.ImcomeType.Value : 0;
                                    companyInsurance = loanbrief.LoanBriefJob.CompanyInsurance != null ? loanbrief.LoanBriefJob.CompanyInsurance : null;
                                    businessPapers = loanbrief.LoanBriefJob.BusinessPapers != null ? loanbrief.LoanBriefJob.BusinessPapers : null;
                                }
                                var groupJobId = 0;
                                if (loanbrief.LoanBriefJob != null && loanbrief.LoanBriefJob.JobId.GetValueOrDefault(0) > 0)
                                    groupJobId = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(loanbrief.LoanBriefJob.JobId.Value, imcomeType, companyInsurance, businessPapers);
                                listDocumentDetail = Ultility.ConvertDocumentTreeDetail(listDocument, new List<LoanBriefFileDetail>(), groupJobId);
                            }
                            await Task.Run(() => Parallel.ForEach(allFiles, item =>
                            {
                                if (item.TypeId.GetValueOrDefault(0) == 0
                                   || (listDocumentDetail != null && !listDocumentDetail.Any(x => x.Id == item.TypeId)))
                                {
                                    if (!string.IsNullOrEmpty(item.FileThumb))
                                        item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                                    if (!string.IsNullOrEmpty(item.FilePath))
                                    {
                                        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                                            item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                                        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                                            item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                                        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                                            item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                                        listLoanFiles.Add(item);
                                    }
                                }
                            }));
                            //foreach (var item in allFiles)
                            //{
                            //    if (item.TypeId.GetValueOrDefault(0) == 0
                            //        || (listDocumentDetail != null && !listDocumentDetail.Any(x => x.Id == item.TypeId)))
                            //    {
                            //        if (!string.IsNullOrEmpty(item.FileThumb))
                            //            item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                            //        if (string.IsNullOrEmpty(item.FilePath)) continue;
                            //        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                            //            item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                            //        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                            //            item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                            //        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                            //            item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                            //        listLoanFiles.Add(item);
                            //    }
                            //}


                            //List<int> DocumentTypeIds = new List<int>();
                            //var listDocument = _dictionaryService.GetDocumentType(access_token, loanbrief.ProductId.Value);

                            //var doc = listDocument.Where(x => x.Version == 2).ToList();
                            //var workRuleType = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(loanbrief.LoanBriefJob.JobId.Value, loanbrief.LoanBriefJob != null && loanbrief.LoanBriefJob.ImcomeType != null ? loanbrief.LoanBriefJob.ImcomeType.Value : 0
                            //            , loanbrief.LoanBriefJob.CompanyInsurance, loanbrief.LoanBriefJob.BusinessPapers);

                            //var ListDocumentOfLoanbrief = Ultility.GetListDocumentNewV2(doc, loanbrief.LoanBriefResident.ResidentType.Value, workRuleType
                            //                    , !String.IsNullOrEmpty(loanbrief.LoanBriefResident.ResultLocation) ? loanbrief.LoanBriefResident.ResultLocation : "");

                            //if (ListDocumentOfLoanbrief != null && ListDocumentOfLoanbrief.Count > 0)
                            //{
                            //    foreach (var item in ListDocumentOfLoanbrief)
                            //    {
                            //        if (item.LstDocumentChild != null && item.LstDocumentChild.Count > 0)
                            //        {
                            //            foreach (var docChild in item.LstDocumentChild)
                            //            {
                            //                if (docChild.Id > 0 && !DocumentTypeIds.Contains(docChild.Id))
                            //                    DocumentTypeIds.Add(docChild.Id);
                            //            }
                            //        }
                            //        else
                            //        {
                            //            if (item.Id > 0 && !DocumentTypeIds.Contains(item.Id))
                            //                DocumentTypeIds.Add(item.Id);
                            //        }
                            //    }
                            //    //DocumentTypeIds = ListDocumentOfLoanbrief.Select(x => x.Id).Distinct().ToList();
                            //}
                            //var listNotDocument = allFiles.Where(x => (x.TypeId.GetValueOrDefault(0) == 0)
                            //    || (x.TypeId > 0 && !DocumentTypeIds.Contains(x.TypeId.Value))).ToList();

                        }
                        model.ListLoanBriefFile = listLoanFiles;
                        //if (model.ListLoanBriefFile != null && model.ListLoanBriefFile.Count > 0)
                        //{
                        //    foreach (var item in model.ListLoanBriefFile)
                        //    {
                        //        if (!string.IsNullOrEmpty(item.FileThumb))
                        //            item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                        //        if (string.IsNullOrEmpty(item.FilePath)) continue;
                        //        if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                        //            item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                        //        if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                        //            item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                        //        if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        //            item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                        //    }
                        //}
                        return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "_ViewFileMulti")));
                    }
                    else
                    {
                        return Json(GetBaseObjectResult(false, "Đơn vay chưa có gói sản phẩm", null));
                    }
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống", null));
                }
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
            }

        }

        #endregion

        #region Add Device
        [PermissionFilter]
        public async Task<IActionResult> AddDevice(int loanbriefId)
        {
            var user = GetUserGlobal();
            if (loanbriefId > 0)
            {
                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, loanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn vay đã được hủy trước đó.", null));

                    var model = new AddDeviceViewModel();
                    model.LoanBriefId = loanbriefId;
                    model.LastSendRequest = loanbrief.LastSendRequestActive;
                    model.ActivedDeviceAt = loanbrief.ActivedDeviceAt;
                    if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode() || user.GroupId == EnumGroupUser.ManagerHub.GetHashCode()
                        || user.GroupId == EnumGroupUser.ApproveLeader.GetHashCode() || user.GroupId == EnumGroupUser.ApproveEmp.GetHashCode())
                    {
                        model.DeviceCode = loanbrief.DeviceId;
                        model.DeviceStatus = loanbrief.DeviceStatus.GetValueOrDefault();
                        if (Enum.IsDefined(typeof(StatusOfDeviceID), loanbrief.DeviceStatus ?? 0))
                        {
                            model.DeviceStatusName = ExtensionHelper.GetDescription((StatusOfDeviceID)(loanbrief.DeviceStatus ?? 0));
                        }
                    }
                    else
                    {
                        return Json(GetBaseObjectResult(false, "Bạn không có quyền cập nhật imei thiết bị", null));
                    }
                    return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "_AddDevice")));
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống", null));
                }
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
            }

        }
        [PermissionFilter]
        public async Task<IActionResult> AddCodeDevice(int LoanBriefId, string DeviceId)
        {
            var user = GetUserGlobal();
            if (LoanBriefId > 0)
            {
                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, LoanBriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn vay đã được hủy trước đó.", null));

                    bool isChangeImei = false;
                    var timeSendActiveDevice = loanbrief.LastSendRequestActive;
                    //nếu TH thay đổi mã imei 
                    if (!string.IsNullOrEmpty(loanbrief.DeviceId) && loanbrief.DeviceId != DeviceId.Trim())
                    {
                        isChangeImei = true;
                        timeSendActiveDevice = null;
                    }
                    if (isChangeImei || loanbrief.DeviceStatus.GetValueOrDefault(0) == StatusOfDeviceID.InActived.GetHashCode())
                    {
                        var contractType = "thuong";
                        if (loanbrief.TypeRemarketing == EnumTypeRemarketing.IsTopUp.GetHashCode())
                            contractType = "topup";
                        //gọi api tạo hợp đồng
                        //gọi api mở hợp đồng
                        var resultOpent = _trackDeviceService.OpenContract(access_token, "HD-" + loanbrief.LoanBriefId, DeviceId, user.ShopGlobal.ToString(), loanbrief.ProductId.Value, loanbrief.LoanBriefId, contractType);
                        //StatusCode == 3 => mã hợp đồng đã tồn tại
                        if (resultOpent != null && (resultOpent.StatusCode == 200 || resultOpent.StatusCode == 3))
                        {
                            if (resultOpent.StatusCode == 200)
                            {
                                loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                                {
                                    LoanBriefId = loanbrief.LoanBriefId,
                                    Note = isChangeImei
                                          ? string.Format("Đơn định vị: Cập nhật imei: {0} => {1}", loanbrief.DeviceId, DeviceId)
                                          : string.Format("Đơn định vị: Tạo hợp đồng thành công với imei: {0}", DeviceId),
                                    FullName = user.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = user.UserId,
                                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                });
                            }
                            loanbrief.DeviceStatus = StatusOfDeviceID.OpenContract.GetHashCode();
                            loanbrief.DeviceId = DeviceId;
                            loanbrief.ContractGinno = "HD-" + loanbrief.LoanBriefId;
                            //cập nhật db
                            var result = loanBriefService.UpdateDevice(access_token, loanbrief);
                            if (result > 0)
                            {
                                if (isChangeImei)
                                    return Json(GetBaseObjectResult(true, "Thay mã thiết bị vào đơn vay thành công", null));
                                else
                                    return Json(GetBaseObjectResult(true, "Gẵn mã thiết bị vào đơn vay thành công", null));
                            }
                            else
                                return Json(GetBaseObjectResult(false, "Xảy ra lỗi. Vui lòng thử lại sau", null));
                        }
                        else
                        {
                            return Json(GetBaseObjectResult(false, string.IsNullOrEmpty(resultOpent.Message)
                                    ? "Xảy ra lỗi khi mở hợp đồng. Vui lòng thử lại trong ít phút."
                                    : resultOpent.Message, null));
                        }
                    }
                    //else if (loanbrief.DeviceStatus == StatusOfDeviceID.OpenContract.GetHashCode())
                    //{

                    //    //gọi api check imei
                    //    if (timeSendActiveDevice == null)
                    //        loanbrief.LastSendRequestActive = DateTime.Now;
                    //    var resultActived = _trackDeviceService.CheckImei(GetToken(), DeviceId, loanbrief.LoanBriefId);
                    //    if (resultActived != null && resultActived.StatusCode == 200 && resultActived.Data != null &&
                    //        resultActived.Data.device_status == EnumDeviceStatusGinno.Actived.GetHashCode())
                    //    {
                    //        if (string.IsNullOrEmpty(resultActived.Data.contractid))
                    //            return Json(GetBaseObjectResult(false, "API kích hoạt không trả ra mã hợp đồng", null));
                    //        if (resultActived.Data.contractid.Trim() == loanbrief.ContractGinno)
                    //        {
                    //            Task.Run(() => loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                    //            {
                    //                LoanBriefId = loanbrief.LoanBriefId,
                    //                Note = "Đơn định vị: Kích hoạt thiết bị thành công",
                    //                FullName = user.FullName,
                    //                Status = 1,
                    //                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    //                CreatedTime = DateTime.Now,
                    //                UserId = user.UserId,
                    //                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                    //                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    //            }));
                    //            //ThreadPool.QueueUserWorkItem(item => loanBriefService.AddLoanBriefNote(GetToken(), note));
                    //        }
                    //        else
                    //        {
                    //            var resultUpdate = _trackDeviceService.UpdateContract(GetToken(), DeviceId, "HD-" + loanbrief.LoanBriefId,
                    //                   resultActived.Data.contractid, loanbrief.LoanBriefId);
                    //            if (!resultUpdate)
                    //            {
                    //                return Json(GetBaseObjectResult(false, "Xảy ra lỗi khi cập nhật lại mã HĐ sang bên Gino", null));
                    //            }
                    //            loanbrief.ContractGinno = "HD-" + loanbrief.LoanBriefId;
                    //        }

                    //        loanbrief.DeviceStatus = StatusOfDeviceID.Actived.GetHashCode();
                    //        loanbrief.DeviceId = DeviceId;

                    //        var result = loanBriefService.UpdateDevice(GetToken(), loanbrief);
                    //        if (result > 0)
                    //            return Json(GetBaseObjectResult(true, "Cập nhật thành công", null));
                    //        else
                    //            return Json(GetBaseObjectResult(false, "Xảy ra lỗi. Vui lòng thử lại sau", null));
                    //    }
                    //    else
                    //    {
                    //        if (resultActived != null && resultActived.StatusCode == 200 && resultActived.Data != null &&
                    //            resultActived.Data.device_status > 0)
                    //        {
                    //            return Json(GetBaseObjectResult(false, string.Format("Thiết bị đang ở trạng thái: {0}", ExtensionHelper.GetDescription((EnumDeviceStatusGinno)resultActived.Data.device_status)), null));
                    //        }
                    //        return Json(GetBaseObjectResult(false, "Thiết bị chưa được kích hoạt. Vui lòng thử lại trong ít phút.", null));
                    //    }
                    //}
                    //else if (loanbrief.DeviceStatus == StatusOfDeviceID.Actived.GetHashCode())
                    //{
                    //    return Json(GetBaseObjectResult(false, "Thiết bị đang được kích hoạt.", null));
                    //}
                    return Json(GetBaseObjectResult(false, "Không thể mở hợp đồng", null));
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống", null));
                }
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
            }
        }

        public async Task<IActionResult> ReGetDeviceStatus(int loanbriefId)
        {
            if (loanbriefId > 0)
            {
                var loanbrief = await loanBriefService.GetLoanBriefById(GetToken(), loanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (Enum.IsDefined(typeof(StatusOfDeviceID), loanbrief.DeviceStatus ?? 0))
                    {
                        return Json(GetBaseObjectResult(true, ExtensionHelper.GetDescription((StatusOfDeviceID)(loanbrief.DeviceStatus ?? 0)), loanbrief.DeviceStatus));
                    }
                    return Json(GetBaseObjectResult(false, "Không tồn tại trạng thái kích hoạt", null));
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống", null));
                }
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
            }
        }

        public async Task<IActionResult> ReActiveDevice(int loanbriefId)
        {
            if (loanbriefId > 0)
            {
                var access_token = GetToken();
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, loanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (loanbrief.DeviceStatus == StatusOfDeviceID.Fail.GetHashCode())
                    {
                        if (_trackDeviceService.ReActiveDevice(GetToken(), "HD-" + loanbrief.LoanBriefId, loanbrief.DeviceId, loanbrief.LoanBriefId))
                        {

                            if (loanBriefService.ReActiveDevice(access_token, loanbriefId))
                            {
                                return Json(GetBaseObjectResult(true, "Gắn thiết bị", StatusOfDeviceID.OpenContract.GetHashCode().ToString()));
                            }
                            else
                            {
                                var message = ExtensionHelper.GetDescription((StatusOfDeviceID)(loanbrief.DeviceStatus ?? 0));
                                return Json(GetBaseObjectResult(true, message, loanbrief.DeviceStatus.ToString()));
                            }
                        }
                        else
                        {
                            return Json(GetBaseObjectResult(false, "Xảy ra lỗi khi kích hoạt lại thiết bị", null));
                        }
                    }
                    else
                    {
                        return Json(GetBaseObjectResult(false, string.Format("Thiết bị đang ở trạng thái: {0}. Không thể kích hoạt lại.", ExtensionHelper.GetDescription((StatusOfDeviceID)(loanbrief.DeviceStatus ?? 0)))));
                    }
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống", null));
                }
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
            }
        }
        #endregion

        #region private func
        private decimal GetMaxPriceProduct(int ProductId, int TypeOfOwnerShip, int productCredit, int LoanBriefId, ref long priceAG, ref decimal priceAi)
        {
            var maxPriceProduct = 0L;
            long originalCarPrice = 0;
            var access_token = GetToken();
            // lấy giá bên AI
            decimal priceCarAI = priceAi = _productService.GetPriceAI(access_token, ProductId, LoanBriefId);
            if (priceCarAI > 0)
            {
                originalCarPrice = (long)priceCarAI;
            }
            else
            {
                //var productPriceCurrent = _productService.GetCurrentPrice(access_token, ProductId);
                //if (productPriceCurrent != null && productPriceCurrent.PriceCurrent > 0)
                //{
                //    originalCarPrice = productPriceCurrent.PriceCurrent ?? 0l;
                //    priceAG = productPriceCurrent.PriceCurrent.Value;
                //}
            }
            maxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPrice(productCredit, originalCarPrice, TypeOfOwnerShip);

            return maxPriceProduct;
        }

        private string RenderViewAsString(object model, string viewName = null)
        {
            viewName = viewName ?? ControllerContext.ActionDescriptor.ActionName;
            ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                IView view = viewEngine.FindView(ControllerContext, viewName, true).View;
                ViewContext viewContext = new ViewContext(ControllerContext, view, ViewData, TempData, sw, new HtmlHelperOptions());
                view.RenderAsync(viewContext).Wait();
                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion

        #region Detail loan
        [PermissionFilter]
        public async Task<IActionResult> Detail(int id = 0)
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            var access_token = user.Token;
            var urlEmbedMapTracking = _baseConfig["AppSettings:UrlEmbedMapTracking"];
            var urlEmbedMapProductAppraiser = _baseConfig["AppSettings:UrlEmbedMapProductAppraiser"];
            var loanBrief = new LoanBriefForDetail();
            if (id > 0)
            {
                loanBrief = await loanBriefService.GetLoanBriefForDetail(access_token, id);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                loanBrief.TypeServiceCall = user.TypeCallService;
                loanBrief.ListRelativeFamilys = CacheData.ListRelativeFamilys;
                loanBrief.ListBrandProduct = CacheData.ListBrandProduct;
                loanBrief.ListLoanProduct = CacheData.ListLoanProduct;
                loanBrief.ListPlatformType = CacheData.ListPlatformType;
                loanBrief.LoanbriefLender = _dictionaryService.GetLoanBriefLender(access_token, id);

                //lấy ra JobDesctionName
                if (loanBrief.LoanBriefJob != null && loanBrief.LoanBriefJob.JobDescriptionId.HasValue)
                {
                    var jobDescription = _dictionaryService.GetJobById(GetToken(), loanBrief.LoanBriefJob.JobDescriptionId.Value);
                    if (jobDescription != null && jobDescription.JobId > 0)
                        loanBrief.JobDesctionName = jobDescription.Name;
                }

                //Nếu là thẩm định hoặc hub mới call
                if (user.GroupId == (int)EnumGroupUser.ApproveLeader || user.GroupId == (int)EnumGroupUser.ApproveEmp || user.GroupId == (int)EnumGroupUser.ManagerHub || user.GroupId == (int)EnumGroupUser.StaffHub)
                {
                    var checkLoanInfo = _dictionaryService.GetCheckImageByLoanBriefId(access_token, id);
                    if (checkLoanInfo != null && checkLoanInfo.Count > 0)
                    {
                        checkLoanInfo = checkLoanInfo.Where(x => x.TypeCheck == (int)EnumCheckLoanInformationType.CheckInfo).ToList();
                        loanBrief.CheckLoanInformation = checkLoanInfo;
                    }
                }
                ViewBag.urlEmbedMapTracking = urlEmbedMapTracking + loanBrief.LoanBriefId;
                ViewBag.urlEmbedMapProductAppraiser = urlEmbedMapProductAppraiser + loanBrief.LoanBriefId;
                ViewBag.GroupID = user.GroupId;
                ViewBag.Ekyc = null;
                ViewBag.EkycNetwork = null;
                ViewBag.EkycMotobike = null;

                if (user.GroupId == (int)EnumGroupUser.StaffHub || user.GroupId == (int)EnumGroupUser.ManagerHub
                    || user.GroupId == (int)EnumGroupUser.ApproveEmp || user.GroupId == (int)EnumGroupUser.ApproveLeader
                    || user.GroupId == (int)EnumGroupUser.Admin || user.UserId == (int)EnumUser.tvv_test)
                {

                    List<int> lstServiceTypeAI = new List<int>()
                            {
                                (int)ServiceTypeAI.FraudDetection,
                                (int)ServiceTypeAI.Ekyc,
                                (int)ServiceTypeAI.EkycGetInfoNetwork,
                                (int)ServiceTypeAI.EkycMotorbikeRegistrationCertificate
                            };

                    var lstLogInfoAI = _logLoanInfoAi.GetByListServiceType(GetToken(), loanBrief.LoanBriefId, lstServiceTypeAI);

                    if (user.GroupId == (int)EnumGroupUser.ApproveEmp || user.GroupId == (int)EnumGroupUser.ApproveLeader
                        || user.GroupId == (int)EnumGroupUser.Admin || user.UserId == (int)EnumUser.tvv_test)
                    {
                        //lấy thông tin fraud check
                        var lstLogFraudCheck = lstLogInfoAI.Where(x => x.ServiceType == (int)ServiceTypeAI.FraudDetection).OrderByDescending(x => x.Id).FirstOrDefault();
                        if (lstLogFraudCheck != null)
                        {
                            if (!string.IsNullOrEmpty(lstLogFraudCheck.Response))
                            {
                                try
                                {
                                    var dataFraudCheck = JsonConvert.DeserializeObject<DAL.Object.ResultFraudCheckModel>(lstLogFraudCheck.Response);
                                    if (dataFraudCheck != null)
                                    {
                                        var fraudCheckModel = new FraudCheckViewModel();
                                        if (dataFraudCheck.tima_result != null && dataFraudCheck.tima_result.unified_result != null)
                                        {
                                            var dataTima = dataFraudCheck.tima_result.unified_result;
                                            if (!string.IsNullOrEmpty(dataTima.channel))
                                                fraudCheckModel.Channel = dataTima.channel;
                                            if (dataTima.details != null && dataTima.details.Count > 0)
                                            {
                                                foreach (var item in dataTima.details)
                                                    if (!string.IsNullOrEmpty(item.message))
                                                        fraudCheckModel.Message.Add(item.message);
                                            }
                                        }

                                        if (dataFraudCheck.hyperverge_result != null && dataFraudCheck.hyperverge_result.unifiedResult != null)
                                        {
                                            var dataHyperveger = dataFraudCheck.hyperverge_result.unifiedResult;
                                            if (!string.IsNullOrEmpty(dataHyperveger.channel))
                                                fraudCheckModel.HvChannel = dataHyperveger.channel;
                                            if (dataHyperveger.details != null && !string.IsNullOrEmpty(dataHyperveger.details.message))
                                            {
                                                fraudCheckModel.HvMessage.Add(dataHyperveger.details.message);
                                            }
                                        }
                                        if (dataFraudCheck.tima_result != null && !string.IsNullOrEmpty(dataFraudCheck.tima_result.report_url))
                                            fraudCheckModel.DetailUrl = dataFraudCheck.tima_result.report_url;
                                        ViewBag.FraudCheck = fraudCheckModel;
                                    }

                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }
                    }
                    //lấy thông tin để check ekyc
                    var lstCheckInfomation = lstLogInfoAI.Where(x => x.ServiceType != (int)ServiceTypeAI.FraudDetection).OrderByDescending(x => x.Id).ToList();
                    if (lstCheckInfomation != null && lstCheckInfomation.Count > 0)
                    {
                        int dem = 0;
                        foreach (var item in lstCheckInfomation)
                        {
                            try
                            {
                                if (!string.IsNullOrEmpty(item.ResponseRequest))
                                {
                                    if (item.ServiceType == (int)ServiceTypeAI.Ekyc && ViewBag.Ekyc == null)
                                    {
                                        dem++;
                                        ViewBag.Ekyc = JsonConvert.DeserializeObject<EkycOutput>(item.ResponseRequest);
                                    }
                                    else if (item.ServiceType == (int)ServiceTypeAI.EkycGetInfoNetwork && item.ResultFinal == "SUCCESS" && ViewBag.EkycNetwork == null)
                                    {
                                        dem++;
                                        ViewBag.EkycNetwork = JsonConvert.DeserializeObject<EkycNetwork>(item.ResponseRequest);
                                    }
                                    else if (item.ServiceType == (int)ServiceTypeAI.EkycMotorbikeRegistrationCertificate && item.ResultFinal == "SUCCESS" && ViewBag.EkycMotobike == null)
                                    {
                                        dem++;
                                        ViewBag.EkycMotobike = JsonConvert.DeserializeObject<EkycMotobike>(!string.IsNullOrEmpty(item.Response) ? item.Response : item.ResponseRequest);
                                    }
                                    if (dem == 3) break;

                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(loanBrief, "Detail")));
        }
        public async Task<IActionResult> MotoPrice(int id = 0)
        {
            var model = new MotoPriceModel();
            var user = GetUserGlobal();
            var access_token = user.Token;
            try
            {
                if (id > 0)
                {
                    #region DuyNV _ thẩm định giá xe clone từ /pricingtool/index.html 
                    var loanbrief = await loanBriefService.GetLoanBriefById(access_token, id);
                    if (loanbrief != null && loanbrief.LoanBriefProperty != null && loanbrief.LoanBriefProperty.ProductId > 0)
                    {
                        model.ToolCalculatorMoney = new ToolCalculatorMoney();
                        model.ToolCalculatorMoney.ListLoanProduct = CacheData.ListLoanProduct;
                        model.ToolCalculatorMoney.ListBrandProduct = CacheData.ListBrandProduct;
                        model.ToolCalculatorMoney.ListOwnerShip = CacheData.ListOwnerShip ?? new List<TypeOwnerShip>();
                        //model.LoanBriefDetailNew = loanBrief;
                        model.LoanBriefProductId = loanbrief.ProductId ?? 0;
                        model.UserDetail = user;
                        model.LoanbriefProperty = loanbrief.LoanBriefProperty;
                        model.LoanBriefResident = loanbrief.LoanBriefResident;
                        var product = _productService.GetProduct(user.Token, loanbrief.LoanBriefProperty.ProductId.Value);
                        ViewBag.Product = product;
                        ViewBag.LoanbriefId = loanbrief.LoanBriefId;

                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {

            }
            return View(model);
        }
        public IActionResult MotoPriceCheckList(int id = 0, int price = 0)
        {
            var model = new MotoPriceModel();
            try
            {
                if (id > 0)
                {
                    #region DuyNV _ thẩm định giá xe clone từ /pricingtool/index.html 
                    var loanBrief = loanBriefService.GetLoanBriefByIdNew(GetToken(), id);
                    if (loanBrief.LoanBriefProperty != null && loanBrief.LoanBriefProperty.Product != null && loanBrief.LoanBriefProperty.Product.Id > 0)
                    {
                        model.ProductReview = _productService.GetProductReview(GetToken(), loanBrief.LoanBriefId, loanBrief.LoanBriefProperty.Product.Id, loanBrief.LoanBriefProperty.Product.ProductTypeId, price) ?? new List<Models.ProductReview>();
                        model.UserDetail = GetUserGlobal();
                        model.LoanBriefId = loanBrief.LoanBriefId;
                        model.Status = loanBrief.Status ?? 0;
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {

            }
            return View(model);
        }
        public async Task<IActionResult> DetailListFile(int id = 0)
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var ServiceURLLocal = _baseConfig["AppSettings:ServiceURLLocal"];
            var model = new ListFileDetail();

            if (id > 0)
            {
                var access_token = GetToken();
                var loanBrief = await loanBriefService.GetLoanBriefById(access_token, id);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                model.LoanBriefId = id;
                model.ScanContractStatus = loanBrief.ScanContractStatus;
                var typeOwnerShip = Common.Utils.ProductPriceUtils.ConvertEnumTypeOfOwnerShipDocument(loanBrief.LoanBriefResident.ResidentType.Value);
                var lstDocument = _documentService.GetDocument(user.Token, loanBrief.ProductId.Value, typeOwnerShip);
                if (lstDocument != null && lstDocument.Count > 0)
                {
                    if (!loanBrief.LoanBriefJob.JobId.HasValue)
                        return Json(GetBaseObjectResult(false, "Chưa có thông tin công việc của KH"));
                    var workRuleType = 0;
                    if (loanBrief.LoanBriefJob != null && loanBrief.LoanBriefJob.JobId.GetValueOrDefault(0) > 0)
                    {
                        workRuleType = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(loanBrief.LoanBriefJob.JobId.Value, loanBrief.LoanBriefJob != null && loanBrief.LoanBriefJob.ImcomeType != null ? loanBrief.LoanBriefJob.ImcomeType.Value : 0
                                        , loanBrief.LoanBriefJob.CompanyInsurance, loanBrief.LoanBriefJob.BusinessPapers);
                    }


                    model.ListLoanBriefFile = loanBriefService.GetLoanBriefFileByLoanId(access_token, id).Where(x => x.TypeId.GetValueOrDefault(0) > 0).ToList();
                    model.ListCheckImage = _dictionaryService.GetCheckImageByLoanBriefId(access_token, id);
                    if (model.ListLoanBriefFile != null && model.ListLoanBriefFile.Count > 0)
                    {
                        await Task.Run(() =>
                        Parallel.ForEach(model.ListLoanBriefFile, item =>
                        {
                            if (!string.IsNullOrEmpty(item.FileThumb))
                            {
                                if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                    item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                                else
                                    item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                            }
                            if (!string.IsNullOrEmpty(item.FilePath))
                            {
                                if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                                    item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                                if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                                    item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                                if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                                {
                                    if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                        item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                                    else
                                        item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                                }
                            }
                        }));

                    }
                    var lstDocumentNew = Ultility.ConvertDocumentTreeDetail(lstDocument, model.ListLoanBriefFile, workRuleType);
                    if (lstDocumentNew != null && lstDocumentNew.Count > 0)
                    {
                        foreach (var item in lstDocumentNew)
                        {
                            if (item.ParentId == (int)EnumDocumentType.AnhNha_TimaCare) item.Name = "(" + ExtentionHelper.GetDescription(EnumDocumentType.AnhNha_TimaCare) + ") " + item.Name;
                            else if (item.ParentId == (int)EnumDocumentType.AnhNoiLamViec_TimaCare) item.Name = "(" + ExtentionHelper.GetDescription(EnumDocumentType.AnhNoiLamViec_TimaCare) + ") " + item.Name;
                            else if (item.ParentId == (int)EnumDocumentType.CVKD_NoiO) item.Name = "(" + ExtentionHelper.GetDescription(EnumDocumentType.CVKD_NoiO) + ") " + item.Name;
                            else if (item.ParentId == (int)EnumDocumentType.CVKD_NoiLamViec) item.Name = "(" + ExtentionHelper.GetDescription(EnumDocumentType.CVKD_NoiLamViec) + ") " + item.Name;
                        }
                    }
                    if (user.GroupId == (int)EnumGroupUser.ApproveEmp || user.GroupId == (int)EnumGroupUser.ApproveLeader)
                        model.ListDocumentNewV2 = lstDocumentNew.OrderByDescending(x => x.ListFiles.Count > 0 ? 1 : 0).ThenBy(x => x.Sort.GetValueOrDefault(100)).ToList();
                    else
                        model.ListDocumentNewV2 = lstDocumentNew.OrderByDescending(x => x.ListFiles.Count > 0 ? 1 : 0).ToList();

                }
            }
            return PartialView("DetailListFileV2", model);

        }

        public async Task<IActionResult> DetailListFileAll(int id = 0)
        {
            var user = GetUserGlobal();
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var ServiceURLLocal = _baseConfig["AppSettings:ServiceURLLocal"];
            var model = new ListFileDetail();
            if (id > 0)
            {
                var access_token = user.Token;
                var loanBrief = await loanBriefService.GetBasicInfo(access_token, id);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                //lấy ra danh sách file 
                model.ListLoanBriefFile = loanBriefService.GetLoanBriefFileByLoanId(access_token, id).OrderBy(x => x.TypeId).ThenBy(n => n.Id).ToList();

                model.ScanContractStatus = loanBrief.ScanContractStatus;

                if (model.ListLoanBriefFile != null && model.ListLoanBriefFile.Count > 0)
                {
                    await Task.Run(() => Parallel.ForEach(model.ListLoanBriefFile, item =>
                     {
                         if (!string.IsNullOrEmpty(item.FileThumb))
                         {
                             if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                 item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                             else
                                 item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                         }
                         if (!string.IsNullOrEmpty(item.FilePath))
                         {
                             if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                                 item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                             if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                                 item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                             if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                             {
                                 if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                     item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                                 else
                                     item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                             }
                         }
                         else if (!string.IsNullOrEmpty(item.LinkImgOfPartner))
                         {
                             item.FilePath = item.DomainOfPartner + item.LinkImgOfPartner;
                         }
                     }));
                }
            }
            return View(model);

        }

        private List<int> IsValidDocument(List<LoanBriefFileDetail> listFiles, string nameDocument, int distanceMax, int timeMax, bool isCheckStep, bool isAllInLocation, ref string message)
        {
            var user = GetUserGlobal();
            var listImageWarning = new List<int>();
            //kiểm tra ảnh chứng từ của KH
            if (listFiles != null && listFiles.Count > 1)
            {
                var resultCheck = Ultility.ValidLoanbriefFiles(listFiles, distanceMax, timeMax, isCheckStep);
                if (resultCheck != null && (resultCheck.InValidStep || resultCheck.totalTime > 0 || resultCheck.distance > 0 || (resultCheck.ListLatLong != null && resultCheck.ListLatLong.Count == 1)))
                {
                    //if (user.GroupId == (int)EnumGroupUser.ManagerHub || user.GroupId == (int)EnumGroupUser.StaffHub)
                    //{
                    //    if (resultCheck.ListLatLong != null && resultCheck.ListLatLong.Count == 1 && !isAllInLocation)
                    //    {
                    //        message = string.Empty;
                    //        return listImageWarning;
                    //    }
                    //    message = $"Bộ chứng từ {nameDocument} không hợp lệ";
                    //    if (resultCheck.fromDocumentId > 0)
                    //        listImageWarning.Add(resultCheck.fromDocumentId);
                    //    if (resultCheck.toDocumentId > 0)
                    //        listImageWarning.Add(resultCheck.toDocumentId);

                    //}
                    //else
                    //{
                    if (resultCheck.InValidStep)
                    {
                        message = $"Bộ chứng từ {nameDocument} không hợp lệ: Thứ tự chụp ảnh của bộ hồ sơ không hợp lệ";
                    }
                    else if (resultCheck.totalTime > 0)
                    {
                        message = $"Bộ chứng từ {nameDocument} không hợp lệ: Thời gian chụp ảnh: {(int)resultCheck.totalTime} phút(quá {timeMax} phút)";
                    }
                    else if (resultCheck.distance > 0)
                    {
                        message = $"Bộ chứng từ {nameDocument} không hợp lệ: Khoảng cách ảnh: {resultCheck.distance}m(quá {distanceMax}m)";
                    }
                    else if (isAllInLocation && resultCheck.ListLatLong != null && resultCheck.ListLatLong.Count == 1)
                    {
                        var latlng = resultCheck.ListLatLong.First();
                        message = $"Bộ chứng từ {nameDocument} không hợp lệ. Tất cả các chứng từ cùng 1 tọa độ: {latlng}";
                    }
                    if (resultCheck.fromDocumentId > 0)
                        listImageWarning.Add(resultCheck.fromDocumentId);
                    if (resultCheck.toDocumentId > 0)
                        listImageWarning.Add(resultCheck.toDocumentId);
                    //}
                }
            }
            if (string.IsNullOrEmpty(message))
                listImageWarning = new List<int>();
            return listImageWarning;
        }

        private List<int> IsValidDocument(LoanBriefFileDetail fromFile, List<LoanBriefFileDetail> listFiles, string nameDocument, int distanceMax, ref string message)
        {
            var user = GetUserGlobal();
            var listImageWarning = new List<int>();
            //kiểm tra ảnh chứng từ của KH
            if (listFiles != null && listFiles.Count > 1)
            {
                var resultCheck = Ultility.ValidLoanbriefFiles(fromFile, listFiles, distanceMax);
                if (resultCheck != null && resultCheck.distance > 0)
                {
                    //if (user.GroupId == (int)EnumGroupUser.ManagerHub || user.GroupId == (int)EnumGroupUser.StaffHub)
                    //{
                    //    message = $"Bộ chứng từ {nameDocument} không hợp lệ";
                    //    if (resultCheck.fromDocumentId > 0)
                    //        listImageWarning.Add(resultCheck.fromDocumentId);
                    //    if (resultCheck.toDocumentId > 0)
                    //        listImageWarning.Add(resultCheck.toDocumentId);
                    //}
                    //else
                    //{
                    message = $"Bộ chứng từ {nameDocument} không hợp lệ: Khoảng cách ảnh: {resultCheck.distance}m(quá {distanceMax}m)";
                    if (resultCheck.fromDocumentId > 0)
                        listImageWarning.Add(resultCheck.fromDocumentId);
                    if (resultCheck.toDocumentId > 0)
                        listImageWarning.Add(resultCheck.toDocumentId);
                    //}
                }
            }
            return listImageWarning;
        }

        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> DetailLoanBrief(LoanBriefInit model)
        {
            try
            {
                var user = GetUserGlobal();
                model.IsHeadOffice = true;
                var access_token = user.Token;
                if (!model.Customer.Phone.ValidPhone())
                    return Json(new { status = 0, message = "Vui lòng nhập đúng địng dạng số điện thoại" });

                if (string.IsNullOrEmpty(model.Customer.FullName))
                    return Json(new { status = 0, message = "Vui lòng nhập họ tên khách hàng" });
                //xử lý ngày sinh
                if (!string.IsNullOrEmpty(model.sBirthDay))
                    model.Customer.Dob = DateTimeUtility.ConvertStringToDate(model.sBirthDay, "dd/MM/yyyy");
                //Nếu gói vay doanh nghiệp
                if (model.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    if (!string.IsNullOrEmpty(model.sBusinessCertificationDate))
                        model.LoanBriefCompany.BusinessCertificationDate = DateTimeUtility.ConvertStringToDate(model.sBusinessCertificationDate, "dd/MM/yyyy");
                    if (!string.IsNullOrEmpty(model.sBirthdayShareholder))
                        model.LoanBriefCompany.BirthdayShareholder = DateTimeUtility.ConvertStringToDate(model.sBirthdayShareholder, "dd/MM/yyyy");
                    if (!string.IsNullOrEmpty(model.sCardNumberShareholderDate))
                        model.LoanBriefCompany.CardNumberShareholderDate = DateTimeUtility.ConvertStringToDate(model.sCardNumberShareholderDate, "dd/MM/yyyy");
                }
                else
                {
                    model.LoanBriefCompany = null;
                }
                model.LoanBriefRelationship = model.LoanBriefRelationship.Where(x => x.RelationshipType > 0 && !string.IsNullOrEmpty(x.FullName) && !string.IsNullOrEmpty(x.Phone)).ToList();
                if (model.LoanBriefRelationship != null && model.LoanBriefRelationship.Count > 0 && model.LoanBriefId > 0)
                    model.LoanBriefRelationship.ToList().ForEach(x => x.LoanBriefId = model.LoanBriefId);
                //else if (user.GroupId == EnumGroupUser.Hub.GetHashCode())

                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                if (model.LoanTime > 0)
                    model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);

                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);
                model.IsHeadOffice = loanbrief.IsHeadOffice;
                var resultUpdate = loanBriefService.UpdateLoanBrief(access_token, model, model.LoanBriefId);
                if (resultUpdate > 0)
                {
                    //Thêm note
                    if (!string.IsNullOrEmpty(model.LoanBriefComment))
                    {
                        loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                        {
                            LoanBriefId = resultUpdate,
                            Note = model.LoanBriefComment,
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        //ThreadPool.QueueUserWorkItem(item => loanBriefService.AddLoanBriefNote(GetToken(), note));
                    }
                    return Json(new { status = 1, message = string.Format("Lưu thông tin thành công đơn vay HĐ-{0}", resultUpdate) });
                }
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });

            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        public IActionResult DeferredPayment(string CustomerName, string NumberCard, int LoanBriefId, string NationCardPlace)
        {
            var data = new DeferredPayment.Output();
            if (!String.IsNullOrEmpty(CustomerName) && !String.IsNullOrEmpty(NumberCard))
            {
                var nationalCard = NumberCard;
                if (!string.IsNullOrEmpty(NationCardPlace))
                    nationalCard = $"{NumberCard},{NationCardPlace}";
                if (!CustomerName.IsNormalized())
                    CustomerName = CustomerName.Normalize();//xử lý đưa về bộ unicode utf8
                var param = new DeferredPayment.Input
                {
                    CustomerName = CustomerName,
                    NumberCard = nationalCard
                };
                //param.CustomerName = "tima giải ngân Hoai";
                //param.NumberCard = "091565695";
                data = _lmsService.CheckCustomerDeferredPayment(GetToken(), LoanBriefId, param);
            }

            //if (data.Result == 1 && data.Data != null)
            //    return Json(new { status = 1, LstLoan = data.Data.LstLoanCustomer });
            //return Json(new { status = 0 });
            return View(data);

        }
        #endregion

        #region
        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> CallCareSoft(int LoanBriefId, int RelationshipId = 0, int TypePhone = 0)
        {
            var link = "";
            var phone = "";
            var user = GetUserGlobal();
            try
            {
                if (LoanBriefId != 0)
                {
                    var access_token = user.Token;
                    var loanInfo = await loanBriefService.GetBasicInfo(access_token, LoanBriefId);
                    if (loanInfo != null)
                    {

                        if (user.TypeCallService == EnumTypeCallService.Caresoft.GetHashCode())
                        {
                            if (RelationshipId > 0)
                            {
                                //Get Info Relationship
                                var RelationshipInfo = _dictionaryService.GetRelationshipInfo(access_token, LoanBriefId, RelationshipId);
                                if (RelationshipInfo != null)
                                {
                                    link = _careSoftService.GenarateLinkClick2Call(user.Ipphone.ToString(), RelationshipInfo.Phone);
                                    phone = RelationshipInfo.Phone;
                                }
                                else
                                {
                                    link = _careSoftService.GenarateLinkClick2Call(user.Ipphone.ToString(), loanInfo.Phone);
                                    phone = loanInfo.Phone;
                                }
                            }
                            else if (TypePhone == (int)LOS.Common.Extensions.TypePhone.PhoneOther)
                            {
                                link = _careSoftService.GenarateLinkClick2Call(user.Ipphone.ToString(), loanInfo.PhoneOther);
                                phone = loanInfo.PhoneOther;
                            }
                            else
                            {
                                link = _careSoftService.GenarateLinkClick2Call(user.Ipphone.ToString(), loanInfo.Phone);
                                phone = loanInfo.Phone;
                            }
                        }

                        var taskRun = new List<Task>();

                        var timeNow = DateTime.Now;

                        var modelLogCall = new DAL.Object.LoanBriefFeedback();
                        modelLogCall.LoanBriefId = loanInfo.LoanBriefId;
                        modelLogCall.CountCall = (loanInfo.CountCall ?? 0) + 1;
                        if (user.GroupId == (int)EnumGroupUser.StaffHub || user.GroupId == (int)EnumGroupUser.ManagerHub)
                        {
                            if (user.GroupId == (int)EnumGroupUser.StaffHub && !loanInfo.FirstTimeStaffHubFeedback.HasValue)
                                modelLogCall.FirstTimeStaffHubFeedback = timeNow;
                            //Thời gian hub call cho KH lần đầu
                            if (!loanInfo.HubEmployeeCallFirst.HasValue)
                                modelLogCall.HubEmployeeCallFirst = timeNow;
                        }

                        if (user.GroupId == (int)EnumGroupUser.Telesale && !loanInfo.FirstProcessingTime.HasValue)
                            modelLogCall.FirstProcessingTime = timeNow;

                        var taskLog = Task.Run(() => _dictionaryService.AddLogClickToCall(access_token, new LogClickToCall
                        {
                            LoanbriefId = loanInfo.LoanBriefId,
                            UserId = user.UserId,
                            PhoneOfCustomer = phone,
                            CreatedAt = timeNow,
                            TypeCallService = user.TypeCallService
                        }));
                        taskRun.Add(taskLog);
                        //Update thời gian phản hồi
                        var taskLogCall = Task.Run(() => loanBriefService.UpdateFeedback(access_token, modelLogCall));
                        taskRun.Add(taskLogCall);

                        //Log Action
                        var taskLogAction = Task.Run(() => _logActionService.AddLogAction(access_token, new LogLoanAction
                        {
                            LoanbriefId = loanInfo.LoanBriefId,
                            ActionId = (int)EnumLogLoanAction.Call,
                            TypeAction = (int)EnumTypeAction.Manual,
                            LoanStatus = loanInfo.Status,
                            TlsLoanStatusDetail = loanInfo.DetailStatusTelesales,
                            HubLoanStatusDetail = loanInfo.LoanStatusChild,
                            TelesaleId = loanInfo.BoundTelesaleId,
                            HubId = loanInfo.HubId,
                            HubEmployeeId = loanInfo.HubEmployeeId,
                            CoordinatorUserId = loanInfo.CoordinatorUserId,
                            UserActionId = user.UserId,
                            GroupUserActionId = user.GroupId,
                            CreatedAt = DateTime.Now,
                            NewValues = $"{user.TypeCallService}"
                        })); ;
                        taskRun.Add(taskLogAction);
                        //End
                        if (taskRun != null && taskRun.Count > 0)
                            Task.WaitAll(taskRun.ToArray());


                    }
                    return Json(new { status = 1, url = link });
                }
                else
                {
                    return Json(new { status = 0, message = "Không có thông tin đơn vay" });
                }


            }
            catch (Exception)
            {
                return Json(new { status = 0 });
            }

        }
        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> CallCareSoftByPhone(string phone)
        {
            var link = "";
            var user = GetUserGlobal();
            try
            {
                var access_token = user.Token;
                link = _careSoftService.GenarateLinkClick2Call(user.Ipphone.ToString(), phone);
                return Json(new { status = 1, url = link });
            }
            catch (Exception)
            {
                return Json(new { status = 0 });
            }

        }
        #endregion

        #region Màn hình tìm kiếm đơn trên hệ thống
        [Route("/search/index.html")]
        [PermissionFilter]
        public IActionResult IndexSearch()
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            return View();
        }
        [PermissionFilter]
        public IActionResult SearchLoanAll()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new LoanBriefSearchDatatable(HttpContext.Request.Form);
                if (modal.loanBriefId != null)
                    modal.loanBriefId = modal.loanBriefId.Trim();
                if (modal.search != null)
                    modal.search = modal.search.Trim();
                int recordsTotal = 0;
                var data = new List<LoanBriefSearchDetail>();
                if (!string.IsNullOrEmpty(modal.search) || !string.IsNullOrEmpty(modal.nationalCard)
                    || !string.IsNullOrEmpty(modal.loanBriefId) || !string.IsNullOrEmpty(modal.phone))
                {
                    data = loanBriefService.SearchLoanAll(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                }
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Lấy lại thông tin location Sim
        [HttpPost]
        [PermissionFilter]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> ReGetLocationSim(int LoanBriefId)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, LoanBriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    var result = loanBriefService.ReGetLocation(access_token, loanbrief);
                    if (result > 0)
                    {
                        //Thêm note
                        loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                        {
                            LoanBriefId = LoanBriefId,
                            Note = string.Format("{0} gửi yêu cầu lấy thông tin location Sim.", user.FullName),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.ReGetLocation.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        return Json(new { status = 1, message = "Lấy lại thông tin location Sim thành công" });
                    }
                    else
                    {
                        return Json(new { status = 0, message = "Xảy ra lỗi khi lấy lại thông tin location Sim" });
                    }
                }
                else
                {
                    return Json(new { status = 0, message = "Không tìm thấy thông tin khoản vay" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region Đơn tái vay

        public IActionResult CheckReLoan(string FullName, string NumberCard)
        {
            try
            {
                var req = new CheckReLoan.Input()
                {
                    CustomerName = FullName,
                    NumberCard = NumberCard
                };
                // Query api   	
                var data = _lmsService.CheckReLoanV3(GetToken(), req);
                //Returning Json Data  
                if (data.Result == 1)
                {
                    //data.Data.IsAccept = 1;
                    if (data.IsAccept == 1)
                    {
                        if (data.LoanBriefID > 0)
                        {
                            return Json(new { Status = 1, LoanBriefId = data.LoanBriefID, Message = "" });
                        }
                        else
                        {
                            return Json(new { Status = 0, Message = "Không lấy được Mã đơn vay bên LOS" });
                        }

                    }
                    else
                    {
                        return Json(new { Status = 0, Message = data.Message });
                    }
                }
                else
                {
                    return Json(new { Status = 0, Message = "Có lỗi xảy ra vui lòng liên hệ kỹ thuật!" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { Status = 0, Message = "Có lỗi xảy ra vui lòng liên hệ kỹ thuật!" });
            }
        }
        public async Task<IActionResult> ReLoanInfo(int LoanBriefId)
        {
            //LoanBriefId = 10033;
            var model = new LoanBriefScriptItem();
            var user = GetUserGlobal();
            ViewBag.User = user;
            var access_token = user.Token;

            if (LoanBriefId > 0)
            {
                var loanBrief = await loanBriefService.GetLoanBriefById(GetToken(), LoanBriefId);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                if (loanBrief.ProductId > 0)
                {
                    //Check nếu là gói tiểu thương hoặc oto thì k cần check KH có đơn xử lý
                    if (loanBrief.ProductId != (int)EnumProductCredit.CamotoCreditType && loanBrief.ProductId != (int)EnumProductCredit.OtoCreditType_CC && loanBrief.ProductId != (int)EnumProductCredit.OtoCreditType_KCC && loanBrief.ProductId != (int)EnumProductCredit.CamCoHangHoa && loanBrief.TypeRemarketing != (int)EnumTypeRemarketing.IsTopUp)
                    {
                        var listLoanBrief = loanBriefService.GetLoanBriefProcessing(access_token, loanBrief.Phone, loanBrief.NationalCard, loanBrief.LoanBriefId);
                        if (listLoanBrief != null && listLoanBrief.LoanBriefId > 0)
                            return Json(GetBaseObjectResult(false, "Khách hàng có đơn vay đang xử lý HĐ-{listLoanBrief.LoanBriefId}"));
                    }
                    //Check xem khách hàng có trong black list không
                    var checkBlackList = _lmsService.CheckBlackList(access_token, loanBrief.FullName, loanBrief.NationalCard, loanBrief.Phone, loanBrief.LoanBriefId);
                    if (checkBlackList != null)
                    {
                        if (checkBlackList.Data == true && checkBlackList.Status == 1)
                            return Json(GetBaseObjectResult(false, "Khách hàng nằm trong BlackList"));
                    }
                    //Check khách hàng có phải nhân viên của Tima hay không
                    var checkEmoloyeeTima = _erpService.CheckEmployeeTima(access_token, loanBrief.NationalCard, loanBrief.Phone, loanBrief.LoanBriefId);
                    if (checkEmoloyeeTima != null)
                    {
                        return Json(GetBaseObjectResult(false, "Khách hàng là nhân viên Tima!"));
                    }
                    var Views = "";
                    if (loanBrief.ProductId == EnumProductCredit.MotorCreditType_CC.GetHashCode()
                        || loanBrief.ProductId == EnumProductCredit.MotorCreditType_KCC.GetHashCode()
                        || loanBrief.ProductId == EnumProductCredit.MotorCreditType_KGT.GetHashCode())
                        Views = "_Motobike";
                    else if (loanBrief.ProductId == EnumProductCredit.SalaryCreditType.GetHashCode())
                        Views = "_Salary";
                    else
                        return Json(GetBaseObjectResult(false, "Tái vay chỉ áp dụng đối với gói Xe máy hoặc lương!"));

                    MapperExtentions.MapToLoanBriefScriptItem(loanBrief, ref model);
                    if (loanBrief.ProductId > 0 && loanBrief.LoanBriefResident != null && loanBrief.LoanBriefResident.ResidentType > 0 && loanBrief.LoanBriefProperty != null && loanBrief.LoanBriefProperty.ProductId > 0)
                    {
                        var priceAG = 0l;
                        decimal priceAI = 0l;
                        model.MaxPriceProduct = GetMaxPriceProduct((int)loanBrief.LoanBriefProperty.ProductId, (int)loanBrief.LoanBriefResident.ResidentType, (int)loanBrief.ProductId, LoanBriefId, ref priceAG, ref priceAI);
                    }
                    model.ListProvince = _dictionaryService.GetProvince(GetToken());
                    model.ListJobs = CacheData.ListJobs;
                    model.ListRelativeFamilys = CacheData.ListRelativeFamilys;
                    model.ListBrandProduct = CacheData.ListBrandProduct;
                    model.ListLoanProduct = CacheData.ListLoanProduct;
                    model.ListPlatformType = CacheData.ListPlatformType;
                    model.ListLoanPurpose = ExtensionHelper.GetEnumToList(typeof(EnumLoadLoanPurpose));
                    return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "ReLoan/" + Views)));
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay chưa có gói sản phẩm. Vui lòng cập nhật đơn vay"));
                }

            }
            else
            {
                return Json(GetBaseObjectResult(false, "Xảy ra lỗi khi lấy thông tin khoản vay. Vui lòng thử lại"));
            }

        }
        [HttpPost]
        [PermissionFilter]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddReLoan(LoanBriefScriptItem model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                if (user.GroupId == (int)EnumGroupUser.Telesale)
                {
                    model.BoundTelesaleId = user.UserId;
                }
                model.IsHeadOffice = true;
                if (user != null)
                    model.CreateBy = user.UserId;
                if (string.IsNullOrEmpty(model.Customer.Phone))
                    return Json(new { status = 0, message = "Vui lòng nhập số điện thoại khách hàng" });
                if (string.IsNullOrEmpty(model.Customer.FullName))
                    return Json(new { status = 0, message = "Vui lòng nhập họ tên khách hàng" });
                //xử lý ngày sinh
                if (!string.IsNullOrEmpty(model.sBirthDay))
                    model.Customer.Dob = DateTimeUtility.ConvertStringToDate(model.sBirthDay, "dd/MM/yyyy");
                model.LoanBriefCompany = null;
                if (model.ListLoanBriefDocument != null && model.ListLoanBriefDocument.Count > 0)
                {
                    model.LoanBriefDocument = model.ListLoanBriefDocument.Select(x => new LoanBriefDocument
                    {
                        DocumentId = x,
                        LoanBriefId = model.LoanBriefId
                    }).ToList();
                }

                //Xử lý trạng thái đơn
                if (model.ActionTelesales == 2)//Đẩy đơn
                {
                    //model.Status = 1;
                    model.TelesalesPushAt = DateTime.Now;
                    model.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                }
                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                if (model.LoanTime > 0)
                    model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);

                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);
                //Xử lý gói sản phẩm
                //kiểm tra nếu là gói xe máy
                if (loanbrief.ProductId == EnumProductCredit.MotorCreditType_CC.GetHashCode()
                    || loanbrief.ProductId == EnumProductCredit.MotorCreditType_KCC.GetHashCode()
                    || loanbrief.ProductId == EnumProductCredit.MotorCreditType_KGT.GetHashCode())
                {
                    //Xe máy chính chủ
                    model.ProductId = model.OwnerProduct == 1 ? EnumProductCredit.MotorCreditType_CC.GetHashCode() : EnumProductCredit.MotorCreditType_KCC.GetHashCode();
                }

                model.LoanBriefRelationship = loanbrief.LoanBriefRelationship;
                if (loanbrief.Customer != null)
                {
                    var oldCustomer = loanbrief.Customer;
                    Ultility.CopyObject(model.Customer, ref oldCustomer, false);
                    model.Customer = oldCustomer;
                }
                if (loanbrief.LoanBriefResident != null)
                {
                    var oldLoanBriefResident = loanbrief.LoanBriefResident;
                    Ultility.CopyObject(model.LoanBriefResident, ref oldLoanBriefResident, false);
                    model.LoanBriefResident = oldLoanBriefResident;
                }

                if (loanbrief.LoanBriefHousehold != null)
                {
                    var oldLoanBriefHousehold = loanbrief.LoanBriefHousehold;
                    Ultility.CopyObject(model.LoanBriefHousehold, ref oldLoanBriefHousehold, false);
                    model.LoanBriefHousehold = oldLoanBriefHousehold;
                }

                if (loanbrief.LoanBriefProperty != null)
                {
                    var oldLoanBriefProperty = loanbrief.LoanBriefProperty;
                    Ultility.CopyObject(model.LoanBriefProperty, ref oldLoanBriefProperty, false);
                    model.LoanBriefProperty = oldLoanBriefProperty;
                }

                if (loanbrief.LoanBriefJob != null)
                {
                    var oldLoanBriefJob = loanbrief.LoanBriefJob;
                    Ultility.CopyObject(model.LoanBriefJob, ref oldLoanBriefJob, false);
                    model.LoanBriefJob = oldLoanBriefJob;
                }
                //kiểm tra nếu là user hub => gán cho hub đó luôn
                if (user.GroupId == EnumGroupUser.ManagerHub.GetHashCode() || user.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                {
                    model.HubId = user.ShopGlobal;
                    model.IsHeadOffice = false;
                }
                var resultAdd = loanBriefService.AddReLoan(access_token, model);
                if (resultAdd > 0)
                {
                    //Thêm note
                    if (!string.IsNullOrEmpty(model.LoanBriefComment))
                    {
                        loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                        {
                            LoanBriefId = resultAdd,
                            Note = model.LoanBriefComment,
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                    }
                    //Thêm note khởi tạo đơn tái vay
                    loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = resultAdd,
                        Note = string.Format("{0}: Khởi tạo đơn tái vay", user.FullName),
                        FullName = user.FullName,
                        UserId = user.UserId,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)

                    });
                    var msg = string.Format("Tạo thành công đơn vay HĐ-{0}", resultAdd);
                    if (model.ActionTelesales == 2)
                        msg = string.Format("Đẩy thành công HĐ-{0}", resultAdd);
                    return Json(new { status = 1, message = msg });
                }
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });

            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        #endregion    }

        #region Hẹn giờ gọi lại
        [HttpPost]
        public IActionResult FormScheduleTime(int LoanBriefId)
        {
            ViewBag.LoanBriefId = LoanBriefId;
            return View();
        }
        [HttpPost]
        public IActionResult AddScheduleTime(ScheduleTimeReq req)
        {
            var user = GetUserGlobal();
            try
            {
                var access_token = user.Token;
                var result = loanBriefService.AddScheduleTimeCall(access_token, req);
                // check if telesale finish
                if (result.meta.errorCode == 200) // Success
                {
                    //Thêm note
                    loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = req.LoanBriefId,
                        Note = string.Format("{0} : {1}", req.Comment, req.DateTimeCall),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.ScheduleTime.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });

                    return Json(new { status = 1, message = "Hẹn gọi lại thành công" });
                }
                else
                {
                    return Json(new { status = 0, message = "Có lỗi xảy ra, vui lòng liên hệ kỹ thuật!" });
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Gửi yêu cầu lấy dữ liệu AI từ kịch bản tele
        //Cập nhât dữ liệu thông tin đơn vay + gửi yêu cầu sang AI
        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> RequestAI(LoanBriefScriptItem model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                if (model.LoanBriefId == 0 && string.IsNullOrEmpty(model.Customer.Phone))
                    return Json(new { status = 0, message = "Vui lòng nhập số điện thoại khách hàng" });
                if (string.IsNullOrEmpty(model.Customer.FullName))
                    return Json(new { status = 0, message = "Vui lòng nhập họ tên khách hàng" });
                //xử lý ngày sinh
                if (!string.IsNullOrEmpty(model.sBirthDay))
                    model.Customer.Dob = DateTimeUtility.ConvertStringToDate(model.sBirthDay, "dd/MM/yyyy");
                model.LoanBriefCompany = null;

                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);
                //Xử lý gói sản phẩm
                //kiểm tra nếu là gói xe máy
                if (loanbrief.ProductId == EnumProductCredit.MotorCreditType_CC.GetHashCode()
                    || loanbrief.ProductId == EnumProductCredit.MotorCreditType_KCC.GetHashCode()
                    || loanbrief.ProductId == EnumProductCredit.MotorCreditType_KGT.GetHashCode())
                {
                    //Xe máy chính chủ
                    model.ProductId = model.OwnerProduct == 1 ? EnumProductCredit.MotorCreditType_CC.GetHashCode() : EnumProductCredit.MotorCreditType_KCC.GetHashCode();
                }
                if (model.LoanBriefRelationship.Count == 0)
                {
                    model.LoanBriefRelationship = loanbrief.LoanBriefRelationship;
                }

                if (loanbrief.Customer != null)
                {
                    var oldCustomer = loanbrief.Customer;
                    Ultility.CopyObject(model.Customer, ref oldCustomer, false);
                    model.Customer = oldCustomer;
                }
                if (loanbrief.LoanBriefResident != null)
                {
                    var oldLoanBriefResident = loanbrief.LoanBriefResident;
                    Ultility.CopyObject(model.LoanBriefResident, ref oldLoanBriefResident, false);
                    model.LoanBriefResident = oldLoanBriefResident;
                }

                if (loanbrief.LoanBriefHousehold != null)
                {
                    var oldLoanBriefHousehold = loanbrief.LoanBriefHousehold;
                    Ultility.CopyObject(model.LoanBriefHousehold, ref oldLoanBriefHousehold, false);
                    model.LoanBriefHousehold = oldLoanBriefHousehold;
                }

                if (loanbrief.LoanBriefProperty != null)
                {
                    var oldLoanBriefProperty = loanbrief.LoanBriefProperty;
                    Ultility.CopyObject(model.LoanBriefProperty, ref oldLoanBriefProperty, false);
                    model.LoanBriefProperty = oldLoanBriefProperty;
                }

                if (loanbrief.LoanBriefJob != null)
                {
                    var oldLoanBriefJob = loanbrief.LoanBriefJob;
                    Ultility.CopyObject(model.LoanBriefJob, ref oldLoanBriefJob, false);
                    model.LoanBriefJob = oldLoanBriefJob;
                }

                var resultUpdate = loanBriefService.UpdateInfoForRequestAI(access_token, model, model.LoanBriefId);
                if (resultUpdate > 0)
                {

                    //Thêm yêu cầu lấy dữ liệu rule check
                    var ObjRequestRuleCheck = new LogLoanInfoAi()
                    {
                        LoanbriefId = loanbrief.LoanBriefId,
                        ServiceType = (int)ServiceTypeAI.RuleCheck,
                        CreatedAt = DateTime.Now,
                        IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest
                    };
                    _logReqestAiService.Create(access_token, ObjRequestRuleCheck);
                    //Thêm yêu cầu lấy dữ liệu location
                    if (_networkService.CheckHomeNetwok(loanbrief.Phone) == (int)HomeNetWorkMobile.Viettel || _networkService.CheckHomeNetwok(loanbrief.Phone) == (int)HomeNetWorkMobile.Mobi)
                    {
                        var ObjRequestLocation = new LogLoanInfoAi()
                        {
                            LoanbriefId = loanbrief.LoanBriefId,
                            ServiceType = (int)ServiceTypeAI.Location,
                            CreatedAt = DateTime.Now,
                            IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest
                        };
                        _logReqestAiService.Create(access_token, ObjRequestLocation);
                    }
                    loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        Note = string.Format("TELESALE {0} gửi yêu cầu lấy dữ liệu AI thành công", user.Username),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                    return Json(new { status = 1, message = "Gửi yêu cầu lấy dữ liệu AI thành công" });
                }
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });

            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        [PermissionFilter]
        public IActionResult GetResultAI(int loanbriefId)
        {
            var loanBrief = loanBriefService.GetLoanBriefById(GetToken(), loanbriefId);
            return PartialView("~/Views/LoanBrief/_ResultAI.cshtml", loanBrief);
        }
        #endregion


        #region Tool tra cứu số tiền vay tối đa 
        [Route("/pricingtool/index.html")]
        [PermissionFilter]
        public IActionResult ToolCalculatormoney()
        {
            var user = GetUserGlobal();
            var access_token = user.Token;
            var model = new ToolCalculatorMoney();

            model.ListLoanProduct = CacheData.ListLoanProduct;
            model.ListBrandProduct = CacheData.ListBrandProduct;
            model.ListOwnerShip = CacheData.ListOwnerShip;
            //danh sách gói sản phẩm chi tiết
            model.ListProductDetail = _dictionaryService.GetInfomationProductDetailByTypeOwnershipAndProduct(access_token, (int)EnumTypeofownership.KT1, (int)EnumProductCredit.MotorCreditType_CC);
            return View(model);
        }


        public IActionResult ToolGetPriceProduct(int productId, int typeOfOwnerShip, int productCredit, int productDetailId, int ltv = 0)
        {
            var maxPriceProduct = 0L;
            decimal priceCarAI = 0;
            decimal priceCarAg = 0;
            var maxPriceLtv = 0L;
            var access_token = GetToken();

            // lấy giá bên AI
            var priceCurrent = 0L;
            priceCarAI = _productService.GetPriceAI(access_token, productId, 0);
            if (priceCarAI > 0)
            {
                priceCurrent = (long)priceCarAI;

            }
            else
            {
                //var productPriceCurrent = _productService.GetCurrentPrice(access_token, ProductId);
                //if (productPriceCurrent != null && productPriceCurrent.PriceCurrent > 0)
                //{
                //    priceCurrent = productPriceCurrent.PriceCurrent.Value;
                //    priceCarAg = productPriceCurrent.PriceCurrent.Value;
                //}
            }

            if (priceCurrent > 0)
            {
                //Lưu log sang AI
                //var productAiLogModel = _productService.PrepareProductAiLogModel(GetToken(), productId);
                //if (productAiLogModel != null)
                //{
                //    productAiLogModel.AI_price = (long)priceCarAI;
                //    productAiLogModel.IT_price = (long)priceCarAg;
                //    productAiLogModel.loan_credit_id = 0;
                //    _priceMotorService.PushLogPriceAIMoto(productAiLogModel, GetToken());
                //}
                maxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPriceV3(productCredit, priceCurrent, typeOfOwnerShip, productDetailId, 0, 0, 0, ltv);
                maxPriceLtv = Common.Utils.ProductPriceUtils.GetMaxPriceLtv(productCredit, priceCurrent, ltv);
                return Json(new { status = 1, CurrentPrice = priceCurrent, MaxPrice = maxPriceProduct, MaxPriceLtv = maxPriceLtv });
            }
            return Json(new { status = 1, CurrentPrice = priceCurrent, MaxPrice = maxPriceProduct, MaxPriceLtv = maxPriceLtv });
        }

        #endregion

        #region Tool Tính lãi phí
        [Route("/interesttool/index.html")]
        [PermissionFilter]
        public IActionResult ToolInterest()
        {
            return View();
        }

        #endregion
        public async Task<IActionResult> LogClickToCall(LogClickToCallItem req)
        {
            var user = GetUserGlobal();
            try
            {
                var access_token = user.Token;
                var timeNow = DateTime.Now;
                var objLog = new LogClickToCall
                {
                    LoanbriefId = req.loanbriefId,
                    UserId = user.UserId,
                    PhoneOfCustomer = req.phone,
                    CreatedAt = timeNow,
                    TypeCallService = user.TypeCallService
                };
                _dictionaryService.AddLogClickToCall(access_token, objLog);
                var loanInfo = await loanBriefService.GetBasicInfo(access_token, req.loanbriefId);
                if (loanInfo != null && loanInfo.LoanBriefId > 0)
                {
                    var dataUpdate = new DAL.Object.LoanBriefFeedback();
                    dataUpdate.LoanBriefId = loanInfo.LoanBriefId;
                    dataUpdate.CountCall = (loanInfo.CountCall ?? 0) + 1;
                    if ((user.GroupId == (int)EnumGroupUser.StaffHub || user.GroupId == (int)EnumGroupUser.ManagerHub)
                        && !loanInfo.HubEmployeeCallFirst.HasValue)
                    {
                        dataUpdate.HubEmployeeCallFirst = timeNow;
                    }
                    else if (user.GroupId == (int)EnumGroupUser.Telesale && !loanInfo.FirstProcessingTime.HasValue)
                    {
                        dataUpdate.FirstProcessingTime = timeNow;
                    }
                    loanBriefService.UpdateFeedback(access_token, dataUpdate);

                    //Log Action
                    await _logActionService.AddLogAction(access_token, new LogLoanAction
                    {
                        LoanbriefId = loanInfo.LoanBriefId,
                        ActionId = (int)EnumLogLoanAction.Call,
                        TypeAction = (int)EnumTypeAction.Manual,
                        LoanStatus = loanInfo.Status,
                        TlsLoanStatusDetail = loanInfo.DetailStatusTelesales,
                        HubLoanStatusDetail = loanInfo.LoanStatusChild,
                        TelesaleId = loanInfo.BoundTelesaleId,
                        HubId = loanInfo.HubId,
                        HubEmployeeId = loanInfo.HubEmployeeId,
                        CoordinatorUserId = loanInfo.CoordinatorUserId,
                        UserActionId = user.UserId,
                        GroupUserActionId = user.GroupId,
                        CreatedAt = DateTime.Now,
                        NewValues = $"{user.TypeCallService}"
                    });
                }

                return Json(new { status = 1, message = "success" });
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "exception" });
            }
        }

        [HttpPost]
        [PermissionFilter]
        public IActionResult RemoveImage(RemoveFileItem model)
        {
            try
            {
                if (model.loanbriefId > 0 && model.fileId > 0)
                {
                    var user = GetUserGlobal();
                    if (user != null)
                    {
                        model.userRemove = user.UserId;
                        if (loanBriefService.RemoveFile(GetToken(), model))
                            return Json(new { status = 1, message = "Xóa file thành công" });
                        else
                            return Json(new { status = 1, message = "Xảy ra lỗi khi xóa file. vui lòng thử lại" });
                    }
                    else
                    {
                        return Json(new { status = 0, message = "Không tìm thấy thông tin user" });
                    }
                }
                else
                {
                    return Json(new { status = 0, message = "Vui lòng truyền đầy đủ thông tin" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        private bool CheckPermissionAction(List<UserActionPermission> ListPermissionAction, int LoanStatus, int Action)
        {
            //Kiêm tra quyền tương tác dữ liệu
            if (ListPermissionAction != null && ListPermissionAction.Any(x => x.LoanStatus == LoanStatus))
            {
                var permission = ListPermissionAction.FirstOrDefault(x => x.LoanStatus == LoanStatus);
                if ((Action & permission.Value) == Action)
                    return true;
            }
            return false;
        }

        //Check Ekyc
        public async Task<IActionResult> CheckEkyc(int LoanBriefId)
        {
            try
            {
                var access_token = GetToken();
                var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
                var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, LoanBriefId);
                var listDocument = _dictionaryService.GetDocumentType(access_token, loanbrief.ProductId.Value);
                var listLoanBriefFile = loanBriefService.GetLoanBriefFileByLoanId(access_token, LoanBriefId);

                if (loanbrief.Dob == null)
                    return Json(new { status = 0, message = "Chưa có thông tin ngày sinh khách hàng!" });

                var lstCMND = listLoanBriefFile.Where(x => x.TypeId == (int)EnumDocumentType.CMND_CCCD).ToList();
                if (lstCMND.Count < 2)
                    return Json(new { status = 0, message = "Ảnh CMT chưa đầy đủ 2 mặt!" });

                foreach (var item in lstCMND)
                {
                    if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                        item.FilePath = ServiceURL + item.FilePath;
                    if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                        item.FilePath = ServiceURLAG + item.FilePath;
                    if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                        item.FilePath = ServiceURLFileTima + item.FilePath;
                }
                //var FrontId = listLoanBriefFile.Where(x => x.TypeId == listDocument.Where(x => x.Name.Contains("CMT mặt trước")).ToList()[0].Id).OrderByDescending(s => s.Id).FirstOrDefault();
                //var BackId = listLoanBriefFile.Where(x => x.TypeId == listDocument.Where(x => x.Name.Contains("CMT mặt sau")).ToList()[0].Id).OrderByDescending(s => s.Id).FirstOrDefault();

                //var Selfie = new LoanBriefFileDetail();
                //try
                //{
                //    Selfie = listLoanBriefFile.Where(x => x.TypeId == listDocument.Where(x => x.Name.Contains("Ảnh Mặt")).ToList()[0].Id).OrderByDescending(s => s.Id).FirstOrDefault();
                //}
                //catch (Exception ex)
                //{
                //    Selfie.FilePath = "";
                //}
                //if (FrontId == null)

                //    return Json(new { status = 0, message = "Đơn vay chưa có ảnh CMT mặt trước!" });
                //if (BackId == null)
                //    return Json(new { status = 0, message = "Đơn vay chưa có ảnh CMT mặt sau!" });
                //if (Selfie == null)
                //    return Json(new { status = 0, message = "Đơn vay chưa có ảnh Selfie!" });
                var obj = new EkycInput
                {
                    FrontId = lstCMND[0].FilePath,
                    BackId = lstCMND[1].FilePath,
                    Selfie = "",
                    Fullname = loanbrief.FullName,
                    Birthday = loanbrief.Dob != null ? loanbrief.Dob.Value.ToString("dd-MM-yyyy") : "",
                    NationalId = loanbrief.NationalCard
                };
                //Check xem Request đã tồn tại trong bảng LogLoanInfoAi hay chưa
                var logAI = _dictionaryService.CheckLogAiByLoanBriefId(access_token, LoanBriefId);
                if (logAI != null)
                {
                    if (String.Compare(logAI.Request.ToString(), JsonConvert.SerializeObject(obj).ToString(), true) == 0)
                    {
                        return Json(new { status = 0, message = "Thông tin trên đã được check Ekyc!" });
                    }
                }
                var result = _ekyc.GetInfoCheckEkyc(access_token, obj, LoanBriefId);
                if (result.StatusCode == 200)
                {
                    return Json(new { status = 1, message = "Check Ekyc thành công!" });
                }
                else
                {
                    return Json(new { status = 0, message = result.Message });
                }

            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi xảy ra, vui lòng thử lại sau!" });
            }
        }
        #region PushLoan and Choose Hub
        [HttpPost]
        public IActionResult PushLoanAndChooseHub(int LoanBriefId, int ProvinceId)
        {
            ViewBag.LoanBriefId = LoanBriefId;
            var lstShops = _dictionaryService.GetShopByCityId(GetToken(), ProvinceId);
            return View(lstShops);
        }
        #endregion
        [HttpPost]
        public IActionResult CheckBank(string BankAccountNumber, string BankCode, int LoanBriefId = 0)
        {

            var result = _checkBankService.CheckBank(GetToken(), LoanBriefId, BankAccountNumber, BankCode);

            if (result.Status == 0)
            {
                return Json(new { Status = 1, accountName = result.Data.AccName });
            }
            else
            {
                return Json(new { Status = 0, message = string.IsNullOrEmpty(result.Messages) ? "Xảy ra lỗi khi kiểm tra tài khoản" : result.Messages });
            }
        }

        [HttpGet]
        public async Task<IActionResult> StatusTelesales(int LoanBriefId, int QuestionQlf)
        {
            var user = GetUserGlobal();
            var access_token = GetToken();
            var lstData = new List<LoanStatusDetail>();
            var loanbrief = await loanBriefService.GetLoanBriefById(access_token, LoanBriefId);
            //Lấy danh sách các Status của Telesales

            //kiểm tra xem đơn có trong giỏ của tls không
            if (user.GroupId == EnumGroupUser.Telesale.GetHashCode() && user.UserId != loanbrief.BoundTelesaleId)
                return Json(GetBaseObjectResult(false, "Đơn không phải của bạn. Bạn không có quyền chuyển trạng thái", null));

            if (loanbrief.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                lstData = _loanStatusDetailService.GetLoanStatusDetail(access_token, (int)EnumLoanStatusDetail.TLS).ToList();
            else
            {
                //đơn của đối tác aff, mmo
                if (!string.IsNullOrEmpty(loanbrief.Tid))
                {
                    if (QuestionQlf == 4 && loanbrief.ProvinceId != null && (loanbrief.ProvinceId == (int)EnumProvince.HaNoi
                        || loanbrief.ProvinceId == (int)EnumProvince.HCM
                        || loanbrief.ProvinceId == (int)EnumProvince.BacNinh
                        || loanbrief.ProvinceId == (int)EnumProvince.VinhPhuc
                        || loanbrief.ProvinceId == (int)EnumProvince.HaiDuong
                        || loanbrief.ProvinceId == (int)EnumProvince.BinhDuong
                        || loanbrief.ProvinceId == (int)EnumProvince.BacGiang))
                        lstData = _loanStatusDetailService.GetLoanStatusDetail(access_token, (int)EnumLoanStatusDetail.TLS).Where(x => x.Id == (int)EnumStatusTelesales.WaitPrepareFile).ToList();
                    else
                        lstData = _loanStatusDetailService.GetLoanStatusDetail(access_token, (int)EnumLoanStatusDetail.TLS).Where(x => x.Id != (int)EnumStatusTelesales.WaitPrepareFile).ToList();
                }
                //đơn inhouse
                else
                {
                    if (QuestionQlf == 4 && loanbrief.ProvinceId != null && (loanbrief.ProvinceId == (int)EnumProvince.HaNoi
                        || loanbrief.ProvinceId == (int)EnumProvince.HCM
                        || loanbrief.ProvinceId == (int)EnumProvince.BacNinh
                        || loanbrief.ProvinceId == (int)EnumProvince.VinhPhuc
                        || loanbrief.ProvinceId == (int)EnumProvince.HaiDuong
                        || loanbrief.ProvinceId == (int)EnumProvince.BinhDuong
                        || loanbrief.ProvinceId == (int)EnumProvince.BacGiang))
                        lstData = _loanStatusDetailService.GetLoanStatusDetail(access_token, (int)EnumLoanStatusDetail.TLS).Where(x => x.Id == (int)EnumStatusTelesales.WaitPrepareFile || x.Id == (int)EnumStatusTelesales.Consider).ToList();
                    else
                        lstData = _loanStatusDetailService.GetLoanStatusDetail(access_token, (int)EnumLoanStatusDetail.TLS).Where(x => x.Id != (int)EnumStatusTelesales.WaitPrepareFile && x.Id != (int)EnumStatusTelesales.Consider).ToList();
                }

            }

            ViewBag.LoanBriefId = LoanBriefId;
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(lstData, "_StatusTelesales")));

        }
        [HttpGet]
        public async Task<IActionResult> DetailStatusTelesales(int parentId)
        {
            var access_token = GetToken();
            //Lấy danh sách chi tiết các Status của Telesales
            var result = _loanStatusDetailService.GetLoanStatusDetailChild(access_token, parentId);
            if (result != null && result.Count > 0)
                return Json(new { status = 1, data = result, message = "Lấy danh sách chi tiết Status của Telesales thành công" });
            else
                return Json(new { status = 0, message = "Lấy danh sách chi tiết Status của Telesales thất bại" });
        }

        [HttpGet]
        public IActionResult GetDetailStatusTelesales()
        {
            var access_token = GetToken();
            var result = _loanStatusDetailService.GetStatusTelesalesDetailAll(access_token);
            return Json(new { data = result });

        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveStatusTelesales(DAL.Object.RequestSaveStatusTelesales entity)
        {
            var user = GetUserGlobal();
            var access_token = GetToken();

            //Cập nhập trạng thái telesales vào bảng loanbrief
            var loanbrief = await loanBriefService.GetBasicInfo(access_token, entity.LoanBriefId);
            if (loanbrief != null)
            {
                var result = loanBriefService.SaveStatusTelesales(access_token, entity);
                if (result)
                {
                    var taskRun = new List<Task>();
                    //lưu lại vào bảng comment
                    if (entity.TelesaleComment != null)
                    {
                        var loanBriefNote = new LoanBriefNote()
                        {
                            LoanBriefId = entity.LoanBriefId,
                            UserId = user.UserId,
                            FullName = user.FullName,
                            ActionComment = (int)EnumActionComment.ChangeStatusTelesales,
                            Note = entity.TelesaleComment,
                            Status = 1,
                            CreatedTime = DateTime.Now,
                            ShopId = 3703,
                            IsDisplay = true,
                            ShopName = "Tima"
                        };
                        var taskNote = Task.Run(() => loanBriefService.AddLoanBriefNote(access_token, loanBriefNote));
                        taskRun.Add(taskNote);

                        //Gửi tin nhắn đến cho khách hàng
                        //Check xem khách đã được gửi tin nhắn chưa
                        var logSendSMS = _dictionaryService.CheckLogSendSMS(access_token, entity.LoanBriefId);
                        //Nếu chưa có thì gửi tin nhắn cho khách
                        if (logSendSMS == null && (entity.StatusTelesales == (int)EnumStatusTelesales.Consider || entity.StatusTelesales == (int)EnumStatusTelesales.WaitPrepareFile))
                        {
                            var loanBrief = loanBriefService.GetLoanBriefByIdNew(access_token, entity.LoanBriefId);
                            _sendSmsBrandName.SendSMS(access_token, loanBrief.Phone, loanBrief.LoanBriefId);
                        }
                    }


                    //Log Action
                    var taskLogAction = Task.Run(() => _logActionService.AddLogAction(access_token, new LogLoanAction
                    {
                        LoanbriefId = loanbrief.LoanBriefId,
                        ActionId = (int)EnumLogLoanAction.TLSChangeStatus,
                        TypeAction = (int)EnumTypeAction.Manual,
                        LoanStatus = loanbrief.Status,
                        TlsLoanStatusDetail = entity.StatusTelesalesDetail, //lưu trạng thái chi tiết được chuyển
                        HubLoanStatusDetail = loanbrief.LoanStatusChild,
                        OldValues = JsonConvert.SerializeObject(new
                        {
                            TlsLoanStatus = loanbrief.StatusTelesales.HasValue ? loanbrief.StatusTelesales.Value.ToString() : null,
                            TlsStatusDetail = loanbrief.DetailStatusTelesales.HasValue ? loanbrief.DetailStatusTelesales.Value.ToString() : null,
                        }),
                        NewValues = JsonConvert.SerializeObject(new
                        {
                            TlsLoanStatus = entity.StatusTelesales,
                            TlsStatusDetail = entity.StatusTelesalesDetail
                        }),
                        TelesaleId = loanbrief.BoundTelesaleId,
                        HubId = loanbrief.HubId,
                        HubEmployeeId = loanbrief.HubEmployeeId,
                        CoordinatorUserId = loanbrief.CoordinatorUserId,
                        UserActionId = user.UserId,
                        GroupUserActionId = user.GroupId,
                        CreatedAt = DateTime.Now
                    })); ;
                    taskRun.Add(taskLogAction);
                    Task.WaitAll(taskRun.ToArray());
                    //End
                    return Json(new { status = 1, message = "Cập nhập trạng thái thành công" });

                }
                else
                    return Json(new { status = 1, message = "Có lỗi xảy ra trong quá trình cập nhập. Vui lòng báo lại với phòng kĩ thuật" });
            }
            else
            {
                return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay" });
            }
        }

        #region Topup
        public async Task<IActionResult> TopupInfo(int loanBriefId = 0)
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            var model = new LoanBriefInit();
            if (loanBriefId > 0)
            {
                var access_token = user.Token;
                var loanBrief = await loanBriefService.GetLoanBriefById(access_token, loanBriefId);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                if (loanBrief.Status != (int)EnumLoanStatus.DISBURSED)
                {
                    return Json(new { status = 0, message = "Đơn vay phải ở trạng thái đã giải ngân!" });
                }
                // Lấy dư nợ còn lại của đơn vay cũ
                long TotalMoneyCurrent = 0;
                //Đối với gói vay xe máy
                if (loanBrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC || loanBrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                    || loanBrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                {
                    if (_environment.EnvironmentName == "Production")
                    {
                        //gọi api lms xem có đủ điều kiện Topup không
                        var checkTopup = await _loanbriefV2Service.CheckCanTopup(loanBrief.LoanBriefId);
                        if (!checkTopup.IsCanTopup)
                            return Json(new { status = 0, message = "Đơn vay không đủ điều kiện vay Topup!" });
                        else
                            TotalMoneyCurrent = (long)checkTopup.CurrentDebt;

                        //var lstPayment = _lmsService.GetPaymentLoanById(loanBrief.LmsLoanId.Value);
                        //if (lstPayment.Status == 1)
                        //{
                        //    TotalMoneyCurrent = lstPayment.Data.Loan.TotalMoneyCurrent;
                        //    if (lstPayment.Data.Loan.TopUp != 1)
                        //        return Json(new { status = 0, message = "Đơn vay không đủ điều kiện vay Topup!" });
                        //}
                    }

                    var maxPrice = GetMaxPriceProductTopupV3((int)loanBrief.LoanBriefProperty.ProductId, (int)loanBrief.ProductId,
                        loanBriefId, TotalMoneyCurrent);
                    var loanAmount = maxPrice.MaxPriceProduct;
                    //Làm tròn tiền xuống đơn vị triệu đồng
                    //loanAmount = Convert.ToInt64(Math.Round((decimal)loanAmount / 1000000, 0, MidpointRounding.ToEven) * 1000000);
                    loanAmount = Convert.ToInt64(LOS.Common.Utils.MathUtils.RoundMoney(loanAmount));
                    if (loanAmount < 1000000)
                        return Json(new { status = 0, message = "Không đủ điều kiện Topup, số tiền < 1tr!" });
                    loanBrief.LoanAmountExpertise = maxPrice.PriceAi;
                    loanBrief.LoanAmountExpertiseLast = maxPrice.MaxPriceProduct;
                    loanBrief.LoanAmount = loanAmount;
                    //kiểm tra xem có HĐ topup nào của đơn vay đang xử lý hay không
                    //nếu có thì không cho tạo
                    if (await _loanbriefV2Service.CheckLoanBriefTopupProcessing(loanBriefId))
                        return Json(new { status = 0, message = "Có đơn Topup đang xử lý. Không thể tạo đơn Topup nữa!" });
                }
                //Đối với gói oto
                else if (loanBrief.ProductId == (int)EnumProductCredit.OtoCreditType_KCC || loanBrief.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                {
                    //Check kiểm tra có đủ điều kiện topup oto hay không
                    model.NationalCard = loanBrief.NationalCard;
                    model.NationCardPlace = loanBrief.NationCardPlace;
                    var nationalCard = model.NationalCard;
                    if (!string.IsNullOrEmpty(model.NationCardPlace))
                        nationalCard = $"{model.NationalCard},{model.NationCardPlace}";
                    var req = new CheckReLoan.Input()
                    {
                        CustomerName = loanBrief.FullName,
                        NumberCard = nationalCard
                    };
                    // Query api   	
                    var data = _lmsService.CheckTopupOto(GetToken(), loanBrief.LoanBriefId, req);
                    if (data.IsAccept == 1)
                        loanBrief.LoanAmountExpertiseLast = data.TotalMoneyCurrent;
                    else
                        return Json(new { status = 0, message = data.Message });

                }
                else
                    return Json(new { status = 0, message = "Đơn Topup chỉ hỗ trợ với gói vay xe máy hoặc ô tô!" });


                model = loanBrief.MapToLoanBriefViewModel();
                model.TotalMoneyCurrent = TotalMoneyCurrent;
                ViewBag.ListInfomationProductDetail = null;
                if (loanBrief.ProductId.HasValue && loanBrief.LoanBriefResident != null
                    && loanBrief.LoanBriefResident.ResidentType.HasValue)
                {
                    ViewBag.ListInfomationProductDetail = _dictionaryService.GetInfomationProductDetailByTypeOwnershipAndProduct(access_token, loanBrief.LoanBriefResident.ResidentType.Value, loanBrief.ProductId.Value);
                }
            }
            //lấy danh sách tls
            if (user.GroupId == (int)EnumGroupUser.ManagerTelesales)
                ViewBag.LstUserTelesales = await _telesalesV2Service.GetTelesalesEnable();
            model.ListProvince = _dictionaryService.GetProvince(GetToken());
            model.ListJobs = CacheData.ListJobs;// _dictionaryService.GetJob(GetToken());
            model.ListRelativeFamilys = CacheData.ListRelativeFamilys;// _dictionaryService.GetRelativeFamily(GetToken());
            model.ListBrandProduct = CacheData.ListBrandProduct;// _dictionaryService.GetBrandProduct(GetToken());
            model.ListLoanProduct = CacheData.ListLoanProduct;// _dictionaryService.GetLoanProduct(GetToken()).Where(x => x.Status == 1).ToList();
            model.ListBank = CacheData.ListBank;// _dictionaryService.GetBank(GetToken());
            model.ListPlatformType = CacheData.ListPlatformType;// _dictionaryService.GetPlatformType(GetToken());
                                                                //return View(model);
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "TopupInfo")));
        }
        [HttpPost]
        public async Task<IActionResult> AddLoanTopup(LoanBriefInit model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                model.IsHeadOffice = true;
                model.PlatformType = (int)EnumPlatformType.ReMarketing;
                //xử lý tên KH
                if (!string.IsNullOrEmpty(model.Customer.FullName))
                {
                    model.Customer.FullName = model.Customer.FullName.ReduceWhitespace();
                    model.Customer.FullName = model.Customer.FullName.TitleCaseString();
                }
                //Nếu là đơn oto
                if (model.ProductId == (int)EnumProductCredit.OtoCreditType_CC
                    || model.ProductId == (int)EnumProductCredit.OtoCreditType_KCC)
                {
                    //Kiểm tra số tiền dư nợ còn lại và số tiền khách hàng muốn vay < 300tr
                    if ((model.LoanAmount + model.LoanAmountExpertiseLast) > 300000000)
                        return Json(new { status = 0, message = "Số tiền vượt quá hạn mức cho phép" });
                }
                else
                {
                    //Kiểm tra nếu số tiền sale nhập lớn hơn số tiền cho vay tối đa
                    if (model.LoanAmount > model.LoanAmountExpertiseLast)
                        return Json(new { status = 0, message = "Số tiền cho vay tối đa không được > " + Convert.ToInt64(model.LoanAmountExpertiseLast).ToString("#,##") });
                    //Kiểm tra nếu số tiền sale nhập < 4tr
                    if (model.LoanAmount < 1000000)
                        return Json(new { status = 0, message = "Số tiền cho vay tối thiểu là 1tr" });
                }

                //kiểm tra CMT
                if (string.IsNullOrEmpty(model.Customer.NationalCard))
                    return Json(new { status = 0, message = "Vui lòng nhập CMT/CCCD" });

                //Check nếu là gói tiểu thương hoặc oto thì k cần check KH có đơn xử lý
                if (model.ProductId != EnumProductCredit.CamotoCreditType.GetHashCode() && model.ProductId != EnumProductCredit.OtoCreditType_CC.GetHashCode() && model.ProductId != EnumProductCredit.OtoCreditType_KCC.GetHashCode() && model.ProductId != EnumProductCredit.CamCoHangHoa.GetHashCode())
                {
                    var listLoanBrief = loanBriefService.GetLoanBriefProcessing(access_token, model.Customer.Phone, model.Customer.NationalCard, model.LoanBriefId);
                    if (listLoanBrief != null && listLoanBrief.LoanBriefId > 0 && listLoanBrief.Status != (int)EnumLoanStatus.DISBURSED)
                        return Json(new { status = 0, message = $"Khách hàng có đơn vay đang xử lý HĐ-{listLoanBrief.LoanBriefId}" });
                }
                //End

                //kiểm tra thành phố đang sống
                if (!model.LoanBriefResident.ProvinceId.HasValue)
                    return Json(new { status = 0, message = "Vui lòng chọn thành phố đang ở" });

                //kiểm tra quận đang sống
                if (!model.LoanBriefResident.DistrictId.HasValue)
                    return Json(new { status = 0, message = "Vui lòng chọn quận/huyện đang ở" });

                //kiểm tra phường
                if (!model.LoanBriefResident.WardId.HasValue)
                    return Json(new { status = 0, message = "Vui lòng chọn phường/xã đang ở" });

                //kiểm tra gói vay
                if (!model.ProductId.HasValue || !Enum.IsDefined(typeof(EnumProductCredit), model.ProductId))
                    return Json(new { status = 0, message = "Vui lòng chọn gói sản phẩm" });

                if (!model.LoanAmount.HasValue || model.LoanAmount <= 0)
                    return Json(new { status = 0, message = "Vui lòng nhập số tiền khách hàng cần vay" });

                //xử lý ngày sinh
                if (!string.IsNullOrEmpty(model.sBirthDay))
                    model.Customer.Dob = DateTimeUtility.ConvertStringToDate(model.sBirthDay, "dd/MM/yyyy");

                //Xử lý đã gán mặc định BuyInsurenceCustomer = true
                if (model.BuyInsurenceCustomer == null)
                    model.BuyInsurenceCustomer = false;


                //Nếu gói vay doanh nghiệp
                if (model.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
                {
                    if (!string.IsNullOrEmpty(model.sBusinessCertificationDate))
                        model.LoanBriefCompany.BusinessCertificationDate = DateTimeUtility.ConvertStringToDate(model.sBusinessCertificationDate, "dd/MM/yyyy");
                    if (!string.IsNullOrEmpty(model.sBirthdayShareholder))
                        model.LoanBriefCompany.BirthdayShareholder = DateTimeUtility.ConvertStringToDate(model.sBirthdayShareholder, "dd/MM/yyyy");
                    if (!string.IsNullOrEmpty(model.sCardNumberShareholderDate))
                        model.LoanBriefCompany.CardNumberShareholderDate = DateTimeUtility.ConvertStringToDate(model.sCardNumberShareholderDate, "dd/MM/yyyy");
                }
                else
                {
                    model.LoanBriefCompany = null;
                }

                if (user != null)
                    model.CreateBy = user.UserId;
                model.Status = EnumLoanStatus.INIT.GetHashCode();
                model.PipelineState = EnumPipelineState.WAITING_FOR_PIPELINE.GetHashCode();
                //nếu tạo đơn + user group telesales
                if (user.GroupId == EnumGroupUser.Telesale.GetHashCode())
                {
                    model.BoundTelesaleId = user.UserId;
                    model.IsHeadOffice = true;
                }
                else if (user.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                {
                    model.HubId = user.ShopGlobal;
                    model.IsHeadOffice = false;
                    model.HubEmployeeId = user.UserId;
                }
                else if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                {
                    model.IsHeadOffice = false;
                    model.HubId = user.ShopGlobal;
                    model.HubEmployeeId = user.UserId;
                }
                else if (user.GroupId == EnumGroupUser.ManagerTelesales.GetHashCode())
                {
                    model.HubId = null;
                    model.HubEmployeeId = null;
                    //bỏ thẩm định HS đi
                    model.CoordinatorUserId = null;
                    model.IsHeadOffice = true;
                    model.UtmSource = "form_rmkt_topup";
                }

                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                if (model.LoanTime > 0)
                    model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);

                model.LoanBriefRelationship = model.LoanBriefRelationship.Where(x => x.RelationshipType > 0 && !string.IsNullOrEmpty(x.FullName) && !string.IsNullOrEmpty(x.Phone)).ToList();
                if (model.LoanBriefRelationship != null && model.LoanBriefRelationship.Count > 0 && model.LoanBriefId > 0)
                    model.LoanBriefRelationship.ToList().ForEach(x => x.LoanBriefId = model.LoanBriefId);
                //else if (user.GroupId == EnumGroupUser.Hub.GetHashCode())




                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                if (model.LoanTime > 0)
                    model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);

                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);

                //Kiểm tra có thay đổi thông tin nơi ở hiện tại hay không
                if ((loanbrief.LoanBriefResident.ProvinceId != model.LoanBriefResident.ProvinceId) || (loanbrief.LoanBriefResident.DistrictId != model.LoanBriefResident.DistrictId) || (loanbrief.LoanBriefResident.Address != model.LoanBriefResident.Address))
                {
                    var lstProvince = CacheData.ListProvince;
                    var ProvinceOld = lstProvince.Where(x => x.ProvinceId == loanbrief.LoanBriefResident.ProvinceId).FirstOrDefault();
                    var ProvinceNew = lstProvince.Where(x => x.ProvinceId == model.LoanBriefResident.ProvinceId).FirstOrDefault();
                    var lstDistrictOld = _dictionaryService.GetDistrict(GetToken(), loanbrief.LoanBriefResident.ProvinceId.Value).Where(x => x.DistrictId == loanbrief.LoanBriefResident.DistrictId).FirstOrDefault();
                    var lstDistrictNew = _dictionaryService.GetDistrict(GetToken(), model.LoanBriefResident.ProvinceId.Value).Where(x => x.DistrictId == model.LoanBriefResident.DistrictId).FirstOrDefault();
                    var lstWardOld = _dictionaryService.GetWard(GetToken(), loanbrief.LoanBriefResident.DistrictId.Value).Where(x => x.WardId == loanbrief.LoanBriefResident.WardId).FirstOrDefault();
                    var lstWardNew = _dictionaryService.GetWard(GetToken(), model.LoanBriefResident.DistrictId.Value).Where(x => x.WardId == model.LoanBriefResident.WardId).FirstOrDefault();

                    var addressOld = ProvinceOld.Name + " - " + lstDistrictOld.Name + " - " + lstWardOld.Name + " - " + loanbrief.LoanBriefResident.Address;
                    var addressNew = ProvinceNew.Name + " - " + lstDistrictNew.Name + " - " + lstWardNew.Name + " - " + model.LoanBriefResident.Address;
                    loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = string.Format("{0} thay đổi thông tin nơi ở từ {1} --> {2}", user.FullName, "<b>" + addressOld + "</b>", "<b>" + addressNew + "</b>"),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                }
                //Kiểm tra có thay đổi thông tin nơi làm việc hay không
                if ((loanbrief.LoanBriefJob.CompanyProvinceId != model.LoanBriefJob.CompanyProvinceId) || (loanbrief.LoanBriefJob.CompanyDistrictId != model.LoanBriefJob.CompanyDistrictId) || (loanbrief.LoanBriefJob.CompanyWardId != model.LoanBriefJob.CompanyWardId) || (loanbrief.LoanBriefJob.CompanyAddress != model.LoanBriefJob.CompanyAddress))
                {
                    var lstProvince = CacheData.ListProvince;

                    var ProvinceOld = lstProvince.Where(x => x.ProvinceId == loanbrief.LoanBriefJob.CompanyProvinceId).FirstOrDefault();
                    var ProvinceNew = lstProvince.Where(x => x.ProvinceId == model.LoanBriefJob.CompanyProvinceId).FirstOrDefault();
                    var lstDistrictOld = _dictionaryService.GetDistrict(GetToken(), loanbrief.LoanBriefJob.CompanyProvinceId.Value).Where(x => x.DistrictId == loanbrief.LoanBriefJob.CompanyDistrictId).FirstOrDefault();
                    var lstDistrictNew = _dictionaryService.GetDistrict(GetToken(), model.LoanBriefJob.CompanyProvinceId.Value).Where(x => x.DistrictId == model.LoanBriefJob.CompanyDistrictId).FirstOrDefault();

                    var WardOld = string.Empty;
                    var WardNew = string.Empty;
                    if (loanbrief.LoanBriefJob.CompanyDistrictId.HasValue)
                    {
                        var lstWard = _dictionaryService.GetWard(GetToken(), loanbrief.LoanBriefJob.CompanyDistrictId.Value);
                        if (loanbrief.LoanBriefJob.CompanyWardId.HasValue)
                        {
                            var ward = lstWard.Where(x => x.WardId == loanbrief.LoanBriefJob.CompanyWardId).FirstOrDefault();
                            if (ward != null)
                                WardOld = ward.Name;

                        }
                    }

                    if (model.LoanBriefJob.CompanyDistrictId.HasValue)
                    {
                        var lstWard = _dictionaryService.GetWard(GetToken(), model.LoanBriefJob.CompanyDistrictId.Value);
                        if (model.LoanBriefJob.CompanyWardId.HasValue)
                        {
                            var ward = lstWard.Where(x => x.WardId == model.LoanBriefJob.CompanyWardId).FirstOrDefault();
                            if (ward != null)
                                WardNew = ward.Name;

                        }
                    }


                    var addressJobOld = ProvinceOld.Name;
                    if (lstDistrictOld != null && !string.IsNullOrEmpty(lstDistrictOld.Name))
                        addressJobOld += $" - {lstDistrictOld.Name}";
                    if (!string.IsNullOrEmpty(WardOld))
                        addressJobOld += $" - {WardOld}";
                    if (loanbrief.LoanBriefJob != null && !string.IsNullOrEmpty(loanbrief.LoanBriefJob.CompanyAddress))
                        addressJobOld += $" - {loanbrief.LoanBriefJob.CompanyAddress}";

                    var addressJobNew = ProvinceNew.Name;
                    if (lstDistrictNew != null && !string.IsNullOrEmpty(lstDistrictNew.Name))
                        addressJobNew += $" - {lstDistrictNew.Name}";
                    if (!string.IsNullOrEmpty(WardNew))
                        addressJobNew += $" - {WardNew}";
                    if (model.LoanBriefJob != null && !string.IsNullOrEmpty(model.LoanBriefJob.CompanyAddress))
                        addressJobNew += $" - {loanbrief.LoanBriefJob.CompanyAddress}";

                    loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = string.Format("{0} thay đổi thông tin nơi làm việc ở từ {1} --> {2}", user.FullName, "<b>" + addressJobOld + "</b>", "<b>" + addressJobNew + "</b>"),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                }
                //Kiểm tra có thay đổi công việc, thu nhập
                if ((loanbrief.LoanBriefJob.JobId != model.LoanBriefJob.JobId) || (loanbrief.LoanBriefJob.TotalIncome != model.LoanBriefJob.TotalIncome))
                {
                    if (loanbrief.LoanBriefJob.JobId != model.LoanBriefJob.JobId)
                    {
                        if (model.ProductId == (int)EnumProductCredit.MotorCreditType_CC || model.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || model.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                        {

                            if (model.LoanBriefResident == null || !model.LoanBriefResident.ResidentType.HasValue)
                                return Json(new { status = 0, message = "Vui lòng chọn hình thức nhà ở!" });

                            var maxPrice = GetMaxPriceProductTopupV3((int)model.LoanBriefProperty.ProductId, (int)model.ProductId, model.LoanBriefId, model.TotalMoneyCurrent);
                            var loanAmount = maxPrice.MaxPriceProduct;
                            //Làm tròn tiền xuống đơn vị triệu đồng
                            //loanAmount = Convert.ToInt64(Math.Round((decimal)loanAmount / 1000000, 0, MidpointRounding.ToEven) * 1000000);
                            loanAmount = Convert.ToInt64(LOS.Common.Utils.MathUtils.RoundMoney(loanAmount));
                            if (loanAmount < 1000000)
                                return Json(new { status = 0, message = "Không đủ điều kiện Topup, số tiền < 1tr!" });
                            model.LoanAmountExpertise = maxPrice.PriceAi;
                            model.LoanAmountExpertiseLast = maxPrice.MaxPriceProduct;
                            model.LoanAmount = loanAmount;
                        }

                    }

                    var lstJob = CacheData.ListJobs;
                    var jobOld = lstJob.Where(x => x.JobId == loanbrief.LoanBriefJob.JobId).FirstOrDefault();
                    var jobNew = lstJob.Where(x => x.JobId == model.LoanBriefJob.JobId).FirstOrDefault();
                    var noteOld = jobOld != null ? jobOld.Name + " - thu nhập:" + Convert.ToInt64(loanbrief.LoanBriefJob.TotalIncome).ToString("#,##") : "";
                    var noteNew = jobNew != null ? jobNew.Name + " - thu nhập:" + Convert.ToInt64(model.LoanBriefJob.TotalIncome).ToString("#,##") : "";
                    loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = string.Format("{0} thay đổi công việc từ {1} --> {2}", user.FullName, "<b>" + noteOld + "</b>", "<b>" + noteNew + "</b>"),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                }
                //Kiểm tra các thông tin công ty, vị trí làm việc
                if ((loanbrief.LoanBriefJob.CompanyName != model.LoanBriefJob.CompanyName) || (loanbrief.LoanBriefJob.CompanyPhone != model.LoanBriefJob.CompanyPhone) || (loanbrief.LoanBriefJob.Description != model.LoanBriefJob.Description))
                {
                    var noteOld = "Công ty:" + loanbrief.LoanBriefJob.CompanyName + " - SDT:" + loanbrief.LoanBriefJob.CompanyPhone + " - vị trí làm việc:" + loanbrief.LoanBriefJob.Description;
                    var noteNew = "Công ty:" + model.LoanBriefJob.CompanyName + " - SDT:" + model.LoanBriefJob.CompanyPhone + " - vị trí làm việc:" + model.LoanBriefJob.Description;
                    loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = string.Format("{0} thay đổi thông tin nghề nghiệp từ {1} --> {2}", user.FullName, "<b>" + noteOld + "</b>", "<b>" + noteNew + "</b>"),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                }
                if (loanbrief.Customer != null)
                {
                    var oldCustomer = loanbrief.Customer;
                    Ultility.CopyObject(model.Customer, ref oldCustomer, false);
                    model.Customer = oldCustomer;
                }
                if (loanbrief.LoanBriefResident != null)
                {
                    var oldLoanBriefResident = loanbrief.LoanBriefResident;
                    Ultility.CopyObject(model.LoanBriefResident, ref oldLoanBriefResident, false);
                    model.LoanBriefResident = oldLoanBriefResident;
                }

                if (loanbrief.LoanBriefHousehold != null)
                {
                    var oldLoanBriefHousehold = loanbrief.LoanBriefHousehold;
                    Ultility.CopyObject(model.LoanBriefHousehold, ref oldLoanBriefHousehold, false);
                    model.LoanBriefHousehold = oldLoanBriefHousehold;
                }

                if (loanbrief.LoanBriefProperty != null)
                {
                    if (model.LoanBriefProperty.PlateNumber == null)
                        model.LoanBriefProperty.PlateNumber = "";
                    var oldLoanBriefProperty = loanbrief.LoanBriefProperty;
                    Ultility.CopyObject(model.LoanBriefProperty, ref oldLoanBriefProperty, false);
                    model.LoanBriefProperty = oldLoanBriefProperty;
                }

                if (loanbrief.LoanBriefJob != null)
                {
                    var oldLoanBriefJob = loanbrief.LoanBriefJob;
                    Ultility.CopyObject(model.LoanBriefJob, ref oldLoanBriefJob, false);
                    model.LoanBriefJob = oldLoanBriefJob;
                }

                if (loanbrief.LoanBriefRelationship != null)
                {
                    var oldLoanBriefRelationship = loanbrief.LoanBriefRelationship;
                    Ultility.CopyObject(model.LoanBriefRelationship, ref oldLoanBriefRelationship, false);
                    model.LoanBriefRelationship = oldLoanBriefRelationship;
                }

                //model.IsHeadOffice = loanbrief.IsHeadOffice;
                var taskRun = new List<Task>();


                model.IsTrackingLocation = loanbrief.IsTrackingLocation;
                model.DeviceId = loanbrief.DeviceId;
                model.IsLocate = loanbrief.IsLocate;
                model.HubId = null;
                model.HubEmployeeId = null;
                var result = await loanBriefService.AddLoanTopup(access_token, model);
                if (result > 0)
                {
                    //Info LoanNew
                    var loanBriefNew = await loanBriefService.GetLoanBriefById(access_token, result);
                    //Thêm note khởi tạo đơn vay
                    var note = new LoanBriefNote
                    {
                        LoanBriefId = loanBriefNew.LoanBriefId,
                        Note = string.Format("{0}: Khởi tạo đơn vay Topup", user.FullName),
                        FullName = user.FullName,
                        UserId = user.UserId,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    };
                    if (!string.IsNullOrEmpty(model.LoanBriefComment))
                        note.Note += "<br />" + model.LoanBriefComment;
                    loanBriefService.AddLoanBriefNote(access_token, note);

                    //gọi api mở hợp đồng
                    var resultOpent = _trackDeviceService.OpenContract(access_token, "HD-" + loanBriefNew.LoanBriefId, loanBriefNew.DeviceId, user.ShopGlobal.ToString(), loanBriefNew.ProductId.Value, loanBriefNew.LoanBriefId, "topup");
                    //StatusCode == 3 => mã hợp đồng đã tồn tại
                    if (resultOpent != null && (resultOpent.StatusCode == 200 || resultOpent.StatusCode == 3))
                    {
                        if (resultOpent.StatusCode == 200)
                        {
                            loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                            {
                                LoanBriefId = result,
                                Note = string.Format("Đơn định vị: Tạo hợp đồng thành công với imei: {0}", loanBriefNew.DeviceId),
                                FullName = user.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = user.UserId,
                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            });
                            loanBriefNew.DeviceStatus = StatusOfDeviceID.OpenContract.GetHashCode();
                        }

                        loanBriefNew.ContractGinno = "HD-" + loanBriefNew.LoanBriefId;
                        //cập nhật db
                        var resultUpdate = loanBriefService.UpdateDevice(access_token, loanBriefNew);

                    }

                    return Json(new { status = 1, message = string.Format("Tạo thành công đơn vay Topup HĐ-{0}", result) });
                }
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi khi tạo mới đơn vay. Vui lòng thử lại sau!" });
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV2/AddLoanTopup Exception");
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        //public IActionResult DeferredPaymentTopUp(int LoanId, int LoanAmount, int LoanTime)
        //{
        //    var param = new DeferredPaymentTopup.Input
        //    {
        //        LoanId = LoanId,
        //        TotalMoneyDisbursement = LoanAmount,
        //        LoanTime = LoanTime
        //    };

        //    var data = _lmsService.CheckCustomerDeferredPaymentTopup(param);

        //    return View(data);

        //}
        #endregion

        #region Request location
        public async Task<IActionResult> RequestLocation(int LoanBriefId)
        {
            try
            {
                if (LoanBriefId > 0)
                {
                    var user = GetUserGlobal();
                    var access_token = user.Token;
                    var loanbrief = await loanBriefService.GetLoanBriefById(access_token, LoanBriefId);
                    if (loanbrief != null && loanbrief.LoanBriefId > 0)
                    {
                        if (_networkService.CheckHomeNetwok(loanbrief.Phone) == (int)HomeNetWorkMobile.Viettel || _networkService.CheckHomeNetwok(loanbrief.Phone) == (int)HomeNetWorkMobile.Mobi)
                        {
                            var ObjRequestLocation = new LogLoanInfoAi()
                            {
                                LoanbriefId = loanbrief.LoanBriefId,
                                ServiceType = (int)ServiceTypeAI.Location,
                                CreatedAt = DateTime.Now,
                                IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest
                            };
                            if (_logReqestAiService.Create(access_token, ObjRequestLocation))
                            {
                                loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                                {
                                    LoanBriefId = loanbrief.LoanBriefId,
                                    Note = string.Format("{0} gửi yêu cầu lấy dữ liệu AI thành công", user.Username),
                                    FullName = user.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = user.UserId,
                                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                });
                                return Json(new { status = 1, message = "Gửi yêu cầu lấy dữ liệu Location thành công" });
                            }
                            else
                            {
                                return Json(new { status = 0, message = "Xảy ra lỗi khi gửi yêu cầu lấy dữ liệu Location. Vui lòng thử lại!!!" });
                            }
                        }
                        else
                        {
                            return Json(new { status = 0, message = "Nhà mạng không hỗ trợ lấy thông tin Location" });
                        }
                    }
                    else
                    {
                        return Json(new { status = 0, message = "Không tồn tại đơn vay trong hệ thống" });
                    }
                }
                else
                {
                    return Json(new { status = 0, message = "Vui lòng truyền mã đơn vay" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi xảy ra, vui lòng thử lại sau!" });
            }
        }
        #endregion

        [HttpGet]
        public async Task<IActionResult> CheckListModal(int loanBriefid)
        {
            var access_token = GetToken();
            var model = new LoanBriefScriptItem();
            var loanBrief = await loanBriefService.GetLoanBriefById(access_token, loanBriefid);
            MapperExtentions.MapToLoanBriefScriptItem(loanBrief, ref model);
            if (loanBrief.ProductId > 0 && loanBrief.LoanBriefResident != null && loanBrief.LoanBriefResident.ResidentType > 0 && loanBrief.LoanBriefProperty != null && loanBrief.LoanBriefProperty.ProductId > 0)
            {
                var priceAG = 0l;
                decimal priceAI = 0l;
                model.MaxPriceProduct = 0;// GetMaxPriceProduct((int)loanBrief.LoanBriefProperty.ProductId, (int)loanBrief.LoanBriefResident.ResidentType, (int)loanBrief.ProductId, loanBriefid, ref priceAG, ref priceAI);
            }
            model.HomeNetwork = _networkService.CheckHomeNetwok(loanBrief.Phone);
            model.ListProvince = _dictionaryService.GetProvince(GetToken());
            model.ListJobs = CacheData.ListJobs;// _dictionaryService.GetJob(GetToken());
            model.ListRelativeFamilys = CacheData.ListRelativeFamilys;// _dictionaryService.GetRelativeFamily(GetToken());
            model.ListBrandProduct = CacheData.ListBrandProduct;// _dictionaryService.GetBrandProduct(GetToken());
            model.ListLoanProduct = CacheData.ListLoanProduct;// _dictionaryService.GetLoanProduct(GetToken()).Where(x => x.Status == 1).ToList();
            model.ListPlatformType = CacheData.ListPlatformType;// _dictionaryService.GetPlatformType(GetToken());
            model.ListLoanPurpose = ExtensionHelper.GetEnumToList(typeof(EnumLoadLoanPurpose));
            model.ListLogRequestAi = _logReqestAiService.GetRequest(access_token, loanBrief.LoanBriefId);
            model.LoanBriefQuestionScript = loanBrief.LoanBriefQuestionScript;

            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "_CheckListModal")));

        }


        #region Danh sách các file ghi âm

        [HttpGet]
        public async Task<IActionResult> GetRecording(int loanBriefid)
        {
            var access_token = GetToken();
            var loanbrief = await loanBriefService.GetLoanBriefById(access_token, loanBriefid);
            if (loanbrief != null && !string.IsNullOrEmpty(loanbrief.Phone))
            {
                var obj = new GetUserReq();
                var listRelationship = await loanBriefService.GetLoanbriefRelationship(access_token, loanBriefid);
                var dicRelationship = new Dictionary<string, LoanBriefRelationship>();
                var listPhoneRelationship = new List<string>();
                if (listRelationship != null)
                {
                    foreach (var item in listRelationship)
                    {
                        if (!string.IsNullOrEmpty(item.Phone))
                        {
                            if (!dicRelationship.ContainsKey(item.Phone))
                            {
                                dicRelationship[item.Phone] = item;
                                listPhoneRelationship.Add(item.Phone);
                            }

                        }
                    }
                }
                var fromDate = loanbrief.CreatedTime.Value.DateTime;
                var toDate = DateTime.Now;
                if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode() && loanbrief.LoanBriefCancelAt.HasValue)
                    toDate = loanbrief.LoanBriefCancelAt.Value;
                else if (loanbrief.Status == EnumLoanStatus.DISBURSED.GetHashCode() && loanbrief.DisbursementAt.HasValue)
                    toDate = loanbrief.DisbursementAt.Value;

                var data = _careSoftService.GetListRecord(loanbrief.Phone, fromDate, toDate);
                //lấy danh sách các cuộc gọi người thân
                if (listPhoneRelationship != null && listPhoneRelationship.Count > 0)
                {
                    var dataRecordRelationship = _careSoftService.GetListRecordByList(listPhoneRelationship, fromDate, toDate);
                    if (dataRecordRelationship != null && dataRecordRelationship.Count > 0)
                    {
                        //if (data == null)
                        //    data = new List<RecordItemV2>();
                        //data.AddRange(dataRecordRelationship);
                    }

                }
                if (data != null && data.Count > 0)
                {
                    var lstIpPhone = data.Select(x => Convert.ToInt32(x.agent_id)).Distinct().ToList();
                    obj.lstIpphone = lstIpPhone;
                    obj.Type = 1;
                    var lstUserName = _userServices.GetListUserByIpPhone(access_token, obj).ToList();

                    var dicUsername = new Dictionary<int, UserDetail>();
                    if (lstUserName != null && lstUserName.Count > 0)
                    {
                        var dataUser = lstUserName.OrderByDescending(x => x.Status.GetValueOrDefault(0)).ToList();
                        foreach (var item in dataUser)
                        {
                            if (item.Ipphone.HasValue && !dicUsername.ContainsKey(item.Ipphone.Value))
                                dicUsername[item.Ipphone.Value] = item;
                        }
                    }
                    if (dicUsername.Keys.Count > 0 || dicRelationship.Keys.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            if (!string.IsNullOrEmpty(item.caller))
                            {
                                if (dicRelationship.ContainsKey(item.caller))
                                {
                                    var relationship = dicRelationship[item.caller];
                                    item.full_name = relationship.FullName;

                                    if (relationship.RelationshipType > 0)
                                    {
                                        if (Enum.IsDefined(typeof(Common.Extensions.EnumRelativeFamily), relationship.RelationshipType))
                                        {
                                            item.relationship_name = Common.Helpers.ExtentionHelper.GetDescription((Common.Extensions.EnumRelativeFamily)relationship.RelationshipType);
                                        }
                                    }
                                }
                            }
                            if (item.agent_id != null && dicUsername.ContainsKey(Convert.ToInt32(item.agent_id)))
                            {
                                var result = dicUsername[Convert.ToInt32(item.agent_id)];
                                item.user_name = result.Username;
                                item.department_name = result.Group?.GroupName;
                            }
                        }
                    }
                }
                //Get data Cisco
                var CSRFTOKEN = "";
                var JSESSIONID = "";
                //if (string.IsNullOrEmpty(HttpContext.Session.GetString("CSRFTOKEN")) || string.IsNullOrEmpty(HttpContext.Session.GetString("JSESSIONID")))
                //{
                var authen = _ciscoService.Authorize(GetToken());
                if (authen != null)
                {
                    CSRFTOKEN = authen[0].CSRFTOKEN;
                    JSESSIONID = authen[0].JSESSIONID;
                    //HttpContext.Session.SetObjectAsString("CSRFTOKEN", authen[0].CSRFTOKEN);
                    //HttpContext.Session.SetObjectAsString("JSESSIONID", authen[0].JSESSIONID);
                }
                //}
                //CSRFTOKEN = await HttpContext.Session.GetObjectFromString<string>("CSRFTOKEN");
                //JSESSIONID = await HttpContext.Session.GetObjectFromString<string>("JSESSIONID");

                var cisco = _ciscoService.GetListByTime(GetToken(), loanbrief.Phone, fromDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd"), CSRFTOKEN, JSESSIONID);
                if (cisco != null && cisco.Count > 0)
                {
                    var lstLine = cisco.Select(x => Convert.ToInt32(x.Line)).Distinct().ToList();
                    obj.lstIpphone = lstLine;
                    obj.Type = 2;
                    var lstUserName = _userServices.GetListUserByIpPhone(access_token, obj).ToList();
                    var dicUsername = new Dictionary<int, UserDetail>();
                    if (lstUserName != null && lstUserName.Count > 0)
                    {
                        foreach (var item in lstUserName)
                        {
                            if (item.CiscoExtension != null && !dicUsername.ContainsKey(Convert.ToInt32(item.CiscoExtension)))
                                dicUsername[Convert.ToInt32(item.CiscoExtension)] = item;
                        }
                    }
                    //if (dicUsername.Keys.Count > 0)
                    //{
                    foreach (var item in cisco)
                    {
                        if (item.Dnis.Length == 13)
                            item.Dnis = item.Dnis.Remove(0, 3);
                        if (item.Line != null && dicUsername.ContainsKey(Convert.ToInt32(item.Line)))
                        {
                            var result = dicUsername[Convert.ToInt32(item.Line)];
                            item.Username = result.Username;
                            item.DepartmentName = result.Group?.GroupName;
                        }

                        if (dicRelationship.ContainsKey(item.Dnis))
                        {
                            var relationship = dicRelationship[item.Dnis];
                            item.full_name = relationship.FullName;
                            if (relationship.RelationshipType > 0)
                            {
                                if (Enum.IsDefined(typeof(Common.Extensions.EnumRelativeFamily), relationship.RelationshipType))
                                {
                                    item.relationship_name = Common.Helpers.ExtentionHelper.GetDescription((Common.Extensions.EnumRelativeFamily)relationship.RelationshipType);
                                }
                            }
                        }
                    }
                    //}
                }
                if (data != null && data.Count > 0)
                    data = data.OrderByDescending(x => x.start_time).ToList();
                var lstdata = new ListRecording
                {
                    LstCareSoft = data,
                    LstCisco = cisco,
                    Fullname = loanbrief.FullName,
                    LoanbriefId = loanbrief.LoanBriefId
                };
                return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(lstdata, "_ListRecording")));
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tồn tại thông tin đơn vay trong hệ thống", null));
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetRecordingV2(int loanBriefid)
        {
            var user = GetUserGlobal();
            var access_token = GetToken();
            var listPhone = new List<string>();// danh sách số dt cần lấy thông tin cuộc gọi: KH và người thân
            var loanbrief = await loanBriefService.GetBasicInfo(access_token, loanBriefid);
            if (loanbrief != null && !string.IsNullOrEmpty(loanbrief.Phone))
            {
                var dataResult = new RecordingModelView()
                {
                    LoanbriefId = loanbrief.LoanBriefId,
                    FullName = loanbrief.FullName,
                    ListRecording = new List<RecordingItemView>()
                };
                var listRecording = new List<RecordingItemView>();
                if (user.GroupId != (int)EnumGroupUser.ApproveEmp)
                    listPhone.Add(loanbrief.Phone);
                var isPhoneHide = true;
                if (user.GroupId == (int)EnumGroupUser.BOD || user.GroupId == (int)EnumGroupUser.Admin || user.GroupId == (int)EnumGroupUser.ManagerTelesales || user.GroupId == (int)EnumGroupUser.ManagerHub || user.GroupId == (int)EnumGroupUser.BusinessManagement)
                    isPhoneHide = false;

                #region lấy thông tin người thân và sdt người thân => lấy thông tin cuộc gọi tham chi
                var listRelationship = await loanBriefService.GetLoanbriefRelationship(access_token, loanBriefid);
                var dicRelationship = new Dictionary<string, LoanBriefRelationship>();
                if (listRelationship != null)
                {
                    foreach (var item in listRelationship)
                    {
                        if (!string.IsNullOrEmpty(item.Phone))
                        {
                            if (!dicRelationship.ContainsKey(item.Phone))
                            {
                                dicRelationship[item.Phone] = item;
                                listPhone.Add(item.Phone);
                            }
                        }
                    }
                }
                #endregion
                var fromDate = loanbrief.CreatedTime.Value.DateTime;
                var toDate = DateTime.Now;
                if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode() && loanbrief.LoanBriefCancelAt.HasValue)
                    toDate = loanbrief.LoanBriefCancelAt.Value;
                else if (loanbrief.Status == EnumLoanStatus.DISBURSED.GetHashCode() && loanbrief.DisbursementAt.HasValue)
                    toDate = loanbrief.DisbursementAt.Value;

                //chuyển từ giờ GMT +7 => GMT
                var data = new List<RecordItemV2>();
                var callApiCaresoft = false;
                if (_baseConfig["AppSettings:GET_RECORD_API_CARESOFT"] != null && _baseConfig["AppSettings:GET_RECORD_API_CARESOFT"] == "1")
                {
                    if ((toDate - fromDate).TotalDays > 15)
                        fromDate = toDate.AddDays(-15);
                    data = _careSoftService.GetListApiCaresoft(listPhone, fromDate, toDate, loanBriefid, access_token);
                    callApiCaresoft = true;
                }
                else
                {
                    var fromDateCS = DateTimeUtility.ConvertGMTDOW(fromDate);
                    var toDateCS = DateTimeUtility.ConvertGMTDOW(toDate);
                    data = _careSoftService.GetListRecordV2(listPhone, fromDateCS, toDateCS, loanBriefid, access_token);
                }
                if (data != null && data.Count > 0)
                {
                    var lstIpPhone = data.Select(x => Convert.ToInt32(x.AgentId)).Distinct().ToList();
                    var obj = new GetUserReq();
                    obj.lstIpphone = lstIpPhone;
                    obj.Type = 1;
                    var lstUserName = _userServices.GetListUserByIpPhone(access_token, obj).ToList();
                    var dicUsername = new Dictionary<int, UserDetail>();
                    if (lstUserName != null && lstUserName.Count > 0)
                    {
                        var dataUser = lstUserName.OrderByDescending(x => x.Status.GetValueOrDefault(0)).ToList();
                        foreach (var item in dataUser)
                        {
                            if (item.Ipphone.HasValue && !dicUsername.ContainsKey(item.Ipphone.Value))
                                dicUsername[item.Ipphone.Value] = item;
                        }
                    }
                    if (dicUsername.Keys.Count > 0 || dicRelationship.Keys.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            if (!string.IsNullOrEmpty(item.Caller))
                            {
                                if (dicRelationship.ContainsKey(item.Caller))
                                {
                                    var relationship = dicRelationship[item.Caller];
                                    item.full_name = relationship.FullName;

                                    if (relationship.RelationshipType > 0)
                                    {
                                        if (Enum.IsDefined(typeof(Common.Extensions.EnumRelativeFamily), relationship.RelationshipType))
                                        {
                                            item.relationship_name = Common.Helpers.ExtentionHelper.GetDescription((Common.Extensions.EnumRelativeFamily)relationship.RelationshipType);
                                        }
                                    }
                                }
                            }
                            if (item.AgentId != null && dicUsername.ContainsKey(Convert.ToInt32(item.AgentId)))
                            {
                                var result = dicUsername[Convert.ToInt32(item.AgentId)];
                                item.user_name = result.Username;
                                item.department_name = result.Group?.GroupName;
                            }
                            //Check Hide Phone
                            if (isPhoneHide)
                                item.Caller = Ultility.HidePhone(item.Caller);
                            if (!callApiCaresoft)
                            {
                                if (item.StartTime.HasValue)
                                    item.StartTime = LOS.WebApp.Helpers.DateTimeUtility.ConvertGMTUP(item.EndTime.Value);
                                if (item.EndTime.HasValue)
                                    item.EndTime = LOS.WebApp.Helpers.DateTimeUtility.ConvertGMTUP(item.EndTime.Value);
                                if (item.WaitingTime > 0)
                                    item.wait_time = LOS.WebApp.Helpers.DateTimeUtility.ConvertHHmmss(item.WaitingTime);
                                if (item.AcdTime > 0)
                                    item.talk_time = LOS.WebApp.Helpers.DateTimeUtility.ConvertHHmmss(item.AcdTime);
                            }
                            var recordingItem = new RecordingItemView()
                            {
                                TypeCallService = (int)EnumTypeCallService.Caresoft,
                                Phone = item.Caller,
                                FullName = item.full_name,
                                RelationshipName = item.relationship_name,
                                UserName = item.user_name,
                                DepartmentName = item.department_name,
                                StartTime = item.StartTime,
                                EndTime = item.EndTime,
                                StrWaitingTime = item.wait_time,
                                StrTalkTime = item.talk_time,
                                Path = item.Path
                            };
                            listRecording.Add(recordingItem);
                        }
                    }
                }

                //Get data Cisco
                var listRecordingCisco = _ciscoService.GetListByTimeV2(GetToken(), listPhone, fromDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd"));
                if (listRecordingCisco != null)
                {
                    var lstLineCiscoWeb = listRecordingCisco.Where(x => x.ContactType == "CALL").Select(x => Convert.ToInt32(x.Line)).Distinct().ToList();
                    var lstUserName = _userServices.GetListUserByIpPhone(access_token, new GetUserReq()
                    {
                        lstIpphone = lstLineCiscoWeb,
                        Type = 2
                    }).ToList();
                    var dicUsername = new Dictionary<int, UserDetail>();
                    if (lstUserName != null && lstUserName.Count > 0)
                    {
                        foreach (var item in lstUserName)
                        {
                            if (item.CiscoExtension != null && !dicUsername.ContainsKey(Convert.ToInt32(item.CiscoExtension)))
                                dicUsername[Convert.ToInt32(item.CiscoExtension)] = item;
                        }
                    }
                    var lstLineCiscoMobile = listRecordingCisco.Where(x => x.ContactType == "CALL_MOBILE").Select(x => Convert.ToInt32(x.Line)).Distinct().ToList();
                    var lstUserNameMobile = _userServices.GetListUserByIpPhone(access_token, new GetUserReq()
                    {
                        lstIpphone = lstLineCiscoWeb,
                        Type = 3
                    }).ToList();

                    var dicUsernameMobile = new Dictionary<int, UserDetail>();
                    if (dicUsernameMobile != null && dicUsernameMobile.Count > 0)
                    {
                        foreach (var item in lstUserNameMobile)
                        {
                            if (item.CiscoExtension != null && !dicUsernameMobile.ContainsKey(Convert.ToInt32(item.CiscoExtension)))
                                dicUsernameMobile[Convert.ToInt32(item.CiscoExtension)] = item;
                        }
                    }

                    foreach (var item in listRecordingCisco)
                    {
                        if (item.PlayUrl.StartsWith("http:192"))
                            item.PlayUrl = item.PlayUrl.Remove(0, 5).Insert(0, "http://");
                        if (!string.IsNullOrEmpty(item.Line))
                        {
                            if (item.ContactType == "CALL")
                            {
                                if (dicUsername.ContainsKey(Convert.ToInt32(item.Line)))
                                {
                                    var result = dicUsername[Convert.ToInt32(item.Line)];
                                    item.Username = result.Username;
                                    item.DepartmentName = result.Group?.GroupName;
                                }

                            }
                            else if (item.ContactType == "CALL_MOBILE")
                            {
                                if (dicUsernameMobile.ContainsKey(Convert.ToInt32(item.Line)))
                                {
                                    var result = dicUsernameMobile[Convert.ToInt32(item.Line)];
                                    item.Username = result.Username;
                                    item.DepartmentName = result.Group?.GroupName;
                                }
                            }
                        }

                        if (dicRelationship.ContainsKey(item.PhoneNumber))
                        {
                            var relationship = dicRelationship[item.PhoneNumber];
                            item.FullName = relationship.FullName;
                            if (relationship.RelationshipType > 0)
                            {
                                if (Enum.IsDefined(typeof(Common.Extensions.EnumRelativeFamily), relationship.RelationshipType))
                                {
                                    item.RelationshipName = Common.Helpers.ExtentionHelper.GetDescription((Common.Extensions.EnumRelativeFamily)relationship.RelationshipType);
                                }
                            }
                        }
                        if (isPhoneHide)
                            item.PhoneNumber = Ultility.HidePhone(item.PhoneNumber);

                        var recordingItem = new RecordingItemView()
                        {
                            TypeCallService = (int)EnumTypeCallService.Metech,
                            Phone = item.PhoneNumber,
                            FullName = item.FullName,
                            RelationshipName = item.RelationshipName,
                            UserName = item.Username,
                            DepartmentName = item.DepartmentName,
                            StartTime = item.StartTime,
                            StrTalkTime = item.CallDuration > 1000 ? DateTimeUtility.ConvertHHmmss(item.CallDuration / 1000) : "",
                            CallDuration = item.CallDuration,
                            PlayUrl = item.PlayUrl,
                            RecordingUrl = item.RecordingUrl
                        };
                        if (item.ContactType == "CALL_MOBILE")
                            recordingItem.TypeCallService = (int)EnumTypeCallService.MetechMobile;
                        listRecording.Add(recordingItem);
                    }
                }

                if (listRecording != null && listRecording.Count > 0)
                    listRecording = listRecording.OrderByDescending(x => x.StartTime).ToList();
                dataResult.ListRecording = listRecording;
                return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(dataResult, "_ListRecordingV2")));
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tồn tại thông tin đơn vay trong hệ thống", null));
            }
        }
        #endregion

        [HttpGet]
        public IActionResult CheckCustomerInAreaSupport(int districtId)
        {
            var district = _dictionaryService.GetDistrictById(GetToken(), districtId);
            return Json(new { data = district });
        }
        public async Task<IActionResult> KeepSession()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> CiscoDownload(int id)
        {
            try
            {
                var CSRFTOKEN = "";
                var JSESSIONID = "";
                IRestResponse res = null;
                //if (string.IsNullOrEmpty(HttpContext.Session.GetString("CSRFTOKEN")) || string.IsNullOrEmpty(HttpContext.Session.GetString("JSESSIONID")))
                //{
                var authen = _ciscoService.Authorize(GetToken());
                if (authen != null)
                {
                    CSRFTOKEN = authen[0].CSRFTOKEN;
                    JSESSIONID = authen[0].JSESSIONID;
                    //HttpContext.Session.SetObjectAsString("CSRFTOKEN", authen[0].CSRFTOKEN);
                    //HttpContext.Session.SetObjectAsString("JSESSIONID", authen[0].JSESSIONID);
                }
                //}
                //CSRFTOKEN = await HttpContext.Session.GetObjectFromString<string>("CSRFTOKEN");
                //JSESSIONID = await HttpContext.Session.GetObjectFromString<string>("JSESSIONID");
                string FileName = "";
                //Call Api ExportingRecordings by ID               
                var ExportingRecordings = await _ciscoService.ExportingRecordings(GetToken(), id, CSRFTOKEN, JSESSIONID);
                if (ExportingRecordings != null)
                {
                    //Call api ExportingDetails
                    Thread.Sleep(2000);
                    var ExportingDetails = await _ciscoService.ExportingDetails(GetToken(), id, ExportingRecordings.Id, CSRFTOKEN, JSESSIONID);
                    if (ExportingDetails != null && ExportingDetails.IsComplete == true)
                    {
                        var client = new RestClient(ExportingDetails.ExportUrl);
                        var request = new RestRequest(Method.GET);
                        request.AddHeader("Cookie", "CSRFTOKEN=" + CSRFTOKEN + ";JSESSIONID=" + JSESSIONID);
                        res = client.Execute(request);
                        FileName = ExportingDetails.Id.ToString();
                    }
                    FileContentResult result = new FileContentResult(res.RawBytes, "application/octet-stream")
                    {
                        FileDownloadName = FileName + ".mp3"
                    };
                    return result;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
            }


        }

        public async Task<IActionResult> CiscoDownloadV2(string url = "")
        {
            try
            {
                var CSRFTOKEN = "";
                var JSESSIONID = "";
                int id = 0;
                IRestResponse res = null;

                var authen = _ciscoService.Authorize(GetToken());
                if (authen != null)
                {
                    CSRFTOKEN = authen[0].CSRFTOKEN;
                    JSESSIONID = authen[0].JSESSIONID;

                }
                var arr = url.Split("/");
                if (arr.Length > 5)
                    id = Convert.ToInt32(arr[5]);
                string FileName = "";
                var ExportingRecordings = await _ciscoService.ExportingRecordings(GetToken(), id, CSRFTOKEN, JSESSIONID);
                if (ExportingRecordings != null)
                {
                    //Call api ExportingDetails
                    Thread.Sleep(2000);
                    var ExportingDetails = await _ciscoService.ExportingDetails(GetToken(), id, ExportingRecordings.Id, CSRFTOKEN, JSESSIONID);
                    if (ExportingDetails != null && ExportingDetails.IsComplete == true)
                    {
                        var client = new RestClient(ExportingDetails.ExportUrl);
                        var request = new RestRequest(Method.GET);
                        request.AddHeader("Cookie", "CSRFTOKEN=" + CSRFTOKEN + ";JSESSIONID=" + JSESSIONID);
                        res = client.Execute(request);
                        FileName = ExportingDetails.Id.ToString();
                    }
                    FileContentResult result = new FileContentResult(res.RawBytes, "application/octet-stream")
                    {
                        FileDownloadName = FileName + ".mp3"
                    };
                    return result;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IActionResult> CiscoDownloadMobile(string url, string phone, int talkTime)
        {
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                IRestResponse res = client.Execute(request);
                FileContentResult result = new FileContentResult(res.RawBytes, "application/octet-stream")
                {
                    FileDownloadName = $"{phone}-{talkTime}.mp3"
                };
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #region Import Đơn vay file Excel
        [HttpGet]
        [Route("loanbrief/import_loan_file_excel.html")]
        public IActionResult ImportFileExcel()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ImportFileExcel(IFormFile file)
        {
            var user = GetUserGlobal();
            string FileName = file.FileName;
            var FileImportId = DateTime.Now.ToString("ddMMyyyyhhMMss");
            var access_token = GetToken();
            List<LoanBriefImportFileExcel> lstData = new List<LoanBriefImportFileExcel>();
            if (file == null || file.Length == 0)
                return Json(new { status = 0, Message = "Bạn chưa chọn file !!!" });

            string fileExtension = Path.GetExtension(file.FileName);
            if (fileExtension != ".xls" && fileExtension != ".xlsx")
                return Json(new { status = 0, Message = "File không đúng định dạng vui lòng tải File mẫu !!!" });

            using (ExcelPackage package = new ExcelPackage(file.OpenReadStream()))
            {

                var currentSheet = package.Workbook.Worksheets;
                var workSheet = currentSheet.First();
                var noOfCol = workSheet.Dimension.End.Column;
                var noOfRow = workSheet.Dimension.End.Row;
                for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                {
                    try
                    {
                        var FullName = workSheet.Cells[rowIterator, 1].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 1].Value.ToString(), @"\s{2,}", "");
                        var Gender = workSheet.Cells[rowIterator, 2].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 2].Value.ToString() == "M" ? "0" : "1", @"\s{2,}", ""); ;
                        var Age = workSheet.Cells[rowIterator, 3].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 3].Value.ToString(), @"\s{2,}", " "); ;
                        var NationalCard = workSheet.Cells[rowIterator, 4].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 4].Value.ToString(), @"\s{2,}", "");
                        var Phone = workSheet.Cells[rowIterator, 5].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 5].Value.ToString(), @"\s{2,}", " ");
                        var DistrictName = workSheet.Cells[rowIterator, 6].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 6].Value.ToString(), @"\s{2,}", "");
                        var CityName = workSheet.Cells[rowIterator, 7].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 7].Value.ToString(), @"\s{2,}", "");
                        var DistrictNameHouseHold = workSheet.Cells[rowIterator, 8].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 8].Value.ToString(), @"\s{2,}", "");
                        var CityNameHouseHold = workSheet.Cells[rowIterator, 9].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 9].Value.ToString(), @"\s{2,}", "");
                        var CompanyName = workSheet.Cells[rowIterator, 10].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 10].Value.ToString(), @"\s{2,}", "");
                        var ProductName = workSheet.Cells[rowIterator, 11].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 11].Value.ToString(), @"\s{2,}", "");
                        var Salary = workSheet.Cells[rowIterator, 12].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 12].Value.ToString(), @"\s{2,}", "");
                        var Source = workSheet.Cells[rowIterator, 13].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 13].Value.ToString(), @"\s{2,}", "");
                        var UserNameTelesales = workSheet.Cells[rowIterator, 14].Value == null ? String.Empty : Regex.Replace(workSheet.Cells[rowIterator, 14].Value.ToString(), @"\s{2,}", "");
                        var CityId = 0;
                        var DistrictId = 0;
                        var CityIdHouseHold = 0;
                        var DistrictIdHouseHold = 0;
                        var ProductId = 0;
                        var BoundTelesaleId = (int)EnumUser.spt_System;

                        //Lấy ra CityId bằng CityName
                        // thành phố hiện tại
                        if (!string.IsNullOrEmpty(CityName))
                        {
                            var resultCity = _dictionaryService.GetProvinceIdByName(access_token, CityName);
                            if (resultCity != null && resultCity.ProvinceId > 0)
                            {
                                CityId = resultCity.ProvinceId;
                            }
                        }
                        // thành phố trong sổ hộ khẩu
                        if (!string.IsNullOrEmpty(CityNameHouseHold))
                        {
                            var resultCityHouseHold = _dictionaryService.GetProvinceIdByName(access_token, CityNameHouseHold);
                            if (resultCityHouseHold != null && resultCityHouseHold.ProvinceId > 0)
                            {
                                CityIdHouseHold = resultCityHouseHold.ProvinceId;
                            }
                        }

                        //Lấy ra DistrictId bằng DistrictName
                        //Quận/huyện hiện tại
                        if (!string.IsNullOrEmpty(DistrictName))
                        {
                            var resultDistrict = _dictionaryService.GetDistrictIdByName(access_token, DistrictName);
                            if (resultDistrict != null && resultDistrict.DistrictId > 0)
                            {
                                DistrictId = resultDistrict.DistrictId;
                            }
                        }
                        //Quận/huyện trong sổ hộ khẩu
                        if (!string.IsNullOrEmpty(DistrictNameHouseHold))
                        {
                            var resultDistrictHouseHold = _dictionaryService.GetDistrictIdByName(access_token, DistrictNameHouseHold);
                            if (resultDistrictHouseHold != null && resultDistrictHouseHold.DistrictId > 0)
                            {
                                DistrictIdHouseHold = resultDistrictHouseHold.DistrictId;
                            }
                        }

                        //Lấy ra ProductId bằng ProductName
                        if (!string.IsNullOrEmpty(ProductName))
                        {
                            var resultProduct = _dictionaryService.GetProductIdByName(access_token, ProductName);
                            if (resultProduct != null && resultProduct.LoanProductId > 0)
                            {
                                ProductId = resultProduct.LoanProductId;
                            }
                            else
                            {
                                ProductId = ProductName.ToLower().Contains("xe máy") ? 2 : ProductName.ToLower().Contains("ô tô") ? 8 : 0;
                            }
                        }

                        //Lấy UserId bằng UserName
                        if (!string.IsNullOrEmpty(UserNameTelesales))
                        {
                            var resultUserName = _userServices.GetByUser(access_token, UserNameTelesales);
                            if (resultUserName != null && resultUserName.UserId > 0 && (resultUserName.GroupId == (int)EnumGroupUser.Telesale || resultUserName.GroupId == (int)EnumGroupUser.ManagerTelesales))
                            {
                                BoundTelesaleId = resultUserName.UserId;
                            }
                        }

                        if (!string.IsNullOrEmpty(FullName) && !string.IsNullOrEmpty(Phone) && CityId > 0 && DistrictId > 0 && ProductId > 0)
                        {
                            var dataExcel = new LoanBriefImportFileExcel()
                            {
                                FileImportName = FileName,
                                FileImportId = FileImportId,
                                Status = 0,
                                FullName = FullName,
                                Phone = Phone,
                                Gender = Gender == "" ? default(short) : Convert.ToInt16(Gender == "M" ? 0 : 1),
                                Age = Age == "" ? default(short) : Convert.ToInt16(Age),
                                NationalCard = NationalCard,
                                Salary = Salary == "" ? 0 : Int64.Parse(Salary.Replace(",", "")),
                                CityName = CityName,
                                DistrictName = DistrictName,
                                CityNameHouseHold = CityNameHouseHold,
                                DistrictNameHouseHold = DistrictNameHouseHold,
                                CompanyName = CompanyName,
                                ProductName = ProductName,
                                Source = Source,
                                CityId = CityId,
                                DistrictId = DistrictId,
                                CityIdHouseHold = CityIdHouseHold,
                                DistrictIdHouseHold = DistrictIdHouseHold,
                                ProductId = ProductId,
                                CreateDate = DateTime.Now,
                                CreateBy = user.UserId,
                                UserName = UserNameTelesales,
                                BoundTelesaleId = BoundTelesaleId
                            };
                            lstData.Add(dataExcel);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            if (lstData.Count == 0)
            {
                return Json(new { status = 0, Message = "Import file sai. Xin vui lòng báo với phòng kĩ thuật !!!" });
            }
            else
            {
                //Lưu dữ liệu vào db
                var result = _loanBriefImportFileExcelServices.AddLoanBriefImportFileExcel(access_token, lstData);
                if (result > 0)
                    return Json(new { status = 1, Message = "Import dữ liệu thành công !!!" });
                else
                    return Json(new { status = 0, Message = "Import dữ liệu không thành thành công. Vui lòng báo với phòng kỹ thuật !!!" });
            }

        }

        [HttpGet]
        public IActionResult GetListTableImportFileExcel(string DateRanger)
        {
            var modal = new SearchImportFileExcel();
            List<TableImportFileExcel> lstData = new List<TableImportFileExcel>();
            var dicTableImportFile = new Dictionary<string, List<TableImportFileExcel>>();
            DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
            DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
            if (!string.IsNullOrEmpty(DateRanger))
                DateTimeUtility.ConvertDateRanger(DateRanger, ref fromDate, ref toDate);

            modal.FromDate = fromDate;
            modal.ToDate = toDate;

            var access_token = GetToken();
            var result = _loanBriefImportFileExcelServices.GetListTableImportFileExcel(access_token, modal.ToQueryObject());
            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    var keyDic = item.FileImportId;
                    if (!dicTableImportFile.ContainsKey(item.FileImportId))
                    {
                        dicTableImportFile[item.FileImportId] = new List<TableImportFileExcel>();
                    }
                    var data = new TableImportFileExcel();
                    data.TotalLoanImport = result.Count(x => x.FileImportId == item.FileImportId);
                    data.TotalLoanImport = result.Count(x => x.FileImportId == item.FileImportId);
                    data.TotalLoanImportSuccess = result.Count(x => x.FileImportId == item.FileImportId && x.Status == (int)EnumStatusLoanBriefImportFileExcel.LoanSuccess);
                    data.TotalLoanImportReject = result.Count(x => x.FileImportId == item.FileImportId && x.Status == (int)EnumStatusLoanBriefImportFileExcel.LoanFail);
                    data.FileName = item.FileImportName;
                    data.FileImportId = item.FileImportId;
                    dicTableImportFile[item.FileImportId].Add(data);
                }
                if (dicTableImportFile.Keys.Count > 0)
                {
                    foreach (var itemKey in dicTableImportFile.Keys)
                    {
                        var data = dicTableImportFile[itemKey];
                        var objTableImportFileExcel = new TableImportFileExcel();
                        objTableImportFileExcel.FileName = data.First().FileName;
                        objTableImportFileExcel.TotalLoanImport = data.First().TotalLoanImport;
                        objTableImportFileExcel.TotalLoanImportSuccess = data.First().TotalLoanImportSuccess;
                        objTableImportFileExcel.TotalLoanImportReject = data.First().TotalLoanImportReject;
                        objTableImportFileExcel.FileImportId = data.First().FileImportId;
                        lstData.Add(objTableImportFileExcel);
                    }
                }
            }
            return View(lstData);
        }

        [HttpPost]
        public IActionResult GetDetailLoanBriefImportFileExcell(GetDetailLoanBriefImportFileExcel entity)
        {
            var access_token = GetToken();
            var lstData = _loanBriefImportFileExcelServices.GetDetailLoanBriefImportFileExcell(access_token, entity.ToQueryObject());
            return View(lstData);
        }
        #endregion

        #region CheckImage
        [HttpPost]
        public IActionResult AddResultCheckImage([FromBody] Common.Models.Request.CheckImageReq entity)
        {
            var user = GetUserGlobal();
            var access_token = user.Token;
            entity.UserId = user.UserId;
            if (entity.LoanBriefId > 0)
            {
                var result = _dictionaryService.AddCheckLoanInformation(GetToken(), entity);
                if (result > 0)
                {
                    var lstCheck = entity.LstCheckImage.Where(x => x.ResultCheck == false).ToList();
                    var mess = "";
                    if (lstCheck != null && lstCheck.Count > 0)
                    {
                        foreach (var item in lstCheck)
                        {
                            mess += item.Description + ": " + item.ResultCheck + "<br />";
                        }
                    }
                    //Lưu Comment
                    loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = entity.LoanBriefId,
                        Note = string.Format("{0} CheckList: {1}", user.FullName, mess),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CheckList.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });

                    if (entity.TypeCheck == 1)
                    {

                        return Json(new { status = 1, message = "CheckList chứng từ Thành công!" });
                    }
                    else
                    {
                        return Json(new { status = 1, message = "CheckList thông tin khách hàng thành công!" });
                    }

                }

                else
                    return Json(new { status = 0, message = "Có lỗi xảy ra, vui lòng thử lại!" });
            }
            else
            {
                return Json(new { status = 0, message = "Mã hợp đồng không tồn tại!" });
            }

        }
        #endregion
        #region Drop Upload
        [HttpPost]
        public async Task<IActionResult> DropUpload(List<IFormFile> files, int documentId, int loanBriefId)
        {
            if (files != null)
            {
                var result = 0;
                var user = GetUserGlobal();
                var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                var BucketName = _baseConfig["AppSettings:BucketName"];
                var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
                {
                    RegionEndpoint = RegionEndpoint.APSoutheast1
                });

                var folder = BucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
                foreach (var file in files)
                {
                    var link = "";
                    var linkThumb = "";
                    var extension = Path.GetExtension(file.FileName);
                    var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                    if (allowedExtensions.Contains(extension.ToLower()))
                    {
                        string img = Guid.NewGuid().ToString();
                        string ImageName = img + extension;
                        string ImageThumb = img + "-thumb" + extension;
                        using (var stream = new MemoryStream())
                        {
                            file.CopyTo(stream);
                            PutObjectRequest req = new PutObjectRequest()
                            {
                                InputStream = stream,
                                BucketName = folder,
                                Key = ImageName,
                                CannedACL = S3CannedACL.PublicRead
                            };
                            await client.PutObjectAsync(req);
                            link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;
                        }

                        //Crop image
                        try
                        {
                            var image = Image.Load(file.OpenReadStream());
                            if (image.Width > 500)
                            {
                                image.Mutate(x => x.Resize(200, image.Height / (image.Width / 200)));
                                using (var outputStream = new MemoryStream())
                                {
                                    image.Save(outputStream, new JpegEncoder());
                                    outputStream.Seek(0, 0);
                                    //Upload Thumb
                                    PutObjectRequest reqThumb = new PutObjectRequest()
                                    {
                                        InputStream = outputStream,
                                        BucketName = folder,
                                        Key = ImageThumb,
                                        CannedACL = S3CannedACL.PublicRead
                                    };
                                    await client.PutObjectAsync(reqThumb);
                                    linkThumb = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + ImageThumb;
                                }
                            }
                        }
                        catch
                        {

                        }
                        //insert to db
                        if (!string.IsNullOrEmpty(link))
                        {
                            result = loanBriefService.AddLoanBriefFile(GetToken(), new LoanBriefFiles()
                            {
                                LoanBriefId = loanBriefId,
                                CreateAt = DateTime.Now,
                                Status = 1,
                                UserId = user.UserId,
                                FilePath = link,
                                TypeId = documentId,
                                S3status = 1,
                                MecashId = 1000000000,
                                FileThumb = linkThumb,
                                SourceUpload = (int)EnumSourceUpload.Web
                            });

                        }
                    }
                    else
                    {
                        return Json(new { status = 0, message = "Vui lòng chọn đúng định dạng ảnh" });
                    }
                }
                if (result > 0)
                    return Json(new { status = 1, message = "Upload ảnh thành công" });
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi khi upload ảnh" });
            }
            else
            {
                return Json(new { status = 0, message = "Không tồn tại file hình ảnh" });
            }
        }

        [HttpPost]
        public IActionResult AddDocumentTypeImage([FromBody] Common.Models.Request.DocumentTypeReq entity)
        {
            var user = GetUserGlobal();
            entity.UserId = user.UserId;
            if (entity.LoanBriefId > 0)
            {
                var result = _dictionaryService.AddDocumentTypeImage(GetToken(), entity);
                if (result > 0)
                    return Json(new { status = 1, message = "Chuyển mục chứng từ thành công!" });
                else
                    return Json(new { status = 0, message = "Có lỗi xảy ra, vui lòng thử lại!" });
            }
            else
            {
                return Json(new { status = 0, message = "Mã hợp đồng không tồn tại!" });
            }

        }

        [HttpGet]
        [PermissionFilter]
        public async Task<IActionResult> GetDocunmentType(int loanbriefId)
        {
            var user = GetUserGlobal();
            var model = new LoanBriefFileViewModel();
            if (loanbriefId > 0)
            {
                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetLoanBriefById(access_token, loanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    model.LoanBriefId = loanbrief.LoanBriefId;
                    if (loanbrief.ProductId > 0)
                    {
                        if (loanbrief.LoanBriefResident == null || !loanbrief.LoanBriefResident.ResidentType.HasValue)
                            return Json(GetBaseObjectResult(false, "Chưa có thông tin Hình thức sở hữu nhà của KH"));
                        if (loanbrief.LoanBriefJob == null || !loanbrief.LoanBriefJob.JobId.HasValue)
                            return Json(GetBaseObjectResult(false, "Chưa có thông tin công việc của KH"));

                        var listDocument = _dictionaryService.GetDocumentTypeV2(GetToken(), loanbrief.ProductId.Value, Common.Utils.ProductPriceUtils.ConvertEnumTypeOfOwnerShipDocument(loanbrief.LoanBriefResident.ResidentType.Value));
                        if (listDocument != null && listDocument.Count > 0)
                        {
                            var imcomeType = 0;
                            bool? companyInsurance = null;
                            bool? businessPapers = null;
                            if (loanbrief.LoanBriefJob != null)
                            {
                                imcomeType = loanbrief.LoanBriefJob.ImcomeType.HasValue ? loanbrief.LoanBriefJob.ImcomeType.Value : 0;
                                companyInsurance = loanbrief.LoanBriefJob.CompanyInsurance != null ? loanbrief.LoanBriefJob.CompanyInsurance : null;
                                businessPapers = loanbrief.LoanBriefJob.BusinessPapers != null ? loanbrief.LoanBriefJob.BusinessPapers : null;
                            }
                            var groupJobId = 0;
                            if (loanbrief.LoanBriefJob != null && loanbrief.LoanBriefJob.JobId.GetValueOrDefault(0) > 0)
                                groupJobId = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(loanbrief.LoanBriefJob.JobId.Value, imcomeType, companyInsurance, businessPapers);

                            var listFiles = loanBriefService.GetLoanBriefFileByLoanId(access_token, loanbriefId);
                            model.ListDocumentV2 = Ultility.ConvertDocumentTree(listDocument, listFiles, 0, groupJobId);
                        }
                        else
                            return Json(GetBaseObjectResult(false, "Không lấy được danh mục chứng từ"));

                        //model.LoanBriefId = loanbrief.LoanBriefId;
                        //var ListDocument = _dictionaryService.GetDocumentType(access_token, loanbrief.ProductId.Value);
                        //if (ListDocument != null && ListDocument.Count > 0)
                        //{
                        //    model.DocumentType = ListDocument.First().Id;
                        //    model.ListDocument = ListDocument;
                        //    var doc = ListDocument.Where(x => x.Version == 2).ToList();
                        //    var workRuleType = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(loanbrief.LoanBriefJob.JobId.Value, loanbrief.LoanBriefJob != null && loanbrief.LoanBriefJob.ImcomeType != null ? loanbrief.LoanBriefJob.ImcomeType.Value : 0
                        //                , loanbrief.LoanBriefJob.CompanyInsurance, loanbrief.LoanBriefJob.BusinessPapers);

                        //    model.ListDocumentNew = Ultility.GetListDocumentNewV2(doc, loanbrief.LoanBriefResident.ResidentType.Value, workRuleType
                        //                        , !String.IsNullOrEmpty(loanbrief.LoanBriefResident.ResultLocation) ? loanbrief.LoanBriefResident.ResultLocation : "");
                        //    model.DocumentType = model.ListDocument.First().Id;
                        //    //model.ListLoanBriefFile = loanBriefService.GetLoanBriefFile(access_token, loanbriefId, model.DocumentType);
                        //    model.ListLoanBriefFile = loanBriefService.GetLoanBriefFileByLoanId(access_token, loanbriefId);
                        //}
                        return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "_GetDocumentType")));
                    }
                    else
                        return Json(GetBaseObjectResult(false, "Đơn vay chưa có gói sản phẩm", null));
                }
                else
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống", null));
            }
            else
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
        }
        #endregion

        [HttpGet]
        public IActionResult InfomationPaymentLMS(RequestPayAmountForCustomer entity)
        {
            //call api sang LMS
            var lstData = new List<LoanInfos>();
            var result = _lmsService.GetPayAmountForCustomer(GetToken(), entity);
            if (result != null || result.Status == 1)
            {
                if (result.Data != null || result.Data.LstLoanInfos != null || result.Data.LstLoanInfos.Count > 0)
                {
                    lstData = result.Data.LstLoanInfos;
                }
            }
            return PartialView("~/Views/LoanBrief/Detail/_InfomationPaymentLMS.cshtml", lstData);
        }

        [HttpPost]
        public IActionResult CheckKalapa(int LoanBriefId, string NationalCard)
        {
            var access_token = GetToken();
            var user = GetUserGlobal();
            bool isCheck = true;
            var lastCallApi = _logCallApi.GetLast(GetToken(), LoanBriefId, (int)ActionCallApi.KalapaFamily);
            if (lastCallApi != null)
            {
                //kiểm tra xem data input có khác nhau k
                if (!string.IsNullOrEmpty(lastCallApi.Input)
                    && lastCallApi.Input == string.Format("{0} - {1}", NationalCard, LoanBriefId))
                {
                    isCheck = false;
                }
            }
            if (isCheck)
            {
                var data = _proxyTimaService.GetFamily(access_token, NationalCard, LoanBriefId);
                if (data != null)
                {
                    var str = new StringBuilder();

                    if (!string.IsNullOrEmpty(data.houseOwner))
                    {
                        str.AppendFormat("<b>Lấy thông tin hộ khẩu</b> <br /> Chủ hộ: <b>{0}</b>:", data.houseOwner);
                    }
                    if (data.familyMember != null && data.familyMember.Count > 0)
                    {
                        foreach (var member in data.familyMember)
                        {
                            if (!string.IsNullOrEmpty(member.name))
                                str.AppendFormat("<br /><b>{0}</b>", member.name);
                            if (!string.IsNullOrEmpty(member.gender))
                                str.AppendFormat("<br />    + Giới tính: <b>{0}</b>", member.gender);
                            if (!string.IsNullOrEmpty(member.dateOfBirth))
                                str.AppendFormat("<br />    + Ngày sinh: <b>{0}</b>", member.dateOfBirth);
                            if (!string.IsNullOrEmpty(member.address))
                                str.AppendFormat("<br />    + Địa chỉ: <b>{0}</b>", member.address);
                            if (!string.IsNullOrEmpty(member.medicalInsurance))
                                str.AppendFormat("<br />    + BHYT: <b>{0}</b>", member.medicalInsurance);
                        }
                    }
                    //Write thông tin ra comment
                    var message = str.ToString();
                    if (!string.IsNullOrEmpty(message))
                    {
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = LoanBriefId,
                            Note = message,
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        loanBriefService.AddLoanBriefNote(access_token, note);

                        //cập nhật vào db
                        if (loanBriefService.UpdateJsonInfoFamilyKalapa(access_token, LoanBriefId, data))
                            return Json(new { status = 1, message = "Lấy thông tin hộ khẩu thành công" });

                    }
                    else
                    {
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = LoanBriefId,
                            Note = "Không có thông tin sổ hộ khẩu",
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        loanBriefService.AddLoanBriefNote(access_token, note);
                        return Json(new { status = 0, message = "Không có thông tin sổ hộ khẩu" });
                    }
                }
                else
                {
                    var note = new LoanBriefNote
                    {
                        LoanBriefId = LoanBriefId,
                        Note = "Không lấy được thông tin sổ hộ khẩu",
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    };
                    loanBriefService.AddLoanBriefNote(access_token, note);
                    return Json(new { status = 0, message = "Không lấy được thông tin sổ hộ khẩu" });
                }
            }

            return Json(new { status = 0, message = "CMT không đổi, không được kiểm tra thông tin hộ khẩu" });
        }

        #region LoanStatusDetail
        [HttpGet]
        public async Task<IActionResult> NextStepLoan(int loanBriefId)
        {
            var user = GetUserGlobal();
            ViewBag.LoanBriefId = loanBriefId;
            var result = new List<LoanStatusDetail>();
            var lstStatusDetail = _loanStatusDetailService.GetLoanStatusDetail(GetToken(), (int)EnumLoanStatusDetail.CVKD);
            var loanbrief = await loanBriefService.GetLoanBriefById(user.Token, loanBriefId);
            if (loanbrief != null)
            {

                var nextStep = "";
                var lstConfigStep = _dictionaryService.GetAllLoanConfigStep(user.Token);
                if (lstConfigStep != null && lstConfigStep.Count > 0)
                {
                    //check LoanStatusDetail nếu không có value set mặc định là Step1
                    if (loanbrief.LoanStatusDetail.GetValueOrDefault(0) == 0)
                        nextStep = "2";
                    else
                        nextStep = _dictionaryService.GetLoanConfigStep(user.Token, loanbrief.LoanStatusDetail.Value).NextStep;

                    List<int> ListStep = null;
                    if (!string.IsNullOrEmpty(nextStep))
                    {
                        ListStep = nextStep.Split(",").Select<string, int>(int.Parse).ToList();
                    }
                    lstConfigStep = lstConfigStep.Where(x => ListStep.Contains(x.CurrentStep.Value)).ToList();

                    if (lstConfigStep != null && lstConfigStep.Count > 0)
                    {
                        foreach (var item in lstConfigStep)
                        {
                            var lstStatusDetail2 = lstStatusDetail.Where(x => x.Id == item.LoanStatusDetailId).ToList();
                            result.AddRange(lstStatusDetail2);
                        }

                    }
                    //if (lstStatusDetail != null)
                    //lstStatusDetail = lstStatusDetail.Where(x => x.Id.).ToList();
                }




            }


            return View(result);
        }

        [HttpPost]
        public async Task<IActionResult> SaveNextStep(DAL.Object.LoanStatusDetail.NextStepLoanHub entity)
        {
            var user = GetUserGlobal();
            var access_token = GetToken();

            var loanbrief = await loanBriefService.GetBasicInfo(access_token, entity.LoanBriefId);
            if (loanbrief != null)
            {
                var result = loanBriefService.SaveNextStep(access_token, entity);
                if (result)
                {
                    var taskRun = new List<Task>();
                    //lưu lại vào bảng comment
                    if (entity.NoteLoanStatusDetail != null)
                    {
                        var loanBriefNote = new LoanBriefNote()
                        {
                            LoanBriefId = entity.LoanBriefId,
                            UserId = user.UserId,
                            FullName = user.FullName,
                            ActionComment = (int)EnumActionComment.HubNextStep,
                            Note = entity.NoteLoanStatusDetail,
                            Status = 1,
                            CreatedTime = DateTime.Now,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        var taskNote = Task.Run(() => loanBriefService.AddLoanBriefNote(access_token, loanBriefNote));
                        taskRun.Add(taskNote);

                    }
                    //Log Action
                    var taskLogAction = Task.Run(() => _logActionService.AddLogAction(access_token, new LogLoanAction
                    {
                        LoanbriefId = loanbrief.LoanBriefId,
                        ActionId = (int)EnumLogLoanAction.HubChangeStatus,
                        TypeAction = (int)EnumTypeAction.Manual,
                        LoanStatus = loanbrief.Status,
                        TlsLoanStatusDetail = loanbrief.DetailStatusTelesales,
                        HubLoanStatusDetail = entity.StatusDetailChild,//lưu trạng thái cuối của hub
                        OldValues = JsonConvert.SerializeObject(new
                        {
                            HubLoanStatus = loanbrief.LoanStatusDetail.HasValue ? loanbrief.LoanStatusDetail.Value.ToString() : null,
                            HubStatusDetail = loanbrief.LoanStatusChild.HasValue ? loanbrief.LoanStatusChild.Value.ToString() : null,
                        }),
                        NewValues = JsonConvert.SerializeObject(new
                        {
                            HubLoanStatus = entity.StatusDetail,
                            HubStatusDetail = entity.StatusDetailChild,
                        }),
                        TelesaleId = loanbrief.BoundTelesaleId,
                        HubId = loanbrief.HubId,
                        HubEmployeeId = loanbrief.HubEmployeeId,
                        CoordinatorUserId = loanbrief.CoordinatorUserId,
                        UserActionId = user.UserId,
                        GroupUserActionId = user.GroupId,
                        CreatedAt = DateTime.Now
                        //JsonConvert.SerializeObject(oldLoanBrief)
                    }));
                    taskRun.Add(taskLogAction);
                    Task.WaitAll(taskRun.ToArray());
                    //End
                    return Json(new { status = 1, message = "Chuyển trạng thái đơn thành công" });

                }
                else
                    return Json(new { status = 1, message = "Có lỗi xảy ra trong quá trình cập nhập. Vui lòng báo lại với phòng kĩ thuật" });
            }
            else
                return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay" });
        }
        #endregion

        [Route("/loanbrief/department.html")]
        [PermissionFilter]
        public IActionResult LoanDepartment()
        {
            var user = GetUserGlobal();
            var access_token = user.Token;
            ViewBag.User = user;
            var model = new LoanbriefHubViewModel();
            if (user.ShopGlobal > 0)
                model.ListUserOfHub = _userServices.GetStaff(access_token, user.ShopGlobal);
            model.ListHub = _shopServices.GetAllHub(access_token);
            if (user.ShopGlobal > 0)
                model.ListHub = model.ListHub.Where(x => x.ShopId != user.ShopGlobal).ToList();

            var lstStatusDetail = _loanStatusDetailService.GetLoanStatusDetail(user.Token, (int)EnumLoanStatusDetail.CVKD);
            ViewBag.ListStatusDetail = lstStatusDetail;
            return View(model);
        }

        public IActionResult GetLoanDepartment()
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var model = new LoanDepartmentDatatable(HttpContext.Request.Form);
                DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
                DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
                if (!string.IsNullOrEmpty(model.DateRanger))
                    DateTimeUtility.ConvertDateRangerV2(model.DateRanger, ref fromDate, ref toDate);

                model.FromDate = fromDate.ToString("yyyy/MM/dd");
                model.ToDate = toDate.ToString("yyyy/MM/dd");

                var listStatus = new List<int>();
                if (string.IsNullOrEmpty(model.status) || model.status == "-1")
                {
                    var dataStatus = _dictionaryService.GetConfigDepartmentStatus(user.Token, Convert.ToInt32(model.departmentId));
                    if (dataStatus != null && dataStatus.Count > 0)
                        listStatus = dataStatus.Select(x => x.LoanStatus.Value).ToList();

                }
                else
                    listStatus.Add(Convert.ToInt32(model.status));



                if (listStatus != null && listStatus.Count > 0)
                    model.status = string.Join(",", listStatus);
                int recordsTotal = 0;
                // Query api   	
                var data = loanBriefService.GetLoanDepartment(access_token, model.ToQueryObject(), ref recordsTotal);
                model.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = model, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult VideoInit(string Url)
        {
            ViewBag.Url = Url;
            return View();
        }

        #region Hàm trong Detail
        [HttpPost]
        public async Task<IActionResult> GetApiCalculateInterest(int loanbriefId, decimal loanAmount, long loanTime, long rateTypeId, decimal ratePercent)
        {
            //Call api tính lãi phí phải trả
            var totalMoneyPaymentOfMonth = 0L;
            var loanbrief = await _loanbriefV2Service.GetBasicInfo(loanbriefId);
            if (loanbrief.Status == (int)EnumLoanStatus.DISBURSED)
            {
                var entity = new RequestPrematureInterest()
                {
                    LoanID = loanbrief.LmsLoanId.Value,
                    CloseDate = DateTime.Now.ToString("dd/MM/yyyy")
                };
                var dataPayment = _lmsService.GetPayAmountForCustomerByLoanID(GetToken(), entity, loanbrief.LoanBriefId);
                if (dataPayment != null && dataPayment.Data != null && dataPayment.Data.LstLoanInfos != null && dataPayment.Data.LstLoanInfos.Count > 0
                    && dataPayment.Data.LstLoanInfos[0].LstPaymentSchedules != null && dataPayment.Data.LstLoanInfos[0].LstPaymentSchedules.Count > 0)
                {
                    var firstPayment = dataPayment.Data.LstLoanInfos[0].LstPaymentSchedules[0];
                    totalMoneyPaymentOfMonth = firstPayment.MoneyOriginal + firstPayment.MoneyInterest + firstPayment.MoneyService + firstPayment.MoneyConsultant;
                }
            }
            else
            {
                var request = new InterestParam.Input
                {
                    TotalMoneyDisbursement = Convert.ToInt64(loanAmount),
                    LoanTime = loanTime * 30,
                    RateType = rateTypeId,
                    Frequency = 30,
                    Rate = ratePercent,
                    LoanBriefId = loanbriefId,
                };
                var data = _lmsService.InterestAndInsurence(GetToken(), request);
                if (data != null && data.Data != null && data.Data.LstPaymentSchedule != null && data.Data.LstPaymentSchedule.Count > 0)
                {
                    totalMoneyPaymentOfMonth = data.Data.LstPaymentSchedule[0].TotalMoneyNeedPayment;
                }

            }
            if (totalMoneyPaymentOfMonth > 0)
            {
                return Json(new { status = 1, data = totalMoneyPaymentOfMonth });
            }
            //var request = new InterestParam.Input
            //{
            //    TotalMoneyDisbursement = Convert.ToInt64(loanAmount),
            //    LoanTime = loanTime * 30,
            //    RateType = rateTypeId,
            //    Frequency = 30,
            //    Rate = ratePercent,
            //    LoanBriefId = loanbriefId,
            //};
            //var data = _lmsService.InterestAndInsurence(GetToken(), request);
            //if (data.Result == 1 && data.Data.LstPaymentSchedule != null)
            return Json(new { status = 0 });
        }

        [HttpGet]
        public IActionResult ResultEkyc(int loanbriefId)
        {
            var data = _resultEkycService.GetEkycByLoanBrief(GetToken(), loanbriefId);
            return Json(GetBaseObjectResult(true, "Success", data, RenderViewAsString(data, "_ResultEkyc")));
        }
        #endregion

        [HttpGet]
        public IActionResult ChangeSupportAll(DAL.Object.LoanBriefChangeSupportAllModel entity)
        {
            var user = GetUserGlobal();
            var access_token = user.Token;
            DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
            DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
            if (!string.IsNullOrEmpty(entity.filtercreateTime))
                DateTimeUtility.ConvertDateRanger(entity.filtercreateTime, ref fromDate, ref toDate);

            //tài khoản qc_phuongnt
            if (user.UserId == (int)EnumUser.qc_phuongnt)
                entity.teamTelesales = "1,2,3,4";
            else if (user.GroupId == (int)EnumGroupUser.ManagerTelesales && user.ListUserTeamTelesales != null && user.ListUserTeamTelesales.Count > 0)
                entity.teamTelesales = string.Join(",", user.ListUserTeamTelesales.Select(x => x.TeamTelesalesId).ToList());
            entity.FromDate = fromDate.ToString("yyyy/MM/dd");
            entity.ToDate = toDate.ToString("yyyy/MM/dd");
            if (user.GroupId == EnumGroupUser.Telesale.GetHashCode())
            {
                entity.boundTelesaleId = user.UserId;
                if (Convert.ToInt32(entity.filterRecare) != -1)
                    entity.boundTelesaleId = Convert.ToInt32(entity.filterRecare);
            }
            if (!string.IsNullOrEmpty(entity.dateRangerLastChangeStatus) && !string.IsNullOrEmpty(entity.dateRangerLastChangeStatus))
            {
                DateTime fromDateLastChangeStatus = DateTimeUtility.FromeDate(DateTime.Now);
                DateTime toDateDateLastChangeStatus = DateTimeUtility.ToDate(DateTime.Now);
                DateTimeUtility.ConvertDateRanger(entity.dateRangerLastChangeStatus, ref fromDateLastChangeStatus, ref toDateDateLastChangeStatus);
                entity.FromDateLastChangeStatus = fromDateLastChangeStatus.ToString("yyyy/MM/dd");
                entity.ToDateLastChangeStatus = toDateDateLastChangeStatus.ToString("yyyy/MM/dd");
            }
            entity.groupId = user.GroupId.Value;
            entity.userId = user.UserId;
            // Query api  
            // Query theo click ở menu
            var lstData = new List<int>();
            if (!string.IsNullOrEmpty(entity.filterClickTop) && Convert.ToInt32(entity.filterClickTop) > 0)
                lstData = loanBriefService.ChangeSupportAllClickTopTls(access_token, entity.ToQueryObject());
            //Query theo tìm kiếm
            else
                lstData = loanBriefService.ChangeSupportAllSearch(access_token, entity.ToQueryObject());

            if (lstData != null && lstData.Count > 0)
                return Json(new { status = 1, data = lstData, message = "Lấy dữ liệu thành công" });
            else
                return Json(new { status = 0, message = "Không tìm thấy đơn vay nào để khởi tạo lại" });
        }

        [HttpGet]
        public IActionResult GetReasonGroup(int groupId)
        {
            // Query api   	
            var data = _reasonService.GetReasonGroup(GetToken(), groupId);
            //Returning Json Data  
            return Json(new { data = data });
        }

        //Lấy chứng từ thu nhập
        [HttpGet]
        public async Task<IActionResult> GetInsuranceInfo(int loanbriefId)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var loanbrief = await loanBriefService.GetBasicInfo(access_token, loanbriefId);
                if (loanbrief != null)
                {
                    var insuInfo = _proxyTimaService.GetInsuranceInfo(access_token, loanbrief.NationalCard, loanbrief.NationCardPlace ?? "", loanbrief.FullName, loanbrief.Dob.Value.ToString("dd/MM/yyyy"), loanbriefId);

                    if (insuInfo.Result == 1)
                    {
                        if (insuInfo.Data != null && insuInfo.Data.Result.Count > 0)
                        {
                            if (insuInfo.Data.Result.Count > 6)
                                insuInfo.Data.Result = insuInfo.Data.Result.Take(6).ToList();
                            var lstInsu = new List<InsuranceInfo>();

                            for (var i = 0; i < insuInfo.Data.Result.Count(); i++)
                            {
                                var obj = new InsuranceInfo();
                                obj.Smonth = string.Format("- Tháng {0} : {1}", insuInfo.Data.Result[i].Smonth, insuInfo.Data.Result[i].Scompany);
                                //obj.CompanyName = string.Format("+ Công ty : {0}", insuInfo.Data.Result[i].Scompany);
                                obj.CompanyAddress = string.Format("+ Địa chỉ : {0}", insuInfo.Data.Result[i].Scompanyaddress);

                                lstInsu.Add(obj);
                            }
                            var objDraw = new DrawImageReq
                            {
                                CustomerName = loanbrief.FullName,
                                NationalCard = loanbrief.NationalCard,
                                Ssiid = insuInfo.Data.Result.First().Ssiid,
                                InsuranceInfo = lstInsu,
                                UrlFileTemp = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}/img/draw-image.jpg"
                            };
                            var draw = _drawService.DrawImage(objDraw);
                            if (draw != null)
                            {
                                var AccessKeyS3 = _baseConfig["AppSettings:AccessKeyS3"];
                                var RecsetAccessKeyS3 = _baseConfig["AppSettings:RecsetAccessKeyS3"];
                                var BucketName = _baseConfig["AppSettings:BucketName"];
                                var client = new AmazonS3Client(AccessKeyS3, RecsetAccessKeyS3, new AmazonS3Config()
                                {
                                    RegionEndpoint = RegionEndpoint.APSoutheast1
                                });
                                var folder = BucketName + "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month;
                                var extension = Path.GetExtension("chung-tu-chung-minh-thu-nhap.jpg");
                                string img = Guid.NewGuid().ToString();
                                string ImageName = img + extension;
                                string ImageThumb = img + "-thumb" + extension;
                                Stream stream = new MemoryStream(draw);
                                PutObjectRequest req = new PutObjectRequest()
                                {
                                    InputStream = stream,
                                    BucketName = folder,
                                    Key = ImageName,
                                    CannedACL = S3CannedACL.PublicRead
                                };
                                await client.PutObjectAsync(req);

                                var link = "/uploads/LOS/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + req.Key;

                                //insert to db
                                var result = loanBriefService.AddLoanBriefFile(GetToken(), new LoanBriefFiles()
                                {
                                    LoanBriefId = loanbriefId,
                                    CreateAt = DateTime.Now,
                                    Status = 1,
                                    UserId = user.UserId,
                                    FilePath = link,
                                    TypeId = 405,
                                    S3status = 1,
                                    MecashId = 1000000000,
                                    SourceUpload = (int)EnumSourceUpload.AutoSystem
                                });
                                //Thêm note
                                loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                                {
                                    LoanBriefId = loanbrief.LoanBriefId,
                                    Note = string.Format("Hợp đồng {0} đã được {1} lấy chứng từ chứng minh thu nhập", loanbriefId, user.FullName),
                                    FullName = user.FullName,
                                    Status = 1,
                                    ActionComment = (int)EnumActionComment.CommentLoanBrief,
                                    CreatedTime = DateTime.Now,
                                    UserId = user.UserId,
                                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                });

                            }
                            else
                                return Json(new { status = 0, message = "Có lỗi xảy ra trong quá trình xử lý ảnh chứng từ. Vui lòng liên hệ kỹ thuật!" });
                        }
                        else
                            return Json(new { status = 0, message = "Không tìm thấy thông tin bảo hiểm" });

                        return Json(new { status = 1, message = "Lấy chứng từ chứng minh thu nhập thành công!" });
                    }
                    else
                        return Json(new { status = 0, message = insuInfo.Message });
                }
                else
                {
                    return Json(new { status = 0, message = "Không tìm thấy thông tin khoản vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "GetInsuranceInfo Exception");
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        private PriceProductTopup GetMaxPriceProductTopupV3(int productId, int productCredit, int loanBriefId, long totalMoneyCurrent = 0)
        {
            var access_token = GetToken();
            long originalCarPrice = 0;
            var maxPriceProduct = 0L;
            // lấy giá bên AI
            decimal priceCarAI = _productService.GetPriceAI(access_token, productId, loanBriefId);
            if (priceCarAI > 0)
            {
                originalCarPrice = (long)priceCarAI;
            }
            else
            {
                //var productPriceCurrent = _productService.GetCurrentPrice(access_token, productId);
                //if (productPriceCurrent != null && productPriceCurrent.PriceCurrent > 0)
                //    originalCarPrice = productPriceCurrent.PriceCurrent ?? 0l;
            }
            //var originalCarPriceLast = originalCarPrice - TotalMoneyCurrent;
            maxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPriceTopup(productCredit, originalCarPrice, totalMoneyCurrent);
            var data = new PriceProductTopup
            {
                PriceAi = originalCarPrice,
                MaxPriceProduct = maxPriceProduct
            };
            return data;
        }

        public async Task<IActionResult> LogLoginFinesse(LogLoginFinesseModel req)
        {
            var user = GetUserGlobal();
            try
            {
                if (!string.IsNullOrEmpty(req.UserName) && !string.IsNullOrEmpty(req.Extension)
                    && !string.IsNullOrEmpty(req.Password))
                {
                    if (await _logCallApiCiscoService.Add(new LogCallApiCisco()
                    {
                        UserId = user.UserId,
                        UserName = req.UserName,
                        Password = req.Password,
                        Extension = req.Extension,
                        StatusResponse = req.StatusResponse,
                        CreatedAt = DateTime.Now,
                        ActionId = (int)LogCallApiCiscoActionId.LoginFinesse
                    }))
                    {
                        return Json(new { status = 1, message = "success" });
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "exception" });
            }
            return Json(new { status = 0, message = "exception" });
        }
    }
}
