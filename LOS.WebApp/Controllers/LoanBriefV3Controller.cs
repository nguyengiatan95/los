﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Services;
using LOS.WebApp.Services.AIService;
using LOS.WebApp.Services.ResultEkyc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using static LOS.WebApp.Models.Ekyc.Ekyc;
using Serilog;
using LOS.WebApp.Cache;
using System.Net.Http;
using LOS.WebApp.Models.Ekyc;
using LOS.Common.Helpers;
using LOS.Services.Services.LogLoanInfoAI;
using LOS.Services.Services.LoanBriefNote;
using LOS.Services.Services.Loanbrief;
using LOS.Services.Services;
using static LOS.DAL.Models.Request.ChangePhone.ChangePhone;
using LOS.WebApp.Services.ToolService;
using LOS.WebApp.Services.CiscoService;
using static LOS.WebApp.Models.Cisco.Cisco;
using LOS.Services.Services.ConfigProductDetail;
using LOS.Common.Utils;

namespace LOS.WebApp.Controllers
{
    public class LoanBriefV3Controller : BaseV2Controller
    {
        private ILoanBriefService _loanBriefService;
        private IConfiguration _configuration;
        private IHttpClientFactory _clientFactory;
        private ICompositeViewEngine _viewEngine;
        private IDictionaryService _dictionaryService;
        private ILMSService _lmsService;
        private IErp _erpService;
        private IProductService _productService;
        private IPriceMotor _priceMotorService;
        private ILogReqestAiService _logReqestAiService;
        private readonly INetworkService _networkService;
        private readonly IPipelineService _pipelineService;
        private IUserServices _userServices;
        private ICheckElectricityAndWater _checkElectricityAndWater;
        private IResultEkycService _resultEkycService;
        private ILogLoanInfoAi _logLoanInfo;
        private ILogActionService _logActionService;
        private IDocumentType _documentService;
        private ILogLoanInfoAIV2Service _logLoanInfoAIV2Service;
        private ILoanBriefNoteV2Service _loanBriefNoteV2Service;
        private ILoanbriefV2Service _loanbriefV2Service;
        private IRelationshipService _relationshipService;
        private IMomoService _momoService;
        private ILogDistributionUserService _logDistributionUserService;
        private ILogLoanActionV2Service _logLoanActionV2Service;
        private IRuleCheckLoanService _ruleCheckLoanService;
        private ILogReLoanBriefService _logReLoanBriefService;
        private IToolService _toolService;
        private ITrackDeviceService _trackDeviceService;
        private ILogCloseLoanService _logCloseLoanService;
        private ICiscoService _ciscoService;
        private IConfigProductDetailService _configProductDetailServiceService;

        public LoanBriefV3Controller(IConfiguration configuration, ILoanBriefService services, IHttpClientFactory clientFactory, ICompositeViewEngine viewEngine, IDictionaryService dictionaryService,
            ILMSService lmsService, IErp erpService, IProductService productService, ILogReqestAiService logReqestAiService, INetworkService networkService, IPriceMotor priceMotorService,
            IPipelineService pipelineService, IUserServices userServices, ICheckElectricityAndWater checkElectricityAndWater,
            IResultEkycService resultEkycService, ILogLoanInfoAi logLoanInfo, IDocumentType documentService, ILogActionService logActionService,
            ILogLoanInfoAIV2Service logLoanInfoAIV2Service, ILoanBriefNoteV2Service loanBriefNoteV2Service, ILoanbriefV2Service loanbriefV2Service,
            IRelationshipService relationshipService, IMomoService momoService, ILogDistributionUserService logDistributionUserService,
            ILogLoanActionV2Service logLoanActionV2Service, IRuleCheckLoanService ruleCheckLoanService, ILogReLoanBriefService logReLoanBriefService,
            IToolService toolService, ITrackDeviceService trackDeviceService, ILogCloseLoanService logCloseLoanService, ICiscoService ciscoService,
            IConfigProductDetailService configProductDetailServiceService)
          : base(configuration, clientFactory, viewEngine)
        {
            this._loanBriefService = services;
            this._clientFactory = clientFactory;
            this._dictionaryService = dictionaryService;
            this._lmsService = lmsService;
            this._erpService = erpService;
            this._productService = productService;
            this._priceMotorService = priceMotorService;
            this._logReqestAiService = logReqestAiService;
            this._networkService = networkService;
            this._pipelineService = pipelineService;
            this._userServices = userServices;
            this._checkElectricityAndWater = checkElectricityAndWater;
            this._resultEkycService = resultEkycService;
            this._logLoanInfo = logLoanInfo;
            this._logActionService = logActionService;
            this._documentService = documentService;
            this._logLoanInfoAIV2Service = logLoanInfoAIV2Service;
            this._loanBriefNoteV2Service = loanBriefNoteV2Service;
            this._loanbriefV2Service = loanbriefV2Service;
            this._relationshipService = relationshipService;
            this._momoService = momoService;
            this._logDistributionUserService = logDistributionUserService;
            this._logLoanActionV2Service = logLoanActionV2Service;
            this._ruleCheckLoanService = ruleCheckLoanService;
            this._logReLoanBriefService = logReLoanBriefService;
            this._toolService = toolService;
            this._trackDeviceService = trackDeviceService;
            this._logCloseLoanService = logCloseLoanService;
            _ciscoService = ciscoService;
            this._configProductDetailServiceService = configProductDetailServiceService;
        }

        #region Form Validate
        private string ValidLoanbriefScriptViewModel(LoanbriefScriptViewModel model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var strMessage = string.Empty;
                if (model.LoanBriefId == 0 && string.IsNullOrEmpty(model.Phone))
                    return "Vui lòng nhập số điện thoại khách hàng";
                if (string.IsNullOrEmpty(model.FullName))
                    return "Vui lòng nhập họ tên khách hàng";
                else
                {
                    model.FullName = model.FullName.Trim();
                    model.FullName = model.FullName.ReduceWhitespace();
                    model.FullName = model.FullName.TitleCaseString();
                }
                //kiểm tra CMT
                if (string.IsNullOrEmpty(model.NationalCard))
                    return "Vui lòng nhập CMT/CCCD";
                if (model.NationalCard.Length != (int)NationalCardLength.CMND_QD && model.NationalCard.Length != (int)NationalCardLength.CMND
                    && model.NationalCard.Length != (int)NationalCardLength.CCCD)
                    return "Vui lòng nhập đúng định dạng CMT/CCCD";

                //Check xem khách hàng có trong black list không
                var checkBlackList = _lmsService.CheckBlackList(user.Token, model.FullName, model.NationalCard, model.Phone, model.LoanBriefId);
                if (checkBlackList != null)
                {
                    if (checkBlackList.Data == true && checkBlackList.Status == 1)
                        return "Khách hàng nằm trong BlackList";
                }
                //Check khách hàng có phải nhân viên của Tima hay không
                if (_baseConfig["AppSettings:IsEnableBlockStaffTima"] != null && _baseConfig["AppSettings:IsEnableBlockStaffTima"] == "1")
                {
                    var checkEmoloyeeTima = _erpService.CheckEmployeeTima(access_token, model.NationalCard, model.Phone, model.LoanBriefId);
                    if (checkEmoloyeeTima != null)
                    {
                        return "Khách hàng là nhân viên Tima!";
                    }
                }

                //kiểm tra thành phố đang sống
                if (!model.ProvinceId.HasValue)
                    return "Vui lòng chọn thành phố đang ở";

                //kiểm tra quận đang sống
                if (!model.DistrictId.HasValue)
                    return "Vui lòng chọn quận/huyện đang ở";

                //kiểm tra phường
                if (!model.WardId.HasValue)
                    return "Vui lòng chọn phường/xã đang ở";
                if (model.LoanAmount <= 0)
                    return "Vui lòng nhập số tiền khách hàng cần vay";
                return strMessage;
            }
            catch (Exception ex)
            {
                return "Xảy ra lỗi. Vui lòng thử lại sau";
            }
        }
        private string ValidLoanbriefViewModel(LoanbriefViewModel model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var strMessage = string.Empty;
                if (model.LoanBriefId == 0 && string.IsNullOrEmpty(model.Phone))
                    return "Vui lòng nhập số điện thoại khách hàng";
                else
                {
                    if (!model.Phone.ValidPhone())
                        return "Vui lòng nhập đúng địng dạng số điện thoại";
                }
                if (string.IsNullOrEmpty(model.FullName))
                    return "Vui lòng nhập họ tên khách hàng";
                else
                {
                    model.FullName = model.FullName.Trim();
                    model.FullName = model.FullName.ReduceWhitespace();
                    model.FullName = model.FullName.TitleCaseString();
                    if (!string.IsNullOrEmpty(model.BankAccountName))
                    {
                        model.BankAccountName = model.BankAccountName.ReduceWhitespace();
                        model.BankAccountName = model.BankAccountName.TitleCaseString();
                        model.BankAccountName = ConvertExtensions.ConvertToUnSign(model.BankAccountName);
                        if (String.Compare(ConvertExtensions.ConvertToUnSign(model.FullName), model.BankAccountName, false) != 0)
                            return "Tên khách hàng và tên chủ thẻ không khớp nhau";
                        //Viết hoa tên tài khoản ngân hàng
                        model.BankAccountName = model.BankAccountName.ToUpper();
                    }
                }
                //kiểm tra CMT
                if (string.IsNullOrEmpty(model.NationalCard))
                    return "Vui lòng nhập CMT/CCCD";

                if (model.NationalCard.Length != (int)NationalCardLength.CMND_QD && model.NationalCard.Length != (int)NationalCardLength.CMND
                    && model.NationalCard.Length != (int)NationalCardLength.CCCD)
                    return "Vui lòng nhập đúng định dạng CMT/CCCD";

                //Check xem khách hàng có trong black list không
                if (model.TypeRemarketing != (int)EnumTypeRemarketing.DebtRevolvingLoan)
                {
                    var checkBlackList = _lmsService.CheckBlackList(access_token, model.FullName, model.NationalCard, model.Phone, model.LoanBriefId);
                    if (checkBlackList != null)
                    {
                        if (checkBlackList.Data == true && checkBlackList.Status == 1)
                            return "Khách hàng nằm trong BlackList";
                    }
                }

                //Check khách hàng có phải nhân viên của Tima hay không
                if (_baseConfig["AppSettings:IsEnableBlockStaffTima"] != null && _baseConfig["AppSettings:IsEnableBlockStaffTima"] == "1")
                {
                    var checkEmoloyeeTima = _erpService.CheckEmployeeTima(access_token, model.NationalCard, model.Phone, model.LoanBriefId);
                    if (checkEmoloyeeTima != null)
                    {
                        return "Khách hàng là nhân viên Tima!";
                    }
                }

                //kiểm tra thành phố đang sống
                if (!model.ProvinceId.HasValue)
                    return "Vui lòng chọn thành phố đang ở";

                //kiểm tra quận đang sống
                if (!model.DistrictId.HasValue)
                    return "Vui lòng chọn quận/huyện đang ở";

                //kiểm tra phường
                if (!model.WardId.HasValue)
                    return "Vui lòng chọn phường/xã đang ở";

                //kiểm tra gói vay
                if (!Enum.IsDefined(typeof(EnumProductCredit), model.ProductId))
                    return "Vui lòng chọn gói sản phẩm";

                if (model.LoanAmount <= 0)
                    return "Vui lòng nhập số tiền khách hàng cần vay";

                if (!string.IsNullOrEmpty(model.PhoneOther))
                {
                    if (!model.PhoneOther.ValidPhone())
                        return "Vui lòng nhập đúng định dạng số điện thoại khác";
                }
                if (!string.IsNullOrEmpty(model.Email))
                {
                    if (!model.Email.ValidEmail())
                        return "Vui lòng nhập đúng định dạng email";
                }
                return strMessage;
            }
            catch (Exception ex)
            {
                return "Xảy ra lỗi. Vui lòng thử lại sau";
            }
        }

        #endregion

        #region Init And Edit Loan
        [HttpPost]
        public async Task<IActionResult> InitLoanBrief(LoanbriefViewModel model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                model.IsHeadOffice = true;
                var messageValid = ValidLoanbriefViewModel(model);

                if (!string.IsNullOrEmpty(messageValid))
                    return Json(new { status = 0, message = messageValid });

                //mặc định mua bảo hiểm đối với gói xe máy
                //Gói oto mặc định k mua bao hiểm
                if (model.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                {
                    model.BuyInsurenceCustomer = false;// model.BuyInsurenceCustomer;
                    model.IsLocate = model.IsLocateCar;
                }
                else
                {
                    //đơn Tái cấu trúc nợ và đơn topup mặc định là không mua bảo hiểm
                    if (model.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan)
                    {
                        model.LoanBriefPropertyModel.BuyInsuranceProperty = false;
                        model.BuyInsurenceCustomer = false;
                    }
                    else
                        model.BuyInsurenceCustomer = true;
                }
                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                if (model.LoanTime > 0)
                    model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);

                bool isChangePipeline = false;
                var loanbrief = new LoanBriefDetail();

                if (model.TypeRemarketing.GetValueOrDefault(0) != (int)EnumTypeRemarketing.DebtRevolvingLoan)
                {
                    //gói vay xe máy
                    if (model.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                            || model.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                            || model.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                    {
                        loanbrief = _loanBriefService.GetLoanBriefProcessing(access_token, model.Phone, model.NationalCard, model.LoanBriefId);
                        if (loanbrief != null && loanbrief.LoanBriefId > 0)
                        {
                            if (model.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                            {
                                if (loanbrief.Status != (int)EnumLoanStatus.DISBURSED)
                                    return Json(new { status = 0, message = $"Khách hàng có đơn vay đang xử lý HĐ-{loanbrief.LoanBriefId}" });
                            }
                            else
                            {
                                return Json(new { status = 0, message = $"Khách hàng có đơn vay đang xử lý HĐ-{loanbrief.LoanBriefId}" });
                            }
                        }
                    }
                    //gói vay ô tô
                    else
                    {
                        //Nếu là tạo đơn mới => kiểm tra xem có đơn vay đang xử lý hay không
                        if (model.LoanBriefId == 0)
                        {
                            loanbrief = await _loanBriefService.GetLoanBriefBorrowing(access_token, model.Phone, model.NationalCard);
                            if (loanbrief != null && loanbrief.LoanBriefId > 0)
                                return Json(new { status = 0, message = $"Khách hàng có đơn vay đang xử lý. Vui lòng tạo đơn con từ HĐ-{loanbrief.LoanBriefId}" });
                        }
                        //Kiểm tra có đơn đang vay hay không
                        //Nếu có đơn đang vay => chỉ cho tạo đơn topup đủ điêu kiện
                        loanbrief = await _loanBriefService.GetLoanBriefDisbursed(access_token, model.Phone, model.NationalCard);
                        if (loanbrief != null && loanbrief.LoanBriefId > 0)
                        {
                            if (!string.IsNullOrEmpty(model.NationalCard) && !string.IsNullOrEmpty(model.FullName) && loanbrief.CustomerId > 0)
                            {
                                //xem có đơn topup nào đang xử lý không
                                decimal totalMoneyBrrowing = 0;
                                var lstLoanTopup = await _loanBriefService.GetListLoanTopupCar(access_token, loanbrief.CustomerId.Value);
                                if (lstLoanTopup != null && lstLoanTopup.Count > 0)
                                    totalMoneyBrrowing = lstLoanTopup.Sum(x => x.LoanAmount.Value);

                                //cập nhập đơn thì loại bỏ số tiền của đơn này ra. Đã được sum ở trên
                                if (model.LoanBriefId > 0)
                                    totalMoneyBrrowing = totalMoneyBrrowing - model.LoanAmount;

                                //check xem có đơn vay topup chưa
                                //có rồi thì thông báo không tạo được đơn Topup
                                //chưa có thì check xem đủ điều kiện Topup không
                                var nationalCard = model.NationalCard;
                                if (!string.IsNullOrEmpty(model.NationCardPlace))
                                    nationalCard = $"{model.NationalCard},{model.NationCardPlace}";
                                var objCheckTopupOto = new CheckReLoan.Input
                                {
                                    NumberCard = nationalCard,
                                    CustomerName = model.FullName,
                                };
                                //check xem đơn có đủ điều kiện để topup không
                                var checkTopupOto = _lmsService.CheckTopupOto(access_token, loanbrief.LoanBriefId, objCheckTopupOto);
                                if (checkTopupOto.IsAccept == 1)
                                {
                                    //số tiền dư nợ + với số tiền vay hiện tại nhỏ hơn 300tr thì cho cập nhập
                                    if (model.LoanAmount + checkTopupOto.TotalMoneyCurrent + totalMoneyBrrowing > 300000000)
                                        return Json(new { status = 0, message = "Số tiền vay và số tiền dư nợ phải nhỏ hơn 300tr. Dư nợ hiện tại:" + (checkTopupOto.TotalMoneyCurrent + totalMoneyBrrowing).ToString("###,0") + " VNĐ." });

                                    //thêm mới
                                    if (model.LoanBriefId == 0)
                                    {
                                        model.ReMarketingLoanBriefId = loanbrief.LoanBriefId;
                                        model.TypeRemarketing = (int)EnumTypeRemarketing.IsTopUp;
                                        model.PlatformType = (int)EnumPlatformType.ReMarketing;
                                        model.Comment += "Đơn vay Topup được khởi tạo từ HĐ-" + loanbrief.LoanBriefId;
                                    }
                                }
                                else
                                    return Json(new { status = 0, message = checkTopupOto.Message });
                            }
                        }
                    }
                }
                //xử lý chuẩn hóa tên KH
                if (!string.IsNullOrEmpty(model.FullName))
                {
                    if (!model.FullName.IsNormalized())
                        model.FullName = model.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                }
                #region Cập nhật thông tin đơn vay
                if (model.LoanBriefId > 0)
                {
                    //Lấy thông tin đơn vay cũ
                    var oldLoanBrief = await _loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);
                    if (oldLoanBrief == null)
                        return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay trong hệ thống" });

                    var resultCheckTopup = new DAL.DTOs.Loanbrief.CheckTopupResult();

                    //kiểm tra số CMND có trùng với đơn đang vay nào không
                    if (model.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                    {

                        if (model.NationalCard != oldLoanBrief.NationalCard && oldLoanBrief.ReMarketingLoanBriefId.HasValue)
                        {
                            //check cmnd với loanbrief của đơn topup có đơn nào đang xử lý hay không
                            var checkCMND = await _loanBriefService.GetLoanBriefByNationalCard(access_token, model.NationalCard, oldLoanBrief.ReMarketingLoanBriefId.Value);
                            if (checkCMND != null)
                                return Json(new { status = 0, message = "Khách hàng đang có hs trùng với hồ sơ đang vay HĐ:" + checkCMND.LoanBriefId });
                        }


                        //Kiểm tra đơm vay gốc có đủ điều kiện topup không
                        if (oldLoanBrief.ReMarketingLoanBriefId > 0
                                       && (model.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                           || model.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                           || model.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))
                        {
                            //Kiểm tra xem đã chọn gói vay chi tiết chưa
                            if (model.ProductDetailId.GetValueOrDefault(0) == 0)
                                return Json(new { status = 0, message = "Vui lòng chọn gói vay chi tiết" });
                            //Nếu là topup lần 1 => Kiểm tra gói vay chi tiết
                            var loanbriefParent = await _loanbriefV2Service.GetBasicInfo(oldLoanBrief.ReMarketingLoanBriefId.Value);
                            //if (loanbriefParent.TypeRemarketing != (int)EnumTypeRemarketing.IsTopUp
                            //    && model.ProductDetailId != loanbriefParent.ProductDetailId)
                            //{
                            resultCheckTopup = await _loanbriefV2Service.CheckCanTopup(oldLoanBrief.ReMarketingLoanBriefId.Value);
                            if (resultCheckTopup.IsCanTopup)
                            {
                                if (!resultCheckTopup.IsTopupOnTopup)
                                {
                                    var typeOwnerShip = oldLoanBrief.LoanBriefResident.ResidentType;
                                    if (typeOwnerShip != (int)EnumTypeofownership.KT1 && typeOwnerShip != (int)EnumTypeofownership.KT3)
                                        typeOwnerShip = model.LoanBriefResidentModel.ResidentType;
                                    //Kiểm tra điều kiện goi vay
                                    if (typeOwnerShip > 0)
                                    {
                                        var productMaxMoney = 0l;
                                        if (loanbriefParent.ProductDetailId > 0)
                                        {
                                            var productDetail = await _configProductDetailServiceService.GetConfigProductCreditDetail(loanbriefParent.ProductId.Value,
                                                    loanbriefParent.ProductDetailId.Value, typeOwnerShip.Value);
                                            if (productDetail != null && productDetail.MaxMoney > 0)
                                                productMaxMoney = productDetail.MaxMoney.Value;
                                        }
                                        else
                                        {
                                            //Nếu đơn cũ không có gói chi tiết thì chọn theo giá trị vay
                                            var listProductDetaiNext = await _configProductDetailServiceService.GetConfigProductCreditDetailNext(oldLoanBrief.ProductId.Value,
                                                         typeOwnerShip.Value, (long)loanbriefParent.LoanAmount);
                                            if (listProductDetaiNext != null && listProductDetaiNext.Count > 0)
                                            {
                                                var productDetailNext = listProductDetaiNext[0];
                                                if (listProductDetaiNext.Count > 1)
                                                    productDetailNext = listProductDetaiNext[1];
                                                if (model.ProductDetailId.GetValueOrDefault(0) != productDetailNext.ProductDetailId
                                                        && listProductDetaiNext.Any(x => x.ProductDetailId == model.ProductDetailId))
                                                    return Json(new { status = 0, message = $"Vui lòng chọn gói vay chi tiết: {productDetailNext.Name} hoặc gói thấp hơn" });
                                            }
                                        }

                                        if (productMaxMoney > 0)
                                        {
                                            var listProductDetaiNext = await _configProductDetailServiceService.GetConfigProductCreditDetailNext(oldLoanBrief.ProductId.Value,
                                                         typeOwnerShip.Value, productMaxMoney);
                                            if (listProductDetaiNext != null && listProductDetaiNext.Count > 0)
                                            {
                                                var productDetailNext = listProductDetaiNext.First();
                                                if (model.ProductDetailId.GetValueOrDefault(0) != productDetailNext.ProductDetailId
                                                        && listProductDetaiNext.Any(x => x.ProductDetailId == model.ProductDetailId))
                                                    return Json(new { status = 0, message = $"Vui lòng chọn gói vay chi tiết: {productDetailNext.Name} hoặc gói thấp hơn" });
                                            }
                                        }
                                    }
                                }

                                //Kiểm tra giới hạn số tiền cho vay
                                var limitedLoanAmount = ProductPriceUtils.LimitedLoanAmount(model.ProductId, model.LoanBriefResidentModel.ResidentType.Value);

                                if ((model.LoanAmount + resultCheckTopup.CurrentDebt) > limitedLoanAmount)
                                {
                                    var limitedLoan = limitedLoanAmount - resultCheckTopup.CurrentDebt;
                                    return Json(new { status = 0, message = string.Format("Số tiền cho vay không được lớn hơn {0}đ", limitedLoan.ToString("##,#")) });
                                }
                            }
                            else
                            {
                                //Đơn gốc không đủ điều kiện topup
                                return Json(new { status = 0, message = "Đơn vay không đủ điều kiện topup" });
                            }
                            //}
                        }
                    }

                    //check xem khách hàng có tài khoản esign chưa
                    //có rồi thì không cho thay đổi tên và số cmnd nữa
                    if (!string.IsNullOrEmpty(oldLoanBrief.Customer.EsignAgreementId))
                    {
                        model.FullName = oldLoanBrief.FullName;
                        model.NationalCard = oldLoanBrief.NationalCard;
                    }
                    var taskRun = new List<Task>();
                    if (oldLoanBrief.FirstProcessingTime.HasValue)
                        model.FirstProcessingTime = oldLoanBrief.FirstProcessingTime;
                    else
                        model.FirstProcessingTime = DateTime.Now;
                    bool IsChangeProduct = false;
                    //Kiểm tra xem có thay đổi gói vay không
                    if (model.ProductId > 0 && oldLoanBrief.ProductId > 0 && model.ProductId != oldLoanBrief.ProductId)
                    {
                        isChangePipeline = true;
                        if (model.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                            || model.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                            || model.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                            IsChangeProduct = true;
                    }
                    //kiểm tra có thay đổi thông tin xe hay không hoặc thay đổi gói vay => định giá lại tài sản
                    var oldProperty = await _loanBriefService.GetLoanbriefProperty(access_token, model.LoanBriefId);
                    if (IsChangeProduct || (oldProperty != null && oldProperty.BrandId > 0 && oldProperty.ProductId > 0)
                        || (model.LoanBriefPropertyModel.ProductId > 0 && (oldProperty == null || oldProperty.ProductId == 0 || !oldProperty.ProductId.HasValue))
                        )
                    {
                        //Thay đổi tài sản
                        if (model.LoanBriefPropertyModel != null && model.LoanBriefPropertyModel.BrandId > 0
                            && model.LoanBriefPropertyModel.ProductId > 0
                            && oldProperty != null && oldProperty.ProductId > 0
                            && model.LoanBriefPropertyModel.ProductId != oldProperty.ProductId)
                        {
                            var oldProductName = string.Empty;
                            if (oldProperty.ProductId.HasValue)
                            {
                                var oldProduct = _productService.GetProduct(access_token, oldProperty.ProductId.Value);
                                if (oldProduct != null)
                                    oldProductName = oldProduct.FullName;
                            }
                            var newProduct = _productService.GetProduct(access_token, model.LoanBriefPropertyModel.ProductId.Value);
                            taskRun.Add(Task.Run(() => _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                            {
                                LoanBriefId = model.LoanBriefId,
                                Note = IsChangeProduct ? string.Format("{0} cập nhật xe {1}", user.FullName, newProduct.FullName) : string.Format("{0} thay đổi xe máy {1} thành {2}", user.FullName, oldProductName, newProduct.FullName),
                                FullName = user.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = user.UserId,
                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            })));
                            var priceAG = 0l;
                            decimal priceAI = 0l;
                            if (model.LoanBriefResidentModel.ResidentType.HasValue && model.LoanBriefQuestionScriptModel != null
                                && model.LoanBriefQuestionScriptModel.MaxPrice.HasValue)
                            {
                                var typeRemarketingLtv = 0;
                                var loanAmountExpertiseLast = 0M;
                                //Nếu là đơn topup => trừ đi dư nợ đang có
                                //Đối với gói vay xe máy topup
                                if (model.TypeRemarketing == EnumTypeRemarketing.IsTopUp.GetHashCode()
                                    && oldLoanBrief.ReMarketingLoanBriefId > 0
                                    && (model.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                        || model.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                        || model.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))
                                {
                                    //Kiểm tra đơm vay gốc có đủ điều kiện topup không
                                    if (resultCheckTopup == null || resultCheckTopup.CurrentDebt <= 0)
                                        resultCheckTopup = await _loanbriefV2Service.CheckCanTopup(oldLoanBrief.ReMarketingLoanBriefId.Value);
                                    var totalMoneyDebtCurrent = (long)resultCheckTopup.CurrentDebt;

                                    loanAmountExpertiseLast = GetMaxPriceProductTopup(newProduct.Id, model.LoanBriefId, model.ProductId, model.LoanBriefResidentModel.ResidentType.Value,
                                        totalMoneyDebtCurrent, ref priceAI);
                                    if (loanAmountExpertiseLast < 3000000)
                                        return Json(new { status = 0, message = "Đơn vay không đủ điều kiện vay Topup định giá tài sản nhỏ hơn 3tr" });
                                    typeRemarketingLtv = (int)TypeRemarketingLtv.TopUp;
                                }
                                if (oldLoanBrief.IsReborrow.GetValueOrDefault(false))
                                    typeRemarketingLtv = (int)TypeRemarketingLtv.Reborrow;
                                if (loanAmountExpertiseLast == 0)
                                    loanAmountExpertiseLast = GetMaxPriceProductV3((int)newProduct.Id, model.LoanBriefId, (long)model.LoanBriefQuestionScriptModel.MaxPrice,
                                        model.ProductId, ref priceAG, ref priceAI);

                                model.LoanAmountExpertiseLast = loanAmountExpertiseLast;
                                model.LoanAmountExpertise = priceAG;
                                if (priceAI > 0)
                                    model.LoanAmountExpertiseAi = priceAI;
                                if (model.LoanAmountExpertiseLast > 0)
                                {
                                    var task1 = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                                    {
                                        LoanBriefId = model.LoanBriefId,
                                        Note = string.Format("Xe của khách hàng được định giá tối đa {0}đ", model.LoanAmountExpertiseLast.Value.ToString("##,#")),
                                        FullName = user.FullName,
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = user.UserId,
                                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                    }));
                                    taskRun.Add(task1);
                                }
                            }

                        }
                        else
                        {
                            if (model.LoanBriefPropertyModel.ProductId > 0)
                            {
                                model.LoanAmountExpertiseLast = oldLoanBrief.LoanAmountExpertiseLast;
                                model.LoanAmountExpertise = oldLoanBrief.LoanAmountExpertise;
                                model.LoanAmountExpertiseAi = oldLoanBrief.LoanAmountExpertiseAi;
                                if (model.LoanAmountExpertiseLast.GetValueOrDefault(0) == 0
                                    || (model.LoanAmountExpertiseAi <= 0 && model.LoanAmountExpertise <= 0) || IsChangeProduct)
                                {
                                    var newProduct = _productService.GetProduct(access_token, model.LoanBriefPropertyModel.ProductId.Value);
                                    var priceAG = 0l;
                                    decimal priceAI = 0l;
                                    if (model.LoanBriefResidentModel.ResidentType.HasValue)
                                    {
                                        var loanAmountExpertiseLast = 0M;
                                        //Nếu là đơn topup => trừ đi dư nợ đang có
                                        //Đối với gói vay xe máy topup
                                        if (model.TypeRemarketing == EnumTypeRemarketing.IsTopUp.GetHashCode()
                                            && oldLoanBrief.ReMarketingLoanBriefId > 0
                                            && (model.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                                                || model.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                                                || model.ProductId == (int)EnumProductCredit.MotorCreditType_KGT))
                                        {

                                            //Kiểm tra đơm vay gốc có đủ điều kiện topup không
                                            if (resultCheckTopup == null || resultCheckTopup.CurrentDebt <= 0)
                                                resultCheckTopup = await _loanbriefV2Service.CheckCanTopup(oldLoanBrief.ReMarketingLoanBriefId.Value);
                                            if (resultCheckTopup.IsCanTopup)
                                            {
                                                var totalMoneyDebtCurrent = (long)resultCheckTopup.CurrentDebt;
                                                loanAmountExpertiseLast = GetMaxPriceProductTopup(newProduct.Id, model.LoanBriefId, model.ProductId, model.LoanBriefResidentModel.ResidentType.Value, totalMoneyDebtCurrent, ref priceAI);
                                                if (loanAmountExpertiseLast < 3000000)
                                                    return Json(new { status = 0, message = "Đơn vay không đủ điều kiện vay Topup định giá tài sản nhỏ hơn 3tr" });
                                            }
                                        }
                                        if (loanAmountExpertiseLast == 0)
                                            loanAmountExpertiseLast = GetMaxPriceProductV3((int)newProduct.Id, model.LoanBriefId, (long)model.LoanBriefQuestionScriptModel.MaxPrice.GetValueOrDefault(0), model.ProductId, ref priceAG, ref priceAI);

                                        model.LoanAmountExpertiseLast = loanAmountExpertiseLast;
                                        model.LoanAmountExpertise = priceAG;
                                        if (priceAI > 0)
                                            model.LoanAmountExpertiseAi = priceAI;
                                        if (model.LoanAmountExpertiseLast > 0)
                                        {
                                            var task1 = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                                            {
                                                LoanBriefId = model.LoanBriefId,
                                                Note = string.Format("Xe của khách hàng được định giá tối đa {0}đ", model.LoanAmountExpertiseLast.Value.ToString("##,#")),
                                                FullName = user.FullName,
                                                Status = 1,
                                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                                CreatedTime = DateTime.Now,
                                                UserId = user.UserId,
                                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                            }));
                                            taskRun.Add(task1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    model.LoanAmountFirst = oldLoanBrief.LoanAmountFirst > 0 ? oldLoanBrief.LoanAmountFirst : model.LoanAmount;
                    //Call qua Result Ekyc check lấy kết quả
                    if (String.IsNullOrEmpty(model.LoanBriefResidentModel.AddressNationalCard))
                    {
                        var ekyc = _resultEkycService.GetEkycByLoanBrief(access_token, model.LoanBriefId);
                        if (ekyc != null && !String.IsNullOrEmpty(ekyc.AddressValue))
                            model.LoanBriefResidentModel.AddressNationalCard = ekyc.AddressValue;
                    }

                    //Check xem trong bảng customer có số cmnd không
                    //nếu có mà đơn vay mới không có thì lấy số cmnd cũ
                    if (oldLoanBrief.Customer != null && !string.IsNullOrEmpty(oldLoanBrief.Customer.NationalCard)
                       && string.IsNullOrEmpty(model.NationalCard))
                        model.NationalCard = oldLoanBrief.Customer.NationalCard;

                    var entity = model.MapToLoanbrief();
                    entity.IsCheckBank = model.IsCheckBank;
                    entity.Frequency = 1;
                    //Check xem có thay đổi thông tin quan trọng không
                    // nếu có lưu lại vào comment
                    var note = await SaveCommentChangeInfomation(model, oldLoanBrief);

                    if (!string.IsNullOrEmpty(note))
                    {
                        var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = model.LoanBriefId,
                            Note = note,
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(task);
                    }
                    //Check rate
                    if (oldLoanBrief.RatePercent > 0 && oldLoanBrief.RateMoney > 0)
                    {
                        if (model.ProductId == EnumProductCredit.OtoCreditType_CC.GetHashCode())
                        {
                            var rate = Common.Utils.ProductPriceUtils.GetCalculatorRate(model.ProductId, model.LoanAmount, model.LoanTime, model.RateTypeId);
                            entity.RateMoney = rate.Item1;
                            entity.RatePercent = rate.Item2;
                        }
                        else
                        {
                            entity.RateMoney = oldLoanBrief.RateMoney;
                            entity.RatePercent = oldLoanBrief.RatePercent;
                        }
                    }
                    else
                    {
                        if (entity.ProductId > 0)
                        {
                            var rate = Common.Utils.ProductPriceUtils.GetCalculatorRate(entity.ProductId.Value, entity.LoanAmount, entity.LoanTime, entity.RateTypeId);
                            entity.RateMoney = rate.Item1;
                            entity.RatePercent = rate.Item2;
                        }
                    }

                    entity.FeePaymentBeforeLoan = Common.Utils.ProductPriceUtils.GetFeePaymentBeforeLoan(entity.ProductId.Value, entity.LoanTime);

                    //Kiểm tra có nhập mã GDDB
                    if (model.IsTransactionsSecured.GetValueOrDefault(false) && !String.IsNullOrEmpty(model.NoticeNumber))
                    {
                        //Lưu vào LogSecuredTransaction
                        var log = _dictionaryService.AddLogSecuredTransaction(user.Token, new LogSecuredTransaction
                        {
                            LoanBriefId = model.LoanBriefId,
                            CreatedAt = DateTime.Now,
                            NoticeNumber = model.NoticeNumber,
                            LoanAmount = model.LoanAmount,
                            Status = 1,
                            UserId = user.UserId
                        });
                    }

                    var resultUpdate = await _loanBriefService.UpdateLoanBriefV2(access_token, entity, model.LoanBriefId);
                    if (resultUpdate > 0)
                    {
                        //kiểm tra có thay đổi quận không
                        if (isChangePipeline)
                        {
                            _pipelineService.ChangePipeline(access_token, new ChangePipelineReq()
                            { loanBriefId = oldLoanBrief.LoanBriefId, productId = model.ProductId, status = oldLoanBrief.Status.Value });
                        }

                        var loanbriefHistory = new LoanBriefHistory()
                        {
                            LoanBriefId = model.LoanBriefId,
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ActorId = EnumActor.UpdateLoanbrief.GetHashCode(),
                            OldValue = JsonConvert.SerializeObject(oldLoanBrief.MapToLoanbriefViewModel(), Formatting.Indented, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            }),
                            NewValue = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            })
                        };
                        //Lưu log lịch sử chuyển 
                        taskRun.Add(Task.Run(() => _loanBriefService.AddLoanBriefHistory(access_token, loanbriefHistory)));
                        //Kiểm tra số tiền thay đổi => lưu comment
                        if (model.LoanAmount != oldLoanBrief.LoanAmount && model.LoanAmount > 0 && oldLoanBrief.LoanAmount > 0)
                        {
                            var t1 = Task.Run(() => _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                            {
                                LoanBriefId = resultUpdate,
                                Note = string.Format("Cập nhật: {0} thay đổi số tiền từ {1}đ thành {2}đ", user.FullName, oldLoanBrief.LoanAmount.Value.ToString("#,##"), model.LoanAmount.ToString("#,##")),
                                FullName = user.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = user.UserId,
                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            }));
                            taskRun.Add(t1);
                        }
                        //Thêm note
                        if (!string.IsNullOrEmpty(model.Comment))
                        {
                            var t2 = Task.Run(() => _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                            {
                                LoanBriefId = resultUpdate,
                                Note = model.Comment,
                                FullName = user.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = user.UserId,
                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            }));
                            taskRun.Add(t2);
                        }

                        //Log Action
                        var taskLogAction = Task.Run(() => _logActionService.AddLogAction(access_token, new LogLoanAction
                        {
                            LoanbriefId = entity.LoanBriefId,
                            ActionId = (int)EnumLogLoanAction.LoanUpdateScript,
                            TypeAction = (int)EnumTypeAction.Manual,
                            LoanStatus = oldLoanBrief.Status,
                            TlsLoanStatusDetail = oldLoanBrief.DetailStatusTelesales,
                            HubLoanStatusDetail = oldLoanBrief.LoanStatusDetailChild,
                            TelesaleId = oldLoanBrief.BoundTelesaleId,
                            HubId = oldLoanBrief.HubId,
                            HubEmployeeId = oldLoanBrief.HubEmployeeId,
                            CoordinatorUserId = oldLoanBrief.CoordinatorUserId,
                            UserActionId = user.UserId,
                            GroupUserActionId = user.GroupId,
                            OldValues = JsonConvert.SerializeObject(oldLoanBrief),
                            NewValues = JsonConvert.SerializeObject(entity),
                            CreatedAt = DateTime.Now
                        }));
                        taskRun.Add(taskLogAction);
                        //End
                        if (taskRun.Count > 0)
                            Task.WaitAll(taskRun.ToArray());
                        return Json(new { status = 1, message = string.Format("Cập nhật thành công đơn vay HĐ-{0}", resultUpdate) });
                    }
                    else
                        return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });
                }
                #endregion

                #region Thêm mới thông tin đơn vay
                else
                {
                    if (user != null)
                        model.CreateBy = user.UserId;
                    //nếu tạo đơn + user group telesales
                    if (model.LoanBriefId == 0 && user.GroupId == EnumGroupUser.Telesale.GetHashCode())
                    {
                        model.BoundTelesaleId = user.UserId;
                        model.TeamTelesaleId = user.TeamTelesalesId;
                        if (model.LoanBriefResidentModel != null)
                        {
                            if (model.LoanBriefResidentModel.ResidentType == (int)EnumTypeofownership.KT1)
                                model.LoanBriefQuestionScriptModel.QuestionAddressCoincideAreaSupport = true;
                            else
                                model.LoanBriefQuestionScriptModel.QuestionAddressCoincideAreaSupport = false;
                        }
                    }
                    else if (model.LoanBriefId == 0 && user.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        model.HubId = user.ShopGlobal;
                        model.IsHeadOffice = false;
                        //model.HubEmployeeId = user.UserId;
                    }
                    else if (model.LoanBriefId == 0 && user.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                    {
                        model.IsHeadOffice = false;
                        model.HubId = user.ShopGlobal;
                        model.HubEmployeeId = user.UserId;
                    }
                    if (model.FromDate == null)
                        model.FromDate = DateTime.Now;
                    if (model.LoanTime > 0)
                        model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);
                    var entity = model.MapToLoanbrief();
                    entity.IsCheckBank = model.IsCheckBank;
                    entity.UtmSource = user.Username;
                    entity.LoanAmountFirst = entity.LoanAmount;
                    entity.Frequency = 1;
                    entity.FeePaymentBeforeLoan = Common.Utils.ProductPriceUtils.GetFeePaymentBeforeLoan(entity.ProductId.Value, entity.LoanTime);
                    if (user.GroupId == EnumGroupUser.ManagerHub.GetHashCode() || user.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                    {
                        entity.LoanStatusDetail = (int)EnumLoanStatusDetailHub.CvkdDaLienHe;
                        entity.LoanStatusDetailChild = (int)EnumLoanStatusDetailHub.CvkdChoHoSoTaiCuaHang;
                        entity.FirstTimeHubFeedBack = DateTime.Now;
                        if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode())
                            entity.FirstTimeStaffHubFeedback = DateTime.Now;
                    }
                    var result = await _loanBriefService.InitLoanBriefV2(access_token, entity);
                    if (result > 0)
                    {
                        //Thêm note khởi tạo đơn vay
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = result,
                            Note = string.Format("{0}: Khởi tạo đơn vay", user.FullName),
                            FullName = user.FullName,
                            UserId = user.UserId,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        };
                        if (!string.IsNullOrEmpty(model.Comment))
                            note.Note += "<br />" + model.Comment;
                        _loanBriefService.AddLoanBriefNote(access_token, note);

                        //Log Action
                        await _logActionService.AddLogAction(access_token, new LogLoanAction
                        {
                            LoanbriefId = entity.LoanBriefId,
                            ActionId = (int)EnumLogLoanAction.CreateLoan,
                            TypeAction = (int)EnumTypeAction.Manual,
                            LoanStatus = entity.Status,
                            TelesaleId = entity.BoundTelesaleId,
                            HubId = model.HubId,
                            HubEmployeeId = entity.HubEmployeeId,
                            CoordinatorUserId = entity.CoordinatorUserId,
                            UserActionId = user.UserId,
                            GroupUserActionId = user.GroupId,
                            NewValues = JsonConvert.SerializeObject(entity),
                            CreatedAt = DateTime.Now
                        });
                        return Json(new { status = 1, message = string.Format("Tạo thành công đơn vay HĐ-{0}", result) });
                    }
                    else
                        return Json(new { status = 0, message = "Xảy ra lỗi khi tạo mới đơn vay. Vui lòng thử lại sau!" });
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Web LoanbriefV3/InitLoanBrief Exception");
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region Gửi yêu cầu AI
        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> RequestAIV3(LoanbriefScriptViewModel model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var messageValid = ValidLoanbriefScriptViewModel(model);
                if (!string.IsNullOrEmpty(messageValid))
                    return Json(new { status = 0, message = messageValid });

                var loanbrief = await _loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);
                if (loanbrief == null)
                    return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay!" });

                //Kiểm tra xem có tồn tại số CMND chưa
                var checkLoanProcessing = await _loanBriefService.GetLoanBriefByNationalCard(access_token, model.NationalCard, loanbrief.LoanBriefId);
                if (checkLoanProcessing != null && checkLoanProcessing.LoanBriefId > 0)
                    return Json(new { status = 0, message = "Số CMND có đơn đang xử lý" });

                var resultUpdate = await _loanBriefService.RequestAIV3(GetToken(), model.MapToLoanbriefRequestAI(), model.LoanBriefId);
                if (resultUpdate > 0)
                {
                    //Thêm yêu cầu lấy dữ liệu rule check
                    var ObjRequestRuleCheck = new LogLoanInfoAi()
                    {
                        LoanbriefId = model.LoanBriefId,
                        ServiceType = (int)ServiceTypeAI.RuleCheck,
                        CreatedAt = DateTime.Now,
                        IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest
                    };
                    _logReqestAiService.Create(access_token, ObjRequestRuleCheck);
                    //Thêm yêu cầu lấy dữ liệu location
                    if (_networkService.CheckHomeNetwok(model.Phone) == (int)HomeNetWorkMobile.Viettel
                        || _networkService.CheckHomeNetwok(model.Phone) == (int)HomeNetWorkMobile.Mobi)
                    {
                        var ObjRequestLocation = new LogLoanInfoAi()
                        {
                            LoanbriefId = model.LoanBriefId,
                            ServiceType = (int)ServiceTypeAI.Location,
                            CreatedAt = DateTime.Now,
                            IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest
                        };
                        _logReqestAiService.Create(access_token, ObjRequestLocation);
                    }
                    _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = string.Format("TELESALE {0} gửi yêu cầu lấy dữ liệu AI thành công", user.Username),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                    return Json(new { status = 1, message = "Gửi yêu cầu lấy dữ liệu AI thành công" });
                }
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });

            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region Gửi yêu cầu refphone
        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> RequestRefphoneV3(LoanbriefScriptViewModel model)
        {
            try
            {
                var objUpdateRefPhone = new UpdateRefPhoneLoanBriefRelationship();
                var user = GetUserGlobal();
                var access_token = user.Token;

                var ListLogRequestAi = _logReqestAiService.GetRequest(access_token, model.LoanBriefId);
                if (ListLogRequestAi.Count(x => x.ServiceType == (int)LOS.Common.Extensions.ServiceTypeAI.RefPhone) >= 3)
                    return Json(new { status = 0, message = "Bạn đã kiểm qua đủ 3 lần cho phép. Vui lòng thử lại sau!" });
                var loanbrief = await _loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);
                if (loanbrief == null)
                    return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay!" });
                if (ListLogRequestAi.Count(x => x.ServiceType == (int)LOS.Common.Extensions.ServiceTypeAI.RefPhone) > 0)
                {
                    if (model.LoanBriefRelationshipModels != null && model.LoanBriefRelationshipModels.Count > 0)
                    {
                        //Thay đổi cả 2 SĐT người thân
                        if (loanbrief.LoanBriefRelationship.ToList()[0].Phone != model.LoanBriefRelationshipModels.ToList()[0].Phone && loanbrief.LoanBriefRelationship.ToList()[1].Phone != model.LoanBriefRelationshipModels.ToList()[1].Phone)
                        {
                            objUpdateRefPhone.LoanBriefId = model.LoanBriefId;
                        }
                        if (loanbrief.LoanBriefRelationship.Count > 1)
                        {
                            //Thay đổi SĐT người thân 1
                            if (loanbrief.LoanBriefRelationship.ToList()[0].Phone != model.LoanBriefRelationshipModels.ToList()[0].Phone && loanbrief.LoanBriefRelationship.ToList()[1].Phone == model.LoanBriefRelationshipModels.ToList()[1].Phone)
                            {
                                objUpdateRefPhone.LoanBriefId = model.LoanBriefId;
                                objUpdateRefPhone.LoanBriefRelationshipId = loanbrief.LoanBriefRelationship.ToList()[0].Id;
                            }

                            //Thay đổi SĐT người thân 2
                            if (loanbrief.LoanBriefRelationship.ToList()[1].Phone != model.LoanBriefRelationshipModels.ToList()[1].Phone && loanbrief.LoanBriefRelationship.ToList()[0].Phone == model.LoanBriefRelationshipModels.ToList()[0].Phone)
                            {
                                objUpdateRefPhone.LoanBriefId = model.LoanBriefId;
                                objUpdateRefPhone.LoanBriefRelationshipId = loanbrief.LoanBriefRelationship.ToList()[1].Id;
                            }
                        }
                    }
                }

                var resultUpdate = await _loanBriefService.RequestRefphoneV3(GetToken(), model.MapToLoanbriefRefPhone(), model.LoanBriefId);
                if (resultUpdate > 0)
                {
                    var taskRun = new List<Task>();
                    //Nếu sdt thay đổi => objUpdateRefPhone.LoanBriefId > 0
                    if (loanbrief.LoanBriefRelationship != null && loanbrief.LoanBriefRelationship.Count > 0)
                    {
                        //update lại 2 trường RefPhoneCallRate, RefPhoneDurationRate trong bảng LoanBriefRelationship về null
                        var t1 = Task.Run(() => _loanBriefService.UpdateRefPhoneIsNullRequestAI(access_token, objUpdateRefPhone));
                        taskRun.Add(t1);
                    }
                    // Thêm yêu cầu lấy dữ liệu RefPhone
                    var ObjRequestRefPhone = new LogLoanInfoAi()
                    {
                        LoanbriefId = model.LoanBriefId,
                        ServiceType = (int)ServiceTypeAI.RefPhone,
                        CreatedAt = DateTime.Now,
                        IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest
                    };
                    var t2 = Task.Run(() => _logReqestAiService.Create(access_token, ObjRequestRefPhone));
                    taskRun.Add(t2);

                    var t3 = Task.Run(() => _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = string.Format("TELESALE {0} gửi yêu cầu lấy dữ liệu Refphone thành công", user.Username),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    }));
                    taskRun.Add(t3);
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    return Json(new { status = 1, message = "Gửi yêu cầu lấy dữ liệu Refphone thành công" });
                }
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });

            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region Lưu lại kịch bản
        [HttpPost]
        public async Task<IActionResult> ScriptUpdateLoanBriefV3(LoanbriefScriptViewModel model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var messageValid = ValidLoanbriefScriptViewModel(model);
                if (!string.IsNullOrEmpty(messageValid))
                    return Json(new { status = 0, message = messageValid });

                //mặc định thời gian vay là 12 tháng
                //model.LoanTime = 12;
                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                if (model.LoanTime > 0)
                    model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);

                bool isChangePipeline = false;

                #region Xử lý phần qlf
                if (model.ValueCheckQualify == null)
                    model.ValueCheckQualify = 0;
                if (model.LoanBriefQuestionScriptModel != null)
                {
                    var item = model.LoanBriefQuestionScriptModel;
                    // có nhu cầu vay
                    if (item.QuestionBorrow == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.NeedLoan;
                    }
                    //Có xe máy
                    if (item.QuestionUseMotobikeGo == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveMotobike;
                    }
                    //Có đăng ký xe bản gốc
                    if (item.QuestionMotobikeCertificate == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveOriginalVehicleRegistration;
                    }
                }
                //Trong độ tuổi cho vay (18 - 65 tuổi)
                if (!string.IsNullOrEmpty(model.sBirthDay))
                {
                    var Dob = DateTimeUtility.ConvertStringToDate(model.sBirthDay, "dd/MM/yyyy");
                    if (Dob != null)
                    {
                        var dateNowYear = DateTime.Now.Year;
                        var dobYear = Dob.Year;
                        var yearOld = dateNowYear - dobYear;
                        if (yearOld >= 18 && yearOld <= 65)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.InAge;
                        }
                    }

                }
                //Trong khu vực hỗ trợ HN-HCM
                if (model.DistrictId > 0)
                {
                    var district = _dictionaryService.GetDistrictById(GetToken(), model.DistrictId.Value);
                    if (district != null && district.IsApply == 1)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.InAreaSupport;
                    }
                }
                //upload chứng từ
                //var loanbriefFiles = _loanBriefService.GetLoanBriefFileByLoanId(GetToken(), model.LoanBriefId);
                //if (loanbriefFiles != null && loanbriefFiles.Count > 0)
                //{
                //    if (loanbriefFiles.Count(x => x.UserId == 0) >= 4)
                //        model.ValueCheckQualify += (int)EnumCheckQualify.UploadImage;
                //}

                #endregion

                //xử lý giấy tờ hình thức nhận lương
                if (model.LoanBriefJobModel != null && model.LoanBriefJobModel.ImcomeType > 0)
                {
                    if (model.LoanBriefJobModel.ImcomeType == (int)EnumImcomeType.ChuyenKhoan)
                        model.LoanBriefQuestionScriptModel.DocumetSalaryCash = null;
                    else if (model.LoanBriefJobModel.ImcomeType == (int)EnumImcomeType.TienMat)
                        model.LoanBriefQuestionScriptModel.DocumentSalaryBankTransfer = null;
                }

                //xử lý phần chủ hộ
                if (model.LoanBriefQuestionScriptModel != null && model.LoanBriefQuestionScriptModel.QuestionHouseOwner != null && model.LoanBriefQuestionScriptModel.QuestionHouseOwner == true)
                {
                    model.LoanBriefHouseholdModel.FullNameHouseOwner = null;
                    model.LoanBriefHouseholdModel.RelationshipHouseOwner = null;
                    model.LoanBriefHouseholdModel.BirdayHouseOwner = null;
                }


                //Hình thức sở hữu nhà
                //model.LoanBriefResidentModel.ResidentType = model.LoanBriefQuestionScriptModel.TypeOwnerShip;

                //var typeJob = 0;
                //if (model.LoanBriefJobModel != null && model.LoanBriefJobModel.JobId > 0)
                //{
                //    if (model.LoanBriefJobModel.JobId == (int)EnumJobTitle.LamTaiXeCongNghe)
                //        typeJob = (int)EnumJobTitle.LamTuDo;
                //    else
                //        typeJob = model.LoanBriefJobModel.JobId.Value;
                //}

                //đơn Tái cấu trúc nợ và đơn topup mặc định là không mua bảo hiểm
                if (model.TypeRemarketing == (int)EnumTypeRemarketing.DebtRevolvingLoan)
                {
                    model.LoanBriefPropertyModel.BuyInsuranceProperty = false;
                    model.BuyInsurenceCustomer = false;
                }
                else
                    model.BuyInsurenceCustomer = true;
                //Xe máy chính chủ
                //model.ProductId = model.OwnerProduct == 1 ? EnumProductCredit.MotorCreditType_CC.GetHashCode() : EnumProductCredit.MotorCreditType_KCC.GetHashCode();

                //Check đơn đang xử lý
                var loanbrief = _loanBriefService.GetLoanBriefProcessing(GetToken(), model.Phone, model.NationalCard, model.LoanBriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (model.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp || model.UtmSource == "form_rmkt_dpd")
                    {
                        if (loanbrief.Status != (int)EnumLoanStatus.DISBURSED)
                            return Json(new { status = 0, message = $"Khách hàng có đơn vay đang xử lý HĐ-{loanbrief.LoanBriefId}" });
                    }
                    else
                    {
                        return Json(new { status = 0, message = $"Khách hàng có đơn vay đang xử lý HĐ-{loanbrief.LoanBriefId}" });
                    }

                }
                //Lấy thông tin đơn vay cũ
                var oldLoanBrief = await _loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);
                if (oldLoanBrief == null)
                    return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay trong hệ thống" });

                //kiểm tra số CMND có trùng với đơn đang vay nào không
                if (model.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp
                    && model.NationalCard != oldLoanBrief.NationalCard && oldLoanBrief.ReMarketingLoanBriefId.HasValue)
                {
                    //check cmnd với loanbrief của đơn topup
                    var checkCMND = await _loanBriefService.GetLoanBriefByNationalCard(access_token, model.NationalCard, oldLoanBrief.ReMarketingLoanBriefId.Value);
                    if (checkCMND != null)
                        return Json(new { status = 0, message = "Khách hàng đang có hs trùng với hồ sơ đang vay HĐ:" + checkCMND.LoanBriefId });
                }

                var taskRun = new List<Task>();
                //Kiểm tra xem có thay đổi gói vay không
                if (model.ProductId > 0 && oldLoanBrief.ProductId > 0 && model.ProductId != oldLoanBrief.ProductId)
                {
                    if (_pipelineService.ChangePipeline(access_token, new ChangePipelineReq() { loanBriefId = oldLoanBrief.LoanBriefId, productId = model.ProductId, status = oldLoanBrief.Status.Value }))
                    {
                        var oldProductName = Enum.IsDefined(typeof(EnumProductCredit), oldLoanBrief.ProductId) ? ExtensionHelper.GetDescription((EnumProductCredit)oldLoanBrief.ProductId) : "";
                        var newProductName = Enum.IsDefined(typeof(EnumProductCredit), model.ProductId) ? ExtensionHelper.GetDescription((EnumProductCredit)model.ProductId) : "";
                        var task1 = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = model.LoanBriefId,
                            Note = string.Format("{0} thay đổi gói vay từ <b>{1}</b> thành <b>{2}</b>", user.FullName, oldProductName, newProductName),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(task1);
                    }
                }

                if (model.DistrictId > 0 && oldLoanBrief.DistrictId > 0 && model.DistrictId != oldLoanBrief.DistrictId)
                {
                    //Nếu là tls thay đổi =>cho thay đổi luồng
                    if (user.GroupId == (int)EnumGroupUser.Telesale || user.GroupId == (int)EnumGroupUser.ManagerTelesales)
                    {
                        isChangePipeline = true;
                    }
                }

                //kiểm tra có thay đổi thông tin xe hay không
                var oldProperty = await _loanBriefService.GetLoanbriefProperty(access_token, model.LoanBriefId);
                if ((oldProperty != null && oldProperty.BrandId > 0 && oldProperty.ProductId > 0)
                    || (model.LoanBriefPropertyModel.ProductId > 0 && (oldProperty == null || oldProperty.ProductId == 0 || !oldProperty.ProductId.HasValue))
                      || (oldLoanBrief.LoanBriefJob != null && oldLoanBrief.LoanBriefJob.JobId > 0 && oldLoanBrief.LoanBriefJob.JobId != model.LoanBriefJobModel.JobId)  //Nếu thay đổi công việc => thay đổi giới hạn cho vay
                    )
                {
                    if (oldProperty != null && oldProperty.ProductId > 0 && model.LoanBriefPropertyModel != null && model.LoanBriefPropertyModel.BrandId > 0
                        && model.LoanBriefPropertyModel.ProductId > 0 && model.LoanBriefPropertyModel.ProductId != oldProperty.ProductId)
                    {
                        var oldProduct = _productService.GetProduct(access_token, oldProperty.ProductId.Value);
                        var newProduct = _productService.GetProduct(access_token, model.LoanBriefPropertyModel.ProductId.Value);
                        var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                        {
                            LoanBriefId = model.LoanBriefId,
                            Note = string.Format("Kịch bản: {0} thay đổi xe máy {1} thành {2}", user.FullName, oldProduct.FullName, newProduct.FullName),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(task);
                        var priceAG = 0l;
                        decimal priceAI = 0l;
                        if (model.LoanBriefResidentModel.ResidentType.HasValue && model.LoanBriefQuestionScriptModel != null && model.LoanBriefQuestionScriptModel.MaxPrice.HasValue)
                        {
                            model.LoanAmountExpertiseLast = GetMaxPriceProductV3((int)newProduct.Id, model.LoanBriefId, (long)model.LoanBriefQuestionScriptModel.MaxPrice, model.ProductId, ref priceAG, ref priceAI);
                            model.LoanAmountExpertise = priceAG;
                            if (priceAI > 0)
                                model.LoanAmountExpertiseAi = priceAI;
                            if (model.LoanAmountExpertiseLast > 0)
                            {
                                var task1 = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                                {
                                    LoanBriefId = model.LoanBriefId,
                                    Note = string.Format("Kịch bản: Xe của khách hàng được định giá tối đa {0}đ", model.LoanAmountExpertiseLast.Value.ToString("##,#")),
                                    FullName = user.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = user.UserId,
                                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                }));
                                taskRun.Add(task1);
                            }

                            // Lưu log sang AI
                            if (priceAG > 0 && priceAI > 0)
                            {
                                var productAiLogModel = _productService.PrepareProductAiLogModel(user.Token, model.LoanBriefPropertyModel.ProductId.Value);
                                if (productAiLogModel != null)
                                {
                                    productAiLogModel.AI_price = (long)priceAI;
                                    productAiLogModel.IT_price = (long)priceAG;
                                    productAiLogModel.loan_credit_id = model.LoanBriefId;
                                    var taskAI = Task.Run(() => _priceMotorService.PushLogPriceAIMoto(productAiLogModel, GetToken()));
                                    taskRun.Add(taskAI);
                                }
                            }
                        }

                    }
                    else
                    {
                        if (model.LoanBriefPropertyModel.ProductId > 0)
                        {
                            model.LoanAmountExpertiseLast = oldLoanBrief.LoanAmountExpertiseLast;
                            model.LoanAmountExpertise = oldLoanBrief.LoanAmountExpertise;
                            model.LoanAmountExpertiseAi = oldLoanBrief.LoanAmountExpertiseAi;
                            if (model.LoanAmountExpertiseLast == 0 || !model.LoanAmountExpertiseLast.HasValue
                                || (model.LoanAmountExpertiseAi <= 0 && model.LoanAmountExpertise <= 0))
                            {
                                var newProduct = _productService.GetProduct(access_token, model.LoanBriefPropertyModel.ProductId.Value);
                                var priceAG = 0l;
                                decimal priceAI = 0l;
                                if (model.LoanBriefResidentModel.ResidentType.HasValue && model.LoanBriefQuestionScriptModel != null && model.LoanBriefQuestionScriptModel.MaxPrice.HasValue)
                                {
                                    model.LoanAmountExpertiseLast = GetMaxPriceProductV3((int)newProduct.Id, model.LoanBriefId, (long)model.LoanBriefQuestionScriptModel.MaxPrice, model.ProductId, ref priceAG, ref priceAI);
                                    model.LoanAmountExpertise = priceAG;
                                    if (priceAI > 0)
                                        model.LoanAmountExpertiseAi = priceAI;
                                    if (model.LoanAmountExpertiseLast > 0)
                                    {
                                        var task1 = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                                        {
                                            LoanBriefId = model.LoanBriefId,
                                            Note = string.Format("Kịch bản: Xe của khách hàng được định giá tối đa {0}đ", model.LoanAmountExpertiseLast.Value.ToString("##,#")),
                                            FullName = user.FullName,
                                            Status = 1,
                                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                            CreatedTime = DateTime.Now,
                                            UserId = user.UserId,
                                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                        }));
                                        taskRun.Add(task1);
                                    }

                                    // Lưu log sang AI
                                    if (priceAG > 0 && priceAI > 0)
                                    {
                                        var productAiLogModel = _productService.PrepareProductAiLogModel(user.Token, model.LoanBriefPropertyModel.ProductId.Value);
                                        if (productAiLogModel != null)
                                        {
                                            productAiLogModel.AI_price = (long)priceAI;
                                            productAiLogModel.IT_price = (long)priceAG;
                                            productAiLogModel.loan_credit_id = model.LoanBriefId;
                                            var taskAI = Task.Run(() => _priceMotorService.PushLogPriceAIMoto(productAiLogModel, GetToken()));
                                            taskRun.Add(taskAI);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                model.LoanAmountFirst = oldLoanBrief.LoanAmountFirst > 0 ? oldLoanBrief.LoanAmountFirst : model.LoanAmount;

                //Check xem có thay đổi thông tin quan trọng không
                // nếu có lưu lại vào comment
                var note = await SaveCommentChangeInfomationScriptView(model);

                if (!string.IsNullOrEmpty(note))
                {
                    var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = note,
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    }));
                    taskRun.Add(task);
                }

                //kiểm tra xem có trường FirstProcessingTime
                //chưa có thì gán thành ngày hiện tại
                //có rồi thì gán lại
                if (!oldLoanBrief.FirstProcessingTime.HasValue)
                    model.FirstProcessingTime = DateTime.Now;
                else
                    model.FirstProcessingTime = oldLoanBrief.FirstProcessingTime;

                model.LoanBriefPropertyModel.BuyInsuranceProperty = false;

                //Check xem trong bảng customer có số cmnd không
                //nếu có mà đơn vay mới không có thì lấy số cmnd cũ
                if (oldLoanBrief.Customer != null && !string.IsNullOrEmpty(oldLoanBrief.Customer.NationalCard)
                   && string.IsNullOrEmpty(model.NationalCard))
                    model.NationalCard = oldLoanBrief.Customer.NationalCard;

                //xử lý chuẩn hóa tên KH
                if (!string.IsNullOrEmpty(model.FullName))
                {
                    if (!model.FullName.IsNormalized())
                        model.FullName = model.FullName.Normalize();//xử lý đưa về bộ unicode utf8
                }
                var modelUpdate = model.MapToLoanbriefV2();
                //Check rate
                if (oldLoanBrief.RatePercent > 0 && oldLoanBrief.RateMoney > 0)
                {
                    if (model.ProductId == EnumProductCredit.OtoCreditType_CC.GetHashCode())
                    {
                        var rate = Common.Utils.ProductPriceUtils.GetCalculatorRate(model.ProductId, model.LoanAmount, model.LoanTime, model.RateTypeId);
                        modelUpdate.RateMoney = rate.Item1;
                        modelUpdate.RatePercent = rate.Item2;
                    }
                    else
                    {
                        modelUpdate.RateMoney = oldLoanBrief.RateMoney;
                        modelUpdate.RatePercent = oldLoanBrief.RatePercent;
                    }

                }
                else
                {
                    if (model.ProductId > 0)
                    {
                        var rate = Common.Utils.ProductPriceUtils.GetCalculatorRate(model.ProductId, model.LoanAmount, model.LoanTime, model.RateTypeId);
                        modelUpdate.RateMoney = rate.Item1;
                        modelUpdate.RatePercent = rate.Item2;
                    }
                }
                modelUpdate.Frequency = 1;
                modelUpdate.FeePaymentBeforeLoan = Common.Utils.ProductPriceUtils.GetFeePaymentBeforeLoan(modelUpdate.ProductId.Value, modelUpdate.LoanTime);
                var resultUpdate = await _loanBriefService.UpdateScriptTelesaleV2(GetToken(), modelUpdate, model.LoanBriefId);
                if (resultUpdate > 0)
                {
                    //kiểm tra có thay đổi quận không
                    if (isChangePipeline)
                    {
                        _pipelineService.ChangePipeline(access_token, new ChangePipelineReq()
                        { loanBriefId = oldLoanBrief.LoanBriefId, productId = model.ProductId, status = oldLoanBrief.Status.Value });
                    }
                    var loanbriefHistory = new LoanBriefHistory()
                    {
                        LoanBriefId = model.LoanBriefId,
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ActorId = EnumActor.UpdateScript.GetHashCode(),
                        OldValue = JsonConvert.SerializeObject(oldLoanBrief.MapToLoanbriefViewModelV2(), Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        }),
                        NewValue = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        })
                    };
                    //Lưu log lịch sử chuyển 
                    taskRun.Add(Task.Run(() => _loanBriefService.AddLoanBriefHistory(access_token, loanbriefHistory)));
                    //Kiểm tra số tiền thay đổi => lưu comment
                    if (model.LoanAmount != oldLoanBrief.LoanAmount && model.LoanAmount > 0 && oldLoanBrief.LoanAmount > 0)
                    {
                        var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = resultUpdate,
                            Note = string.Format("Kịch bản: {0} thay đổi số tiền từ {1}đ thành {2}đ", user.FullName, oldLoanBrief.LoanAmount.Value.ToString("#,##"), model.LoanAmount.ToString("#,##")),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(task);
                    }
                    //Thêm note
                    if (!string.IsNullOrEmpty(model.Comment))
                    {
                        var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = resultUpdate,
                            Note = model.Comment,
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(task);
                    }

                    //Log Action
                    var taskLogAction = Task.Run(() => _logActionService.AddLogAction(access_token, new LogLoanAction
                    {
                        LoanbriefId = resultUpdate,
                        ActionId = (int)EnumLogLoanAction.LoanEdit,
                        TypeAction = (int)EnumTypeAction.Manual,
                        LoanStatus = oldLoanBrief.Status,
                        TlsLoanStatusDetail = oldLoanBrief.DetailStatusTelesales,
                        HubLoanStatusDetail = oldLoanBrief.LoanStatusDetailChild,
                        TelesaleId = oldLoanBrief.BoundTelesaleId,
                        HubId = oldLoanBrief.HubId,
                        HubEmployeeId = oldLoanBrief.HubEmployeeId,
                        CoordinatorUserId = oldLoanBrief.CoordinatorUserId,
                        UserActionId = user.UserId,
                        GroupUserActionId = user.GroupId,
                        OldValues = JsonConvert.SerializeObject(oldLoanBrief),
                        NewValues = JsonConvert.SerializeObject(modelUpdate),
                        CreatedAt = DateTime.Now
                    }));
                    taskRun.Add(taskLogAction);

                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    var msg = string.Format("Cập nhật thành công đơn vay HĐ-{0}", resultUpdate);

                    return Json(new { status = 1, message = msg });
                }
                else
                {
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3Web/UpdateScript PUT Exception");
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> ScriptUpdateLoanBriefCar(LoanbriefScriptViewModel model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var messageValid = ValidLoanbriefScriptViewModel(model);
                if (!string.IsNullOrEmpty(messageValid))
                    return Json(new { status = 0, message = messageValid });
                //mặc định thời gian vay là 12 tháng
                //model.LoanTime = 12;
                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                if (model.LoanTime > 0)
                    model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);

                bool isChangePipeline = false;

                #region Xử lý phần qlf
                if (model.ValueCheckQualify == null)
                    model.ValueCheckQualify = 0;
                if (model.LoanBriefQuestionScriptModel != null)
                {
                    var item = model.LoanBriefQuestionScriptModel;
                    // có nhu cầu vay
                    if (item.QuestionBorrow == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.NeedLoan;
                    }
                    //Có ô tô
                    if (item.QuestionUseMotobikeGo == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveMotobike;
                    }
                    //Có đăng ký xe bản gốc
                    if (item.QuestionMotobikeCertificate == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveOriginalVehicleRegistration;
                    }
                }
                //Trong độ tuổi cho vay (18 - 65 tuổi)
                if (!string.IsNullOrEmpty(model.sBirthDay))
                {
                    var Dob = DateTimeUtility.ConvertStringToDate(model.sBirthDay, "dd/MM/yyyy");
                    if (Dob != null)
                    {
                        var dateNowYear = DateTime.Now.Year;
                        var dobYear = Dob.Year;
                        var yearOld = dateNowYear - dobYear;
                        if (yearOld >= 18 && yearOld <= 65)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.InAge;
                        }
                    }

                }
                //Trong khu vực hỗ trợ HN-HCM
                if (model.DistrictId > 0)
                {
                    var district = _dictionaryService.GetDistrictById(GetToken(), model.DistrictId.Value);
                    if (district != null && district.IsApply == 1)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.InAreaSupport;
                    }
                }
                //upload chứng từ
                //var loanbriefFiles = _loanBriefService.GetLoanBriefFileByLoanId(GetToken(), model.LoanBriefId);
                //if (loanbriefFiles != null && loanbriefFiles.Count > 0)
                //{
                //    if (loanbriefFiles.Count(x => x.UserId == 0) >= 4)
                //        model.ValueCheckQualify += (int)EnumCheckQualify.UploadImage;
                //}

                #endregion

                //mặc định mua bảo hiểm
                model.BuyInsurenceCustomer = true;

                ////Check đơn đang xử lý
                //var loanbrief = await _loanBriefService.GetLoanBriefBorrowing(access_token, model.Phone, model.NationalCard);
                //if (loanbrief != null && loanbrief.LoanBriefId > 0 && model.LoanBriefId != loanbrief.LoanBriefId)
                //    return Json(new { status = 0, message = $"Khách hàng có đơn vay đang xử lý. Vui lòng tạo đơn con từ HĐ-{loanbrief.LoanBriefId}" });

                //Lấy thông tin đơn vay cũ
                var oldLoanBrief = await _loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);
                if (oldLoanBrief == null)
                    return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay trong hệ thống" });
                var taskRun = new List<Task>();

                if (model.DistrictId > 0 && oldLoanBrief.DistrictId > 0 && model.DistrictId != oldLoanBrief.DistrictId)
                {
                    //Nếu là tls thay đổi =>cho thay đổi luồng
                    if (user.GroupId == (int)EnumGroupUser.Telesale || user.GroupId == (int)EnumGroupUser.ManagerTelesales)
                    {
                        isChangePipeline = true;
                    }
                }

                model.LoanAmountFirst = oldLoanBrief.LoanAmountFirst > 0 ? oldLoanBrief.LoanAmountFirst : model.LoanAmount;

                //Check xem có thay đổi thông tin quan trọng không
                // nếu có lưu lại vào comment
                var note = await SaveCommentChangeInfomationScriptView(model);

                if (!string.IsNullOrEmpty(note))
                {
                    var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = note,
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    }));
                    taskRun.Add(task);
                }

                //kiểm tra xem có trường FirstProcessingTime
                //chưa có thì gán thành ngày hiện tại
                //có rồi thì gán lại
                if (!oldLoanBrief.FirstProcessingTime.HasValue)
                    model.FirstProcessingTime = DateTime.Now;
                else
                    model.FirstProcessingTime = oldLoanBrief.FirstProcessingTime;

                model.LoanBriefPropertyModel.BuyInsuranceProperty = false;

                //Check xem trong bảng customer có số cmnd không
                //nếu có mà đơn vay mới không có thì lấy số cmnd cũ
                if (oldLoanBrief.Customer != null && !string.IsNullOrEmpty(oldLoanBrief.Customer.NationalCard)
                   && string.IsNullOrEmpty(model.NationalCard))
                    model.NationalCard = oldLoanBrief.Customer.NationalCard;


                var modelUpdate = model.MapToLoanbriefV2();
                //Check rate
                if (oldLoanBrief.RatePercent > 0 && oldLoanBrief.RateMoney > 0)
                {
                    if (model.ProductId == EnumProductCredit.OtoCreditType_CC.GetHashCode())
                    {
                        var rate = Common.Utils.ProductPriceUtils.GetCalculatorRate(model.ProductId, model.LoanAmount, model.LoanTime, model.RateTypeId);
                        modelUpdate.RateMoney = rate.Item1;
                        modelUpdate.RatePercent = rate.Item2;
                    }
                    else
                    {
                        modelUpdate.RateMoney = oldLoanBrief.RateMoney;
                        modelUpdate.RatePercent = oldLoanBrief.RatePercent;
                    }

                }
                else
                {
                    if (model.ProductId > 0)
                    {
                        var rate = Common.Utils.ProductPriceUtils.GetCalculatorRate(model.ProductId, model.LoanAmount, model.LoanTime, model.RateTypeId);
                        modelUpdate.RateMoney = rate.Item1;
                        modelUpdate.RatePercent = rate.Item2;
                    }
                }
                modelUpdate.Frequency = 1;
                modelUpdate.FeePaymentBeforeLoan = Common.Utils.ProductPriceUtils.GetFeePaymentBeforeLoan(modelUpdate.ProductId.Value, modelUpdate.LoanTime);
                var resultUpdate = await _loanBriefService.UpdateScriptTelesaleV2(GetToken(), modelUpdate, model.LoanBriefId);
                if (resultUpdate > 0)
                {
                    //kiểm tra có thay đổi quận không
                    if (isChangePipeline)
                    {
                        _pipelineService.ChangePipeline(access_token, new ChangePipelineReq()
                        { loanBriefId = oldLoanBrief.LoanBriefId, productId = model.ProductId, status = oldLoanBrief.Status.Value });
                    }
                    var loanbriefHistory = new LoanBriefHistory()
                    {
                        LoanBriefId = model.LoanBriefId,
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ActorId = EnumActor.UpdateScript.GetHashCode(),
                        OldValue = JsonConvert.SerializeObject(oldLoanBrief.MapToLoanbriefViewModelV2(), Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        }),
                        NewValue = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        })
                    };
                    //Lưu log lịch sử chuyển 
                    taskRun.Add(Task.Run(() => _loanBriefService.AddLoanBriefHistory(access_token, loanbriefHistory)));
                    //Kiểm tra số tiền thay đổi => lưu comment
                    if (model.LoanAmount != oldLoanBrief.LoanAmount && model.LoanAmount > 0 && oldLoanBrief.LoanAmount > 0)
                    {
                        var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = resultUpdate,
                            Note = string.Format("Kịch bản: {0} thay đổi số tiền từ {1}đ thành {2}đ", user.FullName, oldLoanBrief.LoanAmount.Value.ToString("#,##"), model.LoanAmount.ToString("#,##")),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(task);
                    }
                    //Thêm note
                    if (!string.IsNullOrEmpty(model.Comment))
                    {
                        var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = resultUpdate,
                            Note = model.Comment,
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(task);
                    }

                    //Log Action
                    var taskLogAction = Task.Run(() => _logActionService.AddLogAction(access_token, new LogLoanAction
                    {
                        LoanbriefId = resultUpdate,
                        ActionId = (int)EnumLogLoanAction.LoanEdit,
                        TypeAction = (int)EnumTypeAction.Manual,
                        LoanStatus = oldLoanBrief.Status,
                        TlsLoanStatusDetail = oldLoanBrief.DetailStatusTelesales,
                        HubLoanStatusDetail = oldLoanBrief.LoanStatusDetailChild,
                        TelesaleId = oldLoanBrief.BoundTelesaleId,
                        HubId = oldLoanBrief.HubId,
                        HubEmployeeId = oldLoanBrief.HubEmployeeId,
                        CoordinatorUserId = oldLoanBrief.CoordinatorUserId,
                        UserActionId = user.UserId,
                        GroupUserActionId = user.GroupId,
                        OldValues = JsonConvert.SerializeObject(oldLoanBrief),
                        NewValues = JsonConvert.SerializeObject(modelUpdate),
                        CreatedAt = DateTime.Now
                    }));
                    taskRun.Add(taskLogAction);

                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    var msg = string.Format("Cập nhật thành công đơn vay HĐ-{0}", resultUpdate);

                    return Json(new { status = 1, message = msg });
                }
                else
                {
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3Web/UpdateScript PUT Exception");
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region Tạo mới đơn vay trên KB
        [HttpPost]
        public async Task<IActionResult> CreateLoanBriefScriptV3(LoanbriefScriptViewModel model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var messageValid = ValidLoanbriefScriptViewModel(model);
                if (!string.IsNullOrEmpty(messageValid))
                    return Json(new { status = 0, message = messageValid });

                //mặc định thời gian vay là 12 tháng
                //model.LoanTime = 12;
                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                if (model.LoanTime > 0)
                    model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);

                #region Xử lý phần qlf
                if (model.ValueCheckQualify == null)
                    model.ValueCheckQualify = 0;
                if (model.LoanBriefQuestionScriptModel != null)
                {
                    var item = model.LoanBriefQuestionScriptModel;
                    // có nhu cầu vay
                    if (item.QuestionBorrow == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.NeedLoan;
                    }
                    //Có xe máy
                    if (item.QuestionUseMotobikeGo == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveMotobike;
                    }
                    //Có đăng ký xe bản gốc
                    if (item.QuestionMotobikeCertificate == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveOriginalVehicleRegistration;
                    }
                }
                //Trong độ tuổi cho vay (18 - 65 tuổi)
                if (!string.IsNullOrEmpty(model.sBirthDay))
                {
                    var Dob = DateTimeUtility.ConvertStringToDate(model.sBirthDay, "dd/MM/yyyy");
                    if (Dob != null)
                    {
                        var dateNowYear = DateTime.Now.Year;
                        var dobYear = Dob.Year;
                        var yearOld = dateNowYear - dobYear;
                        if (yearOld >= 18 && yearOld <= 65)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.InAge;
                        }
                    }

                }
                //Trong khu vực hỗ trợ HN-HCM
                if (model.DistrictId > 0)
                {
                    var district = _dictionaryService.GetDistrictById(GetToken(), model.DistrictId.Value);
                    if (district != null && district.IsApply == 1)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.InAreaSupport;
                    }
                }
                //upload chứng từ
                var loanbriefFiles = _loanBriefService.GetLoanBriefFileByLoanId(GetToken(), model.LoanBriefId);
                if (loanbriefFiles != null && loanbriefFiles.Count > 0)
                {
                    if (loanbriefFiles.Count(x => x.UserId == 0) >= 4)
                        model.ValueCheckQualify += (int)EnumCheckQualify.UploadImage;
                }

                #endregion

                //JobId
                //model.LoanBriefJobModel.JobId = model.LoanBriefQuestionScriptModel.TypeJob;

                ////Hình thức sở hữu nhà
                //model.LoanBriefResidentModel.ResidentType = model.LoanBriefQuestionScriptModel.TypeOwnerShip;

                ////xử lý TypeJob
                //var typeJob = model.LoanBriefJobModel.JobId.Value;

                //Xe máy chính chủ
                //model.ProductId = model.OwnerProduct == 1 ? EnumProductCredit.MotorCreditType_CC.GetHashCode() : EnumProductCredit.MotorCreditType_KCC.GetHashCode();

                //Check đơn đang xử lý
                var listLoanBrief = _loanBriefService.GetLoanBriefProcessing(GetToken(), model.Phone, model.NationalCard, model.LoanBriefId);
                if (listLoanBrief != null && listLoanBrief.LoanBriefId > 0)
                    return Json(GetBaseObjectResult(false, $"Khách hàng có đơn vay đang xử lý HĐ-{listLoanBrief.LoanBriefId}"));
                //Lấy thông tin đơn vay cũ
                var taskRun = new List<Task>();



                //định giá tài sản kH
                if (model.LoanBriefPropertyModel.ProductId.HasValue && model.LoanBriefResidentModel.ResidentType.HasValue)
                {
                    if (model.LoanBriefResidentModel.ResidentType.HasValue && model.LoanBriefQuestionScriptModel != null && model.LoanBriefQuestionScriptModel.MaxPrice.HasValue)
                    {
                        var newProduct = _productService.GetProduct(access_token, model.LoanBriefPropertyModel.ProductId.Value);
                        var priceAG = 0l;
                        decimal priceAI = 0l;
                        model.LoanAmountExpertiseLast = GetMaxPriceProductV3((int)newProduct.Id, model.LoanBriefId, (long)model.LoanBriefQuestionScriptModel.MaxPrice, model.ProductId, ref priceAG, ref priceAI);
                        model.LoanAmountExpertise = priceAG;
                        if (priceAI > 0)
                            model.LoanAmountExpertiseAi = priceAI;
                        if (model.LoanAmountExpertiseLast > 0)
                        {
                            var task1 = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                            {
                                LoanBriefId = model.LoanBriefId,
                                Note = string.Format("Kịch bản: Xe của khách hàng được định giá tối đa {0}đ", model.LoanAmountExpertiseLast.Value.ToString("##,#")),
                                FullName = user.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = user.UserId,
                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            }));
                            taskRun.Add(task1);
                        }

                        // Lưu log sang AI
                        if (priceAG > 0 && priceAI > 0)
                        {
                            var productAiLogModel = _productService.PrepareProductAiLogModel(user.Token, model.LoanBriefPropertyModel.ProductId.Value);
                            if (productAiLogModel != null)
                            {
                                productAiLogModel.AI_price = (long)priceAI;
                                productAiLogModel.IT_price = (long)priceAG;
                                productAiLogModel.loan_credit_id = model.LoanBriefId;
                                var taskAI = Task.Run(() => _priceMotorService.PushLogPriceAIMoto(productAiLogModel, GetToken()));
                                taskRun.Add(taskAI);
                            }
                        }
                    }
                    //xử lý TypeJobDescription
                    //var typeJobDescription = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(typeJob, model.LoanBriefJobModel.ImcomeType, model.LoanBriefJobModel.CompanyInsurance, model.LoanBriefJobModel.BusinessPapers);
                    //if (typeJobDescription > 0)
                    //{
                    //    var newProduct = _productService.GetProduct(access_token, model.LoanBriefPropertyModel.ProductId.Value);
                    //    var priceAG = 0l;
                    //    decimal priceAI = 0l;

                    //    model.LoanAmountExpertiseLast = GetMaxPriceProductV2((int)newProduct.Id, (int)model.LoanBriefResidentModel.ResidentType, (int)model.ProductId, model.LoanBriefId, typeJob, typeJobDescription, ref priceAG, ref priceAI);
                    //    model.LoanAmountExpertise = priceAG;
                    //    if (priceAI > 0)
                    //        model.LoanAmountExpertiseAi = priceAI;
                    //    if (model.LoanAmountExpertiseLast > 0)
                    //    {
                    //        var task1 = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                    //        {
                    //            LoanBriefId = model.LoanBriefId,
                    //            Note = string.Format("Kịch bản: Xe của khách hàng được định giá tối đa {0}đ", model.LoanAmountExpertiseLast.Value.ToString("##,#")),
                    //            FullName = user.FullName,
                    //            Status = 1,
                    //            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    //            CreatedTime = DateTime.Now,
                    //            UserId = user.UserId,
                    //            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                    //            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    //        }));
                    //        taskRun.Add(task1);
                    //    }
                    //}
                }
                model.TypeLoanBrief = LOS.Common.Extensions.LoanBriefType.Customer.GetHashCode();
                var entity = model.MapToLoanbriefV2();
                entity.ReMarketingLoanBriefId = model.ReMarketingLoanBriefId;
                entity.IsHeadOffice = true;
                entity.BoundTelesaleId = user.UserId;
                entity.CreateBy = user.UserId;
                entity.BuyInsurenceCustomer = true;
                entity.UtmSource = !string.IsNullOrEmpty(model.UtmSource) ? model.UtmSource : "rmkt_telesale_autocall";
                entity.TeamTelesalesId = user.TeamTelesalesId;
                entity.PlatformType = (int)EnumPlatformType.Hotline;
                if (entity.ProductId == null)
                    entity.ProductId = (int)EnumProductCredit.MotorCreditType_CC;

                var resultUpdate = await _loanBriefService.CreateLoanbriefScriptV2(GetToken(), entity);
                if (resultUpdate > 0)
                {
                    //Thêm note
                    var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                    {
                        LoanBriefId = resultUpdate,
                        Note = entity.ReMarketingLoanBriefId > 0 ? "Kịch bản: Đơn vay được khởi tạo lại từ HĐ-" + entity.ReMarketingLoanBriefId : "Kịch bản: Khởi tạo đơn vay",
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    }));
                    taskRun.Add(task);
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    var msg = string.Format("Tạo mới đơn vay thành công HĐ-{0}", resultUpdate);

                    return Json(new { status = 1, message = msg, data = resultUpdate });
                }
                else
                {
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    return Json(new { status = 0, message = "Xảy ra lỗi khi tạo mới đơn vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region Định giá xe
        public IActionResult GetMaxPriceProductScriptV2(int productId, int loanBriefId, long maxPrice, int productCredit)
        {
            var priceAG = 0l;
            decimal priceAI = 0l;
            var price = GetMaxPriceProductV3(productId, loanBriefId, maxPrice, productCredit, ref priceAG, ref priceAI);
            return Json(GetBaseObjectResult(true, "Success", price));
        }
        private decimal GetMaxPriceProductV3(int productId, int loanBriefId, long maxPrice, int productCredit, ref long priceAG, ref decimal priceAi)
        {
            var maxPriceProduct = 0L;
            long originalCarPrice = 0;
            var access_token = GetToken();
            // lấy giá bên AI
            decimal priceCarAI = priceAi = _productService.GetPriceAI(access_token, productId, loanBriefId);
            if (priceCarAI > 0)
                originalCarPrice = (long)priceCarAI;
            else
                priceAG = 0;

            var typeRemarketingLtv = 0;
            var loanbrief = _loanBriefService.GetBasicInfo(access_token, loanBriefId).Result;
            if (loanbrief.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                typeRemarketingLtv = (int)TypeRemarketingLtv.TopUp;
            if (loanbrief.IsReborrow.GetValueOrDefault(false))
                typeRemarketingLtv = (int)TypeRemarketingLtv.Reborrow;
            maxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPriceV3(productCredit, originalCarPrice, 0, 0, maxPrice, 0, typeRemarketingLtv);
            return maxPriceProduct;
        }

        private decimal GetMaxPriceProductTopup(int productId, int loanBriefId, int productCredit, int typeOfOwnerShip, long totalMoneyDebtCurrent, ref decimal priceAi)
        {
            long originalCarPrice = 0;
            var access_token = GetToken();
            // lấy giá bên AI
            decimal priceCarAI = priceAi = _productService.GetPriceAI(access_token, productId, loanBriefId);
            if (priceCarAI > 0)
                originalCarPrice = (long)priceCarAI;
            return Common.Utils.ProductPriceUtils.GetMaxPriceTopup(productCredit, originalCarPrice, totalMoneyDebtCurrent, typeOfOwnerShip);

        }
        #endregion

        #region Lấy danh sách Job theo ParentId
        [HttpGet]
        public ActionResult GetJobByParentId(int ParentId)
        {
            var lsData = _dictionaryService.GetJobByParentId(GetToken(), ParentId);
            return Json(new { data = lsData });
        }
        #endregion

        #region Check mã hóa đơn điện và hóa đơn nước
        [HttpGet]
        public IActionResult CallAICheckElectricity(int LoanbriefId, string ElectricityId)
        {
            //check hóa đơn điện
            var resultElectricity = _checkElectricityAndWater.CheckBillElectricityAI(GetToken(), LoanbriefId, ElectricityId);
            return View(resultElectricity);
        }

        public IActionResult CallAICheckWater(int LoanbriefId, string WaterId, string WaterSupplier)
        {
            //check hóa đơn nước
            var resultWater = _checkElectricityAndWater.CheckBillWaterAI(GetToken(), LoanbriefId, WaterId, WaterSupplier);
            return View(resultWater);
        }
        #endregion

        #region Hủy đơn lưu lại thông tin kịch bản
        [HttpPost]
        [PermissionFilter]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> ScriptSaveInformationLoanBriefV3(LoanbriefScriptViewModel model)
        {
            try
            {
                var user = GetUserGlobal();

                //mặc định thời gian vay là 12 tháng
                //model.LoanTime = 12;
                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                if (model.LoanTime > 0)
                    model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);

                #region Xử lý phần qlf
                if (model.ValueCheckQualify == null)
                    model.ValueCheckQualify = 0;
                if (model.LoanBriefQuestionScriptModel != null)
                {
                    var item = model.LoanBriefQuestionScriptModel;
                    // có nhu cầu vay
                    if (item.QuestionBorrow == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.NeedLoan;
                    }
                    //Có xe máy
                    if (item.QuestionUseMotobikeGo == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveMotobike;
                    }
                    //Có đăng ký xe bản gốc
                    if (item.QuestionMotobikeCertificate == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveOriginalVehicleRegistration;
                    }
                }
                //Trong độ tuổi cho vay (18 - 65 tuổi)
                if (!string.IsNullOrEmpty(model.sBirthDay))
                {
                    var Dob = DateTimeUtility.ConvertStringToDate(model.sBirthDay, "dd/MM/yyyy");
                    if (Dob != null)
                    {
                        var dateNowYear = DateTime.Now.Year;
                        var dobYear = Dob.Year;
                        var yearOld = dateNowYear - dobYear;
                        if (yearOld >= 18 && yearOld <= 65)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.InAge;
                        }
                    }

                }
                //Trong khu vực hỗ trợ HN-HCM
                if (model.DistrictId > 0)
                {
                    var district = _dictionaryService.GetDistrictById(GetToken(), model.DistrictId.Value);
                    if (district != null && district.IsApply == 1)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.InAreaSupport;
                    }
                }
                //upload chứng từ
                //var loanbriefFiles = _loanBriefService.GetLoanBriefFileByLoanId(GetToken(), model.LoanBriefId);
                //if (loanbriefFiles != null && loanbriefFiles.Count > 0)
                //{
                //    if (loanbriefFiles.Count(x => x.UserId == 0) >= 4)
                //        model.ValueCheckQualify += (int)EnumCheckQualify.UploadImage;
                //}

                #endregion

                var loanbrief = await _loanBriefService.GetLoanBriefById(user.Token, model.LoanBriefId);
                if (loanbrief == null)
                    return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay!" });
                model.LoanAmountExpertiseLast = loanbrief.LoanAmountExpertiseLast;
                model.LoanAmountExpertise = loanbrief.LoanAmountExpertise;
                model.LoanAmountExpertiseAi = loanbrief.LoanAmountExpertiseAi;
                model.LoanAmountFirst = loanbrief.LoanAmountFirst > 0 ? loanbrief.LoanAmountFirst : model.LoanAmount;
                //mặc định mua bảo hiểm
                model.BuyInsurenceCustomer = true;
                //xử lý giấy tờ hình thức nhận lương
                if (model.LoanBriefJobModel != null && model.LoanBriefJobModel.ImcomeType > 0)
                {
                    if (model.LoanBriefJobModel.ImcomeType == (int)EnumImcomeType.ChuyenKhoan)
                        model.LoanBriefQuestionScriptModel.DocumetSalaryCash = null;
                    else if (model.LoanBriefJobModel.ImcomeType == (int)EnumImcomeType.TienMat)
                        model.LoanBriefQuestionScriptModel.DocumentSalaryBankTransfer = null;
                }

                //Check xem trong bảng customer có số cmnd không
                //nếu có mà đơn vay mới không có thì lấy số cmnd cũ
                if (loanbrief.Customer != null && !string.IsNullOrEmpty(loanbrief.Customer.NationalCard)
                   && string.IsNullOrEmpty(model.NationalCard))
                    model.NationalCard = loanbrief.Customer.NationalCard;

                //Check xem có thay đổi thông tin quan trọng không
                // nếu có lưu lại vào comment
                var note = await SaveCommentChangeInfomationScriptView(model);

                var taskRun = new List<Task>();
                if (!string.IsNullOrEmpty(note))
                {
                    var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = note,
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    }));
                    taskRun.Add(task);
                }

                var resultUpdate = await _loanBriefService.UpdateScriptTelesaleV2(GetToken(), model.MapToLoanbriefV2(), model.LoanBriefId);
                if (resultUpdate > 0)
                {
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    var msg = string.Format("Lưu lại đơn vay thành công", resultUpdate);
                    return Json(new { status = 1, message = msg });
                }
                else
                {
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        [HttpGet]
        [PermissionFilter]
        public IActionResult CancelNotContactModal(int loanbriefId)
        {
            ViewBag.LoanBriefId = loanbriefId;
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(null, "_CancelNotContactModal")));
        }
        #endregion

        #region Lưu comment khi thay đổi những thông tin quan trọng
        private async Task<string> SaveCommentChangeInfomationScriptView(LoanbriefScriptViewModel model)
        {
            var note = string.Empty;
            try
            {
                var access_token = GetToken();
                var oldLoanBrief = await _loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);
                if (oldLoanBrief != null)
                {
                    //Check xem có thay đổi CMND
                    if (!string.IsNullOrEmpty(model.NationalCard) && !string.IsNullOrEmpty(oldLoanBrief.NationalCard)
                        && model.NationalCard != oldLoanBrief.NationalCard)
                    {
                        note += string.Format("CMND: <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.NationalCard, model.NationalCard);
                    }
                    //check xem có thay đổi thành phố không
                    if (model.ProvinceId != null && oldLoanBrief.ProvinceId != null && model.ProvinceId != oldLoanBrief.ProvinceId)
                    {
                        var oldProvinceName = _dictionaryService.GetProvinceById(access_token, oldLoanBrief.ProvinceId.Value);
                        var newProvinceName = _dictionaryService.GetProvinceById(access_token, model.ProvinceId.Value);
                        if (oldProvinceName != null && newProvinceName != null)
                            note += string.Format("Thành phố: <b>{0}</b> thành <b>{1}</b> <br />", oldProvinceName.Name, newProvinceName.Name);
                    }
                    //check xem có thay đổi quận huyện không
                    if (model.DistrictId != null && oldLoanBrief.DistrictId != null && model.DistrictId != oldLoanBrief.DistrictId)
                    {
                        var oldDistrictName = _dictionaryService.GetDistrictById(access_token, oldLoanBrief.DistrictId.Value);
                        var newDistrictName = _dictionaryService.GetDistrictById(access_token, model.DistrictId.Value);
                        if (oldDistrictName != null && newDistrictName != null)
                            note += string.Format("Quận/Huyện: <b>{0}</b> thành <b>{1}</b> <br />", oldDistrictName.Name, newDistrictName.Name);
                    }
                    //check xem số tiền cho vay tối đa có thay đổi không
                    if (model.LoanAmountExpertiseLast != null && oldLoanBrief.LoanAmountExpertiseLast != null &&
                        (model.LoanAmountExpertiseLast != oldLoanBrief.LoanAmountExpertiseLast))
                    {
                        note += string.Format("Số tiền cho vay tối đa thay đổi <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.LoanAmountExpertiseLast.Value.ToString("###,0"), model.LoanAmountExpertiseLast.Value.ToString("###,0"));
                    }
                    if ((model.LoanBriefJobModel != null || model.LoanBriefResidentModel != null))
                    {
                        //Check xem có thay đổi thu nhập không
                        if (model.LoanBriefJobModel.TotalIncome != null && oldLoanBrief.LoanBriefJob != null && oldLoanBrief.LoanBriefJob.TotalIncome != null
                            && model.LoanBriefJobModel.TotalIncome != oldLoanBrief.LoanBriefJob.TotalIncome)
                        {
                            note += string.Format("Thu nhập: <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.LoanBriefJob.TotalIncome.Value.ToString("###,0"), model.LoanBriefJobModel.TotalIncome.Value.ToString("###,0"));
                        }

                        //check job có thay đổi không
                        //thay đổi thêm vào comment
                        if (model.LoanBriefJobModel.JobId != null && oldLoanBrief.LoanBriefJob != null && oldLoanBrief.LoanBriefJob.JobId != null
                            && (model.LoanBriefJobModel.JobId != oldLoanBrief.LoanBriefJob.JobId))
                        {
                            var newJobName = _dictionaryService.GetJobById(GetToken(), model.LoanBriefJobModel.JobId.Value);
                            var oldJobName = _dictionaryService.GetJobById(GetToken(), oldLoanBrief.LoanBriefJob.JobId.Value);
                            if (newJobName != null && oldJobName != null)
                                note = string.Format("Nghề nghiệp: <b>{0}</b> thành <b>{1}</b> <br />", oldJobName.Name, newJobName.Name);
                        }
                        //check xem hình thức sở hữu nhà có thanh đổi không
                        if (model.LoanBriefResidentModel.ResidentType != null && oldLoanBrief.LoanBriefResident != null && oldLoanBrief.LoanBriefResident.ResidentType != null
                            && (model.LoanBriefResidentModel.ResidentType != oldLoanBrief.LoanBriefResident.ResidentType))
                        {
                            var oldResidentType = ExtensionHelper.GetDescription((EnumTypeofownership)oldLoanBrief.LoanBriefResident.ResidentType);
                            var newResidentType = ExtensionHelper.GetDescription((EnumTypeofownership)model.LoanBriefResidentModel.ResidentType);
                            note += string.Format("Hình thức sở hữu nhà: <b>{0}</b> thành <b>{1}</b><br />", oldResidentType, newResidentType);
                        }
                        //check xem tọa độ google map nơi ở có thay đổi không
                        if (oldLoanBrief.LoanBriefResident != null && (!string.IsNullOrEmpty(model.LoanBriefResidentModel.AddressLatLng) && !string.IsNullOrEmpty(oldLoanBrief.LoanBriefResident.AddressLatLng))
                            && model.LoanBriefResidentModel.AddressLatLng != oldLoanBrief.LoanBriefResident.AddressLatLng)
                        {
                            note += string.Format("Tọa độ Google Map nơi ở: <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.LoanBriefResident.AddressLatLng, model.LoanBriefResidentModel.AddressLatLng);
                        }
                    }

                    //check xem có lưu lại comment của công việc không
                    if (model.LoanBriefQuestionScriptModel != null
                        && !string.IsNullOrEmpty(model.LoanBriefQuestionScriptModel.CommentJob)
                        && model.LoanBriefQuestionScriptModel.CommentJob != oldLoanBrief.LoanBriefQuestionScript.CommentJob)
                    {
                        note += string.Format("<b>Công việc kinh doanh: </b> {0}", model.LoanBriefQuestionScriptModel.CommentJob);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3Web/SaveCommentChangeInfomationScriptView PUT Exception");
            }
            return note;
        }

        private async Task<string> SaveCommentChangeInfomation(LoanbriefViewModel model, LoanBriefInitDetail oldLoanBrief)
        {
            var note = string.Empty;
            try
            {
                var access_token = GetToken();
                if (oldLoanBrief != null)
                {
                    //Check xem có thay đổi CMND
                    if (!string.IsNullOrEmpty(model.NationalCard) && !string.IsNullOrEmpty(oldLoanBrief.NationalCard)
                        && model.NationalCard != oldLoanBrief.NationalCard)
                    {
                        note += string.Format("CMND: <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.NationalCard, model.NationalCard);
                    }
                    //check xem có thay đổi thành phố không
                    if (model.ProvinceId != null && oldLoanBrief.ProvinceId != null && model.ProvinceId != oldLoanBrief.ProvinceId)
                    {
                        var oldProvinceName = _dictionaryService.GetProvinceById(access_token, oldLoanBrief.ProvinceId.Value);
                        var newProvinceName = _dictionaryService.GetProvinceById(access_token, model.ProvinceId.Value);
                        note += string.Format("Thành phố: <b>{0}</b> thành <b>{1}</b> <br />", oldProvinceName.Name, newProvinceName.Name);
                    }
                    //check xem có thay đổi quận huyện không
                    if (model.DistrictId != null && oldLoanBrief.DistrictId != null && model.DistrictId != oldLoanBrief.DistrictId)
                    {
                        var oldDistrictName = _dictionaryService.GetDistrictById(access_token, oldLoanBrief.DistrictId.Value);
                        var newDistrictName = _dictionaryService.GetDistrictById(access_token, model.DistrictId.Value);
                        note += string.Format("Quận/Huyện: <b>{0}</b> thành <b>{1}</b> <br />", oldDistrictName.Name, newDistrictName.Name);
                    }
                    //check xem số tiền cho vay tối đa có thay đổi không
                    if (model.LoanAmountExpertiseLast != null && oldLoanBrief.LoanAmountExpertiseLast != null &&
                        (model.LoanAmountExpertiseLast != oldLoanBrief.LoanAmountExpertiseLast))
                    {
                        note += string.Format("Số tiền cho vay tối đa thay đổi <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.LoanAmountExpertiseLast.Value.ToString("###,0"), model.LoanAmountExpertiseLast.Value.ToString("###,0"));
                    }
                    if (model.LoanBriefJobModel != null || model.LoanBriefResidentModel != null)
                    {
                        //Check xem có thay đổi thu nhập không
                        if (oldLoanBrief.LoanBriefJob != null && model.LoanBriefJobModel.TotalIncome != null && oldLoanBrief.LoanBriefJob.TotalIncome != null
                            && model.LoanBriefJobModel.TotalIncome != oldLoanBrief.LoanBriefJob.TotalIncome)
                        {
                            note += string.Format("Thu nhập: <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.LoanBriefJob.TotalIncome.Value.ToString("###,0"), model.LoanBriefJobModel.TotalIncome.Value.ToString("###,0"));
                        }

                        //check job có thay đổi không
                        //thay đổi thêm vào comment
                        if (oldLoanBrief.LoanBriefJob != null && model.LoanBriefJobModel.JobId != null && oldLoanBrief.LoanBriefJob.JobId != null
                            && (model.LoanBriefJobModel.JobId != oldLoanBrief.LoanBriefJob.JobId))
                        {
                            var newJobName = _dictionaryService.GetJobById(GetToken(), model.LoanBriefJobModel.JobId.Value);
                            var oldJobName = _dictionaryService.GetJobById(GetToken(), oldLoanBrief.LoanBriefJob.JobId.Value);
                            note = string.Format("Nghề nghiệp: <b>{0}</b> thành <b>{1}</b> <br />", oldJobName.Name, newJobName.Name);
                        }
                        //check xem hình thức sở hữu nhà có thanh đổi không
                        if (oldLoanBrief.LoanBriefResident != null && model.LoanBriefResidentModel.ResidentType != null && oldLoanBrief.LoanBriefResident.ResidentType != null
                            && (model.LoanBriefResidentModel.ResidentType != oldLoanBrief.LoanBriefResident.ResidentType))
                        {
                            var oldResidentType = ExtensionHelper.GetDescription((EnumTypeofownership)oldLoanBrief.LoanBriefResident.ResidentType);
                            var newResidentType = ExtensionHelper.GetDescription((EnumTypeofownership)model.LoanBriefResidentModel.ResidentType);
                            note += string.Format("Hình thức sở hữu nhà: <b>{0}</b> thành <b>{1}</b><br />", oldResidentType, newResidentType);
                        }
                        //check xem tọa độ google map nơi ở có thay đổi không
                        if (oldLoanBrief.LoanBriefResident != null && !string.IsNullOrEmpty(model.LoanBriefResidentModel.AddressLatLng) && !string.IsNullOrEmpty(oldLoanBrief.LoanBriefResident.AddressLatLng)
                            && model.LoanBriefResidentModel.AddressLatLng != oldLoanBrief.LoanBriefResident.AddressLatLng)
                        {
                            note += string.Format("Tọa độ Google Map nơi ở: <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.LoanBriefResident.AddressLatLng, model.LoanBriefResidentModel.AddressLatLng);
                        }
                    }

                    //Kiểm tra xem có thay đổi từ mua bảo hiểm không
                    if (oldLoanBrief.LoanBriefProperty != null && model.LoanBriefPropertyModel != null
                        && oldLoanBrief.LoanBriefProperty.BuyInsuranceProperty.GetValueOrDefault(false) != model.LoanBriefPropertyModel.BuyInsuranceProperty.GetValueOrDefault(false))
                    {
                        note += string.Format("Bảo hiểm tài sản thay đổi từ: <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.LoanBriefProperty.BuyInsuranceProperty == true ? "có mua" : "không mua", model.LoanBriefPropertyModel.BuyInsuranceProperty == true ? "có mua" : "không mua");
                    }

                    //kiểm tra có thay đổi thời gian vay không
                    var messageChange = string.Empty;
                    if (model.LoanTime != oldLoanBrief.LoanTime && model.LoanTime > 0 && oldLoanBrief.LoanTime > 0)
                    {
                        note += string.Format("Thời gian vay: <b>{0}</b> thành <b>{1}</b> <br />", oldLoanBrief.LoanTime, model.LoanTime);
                    }
                    //kiểm tra có thay đổi LTV không
                    if (model.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                        || model.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                        || model.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                    {
                        if (model.Ltv.GetValueOrDefault(0) > 0
                            && model.Ltv != oldLoanBrief.Ltv.GetValueOrDefault(0))
                            note += string.Format("LTV: <b>{0}%</b> thành <b>{1}%</b> <br />", oldLoanBrief.Ltv, model.Ltv);
                    }
                    //kiểm tra có thay đổi xác nhận giao dịch đảm bảo
                    if (model.IsTransactionsSecured.GetValueOrDefault(false) != oldLoanBrief.IsTransactionsSecured.GetValueOrDefault(false))
                    {
                        var mes1 = oldLoanBrief.IsTransactionsSecured.GetValueOrDefault(false) == true ? "Đã xác nhận" : "Không xác nhận";
                        var mes2 = model.IsTransactionsSecured.GetValueOrDefault(false) == true ? "Đã xác nhận" : "Không xác nhận";
                        note += string.Format("Thay đổi xác nhận giao dịch đảm bảo từ <b>{0}</b> thành <b>{1}</b> <br />", mes1, mes2);
                    }
                    //Thay đổi gói vay
                    if (model.ProductId > 0 && oldLoanBrief.ProductId > 0 && model.ProductId != oldLoanBrief.ProductId)
                    {
                        var oldProductName = Enum.IsDefined(typeof(EnumProductCredit), oldLoanBrief.ProductId) ? ExtensionHelper.GetDescription((EnumProductCredit)oldLoanBrief.ProductId) : "";
                        var newProductName = Enum.IsDefined(typeof(EnumProductCredit), model.ProductId) ? ExtensionHelper.GetDescription((EnumProductCredit)model.ProductId) : "";
                        note += string.Format("Thay thay đổi gói vay từ <b>{0}</b> thành <b>{1}</b> <br />", oldProductName, newProductName);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBriefV3Web/SaveCommentChangeInfomation PUT Exception");
            }
            return note;
        }
        #endregion

        #region Lấy lại thông tin location Sim
        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> ReGetLocationSimV2(int LoanBriefId)
        {
            var user = GetUserGlobal();
            var access_token = user.Token;
            //Lấy ra log gọi AI lấy thông tin location gần đây nhất
            var lstLogLoanInfo = _logLoanInfo.GetRequest(access_token, LoanBriefId, (int)ServiceTypeAI.Location);
            var isCheck = false;
            var model = PrepareCACModel(LoanBriefId, ref isCheck);

            if (!isCheck)
                return Json(new { status = 0, message = "KH chưa chụp hồ sơ để lấy thông tin location" });

            if (lstLogLoanInfo != null && lstLogLoanInfo.Count > 0)
            {
                if (lstLogLoanInfo.Where(x => x.ResultFinal == "YES" || x.ResultFinal == "NO").Count() >= 2)
                {
                    return Json(new { status = 0, message = "Quá số lần lấy Location." });
                }
                else
                {
                    var logLoanInfo = lstLogLoanInfo.OrderByDescending(x => x.Id).FirstOrDefault();
                    if (logLoanInfo != null && model != null)
                    {
                        //kiểm tra xem có yêu cầu đang được xử lý
                        if (!string.IsNullOrEmpty(logLoanInfo.Request) && !string.IsNullOrEmpty(logLoanInfo.RefCode)
                            && logLoanInfo.IsCancel != 1
                            && (logLoanInfo.IsExcuted != (int)EnumLogLoanInfoAiExcuted.HasResult
                            && logLoanInfo.IsExcuted != (int)EnumLogLoanInfoAiExcuted.Excuted))
                        {
                            //Kiểm tra có thay đổi thông tin request không.
                            if (String.Compare(logLoanInfo.Request.ToString(), JsonConvert.SerializeObject(model).ToString(), true) == 0)
                            {
                                isCheck = false;
                                return Json(new { status = 0, message = "Đang có yêu cầu location khác đang được xử lý. Vui lòng chờ kết quả" });
                            }
                        }
                        else
                        {
                            //Nếu có kết quả rồi
                            if (!string.IsNullOrEmpty(logLoanInfo.Request) && !string.IsNullOrEmpty(logLoanInfo.RefCode)
                                && (logLoanInfo.IsExcuted == (int)EnumLogLoanInfoAiExcuted.HasResult
                                || logLoanInfo.IsExcuted == (int)EnumLogLoanInfoAiExcuted.Excuted)
                                && (logLoanInfo.ResultFinal == "YES" || logLoanInfo.ResultFinal == "NO"))
                            {
                                //Kiểm tra đủ 
                                if (String.Compare(logLoanInfo.Request.ToString(), JsonConvert.SerializeObject(model).ToString(), true) == 0)
                                {
                                    isCheck = false;
                                    return Json(new { status = 0, message = "Yêu cầu gửi đã có kết quả" });
                                }
                            }
                        }
                    }
                }
            }

            if (isCheck && model != null)
            {
                var entity = new LoanBriefInitDetail()
                {
                    LoanBriefId = LoanBriefId
                };
                var result = _loanBriefService.ReGetLocation(access_token, entity);
                if (result > 0)
                {
                    //Thêm note
                    _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = LoanBriefId,
                        Note = string.Format("{0} gửi yêu cầu lấy thông tin Location Sim.", user.FullName),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.ReGetLocation.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                    return Json(new { status = 1, message = "Lấy lại thông tin Location Sim thành công" });
                }
                else
                {
                    return Json(new { status = 0, message = "Xảy ra lỗi khi lấy lại thông tin Location Sim" });
                }
            }
            else
                return Json(new { status = 0, message = "Lấy thông tin Location thất bại. Vui lòng báo lại với phòng kỹ thuật" });
        }

        public LocationAIReq PrepareCACModel(int loanbriefId, ref bool isCheck)
        {
            try
            {
                var loanbrief = _loanBriefService.GetLoanBriefByIdNew(GetToken(), loanbriefId);
                isCheck = false;
                var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
                var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
                var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
                if (loanbrief.HomeNetwork.GetValueOrDefault(0) == 0)
                    loanbrief.HomeNetwork = _networkService.CheckHomeNetwok(loanbrief.Phone);
                string NetworkName = Enum.IsDefined(typeof(HomeNetWorkMobile), loanbrief.HomeNetwork) ? ExtensionHelper.GetDescription((HomeNetWorkMobile)loanbrief.HomeNetwork) : string.Empty;
                if (!string.IsNullOrEmpty(NetworkName))
                {
                    var webhook = string.Empty;
                    if (loanbrief.HomeNetwork == HomeNetWorkMobile.Viettel.GetHashCode())
                        webhook = _baseConfig["AppSettings:DomainAPILOS"] + Constants.webhook_location_viettel;
                    else if (loanbrief.HomeNetwork == HomeNetWorkMobile.Mobi.GetHashCode())
                        webhook = _baseConfig["AppSettings:DomainAPILOS"] + Constants.webhook_location_mobi;

                    int age = 0;
                    if (loanbrief.Dob.HasValue)
                    {
                        age = DateTime.Now.Year - loanbrief.Dob.Value.Year;
                        if (DateTime.Now.DayOfYear < loanbrief.Dob.Value.DayOfYear)
                            age = age - 1;
                    }
                    //var addressHome = _loanBriefService.GetLatLng(GetToken(), (int)TypeGetLatLng.Home, loanbrief.LoanBriefId);
                    //var addressCompany = _loanBriefService.GetLatLng(GetToken(), (int)TypeGetLatLng.Company, loanbrief.LoanBriefId);

                    var addressHome = loanbrief.LoanBriefResident != null && loanbrief.LoanBriefResident.AddressLatLng != null ? loanbrief.LoanBriefResident.AddressLatLng : null;
                    var addressCompany = loanbrief.LoanBriefJob != null && loanbrief.LoanBriefJob.CompanyAddressLatLng != null ? loanbrief.LoanBriefJob.CompanyAddressLatLng : null;

                    if (string.IsNullOrEmpty(addressHome) && string.IsNullOrEmpty(addressCompany))
                        return null;
                    //nếu có chứng từ nhà, k có chứng từ công ty thì gán
                    if (!string.IsNullOrEmpty(addressHome) && string.IsNullOrEmpty(addressCompany))
                        addressCompany = addressHome;
                    if (string.IsNullOrEmpty(addressHome) && !string.IsNullOrEmpty(addressCompany))
                        addressHome = addressCompany;
                    var objData = new LocationAIReq()
                    {
                        locationCodes = new LocationCode()
                        {
                            home = addressHome,
                            office = addressCompany,
                            other = string.Empty
                        },
                        phone = loanbrief.Phone,
                        age = age,
                        url = webhook,
                        carrier = !string.IsNullOrEmpty(NetworkName) ? NetworkName.ToLower() : "",
                        office = true,
                        validateAddress = true
                    };
                    isCheck = true;
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        #endregion

        #region Detail
        [PermissionFilter]
        public async Task<IActionResult> Detail(int id = 0)
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            var access_token = user.Token;
            var urlEmbedMapTracking = _baseConfig["AppSettings:UrlEmbedMapTracking"];
            var urlEmbedMapProductAppraiser = _baseConfig["AppSettings:UrlEmbedMapProductAppraiser"];
            var loanBrief = new LoanBriefForDetail();
            if (id > 0)
            {
                loanBrief = await _loanBriefService.GetLoanBriefForDetail(access_token, id);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));

                #region check rule hub
                //Check quyền xem chứng từ của hub
                bool isDisplayDoc = true;
                //Nếu là CHT và QLKV
                if (user.GroupId == (int)EnumGroupUser.ManagerHub)
                {
                    if (loanBrief.HubId.GetValueOrDefault(0) > 0)
                    {
                        if (!user.ListShop.Any(x => x.ShopId == loanBrief.HubId))
                            isDisplayDoc = false;
                        //Kiểm tra nếu không có quyền view thì check đơn đang xly theo sdt/cmnd
                        if (!isDisplayDoc && (loanBrief.Status == (int)EnumLoanStatus.DISBURSED || loanBrief.Status == (int)EnumLoanStatus.FINISH))
                        {
                            var loanProcess = await _loanBriefService.CheckLoanDisplayDocHubEmp(access_token, loanBrief.Phone, loanBrief.NationalCard);
                            if (loanProcess != null && user.ListShop.Any(x => x.ShopId == loanProcess.HubId))
                                isDisplayDoc = true;
                        }
                    }
                    else
                        isDisplayDoc = false;
                }
                //NV HUB
                else if (user.GroupId == (int)EnumGroupUser.StaffHub)
                {
                    //Nếu đơn không phải của nv không được xem chứng từ
                    if (loanBrief.HubEmployeeId.GetValueOrDefault(0) != user.UserId)
                        isDisplayDoc = false;
                    //Kiểm tra nếu không có quyền view thì check đơn đang xly theo sdt/cmnd
                    if (!isDisplayDoc && (loanBrief.Status == (int)EnumLoanStatus.DISBURSED || loanBrief.Status == (int)EnumLoanStatus.FINISH))
                    {
                        var loanProcess = await _loanBriefService.CheckLoanDisplayDocHubEmp(access_token, loanBrief.Phone, loanBrief.NationalCard);
                        if (loanProcess != null && loanProcess.HubEmployeeId.GetValueOrDefault(0) == user.UserId)
                            isDisplayDoc = true;
                    }
                }
                ViewBag.IsDisplayDoc = isDisplayDoc;
                //End
                #endregion
                loanBrief.TypeServiceCall = user.TypeCallService;
                loanBrief.ListRelativeFamilys = CacheData.ListRelativeFamilys;
                loanBrief.ListBrandProduct = CacheData.ListBrandProduct;
                loanBrief.ListLoanProduct = CacheData.ListLoanProduct;
                loanBrief.ListPlatformType = CacheData.ListPlatformType;
                loanBrief.LoanbriefLender = _dictionaryService.GetLoanBriefLender(access_token, id);
                //lấy ra JobDesctionName
                if (loanBrief.LoanBriefJob != null && loanBrief.LoanBriefJob.JobDescriptionId.HasValue)
                {
                    var jobDescription = _dictionaryService.GetJobById(GetToken(), loanBrief.LoanBriefJob.JobDescriptionId.Value);
                    if (jobDescription != null && jobDescription.JobId > 0)
                        loanBrief.JobDesctionName = jobDescription.Name;
                }
                //Nếu là thẩm định hoặc hub mới call
                if (user.GroupId == (int)EnumGroupUser.ApproveLeader || user.GroupId == (int)EnumGroupUser.ApproveEmp || user.GroupId == (int)EnumGroupUser.ManagerHub || user.GroupId == (int)EnumGroupUser.StaffHub)
                {
                    var checkLoanInfo = _dictionaryService.GetCheckImageByLoanBriefId(access_token, id);
                    if (checkLoanInfo != null && checkLoanInfo.Count > 0)
                    {
                        checkLoanInfo = checkLoanInfo.Where(x => x.TypeCheck == (int)EnumCheckLoanInformationType.CheckInfo).ToList();
                        loanBrief.CheckLoanInformation = checkLoanInfo;
                    }
                }
                ViewBag.urlEmbedMapTracking = string.Format(urlEmbedMapTracking, loanBrief.DeviceId, loanBrief.LoanBriefId);
                ViewBag.urlEmbedMapProductAppraiser = urlEmbedMapProductAppraiser + loanBrief.LoanBriefId;
                ViewBag.GroupID = user.GroupId;
                ViewBag.Ekyc = null;
                ViewBag.EkycNetwork = null;
                ViewBag.EkycMotobike = null;
                if (user.GroupId == (int)EnumGroupUser.StaffHub || user.GroupId == (int)EnumGroupUser.ManagerHub
                   || user.GroupId == (int)EnumGroupUser.ApproveEmp || user.GroupId == (int)EnumGroupUser.ApproveLeader
                   || user.GroupId == (int)EnumGroupUser.Admin || user.UserId == (int)EnumUser.tvv_test)
                {

                    List<int> lstServiceTypeAI = new List<int>()
                            {
                                (int)ServiceTypeAI.FraudDetection,
                                (int)ServiceTypeAI.Ekyc,
                                (int)ServiceTypeAI.EkycGetInfoNetwork,
                                (int)ServiceTypeAI.EkycMotorbikeRegistrationCertificate
                            };

                    var lstLogInfoAI = _logLoanInfo.GetByListServiceType(GetToken(), loanBrief.LoanBriefId, lstServiceTypeAI);

                    if (user.GroupId == (int)EnumGroupUser.ApproveEmp || user.GroupId == (int)EnumGroupUser.ApproveLeader
                        || user.GroupId == (int)EnumGroupUser.Admin || user.UserId == (int)EnumUser.tvv_test)
                    {
                        //lấy thông tin fraud check
                        var lstLogFraudCheck = lstLogInfoAI.Where(x => x.ServiceType == (int)ServiceTypeAI.FraudDetection).OrderByDescending(x => x.Id).FirstOrDefault();
                        if (lstLogFraudCheck != null)
                        {
                            if (!string.IsNullOrEmpty(lstLogFraudCheck.Response))
                            {
                                try
                                {
                                    var dataFraudCheck = JsonConvert.DeserializeObject<DAL.Object.ResultFraudCheckModel>(lstLogFraudCheck.Response);
                                    if (dataFraudCheck != null)
                                    {
                                        var fraudCheckModel = new FraudCheckViewModel();
                                        if (dataFraudCheck.tima_result != null && dataFraudCheck.tima_result.unified_result != null)
                                        {
                                            var dataTima = dataFraudCheck.tima_result.unified_result;
                                            if (!string.IsNullOrEmpty(dataTima.channel))
                                                fraudCheckModel.Channel = dataTima.channel;
                                            if (dataTima.details != null && dataTima.details.Count > 0)
                                            {
                                                foreach (var item in dataTima.details)
                                                    if (!string.IsNullOrEmpty(item.message))
                                                        fraudCheckModel.Message.Add(item.message);
                                            }
                                        }

                                        if (dataFraudCheck.hyperverge_result != null && dataFraudCheck.hyperverge_result.unifiedResult != null)
                                        {
                                            var dataHyperveger = dataFraudCheck.hyperverge_result.unifiedResult;
                                            if (!string.IsNullOrEmpty(dataHyperveger.channel))
                                                fraudCheckModel.HvChannel = dataHyperveger.channel;
                                            if (dataHyperveger.details != null && !string.IsNullOrEmpty(dataHyperveger.details.message))
                                            {
                                                fraudCheckModel.HvMessage.Add(dataHyperveger.details.message);
                                            }
                                        }
                                        if (dataFraudCheck.tima_result != null && !string.IsNullOrEmpty(dataFraudCheck.tima_result.report_url))
                                            fraudCheckModel.DetailUrl = dataFraudCheck.tima_result.report_url;
                                        ViewBag.FraudCheck = fraudCheckModel;
                                    }

                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }
                    }
                    //lấy thông tin để check ekyc
                    var lstCheckInfomation = lstLogInfoAI.Where(x => x.ServiceType != (int)ServiceTypeAI.FraudDetection).OrderByDescending(x => x.Id).ToList();
                    if (lstCheckInfomation != null && lstCheckInfomation.Count > 0)
                    {
                        int dem = 0;
                        foreach (var item in lstCheckInfomation)
                        {
                            try
                            {
                                if (!string.IsNullOrEmpty(item.ResponseRequest))
                                {
                                    if (item.ServiceType == (int)ServiceTypeAI.Ekyc && ViewBag.Ekyc == null)
                                    {
                                        dem++;
                                        ViewBag.Ekyc = JsonConvert.DeserializeObject<EkycOutput>(item.ResponseRequest);
                                    }
                                    else if (item.ServiceType == (int)ServiceTypeAI.EkycGetInfoNetwork && item.ResultFinal == "SUCCESS" && ViewBag.EkycNetwork == null)
                                    {
                                        dem++;
                                        ViewBag.EkycNetwork = JsonConvert.DeserializeObject<EkycNetwork>(item.ResponseRequest);
                                    }
                                    else if (item.ServiceType == (int)ServiceTypeAI.EkycMotorbikeRegistrationCertificate && item.ResultFinal == "SUCCESS" && ViewBag.EkycMotobike == null)
                                    {
                                        dem++;
                                        ViewBag.EkycMotobike = JsonConvert.DeserializeObject<EkycMotobike>(!string.IsNullOrEmpty(item.Response) ? item.Response : item.ResponseRequest);
                                    }
                                    if (dem == 3) break;

                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }

                //Check kiểm tra các đơn bị hủy bởi momo
                ViewBag.LstCancelMomo = null;
                var lstCancelMomo = _loanBriefService.GetLoanCancelByMomo(access_token, loanBrief);
                if (lstCancelMomo != null && lstCancelMomo.Count > 0)
                    ViewBag.LstCancelMomo = lstCancelMomo;

                //tính LTV mặc định đối với đơn cũ + gói xe máy
                if (loanBrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC
                    || loanBrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                    || loanBrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                {
                    if (loanBrief.Ltv.GetValueOrDefault(0) == 0)
                        loanBrief.Ltv = Common.Utils.ProductPriceUtils.GetPercentLtv(loanBrief.ProductId.Value);
                }

            }
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(loanBrief, "Detail")));
        }
        public async Task<IActionResult> ListDetailFile(int id = 0)
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var ServiceURLLocal = _baseConfig["AppSettings:ServiceURLLocal"];
            var model = new ListFileDetail();

            if (id > 0)
            {
                var access_token = GetToken();
                var loanBrief = await _loanBriefService.GetLoanBriefById(access_token, id);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                model.LoanBriefId = id;

                var typeOwnerShip = Common.Utils.ProductPriceUtils.ConvertEnumTypeOfOwnerShipDocument(loanBrief.LoanBriefResident.ResidentType.Value);
                var lstDocument = _documentService.GetDocument(user.Token, loanBrief.ProductId.Value, typeOwnerShip);
                if (lstDocument != null && lstDocument.Count > 0)
                {
                    if (!loanBrief.LoanBriefJob.JobId.HasValue)
                        return Json(GetBaseObjectResult(false, "Chưa có thông tin công việc của KH"));

                    var workRuleType = Common.Utils.ProductPriceUtils.ConvertTypeDescriptionJob(loanBrief.LoanBriefJob.JobId.Value, loanBrief.LoanBriefJob != null && loanBrief.LoanBriefJob.ImcomeType != null ? loanBrief.LoanBriefJob.ImcomeType.Value : 0
                                        , loanBrief.LoanBriefJob.CompanyInsurance, loanBrief.LoanBriefJob.BusinessPapers);

                    model.ListLoanBriefFile = _loanBriefService.GetLoanBriefFileByLoanId(access_token, id).Where(x => x.TypeId.GetValueOrDefault(0) > 0).ToList();
                    model.ListCheckImage = _dictionaryService.GetCheckImageByLoanBriefId(access_token, id);
                    if (model.ListLoanBriefFile != null && model.ListLoanBriefFile.Count > 0)
                    {
                        foreach (var item in model.ListLoanBriefFile)
                        {
                            if (!string.IsNullOrEmpty(item.FileThumb))
                            {
                                if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                    item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                                else
                                    item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                            }
                            if (string.IsNullOrEmpty(item.FilePath)) continue;
                            if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                                item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                            if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                                item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                            if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                            {
                                if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                    item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                                else
                                    item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                            }
                        }
                    }
                    var lstDocumentNew = Helpers.Ultility.ConvertDocumentTreeDetail(lstDocument, model.ListLoanBriefFile, workRuleType);
                    if (lstDocumentNew != null && lstDocumentNew.Count > 0)
                    {
                        foreach (var item in lstDocumentNew)
                        {
                            if (item.ParentId == (int)EnumDocumentType.AnhNha_TimaCare) item.Name = "(" + ExtentionHelper.GetDescription(EnumDocumentType.AnhNha_TimaCare) + ") " + item.Name;
                            if (item.ParentId == (int)EnumDocumentType.AnhNoiLamViec_TimaCare) item.Name = "(" + ExtentionHelper.GetDescription(EnumDocumentType.AnhNoiLamViec_TimaCare) + ") " + item.Name;
                            if (item.ParentId == (int)EnumDocumentType.CVKD_NoiO) item.Name = "(" + ExtentionHelper.GetDescription(EnumDocumentType.CVKD_NoiO) + ") " + item.Name;
                            if (item.ParentId == (int)EnumDocumentType.CVKD_NoiLamViec) item.Name = "(" + ExtentionHelper.GetDescription(EnumDocumentType.CVKD_NoiLamViec) + ") " + item.Name;
                        }
                    }
                    model.ListDocumentNewV2 = lstDocumentNew.OrderByDescending(x => x.ListFiles.Count > 0 ? 1 : 0).ToList();
                }


            }
            return View(model);

        }
        public async Task<IActionResult> ListFileAll(int id = 0)
        {
            var user = GetUserGlobal();
            var ServiceURL = _baseConfig["AppSettings:ServiceURL"];
            var ServiceURLAG = _baseConfig["AppSettings:ServiceURLAG"];
            var ServiceURLFileTima = _baseConfig["AppSettings:ServiceURLFileTima"];
            var ServiceURLLocal = _baseConfig["AppSettings:ServiceURLLocal"];
            var model = new ListFileDetail();
            if (id > 0)
            {
                var access_token = GetToken();
                var loanBrief = await _loanBriefService.GetLoanBriefById(access_token, id);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                model.ListLoanBriefFile = _loanBriefService.GetLoanBriefFileByLoanId(access_token, id).OrderBy(x => x.TypeId).ThenBy(n => n.CreateAt).ToList();
                if (model.ListLoanBriefFile != null && model.ListLoanBriefFile.Count > 0)
                {
                    await Task.Run(() => Parallel.ForEach(model.ListLoanBriefFile, item =>
                    {
                        if (!string.IsNullOrEmpty(item.FileThumb))
                        {
                            if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                item.FileThumb = ServiceURLLocal + item.FileThumb.Replace("/", "\\");
                            else
                                item.FileThumb = ServiceURLFileTima + item.FileThumb.Replace("/", "\\");
                        }
                        if (!string.IsNullOrEmpty(item.FilePath))
                        {
                            if (!item.FilePath.Contains("http") && (item.MecashId == null || item.MecashId == 0))
                                item.FilePath = ServiceURL + item.FilePath.Replace("/", "\\");
                            if (item.MecashId > 0 && (item.S3status == 0 || item.S3status == null) && !item.FilePath.Contains("http"))
                                item.FilePath = ServiceURLAG + item.FilePath.Replace("/", "\\");
                            if (item.MecashId > 0 && item.S3status == 1 && !item.FilePath.Contains("http"))
                            {
                                if (item.AsyncLocal == 1 && (DateTime.Now - item.CreateAt.Value).Days <= 30)
                                    item.FilePath = ServiceURLLocal + item.FilePath.Replace("/", "\\");
                                else
                                    item.FilePath = ServiceURLFileTima + item.FilePath.Replace("/", "\\");
                            }
                        }
                        else if (!string.IsNullOrEmpty(item.LinkImgOfPartner))
                        {
                            item.FilePath = item.DomainOfPartner + item.LinkImgOfPartner;
                        }
                    }));
                }

                //Kiểm tra cảnh báo khoảng cách ảnh và thời gian chụp ảnh
                if (user.GroupId == (int)EnumGroupUser.ApproveLeader || user.GroupId == (int)EnumGroupUser.ApproveEmp
                           || user.GroupId == (int)EnumGroupUser.ManagerHub || user.GroupId == (int)EnumGroupUser.StaffHub
                           || user.GroupId == (int)EnumGroupUser.Admin)
                {
                    var message = string.Empty;
                }
            }
            return View(model);
        }
        public IActionResult FollowDevice(string url)
        {
            ViewBag.Url = url;
            return View();
        }
        public async Task<IActionResult> AppraiserMotoPrice(int id = 0)
        {
            var model = new AppraiserMotoPriceViewModel();
            var user = GetUserGlobal();
            var access_token = user.Token;
            if (id > 0)
            {
                try
                {
                    var loanbriefModel = _loanBriefService.GetLoanBriefForAppraiser(access_token, id);
                    if (loanbriefModel != null)
                    {
                        model.Ltv = loanbriefModel.Ltv.GetValueOrDefault(0);
                        if (loanbriefModel.ProductPropertyId > 0)
                        {
                            var product = _productService.GetProduct(user.Token, loanbriefModel.ProductPropertyId.Value);
                            //định giá xe
                            var maxPriceProduct = 0L;
                            decimal priceCarAI = 0;
                            // lấy giá bên AI
                            decimal priceCurrent = 0L;
                            if (loanbriefModel.LoanAmountExpertiseAi > 0)
                                priceCurrent = loanbriefModel.LoanAmountExpertiseAi.Value;
                            else
                            {
                                priceCarAI = _productService.GetPriceAI(access_token, loanbriefModel.ProductPropertyId.Value, 0);
                                if (priceCarAI > 0)
                                    priceCurrent = (long)priceCarAI;
                            }
                            model.TypeRemarketingLtv = 0;
                            if (priceCurrent > 0)
                            {
                                model.CurrentPrice = priceCurrent;
                                if (!loanbriefModel.TypeOwnerShipId.HasValue)
                                    return Json(GetBaseObjectResult(false, "Vui lòng chọn hình thức sở hữu nhà"));

                                model.TypeOwnerShipId = loanbriefModel.TypeOwnerShipId.Value;
                                var productDetailId = loanbriefModel.ProductDetailId;
                                if (productDetailId.GetValueOrDefault(0) == 0)
                                    productDetailId = Common.Utils.ProductPriceUtils.ConvertProductDetail(loanbriefModel.TypeOwnerShipId.Value);

                                //Đối với gói vay xe máy topup
                                if (loanbriefModel.TypeRemarketing == EnumTypeRemarketing.IsTopUp.GetHashCode()
                                    && loanbriefModel.Status != (int)EnumLoanStatus.DISBURSED
                                    && loanbriefModel.Status != (int)EnumLoanStatus.FINISH
                                    && loanbriefModel.RemarketingLoanbriefId > 0
                                    && (loanbriefModel.ProductCreditId == (int)EnumProductCredit.MotorCreditType_CC
                                        || loanbriefModel.ProductCreditId == (int)EnumProductCredit.MotorCreditType_KCC
                                        || loanbriefModel.ProductCreditId == (int)EnumProductCredit.MotorCreditType_KGT))
                                {
                                    var resultCheckTopup = await _loanbriefV2Service.CheckCanTopup(loanbriefModel.RemarketingLoanbriefId.Value);
                                    if (resultCheckTopup.IsCanTopup)
                                    {
                                        model.TotalMoneyDebtCurrent = (long)resultCheckTopup.CurrentDebt;
                                        maxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPriceTopup(loanbriefModel.ProductCreditId.Value, priceCurrent,  model.TotalMoneyDebtCurrent.GetValueOrDefault(0), model.TypeOwnerShipId);
                                        if (maxPriceProduct < 3000000)
                                            return Json(new { status = 0, message = "Đơn vay không đủ điều kiện vay Topup định giá tài sản nhỏ hơn 3tr" });

                                        if (loanbriefModel.LoanAmount > maxPriceProduct)
                                            return Json(new { status = 0, message = $"KH chỉ được vay số tiền không quá {maxPriceProduct}đ" });
                                    }
                                    else
                                    {
                                        return Json(new { status = 0, message = "Đơn vay không đủ điều kiện vay Topup " });
                                    }
                                    model.TypeRemarketingLtv = (int)TypeRemarketingLtv.TopUp;
                                    if (model.Ltv == 0)
                                        model.Ltv = Common.Utils.ProductPriceUtils.GetPercentLtv(loanbriefModel.ProductCreditId.Value, model.TypeRemarketingLtv);
                                }

                                if (model.TotalMoneyDebtCurrent.GetValueOrDefault(0) == 0)
                                {
                                    if (loanbriefModel.IsReborrow.GetValueOrDefault(false))
                                        model.TypeRemarketingLtv = (int)TypeRemarketingLtv.Reborrow;
                                    if (model.Ltv == 0)
                                        model.Ltv = Common.Utils.ProductPriceUtils.GetPercentLtv(loanbriefModel.ProductCreditId.Value, model.TypeRemarketingLtv);
                                    maxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPriceV3(loanbriefModel.ProductCreditId.Value, priceCurrent,
                                    loanbriefModel.TypeOwnerShipId.Value, productDetailId.Value, 0, 0, model.TypeRemarketingLtv, model.Ltv);
                                }
                                model.ProductDetailId = productDetailId.Value;
                            }
                            model.MaxPriceProduct = maxPriceProduct;
                            model.ProductCreditId = loanbriefModel.ProductCreditId.Value;
                            model.ProductId = product.Id;
                            model.ProductReview = _productService.GetProductReviewV2(GetToken(), loanbriefModel.LoanBriefId, loanbriefModel.ProductPropertyId.Value,
                                product.ProductTypeId, priceCurrent) ?? new List<Models.ProductReview>();
                            model.UserDetail = user;
                            model.LoanBriefId = loanbriefModel.LoanBriefId;
                            model.Status = loanbriefModel.Status ?? 0;

                            #region lấy dữ liệu cũ
                            var olDataInfomationAppraiserMoto = new OldInfomationAppraiserMoto();
                            if (loanbriefModel.ProductCreditId == (int)EnumProductCredit.OtoCreditType_CC
                               || loanbriefModel.ProductCreditId == (int)EnumProductCredit.CamotoCreditType
                               || loanbriefModel.ProductCreditId == (int)EnumProductCredit.OtoCreditType_KCC)
                            {
                                if (!string.IsNullOrEmpty(loanbriefModel.CarManufacturer))
                                {
                                    olDataInfomationAppraiserMoto.ProductName = loanbriefModel.CarManufacturer;
                                    olDataInfomationAppraiserMoto.BrandName = loanbriefModel.CarName;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(loanbriefModel.ProductPropertyName))
                                {
                                    olDataInfomationAppraiserMoto.ProductName = loanbriefModel.ProductPropertyName;
                                    olDataInfomationAppraiserMoto.BrandName = loanbriefModel.BrandPropertyName;
                                }
                            }
                            olDataInfomationAppraiserMoto.ProductId = loanbriefModel.ProductCreditId.Value;
                            olDataInfomationAppraiserMoto.ProductDetailName = Common.Helpers.ExtentionHelper.GetDescription((Common.Extensions.InfomationProductDetail)model.ProductDetailId);
                            if (loanbriefModel.ProductCreditId.HasValue)
                                olDataInfomationAppraiserMoto.OwnerProductName = Common.Helpers.ExtentionHelper.GetDescription((Common.Extensions.EnumProductCredit)loanbriefModel.ProductCreditId);
                            if (loanbriefModel.TypeOwnerShipId.HasValue)
                                olDataInfomationAppraiserMoto.TypeOwnerShipName = Common.Helpers.ExtentionHelper.GetDescription((EnumTypeofownership)loanbriefModel.TypeOwnerShipId.Value);
                            model.OldInfomationAppraiserMoto = olDataInfomationAppraiserMoto;
                            #endregion



                            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "AppraiserMotoPrice")));
                        }
                        else
                            return Json(GetBaseObjectResult(false, "Đơn vay chưa có đủ thông tin xe máy. Không thể thẩm định xe."));
                    }
                    else
                        return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống"));
                }
                catch (Exception ex)
                {
                    return Json(GetBaseObjectResult(false, "Xảy ra lỗi khi lấy thông tin thẩm định"));
                }
            }
            else
                return Json(GetBaseObjectResult(false, "Không có mã đơn vay"));
        }
        public async Task<IActionResult> GetNoteDetail(int loanBriefId)
        {
            var lstData = new List<LoanBriefNoteViewModel>();
            var lstNote = _loanBriefService.GetNoteByLoanID(GetToken(), loanBriefId);
            if (lstNote != null && lstNote.Count > 0)
            {
                var lstUserId = lstNote.Select(x => x.UserId ?? 0).Distinct().ToList();
                var lstUser = _userServices.GetUser(GetToken(), lstUserId);
                Dictionary<int, int> dicUser = new Dictionary<int, int>();
                if (lstUser != null && lstUser.Count > 0)
                {
                    foreach (var user in lstUser)
                    {
                        if (user.UserId > 0 && user.GroupId > 0 && !dicUser.ContainsKey(user.UserId))
                            dicUser[user.UserId] = user.GroupId.Value;
                    }
                }

                foreach (var note in lstNote)
                {
                    var data = new LoanBriefNoteViewModel();
                    if (note.UserId.GetValueOrDefault(0) == 0)
                        data.GroupId = 0;
                    else
                        data.GroupId = dicUser.ContainsKey(note.UserId.Value) ? dicUser[note.UserId.Value] : 0;
                    data.LoanBriefNoteId = note.LoanBriefNoteId;
                    data.Note = note.Note;
                    data.CreatedTime = note.CreatedTime.Value.DateTime;
                    data.ShopName = note.ShopName;
                    data.FullName = note.FullName;
                    lstData.Add(data);
                }
            }
            return View(lstData);
        }

        #endregion

        #region Màn hình tìm kiếm đơn trên hệ thống
        [Route("/loanbrief-search/index.html")]
        [PermissionFilter]
        public IActionResult LoanbriefSearch()
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            return View();
        }
        // [PermissionFilter]
        public async Task<IActionResult> DataLoanbriefSearch()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new LoanBriefSearchModelDatatable(HttpContext.Request.Form);
                if (modal.LoanBriefId != null)
                    modal.LoanBriefId = modal.LoanBriefId.Trim();
                if (modal.Search != null)
                    modal.Search = modal.Search.Trim();
                if (modal.CodeId != null)
                    modal.CodeId = modal.CodeId.Trim();
                int recordsTotal = 0;
                var data = new List<LoanbriefSearch>();
                if (!string.IsNullOrEmpty(modal.Search) || !string.IsNullOrEmpty(modal.LoanBriefId) || !string.IsNullOrEmpty(modal.CodeId))
                {
                    data = _loanBriefService.SearchLoanBrief(user.Token, modal.ToQueryObject(), ref recordsTotal);
                    if (data != null && data.Count > 0)
                    {
                        await Task.Run(() => Parallel.ForEach(data, item =>
                        {
                            if (!string.IsNullOrEmpty(item.Phone))
                                item.Phone = LOS.WebApp.Helpers.Ultility.HidePhone(item.Phone);
                        }));
                    }
                }
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Tạo đơn con cho đơn ô tô
        public IActionResult CreateLoanCarChild(int loanbriefId)
        {
            ViewBag.LoanBriefId = loanbriefId;
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(null, "_CreateLoanCarChild")));
        }
        [HttpPost]
        public async Task<IActionResult> CreateLoanCarChild(int LoanBriefId, long LoanAmount)
        {
            var user = GetUserGlobal();
            var access_token = user.Token;
            var entity = new CreateLoanBriefChild()
            {
                LoanBriefId = LoanBriefId,
                LoanAmount = LoanAmount
            };
            var resultUpdate = await _loanBriefService.CreateLoanBriefChild(access_token, entity);
            var taskRun = new List<Task>();
            if (resultUpdate > 0)
            {
                //Thêm note
                var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                {
                    LoanBriefId = resultUpdate,
                    Note = "Hợp đồng được tạo ra từ hợp đồng cha HĐ-" + LoanBriefId,
                    FullName = user.FullName,
                    Status = 1,
                    ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    UserId = user.UserId,
                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                }));
                taskRun.Add(task);
                if (taskRun.Count > 0)
                    Task.WaitAll(taskRun.ToArray());
                var msg = string.Format("Tạo mới đơn vay thành công HĐ-{0}", resultUpdate);

                //Info LoanNew
                var loanBriefNew = await _loanbriefV2Service.GetBasicInfo(LoanBriefId);

                //kiểm tra xem phải HĐ định vị không và có mã định vị không
                if (loanBriefNew.IsLocate == true && !string.IsNullOrEmpty(loanBriefNew.DeviceId))
                {
                    //gọi api mở hợp đồng
                    var resultOpent = _trackDeviceService.OpenContract(access_token, "HD-" + loanBriefNew.LoanBriefId, loanBriefNew.DeviceId, user.ShopGlobal.ToString(), loanBriefNew.ProductId.Value, loanBriefNew.LoanBriefId, "topup");
                    //StatusCode == 3 => mã hợp đồng đã tồn tại
                    if (resultOpent != null && (resultOpent.StatusCode == 200 || resultOpent.StatusCode == 3))
                    {
                        if (resultOpent.StatusCode == 200)
                        {
                            _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                            {
                                LoanBriefId = resultUpdate,
                                Note = string.Format("Đơn định vị: Tạo hợp đồng thành công với imei: {0}", loanBriefNew.DeviceId),
                                FullName = user.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = user.UserId,
                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            });
                            loanBriefNew.DeviceStatus = StatusOfDeviceID.OpenContract.GetHashCode();
                        }
                        loanBriefNew.ContractGinno = "HD-" + loanBriefNew.LoanBriefId;
                        //cập nhật db
                        await _loanbriefV2Service.UpdateDevice(loanBriefNew.LoanBriefId, loanBriefNew.DeviceStatus.Value, loanBriefNew.ContractGinno, loanBriefNew.DeviceId);

                    }
                }


                return Json(new { status = 1, message = msg, data = resultUpdate });
            }
            else
            {
                if (taskRun.Count > 0)
                    Task.WaitAll(taskRun.ToArray());
                return Json(new { status = 0, message = "Xảy ra lỗi khi tạo mới đơn vay. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region Hàm trong Detail
        [HttpGet]
        public IActionResult ResultEkyc(int loanbriefId)
        {
            var data = _resultEkycService.GetEkycByLoanBrief(GetToken(), loanbriefId);
            return Json(GetBaseObjectResult(true, "Success", data, RenderViewAsString(data, "_ResultEkyc")));
        }
        #endregion

        #region Check phone Detail
        [HttpPost]
        public async Task<IActionResult> CheckPhoneDetail(int id, int typePhoneCheck)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = GetToken();
                if (id > 0 && Enum.IsDefined(typeof(TypePhone), typePhoneCheck))
                {
                    var loanbriefId = 0;
                    var phone = string.Empty;
                    if (typePhoneCheck == (int)TypePhone.PhoneLoanbrief || typePhoneCheck == (int)TypePhone.PhoneOther)
                    {
                        var loanbrief = await _loanbriefV2Service.GetBasicInfo(id);
                        if (loanbrief != null)
                        {
                            loanbriefId = loanbrief.LoanBriefId;
                            if (typePhoneCheck == (int)TypePhone.PhoneLoanbrief)
                                phone = loanbrief.Phone;
                            else
                                phone = loanbrief.PhoneOther;
                        }
                        else
                        {
                            return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
                        }
                    }
                    else if (typePhoneCheck == (int)TypePhone.PhoneRelationship)
                    {
                        var relationship = await _relationshipService.GetById(id);
                        if (relationship != null)
                        {
                            phone = relationship.Phone;
                            loanbriefId = relationship.LoanBriefId.Value;
                        }
                        else
                        {
                            return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
                        }

                    }
                    if (!string.IsNullOrEmpty(phone))
                    {
                        var modelView = new CheckPhoneViewModel();
                        modelView.LoanbriefId = loanbriefId;
                        var data = await _loanbriefV2Service.ListLoanAndRelationNotCancel(phone);
                        if (data != null)
                        {
                            modelView.ListData = data.Where(x => x.LoanbriefId != loanbriefId).ToList();
                        }

                        return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(modelView, "CheckPhoneDetail")));
                    }
                    else
                    {
                        return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin số điện thoại", null));
                    }

                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Tham số truyền vào không đúng", null));
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Bỏ trình ngoại lệ
        [HttpPost]
        public async Task<IActionResult> CancelExceptions(int loanbriefId)
        {
            try
            {
                var user = GetUserGlobal();
                var loanbrief = await _loanbriefV2Service.GetBasicInfo(loanbriefId);
                if (loanbrief != null)
                {
                    //cập nhập lại cái trường của ngoại lề về null
                    var result = await _loanbriefV2Service.CancelExceptions(loanbriefId);
                    if (result)
                    {
                        //insert vào bảng LoanBriefNote
                        var objLoanBriefNote = new LoanBriefNote()
                        {
                            LoanBriefId = loanbrief.LoanBriefId,
                            UserId = user.UserId,
                            FullName = user.FullName,
                            ActionComment = (int)EnumActionComment.CancelExceptions,
                            Note = "<b>Bỏ nhãn trình ngoại lệ của đơn vay</b>",
                            Status = (int)EnumStatusBase.Active,
                            CreatedTime = DateTime.Now,
                            ShopId = user.ShopGlobal,
                            ShopName = user.ShopGlobalName,
                            IsDisplay = true
                        };
                        await _loanBriefNoteV2Service.Add(objLoanBriefNote);
                        return Json(new { status = 1, message = "Bỏ trình ngoại lệ thành công" });
                    }
                    else
                        return Json(new { status = 0, message = "Bỏ trình ngoại lệ không thành công. Vui lòng liên hệ phòng kĩ thuật" });
                }
                else
                    return Json(new { status = 0, message = "Không tin thấy thông tin đơn vay" });
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region Check Momo

        [HttpPost]
        public async Task<IActionResult> CheckMomo(int loanBriefId)
        {
            var user = GetUserGlobal();
            var loanbrief = await _loanBriefService.GetLoanBriefById(user.Token, loanBriefId);
            if (loanbrief != null)
            {
                var numBillBad = 0;
                var isCancel = false;
                var isBackList = false;
                var noteStr = "";
                var result = await _momoService.CheckMomo(user.Token, loanBriefId, loanbrief.NationalCard);
                if (result != null)
                {
                    var billsBad = result.Data.Bills.Where(x => x.Status == "BAD").Count();

                    // Nếu Khách hàng đang có 1 khoản vay là nợ xấu tại các tổ chức khác: Chặn đơn vay sản phẩm XMKCC, XMKGT.
                    if (billsBad == 1)
                    {
                        if (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                            isCancel = true;
                        else
                            numBillBad = 1;

                    }

                    //Nếu Khách hàng đang có từ 2 khoản vay là nợ xấu tại các tổ chức khác trở lên: Chặn đơn vay và cho vào blacklist.
                    else if (billsBad >= 2)
                    {
                        isCancel = true;
                        isBackList = true;
                    }
                }
                else
                {
                    return Json(new { status = 0, message = "Xảy ra lỗi khi kiểm tra momo. Vui lòng thử lại!" });
                }
                if (!isCancel && !string.IsNullOrEmpty(loanbrief.NationCardPlace))
                {
                    var result2 = await _momoService.CheckMomo(user.Token, loanbrief.LoanBriefId, loanbrief.NationCardPlace);
                    if (result2 != null)
                    {
                        var billsBad = result2.Data.Bills.Where(x => x.Status == "BAD").Count();
                        // Nếu Khách hàng đang có 1 khoản vay là nợ xấu tại các tổ chức khác: Chặn đơn vay sản phẩm XMKCC, XMKGT.
                        if (billsBad == 1)
                        {
                            if (numBillBad > 0)
                            {
                                isCancel = true;
                                isBackList = true;
                            }
                            else
                            {
                                if (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                                    isCancel = true;
                            }
                        }

                        //Nếu Khách hàng đang có từ 2 khoản vay là nợ xấu tại các tổ chức khác trở lên: Chặn đơn vay và cho vào blacklist.
                        else if (billsBad >= 2)
                        {
                            isCancel = true;
                            isBackList = true;
                        }
                    }
                }

                if (isCancel)
                {
                    if (isBackList)
                    {
                        //Add BlackList
                        var objAddBlacklist = new RequestAddBlacklist()
                        {
                            CustomerCreditId = loanbrief.CustomerId ?? 0,
                            FullName = loanbrief.FullName,
                            NumberPhone = loanbrief.Phone,
                            CardNumber = loanbrief.NationalCard,
                            BirthDay = loanbrief.Dob ?? null,
                            UserIdCreate = 1,
                            UserNameCreate = "Auto System",
                            FullNameCreate = "Auto System",
                            Note = noteStr,
                            LoanCreditId = loanBriefId
                        };
                        _lmsService.AddBlackList(user.Token, objAddBlacklist);
                    }
                    loanbrief.ReasonCancel = 759;
                    loanbrief.ActionState = EnumActionPush.Cancel.GetHashCode();
                    loanbrief.PipelineState = EnumPipelineState.MANUAL_PROCCESSED.GetHashCode();
                    loanbrief.InProcess = EnumInProcess.Process.GetHashCode();
                    loanbrief.LoanBriefCancelAt = DateTime.Now;
                    loanbrief.LoanBriefCancelBy = user.UserId;

                    var cancel = _loanBriefService.CancelLoanBrief(user.Token, loanbrief);

                }

                _loanBriefService.AddLoanBriefNote(user.Token, new LoanBriefNote
                {
                    LoanBriefId = loanBriefId,
                    Note = isCancel ? "Khách hàng có nhiều khoản vay bên khác, hệ thống tự động hủy đơn" : string.Format("Check momo với số cmnd: {0} không có nợ xấu", loanbrief.NationalCard),
                    FullName = user.FullName,
                    Status = 1,
                    ActionComment = isCancel ? (int)EnumActionComment.AutoCancel : (int)EnumActionComment.CommentLoanBrief,
                    CreatedTime = DateTime.Now,
                    UserId = user.UserId,
                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                });

                if (isCancel)
                    return Json(new { status = 1, message = "Khách hàng có nợ xấu, hệ thống tự động hủy đơn!" });
                else
                    return Json(new { status = 1, message = "Khách hàng không có nợ xấu!" });


            }
            else
                return Json(new { status = 0, message = "Không tin thấy thông tin đơn vay" });
        }

        [HttpPost]
        public async Task<IActionResult> CancelCheckMomo(int loanBriefId)
        {
            var user = GetUserGlobal();
            var loanbrief = await _loanbriefV2Service.GetBasicInfo(loanBriefId);
            if (loanbrief != null)
            {
                var lstLogLoanInfoAI = new List<LogLoanInfoAi>();
                //inser vào bảng LogLoanInfoAI
                lstLogLoanInfoAI.Add(new LogLoanInfoAi()
                {
                    ServiceType = (int)ServiceTypeAI.CheckLoanMomo,
                    LoanbriefId = loanbrief.LoanBriefId,
                    Request = loanbrief.NationalCard,
                    ResponseRequest = "{\"status\":0,\"data\":{ \"query_data\":\"'" + loanbrief.NationalCard + "'\",\"unchecked\":[],\"bills\":[]},\"message\":\"Thành công\"}",
                    CreatedAt = DateTime.Now,
                    Status = (int)EnumStatusBase.Active,
                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.Excuted,
                    ResultFinal = "PDNL",
                });
                if (!string.IsNullOrEmpty(loanbrief.NationCardPlace))
                {
                    lstLogLoanInfoAI.Add(new LogLoanInfoAi()
                    {
                        ServiceType = (int)ServiceTypeAI.CheckLoanMomo,
                        LoanbriefId = loanbrief.LoanBriefId,
                        Request = loanbrief.NationCardPlace,
                        ResponseRequest = "{\"status\":0,\"data\":{ \"query_data\":\"'" + loanbrief.NationCardPlace + "'\",\"unchecked\":[],\"bills\":[]},\"message\":\"Thành công\"}",
                        CreatedAt = DateTime.Now,
                        Status = (int)EnumStatusBase.Active,
                        IsExcuted = (int)EnumLogLoanInfoAiExcuted.Excuted,
                        ResultFinal = "PDNL",
                    });
                }
                if (await _logLoanInfoAIV2Service.AddRange(lstLogLoanInfoAI))
                {
                    var taskRun = new List<Task>();

                    //insert vào bảng LoanBriefNote
                    taskRun.Add(_loanBriefNoteV2Service.Add(new LoanBriefNote
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        UserId = user.UserId,
                        FullName = user.FullName,
                        ActionComment = (int)EnumActionComment.CommentLoanBrief,
                        Note = "<b style='color: red;'>Bỏ check khoản vay bên MOMO theo phê duyệt ngoại lệ</b>",
                        Status = (int)EnumStatusBase.Active,
                        CreatedTime = DateTime.Now,
                        ShopId = user.ShopGlobal,
                        ShopName = user.ShopGlobalName,
                        IsDisplay = true
                    }));

                    //bảng RuleCheckLoan
                    taskRun.Add(_ruleCheckLoanService.AddOrUpdate(new RuleCheckLoan
                    {
                        LoanbriefId = loanbrief.LoanBriefId,
                        CreatedAt = DateTime.Now,
                        IsCheckMomo = false,
                        UserId = user.UserId,
                    }));

                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    return Json(new { status = 1, message = "Hủy check momo thành công" });
                }
                else
                    return Json(new { status = 0, message = "Không insert được LogLoanInfoAI" });
            }
            else
                return Json(new { status = 0, message = "Không tin thấy thông tin đơn vay" });
        }
        #endregion

        #region Kiểm soát HĐ giải ngân
        [Route("/loanbrief/disbursement_contract.htm")]
        [PermissionFilter]
        public IActionResult DisbursementContract()
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetDisbursementContract()
        {
            var user = GetUserGlobal();

            var modal = new DisbursementContractDatatable(HttpContext.Request.Form);
            DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
            DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
            if (!string.IsNullOrEmpty(modal.DateRanger))
                DateTimeUtility.ConvertDateRangerV2(modal.DateRanger, ref fromDate, ref toDate);

            modal.FromDate = fromDate.ToString("yyyy/MM/dd");
            modal.ToDate = toDate.ToString("yyyy/MM/dd");

            #region Gán giá trị mặc định cho param
            if (string.IsNullOrEmpty(modal.provinceId))
                modal.provinceId = "-1";

            if (string.IsNullOrEmpty(modal.loanBriefId))
                modal.loanBriefId = "-1";

            if (string.IsNullOrEmpty(modal.productId))
                modal.productId = "-1";

            if (string.IsNullOrEmpty(modal.topup))
                modal.topup = "-1";

            #endregion
            var data = await _loanbriefV2Service.GetDisbursementContract(int.Parse(modal.provinceId), int.Parse(modal.loanBriefId), int.Parse(modal.productId),
               int.Parse(modal.topup), fromDate, toDate, modal.search, user.UserId, user.GroupId, int.Parse(modal.page), int.Parse(modal.perpage));

            modal.total = data.TotalCount.ToString();
            return Json(new { meta = modal, data = data.ListDisbursementContractDetail });
        }

        [HttpPost]
        public async Task<IActionResult> SaveChangeBoundTelesaleByTelesalesOff(int LoanBriefId, int TelesaleId, string TelesaleName,
            int BoundTelesaleIdOld, string BoundTelesaleNameOld)
        {
            var user = GetUserGlobal();

            //Kiểm tra xem đơn vay có tồn tại không
            var loanbrief = await _loanbriefV2Service.GetBasicInfo(LoanBriefId);
            if (loanbrief != null)
            {
                //cập nhập lại BoundTelesaleId
                if (await _loanbriefV2Service.UpdateBoundTelesaleId(LoanBriefId, TelesaleId))
                {
                    var taskRun = new List<Task>();

                    //insert vào bảng LoanBriefNote
                    taskRun.Add(_loanBriefNoteV2Service.Add(new LoanBriefNote
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        UserId = user.UserId,
                        FullName = user.FullName,
                        ActionComment = (int)EnumActionComment.CommentLoanBrief,
                        Note = string.Format("Chuyển đơn đang vay từ TLS {0} qua {1}", BoundTelesaleNameOld, TelesaleName),
                        Status = (int)EnumStatusBase.Active,
                        CreatedTime = DateTime.Now,
                        ShopId = 3703,
                        ShopName = "Tima",
                        IsDisplay = true
                    }));

                    //insert vào bảng LogDistributionUser
                    taskRun.Add(_logDistributionUserService.Add(new LogDistributionUser
                    {
                        LoanbriefId = LoanBriefId,
                        UserId = TelesaleId,
                        TypeDistribution = (int)TypeDistributionLog.TELESALE,
                        CreatedAt = DateTime.Now,
                        CreatedBy = user.UserId,
                    }));

                    //inser vào bảng LogLoanAction
                    taskRun.Add(_logLoanActionV2Service.Add(new LogLoanAction
                    {
                        LoanbriefId = loanbrief.LoanBriefId,
                        ActionId = (int)EnumLogLoanAction.LoanEdit,
                        TypeAction = (int)EnumTypeAction.Manual,
                        LoanStatus = loanbrief.Status,
                        TlsLoanStatusDetail = loanbrief.DetailStatusTelesales,
                        TelesaleId = TelesaleId,
                        HubId = loanbrief.HubId,
                        HubEmployeeId = loanbrief.HubEmployeeId,
                        CoordinatorUserId = loanbrief.CoordinatorUserId,
                        UserActionId = user.UserId,
                        GroupUserActionId = user.GroupId,
                        CreatedAt = DateTime.Now,
                        OldValues = "{\"BoundTelesaleId\": " + BoundTelesaleIdOld + "}",
                        NewValues = "{\"BoundTelesaleId\": " + TelesaleId + "}",
                    }));
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    return Json(new { status = 1, message = "Chuyển telesales thành công" });
                }
                else
                    return Json(new { status = 0, message = "Chuyển telesales thất bại. Vui lòng báo phòng kỹ thuật" });
            }
            else
                return Json(new { status = 0, message = "Không tin thấy thông tin đơn vay" });
        }

        [HttpPost]
        public async Task<IActionResult> CreateLoanToup(string[] listLoanbriefId, int typeLoan)
        {
            var user = GetUserGlobal();
            var lstLoanBriefId = new List<int>();
            var lstData = new List<LogReLoanbrief>();
            for (int i = 0; i < listLoanbriefId.Length; i++)
            {
                if (!string.IsNullOrEmpty(listLoanbriefId[i]) && int.TryParse(listLoanbriefId[i], out _))
                    lstLoanBriefId.Add(Convert.ToInt32(listLoanbriefId[i]));
            }
            if (lstLoanBriefId != null && lstLoanBriefId.Count > 0)
            {
                foreach (var item in lstLoanBriefId)
                {
                    var data = new LogReLoanbrief();
                    data.LoanbriefId = item;
                    data.CreatedAt = DateTime.Now;
                    data.CreatedBy = user.UserId;
                    data.TypeRemarketing = typeLoan == (int)EnumTypeLoanSupport.IsCanTopup ? EnumTypeRemarketing.IsTopUp.GetHashCode() : 0;
                    data.UtmSource = typeLoan == (int)EnumTypeLoanSupport.IsCanTopup ? "form_rmkt_topup" : "form_rmkt_dpd";
                    data.IsExcuted = (int)LogReLoanbriefExcuted.NoExcuted;
                    data.TypeReLoanbrief = typeLoan == (int)EnumTypeLoanSupport.IsCanTopup ? TypeReLoanbrief.LoanBriefTopup.GetHashCode() : TypeReLoanbrief.LoanBriefDpd.GetHashCode();
                    data.TelesaleId = (int)EnumUser.rmkt_topup;

                    lstData.Add(data);
                }
            }

            if (await _logReLoanBriefService.AddRange(lstData))
            {
                //cập nhập lại trường TypeLoanSupport
                await _loanbriefV2Service.UpdateTypeLoanSupportByList(lstLoanBriefId);

                return Json(new { status = 1, message = "Tạo đơn Topup thành công" });
            }
            return Json(new { status = 0, message = "Xảy ra lỗi khi tạo đơn Topup. Vui lòng báo lại phòng kỹ thuật." });

        }
        #endregion

        #region Change Phone
        [HttpGet]
        public async Task<IActionResult> ChangePhone(int loanBriefId)
        {
            var user = GetUserGlobal();
            var loanbrief = await _loanbriefV2Service.GetBasicInfo(loanBriefId);
            return View(loanbrief);
        }

        [HttpPost]
        public async Task<IActionResult> CheckPhone(ChangePhoneReq req)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                if (!req.PhoneNew.ValidPhone())
                    return Json(GetBaseObjectResult(false, "SĐT không đúng định dạng"));

                //Get List danh sách đơn vay được gán với SDT mới
                var data = await _loanBriefService.GetLoanByPhoneChange(user.Token, req.PhoneNew);

                return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(data, "_lstLoanByPhoneChange")));
            }
            catch (Exception ex)
            {
                return Json(GetBaseObjectResult(false, "Có lỗi xảy ra vui lòng thử lại sau"));
            }

        }
        [HttpPost]
        public async Task<IActionResult> ChangePhone(ChangePhoneReq req)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                if (!req.PhoneNew.ValidPhone())
                    return Json(GetBaseObjectResult(false, "SĐT không đúng định dạng"));
                //Kiểm tra sdt mới có được gán cho đơn nào đang xly hay không
                var isLoanProcess = await _loanBriefService.CheckLoanProcessing(access_token, req.PhoneNew);
                if (isLoanProcess)
                {
                    var loanbrief = await _loanbriefV2Service.GetBasicInfo(req.LoanBriefId);
                    if (loanbrief.TypeRemarketing != (int)EnumTypeRemarketing.IsTopUp)
                        return Json(new { status = 0, message = "SĐT đang được gán với đơn vay khác" });
                }
                //Call api change phone
                var isChangePhone = await _toolService.ChangePhone(GetToken(), new DAL.Object.Tool.ToolReq.ChangePhoneReq
                {
                    LoanBriefId = req.LoanBriefId,
                    Phone = req.PhoneNew
                });

                if (isChangePhone)
                {
                    var note = string.Format("Hợp đồng HĐ-{0} đã được {1} đổi SĐT từ {2} -> {3}", req.LoanBriefId, user.Group.GroupName, req.PhoneOld, req.PhoneNew);
                    var data = await _loanBriefService.GetLoanByPhoneChange(user.Token, req.PhoneNew);
                    if (data != null && data.Count > 0)
                    {
                        foreach (var item in data)
                            note += string.Format("<br /> - SĐT: {0} đang được gán với HĐ-{1}. Loại KH: {2}", req.PhoneNew, item.LoanBriefId, item.RelationshipType > 0 ? "Người thân" : "Khách vay");
                    }
                    //Thêm note
                    _loanBriefService.AddLoanBriefNote(user.Token, new LoanBriefNote
                    {
                        LoanBriefId = req.LoanBriefId,
                        Note = note,
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = (int)EnumActionComment.ChangePhone,
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });

                    //Lưu Log change phone
                    await _dictionaryService.AddLogChangePhone(user.Token, new LogChangePhone
                    {
                        LoanbriefId = req.LoanBriefId,
                        UserId = user.UserId,
                        GroupId = user.GroupId,
                        Phone = req.PhoneOld,
                        PhoneNew = req.PhoneNew,
                        CreatedAt = DateTime.Now
                    });

                    return Json(new { status = 1, message = "Thay đổi SĐT thành công!" });
                }
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi khi thay đổi SĐT. Vui lòng thử lại sau!" });
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region Đóng HĐ
        public IActionResult GetCloseLoan(int loanBriefId)
        {
            ViewBag.LoanBriefId = loanBriefId;
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(null, "_GetCloseLoan")));
        }

        [HttpPost]
        public async Task<IActionResult> CloseLoan(int loanbriefId, string content)
        {
            //Kiểm tra xem đơn vay có tồn tại không
            var loanbrief = await _loanbriefV2Service.GetBasicInfo(loanbriefId);
            if (loanbrief != null)
            {
                //kiểm tra trong log xem đơn gửi yêu cầu chưa
                if (!_logCloseLoanService.CheckLoanProcess(loanbriefId))
                    return Json(new { status = 0, message = "HĐ đã có yêu cầu đóng. Vui lòng chờ" });

                var user = GetUserGlobal();
                //gọi api sang bên lms
                var objResquestCloseLoan = new ResquestCloseLoan()
                {
                    LoanAgID = loanbrief.LmsLoanId.Value,
                    RequestByUserID = user.UserId,
                    RequestByName = user.Username,
                    Reason = content,
                    CloseDate = DateTime.Now.ToString("dd/MM/yyyy")
                };
                var closeLoan = _lmsService.CloseLoan(objResquestCloseLoan, loanbriefId);
                if (closeLoan != null)
                {
                    if (closeLoan.Result == 1) // thành công
                    {
                        //insert vào bảng LoanBriefNote
                        await _loanBriefNoteV2Service.Add(new LoanBriefNote
                        {
                            LoanBriefId = loanbrief.LoanBriefId,
                            UserId = user.UserId,
                            FullName = user.FullName,
                            ActionComment = (int)EnumActionComment.CommentLoanBrief,
                            Note = string.Format("{0} Gửi yêu cầu đóng HĐ", user.Username) + "<br />" + content,
                            Status = (int)EnumStatusBase.Active,
                            CreatedTime = DateTime.Now,
                            ShopId = 3703,
                            ShopName = "Tima",
                            IsDisplay = true
                        });
                        return Json(new { status = 1, message = "Gửi yêu cầu đóng HĐ thành công" });
                    }
                    else
                        return Json(new { status = 0, message = closeLoan.Message });
                }
                else
                    return Json(new { status = 0, message = "Không gọi được api bên LMS" });
            }
            else
                return Json(new { status = 0, message = "Không tin thấy thông tin đơn vay" });
        }
        #endregion


        #region Push To Cisco
        [Route("/push_to_cisco/index.htm")]
        public IActionResult PushToCisco()
        {
            return View();
        }
        [HttpPost]
        public IActionResult PushToCiscoSearch()
        {
            try
            {
                var user = GetUserGlobal();
                var model = new PushToCiscoDatatable(HttpContext.Request.Form);

                int recordsTotal = 0;
                // Query api   	
                var data = _loanBriefService.PushToCiscoSearch(GetToken(), model.ToQueryObject(), ref recordsTotal);
                model.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = model, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public IActionResult PushCiscoModal(int LoanBriefId)
        {
            return View();
        }
        [HttpPost]
        [PermissionFilter]
        public async Task<IActionResult> PushToCiscoPost(LogPushToCisco model)
        {
            var user = GetUserGlobal();
            var obj = new DT
            {
                Phone = model.Phone
            };
            var obj2 = new PushToCiscoInput();
            obj2.DT = new List<DT>();
            obj2.DT.Add(obj);

            var result = await _ciscoService.ImportContactCampaign(GetToken(), obj2);
            if (result != null)
            {
                //Insert to table LogToCisco

                await _loanBriefService.AddLogPushToCisco(user.Token, new LogPushToCisco
                {
                    Phone = model.Phone,
                    FullName = model.FullName,
                    LoanbriefId = model.LoanbriefId,
                    CreatedAt = DateTime.Now,
                    CreatedBy = user.UserId,
                    Status = (int)result.Code
                });


                return Json(new { Status = result.Code, Message = result.Message });

            }
            else
            {
                return Json(new { Status = 0, Message = "Có lỗi xảy ra, vui lòng liên hệ kỹ thuật" });
            }
        }
        #endregion
    }
}