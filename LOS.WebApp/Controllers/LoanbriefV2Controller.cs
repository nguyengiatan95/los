﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.Common.Utils;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object;
using LOS.Services.Services.Province;
using LOS.WebApp.Cache;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.AISuggestionsInformation;
using LOS.WebApp.Models.LoanbriefV2;
using LOS.WebApp.Services;
using LOS.WebApp.Services.AIService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace LOS.WebApp.Controllers
{
    public class LoanbriefV2Controller : BaseV2Controller
    {
        private ILoanBriefService _loanBriefService;
        private IConfiguration _configuration;
        private IHttpClientFactory _clientFactory;
        private ICompositeViewEngine _viewEngine;
        private IDictionaryService _dictionaryService;
        private ILMSService _lmsService;
        private IErp _erpService;
        private IProductService _productService;
        private IPriceMotor _priceMotorService;
        private ILogReqestAiService _logReqestAiService;
        private readonly INetworkService _networkService;
        private readonly IPipelineService _pipelineService;
        private IUserServices _userServices;
        private ICheckElectricityAndWater _checkElectricityAndWater;

        private IProvinceService _provinceService;

        public LoanbriefV2Controller(IConfiguration configuration, ILoanBriefService services, IHttpClientFactory clientFactory, ICompositeViewEngine viewEngine, IDictionaryService dictionaryService,
            ILMSService lmsService, IErp erpService, IProductService productService, ILogReqestAiService logReqestAiService, INetworkService networkService, IPriceMotor priceMotorService,
            IPipelineService pipelineService, IUserServices userServices, ICheckElectricityAndWater checkElectricityAndWater,
            IProvinceService provinceService)
          : base(configuration, clientFactory, viewEngine)
        {
            this._loanBriefService = services;
            this._clientFactory = clientFactory;
            this._dictionaryService = dictionaryService;
            this._lmsService = lmsService;
            this._erpService = erpService;
            this._productService = productService;
            this._priceMotorService = priceMotorService;
            this._logReqestAiService = logReqestAiService;
            this._networkService = networkService;
            this._pipelineService = pipelineService;
            this._userServices = userServices;
            this._checkElectricityAndWater = checkElectricityAndWater;
            this._provinceService = provinceService;
        }

        [PermissionFilter]
        public async Task<IActionResult> Init(int id = 0, int type = 0)
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            var model = new LoanbriefViewModel();
            
            var pickLCT = false;
            var editLTV = 0;
            var changeAddressHome = true;
            ViewBag.ListInfomationProductDetail = null;

            if (id > 0 && type == 0)
            {
                var access_token = user.Token;
                var loanBrief = await _loanBriefService.GetLoanBriefById(access_token, id);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                //Check xem đơn có phải hub tự tạo hoặc là gói vay Oto
                //if (loanBrief.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                //    pickLCT = true;
                //else
                //{
                //    if (loanBrief.CreateBy != null)
                //    {
                //        var userInfo = _userServices.GetById(user.Token, loanBrief.CreateBy.Value);
                //        if (userInfo != null && (userInfo.GroupId == (int)EnumGroupUser.StaffHub || userInfo.GroupId == (int)EnumGroupUser.ManagerHub))
                //            pickLCT = true;
                //    }
                //}
                ////Nếu đơn không qua tls => cho phép pick location
                //if(!pickLCT && loanBrief.BoundTelesaleId.GetValueOrDefault(0) == 0)
                //    pickLCT = true;

                //Mở cho hub pick location
                if (user.GroupId == (int)EnumGroupUser.StaffHub || user.GroupId == (int)EnumGroupUser.ManagerHub)
                    pickLCT = true;

                //Kiêm tra quyền tương tác dữ liệu
                if (!CheckPermissionAction(user.ListPermissionAction, loanBrief.Status.Value, EnumActionUser.Edit.GetHashCode()))
                {
                    if (Enum.IsDefined(typeof(EnumLoanStatus), loanBrief.Status))
                        return Json(new { status = 0, message = string.Format("Bạn không có quyền cập nhật đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanBrief.Status)) });
                    else
                        return Json(new { status = 0, message = "Bạn không có quyền cập nhật đơn" });
                }

                model = loanBrief.MapToLoanbriefViewModel();
                if (loanBrief.ProductId.HasValue && loanBrief.LoanBriefResident != null
                && loanBrief.LoanBriefResident.ResidentType.HasValue)
                {
                    ViewBag.ListInfomationProductDetail = _dictionaryService.GetInfomationProductDetailByTypeOwnershipAndProduct(access_token, loanBrief.LoanBriefResident.ResidentType.Value, loanBrief.ProductId.Value, loanBrief.TypeLoanBrief ?? 0, user.GroupId ?? 0);
                }
                model.IsReborrow = loanBrief.IsReborrow;
                //Mặc định cho hub sửa thông tin địa chỉ nơi ở. 25/06/2021
                if (user.GroupId == (int)EnumGroupUser.StaffHub || user.GroupId == (int)EnumGroupUser.ManagerHub)
                {
                    //Nếu đơn có tls => không cho sửa địa chỉ
                    if (loanBrief.BoundTelesaleId > 0)
                        changeAddressHome = false;
                }
                if(loanBrief.TypeRemarketing == EnumTypeRemarketing.IsTopUp.GetHashCode())
                    model.LtvTypeRemarketing = (int)TypeRemarketingLtv.TopUp;
               
            }
            //Tạo đơn tái vay
            else if (id > 0 && type > 0)
            {
                var access_token = user.Token;
                var loanBrief = await _loanBriefService.GetLoanBriefById(access_token, id);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));

                if (loanBrief.CreateBy != null)
                {
                    var userInfo = _userServices.GetById(user.Token, loanBrief.CreateBy.Value);
                    if (userInfo != null && (userInfo.GroupId == (int)EnumGroupUser.StaffHub || userInfo.GroupId == (int)EnumGroupUser.ManagerHub))
                        pickLCT = true;
                }

                model = loanBrief.MapToLoanbriefViewModel();

                if (loanBrief.ProductId.HasValue && loanBrief.LoanBriefResident != null
                && loanBrief.LoanBriefResident.ResidentType.HasValue)
                {
                    ViewBag.ListInfomationProductDetail = _dictionaryService.GetInfomationProductDetailByTypeOwnershipAndProduct(access_token, loanBrief.LoanBriefResident.ResidentType.Value, loanBrief.ProductId.Value);
                }
                model.TypeRemarketing = (int)EnumTypeRemarketing.IsRemarketing;
                model.IsReborrow = true;
                model.ReMarketingLoanBriefId = loanBrief.LoanBriefId;
                model.LoanBriefId = 0;

                if (loanBrief.IsReborrow == true)
                    model.LtvTypeRemarketing = (int)TypeRemarketingLtv.Reborrow;

            }
            else
            {
                if (user.GroupId == (int)EnumGroupUser.StaffHub)
                    pickLCT = true;
            }

            ViewBag.ListProvinceSupport = await _provinceService.GetProvinceSupport();
            ViewBag.ListProvince = _dictionaryService.GetProvince(GetToken());
            ViewBag.ListJobs = CacheData.ListJobs;// _dictionaryService.GetJob(GetToken());
            ViewBag.ListRelativeFamilys = CacheData.ListRelativeFamilys;// _dictionaryService.GetRelativeFamily(GetToken());
            ViewBag.ListBrandProduct = CacheData.ListBrandProduct;// _dictionaryService.GetBrandProduct(GetToken());
            ViewBag.ListLoanProduct = CacheData.ListLoanProduct;// _dictionaryService.GetLoanProduct(GetToken()).Where(x => x.Status == 1).ToList();
            ViewBag.ListBank = CacheData.ListBank;// _dictionaryService.GetBank(GetToken());
            ViewBag.ListPlatformType = CacheData.ListPlatformType;// _dictionaryService.GetPlatformType(GetToken());
            ViewBag.PickLCT = pickLCT;
            ViewBag.ChangeAddressHome = changeAddressHome;

            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "Init")));
        }

        #region Script telesale

        private string ValidLoanbriefScriptViewModel(LoanbriefScriptViewModel model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var strMessage = string.Empty;
                if (model.LoanBriefId == 0 && string.IsNullOrEmpty(model.Phone))
                    return "Vui lòng nhập số điện thoại khách hàng";
                if (string.IsNullOrEmpty(model.FullName))
                    return "Vui lòng nhập họ tên khách hàng";

                //kiểm tra CMT
                if (string.IsNullOrEmpty(model.NationalCard))
                    return "Vui lòng nhập CMT/CCCD";
                if (model.NationalCard.Length != 9 && model.NationalCard.Length != 12)
                    return "Vui lòng nhập đúng định dạng CMT/CCCD";

                //Check xem khách hàng có trong black list không
                var checkBlackList = _lmsService.CheckBlackList(user.Token, model.FullName, model.NationalCard, model.Phone, model.LoanBriefId);
                if (checkBlackList != null)
                {
                    if (checkBlackList.Data == true && checkBlackList.Status == 1)
                        return "Khách hàng nằm trong BlackList";
                }

                //kiểm tra thành phố đang sống
                if (!model.ProvinceId.HasValue)
                    return "Vui lòng chọn thành phố đang ở";

                //kiểm tra quận đang sống
                if (!model.DistrictId.HasValue)
                    return "Vui lòng chọn quận/huyện đang ở";

                //kiểm tra phường
                if (!model.WardId.HasValue)
                    return "Vui lòng chọn phường/xã đang ở";
                if (model.LoanAmount <= 0)
                    return "Vui lòng nhập số tiền khách hàng cần vay";
                return strMessage;
            }
            catch (Exception ex)
            {
                return "Xảy ra lỗi. Vui lòng thử lại sau";
            }
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> ScriptUpdateLoanBriefV2(LoanbriefScriptViewModel model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var messageValid = ValidLoanbriefScriptViewModel(model);
                if (!string.IsNullOrEmpty(messageValid))
                    return Json(new { status = 0, message = messageValid });

                //mặc định thời gian vay là 12 tháng
                model.LoanTime = 12;
                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                if (model.LoanTime > 0)
                    model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);

                //var loanbrief = await _loanBriefService.GetLoanBriefById(GetToken(), model.LoanBriefId);


                #region Xử lý phần qlf
                if (model.ValueCheckQualify == null)
                    model.ValueCheckQualify = 0;
                if (model.LoanBriefQuestionScriptModel != null)
                {
                    var item = model.LoanBriefQuestionScriptModel;
                    // có nhu cầu vay
                    if (item.QuestionBorrow == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.NeedLoan;
                    }
                    //Có xe máy
                    if (item.QuestionUseMotobikeGo == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveMotobike;
                    }
                    //Có đăng ký xe bản gốc
                    if (item.QuestionMotobikeCertificate == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveOriginalVehicleRegistration;
                    }
                }
                //Trong độ tuổi cho vay (18 - 65 tuổi)
                if (!string.IsNullOrEmpty(model.sBirthDay))
                {
                    var Dob = DateTimeUtility.ConvertStringToDate(model.sBirthDay, "dd/MM/yyyy");
                    if (Dob != null)
                    {
                        var dateNowYear = DateTime.Now.Year;
                        var dobYear = Dob.Year;
                        var yearOld = dateNowYear - dobYear;
                        if (yearOld >= 18 && yearOld <= 65)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.InAge;
                        }
                    }

                }
                //Trong khu vực hỗ trợ HN-HCM
                if (model.DistrictId > 0)
                {
                    var district = _dictionaryService.GetDistrictById(GetToken(), model.DistrictId.Value);
                    if (district != null && district.IsApply == 1)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.InAreaSupport;
                    }
                }
                //upload chứng từ
                var loanbriefFiles = _loanBriefService.GetLoanBriefFileByLoanId(GetToken(), model.LoanBriefId);
                if (loanbriefFiles != null && loanbriefFiles.Count > 0)
                {
                    if (loanbriefFiles.Count(x => x.UserId == 0) >= 4)
                        model.ValueCheckQualify += (int)EnumCheckQualify.UploadImage;
                }

                #endregion
                //mặc định mua bảo hiểm
                model.BuyInsurenceCustomer = true;
                //mặc định gói xe máy là lắp định vị
                model.IsLocate = true;
                //Xe máy chính chủ
                model.ProductId = model.OwnerProduct == 1 ? EnumProductCredit.MotorCreditType_CC.GetHashCode() : EnumProductCredit.MotorCreditType_KCC.GetHashCode();

                //Check đơn đang xử lý
                var listLoanBrief = _loanBriefService.GetLoanBriefProcessing(GetToken(), model.Phone, model.NationalCard, model.LoanBriefId);
                if (listLoanBrief != null && listLoanBrief.LoanBriefId > 0)
                    return Json(GetBaseObjectResult(false, $"Khách hàng có đơn vay đang xử lý HĐ-{listLoanBrief.LoanBriefId}"));
                //Lấy thông tin đơn vay cũ
                var oldLoanBrief = await _loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);

                var taskRun = new List<Task>();

                //Kiểm tra xem có thay đổi gói vay không
                if (model.ProductId != oldLoanBrief.ProductId && model.ProductId > 0 && oldLoanBrief.ProductId > 0)
                {
                    if (_pipelineService.ChangePipeline(access_token, new ChangePipelineReq() { loanBriefId = oldLoanBrief.LoanBriefId, productId = model.ProductId, status = oldLoanBrief.Status.Value }))
                    {
                        var oldProductName = Enum.IsDefined(typeof(EnumProductCredit), oldLoanBrief.ProductId) ? ExtensionHelper.GetDescription((EnumProductCredit)oldLoanBrief.ProductId) : "";
                        var newProductName = Enum.IsDefined(typeof(EnumProductCredit), model.ProductId) ? ExtensionHelper.GetDescription((EnumProductCredit)model.ProductId) : "";
                        var task1 = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = model.LoanBriefId,
                            Note = string.Format("{0} thay đổi gói vay từ <b>{1}</b> thành <b>{2}</b>", user.FullName, oldProductName, newProductName),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(task1);
                    }
                }

                //kiểm tra có thay đổi thông tin xe hay không
                var oldProperty = await _loanBriefService.GetLoanbriefProperty(access_token, model.LoanBriefId);
                if ((oldProperty != null && oldProperty.BrandId > 0 && oldProperty.ProductId > 0)
                    || (model.LoanBriefPropertyModel.ProductId > 0 && (oldProperty == null || oldProperty.ProductId == 0 || !oldProperty.ProductId.HasValue))
                    )
                {
                    if (oldProperty != null && oldProperty.ProductId > 0 && model.LoanBriefPropertyModel != null && model.LoanBriefPropertyModel.BrandId > 0
                        && model.LoanBriefPropertyModel.ProductId > 0 && model.LoanBriefPropertyModel.ProductId != oldProperty.ProductId)
                    {
                        var oldProduct = _productService.GetProduct(access_token, oldProperty.ProductId.Value);
                        var newProduct = _productService.GetProduct(access_token, model.LoanBriefPropertyModel.ProductId.Value);
                        var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                        {
                            LoanBriefId = model.LoanBriefId,
                            Note = string.Format("Kịch bản: {0} thay đổi xe máy {1} thành {2}", user.FullName, oldProduct.FullName, newProduct.FullName),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(task);
                        var priceAG = 0l;
                        decimal priceAI = 0l;
                        model.LoanAmountExpertiseLast = GetMaxPriceProduct((int)newProduct.Id, (int)model.LoanBriefResidentModel.ResidentType, (int)model.ProductId, model.LoanBriefId, ref priceAG, ref priceAI);
                        model.LoanAmountExpertise = priceAG;
                        if (priceAI > 0)
                            model.LoanAmountExpertiseAi = priceAI;
                        if (model.LoanAmountExpertiseLast > 0)
                        {
                            var task1 = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                            {
                                LoanBriefId = model.LoanBriefId,
                                Note = string.Format("Kịch bản: Xe của khách hàng được định giá tối đa {0}đ", model.LoanAmountExpertiseLast.Value.ToString("##,#")),
                                FullName = user.FullName,
                                Status = 1,
                                ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                CreatedTime = DateTime.Now,
                                UserId = user.UserId,
                                ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                            }));
                            taskRun.Add(task1);
                        }

                        // Lưu log sang AI
                        if (priceAG > 0 && priceAI > 0)
                        {
                            var productAiLogModel = _productService.PrepareProductAiLogModel(user.Token, model.LoanBriefPropertyModel.ProductId.Value);
                            if (productAiLogModel != null)
                            {
                                productAiLogModel.AI_price = (long)priceAI;
                                productAiLogModel.IT_price = (long)priceAG;
                                productAiLogModel.loan_credit_id = model.LoanBriefId;
                                var taskAI = Task.Run(() => _priceMotorService.PushLogPriceAIMoto(productAiLogModel, GetToken()));
                                taskRun.Add(taskAI);
                            }
                        }
                    }
                    else
                    {
                        if (model.LoanBriefPropertyModel.ProductId > 0)
                        {
                            model.LoanAmountExpertiseLast = oldLoanBrief.LoanAmountExpertiseLast;
                            model.LoanAmountExpertise = oldLoanBrief.LoanAmountExpertise;
                            model.LoanAmountExpertiseAi = oldLoanBrief.LoanAmountExpertiseAi;
                            if (model.LoanAmountExpertiseLast == 0 || !model.LoanAmountExpertiseLast.HasValue
                                || (model.LoanAmountExpertiseAi <= 0 && model.LoanAmountExpertise <= 0))
                            {
                                var newProduct = _productService.GetProduct(access_token, model.LoanBriefPropertyModel.ProductId.Value);
                                var priceAG = 0l;
                                decimal priceAI = 0l;
                                model.LoanAmountExpertiseLast = GetMaxPriceProduct((int)newProduct.Id, (int)model.LoanBriefResidentModel.ResidentType, (int)model.ProductId, model.LoanBriefId, ref priceAG, ref priceAI);
                                model.LoanAmountExpertise = priceAG;
                                if (priceAI > 0)
                                    model.LoanAmountExpertiseAi = priceAI;
                                if (model.LoanAmountExpertiseLast > 0)
                                {
                                    var task1 = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                                    {
                                        LoanBriefId = model.LoanBriefId,
                                        Note = string.Format("Kịch bản: Xe của khách hàng được định giá tối đa {0}đ", model.LoanAmountExpertiseLast.Value.ToString("##,#")),
                                        FullName = user.FullName,
                                        Status = 1,
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        CreatedTime = DateTime.Now,
                                        UserId = user.UserId,
                                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                                    }));
                                    taskRun.Add(task1);
                                }
                            }
                        }
                    }
                }
                model.LoanAmountFirst = oldLoanBrief.LoanAmountFirst > 0 ? oldLoanBrief.LoanAmountFirst : model.LoanAmount;
                //check lại qlf cũ
                if (oldLoanBrief.LoanBriefQuestionScript != null)
                {
                    //Tính lại qlf
                    var item = oldLoanBrief.LoanBriefQuestionScript;
                    // có nhu cầu vay
                    if (model.LoanBriefQuestionScriptModel == null || model.LoanBriefQuestionScriptModel.QuestionBorrow == null)
                    {
                        if (item.QuestionBorrow == true)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.NeedLoan;
                        }
                    }

                    //Có xe máy
                    if (model.LoanBriefQuestionScriptModel == null || model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo == null)
                    {
                        if (item.QuestionUseMotobikeGo == true)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.HaveMotobike;
                        }
                    }
                    //Có đăng ký xe bản gốc
                    if (model.LoanBriefQuestionScriptModel == null || model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate == null)
                    {
                        if (item.QuestionMotobikeCertificate == true)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.HaveOriginalVehicleRegistration;
                        }
                    }

                    model.LoanBriefQuestionScriptModel.QuestionBorrow = model.LoanBriefQuestionScriptModel.QuestionBorrow == null ? oldLoanBrief.LoanBriefQuestionScript.QuestionBorrow : model.LoanBriefQuestionScriptModel.QuestionBorrow;
                    model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo = model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo == null ? oldLoanBrief.LoanBriefQuestionScript.QuestionUseMotobikeGo : model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo;
                    model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate = model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate == null ? oldLoanBrief.LoanBriefQuestionScript.QuestionMotobikeCertificate : model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate;
                }

                var resultUpdate = await _loanBriefService.UpdateScriptTelesale(GetToken(), model.MapToLoanbrief(), model.LoanBriefId);
                if (resultUpdate > 0)
                {

                    var loanbriefHistory = new LoanBriefHistory()
                    {
                        LoanBriefId = model.LoanBriefId,
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ActorId = EnumActor.UpdateScript.GetHashCode(),
                        OldValue = JsonConvert.SerializeObject(oldLoanBrief.MapToLoanbriefViewModel(), Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        }),
                        NewValue = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        })
                    };
                    //Lưu log lịch sử chuyển 
                    taskRun.Add(Task.Run(() => _loanBriefService.AddLoanBriefHistory(access_token, loanbriefHistory)));
                    //Kiểm tra số tiền thay đổi => lưu comment
                    if (model.LoanAmount != oldLoanBrief.LoanAmount && model.LoanAmount > 0 && oldLoanBrief.LoanAmount > 0)
                    {
                        var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = resultUpdate,
                            Note = string.Format("Kịch bản: {0} thay đổi số tiền từ {1}đ thành {2}đ", user.FullName, oldLoanBrief.LoanAmount.Value.ToString("#,##"), model.LoanAmount.ToString("#,##")),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(task);
                    }
                    //Thêm note
                    if (!string.IsNullOrEmpty(model.Comment))
                    {
                        var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = resultUpdate,
                            Note = model.Comment,
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(task);
                    }
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    var msg = string.Format("Cập nhật thành công đơn vay HĐ-{0}", resultUpdate);

                    return Json(new { status = 1, message = msg });
                }
                else
                {
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateLoanBriefScriptV2(LoanbriefScriptViewModel model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var messageValid = ValidLoanbriefScriptViewModel(model);
                if (!string.IsNullOrEmpty(messageValid))
                    return Json(new { status = 0, message = messageValid });

                //mặc định thời gian vay là 12 tháng
                model.LoanTime = 12;
                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                if (model.LoanTime > 0)
                    model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);

                #region Xử lý phần qlf
                if (model.ValueCheckQualify == null)
                    model.ValueCheckQualify = 0;
                if (model.LoanBriefQuestionScriptModel != null)
                {
                    var item = model.LoanBriefQuestionScriptModel;
                    // có nhu cầu vay
                    if (item.QuestionBorrow == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.NeedLoan;
                    }
                    //Có xe máy
                    if (item.QuestionUseMotobikeGo == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveMotobike;
                    }
                    //Có đăng ký xe bản gốc
                    if (item.QuestionMotobikeCertificate == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveOriginalVehicleRegistration;
                    }
                }
                //Trong độ tuổi cho vay (18 - 65 tuổi)
                if (!string.IsNullOrEmpty(model.sBirthDay))
                {
                    var Dob = DateTimeUtility.ConvertStringToDate(model.sBirthDay, "dd/MM/yyyy");
                    if (Dob != null)
                    {
                        var dateNowYear = DateTime.Now.Year;
                        var dobYear = Dob.Year;
                        var yearOld = dateNowYear - dobYear;
                        if (yearOld >= 18 && yearOld <= 65)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.InAge;
                        }
                    }

                }
                //Trong khu vực hỗ trợ HN-HCM
                if (model.DistrictId > 0)
                {
                    var district = _dictionaryService.GetDistrictById(GetToken(), model.DistrictId.Value);
                    if (district != null && district.IsApply == 1)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.InAreaSupport;
                    }
                }
                //upload chứng từ
                var loanbriefFiles = _loanBriefService.GetLoanBriefFileByLoanId(GetToken(), model.LoanBriefId);
                if (loanbriefFiles != null && loanbriefFiles.Count > 0)
                {
                    if (loanbriefFiles.Count(x => x.UserId == 0) >= 4)
                        model.ValueCheckQualify += (int)EnumCheckQualify.UploadImage;
                }

                #endregion
                //mặc định gói xe máy là lắp định vị
                model.IsLocate = true;

                //Xe máy chính chủ
                model.ProductId = model.OwnerProduct == 1 ? EnumProductCredit.MotorCreditType_CC.GetHashCode() : EnumProductCredit.MotorCreditType_KCC.GetHashCode();

                //Check đơn đang xử lý
                var listLoanBrief = _loanBriefService.GetLoanBriefProcessing(GetToken(), model.Phone, model.NationalCard, model.LoanBriefId);
                if (listLoanBrief != null && listLoanBrief.LoanBriefId > 0)
                    return Json(GetBaseObjectResult(false, $"Khách hàng có đơn vay đang xử lý HĐ-{listLoanBrief.LoanBriefId}"));
                //Lấy thông tin đơn vay cũ
                var taskRun = new List<Task>();

                //định giá tài sản kH
                if (model.LoanBriefPropertyModel.ProductId.HasValue && model.LoanBriefResidentModel.ResidentType.HasValue)
                {
                    var newProduct = _productService.GetProduct(access_token, model.LoanBriefPropertyModel.ProductId.Value);
                    var priceAG = 0l;
                    decimal priceAI = 0l;
                    model.LoanAmountExpertiseLast = GetMaxPriceProduct((int)newProduct.Id, (int)model.LoanBriefResidentModel.ResidentType, (int)model.ProductId, model.LoanBriefId, ref priceAG, ref priceAI);
                    model.LoanAmountExpertise = priceAG;
                    if (priceAI > 0)
                        model.LoanAmountExpertiseAi = priceAI;
                    if (model.LoanAmountExpertiseLast > 0)
                    {
                        var task1 = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = model.LoanBriefId,
                            Note = string.Format("Kịch bản: Xe của khách hàng được định giá tối đa {0}đ", model.LoanAmountExpertiseLast.Value.ToString("##,#")),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        }));
                        taskRun.Add(task1);
                    }
                }
                model.TypeLoanBrief = LOS.Common.Extensions.LoanBriefType.Customer.GetHashCode();
                var entity = model.MapToLoanbrief();
                entity.ReMarketingLoanBriefId = model.ReMarketingLoanBriefId;
                entity.IsHeadOffice = true;
                entity.BoundTelesaleId = user.UserId;
                entity.CreateBy = user.UserId;
                entity.BuyInsurenceCustomer = true;
                entity.UtmSource = !string.IsNullOrEmpty(model.UtmSource) ? model.UtmSource : "rmkt_telesale_autocall";
                var resultUpdate = await _loanBriefService.CreateLoanbriefScript(GetToken(), entity);
                if (resultUpdate > 0)
                {
                    //Thêm note
                    var task = Task.Run(() => _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                    {
                        LoanBriefId = resultUpdate,
                        Note = "Kịch bản: Đơn vay được khởi tạo lại từ HĐ-" + entity.ReMarketingLoanBriefId,
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    }));
                    taskRun.Add(task);
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    var msg = string.Format("Tạo mới đơn vay thành công HĐ-{0}", resultUpdate);

                    return Json(new { status = 1, message = msg, data = resultUpdate });
                }
                else
                {
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    return Json(new { status = 0, message = "Xảy ra lỗi khi tạo mới đơn vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }

        #endregion

        #region Gửi yêu cầu AI
        [HttpPost]
        [PermissionFilter]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> RequestAIV2(LoanbriefScriptViewModel model)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var messageValid = ValidLoanbriefScriptViewModel(model);
                if (!string.IsNullOrEmpty(messageValid))
                    return Json(new { status = 0, message = messageValid });

                //Xe máy chính chủ
                model.ProductId = model.OwnerProduct == 1 ? EnumProductCredit.MotorCreditType_CC.GetHashCode() : EnumProductCredit.MotorCreditType_KCC.GetHashCode();
                var loanbrief = await _loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);
                if (loanbrief == null)
                    return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay!" });
                model.LoanAmountExpertiseLast = loanbrief.LoanAmountExpertiseLast;
                model.LoanAmountExpertise = loanbrief.LoanAmountExpertise;
                model.LoanAmountExpertiseAi = loanbrief.LoanAmountExpertiseAi;
                model.ValueCheckQualify = loanbrief.ValueCheckQualify;
                //check lại qlf cũ
                if (loanbrief.LoanBriefQuestionScript != null)
                {
                    model.LoanBriefQuestionScriptModel.QuestionBorrow = model.LoanBriefQuestionScriptModel.QuestionBorrow == null ? loanbrief.LoanBriefQuestionScript.QuestionBorrow : model.LoanBriefQuestionScriptModel.QuestionBorrow;
                    model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo = model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo == null ? loanbrief.LoanBriefQuestionScript.QuestionUseMotobikeGo : model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo;
                    model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate = model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate == null ? loanbrief.LoanBriefQuestionScript.QuestionMotobikeCertificate : model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate;
                }
                //mặc định mua bảo hiểm
                model.BuyInsurenceCustomer = true;
                model.LoanAmountFirst = loanbrief.LoanAmountFirst > 0 ? loanbrief.LoanAmountFirst : model.LoanAmount;
                var resultUpdate = await _loanBriefService.UpdateScriptTelesale(GetToken(), model.MapToLoanbrief(), model.LoanBriefId);
                if (resultUpdate > 0)
                {

                    //Thêm yêu cầu lấy dữ liệu rule check
                    var ObjRequestRuleCheck = new LogLoanInfoAi()
                    {
                        LoanbriefId = model.LoanBriefId,
                        ServiceType = (int)ServiceTypeAI.RuleCheck,
                        CreatedAt = DateTime.Now,
                        IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest
                    };
                    _logReqestAiService.Create(access_token, ObjRequestRuleCheck);
                    //Thêm yêu cầu lấy dữ liệu location
                    if (_networkService.CheckHomeNetwok(model.Phone) == (int)HomeNetWorkMobile.Viettel || _networkService.CheckHomeNetwok(model.Phone) == (int)HomeNetWorkMobile.Mobi)
                    {
                        var ObjRequestLocation = new LogLoanInfoAi()
                        {
                            LoanbriefId = model.LoanBriefId,
                            ServiceType = (int)ServiceTypeAI.Location,
                            CreatedAt = DateTime.Now,
                            IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest
                        };
                        _logReqestAiService.Create(access_token, ObjRequestLocation);
                    }
                    _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = string.Format("TELESALE {0} gửi yêu cầu lấy dữ liệu AI thành công", user.Username),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                    return Json(new { status = 1, message = "Gửi yêu cầu lấy dữ liệu AI thành công" });
                }
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });

            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region Gửi yêu cầu lấy Refphone
        [HttpPost]
        [PermissionFilter]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> RequestRefphoneV2(LoanbriefScriptViewModel model)
        {
            try
            {
                var objUpdateRefPhone = new UpdateRefPhoneLoanBriefRelationship();
                var user = GetUserGlobal();
                var access_token = user.Token;
                var messageValid = ValidLoanbriefScriptViewModel(model);
                if (!string.IsNullOrEmpty(messageValid))
                    return Json(new { status = 0, message = messageValid });


                //Xe máy chính chủ
                model.ProductId = model.OwnerProduct == 1 ? EnumProductCredit.MotorCreditType_CC.GetHashCode() : EnumProductCredit.MotorCreditType_KCC.GetHashCode();

                var ListLogRequestAi = _logReqestAiService.GetRequest(access_token, model.LoanBriefId);
                if (ListLogRequestAi.Count(x => x.ServiceType == (int)LOS.Common.Extensions.ServiceTypeAI.RefPhone) >= 3)
                    return Json(new { status = 0, message = "Bạn đã kiểm qua đủ 3 lần cho phép. Vui lòng thử lại sau!" });
                var loanbrief = await _loanBriefService.GetLoanBriefById(access_token, model.LoanBriefId);
                if (loanbrief == null)
                    return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay!" });
                if (ListLogRequestAi.Count(x => x.ServiceType == (int)LOS.Common.Extensions.ServiceTypeAI.RefPhone) > 0)
                {
                    if (model.LoanBriefRelationshipModels != null && model.LoanBriefRelationshipModels.Count > 0)
                    {

                        //Thay đổi cả 2 SĐT người thân
                        if (loanbrief.LoanBriefRelationship.ToList()[0].Phone != model.LoanBriefRelationshipModels.ToList()[0].Phone && loanbrief.LoanBriefRelationship.ToList()[1].Phone != model.LoanBriefRelationshipModels.ToList()[1].Phone)
                        {
                            objUpdateRefPhone.LoanBriefId = model.LoanBriefId;
                        }

                        if (loanbrief.LoanBriefRelationship.Count > 1)
                        {
                            //Thay đổi SĐT người thân 1
                            if (loanbrief.LoanBriefRelationship.ToList()[0].Phone != model.LoanBriefRelationshipModels.ToList()[0].Phone && loanbrief.LoanBriefRelationship.ToList()[1].Phone == model.LoanBriefRelationshipModels.ToList()[1].Phone)
                            {
                                objUpdateRefPhone.LoanBriefId = model.LoanBriefId;
                                objUpdateRefPhone.LoanBriefRelationshipId = loanbrief.LoanBriefRelationship.ToList()[0].Id;
                            }

                            //Thay đổi SĐT người thân 2
                            if (loanbrief.LoanBriefRelationship.ToList()[1].Phone != model.LoanBriefRelationshipModels.ToList()[1].Phone && loanbrief.LoanBriefRelationship.ToList()[0].Phone == model.LoanBriefRelationshipModels.ToList()[0].Phone)
                            {
                                objUpdateRefPhone.LoanBriefId = model.LoanBriefId;
                                objUpdateRefPhone.LoanBriefRelationshipId = loanbrief.LoanBriefRelationship.ToList()[1].Id;
                            }
                        }
                    }
                }
                model.LoanAmountExpertiseLast = loanbrief.LoanAmountExpertiseLast;
                model.LoanAmountExpertise = loanbrief.LoanAmountExpertise;
                model.LoanAmountExpertiseAi = loanbrief.LoanAmountExpertiseAi;
                model.LoanAmountFirst = loanbrief.LoanAmountFirst > 0 ? loanbrief.LoanAmountFirst : model.LoanAmount;
                //mặc định mua bảo hiểm
                model.BuyInsurenceCustomer = true;
                //check lại qlf cũ
                if (loanbrief.LoanBriefQuestionScript != null)
                {
                    model.LoanBriefQuestionScriptModel.QuestionBorrow = model.LoanBriefQuestionScriptModel.QuestionBorrow == null ? loanbrief.LoanBriefQuestionScript.QuestionBorrow : model.LoanBriefQuestionScriptModel.QuestionBorrow;
                    model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo = model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo == null ? loanbrief.LoanBriefQuestionScript.QuestionUseMotobikeGo : model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo;
                    model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate = model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate == null ? loanbrief.LoanBriefQuestionScript.QuestionMotobikeCertificate : model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate;
                }
                var resultUpdate = await _loanBriefService.UpdateScriptTelesale(GetToken(), model.MapToLoanbrief(), model.LoanBriefId);
                if (resultUpdate > 0)
                {
                    var taskRun = new List<Task>();
                    //Nếu sdt thay đổi => objUpdateRefPhone.LoanBriefId > 0
                    if (loanbrief.LoanBriefRelationship != null && loanbrief.LoanBriefRelationship.Count > 0)
                    {
                        //update lại 2 trường RefPhoneCallRate, RefPhoneDurationRate trong bảng LoanBriefRelationship về null
                        var t1 = Task.Run(() => _loanBriefService.UpdateRefPhoneIsNullRequestAI(access_token, objUpdateRefPhone));
                        taskRun.Add(t1);
                    }
                    // Thêm yêu cầu lấy dữ liệu RefPhone
                    var ObjRequestRefPhone = new LogLoanInfoAi()
                    {
                        LoanbriefId = model.LoanBriefId,
                        ServiceType = (int)ServiceTypeAI.RefPhone,
                        CreatedAt = DateTime.Now,
                        IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitRequest
                    };
                    var t2 = Task.Run(() => _logReqestAiService.Create(access_token, ObjRequestRefPhone));
                    taskRun.Add(t2);

                    var t3 = Task.Run(() => _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = model.LoanBriefId,
                        Note = string.Format("TELESALE {0} gửi yêu cầu lấy dữ liệu Refphone thành công", user.Username),
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    }));
                    taskRun.Add(t3);
                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());
                    return Json(new { status = 1, message = "Gửi yêu cầu lấy dữ liệu Refphone thành công" });
                }
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });

            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion
        private decimal GetMaxPriceProduct(int ProductId, int TypeOfOwnerShip, int productCredit, int LoanBriefId, ref long priceAG, ref decimal priceAi)
        {
            var maxPriceProduct = 0L;
            long originalCarPrice = 0;
            var access_token = GetToken();
            // lấy giá bên AI
            decimal priceCarAI = priceAi = _productService.GetPriceAI(access_token, ProductId, LoanBriefId);
            if (priceCarAI > 0)
            {
                originalCarPrice = (long)priceCarAI;
            }
            else
            {
                var productPriceCurrent = _productService.GetCurrentPrice(access_token, ProductId);
                if (productPriceCurrent != null && productPriceCurrent.PriceCurrent > 0)
                {
                    originalCarPrice = productPriceCurrent.PriceCurrent ?? 0l;
                    priceAG = productPriceCurrent.PriceCurrent.Value;
                }
            }
            maxPriceProduct = Common.Utils.ProductPriceUtils.GetMaxPrice(productCredit, originalCarPrice, TypeOfOwnerShip);

            return maxPriceProduct;
        }
        private bool CheckPermissionAction(List<UserActionPermission> ListPermissionAction, int LoanStatus, int Action)
        {
            //Kiêm tra quyền tương tác dữ liệu
            if (ListPermissionAction != null && ListPermissionAction.Any(x => x.LoanStatus == LoanStatus))
            {
                var permission = ListPermissionAction.FirstOrDefault(x => x.LoanStatus == LoanStatus);
                if ((Action & permission.Value) == Action)
                    return true;
            }
            return false;
        }

        #region Hủy đơn lưu lại thông tin kịch bản
        [HttpPost]
        [PermissionFilter]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> ScriptSaveInformationLoanBriefV2(LoanbriefScriptViewModel model)
        {
            try
            {
                var user = GetUserGlobal();

                //mặc định thời gian vay là 12 tháng
                model.LoanTime = 12;
                if (model.FromDate == null)
                    model.FromDate = DateTime.Now;
                if (model.LoanTime > 0)
                    model.ToDate = model.FromDate.Value.AddMonths((int)model.LoanTime);

                #region Xử lý phần qlf
                if (model.ValueCheckQualify == null)
                    model.ValueCheckQualify = 0;
                if (model.LoanBriefQuestionScriptModel != null)
                {
                    var item = model.LoanBriefQuestionScriptModel;
                    // có nhu cầu vay
                    if (item.QuestionBorrow == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.NeedLoan;
                    }
                    //Có xe máy
                    if (item.QuestionUseMotobikeGo == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveMotobike;
                    }
                    //Có đăng ký xe bản gốc
                    if (item.QuestionMotobikeCertificate == true)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.HaveOriginalVehicleRegistration;
                    }
                }
                //Trong độ tuổi cho vay (18 - 65 tuổi)
                if (!string.IsNullOrEmpty(model.sBirthDay))
                {
                    var Dob = DateTimeUtility.ConvertStringToDate(model.sBirthDay, "dd/MM/yyyy");
                    if (Dob != null)
                    {
                        var dateNowYear = DateTime.Now.Year;
                        var dobYear = Dob.Year;
                        var yearOld = dateNowYear - dobYear;
                        if (yearOld >= 18 && yearOld <= 65)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.InAge;
                        }
                    }

                }
                //Trong khu vực hỗ trợ HN-HCM
                if (model.DistrictId > 0)
                {
                    var district = _dictionaryService.GetDistrictById(GetToken(), model.DistrictId.Value);
                    if (district != null && district.IsApply == 1)
                    {
                        model.ValueCheckQualify += (int)EnumCheckQualify.InAreaSupport;
                    }
                }
                //upload chứng từ
                var loanbriefFiles = _loanBriefService.GetLoanBriefFileByLoanId(GetToken(), model.LoanBriefId);
                if (loanbriefFiles != null && loanbriefFiles.Count > 0)
                {
                    if (loanbriefFiles.Count(x => x.UserId == 0) >= 4)
                        model.ValueCheckQualify += (int)EnumCheckQualify.UploadImage;
                }

                #endregion
                var loanbrief = await _loanBriefService.GetLoanBriefById(user.Token, model.LoanBriefId);
                if (loanbrief == null)
                    return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay!" });
                model.LoanAmountExpertiseLast = loanbrief.LoanAmountExpertiseLast;
                model.LoanAmountExpertise = loanbrief.LoanAmountExpertise;
                model.LoanAmountExpertiseAi = loanbrief.LoanAmountExpertiseAi;
                model.LoanAmountFirst = loanbrief.LoanAmountFirst > 0 ? loanbrief.LoanAmountFirst : model.LoanAmount;
                //mặc định mua bảo hiểm
                model.BuyInsurenceCustomer = true;
                //check lại qlf cũ
                if (loanbrief.LoanBriefQuestionScript != null)
                {
                    //Tính lại qlf
                    var item = loanbrief.LoanBriefQuestionScript;
                    // có nhu cầu vay
                    if (model.LoanBriefQuestionScriptModel == null || model.LoanBriefQuestionScriptModel.QuestionBorrow == null)
                    {
                        if (item.QuestionBorrow == true)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.NeedLoan;
                        }
                    }

                    //Có xe máy
                    if (model.LoanBriefQuestionScriptModel == null || model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo == null)
                    {
                        if (item.QuestionUseMotobikeGo == true)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.HaveMotobike;
                        }
                    }
                    //Có đăng ký xe bản gốc
                    if (model.LoanBriefQuestionScriptModel == null || model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate == null)
                    {
                        if (item.QuestionMotobikeCertificate == true)
                        {
                            model.ValueCheckQualify += (int)EnumCheckQualify.HaveOriginalVehicleRegistration;
                        }
                    }

                    model.LoanBriefQuestionScriptModel.QuestionBorrow = model.LoanBriefQuestionScriptModel.QuestionBorrow == null ? loanbrief.LoanBriefQuestionScript.QuestionBorrow : model.LoanBriefQuestionScriptModel.QuestionBorrow;
                    model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo = model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo == null ? loanbrief.LoanBriefQuestionScript.QuestionUseMotobikeGo : model.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo;
                    model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate = model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate == null ? loanbrief.LoanBriefQuestionScript.QuestionMotobikeCertificate : model.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate;
                }

                //Xe máy chính chủ
                model.ProductId = model.OwnerProduct == 1 ? EnumProductCredit.MotorCreditType_CC.GetHashCode() : EnumProductCredit.MotorCreditType_KCC.GetHashCode();
                var resultUpdate = await _loanBriefService.UpdateScriptTelesale(GetToken(), model.MapToLoanbrief(), model.LoanBriefId);
                if (resultUpdate > 0)
                {
                    var msg = string.Format("Lưu lại đơn vay thành công", resultUpdate);

                    return Json(new { status = 1, message = msg });
                }
                else
                {
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật đơn vay. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }
        }
        #endregion

        #region forhub
        [HttpGet]
        [PermissionFilter]
        public async Task<IActionResult> HubEmployeeChange(int loanbriefId)
        {
            var user = GetUserGlobal();
            if (loanbriefId > 0)
            {
                var access_token = user.Token;
                var loanbrief = await _loanBriefService.GetLoanBriefById(access_token, loanbriefId);
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn vay đã được hủy trước đó.", null));

                    if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
                        return Json(GetBaseObjectResult(false, "Đơn đang được xử lý. Vui lòng thử lại sau!", null));

                    //Kiêm tra quyền tương tác dữ liệu
                    //if (!CheckPermissionAction(user.ListPermissionAction, loanbrief.Status.Value, EnumActionUser.PushLoan.GetHashCode()))
                    //{
                    //    if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                    //        return Json(new { status = 0, message = string.Format("Bạn không có quyền đẩy đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)) });
                    //    else
                    //        return Json(new { status = 0, message = "Bạn không có quyền đẩy đơn" });
                    //}
                    if (loanbrief.Status != EnumLoanStatus.APPRAISER_REVIEW.GetHashCode())
                    {
                        if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                            return Json(new { status = 0, message = string.Format("Bạn không có quyền đẩy đơn ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)) });
                        else
                            return Json(new { status = 0, message = "Bạn không có quyền chuyển đơn vay" });
                    }

                    var model = new DistributingLoanOfHubItem();
                    model.LoanBriefId = loanbriefId;
                    if (loanbrief.EvaluationHomeUserId > 0 && loanbrief.EvaluationCompanyUserId > 0)
                    {
                        model.Appraisal = LOS.Common.Extensions.EnumAppraisal.AppraisalHomeAndCompany.GetHashCode();
                        model.HubEmployeeId = loanbrief.EvaluationHomeUserId;
                    }
                    else if (loanbrief.EvaluationHomeUserId > 0)
                    {
                        model.Appraisal = LOS.Common.Extensions.EnumAppraisal.AppraisalHome.GetHashCode();
                        model.HubEmployeeId = loanbrief.EvaluationHomeUserId;
                    }
                    else if (loanbrief.EvaluationCompanyUserId > 0)
                    {
                        model.Appraisal = LOS.Common.Extensions.EnumAppraisal.AppraisalCompany.GetHashCode();
                        model.HubEmployeeId = loanbrief.EvaluationCompanyUserId;
                    }
                    else
                    {
                        model.Appraisal = LOS.Common.Extensions.EnumAppraisal.NoAppraisalHomeAndCompany.GetHashCode();
                        if (loanbrief.FieldHo > 0)
                            model.HubEmployeeId = loanbrief.FieldHo;
                        else
                            model.HubEmployeeId = loanbrief.HubEmployeeId;
                    }
                    //var userField = _userServices.GetById(access_token, model.HubEmployeeId.Value);
                    //thẩm định thực địa
                    if (user.GroupId == EnumGroupUser.ManagerHub.GetHashCode())
                    {
                        if (user.ShopGlobal > 0)
                            model.ListUserOfHub = _userServices.GetStaff(access_token, user.ShopGlobal);
                        var provinceId = EnumProvince.HaNoi.GetHashCode();
                        if (loanbrief.ProvinceId == EnumProvince.HCM.GetHashCode())
                            provinceId = EnumProvince.HCM.GetHashCode();

                        //var listField = _userServices.GetFieldHo(access_token, provinceId);
                        //if (listField != null && listField.Count > 0)
                        //{
                        //    model.ListUserOfHub.AddRange(listField.Select(x => new UserDetail()
                        //    {
                        //        UserId = x.UserId,
                        //        Username = x.Username + "(HO)"
                        //    }));
                        //}
                    }
                    //else if (userField.GroupId == EnumGroupUser.FieldSurvey.GetHashCode())
                    //{
                    //    model.ListUserOfHub = new List<UserDetail>();
                    //    model.ListUserOfHub.Add(new UserDetail()
                    //    {
                    //        UserId = userField.UserId,
                    //        Username = userField.Username + "(HO)"
                    //    });
                    //}
                    else
                    {
                        if (user != null)
                        {
                            model.ListUserOfHub = new List<UserDetail>();
                            model.ListUserOfHub.Add(new UserDetail()
                            {
                                UserId = user.UserId,
                                Username = user.Username
                            });
                        }
                    }
                    return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(model, "_HubEmployeeChange")));
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống", null));
                }
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay", null));
            }

        }

        [HttpPost]
        [PermissionFilter]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> HubEmployeeChange(int LoanBriefId, int StaffHub, int PlaceOfAppraisal)
        {
            var user = GetUserGlobal();
            if (LoanBriefId > 0)
            {
                var access_token = user.Token;
                var loanbrief = await _loanBriefService.GetLoanBriefById(access_token, LoanBriefId);
                if (loanbrief.Status == EnumLoanStatus.CANCELED.GetHashCode())
                    return Json(GetBaseObjectResult(false, "Đơn vay đã được hủy trước đó.", null));

                if (loanbrief.InProcess == EnumInProcess.Process.GetHashCode())
                    return Json(GetBaseObjectResult(false, "Đơn đang được xử lý. Vui lòng thử lại sau!", null));

                if (loanbrief.Status != EnumLoanStatus.APPRAISER_REVIEW.GetHashCode())
                {
                    if (Enum.IsDefined(typeof(EnumLoanStatus), loanbrief.Status))
                        return Json(new { status = 0, message = string.Format("Bạn không có quyền chuyển CVKD ở trạng thái {0}", ExtensionHelper.GetDescription((EnumLoanStatus)loanbrief.Status)) });
                    else
                        return Json(new { status = 0, message = "Bạn không có quyền đẩy đơn" });
                }
                if (loanbrief != null && loanbrief.LoanBriefId > 0)
                {
                    var userStaff = _userServices.GetById(access_token, StaffHub);
                    if (userStaff == null || userStaff.UserId == 0)
                        return Json(GetBaseObjectResult(false, "CVKD không tồn tại trong hệ thống!", null));

                    var prefMess = "[Chuyển CVKD]";
                    if (userStaff.GroupId == EnumGroupUser.FieldSurvey.GetHashCode())
                        prefMess = "[Chuyển TĐTĐ]";

                    var sNote = string.Empty;
                    //lấy trạng thái thẩm định cũ
                    var oldPlaceOfAppraisal = LOS.Common.Extensions.EnumAppraisal.NoAppraisalHomeAndCompany.GetHashCode();
                    if (loanbrief.EvaluationHomeUserId > 0 && loanbrief.EvaluationCompanyUserId > 0)
                        oldPlaceOfAppraisal = LOS.Common.Extensions.EnumAppraisal.AppraisalHomeAndCompany.GetHashCode();
                    else if (loanbrief.EvaluationHomeUserId > 0)
                        oldPlaceOfAppraisal = LOS.Common.Extensions.EnumAppraisal.AppraisalHome.GetHashCode();
                    else if (loanbrief.EvaluationCompanyUserId > 0)
                        oldPlaceOfAppraisal = LOS.Common.Extensions.EnumAppraisal.AppraisalCompany.GetHashCode();

                    var changeStaff = false;
                    var actionComment = EnumActionComment.CommentLoanBrief.GetHashCode();
                    //Kiểm tra xem có thay đổi CVKD
                    if (loanbrief.HubEmployeeId != userStaff.UserId)
                    {
                        changeStaff = true;
                        if (user.GroupId != EnumGroupUser.ManagerHub.GetHashCode())
                            return Json(new { status = 0, message = "Bạn không có quyền chuyển chuyên viên kinh doanh" });

                        actionComment = EnumActionComment.HubEmployeeChange.GetHashCode();
                        if (PlaceOfAppraisal == EnumAppraisal.AppraisalHome.GetHashCode())
                        {
                            loanbrief.EvaluationHomeUserId = StaffHub;
                            loanbrief.EvaluationHomeState = EnumFieldSurveyState.NotAppraised.GetHashCode();
                            loanbrief.EvaluationCompanyUserId = null;
                            loanbrief.EvaluationCompanyState = null;
                            sNote = string.Format("{0} {1} phải đi thẩm định nhà", prefMess, userStaff.FullName);
                        }
                        else if (PlaceOfAppraisal == EnumAppraisal.AppraisalCompany.GetHashCode())
                        {
                            loanbrief.EvaluationCompanyUserId = StaffHub;
                            loanbrief.EvaluationCompanyState = EnumFieldSurveyState.NotAppraised.GetHashCode();
                            loanbrief.EvaluationHomeUserId = null;
                            loanbrief.EvaluationHomeState = null;
                            sNote = string.Format("{0} {1} phải đi thẩm định công ty", prefMess, userStaff.FullName);
                        }
                        else if (PlaceOfAppraisal == EnumAppraisal.AppraisalHomeAndCompany.GetHashCode())
                        {
                            loanbrief.EvaluationHomeUserId = StaffHub;
                            loanbrief.EvaluationHomeState = EnumFieldSurveyState.NotAppraised.GetHashCode();
                            loanbrief.EvaluationCompanyUserId = StaffHub;
                            loanbrief.EvaluationCompanyState = EnumFieldSurveyState.NotAppraised.GetHashCode();
                            sNote = string.Format("{0} {1} phải đi thẩm định nhà và công ty", prefMess, userStaff.FullName);
                        }
                        else
                        {
                            sNote = string.Format("{0} {1} phải thẩm định xe. Không phải đi thẩm định nhà và công ty", prefMess, userStaff.FullName);
                        }
                        //
                    }
                    //Không thay đổi chuyên viên kịnh doanh
                    else
                    {
                        if (oldPlaceOfAppraisal != PlaceOfAppraisal)
                        {

                            if (PlaceOfAppraisal == EnumAppraisal.AppraisalHome.GetHashCode())
                            {
                                //Nếu là nhóm CVKD
                                //Không được chuyển từ TĐ Cty => TĐ nhà, TĐ cả 2 => thẩm định nhà
                                if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode()
                                    && (oldPlaceOfAppraisal == EnumAppraisal.AppraisalCompany.GetHashCode() || oldPlaceOfAppraisal == EnumAppraisal.AppraisalHomeAndCompany.GetHashCode()))
                                {
                                    return Json(new
                                    {
                                        status = 0,
                                        message = string.Format("Bạn không có quyền chuyển từ {0} sang {1}",
                                        ExtensionHelper.GetDescription((EnumAppraisal)oldPlaceOfAppraisal), ExtensionHelper.GetDescription(EnumAppraisal.AppraisalHome))
                                    });
                                }
                                loanbrief.EvaluationHomeUserId = StaffHub;
                                loanbrief.EvaluationHomeState = EnumFieldSurveyState.NotAppraised.GetHashCode();
                                loanbrief.EvaluationCompanyUserId = null;
                                loanbrief.EvaluationCompanyState = null;
                                sNote = string.Format("[Cập nhật] {0} phải đi thẩm định nhà", userStaff.FullName);
                            }
                            else if (PlaceOfAppraisal == EnumAppraisal.AppraisalCompany.GetHashCode())
                            {
                                //Nếu là nhóm CVKD
                                //Không được chuyển từ TĐ Nhà  => TĐ Cty, TĐ cả 2 => thẩm định Cty
                                if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode()
                                    && (oldPlaceOfAppraisal == EnumAppraisal.AppraisalHome.GetHashCode() || oldPlaceOfAppraisal == EnumAppraisal.AppraisalHomeAndCompany.GetHashCode()))
                                {
                                    return Json(new { status = 0, message = string.Format("Bạn không có quyền chuyển từ {0} sang {1}", ExtensionHelper.GetDescription((EnumAppraisal)oldPlaceOfAppraisal), ExtensionHelper.GetDescription(EnumAppraisal.AppraisalCompany)) });
                                }
                                loanbrief.EvaluationCompanyUserId = StaffHub;
                                loanbrief.EvaluationCompanyState = EnumFieldSurveyState.NotAppraised.GetHashCode();
                                loanbrief.EvaluationHomeUserId = null;
                                loanbrief.EvaluationHomeState = null;
                                sNote = string.Format("[Cập nhật] {0} phải đi thẩm định công ty", userStaff.FullName);
                            }
                            else if (PlaceOfAppraisal == EnumAppraisal.AppraisalHomeAndCompany.GetHashCode())
                            {
                                loanbrief.EvaluationHomeUserId = StaffHub;
                                loanbrief.EvaluationHomeState = EnumFieldSurveyState.NotAppraised.GetHashCode();
                                loanbrief.EvaluationCompanyUserId = StaffHub;
                                loanbrief.EvaluationCompanyState = EnumFieldSurveyState.NotAppraised.GetHashCode();
                                sNote = string.Format("[Cập nhật] {0} phải đi thẩm định nhà và công ty", userStaff.FullName);
                            }
                            else
                            {
                                //Nếu là nhóm CVKD
                                //Không được chuyển từ TĐ => không thẩm định
                                if (user.GroupId == EnumGroupUser.StaffHub.GetHashCode()
                                    && (oldPlaceOfAppraisal == EnumAppraisal.AppraisalHome.GetHashCode()
                                    || oldPlaceOfAppraisal == EnumAppraisal.AppraisalCompany.GetHashCode()
                                    || oldPlaceOfAppraisal == EnumAppraisal.AppraisalHomeAndCompany.GetHashCode()))
                                {
                                    return Json(new { status = 0, message = string.Format("Bạn không có quyền chuyển từ {0} sang {1}", ExtensionHelper.GetDescription((EnumAppraisal)oldPlaceOfAppraisal), ExtensionHelper.GetDescription(EnumAppraisal.NoAppraisalHomeAndCompany)) });
                                }
                                //if (loanbrief.FieldHo.HasValue)
                                //{
                                loanbrief.EvaluationHomeUserId = null;
                                loanbrief.EvaluationHomeState = null;
                                loanbrief.EvaluationCompanyUserId = null;
                                loanbrief.EvaluationCompanyState = null;
                                //}
                                sNote = string.Format("[Cập nhật] {0} phải thẩm định xe. Không phải đi thẩm định nhà và công ty", userStaff.FullName);
                            }
                        }
                        else
                        {
                            return Json(new { status = 0, message = "Vui lòng thay đổi hình thức thẩm định hoặc thay đổi người thẩm định" });
                        }
                    }
                    //loanbrief.HubEmployeeId = StaffHub;
                    if (userStaff.GroupId == EnumGroupUser.FieldSurvey.GetHashCode())
                        loanbrief.FieldHo = StaffHub;
                    else
                        loanbrief.HubEmployeeId = StaffHub;
                    var result = await _loanBriefService.HubEmployeeChange(access_token, loanbrief);
                    if (result > 0)
                    {
                        //Nếu có thay đổi cvkd => lưu log chuyển đơn
                        if (changeStaff)
                        {
                            //Thêm log chuyển cvkd 
                            await _loanBriefService.AddLogDistributionUser(GetToken(), new LogDistributionUser()
                            {
                                LoanbriefId = loanbrief.LoanBriefId,
                                UserId = userStaff.UserId,
                                HubId = loanbrief.HubId,
                                TypeDistribution = (int)TypeDistributionLog.CVKD,
                                CreatedBy = user.UserId,
                                CreatedAt = DateTime.Now
                            });
                        }
                        //Thêm note
                        _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                        {
                            LoanBriefId = result,
                            Note = sNote,
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = actionComment,
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        return Json(GetBaseObjectResult(true, "Phân đơn cho nhân viên xử lý thành công", null));
                    }
                    else
                    {
                        return Json(GetBaseObjectResult(false, "Xảy ra lỗi khi phân đơn cho nhân viên!", null));
                    }
                }
                else
                {
                    return Json(GetBaseObjectResult(false, "Đơn vay không tồn tại trong hệ thống!", null));
                }
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không có mã đơn vay trong hệ thống", null));
            }
        }
        #endregion

        public async Task<IActionResult> RemoveLabelReBorrow(int LoanBriefId, string CommentRemoveLabelReBorrow)
        {
            if (LoanBriefId < 0)
                return Json(new { status = 0, message = "Đơn vay không tồn tại !" });
            var user = GetUserGlobal();
            var access_token = user.Token;
            var oldLoanBrief = await _loanBriefService.GetLoanBriefById(access_token, LoanBriefId);
            if (oldLoanBrief == null)
                return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay trong hệ thống !" });

            var update = _loanBriefService.UpdateLabelReBorrow(access_token, new UpdateReBorrow()
            {
                LoanBriefId = LoanBriefId,
                IsReBorrow = null
            });
            if (update)
            {
                //Setup lại luồng đơn vay
                if (_pipelineService.ChangePipeline(access_token, new ChangePipelineReq() { loanBriefId = oldLoanBrief.LoanBriefId, productId = oldLoanBrief.ProductId.Value, status = oldLoanBrief.Status.Value }))
                {
                    //lưu lại vào bảng comment
                    _loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote()
                    {
                        LoanBriefId = LoanBriefId,
                        UserId = user.UserId,
                        FullName = user.FullName,
                        ActionComment = (int)EnumActionComment.RemoveLabelReBorrow,
                        Note = "Gỡ nhãn tán vay. <br /> Lí do: " + CommentRemoveLabelReBorrow,
                        Status = 1,
                        CreatedTime = DateTime.Now,
                        ShopId = user.ShopGlobal == 0 ? 3703 : user.ShopGlobal,
                        IsDisplay = true,
                        ShopName = user.ShopGlobalName == null ? "Tima" : user.ShopGlobalName
                    });
                    return Json(new { status = 1, message = "Gỡ nhãn tái vay thành công !" });
                }
                else
                {
                    _loanBriefService.UpdateLabelReBorrow(access_token, new UpdateReBorrow()
                    {
                        LoanBriefId = LoanBriefId,
                        IsReBorrow = true
                    });
                    return Json(new { status = 0, message = "Chưa chạy lại luồng của đơn vay !" });
                }
            }
            else
            {
                return Json(new { status = 0, message = "Lỗi. Vui lòng liên hệ với phòng kĩ thuật !" });
            }
        }

        public async Task<IActionResult> KeepSession()
        {
            return View();
        }

        public IActionResult GetOptionLTV(int ltv = 0, int productId = 0, bool isReborrow = false, int typeRemarketing = 0)
        {
            var user = GetUserGlobal();
            var isEdit = false;

            //Kiểm tra nếu là hub cho phép edit LTV
            if (user.GroupId == (int)EnumGroupUser.StaffHub || user.GroupId == (int)EnumGroupUser.ManagerHub)
                isEdit = true;
            //1 -> Chính chủ có quyền edit Topup/tái vay
            //2 -> Chính chủ đơn thường
            //3 -> KCC Topup/tái vay
            //4 -> KCC đơn thường
            var lstData = new List<LTVViewModel>();
            //Nếu TH đã có LTV và không được sửa => mặc định chọn chỉ option đó
            if (!isEdit && ltv > 0)
            {
                lstData.Add(new LTVViewModel
                {
                    Percent = ltv,
                    IsSelected = true
                });
            }
            else
            {
                var typeRemarketingLtv = 0;
                if (isReborrow)
                    typeRemarketingLtv = (int)TypeRemarketingLtv.Reborrow;
                else if (typeRemarketing == (int)EnumTypeRemarketing.IsTopUp)
                    typeRemarketingLtv = (int)TypeRemarketingLtv.TopUp;
                var percent = ProductPriceUtils.GetPercentLtv(productId, typeRemarketingLtv);
                lstData.Add(new LTVViewModel
                {
                    Percent = percent,
                    IsSelected = ltv == 0 || ltv == percent
                });
                if(typeRemarketingLtv != (int)TypeRemarketingLtv.Reborrow && typeRemarketingLtv != (int)TypeRemarketingLtv.TopUp)
                {
                    if (productId == (int)EnumProductCredit.MotorCreditType_CC && isEdit)
                        lstData.Add(new LTVViewModel { Percent = 80, IsSelected = ltv > 0 && ltv == 80 });
                    else if ((productId == (int)EnumProductCredit.MotorCreditType_KCC || productId == (int)EnumProductCredit.MotorCreditType_KGT) && isEdit)
                        lstData.Add(new LTVViewModel { Percent = 70, IsSelected = ltv > 0 && ltv == 70 });
                }
            }           
            return View(lstData);
        }
    }
}
