﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.Common.Models.Response;
using LOS.Common.Utils;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Request;
using LOS.WebApp.Services;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Serilog;

namespace LOS.WebApp.Controllers
{
    public class LoginController : Controller
    {
        private readonly ICareSoftService _careSoftService;
        private IUserServices _userServices;
        private IGroupServices _groupServices;
        private readonly ISSOService _ssoService;
        private readonly IHttpClientFactory _clientFactory;
        protected IConfiguration _configuration;
        public LoginController(IConfiguration configuration, ICareSoftService careSoftService, IUserServices userServices, IGroupServices groupServices, 
            ISSOService ssoService, IHttpClientFactory clientFactory)
        {
            _careSoftService = careSoftService;
            this._userServices = userServices;
            this._groupServices = groupServices;
            _ssoService = ssoService;
            _clientFactory = clientFactory;
            _configuration = configuration;
        }

        //[Route("login.html")]
        //public IActionResult Index()
        //{
        //    // check if user logged in
        //    return View();
        //}

        [Route("logout.html")]
        public IActionResult Logout()
        {
            HttpContext.Session.SetObjectAsJson("USER_DATA", null);
            return Redirect(_configuration["AppSettings:SSO"]);
        }

        [Route("redirect")]
        [HttpGet]
        public async Task<IActionResult> RedirectIndex(string token)
        {
            try
            {
                //Validate Token
                if(!ValidateCurrentToken(token))
                    return Redirect("~/login-error.html");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var ssoUserId = tokenS.Claims.First(claim => claim.Type == "UserId").Value;
                var httpClient = new HttpClient();
                var loginReq = new InfoLoginReq()
                {
                    UserId = Convert.ToInt32(ssoUserId)
                };
                 var response = await httpClient.PostAsJsonAsync(Constants.WEB_API_URL + Constants.GET_REDIRECT_LOGIN, loginReq);
                if (response.IsSuccessStatusCode)
                {
                    var sResult = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<DefaultResponse<UserDetail>>(sResult);
                    if (result.meta.errorCode == 200)
                    {
                        if (result.data.Status == 0 || result.data.IsDeleted == true)
                            return Redirect("~/login-error.html");
                        result.data.TokenSSO = token;

                        var permissionGroup = new GroupDetail();
                        if (result.data.Modules == null || result.data.Modules.Count == 0)
                        {
                            permissionGroup = _groupServices.GetPermission(result.data.Token, result.data.GroupId.Value);
                            if (permissionGroup != null && permissionGroup.Modules != null && permissionGroup.Modules.Count > 0)
                                result.data.Modules = permissionGroup.Modules;
                        }
                        //lấy permission user
                        var permission = _userServices.GetPermission(result.data.Token, result.data.UserId);
                        if (permission != null && permission.ListPermissionAction != null && permission.ListPermissionAction.Count > 0)
                            result.data.ListPermissionAction = permission.ListPermissionAction;
                        else
                        {
                            //get default permission group
                            if (result.data.GroupId > 0)
                            {
                                if (permissionGroup == null || permissionGroup.GroupId == 0)
                                    permissionGroup = _groupServices.GetPermission(result.data.Token, result.data.GroupId.Value);
                                if (permissionGroup != null && permissionGroup.ListPermissionAction != null && permissionGroup.ListPermissionAction.Count > 0)
                                    result.data.ListPermissionAction = permissionGroup.ListPermissionAction.Select(x => new DAL.EntityFramework.UserActionPermission()
                                    {
                                        LoanStatus = x.LoanStatus,
                                        Value = x.Value
                                    }).ToList();
                            }
                        }

                        if (result.data.ListShop != null && result.data.ListShop.Count > 0)
                        {
                            result.data.ShopGlobal = result.data.ListShop.First().ShopId;
                            result.data.ShopGlobalName = result.data.ListShop.First().Name;
                        }
                        // save access token					
                        HttpContext.Session.SetObjectAsJson("USER_DATA", result.data);
                        // redirect to each default path
                        if (result.data.Group.DefaultPath != null)
                        {
                            return Redirect(result.data.Group.DefaultPath);
                        }
                        else
                            return Redirect("/loanbrief/index.html");
                    }
                    else
                        return Redirect("~/login-error.html");
                }
                else
                    return Redirect("~/login-error.html");
            }
            catch (Exception ex)
            {
                return Redirect("~/login-error.html");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PostLogin(LoginReq obj)
        {
            try
            {
                using (var httpClient = _clientFactory.CreateClient())
                {
                    var loginReq = new LoginReq()
                    {
                        Username = obj.Username,
                        Password = Security.GetMD5Hash(obj.Password)
                    };
                    var response = await httpClient.PostAsJsonAsync(Constants.WEB_API_URL + Constants.LOGIN_ENDPOINT, loginReq);
                    if (response.IsSuccessStatusCode)
                    {
                        var sResult = await response.Content.ReadAsStringAsync();
                        var result = JsonConvert.DeserializeObject<DefaultResponse<UserDetail>>(sResult);
                        if (result.meta.errorCode == 200)
                        {
                            if (result.data.Status == 0)// tk bị khóa
                                return Json(new { status = 0, message = "Tài khoản đã bị khóa" });

                            if (result.data.IsDeleted == true)// tk bị xóa
                                return Json(new { status = 0, message = "Tài khoản đã bị xóa" });

                            //Kiểm tra nếu là telesale thì đăng nhập vào caresoft
                            //if (result.data.GroupId == EnumGroupUser.Telesale.GetHashCode())
                            //{
                            //    if (result.data.Ipphone > 0)
                            //    {
                            //        var token = string.Empty;
                            //        var IsLogin = _careSoftService.LoginCareSoft(result.data.Token, result.data.UserId, result.data.Ipphone.ToString(), ref token);
                            //        if (!IsLogin)
                            //            return Json(new { status = 0, message = "Xảy ra lỗi khi đăng nhập Caresoft" });
                            //        result.data.TokenSmartDailer = token;
                            //    }
                            //}
                            var permissionGroup = new GroupDetail();
                            if (result.data.Modules == null || result.data.Modules.Count == 0)
                            {
                                permissionGroup = _groupServices.GetPermission(result.data.Token, result.data.GroupId.Value);
                                if (permissionGroup != null && permissionGroup.Modules != null && permissionGroup.Modules.Count > 0)
                                    result.data.Modules = permissionGroup.Modules;
                            }
                            //lấy permission user
                            var permission = _userServices.GetPermission(result.data.Token, result.data.UserId);
                            if (permission != null && permission.ListPermissionAction != null && permission.ListPermissionAction.Count > 0)
                                result.data.ListPermissionAction = permission.ListPermissionAction;
                            else
                            {

                                //get default permission group
                                if (result.data.GroupId > 0)
                                {
                                    if (permissionGroup == null || permissionGroup.GroupId == 0)
                                        permissionGroup = _groupServices.GetPermission(result.data.Token, result.data.GroupId.Value);
                                    if (permissionGroup != null && permissionGroup.ListPermissionAction != null && permissionGroup.ListPermissionAction.Count > 0)
                                        result.data.ListPermissionAction = permissionGroup.ListPermissionAction.Select(x => new DAL.EntityFramework.UserActionPermission()
                                        {
                                            LoanStatus = x.LoanStatus,
                                            Value = x.Value
                                        }).ToList();
                                }
                            }
                            if (result.data.ListShop != null && result.data.ListShop.Count > 0)
                            {
                                result.data.ShopGlobal = result.data.ListShop.First().ShopId;
                                result.data.ShopGlobalName = result.data.ListShop.First().Name;
                            }
                            // save access token					
                            HttpContext.Session.SetObjectAsJson("USER_DATA", result.data);
                            await HttpContext.Session.CommitAsync();
                            // redirect to each default path
                            if (result.data.Group.DefaultPath != null)
                            {
                                //return Redirect(result.data.Group.DefaultPath);
                                return Json(new { status = 1, message = result.data.Group.DefaultPath });
                            }
                            else
                                //return Redirect("~/home.html");
                                return Json(new { status = 1, message = "loanbrief/index.html" });
                        }
                        if (result.meta != null && !string.IsNullOrEmpty(result.meta.errorMessage))
                            return Json(new { status = 0, message = result.meta.errorMessage });
                        return Json(new { status = 0, message = "Thông tin tài khoản không đúng" });
                    }
                    else
                    {
                        return Json(new { status = 0, message = "Xảy ra lỗi khi đăng nhập. Vui lòng thử lại" });
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Information(ex.Message + "\n" + ex.StackTrace);
                return Json(new { status = 0, message = "Xảy ra lỗi khi đăng nhập. Vui lòng thử lại." });
            }
        }

        private bool ValidateCurrentToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = _configuration["JWT:Issuer"],
                    ValidAudience = _configuration["JWT:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:SigningKey"]))
                }, out SecurityToken validatedToken);
            }
            catch
            {
                return false;
            }
            return true;
        }

    }
}