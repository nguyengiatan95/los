﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.ManagerAreaHub;
using LOS.WebApp.Services;
using LOS.WebApp.Services.ManagerAreaHub;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class ManagerAreaHubController : BaseController
    {
        private IConfiguration _baseConfig;
        private IManagerAreaHubService _managerareahubService;
        private IDictionaryService _dictionaryService;
        private readonly IMapper _mapper;
        private IHttpClientFactory clientFactory;
        public ManagerAreaHubController(IConfiguration configuration, IManagerAreaHubService managerareahubService, IDictionaryService dictionaryService, IMapper mapper, IHttpClientFactory clientFactory)
          : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
            _managerareahubService = managerareahubService;
            _dictionaryService = dictionaryService;
            _mapper = mapper;
            this.clientFactory = clientFactory;
        }
        [Route("module-managerareahub.html")]
        public IActionResult Index()
        {
            ManagerAreaHubModel model = new ManagerAreaHubModel();
            var listProvince = _dictionaryService.GetProvince(GetToken());
            model.listProvince = listProvince;

            var listproduct = _dictionaryService.GetLoanProduct(GetToken());
            model.listProduct = listproduct;

            var listShop = _dictionaryService.GetShop(GetToken());
            model.listShop = listShop;

            return View(model);
        }
        [HttpPost]
        public IActionResult GetDataDistrictManagerAreaHub(int ctiyId, int hubid, int productid,int checkall =0)
        {

           
            ManagerAreaHubDetailsModel model = new ManagerAreaHubDetailsModel();
            model.checkall = checkall;
            model.lstDistrit = new List<SelectItem>();
            var lstWard = new List<SelectItem>();
            var lstDistrict = new List<int>();

            var _listDistrict = _dictionaryService.GetDistrict(GetToken(), ctiyId);
            _listDistrict = _listDistrict.OrderBy(m => m.Name).ToList();
            var listManagerAreaHub = new List<ManagerAreaHub>();
            listManagerAreaHub = _managerareahubService.GetManagerAreaHub(GetToken(), ctiyId, hubid, productid);
            //list quan huyen
            if (_listDistrict != null && _listDistrict.Count > 0)
            {
                model.lstDistrit = _listDistrict.Select(x => new SelectItem()
                {
                    Id = x.DistrictId,
                    Name = x.Name,
                    Selected = checkall == 1 ? true : listManagerAreaHub.Any(y => y.DistrictId == x.DistrictId)
                }).ToList();

            }
            if (checkall == 1)
            {
                lstDistrict.AddRange(_listDistrict.Select(x => x.DistrictId).ToList());
            }
            else
            {
                lstDistrict.AddRange(listManagerAreaHub.Select(x => x.DistrictId).Distinct().ToList());
            }
            var dicWart = new Dictionary<string, List<Ward>>();
            foreach (var item in lstDistrict)
            {
                var _listWard = _dictionaryService.GetWard(GetToken(), item);
                var lst = _listWard.Select(x => new SelectItem()
                {
                    Id = x.WardId,
                    Name = x.Name,
                    DistrictId = item,
                    DistrictName = _listDistrict.FirstOrDefault(x => x.DistrictId == item).Name,
                    Selected = checkall ==1 ? true : listManagerAreaHub.Any(y => y.WardId == x.WardId)
                }).ToList();

                lstWard.AddRange(lst);
            }

            model.lstWard = lstWard;
            //return Json(new { Data = model, status = 1, message = "Lây thông tin thành công" });
            return View(model);
        }


        public IActionResult GetWardByDistrict(int ctiyId, int hubid, int productid, int districtid)
        {
            List<JSTreeModel> lstPermission = new List<JSTreeModel>();
            ManagerAreaHubDetailsModel model = new ManagerAreaHubDetailsModel();
            model.lstDistrit = new List<SelectItem>();
            var lstWard = new List<SelectItem>();

            var _listDistrict = _dictionaryService.GetDistrict(GetToken(), ctiyId);
            _listDistrict = _listDistrict.OrderBy(m => m.DistrictId).ToList();

            //list caus hinh theo ctiyId, hubid, productid
            var listManagerAreaHub = new List<ManagerAreaHub>();
            listManagerAreaHub = _managerareahubService.GetManagerAreaHub(GetToken(), ctiyId, hubid, productid);

            var listDistrict = listManagerAreaHub.Select(x => x.DistrictId).Distinct().ToList();

            var _listWard = _dictionaryService.GetWard(GetToken(), districtid);

            var lst = _listWard.Select(x => new SelectItem()
            {
                Id = x.WardId,
                Name = x.Name,
                DistrictId = districtid,
                DistrictName = _listDistrict.FirstOrDefault(x => x.DistrictId == districtid).Name,
                Selected = listManagerAreaHub.Any(y => y.WardId == x.WardId)
            }).ToList();

            lstWard.AddRange(lst);

            model.lstWard = lstWard;
            //return Json(new { Data = model, status = 1, message = "Lây thông tin thành công" });
            return View(model);
        }
        [RequestFormSizeLimit(valueCountLimit: 2000)]
        public IActionResult SaveSpicesAgency(int CityId, int Shopid, int ProductId, List<DistrictAndWardItem> ListWardId)
        {
            ManagerAreaHubSaveItem obj = new ManagerAreaHubSaveItem();
            obj.CityId = CityId;
            obj.Shopid = Shopid;
            obj.ProductId = ProductId;
            obj.ListWardId = ListWardId;
            var result = _managerareahubService.SaveSpicesAgency(GetToken(), obj);
            if (result)
            {
                return Json(new { status = 1, message = "Thành công" });
            }

            return Json(new { status = 0, message = "Lỗi." });
        }
    }
}