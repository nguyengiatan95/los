﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.Response;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LOS.WebApp.Controllers
{
	[AuthorizeFilter]
	public class MarketingReportController : Controller
	{
		[Route("theonguon.html")]
		public IActionResult Index()
		{
			return View();
		}

		public async Task<IActionResult> LoadData()
		{
			try
			{
				//var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
				//NameValueCollection query = new NameValueCollection();
				//// Skip number of Rows count
				//var start = Request.Form["pagination[page]"].FirstOrDefault();
				//if (start == "1")
				//    start = "0";
				//query.Add("skip", start);
				//// Paging Length 10,20
				//var length = Request.Form["pagination[perpage]"].FirstOrDefault();
				//query.Add("take", length);
				//// Sort Column Name
				//var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
				//if (!String.IsNullOrEmpty(sortColumn))
				//{
				//    query.Add("sortBy", sortColumn);
				//}
				//// Sort Column Direction (asc, desc)
				//var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
				//if (!String.IsNullOrEmpty(sortColumnDirection))
				//{
				//    query.Add("sortOrder", sortColumnDirection);
				//}
				//// Search Value from (Search box)
				//var searchValue = Request.Form["query[filterName]"].FirstOrDefault();
				//if (!String.IsNullOrEmpty(searchValue))
				//{
				//    query.Add("name", searchValue);
				//}

				////Paging Size (10, 20, 50,100)
				//int pageSize = length != null ? Convert.ToInt32(length) : 0;
				//int skip = start != null ? Convert.ToInt32(start) : 0;
				//int recordsTotal = 0;

				// Query api
				var httpClient = new HttpClient();
				string access_token = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result.Token;
				httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
				string url = Constants.GET_MKTREPORT_ENDPOINT /*+ query.ToQueryString()*/;
				var response = await httpClient.GetAsync(url);
				List<MarketingReportUtmDTO> data = new List<MarketingReportUtmDTO>();
				if (response.IsSuccessStatusCode)
				{
					var sResult = await response.Content.ReadAsStringAsync();
					var result = JsonConvert.DeserializeObject<DefaultResponse<Meta, List<MarketingReportUtmDTO>>>(sResult);
					if (result.meta.errorCode == 200)
					{
						if (result.data.Count > 0)
						{
							//recordsTotal = result.meta.totalRecords;
							data = result.data;
						}
					}
				}

				//Returning Json Data
				//return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
				return Json(new { data = data });
			}
			catch (Exception)
			{
				throw;
			}
		}


		public async Task<IActionResult> LoadData2()
		{
			try
			{
				var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
				NameValueCollection query = new NameValueCollection();
				// Skip number of Rows count
				var start = Request.Form["pagination[page]"].FirstOrDefault();
				if (start == "1")
					start = "0";
				query.Add("skip", start);
				// Paging Length 10,20
				var length = Request.Form["pagination[perpage]"].FirstOrDefault();
				query.Add("take", length);
				// Sort Column Name
				var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
				if (!String.IsNullOrEmpty(sortColumn))
				{
					query.Add("sortBy", sortColumn);
				}
				// Sort Column Direction (asc, desc)
				var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
				if (!String.IsNullOrEmpty(sortColumnDirection))
				{
					query.Add("sortOrder", sortColumnDirection);
				}
				// Search Value from (Search box)
				var searchValue = Request.Form["query[filterName]"].FirstOrDefault();
				if (!String.IsNullOrEmpty(searchValue))
				{
					query.Add("name", searchValue);
				}

				var fromDate = Request.Form["query[fromDate]"].FirstOrDefault();
				if (!String.IsNullOrEmpty(fromDate))
				{
					query.Add("fromDate", fromDate);
				}

				var toDate = Request.Form["query[toDate]"].FirstOrDefault();
				if (!String.IsNullOrEmpty(toDate))
				{
					query.Add("toDate", toDate);
				}

				//Paging Size (10, 20, 50,100)
				int pageSize = length != null ? Convert.ToInt32(length) : 0;
				int skip = start != null ? Convert.ToInt32(start) : 0;
				int recordsTotal = 0;

				// Query api
				var httpClient = new HttpClient();
				string access_token = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result.Token;
				httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "bearer " + access_token);
				string url = Constants.GET_MKTREPORT_ENDPOINT + query.ToQueryString();
				var response = await httpClient.GetAsync(url);
				List<MarketingReportUtmDTO> data = new List<MarketingReportUtmDTO>();
				if (response.IsSuccessStatusCode)
				{
					var sResult = await response.Content.ReadAsStringAsync();
					var result = JsonConvert.DeserializeObject<DefaultResponse<SummaryMeta, List<MarketingReportUtmDTO>>>(sResult);
					if (result.meta.errorCode == 200)
					{
						if (result.data.Count > 0)
						{
							recordsTotal = result.meta.totalRecords;
							data = result.data;
						}
					}
				}

				//Returning Json Data
				return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
				//return Json(new { data = data });
			}
			catch (Exception)
			{
				throw;
			}
		}
	}
}