﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Response;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class ModuleController : BaseController
    {
        private IConfiguration _baseConfig;
        private IGroupServices _groupServices;
        private IModuleService _moduleServices;
        private IUserServices _userServices;
        private readonly IMapper _mapper;
        private IHttpClientFactory clientFactory;
        public ModuleController(IModuleService moduleServices, IUserServices userServices, IGroupServices groupServices, IConfiguration configuration, IMapper mapper, IHttpClientFactory clientFactory)
          : base(configuration, clientFactory)
        {
            _moduleServices = moduleServices;
            _baseConfig = configuration;
            _userServices = userServices;
            _groupServices = groupServices;
            _mapper = mapper;
            this.clientFactory = clientFactory;
        }

        [Route("module-manager.html")]
        [PermissionFilter]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> LoadData()
        {
            try
            {

                var modal = new ModuleDatatable(HttpContext.Request.Form);
                int recordsTotal = 0;
                // Query api   	
                List<ModuleDetail> data = new List<ModuleDetail>();
                data = _moduleServices.Search(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult CreateModuleModal(int Id)
        {
            var entity = new ModuleItem();
            var _listModule = _moduleServices.GetAllMenu(GetToken(), (int)Common.Extensions.EnumApplication.WebLOS);
            var _lstAplication = ExtensionHelper.GetEnumToList(typeof(EnumApplication));
            if (Id == 0)
            {
                entity.lstModule = _listModule;
                entity.LstAplication = _lstAplication;
                return View(entity);
            }
            else
            {
                // Query api   
                var module = _moduleServices.GetById(GetToken(), Id);
                entity = _mapper.Map<ModuleItem>(module);
                entity.lstModule = _listModule;
                entity.LstAplication = _lstAplication;
                return View(entity);
            }
        }

        public async Task<IActionResult> CreateModule(ModuleItem entity)
        {
            try
            {
                entity.Status = 1;
                entity.IsMenu = true;
                if (entity.ParentId == 0)
                    entity.ParentId = null;

                if (entity.CheckMenu == 0)
                {
                    entity.ParentId = null;
                    entity.IsMenu = false;
                    //kiểm tra trùng khi tạo aciton
                    var module = _moduleServices.Get(GetToken(), entity.Controller, entity.Action);
                    if (module != null && module.ModuleId > 0)
                    {
                        if (entity.ModuleId > 0)
                        {
                            if (module.ModuleId != entity.ModuleId)
                                return Json(new { status = 0, message = string.Format("Đã tồn tại Action: {0}/{1}", entity.Controller, entity.Action) });
                        }
                        else
                        {
                            return Json(new { status = 0, message = string.Format("Đã tồn tại Action: {0}/{1}", entity.Controller, entity.Action) });
                        }
                    }
                }
                if (entity.ModuleId > 0)
                {
                    var obj = _mapper.Map<ModuleDTO>(entity);
                    var result = _moduleServices.Update(GetToken(), (int)entity.ModuleId, obj);

                    if (result)
                        return Json(new { status = 1, message = "Cập nhật module thành công" });
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật thông tin module. Vui lòng liên hệ quản trị viên!" });
                }
                else
                {
                    entity.ModuleId = null;
                    var obj = _mapper.Map<ModuleDTO>(entity);
                    var result = _moduleServices.Create(GetToken(), obj);

                    if (result)
                        return Json(new { status = 1, message = "Thêm mới module thành công" });
                    return Json(new { status = 0, message = "Xảy ra lỗi khi thêm mới module. Vui lòng liên hệ quản trị viên!" });
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IActionResult DeleteModule(int Id)
        {
            try
            {
                var checkdelete = _moduleServices.CheckDelete(GetToken(), Id);
                if (checkdelete == 1)
                {
                    return Json(new { status = 0, message = "Menu đã phân quyền user không thể xóa" });
                }
                else if (checkdelete == 2)
                {
                    return Json(new { status = 0, message = "Menu đã phân quyền  không thể xóa" });
                }
                else if (checkdelete == 3)
                {
                    return Json(new { status = 0, message = "Menu đã phân quyền trong nhóm  không thể xóa" });
                }
                else
                {
                    var result = _moduleServices.Delete(GetToken(), Id);
                    if (result)
                        return Json(new { status = 1, message = "Xóa module thành công" });
                    return Json(new { status = 0, message = "Xảy ra lỗi khi xóa module. Vui lòng liên hệ quản trị viên!" });
                }
                return Json(new { status = 0, message = "Xảy ra lỗi khi xóa module. Vui lòng liên hệ quản trị viên!" });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public IActionResult GetDataModule(int Id)
        {
            List<JSTreeModel> lstPermission = new List<JSTreeModel>();
            var _listModule = _moduleServices.GetAll(GetToken());
            _listModule = _listModule.OrderBy(m => m.ParentId).ToList();
            var ListModule = new List<ModuleDetail>();
            var groupDetail = _groupServices.GetById(GetToken(), Id);
            if (groupDetail != null && groupDetail.Modules != null)
                ListModule = groupDetail.Modules;
            foreach (var item in _listModule)
            {
                JSTreeModel objCurrentNode = new JSTreeModel();
                objCurrentNode.id = item.ModuleId.ToString();
                objCurrentNode.text = item.Name.ToString();
                //objCurrentNode.IsMenu = item.IsMenu.ToString();
                objCurrentNode.ControllerName = item.Controller == null ? item.Controller : "";
                objCurrentNode.ActionName = item.Action == null ? item.Action : "";

                if (Id > 0 && (ListModule.Any(x => x.ModuleId == item.ModuleId && item.ParentId != null) || ListModule.Any(x => x.ModuleId == item.ModuleId && item.IsMenu != true)))
                    objCurrentNode.state.selected = true;

                if (item.ParentId == null)
                    lstPermission.Add(objCurrentNode);
                else
                    AddCurrentNodeToParent(lstPermission, objCurrentNode, item.ParentId.ToString());
            }
            return Json(new { Data = lstPermission, status = 1, message = "Lây thông tin thành công" });
        }
        public IActionResult GetDataModuleByUser(int Id)
        {
            List<JSTreeModel> lstPermission = new List<JSTreeModel>();
            if (Id == 0)
                return Json(new { Data = lstPermission, status = 1, message = "Lây thông tin thành công" });

            var _listModule = _moduleServices.GetAll(GetToken());
            _listModule = _listModule.OrderBy(m => m.ParentId).ToList();
            var listPermission = new List<ModuleDetail>();
            var userInfo = _userServices.GetById(GetToken(), Id);
            if (userInfo != null && userInfo.Modules != null && userInfo.Modules.Count > 0)
            {
                listPermission = userInfo.Modules;
            }
            //nếu k có cấu hình theo user => lấy permisson group
            if (userInfo.GroupId > 0 && (listPermission == null || listPermission.Count == 0))
            {
                if (listPermission == null || listPermission.Count == 0)
                    listPermission = _moduleServices.GetByGroupId(GetToken(), userInfo.GroupId.Value, (int)EnumApplication.WebLOS);
            }
            foreach (var item in _listModule)
            {
                JSTreeModel objCurrentNode = new JSTreeModel();
                objCurrentNode.id = item.ModuleId.ToString();
                objCurrentNode.text = item.Name.ToString();
                objCurrentNode.ControllerName = item.Controller == null ? item.Controller : "";
                objCurrentNode.ActionName = item.Action == null ? item.Action : "";
                if (Id > 0)
                {
                    if (listPermission.Any(x => x.ModuleId == item.ModuleId && x.ParentId != null) || listPermission.Any(x => x.ModuleId == item.ModuleId && item.IsMenu != true))
                        objCurrentNode.state.selected = true;
                }
                if (item.ParentId == null)
                    lstPermission.Add(objCurrentNode);
                else
                    AddCurrentNodeToParent(lstPermission, objCurrentNode, item.ParentId.ToString());
            }
            return Json(new { Data = lstPermission, status = 1, message = "Lây thông tin thành công" });
        }
        private void AddCurrentNodeToParent(List<JSTreeModel> lstData, JSTreeModel objCurrentNode, string strParentID)
        {
            if (lstData != null && lstData.Count > 0)
            {
                for (int i = 0; i < lstData.Count; i++)
                {
                    if (lstData[i].id == strParentID)
                    {
                        if (lstData[i].children == null) lstData[i].children = new List<JSTreeModel>();
                        lstData[i].children.Add(objCurrentNode);
                        return;
                    }
                }
            }
        }
    }
}
