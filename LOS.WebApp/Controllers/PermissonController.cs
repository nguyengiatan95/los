﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class PermissonController : Controller
    {
        private IModuleService _moduleService;
        public PermissonController(IModuleService moduleService)
        {
            _moduleService = moduleService;
        }
        public IActionResult CheckPermisson(string sController, string sAction)
        {
            var user = GetUserGlobal();
            if (user != null)
            {
                if (!string.IsNullOrEmpty(sController) && !string.IsNullOrEmpty(sAction))
                {
                    var module = _moduleService.Get(user.Token, sController.AsToLower(), sAction.AsToLower());
                    if (module.ModuleId > 0)
                    {
                        if (!user.Modules.Any(x => x.ModuleId == module.ModuleId))
                            return Json(GetBaseObjectResult(false, string.Format("Bạn không có quyền truy cập dữ liệu {0}/{1}", sController, sAction), null));
                    }
                }
                return Json(GetBaseObjectResult(true, "Success", null));
            }
            else
            {
                return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin user", null));
            }

        }
        public virtual object GetBaseObjectResult(bool isSuccess = true, string msg = "", object data = null, string html = "")
        {
            return new
            {
                IsSuccess = isSuccess,
                Message = msg,
                Data = data,
                Html = html
            };
        }
        protected UserDetail GetUserGlobal()
        {
            return HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result;
        }

    }



}