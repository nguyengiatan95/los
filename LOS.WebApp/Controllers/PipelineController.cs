﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.Request;
using LOS.WebApp.Models.Request.Pipeline;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LOS.WebApp.Controllers
{
	public class PipelineController : BaseController
	{
		private IPipelineService pipelineService;
		private IHttpClientFactory clientFactory;

		public PipelineController(IConfiguration configuration, IPipelineService services, IHttpClientFactory clientFactory) : base(configuration: configuration, clientFactory)
		{
			this.pipelineService = services;
			this.clientFactory = clientFactory;
		}

		[Route("/pipeline/index.html")]
		public IActionResult Index()
		{
			try
			{
				ViewBag.Token = GetToken();
				var data = pipelineService.GetAll(GetToken());
				return View(data);
			}
			catch (Exception)
			{
				return View();
			}
		}
		
		public IActionResult CreateUpdatePipelinePartial([FromBody] IdOnlyReq req)
		{
			var entity = new PipelineDTO();
			if (req.id == 0)
			{
				return View("~/Views/Pipeline/Partial/CreateUpdatePipelinePartial.cshtml", entity);
			}
			else
			{
				// Query api   
				entity = pipelineService.GetById(GetToken(), req.id);
				return View("~/Views/Pipeline/Partial/CreateUpdatePipelinePartial.cshtml", entity);
			}
		}
		
		[HttpPost]
		public IActionResult CreateUpdateSectionPartial([FromBody] SectionDTO req)
		{		
			SectionDTO entity = new SectionDTO();
			if (req.SectionId == 0)
			{
				entity.PipelineId = req.PipelineId;
				return View("~/Views/Pipeline/Partial/CreateUpdateSectionPartial.cshtml", entity);
			}
			else
			{
				// Query api   
				entity = pipelineService.GetSectionById(GetToken(), req.SectionId);
				return View("~/Views/Pipeline/Partial/CreateUpdateSectionPartial.cshtml", entity);
			}
		}

		[HttpPost]
		public IActionResult CreateUpdatePipeline([FromBody] PipelineDTO pipeline)
		{
			if (pipeline.PipelineId > 0)
			{
				var result = pipelineService.UpdatePipeline(GetToken(), pipeline);
				return Ok(result);
			}
			else
			{
				var result = pipelineService.CreatePipeline(GetToken(), pipeline);
				return Ok(result);
			}
		}
		
		[HttpPost]
		public IActionResult CreateUpdateSection([FromBody] SectionDTO section)
		{
			if (section.SectionId > 0)
			{
				var result = pipelineService.UpdateSection(GetToken(), section);
				return Ok(result);
			}
			else
			{
				var result = pipelineService.CreateSection(GetToken(), section);
				return Ok(result);
			}
		}

		[HttpPost]
		public IActionResult DeleteSection([FromBody] IdOnlyReq req)
		{
			var result = pipelineService.DeleteSection(GetToken(), req.id);
			return Ok(result);
		}

		[HttpPost]
		public IActionResult ChangeStatus([FromBody] IdOnlyReq req)
		{
			var result = pipelineService.ChangeStatusPipeline(GetToken(), req.id);
			return Ok(result);
		}

		[HttpPost]
		public IActionResult DeletePipeline([FromBody] IdOnlyReq req)
		{		
			var result = pipelineService.DeletePipeline(GetToken(), req.id);
			return Ok(result);
		}

		public IActionResult DeleteSectionDetail([FromBody] DeleteSectionDetailReq req)
		{
			var result = pipelineService.DeleteSectionDetail(GetToken(), req.sectionId, req.detailId);
			return Ok(result);
		}

		public IActionResult GetAllSection()
		{
			var result = pipelineService.GetAllSections(GetToken());
			return Ok(result);
		}

		public IActionResult SectionDetailPartial([FromBody] SectionDetailReq req)
		{
			try
			{
				SectionDetailDTO dto = new SectionDetailDTO();
				dto.SectionId = req.sectionId;
				dto.PipelineId = req.pipelineId;
				ViewBag.Sections = pipelineService.GetAllSections(GetToken());
				ViewBag.Properties = pipelineService.GetAllProperty(GetToken());
				return View("~/Views/Pipeline/Partial/PipelineSectionPartial.cshtml", dto);
			}
			catch (Exception)
			{
				return View("~/Views/Pipeline/Partial/PipelineSectionPartial.cshtml");
			}
		}

		public IActionResult GetPropertyPartial([FromBody] IdOnlyReq req)
		{
			try
			{
				ViewBag.Token = GetToken();
				var data = pipelineService.GetPropertyById(GetToken(), req.id);				
				return View("~/Views/Pipeline/Partial/GetPropertyPartial.cshtml", data);
			}
			catch (Exception)
			{
				return View("~/Views/Pipeline/Partial/GetPropertyPartial.cshtml");
			}
		}

		public IActionResult EditPropertyPartial([FromBody] EditPropertyDTO req)
		{
			try
			{
				ViewBag.Token = GetToken();
				return View("~/Views/Pipeline/Partial/EditPropertyPartial.cshtml", req);
			}
			catch (Exception)
			{
				return View("~/Views/Pipeline/Partial/EditPropertyPartial.cshtml");
			}
		}

		public IActionResult ApproveSectionPartial([FromBody] IdOnlyReq req)
		{
			try
			{
				ViewBag.Sections = pipelineService.GetAllSections(GetToken());				
				ViewBag.Properties = pipelineService.GetAllProperty(GetToken());
				return View("~/Views/Pipeline/Partial/ApproveSectionPartial.cshtml", req);
			}
			catch (Exception)
			{
				return View("~/Views/Pipeline/Partial/ApproveSectionPartial.cshtml", req);
			}
		}

		public IActionResult DisapproveSectionPartial([FromBody] IdOnlyReq req)
		{
			try
			{
				ViewBag.Sections = pipelineService.GetAllSections(GetToken());
				ViewBag.Properties = pipelineService.GetAllProperty(GetToken());
				return View("~/Views/Pipeline/Partial/DisapproveSectionPartial.cshtml", req);
			}
			catch (Exception)
			{
				return View("~/Views/Pipeline/Partial/DisapproveSectionPartial.cshtml", req);
			}
		}

		public IActionResult InitialValuePropertyPartial([FromBody] IdOnlyReq req)
		{
			try
			{
				var data = pipelineService.GetAllProperty(GetToken());
				ViewBag.Properties = data;				
				return View("~/Views/Pipeline/Partial/InitialValuePropertyPartial.cshtml",req);
			}
			catch (Exception)
			{
				return View("~/Views/Pipeline/Partial/InitialValuePropertyPartial.cshtml");
			}
		}

		public IActionResult EditInitialValuePropertyPartial([FromBody] EditPropertyDTO req)
		{
			try
			{
				var data = pipelineService.GetAllProperty(GetToken());
				ViewBag.Properties = data;
				return View("~/Views/Pipeline/Partial/InitialValuePropertyPartial.cshtml", req);
			}
			catch (Exception)
			{
				return View("~/Views/Pipeline/Partial/InitialValuePropertyPartial.cshtml");
			}
		}

		public IActionResult ConditionValuePropertyPartial([FromBody] IdOnlyReq req)
		{
			try
			{
				var data = pipelineService.GetAllProperty(GetToken());
				ViewBag.Properties = data;				
				return View("~/Views/Pipeline/Partial/ConditionValuePropertyPartial.cshtml",req);
			}
			catch (Exception)
			{
				return View("~/Views/Pipeline/Partial/ConditionValuePropertyPartial.cshtml", req);
			}
		}

		public IActionResult DisapproveConditionValuePropertyPartial([FromBody] IdOnlyReq req)
		{
			try
			{
				var data = pipelineService.GetAllProperty(GetToken());
				ViewBag.Properties = data;
				return View("~/Views/Pipeline/Partial/DisapproveConditionValuePropertyPartial.cshtml", req);
			}
			catch (Exception)
			{
				return View("~/Views/Pipeline/Partial/DisapproveConditionValuePropertyPartial.cshtml", req);
			}
		}

		public IActionResult CreateSectionDetail([FromBody] AddSectionDetailReq req)
		{
			var result = pipelineService.CreateSectionDetail(GetToken(), req);
			return Ok(result);
		}

		public IActionResult UpdateSectionDetail([FromBody] AddSectionDetailReq req)
		{
			var result = pipelineService.UpdateSectionDetail(GetToken(), req);
			return Ok(result);
		}

		public IActionResult EditSectionDetailPartial([FromBody] IdOnlyReq req)
		{
			try
			{
				var detail = pipelineService.GetDetailById(GetToken(), req.id);
				ViewBag.Sections = pipelineService.GetAllSections(GetToken());
				ViewBag.Properties = pipelineService.GetAllProperty(GetToken());
				return View("~/Views/Pipeline/Partial/EditPipelineSectionPartial.cshtml", detail);
			}
			catch (Exception)
			{
				return View("~/Views/Pipeline/Partial/EditPipelineSectionPartial.cshtml");
			}
		}

		public IActionResult UpdateSectionOrder([FromBody] UpdateSectionOrderReq req)
		{
			var result = pipelineService.UpdateSectionOrder(GetToken(), req);
			return Ok(result);
		}
		[Route("/flowchart/index.html")]
		public IActionResult Flowchart()
		{
			try
			{
				var data = pipelineService.GetAll(GetToken());
				return View(data);
			}
			catch (Exception)
			{
				return View();
			}
		}
	}
}