﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using LOS.WebApp.Services;
using LOS.Common.Extensions;

namespace LOS.WebApp.Controllers
{
    public class PrintController : BaseController
    {
        private IConfiguration _configuration;
        private IHttpClientFactory _clientFactory;
        private ILoanBriefService _loanBriefService;

        public PrintController(IConfiguration configuration, IHttpClientFactory clientFactory,
             ILoanBriefService loanService) : base(configuration, clientFactory)
        {
            this._configuration = configuration;
            this._clientFactory = clientFactory;
            this._loanBriefService = loanService;
        }

        public async Task<IActionResult> ContractFinancialAgreement(int loanbriefId)
        {
            var data = await _loanBriefService.GetContractFinancialAgreement(GetToken(), loanbriefId);
            if (!string.IsNullOrEmpty(data.FullName))
            {
                data.FullName = data.FullName.ReduceWhitespace();
                data.FullName = data.FullName.TitleCaseString();
                data.FullName = data.FullName.Replace(" ", "").ToUpper(); // xóa hết khoảng trắng và viết hoa tất cả các chữ cái
            }
            if (data.Dob.HasValue && data.Dob != DateTime.MinValue)
            {
                data.strDob = data.Dob.Value.ToString("ddMMyyyy");
                data.strDob = data.strDob.Trim().ToUpper();
            }
            return View(data);
        }

        public async Task<IActionResult> CommittedPaper(int loanbriefId)
        {
            var data = await _loanBriefService.GetCommittedPaper(GetToken(), loanbriefId);
            return View(data);
        }

        public async Task<IActionResult> AddendumAgreementPledgeOfProperty(int loanbriefId)
        {
            var data = await _loanBriefService.GetContractFinancialAgreement(GetToken(), loanbriefId);
            return View(data);
        }

    }
}