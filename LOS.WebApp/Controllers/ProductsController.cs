﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using LOS.WebApp.Services;
using AutoMapper;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Models;
using System.Reflection;
using LOS.DAL.EntityFramework;
using LOS.Common.Utils;
using LOS.WebApp.Models.Request;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using LOS.WebApp.Services.AIService;
using LOS.WebApp.Models.AISuggestionsInformation;
using LOS.Common.Extensions;
using LOS.WebApp.Models.Product;
using System.Net.Http;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class ProductsController : BaseController
    {
        private IProductService _productService;
        private ICompositeViewEngine viewEngine;
        private IPriceMotor _priceMotorService;
        private ILoanBriefService loanBriefService;
        private IDictionaryService _dictionaryService;
        private IHttpClientFactory clientFactory;

        public ProductsController(IConfiguration configuration, IProductService productService, ICompositeViewEngine viewEngine, IPriceMotor priceMotorService, ILoanBriefService services, IDictionaryService dictionaryService, IHttpClientFactory clientFactory)
            : base(configuration, clientFactory)
        {
            this._productService = productService;
            this.viewEngine = viewEngine;
            this._priceMotorService = priceMotorService;
            this.loanBriefService = services;
            this._dictionaryService = dictionaryService;
            this.clientFactory = clientFactory;
        }

        public IActionResult GetProductByBrand(int brandId)
        {
            List<ProductDetail> data = new List<ProductDetail>();
            try
            {
                // Query api   	 
                data = _productService.GetProductByBrand(GetToken(), brandId);
                //Returning Json Data   
            }
            catch (Exception)
            {
                throw;
            }
            return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(data, "GetProductByBrand")));
        }
        public IActionResult GetPriceByProduct(int productId)
        {
            var data = new PriceMotorRes();
            try
            {
                // Query api   
                var product = _productService.GetProduct(GetToken(), productId) ?? new ProductDetail();
                var productBrand = _productService.GetBrand(GetToken(), product.BrandProductId) ?? new BrandProductDetail();
                var phanh = "";
                var vanh = "";
                var brand = productBrand.Name;
                var modelName = product.ShortName;
                if (product.BrakeType != null && product.BrakeType > 0)
                    phanh = LOS.Common.Helpers.ExtentionHelper.GetName((BrakeType)product.BrakeType);
                if (product.RimType != null && product.RimType > 0)
                    vanh = LOS.Common.Helpers.ExtentionHelper.GetName((RimType)product.RimType);
                var regdate = product.Year ?? 0;
                var model = new PriceMotorReq
                {
                    access_token = GetToken(),
                    brand = brand,
                    model = modelName,
                    phanh = phanh,
                    vanh = vanh,
                    regdate = regdate.ToString()
                };
                data = _priceMotorService.GetMotoPrice(model);
                //Returning Json Data   
            }
            catch (Exception)
            {
                throw;
            }
            return Json(GetBaseObjectResult(true, "Success", data, null));
        }
        private string RenderViewAsString(object model, string viewName = null)
        {
            viewName = viewName ?? ControllerContext.ActionDescriptor.ActionName;
            ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                IView view = viewEngine.FindView(ControllerContext, viewName, true).View;
                ViewContext viewContext = new ViewContext(ControllerContext, view, ViewData, TempData, sw, new HtmlHelperOptions());
                view.RenderAsync(viewContext).Wait();
                return sw.GetStringBuilder().ToString();
            }
        }
        public IActionResult GetProductReview(int loanbriefId, int productId, int IdType = 0, int price = 0)
        {
            var data = new List<Models.ProductReview>();
            try
            {
                data = _productService.GetProductReview(GetToken(), loanbriefId, productId, IdType, price) ?? new List<Models.ProductReview>();
            }
            catch (Exception)
            {
                throw;
            }
            return Json(GetBaseObjectResult(true, "Success", data, null));
        }
        public async Task< IActionResult> UpdateProductResultReview(int productId, long totalMoney, int loanBriefId, List<ProductReviewResult> productReviewResults, long? totalMoneyExpertise)
        {
            try
            {
                var userInfor = this.GetUserGlobal();
                if (userInfor != null)
                {
                    if (userInfor.GroupId == (int)EnumGroupUser.StaffHub || userInfor.GroupId == (int)EnumGroupUser.ManagerHub) //(userInfor.GroupId != (int)EnumGroupUser.ApproveEmp)
                    {
                        var inforLoanCredit = await loanBriefService.GetBasicInfo(GetToken(), loanBriefId);
                        if (inforLoanCredit == null)
                            return Json(GetBaseObjectResult(false, "Thông tin hồ sơ không đúng!", null, null));

                        if (inforLoanCredit.Status == (int)EnumLoanStatus.APPRAISER_REVIEW || inforLoanCredit.Status == (int)EnumLoanStatus.HUB_CHT_APPROVE) //(inforLoanCredit.Status != EnumLoanStatus.BRIEF_APPRAISER_REVIEW.GetHashCode()) 
                        {
                            var objProduct = _productService.GetProduct(GetToken(), productId);
                            if (objProduct == null || objProduct.Id == 0)
                            {
                                return Json(GetBaseObjectResult(false, "Thông tin sản phẩm không đúng!", null, null));
                            }
                            var objManufacturerByBrand = _productService.GetBrand(GetToken(), objProduct.BrandProductId);
                            if (objManufacturerByBrand == null || objManufacturerByBrand.Id == 0)
                            {
                                return Json(GetBaseObjectResult(false, "Thông tin hãng xe không đúng!", null, null));
                            }
                            // 1: insert update ProductReviewResult
                            // 2: insert tblProductReviewResultDetail
                            // 3:   UPDATE tblLoanCredit SET TotalMoney = @TotalMoney, Totalmoneyexpertise = @Totalmoneyexpertise, TotalMoneyExpertiseLast = @TotalMoney WHERE ID = @LoanCreditId
                            // 4: INSERT INTO tblCommentCredit 
                            if (productReviewResults == null || productReviewResults.Count == 0)
                            {
                                return Json(GetBaseObjectResult(false, "Thông tin check list không đúng!", null, null));
                            }

                            // Tiền trước khi thẩm định xe
                            var totalmoneyexpertise = (totalMoneyExpertise == null) ? 0 : (long)totalMoneyExpertise;
                            var ProductReviewResultDetail = new ProductReviewResultDetail
                            {
                                LoanBriefId = loanBriefId,
                                ProductId = productId,
                                UserId = userInfor.UserId,
                                GroupUserId = userInfor.GroupId,
                                PriceReview = totalMoney
                            };
                            var productReviewResultObject = new ProductReviewResultObject
                            {
                                LoanBriefId = loanBriefId,
                                ProductReviewResultDetail = ProductReviewResultDetail,
                                ProductReviewResults = productReviewResults,
                                TotalMoneyExpertise = totalmoneyexpertise,
                                TotalMoney = totalMoney
                            };
                            var result = _productService.InsertProductReviewResult(GetToken(), productReviewResultObject);
                            if (result)
                            {
                                //Thêm comment thẩm định xe thành công
                                loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                                {
                                    LoanBriefId = loanBriefId,
                                    Note = string.Format("Thẩm định xe máy HĐ-{0} với số tiền: {1}đ", loanBriefId, totalMoney.ToString("#,##")),
                                    FullName = userInfor.FullName,
                                    Status = 1,
                                    ActionComment = EnumActionComment.VehicleAssessment.GetHashCode(),
                                    CreatedTime = DateTime.Now,
                                    UserId = userInfor.UserId
                                });
                                return Json(GetBaseObjectResult(true, "Thẩm định xe thành công!", null, null));
                            }
                            else
                                return Json(GetBaseObjectResult(false, "Cập nhật không thành công!", null, null));
                        }
                        else
                        {
                            return Json(GetBaseObjectResult(false, "Không đúng bước thẩm định hồ sơ!", null, null));
                        }
                    }
                    else
                    {
                        return Json(GetBaseObjectResult(false, "Bạn không có quyển thẩm định hồ sơ này!", null, null));
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Json(GetBaseObjectResult(false, "cập nhật không thành công!", null, null));
        }

        #region Quản lý giá xe
        [Route("/product/manage_price_car")]
        public IActionResult ManagePriceCar()
        {
            var lstBrandProduct = _dictionaryService.GetBrandProduct(GetToken());
            ViewBag.ListBrandProduct = lstBrandProduct;
            return View();
        }
        [HttpPost]
        public IActionResult LoadDataManagePriceCar()
        {
            try
            {
                var modal = new ProductDatatable(HttpContext.Request.Form);
                int recordsTotal = 0;
                // Query api   	
                List<ProductDetail> data = new List<ProductDetail>();
                data = _productService.Search(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult GetProductById(int Id)
        {
            var product = _productService.GetProduct(GetToken(), Id);
            if (product != null && product.Id > 0)
                return Json(new { data = product, status = 1, message = "Lấy thông tin thành công" });
            else
                return Json(new { data = product, status = 0, message = "Lấy thông tin thất bại" });
        }

        [HttpPost]
        public IActionResult AddOrUpdateProduct(ProductDTO entity)
        {
            var user = GetUserGlobal();
            var obj = new ProductDTO();
            //Cập nhập thông tin
            if (entity != null && entity.Id > 0)
            {
                entity.UpdateTime = DateTime.Now;
                entity.UpdateBy = user.UserId;
                var result = _productService.Update(GetToken(), entity.Id, entity.MapToProduct());

                if (result)
                    return Json(new { status = 1, message = "Cập nhật xe thành công" });
                return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật xe. Vui lòng liên hệ quản trị viên!" });
            }
            //Thêm mới
            else
            {
                var check = _productService.CheckFullNameProduct(GetToken(), entity.FullName.ToLower());
                if (check != null && check.Id > 0)
                    return Json(new{status = -1, message = "Xe đã tồn tại trên hệ thống. Vui lòng kiểm tra lại!"});



                entity.CreateTime = DateTime.Now;
                entity.CreateBy = user.UserId;
                var result = _productService.Create(GetToken(), entity.MapToProduct());
                if (result)
                    return Json(new { status = 1, message = "Thêm mới xe thành công" });
                return Json(new { status = 0, message = "Xảy ra lỗi khi thêm mới xe. Vui lòng liên hệ quản trị viên!" });
            }
        }
        #endregion
    }
}