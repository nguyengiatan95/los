﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object;
using LOS.WebApp.Cache;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestSharp;

namespace LOS.WebApp.Controllers
{
    public class RemarketingController : BaseController
    {
        private ILoanBriefService loanBriefService;
        private IDictionaryService _dictionaryService;
        private readonly IMapper _mapper;
        private IHttpClientFactory clientFactory;
        private ILMSService _lmsService;
        private IShopServices _shopServices;
        private IRemarketingService _remarketingService;
        private IReasonService _reasonService;
        public RemarketingController(IConfiguration configuration, ILoanBriefService services, IDictionaryService dictionaryService, IMapper mapper, IHttpClientFactory clientFactory,
            ILMSService lmsService, IShopServices shopServices, IRemarketingService remarketingService, IReasonService reasonService) : base(configuration: configuration, clientFactory)
        {
            this.loanBriefService = services;
            this._dictionaryService = dictionaryService;
            this._mapper = mapper;
            this._lmsService = lmsService;
            this._shopServices = shopServices;
            this._remarketingService = remarketingService;
            this._reasonService = reasonService;
        }

        [Route("/remarketing/index.html")]
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Search()
        {
            try
            {
                var modal = new LoanBriefDatatable(HttpContext.Request.Form);
                modal.status = "99";
                DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
                DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
                if (!string.IsNullOrEmpty(modal.DateRanger))
                    DateTimeUtility.ConvertDateRanger(modal.DateRanger, ref fromDate, ref toDate);

                modal.FromDate = fromDate.ToString("yyyy/MM/dd");
                modal.ToDate = toDate.ToString("yyyy/MM/dd");
                int recordsTotal = 0;
                // Query api   	
                var data = loanBriefService.RemarketingSearch(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();

                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> CloneLoanBrief(string[] arrId)
        {
            var user = GetUserGlobal();
            for (int i = 0; i < arrId.Length; i++)
            {
                var loanBrief = await loanBriefService.GetLoanBriefById(GetToken(), Convert.ToInt32(arrId[i]));
                if (loanBrief != null && loanBrief.LoanBriefId != 0)
                {
                    var listLoanBrief = loanBriefService.GetLoanBriefByPhone(GetToken(), loanBrief.Phone);
                    if (listLoanBrief.Any(x => x.Status != (int)EnumLoanStatus.FINISH && x.Status != (int)EnumLoanStatus.CANCELED))
                        return Json(new { status = 0, message = "Khách hàng có đơn vay đang xử lý" });

                    var obj = new LoanBriefInit();
                    obj.ProductId = loanBrief.ProductId;
                    obj.CustomerId = loanBrief.CustomerId;
                    obj.RateTypeId = loanBrief.RateTypeId;
                    obj.FullName = loanBrief.FullName;
                    obj.Phone = loanBrief.Phone;
                    obj.Dob = loanBrief.Dob;
                    obj.Gender = loanBrief.Gender;
                    obj.NationalCard = loanBrief.NationalCard;
                    obj.NationalCardDate = loanBrief.NationalCardDate;
                    obj.NationCardPlace = loanBrief.NationCardPlace;
                    obj.LoanAmount = loanBrief.LoanAmount;
                    obj.LoanAmountFirst = loanBrief.LoanAmountFirst;
                    if (loanBrief.LoanTime == null)
                        loanBrief.LoanTime = 3;
                    obj.LoanTime = loanBrief.LoanTime;
                    obj.CreatedTime = DateTime.Now;
                    obj.FromDate = DateTime.Now;
                    obj.ToDate = DateTime.Now.AddMonths((int)loanBrief.LoanTime);
                    obj.ConsultantRate = loanBrief.ConsultantRate;
                    obj.ServiceRate = loanBrief.ServiceRate;
                    obj.LoanRate = loanBrief.LoanRate;
                    obj.ProvinceId = loanBrief.ProvinceId;
                    obj.DistrictId = loanBrief.DistrictId;
                    obj.WardId = loanBrief.WardId;
                    obj.UtmSource = loanBrief.UtmSource;
                    obj.UtmMedium = loanBrief.UtmMedium;
                    obj.UtmCampaign = loanBrief.UtmCampaign;
                    obj.UtmTerm = loanBrief.UtmTerm;
                    obj.UtmContent = loanBrief.UtmContent;
                    obj.ScoreCredit = loanBrief.ScoreCredit;
                    //obj.BoundTelesaleId = loanBrief.BoundTelesaleId;
                    obj.AffStatus = loanBrief.AffStatus;
                    obj.MecashId = loanBrief.MecashId;
                    obj.ReceivingMoneyType = loanBrief.ReceivingMoneyType;
                    obj.BankId = loanBrief.BankId;
                    obj.BankAccountNumber = loanBrief.BankAccountNumber;
                    obj.BankCardNumber = loanBrief.BankCardNumber;
                    obj.BankAccountName = loanBrief.BankAccountName;
                    obj.PlatformType = loanBrief.PlatformType;
                    obj.TypeLoanBrief = loanBrief.TypeLoanBrief;
                    obj.Frequency = loanBrief.Frequency;
                    obj.IsCheckCic = loanBrief.IsCheckCic;
                    obj.IsLocate = loanBrief.IsLocate;
                    obj.IsTrackingLocation = loanBrief.IsTrackingLocation;
                    obj.BuyInsurenceCustomer = loanBrief.BuyInsurenceCustomer;
                    obj.LoanPurpose = loanBrief.LoanPurpose;
                    obj.LoanBriefComment = "Remarketing";
                    obj.CreateBy = user.UserId;
                    obj.Status = EnumLoanStatus.INIT.GetHashCode();
                    obj.ReMarketingLoanBriefId = loanBrief.LoanBriefId;
                    obj.TypeRemarketing = (int)EnumTypeRemarketing.IsRemarketing;

                    var result = loanBriefService.CloneLoanBrief(GetToken(), obj);
                    if (result > 0)
                    {
                        //Thêm note khởi tạo đơn vay remarketing
                        var note = new LoanBriefNote
                        {
                            LoanBriefId = result,
                            Note = string.Format("{0}: Remarketing", user.FullName),
                            FullName = user.FullName,
                            UserId = user.UserId,
                            Status = 1,
                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                            CreatedTime = DateTime.Now
                        };
                        loanBriefService.AddLoanBriefNote(GetToken(), note);

                    }
                    else
                        return Json(new { status = 0, message = "Xảy ra lỗi khi tạo mới đơn vay. Vui lòng thử lại sau!" });
                }
                else
                {
                    return Json(new { status = 0, message = "Không tìm thấy đơn vay!" });
                }
            }
            return Json(new { status = 1, message = "Khởi tạo lại đơn vay thành công" });
        }


        [Route("/loancancel/index.html")]
        public IActionResult IndexLoanCancel()
        {
            var user = GetUserGlobal();
            var model = new RemarketingTlsViewModel();
            model.ListReasonGroup = _reasonService.GetReasonGroup(user.Token, EnumTypeReason.TeleSalesCancel.GetHashCode());
            if (user.GroupId != (int)EnumGroupUser.ManagerHub)
                return RedirectToAction("PermissionDenied", "Home");
            else
                return View(model);
        }
        public IActionResult SearchLoanCancel()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new LoanCancelDatatable(HttpContext.Request.Form);
                //mặc định là gói xe máy
                if (string.IsNullOrEmpty(modal.productId))
                    modal.productId = "2";
                //DateTime fromDate = DateTime.Now;

                //DateTime toDate = DateTime.Now.AddDays(1);
                modal.HubId = user.ShopGlobal;
                //if (!string.IsNullOrEmpty(modal.DateRanger))
                //    DateTimeUtility.ConvertDateRangerV2(modal.DateRanger, ref fromDate, ref toDate);

                //modal.FromDate = fromDate.ToString("yyyy/MM/dd");
                //modal.ToDate = toDate.ToString("yyyy/MM/dd");
                int recordsTotal = 0;
                // Query api   	
                var data = loanBriefService.LoanCancelSearch(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();

                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> ReInitLoanbriefId(int LoanbriefId)
        {
            var user = GetUserGlobal();
            var access_token = user.Token;
            var loanbrief = await loanBriefService.GetLoanBriefById(access_token, LoanbriefId);
            if (loanbrief != null)
            {
                if (loanbrief.ProductId != EnumProductCredit.CamotoCreditType.GetHashCode() && loanbrief.ProductId != EnumProductCredit.OtoCreditType_CC.GetHashCode()
                    && loanbrief.ProductId != EnumProductCredit.OtoCreditType_KCC.GetHashCode() && loanbrief.ProductId != EnumProductCredit.CamCoHangHoa.GetHashCode())
                {
                    var listLoanBrief = loanBriefService.GetLoanBriefProcessing(access_token, loanbrief.Customer.Phone, loanbrief.Customer.NationalCard, loanbrief.LoanBriefId);
                    if (listLoanBrief != null && listLoanBrief.LoanBriefId > 0)
                        return Json(new { status = 0, message = $"Khách hàng có đơn vay đang xử lý HĐ-{listLoanBrief.LoanBriefId}" });
                }
                //Check xem khách hàng có trong black list không
                var checkBlackList = _lmsService.CheckBlackList(access_token, loanbrief.FullName, loanbrief.NationalCard, loanbrief.Phone, loanbrief.LoanBriefId);
                if (checkBlackList != null)
                {
                    if (checkBlackList.Data == true && checkBlackList.Status == 1)
                        return Json(new { status = 0, message = "Khách hàng nằm trong BlackList" });
                }
                loanbrief.Status = EnumLoanStatus.INIT.GetHashCode();
                loanbrief.PipelineState = null;
                loanbrief.CurrentPipelineId = null;
                loanbrief.CurrentSectionDetailId = null;
                loanbrief.CurrentSectionId = null;
                loanbrief.CreatedTime = DateTime.Now;
                loanbrief.BoundTelesaleId = 0;
                loanbrief.CreateBy = user.UserId;
                loanbrief.TypeRemarketing = EnumTypeRemarketing.IsRemarketing.GetHashCode();
                loanbrief.FromDate = DateTime.Now;
                if (loanbrief.LoanTime > 0)
                    loanbrief.ToDate = DateTime.Now.AddMonths(loanbrief.LoanTime.Value);
                loanbrief.LoanAmountExpertise = 0;
                loanbrief.LoanAmountExpertiseLast = 0;
                loanbrief.IsHeadOffice = true;
                loanbrief.NumberCall = 0;
                loanbrief.LoanBriefCancelAt = null;
                loanbrief.BuyInsurenceCustomer = null;
                loanbrief.LoanBriefComment = null;
                loanbrief.ReasonCancel = null;
                loanbrief.ReMarketingLoanBriefId = loanbrief.LoanBriefId;
                loanbrief.TelesalesPushAt = null;
                loanbrief.HubPushAt = null;
                loanbrief.EvaluationCompanyUserId = null;
                loanbrief.EvaluationCompanyState = null;
                loanbrief.EvaluationCompanyTime = null;
                loanbrief.EvaluationHomeUserId = null;
                loanbrief.EvaluationHomeState = null;
                loanbrief.EvaluationHomeTime = null;
                loanbrief.CoordinatorUserId = null;
                loanbrief.LenderId = null;
                loanbrief.CoordinatorPushAt = null;
                loanbrief.DeviceId = null;
                loanbrief.DeviceStatus = null;
                loanbrief.ActivedDeviceAt = null;
                loanbrief.InProcess = null;
                loanbrief.IsLock = null;
                loanbrief.AddedToQueue = null;
                loanbrief.ResultLocation = null;
                loanbrief.ResultRuleCheck = null;
                loanbrief.IsReborrow = null;
                loanbrief.FirstProcessingTime = null;
                loanbrief.FirstTimeHubFeedBack = null;
                loanbrief.ValueCheckQualify = null;
                loanbrief.FeeInsuranceOfCustomer = null;
                loanbrief.FeeInsuranceOfLender = null;
                loanbrief.CodeId = null;
                loanbrief.Passport = null;
                loanbrief.StatusTelesales = null;
                loanbrief.DetailStatusTelesales = null;
                loanbrief.ReCareLoanbrief = null;
                loanbrief.PresenterCode = null;
                loanbrief.Step = null;
                loanbrief.LoanBriefId = 0;
                loanbrief.HubId = user.ShopGlobal;
                loanbrief.ContractGinno = null;
                if (loanbrief.LoanBriefResident != null)
                {
                    loanbrief.LoanBriefResident.LoanBriefResidentId = 0;
                }
                if (loanbrief.LoanBriefHousehold != null)
                {
                    loanbrief.LoanBriefHousehold.LoanBriefHouseholdId = 0;
                }
                if (loanbrief.LoanBriefProperty != null)
                {
                    loanbrief.LoanBriefProperty.LoanBriefPropertyId = 0;
                    loanbrief.LoanBriefProperty.CreatedTime = DateTime.Now;
                }
                if (loanbrief.LoanBriefJob != null)
                {
                    loanbrief.LoanBriefJob.LoanBriefJobId = 0;
                }
                if (loanbrief.LoanBriefRelationship != null)
                {
                    foreach (var item in loanbrief.LoanBriefRelationship)
                    {
                        item.LoanBriefId = null;
                        item.Id = 0;
                        item.CreatedDate = DateTime.Now;
                    }
                }
                loanbrief.LoanBriefQuestionScript = null;
                var result = await loanBriefService.ReInitLoanBrief(access_token, loanbrief);
                if (result > 0)
                {//Thêm note khởi tạo đơn vay
                    loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = result,
                        Note = string.Format("{0}: Khởi tạo lại đơn vay từ HĐ-{1}", user.FullName, loanbrief.ReMarketingLoanBriefId),
                        FullName = user.FullName,
                        UserId = user.UserId,
                        Status = 1,
                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });
                    return Json(new { status = 1, message = string.Format("Khởi tạo lại thành công đơn vay HĐ-{0}", result) });
                }
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi khi khởi tạo lại đơn vay" });
            }
            else
            {
                return Json(new { status = 0, message = "Đơn vay không tồn tại trong hệ thống." });
            }
        }

        [Route("/remarketingcar/index.html")]
        public IActionResult IndexRemarketingCar()
        {
            var lstShop = _shopServices.GetAllHub(GetToken());
            return View(lstShop);
        }
        public IActionResult SearchRemarketingCar()
        {
            try
            {
                //var user = GetUserGlobal();
                var modal = new LoanCancelDatatable(HttpContext.Request.Form);
                int recordsTotal = 0;
                // Query api   	
                var data = loanBriefService.SearchRemaketingCar(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [PermissionFilter]
        public IActionResult TranferLoanForHub(string[] ArrId, int HubId, string HubName)
        {
            var user = GetUserGlobal();
            for (int i = 0; i < ArrId.Length; i++)
            {
                var req = new BoundTelesaleReq();
                req.userId = HubId;
                req.loanBriefId = Convert.ToInt32(ArrId[i]);

                //var re = JsonConvert.SerializeObject(req);
                var result = loanBriefService.UpdateHubId(GetToken(), req);
                if (result.meta.errorCode != 200)
                {
                    return Json(new { status = 0, message = "Xảy ra lỗi khi chuyển hỗ trợ. Vui lòng thử lại sau!" });
                }
                var note = new LoanBriefNote
                {
                    LoanBriefId = Convert.ToInt32(ArrId[i]),
                    Note = "Đơn rmkt oto có mã HĐ-" + ArrId[i] + " đã được " + "LinhTQ" + " chia cho " + HubName,
                    FullName = user.FullName,
                    UserId = user.UserId,
                    Status = 1,
                    ActionComment = EnumActionComment.TranferRemaketingCar.GetHashCode(),
                    CreatedTime = DateTime.Now,
                    ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                    ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                };
                loanBriefService.AddLoanBriefNote(GetToken(), note);
            }
            return Json(new { status = 1, message = "Chuyển hỗ trợ thành công" });
        }


        [Route("/rmkt/index.html")]
        public IActionResult RemarketingTls()
        {
            var user = GetUserGlobal();
            var model = new RemarketingTlsViewModel();
            model.ListReasonGroup = _reasonService.GetReasonGroup(user.Token, EnumTypeReason.TeleSalesCancel.GetHashCode());
            model.ListPlatformType = CacheData.ListPlatformType;
            ViewBag.User = user;
            if (user.UserId != (int)EnumUser.spt_hoanpt && user.UserId != (int)EnumUser.spt_huongkq1
                && user.UserId != (int)EnumUser.spt_nhunglt)
                return RedirectToAction("PermissionDenied", "Home");
            else
                return View(model);

        }
        public IActionResult GetDataRmktTls()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new RemarketingDatatable(HttpContext.Request.Form);
                modal.GroupId = user.GroupId.Value;
                if(user.GroupId == (int)EnumGroupUser.ManagerHub || user.GroupId == (int)EnumGroupUser.StaffHub)
                    modal.ShopId = user.ShopGlobal;
                if (Convert.ToInt32(modal.Status) == (int)EnumLoanStatus.CANCELED)
                {
                    if (!string.IsNullOrEmpty(modal.DateCancelRanger))
                    {
                        DateTime fromDate = DateTime.Now;
                        DateTime toDate = DateTime.Now.AddDays(-30);
                        DateTimeUtility.ConvertDateRangerV2(modal.DateCancelRanger, ref fromDate, ref toDate);
                        modal.FromCancelDate = fromDate.ToString("yyyy/MM/dd");
                        modal.ToCancelDate = toDate.ToString("yyyy/MM/dd");
                    }
                }

                if (!string.IsNullOrEmpty(modal.DateRanger))
                {
                    DateTime fromDate = DateTime.Now;
                    DateTime toDate = DateTime.Now.AddDays(-30);
                    DateTimeUtility.ConvertDateRangerV2(modal.DateRanger, ref fromDate, ref toDate);
                    modal.FromDate = fromDate.ToString("yyyy/MM/dd 00:01");
                    modal.ToDate = toDate.ToString("yyyy/MM/dd 23:59");
                }

                int recordsTotal = 0;
                // Query api   	
                var data = _remarketingService.GetData(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> ReCreateLoanbrief(string[] listLoanbriefId, int status)
        {
            var user = GetUserGlobal();
            if (listLoanbriefId == null || listLoanbriefId.Count() == 0)
                return Json(new { status = 0, message = "Vui lòng chọn danh sách đơn cần khởi tạo lại" });

            if (user.UserId != (int)EnumUser.spt_hoanpt && user.UserId != (int)EnumUser.spt_huongkq1 
                && user.UserId != (int)EnumUser.spt_nhunglt && user.GroupId != (int)EnumGroupUser.ManagerHub)
                return Json(new { status = 0, message = "Bạn không có quyền khởi tạo lại đơn" });

            var model = new RemarketingLoanModel();
            if (status == (int)EnumLoanStatus.CANCELED)
            {
                model.UserId = user.UserId;
                model.ListLoanbrief = new List<int>();
                model.TypeRemarketing = EnumTypeRemarketing.IsRemarketing.GetHashCode();
                model.TypeReLoanbrief = TypeReLoanbrief.CancelLoanbrief.GetHashCode();
                if (user.GroupId != (int)EnumGroupUser.ManagerHub)
                {
                    model.TelesalesId = (int)EnumUser.Remkt;
                    model.UtmSource = "form_cancel_rmkt";
                }
                else
                {
                    model.HubId = user.ShopGlobal;
                    model.UtmSource = "form_remkt_hub";
                }
            }
            else if (status == (int)EnumLoanStatus.FINISH)
            {
                model.UserId = user.UserId;
                model.UtmSource = "rmkt_loan_finish";
                model.ListLoanbrief = new List<int>();
                model.TypeRemarketing = EnumTypeRemarketing.IsRemarketing.GetHashCode();
                model.TypeReLoanbrief = TypeReLoanbrief.FinishLoanbrief.GetHashCode();
                model.TelesalesId = (int)EnumUser.follow;
            }
            for (int i = 0; i < listLoanbriefId.Length; i++)
            {
                if (!string.IsNullOrEmpty(listLoanbriefId[i]) && int.TryParse(listLoanbriefId[i], out _))
                    model.ListLoanbrief.Add(Convert.ToInt32(listLoanbriefId[i]));
            }
            if (await _remarketingService.ReCreateLoanbrief(user.Token, model))
                return Json(new { status = 1, message = "Khởi tạo đơn Remarketing thành công" });
            return Json(new { status = 0, message = "Xảy ra lỗi khi khởi tạo lại đơn vay. Vui lòng thử lại" });
        }

        public IActionResult GetDataRmktTlsAll(RemaketingLoanAllSearch entity)
        {
            if (entity.Status == (int)EnumLoanStatus.CANCELED)
            {
                if (!string.IsNullOrEmpty(entity.strCancelTimeRanger))
                {
                    DateTime fromDate = DateTime.Now;
                    DateTime toDate = DateTime.Now.AddDays(-30);
                    DateTimeUtility.ConvertDateRangerV2(entity.strCancelTimeRanger, ref fromDate, ref toDate);
                    entity.FromCancelTime = fromDate.ToString("yyyy/MM/dd");
                    entity.ToCancelTime = toDate.ToString("yyyy/MM/dd");
                }
            }
            if (!string.IsNullOrEmpty(entity.strCreateDateRanger))
            {
                DateTime fromDate = DateTime.Now;
                DateTime toDate = DateTime.Now.AddDays(-30);
                DateTimeUtility.ConvertDateRangerV2(entity.strCreateDateRanger, ref fromDate, ref toDate);
                entity.FromCreateDate = fromDate.ToString("yyyy/MM/dd 00:01");
                entity.ToCreateDate = toDate.ToString("yyyy/MM/dd 23:59");
            }
            var data = _remarketingService.GetDataAll(GetToken(), entity.ToQueryObject());
            if (data.Count > 0)
                return Json(new { status = 1, data = data, message = "Lấy dữ liệu thành công" });
            else
                return Json(new { status = 0, message = "Không tìm thấy đơn vay nào để khởi tạo lại" });
        }
    }
}