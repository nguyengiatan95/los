﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Services.Report;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class ReportController : BaseController
    {
        private IConfiguration _baseConfig;
        private IReportService _reportService;
        private readonly IMapper _mapper;
        private IHttpClientFactory clientFactory;
        public ReportController(IReportService reportService, IConfiguration configuration, IMapper mapper, IHttpClientFactory clientFactory)
           : base(configuration, clientFactory)
        {
            _reportService = reportService;
            _baseConfig = configuration;
            _mapper = mapper;
            this.clientFactory = clientFactory;
        }
        [Route("don-vay-gioi-thieu.html")]
        public IActionResult ReporLoanRegisterOfUserMmo()
        {
            //var lstStatusAff = EnumConvert.GetEnumToList(typeof(StatusAff));
            //ViewBag.LstStatusAff = lstStatusAff;
            return View();
        }
        public async Task<IActionResult> GetReporLoanRegisterOfUserMmo()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new ReportLoanBriefByUserMmo(HttpContext.Request.Form);

                DateTime fromDate = DateTimeUtility.FirstDayOfWeek(DateTime.Now).AddDays(-1);
                DateTime toDate = DateTimeUtility.LastDayOfWeek(DateTime.Now).AddDays(+1);
                if (!string.IsNullOrEmpty(modal.DateRanger))
                    DateTimeUtility.ConvertDateRanger(modal.DateRanger, ref fromDate, ref toDate);

                int recordsTotal = 0;
                modal.Affcode = -1;
                modal.FromDate = fromDate;
                modal.ToDate = toDate;
                modal.AffStatus = StatusAff.Introduced.GetHashCode();
                // Query api   	
                List<LoanBriefDetail> data = new List<LoanBriefDetail>();
                data = _reportService.ReporLoanRegisterOfUserMmo(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });

            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("don-vay-ban-thanh-cong.html")]
        public IActionResult ReportLoanBriefSuccessfullySold()
        {
            return View();
        }
        public async Task<IActionResult> GetReportLoanBriefSuccessfullySold()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new ReportLoanBriefByUserMmo(HttpContext.Request.Form);
                DateTime fromDate = DateTimeUtility.FirstDayOfWeek(DateTime.Now).AddDays(-1);
                DateTime toDate = DateTimeUtility.LastDayOfWeek(DateTime.Now).AddDays(+1);
                if (!string.IsNullOrEmpty(modal.DateRanger))
                    DateTimeUtility.ConvertDateRanger(modal.DateRanger, ref fromDate, ref toDate);


                int recordsTotal = 0;
                modal.Affcode = -1;// user.UserId;
                modal.FromDate = fromDate;
                modal.ToDate = toDate;
                modal.AffStatus = StatusAff.SuccessfulSale.GetHashCode();
                // Query api   	
                List<LoanBriefSuccessFullySoldDetail> data = new List<LoanBriefSuccessFullySoldDetail>();
                data = _reportService.ReporLoanSuccessFullySoldMMO(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });

            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("report/approve_employee.html")]
        public IActionResult ReportApproveEmployee()
        {
            return View();
        }
        public async Task<IActionResult> GetReportApproveEmployee(int month)
        {
            try
            {
                var user = GetUserGlobal();

                // Query api   	
                var data = await _reportService.ReporApproveEmployee(GetToken(), month);
                var totalPush = 0;
                var totalDis = 0;
                if (data != null)
                {
                    totalPush = data.Sum(x => x.TotalPush);
                    totalDis = data.Sum(x => x.Disbursement);
                }
                //Returning Json Data  
                return Json(new { data = data, totalPush = totalPush, totalDis = totalDis });

            }
            catch (Exception)
            {
                throw;
            }
        }

        [Produces("application/json")]
        public async Task<IActionResult> ExportReportApproveEmployee(int month)
        {
            // Query api   	
            var data = await _reportService.ReporApproveEmployee(GetToken(), month);
            List<string> lstHeaderLoan = new List<string> { "STT", "Tên nhân viên", "Số hồ sơ đẩy", "Số hồ sơ được giải ngân", "Tỉ lệ giải ngân/đẩy" };
            List<int> lstWidthLoan = new List<int> { 10, 30, 20, 20, 20 };
            List<ExcelHorizontalAlignment> lstAlign = new List<ExcelHorizontalAlignment> { ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left
                                                                                           };

            List<System.Drawing.Color> lstColor = new List<System.Drawing.Color> { Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black
                                                                                  };

            byte[] fileContents;
            ExcelPackage package = new ExcelPackage();

            var ws = package.Workbook.Worksheets.Add("Báo cáo hiệu suất làm việc");
            // Set Font
            ws.Cells.Style.Font.Name = "Calibri";
            ws.Cells.Style.Font.Size = 11;

            // Format All Cells
            ws.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            // Format All Column
            for (int i = 1; i <= lstHeaderLoan.Count; i++)
            {
                ws.Column(i).Width = lstWidthLoan[i - 1];
                ws.Column(i).Style.HorizontalAlignment = lstAlign[i - 1];
                ws.Column(i).Style.Font.Color.SetColor(lstColor[i - 1]);
            }

            int iPosHeader = 1;
            // mapping title header
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(77, 119, 204));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Font.Bold = true;
            ws.Row(iPosHeader).Height = 16;
            for (int i = 1; i <= lstHeaderLoan.Count; i++)
            {
                ws.Cells[iPosHeader, i].Value = lstHeaderLoan[i - 1];
            }

            // SET VALUE
            iPosHeader++;

            int j = 1;
            for (int i = 0; i < data.Count(); i++)
            {
                ws.Cells[iPosHeader, 1].Value = j;
                ws.Cells[iPosHeader, 2].Value = data[i].FullName;
                ws.Cells[iPosHeader, 3].Value = data[i].TotalPush;
                ws.Cells[iPosHeader, 4].Value = data[i].Disbursement;
                ws.Cells[iPosHeader, 5].Value = Math.Round(((decimal)data[i].Disbursement / (decimal)data[i].TotalPush * 100), 2).ToString() + " %";

                iPosHeader++;
                j++;
            }
            fileContents = package.GetAsByteArray();
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: "bao-cao-hieu-suat-lam-viec.xlsx"
            );

        }

        [Route("report/esign_customer.html")]
        public IActionResult ReportEsignCustomer()
        {
            return View();
        }
        public async Task<IActionResult> GetReportEsignCustomer(string dateRanger)
        {
            try
            {
                var user = GetUserGlobal();
                DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
                DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
                if (!string.IsNullOrEmpty(dateRanger))
                    DateTimeUtility.ConvertDateRanger(dateRanger, ref fromDate, ref toDate);
                // Query api   	
                var data = await _reportService.ReportEsignCustomer(user.Token, fromDate, toDate);
                var count = data.Count();

                //Returning Json Data  
                return Json(new { data = data,total = count });

            }
            catch (Exception)
            {
                throw;
            }
        }

        [Produces("application/json")]
        public async Task<IActionResult> ExportReportEsignCustomer(string dateRanger)
        {
            var user = GetUserGlobal();
            // Query api   	
            DateTime fromDate = DateTimeUtility.FromeDate(DateTime.Now);
            DateTime toDate = DateTimeUtility.ToDate(DateTime.Now);
            if (!string.IsNullOrEmpty(dateRanger))
                DateTimeUtility.ConvertDateRanger(dateRanger, ref fromDate, ref toDate);
            // Query api   	
            var data = await _reportService.ReportEsignCustomer(user.Token, fromDate, toDate);
            List<string> lstHeaderLoan = new List<string> { "STT", "Mã HĐ", "Khách hàng", "AgreementId", "Ngày ký" };
            List<int> lstWidthLoan = new List<int> { 10, 30, 30, 50, 30 };
            List<ExcelHorizontalAlignment> lstAlign = new List<ExcelHorizontalAlignment> { ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left,
                                                                                           ExcelHorizontalAlignment.Left
                                                                                           };

            List<System.Drawing.Color> lstColor = new List<System.Drawing.Color> { Color.Black, Color.Black , Color.Black , Color.Black , Color.Black};

            byte[] fileContents;
            ExcelPackage package = new ExcelPackage();

            var ws = package.Workbook.Worksheets.Add("Báo cáo số lượng chữ ký Esign của khách hàng");
            // Set Font
            ws.Cells.Style.Font.Name = "Calibri";
            ws.Cells.Style.Font.Size = 11;

            // Format All Cells
            ws.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            // Format All Column
            for (int i = 1; i <= lstHeaderLoan.Count; i++)
            {
                ws.Column(i).Width = lstWidthLoan[i - 1];
                ws.Column(i).Style.HorizontalAlignment = lstAlign[i - 1];
                ws.Column(i).Style.Font.Color.SetColor(lstColor[i - 1]);
            }

            int iPosHeader = 1;
            // mapping title header
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(77, 119, 204));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeaderLoan.Count].Style.Font.Bold = true;
            ws.Row(iPosHeader).Height = 16;
            for (int i = 1; i <= lstHeaderLoan.Count; i++)
            {
                ws.Cells[iPosHeader, i].Value = lstHeaderLoan[i - 1];
            }

            // SET VALUE
            iPosHeader++;

            int j = 1;
            for (int i = 0; i < data.Count(); i++)
            {
                ws.Cells[iPosHeader, 1].Value = j;
                ws.Cells[iPosHeader, 2].Value = "HĐ-" + data[i].LoanBriefId;
                ws.Cells[iPosHeader, 3].Value = data[i].FullName;
                ws.Cells[iPosHeader, 4].Value = data[i].AgreementId;
                ws.Cells[iPosHeader, 5].Value = data[i].CreatedAt.ToString("dd/MM/yyyy");

                iPosHeader++;
                j++;
            }
            fileContents = package.GetAsByteArray();
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: "bao-cao-so-luong-chu-ky-esign.xlsx"
            );

        }
    }
}