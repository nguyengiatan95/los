﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class ScheduleTimeController : BaseController
    {
        private ILoanBriefService loanBriefService;
        private IDictionaryService _dictionaryService;
        private IReasonService _reasonService;
        private readonly IMapper _mapper;
        private IConfiguration _configuration;
        private ILMSService _lmsService;
        private IHttpClientFactory clientFactory;
        private IUserServices _userServices;
        private IShopServices _shopServices;
        public ScheduleTimeController(IConfiguration configuration, ILoanBriefService loanService, IDictionaryService dictionaryService,
            IReasonService reationService, IMapper mapper, ILMSService lmsService, IHttpClientFactory clientFactory,
            IUserServices userServices, IShopServices shopServices) : base(configuration: configuration, clientFactory)
        {
            this.loanBriefService = loanService;
            this._dictionaryService = dictionaryService;
            this._reasonService = reationService;
            this._mapper = mapper;
            this._configuration = configuration;
            _lmsService = lmsService;
            this.clientFactory = clientFactory;
            _userServices = userServices;
            _shopServices = shopServices;
        }


        [HttpGet]
        public IActionResult Init(int LoanBriefId)
        {
            ViewBag.LoanBriefId = LoanBriefId;
            var user = GetUserGlobal();
            var lstSchedule = loanBriefService.GetListScheduleByLoan(user.Token, LoanBriefId).Result.FirstOrDefault();
            return View(lstSchedule);
        }

        [HttpGet]
        public async Task<IActionResult> GetScheduleByLoan(int loanBriefId)
        {
            var user = GetUserGlobal();

            var access_token = user.Token;
            var lstSchedule = await loanBriefService.GetListScheduleByLoan(access_token, loanBriefId);
            return Json(new { data = lstSchedule });
            //return Json(new { status = 1, data = lstSchedule });
        }
        [HttpPost]
        public IActionResult AddEndUpdate(ScheduleTimeHubReq req)
        {
            var user = GetUserGlobal();
            try
            {
                var access_token = user.Token;
                var loanBrief = loanBriefService.GetLoanBriefByIdNew(access_token, req.LoanBriefId);
                if (loanBrief == null || loanBrief.LoanBriefId == 0)
                    return Json(GetBaseObjectResult(false, "Không tìm thấy thông tin đơn vay"));
                req.UserId = user.UserId;
                req.HubId = user.ShopGlobal;
                req.UserFullName = user.FullName;
                req.CusName = loanBrief.FullName;
                var result = loanBriefService.AddAndEditSchedule(access_token, req);
                // check if telesale finish
                if (result.meta.errorCode == 200) // Success
                {
                    var note = string.Format("{0} đã đặt lịch hẹn : {1} - {2} với nội dung: {3}", user.FullName, req.StartTime, req.EndTime, req.Comment.ToString());
                    var message = "Đặt lịch hẹn thành công";

                    if (req.TypeSubmit == 2)
                    {
                        note = string.Format("{0} đã sửa lịch hẹn : {1} - {2} với nội dung: {3}", user.FullName, req.StartTime, req.EndTime, req.Comment.ToString());
                        message = "Cập nhật lịch hẹn thành công";
                    }
                    if (req.TypeSubmit == 3)
                    {
                        note = string.Format("{0} đã hoàn thành lịch hẹn", user.FullName);
                        message = "Lịch hẹn đã được hoàn thành";
                    }

                    //Thêm note
                    loanBriefService.AddLoanBriefNote(access_token, new LoanBriefNote
                    {
                        LoanBriefId = req.LoanBriefId,
                        Note = note,
                        FullName = user.FullName,
                        Status = 1,
                        ActionComment = EnumActionComment.ScheduleTime.GetHashCode(),
                        CreatedTime = DateTime.Now,
                        UserId = user.UserId,
                        ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                        ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                    });

                    return Json(new { status = 1, message = message });
                }
                else
                {
                    return Json(new { status = 0, message = "Có lỗi xảy ra, vui lòng liên hệ kỹ thuật!" });
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [Route("/calendar/index.html")]
        public IActionResult Calendar()
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            var lstUser = new List<UserDetail>();
            if (user.GroupId == (int)EnumGroupUser.ManagerHub)
                lstUser = _userServices.GetStaff(user.Token, user.ShopGlobal);

            ViewBag.StartTime = DateTimeUtility.FirstDayOfWeek(DateTime.Now).ToString("dd/MM/yyyy HH:ss");
            ViewBag.EndTime = DateTimeUtility.LastDayOfWeek(DateTime.Now).ToString("dd/MM/yyyy HH:ss");

            return View(lstUser);

        }
        [HttpPost]
        public async Task<IActionResult> GetListScheduleHub(GetListScheduleTimeHubReq req)
        {
            var user = GetUserGlobal();
            try
            {
                var access_token = user.Token;
                List<CalendarScheduleTimeReq> lst = new List<CalendarScheduleTimeReq>();
                req.HubId = -1;
                req.GroupId = user.GroupId.Value;
                //if (user.GroupId == (int)EnumGroupUser.ManagerHub)
                //    req.HubId = user.ShopGlobal;
                //else
                req.UserId = user.UserId;

                var lstSchedule = await loanBriefService.SearchSchedule(access_token, req);

                if (lstSchedule != null)
                {
                    var start = DateTime.ParseExact(req.StartTime, "dd/MM/yyyy HH:mm", null);
                    var end = DateTime.ParseExact(req.EndTime, "dd/MM/yyyy HH:mm", null);
                    var day = (end - start).TotalDays;
                    if (day <= 0)
                        return Json(new { status = 0, message = "Thời gian ngày kết thúc phải lớn hơn ngày bắt đầu!" });
                    foreach (var item in lstSchedule)
                    {
                        lst.Add(new CalendarScheduleTimeReq()
                        {
                            ResourceId = "1",
                            Start = item.StartTime.Value,
                            End = item.EndTime.Value,
                            Title = item.Note != null ? Regex.Replace(HttpUtility.HtmlDecode(item.Note.ToString()), "<.*?>", String.Empty) : "",
                            Color = "#FF" + item.UserId,
                            LoanBriefId = item.LoanBriefId.Value,
                            Url = "javascript:;"
                        });
                    }

                    return Json(new { status = 1, data = lst, day = Convert.ToInt32(day), initialDate = start });
                }


                else
                    return Json(new { status = 0, message = "Có lỗi xảy ra, vui lòng liên hệ kỹ thuật!" });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        public async Task<IActionResult> DetailSchedule(int loanBriefId)
        {
            var user = GetUserGlobal();

            var access_token = user.Token;
            var lstSchedule = await loanBriefService.GetListScheduleByLoan(access_token, loanBriefId);
            return View(lstSchedule);
            //return Json(new { status = 1, data = lstSchedule });
        }


    }
}