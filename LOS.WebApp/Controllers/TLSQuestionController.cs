﻿using LOS.Common.Extensions;
using LOS.Common.Helpers;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Filter;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.TlsQuestionDto;
using LOS.WebApp.Services.TlsQuestionService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class TLSQuestionController : BaseController
    {
        private ITlsQuestionService _tlsQuestionService;
        private IHttpClientFactory clientFactory;
        public TLSQuestionController(IConfiguration configuration, ITlsQuestionService tlsQuestionService, IHttpClientFactory clientFactory)
            : base(configuration, clientFactory)
        {
            _tlsQuestionService = tlsQuestionService;
            this.clientFactory = clientFactory;
        }


        [Route("tls-question.html")]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> LoadData()
        {
            try
            {
                var input = new TlsQuestionDatatable(HttpContext.Request.Form);
                // Query api   	
                var inputModel = new ApiPagingPostModel
                {
                    Keyword = input.Keyword,
                    IsSortByAsc = input.sort == "asc" ? true : false,
                    SortColumn = input.field,
                    CurrentPage = int.Parse(input.page),
                    PageSize = int.Parse(input.perpage)
                };

                var data = await _tlsQuestionService.GetAllAsync(GetToken(), inputModel);
                input.total = data.meta.totalRecords.ToString();
                //Returning Json Data  
                return Json(new { meta = input, data = data.data });
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Lỗi liên hệ kĩ thuật" });
            }
        }


        [HttpGet]
        public async Task<IActionResult> CreateOrUpdate(int? id)
        {
            TlsQuestion configOutput = null;
            if (!id.HasValue)
                configOutput = new TlsQuestion();
            else
            {
                // Query api
                var queryResult = await _tlsQuestionService.GetByIdAsync(GetToken(), id.Value);
                if (queryResult == null || queryResult.meta == null || queryResult.meta.errorCode != ResponseHelper.SUCCESS_CODE)
                {
                    return RedirectToAction("Error", "Home");
                }
                configOutput = queryResult.data;
            }
            var output = new CreateTlsQuestionViewModel { TlsQuestion = configOutput };
            return View("/Views/TlsQuestion/CreateOrUpdate.cshtml", output);
        }
        [HttpPost]
        [IfModelIsInvalid(IsReturnJson = true)]
        public async Task<DefaultResponse<TlsQuestion>> CreateOrUpdate(CreateTlsQuestionPostModel input)
        {
            var output = new DefaultResponse<TlsQuestion>();
            if (input.TypeOfQuestion == (int)TypeOfQuestion.Checkbox || input.TypeOfQuestion == (int)TypeOfQuestion.Radio || input.TypeOfQuestion == (int)TypeOfQuestion.Select)
            {
                if (input.JSonOptionsText == null || input.JSonOptionsText.Length == 0)
                {
                    output.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                    return output;
                }
                var listSelectListItem = new List<CustomSelectListItem>();
                for (int i = 0; i < input.JSonOptionsText.Length; i++)
                {
                    var textItem = input.JSonOptionsText[i];
                    var valueItem = input.JSonOptionsValue[i];
                    if (!string.IsNullOrEmpty(textItem) && !string.IsNullOrEmpty(valueItem))
                    {
                        listSelectListItem.Add(new CustomSelectListItem(textItem, valueItem));
                    }
                    else
                        continue;
                }
                if (listSelectListItem.Count == 0)
                {
                    output.meta = new Meta(ResponseHelper.BAD_REQUEST_CODE, ResponseHelper.BAD_REQUEST_MESSAGE);
                    return output;
                }
                input.JsonOptions = JsonConvert.SerializeObject(listSelectListItem);
            }
            else
            {
                input.JsonOptions = null;
            }
            output = await _tlsQuestionService.CreateOrUpdateAsync(GetToken(), input);
            return output;
        }



        [HttpDelete]
        public async Task<DefaultResponse<bool>> Delete(int id)
        {
            return await _tlsQuestionService.DeleteByIdAsync(GetToken(), id);
        }
    }
}