﻿using LOS.Common.Helpers;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Filter;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.TlsScenarioDto;
using LOS.WebApp.Services.TlsQuestionService;
using LOS.WebApp.Services.TlsScenarioService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class TLSScenarioController : BaseController
    {
        private ITlsScenarioService _tlsScenaritoService;
        private ITlsQuestionService _tlsQuestionService;
        private IHttpClientFactory clientFactory;
        public TLSScenarioController(IConfiguration configuration, ITlsScenarioService tlsScenaritoService, ITlsQuestionService tlsQuestionService, IHttpClientFactory clientFactory)
            : base(configuration, clientFactory)
        {
            _tlsScenaritoService = tlsScenaritoService;
            _tlsQuestionService = tlsQuestionService;
            this.clientFactory = clientFactory;
        }


        [Route("tls-scenario.html")]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> LoadData()
        {
            try
            {
                var input = new TlsScenarioDatatable(HttpContext.Request.Form);
                // Query api   	
                var inputModel = new ApiPagingPostModel
                {
                    Keyword = input.Keyword,
                    IsSortByAsc = input.sort == "asc" ? true : false,
                    SortColumn = input.field,
                    CurrentPage = int.Parse(input.page),
                    PageSize = int.Parse(input.perpage)
                };

                var data = await _tlsScenaritoService.GetAllAsync(GetToken(), inputModel);
                input.total = data.meta.totalRecords.ToString();
                //Returning Json Data  
                return Json(new { meta = input, data = data.data });
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Lỗi liên hệ kĩ thuật" });
            }
        }


        [HttpGet]
        public async Task<IActionResult> CreateOrUpdate(int? id)
        {
            TlsScenarioViewModel configOutput = new TlsScenarioViewModel() { TlsScenario = new TlsScenario() };
            if (id.HasValue)
            {
                var scenario = await _tlsScenaritoService.GetByIdAsync(GetToken(), id.Value);
                if (scenario.meta.errorCode != ResponseHelper.SUCCESS_CODE && scenario.data != null)
                {
                    return RedirectToAction("Error", "Home");
                }
                var scenarioQuestionIds = await _tlsScenaritoService.GetScenarioQuestionIds(GetToken(), id.Value);
                if (scenarioQuestionIds.meta.errorCode != ResponseHelper.SUCCESS_CODE && scenarioQuestionIds.data != null)
                {
                    return RedirectToAction("Error", "Home");
                }
               
                configOutput.TlsScenario = scenario.data;
                configOutput.TlsScenarioQuestionIds = scenarioQuestionIds.data;
                
            }
            var questions = await _tlsQuestionService.GetAllAsync(GetToken(), new ApiPagingPostModel { CurrentPage = 1, PageSize = 100000000 });
            if (questions.meta.errorCode != ResponseHelper.SUCCESS_CODE && questions.data != null)
            {
                return RedirectToAction("Error", "Home");
            }
            configOutput.Questions = questions.data;

            return View("/Views/TLSScenario/CreateOrUpdate.cshtml", configOutput);
        }

        [HttpPost]
        [IfModelIsInvalid(IsReturnJson = true)]
        public async Task<DefaultResponse<TlsScenario>> CreateOrUpdate(CreateTlsScenarioPostModel input)
        {
            var output = new DefaultResponse<TlsScenario>();
            output = await _tlsScenaritoService.CreateOrUpdateAsync(GetToken(), input);
            return output;
        }

        [HttpDelete]
        public async Task<DefaultResponse<bool>> Delete(int id)
        {
            return await _tlsScenaritoService.DeleteByIdAsync(GetToken(), id);
        }
    }
}