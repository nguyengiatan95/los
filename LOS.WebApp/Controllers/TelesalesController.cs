﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Hazelcast.Client;
using Hazelcast.Config;
using Hazelcast.Core;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.Common.Utils;
using LOS.DAL.DTOs;
using LOS.DAL.Helpers;
using LOS.Services.Services.Telesales;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class TelesalesController : BaseController
    {
        private readonly IHubContext<TelesalesHub> _hubContext;
        private ITelesalesServices _telesalesServices;
        private static IHazelcastInstance client;
        private ILoanBriefService _loanBriefService;
        private ILogWorkingService _logWorkingService;
        private IDictionaryService _dictionaryService;
        private IUserServices _userServices;
        private IHttpClientFactory clientFactory;

        private ITelesalesV2Service _telesalesV2Service;

        public TelesalesController(IUserServices userServices, IConfiguration configuration, IHubContext<TelesalesHub> hubContext,
            ITelesalesServices _services, ILoanBriefService loanBriefService, ILogWorkingService logWorkingService, IDictionaryService dictionaryService,
            IHttpClientFactory clientFactory, ITelesalesV2Service telesalesV2Service) : base(configuration: configuration, clientFactory)
        {
            _userServices = userServices;
            this._hubContext = hubContext;
            this._telesalesServices = _services;
            this._loanBriefService = loanBriefService;
            this._logWorkingService = logWorkingService;
            this._dictionaryService = dictionaryService;
            this._dictionaryService = dictionaryService;
            //var cfg = new ClientConfig();
            // set group name and password
            // GroupConfig groupConfig = new GroupConfig();
            // groupConfig.SetName("dev");
            // grougroupConfigp.SetPassword("J7MLhWQtUC");
            // cfg.SetGroupConfig(groupConfig);
            //cfg.GetNetworkConfig().AddAddress("178.128.86.175:5701");
            //client = HazelcastClient.NewHazelcastClient(cfg);
            this.clientFactory = clientFactory;
            this._telesalesV2Service = telesalesV2Service;
        }

        #region telesale dashboard
        //[Route("/telesales/dashboard.html")]
        public IActionResult DashBoard()
        {
            ViewBag.User = GetUserGlobal();
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ListScheduleToDay()
        {
            var user = GetUserGlobal();
            var modal = new DatatableBase(HttpContext.Request.Form);
            var data = _loanBriefService.GetLoanBriefSchedule(GetToken(), user.UserId, DateTime.Now);
            modal.total = data.Count().ToString();
            //Returning Json Data  
            return Json(new { meta = modal, data = data.OrderByDescending(x => x.ScheduleTime) });
        }

        public IActionResult GetDataHandelToday()
        {
            var result = new LoanbriefHandel();
            var user = GetUserGlobal();
            var data = _loanBriefService.GetLoanBriefHandle(GetToken(), user.UserId, DateTime.Now);
            if (data != null && data.Count > 0)
            {
                result.TotalPush = data.Count(x => x.TelesalesPushAt.HasValue);
                result.TotalCancel = data.Count(x => x.LoanBriefCancelAt.HasValue && x.LoanBriefCancelBy == user.UserId);
            }
            return Json(new { data = result });
        }

        [HttpPost]
        public async Task<IActionResult> LogWorkingUser(int state)
        {
            var user = GetUserGlobal();
            if (user.GroupId == EnumGroupUser.Telesale.GetHashCode())
            {
                var entity = new LogWorkingDTO()
                {
                    UserId = user.UserId,
                    CreateAt = DateTime.Now,
                    State = state
                };
                _logWorkingService.Add(GetToken(), entity);
                return Json(new { status = 1, message = "Success" });
            }
            else
            {
                return Json(new { status = 0, message = "User không thuộc nhóm telesale" });
            }

        }

        public async Task<IActionResult> LoanBriefInfo(string phone, int? loanbriefId = 0)
        {
            var model = new LoanBriefDetail();
            try
            {
                if (loanbriefId > 0)
                {
                    var loanBrief = await _loanBriefService.GetLoanBriefById(GetToken(), loanbriefId.Value);
                    if (loanBrief.Phone == phone)
                    {
                        model.Phone = loanBrief.Phone;
                        model.FullName = loanBrief.FullName;
                        model.District = new DistrictDTO();
                        if (loanBrief.District != null)
                            model.District.Name = loanBrief.District.Name;
                        model.LoanBriefId = loanBrief.LoanBriefId;
                        model.LoanTime = loanBrief.LoanTime;
                        model.LoanAmount = loanBrief.LoanAmount;
                        model.LoanProduct = new LoanProductDTO();
                    }
                }
                else if (!string.IsNullOrEmpty(phone))
                {
                    var loanBrief = _loanBriefService.GetLoanBriefByPhone(GetToken(), phone);
                    if (loanBrief != null && loanBrief.Count > 0)
                    {
                        model = loanBrief.OrderByDescending(x => x.LoanBriefId).FirstOrDefault();
                    }
                    else
                    {
                        model.Phone = phone;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return PartialView("~/Views/Telesales/Partial/_LoanbriefInfo.cshtml", model);
        }
        #endregion

        #region smartdailer cisco
        [Route("/telesales/dashboard.html")]
        public IActionResult SmartDailer()
        {
            ViewBag.User = GetUserGlobal();
            return View();
        }
        #endregion

        public IActionResult Index()
        {
            // Get Username
            var user = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result;
            ViewBag.Username = user.Username;
            return View();
        }

        public IActionResult TriggerOnline([FromQuery] string state)
        {
            try
            {
                //get session
                var user = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result;
#if !DEBUG
				var users = client.GetMap<int, HazelcastJsonValue>("online_users");
#else
                var users = client.GetMap<int, HazelcastJsonValue>("online_users_test");
#endif
                var onlineUserJson = users.Get(user.UserId);
                if (onlineUserJson != null)
                {
                    // update time 
                    var onlineUser = JsonConvert.DeserializeObject<OnlineUser>(onlineUserJson.ToString());
                    // kiểm tra nếu last trigger < 1 phút
                    TimeSpan difference = DateTimeOffset.Now - onlineUser.LastTriggerTime;
                    double seconds = difference.TotalSeconds;
                    if (seconds <= 18)
                    {
                        return Ok(onlineUser);
                    }
                    // fix isprocessing
                    // onlineUser.IsProcessing = false;
                    // check if next day
                    if (onlineUser.LoginTime.DayOfYear == DateTimeOffset.Now.DayOfYear)
                    {
                        onlineUser.LastTriggerTime = DateTimeOffset.Now;
                        onlineUser.IsOnline = true;
                        onlineUser.OnlineTime += 20;
                        onlineUser.Username = user.Username;
                        onlineUser.UserId = user.UserId;
                        if (state == "true")
                        {
                            onlineUser.IsWorking = true;
                            onlineUser.WorkingTime += 20;
                        }
                        else
                        {
                            onlineUser.IsWorking = false;
                            onlineUser.RestTime += 20;
                        }
                    }
                    else
                    {
                        // next day
                        onlineUser.LoginTime = DateTimeOffset.Now;
                        onlineUser.LastTriggerTime = DateTimeOffset.Now;
                        onlineUser.IsOnline = true;
                        onlineUser.OnlineTime = 20;
                        onlineUser.Username = user.Username;
                        onlineUser.UserId = user.UserId;
                        if (state == "true")
                        {
                            onlineUser.IsWorking = true;
                            onlineUser.WorkingTime = 20;
                            onlineUser.RestTime = 0;
                        }
                        else
                        {
                            onlineUser.IsWorking = false;
                            onlineUser.WorkingTime = 0;
                            onlineUser.RestTime = 20;
                        }
                    }
                    users.Set(user.UserId, new HazelcastJsonValue(JsonConvert.SerializeObject(onlineUser)));
                    // send statistic every minute
                    //_hubContext.Clients.Group(user.Username).SendAsync("SendStatistics", JsonConvert.SerializeObject(onlineUser));
                    return Ok(onlineUser);
                }
                else
                {
                    //create new
                    var onlineUser = new OnlineUser();
                    onlineUser.LoginTime = DateTimeOffset.Now;
                    onlineUser.IsOnline = true;
                    onlineUser.IsProcessing = false;
                    onlineUser.OnlineTime = 20;
                    if (state == "true")
                    {
                        onlineUser.IsWorking = true;
                        onlineUser.WorkingTime = 20;
                        onlineUser.RestTime = 0;
                    }
                    else
                    {
                        onlineUser.IsWorking = false;
                        onlineUser.WorkingTime = 0;
                        onlineUser.RestTime = 20;
                    }
                    onlineUser.UserId = user.UserId;
                    onlineUser.Username = user.Username;
                    onlineUser.GroupId = user.GroupId;
                    onlineUser.LastTriggerTime = DateTimeOffset.Now;
                    users.Put(user.UserId, new HazelcastJsonValue(JsonConvert.SerializeObject(onlineUser)));
                    // send statistic every minute
                    //_hubContext.Clients.Group(user.Username).SendAsync("SendStatistics", JsonConvert.SerializeObject(onlineUser));
                    return Ok(onlineUser);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
            finally
            {

            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateCallStatus([FromBody]UpdateCallStatusReq req)
        {
            try
            {
                var result = _telesalesServices.UpdateCallStatus(GetToken(), req);
                // check if telesale finish
                if (req.actionStatusId >= 5 && result.meta.errorCode == 200) // TELESALE_FINISH
                {
                    try
                    {
                        //get session
                        var user = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result;
                        var users = client.GetMap<int, HazelcastJsonValue>("online_users");
                        var onlineUserJson = users.Get(user.UserId);
                        if (onlineUserJson != null)
                        {
                            // update time 
                            var onlineUser = JsonConvert.DeserializeObject<OnlineUser>(onlineUserJson.ToString());
                            onlineUser.IsProcessing = false;
                            users.Set(user.UserId, new HazelcastJsonValue(JsonConvert.SerializeObject(onlineUser)));
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {

                    }
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateScheduleCall([FromBody]UpdateScheduleCallReq req)
        {
            try
            {
                var result = _telesalesServices.UpdateScheduleCall(GetToken(), req);
                // check if telesale finish
                if (result.meta.errorCode == 200) // Success
                {
                    // update hazelcast				
                    try
                    {
                        //get session
                        var user = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result;
                        var users = client.GetMap<int, HazelcastJsonValue>("online_users");
                        var onlineUserJson = users.Get(user.UserId);
                        if (onlineUserJson != null)
                        {
                            // update time 
                            var onlineUser = JsonConvert.DeserializeObject<OnlineUser>(onlineUserJson.ToString());
                            onlineUser.IsProcessing = false;
                            users.Set(user.UserId, new HazelcastJsonValue(JsonConvert.SerializeObject(onlineUser)));
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {

                    }
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<IActionResult> GetMissedCalls()
        {
            try
            {
                var modal = new TelesalesDatatable(HttpContext.Request.Form);
                var user = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result;
                modal.userId = user.UserId.ToString();
                int recordsTotal = 0;
                // Query api   	
                var data = _telesalesServices.GetMissedCalls(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IActionResult> GetScheduledCalls()
        {
            try
            {
                var modal = new TelesalesDatatable(HttpContext.Request.Form);
                var user = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result;
                modal.userId = user.UserId.ToString();
                int recordsTotal = 0;
                // Query api   	
                var data = _telesalesServices.GetScheduleCalls(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IActionResult> GetCalled()
        {
            try
            {
                var modal = new TelesalesDatatable(HttpContext.Request.Form);
                var user = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result;
                modal.userId = user.UserId.ToString();
                int recordsTotal = 0;
                // Query api   	
                var data = _telesalesServices.GetCalled(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> BoundTelesale([FromBody]BoundTelesaleReq req)
        {
            try
            {
                var user = HttpContext.Session.GetObjectFromJson<UserDetail>("USER_DATA").Result;
                req.userId = user.UserId;
                var result = _telesalesServices.BoundTelesale(GetToken(), req);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<IActionResult> UpdateLoanBriefModalPartialAsync()
        {
            return View("/Views/Telesales/Partial/_UpdateLoanBriefModal.cshtml");
        }

        public async Task<IActionResult> UpdateLoanBriefBoundModalPartialAsync()
        {
            return View("/Views/Telesales/Partial/_UpdateLoanBriefModalBound.cshtml");
        }

        #region Quản lý nhân viên Telesale
        [Route("/telesales/managestaff.html")]
        [PermissionFilter]
        public IActionResult ManageStaffTelesale()
        {
            var user = GetUserGlobal();
            var lstTeamTelesalesId = user.ListUserTeamTelesales.Select(x => x.TeamTelesalesId).ToList();
            var lstTelesalesShift = _telesalesServices.GetListTelesalesShift(GetToken());
            var lstTeamTelesales = _telesalesServices.GetListTeamTelesales(GetToken()).Where(x => x.TeamTelesalesId > 0 && lstTeamTelesalesId.Contains(x.TeamTelesalesId)).ToList();
            ViewBag.LstTelesalesShift = lstTelesalesShift;
            ViewBag.LstTeamTelesales = lstTeamTelesales;
            return View();
        }
        [HttpPost]
        public IActionResult LoadDataStaffTelesale()
        {
            try
            {
                var user = GetUserGlobal();
                var modal = new TelesalesDatatable(HttpContext.Request.Form);
                //if (user.ListUserTeamTelesales != null && user.ListUserTeamTelesales.Count > 0)
                //{
                //    modal.teamTelesales = string.Join(",", user.ListUserTeamTelesales.Select(x => x.TeamTelesalesId).ToList());
                //}
                int recordsTotal = 0;
                // Query api   	
                List<UserDetail> data = new List<UserDetail>();
                data = _telesalesServices.Search(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                var lstTelesalesShift = _telesalesServices.GetListTelesalesShift(GetToken());

                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        if (lstTelesalesShift != null && lstTelesalesShift.Count > 0)
                            item.ListTelesalesShift = lstTelesalesShift;

                        item.UrlEdit = EncryptUtils.Encrypt(string.Format("?Id={0}", item.UserId), true);
                    }
                }
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        public IActionResult UpdateTelesaleRecievedLoan(int Id, int TelesaleRecievedLoan)
        {
            var obj = new UserDTO();
            var userInfo = _userServices.GetById(GetToken(), Id);
            Common.Utils.Ultility.CopyObject(userInfo, ref obj, true);
            obj.TelesaleRecievedLoan = Convert.ToBoolean(TelesaleRecievedLoan);

            var result = _userServices.Update(GetToken(), Id, obj);
            if (result)
                return Json(new { status = 1, message = "Cấu hình thành công" });
            return Json(new { status = 0, message = "Xảy ra lỗi khi cấu hình. Vui lòng liên hệ quản trị viên!" });
        }
        [HttpPost]
        public async Task<IActionResult> ChangeLook(string[] ArrId, int TelesaleRecievedLoan)
        {
            for (int i = 0; i < ArrId.Length; i++)
            {
                var obj = new UserDTO();
                var userId = Convert.ToInt32(ArrId[i]);
                var userInfo = _userServices.GetById(GetToken(), userId);
                Common.Utils.Ultility.CopyObject(userInfo, ref obj, true);
                obj.TelesaleRecievedLoan = Convert.ToBoolean(TelesaleRecievedLoan);

                var result = _userServices.Update(GetToken(), userId, obj);
                if (!result)
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cấu hình. Vui lòng liên hệ quản trị viên!" });
            }
            return Json(new { status = 1, message = "Cấu hình thành công" });
        }
        #endregion

        [HttpGet]
        public IActionResult GetTelesalesByTeam()
        {
            var user = GetUserGlobal();
            var TeamTelesales = "";
            if (user.ListUserTeamTelesales != null && user.ListUserTeamTelesales.Count > 0)
                TeamTelesales = string.Join(",", user.ListUserTeamTelesales.Select(x => x.TeamTelesalesId).ToList());
            var lstData = _telesalesServices.GetTelesalesByTeam(user.Token, TeamTelesales);
            return Json(new { data = lstData });
        }

        public async Task<IActionResult> GetTeamEnable()
        {
            var lstData = await _telesalesV2Service.GetTeamEnable();
            return Json(new { data = lstData });
        }
        public async Task<IActionResult> GetTelesalesByTeamV2(int team)
        {
            var lstData = await _telesalesV2Service.GetTelesalesByTeam(team);
            return Json(new { data = lstData });
        }
    }
}