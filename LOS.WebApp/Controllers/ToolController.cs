﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.DAL.Models;
using LOS.DAL.Object;
using LOS.Services.Services;
using LOS.Services.Services.Loanbrief;
using LOS.Services.Services.LoanBriefNote;
using LOS.Services.Services.LogLoanInfoAI;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Cisco;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Services;
using LOS.WebApp.Services.CiscoService;
using LOS.WebApp.Services.ToolService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using static LOS.DAL.Object.Tool.ToolReq;
using static LOS.WebApp.Models.Cisco.Cisco;

namespace LOS.WebApp.Controllers
{
    public class ToolController : BaseController
    {
        private IConfiguration _configuration;
        private ICareSoftService _caresoftService;
        private ILogLoanInfoAi _logLoanInfoAiService;
        private ICiscoService _ciscoService;
        private ILogCallApi _logCallApi;
        private IHttpClientFactory clientFactory;
        private ILoanBriefService _loanBriefService;
        private ILMSService _lmsService;
        private IDictionaryService _dictionaryService;
        private ICompositeViewEngine viewEngine;
        private IToolService _toolService;
        private ISSOService _ssoService;
        private IUserServices _userServices;

        private ILogLoanInfoAIV2Service _logLoanInfoAIV2Service;
        private ILoanBriefNoteV2Service _loanBriefNoteV2Service;
        private ILoanbriefV2Service _loanbriefV2Service;
        private IRuleCheckLoanService _ruleCheckLoanService;
        private ITrackDeviceService _trackDeviceService;
        private IErp _erpService;
        private IPushLoanToPartnerService _pushLoanToPartnerService;

        public ToolController(IConfiguration configuration, ICareSoftService caresoftService, ILogLoanInfoAi logLoanInfoAiService,
            ILogCallApi logCallApi, ICiscoService ciscoService, IHttpClientFactory clientFactory, ICompositeViewEngine viewEngine,
            ILoanBriefService loanBriefServices, ILMSService lmsService, IDictionaryService dictionaryService, IToolService toolService,
            ISSOService ssoService, IUserServices userServices, ILogLoanInfoAIV2Service logLoanInfoAIV2Service, ILoanBriefNoteV2Service loanBriefNoteV2Service,
            ILoanbriefV2Service loanbriefV2Service, IRuleCheckLoanService ruleCheckLoanService, ITrackDeviceService trackDeviceService, IErp erpService,
            IPushLoanToPartnerService pushLoanToPartnerService)
           : base(configuration, clientFactory)
        {
            _caresoftService = caresoftService;
            this.viewEngine = viewEngine;
            _configuration = configuration;
            _logLoanInfoAiService = logLoanInfoAiService;
            _ciscoService = ciscoService;
            _logCallApi = logCallApi;
            this.clientFactory = clientFactory;
            _loanBriefService = loanBriefServices;
            _lmsService = lmsService;
            _dictionaryService = dictionaryService;
            _toolService = toolService;
            _ssoService = ssoService;
            _userServices = userServices;
            _logLoanInfoAIV2Service = logLoanInfoAIV2Service;
            _loanBriefNoteV2Service = loanBriefNoteV2Service;
            _loanbriefV2Service = loanbriefV2Service;
            _ruleCheckLoanService = ruleCheckLoanService;
            _trackDeviceService = trackDeviceService;
            _erpService = erpService;
            _pushLoanToPartnerService = pushLoanToPartnerService;
        }


        [Route("/sound-recording/index.html")]
        //[PermissionFilter]
        public IActionResult Recording()
        {
            return View();
        }
        [PermissionFilter]
        public async Task<IActionResult> SearchRecording(string phone, int loanBriefId, string timerange, int type = 1)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = GetToken();
                //phone = "0964345411";

                var fromDate = DateTime.Now;
                var toDate = DateTime.Now;
                var dataResult = new RecordingModelView()
                {
                    ListRecording = new List<RecordingItemView>()
                };
                //Kiểm tra nếu là nhóm telesale thì check có phải đơn được chia
                if (user.GroupId == (int)EnumGroupUser.Telesale)
                {
                    var check = await _loanBriefService.CheckLoanAssign(access_token, loanBriefId, phone, user.UserId);
                    if (!check)
                    {
                        dataResult.MessErr = "Chỉ nghe được ghi âm trên những đơn vay của bạn!";
                        return PartialView("_SearchRecordingV2", dataResult);
                    };
                }
                var listPhone = new List<string>();
                var listRecording = new List<RecordingItemView>();
                listPhone.Add(phone);
                if (!string.IsNullOrEmpty(timerange))
                    DateTimeUtility.ConvertDateRanger(timerange, ref fromDate, ref toDate);


                if (loanBriefId > 0)
                {
                    var loanInfo = await _loanBriefService.GetLoanBriefById(GetToken(), loanBriefId);
                    if (loanInfo != null)
                    {
                        phone = loanInfo.Phone;
                        dataResult.FullName = loanInfo.FullName;
                        dataResult.LoanbriefId = loanInfo.LoanBriefId;
                    }

                }
                var data = new List<RecordItemV2>();
                var callApiCaresoft = false;
                if (type == 1)
                {
                    if (_baseConfig["AppSettings:GET_RECORD_API_CARESOFT"] != null && _baseConfig["AppSettings:GET_RECORD_API_CARESOFT"] == "1")
                    {
                        data = _caresoftService.GetListApiCaresoft(listPhone, fromDate, toDate, loanBriefId, GetToken());
                        callApiCaresoft = true;
                    }
                    else
                    {
                        var fromDateCS = DateTimeUtility.ConvertGMTDOW(fromDate);
                        var toDateCS = DateTimeUtility.ConvertGMTDOW(toDate);
                        data = _caresoftService.GetListRecordV2(listPhone, fromDateCS, toDateCS, loanBriefId, GetToken());
                    }

                    if (data != null && data.Count > 0)
                    {
                        var lstIpPhone = data.Select(x => Convert.ToInt32(x.AgentId)).Distinct().ToList();
                        var obj = new GetUserReq();
                        obj.lstIpphone = lstIpPhone;
                        obj.Type = 1;
                        var lstUserName = _userServices.GetListUserByIpPhone(GetToken(), obj).ToList();
                        var dicUsername = new Dictionary<int, UserDetail>();
                        if (lstUserName != null && lstUserName.Count > 0)
                        {
                            var dataUser = lstUserName.OrderByDescending(x => x.Status.GetValueOrDefault(0)).ToList();
                            foreach (var item in dataUser)
                            {
                                if (item.Ipphone.HasValue && !dicUsername.ContainsKey(item.Ipphone.Value))
                                    dicUsername[item.Ipphone.Value] = item;
                            }
                        }
                        if (dicUsername.Keys.Count > 0)
                        {
                            foreach (var item in data)
                            {
                                if (item.AgentId != null && dicUsername.ContainsKey(Convert.ToInt32(item.AgentId)))
                                {
                                    var result = dicUsername[Convert.ToInt32(item.AgentId)];
                                    item.user_name = result.Username;
                                    item.department_name = result.Group?.GroupName;
                                }
                                if (!callApiCaresoft)
                                {
                                    if (item.StartTime.HasValue)
                                        item.StartTime = LOS.WebApp.Helpers.DateTimeUtility.ConvertGMTUP(item.EndTime.Value);
                                    if (item.EndTime.HasValue)
                                        item.EndTime = LOS.WebApp.Helpers.DateTimeUtility.ConvertGMTUP(item.EndTime.Value);
                                    if (item.WaitingTime > 0)
                                        item.wait_time = LOS.WebApp.Helpers.DateTimeUtility.ConvertHHmmss(item.WaitingTime);
                                    if (item.AcdTime > 0)
                                        item.talk_time = LOS.WebApp.Helpers.DateTimeUtility.ConvertHHmmss(item.AcdTime);
                                }
                                var recordingItem = new RecordingItemView()
                                {
                                    TypeCallService = (int)EnumTypeCallService.Caresoft,
                                    Phone = item.Caller,
                                    FullName = item.full_name,
                                    RelationshipName = item.relationship_name,
                                    UserName = item.user_name,
                                    DepartmentName = item.department_name,
                                    StartTime = item.StartTime,
                                    EndTime = item.EndTime,
                                    StrWaitingTime = item.wait_time,
                                    StrTalkTime = item.talk_time,
                                    Path = item.Path
                                };
                                listRecording.Add(recordingItem);
                            }
                        }
                    }
                }
                else
                {
                    var now = DateTime.Now;
                    string beginTime = now.ToString("yyyy-MM-dd");
                    var cisco = _ciscoService.GetListByTimeV2(GetToken(), phone, fromDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd"));
                    if (cisco != null)
                    {
                        var lstLineCiscoWeb = cisco.Where(x => x.ContactType == "CALL").Select(x => Convert.ToInt32(x.Line)).Distinct().ToList();
                        var lstUserName = _userServices.GetListUserByIpPhone(access_token, new GetUserReq()
                        {
                            lstIpphone = lstLineCiscoWeb,
                            Type = 2
                        }).ToList();
                        var dicUsername = new Dictionary<int, UserDetail>();
                        if (lstUserName != null && lstUserName.Count > 0)
                        {
                            foreach (var item in lstUserName)
                            {
                                if (item.CiscoExtension != null && !dicUsername.ContainsKey(Convert.ToInt32(item.CiscoExtension)))
                                    dicUsername[Convert.ToInt32(item.CiscoExtension)] = item;
                            }
                        }
                        var lstLineCiscoMobile = cisco.Where(x => x.ContactType == "CALL_MOBILE").Select(x => Convert.ToInt32(x.Line)).Distinct().ToList();
                        var lstUserNameMobile = _userServices.GetListUserByIpPhone(access_token, new GetUserReq()
                        {
                            lstIpphone = lstLineCiscoWeb,
                            Type = 3
                        }).ToList();

                        var dicUsernameMobile = new Dictionary<int, UserDetail>();
                        if (dicUsernameMobile != null && dicUsernameMobile.Count > 0)
                        {
                            foreach (var item in lstUserNameMobile)
                            {
                                if (item.CiscoExtension != null && !dicUsernameMobile.ContainsKey(Convert.ToInt32(item.CiscoExtension)))
                                    dicUsernameMobile[Convert.ToInt32(item.CiscoExtension)] = item;
                            }
                        }

                        foreach (var item in cisco)
                        {

                            if (item.PlayUrl.StartsWith("http:192"))
                                item.PlayUrl = item.PlayUrl.Remove(0, 5).Insert(0, "http://");
                            if (!string.IsNullOrEmpty(item.Line))
                            {
                                if (item.ContactType == "CALL")
                                {
                                    if (dicUsername.ContainsKey(Convert.ToInt32(item.Line)))
                                    {
                                        var result = dicUsername[Convert.ToInt32(item.Line)];
                                        item.Username = result.Username;
                                        item.DepartmentName = result.Group?.GroupName;
                                    }

                                }
                                else if (item.ContactType == "CALL_MOBILE")
                                {
                                    if (dicUsernameMobile.ContainsKey(Convert.ToInt32(item.Line)))
                                    {
                                        var result = dicUsernameMobile[Convert.ToInt32(item.Line)];
                                        item.Username = result.Username;
                                        item.DepartmentName = result.Group?.GroupName;
                                    }
                                }
                            }

                            var recordingItem = new RecordingItemView()
                            {
                                TypeCallService = (int)EnumTypeCallService.Metech,
                                Phone = item.PhoneNumber,
                                FullName = item.FullName,
                                RelationshipName = item.RelationshipName,
                                UserName = item.Username,
                                DepartmentName = item.DepartmentName,
                                StartTime = item.StartTime,
                                StrTalkTime = item.CallDuration > 1000 ? DateTimeUtility.ConvertHHmmss(item.CallDuration / 1000) : "",
                                CallDuration = item.CallDuration,
                                PlayUrl = item.PlayUrl,
                                RecordingUrl = item.RecordingUrl
                            };
                            if (item.ContactType == "CALL_MOBILE")
                                recordingItem.TypeCallService = (int)EnumTypeCallService.MetechMobile;
                            listRecording.Add(recordingItem);
                        }
                    }

                }
                if (listRecording != null && listRecording.Count > 0)
                    dataResult.ListRecording = listRecording.OrderByDescending(x => x.StartTime).ToList();
                return PartialView("_SearchRecordingV2", dataResult);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IActionResult DownloadFile(int Id)
        {
            string url = "http://192.168.100.4:80/export/recording-3-11006-419912450542.wav";
            //using (var client = new WebClient())
            //{
            //    client.DownloadFile(url, "a.wav");
            //}
            return Json(new { status = 1, message = "Download File thành công" });
        }

        #region tra cứu request gọi AI
        [Route("/log-reqest-ai/index.html")]
        [PermissionFilter]
        public IActionResult LogRequestAi()
        {
            return View();
        }

        public IActionResult SearchLogRequestAI(int loanbriefId, int serviceType)
        {
            try
            {

                var data = _logLoanInfoAiService.GetRequest(GetToken(), loanbriefId, serviceType);


                return PartialView("_SearchLogRequestAI", data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult GetDetailLogRequestAi(int Id)
        {
            try
            {
                var data = _logLoanInfoAiService.GetById(GetToken(), Id);
                return View(data);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region tra cứu log call api
        [Route("/log-call-api/index.html")]
        [PermissionFilter]
        public IActionResult LogCallApi()
        {
            return View();
        }
        [HttpPost]
        public IActionResult SearchLogCallApi(int loanbriefId, int actionCallApi)
        {
            try
            {
                var data = _logCallApi.GetRequest(GetToken(), loanbriefId, actionCallApi);
                return PartialView(data);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult GetDetailLogCallApi(int Id)
        {
            try
            {
                var data = _logCallApi.GetById(GetToken(), Id);
                return View(data);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Báo cáo tổng thời gian gọi theo đầu số Cisco
        [Route("/recor-cisco/index.html")]
        //[PermissionFilter]
        public IActionResult CiscoIndex()
        {
            return View();
        }
        //[PermissionFilter]
        public async Task<IActionResult> SearchRecordingCisco(string timerange)
        {
            try
            {

                var fromDate = DateTime.Now;
                var toDate = DateTime.Now;
                if (!string.IsNullOrEmpty(timerange))
                    DateTimeUtility.ConvertDateRangerV2(timerange, ref fromDate, ref toDate);

                string FromDate = fromDate.ToString("yyyy-MM-dd");
                string ToDate = toDate.ToString("yyyy-MM-dd");
                var CSRFTOKEN = "";
                var JSESSIONID = "";
                //if (string.IsNullOrEmpty(HttpContext.Session.GetString("CSRFTOKEN")) || string.IsNullOrEmpty(HttpContext.Session.GetString("JSESSIONID")))
                //{
                var authen = _ciscoService.Authorize(GetToken());
                if (authen != null)
                {
                    CSRFTOKEN = authen[0].CSRFTOKEN;
                    JSESSIONID = authen[0].JSESSIONID;
                    //HttpContext.Session.SetObjectAsString("CSRFTOKEN", authen[0].CSRFTOKEN);
                    //HttpContext.Session.SetObjectAsString("JSESSIONID", authen[0].JSESSIONID);
                }
                //}
                //CSRFTOKEN = await HttpContext.Session.GetObjectFromString<string>("CSRFTOKEN");
                //JSESSIONID = await HttpContext.Session.GetObjectFromString<string>("JSESSIONID");

                var data = _ciscoService.GetListByTime(GetToken(), "", FromDate, ToDate, CSRFTOKEN, JSESSIONID);
                var lst = new List<ListExport>();
                if (data != null)
                {
                    var dic = new Dictionary<string, string>();
                    for (int i = 0; i < data.Count; i++)
                    {
                        var item = data[i];
                        var prefix = "";
                        var phone = "";
                        var departmentName = "";

                        if (item.Dnis.StartsWith("701") || item.Dnis.StartsWith("702") || item.Dnis.StartsWith("703") || item.Dnis.StartsWith("704") || item.Dnis.StartsWith("705") || item.Dnis.StartsWith("706")
                            || item.Dnis.StartsWith("707") || item.Dnis.StartsWith("708") || item.Dnis.StartsWith("709") || item.Dnis.StartsWith("710"))
                        {
                            prefix = item.Dnis.Remove(3);
                            phone = item.Dnis.Remove(0, 3);
                            departmentName = "Thu hồi nợ";
                        }
                        else
                        {
                            prefix = "Other";
                            phone = item.Dnis;

                        }
                        switch (prefix)
                        {
                            case "701":
                                phone = "0969286028-0936045938-0916391265";
                                break;
                            case "702":
                                phone = "0969289792-0936092538-0911255850";
                                break;
                            case "703":
                                phone = "0969291429-0902260938-0911648914";
                                break;
                            case "704":
                                phone = "0969301489-0903442638-0916271542";
                                break;
                            case "705":
                                phone = "0969302766-0936426499-0911218574";
                                break;
                            case "706":
                                phone = "0969303073-0934537499-0915387742";
                                break;
                            case "707":
                                phone = "0969303084-0904590038-0919073954";
                                break;
                            case "708":
                                phone = "0969303832-0934475498-0916218834";
                                break;
                            case "709":
                                phone = "0969310486-0936399661-0915027894";
                                break;
                            case "710":
                                phone = "0969310680-0904561800-0918247746";
                                break;
                            default:
                                phone = "";
                                break;
                        }
                        var obj = new ListExport()
                        {
                            Prefix = prefix,
                            DepartmentName = departmentName,
                            Phone = phone,
                            CallDuration = item.CallDuration.GetValueOrDefault(0)
                        };
                        lst.Add(obj);

                    }
                }
                List<ListExport> lstdata = lst
                    .GroupBy(l => l.Prefix)
                    .Select(cl => new ListExport
                    {
                        Phone = cl.First().Phone,
                        DepartmentName = cl.First().DepartmentName,
                        Prefix = cl.First().Prefix,
                        CallDuration = cl.Sum(c => c.CallDuration),
                        Time = TimeSpan.FromMilliseconds(cl.Sum(c => c.CallDuration))
                    }).ToList();

                lstdata = lstdata.OrderBy(x => x.Prefix).ToList();
                ViewBag.CallDuration = lstdata.Sum(x => x.CallDuration);
                return PartialView("_ExportRecordingCisco", lstdata);

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Tính khoảng cách lat long
        [Route("/calculate-range-latlng/index.html")]
        [PermissionFilter]
        public IActionResult CalculatrRangeLatLng()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CalculatrRangeLatLng(string FromLatLng, string ToLatLng)
        {
            var result = Ultility.DistanceTwoPoint(FromLatLng, ToLatLng);
            return Json(new { data = result });
        }
        #endregion

        #region Tool tính lãi phí trả sớm
        [Route("/premature_interest_tool/index.html")]
        [PermissionFilter]
        public IActionResult PrematureInterestTool()
        {
            return View();
        }
        public async Task<IActionResult> CalculatePrematureInterest(int LoanBriefId = 0, string TextSearch = "", string Date = "")
        {
            var access_token = GetToken();
            var date = DateTimeUtility.ConvertStringToDate(Date, DateTimeUtility.DATE_FORMAT);
            if (date < DateTime.Now.AddDays(-1))
                return Json(GetBaseObjectResult(false, "Ngày tất toán phải lớn hơn ngày hiện tại.", null, null));
            List<int> lstLoanId = new List<int>();

            var infomationCustomer = new List<LoanBriefPrematureInterest>();
            var lstItemPrematureInterestMoneyCustomer = new List<ItemPrematureInterestMoneyCustomer>();
            var lstPrematureInterestPaymentSchedules = new List<PrematureInterestPaymentSchedules>();
            if (LoanBriefId > 0)
            {
                //lấy thông tin cơ bản của đơn vay
                var loanbrief = await _loanbriefV2Service.GetBasicInfo(LoanBriefId);
                if (loanbrief != null)
                {
                    if (loanbrief.Status != (int)EnumLoanStatus.FINISH && loanbrief.Status != (int)EnumLoanStatus.DISBURSED)
                        return Json(GetBaseObjectResult(false, "Hợp đồng chưa được giải ngân. Vui lòng kiểm tra lại.", null, null));
                    else
                    {
                        //add thông tin đơn vay trả ra view
                        var loanBriefPrematureInterest = new LoanBriefPrematureInterest()
                        {
                            FullName = loanbrief.FullName,
                            IsLocate = loanbrief.IsLocate,
                            Status = loanbrief.Status,
                            LmsLoanId = loanbrief.LmsLoanId,
                            LoanBriefId = loanbrief.LoanBriefId,
                            CodeId = loanbrief.CodeId,
                        };
                        infomationCustomer.Add(loanBriefPrematureInterest);
                        lstLoanId.Add(loanbrief.LmsLoanId.Value);
                    }
                }
                else
                    return Json(GetBaseObjectResult(false, "Không tìm thấy đơn vay. Vui lòng kiểm tra lại.", null, null));
            }
            else if (!string.IsNullOrEmpty(TextSearch))
            {
                if (TextSearch.Length == 10)
                {
                    if (!TextSearch.ValidPhone())
                        return Json(GetBaseObjectResult(false, "Số điện thoại không đúng định dạng. Vui lòng kiểm tra lại.", null, null));
                }

                //lấy danh sách đơn vay
                var lstLoanBrief = await _loanbriefV2Service.GetListLoanBriefByPhoneOrNationalCard(TextSearch);
                if (lstLoanBrief != null && lstLoanBrief.Count > 0)
                {
                    foreach (var item in lstLoanBrief)
                    {
                        infomationCustomer.Add(item);
                        lstLoanId.Add(item.LmsLoanId.Value);
                    }
                }
                else
                    return Json(GetBaseObjectResult(false, "Không tìm thấy đơn vay. Vui lòng kiểm tra lại.", null, null));
            }
            else
                return Json(GetBaseObjectResult(false, "Phải nhập nhập SĐT (CMND) hoặc Mã hợp đồng để tìm kiếm.", null, null));

            if (lstLoanId != null && lstLoanId.Count > 0)
            {
                var lstData = new List<PrematureInterest>();
                foreach (var item in lstLoanId)
                {
                    var entity = new RequestPrematureInterest()
                    {
                        LoanID = item,
                        CloseDate = Date
                    };
                    var dataLms = _lmsService.GetPayAmountForCustomerByLoanID(GetToken(), entity, LoanBriefId);
                    if (dataLms != null && dataLms.Data != null && dataLms.Data.LstLoanInfos != null && dataLms.Data.LstLoanInfos.Count > 0)
                    {
                        lstData.Add(dataLms);
                    }
                }

                if (lstData != null && lstData.Count > 0)
                {
                    foreach (var item in lstData)
                    {
                        foreach (var loanInfo in item.Data.LstLoanInfos)
                        {
                            lstItemPrematureInterestMoneyCustomer.Add(new ItemPrematureInterestMoneyCustomer()
                            {
                                CodeId = loanInfo.LoanCodeID,
                                TotalMoneyCurrent = loanInfo.TotalMoneyCurrent,
                                MoneyFineOriginal = loanInfo.MoneyFineOriginal,
                                MoneyInterest = loanInfo.MoneyInterest,
                                OtherMoney = loanInfo.OtherMoney,
                                MoneyCloseLoan = loanInfo.MoneyCloseLoan,
                                TotalPaymentCustomer = item.Data.CustomerMoney > 0 ? (loanInfo.MoneyCloseLoan - item.Data.CustomerMoney) : loanInfo.MoneyCloseLoan,
                            });

                            lstPrematureInterestPaymentSchedules.Add(new PrematureInterestPaymentSchedules()
                            {
                                CodeId = loanInfo.LoanCodeID,
                                LstPaymentSchedules = loanInfo.LstPaymentSchedules,
                            });
                        }
                    }
                }
                ViewBag.ItemPrematureInterestMoneyCustomer = lstItemPrematureInterestMoneyCustomer;
                ViewBag.PrematureInterestMoneyCustomer = new PrematureInterestMoneyCustomer()
                {
                    FullName = infomationCustomer[0].FullName,
                    DateSettlement = Date,
                    CustomerMoney = lstData[0].Data.CustomerMoney.ToString("###,0")
                };
                ViewBag.InfomationCustomer = infomationCustomer;
                return Json(GetBaseObjectResult(true, "Success", null, RenderViewAsString(lstPrematureInterestPaymentSchedules, "_CalculatePrematureInterest")));
            }
            else
                return Json(GetBaseObjectResult(false, "Phải nhập nhập SĐT (CMND) hoặc Mã hợp đồng để tìm kiếm.", null, null));
        }
        #endregion

        #region thay đổi luồng của đơn vay
        [Route("/tool-pipeline/index.html")]
        [PermissionFilter]
        public IActionResult ToolPipeline()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ChangePipeline(int loanbriefId, int actionPipeline)
        {
            var user = GetUserGlobal();
            var access_token = user.Token;
            if (loanbriefId > 0)
            {
                if (Enum.IsDefined(typeof(ActionChangePipeline), actionPipeline))
                {
                    var loanbrief = _loanBriefService.GetLoanBriefById(access_token, loanbriefId);
                    if (loanbrief != null)
                    {
                        var result = _loanBriefService.ToolChangePipeline(access_token, loanbriefId, actionPipeline);
                        if (result.Result)
                            return Json(new { status = 1, message = "Cập nhật thành công" });
                        return Json(new { status = 0, message = "Action đơn vay không đúng" });

                    }

                }
                else
                {
                    return Json(new { status = 0, message = "Action đơn vay không đúng" });
                }
            }
            else
            {
                return Json(new { status = 0, message = "Vui lòng nhập mã đơn vay" });
            }
            return Json(new { status = 0, message = "Vui lòng nhập mã đơn vay" });
        }

        #endregion

        #region Thêm Quận/Huyện và phường xã
        [Route("/tool/add-district-and-ward.html")]
        [PermissionFilter]
        public IActionResult AddDistrictAndWard()
        {
            ViewBag.ListProvince = _dictionaryService.GetProvince(GetToken());
            return View();
        }

        [HttpPost]
        public IActionResult AddDistrict(AddDistrictModel modal)
        {
            var data = new District()
            {
                ProvinceId = modal.ProvinceId,
                Name = modal.DistrictName,
                LatLong = modal.LatLong,
                Type = modal.TypeDistrict
            };
            var result = _dictionaryService.AddDistrict(GetToken(), data);
            if (result > 0)
                return Json(new { status = 1, message = "Thêm mới thành công" });
            else if (result == -1)
                return Json(new { status = 0, message = "Quận/Huyện đã tồn tại. Vui lòng kiểm tra lại" });
            else
                return Json(new { status = 0, message = "Thêm mới thất bại. Vui lòng liên hệ với phòng kĩ thuật" });
        }

        [HttpPost]
        public IActionResult AddWard(AddWardModel modal)
        {
            var data = new Ward()
            {
                DistrictId = modal.DistrictId,
                Name = modal.WardName,
                Type = modal.TypeWard
            };
            var result = _dictionaryService.AddWard(GetToken(), data);
            if (result > 0)
                return Json(new { status = 1, message = "Thêm mới thành công" });
            else if (result == -1)
                return Json(new { status = 0, message = "Phường/Xã đã tồn tại. Vui lòng kiểm tra lại" });
            else
                return Json(new { status = 0, message = "Thêm mới thất bại. Vui lòng liên hệ với phòng kĩ thuật" });
        }
        #endregion

        #region Thay đổi SDT khách hàng
        [Route("/change-phone/index.html")]
        [PermissionFilter]
        public IActionResult ChangePhone()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePhonePost(ChangePhoneReq req)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                if (!req.Phone.ValidPhone())
                    return Json(new { status = 0, message = "Vui lòng nhập đúng định dạng số điện thoại" });
                var loanbrief = await _loanBriefService.GetBasicInfo(access_token, req.LoanBriefId);
                if (loanbrief != null)
                {
                    if (loanbrief.Status == (int)EnumLoanStatus.CANCELED || loanbrief.Status == (int)EnumLoanStatus.FINISH || loanbrief.Status == (int)EnumLoanStatus.DISBURSED)
                        return Json(new { status = 0, message = "Đơn vay ở trạng thái không được phép thay đổi SDT" });
                    var data = await _toolService.ChangePhone(GetToken(), req);
                    if (data)
                    {
                        //Thêm note
                        _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = req.LoanBriefId,
                            Note = string.Format("Hợp đồng HĐ-{0} đã được {1} đổi SDT từ {2} -> {3}", req.LoanBriefId, user.Group.GroupName, loanbrief.Phone, req.Phone),
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = (int)EnumActionComment.ChangePhone,
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        return Json(new { status = 1, message = "Thay đổi SDT thành công!" });
                    }
                    else
                        return Json(new { status = 0, message = "Xảy ra lỗi khi thay đổi SDT. Vui lòng thử lại sau!" });
                }
                else
                {
                    return Json(new { status = 0, message = "Không tin thấy thông tin đơn vay!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }

        }
        #endregion

        #region Tool cho phép ký lại hợp đồng
        [HttpPost]
        public async Task<IActionResult> ReEsignCustomer(ReEsignReq req)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var loanbrief = await _loanBriefService.GetBasicInfo(access_token, req.LoanBriefId);
                if (loanbrief != null)
                {
                    if (loanbrief.Status != (int)EnumLoanStatus.HUB_CHT_APPROVE && loanbrief.Status != (int)EnumLoanStatus.APPRAISER_REVIEW)
                        return Json(new { status = 0, message = "Đơn vay không còn ở hub xử lý" });

                    if (loanbrief.EsignState != (int)EsignState.BORROWER_SIGNED && loanbrief.EsignState != (int)EsignState.LENDER_SIGNED)
                        return Json(new { status = 0, message = "KH chưa ký hợp đồng điện tử" });
                    if (req.EsignNumber.GetValueOrDefault(0) == 0)
                        req.EsignNumber = 2;
                    var data = await _toolService.ReEsignCustomer(GetToken(), req);
                    if (data)
                    {
                        //Thêm note
                        _loanBriefService.AddLoanBriefNote(GetToken(), new LoanBriefNote
                        {
                            LoanBriefId = req.LoanBriefId,
                            Note = "Hợp đồng đã được mở cho Khách hàng ký lại hợp đồng điện tử",
                            FullName = user.FullName,
                            Status = 1,
                            ActionComment = (int)EnumActionComment.CommentLoanBrief,
                            CreatedTime = DateTime.Now,
                            UserId = user.UserId,
                            ShopId = user.ShopGlobal > 0 ? user.ShopGlobal : EnumShop.Tima.GetHashCode(),
                            ShopName = user.ShopGlobal > 0 ? user.ShopGlobalName : ExtensionHelper.GetDescription(EnumShop.Tima)
                        });
                        return Json(new { status = 1, message = "Mở cho KH ký lại thành công" });
                    }
                    else
                        return Json(new { status = 0, message = "Xảy ra lỗi khi mở ký lại cho KH. Vui lòng thử lại sau!" });
                }
                else
                {
                    return Json(new { status = 0, message = "Không tin thấy thông tin đơn vay!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }

        }
        #endregion

        #region Gen new otp
        [Route("/tool/gen_new_otp.html")]
        public IActionResult GenNewOtp()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GenNewOtp(string TextSearch)
        {
            var result = _ssoService.GenNewOtp(GetToken(), TextSearch);
            return Json(new { data = result });
        }
        #endregion

        private string RenderViewAsString(object model, string viewName = null)
        {
            viewName = viewName ?? ControllerContext.ActionDescriptor.ActionName;
            ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                IView view = viewEngine.FindView(ControllerContext, viewName, true).View;
                ViewContext viewContext = new ViewContext(ControllerContext, view, ViewData, TempData, sw, new HtmlHelperOptions());
                view.RenderAsync(viewContext).Wait();
                return sw.GetStringBuilder().ToString();
            }
        }

        #region Get OTP khách hàng
        [Route("/tool/get-log-otp.html")]
        [PermissionFilter]
        public IActionResult GetOTPCustomer()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> GetOTPCustomer(string search, int type)
        {
            try
            {
                var user = GetUserGlobal();
                var access_token = user.Token;
                var loanInfo = new LoanBriefBasicInfo();
                if (!search.ValidPhone())
                {
                    loanInfo = await _loanBriefService.GetBasicInfo(access_token, Convert.ToInt32(search));
                    if (loanInfo == null)
                        return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay" });
                    search = loanInfo.Phone;
                }

                var result = await _toolService.GetLogSendOTP(access_token, search, type);
                if (result != null)
                {
                    if (result.meta.errorCode == 200)
                        return Json(new { status = 1, otp = result.data, message = "Thành công" });
                    else
                        return Json(new { status = 0, message = result.meta.errorMessage });
                }
                else
                {
                    return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng thử lại sau!" });
            }

        }
        #endregion

        #region Hủy check Momo
        [HttpPost]
        public async Task<IActionResult> CancelCheckMomo(int LoanBriefId)
        {
            var loanbrief = await _loanbriefV2Service.GetBasicInfo(LoanBriefId);
            if (loanbrief != null)
            {
                var lstLogLoanInfoAI = new List<LogLoanInfoAi>();
                //inser vào bảng LogLoanInfoAI
                lstLogLoanInfoAI.Add(new LogLoanInfoAi()
                {
                    ServiceType = (int)ServiceTypeAI.CheckLoanMomo,
                    LoanbriefId = loanbrief.LoanBriefId,
                    Request = loanbrief.NationalCard,
                    ResponseRequest = "{\"status\":0,\"data\":{ \"query_data\":\"'" + loanbrief.NationalCard + "'\",\"unchecked\":[],\"bills\":[]},\"message\":\"Thành công\"}",
                    CreatedAt = DateTime.Now,
                    Status = (int)EnumStatusBase.Active,
                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.Excuted,
                    ResultFinal = "PDNL",
                });
                if (!string.IsNullOrEmpty(loanbrief.NationCardPlace))
                {
                    lstLogLoanInfoAI.Add(new LogLoanInfoAi()
                    {
                        ServiceType = (int)ServiceTypeAI.CheckLoanMomo,
                        LoanbriefId = loanbrief.LoanBriefId,
                        Request = loanbrief.NationCardPlace,
                        ResponseRequest = "{\"status\":0,\"data\":{ \"query_data\":\"'" + loanbrief.NationCardPlace + "'\",\"unchecked\":[],\"bills\":[]},\"message\":\"Thành công\"}",
                        CreatedAt = DateTime.Now,
                        Status = (int)EnumStatusBase.Active,
                        IsExcuted = (int)EnumLogLoanInfoAiExcuted.Excuted,
                        ResultFinal = "PDNL",
                    });
                }
                if (await _logLoanInfoAIV2Service.AddRange(lstLogLoanInfoAI))
                {
                    var taskRun = new List<Task>();

                    //insert vào bảng LoanBriefNote
                    taskRun.Add(_loanBriefNoteV2Service.Add(new LoanBriefNote
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        UserId = 1,
                        FullName = "System",
                        ActionComment = (int)EnumActionComment.CommentLoanBrief,
                        Note = "Bỏ check khoản vay bên MOMO theo phê duyệt ngoại lệ",
                        Status = (int)EnumStatusBase.Active,
                        CreatedTime = DateTime.Now,
                        ShopId = 3703,
                        ShopName = "Tima",
                        IsDisplay = true
                    }));

                    //bảng RuleCheckLoan
                    taskRun.Add(_ruleCheckLoanService.AddOrUpdate(new RuleCheckLoan
                    {
                        LoanbriefId = loanbrief.LoanBriefId,
                        CreatedAt = DateTime.Now,
                        IsCheckMomo = false,
                        UserId = 1,
                    }));

                    if (taskRun.Count > 0)
                        Task.WaitAll(taskRun.ToArray());

                    return Json(new { status = 1, message = "Hủy check momo thành công" });
                }
                else
                    return Json(new { status = 0, message = "Không insert được LogLoanInfoAI" });
            }
            else
                return Json(new { status = 0, message = "Không tin thấy thông tin đơn vay" });
        }
        #endregion

        #region Tool check phone
        [Route("/tool/check-phone.html")]
        //[PermissionFilter]
        public IActionResult CheckPhone()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CheckPhone(string phone)
        {
            try
            {
                var data = new List<LoanAndRelationNotCancelView>();
                if (!string.IsNullOrEmpty(phone) && phone.ValidPhone())
                    data = await _loanbriefV2Service.ListLoanAndRelationNotCancel(phone);
                return PartialView("_CheckPhone", data);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Hủy ký HĐ điện tử
        [HttpPost]
        public async Task<IActionResult> CancelEsign(int LoanBriefId)
        {
            var loanbrief = await _loanbriefV2Service.GetBasicInfo(LoanBriefId);
            if (loanbrief != null)
            {
                if (await _loanbriefV2Service.CancelEsign(LoanBriefId))
                {
                    //insert vào bảng LoanBriefNote
                    var objLoanBriefNote = new LoanBriefNote()
                    {
                        LoanBriefId = loanbrief.LoanBriefId,
                        UserId = 1,
                        FullName = "System",
                        ActionComment = (int)EnumActionComment.CommentLoanBrief,
                        Note = "Hủy ký Esign chuyển sang ký hợp đồng giấy do KH đã ký 2 lần Esign",
                        Status = (int)EnumStatusBase.Active,
                        CreatedTime = DateTime.Now,
                        ShopId = 3703,
                        ShopName = "Tima",
                        IsDisplay = true
                    };
                    await _loanBriefNoteV2Service.Add(objLoanBriefNote);
                    return Json(new { status = 1, message = "Hủy ký hợp đồng điện tử thành công" });
                }
                return Json(new { status = 1, message = "Hủy ký hợp đồng điện tử không thành công" });
            }
            else
                return Json(new { status = 0, message = "Không tin thấy thông tin đơn vay" });
        }
        #endregion

        #region Hủy check kỳ thanh toán
        [HttpPost]
        public async Task<IActionResult> CancelCheckPayment(int LoanBriefId)
        {
            var loanbrief = await _loanbriefV2Service.GetBasicInfo(LoanBriefId);
            if (loanbrief != null)
            {
                var taskRun = new List<Task>();

                //insert vào bảng LoanBriefNote
                taskRun.Add(_loanBriefNoteV2Service.Add(new LoanBriefNote
                {
                    LoanBriefId = loanbrief.LoanBriefId,
                    UserId = 1,
                    FullName = "System",
                    ActionComment = (int)EnumActionComment.CommentLoanBrief,
                    Note = "Đơn được phê duyệt ngoại lệ bỏ kiểm tra kỳ thanh toán(có kỳ trả chậm quá 30 ngày)",
                    Status = (int)EnumStatusBase.Active,
                    CreatedTime = DateTime.Now,
                    ShopId = 3703,
                    ShopName = "Tima",
                    IsDisplay = true
                }));

                //bảng RuleCheckLoan
                taskRun.Add(_ruleCheckLoanService.AddOrUpdate(new RuleCheckLoan
                {
                    LoanbriefId = loanbrief.LoanBriefId,
                    CreatedAt = DateTime.Now,
                    IsCheckPayment = false,
                    UserId = 1,
                }));

                if (taskRun.Count > 0)
                    Task.WaitAll(taskRun.ToArray());

                return Json(new { status = 1, message = "Hủy check kỳ thanh toán thành công" });
            }
            else
                return Json(new { status = 0, message = "Không tin thấy thông tin đơn vay" });
        }
        #endregion

        #region Tool hỗ trợ cho các phòng ban
        [PermissionFilter]
        [Route("/tool/support.html")]
        public IActionResult ToolSupport()
        {
            return View();
        }
        #endregion

        #region Đóng HĐ
        [HttpPost]
        public async Task<IActionResult> CloseLoan(int LoanBriefId)
        {
            //kiểm tra xem có phải là HĐ đang vay không
            var loanbrief = await _loanbriefV2Service.GetBasicInfo(LoanBriefId);
            if (loanbrief != null && loanbrief.Status == (int)EnumLoanStatus.DISBURSED)
            {
                var param = new DeferredPayment.Input
                {
                    CustomerName = loanbrief.FullName,
                    NumberCard = loanbrief.NationalCard
                };
                //gọi api sang ag kiểm tra xem đơn đã đóng chưa
                var checkAg = _lmsService.CheckCustomerDeferredPayment(GetToken(), loanbrief.LoanBriefId, param);
                if (checkAg != null && checkAg.Data != null && checkAg.Data.LstLoanCustomer != null && checkAg.Data.LstLoanCustomer.Count() > 0)
                {

                    foreach (var item in checkAg.Data.LstLoanCustomer)
                    {
                        //kiểm tra xem có mã LoanBriefId trùng với bên LMS hay không và HĐ đấy đã GN hay chưa
                        if (item.LoanBriefID == loanbrief.LoanBriefId && item.LoanId == loanbrief.LmsLoanId)
                        {
                            if (item.Status == (int)EnumLoanStatus.FINISH)
                            {
                                //kiểm tra xem có phải đơn định vị không
                                //nếu phải thì gọi api đóng HĐ định vị
                                if (loanbrief.IsLocate == true && !string.IsNullOrEmpty(loanbrief.DeviceId) && loanbrief.DeviceStatus == (int)StatusOfDeviceID.Actived)
                                {
                                    if (_trackDeviceService.CloseContract(GetToken(), string.Format("HD-{0}", loanbrief.LoanBriefId), loanbrief.DeviceId, loanbrief.LoanBriefId))
                                    {
                                        await _loanBriefNoteV2Service.Add(new LoanBriefNote()
                                        {
                                            FullName = "Auto System",
                                            ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                            Note = string.Format("Đóng hợp đồng định vị bên AI"),
                                            Status = 1,
                                            CreatedTime = DateTime.Now,
                                            ShopId = 3703,
                                            ShopName = "Tima",
                                            LoanBriefId = loanbrief.LoanBriefId
                                        });
                                    }
                                    //else
                                    //    return Json(new { status = 1, message = "Lỗi đóng HĐ định vị" });
                                }
                                //cập nhập lại Status, PipelineState, FinishAt
                                var finishAt = DateTime.Now;
                                if (!string.IsNullOrEmpty(item.FinishDate))
                                {
                                    var finishDate = DateTimeUtility.ToDateTime(item.FinishDate, "yyyy-MM-dd'T'HH:mm:ss");
                                    if (finishDate != null)
                                        finishAt = finishDate.Value;
                                }

                                var result = await _loanbriefV2Service.CloseLoan(loanbrief.LoanBriefId, finishAt);
                                if (result)
                                {
                                    //lưu lại comment
                                    await _loanBriefNoteV2Service.Add(new LoanBriefNote()
                                    {
                                        FullName = "Auto System",
                                        ActionComment = EnumActionComment.CommentLoanBrief.GetHashCode(),
                                        Note = string.Format("Đóng hợp đồng"),
                                        Status = 1,
                                        CreatedTime = DateTime.Now,
                                        ShopId = 3703,
                                        ShopName = "Tima",
                                        LoanBriefId = loanbrief.LoanBriefId
                                    });

                                    return Json(new { status = 1, message = "Đóng hợp đồng thành công" });
                                }
                                else
                                    return Json(new { status = 0, message = "Lỗi đóng HĐ vui lòng thử lại sau" });
                            }
                            else
                                return Json(new { status = 0, message = "HĐ bên LMS chưa đóng" });
                        }
                        else continue;
                    }
                }
                else
                    return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay bên LMS." });
            }

            return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay hoặc đơn vay đang không ở trạng thái đã giải ngân." });
        }
        #endregion

        #region Kiểm tra và cập nhập trạng thái có thể Topup 
        [HttpPost]
        public async Task<IActionResult> CheckAndUpdateLoanTopup(int LoanBriefId)
        {
            //kiểm tra xem có phải là HĐ đang vay không
            var loanbrief = await _loanbriefV2Service.GetBasicInfo(LoanBriefId);
            if (loanbrief != null && loanbrief.Status == (int)EnumLoanStatus.DISBURSED && loanbrief.LmsLoanId.GetValueOrDefault(0) != 0)
            {
                if (loanbrief.TypeLoanSupport.GetValueOrDefault(0) == 0)
                {
                    //kiểm tra xem là gói vay oto hay là xe máy
                    if (loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_CC || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KCC
                        || loanbrief.ProductId == (int)EnumProductCredit.MotorCreditType_KGT)
                    {
                        //gọi api sang lms kiểm tra có phải đơn được Topup không
                        var checkTopup = await _loanbriefV2Service.CheckCanTopup(LoanBriefId);
                        if (checkTopup != null)
                        {
                            if (checkTopup.IsCanTopup == true)
                            {
                                //cập nhập lại trường TypeLoanSupport
                                var result = await _loanbriefV2Service.UpdateTypeLoanSupport(loanbrief.LoanBriefId, (int)EnumTypeLoanSupport.IsCanTopup);
                                if (result)
                                    return Json(new { status = 1, message = "Cập nhập trạng thái đơn có thể Topup thành công." });
                                else
                                    return Json(new { status = 0, message = "Cập nhập trạng thái đơn có thể Topup bị lỗi. Vui lòng thử lại sau." });
                            }
                            else
                                return Json(new { status = 0, message = "Không đủ điều kiện để Topup" });
                        }
                        else
                            return Json(new { status = 0, message = "Có lỗi sảy ra. Vui lòng thử lại sau" });
                    }

                    else if (loanbrief.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                    {
                        var req = new CheckReLoan.Input()
                        {
                            CustomerName = loanbrief.FullName,
                            NumberCard = loanbrief.NationalCard,
                        };
                        var res = _lmsService.CheckTopupOto(GetToken(), loanbrief.LoanBriefId, req);
                        if (res != null && res.IsAccept == 1)
                        {
                            //cập nhập lại trường TypeLoanSupport
                            var result = await _loanbriefV2Service.UpdateTypeLoanSupport(loanbrief.LoanBriefId, (int)EnumTypeLoanSupport.IsCanTopup);
                            if (result)
                                return Json(new { status = 1, message = "Cập nhập trạng thái đơn có thể Topup thành công." });
                            else
                                return Json(new { status = 0, message = "Cập nhập trạng thái đơn có thể Topup bị lỗi. Vui lòng thử lại sau." });
                        }
                        else
                            return Json(new { status = 0, message = "Đơn vay không đủ điều kiện Topup bên LMS." });
                    }
                }
                else
                    return Json(new { status = 0, message = "Đã là HĐ có thể Topup." });
            }
            return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay hoặc đơn vay đang không ở trạng thái đã giải ngân." });
        }
        #endregion

        #region Tool hỗ trợ cho Team lead Telesale
        [PermissionFilter]
        [Route("/tool/manage-telesale-support.html")]
        public IActionResult ToolManageTelesaleSupport()
        {
            return View();
        }
        #endregion

        #region Khôi phục lại đơn Topup
        [HttpPost]
        public async Task<IActionResult> RestoreTopup(int loanBriefId)
        {
            var user = GetUserGlobal();
            var loanbrief = await _loanbriefV2Service.GetBasicInfo(loanBriefId);
            if (loanbrief != null && loanbrief.LoanBriefId > 0 && loanbrief.ReMarketingLoanBriefId > 0)
            {
                //kiểm tra xem có đơn topup đang xử lý hay không
                if (await _loanbriefV2Service.CheckLoanBriefTopupProcessing(loanBriefId))
                    return Json(new { status = 0, message = "Có đơn Topup đang xử lý." });
                //kiểm tra xem đơn gốc có đang là cho vay không
                var loanbriefOld = await _loanbriefV2Service.GetBasicInfo(loanbrief.ReMarketingLoanBriefId.Value);
                if (loanbriefOld != null && loanbriefOld.Status == (int)EnumLoanStatus.DISBURSED)
                {
                    if (await _loanbriefV2Service.RestoreTopup(loanBriefId))
                    {
                        //lưu lại log khi khôi phục lại đơn
                        await _loanBriefNoteV2Service.Add(new LoanBriefNote
                        {
                            LoanBriefId = loanbrief.LoanBriefId,
                            UserId = user.UserId,
                            FullName = user.FullName,
                            ActionComment = (int)EnumActionComment.CommentLoanBrief,
                            Note = "Khôi phục lại đơn Topup bị hủy",
                            Status = (int)EnumStatusBase.Active,
                            CreatedTime = DateTime.Now,
                            ShopId = 3703,
                            ShopName = "Tima",
                            IsDisplay = true
                        });

                        return Json(new { status = 1, message = "Khôi phục đơn vay Topup thành công." });
                    }
                    else
                        return Json(new { status = 1, message = "Khôi phục đơn vay Topup thất bại. Vui lòng thử lại sau" });
                }
                else
                    return Json(new { status = 0, message = "Đơn gốc không phải trạng thái đang vay." });
            }
            else
                return Json(new { status = 0, message = "Không tìm thấy thông tin đơn vay." });
        }
        #endregion

        #region Tool kiểm tra thông tin cavet
        [Route("/tool/check-cavet.html")]
        //[PermissionFilter]
        public IActionResult CheckCavet()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CheckCavet(string search)
        {
            try
            {
                var user = GetUserGlobal();

                var data = await _loanBriefService.GetLoanToolCheckCavet(user.Token, search);
                if (data != null && data.Count > 0)
                {
                    //Call api ERP
                    foreach (var item in data)
                    {
                        if (item.CodeId.HasValue)
                        {
                            var erp = _erpService.CheckCavet(user.Token, item.CodeId.Value);
                            if (erp != null && erp.Count > 0)
                            {
                                //Tình trạng định vị
                                if (erp[0].Tracking != null && !string.IsNullOrEmpty(erp[0].Tracking.Status))
                                    item.StatusDevices = GetStatusDeviceErp(erp[0].Tracking.Status);
                                if (!string.IsNullOrEmpty(item.StatusDevices) && erp[0].HubTracking != null && !string.IsNullOrEmpty(erp[0].HubTracking.Name))
                                    item.StatusDevices = item.StatusDevices + " - kho: " + erp[0].HubTracking.Name;


                                //Tình trạng cavet
                                item.StatusVehicleRegistration = GetStatusCavet(erp[0].Status);
                                //Đề xuất mượn cavet
                                if (erp[0].HubRequest != null && erp[0].HubRequest.Name != null)
                                    item.ProposeBorrow = erp[0].HubRequest.Name;

                                //Mô tả
                                if (erp[0].Request != null && erp[0].Request.Description != null)
                                {
                                    item.StatusPropose = GetStatusRequest(erp[0].Request.Status);
                                    item.Description = erp[0].Request.Description;
                                    //Thời gian đề xuất
                                    item.CreatedAt = ConvertExtensions.UnixTimeStampToDateTime(erp[0].Request.CreatedAt);
                                }

                            }
                        }
                    }
                }
                return View("_CheckCavet", data);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GetStatusCavet(string status)
        {
            var sta = "";
            if (status == "Import" || status == "ImportError" || status == "ImportRecovery" || status == "Export" || status == "OpenContract") sta = "Tồn kho";
            else if (status == "ExportError") sta = "Xuất lỗi trả GINNO";
            else if (status == "ExportRecovery") sta = "Xuất lỗi thu hồi từ HUB";
            else if (status == "CloseContract") sta = "Đóng hợp đồng, gỡ khỏi phương tiện";
            else if (status == "Active") sta = "Kích hoạt theo đơn";
            else if (status == "Active" || status == "InActive") sta = "Kích hoạt theo đơn";
            else if (status == "Lost") sta = "Bị mất";
            else if (status == "Error") sta = "Bị lỗi";
            else if (status == "Broken") sta = "Bị hỏng";
            else if (status == "Compensated") sta = "Đã đền bù";
            else if (status == "Returned") sta = "Đã trả khách";
            else if (status == "ReLending") sta = "Tái vay";
            else if (status == "Unknown") sta = "Không xác định";
            else if (status == "UnReclaim") sta = "Chưa thu hồi";
            else if (status == "WaitingImport") sta = "Chờ nhập kho";
            else if (status == "NoRenewal") sta = "Không gia hạn";
            else if (status == "UnderWarranty") sta = "Đang bảo hành";
            else if (status == "Liquidated") sta = "Đã thanh lý";
            else if (status == "Canceled") sta = "Đã huỷ";
            return sta;
        }

        public string GetStatusRequest(string status)
        {
            var sta = "";
            if (status == "Pending") sta = "Pending";
            else if (status == "Approved") sta = "Chấp thuận";
            else if (status == "Exported") sta = "Đã xuất";
            else if (status == "Rejected") sta = "Đã Từ chối";
            return sta;
        }
        public string GetStatusDeviceErp(string status)
        {
            var sta = "";
            if (status == "Import") sta = "Nhập kho";
            else if (status == "ImportError") sta = "Nhập lỗi từ HUB";
            else if (status == "ImportRecovery") sta = "Nhập lỗi thu hồi từ HUB";
            else if (status == "Export") sta = "Xuất kho";
            else if (status == "OpenContract") sta = "Mở Hợp đồng, gắn vào phương tiện";
            else if (status == "ExportError") sta = "Xuất lỗi trả GINNO";
            else if (status == "ExportRecovery") sta = "Xuất lỗi thu hồi từ HUB";
            else if (status == "CloseContract") sta = "Đóng hợp đồng, gỡ khỏi phương tiện";
            else if (status == "Active") sta = "Kích hoạt theo đơn";
            else if (status == "Active" || status == "InActive") sta = "Kích hoạt theo đơn";
            else if (status == "Lost") sta = "Bị mất";
            else if (status == "Error") sta = "Bị lỗi";
            else if (status == "Broken") sta = "Bị hỏng";
            else if (status == "Compensated") sta = "Đã đền bù";
            else if (status == "Returned") sta = "Đã trả khách";
            else if (status == "ReLending") sta = "Tái vay";
            else if (status == "Unknown") sta = "Không xác định";
            else if (status == "UnReclaim") sta = "Chưa thu hồi";
            else if (status == "WaitingImport") sta = "Chờ nhập kho";
            else if (status == "NoRenewal") sta = "Không gia hạn";
            else if (status == "UnderWarranty") sta = "Đang bảo hành";
            else if (status == "Liquidated") sta = "Đã thanh lý";
            else if (status == "Canceled") sta = "Đã huỷ";

            return sta;
        }
        #endregion

        #region Tool cập nhập lại trạng thái cho bảng PushLoanToLender
        [Route("/tool/update_status_push_loan_to_lender.html")]
        public IActionResult UpdateStatusPushLoanToLender()
        {
            return View();
        }
        [HttpPost]
        public IActionResult UpdateStatusPushLoanToLender(string lstId)
        {
            try
            {
                if (!string.IsNullOrEmpty(lstId))
                {
                    List<string> lstData = lstId.Split(',').ToList();
                    if (lstData != null && lstData.Count > 0)
                    {
                        foreach (var item in lstData)
                        {
                            _pushLoanToPartnerService.UpdateStatusDisbursement(Convert.ToInt32(item));
                        }
                    }
                }
                return Json(new { status = 1 });
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = ex });
            }
        }
        #endregion
    }
}
