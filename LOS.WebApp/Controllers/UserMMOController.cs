﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Services.UserMMO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.FileProviders;
using LOS.Common.Utils;
using System.Net.Http;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class UserMmoController : BaseController
    {
        private IConfiguration _baseConfig;
        private IUserMMOService _userMmoServive;
        private readonly IMapper _mapper;
        private IHttpClientFactory clientFactory;

        public UserMmoController(IUserMMOService userMmoServive, IConfiguration configuration, IMapper mapper, IHttpClientFactory clientFactory)
            : base(configuration, clientFactory)
        {
            _userMmoServive = userMmoServive;
            _baseConfig = configuration;
            _mapper = mapper;
            this.clientFactory = clientFactory;
        }

        [Route("usermmo-manager.html")]
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> LoadData()
        {
            try
            {
                var modal = new UserMmoDatatable(HttpContext.Request.Form);
                int recordsTotal = 0;
                // Query api   	
                List<UserMMODetail> data = new List<UserMMODetail>();
                data = _userMmoServive.Search(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult CreateUserMmoModal(int Id)
        {
            var entity = new UserMmoItem();
            if (Id == 0)
            {
                return View(entity);
            }
            else
            {
                // Query api   
                var user = _userMmoServive.GetById(GetToken(), Id);
                entity = _mapper.Map<UserMmoItem>(user);
                //return View("/Views/Users/Partial/_CreateUserModalPartial.cshtml", entity);
                return View(entity);
            }
        }
    
        public async Task<IActionResult> CreateUser(UserMmoItem entity)
        {
            try
            {
                entity.IsActive = true;
                if (entity.CheckIsActiveValue == 0)
                {
                    entity.IsActive = false;
                }
                var ImageFacedeCmt = UploadFile(entity.ImageFacedeCmt);
                if (ImageFacedeCmt == "1")
                {
                    return Json(new { status = 0, message = "Định dạng file tải lên không đúng" });
                }
                var ImageBacksideCmt = UploadFile(entity.ImageBacksideCmt);
                if (ImageBacksideCmt == "1")
                {
                    return Json(new { status = 0, message = "Định dạng file tải lên không đúng" });
                }
                entity.FacedeCmt = ImageFacedeCmt;
                entity.BacksideCmt = ImageBacksideCmt;

                if (entity.UserId > 0)
                {
                    var obj = _mapper.Map<UserMMODTO>(entity);
                    obj.UpdateDate = DateTime.Now;
                    var result = _userMmoServive.Update(GetToken(), entity.UserId, obj);
                    if (result)
                        return Json(new { status = 1, message = "Cập nhật tài khoản thành công" });
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật thông tin tài khoản. Vui lòng liên hệ quản trị viên!" });
                }
                else
                {
                    var objuser = _userMmoServive.GetByUser(GetToken(), entity.Email.Trim());
                    if (objuser.UserId > 0)
                    {
                        return Json(new { status = 0, message = "Email đã tồn tại." });
                    }
                    var CheckObjPhone = _userMmoServive.GetByPhone(GetToken(), entity.Phone.Trim());
                    if (CheckObjPhone.UserId > 0)
                    {
                        return Json(new { status = 0, message = "Số điện thoại đã tồn tại." });
                    }
                    var obj = _mapper.Map<UserMMODTO>(entity);
                    obj.CreateDate = DateTime.Now;
                    obj.Password = Security.GetMD5Hash(entity.Password);
                    var result = _userMmoServive.Create(GetToken(), obj);
                    if (result > 0)
                    {
                        obj.AffCode = "tima" + result;
                        obj.UserId = result;
                        _userMmoServive.Update(GetToken(), result, obj);
                        return Json(new { status = 1, message = "Thêm mới tài khoản thành công" });
                    }
                    return Json(new { status = 0, message = "Xảy ra lỗi khi thêm mới tài khoản. Vui lòng liên hệ quản trị viên!" });
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public string UploadFile(IFormFile file)
        {
            string str = null;
            if (file != null)
            {
                string path = _baseConfig["Configuration:UrlUserMMo"];

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                var extension = Path.GetExtension(file.FileName);
                var allowedExtensions = new[] { ".jpeg", ".png", ".jpg", ".gif" };
                if (allowedExtensions.Contains(extension.ToLower()))
                {
                    //do some logic here because the data is smaller than 5mb   
                    string ImageName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                    //Get url To Save

                    string SavePath = Path.Combine(Directory.GetCurrentDirectory(), path, ImageName);
                    using (var stream = new FileStream(SavePath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    SavePath = SavePath.Replace(Directory.GetCurrentDirectory(), "");
                    str = SavePath;
                }
                else
                {
                    str = "1";
                }
                
            }
            return str;
        }
        public IActionResult DeleteUser(int Id)
        {
            try
            {
                var result = _userMmoServive.Delete(GetToken(), Id);
                if (result)
                    return Json(new { status = 1, message = "Xóa tài khoản thành công" });
                return Json(new { status = 0, message = "Xảy ra lỗi khi xóa tài khoản. Vui lòng liên hệ quản trị viên!" });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}