﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using LOS.WebApp.Services;
using AutoMapper;
using LOS.WebApp.Models.Users;
using LOS.WebApp.Models;
using System.Reflection;
using LOS.DAL.EntityFramework;
using LOS.Common.Utils;
using LOS.WebApp.Models.Request;
using System.Net.Http;
using LOS.WebApp.Models.ReqSSO;
using LOS.Services.Services;

namespace LOS.WebApp.Controllers
{
    [AuthorizeFilter]
    public class UsersController : BaseController
    {
        private IConfiguration _baseConfig;
        private IUserServices _userServices;
        private IGroupServices _groupServices;
        private readonly IMapper _mapper;
        private IPipelineService _pipelineService;
        private IDictionaryService _dictionaryService;
        private IShopServices _shopService;
        private IModuleService _moduleService;
        private IHttpClientFactory clientFactory;
        private ITelesalesServices _telesalesServices;
        private ISSOService _ssoService;

        private IUserV2Service _userV2Service;
        public UsersController(IUserServices userServices, IGroupServices groupServices, IConfiguration configuration, IMapper mapper,
            IPipelineService pipelineService, IDictionaryService dictionaryService, ITelesalesServices telesalesServices, ISSOService ssoService,
        IShopServices shopServices, IModuleService moduleService, IUserV2Service userV2Service, IHttpClientFactory clientFactory)
            : base(configuration, clientFactory)
        {
            _userServices = userServices;
            _groupServices = groupServices;
            _baseConfig = configuration;
            _mapper = mapper;
            this._pipelineService = pipelineService;
            this._dictionaryService = dictionaryService;
            this._shopService = shopServices;
            this._moduleService = moduleService;
            this.clientFactory = clientFactory;
            _telesalesServices = telesalesServices;
            _ssoService = ssoService;
            _userV2Service = userV2Service;
        }

        [Route("user-manager.html")]
        public async Task<IActionResult> Index()
        {
            var model = new UserViewModel();
            var listGroup = _groupServices.GetAll(GetToken());
            model.ListGroup = listGroup;
            return View(model);
        }

        public async Task<IActionResult> LoadData()
        {
            try
            {
                var modal = new UserDatatable(HttpContext.Request.Form);
                int recordsTotal = 0;
                // Query api   	
                List<UserDetail> data = new List<UserDetail>();
                data = _userServices.Search(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        item.UrlEdit = EncryptUtils.Encrypt(string.Format("?Id={0}", item.UserId), true);
                    }
                }
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [EncryptedActionParameter]
        [Route("user")]
        public async Task<IActionResult> CreateUserModalPartialAsync(int Id = 0)
        {
            var entity = new UserItem();
            var listGroup = _groupServices.GetAll(GetToken());
            entity.ListGroup = listGroup;
            entity.ListAllHub = _shopService.GetAllHub(GetToken());
            if (Id == 0)
            {
                return View("~/Views/Users/CreateUserModalPartialAsync.cshtml", entity);
                //return View(entity);
            }
            else
            {
                // Query api   
                var user = _userServices.GetById(GetToken(), Id);
                entity = _mapper.Map<UserItem>(user);
                entity.ListGroup = listGroup;
                entity.ListAllHub = _shopService.GetAllHub(GetToken());
                if (user.ListShop != null && user.ListShop.Count > 0)
                    entity.ListHub = user.ListShop;
                //return View("~/Views/Users/Partial/_CreateUserModalPartial.cshtml", entity);
                return View("~/Views/Users/CreateUserModalPartialAsync.cshtml", entity);
            }
        }

        public async Task<IActionResult> CreateUser(UserItem entity)
        {
            try
            {
                //gọi api tạo user bên sso
                var reqCreateSSO = new ResquestUserSSO()
                {
                    Username = entity.Username,
                    Password = entity.Password ?? "Tima@2021",
                    Email = entity.Email,
                    IsValidOtp = true

                };
                var createSSO = _ssoService.CreateUserSSO(GetToken(), reqCreateSSO);
                if (createSSO != null && createSSO.data != null && createSSO.data.id > 0)
                {
                    List<string> stringList = new List<string>();
                    if (entity.lstMenu != null && entity.lstMenu[0] != null)
                    {
                        stringList = entity.lstMenu[0].Split(',').ToList();
                        stringList.Select(int.Parse).ToList();
                    }

                    if (entity.UserId > 0)
                    {
                        List<UserModule> lstmodule = new List<UserModule>();
                        //xử lý menu
                        foreach (var item in stringList)
                        {
                            UserModule objmodule = new UserModule();
                            objmodule.ModuleId = Convert.ToInt32(item);
                            objmodule.UserId = entity.UserId;
                            lstmodule.Add(objmodule);
                        }
                        //xử lý action phân quyền
                        if (entity.lstAction != null && entity.lstAction.Count > 0)
                        {
                            foreach (var item in entity.lstAction)
                            {
                                UserModule objmodule = new UserModule();
                                objmodule.ModuleId = Convert.ToInt32(item);
                                objmodule.UserId = entity.UserId;
                                lstmodule.Add(objmodule);
                            }
                        }
                        entity.ListUserModule = lstmodule;
                        entity.lstMenu = null;
                        var obj = new UserDTO();
                        Common.Utils.Ultility.CopyObject(entity, ref obj, true);
                        //var obj = _mapper.Map<UserDTO>(entity);
                        obj.UpdatedTime = DateTime.Now;
                        obj.ListUserModule = entity.ListUserModule;
                        obj.HubIds = entity.HubIds;
                        obj.UserIdSSO = createSSO.data.id;
                        var result = _userServices.Update(GetToken(), entity.UserId, obj);
                        if (result)
                        {
                            return Json(new { status = 1, message = "Cập nhật tài khoản thành công" });
                        }
                        return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật thông tin tài khoản. Vui lòng liên hệ quản trị viên!" });
                    }
                    else
                    {
                        var objuser = _userServices.GetByUser(GetToken(), entity.Username.Trim());
                        if (objuser.UserId > 0)
                        {
                            return Json(new { status = 0, message = "Tên tài khoản đã tồn tại." });
                        }
                        List<UserModule> lstmodule = new List<UserModule>();
                        //xử lý menu
                        foreach (var item in stringList)
                        {
                            UserModule objmodule = new UserModule();
                            objmodule.ModuleId = Convert.ToInt32(item);
                            //objmodule.GroupId = (int)entity.GroupId;
                            lstmodule.Add(objmodule);
                        }
                        //xử lý action phân quyền
                        if (entity.lstAction != null && entity.lstAction.Count > 0)
                        {
                            foreach (var item in entity.lstAction)
                            {
                                UserModule objmodule = new UserModule();
                                objmodule.ModuleId = Convert.ToInt32(item);
                                objmodule.UserId = entity.UserId;
                                lstmodule.Add(objmodule);
                            }
                        }
                        entity.ListUserModule = lstmodule;

                        //var obj = _mapper.Map<UserDTO>(entity);
                        var obj = new UserDTO();
                        Common.Utils.Ultility.CopyObject(entity, ref obj, true);
                        obj.HubIds = entity.HubIds;
                        obj.CreatedTime = DateTime.Now;
                        obj.Password = Security.GetMD5Hash(entity.Password);
                        obj.CreatedBy = GetUserGlobal().UserId;
                        obj.UserIdSSO = createSSO.data.id;
                        var result = _userServices.Create(GetToken(), obj);
                        if (result)
                        {
                            return Json(new { status = 1, message = "Thêm mới tài khoản thành công" });
                        }
                        return Json(new { status = 0, message = "Xảy ra lỗi khi thêm mới tài khoản. Vui lòng liên hệ quản trị viên!" });
                    }
                }
                else
                    return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng liên hệ quản trị viên!" });
            }

            catch (Exception ex)
            {
                throw;
            }
        }
        [EncryptedActionParameter]
        public IActionResult DeleteUser(int Id)
        {
            try
            {
                var result = _userServices.Delete(GetToken(), Id);
                if (result)
                    return Json(new { status = 1, message = "Xóa tài khoản thành công" });
                return Json(new { status = 0, message = "Xảy ra lỗi khi xóa tài khoản. Vui lòng liên hệ quản trị viên!" });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [EncryptedActionParameter]
        public async Task<IActionResult> GetPermissionData(int Id)
        {
            var userModel = new UserDetail();
            ViewBag.Properties = _pipelineService.GetAllProperty(GetToken());
            ViewBag.LoanStatus = _dictionaryService.GetLoanStatus(GetToken());
            if (Id > 0)
            {

                userModel = _userServices.GetPermission(GetToken(), Id);
                if (userModel.GroupId > 0 && (userModel.ListCondition == null || userModel.ListCondition.Count == 0)
                    && (userModel.ListPermissionAction == null || userModel.ListPermissionAction.Count == 0))
                {
                    var permissionGroup = _groupServices.GetPermission(GetToken(), userModel.GroupId.Value);
                    if (permissionGroup != null && permissionGroup.ListCondition != null && permissionGroup.ListCondition.Count > 0)
                    {
                        userModel.ListCondition = permissionGroup.ListCondition.Select(x => new UserCondition()
                        {
                            PropertyId = x.PropertyId,
                            Operator = x.Operator,
                            Value = x.Value
                        }).ToList();
                    }
                    if (permissionGroup != null && permissionGroup.ListPermissionAction != null && permissionGroup.ListPermissionAction.Count > 0)
                    {
                        userModel.ListPermissionAction = permissionGroup.ListPermissionAction.Select(x => new UserActionPermission()
                        {
                            LoanStatus = x.LoanStatus,
                            Value = x.Value
                        }).ToList();
                    }
                }
            }
            return View("~/Views/Users/Partial/UserPermissionPartial.cshtml", userModel);
        }

        public IActionResult ActionPropertyPartial()
        {
            ViewBag.LoanStatus = _dictionaryService.GetLoanStatus(GetToken());
            return View("~/Views/Users/Partial/ActionPropertyPartial.cshtml", new UserDetail());
        }
        public IActionResult ConditionValuePropertyPartial([FromBody] IdOnlyReq req)
        {
            var data = _pipelineService.GetAllProperty(GetToken());
            ViewBag.Properties = data;
            return View("~/Views/Users/Partial/ConditionValuePropertyPartial.cshtml", new UserDetail());
        }

        public IActionResult GetPropertyPartial([FromBody] IdOnlyReq req)
        {
            try
            {
                var data = _pipelineService.GetPropertyById(GetToken(), req.id);

                if (data.LinkObject != null)
                {
                    // Get Link Object
                    Helpers.Ultility util = new Helpers.Ultility(clientFactory);
                    var objects = util.GetLinkObject(data.LinkObject, GetToken());
                    ViewBag.LinkObjects = objects;
                }
                return View("~/Views/Users/Partial/GetPropertyPartial.cshtml", data);
            }
            catch (Exception)
            {
                return View("~/Views/Users/Partial/GetPropertyPartial.cshtml");
            }
        }
        [HttpPost]
        public IActionResult UpdatePermissionUser([FromBody] PermissionUserItem data)
        {
            if (data.UserId > 0)
            {
                var user = GetUserGlobal();
                if (data.ConditionUsers != null && data.ConditionUsers.Count > 0)
                {
                    var model = new PermissionUserDTO()
                    {
                        UserId = data.UserId,
                        UserConditions = (data.ConditionUsers != null && data.ConditionUsers.Count > 0) ?
                                data.ConditionUsers.Select(x => new UserCondition
                                {
                                    UserId = data.UserId,
                                    PropertyId = x.PropertyId,
                                    Operator = x.Operator.Trim().ToUpper(),
                                    Value = x.Value,
                                    CreatedBy = user.UserId
                                }).ToList() : new List<UserCondition>(),
                        UserActionPermissions = data.ActionUsers != null && data.ActionUsers.Count > 0 ?
                            data.ActionUsers.Select(x => new UserActionPermission()
                            {
                                UserId = data.UserId,
                                LoanStatus = x.PropertyId,
                                Value = x.Value,
                                CreatedBy = user.UserId
                            }).ToList() : new List<UserActionPermission>()
                    };

                    if (_userServices.UpdatePermission(GetToken(), model))
                        return Json(new { isSuccess = 1, message = "Phân quyền thành công" });
                    return Json(new { isSuccess = 0, message = "Xảy ra lỗi khi phân quyền dữ liệu user. Vui lòng thử lại" });
                }
                return Json(new { isSuccess = 0, message = "Vui lòng phân quyền dữ liệu cho user" });
            }
            else
            {
                return Json(new { isSuccess = 0, message = "Không tồn tại thông tin user" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> GetActionPermisson(int Id)
        {
            //Lấy ra tất cả các action phân quyền
            var ListActionPermisson = _moduleService.GetAllAction(GetToken(), (int)Common.Extensions.EnumApplication.WebLOS);
            var userInfo = _userServices.GetById(GetToken(), Id);
            //Lấy ra các quyền đã được gán cho user đó
            var listPermission = new List<ModuleDetail>();
            if (userInfo != null && userInfo.Modules != null && userInfo.Modules.Count > 0)
            {
                listPermission = userInfo.Modules.Where(x => x.ApplicationId == (int)Common.Extensions.EnumApplication.WebLOS).ToList();
            }
            //nếu k có cấu hình theo user => lấy permisson group của user
            if (userInfo.GroupId > 0 && (listPermission == null || listPermission.Count == 0))
            {
                if (listPermission == null || listPermission.Count == 0)
                {
                    listPermission = _moduleService.GetByGroupId(GetToken(), userInfo.GroupId.Value, (int)Common.Extensions.EnumApplication.WebLOS);
                }
            }

            var ListResult = new List<ActionPermissionItem>();
            if (ListActionPermisson != null && ListActionPermisson.Count > 0)
            {
                foreach (var item in ListActionPermisson)
                {
                    var obj = new ActionPermissionItem() { Name = item.Name, ModuleId = item.ModuleId, Controller = item.Controller, Action = item.Action, Selected = false };

                    if (listPermission.Any(x => x.ModuleId == item.ModuleId))
                    {
                        obj.Selected = true;
                    }
                    ListResult.Add(obj);
                }
            }
            return View("/Views/Users/Partial/_ActionPermisson.cshtml", ListResult);
        }

        public async Task<IActionResult> GetActionPermissonGroup(int GroupId)
        {
            //Lấy ra tất cả các action phân quyền
            var ListActionPermisson = _moduleService.GetAllAction(GetToken(), (int)Common.Extensions.EnumApplication.WebLOS);
            //Lấy ra các quyền đã được gán cho user đó
            var listPermission = _moduleService.GetByGroupId(GetToken(), GroupId, (int)Common.Extensions.EnumApplication.WebLOS);

            var ListResult = new List<ActionPermissionItem>();
            if (ListActionPermisson != null && ListActionPermisson.Count > 0)
            {
                foreach (var item in ListActionPermisson)
                {
                    var obj = new ActionPermissionItem() { Name = item.Name, ModuleId = item.ModuleId, Controller = item.Controller, Action = item.Action, Selected = false };

                    if (listPermission.Any(x => x.ModuleId == item.ModuleId))
                    {
                        obj.Selected = true;
                    }
                    ListResult.Add(obj);
                }
            }
            return View("/Views/Users/Partial/_ActionPermisson.cshtml", ListResult);
        }


        public async Task<IActionResult> GetMenuPermisson(int Id)
        {
            List<JSTreeModel> lstPermission = new List<JSTreeModel>();
            if (Id == 0)
                return Json(new { Data = lstPermission, status = 1, message = "Lây thông tin thành công" });

            var _listModule = _moduleService.GetAllMenu(GetToken(), (int)Common.Extensions.EnumApplication.WebLOS);
            _listModule = _listModule.OrderBy(m => m.ParentId).ToList();
            var listPermission = new List<ModuleDetail>();
            var userInfo = _userServices.GetById(GetToken(), Id);
            if (userInfo != null && userInfo.Modules != null && userInfo.Modules.Count > 0)
                listPermission = userInfo.Modules;

            //nếu k có cấu hình theo user => lấy permisson group
            if (userInfo.GroupId > 0 && (listPermission == null || listPermission.Count == 0))
            {
                if (listPermission == null || listPermission.Count == 0)
                    listPermission = _moduleService.GetByGroupId(GetToken(), userInfo.GroupId.Value, (int)Common.Extensions.EnumApplication.WebLOS);
            }
            foreach (var item in _listModule)
            {
                JSTreeModel objCurrentNode = new JSTreeModel();
                objCurrentNode.id = item.ModuleId.ToString();
                objCurrentNode.text = item.Name.ToString();
                objCurrentNode.ControllerName = item.Controller == null ? item.Controller : "";
                objCurrentNode.ActionName = item.Action == null ? item.Action : "";
                if (Id > 0)
                {
                    if (listPermission.Any(x => x.ModuleId == item.ModuleId && x.ParentId != null) || listPermission.Any(x => x.ModuleId == item.ModuleId && item.IsMenu != true))
                        objCurrentNode.state.selected = true;
                }
                if (item.ParentId == null)
                    lstPermission.Add(objCurrentNode);
                else
                    AddCurrentNodeToParent(lstPermission, objCurrentNode, item.ParentId.ToString());
            }
            return Json(new { Data = lstPermission, status = 1, message = "Lây thông tin thành công" });
        }

        public async Task<IActionResult> GetMenuPermissonByGroup(int Id)
        {
            List<JSTreeModel> lstPermission = new List<JSTreeModel>();
            if (Id == 0)
                return Json(new { Data = lstPermission, status = 1, message = "Lây thông tin thành công" });

            var _listModule = _moduleService.GetAllMenu(GetToken(), (int)Common.Extensions.EnumApplication.WebLOS);
            _listModule = _listModule.OrderBy(m => m.ParentId).ToList();
            var listPermission = _moduleService.GetByGroupId(GetToken(), Id, (int)Common.Extensions.EnumApplication.WebLOS);
            foreach (var item in _listModule)
            {
                JSTreeModel objCurrentNode = new JSTreeModel();
                objCurrentNode.id = item.ModuleId.ToString();
                objCurrentNode.text = item.Name.ToString();
                objCurrentNode.ControllerName = item.Controller == null ? item.Controller : "";
                objCurrentNode.ActionName = item.Action == null ? item.Action : "";
                if (Id > 0)
                {
                    if (listPermission.Any(x => x.ModuleId == item.ModuleId && x.ParentId != null) || listPermission.Any(x => x.ModuleId == item.ModuleId && item.IsMenu != true))
                        objCurrentNode.state.selected = true;
                }
                if (item.ParentId == null)
                    lstPermission.Add(objCurrentNode);
                else
                    AddCurrentNodeToParent(lstPermission, objCurrentNode, item.ParentId.ToString());
            }
            return Json(new { Data = lstPermission, status = 1, message = "Lây thông tin thành công" });
        }


        private void AddCurrentNodeToParent(List<JSTreeModel> lstData, JSTreeModel objCurrentNode, string strParentID)
        {
            if (lstData != null && lstData.Count > 0)
            {
                for (int i = 0; i < lstData.Count; i++)
                {
                    if (lstData[i].id == strParentID)
                    {
                        if (lstData[i].children == null) lstData[i].children = new List<JSTreeModel>();
                        lstData[i].children.Add(objCurrentNode);
                        return;
                    }
                }
            }
        }

        public IActionResult GetUserById(int Id)
        {
            var entity = new UserItem();
            var listGroup = _groupServices.GetAll(GetToken());
            entity.ListGroup = listGroup;
            entity.ListAllHub = _shopService.GetAllHub(GetToken());

            // Query api   
            var user = _userServices.GetById(GetToken(), Id);
            entity = _mapper.Map<UserItem>(user);
            entity.ListGroup = listGroup;
            entity.ListAllHub = _shopService.GetAllHub(GetToken());
            var lstTelesalesShift = _telesalesServices.GetListTelesalesShift(GetToken());
            if (lstTelesalesShift != null && lstTelesalesShift.Count > 0)
                entity.ListTelesalesShift = lstTelesalesShift;

            if (user.ListShop != null && user.ListShop.Count > 0)
                entity.ListHub = user.ListShop;

            if (entity != null && entity.UserId > 0)
                return Json(new { data = entity, status = 1, message = "Lấy thông tin thành công" });
            else
                return Json(new { data = entity, status = 0, message = "Lấy thông tin thất bại" });
        }

        [HttpPost]
        public IActionResult AddOrUpdateUser(UserItem entity)
        {
            //gọi api tạo user bên sso
            var reqCreateSSO = new ResquestUserSSO()
            {
                Username = entity.Username,
                Password = entity.Password ?? "Tima@2021",
                Email = entity.Email,
                IsValidOtp = true
            };
            var createSSO = _ssoService.CreateUserSSO(GetToken(), reqCreateSSO);
            if (createSSO != null && createSSO.data != null && createSSO.data.id > 0)
            {
                var obj = new UserDTO();
                //cập nhập thông tin nhân viên
                if (entity != null && entity.UserId > 0)
                {
                    var userInfo = _userServices.GetById(GetToken(), entity.UserId);
                    Common.Utils.Ultility.CopyObject(entity, ref obj, true);
                    //var obj = _mapper.Map<UserDTO>(entity);
                    obj.UpdatedTime = DateTime.Now;
                    obj.Password = userInfo.Password;
                    obj.UserIdSSO = createSSO.data.id;
                    var result = _userServices.Update(GetToken(), entity.UserId, obj);
                    if (result)
                        return Json(new { status = 1, message = "Cập nhật tài khoản thành công" });
                    return Json(new { status = 0, message = "Xảy ra lỗi khi cập nhật thông tin tài khoản. Vui lòng liên hệ quản trị viên!" });
                }
                //Tạo mới nhân viên
                else
                {
                    var objuser = _userServices.GetByUser(GetToken(), entity.Username.Trim());
                    if (objuser.UserId > 0)
                    {
                        return Json(new { status = 0, message = "Tên tài khoản đã tồn tại." });
                    }
                    Common.Utils.Ultility.CopyObject(entity, ref obj, true);
                    obj.HubIds = entity.HubIds;
                    obj.CreatedTime = DateTime.Now;
                    obj.Password = Security.GetMD5Hash(entity.Password);
                    obj.CreatedBy = GetUserGlobal().UserId;
                    obj.UserIdSSO = createSSO.data.id;
                    var result = _userServices.Create(GetToken(), obj);

                    if (result)
                        return Json(new { status = 1, message = "Thêm mới tài khoản thành công" });
                    return Json(new { status = 0, message = "Xảy ra lỗi khi thêm mới tài khoản. Vui lòng liên hệ quản trị viên!" });
                }
            }
            else
                return Json(new { status = 0, message = "Có lỗi không mong muốn xảy ra. Vui lòng liên hệ quản trị viên!" });
        }
        [HttpPost]
        public IActionResult ResetPassword(int Id, int type = 0, string PasswordOld = "", string PasswordNew = "")
        {

            var obj = new UserDTO();
            var _password = "";
            var _userId = 0;
            // reset lại mật khẩu
            if (Id > 0 && type == 0 && PasswordNew == "")
            {
                _password = "Tima@2021";
                _userId = Id;
            }
            // đổi mật khẩu
            else
            {
                var user = GetUserGlobal();
                _password = PasswordNew;
                _userId = user.UserId;
            }
            // lấy ra bảng user theo userId
            var userInfo = _userServices.GetById(GetToken(), _userId);
            Common.Utils.Ultility.CopyObject(userInfo, ref obj, true);
            if (obj.CityId == 0)
                obj.CityId = null;
            obj.Password = Security.GetMD5Hash(_password);
            if (userInfo != null && Id == 0)
            {
                var resultPass = _userServices.GetPasswordById(GetToken(), _userId);
                if (resultPass != null)
                {
                    if (Security.GetMD5Hash(PasswordOld) != resultPass.Password)
                        return Json(new { status = 0, message = "Bạn nhập sai mật khẩu cũ." });
                }
            }
            //gọi api cập nhập sang sso
            if (userInfo.UserIdSSO > 0)
            {
                var reqObjSSO = new RequestUpdateUserSSO()
                {
                    Id = userInfo.UserIdSSO.Value,
                    Password = _password
                };
                var updateSSO = _ssoService.UpdatePassword(GetToken(), reqObjSSO);
                if (updateSSO == null || updateSSO.meta.errorCode != 200)
                    return Json(new { status = 0, message = "Lỗi. Rest mật khẩu sang SSO. Vui lòng báo kỹ thuật." });
            }

            var result = _userServices.UpdatePassword(GetToken(), _userId, obj);
            if (result)
                return Json(new { status = 1, message = "Đặt lại mật khẩu thành công" });
            return Json(new { status = 0, message = "Xảy ra lỗi khi mật khẩu về mặc định. Vui lòng liên hệ quản trị viên!" });
        }

        [HttpPost]
        public IActionResult ChangeShopGlobal(int ShopId, string ShopName)
        {
            var user = GetUserGlobal();
            if (user != null && user.ListShop != null && user.ListShop.Count > 0)
            {
                user.ShopGlobal = ShopId;
                user.ShopGlobalName = ShopName;
                // save access token					
                HttpContext.Session.SetObjectAsJson("USER_DATA", user);
                return Json(new { status = 1, message = "Thay đổi cửa hàng thành công" });
            }
            else
                return Json(new { status = 0, message = "Xảy ra lỗi khi thay đổi cửa hàng. Vui lòng liên hệ quản trị viên!" });
        }

        #region Quản lý tài khoản cho HCNS
        [HttpGet]
        [Route("user/manageruser.html")]
        public IActionResult ManageAccountHCNS()
        {
            var model = new UserViewModel();
            var listGroup = _groupServices.GetAllNotAdministrator(GetToken());
            var listAllHub = _shopService.GetAllHub(GetToken());
            model.ListGroup = listGroup;
            ViewBag.ListGroup = model;
            ViewBag.ListAllHub = listAllHub;
            return View(model);
        }
        public async Task<IActionResult> GetListAccountOfTima()
        {
            try
            {
                var modal = new UserDatatable(HttpContext.Request.Form);
                int recordsTotal = 0;
                // Query api   	
                List<UserDetail> data = new List<UserDetail>();
                data = _userServices.GetListAccountOfTima(GetToken(), modal.ToQueryObject(), ref recordsTotal);
                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        item.UrlEdit = EncryptUtils.Encrypt(string.Format("?Id={0}", item.UserId), true);
                    }

                }
                modal.total = recordsTotal.ToString();
                //Returning Json Data  
                return Json(new { meta = modal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public IActionResult ChangeStatus(int Id, int Status)
        {
            var obj = new UserDTO();
            var userInfo = _userServices.GetById(GetToken(), Id);
            Common.Utils.Ultility.CopyObject(userInfo, ref obj, true);
            obj.CityId = null;
            obj.Status = Status;
            var result = _userServices.Update(GetToken(), Id, obj);

            if (result)
                return Json(new { status = 1, message = "Thay đổi trạng thái thành công" });
            return Json(new { status = 0, message = "Xảy ra lỗi khi thay đổi trạng thái. Vui lòng liên hệ quản trị viên!" });
        }
        #endregion

        [HttpPost]
        public IActionResult SaveChangeInformation(ChangeInformation entity)
        {
            if (entity != null && entity.UserId > 0)
            {
                var resultPass = _userServices.GetPasswordById(GetToken(), entity.UserId);
                if (Security.GetMD5Hash(entity.Password) != resultPass.Password)
                    return Json(new { status = 0, message = "Bạn nhập sai mật khẩu. Vui lòng kiểm tra lại." });
                var result = _userServices.ChangeInformation(GetToken(), entity.MapToUserChangeInformation());
                if (result)
                    return Json(new { status = 1, message = "Cập nhập thông tin thành công." });
                else
                    return Json(new { status = 0, message = "Cập nhập thông tin thất bại." });
            }
            else
                return Json(new { status = 0, message = "Tài khoản không tồn tại." });

        }

        public IActionResult GetPermissionApi(int userId)
        {
            //Lấy ra tất cả các action phân quyền
            var ListActionPermisson = _moduleService.GetAllAction(GetToken(), (int)Common.Extensions.EnumApplication.ApiLOS);
            var userInfo = _userServices.GetById(GetToken(), userId);
            //Lấy ra các quyền đã được gán cho user đó
            var listPermission = new List<ModuleDetail>();
            if (userInfo != null && userInfo.Modules != null && userInfo.Modules.Count > 0)
            {
                listPermission = userInfo.Modules.Where(x => x.ApplicationId == (int)Common.Extensions.EnumApplication.ApiLOS).ToList();
            }
            //nếu k có cấu hình theo user => lấy permisson group của user
            if (userInfo.GroupId > 0 && (listPermission == null || listPermission.Count == 0))
            {
                if (listPermission == null || listPermission.Count == 0)
                {
                    listPermission = _moduleService.GetByGroupId(GetToken(), userInfo.GroupId.Value, (int)Common.Extensions.EnumApplication.ApiLOS);
                }
            }

            var ListResult = new List<ActionPermissionItem>();
            if (ListActionPermisson != null && ListActionPermisson.Count > 0)
            {
                foreach (var item in ListActionPermisson)
                {
                    var obj = new ActionPermissionItem() { Name = item.Name, ModuleId = item.ModuleId, Controller = item.Controller, Action = item.Action, Selected = false, Url = item.Path };

                    if (listPermission.Any(x => x.ModuleId == item.ModuleId))
                    {
                        obj.Selected = true;
                    }
                    ListResult.Add(obj);
                }
            }
            return View("~/Views/Users/Partial/UserPermissionApi.cshtml", ListResult);
        }

        [HttpPost]
        public IActionResult SaveActionPermissonApi(int UserId, List<int> lstAction)
        {
            List<UserModule> lstModule = new List<UserModule>();
            //xử lý action phân quyền
            if (lstAction != null && lstAction.Count > 0)
            {
                foreach (var item in lstAction)
                {
                    UserModule objmodule = new UserModule();
                    objmodule.ModuleId = Convert.ToInt32(item);
                    objmodule.UserId = UserId;
                    objmodule.ApplicationId = (int)Common.Extensions.EnumApplication.ApiLOS;
                    lstModule.Add(objmodule);
                }
            }
            var result = _moduleService.SaveActionPermissonApiUser(GetToken(), UserId, lstModule);
            if (result)
                return Json(new { status = 1, message = "Lưu lại thành công" });
            else
                return Json(new { status = 0, message = "Lưu lại không thành công" });
        }

        public async Task<IActionResult> ConfigCiscoMobile(int userId)
        {
            var data = await _userV2Service.GetUserMobileCisco(userId);
            if (data == null || data.UserId == 0)
            {
                //thêm mới UserMobileCisco
                var user = await _userV2Service.GetById(userId);
                var insertUserMobileCisco = await _userV2Service.CreateConfigCiscoMobile(new UserMobileCisco()
                {
                    UserId = user.UserId,
                    CreatedTime = DateTime.Now,
                    Username = user.Username,
                    FullName = user.FullName,
                    Status = user.Status,
                });
                if (insertUserMobileCisco > 0)
                    data = new UserMobileCiscoDetail()
                    {
                        UserId = user.UserId,
                        Username = user.Username,
                        FullName = user.FullName,
                    };
            }
            return View("~/Views/Users/Partial/ConfigCiscoMobile.cshtml", data);
        }

        [HttpPost]
        public async Task<IActionResult> SaveConfigCiscoMobile(UserMobileCisco entity)
        {
            var update = await _userV2Service.SaveConfigCiscoMobile(entity);
            if (update)
                return Json(new { status = 1, message = "Cấu hình Cisco mobile thành công" });
            else
                return Json(new { status = 0, message = "Cấu hình Cisco mobile thất bại" });
        }
    }
}