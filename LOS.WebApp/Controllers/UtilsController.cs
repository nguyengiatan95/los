﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using RestSharp;

namespace LOS.WebApp.Controllers
{
    public class UtilsController : Controller
    {
        private IDictionaryService _dictionaryService;
        private ILMSService _lmsService;

        public UtilsController(ILMSService lmsService)
        {
            _lmsService = lmsService;
        }
        [HttpPost]
        public IActionResult SendSMS(int loanBriefId,string phone)
        {
            if(!phone.StartsWith("0"))
            {
                phone = phone.Insert(0, "0");
            }
            ViewBag.LoanBriefId = loanBriefId;
            ViewBag.Phone = phone;
            return View();
        }

        [HttpPost]
        public IActionResult SendSMSFPT(string Phone, string Content)
        {
            
            var result = _lmsService.SendSMS(Phone, Content);

            if (result.Result == 1 && result.Message.Contains("thành công"))
            {
                return Json(new { Status = 1, Message = "Gửi tin nhắn thành công" });
            }
            else
            {
                return Json(new { Status = 0 , Message = "Gửi tin nhắn thất bại" });
            }
        }

        
        [HttpPost]
        public IActionResult GetNoteByLoanID(int loanBriefId)
        {
            try
            {
                //loanBriefId = 10019;
                var data = new List<LoanBriefNoteDetail>();
                // Query api   	
                //var data = loanBriefNoteService.GetNoteByLoanID(GetToken(), loanBriefId);
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}