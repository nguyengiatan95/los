FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 8888

RUN apt-get update \
    && apt-get install -y --no-install-recommends libgdiplus libc6-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ["LOS.WebApp/LOS.WebApp.csproj", "LOS.WebApp/"]
RUN dotnet restore "LOS.WebApp/LOS.WebApp.csproj"
COPY . .
WORKDIR "/src/LOS.WebApp"
RUN dotnet build "LOS.WebApp.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "LOS.WebApp.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "LOS.WebApp.dll"]