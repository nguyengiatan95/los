﻿using LOS.DAL.DTOs;
using LOS.WebApp.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.WebApp.Factories
{
    public interface ILoanbriefModelFactory
    {
        Task<LoanBriefHubDatatable> PrepareHubRequestModels(LoanBriefHubDatatable model, UserDetail user);
        Task<List<LoanBriefDetail>> PrepareLoanbriefHubOverviewModels(List<LoanBriefDetail> data, UserDetail user);
        Task<List<LoanBriefDetail>> PrepareLoanbriefStaffHubOverviewModels(List<LoanBriefDetail> data, UserDetail user);
        Task<LoanBriefHubDatatable> PrepareStaffHubRequestModels(LoanBriefHubDatatable model, UserDetail user);
    }
}