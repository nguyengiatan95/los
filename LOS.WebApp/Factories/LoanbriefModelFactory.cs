﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.Respository;
using LOS.Services.Services;
using LOS.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Factories
{
    public class LoanbriefModelFactory : ILoanbriefModelFactory
    {
        public readonly ILogDistributionUserService _logDistributionUserService;
        public readonly IUserV2Service _userService;
        public LoanbriefModelFactory(ILogDistributionUserService logDistributionUserService, IUserV2Service userService)
        {
            this._logDistributionUserService = logDistributionUserService;
            this._userService = userService;
        }
        public async Task<LoanBriefHubDatatable> PrepareHubRequestModels(LoanBriefHubDatatable model, UserDetail user)
        {
            //Lấy ra những đơn có chứng từ
            model.HubId = user.ShopGlobal;
            var listStatus = new List<int>();
            if (model.LoanStatus == null || model.LoanStatus == "-1")
            {
                listStatus.Add((int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING);
                listStatus.Add((int)EnumLoanStatus.APPRAISER_REVIEW);
                listStatus.Add((int)EnumLoanStatus.HUB_CHT_APPROVE);
                listStatus.Add((int)EnumLoanStatus.BRIEF_APPRAISER_APPROVE_PROPOSION);
                listStatus.Add((int)EnumLoanStatus.LENDER_LOAN_DISTRIBUTING);
                listStatus.Add((int)EnumLoanStatus.WAIT_DISBURSE);
            }
            else
            {
                if (model.LoanStatus.Contains(","))
                {
                    var arrStatus = model.LoanStatus.Split(",").Select<string, int>(int.Parse).ToList();
                    listStatus.AddRange(arrStatus);
                    model.LoanStatus = "-1";
                }
                else
                {
                    listStatus.Add(int.Parse(model.LoanStatus));
                }
            }
            if (model.LoanStatusDetail == "0" || string.IsNullOrEmpty(model.LoanStatusDetail))
                model.LoanStatusDetail = "-1";

            if (listStatus != null && listStatus.Count > 0)
                model.ListLoanStatus = string.Join(",", listStatus);
            return model;
        }
        public async Task<List<LoanBriefDetail>> PrepareLoanbriefHubOverviewModels(List<LoanBriefDetail> data, UserDetail user)
        {
            //Lấy ra những đơn có chứng từ
            var listLoanbriefId = data.Select(x => x.LoanBriefId).ToList();

            var logDistributionUsers = await _logDistributionUserService.GetLogDistributionUser((int)TypeDistributionLog.CVKD, listLoanbriefId);

            var dicLogDistributionUser = new Dictionary<string, LogDistributionUserDetail>();
            if (logDistributionUsers != null && logDistributionUsers.Count > 0)
            {
                foreach (var item in logDistributionUsers)
                {
                    if (item.UserId != (int)EnumUser.spt_System && item.UserId != (int)EnumUser.ctv_autocall
                        && item.UserId != (int)EnumUser.follow && item.UserId != (int)EnumUser.autocall_coldlead
                        && item.LoanbriefId > 0 && item.UserId > 0)
                    {
                        var key = $"{item.LoanbriefId}-{item.UserId}";
                        if (!dicLogDistributionUser.ContainsKey(key))
                            dicLogDistributionUser[key] = item;
                    }
                }
            }
            //Lấy ra danh sách user thuộc nhóm tài khoản Hub
            var listUserHub = await _userService.GetUserBasicByGroup(new List<int>() { (int)EnumGroupUser.ManagerHub, (int)EnumGroupUser.StaffHub });
            foreach (var item in data)
            {
                bool isHubCreated = false;
                //Check xem đơn có phải hub tự tạo hoặc là gói vay Oto
                if (item.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                    item.PickTLS = true;
                else
                {
                    if (item.CreateBy != null)
                    {
                        if (listUserHub.Any(x => x.UserId == item.CreateBy))
                        {
                            isHubCreated = true;
                            item.PickTLS = true;
                        }
                    }
                }
                ////Nếu đơn không qua tls => cho phép lấy location ở hub
                //if (!item.PickTLS && item.BoundTelesaleId.GetValueOrDefault(0) == 0)
                //    item.PickTLS = true;

                //Mặc định cho hub lấy location
                item.PickTLS = true;
                //set permission row
                if (user.ListPermissionAction != null && user.ListPermissionAction.Count > 0)
                {
                    if (item.InProcess != EnumInProcess.Process.GetHashCode()
                        && item.Status != EnumLoanStatus.CANCELED.GetHashCode() && user.ListPermissionAction.Any(x => x.LoanStatus == item.Status))
                    {
                        var permission = user.ListPermissionAction.FirstOrDefault(x => x.LoanStatus == item.Status);
                        if (permission != null && permission.Value > 0)
                        {
                            if ((EnumActionUser.Edit.GetHashCode() & permission.Value) == EnumActionUser.Edit.GetHashCode())
                                item.IsEdit = true;
                            if ((EnumActionUser.Script.GetHashCode() & permission.Value) == EnumActionUser.Script.GetHashCode())
                                item.IsScript = true;
                            if ((EnumActionUser.PushLoan.GetHashCode() & permission.Value) == EnumActionUser.PushLoan.GetHashCode())
                                item.IsPush = true;
                            if ((EnumActionUser.Cancel.GetHashCode() & permission.Value) == EnumActionUser.Cancel.GetHashCode())
                            {
                                if (isHubCreated || item.ProductId == (int)EnumProductCredit.OtoCreditType_CC || item.BoundTelesaleId.GetValueOrDefault(0) == 0)
                                    item.IsCancel = true;
                            }
                            if ((EnumActionUser.Upload.GetHashCode() & permission.Value) == EnumActionUser.Upload.GetHashCode())
                                item.IsUpload = true;
                            if ((EnumActionUser.ComfirmCancel.GetHashCode() & permission.Value) == EnumActionUser.ComfirmCancel.GetHashCode())
                                item.IsConfirmCancel = true;

                            //Nếu là đơn topup và trạng thái chờ CHT chia đơn và do hub tạo thì k cho trả lại
                            if (item.Status == (int)EnumLoanStatus.HUB_CHT_LOAN_DISTRIBUTING && item.TypeRemarketing == (int)EnumTypeRemarketing.IsTopUp && isHubCreated)
                                item.IsBack = false;
                            else if ((EnumActionUser.BackLoan.GetHashCode() & permission.Value) == EnumActionUser.BackLoan.GetHashCode())
                                item.IsBack = true;

                            if (isHubCreated)
                                item.IsChangePhone = true;
                        }
                    }


                }
                //phần thời gian xử lý đơn từ khi nhận đơn
                if (item.HubEmployeeId > 0)
                {
                    var key = $"{item.LoanBriefId}-{item.HubEmployeeId}";
                    if (dicLogDistributionUser.ContainsKey(key))
                    {
                        var dataResponseTelesalse = new LOS.DAL.Object.TimeProcessingLoanBrief()
                        {
                            LoanBriefId = item.LoanBriefId,
                            IsFeedBack = false
                        };
                        var logDistributionUser = dicLogDistributionUser[key];
                        if (logDistributionUser.CreatedAt.HasValue)
                        {
                            if (item.HubEmployeeCallFirst.HasValue)
                            {
                                dataResponseTelesalse.TimeProcessing = (int)item.HubEmployeeCallFirst.Value.Subtract(logDistributionUser.CreatedAt.Value).TotalMinutes;
                                dataResponseTelesalse.IsFeedBack = true;
                            }
                            else
                                dataResponseTelesalse.TimeProcessing = (int)DateTime.Now.Subtract(logDistributionUser.CreatedAt.Value).TotalMinutes;
                            item.TimeProcessingLoan = dataResponseTelesalse;
                        }
                    }
                }
            }
            return data;
        }

        public async Task<LoanBriefHubDatatable> PrepareStaffHubRequestModels(LoanBriefHubDatatable model, UserDetail user)
        {
            model.HubEmployeeId = user.UserId.ToString();
            model.HubId = user.ShopGlobal;
            var listStatus = new List<int>();

            if (model.HubLoanStatusChild == "0" || string.IsNullOrEmpty(model.HubLoanStatusChild))
                model.HubLoanStatusChild = "-1";
            if (model.LoanStatusDetail == "0" || string.IsNullOrEmpty(model.LoanStatusDetail))
                model.LoanStatusDetail = "-1";
            //Set mặc định nếu là nv hub sẽ ở trạng thái chờ tư 
            if (model.LoanStatus == null || model.LoanStatus == "-1")
                listStatus.Add((int)EnumLoanStatus.APPRAISER_REVIEW);
            else
            {
                if (model.LoanStatus.Contains(","))
                {
                    var arrStatus = model.LoanStatus.Split(",").Select<string, int>(int.Parse).ToList();
                    listStatus.AddRange(arrStatus);
                    model.LoanStatus = "-1";
                }
                else
                {
                    listStatus.Add(int.Parse(model.LoanStatus));
                    model.LoanStatus = model.LoanStatus;
                }
            }

            if (listStatus != null && listStatus.Count > 0)
                model.ListLoanStatus = string.Join(",", listStatus);
            return model;
        }
        public async Task<List<LoanBriefDetail>> PrepareLoanbriefStaffHubOverviewModels(List<LoanBriefDetail> data, UserDetail user)
        {
            //Lấy ra những đơn có chứng từ
            var listLoanbriefId = data.Select(x => x.LoanBriefId).ToList();

            var logDistributionUsers = await _logDistributionUserService.GetLogDistributionUser((int)TypeDistributionLog.CVKD, listLoanbriefId);

            var dicLogDistributionUser = new Dictionary<string, LogDistributionUserDetail>();
            if (logDistributionUsers != null && logDistributionUsers.Count > 0)
            {
                foreach (var item in logDistributionUsers)
                {
                    if (item.UserId != (int)EnumUser.spt_System && item.UserId != (int)EnumUser.ctv_autocall
                        && item.UserId != (int)EnumUser.follow && item.UserId != (int)EnumUser.autocall_coldlead
                        && item.LoanbriefId > 0 && item.UserId > 0)
                    {
                        var key = $"{item.LoanbriefId}-{item.UserId}";
                        if (!dicLogDistributionUser.ContainsKey(key))
                            dicLogDistributionUser[key] = item;
                    }
                }
            }
            //Lấy ra danh sách user thuộc nhóm tài khoản Hub
            var listUserHub = await _userService.GetUserBasicByGroup(new List<int>() { (int)EnumGroupUser.ManagerHub, (int)EnumGroupUser.StaffHub });

            var task = Task.Run(() => Parallel.ForEach(data, item =>
            {
                bool isHubCreated = false;
                //Check xem đơn có phải hub tự tạo hoặc là gói vay Oto
                if (item.ProductId == (int)EnumProductCredit.OtoCreditType_CC)
                    item.PickTLS = true;
                else
                {
                    if (item.CreateBy != null)
                    {
                        if (listUserHub.Any(x => x.UserId == item.CreateBy))
                        {
                            isHubCreated = true;
                            item.PickTLS = true;
                        }
                    }
                }
                //Kiểm tra đơn có đủ điều kiện check Momo
                if (!string.IsNullOrEmpty(item.FullName) && !string.IsNullOrEmpty(item.Dob.ToString()) && !string.IsNullOrEmpty(item.NationalCard))
                    item.IsCheckMomo = true;


                ////Nếu đơn không qua tls => cho phép lấy location ở hub
                //if (!item.PickTLS && item.BoundTelesaleId.GetValueOrDefault(0) == 0)
                //    item.PickTLS = true;

                //Mặc định cho hub check location
                item.PickTLS = true;

                if (user.ListPermissionAction != null && user.ListPermissionAction.Count > 0)
                {

                    if (item.InProcess != EnumInProcess.Process.GetHashCode()
                        && (item.Status != EnumLoanStatus.CANCELED.GetHashCode() && user.ListPermissionAction.Any(x => x.LoanStatus == item.Status)))
                    {
                        var permission = user.ListPermissionAction.FirstOrDefault(x => x.LoanStatus == item.Status);
                        if (permission != null && permission.Value > 0)
                        {
                            if ((EnumActionUser.Edit.GetHashCode() & permission.Value) == EnumActionUser.Edit.GetHashCode())
                                item.IsEdit = true;
                            if ((EnumActionUser.Script.GetHashCode() & permission.Value) == EnumActionUser.Script.GetHashCode())
                                item.IsScript = true;
                            if ((EnumActionUser.PushLoan.GetHashCode() & permission.Value) == EnumActionUser.PushLoan.GetHashCode())
                                item.IsPush = true;
                            if ((EnumActionUser.Cancel.GetHashCode() & permission.Value) == EnumActionUser.Cancel.GetHashCode())
                            {
                                if (isHubCreated || item.ProductId == (int)EnumProductCredit.OtoCreditType_CC || item.BoundTelesaleId.GetValueOrDefault(0) == 0 || item.PlatformType == (int)EnumPlatformType.LendingOnlineWeb || item.PlatformType == (int)EnumPlatformType.LendingOnlineApp)
                                    item.IsCancel = true;
                            }
                            if ((EnumActionUser.Upload.GetHashCode() & permission.Value) == EnumActionUser.Upload.GetHashCode())
                                item.IsUpload = true;
                            if ((EnumActionUser.ComfirmCancel.GetHashCode() & permission.Value) == EnumActionUser.ComfirmCancel.GetHashCode())
                                item.IsConfirmCancel = true;
                        }
                    }
                    //phần thời gian xử lý đơn từ khi nhận đơn
                    //Ngoại trừ đơn từ rmkt về
                    if (item.HubEmployeeId > 0 && item.UtmSource != "form_remkt_hub")
                    {
                        var key = $"{item.LoanBriefId}-{item.HubEmployeeId}";
                        if (dicLogDistributionUser.ContainsKey(key))
                        {
                            var dataResponseTelesalse = new LOS.DAL.Object.TimeProcessingLoanBrief()
                            {
                                LoanBriefId = item.LoanBriefId,
                                IsFeedBack = false
                            };
                            var logDistributionUser = dicLogDistributionUser[key];
                            if (logDistributionUser.CreatedAt.HasValue)
                            {
                                if (item.HubEmployeeCallFirst.HasValue)
                                {
                                    dataResponseTelesalse.TimeProcessing = (int)item.HubEmployeeCallFirst.Value.Subtract(logDistributionUser.CreatedAt.Value).TotalMinutes;
                                    dataResponseTelesalse.IsFeedBack = true;
                                }
                                else
                                    dataResponseTelesalse.TimeProcessing = (int)DateTime.Now.Subtract(logDistributionUser.CreatedAt.Value).TotalMinutes;
                                item.TimeProcessingLoan = dataResponseTelesalse;
                            }
                        }
                    }
                }
            }));
            Task.WaitAll(task);
            return data;
        }
    }
}
