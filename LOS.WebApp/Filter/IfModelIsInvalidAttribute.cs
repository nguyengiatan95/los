﻿using LOS.Common.Helpers;
using LOS.Common.Models.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System.Linq;

namespace LOS.WebApp.Filter
{
    public class IfModelIsInvalidAttribute : ActionFilterAttribute
    {

        #region Properties

        public string RedirectToController { get; set; }

        public string RedirectToAction { get; set; }

        public string RedirectToPage { get; set; }

        public bool IsReturnJson { get; set; } = false;
        #endregion

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                if (IsReturnJson)
                    context.Result = new JsonResult(new DefaultResponse<string> { meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE) });
                else
                    context.Result = new RedirectToRouteResult(ConstructRouteValueDictionary());
            }
        }

        #region Private Methods

        private RouteValueDictionary ConstructRouteValueDictionary()
        {
            var dict = new RouteValueDictionary();

            if (!string.IsNullOrWhiteSpace(RedirectToPage))
            {
                dict.Add("page", RedirectToPage);
            }
            // Assuming RedirectToController & RedirectToAction are set
            else
            {
                dict.Add("controller", RedirectToController);
                dict.Add("action", RedirectToAction);
            }

            return dict;
        }

        #endregion
    }
}
