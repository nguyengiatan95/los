﻿using LOS.DAL.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Helpers
{
	public class AuthorizeFilterAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			var session = filterContext.HttpContext.Session;
			if (session == null || session.GetObjectFromJson<UserDetail>("USER_DATA") == null|| session.GetObjectFromJson<UserDetail>("USER_DATA").Result == null)
			{
				var _baseConfig = filterContext.HttpContext.RequestServices.GetService<IConfiguration>();
				filterContext.Result = new RedirectResult(_baseConfig["AppSettings:SSO"]);
                //filterContext.Result = new RedirectToActionResult("Index", "Login", null);
			}
		}
	}
}
