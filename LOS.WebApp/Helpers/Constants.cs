using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Helpers
{
    public class Constants
    {
        public static string KEY_GOOGLE_MAP { get; set; }
        public static string WEB_API_URL { get; set; }
        public static string LOGIN_ENDPOINT = "security/login";
        public static string GET_REDIRECT_LOGIN = "security/redirect_login";
        public static string USERS_ENDPOINT = "users";
        public static string USERS_GET_USER_BY_IPPHONE = "users/get_list_user_by_ipphone";
        public static string USERS_GET_USER_BY_CISCO = "users/get_list_user_by_cisco";
        public static string USERS_GET_LIST_ACCOUNT_OF_TIMA = "users/getlistaccountoftima";
        public static string USERS_GET_ALL = "users/getall";
        public static string USERS_GET_ENDPOINT = "users/{0}";
        public static string USERS_GET_USERNAME_ENDPOINT = "users/getuser?username={0}";
        public static string USERS_GET_USER_LOAN_ASC_ENDPOINT = "users/get_staff_hub_loan_asc?hubId={0}";
        public static string USERS_HUB_EMPLOYEE_LOAN_ASC_ENDPOINT = "users/staff_hub_loan_asc";
        public static string USERS_GET_PERMISSION_ENDPOINT = "users/get_user_permission?userId={0}";
        public static string USERS_UPDATE_PERMISSION_ENDPOINT = "users/update_permission";
        public static string USERS_UPDATE_ENDPOINT = "users/{0}";
        public static string USERS_UPDATE_PASSWORD_ENDPOINT = "users/update_password";
        public static string USERS_DELETE_ENDPOINT = "users/{0}";
        public static string USERS_CHANGE_INFORMATION = "users/chang_information";
        public static string USERS_PERMISSION_ENDPOINT = "users/permission?groupId={0}&userId={1}";
        public static string USERS_GET_PASSWORD = "users/get_password?userId={0}";
        public static string GET_USER_BY_LIST_USERID = "users/get_user_by_list_userid?userId={0}";
        public static string GET_PROVINCES_ENPOINT = "Dictionary/provinces";
        public static string GET_DISTRICTS_ENPOINT = "Dictionary/districts";
        public static string GET_WARDS_ENPOINT = "Dictionary/wards?district_id={0}";
        public static string GET_JOBS_ENPOINT = "Dictionary/jobs";
        public static string GET_JOBS_V2_ENPOINT = "Dictionary/jobs_v2";
        public static string GET_JOBS_BY_PARENTID_ENPOINT = "Dictionary/get_job_parentId?parentId={0}";
        public static string GET_JOBS_BY_ID_ENPOINT = "Dictionary/get_job_by_id?id={0}";
        public static string GET_LOAN_STATUS_ENPOINT = "Dictionary/loan_status";
        public static string GET_PIPELINE_STATE_ENPOINT = "Dictionary/pipeline_state";
        public static string GET_LOAN_PRODUCT_ENPOINT = "Dictionary/loan_product";
        public static string GET_UTMSOURCE_ENPOINT = "Dictionary/utm_source";
        public static string GET_RELATIVE_FAMILY_ENPOINT = "Dictionary/relative_family";
        public static string GET_BRAND_PRODUCT_ENPOINT = "Dictionary/brand_product";
        public static string GET_PRODUCT_ENPOINT = "Dictionary/list_product?brandId={0}";
        public static string GET_BANK_ENPOINT = "Dictionary/banks";
        public static string GET_SHOP_ENPOINT = "Dictionary/shops";
        public static string GET_SHOP_BY_CITYID_ENPOINT = "Dictionary/shops_cityid?cityId={0}";
        public static string GET_LENDER_ENPOINT = "Dictionary/lender_search";
        public static string GET_DOCUMENT_TYPE_ENPOINT = "Dictionary/document?ProductId={0}";
        public static string GET_DOCUMENT_TYPE_ALL_ENPOINT = "Dictionary/document_all";
        public static string GET_PLATFORM_ENPOINT = "Dictionary/list_platform";
        public static string GET_SCRIPT_ENPOINT = "Dictionary/script?productid={0}";
        public static string GET_USERBYGROUP_ENPOINT = "Dictionary/list_userbygroup?groupId={0}";
        public static string GET_REASON_CANCEL_ENPOINT = "Dictionary/reason_cancel";
        public static string ADD_LOG_CALL_API = "Dictionary/add_log_api";
        public static string ADD_LOG_CALL_AI = "Dictionary/add_log_ai";
        public static string ADD_LOG_SEND_SMS = "Dictionary/add_log_send_sms";
        public static string CHECK_LOG_CALL_AI_BY_ID_ENDPOINT = "Dictionary/check_logai/{0}";
        public static string GET_LOG_RESULT_AUTO_CALL = "Dictionary/get_log_result_auto_call/{0}";
        public static string LOANBRIEF_LENDER_BY_ID_ENDPOINT = "Dictionary/loanbrief_lender/{0}";
        public static string ADD_LOG_CLICK_TO_CALL = "Dictionary/add_log_click_to_call";
        public static string ADD_DISTRICT = "Dictionary/add_district";
        public static string ADD_WARD = "Dictionary/add_ward";
        public static string UPDATE_LOG_CALL_API = "Dictionary/update_log_api";
        public static string UPDATE_LOG_CALL_AI = "Dictionary/update_log_ai";
        public static string UPDATE_LOG_SEND_SMS = "Dictionary/update_log_send_sms";
        public static string GET_TYPE_OWNERSHIP_ENPOINT = "Dictionary/type_ownership";
        public static string GET_EVENT_CONFIG_ENPOINT = "Dictionary/event_config";
        public static string GET_TYPE_REMARKETING_ENPOINT = "Dictionary/type_remarketing";
        public static string GET_PRODUCT_REVIEWDETAIL_ENPOINT = "Dictionary/get_product_reviewresult?id={0}";
        public static string GET_CONFIG_DOCUMENT_ENPOINT = "Dictionary/get_config_document?productid={0}&typeownership={1}";
        public static string GET_DOCUMENT_TYPE_V2_ENPOINT = "Dictionary/get_docment_type_v2?productid={0}&typeownership={1}";
        public static string GET_INFOMATION_PRODUCT_DETAIL_BY_ID = "Dictionary/get_infomation_product_detail_by_id?id={0}";
        public static string BOUND_TELESALE_ID_ENDPOINT = "LoanBrief/update_userid";
        public static string APPROVE_ID_ENDPOINT = "LoanBrief/update_approveid";
        public static string LENDER_ID_ENDPOINT = "LoanBrief/update_lenderid";
        public static string ISUPLOADSTATE_ENDPOINT = "LoanBrief/update_isupload";
        public static string INSURENCE_ID_ENDPOINT = "LoanBrief/update_insurence";
        public static string ADD_SCHEDULE_TIME = "LoanBrief/add_schedule_time";
        public static string ADD__AND_EDIT_SCHEDULE = "LoanBrief/add_and_edit_schedule";
        public static string GET_DISTRICT_BY_ID = "Dictionary/get_district_by_id/{0}";
        public static string GET_PROVINCE_BY_ID = "Dictionary/get_province_by_id/{0}";
        public static string CHECK_LOG_SEND_SMS = "Dictionary/check_log_send_sms?loanbriefId={0}";
        public static string GET_PROVINCEID_BY_NAME = "Dictionary/get_provinceid_by_name?name={0}";
        public static string GET_DISTRICTID_BY_NAME = "Dictionary/get_districtid_by_name?name={0}";
        public static string GET_PRODUCTID_BY_NAME = "Dictionary/get_productid_by_name?name={0}";
        public static string ADD_CHECKLOAN_INFORMATION = "Dictionary/add_checkloan_information";
        public static string GET_CHECK_IMAGE_BY_LOANBRIEFID = "Dictionary/get_checkimage_by_loanBriefId/{0}";
        public static string ADD_DOCUMENTTYPE_IMAGE = "Dictionary/add_documenttype_image";
        public static string ADD_REASON_COORDINATOR = "Dictionary/add_reason_coordinator";
        public static string ADD_LOG_MOMO_INFO = "Dictionary/add_log_momo_info";
        public static string ADD_LOG_TRANSACTION_SECURED = "Dictionary/add_log_transaction_secured";

        public static string ADD_CONFIG_DOCUMENT = "Dictionary/add_config_document";
        public static string GET_LOAN_CONFIG_STEP = "Dictionary/get_loan_config_step";
        public static string GET_ALL_LOAN_CONFIG_STEP = "Dictionary/get_all_loan_config_step";
        public static string GET_ALL_INFOMATION_PRODUCT_DETAIL = "Dictionary/get_all_infomation_product_detail";
        public static string GET_INFOMATION_PRODUCT_DETAIL_BY_TYPE_OWNERSHIP = "Dictionary/get_infomation_product_detail_by_type_ownership?typeOwnership={0}";
        public static string GET_INFOMATION_PRODUCT_DETAIL_BY_TYPE_OWNERSHIP_AND_PRODUCT = "Dictionary/get_infomation_product_detail_by_type_ownership_and_product?typeOwnership={0}&productId={1}&typeLoanBrief={2}&groupId={3}";
        public static string GET_INFOMATION_PRODUCT_DETAIL_BY_PRODUCT_DETAIL_AND_PRODUCT = "Dictionary/get_infomation_product_detail_by_product_detail_and_product?productDetailId={0}&productId={1}";
        public static string GET_CONFIG_DEPARTMENT_ENPOINT = "Dictionary/get_config_department_status?departmentId={0}";
        public static string CHANGE_PHONE_ENPOINT = "Dictionary/change_phone";
        public static string GET_RELATIONSHIP_INFO_ENPOINT = "Dictionary/get_relationship_info?loanBriefId={0}&relationshipId={1}";
        public static string SAVE_COMPARE_DOCUMENT_ENDPOINT = "Dictionary/save_compare_document";
        public static string GET_LAST_COMPARE_DOCUMENT = "Dictionary/get_last_compare?loanBriefId={0}";
        public static string GET_LAST_LOGSECUREDTrANSACTION = "Dictionary/get_last_log_secured_transaction?loanBriefId={0}";
        public static string CHECK_COMPARE_FINISH_ENDPOINT = "Dictionary/check_compare_finish?loanBriefId={0}&groupId={1}";
        public static string GET_DOCUMENT_EXCEPTIONS = "Dictionary/get_document_exception";
        public static string SAVE_LOG_CHANGE_PHONE_ENDPOINT = "Dictionary/save_log_change_phone";

        public static string SEARCH_SCHEDULE = "LoanBrief/search_schedule";
        public static string GET_LIST_SCHEDULETIME_BY_LOAN = "LoanBrief/get_schedule_by_loan/{0}";
        public static string UPDATE_HUB_ID_ENDPOINT = "LoanBrief/update_hubid";

        public static string GROUP_ENDPOINT = "group";
        public static string GROUP_GET_ALL = "group/getall";
        public static string GROUP_GET_ENDPOINT = "group/{0}";
        public static string GROUP_UPDATE_ENDPOINT = "group/{0}";
        public static string GROUP_DELETE_ENDPOINT = "group/{0}";
        public static string GROUP_CHECK_DELETE_ENDPOINT = "group/check_delete?id={0}";
        public static string GROUP_GET_PERMISSION_ENDPOINT = "group/get_group_permission?groupId={0}";
        public static string GROUP_UPDATE_PERMISSION_ENDPOINT = "group/update_permission";
        public static string GROUP_GET_ALL_NOT_ADMINISTRATOR = "group/get_all_not_administrator";

        public static string MODULE_ENDPOINT = "module";
        public static string MODULE_GET_ALL = "module/getall";
        public static string MODULE_GET_ENDPOINT = "module/{0}";
        public static string MODULE_UPDATE_ENDPOINT = "module/{0}";
        public static string MODULE_DELETE_ENDPOINT = "module/{0}";
        public static string MODULE_CHECK_DELETE_ENDPOINT = "module/check_delete?id={0}";
        public static string MODULE_GET = "module/get?controller={0}&action={1}";
        public static string MODULE_COUNT_USER_ENDPOINT = "module/count_user?id={0}";
        public static string MODULE_GET_GROUP_ENDPOINT = "module/get_group?groupId={0}&applicationId={1}";
        public static string MODULE_BY_TYPE = "module/get_by_type?ismenu={0}&applicationId={1}";
        public static string MODULE_SAVE_PERMISSION_USER_API = "module/save_permission_user_api/{0}";
        public static string MODULE_SAVE_PERMISSION_GROUP_API = "module/save_permission_group_api/{0}";



        public static string USERSMMO_ENDPOINT = "UserMMO";
        public static string USERSMMO_GET_ENDPOINT = "UserMMO/{0}";
        public static string USERSMMO_GET_USERNAME_ENDPOINT = "UserMMO/getuser?username={0}";
        public static string USERSMMO_GET_PHONE_ENDPOINT = "UserMMO/GetByPhoneUser?phone={0}";
        public static string USERSMMO_UPDATE_ENDPOINT = "UserMMO/{0}";
        public static string USERSMMO_DELETE_ENDPOINT = "UserMMO/{0}";


        public static string REPORT_LOANBRIEF_REGISTER_BY_USERMMO_ENDPOINT = "Report/ReporLoanRegisterOfUserMmo";
        public static string REPORT_LOANBRIEF_SUCCESSFULLSOLD_BY_USERMMO_ENDPOINT = "Report/ReporLoanSuccessFullySoldMMO";
        public static string REPORT_APPROVE_EMPLOYEE_ENDPOINT = "Report/get_report_approve_employee?month={0}";
        public static string REPORT_ESIGN_CUSTOMER_ENDPOINT = "Report/get_report_esign_customer?fromDate={0}&toDate={1}";

        public static string RESULTEKYC_GET_BY_LOANBRIEFID_ENDPOINT = "ResultEkyc/get_ekyc_by_loanbrief?LoanBriefId={0}";
        public static string RESULTEKYC_GET_BY_ID_ENDPOINT = "ResultEkyc/{0}";
        public static string ADD_RESULTEKYC = "ResultEkyc/add";

        public static string GET_ALL_JOB_ENDPOINT { get; set; }
        public static string GET_JOB_ENDPOINT { get; set; }

        public static string GET_ALL_JOBTITLE_ENDPOINT { get; set; }
        public static string GET_JOBTITLE_ENDPOINT { get; set; }
        public static string GET_HUBTYPE_ENPOINT = "Dictionary/hubtypes";

        public static string ADD_LOANBRIEF_IMPORT_FILE_EXCEL = "loanbriefimportfileexcel/add";
        public static string GET_LOANBRIEF_IMPORT_FILE_EXCEL = "loanbriefimportfileexcel/search";
        public static string GET_DETAIL_LOANBRIEF_IMPORT_FILE_EXCEL = "loanbriefimportfileexcel/get_detail";

        public static string RE_ESIGN_CUSTOMER_ENPOINT = "LoanBriefV3/re_esign_customer";
        public static string GET_LOAN_BY_PHONE_CHANGE_ENPOINT = "LoanBriefV3/get_loan_by_phone_change?phone={0}";
        public static string GET_LOAN_TOOL_CHECK_CAVET_ENPOINT = "LoanBriefV3/get_loan_tool_check_cavet?search={0}";
        public static string SEARCH_PUSH_TO_CISCO_ENDPOINT = "LoanBriefV3/push_to_cisco_search";

        #region TELESALES API ENDPOINT
        public static string UPDATE_CALL_STATUS_ENDPOINT = "Telesales/update_call_status";
        public static string UPDATE_SCHEDULE_CALL_ENDPOINT = "Telesales/update_schedule_call";
        public static string GET_MISSED_CALLS_ENDPOINT = "Telesales/get_missed_calls";
        public static string GET_SCHEDULE_CALLS_ENDPOINT = "Telesales/get_scheduled_calls";
        public static string GET_CALLED_ENDPOINT = "Telesales/get_called";
        public static string BOUND_TELESALE_ENDPOINT = "Telesales/bound_telesale";
        public static string GET_TELESALE_STAFF = "Telesales/get_staff";
        public static string GET_LIST_TELESALES_SHIFT = "telesales/get_list_telesales_shift";
        public static string GET_LIST_TEAM_TELESALES = "telesales/get_list_team_telesales";
        public static string GET_TELESALES_BY_TEAM = "telesales/get_telesales_by_team?teamTelesales={0}";
        public static string GET_TELESALES_TEAM_ENDPOINT = "telesales/get_team?teamTelesaleId={0}";
        #endregion

        #region LOANBRIEF API ENDPOINT
        public static string SEARCH_LOANBRIEF_ENDPOINT = "LoanBrief/Search";
        public static string REMARKETING_SEARCH_ENDPOINT = "LoanBrief/remarketing_search";
        public static string LOANBRIEF_GET_BYPHONE_ENDPOINT = "LoanBrief/get_loanbrief?Phone={0}";
        public static string LOANBRIEF_GET_BY_PHONE_OR_NATIONALCARD_ENDPOINT = "LoanBrief/get_loanbrief_by_phone_nationalcard?text_search={0}";
        public static string LOANBRIEF_GET_ENDPOINT = "LoanBrief/{0}";
        public static string LOANBRIEF_DETAIL_ENDPOINT = "LoanBrief/detail/{0}";
        public static string LOANBRIEF_FOR_DETAIL_ENDPOINT = "LoanBrief/for_detail/{0}";
        public static string LOANBRIEF_INIT_ENDPOINT = "LoanBrief/init_loanbrief";
        public static string LOANBRIEF_ADD_TOPUP = "LoanBrief/add_loan_topup";
        public static string LOANCANCELL_SEARCH_ENDPOINT = "LoanBrief/loancancel_search";
        public static string LOANBRIEF_UPDATE_ENDPOINT = "LoanBrief/init_loanbrief/{0}";
        public static string LOANBRIEF_UPDATE_SCRIPT_ENDPOINT = "LoanBrief/update_loanbrief_script/{0}";
        public static string LOANBRIEF_UPDATE_SCRIPTV2_ENDPOINT = "LoanBrief/update_loanbrief_scriptv2/{0}";
        public static string LOANBRIEF_UPDATE_FOR_REQUEST_AI_ENDPOINT = "LoanBrief/save_request_ai/{0}";
        public static string LOANBRIEF_ADD_NOTE_ENDPOINT = "LoanBrief/add_note";
        public static string LOANBRIEF_ADD_LOG_ENDPOINT = "LoanBrief/add_log";
        public static string PRODUCT_CURRENT_PRICE_ENDPOINT = "Product/current_price?id={0}";
        public static string PRODUCT_CHECK_FULLNAME = "Product/check_fullname?fullname={0}";
        public static string PRODUCT_GET_PRODUCT_BY_BRAND = "Product/get_product_by_brand?brandId={0}";
        public static string PRODUCT_GET_PRODUCT = "Product/get_product?productId={0}";
        public static string PRODUCT_GET_BRAND = "Product/get_brand?brandId={0}";
        public static string PRODUCT_GET_PRODUCT_REVIEW = "Product/get_product_review?loanbriefId={0}&productId={1}&IdType={2}&price={3}";
        public static string INSERT_PRODUCT_REVIEW_RESULT = "Product/insert_product_review_result";
        public static string LOANBRIEF_UPDATE_STATUS_ENDPOINT = "LoanBrief/update_status";
        public static string GET_LOANBRIEF_BYTELESALE_ENDPOINT = "LoanBrief/get_telesaleid?TelesaleId={0}";
        public static string LOANBRIEF_PUSH_ENDPOINT = "LoanBrief/push";
        public static string LOANBRIEF_RETURN_ENDPOINT = "LoanBrief/return";
        public static string LOANBRIEF_CANCEL_ENDPOINT = "LoanBrief/cancel";
        public static string LOANBRIEF_SCHEDULE_ENDPOINT = "LoanBrief/get_loanbrief_schedule?telesale={0}&dateTime={1}";
        public static string LOANBRIEF_HANDLE_ENDPOINT = "LoanBrief/get_loanbrief_handle?telesale={0}&dateTime={1}";
        public static string LOANBRIEF_GET_PROCESSING_BYPHONE_ENDPOINT = "LoanBrief/get_loanbrief_processing_by_phone?Phone={0}";
        public static string LOANBRIEF_GET_PROCESSING_ENDPOINT = "LoanBrief/get_loanbrief_processing?Phone={0}&NationalCard={1}&LoanBriefId={2}";
        public static string LOANBRIEF_DATA_FOR_HUB_ENDPOINT = "LoanBrief/get_data_hub";
        public static string LOANBRIEF_ADD_FILE_ENDPOINT = "LoanBrief/add_file";
        public static string LOANBRIEF_GET_FILE_ENDPOINT = "LoanBrief/get_file?LoanBriefId={0}&documentType={1}";
        public static string LOANBRIEF_GET_FILE_BY_LOANID_ENDPOINT = "LoanBrief/file_by_id?loanBriefId={0}";
        public static string LOANBRIEF_GET_FILE_BY_LIST_LOANID_ENDPOINT = "LoanBrief/file_by_list_id?loanbriefId={0}";
        public static string LOANBRIEF_GET_LOANBRIEFID_FOR_REASON_ENDPOINT = "LoanBrief/get_loanbief_for_reason?loanbriefId={0}";
        public static string LOANBRIEF_CALCULATE_TIME_PROCESSING_LOANBRIEF = "LoanBrief/calculate_time_processing_loanbrief?loanbriefId={0}&groupId={1}";
        public static string LOANBRIEF_DELETE_FILE_ENDPOINT = "LoanBrief/delete_file/{0}";
        public static string LOANBRIEF_UPDATE_DEVICE_ENDPOINT = "LoanBrief/update_device";
        public static string LOANBRIEF_UPDATE_STATUS_DEVICE_ENDPOINT = "LoanBrief/update_status_device";
        public static string LOANBRIEF_REMOVE_FILE_ENDPOINT = "LoanBrief/delete_file";
        public static string LOANBRIEF_RE_ACTIVE_DEVICE_ENDPOINT = "LoanBrief/re_active_device";
        public static string LOANBRIEF_GET_BY_CODEID = "LoanBrief/get_loanbrief_by_codeid?codeId={0}";
        public static string LOAN_DEPARTMENT_ENDPOINT = "LoanBrief/get_loan_department";
        public static string LOANBRIEF_GET_DATA_INTERNALCONTROL_ENDPOINT = "LoanBrief/internalControl_search";
        public static string LOANBRIEF_CHECK_TOPUP = "LoanBrief/check_loan_topup?nationalCard={0}&phone={1}&loanbriefId={2}";
        public static string LOANBRIEF_GET_BORROWING = "LoanBrief/loanbrief_get_borrowing?nationalCard={0}&phone={1}";
        public static string LOANBRIEF_DISBURSED = "LoanBrief/loanbrief_disbursed?nationalCard={0}&phone={1}";
        public static string LOANBRIEF_GET_LIST_TOPUP_CAR = "LoanBrief/loanbrief_get_list_topup_car?customerId={0}";
        public static string LOANBRIEF_GET_CLICK_TOP_TLS = "LoanBrief/loanbrief_get_click_top_tls";
        public static string LOANBRIEF_GET_BY_NATIONAL_CARD = "LoanBrief/loanbrief_get_by_national_card?nationalCard={0}&loanbriefId={1}";
        public static string GET_LOG_DISTRIBUTION_USER = "LoanBrief/get_log_distribution_user";
        public static string LOANBRIEF_CHANGE_SUPPORT_ALL_CLICK_TOP_TLS = "LoanBrief/loanbrief_change_support_all_click_top_tls";
        public static string LOANBRIEF_CHANGE_SUPPORT_ALL = "LoanBrief/loanbrief_change_support_all";

        public static string SEARCH_LOANBRIEF_ALL = "LoanBrief/search_loan_all";
        public static string LOANBRIEF_RE_GET_LOCATION_ENDPOINT = "LoanBrief/re_get_location";

        public static string ADD_RELOAN = "LoanBrief/add_reloan";

        public static string LOANBRIEF_COUNTLOAN_TLS_ENDPOINT = "LoanBrief/count_loan_tls?TelesaleId={0}&teamTelesales={1}";

        public static string LOANBRIEF_UPDATE_COUNTCALL = "LoanBrief/update_countcall?loanBriefId={0}";
        public static string LOANBRIEF_GET_LOAN_CANCEL_MOMO_ENDPOINT = "LoanBrief/get_loan_cancel_momo";
        public static string LOANBRIEF_GET_LOAN_TRANSACTION_SECURED_ENDPOINT = "LoanBrief/get_loan_transaction_secured?loanBriefId={0}";

        public static string LOANBRIEFV2_UPDATE_COUNTCALL = "LoanBriefV2/update_countcall";

        public static string LOANBRIEF_UPDATE_FOR_REFPHONE_ISNULL_AI_ENDPOINT = "LoanBrief/save_refphone_isnull_ai/";
        public static string LOANBRIEF_UPDATE_FIRST_HUB_FEEDBACK_ENDPOINT = "LoanBrief/update_first_hub_feedback";
        public static string LOANBRIEF_UPDATE_FIRST_TELESALE_FEEDBACK_ENDPOINT = "LoanBrief/update_first_telesale_feedback";
        public static string LOANBRIEF_UPDATE_LAST_TELESALE_FEEDBACK_ENDPOINT = "LoanBrief/update_last_telesale_feedback";
        public static string LOANBRIEF_UPDATE_FIRST_STAFF_HUB_FEEDBACK_ENDPOINT = "LoanBrief/update_first_hub_feedback";
        public static string LOANBRIEF_UPDATE_HUB_EMPLOYEE_CALL_FIRST_ENDPOINT = "LoanBrief/update_hub_employee_call_first";
        public static string LOANBRIEF_UPDATE_TELESALES_FIRST_PROCESSING_TIME_ENDPOINT = "LoanBrief/update_telesales_first_processing_time";
        public static string LOANBRIEF_UPDATE_REMARKETING_ENDPOINT = "LoanBrief/update_remarketing_loanbriefid";

        public static string LOANBRIEF_UPDATE_CAVET = "LoanBrief/update_cavet?loanbriefId={0}";
        public static string LOANBRIEF_PROPERTY_GET_ENDPOINT = "LoanbriefV2/property?loanbriefId={0}";
        public static string LOANBRIEF_ADD_HISTORY_ENDPOINT = "LoanbriefV2/add_loanbrief_history";
        public static string LOANBRIEF_INIT_V2_ENDPOINT = "LoanbriefV2/init_loanbrief";
        public static string LOANBRIEF_UPDATE_V2_ENDPOINT = "LoanbriefV2/init_loanbrief/{0}";
        public static string LOANBRIEF_UPDATE_SCRIPT_TELESALE_V2_ENDPOINT = "LoanbriefV2/script_loanbrief/{0}";
        public static string LOANBRIEF_CREATE_SCRIPT_TELESALE_V2_ENDPOINT = "LoanbriefV2/create_loanbrief_script";


        public static string LOANBRIEF_REQUEST_AI_V3 = "LoanBriefV3/request_ai_v3/{0}";
        public static string LOANBRIEF_REQUEST_REFPHONE_V3 = "LoanBriefV3/request_refphone_v3/{0}";
        public static string LOANBRIEF_UPDATE_SCRIPT_TELESALE_V3_ENDPOINT = "LoanBriefV3/script_loanbrief/{0}";
        public static string LOANBRIEF_HOUSEHOLD_UPDATE_JSONINFOFAMILYKALAPA = "LoanBrief/update_json_info_familykalapa/{0}";
        public static string LOANBRIEF_CREATE_SCRIPT_TELESALE_V3_ENDPOINT = "LoanbriefV3/create_loanbrief_script";
        public static string LOANBRIEF_CREATE_CHILD_ENDPOINT = "LoanbriefV3/create_loanbrief_child";
        public static string LOANBRIEF_GET_LAT_LNG = "LoanBriefV3/getlatlng?loanbriefId={0}&typeGetLatLng={1}";

        public static string LOANBRIEF_INIT_DEBTREVOLVINGLOAN_ENDPOINT = "LoanbriefV3/init_debtRevolving_loan";
        public static string ADD_LOG_PUSH_TO_CISCO_ENDPOINT = "LoanbriefV3/push_to_cisco_create";

        public static string LOANBRIEF_COUNT_APPROVEID_ENDPOINT = "LoanBrief/countloan_approve?approveId={0}";
        public static string LOANBRIEF_UPDATE_LABEL_REBORROW = "LoanBrief/update_label_reborrow";

        public static string LOANBRIEF_UPDATE_EXCEPTIONS = "LoanBrief/update_exceptions";

        public static string LOANBRIEF_ADD_LOG_CHANGE_BOUNDTELESALE_V2_ENDPOINT = "LoanbriefV2/add_log_change_boundtelesale";

        public static string SEARCH_REMARKETINGCAR_ENDPOINT = "LoanBrief/SearchRemarketingCar";

        public static string LOANBRIEF_FOR_APPRAISER_ENDPOINT = "LoanBriefV3/detail_for_appraiser/{0}";

        public static string LOANBRIEF_RELATIONSHIP_GET_ENDPOINT = "LoanbriefV2/relationship?loanbriefId={0}";

        public static string PRODUCT_GET_PRODUCT_REVIEW_V2 = "Product/get_product_review_v2?loanbriefId={0}&productId={1}&productTypeId={2}&price={3}";

        public static string LOANBRIEF_SEARCH_ENDPOINT = "LoanBriefV3/loanbrief_search";

        public static string LOANBRIEF_GET_BASIC_INFO_ENDPOINT = "LoanBriefV3/basic_info?loanbriefId={0}";
        public static string CHECK_DEBTREVOlVING_LOAN = "LoanBriefV3/check_debt_revolving?loanBriefId={0}";
        public static string CHECK_LOAN_PROCESSING = "LoanBriefV3/check_loan_processing?phone={0}";
        public static string CHECK_LOAN_DISPLAY_DOC_HUB_EMP = "LoanBriefV3/check_loan_display_doc_hub_emp?phone={0}&nationalCard={1}";

        public static string LOANBRIEF_TOTAL_LOAN_DISBURSEMENT_IN_MONTH = "LoanBriefV3/total_loan_disbursement_in_month/{0}";

        public static string LOANBRIEF_ADD_LOG_DISTRIBUTION_USER_ENDPOINT = "LoanbriefV2/add_log_distribution_user";

        public static string LOANBRIEF_GET_CONTRACT_FINANCIAL_AGREEMENT = "LoanBriefV3/loanbrief_get_contract_financial_agreement?loanbriefId={0}";
        public static string LOANBRIEF_GET_COMMITTED_PAPER = "LoanBriefV3/loanbrief_get_committed_paper?loanbriefId={0}";
        public static string LOANBRIEF_GET_INFO_COMPARE_ENDPOINT = "LoanBrief/compare_info?loanbriefId={0}";
        public static string LOANBRIEF_GET_FILE_BY_DOCUMENT = "LoanBrief/get_file_by_document_multi?loanbriefId={0}&documentId={1}";

        public static string LOANBRIEF_ADD_RANGE_NOTE_ENDPOINT = "LoanBrief/add_range_note";

        public static string LOANBRIEF_ADD_RANGE_LOG_DISTRIBUTION_USER_ENDPOINT = "LoanbriefV2/add_range_log_distribution_user";

        public static string RANGE_BOUND_TELESALE_ID_ENDPOINT = "LoanBrief/update_range_telesale";
        public static string CHECK_LOAN_ASSIGN_ENDPONT = "LoanBriefV3/check_loan_assign?loanBriefId={0}&phone={1}&userId={2}";

        //Exceptions
        public static string LOANBRIEF_GET_LIST_EXCEPTIONS = "LoanBrief/get_list_exception?parentId={0}";
        public static string LOANBRIEF_GET_PROPOSE_EXCEPTION = "LoanBrief/get_propose_exception?loanBriefId={0}";
        public static string ADD_PROPOSE_EXCEPTIONS = "LoanBrief/add_propose_exceptions";

        public static string GET_TOTAL_LOAN_AMOUNT_SECURED= "LoanBrief/get_total_loan_amount_secured?customerId={0}";

        public static string LOANBRIEF_UPDATE_TRANSACTION_STATE = "LoanBrief/update_transaction_state";

        #endregion

        #region BOD
        public static string BOD_SEARCH = "BOD/search";
        public static string BUSINESSMANAGER_SEARCH = "BOD/business_manager_search";
        #endregion

        #region APPROVED API ENDPOINT
        public static string SEARCH_APPROVE_ENDPOINT = "Approve/search";
        public static string SEARCH_APPROVE_EMP_ENDPOINT = "Approve/search_emp";
        public static string SEARCH_DISBURSEMENAFTER_ENDPOINT = "Approve/disbursement_after";
        public static string CONFIRM_LOAN_MONEY_ENDPOINT = "Approve/confirm_loan_money";

        #endregion

        #region
        public static string LOANBRIEF_CLONE_ENDPOINT = "LoanBrief/create_remarketing";
        #endregion

        #region LOANBRIEFNOTE API ENDPOINT
        public static string GET_LOANBRIEFNOTE_BYLOANID_ENDPOINT = "LoanBriefNote/get_notebyloanid?loanBriefId={0}";
        public static string CREATE_LOANNOTE_ENDPOINT = "LoanBriefNote/postloannote";
        #endregion

        #region pipeline
        public static string GET_ALL_PIPELINE_ENDPOINT = "pipeline";
        public static string GET_PIPELINE_BY_ID_ENDPOINT = "pipeline/{0}";
        public static string CREATE_PIPELINE_ENDPOINT = "pipeline";
        public static string UPDATE_PIPELINE_ENDPOINT = "pipeline/{0}";
        public static string CREATE_SECTION_ENDPOINT = "section";
        public static string UPDATE_SECTION_ENDPOINT = "section/{0}";
        public static string DELETE_SECTION_ENDPOINT = "section/{0}";
        public static string DELETE_PIPELINE_ENDPOINT = "pipeline/{0}";
        public static string CHANGE_STATUS_PIPELINE_ENDPOINT = "pipeline/{0}/change_status";
        public static string DELETE_PIPELINE_SECTION_ENDPOINT = "section/{0}";
        public static string GET_ALL_SECTION_ENDPOINT = "section";
        public static string GET_SECTION_BY_ID_ENDPOINT = "section/{0}";
        public static string GET_ALL_PROPERTY_ENDPOINT = "property";
        public static string GET_PROPERTY_BY_ID_ENDPOINT = "property/{0}";
        public static string CREATE_SECTION_DETAIL_ENDPOINT = "section/{0}/details";
        public static string UPDATE_SECTION_DETAIL_ENDPOINT = "section/{0}/details/{1}";
        public static string DELETE_SECTION_DETAIL_ENDPOINT = "section/{0}/details/{1}";
        public static string GET_SECTION_DETAIL_ENDPOINT = "section/detail/{0}";
        public static string UPDATE_SECTION_ORDER_ENDPOINT = "pipeline/orders";
        public static string CHANGE_PIPELINE_ENDPOINT = "pipeline/change_pipeline";
        #endregion

        public static string GET_ALL_POSITION_ENDPOINT { get; set; }
        public static string GET_POSITION_ENDPOINT { get; set; }
        public static string GET_MKTREPORT_ENDPOINT { get; set; }


        #region TlsScenarioConfig
        public static string MAIN_TLSSCENARIO = "TlsScenario";
        public static string GETALL_TLSSCENARIO = "TlsScenario/GetAll";

        public static string INSERT_TLSSCENARIO = "TlsScenario/InsertAsync";
        public static string UPDATE_TLSSCENARIO = "TlsScenario/UpdateAsync";
        #endregion
        #region TlsQuestionConfig
        public static string MAIN_TLSQUESTIION = "TlsQuestion";
        public static string GETALL_TLSQUESTIION = "TlsQuestion/GetAll";
        public static string INSERT_TLSQUESTIION = "TlsQuestion/InsertAsync";
        public static string UPDATE_TLSQUESTIION = "TlsQuestion/UpdateAsync";
        #endregion
        #region ScenarioQuestion
        public static string MAPPING_TLSSCENARIOQUESTION = "TlsScenarioQuestion/CreateMappingQuestionWithScenarioAsync";
        public static string GETQUESTIONIDS_TLSSCENARIOQUESTION = "TlsScenarioQuestion/GetQuestionIdsByScenarioIdAsync/{0}";
        #endregion

        #region REASON CANCEL API ENDPOINT
        public static string GET_LIST_REASON_GROUP_ENDPOINT = "Reason/list_reason_group?reasonType={0}";
        public static string GET_LIST_REASON_DETAIL_ENDPOINT = "Reason/list_reason_detail?parentId={0}";
        public static string GET_DAY_IN_BLACK_LIST = "Reason/get_day_in_black_list?id={0}";
        #endregion

        #region STATUS TELESALES API ENDPOINT
        public static string GET_LIST_LOAN_STATUS_DETAIL_ENDPOINT = "LoanStatusDetail/list_status_detail?typeId={0}";
        public static string GET_LIST_LOAN_STATUS_CHILD_DETAIL_ENDPOINT = "LoanStatusDetail/list_status_child?parentId={0}";
        public static string GET_LIST_STATUS_TELESALES_DETAIL_ALL_ENDPOINT = "LoanStatusDetail/list_status_telesales_detail_all";

        #endregion


        #region Token Smartdailer
        public static string SMARTDAILER_LAST_TOKEN_ENDPOINT = "TokenSmartDailer/get_last_token?userId={0}";
        public static string SMARTDAILER_ADD_TOKEN_ENDPOINT = "TokenSmartDailer/add";
        public static string LOG_WORKING_ADD_ENDPOINT = "LogWorking/add";
        #endregion

        #region HUB

        public static string USERS_GET_STAFF_HUB = "users/get_staff_hub?hubId={0}";
        public static string USERS_GET_MANAGER_HUB = "users/get_manager_hub?hubId={0}";
        public static string USERS_GET_MULTI_USER = "users/get_multi_user?group1={0}&group2={1}";
        public static string LOANBRIEF_UPDATE_HUB_ENDPOINT = "LoanBrief/update_hub";
        public static string LOANBRIEF_DISTRIBUTING_STAFF_HUB_ENDPOINT = "LoanBrief/distributing_staff_hub";
        public static string LOANBRIEF_CHANGE_EMPLOYEE_IN_HUB_ENDPOINT = "LoanBriefV2/hub_employee_change";
        public static string GET_LOAN_FROM_ID_STATUS_ENDPOINT = "LoanBriefV2/list_loan_from_id_status?loanbriefIds={0}&Status={1}";
        public static string LOANBRIEF_CHANGE_HUB_ENDPOINT = "LoanBriefV2/change_hub";
        public static string LOANBRIEF_CHANGE_HUB_EMPLOYEE_ENDPOINT = "LoanBriefV2/change_hub_employee";

        public static string GET_LOAN_BY_CODEID_ENDPOINT = "LoanBrief/list_loan_by_codeid";

        public static string USERS_FIELD_HO = "users/get_field_ho?provinceId={0}";
        #endregion

        #region Shop
        public static string SHOP_GET_ALL_HUB_ENPOINT = "shop/get_hubs";
        public static string SHOP_GET_BY_ID_ENPOINT = "shop/{0}";
        #endregion

        #region Token
        public static string TOKEN_GET_LAST_ENPOINT = "AccessToken/get_last_token?app_id={0}";
        public static string TOKEN_ADD_ENPOINT = "AccessToken/add_token";
        #endregion

        #region AI
        public static string APP_ID_SEARCH = "data_search1";
        public static string APP_KEY_SEARCH = "J;Qk{xn){Yx-h5GH";
        public static string APP_ID_VAY_SIEU_NHANH = "service_ai_los";
        public static string APP_KEY_VAY_SIEU_NHANH = "GdJszp99aFYGAtUXOfSu";

        public static string APP_ID_TIMA_IT3 = "tima_it3";
        public static string APP_KEY_TIMA_IT3 = "AM59NAXm6NdUo5lDmq0u";

        public static string AI_AUTHENTICATION_ENPOINT = "/security/authentication";
        public static string gps_interval = "/gps-device/tracking/gps-interval";
        public static string open_contract = "/gps-device/tracking/open-contract";
        public static string close_contract = "/gps-device/tracking/close-contract";
        public static string gps_stop_point = "/gps-device/tracking/gps-stop-point";
        public static string check_imei = "/gps-device/tracking/infor-by-imei/";
        public static string update_contract = "/gps-device/tracking/update-by-imei";
        public static string verify_address = "/cac/verify/address";
        public static string info_address = "/cac/verify/address/{0}";
        public static string get_first_location = "/gps-device/tracking/gps-last-location";
        public static string get_report_address = "/gps-device/tracking/gps-report-address";
        public static string get_report_location = "/gps-device/tracking/gps-report-location";
        public static string re_active_imei = "/gps-device/tracking/retry-check-imei";

        public static string FaceSearch = "/computer_vision/image/face/search";
        public static string GetLoanByPhone = "/product/tima/loans/customerPhone/";
        public static string GetCustomerByPhone = "/product/tima/customers/phone/";
        public static string GET_MOTO_PRICE = "/moto-price/get_price";
        public static string SAVE_LOG_PRICE_MOTO_AI = "/moto-price-report/save-log";

        public static string GET_INFO_CHECK_EKYC = "/computer-vision/ekyc/v2";
        public static string GET_PRODUCT = "Product/get_product_all";
        public static string PRODUCT_UPDATE = "product/{0}";
        public static string PRODUCT_CREATE = "product";

        #endregion

        #region LMS
        public static string TokenLms = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiMiIsIklkZW50aXR5X1VzZXJJRCI6IjIiLCJuYmYiOjE2MzcwNTM2MzksImV4cCI6MTczMTY2MTYzOSwiaWF0IjoxNjM3MDUzNjM5fQ.8WfKdetnTk5kxAbhu4wX-pC8F7IPlygT0B67hRjIJvc";
        public static string DeferredPayment = "/api/S2S/CheckCustomerDeferredPayment";
        public static string ListLender = "/api/s2s/GetLenderInfosByCondition";
        public static string InterestAndInsurence = "/api/s2s/GetPaymentSchedulePresume";
        public static string SendSMS = "/api/SMSBrandName/SendSMSBrandNameFPT";
        public static string CheckReLoan = "/api/S2S/CheckCustomerReBorrow";
        public static string CalculatorMoneyInsurance = "/api/loan/CalculatorMoneyInsurance";
        public static string CheckBlackList = "/api/S2S/CheckBlacklistLOS";
        public static string GetPaymentById = "/api/Loan/GetPaymentLoanAPIForLos?LoanId={0}";
        public static string InterestTopup = "/api/Loan/GetPaymentTopUpLoanAPIForLos";
        public static string GetPayAmountForCustomer = "/api/s2s/getpayamountforcustomer";
        public static string Kalapa_GetFamily = "/api/Kapala/GetFamily?cardNumber={0}";
        public static string ProxyGetInsuranceInfo = "/api/Trandata/GetSiByInfo?nationalId1={0}&nationalId2={1}&fullName={2}&dob={3}";
        public static string GetReportDebt = "/api/Report/ReportDebt";
        public static string GetPayAmountForCustomerByLoanID = "/api/s2s/GetPayAmountForCustomerByLoanID";
        public static string GetListLoanHubDebt = "/api/LoanCredit/InformationLoan";
        public static string AddBlacklist = "/api/BlackList/InsertBlackList";
        public static string DebtRestructuring = "/api/Loan/DebtRestructuring";
        public static string CloseLoan = "/loan/api/loan/CreateRequestCloseLoan";

        #endregion ERP
        public static string CheckEmployeeTima = "/api/api/userStatus";
        public static string CheckDeviceErp = "/api/api/productStatus";
        public static string get_employee_hub_checkin = "/api/api/userCheckedIn/{0}";
        public static string CheckCavet = "/api/requests/requestLicense?loanId={0}";
        #region

        #endregion
        #region SSO
        public static string LoginSSO = "/api/v1.0/Users/Authen";
        public static string CreateUserSSO = "/api/v1.0/User/create";
        public static string UpdatePasswordUserSSO = "/api/v1.0/User/reset_pass";
        public static string GenNewOtpSSO = "/api/v1.0/User/gen_new_otp?text={0}";
        public static string TokenSSO = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZmYiOiJhY2Nlc3N0cmFkZSJ9.fVKdW5bWSNSq6j4dthw2OBioVsQhHxaDgF_sinZEGPo";
        #endregion

        #region APP API LOS
        public static string GetRecordingByUser = "/api/v1.0/recording/get_recording_by_user";
        public static string GetF2LTelesales = "/api/v1.0/reportLos/f2l_telesale";
        #endregion

        #region Cisco
        public static string CISCO_AUTHORIZE = "/api/rest/authorize";
        public static string CISCO_EXPORTINGRECORDINGS = "/api/rest/recording/contact/{0}/export";
        public static string CISCO_EXPORTINGDETAILS = "/api/rest/recording/contact/{0}/export/{1}";
        public static string CISCO_GETLIST = "/api/rest/recording/contact?beginTime={0}";
        public static string CISCO_GETLIST_PHONE = "/api/rest/recording/contact?number={0}&beginTime={1}&endTime={2}";
        public static string CISCO_GETLIST_TIME = "/api/rest/recording/contact?beginTime={0}&endTime={1}";
        public static string CISCO_GETLIST_BY_TIME_V2 = "/api/v1.0/Recording/get_recording?phoneNumber={0}&fromDate={1}&toDate={2}";
        public static string CISCO_GETLIST_BY_LIST_PHONE_TIME_V2 = "/api/v1.0/Recording/get_recording_phones?fromDate={0}&toDate={1}";

        public static string CISCO_ID = "recording";
        public static string CISCO_USERID = "ccadmin";
        public static string CISCO_PASSWORD = "Tima@2020";
        public static string CISCO_DOMAIN = "tima.vn";

        public static string PUSH_TO_CISCO = "/api/Cisco/ImportContactCampaign";

        #endregion
        public static string GET_LIST_MANAGER_AREA_HUB_ENDPOINT = "ManagerAreaHub/GetListManagerAreaHub?cityid={0}&hubid={1}&procductid={2}";
        public static string MANAGER_AREA_HUB_ENDPOINT = "ManagerAreaHub";

        public static string CONSTANTS_VERSION_JS = "6.85";

        public static string LOG_REQUEST_AI_ENDPOINT = "LogRequestAi";
        public static string UPDATE_LOG_REQUEST_AI_ENDPOINT = "LogRequestAi/update?id={0}";
        public static string LOG_REQUEST_V2_AI_ENDPOINT = "LogRequestAi/create_v2";
        public static string LOG_REQUEST_MULT_AI_ENDPOINT = "LogRequestAi/adds";
        public static string LOG_GET_LOANBRIEF_ENDPOINT = "LogRequestAi/get_by_loanbrief?loanbriefId={0}";
        public static string UpdateStatusDeviceToERP = "/api/api/updateProductStatus";


        public static string LOG_LOAN_AI_SEARCH_ENDPOINT = "LogLoanInfoAi/search?loanbriefId={0}&serviceType={1}";
        public static string LOG_LOAN_AI_BY_SERVICETYPE_ENDPOINT = "LogLoanInfoAi/getbyservicetype?loanbriefId={0}&serviceType={1}";
        public static string LOG_LOAN_AI_GET_ENDPOINT = "LogLoanInfoAi/get?Id={0}";
        public static string LOG_LOAN_AI_BY_LST_SERVICETYPE_ENDPOINT = "LogLoanInfoAi/get_by_list_servicetype?loanbriefId={0}&serviceType={1}";
        public static string LOG_CALL_API_SEARCH_ANDPOINT = "LogCallApi/search?loanbriefId={0}&actionCallApi={1}";
        public static string LOG_CALL_API_GET_LAST_ANDPOINT = "LogCallApi/get_last?loanbriefId={0}&actionCallApi={1}";
        public static string LOG_CALL_API_GET_ENDPOINT = "LogCallApi/get?Id={0}";
        public static string CHECK_BANK_ENDPOINT = "/tools/api/v1/check_bank?bank={0}&bank_account={1}";
        public static string CHECK_BILL_ELECTRICITY = "/tools/api/v1/check_bill_electricity?bill_id={0}";
        public static string CHECK_BILL_WATER = "/tools/api/v1/check_bill_water?bill_id={0}&water_supplier={1}";
        public static string LOANBRIEF_SAVE_STATUS_TELESALES = "LoanBrief/save_status_telesales/";
        public static string LOANBRIEF_RE_INIT_ENDPOINT = "LoanBrief/re_init_loanbrief";
        public static string LOANBRIEF_SAVE_NEXT_STEP = "LoanBrief/save_next_step";
        public static string LOANBRIEF_SAVE_CHANGE_STATUS_APPROVER = "LoanBrief/save_change_status_approver";
        public static string LOANBRIEF_FEEDBACK_LOANBRIEF_ENDPOINT = "LoanBrief/update_feedback";

        public static string LOANBRIEF_TOOL_CHANGE_PIPELINE_ENDPOINT = "LoanBrief/tool_change_pipeline";
        public static string TOOL_GET_LOG_SEND_OTP_ENDPOINT = "Dictionary/get_log_send_otp?search={0}&type={1}";

        public static string webhook_location_viettel = "/api/v1.0/webhook/location_viettel";
        public static string webhook_location_mobi = "/api/v1.0/webhook/location_mobi";

        public static string CHECK_LOAN_MOMO = "/amazing-crawler/api/v1/momo/check_loan/{0}";

        public static string REGISTER_SECURED_TRANSACTION_ENDPOINT = "/auto-secured-transaction/register";
        public static string CHECK_STATUS_SECURED_TRANSACTION_ENDPOINT = "/auto-secured-transaction/get-info?notice_number={0}";

        #region Quản lý danh mục chứng từ
        public static string DOCUMENTTYPE_GET_ENPOINT = "DocumentType/get_all";
        public static string DOCUMENTTYPE_GET_PARENT = "DocumentType/get_parent?productId={0}";
        public static string DOCUMENTTYPE_GET_BY_ID = "DocumentType/{0}";
        public static string DOCUMENTTYPE_UPDATE_ENDPOINT = "DocumentType/{0}";
        public static string DOCUMENTTYPE_CREATE_ENDPOINT = "DocumentType";
        public static string DOCUMENTTYPE_GET_ENABLE = "DocumentType/get_all_enable";
        public static string DOCUMENTTYPE_GET_BY_PRODUCT_ENDPOINT = "DocumentType/get?productId={0}&typeOwnerShip={1}";
        #endregion
        #region Contact customer
        public static string ContactCustomer_ENDPOINT = "ContactCustomer/GetAllContactCustomer";
        public static string ContactCustomer_ADD_ENDPOINT = "ContactCustomer/add_contactcustomer";
        public static string ContactCustomer_GET_ENDPOINT = "ContactCustomer/getcontactcustomer?id={0}";
        public static string ContactCustomerByPhone_GET_ENDPOINT = "ContactCustomer/getcontactcustomerbyphone?phone={0}";
        //public static string ContactCustomer_UPDATE_ENDPOINT = "ContactCustomer/update/{0}";
        public static string ContactCustomer_UPDATE_ENDPOINT = "ContactCustomer/{0}";
        #endregion

        #region remarketing

        public static string REMARKETING_GET_DATA_ENDPOINT = "Remarketing/get_data";
        public static string REMARKETING_RE_LOANBRIEF_ENDPOINT = "Remarketing/remarketing_loanbrief";
        public static string REMARKETING_GET_DATA_ALL_ENDPOINT = "Remarketing/get_data_all";

        #endregion


        public static int DISTANCE_HOME_DOCUMENT_LIMITED = 200;
        public static int DISTANCE_COMAPANY_DOCUMENT_LIMITED = 200;
        public static int TIME_DOCUMENT_LIMITED = 60;

        public static int REDIS_DB_CACHE = 1;
        public static string REDIS_KEY_PROVINCE_CACHE = "redis_data_province";
        public static string REDIS_KEY_DISTRICT_CACHE = "redis_data_district";
        public static string REDIS_KEY_WARD_CACHE = "redis_data_ward";
        public static string REDIS_KEY_JOB_CACHE = "redis_data_job";
        public static string REDIS_KEY_RELATIONSHIP_CACHE = "redis_data_relationship";
        public static string REDIS_KEY_BRAND_PRODUCT_CACHE = "redis_data_brand_product";
        public static string REDIS_KEY_PRODUCT_CACHE = "redis_data_product";
        public static string REDIS_KEY_PLATFORM_CACHE = "redis_data_platform";
        public static string REDIS_KEY_BANK_CACHE = "redis_data_bank";
        public static string REDIS_KEY_OWNERSHIP_CACHE = "redis_data_ownership";
        public static void init()
        {
            GET_JOBTITLE_ENDPOINT = WEB_API_URL + "jobTitle/{0}";
            GET_ALL_POSITION_ENDPOINT = WEB_API_URL + "position";
            GET_POSITION_ENDPOINT = WEB_API_URL + "position/{0}";
            GET_MKTREPORT_ENDPOINT = WEB_API_URL + "marketing/report";
        }

        #region Log Loan action
        public static string log_loan_action_add = "LogLoanAction/add";
        public static string log_action_restruct_add = "LogLoanAction/add_log_restruct";
        #endregion
    }

    public class ConstantCareSoft
    {
        public static string TokenSystem = "fIsfcZ3Ze_X9710IZH";
        public static string BaseUrl = "https://tongdai.tima.vn/";
        public static string LoginCareSoft = "tima/thirdParty/login";
        public static string PushPhoneToCampaign = "tima/api/v1/campaign/dataPhone";
        public static string GetAllAgents = "tima/api/v1/smartdialer/agents";
        public static string GetListCampaignData = "tima/api/v1/smartdialer/list-campaign-data?campaign_id={0}&page={1}&page_size{2}";
        public static string Remove_Campaign_Data = "tima/api/v1/smartdialer/remove-campaign-data";
        public static string get_record = "tima/api/v1/calls?call_type={0}&phone={1}&start_time_since={2}&end_time_since={3}&count={4}";
        public static string get_recordV2 = "/api/call-infos?filter=CALLER||in||{0}&filter=START_TIME||between||{1},{2}&filter=CALL_TYPE||eq||{3}&limit={4}";
    }
}
