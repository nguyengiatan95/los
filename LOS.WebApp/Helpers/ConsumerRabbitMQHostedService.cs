﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using LOS.DAL.DTOs;
using Serilog;
using Microsoft.Extensions.Configuration;
using LOS.DAL.Object;
using System.Text;

namespace LOS.WebApp.Helpers
{
	public class ConsumerRabbitMQHostedService : BackgroundService
	{
		private IConnection _connection;
		private IModel _channel;

		private readonly IHubContext<HubManagerHub> _hubContext;
		private IConfiguration _configuration;
		public ConsumerRabbitMQHostedService(ILoggerFactory loggerFactory, IHubContext<HubManagerHub> hubContext, IConfiguration configuration)
		{		
			this._hubContext = hubContext;
			_configuration = configuration;
			InitRabbitMQ();
		}

		private void InitRabbitMQ()
		{
			var username = _configuration["AppSettings:RabbitmqUser"];
			var Password = _configuration["AppSettings:RabbitmqPass"];
			var queue_notification = _configuration["AppSettings:QueueNotification"];
			var factory = new ConnectionFactory { HostName = _configuration["AppSettings:RabbitmqHost"], Port = 5673, UserName = username, Password = Password };

			// create connection  
			_connection = factory.CreateConnection();
			// create channel  
			IModel channel = _connection.CreateModel();
			//channel.ExchangeDeclare("los.exchange", ExchangeType.Direct);
			channel.QueueDeclare(queue: queue_notification,
								 durable: true,
								 exclusive: false,
								 autoDelete: false,
								 arguments: null);
			//channel.QueueBind("los.init_loan_queue", "", "", null);
			channel.BasicQos(0, 0, false);
			_connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;

			var consumer = new EventingBasicConsumer(channel);
			consumer.Received += (ch, ea) =>
			{
				// received message  
				var content = System.Text.Encoding.UTF8.GetString(ea.Body.ToArray());

				Thread t = new Thread(() => HandleMessage(content));
				t.Start();
				channel.BasicAck(ea.DeliveryTag, false);
			};

			consumer.Shutdown += OnConsumerShutdown;
			consumer.Registered += OnConsumerRegistered;
			consumer.Unregistered += OnConsumerUnregistered;
			consumer.ConsumerCancelled += OnConsumerConsumerCancelled;

			channel.BasicConsume(queue: queue_notification,
								 autoAck: false,
								 consumer: consumer);
		}

		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{
			stoppingToken.ThrowIfCancellationRequested();
            return Task.CompletedTask;
		}

		private void HandleMessage(string content)
		{
			try
			{
				// we just print this message   
				var notify = JsonConvert.DeserializeObject<NotificationSystem>(content);
				// signalR to push message
				if(!string.IsNullOrEmpty(notify.UserName))
					_hubContext.Clients.Group(notify.UserName).SendAsync("Notification", notify);				
			}
			catch (Exception ex)
			{
				Log.Information(ex.Message + "\n" + ex.StackTrace);
				Log.Information("not connected");
			}
		}

		private void OnConsumerConsumerCancelled(object sender, ConsumerEventArgs e) { }
		private void OnConsumerUnregistered(object sender, ConsumerEventArgs e) { }
		private void OnConsumerRegistered(object sender, ConsumerEventArgs e) {
			
		}
		private void OnConsumerShutdown(object sender, ShutdownEventArgs e) { }
		private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e) { }

		public override void Dispose()
		{
			_channel.Close();
			_connection.Close();
			base.Dispose();
		}
	}

}
