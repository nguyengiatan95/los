﻿using LOS.Common.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class EncryptedActionParameterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Dictionary<string, object> decryptedParameters = new Dictionary<string, object>();
            if (filterContext.HttpContext.Request.Query["q"].ToString() != null && filterContext.HttpContext.Request.Query["q"].ToString() != "")
            {
                string encryptedQueryString = filterContext.HttpContext.Request.Query["q"];
                string decrptedString =  EncryptUtils.Decrypt(encryptedQueryString.ToString(), true);
                string[] paramsArrs = decrptedString.Split('&');

                for (int i = 0; i < paramsArrs.Length; i++)
                {
                    string[] paramArr = paramsArrs[i].Split('=');
                    decryptedParameters.Add(paramArr[0].Replace("?",""), paramArr[1]);
                }
            }
            var allParams = filterContext.ActionDescriptor.Parameters;
            var dicParamsType = new Dictionary<string, Type>();

            foreach (var parameter in allParams)
            {
                dicParamsType.Add(parameter.Name, parameter.ParameterType);
            }

            for (int i = 0; i < decryptedParameters.Count; i++)
            {
                if (dicParamsType.ContainsKey(decryptedParameters.Keys.ElementAt(i)))
                {
                    var typeOfParamsOrg = dicParamsType[decryptedParameters.Keys.ElementAt(i)];
                    //Process Nullable Type
                    var typeOfParams = Nullable.GetUnderlyingType(typeOfParamsOrg) ?? typeOfParamsOrg;
                    var paramObjValue = decryptedParameters.Values.ElementAt(i);
                    if (paramObjValue != null)
                    {
                        filterContext.ActionArguments[decryptedParameters.Keys.ElementAt(i)] = Convert.ChangeType(paramObjValue, typeOfParams);
                    }
                }                
            }
            base.OnActionExecuting(filterContext);

        }

    }
}
