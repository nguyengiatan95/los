﻿using Hanssens.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Helpers
{
    public class LocalStorageHelper 
    {
        public LocalStorageConfiguration _config;
        public string _keyEncrypt;
        public LocalStorageHelper(string keyEncrypt)
        {
            _keyEncrypt = keyEncrypt;
            _config = new LocalStorageConfiguration()
            {
                EnableEncryption = true,
                EncryptionSalt = keyEncrypt
            };
        }

        public void Store(string key, string value)
        {
            using (var encryptedStorage =  new LocalStorage(_config, _keyEncrypt))
            {
                encryptedStorage.Store(key, value);
            }
        }

        public T Get<T>(string key)
        {
            using (var encryptedStorage = new LocalStorage(_config, _keyEncrypt))
            {
                return encryptedStorage.Get<T>(key);
            }           
        }
    }
}
