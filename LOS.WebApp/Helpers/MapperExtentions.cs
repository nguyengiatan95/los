﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Models;
using LOS.WebApp.Models.ContactCustomer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Helpers
{
    public static class MapperExtentions
    {
        public static LoanBriefInit MapToLoanBriefViewModel(this LoanBriefInitDetail item)
        {
            var outLoanBrief = new LoanBriefInit();
            Ultility.CopyObject(item, ref outLoanBrief, true);
            outLoanBrief.LoanBriefCompany = item.LoanBriefCompany ?? new LOS.DAL.EntityFramework.LoanBriefCompany();
            outLoanBrief.LoanBriefRelationship = item.LoanBriefRelationship;
            return outLoanBrief;
        }

        public static LoanBriefInit MapToLoanBriefInfoViewModel(this LoanBriefInitDetail item)
        {
            var outLoanBrief = new LoanBriefInit();
            Ultility.CopyObject(item, ref outLoanBrief, true);
            outLoanBrief.LoanBriefCompany = item.LoanBriefCompany ?? new LOS.DAL.EntityFramework.LoanBriefCompany();
            outLoanBrief.LoanBriefRelationship = item.LoanBriefRelationship;
            //outLoanBrief.LoanBriefFiles = item.LoanBriefFiles;
            outLoanBrief.LoanBriefNote = item.LoanBriefNote;
            return outLoanBrief;
        }

        public static void MapToLoanBriefScriptItem(LoanBriefInitDetail item, ref LoanBriefScriptItem outLoanBrief)
        {
            Ultility.CopyObject(item, ref outLoanBrief, true);
            outLoanBrief.LoanBriefCompany = item.LoanBriefCompany ?? new LOS.DAL.EntityFramework.LoanBriefCompany();
            outLoanBrief.LoanBriefRelationship = item.LoanBriefRelationship;
            outLoanBrief.LoanBriefDocument = item.LoanBriefDocument;
        }


        public static LoanbriefViewModel MapToLoanbriefViewModel(this LoanBriefInitDetail item)
        {
            #region map Loanbrief
            var outLoanBrief = new LoanbriefViewModel();
            outLoanBrief.LoanBriefId = item.LoanBriefId;
            outLoanBrief.CustomerId = item.CustomerId.GetValueOrDefault();
            outLoanBrief.PlatformType = item.PlatformType.GetValueOrDefault();
            outLoanBrief.TypeLoanBrief = item.TypeLoanBrief.GetValueOrDefault();
            outLoanBrief.FullName = item.FullName;
            outLoanBrief.Phone = item.Phone;
            outLoanBrief.NationalCard = item.NationalCard;
            outLoanBrief.NationCardPlace = item.NationCardPlace;
            outLoanBrief.IsTrackingLocation = item.IsTrackingLocation;
            outLoanBrief.sBirthDay = item.Dob.HasValue ? item.Dob.Value.ToString("dd/MM/yyyy") : "";
            outLoanBrief.Passport = item.Passport;
            outLoanBrief.Gender = item.Gender;
            outLoanBrief.NumberCall = item.NumberCall;
            outLoanBrief.ProvinceId = item.ProvinceId;
            outLoanBrief.ProvinceName = item.ProvinceName;
            outLoanBrief.DistrictName = item.DistrictName;
            outLoanBrief.WardName = item.WardName;
            outLoanBrief.DistrictId = item.DistrictId;
            outLoanBrief.WardId = item.WardId;
            outLoanBrief.ProductId = item.ProductId.GetValueOrDefault();
            outLoanBrief.RateTypeId = item.RateTypeId.GetValueOrDefault();
            outLoanBrief.LoanAmount = item.LoanAmount.GetValueOrDefault();
            outLoanBrief.BuyInsurenceCustomer = item.BuyInsurenceCustomer;
            outLoanBrief.LoanTime = item.LoanTime;
            outLoanBrief.Frequency = item.Frequency;
            outLoanBrief.RateMoney = item.RateMoney;
            outLoanBrief.RatePercent = item.RatePercent;
            outLoanBrief.ReceivingMoneyType = item.ReceivingMoneyType;
            outLoanBrief.BankId = item.BankId;
            outLoanBrief.BankAccountNumber = item.BankAccountNumber;
            outLoanBrief.BankCardNumber = item.BankCardNumber;
            outLoanBrief.BankAccountName = item.BankAccountName;
            outLoanBrief.IsLocate = item.IsLocate;
            outLoanBrief.TypeRemarketing = item.TypeRemarketing;
            outLoanBrief.FromDate = item.FromDate;
            outLoanBrief.ToDate = item.ToDate;
            outLoanBrief.CreatedTime = item.CreatedTime.HasValue ? item.CreatedTime.Value.DateTime : DateTime.MinValue;
            outLoanBrief.IsHeadOffice = item.IsHeadOffice;
            outLoanBrief.HubId = item.HubId;
            outLoanBrief.CreateBy = item.CreateBy;
            outLoanBrief.BoundTelesaleId = item.BoundTelesaleId;
            outLoanBrief.HubEmployeeId = item.HubEmployeeId;
            outLoanBrief.PresenterCode = item.PresenterCode;
            outLoanBrief.LoanAmountFirst = item.LoanAmountFirst;
            outLoanBrief.LoanAmountExpertise = item.LoanAmountExpertise;
            outLoanBrief.LoanAmountExpertiseLast = item.LoanAmountExpertiseLast;
            outLoanBrief.LoanAmountExpertiseAi = item.LoanAmountExpertiseAi;
            outLoanBrief.FirstProcessingTime = item.FirstProcessingTime;
            outLoanBrief.ProductType = item.ProductType;
            outLoanBrief.IsCheckBank = item.IsCheckBank;
            outLoanBrief.HomeNetwork = item.HomeNetwork;
            outLoanBrief.PhoneOther = item.PhoneOther;
            outLoanBrief.BankBranch = item.BankBranch;
            outLoanBrief.ProductDetailId = item.ProductDetailId;
            outLoanBrief.IsSim = item.IsSim;
            outLoanBrief.LoanPurpose = item.LoanPurpose;
            outLoanBrief.Email = item.Email;
            outLoanBrief.TransactionState = item.TransactionState;
            outLoanBrief.IsTransactionsSecured = item.IsTransactionsSecured;
            outLoanBrief.Ltv = item.Ltv;
            #endregion

            #region map customer
            outLoanBrief.CustomerModel.FacebookAddress = item.Customer != null ? item.Customer.FacebookAddress : "";
            outLoanBrief.CustomerModel.InsurenceNumber = item.Customer != null ? item.Customer.InsurenceNumber : "";
            outLoanBrief.CustomerModel.InsurenceNote = item.Customer != null ? item.Customer.InsurenceNote : "";
            outLoanBrief.CustomerModel.IsMerried = item.Customer != null ? item.Customer.IsMerried : null;
            outLoanBrief.CustomerModel.NumberBaby = item.Customer != null ? item.Customer.NumberBaby : null;
            outLoanBrief.CustomerModel.EsignAgreementId = item.Customer != null ? item.Customer.EsignAgreementId : null;
            #endregion

            #region map LoanBriefResident
            outLoanBrief.LoanBriefResidentModel.LivingTime = item.LoanBriefResident != null ? item.LoanBriefResident.LivingTime : 0;
            outLoanBrief.LoanBriefResidentModel.ResidentType = item.LoanBriefResident != null ? item.LoanBriefResident.ResidentType : 0;
            outLoanBrief.LoanBriefResidentModel.LivingWith = item.LoanBriefResident != null ? item.LoanBriefResident.LivingWith : 0;
            outLoanBrief.LoanBriefResidentModel.Address = item.LoanBriefResident != null ? item.LoanBriefResident.Address : "";
            outLoanBrief.LoanBriefResidentModel.LoanBriefResidentId = item.LoanBriefResident != null ? item.LoanBriefResident.LoanBriefResidentId : 0;
            outLoanBrief.LoanBriefResidentModel.AddressGoogleMap = item.LoanBriefResident != null ? item.LoanBriefResident.AddressGoogleMap : "";
            outLoanBrief.LoanBriefResidentModel.AddressLatLng = item.LoanBriefResident != null ? item.LoanBriefResident.AddressLatLng : "";
            outLoanBrief.LoanBriefResidentModel.BillElectricityId = item.LoanBriefResident != null ? item.LoanBriefResident.BillElectricityId : "";
            outLoanBrief.LoanBriefResidentModel.BillWaterId = item.LoanBriefResident != null ? item.LoanBriefResident.BillWaterId : "";
            outLoanBrief.LoanBriefResidentModel.AddressNationalCard = item.LoanBriefResident != null ? item.LoanBriefResident.AddressNationalCard : "";
            outLoanBrief.LoanBriefResidentModel.CustomerShareLocation = item.LoanBriefResident != null ? item.LoanBriefResident.CustomerShareLocation : "";
            #endregion

            #region map LoanBriefHouseholdModel
            outLoanBrief.LoanBriefHouseholdModel.Address = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.Address : "";
            outLoanBrief.LoanBriefHouseholdModel.ProvinceId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.ProvinceId : 0;
            outLoanBrief.LoanBriefHouseholdModel.DistrictId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.DistrictId : 0;
            outLoanBrief.LoanBriefHouseholdModel.WardId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.WardId : 0;
            outLoanBrief.LoanBriefHouseholdModel.FullNameHouseOwner = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.FullNameHouseOwner : null;
            outLoanBrief.LoanBriefHouseholdModel.RelationshipHouseOwner = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.RelationshipHouseOwner : null;
            outLoanBrief.LoanBriefHouseholdModel.BirdayHouseOwner = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.BirdayHouseOwner : null;
            #endregion

            #region map LoanBriefProperty
            outLoanBrief.LoanBriefPropertyModel.BrandId = item.LoanBriefProperty != null ? item.LoanBriefProperty.BrandId : 0;
            outLoanBrief.LoanBriefPropertyModel.ProductId = item.LoanBriefProperty != null ? item.LoanBriefProperty.ProductId : 0;
            outLoanBrief.LoanBriefPropertyModel.PlateNumber = item.LoanBriefProperty != null ? item.LoanBriefProperty.PlateNumber : "";
            outLoanBrief.LoanBriefPropertyModel.CarManufacturer = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarManufacturer : "";
            outLoanBrief.LoanBriefPropertyModel.CarName = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarName : "";
            outLoanBrief.LoanBriefPropertyModel.CarColor = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarColor : "";
            outLoanBrief.LoanBriefPropertyModel.PlateNumberCar = item.LoanBriefProperty != null ? item.LoanBriefProperty.PlateNumberCar : "";
            outLoanBrief.LoanBriefPropertyModel.Chassis = item.LoanBriefProperty != null ? item.LoanBriefProperty.Chassis : "";
            outLoanBrief.LoanBriefPropertyModel.Engine = item.LoanBriefProperty != null ? item.LoanBriefProperty.Engine : "";
            outLoanBrief.LoanBriefPropertyModel.BuyInsuranceProperty = item.LoanBriefProperty != null ? item.LoanBriefProperty.BuyInsuranceProperty : false;
            outLoanBrief.LoanBriefPropertyModel.OwnerFullName = item.LoanBriefProperty != null ? item.LoanBriefProperty.OwnerFullName : "";
            outLoanBrief.LoanBriefPropertyModel.MotobikeCertificateNumber = item.LoanBriefProperty != null ? item.LoanBriefProperty.MotobikeCertificateNumber : "";
            outLoanBrief.LoanBriefPropertyModel.MotobikeCertificateDate = item.LoanBriefProperty != null ? item.LoanBriefProperty.MotobikeCertificateDate : "";
            outLoanBrief.LoanBriefPropertyModel.MotobikeCertificateAddress = item.LoanBriefProperty != null ? item.LoanBriefProperty.MotobikeCertificateAddress : "";
            outLoanBrief.LoanBriefPropertyModel.PostRegistration = item.LoanBriefProperty != null ? item.LoanBriefProperty.PostRegistration : false;
            outLoanBrief.LoanBriefPropertyModel.Description = item.LoanBriefProperty != null ? item.LoanBriefProperty.Description : null;

            #endregion

            #region map LoanBriefJobModel
            outLoanBrief.LoanBriefJobModel.LoanBriefJobId = item.LoanBriefJob != null ? item.LoanBriefJob.LoanBriefJobId : 0;
            outLoanBrief.LoanBriefJobModel.JobId = item.LoanBriefJob != null ? item.LoanBriefJob.JobId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyName = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyName : "";
            outLoanBrief.LoanBriefJobModel.CompanyPhone = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyPhone : "";
            outLoanBrief.LoanBriefJobModel.CompanyTaxCode = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyTaxCode : "";
            outLoanBrief.LoanBriefJobModel.CompanyProvinceId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyProvinceId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyDistrictId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyDistrictId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyWardId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyWardId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyAddress = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddress : "";
            outLoanBrief.LoanBriefJobModel.TotalIncome = item.LoanBriefJob != null ? item.LoanBriefJob.TotalIncome : 0;
            outLoanBrief.LoanBriefJobModel.ImcomeType = item.LoanBriefJob != null ? item.LoanBriefJob.ImcomeType : 0;
            outLoanBrief.LoanBriefJobModel.Description = item.LoanBriefJob != null ? item.LoanBriefJob.Description : "";
            outLoanBrief.LoanBriefJobModel.CompanyAddressGoogleMap = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddressGoogleMap : "";
            outLoanBrief.LoanBriefJobModel.CompanyAddressLatLng = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddressLatLng : "";
            outLoanBrief.LoanBriefJobModel.CompanyInsurance = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyInsurance : null;
            outLoanBrief.LoanBriefJobModel.WorkLocation = item.LoanBriefJob != null ? item.LoanBriefJob.WorkLocation : null;
            outLoanBrief.LoanBriefJobModel.BusinessPapers = item.LoanBriefJob != null ? item.LoanBriefJob.BusinessPapers : null;
            outLoanBrief.LoanBriefJobModel.JobDescriptionId = item.LoanBriefJob != null ? item.LoanBriefJob.JobDescriptionId : null;
            #endregion

            #region map LoanBriefCompany
            if (item.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
            {
                outLoanBrief.LoanBriefCompanyModel.LoanBriefId = item.LoanBriefCompany != null ? item.LoanBriefCompany.LoanBriefId : 0;
                outLoanBrief.LoanBriefCompanyModel.CompanyName = item.LoanBriefCompany != null ? item.LoanBriefCompany.CompanyName : "";
                outLoanBrief.LoanBriefCompanyModel.CareerBusiness = item.LoanBriefCompany != null ? item.LoanBriefCompany.CareerBusiness : "";
                outLoanBrief.LoanBriefCompanyModel.BusinessCertificationAddress = item.LoanBriefCompany != null ? item.LoanBriefCompany.BusinessCertificationAddress : "";
                outLoanBrief.LoanBriefCompanyModel.HeadOffice = item.LoanBriefCompany != null ? item.LoanBriefCompany.HeadOffice : "";
                outLoanBrief.LoanBriefCompanyModel.Address = item.LoanBriefCompany != null ? item.LoanBriefCompany.Address : "";
                outLoanBrief.LoanBriefCompanyModel.CompanyShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.CompanyShareholder : "";
                outLoanBrief.LoanBriefCompanyModel.sCardNumberShareholderDate = item.LoanBriefCompany != null && item.LoanBriefCompany.CardNumberShareholderDate.HasValue
                    ? item.LoanBriefCompany.CardNumberShareholderDate.Value.ToString("dd/MM/yyyy") : "";
                outLoanBrief.LoanBriefCompanyModel.sBusinessCertificationDate = item.LoanBriefCompany != null && item.LoanBriefCompany.BusinessCertificationDate.HasValue
                    ? item.LoanBriefCompany.BusinessCertificationDate.Value.ToString("dd/MM/yyyy") : "";
                outLoanBrief.LoanBriefCompanyModel.sBirthdayShareholder = item.LoanBriefCompany != null && item.LoanBriefCompany.BirthdayShareholder.HasValue
                    ? item.LoanBriefCompany.BirthdayShareholder.Value.ToString("dd/MM/yyyy") : "";
                outLoanBrief.LoanBriefCompanyModel.CardNumberShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.CardNumberShareholder : "";
                outLoanBrief.LoanBriefCompanyModel.PlaceOfBirthShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.PlaceOfBirthShareholder : "";
            }
            #endregion

            #region map LoanBriefRelationshipModel
            if (item.LoanBriefRelationship != null && item.LoanBriefRelationship.Count > 0)
            {
                foreach (var relationship in item.LoanBriefRelationship)
                {
                    if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                        outLoanBrief.LoanBriefRelationshipModels.Add(new LoanBriefRelationshipModel()
                        {
                            Id = relationship.Id,
                            RelationshipType = relationship.RelationshipType,
                            FullName = relationship.FullName,
                            Phone = relationship.Phone,
                            Address = relationship.Address,
                        });
                }
            }
            #endregion

            #region map LoanBriefQuestionScript
            outLoanBrief.LoanBriefQuestionScriptModel.WaterSupplier = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.WaterSupplier : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionHouseOwner = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionHouseOwner : null;
            outLoanBrief.LoanBriefQuestionScriptModel.Appraiser = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.Appraiser : null;
            outLoanBrief.LoanBriefQuestionScriptModel.DocumentBusiness = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.DocumentBusiness : null;
            outLoanBrief.LoanBriefQuestionScriptModel.MaxPrice = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.MaxPrice : null;
            return outLoanBrief;
        }

        public static LoanBrief MapToLoanbrief(this LoanbriefViewModel item)
        {
            #region map Loanbrief
            var outLoanBrief = new LoanBrief();
            outLoanBrief.LoanBriefId = item.LoanBriefId;
            outLoanBrief.CustomerId = item.CustomerId;
            outLoanBrief.PlatformType = item.PlatformType;
            outLoanBrief.TypeLoanBrief = item.TypeLoanBrief;
            outLoanBrief.FullName = item.FullName;
            outLoanBrief.Phone = item.Phone;
            outLoanBrief.NationalCard = item.NationalCard;
            outLoanBrief.NationCardPlace = item.NationCardPlace;
            outLoanBrief.IsTrackingLocation = item.IsTrackingLocation;
            if (!string.IsNullOrEmpty(item.sBirthDay))
                outLoanBrief.Dob = DateTimeUtility.ConvertStringToDate(item.sBirthDay, "dd/MM/yyyy");
            outLoanBrief.Passport = item.Passport;
            outLoanBrief.Gender = item.Gender;
            outLoanBrief.NumberCall = item.NumberCall;
            outLoanBrief.ProvinceId = item.ProvinceId;
            outLoanBrief.DistrictId = item.DistrictId;
            outLoanBrief.WardId = item.WardId;
            outLoanBrief.ProductId = item.ProductId;
            outLoanBrief.RateTypeId = item.RateTypeId;
            outLoanBrief.LoanAmount = item.LoanAmount;
            outLoanBrief.BuyInsurenceCustomer = item.BuyInsurenceCustomer;
            outLoanBrief.LoanTime = item.LoanTime;
            outLoanBrief.Frequency = item.Frequency;
            outLoanBrief.RateMoney = item.RateMoney;
            outLoanBrief.RatePercent = item.RatePercent;
            outLoanBrief.ReceivingMoneyType = item.ReceivingMoneyType;
            outLoanBrief.BankId = item.BankId;
            outLoanBrief.BankAccountNumber = item.BankAccountNumber;
            outLoanBrief.BankCardNumber = item.BankCardNumber;
            outLoanBrief.BankAccountName = item.BankAccountName;
            outLoanBrief.IsLocate = item.IsLocate;
            outLoanBrief.TypeRemarketing = item.TypeRemarketing;
            outLoanBrief.FromDate = item.FromDate;
            outLoanBrief.ToDate = item.ToDate;
            outLoanBrief.CreatedTime = item.CreatedTime;
            outLoanBrief.IsHeadOffice = item.IsHeadOffice;
            outLoanBrief.HubId = item.HubId;
            outLoanBrief.CreateBy = item.CreateBy;
            outLoanBrief.BoundTelesaleId = item.BoundTelesaleId;
            outLoanBrief.HubEmployeeId = item.HubEmployeeId;
            outLoanBrief.PresenterCode = item.PresenterCode;
            outLoanBrief.LoanAmountFirst = item.LoanAmountFirst;
            outLoanBrief.LoanAmountExpertise = item.LoanAmountExpertise;
            outLoanBrief.LoanAmountExpertiseLast = item.LoanAmountExpertiseLast;
            outLoanBrief.LoanAmountExpertiseAi = item.LoanAmountExpertiseAi;
            outLoanBrief.FirstProcessingTime = item.FirstProcessingTime;
            outLoanBrief.HomeNetwork = item.HomeNetwork;
            outLoanBrief.PhoneOther = item.PhoneOther;
            outLoanBrief.BankBranch = item.BankBranch;
            outLoanBrief.TeamTelesalesId = item.TeamTelesaleId;
            outLoanBrief.ProductDetailId = item.ProductDetailId;
            outLoanBrief.IsSim = item.IsSim;
            outLoanBrief.IsReborrow = item.IsReborrow;
            outLoanBrief.ReMarketingLoanBriefId = item.ReMarketingLoanBriefId;
            outLoanBrief.LoanPurpose = item.LoanPurpose;
            outLoanBrief.Email = item.Email;
            outLoanBrief.IsTransactionsSecured = item.IsTransactionsSecured;
            outLoanBrief.Ltv = item.Ltv;
            #endregion

            #region map customer
            outLoanBrief.Customer = new Customer();
            outLoanBrief.Customer.FacebookAddress = item.CustomerModel != null ? item.CustomerModel.FacebookAddress : "";
            outLoanBrief.Customer.InsurenceNumber = item.CustomerModel != null ? item.CustomerModel.InsurenceNumber : "";
            outLoanBrief.Customer.InsurenceNote = item.CustomerModel != null ? item.CustomerModel.InsurenceNote : "";
            outLoanBrief.Customer.IsMerried = item.CustomerModel != null ? item.CustomerModel.IsMerried : null;
            outLoanBrief.Customer.NumberBaby = item.CustomerModel != null ? item.CustomerModel.NumberBaby : null;
            #endregion

            #region map LoanBriefResident
            outLoanBrief.LoanBriefResident = new LoanBriefResident();
            outLoanBrief.LoanBriefResident.LivingTime = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.LivingTime : 0;
            outLoanBrief.LoanBriefResident.ResidentType = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.ResidentType : 0;
            outLoanBrief.LoanBriefResident.LivingWith = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.LivingWith : 0;
            outLoanBrief.LoanBriefResident.Address = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.Address : "";
            outLoanBrief.LoanBriefResident.LoanBriefResidentId = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.LoanBriefResidentId : 0;
            outLoanBrief.LoanBriefResident.AddressGoogleMap = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.AddressGoogleMap : "";
            outLoanBrief.LoanBriefResident.AddressLatLng = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.AddressLatLng : "";
            outLoanBrief.LoanBriefResident.BillElectricityId = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.BillElectricityId : "";
            outLoanBrief.LoanBriefResident.BillWaterId = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.BillWaterId : "";
            outLoanBrief.LoanBriefResident.AddressNationalCard = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.AddressNationalCard : "";
            outLoanBrief.LoanBriefResident.CustomerShareLocation = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.CustomerShareLocation : "";
            #endregion

            #region map LoanBriefHouseholdModel
            outLoanBrief.LoanBriefHousehold = new LoanBriefHousehold();
            outLoanBrief.LoanBriefHousehold.Address = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.Address : "";
            outLoanBrief.LoanBriefHousehold.ProvinceId = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.ProvinceId : 0;
            outLoanBrief.LoanBriefHousehold.DistrictId = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.DistrictId : 0;
            outLoanBrief.LoanBriefHousehold.WardId = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.WardId : 0;
            outLoanBrief.LoanBriefHousehold.FullNameHouseOwner = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.FullNameHouseOwner : null;
            outLoanBrief.LoanBriefHousehold.RelationshipHouseOwner = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.RelationshipHouseOwner : null;
            outLoanBrief.LoanBriefHousehold.BirdayHouseOwner = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.BirdayHouseOwner : null;
            #endregion

            #region map LoanBriefProperty
            outLoanBrief.LoanBriefProperty = new LoanBriefProperty();
            outLoanBrief.LoanBriefProperty.BrandId = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.BrandId : 0;
            outLoanBrief.LoanBriefProperty.ProductId = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.ProductId : 0;
            outLoanBrief.LoanBriefProperty.PlateNumber = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.PlateNumber : "";
            outLoanBrief.LoanBriefProperty.CarManufacturer = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.CarManufacturer : "";
            outLoanBrief.LoanBriefProperty.CarName = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.CarName : "";
            outLoanBrief.LoanBriefProperty.CarColor = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.CarColor : "";
            outLoanBrief.LoanBriefProperty.PlateNumberCar = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.PlateNumberCar : "";
            outLoanBrief.LoanBriefProperty.Chassis = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.Chassis : "";
            outLoanBrief.LoanBriefProperty.Engine = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.Engine : "";
            outLoanBrief.LoanBriefProperty.BuyInsuranceProperty = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.BuyInsuranceProperty : false;
            outLoanBrief.LoanBriefProperty.OwnerFullName = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.OwnerFullName : "";
            outLoanBrief.LoanBriefProperty.MotobikeCertificateNumber = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.MotobikeCertificateNumber : "";
            outLoanBrief.LoanBriefProperty.MotobikeCertificateDate = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.MotobikeCertificateDate : "";
            outLoanBrief.LoanBriefProperty.MotobikeCertificateAddress = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.MotobikeCertificateAddress : "";
            outLoanBrief.LoanBriefProperty.PostRegistration = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.PostRegistration : false;
            outLoanBrief.LoanBriefProperty.Description = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.Description : null;
            #endregion

            #region map LoanBriefJobModel
            outLoanBrief.LoanBriefJob = new LoanBriefJob();
            outLoanBrief.LoanBriefJob.LoanBriefJobId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.LoanBriefJobId : 0;
            outLoanBrief.LoanBriefJob.JobId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.JobId : 0;
            outLoanBrief.LoanBriefJob.CompanyName = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyName : "";
            outLoanBrief.LoanBriefJob.CompanyPhone = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyPhone : "";
            outLoanBrief.LoanBriefJob.CompanyTaxCode = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyTaxCode : "";
            outLoanBrief.LoanBriefJob.CompanyProvinceId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyProvinceId : 0;
            outLoanBrief.LoanBriefJob.CompanyDistrictId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyDistrictId : 0;
            outLoanBrief.LoanBriefJob.CompanyWardId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyWardId : 0;
            outLoanBrief.LoanBriefJob.CompanyAddress = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyAddress : "";
            outLoanBrief.LoanBriefJob.TotalIncome = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.TotalIncome : 0;
            outLoanBrief.LoanBriefJob.ImcomeType = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.ImcomeType : 0;
            outLoanBrief.LoanBriefJob.Description = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.Description : "";
            outLoanBrief.LoanBriefJob.CompanyAddressGoogleMap = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyAddressGoogleMap : "";
            outLoanBrief.LoanBriefJob.CompanyAddressLatLng = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyAddressLatLng : "";
            outLoanBrief.LoanBriefJob.CompanyInsurance = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyInsurance : null;
            outLoanBrief.LoanBriefJob.WorkLocation = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.WorkLocation : null;
            outLoanBrief.LoanBriefJob.BusinessPapers = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.BusinessPapers : null;
            outLoanBrief.LoanBriefJob.JobDescriptionId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.JobDescriptionId : null;
            #endregion

            #region map LoanBriefCompany
            if (item.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
            {
                outLoanBrief.LoanBriefCompany = new LoanBriefCompany();
                outLoanBrief.LoanBriefCompany.LoanBriefId = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.LoanBriefId : 0;
                outLoanBrief.LoanBriefCompany.CompanyName = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.CompanyName : "";
                outLoanBrief.LoanBriefCompany.CareerBusiness = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.CareerBusiness : "";
                outLoanBrief.LoanBriefCompany.BusinessCertificationAddress = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.BusinessCertificationAddress : "";
                outLoanBrief.LoanBriefCompany.HeadOffice = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.HeadOffice : "";
                outLoanBrief.LoanBriefCompany.Address = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.Address : "";
                outLoanBrief.LoanBriefCompany.CompanyShareholder = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.CompanyShareholder : "";
                if (!string.IsNullOrEmpty(item.LoanBriefCompanyModel.sCardNumberShareholderDate))
                    outLoanBrief.LoanBriefCompany.CardNumberShareholderDate = DateTimeUtility.ConvertStringToDate(item.LoanBriefCompanyModel.sCardNumberShareholderDate, "dd/MM/yyyy");
                if (!string.IsNullOrEmpty(item.LoanBriefCompanyModel.sBusinessCertificationDate))
                    outLoanBrief.LoanBriefCompany.BusinessCertificationDate = DateTimeUtility.ConvertStringToDate(item.LoanBriefCompanyModel.sBusinessCertificationDate, "dd/MM/yyyy");
                if (!string.IsNullOrEmpty(item.LoanBriefCompanyModel.sBirthdayShareholder))
                    outLoanBrief.LoanBriefCompany.BirthdayShareholder = DateTimeUtility.ConvertStringToDate(item.LoanBriefCompanyModel.sBirthdayShareholder, "dd/MM/yyyy");
                outLoanBrief.LoanBriefCompany.CardNumberShareholder = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.CardNumberShareholder : "";
                outLoanBrief.LoanBriefCompany.PlaceOfBirthShareholder = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.PlaceOfBirthShareholder : "";
            }
            #endregion

            #region map LoanBriefRelationshipModel
            outLoanBrief.LoanBriefRelationship = new List<LoanBriefRelationship>();
            if (item.LoanBriefRelationshipModels != null && item.LoanBriefRelationshipModels.Count > 0)
            {
                foreach (var relationship in item.LoanBriefRelationshipModels)
                {
                    if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                    {
                        outLoanBrief.LoanBriefRelationship.Add(new LoanBriefRelationship()
                        {
                            Id = relationship.Id,
                            RelationshipType = relationship.RelationshipType,
                            FullName = relationship.FullName,
                            Phone = relationship.Phone,
                            Address = relationship.Address,
                        });
                    }
                }
            }
            #endregion

            #region map LoanBriefQuestionScript
            outLoanBrief.LoanBriefQuestionScript = new LoanBriefQuestionScript();
            outLoanBrief.LoanBriefQuestionScript.LoanBriefId = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.LoanBriefId : null;
            outLoanBrief.LoanBriefQuestionScript.WaterSupplier = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.WaterSupplier : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionHouseOwner = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionHouseOwner : null;
            outLoanBrief.LoanBriefQuestionScript.Appraiser = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.Appraiser : null;
            outLoanBrief.LoanBriefQuestionScript.DocumentBusiness = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.DocumentBusiness : null;
            outLoanBrief.LoanBriefQuestionScript.MaxPrice = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.MaxPrice : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionAddressCoincideAreaSupport = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionAddressCoincideAreaSupport : null;
            #endregion

            return outLoanBrief;
        }

        #region map for script
        public static LoanbriefScriptViewModel MapToLoanbriefScriptViewModel(this LoanBriefInitDetail item)
        {
            #region map Loanbrief
            var outLoanBrief = new LoanbriefScriptViewModel();
            outLoanBrief.LoanBriefId = item.LoanBriefId;
            outLoanBrief.CustomerId = item.CustomerId.GetValueOrDefault();
            outLoanBrief.PlatformType = item.PlatformType.GetValueOrDefault();
            outLoanBrief.TypeLoanBrief = item.TypeLoanBrief.GetValueOrDefault();
            outLoanBrief.FullName = item.FullName;
            outLoanBrief.Phone = item.Phone;
            outLoanBrief.NationalCard = item.NationalCard;
            outLoanBrief.NationCardPlace = item.NationCardPlace;
            outLoanBrief.IsTrackingLocation = item.IsTrackingLocation;
            outLoanBrief.sBirthDay = item.Dob.HasValue ? item.Dob.Value.ToString("dd/MM/yyyy") : "";
            outLoanBrief.Passport = item.Passport;
            outLoanBrief.Gender = item.Gender;
            outLoanBrief.NumberCall = item.NumberCall;
            outLoanBrief.ProvinceId = item.ProvinceId;
            outLoanBrief.ProvinceName = item.ProvinceName;
            outLoanBrief.DistrictName = item.DistrictName;
            outLoanBrief.WardName = item.WardName;
            outLoanBrief.DistrictId = item.DistrictId;
            outLoanBrief.WardId = item.WardId;
            outLoanBrief.ProductId = item.ProductId.GetValueOrDefault();
            outLoanBrief.RateTypeId = item.RateTypeId.GetValueOrDefault();
            outLoanBrief.LoanAmount = item.LoanAmount.GetValueOrDefault();
            outLoanBrief.BuyInsurenceCustomer = item.BuyInsurenceCustomer;
            outLoanBrief.LoanTime = item.LoanTime;
            outLoanBrief.Frequency = item.Frequency;
            outLoanBrief.RateMoney = item.RateMoney;
            outLoanBrief.RatePercent = item.RatePercent;
            outLoanBrief.ReceivingMoneyType = item.ReceivingMoneyType;
            outLoanBrief.BankId = item.BankId;
            outLoanBrief.BankAccountNumber = item.BankAccountNumber;
            outLoanBrief.BankCardNumber = item.BankCardNumber;
            outLoanBrief.BankAccountName = item.BankAccountName;
            outLoanBrief.IsLocate = item.IsLocate;
            outLoanBrief.TypeRemarketing = item.TypeRemarketing;
            outLoanBrief.FromDate = item.FromDate;
            outLoanBrief.ToDate = item.ToDate;
            outLoanBrief.CreatedTime = item.CreatedTime.HasValue ? item.CreatedTime.Value.DateTime : DateTime.MinValue;
            outLoanBrief.IsHeadOffice = item.IsHeadOffice;
            outLoanBrief.HubId = item.HubId;
            outLoanBrief.CreateBy = item.CreateBy;
            outLoanBrief.BoundTelesaleId = item.BoundTelesaleId;
            outLoanBrief.HubEmployeeId = item.HubEmployeeId;
            outLoanBrief.PresenterCode = item.PresenterCode;
            outLoanBrief.LoanAmountFirst = item.LoanAmountFirst;
            outLoanBrief.LoanAmountExpertise = item.LoanAmountExpertise;
            outLoanBrief.LoanAmountExpertiseLast = item.LoanAmountExpertiseLast;
            outLoanBrief.LoanAmountExpertiseAi = item.LoanAmountExpertiseAi;
            outLoanBrief.FirstProcessingTime = item.FirstProcessingTime;
            outLoanBrief.ProductType = item.ProductType;
            outLoanBrief.LoanPurpose = item.LoanPurpose;
            outLoanBrief.ValueCheckQualify = item.ValueCheckQualify;
            #endregion

            #region map customer
            outLoanBrief.CustomerModel.FacebookAddress = item.Customer != null ? item.Customer.FacebookAddress : "";
            outLoanBrief.CustomerModel.InsurenceNumber = item.Customer != null ? item.Customer.InsurenceNumber : "";
            outLoanBrief.CustomerModel.InsurenceNote = item.Customer != null ? item.Customer.InsurenceNote : "";
            outLoanBrief.CustomerModel.IsMerried = item.Customer != null ? item.Customer.IsMerried : null;
            outLoanBrief.CustomerModel.NumberBaby = item.Customer != null ? item.Customer.NumberBaby : null;
            #endregion

            #region map LoanBriefResident
            outLoanBrief.LoanBriefResidentModel.LivingTime = item.LoanBriefResident != null ? item.LoanBriefResident.LivingTime : 0;
            outLoanBrief.LoanBriefResidentModel.ResidentType = item.LoanBriefResident != null ? item.LoanBriefResident.ResidentType : 0;
            outLoanBrief.LoanBriefResidentModel.LivingWith = item.LoanBriefResident != null ? item.LoanBriefResident.LivingWith : 0;
            outLoanBrief.LoanBriefResidentModel.Address = item.LoanBriefResident != null ? item.LoanBriefResident.Address : "";
            outLoanBrief.LoanBriefResidentModel.LoanBriefResidentId = item.LoanBriefResident != null ? item.LoanBriefResident.LoanBriefResidentId : 0;
            outLoanBrief.LoanBriefResidentModel.Address = item.LoanBriefResident != null ? item.LoanBriefResident.Address : "";
            outLoanBrief.LoanBriefResidentModel.AddressGoogleMap = item.LoanBriefResident != null ? item.LoanBriefResident.AddressGoogleMap : "";
            outLoanBrief.LoanBriefResidentModel.AddressLatLng = item.LoanBriefResident != null ? item.LoanBriefResident.AddressLatLng : "";
            #endregion

            #region map LoanBriefHouseholdModel
            outLoanBrief.LoanBriefHouseholdModel.Address = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.Address : "";
            outLoanBrief.LoanBriefHouseholdModel.ProvinceId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.ProvinceId : 0;
            outLoanBrief.LoanBriefHouseholdModel.DistrictId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.DistrictId : 0;
            outLoanBrief.LoanBriefHouseholdModel.WardId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.WardId : 0;
            #endregion

            #region map LoanBriefProperty
            outLoanBrief.LoanBriefPropertyModel.BrandId = item.LoanBriefProperty != null ? item.LoanBriefProperty.BrandId : 0;
            outLoanBrief.LoanBriefPropertyModel.ProductId = item.LoanBriefProperty != null ? item.LoanBriefProperty.ProductId : 0;
            outLoanBrief.LoanBriefPropertyModel.PlateNumber = item.LoanBriefProperty != null ? item.LoanBriefProperty.PlateNumber : "";
            outLoanBrief.LoanBriefPropertyModel.CarManufacturer = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarManufacturer : "";
            outLoanBrief.LoanBriefPropertyModel.CarName = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarName : "";
            outLoanBrief.LoanBriefPropertyModel.CarColor = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarColor : "";
            outLoanBrief.LoanBriefPropertyModel.PlateNumberCar = item.LoanBriefProperty != null ? item.LoanBriefProperty.PlateNumberCar : "";
            #endregion

            #region map LoanBriefJobModel
            outLoanBrief.LoanBriefJobModel.LoanBriefJobId = item.LoanBriefJob != null ? item.LoanBriefJob.LoanBriefJobId : 0;
            outLoanBrief.LoanBriefJobModel.JobId = item.LoanBriefJob != null ? item.LoanBriefJob.JobId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyName = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyName : "";
            outLoanBrief.LoanBriefJobModel.CompanyPhone = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyPhone : "";
            outLoanBrief.LoanBriefJobModel.CompanyTaxCode = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyTaxCode : "";
            outLoanBrief.LoanBriefJobModel.CompanyProvinceId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyProvinceId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyDistrictId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyDistrictId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyWardId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyWardId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyAddress = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddress : "";
            outLoanBrief.LoanBriefJobModel.TotalIncome = item.LoanBriefJob != null ? item.LoanBriefJob.TotalIncome : 0;
            outLoanBrief.LoanBriefJobModel.ImcomeType = item.LoanBriefJob != null ? item.LoanBriefJob.ImcomeType : 0;
            outLoanBrief.LoanBriefJobModel.Description = item.LoanBriefJob != null ? item.LoanBriefJob.Description : "";
            outLoanBrief.LoanBriefJobModel.CompanyAddressGoogleMap = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddressGoogleMap : "";
            outLoanBrief.LoanBriefJobModel.CompanyAddressLatLng = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddressLatLng : "";
            #endregion

            #region map LoanBriefCompany
            if (item.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
            {
                outLoanBrief.LoanBriefCompanyModel.LoanBriefId = item.LoanBriefCompany != null ? item.LoanBriefCompany.LoanBriefId : 0;
                outLoanBrief.LoanBriefCompanyModel.CompanyName = item.LoanBriefCompany != null ? item.LoanBriefCompany.CompanyName : "";
                outLoanBrief.LoanBriefCompanyModel.CareerBusiness = item.LoanBriefCompany != null ? item.LoanBriefCompany.CareerBusiness : "";
                outLoanBrief.LoanBriefCompanyModel.BusinessCertificationAddress = item.LoanBriefCompany != null ? item.LoanBriefCompany.BusinessCertificationAddress : "";
                outLoanBrief.LoanBriefCompanyModel.HeadOffice = item.LoanBriefCompany != null ? item.LoanBriefCompany.HeadOffice : "";
                outLoanBrief.LoanBriefCompanyModel.Address = item.LoanBriefCompany != null ? item.LoanBriefCompany.Address : "";
                outLoanBrief.LoanBriefCompanyModel.CompanyShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.CompanyShareholder : "";
                outLoanBrief.LoanBriefCompanyModel.sCardNumberShareholderDate = item.LoanBriefCompany != null && item.LoanBriefCompany.CardNumberShareholderDate.HasValue
                    ? item.LoanBriefCompany.CardNumberShareholderDate.Value.ToString("dd/MM/yyyy") : "";
                outLoanBrief.LoanBriefCompanyModel.sBusinessCertificationDate = item.LoanBriefCompany != null && item.LoanBriefCompany.BusinessCertificationDate.HasValue
                    ? item.LoanBriefCompany.BusinessCertificationDate.Value.ToString("dd/MM/yyyy") : "";
                outLoanBrief.LoanBriefCompanyModel.sBirthdayShareholder = item.LoanBriefCompany != null && item.LoanBriefCompany.BirthdayShareholder.HasValue
                    ? item.LoanBriefCompany.BirthdayShareholder.Value.ToString("dd/MM/yyyy") : "";
                outLoanBrief.LoanBriefCompanyModel.CardNumberShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.CardNumberShareholder : "";
                outLoanBrief.LoanBriefCompanyModel.PlaceOfBirthShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.PlaceOfBirthShareholder : "";
            }
            #endregion

            #region map LoanBriefRelationshipModel
            if (item.LoanBriefRelationship != null && item.LoanBriefRelationship.Count > 0)
            {
                foreach (var relationship in item.LoanBriefRelationship)
                {
                    if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                        outLoanBrief.LoanBriefRelationshipModels.Add(new LoanBriefRelationshipModel()
                        {
                            Id = relationship.Id,
                            RelationshipType = relationship.RelationshipType,
                            FullName = relationship.FullName,
                            Phone = relationship.Phone,
                            Address = relationship.Address,
                            RefPhoneCallRate = relationship.RefPhoneCallRate
                        });
                }
            }
            #endregion

            #region map LoanBriefQuestionScript
            outLoanBrief.LoanBriefQuestionScriptModel.LoanBriefId = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.LoanBriefId : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionUseMotobikeGo : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionMotobikeCertificate : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionAccountBank = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionAccountBank : null;
            outLoanBrief.LoanBriefQuestionScriptModel.TypeJob = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.TypeJob : null;
            outLoanBrief.LoanBriefQuestionScriptModel.AdvisoryDeductInsurance = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.AdvisoryDeductInsurance : null;
            outLoanBrief.LoanBriefQuestionScriptModel.AdvisoryKeepRegisteredMotobike = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.AdvisoryKeepRegisteredMotobike : null;
            outLoanBrief.LoanBriefQuestionScriptModel.AdvisorySettingGps = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.AdvisorySettingGps : null;
            outLoanBrief.LoanBriefQuestionScriptModel.ChooseCareer = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.ChooseCareer : null;
            outLoanBrief.LoanBriefQuestionScriptModel.Signs = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.Signs : null;
            outLoanBrief.LoanBriefQuestionScriptModel.AddressBusiness = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.AddressBusiness : null;
            outLoanBrief.LoanBriefQuestionScriptModel.WorkingTime = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.WorkingTime : null;
            outLoanBrief.LoanBriefQuestionScriptModel.DriverOfCompany = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.DriverOfCompany : null;
            outLoanBrief.LoanBriefQuestionScriptModel.AccountDriver = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.AccountDriver : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionDriverGo = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionDriverGo : null;
            outLoanBrief.LoanBriefQuestionScriptModel.TimeDriver = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.TimeDriver : null;
            outLoanBrief.LoanBriefQuestionScriptModel.ContractInShop = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.ContractInShop : null;
            outLoanBrief.LoanBriefQuestionScriptModel.PhotographShop = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.PhotographShop : null;
            outLoanBrief.LoanBriefQuestionScriptModel.CommentStatusTelesales = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.CommentStatusTelesales : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionBorrow = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionBorrow : null;
            outLoanBrief.LoanBriefQuestionScriptModel.SigningAlaborContract = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.SigningAlaborContract : null;
            outLoanBrief.LoanBriefQuestionScriptModel.TypeWork = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.TypeWork : null;
            outLoanBrief.LoanBriefQuestionScriptModel.LatestPayday = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.LatestPayday : null;
            outLoanBrief.LoanBriefQuestionScriptModel.NameWeb = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.NameWeb : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionWarehouse = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionWarehouse : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionImageGoods = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionImageGoods : null;
            outLoanBrief.LoanBriefQuestionScriptModel.DetailJob = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.DetailJob : null;
            outLoanBrief.LoanBriefQuestionScriptModel.TimeSellWeb = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.TimeSellWeb : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionCarOwnership = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionCarOwnership : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionAddressCoincideAreaSupport = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionAddressCoincideAreaSupport : null;
            #endregion

            return outLoanBrief;
        }

        public static LoanBrief MapToLoanbrief(this LoanbriefScriptViewModel item)
        {
            #region map Loanbrief
            var outLoanBrief = new LoanBrief();
            outLoanBrief.LoanBriefId = item.LoanBriefId;
            outLoanBrief.CustomerId = item.CustomerId;
            outLoanBrief.PlatformType = item.PlatformType;
            outLoanBrief.TypeLoanBrief = item.TypeLoanBrief;
            outLoanBrief.FullName = item.FullName;
            outLoanBrief.Phone = item.Phone;
            outLoanBrief.NationalCard = item.NationalCard;
            outLoanBrief.NationCardPlace = item.NationCardPlace;
            outLoanBrief.IsTrackingLocation = item.IsTrackingLocation;
            if (!string.IsNullOrEmpty(item.sBirthDay))
                outLoanBrief.Dob = DateTimeUtility.ConvertStringToDate(item.sBirthDay, "dd/MM/yyyy");
            outLoanBrief.Passport = item.Passport;
            outLoanBrief.Gender = item.Gender;
            outLoanBrief.NumberCall = item.NumberCall;
            outLoanBrief.ProvinceId = item.ProvinceId;
            outLoanBrief.DistrictId = item.DistrictId;
            outLoanBrief.WardId = item.WardId;
            outLoanBrief.ProductId = item.ProductId;
            outLoanBrief.RateTypeId = item.RateTypeId;
            outLoanBrief.LoanAmount = item.LoanAmount;
            outLoanBrief.BuyInsurenceCustomer = item.BuyInsurenceCustomer;
            outLoanBrief.LoanTime = item.LoanTime;
            outLoanBrief.Frequency = item.Frequency;
            outLoanBrief.RateMoney = item.RateMoney;
            outLoanBrief.RatePercent = item.RatePercent;
            outLoanBrief.ReceivingMoneyType = item.ReceivingMoneyType;
            outLoanBrief.BankId = item.BankId;
            outLoanBrief.BankAccountNumber = item.BankAccountNumber;
            outLoanBrief.BankCardNumber = item.BankCardNumber;
            outLoanBrief.BankAccountName = item.BankAccountName;
            outLoanBrief.IsLocate = item.IsLocate;
            outLoanBrief.TypeRemarketing = item.TypeRemarketing;
            outLoanBrief.FromDate = item.FromDate;
            outLoanBrief.ToDate = item.ToDate;
            outLoanBrief.CreatedTime = item.CreatedTime;
            outLoanBrief.IsHeadOffice = item.IsHeadOffice;
            outLoanBrief.HubId = item.HubId;
            outLoanBrief.CreateBy = item.CreateBy;
            outLoanBrief.BoundTelesaleId = item.BoundTelesaleId;
            outLoanBrief.HubEmployeeId = item.HubEmployeeId;
            outLoanBrief.PresenterCode = item.PresenterCode;
            outLoanBrief.LoanAmountFirst = item.LoanAmountFirst;
            outLoanBrief.LoanAmountExpertise = item.LoanAmountExpertise;
            outLoanBrief.LoanAmountExpertiseLast = item.LoanAmountExpertiseLast;
            outLoanBrief.LoanAmountExpertiseAi = item.LoanAmountExpertiseAi;
            outLoanBrief.FirstProcessingTime = item.FirstProcessingTime;
            outLoanBrief.LoanPurpose = item.LoanPurpose;
            outLoanBrief.ValueCheckQualify = item.ValueCheckQualify;
            #endregion

            #region map customer
            outLoanBrief.Customer = new Customer();
            outLoanBrief.Customer.FacebookAddress = item.CustomerModel != null ? item.CustomerModel.FacebookAddress : "";
            outLoanBrief.Customer.InsurenceNumber = item.CustomerModel != null ? item.CustomerModel.InsurenceNumber : "";
            outLoanBrief.Customer.InsurenceNote = item.CustomerModel != null ? item.CustomerModel.InsurenceNote : "";
            outLoanBrief.Customer.IsMerried = item.CustomerModel != null ? item.CustomerModel.IsMerried : null;
            outLoanBrief.Customer.NumberBaby = item.CustomerModel != null ? item.CustomerModel.NumberBaby : null;
            #endregion

            #region map LoanBriefResident
            outLoanBrief.LoanBriefResident = new LoanBriefResident();
            outLoanBrief.LoanBriefResident.LivingTime = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.LivingTime : 0;
            outLoanBrief.LoanBriefResident.ResidentType = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.ResidentType : 0;
            outLoanBrief.LoanBriefResident.LivingWith = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.LivingWith : 0;
            outLoanBrief.LoanBriefResident.Address = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.Address : "";
            outLoanBrief.LoanBriefResident.LoanBriefResidentId = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.LoanBriefResidentId : 0;
            outLoanBrief.LoanBriefResident.AddressGoogleMap = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.AddressGoogleMap : "";
            outLoanBrief.LoanBriefResident.AddressLatLng = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.AddressLatLng : "";
            #endregion

            #region map LoanBriefHouseholdModel
            outLoanBrief.LoanBriefHousehold = new LoanBriefHousehold();
            outLoanBrief.LoanBriefHousehold.Address = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.Address : "";
            outLoanBrief.LoanBriefHousehold.ProvinceId = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.ProvinceId : 0;
            outLoanBrief.LoanBriefHousehold.DistrictId = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.DistrictId : 0;
            outLoanBrief.LoanBriefHousehold.WardId = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.WardId : 0;
            #endregion

            #region map LoanBriefProperty
            outLoanBrief.LoanBriefProperty = new LoanBriefProperty();
            outLoanBrief.LoanBriefProperty.BrandId = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.BrandId : 0;
            outLoanBrief.LoanBriefProperty.ProductId = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.ProductId : 0;
            outLoanBrief.LoanBriefProperty.PlateNumber = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.PlateNumber : "";
            outLoanBrief.LoanBriefProperty.CarManufacturer = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.CarManufacturer : "";
            outLoanBrief.LoanBriefProperty.CarName = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.CarName : "";
            outLoanBrief.LoanBriefProperty.CarColor = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.CarColor : "";
            outLoanBrief.LoanBriefProperty.PlateNumberCar = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.PlateNumberCar : "";
            #endregion

            #region map LoanBriefJobModel
            outLoanBrief.LoanBriefJob = new LoanBriefJob();
            outLoanBrief.LoanBriefJob.LoanBriefJobId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.LoanBriefJobId : 0;
            outLoanBrief.LoanBriefJob.JobId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.JobId : 0;
            outLoanBrief.LoanBriefJob.CompanyName = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyName : "";
            outLoanBrief.LoanBriefJob.CompanyPhone = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyPhone : "";
            outLoanBrief.LoanBriefJob.CompanyTaxCode = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyTaxCode : "";
            outLoanBrief.LoanBriefJob.CompanyProvinceId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyProvinceId : 0;
            outLoanBrief.LoanBriefJob.CompanyDistrictId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyDistrictId : 0;
            outLoanBrief.LoanBriefJob.CompanyWardId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyWardId : 0;
            outLoanBrief.LoanBriefJob.CompanyAddress = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyAddress : "";
            outLoanBrief.LoanBriefJob.TotalIncome = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.TotalIncome : 0;
            outLoanBrief.LoanBriefJob.ImcomeType = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.ImcomeType : 0;
            outLoanBrief.LoanBriefJob.Description = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.Description : "";
            outLoanBrief.LoanBriefJob.CompanyAddressGoogleMap = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyAddressGoogleMap : "";
            outLoanBrief.LoanBriefJob.CompanyAddressLatLng = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyAddressLatLng : "";
            #endregion

            #region map LoanBriefCompany
            if (item.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
            {
                outLoanBrief.LoanBriefCompany = new LoanBriefCompany();
                outLoanBrief.LoanBriefCompany.LoanBriefId = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.LoanBriefId : 0;
                outLoanBrief.LoanBriefCompany.CompanyName = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.CompanyName : "";
                outLoanBrief.LoanBriefCompany.CareerBusiness = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.CareerBusiness : "";
                outLoanBrief.LoanBriefCompany.BusinessCertificationAddress = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.BusinessCertificationAddress : "";
                outLoanBrief.LoanBriefCompany.HeadOffice = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.HeadOffice : "";
                outLoanBrief.LoanBriefCompany.Address = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.Address : "";
                outLoanBrief.LoanBriefCompany.CompanyShareholder = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.CompanyShareholder : "";
                if (!string.IsNullOrEmpty(item.LoanBriefCompanyModel.sCardNumberShareholderDate))
                    outLoanBrief.LoanBriefCompany.CardNumberShareholderDate = DateTimeUtility.ConvertStringToDate(item.LoanBriefCompanyModel.sCardNumberShareholderDate, "dd/MM/yyyy");
                if (!string.IsNullOrEmpty(item.LoanBriefCompanyModel.sBusinessCertificationDate))
                    outLoanBrief.LoanBriefCompany.BusinessCertificationDate = DateTimeUtility.ConvertStringToDate(item.LoanBriefCompanyModel.sBusinessCertificationDate, "dd/MM/yyyy");
                if (!string.IsNullOrEmpty(item.LoanBriefCompanyModel.sBirthdayShareholder))
                    outLoanBrief.LoanBriefCompany.BirthdayShareholder = DateTimeUtility.ConvertStringToDate(item.LoanBriefCompanyModel.sBirthdayShareholder, "dd/MM/yyyy");
                outLoanBrief.LoanBriefCompany.CardNumberShareholder = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.CardNumberShareholder : "";
                outLoanBrief.LoanBriefCompany.PlaceOfBirthShareholder = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.PlaceOfBirthShareholder : "";
            }
            #endregion

            #region map LoanBriefRelationshipModel
            outLoanBrief.LoanBriefRelationship = new List<LoanBriefRelationship>();
            if (item.LoanBriefRelationshipModels != null && item.LoanBriefRelationshipModels.Count > 0)
            {
                foreach (var relationship in item.LoanBriefRelationshipModels)
                {
                    if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                        outLoanBrief.LoanBriefRelationship.Add(new LoanBriefRelationship()
                        {
                            Id = relationship.Id,
                            RelationshipType = relationship.RelationshipType,
                            FullName = relationship.FullName,
                            Phone = relationship.Phone,
                            Address = relationship.Address,
                        });
                }
            }
            #endregion

            #region map LoanBriefQuestionScript
            outLoanBrief.LoanBriefQuestionScript = new LoanBriefQuestionScript();
            outLoanBrief.LoanBriefQuestionScript.LoanBriefId = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.LoanBriefId : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionUseMotobikeGo = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionMotobikeCertificate = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionAccountBank = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionAccountBank : null;
            outLoanBrief.LoanBriefQuestionScript.TypeJob = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.TypeJob : null;
            outLoanBrief.LoanBriefQuestionScript.AdvisoryDeductInsurance = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.AdvisoryDeductInsurance : null;
            outLoanBrief.LoanBriefQuestionScript.AdvisoryKeepRegisteredMotobike = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.AdvisoryKeepRegisteredMotobike : null;
            outLoanBrief.LoanBriefQuestionScript.AdvisorySettingGps = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.AdvisorySettingGps : null;
            outLoanBrief.LoanBriefQuestionScript.ChooseCareer = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.ChooseCareer : null;
            outLoanBrief.LoanBriefQuestionScript.Signs = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.Signs : null;
            outLoanBrief.LoanBriefQuestionScript.AddressBusiness = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.AddressBusiness : null;
            outLoanBrief.LoanBriefQuestionScript.WorkingTime = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.WorkingTime : null;
            outLoanBrief.LoanBriefQuestionScript.DriverOfCompany = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.DriverOfCompany : null;
            outLoanBrief.LoanBriefQuestionScript.AccountDriver = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.AccountDriver : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionDriverGo = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionDriverGo : null;
            outLoanBrief.LoanBriefQuestionScript.TimeDriver = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.TimeDriver : null;
            outLoanBrief.LoanBriefQuestionScript.ContractInShop = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.ContractInShop : null;
            outLoanBrief.LoanBriefQuestionScript.PhotographShop = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.PhotographShop : null;
            outLoanBrief.LoanBriefQuestionScript.CommentStatusTelesales = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.CommentStatusTelesales : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionBorrow = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionBorrow : null;
            outLoanBrief.LoanBriefQuestionScript.SigningAlaborContract = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.SigningAlaborContract : null;
            outLoanBrief.LoanBriefQuestionScript.TypeWork = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.TypeWork : null;
            outLoanBrief.LoanBriefQuestionScript.LatestPayday = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.LatestPayday : null;
            outLoanBrief.LoanBriefQuestionScript.NameWeb = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.NameWeb : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionWarehouse = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionWarehouse : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionImageGoods = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionImageGoods : null;
            outLoanBrief.LoanBriefQuestionScript.DetailJob = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.DetailJob : null;
            outLoanBrief.LoanBriefQuestionScript.TimeSellWeb = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.TimeSellWeb : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionCarOwnership = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionCarOwnership : null;
            #endregion

            return outLoanBrief;
        }


        public static LoanBrief MapToLoanbriefV2(this LoanbriefScriptViewModel item)
        {
            #region map Loanbrief
            var outLoanBrief = new LoanBrief();
            outLoanBrief.LoanBriefId = item.LoanBriefId;
            outLoanBrief.CustomerId = item.CustomerId;
            outLoanBrief.PlatformType = item.PlatformType;
            outLoanBrief.TypeLoanBrief = item.TypeLoanBrief;
            outLoanBrief.FullName = item.FullName;
            outLoanBrief.Phone = item.Phone;
            outLoanBrief.NationalCard = item.NationalCard;
            outLoanBrief.NationCardPlace = item.NationCardPlace;
            outLoanBrief.IsTrackingLocation = item.IsTrackingLocation;
            if (!string.IsNullOrEmpty(item.sBirthDay))
                outLoanBrief.Dob = DateTimeUtility.ConvertStringToDate(item.sBirthDay, "dd/MM/yyyy");
            outLoanBrief.Passport = item.Passport;
            outLoanBrief.Gender = item.Gender;
            outLoanBrief.NumberCall = item.NumberCall;
            outLoanBrief.ProvinceId = item.ProvinceId;
            outLoanBrief.DistrictId = item.DistrictId;
            outLoanBrief.WardId = item.WardId;
            outLoanBrief.ProductId = item.ProductId;
            outLoanBrief.RateTypeId = item.RateTypeId;
            outLoanBrief.LoanAmount = item.LoanAmount;
            outLoanBrief.BuyInsurenceCustomer = item.BuyInsurenceCustomer;
            outLoanBrief.LoanTime = item.LoanTime;
            outLoanBrief.Frequency = item.Frequency;
            outLoanBrief.RateMoney = item.RateMoney;
            outLoanBrief.RatePercent = item.RatePercent;
            outLoanBrief.ReceivingMoneyType = item.ReceivingMoneyType;
            outLoanBrief.BankId = item.BankId;
            outLoanBrief.BankAccountNumber = item.BankAccountNumber;
            outLoanBrief.BankCardNumber = item.BankCardNumber;
            outLoanBrief.BankAccountName = item.BankAccountName;
            outLoanBrief.IsLocate = item.IsLocate;
            outLoanBrief.TypeRemarketing = item.TypeRemarketing;
            outLoanBrief.FromDate = item.FromDate;
            outLoanBrief.ToDate = item.ToDate;
            outLoanBrief.CreatedTime = item.CreatedTime;
            outLoanBrief.IsHeadOffice = item.IsHeadOffice;
            outLoanBrief.HubId = item.HubId;
            outLoanBrief.CreateBy = item.CreateBy;
            outLoanBrief.BoundTelesaleId = item.BoundTelesaleId;
            outLoanBrief.HubEmployeeId = item.HubEmployeeId;
            outLoanBrief.PresenterCode = item.PresenterCode;
            outLoanBrief.LoanAmountFirst = item.LoanAmountFirst;
            outLoanBrief.LoanAmountExpertise = item.LoanAmountExpertise;
            outLoanBrief.LoanAmountExpertiseLast = item.LoanAmountExpertiseLast;
            outLoanBrief.LoanAmountExpertiseAi = item.LoanAmountExpertiseAi;
            outLoanBrief.FirstProcessingTime = item.FirstProcessingTime;
            outLoanBrief.LoanPurpose = item.LoanPurpose;
            outLoanBrief.ValueCheckQualify = item.ValueCheckQualify;
            outLoanBrief.ProductDetailId = item.ProductDetailId;
            #endregion

            #region map customer
            outLoanBrief.Customer = new Customer();
            outLoanBrief.Customer.FacebookAddress = item.CustomerModel != null ? item.CustomerModel.FacebookAddress : "";
            outLoanBrief.Customer.InsurenceNumber = item.CustomerModel != null ? item.CustomerModel.InsurenceNumber : "";
            outLoanBrief.Customer.InsurenceNote = item.CustomerModel != null ? item.CustomerModel.InsurenceNote : "";
            outLoanBrief.Customer.IsMerried = item.CustomerModel != null ? item.CustomerModel.IsMerried : null;
            outLoanBrief.Customer.NumberBaby = item.CustomerModel != null ? item.CustomerModel.NumberBaby : null;
            #endregion

            #region map LoanBriefResident
            outLoanBrief.LoanBriefResident = new LoanBriefResident();
            outLoanBrief.LoanBriefResident.LivingTime = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.LivingTime : 0;
            outLoanBrief.LoanBriefResident.ResidentType = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.ResidentType : 0;
            outLoanBrief.LoanBriefResident.LivingWith = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.LivingWith : 0;
            outLoanBrief.LoanBriefResident.Address = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.Address : "";
            outLoanBrief.LoanBriefResident.LoanBriefResidentId = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.LoanBriefResidentId : 0;
            outLoanBrief.LoanBriefResident.AddressGoogleMap = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.AddressGoogleMap : "";
            outLoanBrief.LoanBriefResident.AddressLatLng = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.AddressLatLng : "";
            outLoanBrief.LoanBriefResident.BillElectricityId = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.BillElectricityId : "";
            outLoanBrief.LoanBriefResident.BillWaterId = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.BillWaterId : "";
            #endregion

            #region map LoanBriefHouseholdModel
            outLoanBrief.LoanBriefHousehold = new LoanBriefHousehold();
            outLoanBrief.LoanBriefHousehold.Address = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.Address : "";
            outLoanBrief.LoanBriefHousehold.ProvinceId = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.ProvinceId : 0;
            outLoanBrief.LoanBriefHousehold.DistrictId = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.DistrictId : 0;
            outLoanBrief.LoanBriefHousehold.WardId = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.WardId : 0;
            outLoanBrief.LoanBriefHousehold.FullNameHouseOwner = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.FullNameHouseOwner : null;
            outLoanBrief.LoanBriefHousehold.RelationshipHouseOwner = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.RelationshipHouseOwner : null;
            outLoanBrief.LoanBriefHousehold.BirdayHouseOwner = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.BirdayHouseOwner : null;
            #endregion

            #region map LoanBriefProperty
            outLoanBrief.LoanBriefProperty = new LoanBriefProperty();
            outLoanBrief.LoanBriefProperty.BrandId = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.BrandId : 0;
            outLoanBrief.LoanBriefProperty.ProductId = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.ProductId : 0;
            outLoanBrief.LoanBriefProperty.PlateNumber = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.PlateNumber : "";
            outLoanBrief.LoanBriefProperty.CarManufacturer = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.CarManufacturer : "";
            outLoanBrief.LoanBriefProperty.CarName = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.CarName : "";
            outLoanBrief.LoanBriefProperty.CarColor = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.CarColor : "";
            outLoanBrief.LoanBriefProperty.PlateNumberCar = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.PlateNumberCar : "";
            outLoanBrief.LoanBriefProperty.BuyInsuranceProperty = item.LoanBriefPropertyModel != null ? item.LoanBriefPropertyModel.BuyInsuranceProperty : false;
            #endregion

            #region map LoanBriefJobModel
            outLoanBrief.LoanBriefJob = new LoanBriefJob();
            outLoanBrief.LoanBriefJob.LoanBriefJobId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.LoanBriefJobId : 0;
            outLoanBrief.LoanBriefJob.JobId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.JobId : 0;
            outLoanBrief.LoanBriefJob.CompanyName = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyName : "";
            outLoanBrief.LoanBriefJob.CompanyPhone = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyPhone : "";
            outLoanBrief.LoanBriefJob.CompanyTaxCode = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyTaxCode : "";
            outLoanBrief.LoanBriefJob.CompanyProvinceId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyProvinceId : 0;
            outLoanBrief.LoanBriefJob.CompanyDistrictId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyDistrictId : 0;
            outLoanBrief.LoanBriefJob.CompanyWardId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyWardId : 0;
            outLoanBrief.LoanBriefJob.CompanyAddress = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyAddress : "";
            outLoanBrief.LoanBriefJob.TotalIncome = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.TotalIncome : 0;
            outLoanBrief.LoanBriefJob.ImcomeType = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.ImcomeType : 0;
            outLoanBrief.LoanBriefJob.Description = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.Description : "";
            outLoanBrief.LoanBriefJob.CompanyAddressGoogleMap = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyAddressGoogleMap : "";
            outLoanBrief.LoanBriefJob.CompanyAddressLatLng = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyAddressLatLng : "";
            outLoanBrief.LoanBriefJob.CompanyInsurance = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyInsurance : null;
            outLoanBrief.LoanBriefJob.BusinessPapers = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.BusinessPapers : null;
            outLoanBrief.LoanBriefJob.WorkLocation = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.WorkLocation : null;
            outLoanBrief.LoanBriefJob.JobDescriptionId = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.JobDescriptionId : null;
            #endregion

            #region map LoanBriefCompany
            if (item.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
            {
                outLoanBrief.LoanBriefCompany = new LoanBriefCompany();
                outLoanBrief.LoanBriefCompany.LoanBriefId = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.LoanBriefId : 0;
                outLoanBrief.LoanBriefCompany.CompanyName = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.CompanyName : "";
                outLoanBrief.LoanBriefCompany.CareerBusiness = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.CareerBusiness : "";
                outLoanBrief.LoanBriefCompany.BusinessCertificationAddress = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.BusinessCertificationAddress : "";
                outLoanBrief.LoanBriefCompany.HeadOffice = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.HeadOffice : "";
                outLoanBrief.LoanBriefCompany.Address = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.Address : "";
                outLoanBrief.LoanBriefCompany.CompanyShareholder = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.CompanyShareholder : "";
                if (!string.IsNullOrEmpty(item.LoanBriefCompanyModel.sCardNumberShareholderDate))
                    outLoanBrief.LoanBriefCompany.CardNumberShareholderDate = DateTimeUtility.ConvertStringToDate(item.LoanBriefCompanyModel.sCardNumberShareholderDate, "dd/MM/yyyy");
                if (!string.IsNullOrEmpty(item.LoanBriefCompanyModel.sBusinessCertificationDate))
                    outLoanBrief.LoanBriefCompany.BusinessCertificationDate = DateTimeUtility.ConvertStringToDate(item.LoanBriefCompanyModel.sBusinessCertificationDate, "dd/MM/yyyy");
                if (!string.IsNullOrEmpty(item.LoanBriefCompanyModel.sBirthdayShareholder))
                    outLoanBrief.LoanBriefCompany.BirthdayShareholder = DateTimeUtility.ConvertStringToDate(item.LoanBriefCompanyModel.sBirthdayShareholder, "dd/MM/yyyy");
                outLoanBrief.LoanBriefCompany.CardNumberShareholder = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.CardNumberShareholder : "";
                outLoanBrief.LoanBriefCompany.PlaceOfBirthShareholder = item.LoanBriefCompanyModel != null ? item.LoanBriefCompanyModel.PlaceOfBirthShareholder : "";
            }
            #endregion

            #region map LoanBriefRelationshipModel
            //outLoanBrief.LoanBriefRelationship = new List<LoanBriefRelationship>();
            //if (item.LoanBriefRelationshipModels != null && item.LoanBriefRelationshipModels.Count > 0)
            //{
            //    foreach (var relationship in item.LoanBriefRelationshipModels)
            //    {
            //        outLoanBrief.LoanBriefRelationship.Add(new LoanBriefRelationship()
            //        {
            //            Id = relationship.Id,
            //            RelationshipType = relationship.RelationshipType,
            //            FullName = relationship.FullName,
            //            Phone = relationship.Phone,
            //            Address = relationship.Address,
            //        });
            //    }
            //    if (item.LoanBriefRelationshipModels.Count == 1)
            //        outLoanBrief.LoanBriefRelationship.Add(new LoanBriefRelationship());
            //}
            //else
            //{
            //    outLoanBrief.LoanBriefRelationship.Add(new LoanBriefRelationship());
            //    outLoanBrief.LoanBriefRelationship.Add(new LoanBriefRelationship());
            //}
            #endregion

            #region map LoanBriefQuestionScript
            outLoanBrief.LoanBriefQuestionScript = new LoanBriefQuestionScript();
            outLoanBrief.LoanBriefQuestionScript.LoanBriefId = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.LoanBriefId : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionUseMotobikeGo = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionMotobikeCertificate = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionAccountBank = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionAccountBank : null;
            outLoanBrief.LoanBriefQuestionScript.TypeJob = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.TypeJob : null;
            outLoanBrief.LoanBriefQuestionScript.AdvisoryDeductInsurance = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.AdvisoryDeductInsurance : null;
            outLoanBrief.LoanBriefQuestionScript.AdvisoryKeepRegisteredMotobike = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.AdvisoryKeepRegisteredMotobike : null;
            outLoanBrief.LoanBriefQuestionScript.AdvisorySettingGps = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.AdvisorySettingGps : null;
            outLoanBrief.LoanBriefQuestionScript.ChooseCareer = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.ChooseCareer : null;
            outLoanBrief.LoanBriefQuestionScript.Signs = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.Signs : null;
            outLoanBrief.LoanBriefQuestionScript.AddressBusiness = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.AddressBusiness : null;
            outLoanBrief.LoanBriefQuestionScript.WorkingTime = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.WorkingTime : null;
            outLoanBrief.LoanBriefQuestionScript.DriverOfCompany = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.DriverOfCompany : null;
            outLoanBrief.LoanBriefQuestionScript.AccountDriver = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.AccountDriver : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionDriverGo = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionDriverGo : null;
            outLoanBrief.LoanBriefQuestionScript.TimeDriver = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.TimeDriver : null;
            outLoanBrief.LoanBriefQuestionScript.ContractInShop = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.ContractInShop : null;
            outLoanBrief.LoanBriefQuestionScript.PhotographShop = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.PhotographShop : null;
            outLoanBrief.LoanBriefQuestionScript.CommentStatusTelesales = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.CommentStatusTelesales : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionBorrow = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionBorrow : null;
            outLoanBrief.LoanBriefQuestionScript.SigningAlaborContract = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.SigningAlaborContract : null;
            outLoanBrief.LoanBriefQuestionScript.TypeWork = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.TypeWork : null;
            outLoanBrief.LoanBriefQuestionScript.LatestPayday = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.LatestPayday : null;
            outLoanBrief.LoanBriefQuestionScript.NameWeb = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.NameWeb : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionWarehouse = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionWarehouse : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionImageGoods = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionImageGoods : null;
            outLoanBrief.LoanBriefQuestionScript.DetailJob = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.DetailJob : null;
            outLoanBrief.LoanBriefQuestionScript.TimeSellWeb = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.TimeSellWeb : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionCarOwnership = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionCarOwnership : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionChangeColorMotobike = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionChangeColorMotobike : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionUseMotobikeGoNormal = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionUseMotobikeGoNormal : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionAddress = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionAddress : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionCheckCurrentStatusMotobike = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionCheckCurrentStatusMotobike : null;
            outLoanBrief.LoanBriefQuestionScript.TypeOwnerShip = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.TypeOwnerShip : null;
            outLoanBrief.LoanBriefQuestionScript.DocumentCoincideHouseHold = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.DocumentCoincideHouseHold : null;
            outLoanBrief.LoanBriefQuestionScript.DocumentNoCoincideHouseHold = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.DocumentNoCoincideHouseHold : null;
            outLoanBrief.LoanBriefQuestionScript.StartWokingJob = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.StartWokingJob : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionNowInWorkingJob = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionNowInWorkingJob : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionImageWorkplace = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionImageWorkplace : null;
            outLoanBrief.LoanBriefQuestionScript.DocumentSalaryBankTransfer = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.DocumentSalaryBankTransfer : null;
            outLoanBrief.LoanBriefQuestionScript.DocumetSalaryCash = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.DocumetSalaryCash : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionLicenseDkkd = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionLicenseDkkd : null;
            outLoanBrief.LoanBriefQuestionScript.CommentJob = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.CommentJob : null;
            outLoanBrief.LoanBriefQuestionScript.DocmentRelationship = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.DocmentRelationship : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionBlurredSignCertificate = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionBlurredSignCertificate : null;
            outLoanBrief.LoanBriefQuestionScript.HourOpenDoor = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.HourOpenDoor : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionProvidedImageOrVideo = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionProvidedImageOrVideo : null;
            outLoanBrief.LoanBriefQuestionScript.WaterSupplier = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.WaterSupplier : null;
            outLoanBrief.LoanBriefQuestionScript.RemindCustomerDocument = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.RemindCustomerDocument : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionHouseOwner = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionHouseOwner : null;
            outLoanBrief.LoanBriefQuestionScript.DocumentFormsOfResidence = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.DocumentFormsOfResidence : null;
            outLoanBrief.LoanBriefQuestionScript.BusinessStatus = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.BusinessStatus : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionBusinessLocation = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionBusinessLocation : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionImageAppAccountRevenue = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionImageAppAccountRevenue : null;
            outLoanBrief.LoanBriefQuestionScript.DocumentBusiness = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.DocumentBusiness : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionImageRegistrationBook = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionImageRegistrationBook : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionAddressCoincideAreaSupport = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionAddressCoincideAreaSupport : null;
            outLoanBrief.LoanBriefQuestionScript.Appraiser = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.Appraiser : null;
            outLoanBrief.LoanBriefQuestionScript.MaxPrice = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.MaxPrice : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionOriginCardNumber = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionOriginCardNumber : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionOriginCavet = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionOriginCavet : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionMotobikeNearby = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionMotobikeNearby : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionDocumentJob = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionDocumentJob : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionCustomerBorrowProductFast4tr = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionCustomerBorrowProductFast4tr : null;
            outLoanBrief.LoanBriefQuestionScript.LoanAmountFirst = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.LoanAmountFirst : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionSupportArea = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionSupportArea : null;
            outLoanBrief.LoanBriefQuestionScript.OwnerCar = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.OwnerCar : null;
            outLoanBrief.LoanBriefQuestionScript.QuestionSaleContractCar = item.LoanBriefQuestionScriptModel != null ? item.LoanBriefQuestionScriptModel.QuestionSaleContractCar : null;
            #endregion

            return outLoanBrief;
        }

        public static LoanbriefScriptViewModel MapToLoanbriefScriptViewModelV2(this LoanBriefInitDetail item)
        {
            #region map Loanbrief
            var outLoanBrief = new LoanbriefScriptViewModel();
            outLoanBrief.LoanBriefId = item.LoanBriefId;
            outLoanBrief.CustomerId = item.CustomerId.GetValueOrDefault();
            outLoanBrief.PlatformType = item.PlatformType.GetValueOrDefault();
            outLoanBrief.TypeLoanBrief = item.TypeLoanBrief.GetValueOrDefault();
            outLoanBrief.FullName = item.FullName;
            outLoanBrief.Phone = item.Phone;
            outLoanBrief.NationalCard = item.NationalCard;
            outLoanBrief.NationCardPlace = item.NationCardPlace;
            outLoanBrief.IsTrackingLocation = item.IsTrackingLocation;
            outLoanBrief.sBirthDay = item.Dob.HasValue ? item.Dob.Value.ToString("dd/MM/yyyy") : "";
            outLoanBrief.Passport = item.Passport;
            outLoanBrief.Gender = item.Gender;
            outLoanBrief.NumberCall = item.NumberCall;
            outLoanBrief.ProvinceId = item.ProvinceId;
            outLoanBrief.ProvinceName = item.ProvinceName;
            outLoanBrief.DistrictName = item.DistrictName;
            outLoanBrief.WardName = item.WardName;
            outLoanBrief.DistrictId = item.DistrictId;
            outLoanBrief.WardId = item.WardId;
            outLoanBrief.ProductId = item.ProductId.GetValueOrDefault();
            outLoanBrief.RateTypeId = item.RateTypeId.GetValueOrDefault();
            outLoanBrief.LoanAmount = item.LoanAmount.GetValueOrDefault();
            outLoanBrief.BuyInsurenceCustomer = item.BuyInsurenceCustomer;
            outLoanBrief.LoanTime = item.LoanTime;
            outLoanBrief.Frequency = item.Frequency;
            outLoanBrief.RateMoney = item.RateMoney;
            outLoanBrief.RatePercent = item.RatePercent;
            outLoanBrief.ReceivingMoneyType = item.ReceivingMoneyType;
            outLoanBrief.BankId = item.BankId;
            outLoanBrief.BankAccountNumber = item.BankAccountNumber;
            outLoanBrief.BankCardNumber = item.BankCardNumber;
            outLoanBrief.BankAccountName = item.BankAccountName;
            outLoanBrief.IsLocate = item.IsLocate;
            outLoanBrief.TypeRemarketing = item.TypeRemarketing;
            outLoanBrief.FromDate = item.FromDate;
            outLoanBrief.ToDate = item.ToDate;
            outLoanBrief.CreatedTime = item.CreatedTime.HasValue ? item.CreatedTime.Value.DateTime : DateTime.MinValue;
            outLoanBrief.IsHeadOffice = item.IsHeadOffice;
            outLoanBrief.HubId = item.HubId;
            outLoanBrief.CreateBy = item.CreateBy;
            outLoanBrief.BoundTelesaleId = item.BoundTelesaleId;
            outLoanBrief.HubEmployeeId = item.HubEmployeeId;
            outLoanBrief.PresenterCode = item.PresenterCode;
            outLoanBrief.LoanAmountFirst = item.LoanAmountFirst;
            outLoanBrief.LoanAmountExpertise = item.LoanAmountExpertise;
            outLoanBrief.LoanAmountExpertiseLast = item.LoanAmountExpertiseLast;
            outLoanBrief.LoanAmountExpertiseAi = item.LoanAmountExpertiseAi;
            outLoanBrief.FirstProcessingTime = item.FirstProcessingTime;
            outLoanBrief.ProductType = item.ProductType;
            outLoanBrief.LoanPurpose = item.LoanPurpose;
            outLoanBrief.ValueCheckQualify = item.ValueCheckQualify;
            outLoanBrief.ProductDetailId = item.ProductDetailId;
            outLoanBrief.ReMarketingLoanBriefId = item.ReMarketingLoanBriefId;
            outLoanBrief.UtmSource = item.UtmSource;
            #endregion

            #region map customer
            outLoanBrief.CustomerModel.FacebookAddress = item.Customer != null ? item.Customer.FacebookAddress : "";
            outLoanBrief.CustomerModel.InsurenceNumber = item.Customer != null ? item.Customer.InsurenceNumber : "";
            outLoanBrief.CustomerModel.InsurenceNote = item.Customer != null ? item.Customer.InsurenceNote : "";
            outLoanBrief.CustomerModel.IsMerried = item.Customer != null ? item.Customer.IsMerried : null;
            outLoanBrief.CustomerModel.NumberBaby = item.Customer != null ? item.Customer.NumberBaby : null;
            #endregion

            #region map LoanBriefResident
            outLoanBrief.LoanBriefResidentModel.LivingTime = item.LoanBriefResident != null ? item.LoanBriefResident.LivingTime : 0;
            outLoanBrief.LoanBriefResidentModel.ResidentType = item.LoanBriefResident != null ? item.LoanBriefResident.ResidentType : 0;
            outLoanBrief.LoanBriefResidentModel.LivingWith = item.LoanBriefResident != null ? item.LoanBriefResident.LivingWith : 0;
            outLoanBrief.LoanBriefResidentModel.Address = item.LoanBriefResident != null ? item.LoanBriefResident.Address : "";
            outLoanBrief.LoanBriefResidentModel.LoanBriefResidentId = item.LoanBriefResident != null ? item.LoanBriefResident.LoanBriefResidentId : 0;
            outLoanBrief.LoanBriefResidentModel.Address = item.LoanBriefResident != null ? item.LoanBriefResident.Address : "";
            outLoanBrief.LoanBriefResidentModel.AddressGoogleMap = item.LoanBriefResident != null ? item.LoanBriefResident.AddressGoogleMap : "";
            outLoanBrief.LoanBriefResidentModel.AddressLatLng = item.LoanBriefResident != null ? item.LoanBriefResident.AddressLatLng : "";
            outLoanBrief.LoanBriefResidentModel.BillElectricityId = item.LoanBriefResident != null ? item.LoanBriefResident.BillElectricityId : "";
            outLoanBrief.LoanBriefResidentModel.BillWaterId = item.LoanBriefResident != null ? item.LoanBriefResident.BillWaterId : "";
            #endregion

            #region map LoanBriefHouseholdModel
            outLoanBrief.LoanBriefHouseholdModel.Address = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.Address : "";
            outLoanBrief.LoanBriefHouseholdModel.ProvinceId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.ProvinceId : 0;
            outLoanBrief.LoanBriefHouseholdModel.DistrictId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.DistrictId : 0;
            outLoanBrief.LoanBriefHouseholdModel.WardId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.WardId : 0;
            outLoanBrief.LoanBriefHouseholdModel.FullNameHouseOwner = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.FullNameHouseOwner : null;
            outLoanBrief.LoanBriefHouseholdModel.RelationshipHouseOwner = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.RelationshipHouseOwner : null;
            outLoanBrief.LoanBriefHouseholdModel.BirdayHouseOwner = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.BirdayHouseOwner : null;
            #endregion

            #region map LoanBriefProperty
            outLoanBrief.LoanBriefPropertyModel.BrandId = item.LoanBriefProperty != null ? item.LoanBriefProperty.BrandId : 0;
            outLoanBrief.LoanBriefPropertyModel.ProductId = item.LoanBriefProperty != null ? item.LoanBriefProperty.ProductId : 0;
            outLoanBrief.LoanBriefPropertyModel.PlateNumber = item.LoanBriefProperty != null ? item.LoanBriefProperty.PlateNumber : "";
            outLoanBrief.LoanBriefPropertyModel.CarManufacturer = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarManufacturer : "";
            outLoanBrief.LoanBriefPropertyModel.CarName = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarName : "";
            outLoanBrief.LoanBriefPropertyModel.CarColor = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarColor : "";
            outLoanBrief.LoanBriefPropertyModel.PlateNumberCar = item.LoanBriefProperty != null ? item.LoanBriefProperty.PlateNumberCar : "";
            #endregion

            #region map LoanBriefJobModel
            outLoanBrief.LoanBriefJobModel.LoanBriefJobId = item.LoanBriefJob != null ? item.LoanBriefJob.LoanBriefJobId : 0;
            outLoanBrief.LoanBriefJobModel.JobId = item.LoanBriefJob != null ? item.LoanBriefJob.JobId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyName = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyName : "";
            outLoanBrief.LoanBriefJobModel.CompanyPhone = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyPhone : "";
            outLoanBrief.LoanBriefJobModel.CompanyTaxCode = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyTaxCode : "";
            outLoanBrief.LoanBriefJobModel.CompanyProvinceId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyProvinceId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyDistrictId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyDistrictId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyWardId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyWardId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyAddress = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddress : "";
            outLoanBrief.LoanBriefJobModel.TotalIncome = item.LoanBriefJob != null ? item.LoanBriefJob.TotalIncome : 0;
            outLoanBrief.LoanBriefJobModel.ImcomeType = item.LoanBriefJob != null ? item.LoanBriefJob.ImcomeType : 0;
            outLoanBrief.LoanBriefJobModel.Description = item.LoanBriefJob != null ? item.LoanBriefJob.Description : "";
            outLoanBrief.LoanBriefJobModel.CompanyAddressGoogleMap = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddressGoogleMap : "";
            outLoanBrief.LoanBriefJobModel.CompanyAddressLatLng = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddressLatLng : "";
            outLoanBrief.LoanBriefJobModel.CompanyInsurance = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyInsurance : null;
            outLoanBrief.LoanBriefJobModel.BusinessPapers = item.LoanBriefJob != null ? item.LoanBriefJob.BusinessPapers : null;
            outLoanBrief.LoanBriefJobModel.WorkLocation = item.LoanBriefJob != null ? item.LoanBriefJob.WorkLocation : null;
            outLoanBrief.LoanBriefJobModel.JobDescriptionId = item.LoanBriefJob != null ? item.LoanBriefJob.JobDescriptionId : null;
            #endregion

            #region map LoanBriefCompany
            if (item.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
            {
                outLoanBrief.LoanBriefCompanyModel.LoanBriefId = item.LoanBriefCompany != null ? item.LoanBriefCompany.LoanBriefId : 0;
                outLoanBrief.LoanBriefCompanyModel.CompanyName = item.LoanBriefCompany != null ? item.LoanBriefCompany.CompanyName : "";
                outLoanBrief.LoanBriefCompanyModel.CareerBusiness = item.LoanBriefCompany != null ? item.LoanBriefCompany.CareerBusiness : "";
                outLoanBrief.LoanBriefCompanyModel.BusinessCertificationAddress = item.LoanBriefCompany != null ? item.LoanBriefCompany.BusinessCertificationAddress : "";
                outLoanBrief.LoanBriefCompanyModel.HeadOffice = item.LoanBriefCompany != null ? item.LoanBriefCompany.HeadOffice : "";
                outLoanBrief.LoanBriefCompanyModel.Address = item.LoanBriefCompany != null ? item.LoanBriefCompany.Address : "";
                outLoanBrief.LoanBriefCompanyModel.CompanyShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.CompanyShareholder : "";
                outLoanBrief.LoanBriefCompanyModel.sCardNumberShareholderDate = item.LoanBriefCompany != null && item.LoanBriefCompany.CardNumberShareholderDate.HasValue
                    ? item.LoanBriefCompany.CardNumberShareholderDate.Value.ToString("dd/MM/yyyy") : "";
                outLoanBrief.LoanBriefCompanyModel.sBusinessCertificationDate = item.LoanBriefCompany != null && item.LoanBriefCompany.BusinessCertificationDate.HasValue
                    ? item.LoanBriefCompany.BusinessCertificationDate.Value.ToString("dd/MM/yyyy") : "";
                outLoanBrief.LoanBriefCompanyModel.sBirthdayShareholder = item.LoanBriefCompany != null && item.LoanBriefCompany.BirthdayShareholder.HasValue
                    ? item.LoanBriefCompany.BirthdayShareholder.Value.ToString("dd/MM/yyyy") : "";
                outLoanBrief.LoanBriefCompanyModel.CardNumberShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.CardNumberShareholder : "";
                outLoanBrief.LoanBriefCompanyModel.PlaceOfBirthShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.PlaceOfBirthShareholder : "";
            }
            #endregion

            #region map LoanBriefRelationshipModel
            if (item.LoanBriefRelationship != null && item.LoanBriefRelationship.Count > 0)
            {
                foreach (var relationship in item.LoanBriefRelationship)
                {
                    if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                        outLoanBrief.LoanBriefRelationshipModels.Add(new LoanBriefRelationshipModel()
                        {
                            Id = relationship.Id,
                            RelationshipType = relationship.RelationshipType,
                            FullName = relationship.FullName,
                            Phone = relationship.Phone,
                            Address = relationship.Address,
                            RefPhoneCallRate = relationship.RefPhoneCallRate
                        });
                }

            }


            #endregion

            #region map LoanBriefQuestionScript
            outLoanBrief.LoanBriefQuestionScriptModel.LoanBriefId = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.LoanBriefId : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionUseMotobikeGo = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionUseMotobikeGo : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionMotobikeCertificate = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionMotobikeCertificate : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionAccountBank = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionAccountBank : null;
            outLoanBrief.LoanBriefQuestionScriptModel.TypeJob = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.TypeJob : null;
            outLoanBrief.LoanBriefQuestionScriptModel.AdvisoryDeductInsurance = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.AdvisoryDeductInsurance : null;
            outLoanBrief.LoanBriefQuestionScriptModel.AdvisoryKeepRegisteredMotobike = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.AdvisoryKeepRegisteredMotobike : null;
            outLoanBrief.LoanBriefQuestionScriptModel.AdvisorySettingGps = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.AdvisorySettingGps : null;
            outLoanBrief.LoanBriefQuestionScriptModel.ChooseCareer = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.ChooseCareer : null;
            outLoanBrief.LoanBriefQuestionScriptModel.Signs = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.Signs : null;
            outLoanBrief.LoanBriefQuestionScriptModel.AddressBusiness = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.AddressBusiness : null;
            outLoanBrief.LoanBriefQuestionScriptModel.WorkingTime = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.WorkingTime : null;
            outLoanBrief.LoanBriefQuestionScriptModel.DriverOfCompany = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.DriverOfCompany : null;
            outLoanBrief.LoanBriefQuestionScriptModel.AccountDriver = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.AccountDriver : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionDriverGo = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionDriverGo : null;
            outLoanBrief.LoanBriefQuestionScriptModel.TimeDriver = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.TimeDriver : null;
            outLoanBrief.LoanBriefQuestionScriptModel.ContractInShop = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.ContractInShop : null;
            outLoanBrief.LoanBriefQuestionScriptModel.PhotographShop = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.PhotographShop : null;
            outLoanBrief.LoanBriefQuestionScriptModel.CommentStatusTelesales = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.CommentStatusTelesales : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionBorrow = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionBorrow : null;
            outLoanBrief.LoanBriefQuestionScriptModel.SigningAlaborContract = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.SigningAlaborContract : null;
            outLoanBrief.LoanBriefQuestionScriptModel.TypeWork = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.TypeWork : null;
            outLoanBrief.LoanBriefQuestionScriptModel.LatestPayday = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.LatestPayday : null;
            outLoanBrief.LoanBriefQuestionScriptModel.NameWeb = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.NameWeb : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionWarehouse = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionWarehouse : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionImageGoods = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionImageGoods : null;
            outLoanBrief.LoanBriefQuestionScriptModel.DetailJob = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.DetailJob : null;
            outLoanBrief.LoanBriefQuestionScriptModel.TimeSellWeb = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.TimeSellWeb : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionCarOwnership = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionCarOwnership : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionChangeColorMotobike = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionChangeColorMotobike : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionUseMotobikeGoNormal = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionUseMotobikeGoNormal : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionAddress = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionAddress : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionCheckCurrentStatusMotobike = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionCheckCurrentStatusMotobike : null;
            outLoanBrief.LoanBriefQuestionScriptModel.TypeOwnerShip = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.TypeOwnerShip : null;
            outLoanBrief.LoanBriefQuestionScriptModel.DocumentCoincideHouseHold = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.DocumentCoincideHouseHold : null;
            outLoanBrief.LoanBriefQuestionScriptModel.DocumentNoCoincideHouseHold = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.DocumentNoCoincideHouseHold : null;
            outLoanBrief.LoanBriefQuestionScriptModel.StartWokingJob = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.StartWokingJob : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionNowInWorkingJob = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionNowInWorkingJob : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionImageWorkplace = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionImageWorkplace : null;
            outLoanBrief.LoanBriefQuestionScriptModel.DocumentSalaryBankTransfer = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.DocumentSalaryBankTransfer : null;
            outLoanBrief.LoanBriefQuestionScriptModel.DocumetSalaryCash = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.DocumetSalaryCash : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionLicenseDkkd = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionLicenseDkkd : null;
            outLoanBrief.LoanBriefQuestionScriptModel.CommentJob = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.CommentJob : null;
            outLoanBrief.LoanBriefQuestionScriptModel.DocmentRelationship = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.DocmentRelationship : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionBlurredSignCertificate = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionBlurredSignCertificate : null;
            outLoanBrief.LoanBriefQuestionScriptModel.HourOpenDoor = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.HourOpenDoor : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionProvidedImageOrVideo = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionProvidedImageOrVideo : null;
            outLoanBrief.LoanBriefQuestionScriptModel.WaterSupplier = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.WaterSupplier : null;
            outLoanBrief.LoanBriefQuestionScriptModel.RemindCustomerDocument = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.RemindCustomerDocument : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionHouseOwner = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionHouseOwner : null;
            outLoanBrief.LoanBriefQuestionScriptModel.DocumentFormsOfResidence = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.DocumentFormsOfResidence : null;
            outLoanBrief.LoanBriefQuestionScriptModel.BusinessStatus = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.BusinessStatus : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionBusinessLocation = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionBusinessLocation : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionImageAppAccountRevenue = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionImageAppAccountRevenue : null;
            outLoanBrief.LoanBriefQuestionScriptModel.DocumentBusiness = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.DocumentBusiness : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionImageRegistrationBook = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionImageRegistrationBook : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionAddressCoincideAreaSupport = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionAddressCoincideAreaSupport : null;
            outLoanBrief.LoanBriefQuestionScriptModel.Appraiser = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.Appraiser : null;
            outLoanBrief.LoanBriefQuestionScriptModel.MaxPrice = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.MaxPrice : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionOriginCardNumber = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionOriginCardNumber : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionOriginCavet = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionOriginCavet : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionMotobikeNearby = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionMotobikeNearby : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionDocumentJob = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionDocumentJob : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionCustomerBorrowProductFast4tr = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionCustomerBorrowProductFast4tr : null;
            outLoanBrief.LoanBriefQuestionScriptModel.LoanAmountFirst = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.LoanAmountFirst : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionSupportArea = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionSupportArea : null;
            outLoanBrief.LoanBriefQuestionScriptModel.OwnerCar = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.OwnerCar : null;
            outLoanBrief.LoanBriefQuestionScriptModel.QuestionSaleContractCar = item.LoanBriefQuestionScript != null ? item.LoanBriefQuestionScript.QuestionSaleContractCar : null;
            #endregion

            return outLoanBrief;
        }

        public static LoanbriefViewModel MapToLoanbriefViewModelV2(this LoanBriefInitDetail item)
        {
            #region map Loanbrief
            var outLoanBrief = new LoanbriefViewModel();
            outLoanBrief.LoanBriefId = item.LoanBriefId;
            outLoanBrief.CustomerId = item.CustomerId.GetValueOrDefault();
            outLoanBrief.PlatformType = item.PlatformType.GetValueOrDefault();
            outLoanBrief.TypeLoanBrief = item.TypeLoanBrief.GetValueOrDefault();
            outLoanBrief.FullName = item.FullName;
            outLoanBrief.Phone = item.Phone;
            outLoanBrief.NationalCard = item.NationalCard;
            outLoanBrief.NationCardPlace = item.NationCardPlace;
            outLoanBrief.IsTrackingLocation = item.IsTrackingLocation;
            outLoanBrief.sBirthDay = item.Dob.HasValue ? item.Dob.Value.ToString("dd/MM/yyyy") : "";
            outLoanBrief.Passport = item.Passport;
            outLoanBrief.Gender = item.Gender;
            outLoanBrief.NumberCall = item.NumberCall;
            outLoanBrief.ProvinceId = item.ProvinceId;
            outLoanBrief.ProvinceName = item.ProvinceName;
            outLoanBrief.DistrictName = item.DistrictName;
            outLoanBrief.WardName = item.WardName;
            outLoanBrief.DistrictId = item.DistrictId;
            outLoanBrief.WardId = item.WardId;
            outLoanBrief.ProductId = item.ProductId.GetValueOrDefault();
            outLoanBrief.RateTypeId = item.RateTypeId.GetValueOrDefault();
            outLoanBrief.LoanAmount = item.LoanAmount.GetValueOrDefault();
            outLoanBrief.BuyInsurenceCustomer = item.BuyInsurenceCustomer;
            outLoanBrief.LoanTime = item.LoanTime;
            outLoanBrief.Frequency = item.Frequency;
            outLoanBrief.RateMoney = item.RateMoney;
            outLoanBrief.RatePercent = item.RatePercent;
            outLoanBrief.ReceivingMoneyType = item.ReceivingMoneyType;
            outLoanBrief.BankId = item.BankId;
            outLoanBrief.BankAccountNumber = item.BankAccountNumber;
            outLoanBrief.BankCardNumber = item.BankCardNumber;
            outLoanBrief.BankAccountName = item.BankAccountName;
            outLoanBrief.IsLocate = item.IsLocate;
            outLoanBrief.TypeRemarketing = item.TypeRemarketing;
            outLoanBrief.FromDate = item.FromDate;
            outLoanBrief.ToDate = item.ToDate;
            outLoanBrief.CreatedTime = item.CreatedTime.HasValue ? item.CreatedTime.Value.DateTime : DateTime.MinValue;
            outLoanBrief.IsHeadOffice = item.IsHeadOffice;
            outLoanBrief.HubId = item.HubId;
            outLoanBrief.CreateBy = item.CreateBy;
            outLoanBrief.BoundTelesaleId = item.BoundTelesaleId;
            outLoanBrief.HubEmployeeId = item.HubEmployeeId;
            outLoanBrief.PresenterCode = item.PresenterCode;
            outLoanBrief.LoanAmountFirst = item.LoanAmountFirst;
            outLoanBrief.LoanAmountExpertise = item.LoanAmountExpertise;
            outLoanBrief.LoanAmountExpertiseLast = item.LoanAmountExpertiseLast;
            outLoanBrief.LoanAmountExpertiseAi = item.LoanAmountExpertiseAi;
            outLoanBrief.FirstProcessingTime = item.FirstProcessingTime;
            outLoanBrief.ProductType = item.ProductType;
            outLoanBrief.IsCheckBank = item.IsCheckBank;
            outLoanBrief.HomeNetwork = item.HomeNetwork;
            outLoanBrief.PhoneOther = item.PhoneOther;
            outLoanBrief.BankBranch = item.BankBranch;
            #endregion

            #region map customer
            outLoanBrief.CustomerModel.FacebookAddress = item.Customer != null ? item.Customer.FacebookAddress : "";
            outLoanBrief.CustomerModel.InsurenceNumber = item.Customer != null ? item.Customer.InsurenceNumber : "";
            outLoanBrief.CustomerModel.InsurenceNote = item.Customer != null ? item.Customer.InsurenceNote : "";
            outLoanBrief.CustomerModel.IsMerried = item.Customer != null ? item.Customer.IsMerried : null;
            outLoanBrief.CustomerModel.NumberBaby = item.Customer != null ? item.Customer.NumberBaby : null;
            #endregion

            #region map LoanBriefResident
            outLoanBrief.LoanBriefResidentModel.LivingTime = item.LoanBriefResident != null ? item.LoanBriefResident.LivingTime : 0;
            outLoanBrief.LoanBriefResidentModel.ResidentType = item.LoanBriefResident != null ? item.LoanBriefResident.ResidentType : 0;
            outLoanBrief.LoanBriefResidentModel.LivingWith = item.LoanBriefResident != null ? item.LoanBriefResident.LivingWith : 0;
            outLoanBrief.LoanBriefResidentModel.Address = item.LoanBriefResident != null ? item.LoanBriefResident.Address : "";
            outLoanBrief.LoanBriefResidentModel.LoanBriefResidentId = item.LoanBriefResident != null ? item.LoanBriefResident.LoanBriefResidentId : 0;
            outLoanBrief.LoanBriefResidentModel.AddressGoogleMap = item.LoanBriefResident != null ? item.LoanBriefResident.AddressGoogleMap : "";
            outLoanBrief.LoanBriefResidentModel.AddressLatLng = item.LoanBriefResident != null ? item.LoanBriefResident.AddressLatLng : "";
            outLoanBrief.LoanBriefResidentModel.BillElectricityId = item.LoanBriefResident != null ? item.LoanBriefResident.BillElectricityId : "";
            outLoanBrief.LoanBriefResidentModel.BillWaterId = item.LoanBriefResident != null ? item.LoanBriefResident.BillWaterId : "";
            #endregion

            #region map LoanBriefHouseholdModel
            outLoanBrief.LoanBriefHouseholdModel.Address = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.Address : "";
            outLoanBrief.LoanBriefHouseholdModel.ProvinceId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.ProvinceId : 0;
            outLoanBrief.LoanBriefHouseholdModel.DistrictId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.DistrictId : 0;
            outLoanBrief.LoanBriefHouseholdModel.WardId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.WardId : 0;
            #endregion

            #region map LoanBriefProperty
            outLoanBrief.LoanBriefPropertyModel.BrandId = item.LoanBriefProperty != null ? item.LoanBriefProperty.BrandId : 0;
            outLoanBrief.LoanBriefPropertyModel.ProductId = item.LoanBriefProperty != null ? item.LoanBriefProperty.ProductId : 0;
            outLoanBrief.LoanBriefPropertyModel.PlateNumber = item.LoanBriefProperty != null ? item.LoanBriefProperty.PlateNumber : "";
            outLoanBrief.LoanBriefPropertyModel.CarManufacturer = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarManufacturer : "";
            outLoanBrief.LoanBriefPropertyModel.CarName = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarName : "";
            outLoanBrief.LoanBriefPropertyModel.CarColor = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarColor : "";
            outLoanBrief.LoanBriefPropertyModel.PlateNumberCar = item.LoanBriefProperty != null ? item.LoanBriefProperty.PlateNumberCar : "";
            #endregion

            #region map LoanBriefJobModel
            outLoanBrief.LoanBriefJobModel.LoanBriefJobId = item.LoanBriefJob != null ? item.LoanBriefJob.LoanBriefJobId : 0;
            outLoanBrief.LoanBriefJobModel.JobId = item.LoanBriefJob != null ? item.LoanBriefJob.JobId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyName = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyName : "";
            outLoanBrief.LoanBriefJobModel.CompanyPhone = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyPhone : "";
            outLoanBrief.LoanBriefJobModel.CompanyTaxCode = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyTaxCode : "";
            outLoanBrief.LoanBriefJobModel.CompanyProvinceId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyProvinceId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyDistrictId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyDistrictId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyWardId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyWardId : 0;
            outLoanBrief.LoanBriefJobModel.CompanyAddress = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddress : "";
            outLoanBrief.LoanBriefJobModel.TotalIncome = item.LoanBriefJob != null ? item.LoanBriefJob.TotalIncome : 0;
            outLoanBrief.LoanBriefJobModel.ImcomeType = item.LoanBriefJob != null ? item.LoanBriefJob.ImcomeType : 0;
            outLoanBrief.LoanBriefJobModel.Description = item.LoanBriefJob != null ? item.LoanBriefJob.Description : "";
            outLoanBrief.LoanBriefJobModel.CompanyAddressGoogleMap = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddressGoogleMap : "";
            outLoanBrief.LoanBriefJobModel.CompanyAddressLatLng = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddressLatLng : "";
            outLoanBrief.LoanBriefJobModel.CompanyInsurance = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyInsurance : null;
            outLoanBrief.LoanBriefJobModel.WorkLocation = item.LoanBriefJob != null ? item.LoanBriefJob.WorkLocation : null;
            outLoanBrief.LoanBriefJobModel.BusinessPapers = item.LoanBriefJob != null ? item.LoanBriefJob.BusinessPapers : null;
            outLoanBrief.LoanBriefJobModel.CompanyInsurance = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyInsurance : null;
            outLoanBrief.LoanBriefJobModel.BusinessPapers = item.LoanBriefJob != null ? item.LoanBriefJob.BusinessPapers : null;
            outLoanBrief.LoanBriefJobModel.WorkLocation = item.LoanBriefJob != null ? item.LoanBriefJob.WorkLocation : null;
            outLoanBrief.LoanBriefJobModel.JobDescriptionId = item.LoanBriefJob != null ? item.LoanBriefJob.JobDescriptionId : null;

            #endregion

            #region map LoanBriefCompany
            if (item.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
            {
                outLoanBrief.LoanBriefCompanyModel.LoanBriefId = item.LoanBriefCompany != null ? item.LoanBriefCompany.LoanBriefId : 0;
                outLoanBrief.LoanBriefCompanyModel.CompanyName = item.LoanBriefCompany != null ? item.LoanBriefCompany.CompanyName : "";
                outLoanBrief.LoanBriefCompanyModel.CareerBusiness = item.LoanBriefCompany != null ? item.LoanBriefCompany.CareerBusiness : "";
                outLoanBrief.LoanBriefCompanyModel.BusinessCertificationAddress = item.LoanBriefCompany != null ? item.LoanBriefCompany.BusinessCertificationAddress : "";
                outLoanBrief.LoanBriefCompanyModel.HeadOffice = item.LoanBriefCompany != null ? item.LoanBriefCompany.HeadOffice : "";
                outLoanBrief.LoanBriefCompanyModel.Address = item.LoanBriefCompany != null ? item.LoanBriefCompany.Address : "";
                outLoanBrief.LoanBriefCompanyModel.CompanyShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.CompanyShareholder : "";
                outLoanBrief.LoanBriefCompanyModel.sCardNumberShareholderDate = item.LoanBriefCompany != null && item.LoanBriefCompany.CardNumberShareholderDate.HasValue
                    ? item.LoanBriefCompany.CardNumberShareholderDate.Value.ToString("dd/MM/yyyy") : "";
                outLoanBrief.LoanBriefCompanyModel.sBusinessCertificationDate = item.LoanBriefCompany != null && item.LoanBriefCompany.BusinessCertificationDate.HasValue
                    ? item.LoanBriefCompany.BusinessCertificationDate.Value.ToString("dd/MM/yyyy") : "";
                outLoanBrief.LoanBriefCompanyModel.sBirthdayShareholder = item.LoanBriefCompany != null && item.LoanBriefCompany.BirthdayShareholder.HasValue
                    ? item.LoanBriefCompany.BirthdayShareholder.Value.ToString("dd/MM/yyyy") : "";
                outLoanBrief.LoanBriefCompanyModel.CardNumberShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.CardNumberShareholder : "";
                outLoanBrief.LoanBriefCompanyModel.PlaceOfBirthShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.PlaceOfBirthShareholder : "";
            }
            #endregion

            #region map LoanBriefRelationshipModel
            if (item.LoanBriefRelationship != null && item.LoanBriefRelationship.Count > 0)
            {
                foreach (var relationship in item.LoanBriefRelationship)
                {
                    if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                        outLoanBrief.LoanBriefRelationshipModels.Add(new LoanBriefRelationshipModel()
                        {
                            Id = relationship.Id,
                            RelationshipType = relationship.RelationshipType,
                            FullName = relationship.FullName,
                            Phone = relationship.Phone,
                            Address = relationship.Address,
                        });
                }
            }
            #endregion

            return outLoanBrief;
        }
        #endregion
        public static DocumentType MapToDocumentType(this DocumentTypeDTO item)
        {
            var outDocumentType = new DocumentType();
            outDocumentType.Id = item.Id;
            outDocumentType.Name = item.Name;
            outDocumentType.IsEnable = item.IsEnable;
            outDocumentType.ProductId = item.ProductId;
            outDocumentType.NumberImage = item.NumberImage;
            outDocumentType.Guide = item.Guide;
            outDocumentType.ParentId = item.ParentId;
            outDocumentType.TypeOwnerShip = item.TypeOwnerShip;
            outDocumentType.ObjectRuleType = item.ObjectRuleType != null ? item.ObjectRuleType : "0";
            outDocumentType.MobileUpload = item.MobileUpload;
            outDocumentType.Version = item.Version != null ? item.Version : 2;
            outDocumentType.CreatedTime = item.CreatedTime != null ? item.CreatedTime : DateTime.Now;

            return outDocumentType;
        }
        public static Product MapToProduct(this ProductDTO item)
        {
            var outProduct = new Product();
            outProduct.Id = item.Id;
            outProduct.BrandProductId = item.BrandProductId;
            outProduct.ProductTypeId = item.ProductTypeId;
            outProduct.Name = item.Name;
            outProduct.FullName = item.FullName;
            outProduct.Year = item.Year;
            outProduct.BrakeType = item.BrakeType;
            outProduct.RimType = item.RimType;
            outProduct.ShortName = item.ShortName;
            outProduct.CreateTime = item.CreateTime;
            outProduct.CreateBy = item.CreateBy;
            outProduct.UpdateTime = item.UpdateTime;
            outProduct.UpdateBy = item.UpdateBy;

            return outProduct;
        }
        public static ContactCustomer MapToContactCustomer(this ContactCustomerViewModel item)
        {
            var outContactCustomer = new ContactCustomer
            {
                ContactId = item.ContactId,
                FullName = item.FullName,
                ProvinceId = item.ProvinceId,
                DistrictId = item.DistrictId,
                WardId = item.WardId,
                JobId = item.JobId,
                Income = item.Income,
                HaveCarsOrNot = item.HaveCarsOrNot,
                BrandProductId = item.BrandProductId,
                ProductId = item.ProductId,
                Phone = item.Phone,
                CreateDate = item.CreateDate
            };
            return outContactCustomer;
        }
        public static ContactCustomerViewModel MapContactCustomerToView(this ContactCustomer item)
        {
            var outContactCustomer = new ContactCustomerViewModel
            {
                ContactId = item.ContactId,
                FullName = item.FullName,
                ProvinceId = item.ProvinceId,
                DistrictId = item.DistrictId,
                WardId = item.WardId,
                JobId = item.JobId,
                Income = item.Income,
                HaveCarsOrNot = item.HaveCarsOrNot,
                BrandProductId = item.BrandProductId,
                ProductId = item.ProductId,
                Phone = item.Phone,
                CreateDate = item.CreateDate
            };
            return outContactCustomer;
        }

        public static LoanBrief MapContactCustomerToLoanbrief(this ContactCustomer item, LoanBrief OutLoanBrief)
        {

            OutLoanBrief.FullName = item.FullName;
            OutLoanBrief.ProvinceId = item.ProvinceId;
            OutLoanBrief.DistrictId = item.DistrictId;
            OutLoanBrief.WardId = item.WardId;
            OutLoanBrief.Phone = item.Phone;
            OutLoanBrief.LoanBriefJob = new LoanBriefJob();
            OutLoanBrief.LoanBriefJob.TotalIncome = item.Income == null ? 0 : item.Income;
            OutLoanBrief.LoanBriefJob.JobId = item.JobId == null ? 0 : item.JobId;
            OutLoanBrief.Status = EnumLoanStatus.INIT.GetHashCode();
            OutLoanBrief.ProductId = EnumProductCredit.MotorCreditType_CC.GetHashCode();
            OutLoanBrief.LoanBriefProperty = new LoanBriefProperty();
            OutLoanBrief.LoanBriefProperty.BrandId = item.BrandProductId;
            OutLoanBrief.LoanBriefProperty.ProductId = item.ProductId;
            OutLoanBrief.LoanBriefResident = new LoanBriefResident();
            OutLoanBrief.LoanBriefResident.ProvinceId = item.ProvinceId;
            OutLoanBrief.LoanBriefResident.DistrictId = item.DistrictId;
            OutLoanBrief.LoanBriefResident.WardId = item.WardId;
            return OutLoanBrief;
        }
        #endregion

        public static User MapToUserChangeInformation(this ChangeInformation item)
        {
            var outUser = new User();
            outUser.UserId = item.UserId;
            outUser.FullName = item.FullName;
            outUser.Phone = item.Phone;
            outUser.Email = item.Email;
            outUser.TypeCallService = item.TypeCallService;
            outUser.Ipphone = item.Ipphone;
            outUser.CiscoExtension = item.CiscoExtension;
            outUser.CiscoPassword = item.CiscoPassword;
            outUser.CiscoUsername = item.CiscoUsername;

            return outUser;
        }

        public static LoanBrief MapToLoanbriefRequestAI(this LoanbriefScriptViewModel item)
        {
            #region map Loanbrief
            var outLoanBrief = new LoanBrief();
            outLoanBrief.LoanBriefId = item.LoanBriefId;
            outLoanBrief.CustomerId = item.CustomerId;
            outLoanBrief.FullName = item.FullName;
            outLoanBrief.Phone = item.Phone;
            outLoanBrief.NationalCard = item.NationalCard;
            outLoanBrief.Gender = item.Gender;
            outLoanBrief.ProvinceId = item.ProvinceId;
            outLoanBrief.DistrictId = item.DistrictId;
            outLoanBrief.WardId = item.WardId;
            if (!string.IsNullOrEmpty(item.sBirthDay))
                outLoanBrief.Dob = DateTimeUtility.ConvertStringToDate(item.sBirthDay, "dd/MM/yyyy");
            #endregion

            #region map LoanBriefResident
            outLoanBrief.LoanBriefResident = new LoanBriefResident();
            outLoanBrief.LoanBriefResident.ResidentType = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.ResidentType : 0;
            outLoanBrief.LoanBriefResident.Address = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.Address : "";
            outLoanBrief.LoanBriefResident.AddressGoogleMap = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.AddressGoogleMap : "";
            outLoanBrief.LoanBriefResident.AddressLatLng = item.LoanBriefResidentModel != null ? item.LoanBriefResidentModel.AddressLatLng : "";
            #endregion

            #region map LoanBriefHouseholdModel
            outLoanBrief.LoanBriefHousehold = new LoanBriefHousehold();
            outLoanBrief.LoanBriefHousehold.Address = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.Address : "";
            outLoanBrief.LoanBriefHousehold.ProvinceId = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.ProvinceId : 0;
            outLoanBrief.LoanBriefHousehold.DistrictId = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.DistrictId : 0;
            outLoanBrief.LoanBriefHousehold.WardId = item.LoanBriefHouseholdModel != null ? item.LoanBriefHouseholdModel.WardId : 0;
            #endregion

            #region map LoanBriefJobModel
            outLoanBrief.LoanBriefJob = new LoanBriefJob();
            outLoanBrief.LoanBriefJob.CompanyAddressLatLng = item.LoanBriefJobModel != null ? item.LoanBriefJobModel.CompanyAddressLatLng : "";
            #endregion

            return outLoanBrief;
        }

        public static LoanBrief MapToLoanbriefRefPhone(this LoanbriefScriptViewModel item)
        {
            #region map Loanbrief
            var outLoanBrief = new LoanBrief();
            outLoanBrief.LoanBriefId = item.LoanBriefId;
            outLoanBrief.CustomerId = item.CustomerId;
            #endregion

            #region map LoanBriefRelationshipModel
            outLoanBrief.LoanBriefRelationship = new List<LoanBriefRelationship>();
            if (item.LoanBriefRelationshipModels != null && item.LoanBriefRelationshipModels.Count > 0)
            {
                foreach (var relationship in item.LoanBriefRelationshipModels)
                {
                    if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                        outLoanBrief.LoanBriefRelationship.Add(new LoanBriefRelationship()
                        {
                            Id = relationship.Id,
                            RelationshipType = relationship.RelationshipType,
                            FullName = relationship.FullName,
                            Phone = relationship.Phone,
                            Address = relationship.Address,
                        });
                }
            }
            #endregion

            return outLoanBrief;
        }

        public static LoanBrief MapToLoanbriefDebtRevolving(this LoanBriefInitDetail item)
        {
            #region map Loanbrief
            var outLoanBrief = new LoanBrief();
            outLoanBrief.LoanBriefId = item.LoanBriefId;
            outLoanBrief.CustomerId = item.CustomerId;
            outLoanBrief.PlatformType = item.PlatformType;
            outLoanBrief.TypeLoanBrief = item.TypeLoanBrief;
            outLoanBrief.FullName = item.FullName;
            outLoanBrief.Phone = item.Phone;
            outLoanBrief.NationalCard = item.NationalCard;
            outLoanBrief.NationCardPlace = item.NationCardPlace;
            outLoanBrief.IsTrackingLocation = item.IsTrackingLocation;
            outLoanBrief.Dob = item.Dob;
            outLoanBrief.Passport = item.Passport;
            outLoanBrief.Gender = item.Gender;
            outLoanBrief.NumberCall = item.NumberCall;
            outLoanBrief.ProvinceId = item.ProvinceId;
            outLoanBrief.DistrictId = item.DistrictId;
            outLoanBrief.WardId = item.WardId;
            outLoanBrief.ProductId = item.ProductId;
            outLoanBrief.RateTypeId = item.RateTypeId;
            outLoanBrief.LoanAmount = item.LoanAmount;
            outLoanBrief.BuyInsurenceCustomer = item.BuyInsurenceCustomer;
            outLoanBrief.LoanTime = item.LoanTime;
            outLoanBrief.Frequency = item.Frequency;
            outLoanBrief.RateMoney = item.RateMoney;
            outLoanBrief.RatePercent = item.RatePercent;
            outLoanBrief.ReceivingMoneyType = item.ReceivingMoneyType;
            outLoanBrief.BankId = item.BankId;
            outLoanBrief.BankAccountNumber = item.BankAccountNumber;
            outLoanBrief.BankCardNumber = item.BankCardNumber;
            outLoanBrief.BankAccountName = item.BankAccountName;
            outLoanBrief.IsLocate = item.IsLocate;
            outLoanBrief.TypeRemarketing = item.TypeRemarketing;
            outLoanBrief.FromDate = item.FromDate;
            outLoanBrief.ToDate = item.ToDate;
            outLoanBrief.CreatedTime = item.CreatedTime;
            outLoanBrief.IsHeadOffice = item.IsHeadOffice;
            outLoanBrief.HubId = item.HubId;
            outLoanBrief.CreateBy = item.CreateBy;
            outLoanBrief.BoundTelesaleId = item.BoundTelesaleId;
            outLoanBrief.HubEmployeeId = item.HubEmployeeId;
            outLoanBrief.PresenterCode = item.PresenterCode;
            outLoanBrief.LoanAmountFirst = item.LoanAmountFirst;
            outLoanBrief.LoanAmountExpertise = item.LoanAmountExpertise;
            outLoanBrief.LoanAmountExpertiseLast = item.LoanAmountExpertiseLast;
            outLoanBrief.LoanAmountExpertiseAi = item.LoanAmountExpertiseAi;
            outLoanBrief.FirstProcessingTime = item.FirstProcessingTime;
            outLoanBrief.HomeNetwork = item.HomeNetwork;
            outLoanBrief.PhoneOther = item.PhoneOther;
            outLoanBrief.BankBranch = item.BankBranch;
            outLoanBrief.ProductDetailId = item.ProductDetailId;
            outLoanBrief.IsSim = item.IsSim;
            outLoanBrief.IsReborrow = item.IsReborrow;
            outLoanBrief.ReMarketingLoanBriefId = item.ReMarketingLoanBriefId;
            outLoanBrief.LoanPurpose = item.LoanPurpose;
            #endregion


            #region map LoanBriefResident
            outLoanBrief.LoanBriefResident = new LoanBriefResident();
            outLoanBrief.LoanBriefResident.LivingTime = item.LoanBriefResident != null ? item.LoanBriefResident.LivingTime : 0;
            outLoanBrief.LoanBriefResident.LivingWith = item.LoanBriefResident != null ? item.LoanBriefResident.LivingWith : 0;
            outLoanBrief.LoanBriefResident.ResidentType = item.LoanBriefResident != null ? item.LoanBriefResident.ResidentType : 0;
            outLoanBrief.LoanBriefResident.Address = item.LoanBriefResident != null ? item.LoanBriefResident.Address : "";
            outLoanBrief.LoanBriefResident.LoanBriefResidentId = item.LoanBriefResident != null ? item.LoanBriefResident.LoanBriefResidentId : 0;
            outLoanBrief.LoanBriefResident.AddressGoogleMap = item.LoanBriefResident != null ? item.LoanBriefResident.AddressGoogleMap : "";
            outLoanBrief.LoanBriefResident.AddressLatLng = item.LoanBriefResident != null ? item.LoanBriefResident.AddressLatLng : "";
            outLoanBrief.LoanBriefResident.BillElectricityId = item.LoanBriefResident != null ? item.LoanBriefResident.BillElectricityId : "";
            outLoanBrief.LoanBriefResident.BillWaterId = item.LoanBriefResident != null ? item.LoanBriefResident.BillWaterId : "";
            outLoanBrief.LoanBriefResident.AddressNationalCard = item.LoanBriefResident != null ? item.LoanBriefResident.AddressNationalCard : "";
            outLoanBrief.LoanBriefResident.CustomerShareLocation = item.LoanBriefResident != null ? item.LoanBriefResident.CustomerShareLocation : "";
            #endregion

            #region map LoanBriefHouseholdModel
            outLoanBrief.LoanBriefHousehold = new LoanBriefHousehold();
            outLoanBrief.LoanBriefHousehold.Address = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.Address : "";
            outLoanBrief.LoanBriefHousehold.ProvinceId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.ProvinceId : 0;
            outLoanBrief.LoanBriefHousehold.DistrictId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.DistrictId : 0;
            outLoanBrief.LoanBriefHousehold.WardId = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.WardId : 0;
            outLoanBrief.LoanBriefHousehold.FullNameHouseOwner = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.FullNameHouseOwner : null;
            outLoanBrief.LoanBriefHousehold.RelationshipHouseOwner = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.RelationshipHouseOwner : null;
            outLoanBrief.LoanBriefHousehold.BirdayHouseOwner = item.LoanBriefHousehold != null ? item.LoanBriefHousehold.BirdayHouseOwner : null;
            #endregion

            #region map LoanBriefProperty
            outLoanBrief.LoanBriefProperty = new LoanBriefProperty();
            outLoanBrief.LoanBriefProperty.BrandId = item.LoanBriefProperty != null ? item.LoanBriefProperty.BrandId : 0;
            outLoanBrief.LoanBriefProperty.ProductId = item.LoanBriefProperty != null ? item.LoanBriefProperty.ProductId : 0;
            outLoanBrief.LoanBriefProperty.PlateNumber = item.LoanBriefProperty != null ? item.LoanBriefProperty.PlateNumber : "";
            outLoanBrief.LoanBriefProperty.CarManufacturer = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarManufacturer : "";
            outLoanBrief.LoanBriefProperty.CarName = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarName : "";
            outLoanBrief.LoanBriefProperty.CarColor = item.LoanBriefProperty != null ? item.LoanBriefProperty.CarColor : "";
            outLoanBrief.LoanBriefProperty.PlateNumberCar = item.LoanBriefProperty != null ? item.LoanBriefProperty.PlateNumberCar : "";
            outLoanBrief.LoanBriefProperty.Chassis = item.LoanBriefProperty != null ? item.LoanBriefProperty.Chassis : "";
            outLoanBrief.LoanBriefProperty.Engine = item.LoanBriefProperty != null ? item.LoanBriefProperty.Engine : "";
            outLoanBrief.LoanBriefProperty.BuyInsuranceProperty = item.LoanBriefProperty != null ? item.LoanBriefProperty.BuyInsuranceProperty : false;
            outLoanBrief.LoanBriefProperty.OwnerFullName = item.LoanBriefProperty != null ? item.LoanBriefProperty.OwnerFullName : "";
            outLoanBrief.LoanBriefProperty.MotobikeCertificateNumber = item.LoanBriefProperty != null ? item.LoanBriefProperty.MotobikeCertificateNumber : "";
            outLoanBrief.LoanBriefProperty.MotobikeCertificateDate = item.LoanBriefProperty != null ? item.LoanBriefProperty.MotobikeCertificateDate : "";
            outLoanBrief.LoanBriefProperty.MotobikeCertificateAddress = item.LoanBriefProperty != null ? item.LoanBriefProperty.MotobikeCertificateAddress : "";
            #endregion

            #region map LoanBriefJobModel
            outLoanBrief.LoanBriefJob = new LoanBriefJob();
            outLoanBrief.LoanBriefJob.LoanBriefJobId = item.LoanBriefJob != null ? item.LoanBriefJob.LoanBriefJobId : 0;
            outLoanBrief.LoanBriefJob.JobId = item.LoanBriefJob != null ? item.LoanBriefJob.JobId : 0;
            outLoanBrief.LoanBriefJob.CompanyName = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyName : "";
            outLoanBrief.LoanBriefJob.CompanyPhone = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyPhone : "";
            outLoanBrief.LoanBriefJob.CompanyTaxCode = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyTaxCode : "";
            outLoanBrief.LoanBriefJob.CompanyProvinceId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyProvinceId : 0;
            outLoanBrief.LoanBriefJob.CompanyDistrictId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyDistrictId : 0;
            outLoanBrief.LoanBriefJob.CompanyWardId = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyWardId : 0;
            outLoanBrief.LoanBriefJob.CompanyAddress = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddress : "";
            outLoanBrief.LoanBriefJob.TotalIncome = item.LoanBriefJob != null ? item.LoanBriefJob.TotalIncome : 0;
            outLoanBrief.LoanBriefJob.ImcomeType = item.LoanBriefJob != null ? item.LoanBriefJob.ImcomeType : 0;
            outLoanBrief.LoanBriefJob.Description = item.LoanBriefJob != null ? item.LoanBriefJob.Description : "";
            outLoanBrief.LoanBriefJob.CompanyAddressGoogleMap = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddressGoogleMap : "";
            outLoanBrief.LoanBriefJob.CompanyAddressLatLng = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyAddressLatLng : "";
            outLoanBrief.LoanBriefJob.CompanyInsurance = item.LoanBriefJob != null ? item.LoanBriefJob.CompanyInsurance : null;
            outLoanBrief.LoanBriefJob.WorkLocation = item.LoanBriefJob != null ? item.LoanBriefJob.WorkLocation : null;
            outLoanBrief.LoanBriefJob.BusinessPapers = item.LoanBriefJob != null ? item.LoanBriefJob.BusinessPapers : null;
            outLoanBrief.LoanBriefJob.JobDescriptionId = item.LoanBriefJob != null ? item.LoanBriefJob.JobDescriptionId : null;
            #endregion

            #region map LoanBriefCompany
            if (item.TypeLoanBrief == LoanBriefType.Company.GetHashCode())
            {
                outLoanBrief.LoanBriefCompany = new LoanBriefCompany();
                outLoanBrief.LoanBriefCompany.LoanBriefId = item.LoanBriefCompany != null ? item.LoanBriefCompany.LoanBriefId : 0;
                outLoanBrief.LoanBriefCompany.CompanyName = item.LoanBriefCompany != null ? item.LoanBriefCompany.CompanyName : "";
                outLoanBrief.LoanBriefCompany.CareerBusiness = item.LoanBriefCompany != null ? item.LoanBriefCompany.CareerBusiness : "";
                outLoanBrief.LoanBriefCompany.BusinessCertificationAddress = item.LoanBriefCompany != null ? item.LoanBriefCompany.BusinessCertificationAddress : "";
                outLoanBrief.LoanBriefCompany.HeadOffice = item.LoanBriefCompany != null ? item.LoanBriefCompany.HeadOffice : "";
                outLoanBrief.LoanBriefCompany.Address = item.LoanBriefCompany != null ? item.LoanBriefCompany.Address : "";
                outLoanBrief.LoanBriefCompany.CompanyShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.CompanyShareholder : "";

                outLoanBrief.LoanBriefCompany.CardNumberShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.CardNumberShareholder : "";
                outLoanBrief.LoanBriefCompany.PlaceOfBirthShareholder = item.LoanBriefCompany != null ? item.LoanBriefCompany.PlaceOfBirthShareholder : "";
            }
            #endregion

            #region map LoanBriefRelationshipModel
            outLoanBrief.LoanBriefRelationship = new List<LoanBriefRelationship>();
            if (item.LoanBriefRelationship != null && item.LoanBriefRelationship.Count > 0)
            {
                foreach (var relationship in item.LoanBriefRelationship)
                {
                    if (!string.IsNullOrEmpty(relationship.FullName) || !string.IsNullOrEmpty(relationship.Phone))
                    {
                        outLoanBrief.LoanBriefRelationship.Add(new LoanBriefRelationship()
                        {
                            Id = relationship.Id,
                            RelationshipType = relationship.RelationshipType,
                            FullName = relationship.FullName,
                            Phone = relationship.Phone,
                            Address = relationship.Address,
                        });
                    }
                }
            }
            #endregion

            return outLoanBrief;
        }
    }
}
