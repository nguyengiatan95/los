﻿using AutoMapper;
using LOS.DAL.DTOs;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Groups;
using LOS.WebApp.Models.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Helpers
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<UserDetail,UserItem>().ReverseMap();
            CreateMap<GroupDetail,GroupItem>().ReverseMap();
            CreateMap<ModuleDetail, ModuleItem>().ReverseMap();
            CreateMap<UserMMODetail, UserMmoItem>().ReverseMap();
        }
    }

    //public class NoMapAttribute : System.Attribute
    //{
    //}
    //public static class IgnoreNoMapExtensions
    //{
    //    public static IMappingExpression<TSource, TDestination> IgnoreNoMap<TSource, TDestination>(
    //        this IMappingExpression<TSource, TDestination> expression)
    //    {
    //        var sourceType = typeof(TSource);

    //        foreach (var property in sourceType.GetProperties())
    //        {
    //            PropertyDescriptor descriptor = TypeDescriptor.GetProperties(sourceType)[property.Name];

    //            NoMapAttribute attribute = (NoMapAttribute)descriptor.Attributes[typeof(NoMapAttribute)];

    //            if (attribute != null)
    //                expression.ForMember(property.Name, opt => opt.Ignore());
    //        }
    //        return expression;
    //    }
    //}
}