﻿using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.WebApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Helpers
{
    public class PermissionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ContentResult content = new ContentResult();
            content.ContentType = "application/json";
            var session = filterContext.HttpContext.Session;
            if (session == null || session.GetObjectFromJson<UserDetail>("USER_DATA") == null)
            {
                var result = new ResultResponse(0, "Không tìm thấy thông tin đăng nhập. Vui lòng đăng nhập lại!!!");
                content.Content = JsonConvert.SerializeObject(result);
                filterContext.Result = content;
            }
            else
            {
                var user = session.GetObjectFromJson<UserDetail>("USER_DATA").Result;
                if(user == null)
                {
                    var result = new ResultResponse(0, "Không tìm thấy thông tin đăng nhập. Vui lòng đăng nhập lại!!!");
                    content.Content = JsonConvert.SerializeObject(result);
                    filterContext.Result = content;
                }
                else
                {
                    IModuleService repo = (IModuleService)filterContext.HttpContext.RequestServices.GetService(typeof(IModuleService));
                    var controller = filterContext.RouteData.Values["Controller"].ToString();
                    var action = filterContext.RouteData.Values["Action"].ToString();
                    if (!string.IsNullOrEmpty(controller) && !string.IsNullOrEmpty(action))
                    {
                        var module = repo.Get(user.Token, controller.AsToLower(), action.AsToLower());
                        if (module.ModuleId > 0)
                        {
                            if (!user.Modules.Any(x => x.ModuleId == module.ModuleId))
                            {
                                var result = new ResultResponse(0, string.Format("Bạn không có quyền truy cập: {0}/{1}", controller, action));
                                content.Content = JsonConvert.SerializeObject(result);
                                filterContext.Result = content;
                            }
                        }
                    }
                }
            }
            base.OnActionExecuting(filterContext);
        }
    }

    public class ResultResponse
    {
        public ResultResponse()
        {

        }
        public ResultResponse(int _isSuccess, string _message)
        {
            isSuccess = _isSuccess;
            message = _message;
            status = _isSuccess;
        }
        public int isSuccess { get; set; }
        public int status { get; set; }
        public string message { get; set; }
    }
}
