
using LOS.Common.Extensions;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Models;
using LOS.WebApp.Services;
using LOS.WebApp.Services.Location;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LOS.WebApp.Helpers
{
    public class Ultility
    {
        private static IHttpClientFactory clientFactory;

        public Ultility(IHttpClientFactory _clientFactory)
        {
            clientFactory = _clientFactory;
        }

        public static List<ProvinceDetail> provinces;
        public static List<LoanStatus> loanStatutes;
        public static List<PipelineState> pipelineStates;
        public static List<LoanProduct> loanProducts;
        public static List<PropertyDTO> properties;
        public static List<HubType> hubTypes;
        public static List<EventConfig> eventConfigs;
        public static List<TypeRemarketing> typeRemarketings;
        public static List<DistrictDetail> districts;
        public static List<InfomationProductDetailDTO> productDetails;
        public static List<DocumentException> documentExceptions;

        public static string ToQueryString(NameValueCollection nvc)
        {
            var array = (from key in nvc.AllKeys
                         from value in nvc.GetValues(key)
                         select string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value)))
                .ToArray();
            return "?" + string.Join("&", array);
        }

        public static string HidePhone(string input)
        {
            if (input != null)
            {
                var aStringBuilder = new StringBuilder(input);
                aStringBuilder.Remove(4, 4);
                aStringBuilder.Insert(4, "****");
                input = aStringBuilder.ToString();
            }
            return input;
        }

        public object GetLinkObject(string linkObject, string token)
        {
            if (linkObject != null)
            {
                if (linkObject.Equals("PipelineState"))
                {
                    if (pipelineStates == null || pipelineStates.Count == 0)
                    {
                        IDictionaryService service = new DictionaryService(null, clientFactory);
                        pipelineStates = service.GetPipelineState(token);
                    }
                    return pipelineStates;
                }
                else if (linkObject.Equals("LoanStatus"))
                {
                    if (loanStatutes == null || loanStatutes.Count == 0)
                    {
                        DictionaryService service = new DictionaryService(null, clientFactory);
                        loanStatutes = service.GetLoanStatus(token);
                    }
                    return loanStatutes;
                }
                else if (linkObject.Equals("Province"))
                {
                    if (provinces == null || provinces.Count == 0)
                    {
                        DictionaryService service = new DictionaryService(null, clientFactory);
                        provinces = service.GetProvince(token);
                    }
                    return provinces;
                }
                else if (linkObject.Equals("LoanProduct"))
                {
                    if (loanProducts == null || loanProducts.Count == 0)
                    {
                        DictionaryService service = new DictionaryService(null, clientFactory);
                        loanProducts = service.GetLoanProduct(token);
                    }
                    return loanProducts;
                }
                else if (linkObject.Equals("HubType"))
                {
                    if (hubTypes == null || hubTypes.Count == 0)
                    {
                        DictionaryService service = new DictionaryService(null, clientFactory);
                        hubTypes = service.GetHubType(token);
                    }
                    return hubTypes;
                }
                else if (linkObject.Equals("EventConfig"))
                {
                    if (eventConfigs == null || eventConfigs.Count == 0)
                    {
                        DictionaryService service = new DictionaryService(null, clientFactory);
                        eventConfigs = service.GetEventConfig(token);
                    }
                    return eventConfigs;
                }
                else if (linkObject.Equals("TypeRemarketing"))
                {
                    if (typeRemarketings == null || typeRemarketings.Count == 0)
                    {
                        DictionaryService service = new DictionaryService(null, clientFactory);
                        typeRemarketings = service.GetTypeRemarketing(token);
                    }
                    return typeRemarketings;
                }
                else if (linkObject.Equals("District"))
                {
                    if (districts == null || districts.Count == 0)
                    {
                        DictionaryService service = new DictionaryService(null, clientFactory);
                        districts = service.GetDistrict(token, -1);
                    }
                    return districts;
                }
                else if (linkObject.Equals("InfomationProductDetail"))
                {
                    if (productDetails == null || productDetails.Count == 0)
                    {
                        DictionaryService service = new DictionaryService(null, clientFactory);
                        productDetails = service.GetAllInfomationProductDetail(token);
                    }
                    return productDetails;
                }
                else if (linkObject.Equals("DocumentException"))
                {
                    if (documentExceptions == null || documentExceptions.Count == 0)
                    {
                        DictionaryService service = new DictionaryService(null, clientFactory);
                        documentExceptions = service.GetDocumentExceptions(token);
                    }
                    return documentExceptions;
                }
            }
            return null;
        }

        public static string GetOperatorValue(string op)
        {
            string value = "";
            switch (op)
            {
                case "EQUAL":
                    value = "BẰNG";
                    break;
                case "LESS_THEN":
                    value = "NHỎ HƠN";
                    break;
                case "LESS_THEN_OR_EQUAL":
                    value = "NHỎ HƠN HOẶC BẰNG";
                    break;
                case "GREATER_THAN":
                    value = "LỚN HƠN";
                    break;
                case "GREATER_THAN_OR_EQUAL":
                    value = "LỚN HƠN HOẶC BẰNG";
                    break;
                case "IN":
                    value = "NẰM TRONG";
                    break;
                case "NOT_IN":
                    value = "KHÔNG NẰM TRONG";
                    break;
                default:
                    break;
            }
            return value;
        }

        public static string GetValue(string value, string linkObject, string token, IHttpClientFactory clientFactory)
        {
            if (linkObject != null)
            {
                if (linkObject.Equals("PipelineState"))
                {
                    int iValue = Int32.Parse(value);
                    if (pipelineStates == null)
                    {
                        DictionaryService service = new DictionaryService(null, clientFactory);
                        pipelineStates = service.GetPipelineState(token);
                        if (pipelineStates != null && pipelineStates.Count > 0)
                            return pipelineStates.Where(x => x.State == iValue).Select(x => x.Description).FirstOrDefault();
                    }
                    else
                    {
                        if (pipelineStates != null && pipelineStates.Count > 0)
                            return pipelineStates.Where(x => x.State == iValue).Select(x => x.Description).FirstOrDefault();
                    }
                }
                else if (linkObject.Equals("LoanStatus"))
                {
                    if (value.IndexOf(",") > 0)
                    {
                        string[] values = value.Split(",");
                        int iValue = Int32.Parse(value);
                        List<string> returnValues = new List<string>();
                        if (loanStatutes == null)
                        {
                            DictionaryService service = new DictionaryService(null, clientFactory);
                            loanStatutes = service.GetLoanStatus(token);
                        }
                        foreach (var v in values)
                        {
                            returnValues.Add(loanStatutes.Where(x => x.Status == Int32.Parse(v)).Select(x => x.Description).FirstOrDefault());
                        }
                        return String.Join(",", returnValues);
                    }
                    else
                    {
                        int iValue = Int32.Parse(value);
                        if (provinces == null)
                        {
                            DictionaryService service = new DictionaryService(null, clientFactory);
                            loanStatutes = service.GetLoanStatus(token);
                            if (loanStatutes != null && loanStatutes.Count > 0)
                                return loanStatutes.Where(x => x.LoanStatusId == iValue).Select(x => x.Description).FirstOrDefault();
                        }
                        else
                        {
                            if (loanStatutes != null && loanStatutes.Count > 0)
                                return loanStatutes.Where(x => x.LoanStatusId == iValue).Select(x => x.Description).FirstOrDefault();
                        }
                    }
                }
                else if (linkObject.Equals("Province"))
                {
                    if (value == "ALL")
                    {
                        return "Tất cả";
                    }
                    else
                    {
                        if (value.IndexOf(",") > 0)
                        {
                            string[] values = value.Split(",");
                            //int iValue = Int32.Parse(value);
                            List<string> returnValues = new List<string>();
                            if (provinces == null)
                            {
                                DictionaryService service = new DictionaryService(null, clientFactory);
                                provinces = service.GetProvince(token);
                            }
                            foreach (var v in values)
                            {
                                returnValues.Add(provinces.Where(x => x.ProvinceId == Int32.Parse(v)).Select(x => x.Name).FirstOrDefault());
                            }
                            return String.Join(",", returnValues);
                        }
                        else
                        {
                            int iValue = Int32.Parse(value);
                            if (provinces == null)
                            {
                                DictionaryService service = new DictionaryService(null, clientFactory);
                                provinces = service.GetProvince(token);
                                if (provinces != null && provinces.Count > 0)
                                    return provinces.Where(x => x.ProvinceId == iValue).Select(x => x.Name).FirstOrDefault();
                            }
                            else
                            {
                                if (provinces != null && provinces.Count > 0)
                                    return provinces.Where(x => x.ProvinceId == iValue).Select(x => x.Name).FirstOrDefault();
                            }
                        }
                    }
                }
                else if (linkObject.Equals("District"))
                {
                    if (value == "ALL")
                    {
                        return "Tất cả";
                    }
                    else
                    {
                        if (value.IndexOf(",") > 0)
                        {
                            string[] values = value.Split(",");
                            //int iValue = Int32.Parse(value);
                            List<string> returnValues = new List<string>();
                            if (districts == null)
                            {
                                DictionaryService service = new DictionaryService(null, clientFactory);
                                districts = service.GetDistrict(token, -1);
                            }
                            foreach (var v in values)
                            {
                                returnValues.Add(districts.Where(x => x.DistrictId == Int32.Parse(v)).Select(x => x.Name).FirstOrDefault());
                            }
                            return String.Join(",", returnValues);
                        }
                        else
                        {
                            int iValue = Int32.Parse(value);
                            if (districts == null)
                            {
                                DictionaryService service = new DictionaryService(null, clientFactory);
                                districts = service.GetDistrict(token, -1);
                                if (districts != null && districts.Count > 0)
                                    return districts.Where(x => x.DistrictId == iValue).Select(x => x.Name).FirstOrDefault();
                            }
                            else
                            {
                                if (districts != null && districts.Count > 0)
                                    return districts.Where(x => x.DistrictId == iValue).Select(x => x.Name).FirstOrDefault();
                            }
                        }
                    }
                }
                else if (linkObject.Equals("LoanProduct"))
                {
                    if (value == "ALL")
                    {
                        return "Tất cả";
                    }
                    else
                    {
                        if (value.IndexOf(",") > 0)
                        {
                            string[] values = value.Split(",");
                            List<string> returnValues = new List<string>();
                            if (loanProducts == null)
                            {
                                DictionaryService service = new DictionaryService(null, clientFactory);
                                loanProducts = service.GetLoanProduct(token);
                            }
                            foreach (var v in values)
                            {
                                returnValues.Add(loanProducts.Where(x => x.LoanProductId == Int32.Parse(v)).Select(x => x.Name).FirstOrDefault());
                            }
                            return String.Join(",", returnValues);
                        }
                        else
                        {
                            int iValue = Int32.Parse(value);
                            if (loanProducts == null)
                            {
                                DictionaryService service = new DictionaryService(null, clientFactory);
                                loanProducts = service.GetLoanProduct(token);
                                if (loanProducts != null && loanProducts.Count > 0)
                                    return loanProducts.Where(x => x.LoanProductId == iValue).Select(x => x.Name).FirstOrDefault();
                            }
                            else
                            {
                                if (loanProducts != null && loanProducts.Count > 0)
                                    return loanProducts.Where(x => x.LoanProductId == iValue).Select(x => x.Name).FirstOrDefault();
                            }
                        }
                        //int iValue = Int32.Parse(value);
                        //if (loanProducts == null)
                        //{
                        //	DictionaryService service = new DictionaryService(null);
                        //	loanProducts = service.GetLoanProduct(token);
                        //	if (loanProducts != null && loanProducts.Count > 0)
                        //		return loanProducts.Where(x => x.LoanProductId == iValue).Select(x => x.Name).FirstOrDefault();
                        //}
                        //else
                        //{
                        //	if (loanProducts != null && loanProducts.Count > 0)
                        //		return loanProducts.Where(x => x.LoanProductId == iValue).Select(x => x.Name).FirstOrDefault();
                        //}
                    }
                }
                else if (linkObject.Equals("HubType"))
                {
                    int iValue = Int32.Parse(value);
                    if (hubTypes == null)
                    {
                        DictionaryService service = new DictionaryService(null, clientFactory);
                        hubTypes = service.GetHubType(token);
                        if (hubTypes != null && hubTypes.Count > 0)
                            return hubTypes.Where(x => x.HubTypeId == iValue).Select(x => x.Name).FirstOrDefault();
                    }
                    else
                    {
                        if (hubTypes != null && hubTypes.Count > 0)
                            return hubTypes.Where(x => x.HubTypeId == iValue).Select(x => x.Name).FirstOrDefault();
                    }
                }
                else if (linkObject.Equals("TypeRemarketing"))
                {
                    if (value.IndexOf(",") > 0)
                    {
                        string[] values = value.Split(",");
                        List<string> returnValues = new List<string>();
                        if (typeRemarketings == null)
                        {
                            DictionaryService service = new DictionaryService(null, clientFactory);
                            typeRemarketings = service.GetTypeRemarketing(token);
                        }
                        foreach (var v in values)
                        {
                            returnValues.Add(typeRemarketings.Where(x => x.Id == Int32.Parse(v)).Select(x => x.Name).FirstOrDefault());
                        }
                        return String.Join(",", returnValues);
                    }
                    else
                    {
                        int iValue = Int32.Parse(value);
                        if (typeRemarketings == null)
                        {
                            DictionaryService service = new DictionaryService(null, clientFactory);
                            typeRemarketings = service.GetTypeRemarketing(token);
                            if (typeRemarketings != null && typeRemarketings.Count > 0)
                                return typeRemarketings.Where(x => x.Id == iValue).Select(x => x.Name).FirstOrDefault();
                        }
                        else
                        {
                            if (typeRemarketings != null && typeRemarketings.Count > 0)
                                return typeRemarketings.Where(x => x.Id == iValue).Select(x => x.Name).FirstOrDefault();
                        }
                    }
                }
                else if (linkObject.Equals("InfomationProductDetail"))
                {
                    if (value.IndexOf(",") > 0)
                    {
                        string[] values = value.Split(",");
                        List<string> returnValues = new List<string>();
                        if (productDetails == null)
                        {
                            DictionaryService service = new DictionaryService(null, clientFactory);
                            productDetails = service.GetAllInfomationProductDetail(token);
                        }
                        foreach (var v in values)
                        {
                            returnValues.Add(productDetails.Where(x => x.Id == Int32.Parse(v)).Select(x => x.Name).FirstOrDefault());
                        }
                        return String.Join(",", returnValues);
                    }
                    else
                    {
                        int iValue = Int32.Parse(value);
                        if (productDetails == null)
                        {
                            DictionaryService service = new DictionaryService(null, clientFactory);
                            productDetails = service.GetAllInfomationProductDetail(token);
                            if (productDetails != null && productDetails.Count > 0)
                                return productDetails.Where(x => x.Id == iValue).Select(x => x.Name).FirstOrDefault();
                        }
                        else
                        {
                            if (productDetails != null && productDetails.Count > 0)
                                return productDetails.Where(x => x.Id == iValue).Select(x => x.Name).FirstOrDefault();
                        }
                    }
                }
                else if (linkObject.Equals("DocumentException"))
                {
                    if (value.IndexOf(",") > 0)
                    {
                        string[] values = value.Split(",");
                        List<string> returnValues = new List<string>();
                        if (documentExceptions == null)
                        {
                            DictionaryService service = new DictionaryService(null, clientFactory);
                            documentExceptions = service.GetDocumentExceptions(token);
                        }
                        foreach (var v in values)
                        {
                            returnValues.Add(documentExceptions.Where(x => x.Id == Int32.Parse(v)).Select(x => x.Name).FirstOrDefault());
                        }
                        return String.Join(",", returnValues);
                    }
                    else
                    {
                        int iValue = Int32.Parse(value);
                        if (documentExceptions == null)
                        {
                            DictionaryService service = new DictionaryService(null, clientFactory);
                            documentExceptions = service.GetDocumentExceptions(token);
                            if (documentExceptions != null && documentExceptions.Count > 0)
                                return documentExceptions.Where(x => x.Id == iValue).Select(x => x.Name).FirstOrDefault();
                        }
                        else
                        {
                            if (documentExceptions != null && documentExceptions.Count > 0)
                                return documentExceptions.Where(x => x.Id == iValue).Select(x => x.Name).FirstOrDefault();
                        }
                    }
                }
            }
            return value;
        }

        public static void CopyObject<T>(object sourceObject, ref T destObject, bool bolNoList)
        {
            //	If either the source, or destination is null, return
            if (sourceObject == null || destObject == null)
                return;

            //	Get the type of each object
            Type sourceType = sourceObject.GetType();
            Type targetType = destObject.GetType();

            //	Loop through the source properties
            foreach (PropertyInfo p in sourceType.GetProperties())
            {
                //	Get the matching property in the destination object
                PropertyInfo targetObj = targetType.GetProperty(p.Name);
                //	If there is none, skip
                if (targetObj == null)
                    continue;
                if (p.PropertyType.Namespace.Contains("eSport5.ActionService"))
                    continue;
                if (bolNoList)
                {
                    if (p.PropertyType.Namespace.Contains("System.Collections.Generic"))
                        continue;
                }
                try
                {
                    if (p.CanWrite && p.GetValue(sourceObject, null) != null)
                        targetObj.SetValue(destObject, p.GetValue(sourceObject, null), null);
                }
                catch (Exception)
                {
                }

            }
        }

        public static EditPropertyDTO GetEditProperty(PropertyDTO dto, int index, string value, int type)
        {
            EditPropertyDTO editProperty = new EditPropertyDTO();
            editProperty.DefaultValue = dto.DefaultValue;
            editProperty.FieldName = dto.FieldName;
            editProperty.Index = index;
            editProperty.LinkObject = dto.LinkObject;
            editProperty.Name = dto.Name;
            editProperty.Object = dto.Object;
            editProperty.Priority = dto.Priority;
            editProperty.PropertyId = dto.PropertyId;
            editProperty.Regex = dto.Regex;
            editProperty.Status = dto.Status;
            editProperty.Type = dto.Type;
            editProperty.Value = value;
            editProperty.ActionType = type;
            editProperty.MultipleSelect = dto.MultipleSelect;
            return editProperty;
        }

        public static EditPropertyDTO GetEditProperty(PropertyDTO dto, int parentIndex, int index, string value, int type, string op)
        {
            EditPropertyDTO editProperty = new EditPropertyDTO();
            editProperty.DefaultValue = dto.DefaultValue;
            editProperty.FieldName = dto.FieldName;
            editProperty.Index = index;
            editProperty.LinkObject = dto.LinkObject;
            editProperty.Name = dto.Name;
            editProperty.Object = dto.Object;
            editProperty.ParentIndex = parentIndex;
            editProperty.Priority = dto.Priority;
            editProperty.PropertyId = dto.PropertyId;
            editProperty.Regex = dto.Regex;
            editProperty.Status = dto.Status;
            editProperty.Type = dto.Type;
            editProperty.Value = value;
            editProperty.ActionType = type;
            editProperty.Operator = op;
            editProperty.MultipleSelect = dto.MultipleSelect;
            return editProperty;
        }

        public static SectionApproveDTO GetEditSectionApprove(SectionApproveDTO dto, int index, int pipelineId)
        {
            dto.Index = index;
            dto.PipelineId = pipelineId;
            return dto;
        }

        public static List<DocumentTypeNew> GetListDocumentNew(List<DocumentTypeDetail> entity)
        {
            var lst = new List<DocumentTypeNew>();
            foreach (var item in entity)
            {
                if (item.ParentId.GetValueOrDefault(0) == 0)
                {
                    var lstdocChild = entity.Where(x => x.ParentId == item.Id).ToList();
                    if (lstdocChild != null && lstdocChild.Count > 0)
                    {
                        var itemChild = new List<DocumentTypeDTO>();
                        for (int i = 0; i < lstdocChild.Count; i++)
                        {
                            itemChild.Add(new DocumentTypeDTO
                            {
                                Id = lstdocChild[i].Id,
                                Name = lstdocChild[i].Name,
                                Guide = lstdocChild[i].Guide,
                                ParentId = lstdocChild[i].ParentId,
                                NumberImage = lstdocChild[i].NumberImage,
                                ProductId = lstdocChild[i].ProductId
                            });
                        }
                        lst.Add(new DocumentTypeNew
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Guide = item.Guide,
                            ParentId = item.ParentId,
                            LstDocumentChild = itemChild,
                            NumberImage = item.NumberImage
                        });
                    }
                    else
                    {
                        lst.Add(new DocumentTypeNew
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Guide = item.Guide,
                            ParentId = item.ParentId,
                            NumberImage = item.NumberImage
                        });
                    }

                }

            }
            return lst;

        }

        public static List<DocumentTypeNew> GetListDocumentNewV2(List<DocumentTypeDetail> entity, int TypeOwnerShip, int ObjectRuleType, string ResultLocationResident)
        {
            var lst = new List<DocumentTypeNew>();
            var typeOwnerShip = 0;
            if (TypeOwnerShip == (int)EnumTypeofownership.NhaSoHuuTrungSHK || TypeOwnerShip == (int)EnumTypeofownership.NhaSoHuuKhongTrungSHK)
                typeOwnerShip = 1;
            else
                typeOwnerShip = 2;
            entity = entity.Where(x => (x.TypeOwnerShip == typeOwnerShip || x.TypeOwnerShip == 0) && (x.ObjectRuleType.Contains(ObjectRuleType.ToString()) || x.ObjectRuleType == "0")).ToList();
            //entity = entity.Where(x => (x.TypeOwnerShip == typeOwnerShip || x.TypeOwnerShip == 0)).ToList();

            foreach (var item in entity)
            {
                var RequiredNew = true;// item.Required.HasValue ? item.Required : false ;
                if (item.ParentId.GetValueOrDefault(0) == 0)
                {
                    //Check Location địa chỉ đạt
                    if (ResultLocationResident == "YES" && (item.Id == (int)EnumDocumentType.TDNO_XECC_NhaSH || item.Id == (int)EnumDocumentType.TDNO_XEKCC_NhaSH))
                        RequiredNew = false;
                    if (typeOwnerShip == 1 && (item.Id == (int)EnumDocumentType.TDCV_XECC_NhaSH || item.Id == (int)EnumDocumentType.TDCV_XEKCC_NhaSH))
                        RequiredNew = false;
                    if (typeOwnerShip == 2 && (item.Id == (int)EnumDocumentType.ChungMinhNoiO_CC || item.Id == (int)EnumDocumentType.ChungMinhNoiO_KCC))
                        RequiredNew = false;
                    //var lstdocChild = new List<DocumentTypeDetail>();
                    var lstdocChild = entity.Where(x => x.ParentId == item.Id).ToList();
                    if (lstdocChild != null && lstdocChild.Count > 0)
                    {
                        var itemChild = new List<DocumentTypeDTO>();
                        for (int i = 0; i < lstdocChild.Count; i++)
                        {
                            itemChild.Add(new DocumentTypeDTO
                            {
                                Id = lstdocChild[i].Id,
                                Name = lstdocChild[i].Name,
                                Guide = lstdocChild[i].Guide,
                                ParentId = lstdocChild[i].ParentId,
                                NumberImage = lstdocChild[i].NumberImage,
                                ProductId = lstdocChild[i].ProductId
                            });
                        }
                        lst.Add(new DocumentTypeNew
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Guide = item.Guide,
                            ParentId = item.ParentId,
                            LstDocumentChild = itemChild,
                            NumberImage = item.NumberImage,
                            Required = item.Required,
                            RequiredNew = RequiredNew
                        });
                    }
                    else
                    {
                        lst.Add(new DocumentTypeNew
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Guide = item.Guide,
                            ParentId = item.ParentId,
                            NumberImage = item.NumberImage,
                            Required = item.Required,
                            RequiredNew = RequiredNew
                        });
                    }

                }

            }

            return lst;

        }

        public static List<DocumentMapConfig> MapDocumentAndConfig(List<DocumentTypeDetail> document, List<ConfigDocument> config)
        {
            var lst = new List<DocumentMapConfig>();
            foreach (var item in document)
            {
                if (item.ParentId.GetValueOrDefault(0) == 0)
                {
                    var lstConfig = config.Where(x => x.DocumentTypeId == item.Id).FirstOrDefault();
                    var lstdocChild = document.Where(x => x.ParentId == item.Id).ToList();
                    if (lstdocChild != null && lstdocChild.Count > 0)
                    {
                        var itemChild = new List<DocumentChild>();
                        for (int i = 0; i < lstdocChild.Count; i++)
                        {
                            var configChild = config.Where(x => x.DocumentTypeId == lstdocChild[i].Id).FirstOrDefault();
                            itemChild.Add(new DocumentChild
                            {
                                Id = lstdocChild[i].Id,
                                Name = lstdocChild[i].Name,
                                Guide = lstdocChild[i].Guide,
                                ParentId = lstdocChild[i].ParentId,
                                ConfigDocument = configChild
                            });
                        }
                        lst.Add(new DocumentMapConfig
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Guide = item.Guide,
                            ParentId = item.ParentId,
                            ConfigDocument = lstConfig,
                            //LstDocumentChild = itemChild
                        });
                    }
                    else
                    {
                        lst.Add(new DocumentMapConfig
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Guide = item.Guide,
                            ParentId = item.ParentId,
                            ConfigDocument = lstConfig
                        });
                    }

                }

            }
            return lst;

        }

        public static List<DocumentMapConfig> ConvertDocumentTree(List<DocumentTypeDetail> listData, List<ConfigDocument> configData, int productId, int typeOwnerShip)
        {
            var lst = new List<DocumentMapConfig>();

            var result = new List<DocumentMapConfig>();
            var dicData = new Dictionary<int, List<DocumentTypeDetail>>();
            foreach (var item in listData)
            {
                if (item.ParentId > 0)
                {
                    var key = item.ParentId.Value;
                    if (!dicData.ContainsKey(key))
                        dicData[key] = new List<DocumentTypeDetail>();
                    dicData[key].Add(item);
                }
            }
            var dicConfig = new Dictionary<string, ConfigDocument>();
            foreach (var item in configData)
            {
                if (item.DocumentTypeId > 0 && item.TypeOwnerShip > 0 && item.ProductId > 0)
                {
                    var key = $"{item.DocumentTypeId}-{item.TypeOwnerShip}-{item.ProductId}";
                    if (!dicConfig.ContainsKey(key))
                        dicConfig[key] = item;
                }
            }
            foreach (var item in listData)
            {
                if (item.ParentId.GetValueOrDefault(0) == 0)
                {
                    var itemDocumentMap = new DocumentMapConfig()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Guide = item.Guide,
                        IsChecked = false,
                        ParentId = item.ParentId,
                        LstDocumentChild = new List<DocumentMapConfig>()

                    };
                    //Lấy thông tin từ bảng config 
                    //Nếu tồn tại trong bảng config => chứng từ đó đang được chọn checked
                    var keyConfig = $"{item.Id}-{typeOwnerShip}-{productId}";
                    if (dicConfig.ContainsKey(keyConfig))
                    {
                        itemDocumentMap.IsChecked = true;
                        itemDocumentMap.ConfigDocument = dicConfig[keyConfig];
                    }
                    var key = item.Id;
                    if (dicData.ContainsKey(item.Id))
                    {
                        //danh sách con
                        var childs = dicData[item.Id];
                        foreach (var child in childs)
                        {
                            var childItem = new DocumentMapConfig()
                            {
                                Id = child.Id,
                                Name = child.Name,
                                Guide = child.Guide,
                                IsChecked = false,
                                ParentId = child.ParentId,
                                LstDocumentChild = new List<DocumentMapConfig>()
                            };
                            var keyConfigChild = $"{childItem.Id}-{typeOwnerShip}-{productId}";
                            if (dicConfig.ContainsKey(keyConfigChild))
                            {
                                childItem.IsChecked = true;
                                childItem.ConfigDocument = dicConfig[keyConfigChild];
                            }
                            //Nếu có con tiếp => add vào 
                            if (dicData.ContainsKey(child.Id))
                            {
                                var lstChild = dicData[child.Id].OrderBy(x => x.Id);
                                foreach (var child2 in lstChild)
                                {
                                    var childItem2 = new DocumentMapConfig()
                                    {
                                        Id = child2.Id,
                                        Name = child2.Name,
                                        Guide = child2.Guide,
                                        IsChecked = false,
                                        ParentId = child2.ParentId,
                                        LstDocumentChild = new List<DocumentMapConfig>()
                                    };
                                    var keyConfigChild2 = $"{childItem2.Id}-{typeOwnerShip}-{productId}";
                                    if (dicConfig.ContainsKey(keyConfigChild2))
                                    {
                                        childItem2.IsChecked = true;
                                        childItem2.ConfigDocument = dicConfig[keyConfigChild2];
                                    }
                                    childItem.LstDocumentChild.Add(childItem2);
                                }
                            }
                            itemDocumentMap.LstDocumentChild.Add(childItem);
                        }
                    }
                    //Thêm chứng từ đó vào kết quả trả về                  
                    result.Add(itemDocumentMap);
                }
            }
            return result;

        }
        /// <summary>
        /// Lấy ra danh sách chứng từ con và những chứng từ không có parentId
        /// </summary>
        /// <param name="listData"></param>
        /// <param name="listFiles"></param>
        /// <param name="groupJobId"></param>
        /// <returns></returns>
        public static List<DocumentMapInfo> ConvertDocumentTreeDetail(List<DocumentMapInfo> listData, List<LoanBriefFileDetail> listFiles, int groupJobId = 0)
        {

            var result = new List<DocumentMapInfo>();

            #region Xử lý dữ liệu cho dictionary
            //Tạo dictionary theo key: loại chứng từ
            //Value: D/s chứng từ của loại đó
            var dicFiles = new Dictionary<int, List<LoanBriefFileDetail>>();
            if(listFiles != null && listFiles.Count > 0)
            {
                foreach (var item in listFiles)
                {
                    if (item.TypeId > 0)
                    {
                        var key = item.TypeId.Value;
                        if (!dicFiles.ContainsKey(key))
                            dicFiles[key] = new List<LoanBriefFileDetail>();
                        dicFiles[key].Add(item);
                    }
                }
            }
            //Tạo HashSet chứa các loại chứng từ nằm trong danh mục của bộ sản phẩm
            
            HashSet<int> hashDocument = new HashSet<int>();
            
            //Tạo dictionary với key: ID của chứng từ
            //Value: D/s chứng từ con của ID đó
            foreach (var item in listData)
            {
                if (item.ParentId > 0)
                {
                    if (!hashDocument.Contains(item.ParentId.Value))
                        hashDocument.Add(item.ParentId.Value);
                }
            }

            //tạo dic lấy sort của parentId
            var dicDocumentParent = new Dictionary<int, int>();
            foreach (var item in listData)
            {
                if (hashDocument.Contains(item.Id))
                {
                    if (!dicDocumentParent.ContainsKey(item.Id))
                        dicDocumentParent[item.Id] = item.Sort ?? 0;
                }
            }
            #endregion

            foreach (var item in listData)
            {
                //xử lý những chứng từ không có cha
                if (hashDocument.Count == 0 || (hashDocument.Count > 0 && !hashDocument.Contains(item.Id)))
                {
                    if (groupJobId > 0 && !string.IsNullOrEmpty(item.GroupJobId))
                    {
                        if (!string.IsNullOrEmpty(item.GroupJobId))
                        {
                            var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(item.GroupJobId);
                            if (arr != null && !arr.Contains(groupJobId))
                                continue;
                        }
                    }
                    var itemDocumentMap = new DocumentMapInfo()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Guide = item.Guide,
                        ParentId = item.ParentId,
                        ProductId = item.ProductId,
                        NumberImage = item.NumberImage,
                        Required = item.Required,
                        TypeRequired = item.TypeRequired,
                        AllowFromUpload = item.AllowFromUpload,
                        GroupJobId = item.GroupJobId,
                        Sort = item.Sort,
                        ListChilds = new List<DocumentMapInfo>(),
                        ListFiles = new List<LoanBriefFileDetail>()

                    };
                    if(itemDocumentMap.Sort.GetValueOrDefault(0) == 0 && item.ParentId > 0 && dicDocumentParent.ContainsKey(item.ParentId.Value))
                        itemDocumentMap.Sort = dicDocumentParent[item.ParentId.Value];
                    
                    if (dicFiles.ContainsKey(itemDocumentMap.Id))
                        itemDocumentMap.ListFiles = dicFiles[itemDocumentMap.Id];

                    //Thêm chứng từ đó vào kết quả trả về                  
                    result.Add(itemDocumentMap);
                }
            }
            return result;

        }


        public static ResultCheckDocument CheckDocument(List<LoanBriefFileDetail> lstFiles, int idVideoHome, int idVideoJob, int idCVKDHome, int idCVKDJob, int idTimaCareHome, int idTimaCareJob, string rangeHome, string rangeJob)
        {
            //var 
            var result = new ResultCheckDocument();
            result.status = 1;
            //Check thẩm định nơi ở
            if (lstFiles.Where(x => x.TypeId == idVideoHome).Count() == 0)
            {
                //Kiểm tra ảnh CVKD chụp
                if (lstFiles.Where(x => x.TypeId == idCVKDHome).Count() == 0)
                {
                    //Kiểm tra ảnh KH chụp từ TimaCare
                    if (lstFiles.Where(x => x.TypeId == idTimaCareHome).Count() == 0)
                    {
                        result.status = 0;
                        result.mess = "Hồ sơ này cần thẩm định nơi ở";
                        return result;
                    }
                    else
                    {
                        //Check khoảng cách
                        var lstfile = lstFiles.Where(x => x.TypeId == idTimaCareHome).ToList();
                        foreach (var item in lstfile)
                        {
                            if (rangeHome != null && rangeHome != "0")
                            {
                                if (item.DistanceAddressHomeGoogleMap.GetValueOrDefault(0) > Convert.ToInt32(rangeHome))
                                {
                                    result.status = 0;
                                    result.mess = "Hồ sơ này cần thẩm định nơi ở";
                                    return result;
                                }
                            }

                        }
                    }

                }
            }
            //Check thẩm định nơi làm việc
            if (lstFiles.Where(x => x.TypeId == idVideoJob).Count() == 0)
            {
                //Kiểm tra ảnh CVKD chụp
                if (lstFiles.Where(x => x.TypeId == idCVKDJob).Count() == 0)
                {
                    //Kiểm tra ảnh KH chụp từ TimaCare
                    if (lstFiles.Where(x => x.TypeId == idTimaCareJob).Count() == 0)
                    {
                        result.status = 0;
                        result.mess = "Hồ sơ này cần thẩm định nơi làm việc";
                        return result;
                    }
                    else
                    {
                        //Check khoảng cách
                        var lstfile = lstFiles.Where(x => x.TypeId == idTimaCareHome).ToList();
                        foreach (var item in lstfile)
                        {
                            if (rangeJob != null && rangeJob != "0")
                            {
                                if (item.DistanceAddressCompanyGoogleMap.GetValueOrDefault(0) > Convert.ToInt32(rangeJob))
                                {
                                    result.status = 0;
                                    result.mess = "Hồ sơ này cần thẩm định nơi làm việc";
                                    return result;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="listData">Danh sách chứng từ theo gói sản phẩm và hình thức sở hưu nhà</param>
        /// <param name="listFiles">Danh sách file của đơn đó</param>
        /// <param name="fromUpload">Cho phép upload từ: web, cvkd, tima care</param>
        /// <param name="groupJobId">Nhóm công việc kh: chia làm 6 nhóm</param>
        /// <returns></returns>
        public static List<DocumentMapInfo> ConvertDocumentTree(List<DocumentMapInfo> listData, List<LoanBriefFileDetail> listFiles, int fromUpload = 0, int groupJobId = 0)
        {

            var result = new List<DocumentMapInfo>();

            #region Xử lý dữ liệu cho dictionary
            //Tạo dictionary theo key: loại chứng từ
            //Value: D/s chứng từ của loại đó
            var dicFiles = new Dictionary<int, List<LoanBriefFileDetail>>();
            foreach (var item in listFiles)
            {
                if (item.TypeId > 0)
                {
                    var key = item.TypeId.Value;
                    if (!dicFiles.ContainsKey(key))
                        dicFiles[key] = new List<LoanBriefFileDetail>();
                    dicFiles[key].Add(item);
                }
                else
                {
                    //Nếu không có danh mục chứng từ => add vào danh mục không xác định
                    var key = -100;
                    if (!dicFiles.ContainsKey(key))
                        dicFiles[key] = new List<LoanBriefFileDetail>();
                    dicFiles[key].Add(item);
                }

            }

            //Tạo HashSet chứa các loại chứng từ nằm trong danh mục của bộ sản phẩm
            HashSet<int> hashDocument = new HashSet<int>();

            //Tạo dictionary với key: ID của chứng từ
            //Value: D/s chứng từ con của ID đó
            var dicData = new Dictionary<int, List<DocumentMapInfo>>();
            foreach (var item in listData)
            {
                if (!hashDocument.Contains(item.Id))
                    hashDocument.Add(item.Id);
                if (item.ParentId > 0)
                {
                    var key = item.ParentId.Value;
                    if (!dicData.ContainsKey(key))
                        dicData[key] = new List<DocumentMapInfo>();
                    dicData[key].Add(item);
                }
            }
            #endregion

            foreach (var item in listData)
            {
                //xử lý những chứng từ không có cha
                if (item.ParentId.GetValueOrDefault(0) == 0)
                {
                    var itemDocumentMap = new DocumentMapInfo()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Guide = item.Guide,
                        ParentId = item.ParentId,
                        ProductId = item.ProductId,
                        NumberImage = item.NumberImage,
                        Required = item.Required,
                        TypeRequired = item.TypeRequired,
                        AllowFromUpload = item.AllowFromUpload,
                        GroupJobId = item.GroupJobId,
                        ListChilds = new List<DocumentMapInfo>(),
                        ListFiles = new List<LoanBriefFileDetail>()

                    };
                    //Nếu xác định nguồn upload
                    if (fromUpload > 0 && !string.IsNullOrEmpty(itemDocumentMap.AllowFromUpload))
                    {
                        if (!string.IsNullOrEmpty(itemDocumentMap.AllowFromUpload))
                        {
                            var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(itemDocumentMap.AllowFromUpload);
                            if (arr != null && !arr.Contains(fromUpload))
                                continue;
                        }
                    }
                    //Nếu xác nhóm công việc
                    if (groupJobId > 0 && !string.IsNullOrEmpty(itemDocumentMap.GroupJobId))
                    {
                        if (!string.IsNullOrEmpty(itemDocumentMap.GroupJobId))
                        {
                            var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(itemDocumentMap.GroupJobId);
                            if (arr != null && !arr.Contains(groupJobId))
                                continue;
                        }
                    }
                    //kiểm tra xem có file không
                    if (dicFiles.ContainsKey(itemDocumentMap.Id))
                        itemDocumentMap.ListFiles = dicFiles[itemDocumentMap.Id];
                    var key = item.Id;
                    if (dicData.ContainsKey(item.Id))
                    {
                        //danh sách con
                        var childs = dicData[item.Id];
                        foreach (var child in childs)
                        {
                            var childItem = new DocumentMapInfo()
                            {
                                Id = child.Id,
                                Name = child.Name,
                                Guide = child.Guide,
                                ParentId = child.ParentId,
                                ProductId = child.ProductId,
                                NumberImage = child.NumberImage,
                                Required = child.Required,
                                TypeRequired = child.TypeRequired,
                                AllowFromUpload = child.AllowFromUpload,
                                GroupJobId = child.GroupJobId,
                                ListChilds = new List<DocumentMapInfo>(),
                                ListFiles = new List<LoanBriefFileDetail>()
                            };
                            //Nếu xác định nguồn upload
                            if (fromUpload > 0 && !string.IsNullOrEmpty(childItem.AllowFromUpload))
                            {
                                if (!string.IsNullOrEmpty(childItem.AllowFromUpload))
                                {
                                    var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(childItem.AllowFromUpload);
                                    if (arr != null && !arr.Contains(fromUpload))
                                        continue;
                                }
                            }
                            //Nếu xác nhóm công việc
                            if (groupJobId > 0 && !string.IsNullOrEmpty(childItem.GroupJobId))
                            {
                                if (!string.IsNullOrEmpty(childItem.GroupJobId))
                                {
                                    var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(childItem.GroupJobId);
                                    if (arr != null && !arr.Contains(groupJobId))
                                        continue;
                                }
                            }
                            //kiểm tra xem có file không
                            if (dicFiles.ContainsKey(childItem.Id))
                                childItem.ListFiles = dicFiles[childItem.Id];
                            //Nếu có con tiếp => add vào 
                            if (dicData.ContainsKey(child.Id))
                            {
                                var lstChild = dicData[child.Id].OrderBy(x => x.Id);
                                foreach (var child2 in lstChild)
                                {
                                    var childItem2 = new DocumentMapInfo()
                                    {
                                        Id = child2.Id,
                                        Name = child2.Name,
                                        Guide = child2.Guide,
                                        ParentId = child2.ParentId,
                                        ProductId = child2.ProductId,
                                        NumberImage = child2.NumberImage,
                                        Required = child2.Required,
                                        TypeRequired = child2.TypeRequired,
                                        AllowFromUpload = child2.AllowFromUpload,
                                        GroupJobId = child2.GroupJobId,
                                        ListChilds = new List<DocumentMapInfo>(),
                                        ListFiles = new List<LoanBriefFileDetail>()
                                    };

                                    //Nếu xác định nguồn upload
                                    if (fromUpload > 0 && !string.IsNullOrEmpty(childItem2.AllowFromUpload))
                                    {
                                        if (!string.IsNullOrEmpty(childItem2.AllowFromUpload))
                                        {
                                            var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(childItem2.AllowFromUpload);
                                            if (arr != null && !arr.Contains(fromUpload))
                                                continue;
                                        }
                                    }
                                    //Nếu xác nhóm công việc
                                    if (groupJobId > 0 && !string.IsNullOrEmpty(childItem2.GroupJobId))
                                    {
                                        if (!string.IsNullOrEmpty(childItem2.GroupJobId))
                                        {
                                            var arr = LOS.Common.Helpers.ExtentionHelper.ConvertToInt(childItem2.GroupJobId);
                                            if (arr != null && !arr.Contains(groupJobId))
                                                continue;
                                        }
                                    }
                                    //kiểm tra xem có file không
                                    if (dicFiles.ContainsKey(childItem2.Id))
                                        childItem2.ListFiles = dicFiles[childItem2.Id];
                                    childItem.ListChilds.Add(childItem2);
                                }
                            }
                            itemDocumentMap.ListChilds.Add(childItem);
                        }
                    }
                    //Thêm chứng từ đó vào kết quả trả về                  
                    result.Add(itemDocumentMap);
                }
            }
            //Kiểm tra xem có file nào khong nằm trong d/s chứng từ của bộ sản phẩm không 
            //=> Gán thành 1 danh mục chứng từ không xác định
            var documentUnknown = new DocumentMapInfo()
            {
                Name = "Không xác định",
                ListFiles = new List<LoanBriefFileDetail>()
            };
            foreach (var key in dicFiles.Keys)
            {
                if (!hashDocument.Contains(key))
                {
                    var files = dicFiles[key];
                    if (files.Count > 0)
                        documentUnknown.ListFiles.AddRange(files);
                }
            }
            if (documentUnknown.ListFiles != null && documentUnknown.ListFiles.Count > 0)
                result.Add(documentUnknown);
            return result;

        }

        public static DocumentMapInfo ValidRequiredDocument(List<DocumentMapInfo> resultDocuments, List<int> listDocumentBreak = null)
        {
            if (resultDocuments != null && resultDocuments.Count > 0)
            {
                var isPassNational = true;
                var isPassPassport = true;
                foreach (var document in resultDocuments)
                {
                    if (listDocumentBreak != null && listDocumentBreak.Count > 0 && listDocumentBreak.Contains(document.Id))
                        continue;
                    //Kiểm tra chứng từ đó có băt buộc không
                    if (document.Required == true)
                    {
                        //kiểm tra có con không
                        if (document.ListChilds != null && document.ListChilds.Count > 0)
                        {
                            var isPass = false;
                            //kiểm tra loại bắt buộc các con
                            if (document.TypeRequired == (int)EnumTypeRequired.Or)
                            {
                                foreach (var child in document.ListChilds)
                                {
                                    //Kiểm tra xem có bắt buộc hay không
                                    if (child.Required == true)
                                    {
                                        //Kiểm tra xem có con không
                                        if (child.ListChilds != null && child.ListChilds.Count > 0)
                                        {
                                            if (child.TypeRequired == (int)EnumTypeRequired.Or)
                                            {
                                                foreach (var child2 in child.ListChilds)
                                                {
                                                    if (child2.NumberImage > 0)
                                                    {
                                                        //Nếu đạt điều kiện chứng từ => đánh dấu pass document
                                                        if (child2.ListFiles != null && child2.ListFiles.Count >= child2.NumberImage)
                                                        {
                                                            isPass = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (isPass)
                                                    break;
                                            }
                                            else if (child.TypeRequired == (int)EnumTypeRequired.Full)
                                            {
                                                int dem = 0;
                                                foreach (var child2 in child.ListChilds)
                                                {
                                                    if (child2.NumberImage > 0 && child2.Required.GetValueOrDefault(false))
                                                    {
                                                        //Nếu đạt điều kiện chứng từ => đánh dấu pass document
                                                        if (child2.ListFiles != null && child2.ListFiles.Count >= child2.NumberImage)
                                                        {
                                                            dem++;
                                                        }

                                                    }
                                                }
                                                if (dem == child.ListChilds.Count(x => x.Required.GetValueOrDefault(false)))
                                                {
                                                    isPass = true;
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //Kiểm tra xem có chứng từ chưa
                                            if (child.NumberImage > 0)
                                            {
                                                //Nếu đạt điều kiện chứng từ => đánh dấu pass document
                                                if (child.ListFiles != null && child.ListFiles.Count >= child.NumberImage)
                                                {
                                                    isPass = true;
                                                    break;
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                            //Tất cả các con
                            else if (document.TypeRequired == (int)EnumTypeRequired.Full)
                            {
                                var childPass = 0;
                                foreach (var child in document.ListChilds)
                                {
                                    //Kiểm tra xem có bắt buộc hay không
                                    if (child.Required == true)
                                    {
                                        //Kiểm tra xem có con không
                                        if (child.ListChilds != null && child.ListChilds.Count > 0)
                                        {
                                            if (child.TypeRequired == (int)EnumTypeRequired.Or)
                                            {
                                                foreach (var child2 in child.ListChilds)
                                                {
                                                    if (child2.NumberImage > 0)
                                                    {
                                                        //Nếu đạt điều kiện chứng từ => đánh dấu pass document
                                                        if (child2.ListFiles != null && child2.ListFiles.Count >= child2.NumberImage)
                                                        {
                                                            childPass++; ;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            else if (child.TypeRequired == (int)EnumTypeRequired.Full)
                                            {
                                                int dem = 0;
                                                foreach (var child2 in child.ListChilds)
                                                {
                                                    if (child2.NumberImage > 0 && child2.Required.GetValueOrDefault(false))
                                                    {
                                                        //Nếu đạt điều kiện chứng từ => đánh dấu pass document
                                                        if (child2.ListFiles != null && child2.ListFiles.Count >= child2.NumberImage)
                                                        {
                                                            dem++;
                                                        }

                                                    }
                                                }
                                                if (dem == child.ListChilds.Count(x => x.Required.GetValueOrDefault(false)))
                                                {
                                                    childPass++;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //Kiểm tra xem có chứng từ chưa
                                            if (child.NumberImage > 0)
                                            {
                                                //Nếu đạt điều kiện chứng từ => đánh dấu pass document
                                                if (child.ListFiles != null && child.ListFiles.Count >= child.NumberImage)
                                                {
                                                    childPass++;
                                                }
                                            }
                                        }

                                    }
                                }
                                if (childPass == document.ListChilds.Count)
                                    isPass = true;
                            }
                            if (!isPass)
                            {
                                if (document.Id == (int)EnumDocumentType.EnumNationalCardType || document.Id == (int)EnumDocumentType.EnumPassportType)
                                {
                                    if (document.Id == (int)EnumDocumentType.EnumNationalCardType)
                                        isPassNational = false;
                                    else
                                        isPassPassport = false;

                                    if (!isPassPassport && !isPassNational)
                                        return document;
                                }
                                else
                                {
                                    return document;
                                }
                            }

                        }
                        else
                        {
                            //Kiểm tra xem có chứng từ chưa
                            if (document.NumberImage > 0)
                            {
                                //Nếu không có chứng từ hoặc số lượng ít hơn quy định
                                //Trả ra thông báo
                                if (document.ListFiles == null || document.ListFiles.Count < document.NumberImage)
                                {
                                    if (document.Id == (int)EnumDocumentType.EnumNationalCardType || document.Id == (int)EnumDocumentType.EnumPassportType)
                                    {
                                        if (document.Id == (int)EnumDocumentType.EnumNationalCardType)
                                            isPassNational = false;
                                        else
                                            isPassPassport = false;

                                        if (!isPassPassport && !isPassNational)
                                            return document;
                                    }
                                    else
                                    {
                                        return document;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Kiểm tra tính hợp lệ của bộ chứng từ: So sánh khoảng cách và thời gian chụp các chứng từ
        /// </summary>
        /// <param name="distanceMax">Khoảng cách cho phép giữa các ảnh: đơn vị met</param>
        /// <param name="timeMax">Thời gian tối đa chụp đủ các ảnh: đơn vị phút</param>
        public static ResultCompare ValidLoanbriefFiles(List<LoanBriefFileDetail> listFiles, int distanceMax = 0, int timeMax = 0, bool isCheckStep = false)
        {
            if (distanceMax > 0 || timeMax > 0)
            {
                var totalFile = listFiles.Count;
                //Kiểm tra tính thời gian tối đa chụp các ảnh
                if (timeMax > 0)
                {
                    listFiles = listFiles.OrderBy(x => x.CreateAt).ToList();
                    var totalMinutes = (listFiles[totalFile - 1].CreateAt.Value - listFiles[0].CreateAt.Value).TotalMinutes;
                    if (totalMinutes > timeMax)
                    {
                        return new ResultCompare()
                        {
                            totalTime = totalMinutes,
                            fromDocumentId = listFiles[0].Id,
                            toDocumentId = listFiles[totalFile - 1].Id,
                        };
                    }
                    //Kiểm tra chứng bộ chứng từ có chứng từ của KH upload và nằm ở giữa chứng từ cvkd upload không
                    if (isCheckStep)
                    {
                        //có chứng từ upload từ tima care
                        if (listFiles.Any(x => x.SourceUpload == (int)EnumSourceUpload.TimaCare && !string.IsNullOrEmpty(x.LatLong)))
                        {
                            //chứng từ upload từ tima care ở đầu hoặc cuối danh sách đã sắp xếp => thông báo 
                            if (listFiles[0].SourceUpload == (int)EnumSourceUpload.TimaCare || listFiles[totalFile - 1].SourceUpload == (int)EnumSourceUpload.TimaCare)
                            {
                                return new ResultCompare()
                                {
                                    InValidStep = true,
                                    fromDocumentId = listFiles[0].Id,
                                    toDocumentId = listFiles[totalFile - 1].Id,
                                };
                            }
                        }
                    }

                }
                //kiển tra tính khoảng cách
                if (distanceMax > 0)
                {
                    HashSet<int> listCompare = new HashSet<int>();
                    HashSet<string> listLatLong = new HashSet<string>();
                    for (int i = 0; i < totalFile; i++)
                    {
                        var start = listFiles[i];
                        if (!string.IsNullOrEmpty(start.LatLong) && !listLatLong.Contains(start.LatLong))
                            listLatLong.Add(start.LatLong);
                        //Nếu là phần tử cuối thì k cần so sánh
                        if (i == totalFile - 1)
                            continue;
                        //Gán đối tượng cần so sánh

                        //thêm vị trí đã so sánh rồi => không cần so sánh lại
                        listCompare.Add(i);
                        //Nếu không có latlng => bỏ qua
                        if (string.IsNullOrEmpty(start.LatLong))
                            continue;
                        for (int j = 0; j < totalFile; j++)
                        {
                            //Nếu đã so sánh => bỏ qua
                            if (listCompare.Contains(j)) continue;

                            if (!string.IsNullOrEmpty(listFiles[j].LatLong))
                            {
                                var distince = DistanceTwoPoint(start.LatLong, listFiles[j].LatLong);
                                //Khoảng cách lớn hơn giới hạn => return;
                                if (distince > distanceMax)
                                {
                                    return new ResultCompare()
                                    {
                                        distance = distince,
                                        fromDocumentId = start.Id,
                                        toDocumentId = listFiles[j].Id,
                                    };
                                }
                            }
                        }

                    }
                    if (listLatLong != null && listLatLong.Count > 0)
                    {
                        return new ResultCompare()
                        {
                            ListLatLong = listLatLong
                        };
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Kiểm tra tính hợp lệ của chứng từ so với 1 điểm
        /// </summary>
        /// <param name="latlng"></param>
        /// <param name="listFiles"></param>
        /// <param name="distanceMax"></param>
        /// <param name="timeMax"></param>
        /// <returns></returns>
        public static ResultCompare ValidLoanbriefFiles(LoanBriefFileDetail fromFile, List<LoanBriefFileDetail> listFiles, int distanceMax = 0)
        {
            //kiển tra tính khoảng cách
            if (distanceMax > 0 && !string.IsNullOrEmpty(fromFile.LatLong))
            {
                foreach (var item in listFiles)
                {
                    if (!string.IsNullOrEmpty(item.LatLong))
                    {
                        var distince = DistanceTwoPoint(fromFile.LatLong, item.LatLong);
                        //Khoảng cách lớn hơn giới hạn => return;
                        if (distince > distanceMax)
                        {
                            return new ResultCompare()
                            {
                                distance = distince,
                                fromDocumentId = fromFile.Id,
                                toDocumentId = item.Id,
                            };
                        }
                    }
                }
            }
            return null;
        }

        public static double DistanceTwoPoint(string fromAddress, string toAddress)
        {

            try
            {
                double lat = 0l, latTo = 0l, lng = 0l, lngTo = 0l;
                ConvertToPoint(fromAddress, ref lat, ref lng);
                ConvertToPoint(toAddress, ref latTo, ref lngTo);
                var R = 6371; //Km
                var dLat = Radians(latTo) - Radians(lat);
                var dLon = Radians(lngTo) - Radians(lng);
                var a = Math.Pow(Math.Sin(dLat / 2), 2) + (Math.Pow(Math.Sin(dLon / 2), 2) * Math.Cos(Radians(lat)) * Math.Cos(Radians(latTo)));
                var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
                return Math.Round(R * c * 1000, 0);
            }
            catch (Exception e)
            {
                return 0;
            }

        }
        private static double Radians(double x)
        {
            return x * Math.PI / 180;
        }
        private static void ConvertToPoint(string sLatLng, ref double lat, ref double lng)
        {
            try
            {
                if (sLatLng.Contains(","))
                {
                    var arr = sLatLng.Split(",");
                    if (arr.Length == 2)
                    {
                        lat = Convert.ToDouble(arr[0]);
                        lng = Convert.ToDouble(arr[1]);
                    }
                }
            }
            catch
            {

            }
        }

        public static List<DocumentMapInfo> ConvertDocumentOto(List<DocumentTypeDetail> listData, List<LoanBriefFileDetail> listFiles)
        {

            var result = new List<DocumentMapInfo>();

            //Tạo dictionary theo key: loại chứng từ
            //Value: D/s chứng từ của loại đó
            var dicFiles = new Dictionary<int, List<LoanBriefFileDetail>>();
            foreach (var item in listFiles)
            {
                if (item.TypeId > 0)
                {
                    var key = item.TypeId.Value;
                    if (!dicFiles.ContainsKey(key))
                        dicFiles[key] = new List<LoanBriefFileDetail>();
                    dicFiles[key].Add(item);
                }
                else
                {
                    //Nếu không có danh mục chứng từ => add vào danh mục không xác định
                    var key = -100;
                    if (!dicFiles.ContainsKey(key))
                        dicFiles[key] = new List<LoanBriefFileDetail>();
                    dicFiles[key].Add(item);
                }

            }

            //Tạo HashSet chứa các loại chứng từ nằm trong danh mục của bộ sản phẩm
            HashSet<int> hashDocument = new HashSet<int>();

            //Tạo dictionary với key: ID của chứng từ
            //Value: D/s chứng từ con của ID đó
            var dicData = new Dictionary<int, List<DocumentMapInfo>>();

            foreach (var item in listData)
            {
                var itemDocumentMap = new DocumentMapInfo()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Guide = item.Guide,
                    ParentId = item.ParentId,
                    ProductId = item.ProductId,
                    NumberImage = item.NumberImage,
                    Required = item.Required,
                    ListChilds = new List<DocumentMapInfo>(),
                    ListFiles = new List<LoanBriefFileDetail>()

                };
                //kiểm tra xem có file không
                if (dicFiles.ContainsKey(itemDocumentMap.Id))
                    itemDocumentMap.ListFiles = dicFiles[itemDocumentMap.Id];
                var key = item.Id;
                //Thêm chứng từ đó vào kết quả trả về                  
                result.Add(itemDocumentMap);
            }
            //Kiểm tra xem có file nào khong nằm trong d/s chứng từ của bộ sản phẩm không 
            //=> Gán thành 1 danh mục chứng từ không xác định
            var documentUnknown = new DocumentMapInfo()
            {
                Name = "Không xác định",
                ListFiles = new List<LoanBriefFileDetail>()
            };
            foreach (var key in dicFiles.Keys)
            {
                if (!hashDocument.Contains(key))
                {
                    var files = dicFiles[key];
                    if (files.Count > 0)
                        documentUnknown.ListFiles.AddRange(files);
                }
            }
            if (documentUnknown.ListFiles != null && documentUnknown.ListFiles.Count > 0)
                result.Add(documentUnknown);
            return result;

        }
    }
}
