﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp
{
    public class HubManagerHub : Hub
	{
		public async Task SendMessageToGroup(string groupName, string message)
		{
			await Clients.Group(groupName).SendAsync("Send", message);
		}

		public async Task JoinGroup(string groupName)
		{
			await Groups.AddToGroupAsync(Context.ConnectionId, groupName);

			await Clients.Group(groupName).SendAsync("Send", $"{Context.ConnectionId} joined {groupName}");
		}

		public async Task LeaveGroup(string groupName)
		{
			await Clients.Group(groupName).SendAsync("Send", $"{Context.ConnectionId} left {groupName}");

			await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
		}
	}
}