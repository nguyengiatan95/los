﻿using LOS.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class AddDeviceViewModel
    {
        public int LoanBriefId { get; set; }
        public string DeviceCode { get; set; }

        public int DeviceStatus { get; set; }
        public string DeviceStatusName { get; set; }
        public DateTime? LastSendRequest { get; set; }
        public DateTime? ActivedDeviceAt { get; set; }

        public string TextButton
        {
            get
            {
                if (DeviceStatus == StatusOfDeviceID.InActived.GetHashCode())
                    return "Gắn mã thiết bị định vị";
                if (DeviceStatus == StatusOfDeviceID.OpenContract.GetHashCode())
                    return "Thay thiết bị";
                if (DeviceStatus == StatusOfDeviceID.Actived.GetHashCode())
                    return "Thay thiết bị";
                if (DeviceStatus == StatusOfDeviceID.Close.GetHashCode() || DeviceStatus == StatusOfDeviceID.Fail.GetHashCode())
                    return "Thay thiết bị";
                return string.Empty;
            }
        }
    }
}
