﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LoginCaresoft
    {
        public string token { get; set; }
    }
    public class OutputLoginCaresoft
    {
        public string code { get; set; }
        public UserCareSoftInfo user { get; set; }
    }
    public class PayloadToken
    {
        public string ipphone { get; set; }
        public string expired { get; set; }
    }

    public class UserCareSoftInfo
    {
        public int accountId { get; set; }
        public int account_id { get; set; }
        public string userId { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string agentId { get; set; }
        public string agent_id { get; set; }
        public string phone_no { get; set; }
        public string ipAddress { get; set; }
        public string groupId { get; set; }
        public string role { get; set; }
        public string active { get; set; }
    }
}
