﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class RecordItemRes
    {
        public string code { get; set; }
        public int numFound { get; set; }
        public List<RecordItem> calls { get; set; }
    }

    public class RecordItem
    {
        public int id { get; set; }
        public int customer_id { get; set; }
        public string call_id { get; set; }
        public string path { get; set; }
        public string path_download { get; set; }
        public string caller { get; set; }
        public string called { get; set; }
        public string user_id { get; set; }
        public string agent_id { get; set; }
        public string group_id { get; set; }
        public string call_type { get; set; }
        public DateTime? start_time { get; set; }
        public string call_status { get; set; }
        public DateTime? end_time { get; set; }
        public string wait_time { get; set; }
        public string hold_time { get; set; }
        public string talk_time { get; set; }
        public string end_status { get; set; }
        public string user_name { get; set; }
        public string department_name { get; set; }

        public string full_name { get; set; }
        public string relationship_name { get; set; }
    }

    public class RecordItemResV2
    {
        [JsonProperty("data")]
        public List<RecordItemV2> Data { get; set; }

        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("total")]
        public long Total { get; set; }

        [JsonProperty("page")]
        public long Page { get; set; }

        [JsonProperty("pageCount")]
        public long PageCount { get; set; }
    }

    public class RecordApiCaresoftRes
    {
        [JsonProperty("calls")]
        public List<RecordItemV2> Data { get; set; }
    }

    public partial class RecordItemV2
    {

        [JsonProperty("CALL_TYPE")]
        public long CallType { get; set; }

        [JsonProperty("CALLER")]
        public string Caller { get; set; }

        [JsonProperty("CALLED")]
        public string Called { get; set; }

        [JsonProperty("START_TIME")]
        public DateTime? StartTime { get; set; }

        [JsonProperty("END_TIME")]
        public DateTime? EndTime { get; set; }

        [JsonProperty("ACD_TIME")]
        public long AcdTime { get; set; }       

        [JsonProperty("CALLED_TYPE")]
        public long CalledType { get; set; }


        [JsonProperty("DEVICE_ID")]
        public string DeviceId { get; set; }


        [JsonProperty("DURATION")]
        public long Duration { get; set; }


        [JsonProperty("WAITING_TIME")]
        public long WaitingTime { get; set; }


        [JsonProperty("AGENT_ID")]
        public string AgentId { get; set; }

        [JsonProperty("LAST_AGENT_ID")]
        public long LastAgentId { get; set; }

        [JsonProperty("GROUP_ID")]
        public long GroupId { get; set; }


        [JsonProperty("ACCOUNT_ID")]
        public long AccountId { get; set; }

        [JsonProperty("TICKET_ID")]
        public long TicketId { get; set; }

        [JsonProperty("PATH")]
        public string Path { get; set; }
        [JsonProperty("PATH_DOWNLOAD")]
        public string Path_Download { get; set; }

        [JsonProperty("wait_time")]
        public string wait_time { get; set; }
        [JsonProperty("talk_time")]
        public string talk_time { get; set; }

        [JsonProperty("call_status")]
        public string call_status { get; set; }


        public string user_name { get; set; }
        public string department_name { get; set; }

        public string full_name { get; set; }
        public string relationship_name { get; set; }

    }
}
