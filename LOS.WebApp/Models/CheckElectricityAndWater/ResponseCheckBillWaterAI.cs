﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ResponseCheckBillWaterAI
    {
        public DataCheckBillWaterAI data { get; set; }
        public string message { get; set; }
        public int status { get; set; }
    }
    public class DataCheckBillWaterAI
    {
        public string bill_id { get; set; }
        public InfoCheckBillWaterAI info { get; set; }
        public string water_supplier { get; set; }
    }
    public class InfoCheckBillWaterAI
    {
        public string address { get; set; }
        public string name { get; set; }
    }
}

