﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ResponseCheckElectricityAI
    {
        public DataCheckElectricityAI data { get; set; }
        public string message { get; set; }
        public int status { get; set; }
    }
    public class DataCheckElectricityAI
    {
        public string bill_id { get; set; }
        public InfoCheckElectricityAI info { get; set; }
    }
    public class InfoCheckElectricityAI
    {
        public string address { get; set; }
        public string companyName { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string personalid { get; set; }
        public string phoneNumber { get; set; }
    }
}
