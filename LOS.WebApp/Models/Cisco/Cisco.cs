﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Cisco
{
    public class Cisco
    {
        #region Authorize
        public partial class AuthorizeInput
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("userId")]
            public string UserId { get; set; }

            [JsonProperty("password")]
            public string Password { get; set; }

            [JsonProperty("data")]
            public Data Data { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("domain")]
            public string Domain { get; set; }
        }
        public partial class AuthorizeOutput
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("status")]
            public long Status { get; set; }

            [JsonProperty("userName")]
            public UserName UserName { get; set; }

            [JsonProperty("lang")]
            public string Lang { get; set; }

            [JsonProperty("country")]
            public string Country { get; set; }

            [JsonProperty("roles")]
            public string[] Roles { get; set; }

            [JsonProperty("bundle")]
            public string Bundle { get; set; }

            public string CSRFTOKEN { get; set; }
            public string JSESSIONID { get; set; }

        }
        public partial class UserName
        {
            [JsonProperty("first")]
            public string First { get; set; }

            [JsonProperty("last")]
            public string Last { get; set; }
        }
        #endregion

        #region List by Date
        public partial class GetList
        {
            [JsonProperty("queryCalculations")]
            public QueryCalculations QueryCalculations { get; set; }

            [JsonProperty("id")]
            public long Id { get; set; }

            public string Username { get; set; }
            public string DepartmentName { get; set; }

            [JsonProperty("startTime")]
            public long StartTime { get; set; }

            [JsonProperty("tz")]
            public string Tz { get; set; }

            [JsonProperty("callDuration")]
            public long? CallDuration { get; set; }

            [JsonProperty("offset")]
            public long Offset { get; set; }

            [JsonProperty("ani")]
            public string Ani { get; set; }
            [JsonProperty("line")]
            public string Line { get; set; }

            [JsonProperty("dnis")]
            public string Dnis { get; set; }

            [JsonProperty("audioUploadState")]
            public long AudioUploadState { get; set; }

            [JsonProperty("screenUploadState")]
            public long ScreenUploadState { get; set; }

            [JsonProperty("recordingType")]
            public string RecordingType { get; set; }

            [JsonProperty("icmCallId")]
            public string IcmCallId { get; set; }

            [JsonProperty("assocCallId")]
            public string AssocCallId { get; set; }

            [JsonProperty("contactType")]
            public string ContactType { get; set; }

            [JsonProperty("agent")]
            public Agent Agent { get; set; }

            [JsonProperty("isRootContact")]
            public bool IsRootContact { get; set; }


            [JsonProperty("group")]
            public Group Group { get; set; }

            [JsonProperty("recordingUrl")]
            public string RecordingUrl { get; set; }

            public string full_name { get; set; }
            public string relationship_name { get; set; }

        }
        public partial class QueryCalculations
        {
            [JsonProperty("avgCallDuration")]
            public string AvgCallDuration { get; set; }

            [JsonProperty("avgScore")]
            public string AvgScore { get; set; }

            [JsonProperty("count")]
            public string Count { get; set; }
        }
        public partial class Agent
        {
            [JsonProperty("displayId")]
            public string DisplayId { get; set; }

            [JsonProperty("lastName")]
            public string LastName { get; set; }

            [JsonProperty("firstName")]
            public string FirstName { get; set; }

            [JsonProperty("username")]
            public string Username { get; set; }
        }
        public partial class Group
        {
            [JsonProperty("name")]
            public string Name { get; set; }
        }
        #endregion

        #region Detail
        public partial class ExportInput
        {
            [JsonProperty("mediaFormat")]
            public string MediaFormat { get; set; }            
        }
        public partial class ExportDetail
        {
            [JsonProperty("id")]
            public long Id { get; set; }

            [JsonProperty("exportUrl")]
            public string ExportUrl { get; set; }

            [JsonProperty("mediaFormat")]
            public string MediaFormat { get; set; }

            [JsonProperty("isComplete")]
            public bool IsComplete { get; set; }
        }
        #endregion
        public partial class ListExport
        {
            public string Phone { get; set; }
            public string DepartmentName { get; set; }

            public string Prefix { get; set; }
            public long CallDuration { get; set; }
            public TimeSpan Time { get; set; }

        }

        public partial class GetDataCisco
        {
            [JsonProperty("meta")]
            public Meta Meta { get; set; }

            [JsonProperty("data")]
            public List<lstCisco> Data { get; set; }
        }

        public partial class lstCisco
        {
            [JsonProperty("startTime")]
            public DateTime? StartTime { get; set; }

            [JsonProperty("callDuration")]
            public long CallDuration { get; set; }

            [JsonProperty("line")]
            public string Line { get; set; }

            [JsonProperty("agentUserName")]
            public string AgentUserName { get; set; }

            [JsonProperty("recordingUrl")]
            public string RecordingUrl { get; set; }

            [JsonProperty("playUrl")]
            public string PlayUrl { get; set; }

            [JsonProperty("phoneNumber")]
            public string PhoneNumber { get; set; }

            [JsonProperty("contactType")]
            public string ContactType { get; set; }            
            public string Username { get; set; }
            public string DepartmentName { get; set; }
            public string FullName { get; set; }
            public string RelationshipName { get; set; }
        }

        public partial class Meta
        {
            [JsonProperty("errorCode")]
            public long ErrorCode { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrorMessage { get; set; }
        }


        public partial class PushToCiscoInput
        {
            [JsonProperty("data")]
            public List<DT> DT { get; set; }
        }

        public partial class DT
        {
            [JsonProperty("phone")]
            public string Phone { get; set; }
        }
        public partial class PushToCiscoOutput
        {
            [JsonProperty("code")]
            public long Code { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }

            [JsonProperty("userMessage")]
            public object UserMessage { get; set; }

            [JsonProperty("error")]
            public long Error { get; set; }

            [JsonProperty("totalPage")]
            public long TotalPage { get; set; }

            [JsonProperty("totalRow")]
            public long TotalRow { get; set; }

            [JsonProperty("pageSize")]
            public long PageSize { get; set; }

            [JsonProperty("pageCurrent")]
            public long PageCurrent { get; set; }

            [JsonProperty("data")]
            public long Data { get; set; }
        }
    }
}
