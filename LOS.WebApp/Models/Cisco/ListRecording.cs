﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static LOS.WebApp.Models.Cisco.Cisco;

namespace LOS.WebApp.Models.Cisco
{
    public class ListRecording
    {
        public int LoanbriefId { get; set; }
        public string Fullname { get; set; }
        public List<RecordItem> LstCareSoft { get; set; }
        public List<RecordItemV2> LstCareSoftV2 { get; set; }
        public List<GetList> LstCisco { get; set; }
        public List<lstCisco> LstCiscoV2 { get; set; }
    }
}
