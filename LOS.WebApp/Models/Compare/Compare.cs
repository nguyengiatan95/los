﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Compare
{
    public class Compare
    {
        public class LoadDataCMNDReq
        {
            public int LoanBriefId { get; set; }
            public int Step { get; set; }
            public List<LoanBriefFileDetail> LstFiles { get; set; }
        }
        public class CMND
        {
            public LoanInfoCompare LoanInfo { get; set; }
            public List<LoanBriefFileDetail> lstFilesRoot { get; set; }
            public List<LoanBriefFileDetail> lstFilesCompare { get; set; }
            public List<RelativeFamilyDetail> ListRelativeFamilys { get; set; }
        }
    }
}
