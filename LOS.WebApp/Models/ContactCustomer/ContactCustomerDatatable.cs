﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.ContactCustomer
{

    public class ContactCustomerDatatable : DatatableBase
    {
        public ContactCustomerDatatable(IFormCollection form) : base(form)
        {
            name = form["query[filterName]"].FirstOrDefault();
            phone = form["query[filterPhone]"].FirstOrDefault();
            province = form["query[filterProvince]"].FirstOrDefault();
        }
        public string name { get; set; }
        public string phone { get; set; }
        public string province { get; set; }
        public int createBy { get; set; }
    }
}
