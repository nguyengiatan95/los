﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.ContactCustomer
{
    public class ContactCustomerViewModel
    {
        public int ContactId { get; set; }
        public string FullName { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public int? JobId { get; set; }
        public decimal? Income { get; set; }
        public bool? HaveCarsOrNot { get; set; }
        public int? BrandProductId { get; set; }
        public int? ProductId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Phone { get; set; }
    }
}
