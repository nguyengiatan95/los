﻿namespace LOS.WebApp.Models
{
    public class CustomSelectListItem
    {
        public CustomSelectListItem(string text, string value)
        {
            Text = text;
            Value = value;
        }
        public CustomSelectListItem()
        { }
        public string Text { get; set; }
        public string Value { get; set; }

        public bool Selected { get; set; }
    }
}
