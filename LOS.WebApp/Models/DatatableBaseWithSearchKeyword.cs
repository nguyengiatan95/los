﻿using Microsoft.AspNetCore.Http;
using System.Linq;

namespace LOS.WebApp.Models
{
    public class DatatableBaseWithSearchKeyword : DatatableBase
    {
        public DatatableBaseWithSearchKeyword(IFormCollection form) : base(form)
        {
            Keyword = form["query[keyword]"].FirstOrDefault();
        }
        public string Keyword { get; set; }
    }
}
