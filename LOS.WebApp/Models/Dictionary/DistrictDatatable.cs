﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Dictionary
{
    public class DistrictDatatable : DatatableBase
    {
        public DistrictDatatable(IFormCollection form) : base(form)
        {
            searchName = form["query[filterName]"].FirstOrDefault();
            status = form["query[status]"].FirstOrDefault();
            provinceId = form["query[provinceId]"].FirstOrDefault();
        }

        public string searchName { get; set; }
        public string status { get; set; }
        public string provinceId { get; set; }
    }
}
