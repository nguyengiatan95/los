﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Dictionary
{
    public class ProvinceDatatable : DatatableBase
    {
        public ProvinceDatatable(IFormCollection form) : base(form)
        {
            searchName = form["query[filterName]"].FirstOrDefault();
            status = form["query[status]"].FirstOrDefault();
        }

        public string searchName { get; set; }
        public string status { get; set; }
    }
}
