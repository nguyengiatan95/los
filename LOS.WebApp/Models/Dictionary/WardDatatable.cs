﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Dictionary
{
    public class WardDatatable : DatatableBase
    {
        public WardDatatable(IFormCollection form) : base(form)
        {
            searchName = form["query[filterName]"].FirstOrDefault();
            districtId = form["query[filterDistrict]"].FirstOrDefault();
        }

        public string searchName { get; set; }
        public string districtId { get; set; }
    }
}
