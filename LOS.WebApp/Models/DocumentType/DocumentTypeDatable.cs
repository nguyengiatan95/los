﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class DocumentTypeDatable : DatatableBase
    {
        public DocumentTypeDatable(IFormCollection form) : base(form)
        {
            name = form["query[filterName]"].FirstOrDefault();
            productId = form["query[productId]"].FirstOrDefault();
            isEnable = form["query[isEnable]"].FirstOrDefault();
        }
        public string name { get; set; }
        public string productId { get; set; }
        public string isEnable { get; set; }
    }
}
