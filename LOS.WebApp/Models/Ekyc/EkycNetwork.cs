﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Ekyc
{
    public class EkycNetwork
    {
        public string response_code { get; set; }
        public string mess { get; set; }
        public bool? info_check_result { get; set; }
        public ExtractedInfo? extracted_info { get; set; }
        public CompareInfo? compare_results { get; set; }
    }

    public class ExtractedInfo
    {
        public string dob { get; set; }
        public string fullname { get; set; }
        public string id_number { get; set; }
        public string issue_date { get; set; }
    }
    public class CompareInfo
    {
        public bool? same_dob { get; set; }
        public bool? same_fullname { get; set; }
        public bool? same_id_number { get; set; }
    }
}
