﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Erp
{
    public class CheckEmployeeTima
    {
        [JsonProperty("warehouses")]
        public object[] Warehouses { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }

        [JsonProperty("tel")]
        public string[] Tel { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("picture")]
        public Uri Picture { get; set; }

        [JsonProperty("nationalId")]
        public string NationalId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("createdAt")]
        public long CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public long UpdatedAt { get; set; }

        [JsonProperty("__v")]
        public long V { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public partial class CheckCavet
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("serial")]
        public string Serial { get; set; }

        [JsonProperty("loanId")]
        public long LoanId { get; set; }

        [JsonProperty("loanCreditId")]
        public long LoanCreditId { get; set; }

        [JsonProperty("loanBriefId")]
        public long LoanBriefId { get; set; }

        [JsonProperty("createdAt")]
        public long CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public long UpdatedAt { get; set; }

        [JsonProperty("hub")]
        public Hub Hub { get; set; }

        [JsonProperty("request")]
        public Request Request { get; set; }

        [JsonProperty("userRequest")]
        public UserRequest UserRequest { get; set; }

        [JsonProperty("hubRequest")]
        public HubRequest HubRequest { get; set; }

        [JsonProperty("tracking")]
        public Tracking Tracking { get; set; }

        [JsonProperty("hubTracking")]
        public HubTracking HubTracking { get; set; }
    }

    public partial class Hub
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class HubRequest
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }
    }

    public partial class Request
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("createdAt")]
        public long CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public long UpdatedAt { get; set; }
    }

    public partial class UserRequest
    {
        [JsonProperty("role")]
        public string Role { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }

    public partial class Tracking
    {
        [JsonProperty("status")]
        public string Status { get; set; }
    }
    public partial class HubTracking
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }

}
