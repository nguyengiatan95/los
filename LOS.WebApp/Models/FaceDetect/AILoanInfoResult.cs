﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LoanInfoResult
    {
        public int responseCode { get; set; }
        public string mess { get; set; }
        public LoansResult loansResult { get; set; }
    }

    public class LoansResult
    {
        public string status { get; set; }
        public string statusDes { get; set; }
        public List<LoanInfo> disbursementLoans { get; set; }
        public List<NotDisbursementLoan> notDisbursementLoans { get; set; }
        public List<LoanVay1h> loansVay1h { get; set; }
    }

    public class LoanInfo
    {
        public int id { get; set; }
        public int userId { get; set; }
        public int shopId { get; set; }
        public int customerId { get; set; }
        public long totalMoney { get; set; }
        public long totalMoneyCurrent { get; set; }
        public int? collateral { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public int loanTime { get; set; }
        public Nullable<DateTime> lastDateOfPay { get; set; }
        public decimal rate { get; set; }
        public int rateType { get; set; }
        public int frequency { get; set; }
        public int isBefore { get; set; }
        public long paymentMoney { get; set; }
        public int status { get; set; }
        public Nullable<DateTime> nextDate { get; set; }
        public int approveBy { get; set; }
        public string note { get; set; }
        public long totalInterest { get; set; }
        public long debitMoney { get; set; }
        public int codeId { get; set; }
        public Nullable<DateTime> modifyDate { get; set; }
        public long interestToDay { get; set; }
        public Nullable<DateTime> finishDate { get; set; }
        public Nullable<DateTime> alarmDate { get; set; }
        public Nullable<DateTime> alarmNote { get; set; }
        public int? timaId { get; set; }
        public int? isHide { get; set; }
        public int productId { get; set; }
        public string productName { get; set; }
        public string labelGoodBad { get; set; }
    }

    public class NotDisbursementLoan
    {
        public int id { get; set; }
        public int customerCreditId { get; set; }
        public long totalMoney { get; set; }
        public int? loanTime { get; set; }
        public decimal? rateconsultant { get; set; }
        public decimal? rate { get; set; }
        public decimal? rateService { get; set; }
        public long? totalReturn { get; set; }
        public int? status { get; set; }
        public Nullable<DateTime> createDate { get; set; }
        public Nullable<DateTime> modifyDate { get; set; }
        public Nullable<DateTime> approveDate { get; set; }
        public Nullable<DateTime> fromDate { get; set; }
        public Nullable<DateTime> toDate { get; set; }
        public int? owerShopId { get; set; }
        public int? typeReceivingMoney { get; set; }
        public string nameBanking { get; set; }
        public string numberCardBanking { get; set; }
        public string accountNumberBanking { get; set; }
        public string note { get; set; }
        public int? loanID { get; set; }
        public int? code { get; set; }
        public int? agentLoanID { get; set; }
        public int? approveId { get; set; }
        public Nullable<DateTime> nextDate { get; set; }
        public long? totalMoneyFirst { get; set; }
        public int? reasonID { get; set; }
        public int? countCall { get; set; }
        public Nullable<DateTime> beginStartTime { get; set; }
        public int? rateType { get; set; }
        public string phone { get; set; }
        public string nameCardHolder { get; set; }
        public int? step { get; set; }
        public string productName { get; set; }
    }

    public class LoanVay1h
    {
        public string id { get; set; }
        public DateTime created { get; set; }
        public string customerid { get; set; }
        public int days { get; set; }
        public DateTime disbursement { get; set; }
        public DateTime endTime { get; set; }
        public string linkcontract { get; set; }
        public int loanidag { get; set; }
        public long moneyLend { get; set; }
        public long moneyPay { get; set; }
        public long mmoneyTotaloneyPay { get; set; }
        public int productid { get; set; }
        public decimal? rate { get; set; }
        public int? reloan { get; set; }
        public Nullable<DateTime> startTime { get; set; }
        public int? loanStatus { get; set; }
        public int? step { get; set; }
        public int? typepayment { get; set; }
        public Nullable<DateTime> fromDate { get; set; }
        public Nullable<DateTime> toDate { get; set; }
        public int? loanTime { get; set; }
        public Nullable<DateTime> lastDateOfPay { get; set; }
        public int? frequency { get; set; }
        public long? paymentMoney { get; set; }
        public Nullable<DateTime> nextDate { get; set; }
        public long? totalMoney { get; set; }
        public long? totalMoneyCurrent { get; set; }
        public long? totalInterest { get; set; }
        public long? debitMoney { get; set; }
        public Nullable<DateTime> finishDate { get; set; }
        public string labelGoodBad { get; set; }
        public string noteForLabel { get; set; }
        public string phone { get; set; }
        public string note { get; set; }
        public string productName { get; set; }
    }

    public class CustomerInfoResult
    {
        public int responseCode { get; set; }
        public string mess { get; set; }
        public CustomerResult customerResult { get; set; }
    }

    public class CustomerResult
    {
        public string status { get; set; }
        public string statusDes { get; set; }
        public List<CustomerInfo> customerCredit { get; set; }
    }

    public class CustomerInfo
    {
        public int customerId { get; set; }
        public string fullName { get; set; }
        public Nullable<DateTime> birthday { get; set; }
        public string phone { get; set; }
        public string cardNumber { get; set; }
        public int? cityId { get; set; }
        public int? districtId { get; set; }
        public int? wardId { get; set; }
        public int? gender { get; set; }
        public string email { get; set; }
        public string street { get; set; }
        public string numberLiving { get; set; }
        public int? livingTimeId { get; set; }
        public int? typeOfOwnershipId { get; set; }
        public int? jobId { get; set; }
        public int? workingIndustryId { get; set; }
        public string companyName { get; set; }
        public string companyPhone { get; set; }
        public decimal? salary { get; set; }
        public int? receiveYourIncome { get; set; }
        public int? relativeFamilyId { get; set; }
        public string fullNameFamily { get; set; }
        public string phoneFamily { get; set; }
        public int? manufacturerId { get; set; }
        public string modelPhone { get; set; }
        public string yearMade { get; set; }
        public decimal? valuationAsset { get; set; }
        public Nullable<DateTime> createDate { get; set; }
        public string facebook { get; set; }
        public string addressCompany { get; set; }
        public int? typeCustomerCreditId { get; set; }
        public string addressHouseHold { get; set; }
        public string numberRemarketing { get; set; }
        public bool? isMarried { get; set; }
    }
}
