﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ImageSearchInput
    {
        public string image { get; set; }
    }
    public class FaceSearchOutput
    {
        [JsonProperty("status")]
        public string Code { get; set; }
        [JsonProperty("result")]
        public List<string> ListPhone { get; set; }
    }

    public class DetectFaceItem
    {
        [JsonProperty("label")]
        public string Phone { get; set; }

        [JsonProperty("distance")]
        public string Distance { get; set; }

        [JsonProperty("unknown")]
        public bool Unknown { get; set; }
    }
}
