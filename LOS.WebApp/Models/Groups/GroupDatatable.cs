﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
   public class GroupDatatable : DatatableBase
    {
        public GroupDatatable(IFormCollection form) : base(form)
        {
            name = form["query[filterName]"].FirstOrDefault();
            //status = form["query[status]"].FirstOrDefault();
            groupId = form["query[groupId]"].FirstOrDefault();
        }

        public string name { get; set; }
        //public string status { get; set; }
        public string groupId { get; set; }


    }
}
