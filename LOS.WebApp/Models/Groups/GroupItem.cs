﻿using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Groups
{
    public class GroupItem
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public string DefaultPath { get; set; }
        public List<string> lstMenu { get; set; }
        public List<string> lstAction { get; set; }
        public List<GroupModule> ListGroupModule { get; set; }
    }
}
