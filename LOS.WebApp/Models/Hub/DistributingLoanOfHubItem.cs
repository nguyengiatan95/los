﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class DistributingLoanOfHubItem
    {
        public int LoanBriefId { get; set; }
        public List<UserDetail> ListUserOfHub { get; set; }

        public int? HubEmployeeId { get; set; }
        public int? Appraisal { get; set; }
    }
}
