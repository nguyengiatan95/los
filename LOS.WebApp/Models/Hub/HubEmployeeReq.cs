﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class HubEmployeeReq
    {
        public int hubId { get; set; }
        public List<int>? userIds { get; set; }
    }
}
