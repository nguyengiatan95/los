﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ListLoanDebtDatatable : DatatableBase
    {
        public ListLoanDebtDatatable(IFormCollection form) : base(form)
        {
            HubId = form["datatable[query][hubId][]"].ToArray();
            Date = form["query[date]"].FirstOrDefault();
        }

        public string[] HubId { get; set; }
        public string Date { get; set; }
    }
}
