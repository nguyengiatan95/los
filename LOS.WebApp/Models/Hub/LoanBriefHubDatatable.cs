﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LoanBriefHubDatatable : DatatableBase
    {
        public LoanBriefHubDatatable(IFormCollection form) : base(form)
        {
            LoanBriefId = form["query[LoanbriefId]"].FirstOrDefault();
            SearchName = form["query[SearchName]"].FirstOrDefault();
            LoanStatus = form["query[LoanStatus]"].FirstOrDefault();
            HubEmployeeId = form["query[HubEmployeeId]"].FirstOrDefault();
            HubLoanStatusChild = form["query[HubLoanStatusChild]"].FirstOrDefault();
            LoanStatusDetail = form["query[LoanStatusDetail]"].FirstOrDefault();
        }

        public string LoanBriefId { get; set; }
        public string SearchName { get; set; }
        public string LoanStatus { get; set; }
        public string HubEmployeeId { get; set; }
        public int HubId { get; set; }
        public string ListLoanStatus { get; set; }
        public string HubLoanStatusChild { get; set; }
        public string LoanStatusDetail { get; set; }

    }
}
