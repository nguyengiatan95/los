﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LoanbriefHubViewModel
    {
        public string LoanBriefId { get; set; }
        public string SearchName { get; set; }
        public int HubManagerStatusSearch { get; set; }
        public List<UserDetail> ListUserOfHub { get; set; }
        public List<ShopDetail> ListHub { get; set; }

    }
}
