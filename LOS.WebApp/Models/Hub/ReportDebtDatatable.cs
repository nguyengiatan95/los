﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ReportDebtDatatable : DatatableBase
    {
        public ReportDebtDatatable(IFormCollection form) : base(form)
        {
            hubId = form["query[hubId]"].FirstOrDefault();
            date = form["query[date]"].FirstOrDefault();
        }

        public string hubId { get; set; }
        public string date { get; set; }
    }
}
