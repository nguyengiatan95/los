﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class CICParam
    {
        public partial class OutPut
        {
            [JsonProperty("Result")]
            public long Result { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("Data")]
            public Data Data { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("creditInfo")]
            public string CreditInfo { get; set; }

            [JsonProperty("checkTime")]
            public string CheckTime { get; set; }

            [JsonProperty("brief")]
            public string Brief { get; set; }

            [JsonProperty("numberOfLoans")]
            public long NumberOfLoans { get; set; }

            [JsonProperty("hasBadDebt")]
            public bool HasBadDebt { get; set; }

            [JsonProperty("hasLatePayment")]
            public bool HasLatePayment { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("address")]
            public string Address { get; set; }
        }
    }
}
