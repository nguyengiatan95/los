﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.LMS
{
    public class CalculatorMoneyInsurance
    {
        public partial class InPut
        {
            [JsonProperty("MoneyDisbursement")]
            public long MoneyDisbursement { get; set; }

            [JsonProperty("LenderID")]
            public long LenderId { get; set; }

            [JsonProperty("LoanTime")]
            public int LoanTime { get; set; }

            [JsonProperty("RateType")]
            public int RateType { get; set; }
        }

        public partial class OutPut
        {
            [JsonProperty("Data")]
            public Data Data { get; set; }

            [JsonProperty("Messages")]
            public object[] Messages { get; set; }

            [JsonProperty("Message")]
            public object Message { get; set; }

            [JsonProperty("Status")]
            public long Status { get; set; }

            [JsonProperty("Total")]
            public long Total { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("FeeInsuranceOfCustomer")]
            public long FeeInsuranceOfCustomer { get; set; }

            [JsonProperty("FeeInsuranceOfLender")]
            public long FeeInsuranceOfLender { get; set; }
        }

        public partial class DataOutPut
        {
            [JsonProperty("FeeInsuranceOfCustomer")]
            public long FeeInsuranceOfCustomer { get; set; }

            [JsonProperty("FeeInsuranceOfLender")]
            public long FeeInsuranceOfLender { get; set; }
        }
    }
}
