﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.LMS
{
    public class CheckBlackList
    {
        public class Input
        {
            public string NumberCard { get; set; }
            public string FullName { get; set; }
            public string Phone { get; set; }
        }

        public class Output
        {
            public bool Data { get; set; }
            public List<string> Messages { get; set; }
            public string Message { get; set; }
            public int Status { get; set; }
            public int Total { get; set; }
        }
    }
}
