﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class CheckReLoan
    {
        public partial class Input
        {
            public string NumberCard { get; set; }
            public string CustomerName { get; set; }
        }

        public partial class OutPut
        {
            [JsonProperty("Result")]
            public long Result { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("LoanID")]
            public long LoanId { get; set; }

            [JsonProperty("LoanBriefID")]
            public long LoanBriefID { get; set; }

            [JsonProperty("LoanBriefID")]
            public long IsAccept { get; set; }
        }

        public partial class TopupOutPut
        {
            public long IsAccept { get; set; }
            public string Message { get; set; }
            public long LoanBriefId { get; set; }
            public long TotalMoneyCurrent { get; set; }
        }
        public partial class Data
        {
            [JsonProperty("LoanID")]
            public long LoanId { get; set; }

            [JsonProperty("LoanCreditID")]
            public long LoanCreditId { get; set; }

            [JsonProperty("IsAccept")]
            public long IsAccept { get; set; }
        }

    }
}
