﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.LMS
{
    public class DebtRestructuring
    {
        public partial class Input
        {
            [JsonProperty("LoanBriefId")]
            public long LoanBriefId { get; set; }

            [JsonProperty("LoanId")]
            public long LoanId { get; set; }

            [JsonProperty("UserId")]
            public long UserId { get; set; }

            [JsonProperty("FullName")]
            public string FullName { get; set; }

            [JsonProperty("UserName")]
            public string UserName { get; set; }

            [JsonProperty("TypeDebtRestructuring")]
            public long TypeDebtRestructuring { get; set; }

            [JsonProperty("NumberOfRepaymentPeriod")]
            public long NumberOfRepaymentPeriod { get; set; }

            [JsonProperty("AppointmentDate")]
            public string AppointmentDate { get; set; }

            [JsonProperty("PercentDiscount")]
            public int PercentDiscount { get; set; }
        }
        public partial class Output
        {
            [JsonProperty("Data")]
            public bool Data { get; set; }

            [JsonProperty("Messages")]
            public object[] Messages { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("Status")]
            public long Status { get; set; }

            [JsonProperty("Total")]
            public long Total { get; set; }
        }
    }
}
