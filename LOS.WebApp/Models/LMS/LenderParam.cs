﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LenderParam
    {
        public partial class Input
        {
            [JsonProperty("LenderCode")]
            public string LenderCode { get; set; }

            [JsonProperty("LenderType")]
            public long LenderType { get; set; }

            [JsonProperty("HasMoney")]
            public long HasMoney { get; set; }

            [JsonProperty("PageIndex")]
            public long PageIndex { get; set; }

            [JsonProperty("PageSize")]
            public long PageSize { get; set; }
        }

        public partial class Output
        {
            [JsonProperty("Result")]
            public long Result { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("Data")]
            public Data Data { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("LstLender")]
            public LstLender[] LstLender { get; set; }

            [JsonProperty("TotalRow")]
            public long TotalRow { get; set; }
        }

        public partial class LstLender
        {
            [JsonProperty("LenderID")]
            public long LenderId { get; set; }

            [JsonProperty("LenderCode")]
            public string LenderCode { get; set; }

            [JsonProperty("TotalMoney")]
            public long TotalMoney { get; set; }

            [JsonProperty("CreateDate")]
            public DateTimeOffset CreateDate { get; set; }

            [JsonProperty("Address")]
            public string Address { get; set; }
        }
    }
}
