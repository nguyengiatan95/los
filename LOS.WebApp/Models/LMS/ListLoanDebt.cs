﻿using LOS.DAL.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ListLoanDebt
    {
        public class RequestListLoanDebt
        {
            public string Date { get; set; }
            public int[] LstHubId { get; set; }
        }

        public partial class ListLoanDebtOutput
        {
            [JsonProperty("Data")]
            public Data[] Data { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("Code")]
            public long Code { get; set; }

            [JsonProperty("TotalCount")]
            public long TotalCount { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("LoanID")]
            public int LoanId { get; set; }

            [JsonProperty("Code")]
            public long Code { get; set; }

            [JsonProperty("FromDate")]
            public DateTime FromDate { get; set; }

            [JsonProperty("NextDate")]
            public DateTime NextDate { get; set; }

            [JsonProperty("ToDate")]
            public DateTime ToDate { get; set; }

            [JsonProperty("TotalMoney")]
            public long TotalMoney { get; set; }

            [JsonProperty("TotalMoneyCurrent")]
            public long TotalMoneyCurrent { get; set; }

            [JsonProperty("NumberDayOverdue")]
            public long NumberDayOverdue { get; set; }

            [JsonProperty("MaTC")]
            public string MaTc { get; set; }

            [JsonProperty("TotalPayInterestFees")]
            public long TotalPayInterestFees { get; set; }

            [JsonProperty("HubId")]
            public long HubId { get; set; }

            [JsonProperty("MoneyFineLate")]
            public long MoneyFineLate { get; set; }

            [JsonProperty("MoneyInPeriods")]
            public long MoneyInPeriods { get; set; }

            [JsonProperty("MoneyInPeriodsNext")]
            public long MoneyInPeriodsNext { get; set; }
        }

        public class lstData
        {
            public Data LMSData { get; set; }
            public LoanBriefDetailDebt LOSData { get; set; }
        }
    }


}
