﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class PayAmountForCustomer
    {
        public DataPayAmountForCustomer Data { get; set; }
        public List<string> Messages { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }
        public int Total { get; set; }

    }
    public class DataPayAmountForCustomer
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public long TotalMoneyNeedPay { get; set; }
        public long TotalMoneyCloseLoan { get; set; }
        public long TotalMoneyFineOriginal { get; set; }
        public string VaAccountName { get; set; }
        public string VaAcountNo { get; set; }
        public string VaBankName { get; set; }
        public DateTime DatePayment { get; set; }
        public List<LoanInfos> LstLoanInfos { get; set; }
    }
    public class LoanInfos
    {
        public string LoanCodeID { get; set; }
        public int LoanID { get; set; }
        public long TotalMoneyNeedPay { get; set; }
        public long MoneyCloseLoan { get; set; }
        public long MoneyFineOriginal { get; set; }
        public int CloseType { get; set; }
        public List<PaymentSchedule> LstPaymentSchedules { get; set; }
    }
    public class PaymentSchedule
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime PayDate { get; set; }
        public long MoneyOriginal { get; set; }
        public long MoneyInterest { get; set; }
        public long MoneyService { get; set; }
        public long MoneyConsultant { get; set; }
        public long OtherMoney { get; set; }
        public bool Done { get; set; }
        public DateTime DaysPayable { get; set; }
        public long PayMoney { get; set; }
        public int IsComplete { get; set; }
    }
}
