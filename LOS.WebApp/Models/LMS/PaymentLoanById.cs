﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class PaymentLoanById
    {
        public partial class OutPut
        {
            [JsonProperty("Data")]
            public Data Data { get; set; }

            [JsonProperty("Messages")]
            public string[] Messages { get; set; }

            [JsonProperty("Status")]
            public long Status { get; set; }

            [JsonProperty("Total")]
            public long Total { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("Loan")]
            public Loan Loan { get; set; }

        }

        public partial class Loan
        {
            [JsonProperty("ID")]
            public long Id { get; set; }
           

            [JsonProperty("TotalMoney")]
            public long TotalMoney { get; set; }

            [JsonProperty("TotalMoneyCurrent")]
            public long TotalMoneyCurrent { get; set; }

            public int TopUp { get; set; }
           
        }

        public partial class PaymentSchedule
        {
            [JsonProperty("FromDate")]
            public DateTimeOffset FromDate { get; set; }

            [JsonProperty("ToDate")]
            public DateTimeOffset ToDate { get; set; }

            [JsonProperty("PayDate")]
            public DateTimeOffset PayDate { get; set; }

            [JsonProperty("MoneyOriginal")]
            public long MoneyOriginal { get; set; }

            [JsonProperty("MoneyInterest")]
            public long MoneyInterest { get; set; }

            [JsonProperty("MoneyService")]
            public long MoneyService { get; set; }

            [JsonProperty("MoneyConsultant")]
            public long MoneyConsultant { get; set; }

            [JsonProperty("OtherMoney")]
            public long OtherMoney { get; set; }

            [JsonProperty("Done")]
            public bool Done { get; set; }

        }
    }
}
