﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class PrematureInterest
    {
        public PrematureInterestData Data { get; set; }
        public List<string> Messages { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }
        public int Total { get; set; }
    }
    public class PrematureInterestData
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public long CustomerMoney { get; set; }
        public long TotalMoneyNeedPay { get; set; }
        public long TotalMoneyCloseLoan { get; set; }
        public long TotalMoneyFineOriginal { get; set; }
        public long TotalMoneyInterest { get; set; }
        public long TotalOtherMoney { get; set; }
        public string VaAccountName { get; set; }
        public string VaAcountNo { get; set; }
        public string VaBankName { get; set; }
        public DateTime DatePayment { get; set; }
        public string CiscoExtension { get; set; }
        public List<LstLoanInfos> LstLoanInfos { get; set; }
    }
    public class LstLoanInfos
    {
        public string LoanCodeID { get; set; }
        public int LoanID { get; set; }
        public long TotalMoneyNeedPay { get; set; }
        public long MoneyCloseLoan { get; set; }
        public long MoneyFineOriginal { get; set; }
        public long MoneyInterest { get; set; }
        public long OtherMoney { get; set; }
        public long TotalMoneyCurrent { get; set; }
        public int OverDate { get; set; }
        public int CloseType { get; set; }

        public List<PaymentSchedule> LstPaymentSchedules { get; set; }
    }
}
