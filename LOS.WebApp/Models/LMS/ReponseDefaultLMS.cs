﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ReponseDefaultLMS
    {
        public int Result { get; set; }
        public int Total { get; set; }
        public string Message { get; set; }
        public long? Data { get; set; }
    }
}
