﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class RequestAddBlacklist
    {
        public int CustomerCreditId { get; set; }
        public string FullName { get; set; }
        public string NumberPhone { get; set; }
        public string CardNumber { get; set; }
        public DateTime? BirthDay { get; set; }
        public int UserIdCreate { get; set; }
        public string UserNameCreate { get; set; }
        public string FullNameCreate { get; set; }
        public string Note { get; set; }
        public int NumberDay { get; set; }
        public int LoanCreditId { get; set; }
    }
}
