﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class RequestPayAmountForCustomer
    {
        public string NumberCard { get; set; }
        public int LoanBriefId { get; set; }
    }
}
