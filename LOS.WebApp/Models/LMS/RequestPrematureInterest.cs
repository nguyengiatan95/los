﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class RequestPrematureInterest
    {
        public int LoanID { get; set; }
        public string CloseDate { get; set; }

    }
}
