﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ResponeAddBlacklist
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public List<string> Messages { get; set; }
        public int Data { get; set; }
        public int Total { get; set; }
    }
}
