﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ResponeReportDebt
    {
        public List<ReportDebt> Data { get; set; }
        public List<string> Messages { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }
        public int Total { get; set; }
    }
    public class ReportDebt
    {
        public string HubName { get; set; }
        public long TotalMoneyCurrentBegin { get; set; } //Dư nợ đầu kỳ tại ngày chọn
        public long TotalMoneyCurrentBegin_10PDP { get; set; } //dự nợ đầu kì <10 ngày
        public long TotalMoneyDisbursementNew { get; set; }//dự nợ giải ngân trong tháng
        public long TotalMoneyCurrent { get; set; } //dự nợ hiện tại
        public long TotalMoneyCurrent_10DPD { get; set; }//dự nợ hiện tại  <10
        public long TotalMoneyCurrent_LastMonth { get; set; }
        public long TotalMoneyDisbursement_LastMonth { get; set; }
        public long TotalMoneyCurrent_LastMonth1 { get; set; }
        public long TotalMoneyDisbursement_LastMonth1 { get; set; }
        public long TotalMoneyCurrent_LastMonth2 { get; set; }
        public long TotalMoneyDisbursement_LastMonth2 { get; set; }
        public long TotalMoneyCurrent_3MonthAgo { get; set; }
        public long TotalMoneyDisbursement_3MonthAgo { get; set; }
        public double CompletionRateT_3 { get; set; }
        public double CompletionRateT_1 { get; set; }
        public double CompletionRateT_2 { get; set; }
        public double CompletionRate3Month { get; set; }
        public long NetAugment { get; set; }
    }
}
