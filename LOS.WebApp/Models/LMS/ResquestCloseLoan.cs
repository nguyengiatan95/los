﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ResquestCloseLoan
    {
        public long LoanAgID { get; set; }
        public long RequestByUserID { get; set; }
        public string RequestByName { get; set; }
        public string Reason { get; set; }
        public string CloseDate { get; set; }
    }
}
