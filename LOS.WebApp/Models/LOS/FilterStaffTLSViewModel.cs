﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class FilterStaffTLSViewModel
    {
        public int TotalMinutesCall { get; set; }
        public int Lead { get; set; }
        public int Form { get; set; }
        public decimal PercentFormToLead { get; set; }
    }
}
