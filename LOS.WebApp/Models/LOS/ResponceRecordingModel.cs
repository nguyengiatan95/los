﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ResponceRecordingModel
    {
        public ResponceMeta meta { get; set; }
        public List<RecordingItem> data { get; set; }
    }
    public class ResponceMeta
    {
        public int errorCode { get; set; }
        public string errorMessage { get; set; }
    }
    public class RecordingItem
    {
        public DateTime? startTime { get; set; }
        public int? callDuration { get; set; }
        public string line { get; set; }
        public string agentUserName { get; set; }
        public string recordingUrl { get; set; }
        public string playUrl { get; set; }
        public string phoneNumber { get; set; }
    }
}
