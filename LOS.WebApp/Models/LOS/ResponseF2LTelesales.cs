﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ResponseF2LTelesales
    {
        public ResponceMeta meta { get; set; }
        public F2LTelesales data { get; set; }
    }
    public class F2LTelesales
    {
        public int telesaleId { get; set; }
        public int? form { get; set; }
        public int? lead { get; set; }
    }
}
