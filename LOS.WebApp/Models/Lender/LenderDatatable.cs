﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LenderDatatable : DatatableBase
    {
        public LenderDatatable(IFormCollection form) : base(form)
        {
            LenderCode = form["query[LenderCode]"].FirstOrDefault();
            LenderType = form["query[LenderType]"].FirstOrDefault();
            HasMoney = form["query[HasMoney]"].FirstOrDefault();
            PageIndex = form["query[PageIndex]"].FirstOrDefault();
            PageSize = form["query[PageSize]"].FirstOrDefault();

        }
        public string LenderCode { get; set; }
        public string LenderType { get; set; }
        public string HasMoney { get; set; }
        public string PageIndex { get; set; }
        public string PageSize { get; set; }

    }
}
