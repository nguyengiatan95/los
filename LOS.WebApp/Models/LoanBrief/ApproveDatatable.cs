﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Users
{
    public class ApproveDatatable : DatatableBase
    {
        public ApproveDatatable(IFormCollection form) :base(form)
        {			          
            loanBriefId = form["query[loanBriefId]"].FirstOrDefault();
            search = form["query[search]"].FirstOrDefault();
            hubId = form["query[hubId]"].FirstOrDefault();
            status = form["query[status]"].FirstOrDefault();
            provinceId = form["query[provinceId]"].FirstOrDefault();
            productId = form["query[productId]"].FirstOrDefault();
            DateRanger = form["query[filtercreateTime]"].FirstOrDefault();
            ShopName = form["query[shopName]"].FirstOrDefault();
        }
        //public int userId { get; set; }
        public int coordinatorUserId { get; set; }
        public string status { get; set; }
		public string search { get; set; }
        public string loanBriefId { get; set; }
        public string hubId { get; set; }

        public string provinceId { get; set; }
        public string productId { get; set; }

        public string DateRanger { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string ShopName { get; set; }
    }
}
