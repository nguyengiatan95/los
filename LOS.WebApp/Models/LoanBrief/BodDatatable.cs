﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Users
{
    public class BodDatatable : DatatableBase
    {
        public BodDatatable(IFormCollection form) : base(form)
        {
            loanBriefId = form["query[loanBriefId]"].FirstOrDefault();
            search = form["query[search]"].FirstOrDefault();
            typeLoan = form["query[typeLoan]"].FirstOrDefault();
            status = form["query[status]"].FirstOrDefault();
        }
        //public int userId { get; set; }
        public string status { get; set; }
        public string search { get; set; }
        public string loanBriefId { get; set; }
        public string typeLoan { get; set; }
    }
}
