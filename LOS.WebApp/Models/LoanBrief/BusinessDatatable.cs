﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Users
{
    public class BusinessDatatable : DatatableBase
    {
        public BusinessDatatable(IFormCollection form) : base(form)
        {
            loanBriefId = form["query[loanBriefId]"].FirstOrDefault();
            search = form["query[search]"].FirstOrDefault();
            typeLoan = form["query[typeLoan]"].FirstOrDefault();
        }

        public string loanBriefId { get; set; }
        public string search { get; set; }
        public string typeLoan { get; set; }
        public string shops { get; set; }
    }
}
