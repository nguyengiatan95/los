﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class CheckLeadQualify
    {
        public bool NoQualify { get; set; }
        public bool UploadImage { get; set; }
        public bool NeedLoan { get; set; }
        public bool InAge { get; set; }
        public bool InAreaSupport { get; set; }
        public bool HaveMotobike { get; set; }
        public bool HaveOriginalVehicleRegistration { get; set; }
    }
}
