﻿using LOS.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class CheckPhoneViewModel
    {
        public int LoanbriefId { get; set; }
        public List<LoanAndRelationNotCancelView> ListData { get; set; }
    }
}
