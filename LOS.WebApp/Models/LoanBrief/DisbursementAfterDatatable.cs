﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Users
{
    public class DisbursementAfterDatatable : DatatableBase
    {
        public DisbursementAfterDatatable(IFormCollection form) :base(form)
        {			          
            loanBriefId = form["query[loanBriefId]"].FirstOrDefault();
            search = form["query[search]"].FirstOrDefault();
            provinceId = form["query[provinceId]"].FirstOrDefault();
            status = form["query[status]"].FirstOrDefault();
            DateRanger = form["query[filtercreateTime]"].FirstOrDefault();
            productId = form["query[productId]"].FirstOrDefault();
            hubId = form["query[hubId]"].FirstOrDefault();;

            locate = form["query[locate]"].FirstOrDefault();
            coordinatorUserId = form["query[coordinatorUserId]"].FirstOrDefault();
            borrowCavet = form["query[borrowCavet]"].FirstOrDefault();
            typeSearch = form["query[typeSearch]"].FirstOrDefault();
            topup = form["query[topup]"].FirstOrDefault();
            empHubId = form["query[empHubId]"].FirstOrDefault();
            telesaleId = form["query[telesaleId]"].FirstOrDefault();
        }

        public int userId { get; set; }
        public string coordinatorUserId { get; set; }
        public string loanBriefId { get; set; }
        public string search { get; set; }
        public string provinceId { get; set; }
        public string status { get; set; }       
        public string DateRanger { get; set; }
        public string productId { get; set; }
        public string hubId { get; set; }
             
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string locate { get; set; }
        public string borrowCavet { get; set; }
        public string typeSearch { get; set; }
        public string topup { get; set; }
        public string empHubId { get; set; }
        public string telesaleId { get; set; }

    }
}
