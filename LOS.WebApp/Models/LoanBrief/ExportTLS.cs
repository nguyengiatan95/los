﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ExportTLS
    {
        public string loanBriefId { get; set; }
        public string search { get; set; }

        public string provinceId { get; set; }
        public string status { get; set; }
        public string productId { get; set; }
                    
        public string DateRanger { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string team { get; set; }
        public string statusTelesales { get; set; }
        public string filterStaffTelesales { get; set; }
        public string page { get; set; }
        public string pageSize { get; set; }
        public string teamTelesales { get; set; }
        public string FromDateLastChangeStatus { get; set; }
        public string ToDateLastChangeStatus { get; set; }
    }
}
