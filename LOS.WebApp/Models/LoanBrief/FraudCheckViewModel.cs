﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class FraudCheckViewModel
    {
        public string Channel { get; set; }
        public List<string> Message { get; set; } = new List<string>();
        public string DetailUrl { get; set; }
        public string HvChannel { get; set; }
        public List<string> HvMessage { get; set; } = new List<string>();
    }
}
