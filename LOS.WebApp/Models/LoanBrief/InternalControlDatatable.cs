﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Users
{
    public class InternalControlDatatable : DatatableBase
    {
        public InternalControlDatatable(IFormCollection form) :base(form)
        {			          
            loanBriefId = form["query[loanbriefId]"].FirstOrDefault();
            search = form["query[search]"].FirstOrDefault();
            status = form["query[status]"].FirstOrDefault();
            hubId = form["query[hubId]"].FirstOrDefault();;
            locate = form["query[locate]"].FirstOrDefault();
            DateRanger = form["query[filtercreateTime]"].FirstOrDefault();
        }

        public string loanBriefId { get; set; }
        public string search { get; set; }
        public string status { get; set; }             
        public string hubId { get; set; }                  
        public string locate { get; set; }
        public string DateRanger { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

    }
}
