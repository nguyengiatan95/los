﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LoanBriefCancelItem
    {
        public LoanBriefCancelItem()
        {
            HasGroupReason = true;
        }
        public int LoanbriefId { get; set; }
        public bool HasGroupReason { get; set; }
        public List<ReasonCancelDetail> ListReasonGroup { get; set; }

        public List<ReasonCancelDetail> ListReasonDetail { get; set; }
    }
}
