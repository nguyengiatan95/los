﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Users
{
    public class LoanBriefDatatable : DatatableBase
    {
        public LoanBriefDatatable(IFormCollection form) :base(form)
        {
            provinceId = form["query[provinceId]"].FirstOrDefault();			
            productId = form["query[productId]"].FirstOrDefault();
			status = form["query[status]"].FirstOrDefault();
            loanBriefId = form["query[loanBriefId]"].FirstOrDefault();
            search = form["query[search]"].FirstOrDefault();
            filterSource = form["query[filterSource]"].FirstOrDefault();
            DateRanger = form["query[filtercreateTime]"].FirstOrDefault();
            actionId = form["query[actionId]"].FirstOrDefault();
            boundTelesaleId = -1;
            countCall = form["query[countCall]"].FirstOrDefault();
            filterClickTop = form["query[filterClickTop]"].FirstOrDefault();
            detailStatusTelesales= form["query[detailStatusTelesales]"].FirstOrDefault();
            statusTelesales = form["query[statusTelesales]"].FirstOrDefault();
            filterStaffTelesales = form["query[filterStaffTelesales]"].FirstOrDefault();
            team = form["query[team]"].FirstOrDefault();
            dateRangerLastChangeStatus = form["query[dateRangerLastChangeStatus]"].FirstOrDefault();
            utmSource = form["query[utmSource]"].FirstOrDefault();
            filterRecare = form["query[filterRecare]"].FirstOrDefault();
        }
        //public int userId { get; set; }
        public string provinceId { get; set; }
		public string productId { get; set; }
		public string status { get; set; }
		public string search { get; set; }
        public string loanBriefId { get; set; }
        public string filterSource { get; set; }
        public string DateRanger { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string actionId { get; set; }
        public int boundTelesaleId { get; set; }
        public string countCall { get; set; }
        public string filterClickTop { get; set; }
        public string detailStatusTelesales { get; set; }
        public string statusTelesales { get; set; }
        public string filterStaffTelesales { get; set; }
        public string teamTelesales { get; set; }
        public string team { get; set; }
        public string dateRangerLastChangeStatus { get; set; }
        public string FromDateLastChangeStatus { get; set; }
        public string ToDateLastChangeStatus { get; set; }
        public int groupId { get; set; }
        public string utmSource { get; set; }
        public string filterRecare { get; set; }
        public int userId { get; set; }
    }
}
