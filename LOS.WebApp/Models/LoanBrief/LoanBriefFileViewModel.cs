﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LoanBriefFileViewModel
    {
        public int LoanBriefId { get; set; }
        public int DocumentType { get; set; }
        public bool IsDelete { get; set; }
        public List<DocumentTypeDetail> ListDocument { get; set; }
        public List<DocumentTypeNew> ListDocumentNew { get; set; }
        public List<LoanBriefFileDetail> ListLoanBriefFile { get; set; }
        public List<DocumentMapInfo> ListDocumentV2 { get; set; }
    }
}
