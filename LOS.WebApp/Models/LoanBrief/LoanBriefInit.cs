﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LoanBriefInit : LoanBrief
    {
        public LoanBriefInit()
        {
            Customer = new Customer();
            LoanBriefCompany = new LoanBriefCompany();
            LoanBriefHousehold = new LoanBriefHousehold();
            LoanBriefJob = new LoanBriefJob();
            LoanBriefProperty = new LoanBriefProperty();
            LoanBriefResident = new LoanBriefResident();
            TypeLoanBrief = LOS.Common.Extensions.LoanBriefType.Customer.GetHashCode();
        }
        public string sBirthDay { get; set; }
        public string sCardNumberShareholderDate { get; set; }
        public string sBusinessCertificationDate { get; set; }
        public string sBirthdayShareholder { get; set; }
        public int ProductType { get; set; }
        public string LoanBriefComment { get; set; }
        public long TotalMoneyCurrent { get; set; }
        public decimal? FeeInsuranceMaterialCovered { get; set; }
        public List<ProvinceDetail> ListProvince { get; set; }
        public List<JobDetail> ListJobs { get; set; }
        public List<RelativeFamilyDetail> ListRelativeFamilys { get; set; }
        public List<BrandProductDetail> ListBrandProduct { get; set; }
        public List<LoanProduct> ListLoanProduct { get; set; }
        public List<BankDetail> ListBank { get; set; }
        public List<PlatformTypeDetail> ListPlatformType { get; set; }

        public List<DocumentTypeDetail> ListDocumentType { get; set; }
        public List<LoanBriefFileDetail> ListLoanBriefFile { get; set; }

    }
}
