﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LoanBriefScriptItem : LoanBrief
    {
        public LoanBriefScriptItem()
        {
            Customer = new Customer();
            LoanBriefCompany = new LoanBriefCompany();
            LoanBriefHousehold = new LoanBriefHousehold();
            LoanBriefJob = new LoanBriefJob();
            LoanBriefProperty = new LoanBriefProperty();
            LoanBriefResident = new LoanBriefResident();
            CheckLeadQualify = new CheckLeadQualify();
        }
        public string sBirthDay { get; set; }
        public int OwnerProduct { get; set; }
        public int Contract { get; set; }
        public int ActionTelesales { get; set; }
        public int HomeNetWork { get; set; }

        public decimal MaxPriceProduct { get; set; }
        public List<int> ListLoanBriefDocument { get; set; }
        public List<ProvinceDetail> ListProvince { get; set; }
        public List<JobDetail> ListJobs { get; set; }
        public List<RelativeFamilyDetail> ListRelativeFamilys { get; set; }
        public List<BrandProductDetail> ListBrandProduct { get; set; }
        public List<LoanProduct> ListLoanProduct { get; set; }
        public List<PlatformTypeDetail> ListPlatformType { get; set; }
        public List<SelectListItem> ListLoanPurpose { get; set; }
        public List<LogLoanInfoAi> ListLogRequestAi { get; set; }
        public CheckLeadQualify CheckLeadQualify { get; set; }
    }
}
