﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LoanBriefSearchDatatable : DatatableBase
    {
        public LoanBriefSearchDatatable(IFormCollection form) : base(form)
        {
           
            loanBriefId = form["query[loanBriefId]"].FirstOrDefault();
            search = form["query[search]"].FirstOrDefault();
            phone = form["query[phone]"].FirstOrDefault();
            nationalCard = form["query[nationalCard]"].FirstOrDefault();

        }
        public string search { get; set; }
        public string loanBriefId { get; set; }
        public string phone { get; set; }
        public string nationalCard { get; set; }

    }

    public class LoanBriefSearchModelDatatable : DatatableBase
    {
        public LoanBriefSearchModelDatatable(IFormCollection form) : base(form)
        {

            CodeId = form["query[codeId]"].FirstOrDefault();
            LoanBriefId = form["query[loanBriefId]"].FirstOrDefault();
            Search = form["query[search]"].FirstOrDefault();
        }
        public string Search { get; set; }
        public string LoanBriefId { get; set; }
        public string CodeId { get; set; }
    }
}
