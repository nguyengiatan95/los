﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LoanBriefStatus
    {
        public int LoanBriefId { get; set; }
        public int Status { get; set; }
    }
}
