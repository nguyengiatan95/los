﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Users
{
    public class LoanCancelDatatable : DatatableBase
    {
        public LoanCancelDatatable(IFormCollection form) : base(form)
        {
            provinceId = form["query[provinceId]"].FirstOrDefault();
            productId = form["query[productId]"].FirstOrDefault();
            districtId = form["query[districtId]"].FirstOrDefault();

            actionId = form["query[actionId]"].FirstOrDefault();

            DateRanger = form["query[filtercreateTime]"].FirstOrDefault();

        }
        //public int userId { get; set; }
        public string provinceId { get; set; }
        public string productId { get; set; }
        public string districtId { get; set; }
        public string actionId { get; set; }
        public string DateRanger { get; set; }
        public int HubId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }


    }
}
