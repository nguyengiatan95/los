﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Users
{
    public class LoanDepartmentDatatable : DatatableBase
    {
        public LoanDepartmentDatatable(IFormCollection form) :base(form)
        {			          
            loanBriefId = form["query[loanBriefId]"].FirstOrDefault();            
            status = form["query[status]"].FirstOrDefault();
            DateRanger = form["query[filtercreateTime]"].FirstOrDefault();
            departmentId = form["query[departmentId]"].FirstOrDefault();
            hubId = form["query[hubId]"].FirstOrDefault();;           
        }

        public string loanBriefId { get; set; }
        public string departmentId { get; set; }
        public string status { get; set; }       
        public string DateRanger { get; set; }
        public string hubId { get; set; }            
        public string FromDate { get; set; }
        public string ToDate { get; set; }       

    }
}
