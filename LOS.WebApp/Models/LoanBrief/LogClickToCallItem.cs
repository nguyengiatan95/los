﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LogClickToCallItem
    {
        public int loanbriefId { get; set; }
        public string phone { get; set; }
    }

    public class LogLoginFinesseModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Extension { get; set; }
        public int StatusResponse { get; set; }
    }
}
