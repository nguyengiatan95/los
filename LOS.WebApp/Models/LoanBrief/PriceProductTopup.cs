﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class PriceProductTopup
    {
        public long PriceAi { get; set; }
        public long MaxPriceProduct { get; set; }
    }
}
