﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class RemoveFileItem
    {
        public int loanbriefId { get; set; }
        public int fileId { get; set; }
        public int userRemove { get; set; }
    }
}
