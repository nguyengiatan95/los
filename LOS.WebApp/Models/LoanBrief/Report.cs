﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class Report
    {
        public class ReportDisberment
        {
            public int userId { get; set; }
            public int export { get; set; }
            public string coordinatorUserId { get; set; }
            public string loanBriefId { get; set; }
            public string search { get; set; }
            public string provinceId { get; set; }
            public string status { get; set; }
            public string DateRanger { get; set; }
            public string productId { get; set; }
            public string hubId { get; set; }

            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string Locate { get; set; }
            public string empHubId { get; set; }
        }

        public class ReportInternalControll
        {
            public string loanBriefId { get; set; }
            public string search { get; set; }
            public string status { get; set; }
            public string DateRanger { get; set; }
            public string hubId { get; set; }
            public string Locate { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public int export { get; set; }
            
        }
    }
    
}
