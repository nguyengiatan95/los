﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class UpdateRefPhoneLoanBriefRelationship
    {
        public int LoanBriefId { get; set; }
        public int LoanBriefRelationshipId { get; set; }
    }
}
