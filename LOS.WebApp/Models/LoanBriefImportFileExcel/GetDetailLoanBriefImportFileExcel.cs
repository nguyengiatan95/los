﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class GetDetailLoanBriefImportFileExcel
    {
        public string ImportFileId { get; set; }
        public int Status { get; set; }
    }
}
