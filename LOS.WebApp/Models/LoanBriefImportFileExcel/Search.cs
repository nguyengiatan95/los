﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class SearchImportFileExcel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
