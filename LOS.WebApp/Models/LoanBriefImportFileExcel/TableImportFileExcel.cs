﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class TableImportFileExcel
    {
        public int RowId { get; set; }
        public string FileName { get; set; }
        public string FileImportId { get; set; }
        public int TotalLoanImport { get; set; }
        public int TotalLoanImportSuccess { get; set; }
        public int TotalLoanImportReject { get; set; }
    }
}
