﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LoanBriefNoteViewModel
    {
        public int LoanBriefNoteId { get; set; }
        public string Note { get; set; }
        public string ShopName { get; set; }
        public DateTime CreatedTime { get; set; }
        public string FullName { get; set; }
        public int GroupId { get; set; }
    }
}
