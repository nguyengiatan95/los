﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class DisbursementContractDatatable : DatatableBase
    {
        public DisbursementContractDatatable(IFormCollection form) : base(form)
        {
            loanBriefId = form["query[loanBriefId]"].FirstOrDefault();
            search = form["query[search]"].FirstOrDefault();
            provinceId = form["query[provinceId]"].FirstOrDefault();
            DateRanger = form["query[filtercreateTime]"].FirstOrDefault();
            productId = form["query[productId]"].FirstOrDefault();
            topup = form["query[topup]"].FirstOrDefault();
        }

        public string loanBriefId { get; set; }
        public string search { get; set; }
        public string provinceId { get; set; }
        public string DateRanger { get; set; }
        public string productId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string topup { get; set; }
    }
}
