﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class PushToCiscoDatatable : DatatableBase
    {
        public PushToCiscoDatatable(IFormCollection form) : base(form)
        {
            search = form["query[search]"].FirstOrDefault();
            status = form["query[status]"].FirstOrDefault();
        }

        public string search { get; set; }
        public string status { get; set; }
    }
}
