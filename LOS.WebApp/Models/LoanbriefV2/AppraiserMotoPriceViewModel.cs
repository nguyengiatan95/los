﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class AppraiserMotoPriceViewModel
    {
        public LoanBriefDetailNew LoanBriefDetailNew { get; set; }
        public List<ProductReview> ProductReview { get; set; }
        public UserDetail UserDetail { get; set; }
        public int LoanBriefId { get; set; }
        public int LoanBriefProductId { get; set; }
        public int Status { get; set; }
        public List<TypeOwnerShip> ListOwnerShip { get; set; }
        public List<BrandProductDetail> ListBrandProduct { get; set; }
        public List<LoanProduct> ListLoanProduct { get; set; }
        public LoanBriefProperty LoanbriefProperty { get; set; }
        public LoanBriefResident LoanBriefResident { get; set; }
        public OldInfomationAppraiserMoto OldInfomationAppraiserMoto { get; set; }
        //phân loại khách hàng
        public int JobDescription { get; set; }
        public int TypeJob { get; set; }
        public decimal CurrentPrice { get; set; }
        public decimal MaxPriceProduct { get; set; }
        public long? TotalMoneyDebtCurrent { get; set; }
        public int ProductDetailId { get; set; }
        public int TypeOwnerShipId { get; set; }
        public int ProductId { get; set; }
        public int ProductCreditId { get; set; }
        public int TypeRemarketingLtv { get; set; }
        public List<ProductDetailModel> ListProductDetail { get; set; }
        public int Ltv { get; set; }
    }

    public class OldInfomationAppraiserMoto
    {
        public string ProductName { get; set; }
        public string BrandName { get; set; }
        public string OwnerProductName { get; set; }
        public string TypeOwnerShipName { get; set; }
        public string ProductDetailName { get; set; }
        public int ProductId { get; set; }
    }
}
