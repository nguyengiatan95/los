﻿namespace LOS.WebApp.Models.LoanbriefV2
{
    public class LTVViewModel
    {
        public int Percent { get; set; }
        public bool IsSelected { get; set; }
    }
}
