﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LoanbriefViewModel
    {
        public int LoanBriefId { get; set; }
        public int CustomerId { get; set; }
        public int PlatformType { get; set; }
        public int TypeLoanBrief { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string NationalCard { get; set; }
        public string NationCardPlace { get; set; }
        public bool? IsTrackingLocation { get; set; }
        public bool? IsLocate { get; set; }
        public bool? IsLocateCar { get; set; }
        public string sBirthDay { get; set; }
        public string Passport { get; set; }
        public int? Gender { get; set; }
        public int? NumberCall { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? WardId { get; set; }
        public string WardName { get; set; }
        public int ProductId { get; set; }
        public int RateTypeId { get; set; }
        public decimal LoanAmount { get; set; }
        public bool? BuyInsurenceCustomer { get; set; }
        public int? LoanTime { get; set; }
        public int? Frequency { get; set; }
        public decimal? RateMoney { get; set; }
        public double? RatePercent { get; set; }
        public int? ReceivingMoneyType { get; set; }
        public int? BankId { get; set; }
        public int? TypeRemarketing { get; set; }
        public int? ProductType { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }
        public string BankAccountName { get; set; }
        public string Comment { get; set; }
        public string PresenterCode { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool? IsHeadOffice { get; set; }
        public int? HubId { get; set; }
        public int? CreateBy { get; set; }
        public int? BoundTelesaleId { get; set; }
        public int? HubEmployeeId { get; set; }
        public decimal? LoanAmountFirst { get; set; }
        public decimal? LoanAmountExpertise { get; set; }
        public decimal? LoanAmountExpertiseLast { get; set; }
        public decimal? LoanAmountExpertiseAi { get; set; }
        public DateTime? FirstProcessingTime { get; set; }
        public int? LoanPurpose { get; set; }
        public int? ReMarketingLoanBriefId { get; set; }
        public bool? IsCheckBank{get;set;}
        public int? HomeNetwork { get; set; }
        public string PhoneOther { get; set; }
        public string BankBranch { get; set; }
        public int? TeamTelesaleId { get; set; }
        public int? ProductDetailId { get; set; }
        public bool? IsSim { get; set; }
        public bool? IsReborrow { get; set; }
        public bool? IsChangeInsurance { get; set; }
        public string Email { get; set; }
        public int? TransactionState { get; set; }
        public bool? IsTransactionsSecured { get; set; }
        public int? Ltv { get; set; }
        public int? LtvTypeRemarketing { get; set; }
        public string NoticeNumber { get; set; }
        public CustomerModel CustomerModel { get; set; } = new CustomerModel();
        public LoanBriefResidentModel LoanBriefResidentModel { get; set; } = new LoanBriefResidentModel();
        public LoanBriefHouseholdModel LoanBriefHouseholdModel { get; set; } = new LoanBriefHouseholdModel();
        public LoanBriefJobModel LoanBriefJobModel { get; set; } = new LoanBriefJobModel();
        public LoanBriefCompanyModel LoanBriefCompanyModel { get; set; } = new LoanBriefCompanyModel();
        public LoanBriefPropertyModel LoanBriefPropertyModel { get; set; } = new LoanBriefPropertyModel();
        public LoanBriefQuestionScriptModel LoanBriefQuestionScriptModel { get; set; } = new LoanBriefQuestionScriptModel();
        public List<LoanBriefRelationshipModel> LoanBriefRelationshipModels { get; set; } = new List<LoanBriefRelationshipModel>();
    }

    public class CustomerModel
    {
        public string FacebookAddress { get; set; }
        public string InsurenceNumber { get; set; }
        public string InsurenceNote { get; set; }
        public int? IsMerried { get; set; }
        public int? NumberBaby { get; set; }
        public string EsignAgreementId { get; set; }
    }

    public class LoanBriefResidentModel
    {
        public int LoanBriefResidentId { get; set; }
        public int? LivingTime { get; set; }
        public int? ResidentType { get; set; }
        public int? LivingWith { get; set; }
        public string Address { get; set; }
        public string AddressGoogleMap { get; set; }
        public string AddressLatLng { get; set; }
        public string BillElectricityId { get; set; }
        public string BillWaterId { get; set; }
        public string AddressNationalCard { get; set; }
        public string CustomerShareLocation { get; set; }
    }
    public class LoanBriefHouseholdModel
    {
        public string Address { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string FullNameHouseOwner { get; set; }
        public int? RelationshipHouseOwner { get; set; }
        public string BirdayHouseOwner { get; set; }
    }
    public class LoanBriefJobModel
    {
        public int LoanBriefJobId { get; set; }
        public int? JobId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyTaxCode { get; set; }
        public int? CompanyProvinceId { get; set; }
        public int? CompanyDistrictId { get; set; }
        public int? CompanyWardId { get; set; }
        public string CompanyAddress { get; set; }
        public decimal? TotalIncome { get; set; }
        public int? ImcomeType { get; set; }
        public string Description { get; set; }
        public string CompanyAddressGoogleMap { get; set; }
        public string CompanyAddressLatLng { get; set; }
        public bool? CompanyInsurance { get; set; }
        public bool? WorkLocation { get; set; }
        public bool? BusinessPapers { get; set; }
        public int? JobDescriptionId { get; set; }

    }
    public class LoanBriefCompanyModel
    {
        public int LoanBriefId { get; set; }
        public string CompanyName { get; set; }
        public string CareerBusiness { get; set; }
        public string BusinessCertificationAddress { get; set; }
        public string HeadOffice { get; set; }
        public string Address { get; set; }
        public string CompanyShareholder { get; set; }
        public string sCardNumberShareholderDate { get; set; }
        public string sBusinessCertificationDate { get; set; }
        public string sBirthdayShareholder { get; set; }
        public string CardNumberShareholder { get; set; }
        public string PlaceOfBirthShareholder { get; set; }
    }

    public class LoanBriefRelationshipModel
    {
        public int Id { get; set; }
        public int? RelationshipType { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int? RefPhoneCallRate { get; set; }
    }
    public class LoanBriefPropertyModel
    {
        public int LoanBriefPropertyId { get; set; }
        public int? BrandId { get; set; }
        public int? ProductId { get; set; }
        public string PlateNumber { get; set; }
        public string CarManufacturer { get; set; }
        public string CarName { get; set; }
        public string CarColor { get; set; }
        public string PlateNumberCar { get; set; }
        public string Chassis { get; set; }
        public string Engine { get; set; }
		public bool? BuyInsuranceProperty { get; set; }

        public string OwnerFullName { get; set; }
        public string MotobikeCertificateNumber { get; set; }
        public string MotobikeCertificateDate { get; set; }
        public string MotobikeCertificateAddress { get; set; }
        public bool? PostRegistration { get; set; }
        public string Description { get; set; }

    }

}
