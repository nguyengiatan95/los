﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class LocationAIReq
    {
        public LocationCode locationCodes { get; set; }
        public string phone { get; set; }
        public int age { get; set; }
        public string url { get; set; }
        public string carrier { get; set; }
        public bool office { get; set; }
        public bool validateAddress { get; set; }
    }
    public class LocationCode
    {
        public string home { get; set; }
        public string office { get; set; }
        public string other { get; set; }
    }

    public class LocationAIRes
    {
        public int code { get; set; }
        public string messages { get; set; }
        public DataVerify data { get; set; }
    }

    public class DataVerify
    {
        public string refCode { get; set; }
        public string status { get; set; }
    }

    public class RefPhoneAIRes
    {
        public int code { get; set; }
        public string messages { get; set; }
        public DataVerify data { get; set; }
    }

    public class CreditScoringRes
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public string RequestID { get; set; }
    }
}
