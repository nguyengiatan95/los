﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.ManagerAreaHub
{
    public class ManagerAreaHubDetailsModel
    {
        public List<SelectItem> lstDistrit { get; set; }
        public List<SelectItem> lstWard { get; set; }
        public int checkall { get; set; }

    }
    public class SelectItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
        public int DistrictId { get; set; }
        public string  DistrictName { get; set; }
    }
}
