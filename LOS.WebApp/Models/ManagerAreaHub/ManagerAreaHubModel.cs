﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.ManagerAreaHub
{
    public class ManagerAreaHubModel
    {
        public List<ProvinceDetail> listProvince { get; set; }
        public List<LoanProduct> listProduct { get; set; }
        public List<ShopDetail> listShop { get; set; }
        
    }
}
