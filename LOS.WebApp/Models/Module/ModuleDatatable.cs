﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ModuleDatatable : DatatableBase
    {
        public ModuleDatatable(IFormCollection form) : base(form)
        {
            name = form["query[filterName]"].FirstOrDefault();
            status = form["query[status]"].FirstOrDefault();
            ismenu = form["query[menu]"].FirstOrDefault();
            applicationId = form["query[ApplicationId]"].FirstOrDefault();
        }

        public string name { get; set; }
        public string status { get; set; }
        public string ismenu { get; set; }
        public string applicationId { get; set; }


    }
}
