﻿using LOS.DAL.DTOs;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace LOS.WebApp.Models
{
    public class ModuleItem
    {
        public int? ModuleId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int? Status { get; set; }
        public int? ParentId { get; set; }
        public int? Priority { get; set; }
        public int ApplicationId { get; set; }
        public string Icon { get; set; }
        public bool IsMenu { get; set; }
        public int CheckMenu { get; set; }
        public List<LOS.DAL.EntityFramework.Module> lstModule { get; set; }
        public List<SelectListItem> LstAplication { get; set; }


    }
}
