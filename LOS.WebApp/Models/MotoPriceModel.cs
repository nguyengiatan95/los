﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class MotoPriceModel
    {
        public ToolCalculatorMoney ToolCalculatorMoney { get; set; }
        public LoanBriefDetailNew LoanBriefDetailNew { get; set; }
        public List<ProductReview> ProductReview { get; set; }
        public UserDetail UserDetail { get; set; }
        public int LoanBriefId { get; set; }
        public int LoanBriefProductId { get; set; }
        public int Status { get; set; }

        public LoanBriefProperty LoanbriefProperty { get; set; }
        public LoanBriefResident LoanBriefResident { get; set; }
    }
    public class ProductReview
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int? Status { get; set; }
        public bool? State { get; set; }
        public bool? IsCancel { get; set; }
        public int? IdType { get; set; }
        public bool? IsCheck { get; set; }
        public long? MoneyDiscount { get; set; }
    }
}
