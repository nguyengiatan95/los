﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ChangePipelineReq
    {
        public int loanBriefId { get; set; }
        public int productId { get; set; }
        public int status { get; set; }
    }
}
