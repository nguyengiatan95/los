﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Product
{
    public class ProductDatatable : DatatableBase
    {
        public ProductDatatable(IFormCollection form) : base(form)
        {
            name = form["query[filterName]"].FirstOrDefault();
        }
        public string name { get; set; }
    }
}
