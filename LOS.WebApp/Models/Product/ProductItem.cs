﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Product
{
    public class ProductItem
    {
        public int Id { get; set; }
        public int BrandProductId { get; set; }
        public int ProductTypeId { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public int Year { get; set; }
        public int BrakeType { get; set; }
        public int RimType { get; set; }
        public string ShortName { get; set; }
    }
}
