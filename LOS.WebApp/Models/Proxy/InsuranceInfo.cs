﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Proxy
{
    public class InsuranceInfo
    {
        public partial class Output
        {
            [JsonProperty("Result")]
            public long Result { get; set; }

            [JsonProperty("Message")]
            public string Message { get; set; }

            [JsonProperty("Data")]
            public Data Data { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("providerCode")]
            public string ProviderCode { get; set; }

            [JsonProperty("responseTime")]
            public double ResponseTime { get; set; }

            [JsonProperty("result")]
            public List<Result> Result { get; set; }
        }

        public partial class Result
        {
            [JsonProperty("code")]
            public string Code { get; set; }

            [JsonProperty("nid")]
            public string Nid { get; set; }

            [JsonProperty("fullname")]
            public string Fullname { get; set; }

            [JsonProperty("dob")]
            public string Dob { get; set; }

            [JsonProperty("range")]
            public string Range { get; set; }

            [JsonProperty("scompany")]
            public string Scompany { get; set; }

            [JsonProperty("scompanyaddress")]
            public string Scompanyaddress { get; set; }

            [JsonProperty("scompanysiid")]
            public string Scompanysiid { get; set; }

            [JsonProperty("scompanytaxid")]
            public string Scompanytaxid { get; set; }

            [JsonProperty("sfrommonth")]
            public string Sfrommonth { get; set; }

            [JsonProperty("stomonth")]
            public string Stomonth { get; set; }

            [JsonProperty("ssiid")]
            public string Ssiid { get; set; }

            [JsonProperty("staxid")]
            public string Staxid { get; set; }

            [JsonProperty("checkBy")]
            public string CheckBy { get; set; }

            [JsonProperty("smonth")]
            public string Smonth { get; set; }
        }
    }

}
