﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class RecordingModelView
    {
        public int LoanbriefId { get; set; }
        public string FullName { get; set; }
        public List<RecordingItemView> ListRecording { get; set; }
        public string MessErr { get; set; }
    }

    public class RecordingItemView
    {
        public string Phone { get; set; }
        public string FullName { get; set; }
        public string RelationshipName { get; set; }
        public string UserName { get; set; }
        public string DepartmentName { get; set; }
        public string CallStatus { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string StrWaitingTime { get; set; }
        public string StrTalkTime { get; set; }
        public string Path { get; set; }
        public int TypeCallService { get; set; }
        public string PlayUrl { get; set; }
        public string RecordingUrl { get; set; }
        public long CallDuration { get; set; }
    }
}
