﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class RemarketingTlsViewModel
    {
        public List<ReasonCancelDetail> ListReasonGroup { get; set; }
        public List<PlatformTypeDetail> ListPlatformType { get; set; }
    }
}
