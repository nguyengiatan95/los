﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class RemarketingDatatable : DatatableBase
    {
        public RemarketingDatatable(IFormCollection form) : base(form)
        {
            actionId = form["query[actionId]"].FirstOrDefault();
            DateRanger = form["query[filterCreateTime]"].FirstOrDefault();
            DateCancelRanger = form["query[filterCancelTime]"].FirstOrDefault();
            ReasonCancel = form["query[reasonCancel]"].FirstOrDefault();
            DetailReason = form["query[detailReason]"].FirstOrDefault();
            Status = form["query[status]"].FirstOrDefault();
            TextSearch = form["query[filterTextSearch]"].FirstOrDefault();
            Source = form["query[filterSource]"].FirstOrDefault();
            UtmSource = form["query[filterUtmSource]"].FirstOrDefault();
            ProvinceId = form["query[filterProvince]"].FirstOrDefault();
        }
        public string ReasonCancel { get; set; }
        public string actionId { get; set; }
        public string DateRanger { get; set; }
        public string DateCancelRanger { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string FromCancelDate { get; set; }
        public string ToCancelDate { get; set; }
        public string DetailReason { get; set; }
        public string Status { get; set; }
        public string TextSearch { get; set; }
        public string Source { get; set; }
        public string UtmSource { get; set; }
        public int GroupId { get; set; }
        public string ProvinceId { get; set; }
        public int ShopId { get; set; }
    }
}
