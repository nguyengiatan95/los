﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ReportLoanBriefByUserMmo: DatatableBase
    {
        public ReportLoanBriefByUserMmo(IFormCollection form) : base(form)
        {
            Name = form["query[filterName]"].FirstOrDefault();
            DateRanger = form["query[filtercreateTime]"].FirstOrDefault();

        }

        public string DateRanger { get; set; }
        public string Name { get; set; }
        public int Affcode { get; set; }
        public int AffStatus { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
