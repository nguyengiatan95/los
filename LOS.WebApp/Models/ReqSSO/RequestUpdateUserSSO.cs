﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.ReqSSO
{
    public class RequestUpdateUserSSO
    {
        public int Id { get; set; }
        public string Password { get; set; }
    }
}
