﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.ReqSSO
{
    public class ResponeGenNewOtp
    {
        public Meta meta { get; set; }
        public GenNewOtp data { get; set; }
    }
    public class GenNewOtp
    {
        public string UserName { get; set; }
        public int Otp { get; set; }
    }
}
