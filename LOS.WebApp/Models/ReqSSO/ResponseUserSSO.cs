﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.ReqSSO
{
    public class ResponseUserSSO
    {
        public Meta meta { get; set; }
        public Data data { get; set; }
    }

    public class Meta
    {
        public int errorCode { get; set; }
        public string errorMessage { get; set; }
    }
    public class Data
    {
        public int id { get; set; }
        public string username { get; set; }
        public string email { get; set; }
    }
}
