﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class SSO
    {
        public partial class Login
        {
            [JsonProperty("meta")]
            public Meta Meta { get; set; }

            [JsonProperty("data")]
            public Data Data { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("token")]
            public string Token { get; set; }
        }

        public partial class Meta
        {
            [JsonProperty("errorCode")]
            public long ErrorCode { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrorMessage { get; set; }
        }

    }
}
