﻿using LOS.WebApp.Services;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Request
{
	public class DeleteReq
	{
		public int id { get; set; }
	}
}
