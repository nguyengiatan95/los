﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Request.Group
{
    public class GroupReq
    {
        public int groupId { get; set; }
        public string groupName { get; set; }
        public string description { get; set; }
        public int status { get; set; }
        public string defaultPath { get; set; }

    }
}
