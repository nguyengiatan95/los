﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Request
{
	public class IdOnlyReq
	{
		public int id { get; set; }
		public int index { get; set; }
		public int type { get; set; }
	}
}
