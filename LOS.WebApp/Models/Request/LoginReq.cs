﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Request
{
	public class LoginReq
	{
		public string Username { get; set; }
		public string Password { get; set; }
	}
}
