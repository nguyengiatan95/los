﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Request.Pipeline
{
    public class AddSectionDetailReq
    {        
		public int? SectionDetailId { get; set; }
        public int? SectionId { get; set; }
        public string Name { get; set; }        
        public string Mode { get; set; }        
        public int? ReturnId { get; set; }
        public int? DisapproveId { get; set; }
        public List<AddSectionDetailAction> Actions { get; set; }
        public List<AddActionDetailAppove> Approves { get; set; }
        public List<AddActionDetailAppove> Disapproves { get; set; }
    }

    public class AddSectionDetailAction
    {
        public int? PropertyId { get; set; }       
        public string Value { get; set; }
		public int? Type { get; set; }
    }

    public class AddActionDetailAppoveCondition
	{
        public int? PropertyId { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
    }

	public class AddActionDetailAppove
	{
		public int? ApproveId { get; set; }
		public List<AddActionDetailAppoveCondition> Conditions { get; set; }
	}
}
