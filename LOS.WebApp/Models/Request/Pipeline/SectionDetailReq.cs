﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Request.Pipeline
{
    public class SectionDetailReq
    {
        public int sectionId { get; set; }
        public int pipelineId { get; set; }
    }
}
