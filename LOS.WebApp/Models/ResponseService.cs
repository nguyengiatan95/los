﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ResponseService<TEntity> where TEntity : class
    {
        public string Message { get; set; }
        public TEntity Data { get; set; }
    }
    public class ResponseService
    {
        public string Message { get; set; }
        public bool Success { get; set; }
    }
}
