﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Users
{
    public class TelesalesDatatable : DatatableBase
    {
        public TelesalesDatatable(IFormCollection form) :base(form)
        {
            name = form["query[filterName]"].FirstOrDefault();
            status = form["query[status]"].FirstOrDefault();
            team = form["query[team]"].FirstOrDefault();
            typeRecievedProduct = form["query[typeRecievedProduct]"].FirstOrDefault();
            //telesaleRecievedLoan = form["query[telesaleRecievedLoan]"].FirstOrDefault();

        }

        public string userId { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public string team { get; set; }
        public string typeRecievedProduct { get; set; }
        public string teamTelesales { get; set; }
        //public string telesaleRecievedLoan { get; set; }
    }
}
