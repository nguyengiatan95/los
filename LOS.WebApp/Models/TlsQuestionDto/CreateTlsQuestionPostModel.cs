﻿using LOS.DAL.EntityFramework;

namespace LOS.WebApp.Models.TlsQuestionDto
{
    public class CreateTlsQuestionPostModel : TlsQuestion
    {
        public string[] JSonOptionsText { get; set; }
        public string[] JSonOptionsValue { get; set; }
    }
}
