﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.TlsScenarioDto;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LOS.WebApp.Models.TlsQuestionDto
{
    public class CreateTlsQuestionViewModel
    {
        public TlsQuestion TlsQuestion { get; set; }
        public List<SelectListItem> TypeOfQuestion 
        {
            get {
                return Enum.GetValues(typeof(TypeOfQuestion)).Cast<TypeOfQuestion>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList();
            }
        }
        public List<SelectListItem> FieldMapping
        {
            get
            {
                System.Type type = typeof(TlsScenario_Telesales_CreateNew);
                System.Reflection.PropertyInfo[] properties = type.GetProperties();

                return properties.Select(x => new SelectListItem
                {
                    Text = x.Name.ToString(),
                    Value = x.Name.ToString()
                }).ToList();
            }
        }

    }
}
