﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;

namespace LOS.WebApp.Models.TlsQuestionDto
{
    public class TlsQuestionViewModel : TlsQuestion
    {

        public string TypeOfQuestionName
        {
            get { return ((TypeOfQuestion)base.TypeOfQuestion).ToString(); }
        }

    }
}
