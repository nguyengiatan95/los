﻿using LOS.DAL.EntityFramework;

namespace LOS.WebApp.Models.TlsScenarioDto
{
    public class CreateTlsScenarioPostModel : TlsScenario
    {
        public int[] QuestionIds { get; set; }
    }
}
