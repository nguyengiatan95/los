﻿using LOS.DAL.EntityFramework;
using System.Collections.Generic;

namespace LOS.WebApp.Models.TlsScenarioDto
{
    public class TlsScenarioViewModel
    {
        public TlsScenarioViewModel()
        {
            TlsScenario = new TlsScenario();
        }


        public TlsScenario TlsScenario { get; set; }
        public IEnumerable<int> TlsScenarioQuestionIds { get; set; }
        public IEnumerable<TlsQuestion> Questions{ get; set; }
    }
}
