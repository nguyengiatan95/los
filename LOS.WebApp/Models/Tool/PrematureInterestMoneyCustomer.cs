﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class PrematureInterestMoneyCustomer
    {
        public string DateSettlement { get; set; }
        public string CustomerMoney { get; set; }
        public string FullName { get; set; }
    }
    public class ItemPrematureInterestMoneyCustomer
    {
        public string CodeId { get; set; }
        public long TotalMoneyCurrent { get; set; }
        public long MoneyFineOriginal { get; set; }
        public long MoneyInterest { get; set; }
        public long OtherMoney { get; set; }
        public long MoneyCloseLoan { get; set; }
        public long TotalPaymentCustomer { get; set; }
    }
    public class PrematureInterestPaymentSchedules
    {
        public string CodeId { get; set; }
        public List<PaymentSchedule> LstPaymentSchedules { get; set; }
    }
}
