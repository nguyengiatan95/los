﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ToolCalculatorMoney
    {
        public decimal MaxPriceProduct { get; set; }
        public List<TypeOwnerShip> ListOwnerShip { get; set; }
        public List<BrandProductDetail> ListBrandProduct { get; set; }
        public List<LoanProduct> ListLoanProduct { get; set; }
        public List<ProductDetailModel> ListProductDetail { get; set; }

    }
}
