﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class AuthenticationResult
    {
        public string app_id { get; set; }
        public string token { get; set; }
        public DateTime expired_date { get; set; }
    }

    public class AuthenticationInput
    {
        public string app_id { get; set; }
        public string app_key { get; set; }
    }
}
