﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class GeocodingResult
    {
        public PlusCode plus_code { get; set; }
        public List<result> results { get; set; }
    }
    public class result
    {
        public List<address_component> address_components { get; set; }
        public string formatted_address { get; set; }
        public string place_id { get; set; }
    }
    public class PlusCode
    {
        public string compound_code { get; set; }
        public string global_code { get; set; }
    }
    public class address_component
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }
}
