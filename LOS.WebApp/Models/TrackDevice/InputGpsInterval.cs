﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class InputGpsInterval
    {
        public string contractid { get; set; }
        public string imei { get; set; }
        public long time_received1 { get; set; }
        public long time_received2 { get; set; }
    }

    public class OutputGpsInterval
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<LocationItem> Data { get; set; }
    }

    public class LocationItem
    {
        public long start { get; set; }
        public long time { get; set; }
        public decimal lat { get; set; }
        public decimal lng { get; set; }
        public decimal velocity { get; set; }
        public string FormattedAddess { get; set; }
        public double timeToMinute
        {
            get
            {
                double resul = 0;
                try
                {
                    if (time > 0)
                    {
                        System.DateTime timeToString = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        resul = TimeSpan.FromMilliseconds(time).TotalMinutes;
                    }
                }
                catch { }
                return resul;
            }
        }
        public string timeToString
        {
            get
            {
                var resul = "";
                try
                {
                    if (start > 0 && time > 0)
                    {
                        var parts = new List<string>();
                        Action<int, string> add = (val, unit) => { if (val > 0) parts.Add(val + unit); };
                        var t = TimeSpan.FromMilliseconds(time);
                        add(t.Days, " ngày");
                        add(t.Hours, " tiếng");
                        add(t.Minutes, " phút");
                        //add(t.Seconds, "s");
                        //add(t.Milliseconds, "ms");
                        resul = string.Join(" ", parts);
                    }
                    else if (time > 0)
                    {
                        System.DateTime timeToString = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        resul = timeToString.AddMilliseconds(time).ToLocalTime().ToString("dd/MM/yyyy HH:mm");
                    }
                }
                catch { }
                return resul;
            }
        }
        public string startToString
        {
            get
            {
                var result = "";
                try
                {
                    if (start > 0)
                    {
                        System.DateTime timeToString = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        result = timeToString.AddMilliseconds(start).ToLocalTime().ToString("dd/MM/yyyy HH:mm");
                    }
                }
                catch { }
                return result;//  
            }
        }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public long fromDateToUnixTimesTampMilliseconds
        {
            get
            {
                long result = 0;
                try
                {
                    result = (long)(fromDate.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                }
                catch { result = 0; }
                return result;
            }
        }
        public long toDateToUnixTimesTampMilliseconds
        {
            get
            {
                long result = 0;
                try
                {
                    result = (long)(toDate.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                }
                catch { result = 0; }
                return result;
            }
        }
    }
    public class GetLocations
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public List<LocationItem> StopPoint { get; set; }
        public List<LocationItem> StopPointAdress { get; set; }
        public List<LocationItem> LocationDevice { get; set; }
        public List<FirstLocationData> LocationFirstTime { get; set; }
        public DataReportAddress GetDataReportAddress { get; set; }
    }
    #region FB, BHXH, BHYT, TAX, CMT

    public class InputFbGetScore
    {
        public string phone { get; set; }
    }
    public class OutputFbGetScore
    {
        public string fb_score { get; set; }
        public string fb_id { get; set; }
        public long num_friend { get; set; }
        public string message { get; set; }
        public string status { get; set; }
    }
    //BHXH 
    public class InputSocialInsurance
    {
        public string name { get; set; }
        public string personal_code { get; set; }
        public string region { get; set; }
        public string referenceCode { get; set; }
    }
    public class OutputSocialInsurance
    {
        public int responseCode { get; set; }
        public string mess { get; set; }
        public OutputSocialInsurance_result result { get; set; }
    }
    public class OutputSocialInsurance_result
    {
        public string referenceCode { get; set; }
        public string status { get; set; }
        public string statusDes { get; set; }
        public List<OutputSocialInsurance_details> details { get; set; }
    }
    public class OutputSocialInsurance_details
    {
        public string referenceCode { get; set; }
        public string insurance_code { get; set; }
        public string personal_code { get; set; }
        public string name { get; set; }
        public string gender { get; set; }
        public string birth_date { get; set; }
        public string family_code { get; set; }
        public string address { get; set; }
        public string last_update { get; set; }
        public string start_date { get; set; }
        public string expired_date { get; set; }
        public string kcbbd { get; set; }
        public string process { get; set; }
    }
    public class InputTaxByPeopleId
    {
        public string PeopleId { get; set; }
    }
    public class OutputTaxByPeopleId
    {
        public string responseCode { get; set; }
        public string mess { get; set; }
        public List<OutputTaxByPeopleId_result> result { get; set; }
    }
    public class OutputTaxByPeopleId_result
    {
        public string gender { get; set; }
        public string address { get; set; }
        public string commune { get; set; }
        public string district { get; set; }
        public string province { get; set; }
        public string phone { get; set; }
        public string fullName { get; set; }
        public string peopleId { get; set; }
        public string dateOfBirth { get; set; }
    }
    public class InputNationalId
    {
        public string front_side { get; set; }
        public string back_side { get; set; }
    }
    public class OutputNationalId
    {
        public string status_code { get; set; }
        public string status { get; set; }
        public string id { get; set; }
        public string fullname { get; set; }
        public string address { get; set; }
        public string birthday { get; set; }
        public string gender { get; set; }
        public string expiry { get; set; }
        public string ethnicity { get; set; }
        public string issue_by { get; set; }
        public string issue_date { get; set; }
        public string religion { get; set; }
    }
    #endregion
    public class InputGpsDeviceTrackingReport
    {
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public int percent { get; set; }
    }
    public class OutputGpsDeviceTrackingReport
    {
        public string Message { get; set; }
        public int StatusCode { get; set; }
        public List<OutputGpsDeviceTrackingReport_Detail> details { get; set; }
    }
    public class OutputGpsDeviceTrackingReport_Detail
    {
        public int loanCreditId { get; set; }
        public decimal percentHome { get; set; }
        public decimal percentCompany { get; set; }
    }
    public class InputGpsListDeviceTrackingDisconnected
    {
        public int minutes { get; set; }
    }
    public class OutputGpsListDeviceTrackingDisconnected
    {
        public string Message { get; set; }
        public int StatusCode { get; set; }
        public List<OutputGpsListDeviceTrackingDisconnected_Detail> details { get; set; }
    }
    public class OutputGpsListDeviceTrackingDisconnected_Detail
    {
        public int loanCreditId { get; set; }
        public DateTime lastUpdate { get; set; }
    }
}
