﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class InputGpsReport
    {
        public AddressItem address { get; set; }
        public string contractid { get; set; }
        public string imei { get; set; }
        public int radius { get; set; }
        public double time_received1 { get; set; }
        public double time_received2 { get; set; }

    }

    public class AddressItem
    {
        public string home { get; set; }
        public string office { get; set; }
        public string other { get; set; }

    }

    public class LocationItemReport
    {
        public InputLocationItem home { get; set; }
        public InputLocationItem office { get; set; }
        public InputLocationItem other { get; set; }

    }

    public class InputGpsReportLocation
    {
        public LocationItemReport location { get; set; }
        public string contractid { get; set; }
        public string imei { get; set; }
        public int radius { get; set; }
        public double time_received1 { get; set; }
        public double time_received2 { get; set; }

    }

    public class InputLocationItem
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }
}
