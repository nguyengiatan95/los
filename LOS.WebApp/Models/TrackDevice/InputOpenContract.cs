﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class InputOpenContract
    {
        public string contractid { get; set; }
        public string imei { get; set; }
        public string shopid { get; set; }
        public string vehicle_type { get; set; }
        public string contract_type { get; set; }
    }

    public class OutputOpenContract
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class InputCloseContract
    {
        public string contractid { get; set; }
        public string imei { get; set; }
    }

    public class OutputCloseContract
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class ReActiveDeviceRes
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class OutputCheckImei
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public CheckImeiItem Data { get; set; }
    }

    public class CheckImeiItem
    {
        public string imei { get; set; }
        public string contractid { get; set; }
        public int device_status { get; set; }
    }

    public class InputUpdateContractId
    {
        public string imei { get; set; }
        public string newContractId { get; set; }
        public string oldContractId { get; set; }
    }

    public class OutputUpdateContract
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class InputFirstLocation
    {
        public List<string> imeis { get; set; }
    }

    public class OutputFirstLocation
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<FirstLocationData> Data { get; set; }
    }

    public class FirstLocationData
    {
        public string Imei { get; set; }
        public string CustomerId { get; set; }
        public string ContractId { get; set; }
        public string RecordId { get; set; }
        public decimal Distance { get; set; }
        public string VehicleState { get; set; }
        public double Time { get; set; }
        public decimal Velocity { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public double Timestamp { get; set; }
        public string FormattedAddess { get; set; }
        public List<ContractInfo> contract_info { get; set; }
        public double TimestampSubtractNowToMinute
        {
            get
            {
                double resul = 0;
                try
                {
                    if (Timestamp > 0)
                    {
                        var now = (DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                        System.DateTime timeToString = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        var milliseconds = now - Timestamp;
                        resul = TimeSpan.FromMilliseconds(milliseconds).TotalMinutes;
                    }
                }
                catch { }
                return resul;
            }
        }
        public string timeToString
        {
            get
            {
                var resul = "";
                try
                {
                    if (Timestamp > 0)
                    {
                        System.DateTime timeToString = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        resul = timeToString.AddMilliseconds(Timestamp).ToLocalTime().ToString("dd/MM/yyyy HH:mm");
                    }
                }
                catch { }
                return resul;
            }
        }
        public string CreatedTimeToString
        {
            get
            {
                var resul = "";
                try
                {
                    if (contract_info.Count > 0)
                    {
                        var createdTimeToString = contract_info.FirstOrDefault() ?? new ContractInfo();
                        resul = (createdTimeToString.CreatedTime ?? DateTime.Now).ToString("dd/MM/yyyy HH:mm");
                    }
                }
                catch { }
                return resul;
            }
        }
    }

    public class ContractInfo
    {
        public string DeviceIdHex { get; set; }
        public string Imei { get; set; }
        public int DeviceType { get; set; }
        public string DeviceVersion { get; set; }
        public int DeviceStatus { get; set; }
        public string HardwareVersion { get; set; }
        public string FirmwareVersion { get; set; }
        public string DevicePhoneNumber { get; set; }
        public int GuaranteeId { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public string CustomerName { get; set; }
        public string PhoneNumber { get; set; }
        public int? PersonalId { get; set; }
        public string LicensePlate { get; set; }
        public string VehicleType { get; set; }
        public string ContractId { get; set; }
        public string RealContractId { get; set; }
        public string VehicleManufacturer { get; set; }
        public string ContractIdHex { get; set; }
        public int ContractStatus { get; set; }
    }
}
