﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class OutputGpsReport
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public DataReportAddress data { get; set; }
    }

    public class ReportAddressItem
    {
        public double lng { get; set; }
        public double lat { get; set; }
        public int radius { get; set; }
        public decimal rate { get; set; }
        public string FormattedAddess { get; set; }
    }

    public class DataReportAddress
    {
        public ReportAddressItem home { get; set; }
        public ReportAddressItem office { get; set; }
        public ReportAddressItem other { get; set; }
    }
}
