﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class JSTreeModel
    {
        public string id { get; set; }
        public string text { get; set; }
        public StateJtree state { get; set; }
        public string IsMenu { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string InputControl { get; set; }
        public string ApiController { get; set; }
        public string Icon { get; set; }
        public List<JSTreeModel> children { get; set; }

        public JSTreeModel()
        {
            children = new List<JSTreeModel>();
            state = new StateJtree();
        }
        public string Link
        {
            get
            {
                return "/" + this.ControllerName + "/" + this.ActionName + "/";
            }
        }
        public string parentId { get; set; }
    }
    public class StateJtree
    {
        public StateJtree()
        {
            opened = true;
            disabled = false;
            selected = false;
        }
        public bool opened { get; set; }
        public bool disabled { get; set; }
        public bool selected { get; set; }
    }
}
