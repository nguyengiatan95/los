﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ResultCompare
    {
        public bool InValidStep { get; set; }
        public double distance { get; set; }
        public double totalTime { get; set; }
        public int fromDocumentId { get; set; }
        public int toDocumentId { get; set; }
        public HashSet<string> ListLatLong { get; set; }
    }
}
