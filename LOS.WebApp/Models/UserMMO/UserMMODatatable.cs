﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class UserMmoDatatable : DatatableBase
    {
        public UserMmoDatatable(IFormCollection form) : base(form)
        {
            name = form["query[filterName]"].FirstOrDefault();
            isActive = form["query[isActive]"].FirstOrDefault();
           
        }

        public string name { get; set; }
        public string isActive { get; set; }

    }
}
