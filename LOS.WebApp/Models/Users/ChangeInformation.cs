﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class ChangeInformation
    {
        public int UserId { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int TypeCallService { get; set; }
        public int? Ipphone { get; set; }
        public string CiscoUsername { get; set; }
        public string CiscoPassword { get; set; }
        public string CiscoExtension { get; set; }

    }
}
