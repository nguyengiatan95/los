﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models
{
    public class PermissionUserItem
    {
        public int UserId { get; set; }
        public List<ActionItem> ActionUsers { get; set; }
        public List<ConditionItem> ConditionUsers { get; set; }
    }

    public class PermissionGroupItem
    {
        public int GroupId { get; set; }
        public List<ActionItem> ActionGroups { get; set; }
        public List<ConditionItem> ConditionGroups { get; set; }
    }

    public class ActionItem
    {
        public int PropertyId { get; set; }
        public int Value { get; set; }
    }

    public class ConditionItem
    {
        public int PropertyId { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
    }
}
