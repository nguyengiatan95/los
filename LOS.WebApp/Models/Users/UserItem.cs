﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Users
{
    public class UserItem
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? CreatedTime { get; set; }
        public DateTimeOffset? UpdatedTime { get; set; }
        public int? CompanyId { get; set; }
        public int? DepartmentId { get; set; }
        public int? PositionId { get; set; }
        public int? CityId { get; set; }
        public int? GroupId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public int? CreatedBy { get; set; }
        public List<int> HubIds { get; set; }
        public int? Ipphone { get; set; }
        public string CiscoUsername { get; set; }
        public string CiscoPassword { get; set; }
        public string CiscoExtension { get; set; }
        public int TypeCallService { get; set; }
        public bool TelesaleRecievedLoan { get; set; }
        public int TypeRecievedProduct { get; set; }
        public int? ValueTelesalesShift { get; set; }
        public int? TeamTelesalesId { get; set; }
        public List<LOS.DAL.EntityFramework.Group> ListGroup { get; set; }

        public List<ShopDetail> ListAllHub { get; set; }
        public List<string> lstMenu { get; set; }
        public List<string> lstAction { get; set; }
        public List<UserModule> ListUserModule { get; set; }
        public List<ShopDetail> ListHub { get; set; }
        public List<TelesalesShift> ListTelesalesShift { get; set; }


    }
}
