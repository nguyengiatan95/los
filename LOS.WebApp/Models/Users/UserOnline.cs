﻿using Hazelcast.IO.Serialization;
using LOS.WebApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Models.Users
{
	public class OnlineUser
	{
		public int UserId { get; set; }
		public string Username { get; set; }
		public int? GroupId { get; set; }
		public DateTimeOffset LoginTime { get; set; }
		public DateTimeOffset LastTriggerTime { get; set; }
		public int OnlineTime { get; set; }
		public int WorkingTime { get; set; }
		public int RestTime { get; set; }
		public bool IsOnline { get; set; }
		public bool IsWorking { get; set; }
		public bool IsProcessing { get; set; }
		public List<OnlineData> OnlineData { get; set; }
	}

	public class OnlineData
	{
		public DateTimeOffset StartTime { get; set; }
		public DateTimeOffset EndTime { get; set; }
		public int Duration { get; set; }
	}
}
