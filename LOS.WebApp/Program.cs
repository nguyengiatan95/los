using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LOS.WebApp.Services;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Compact;

namespace LOS.WebApp
{
    public class Program
    {
        public static void Main(string[] args)
        {

            Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Override("Microsoft", LogEventLevel.Fatal)
                        .Enrich.FromLogContext()
                        .WriteTo.File(new CompactJsonFormatter(), "logs/tima.log", rollOnFileSizeLimit: true, fileSizeLimitBytes: 10_000_000, rollingInterval: RollingInterval.Day)
                        .WriteTo.Seq("http://seq:5341")
                        .CreateLogger();
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            Console.WriteLine("ASPNETCORE_ENVIRONMENT: " + environmentName);
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", environmentName);

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseSetting("detailedErrors", "true")
                .UseKestrel(serverOptions =>
                {
                    serverOptions.Limits.KeepAliveTimeout =
                        TimeSpan.FromMinutes(5);
                    serverOptions.Limits.RequestHeadersTimeout =
                        TimeSpan.FromMinutes(5);
                })
                .UseUrls("http://0.0.0.0:80", "http://0.0.0.0:8888")
                .UseStartup<Startup>();

    }
}
