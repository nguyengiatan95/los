﻿using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IAuthenticationAI
    {
        string GetToken(string app_id, string app_key, string access_token);
        string GetTokenRetry(string app_id, string app_key, string access_token, bool reGetToken = false);
    }
    public class AuthenticationService: IAuthenticationAI
    {
        protected IConfiguration _baseConfig;
        protected ITokenService _tokenService;
        public AuthenticationService(IConfiguration configuration, ITokenService tokenService)
        {
            _baseConfig = configuration;
            _tokenService = tokenService;
        }

        private AuthenticationResult Authentication(string app_id, string app_key)
        {
            try
            {
                
                var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.AI_AUTHENTICATION_ENPOINT;
                var client = new RestClient(url);
                var objInput = new AuthenticationInput()
                {
                    app_id = app_id,
                    app_key = app_key
                };
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddParameter("application/json", Newtonsoft.Json.JsonConvert.SerializeObject(objInput), ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                AuthenticationResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthenticationResult>(jsonResponse);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GetToken(string app_id, string app_key, string access_token)
        {
            try
            {
                var token = string.Empty;
                var tokenAPI = _tokenService.GetLastToken(access_token, app_id);
                if (tokenAPI == null || string.IsNullOrEmpty(tokenAPI.AccessToken1) 
                    || (tokenAPI.DateExpiration.HasValue && tokenAPI.DateExpiration.Value.Date <= DateTime.Now.Date))
                {
                    var reToken = Authentication(app_id, app_key);
                    if (!string.IsNullOrEmpty(reToken.token))
                    {
                        token = reToken.token;
                        //insert to db
                        var objToken = new TokenDetail()
                        {
                            AccessToken1 = reToken.token,
                            DateExpiration = reToken.expired_date,
                            AppId = app_id
                        };
                        _tokenService.AddToken(access_token, objToken);
                        //ThreadPool.QueueUserWorkItem(item => _tokenService.AddToken(access_token, objToken));
                    }
                }
                else
                {
                    token = tokenAPI.AccessToken1;
                }
                return token;
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }
        public string GetTokenRetry(string app_id, string app_key, string access_token, bool reGetToken = false)
        {
            try
            {
                var token = string.Empty;
                var tokenAPI = _tokenService.GetLastToken(access_token, app_id);
                if (tokenAPI == null || string.IsNullOrEmpty(tokenAPI.AccessToken1)
                    || (tokenAPI.DateExpiration.HasValue && tokenAPI.DateExpiration.Value.Date <= DateTime.Now.Date) || reGetToken)
                {
                    var reToken = Authentication(app_id, app_key);
                    if (!string.IsNullOrEmpty(reToken.token))
                    {
                        token = reToken.token;
                        //insert to db
                        var objToken = new TokenDetail()
                        {
                            AccessToken1 = reToken.token,
                            DateExpiration = reToken.expired_date,
                            AppId = app_id
                        };
                        _tokenService.AddToken(access_token, objToken);
                        //ThreadPool.QueueUserWorkItem(item => _tokenService.AddToken(access_token, objToken));
                    }
                }
                else
                {
                    token = tokenAPI.AccessToken1;
                }
                return token;
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }
    }
}
