﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.AISuggestionsInformation;
using LOS.WebApp.Models.CheckBank;
using LOS.WebApp.Services.ResultEkyc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.AIService
{
    public interface ICheckBankService
    {
        CheckBank.OutPut CheckBank(string access_token, int LoanBriefId, string STK, string BankId);
    }
    public class CheckBankService : ICheckBankService
    {
        protected IConfiguration _baseConfig;
        protected IDictionaryService _dictionaryService;

        public CheckBankService(IConfiguration configuration, IDictionaryService dictionaryService)
        {
            _baseConfig = configuration;
            _dictionaryService = dictionaryService;
        }

        #region Call AI
        public CheckBank.OutPut CheckBank(string access_token, int LoanBriefId, string STK, string BankId)
        {
            var objLogCallApi = new LogCallApi
            {
                ActionCallApi = (int)ActionCallApi.CheckBankAI,
                TokenCallApi = access_token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = LoanBriefId,
                CreatedAt = DateTime.Now
            };
            try
            {
                var url = _baseConfig["AppSettings:CheckBank"] + String.Format(Constants.CHECK_BANK_ENDPOINT, BankId, STK);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                objLogCallApi.LinkCallApi = url;
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                objLogCallApi.ModifyAt = DateTime.Now;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                CheckBank.OutPut lst = Newtonsoft.Json.JsonConvert.DeserializeObject<CheckBank.OutPut>(jsonResponse);
                return lst;
            }
            catch (Exception ex)
            {
                if(objLogCallApi.Id > 0)
                {
                    objLogCallApi.ModifyAt = DateTime.Now;
                    objLogCallApi.Output = ex.ToString();
                    _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                }
               
            }
            return null;
        }
        #endregion

    }
}