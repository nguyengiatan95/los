﻿using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using RestSharp;
using LOS.WebApp.Models;
using LOS.WebApp.Helpers;
using System.Linq;
using System.Net;
using LOS.DAL.EntityFramework;
using LOS.Common.Extensions;
using Newtonsoft.Json;

namespace LOS.WebApp.Services.AIService
{
    public interface ICheckElectricityAndWater
    {
        ResponseCheckElectricityAI CheckBillElectricityAI(string access_token, int loanbriefId, string electricityId);
        ResponseCheckBillWaterAI CheckBillWaterAI(string access_token, int loanbriefId, string waterId, string waterSupplier);
    }
    public class CheckElectricityAndWater : ICheckElectricityAndWater
    {
        protected IConfiguration _baseConfig;
        private ILogReqestAiService _logReqestAiService;
        public CheckElectricityAndWater(IConfiguration configuration, ILogReqestAiService logReqestAiService)
        {
            _baseConfig = configuration;
            _logReqestAiService = logReqestAiService;
        }

        public ResponseCheckElectricityAI CheckBillElectricityAI(string access_token, int loanbriefId, string electricityId)
        {
            var url = _baseConfig["AppSettings:CheckBank"] + String.Format(Constants.CHECK_BILL_ELECTRICITY, electricityId);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            try
            {
                var response = client.Execute<ResponseCheckElectricityAI>(request);
                //Lưu lại log
                var objLogLoanInfoAi = new LogLoanInfoAi()
                {
                    LoanbriefId = loanbriefId,
                    ServiceType = (int)ServiceTypeAI.CheckBillElectricityAI,
                    CreatedAt = DateTime.Now,
                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitHandle,
                    ResponseRequest = response.Content,
                    Request = _baseConfig["AppSettings:CheckBank"] + "/tools/api/v1/check_bill_electricity?bill_id=" + electricityId
                };
                var resultLog = _logReqestAiService.CreateV2(access_token, objLogLoanInfoAi);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = JsonConvert.DeserializeObject<ResponseCheckElectricityAI>(response.Content);
                    var objUpdateLogLoanInfoAi = new LogLoanInfoAi()
                    {
                        IsExcuted = (int)EnumLogLoanInfoAiExcuted.Excuted,
                        Response = JsonConvert.SerializeObject(objData.data),
                        UpdatedAt = DateTime.Now
                    };
                    _logReqestAiService.Update(access_token, objUpdateLogLoanInfoAi, resultLog);
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public ResponseCheckBillWaterAI CheckBillWaterAI(string access_token, int loanbriefId, string waterId, string waterSupplier)
        {
            var url = _baseConfig["AppSettings:CheckBank"] + String.Format(Constants.CHECK_BILL_WATER, waterId, waterSupplier);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            try
            {
                var response = client.Execute<ResponseCheckBillWaterAI>(request);
                //Lưu lại log
                var objLogLoanInfoAi = new LogLoanInfoAi()
                {
                    LoanbriefId = loanbriefId,
                    ServiceType = (int)ServiceTypeAI.CheckBillWaterAI,
                    CreatedAt = DateTime.Now,
                    IsExcuted = (int)EnumLogLoanInfoAiExcuted.WaitHandle,
                    ResponseRequest = response.Content,
                    Request = _baseConfig["AppSettings:CheckBank"] + "tools/api/v1/check_bill_water?bill_id=" + waterId + "&water_supplier=" + waterSupplier
                };
                var resultLog = _logReqestAiService.CreateV2(access_token, objLogLoanInfoAi);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseCheckBillWaterAI>(response.Content);
                    var objUpdateLogLoanInfoAi = new LogLoanInfoAi()
                    {
                        LoanbriefId = loanbriefId,
                        IsExcuted = (int)EnumLogLoanInfoAiExcuted.Excuted,
                        Response = JsonConvert.SerializeObject(objData.data),
                        UpdatedAt = DateTime.Now
                    };
                    _logReqestAiService.Update(access_token, objUpdateLogLoanInfoAi, resultLog);
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
