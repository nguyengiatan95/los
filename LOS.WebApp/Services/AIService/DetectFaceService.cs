﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IDetectFace
    {
        FaceSearchOutput FaceSearch(string access_token, string urlImage, int loanCreditId);
        LoansResult GetListLoans(string access_token, string Phone, int loanCreditId);
        CustomerInfo GetCustomerInfo(string access_token, string Phone, int loanCreditId);
    }
    public class DetectFaceService: IDetectFace
    {
        protected IConfiguration _baseConfig;
        protected IAuthenticationAI _authenService;
        protected IDictionaryService _dictionaryService;
        public DetectFaceService(IConfiguration configuration, IAuthenticationAI authenService, IDictionaryService dictionaryService)
        {
            _baseConfig = configuration;
            _authenService = authenService;
            _dictionaryService = dictionaryService;
        }
        private FaceSearchOutput api_face_search(string access_token, string urlImage, int loanCreditId)
        {
            try
            {
                var url = _baseConfig["AppSettings:SearchFace"] + Constants.FaceSearch;
                var client = new RestClient(url);
                var objData = new ImageSearchInput()
                {
                    image = urlImage
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", access_token);
                request.AddHeader("cache-control", "no-cache");
               // request.AddParameter("image", urlImage);
               request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);

                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.DetectFace,
                    LinkCallApi = url,
                    TokenCallApi = access_token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanCreditId,
                    Input = jsonInput
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                FaceSearchOutput lst = Newtonsoft.Json.JsonConvert.DeserializeObject<FaceSearchOutput>(jsonResponse);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private LoansResult api_get_loans(string access_token, string Phone, int loanCreditId)
        {
            try
            {
                var url = _baseConfig["AppSettings:SearchLoan"] + Constants.GetLoanByPhone + Phone;
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", access_token);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.GetLoanByPhone,
                    LinkCallApi = url,
                    TokenCallApi = access_token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanCreditId,
                    Input = string.Format("Phone: {0} - Token: {1}", Phone, access_token)
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                LoanInfoResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<LoanInfoResult>(jsonResponse);
                if (result != null && result.loansResult != null && (result.loansResult.disbursementLoans != null || result.loansResult.notDisbursementLoans != null || result.loansResult.loansVay1h != null))
                    return result.loansResult;
            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }
        private CustomerInfo api_get_customer(string access_token, string Phone, int loanCreditId)
        {
            try
            {
                var url = _baseConfig["AppSettings:SearchLoan"] + Constants.GetCustomerByPhone + Phone;
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", access_token);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.GetCustomerByPhone,
                    LinkCallApi = url,
                    TokenCallApi = access_token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanCreditId,
                    Input = string.Format("Phone: {0} - Token: {1}", Phone, access_token)
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                CustomerInfoResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<CustomerInfoResult>(jsonResponse);
                if (result != null && result.customerResult != null && result.customerResult.customerCredit != null)
                    return result.customerResult.customerCredit[0];
            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }
        public FaceSearchOutput FaceSearch(string access_token,string urlImage, int loanCreditId)
        {
            var tokenAI = _authenService.GetToken(Constants.APP_ID_SEARCH, Constants.APP_KEY_SEARCH, access_token);
            if (!string.IsNullOrEmpty(tokenAI))
                return api_face_search(tokenAI, urlImage,loanCreditId);
            return null;
        }
        public LoansResult GetListLoans(string access_token, string Phone, int loanCreditId)
        {
            var tokenAI = _authenService.GetToken(Constants.APP_ID_SEARCH, Constants.APP_KEY_SEARCH, access_token);
            if (!string.IsNullOrEmpty(tokenAI))
                return api_get_loans(tokenAI, Phone, loanCreditId);
            return null;
        }
        public CustomerInfo GetCustomerInfo(string access_token, string Phone, int loanCreditId)
        {
            var tokenAI = _authenService.GetToken(Constants.APP_ID_SEARCH, Constants.APP_KEY_SEARCH, access_token);
            if (!string.IsNullOrEmpty(tokenAI))
                return api_get_customer(tokenAI, Phone, loanCreditId);
            return null;
        }

    }
}
