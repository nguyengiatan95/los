﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.AISuggestionsInformation;
using LOS.WebApp.Services.ResultEkyc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static LOS.WebApp.Models.Ekyc.Ekyc;

namespace LOS.WebApp.Services.AIService
{
    public interface IEkycService
    {
        EkycOutput GetInfoCheckEkyc(string access_token, EkycInput input, int loanBriefId, bool reGetToken = false, int retry = 0); 
    }
    public class EkycService : IEkycService
    {
        protected IConfiguration _baseConfig;
        protected IAuthenticationAI _authenService;
        protected IDictionaryService _dictionaryService;
        private IResultEkycService _resultEkyc;
        public EkycService(IConfiguration configuration, IAuthenticationAI authenService, IDictionaryService dictionaryService, IResultEkycService resultEkyc)
        {
            _baseConfig = configuration;
            _authenService = authenService;
            _dictionaryService = dictionaryService;
            _resultEkyc = resultEkyc;
        }

        #region Call AI
        private EkycOutput api_GetInfoCheckEkyc(string access_token,string token, EkycInput input, int loanBriefId, int retry = 0)
        {
            try
            {               
                    var url = _baseConfig["AppSettings:AIAuthentication"] + Constants.GET_INFO_CHECK_EKYC;
                    var client = new RestClient(url);
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("cache-control", "no-cache");
                    request.AddHeader("Authorization", token);
                    var body = JsonConvert.SerializeObject(input);
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    var logLoanInfoAi = new LogLoanInfoAi()
                    {
                        ServiceType = ServiceTypeAI.Ekyc.GetHashCode(),
                        LoanbriefId = loanBriefId,
                        Request = body,
                        Token = token,
                        Url = url,
                        Retry = retry,
                        CreatedAt = DateTime.Now
                    };
                    logLoanInfoAi.Id = _dictionaryService.AddLogLoanInfoAi(access_token, logLoanInfoAi);
                    if (logLoanInfoAi.Id > 0)
                    {
                        IRestResponse response = client.Execute(request);
                        var jsonResponse = response.Content;
                        logLoanInfoAi.Status = 1;//gọi thành công
                        logLoanInfoAi.ResponseRequest = jsonResponse;
                        logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode();
                        _dictionaryService.UpdateLogLoanInfoAi(access_token, logLoanInfoAi);
                        EkycOutput result = Newtonsoft.Json.JsonConvert.DeserializeObject<EkycOutput>(jsonResponse);
                        if (result.StatusCode == 200)
                        {
                            //Lưu vào bảng ResultEkyc
                            var objEkyc = new DAL.EntityFramework.ResultEkyc()
                            {
                                LoanbriefId = loanBriefId,
                                CreatedAt = DateTime.Now,
                                AddressValue = result.NationalId.Address.Value,
                                AddressCheck = result.NationalId.Address.Check,
                                IdValue = result.NationalId.Id.Value,
                                IdCheck = result.NationalId.Id.Check,
                                FullNameValue = result.NationalId.Fullname.Value,
                                FullNameCheck = result.NationalId.Fullname.Check,
                                BirthdayValue = result.NationalId.Birthday.Value,
                                BirthdayCheck = result.NationalId.Birthday.Check,
                                ExpiryValue = result.NationalId.Expiry.Value,
                                ExpiryCheck = result.NationalId.Expiry.Check,
                                GenderValue = result.NationalId.Gender.Value,
                                GenderCheck = result.NationalId.Gender.Check,
                                EthnicityValue = result.NationalId.Ethnicity.Value,
                                EthnicityCheck = result.NationalId.Ethnicity.Check,
                                IssueByValue = result.NationalId.IssueBy.Value,
                                IssueByCheck = result.NationalId.IssueBy.Check,
                                IssueDateValue = result.NationalId.IssueDate.Value,
                                IssueDateCheck = result.NationalId.IssueDate.Check,
                                ReligionValue = result.NationalId.Religion.Value,
                                ReligionCheck = result.NationalId.Religion.Check,
                                FaceCompareCode = result.FraudCheck.FaceCompare.Code,
                                FaceCompareMessage = result.FraudCheck.FaceCompare.Message,
                                FaceQueryCode = result.FraudCheck.FaceQuery.Code,
                                FaceQueryMessage = result.FraudCheck.FaceQuery.Message
                            };
                            _resultEkyc.Add(access_token, objEkyc);
                        }
                        return result;
                    }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public EkycOutput GetInfoCheckEkyc(string access_token, EkycInput input, int loanBriefId, bool reGetToken = false, int retry = 0)
        {
            var token = _authenService.GetTokenRetry(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token, reGetToken);
            if (!string.IsNullOrEmpty(token))
            {
                var result = api_GetInfoCheckEkyc(access_token,token, input, loanBriefId, retry);
                //nếu token hết hạn thì gọi lại
                if (result.StatusCode == 401 && retry < 3)
                    GetInfoCheckEkyc(access_token, input,loanBriefId, true, retry++);
                return result;
            }
            return null;
        }
        #endregion

    }
}