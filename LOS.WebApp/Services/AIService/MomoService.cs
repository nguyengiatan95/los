﻿using LOS.Common.Extensions;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.DAL.Models.Response.CheckMomo;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.AIService
{
    public interface IMomoService
    {
        Task<CheckMomoResponse.Output> CheckMomo(string access_token, int loanBriefId, string nationalCard);
    }
    public class MomoService : BaseServices, IMomoService
    {
        protected IConfiguration _baseConfig;
        protected IDictionaryService _dictionaryService;
        public MomoService(IConfiguration configuration, IDictionaryService dictionaryService, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
            _dictionaryService = dictionaryService;
        }

        public async Task<CheckMomoResponse.Output> CheckMomo(string access_token, int loanBriefId, string nationalCard)
        {
            try
            {
                var url = _baseConfig["AppSettings:AIAuthentication"] + String.Format(Constants.CHECK_LOAN_MOMO, nationalCard);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Timeout = 20000;
                request.AddHeader("Content-Type", "application/json");

                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    ServiceType = ServiceTypeAI.CheckLoanMomo.GetHashCode(),
                    LoanbriefId = loanBriefId,
                    Token = access_token,
                    Url = url,
                    Request = nationalCard,
                    CreatedAt = DateTime.Now
                };
                logLoanInfoAi.Id = _dictionaryService.AddLogLoanInfoAi(access_token, logLoanInfoAi);
                if (logLoanInfoAi.Id > 0)
                {
                    IRestResponse response = client.Execute(request);
                    var jsonResponse = response.Content;
                    logLoanInfoAi.Status = 1;//gọi thành công
                    logLoanInfoAi.ResponseRequest = jsonResponse;
                    logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode();
                    _dictionaryService.UpdateLogLoanInfoAi(access_token, logLoanInfoAi);
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<CheckMomoResponse.Output>(jsonResponse);
                    if (result != null && result.Status == 0)
                    {
                        if(result.Data.Bills != null && result.Data.Bills.Count > 0)
                        {
                            foreach (var item in result.Data.Bills)
                            {
                                //Lưu log check Momo
                                var objLogCheck = new LogLoanInfoPartner
                                {
                                    LoanbriefId = loanBriefId,
                                    BillId = item.BillId,
                                    ServiceName = item.ServiceName,
                                    TotalAmount = item.TotalAmount,
                                    PartnerName = item.PartnerName,
                                    PartnerPhone = item.PartnerPhone,
                                    PartnerEmail = item.PartnerEmail,
                                    PartnerPersonalId = item.PartnerPersonalId,
                                    ExpiredDate = item.ExpiredDate,
                                    Status = item.Status,
                                    CreatedDate = DateTime.Now
                                };
                                _dictionaryService.AddLogInfoCheckMomo(access_token, objLogCheck);
                            }
                        }                       
                        return result;
                    }
                }

            }
            catch (Exception ex)
            {


            }
            return null;
        }
    }
}
