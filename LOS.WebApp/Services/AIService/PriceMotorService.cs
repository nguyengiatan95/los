﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.AISuggestionsInformation;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IPriceMotor
    {
        PriceMotorRes GetMotoPrice(PriceMotorReq req);
        PriceMotorLogRes PushLogPriceAIMoto(PriceMotorLogReq req, string access_token);
    }
    public class PriceMotorService : IPriceMotor
    {
        protected IConfiguration _baseConfig;
        protected IAuthenticationAI _authenService;
        protected IDictionaryService _dictionaryService;
        public PriceMotorService(IConfiguration configuration, IAuthenticationAI authenService, IDictionaryService dictionaryService)
        {
            _baseConfig = configuration;
            _authenService = authenService;
            _dictionaryService = dictionaryService;
        }
        public PriceMotorRes GetMotoPrice(PriceMotorReq req)
        {
            var body = JsonConvert.SerializeObject(req);
            var token = _authenService.GetToken(Constants.APP_ID_TIMA_IT3, Constants.APP_KEY_TIMA_IT3, req.access_token);
            var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.GET_MOTO_PRICE;
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", token);
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            request.Timeout = 15000;
            //lưu log
            var objLogCallApi = new LogCallApi
            {
                ActionCallApi = (int)ActionCallApi.GetMotoPrice,
                LinkCallApi = url,
                TokenCallApi = token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = req.LoanBriefId,
                Input = body,
                CreatedAt = DateTime.Now
            };
            try
            {
                
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(req.access_token, objLogCallApi);

                var response = client.Execute<PriceMotorRes>(request);

                // cập nhập lại log
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(req.access_token, objLogCallApi);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<PriceMotorRes>(response.Content);
                    return objData;
                }
            }
            catch (Exception ex)
            {
                var jsonResponse = ex.ToString();
                objLogCallApi.Status = (int)StatusCallApi.Error;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(req.access_token, objLogCallApi);
            }
            return null;
        }
        #region Call AI
        public PriceMotorLogRes PushLogPriceAIMoto(PriceMotorLogReq req, string access_token)
        {
            var body = JsonConvert.SerializeObject(req);
            var token = _authenService.GetToken(Constants.APP_ID_TIMA_IT3, Constants.APP_KEY_TIMA_IT3, access_token);
            var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.SAVE_LOG_PRICE_MOTO_AI;
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", token);
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                //lưu log
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.PushLogPriceAIMoto,
                    LinkCallApi = url,
                    TokenCallApi = token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = req.loan_credit_id,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);

                var response = client.Execute<PriceMotorLogRes>(request);

                // cập nhập lại log
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<PriceMotorLogRes>(response.Content);
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        #endregion
    }
}