﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.Models.Response;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.AIService
{
    public interface ISecuredTransaction
    {
        SercuredTransactionRes RegisterSecuredTransaction(SercuredTransactionReq req, string access_token, int loanbriefId);
        SercuredTransactionRes CheckStatusSecuredTransaction(string registration_number, string access_token, int loanbriefId);
    }
    public class SecuredTransactionService: ISecuredTransaction
    {
        protected IConfiguration _baseConfig;
        protected IAuthenticationAI _authenService;
        protected IDictionaryService _dictionaryService;
        public SecuredTransactionService(IConfiguration configuration, IAuthenticationAI authenService, IDictionaryService dictionaryService)
        {
            _baseConfig = configuration;
            _authenService = authenService;
            _dictionaryService = dictionaryService;
        }

        public SercuredTransactionRes CheckStatusSecuredTransaction(string notice_number, string access_token, int loanbriefId)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token);
            var url = string.Format(_baseConfig["AppSettings:AITrackDevice"] + Constants.CHECK_STATUS_SECURED_TRANSACTION_ENDPOINT, notice_number);
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", token);
            try
            {
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    ServiceType = ServiceTypeAI.CheckStatusSecuredTransaction.GetHashCode(),
                    LoanbriefId = loanbriefId,
                    Token = access_token,
                    Url = url,
                    CreatedAt = DateTime.Now,
                    IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode()
                };
                logLoanInfoAi.Id = _dictionaryService.AddLogLoanInfoAi(access_token, logLoanInfoAi);
                var response = client.Execute<SercuredTransactionRes>(request);
                // cập nhập lại log
                var jsonResponse = response.Content;
                logLoanInfoAi.Status = 1;//gọi thành công
                logLoanInfoAi.ResponseRequest = jsonResponse;
                logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode();
                _dictionaryService.UpdateLogLoanInfoAi(access_token, logLoanInfoAi);
                var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<SercuredTransactionRes>(response.Content);
                return objData;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public SercuredTransactionRes RegisterSecuredTransaction(SercuredTransactionReq req, string access_token, int loanbriefId)
        {
            var body = JsonConvert.SerializeObject(req);
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token);
            var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.REGISTER_SECURED_TRANSACTION_ENDPOINT;
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", token);
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                var logLoanInfoAi = new LogLoanInfoAi()
                {
                    ServiceType = ServiceTypeAI.RegisterSecuredTransaction.GetHashCode(),
                    LoanbriefId = loanbriefId,
                    Token = token,
                    Url = url,
                    CreatedAt = DateTime.Now,
                    Request = body,
                    IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode()
                };
                logLoanInfoAi.Id = _dictionaryService.AddLogLoanInfoAi(access_token, logLoanInfoAi);
                var response = client.Execute<SercuredTransactionRes>(request);
                // cập nhập lại log
                var jsonResponse = response.Content;
                logLoanInfoAi.Status = 1;//gọi thành công
                logLoanInfoAi.ResponseRequest = jsonResponse;
                logLoanInfoAi.IsExcuted = EnumLogLoanInfoAiExcuted.Excuted.GetHashCode();
                _dictionaryService.UpdateLogLoanInfoAi(access_token, logLoanInfoAi);
                var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<SercuredTransactionRes>(response.Content);
                return objData;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
