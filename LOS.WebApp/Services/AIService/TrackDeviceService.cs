﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface ITrackDeviceService
    {
        List<LocationItem> GetLocationDevice(string access_token, string maHD, string deviceId, long from, long to, int loanbriefId, ref string msg);
        List<LocationItem> GetStopPoint(string access_token, string maHD, string deviceId, long from, long to, int loanbriefId, ref string msg);
        OutputOpenContract OpenContract(string access_token, string maHD, string deviceId, string shopid, int productId, int loanbriefId, string contractType = "thuong");
        bool CloseContract(string access_token, string maHD, string deviceId, int loanbriefId);
        OutputCheckImei CheckImei(string access_token, string deviceId, int loanbriefId);
        bool UpdateContract(string access_token, string deviceId, string newContractId, string oldContractId, int loanbriefId);
        List<FirstLocationData> GetFirstLocation(string access_token, string deviceId, int loanbriefId, ref string msg);
        DataReportAddress GetReportAddress(string access_token, string home, string office, string other, string contractId, string imei, long from, long to, int loanbriefId, int radius = 500);
        DataReportAddress GetReportLoaction(string access_token, InputLocationItem home, InputLocationItem office, InputLocationItem other, string contractId, string imei, long from, long to, int loanbriefId, int radius = 500);
        bool ReActiveDevice(string access_token, string maHD, string deviceId, int loanbriefId);
        string GetAddressByLatLng(decimal lat, decimal lng);
    }

    public class TrackDeviceService : ITrackDeviceService
    {
        protected IConfiguration _baseConfig;
        protected IAuthenticationAI _authenService;
        protected IDictionaryService _dictionaryService;
        public TrackDeviceService(IConfiguration configuration, IAuthenticationAI authenService, IDictionaryService dictionaryService)
        {
            _baseConfig = configuration;
            _authenService = authenService;
            _dictionaryService = dictionaryService;
        }

        #region private fun call to AI       
        private List<LocationItem> api_gps_interval(string access_token, string maHD, string deviceId, long from, long to, string token, int loanbriefId, ref string msg)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.gps_interval;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new InputGpsInterval()
                {
                    contractid = maHD,
                    imei = deviceId,
                    time_received1 = from,
                    time_received2 = to
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.LocationDevice,
                    LinkCallApi = url,
                    TokenCallApi = access_token,
                     Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                OutputGpsInterval result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputGpsInterval>(jsonResponse);
                if (result != null && result.Data != null)
                {
                    msg = result.Message;
                    return result.Data;
                }

            }
            catch (Exception ex)
            {
            }
            return null;
        }

        private List<LocationItem> api_gps_stop_point(string access_token, string maHD, string deviceId, long from, long to, string token, int loanbriefId, ref string msg)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.gps_stop_point;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new InputGpsInterval()
                {
                    contractid = maHD,
                    imei = deviceId,
                    time_received1 = from,
                    time_received2 = to
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.StopPoint,
                    LinkCallApi = url,
                     Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = access_token,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                OutputGpsInterval result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputGpsInterval>(jsonResponse);
                if (result != null && result.Data != null)
                {
                    msg = result.Message;
                    return result.Data;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        private OutputOpenContract api_open_contract(string access_token, string maHD, string deviceId, string shopid, int productId, string token, int loanbriefId, string contractType)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.open_contract;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new InputOpenContract()
                {
                    contractid = maHD,
                    imei = deviceId,
                    shopid = shopid,
                    vehicle_type = (productId == EnumProductCredit.CamotoCreditType.GetHashCode() || productId == EnumProductCredit.OtoCreditType_CC.GetHashCode()
                    || productId == EnumProductCredit.OtoCreditType_KCC.GetHashCode() || productId == EnumProductCredit.LoanFastCar_CC.GetHashCode()) ? "oto" : "xe may",
                    contract_type = contractType
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);

                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.OpenContract,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = access_token,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput,
                    CreatedAt = DateTime.Now
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                OutputOpenContract result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputOpenContract>(jsonResponse);
                return result;// (result != null && result.StatusCode == 200);
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        private OutputCheckImei api_check_imei(string access_token, string deviceId, string token, int loanbriefId)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.check_imei + deviceId;
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.CheckImei,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = access_token,
                    LoanBriefId = loanbriefId,
                    Input = string.Format("{0}-{1}-{2}", deviceId, token, loanbriefId)
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                OutputCheckImei result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputCheckImei>(jsonResponse);
                return result;// (result != null && result.StatusCode == 200 && result.Data != null && result.Data.device_status == EnumDeviceStatus.Actived.GetHashCode());
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        private bool api_close_contract(string access_token, string maHD, string deviceId, string token, int loanbriefId)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.close_contract;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new InputCloseContract()
                {
                    contractid = maHD,
                    imei = deviceId
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.CloseContract,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = access_token,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                OutputCloseContract result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputCloseContract>(jsonResponse);
                return (result != null && result.StatusCode == 200);
            }
            catch (Exception ex)
            {

            }

            return false;
        }

        private bool api_update_contractid_by_imei(string access_token, string deviceId, string newContractId, string oldContractId, string token, int loanbriefId)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.update_contract;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new InputUpdateContractId()
                {
                    imei = deviceId,
                    newContractId = newContractId,
                    oldContractId = oldContractId
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.UpdateContractId,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = access_token,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                OutputUpdateContract result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputUpdateContract>(jsonResponse);
                return (result != null && result.StatusCode == 200);
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        private List<FirstLocationData> api_gps_first_location(string access_token, string deviceId, string token, int loanbriefId, ref string msg)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.get_first_location;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new InputFirstLocation();
                objData.imeis = new List<string>();
                objData.imeis.Add(deviceId);

                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.GetFirstLocation,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = access_token,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                OutputFirstLocation result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputFirstLocation>(jsonResponse);
                if (result != null && result.Data != null && result.Data.Count > 0)
                {
                    msg = result.Message;
                    return result.Data;
                }

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        private DataReportAddress api_gps_report_address(string access_token, string home, string office, string other, string contractId, int radius, string imei, long from, long to, string token, int loanbriefId)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.get_report_address;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new InputGpsReport();
                objData.address = new AddressItem();
                objData.address.home = home;
                objData.address.office = office;
                objData.address.other = other;
                objData.contractid = contractId;
                objData.imei = imei;
                objData.radius = radius;
                objData.time_received1 = from;
                objData.time_received2 = to;

                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.GetReportAddress,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = access_token,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                OutputGpsReport result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputGpsReport>(jsonResponse);
                if (result != null && result.data != null)
                    return result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        private DataReportAddress api_gps_report_location(string access_token, InputLocationItem home, InputLocationItem office, InputLocationItem other, string contractId, int radius, string imei, long from, long to, string token, int loanbriefId)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.get_report_location;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new InputGpsReportLocation();
                objData.location = new LocationItemReport();
                objData.location.home = home;
                objData.location.office = office;
                objData.location.other = other;
                objData.contractid = contractId;
                objData.imei = imei;
                objData.radius = radius;
                objData.time_received1 = from;
                objData.time_received2 = to;

                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.GetReportLocation,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = access_token,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                OutputGpsReport result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputGpsReport>(jsonResponse);
                if (result != null && result.data != null)
                    return result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        private bool api_reactive_imei(string access_token, string maHD, string deviceId, string token, int loanbriefId)
        {
            try
            {
                var url = _baseConfig["AppSettings:AITrackDevice"] + Constants.re_active_imei;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", token);
                var objData = new 
                {
                    contractId = maHD,
                    imei = deviceId
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.ReActiveDevice,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = access_token,
                    LoanBriefId = loanbriefId,
                    Input = jsonInput,
                    CreatedAt = DateTime.Now
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = jsonResponse;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                ReActiveDeviceRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<ReActiveDeviceRes>(jsonResponse);
                return (result != null && result.StatusCode == 200);
            }
            catch (Exception ex)
            {

            }

            return false;
        }
        #endregion
        public List<LocationItem> GetLocationDevice(string access_token, string maHD, string deviceId, long from, long to, int loanbriefId, ref string msg)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token);
            if (!string.IsNullOrEmpty(token))
                return api_gps_interval(access_token, maHD, deviceId, from, to, token, loanbriefId, ref msg);
            return null;
        }

        public List<LocationItem> GetStopPoint(string access_token, string maHD, string deviceId, long from, long to, int loanbriefId, ref string msg)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token);
            if (!string.IsNullOrEmpty(token))
                return api_gps_stop_point(access_token, maHD, deviceId, from, to, token, loanbriefId, ref msg);
            return null;
        }

        public OutputOpenContract OpenContract(string access_token, string maHD, string deviceId, string shopid, int productId, int loanbriefId, string contractType)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token);
            if (!string.IsNullOrEmpty(token))
                return api_open_contract(access_token, maHD, deviceId, shopid, productId, token, loanbriefId, contractType);
            return null;
        }

        public bool CloseContract(string access_token, string maHD, string deviceId, int loanbriefId)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token);
            if (!string.IsNullOrEmpty(token))
                return api_close_contract(access_token, maHD, deviceId, token, loanbriefId);
            return false;
        }

        public OutputCheckImei CheckImei(string access_token, string deviceId, int loanbriefId)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token);
            if (!string.IsNullOrEmpty(token))
                return api_check_imei(access_token, deviceId, token, loanbriefId);
            return null;
        }

        public bool UpdateContract(string access_token, string deviceId, string newContractId, string oldContractId, int loanbriefId)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token);
            if (!string.IsNullOrEmpty(token))
                return api_update_contractid_by_imei(access_token, deviceId, newContractId, oldContractId, token, loanbriefId);
            return false;
        }

        public List<FirstLocationData> GetFirstLocation(string access_token, string deviceId, int loanbriefId, ref string msg)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token);
            if (!string.IsNullOrEmpty(token))
                return api_gps_first_location(access_token, deviceId, token, loanbriefId, ref msg);
            return null;
        }

        public DataReportAddress GetReportAddress(string access_token, string home, string office, string other, string contractId, string imei, long from, long to, int loanbriefId, int radius = 500)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token);
            if (!string.IsNullOrEmpty(token))
                return api_gps_report_address(access_token, home, office, other, contractId, radius, imei, from, to, token, loanbriefId);
            return null;
        }

        public DataReportAddress GetReportLoaction(string access_token, InputLocationItem home, InputLocationItem office, InputLocationItem other, string contractId, string imei, long from, long to, int loanbriefId, int radius = 500)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token);
            if (!string.IsNullOrEmpty(token))
                return api_gps_report_location(access_token, home, office, other, contractId, radius, imei, from, to, token, loanbriefId);
            return null;
        }

        public bool ReActiveDevice(string access_token, string maHD, string deviceId, int loanbriefId)
        {
            var token = _authenService.GetToken(Constants.APP_ID_VAY_SIEU_NHANH, Constants.APP_KEY_VAY_SIEU_NHANH, access_token);
            if (!string.IsNullOrEmpty(token))
                return api_reactive_imei(access_token, maHD, deviceId, token, loanbriefId);
            return false;
        }

        #region Get Address from google by lat and lng
        private GeocodingResult GetAddress(decimal lat, decimal lng)
        {
            var APIKEY = _baseConfig["AppSettings:KEY_GOOGLE_MAP"];
            var client = new RestClient("https://maps.googleapis.com");
            var request = new RestRequest("maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=" + APIKEY, Method.GET);
            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var strJson = response.Content;
                    var objResultGet = JsonConvert.DeserializeObject<GeocodingResult>(strJson);
                    return objResultGet;
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string GetAddressByLatLng(decimal lat, decimal lng)
        {
            try
            {
                var result = GetAddress(lat, lng);
                if (result != null && result.results != null && result.results.Count > 0)
                    return string.IsNullOrEmpty(result.results[0].formatted_address) ? "Không xác định" : result.results[0].formatted_address;
            }
            catch (Exception e)
            {
                return null;
            }

            return "Không xác định";
        }
        #endregion
    }
}
