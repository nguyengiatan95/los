﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace LOS.WebApp.Services
{
    public class ApproveService : BaseServices, IApproveService
    {
        public ApproveService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public List<LoanBriefDetail> Search(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefDetail> data = new List<LoanBriefDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>>(Constants.SEARCH_APPROVE_ENDPOINT + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Approved/Search Exception");
                return null;
            }
            return data;
        }
        
        public List<LoanBriefDetail> SearchEmp(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefDetail> data = new List<LoanBriefDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>>(Constants.SEARCH_APPROVE_EMP_ENDPOINT + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Approve/SearchEmp Exception");
                return null;
            }
            return data;
        }

        public GetDisbursementAfter DisbursementAfter(string access_token, string param, ref int recordTotals)
        {
            var data = new GetDisbursementAfter();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, GetDisbursementAfter>>(Constants.SEARCH_DISBURSEMENAFTER_ENDPOINT + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.lstLoan.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords; 
                    }
                    data = response.Result.data;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Approved/DisbursementAfter Exception");
                return null;
            }
            return data;
        }

        public DefaultResponse<object> ConfirmLoanMoney(string access_token, LoanBrief entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.CONFIRM_LOAN_MONEY_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL).Result;
                return response;
            }
            catch (Exception ex)
            {
                DefaultResponse<object> def = new DefaultResponse<object>();
                def.meta = new Meta(500, "internal server error");
                return def;
            }
        }

    }
}
