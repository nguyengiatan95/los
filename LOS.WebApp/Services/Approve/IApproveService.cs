﻿using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IApproveService
    {
        List<LoanBriefDetail> Search(string access_token, string param, ref int recordTotals);
        List<LoanBriefDetail> SearchEmp(string access_token, string param, ref int recordTotals);
        GetDisbursementAfter DisbursementAfter(string access_token, string param, ref int recordTotals);

        DefaultResponse<object> ConfirmLoanMoney(string access_token, LoanBrief entity);
    }
}
