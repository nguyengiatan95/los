﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace LOS.WebApp.Services
{
    public class BODService : BaseServices, IBODService
    {
        public BODService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public List<LoanBriefSearchDetail> BusinessManagerSearch(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefSearchDetail> data = new List<LoanBriefSearchDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefSearchDetail>>>(Constants.BUSINESSMANAGER_SEARCH + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "BOD/Search Exception");
                return null;
            }
            return data;
        }

        public List<LoanBriefDetail> InternalControlSearch(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefDetail> data = new List<LoanBriefDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>>(Constants.LOANBRIEF_GET_DATA_INTERNALCONTROL_ENDPOINT + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/InternalControlSearch Exception");
                return null;
            }
            return data;
        }

        public List<LoanBriefSearchDetail> Search(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefSearchDetail> data = new List<LoanBriefSearchDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefSearchDetail>>>(Constants.BOD_SEARCH + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "BOD/Search Exception");
                return null;
            }
            return data;
        }

    }
}
