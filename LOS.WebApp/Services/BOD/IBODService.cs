﻿using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IBODService
    {
        List<LoanBriefSearchDetail> Search(string access_token, string param, ref int recordTotals);
        List<LoanBriefSearchDetail> BusinessManagerSearch(string access_token, string param, ref int recordTotals);
        List<LoanBriefDetail> InternalControlSearch(string access_token, string param, ref int recordTotals);
    }
}
