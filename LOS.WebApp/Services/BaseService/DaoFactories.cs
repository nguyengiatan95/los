﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.BaseService
{
    public class DaoFactories
    {
        public static IDaoFactory GetFactory(string baseUrl)
        {
            return new DaoFactory(baseUrl);
        }
    }
}
