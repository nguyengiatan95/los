﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Net.Client;
using GrpcUserServices;

namespace LOS.WebApp.Services.BaseService
{
    public class DaoFactory : IDaoFactory
    {
        private string _baseUrl;
        public DaoFactory(string baseUrl)
        {
            _baseUrl = baseUrl;
        }
        Sercurity.SercurityClient IDaoFactory.SercuritySercives
        {
            get
            {
                var channel = GrpcChannel.ForAddress(_baseUrl);
                return new GrpcUserServices.Sercurity.SercurityClient(channel);
            }
        }

    }
}
