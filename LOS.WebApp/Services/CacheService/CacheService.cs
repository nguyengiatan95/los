﻿using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public class CacheService : ICacheService
    {
        private readonly IDatabase _cacheRedis;
        public CacheService(IDatabase cacheRedis)
        {
            _cacheRedis = cacheRedis;
        }
        public void InitCacheRedis()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
                .AddJsonFile("appsettings.json", true)
                .AddJsonFile($"appsettings.{environmentName}.json", true);
            var configuration = builder.Build();
            var serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
            var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
            var _dictionaryService = new DictionaryService(configuration, httpClientFactory);

            var listProvince = _dictionaryService.GetProvince("");
            _cacheRedis.StringSet(Constants.REDIS_KEY_PROVINCE_CACHE, JsonConvert.SerializeObject(listProvince));

            var listJob = _dictionaryService.GetJobV2("");
            _cacheRedis.StringSet(Constants.REDIS_KEY_JOB_CACHE, JsonConvert.SerializeObject(listJob));

            var listRelativeFamilys = _dictionaryService.GetRelativeFamily("");
            _cacheRedis.StringSet(Constants.REDIS_KEY_RELATIONSHIP_CACHE, JsonConvert.SerializeObject(listRelativeFamilys));

            var listBrandProduct = _dictionaryService.GetBrandProduct("");
            _cacheRedis.StringSet(Constants.REDIS_KEY_BRAND_PRODUCT_CACHE, JsonConvert.SerializeObject(listBrandProduct));

            var product = _dictionaryService.GetLoanProduct("");
            if (product != null && product.Count > 0)
            {
                product= product.Where(x => x.Status == 1).ToList();
                _cacheRedis.StringSet(Constants.REDIS_KEY_PRODUCT_CACHE, JsonConvert.SerializeObject(listBrandProduct));
            }

            var listPlatformType = _dictionaryService.GetPlatformType("");
            _cacheRedis.StringSet(Constants.REDIS_KEY_PLATFORM_CACHE, JsonConvert.SerializeObject(listPlatformType));

            var listBank = _dictionaryService.GetBank("");
            _cacheRedis.StringSet(Constants.REDIS_KEY_BANK_CACHE, JsonConvert.SerializeObject(listBank));

            var listOwnerShip = _dictionaryService.GetTypeOwnerShip("");
            _cacheRedis.StringSet(Constants.REDIS_KEY_OWNERSHIP_CACHE, JsonConvert.SerializeObject(listOwnerShip));
        }
    }
}
