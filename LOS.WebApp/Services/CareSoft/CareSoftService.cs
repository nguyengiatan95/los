﻿using JWT;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Serilog;
using LOS.DAL.EntityFramework;
using LOS.Common.Extensions;

namespace LOS.WebApp.Services
{
    public class CareSoftService : ICareSoftService
    {
        private readonly ITokenSmartDailerService _tokenSmartDailerService;
        private readonly IConfiguration _configuration;
        private readonly IDictionaryService _dictionaryService;
        public CareSoftService(ITokenSmartDailerService tokenSmartDailerService, IConfiguration configuration, IDictionaryService dictionaryService)
        {
            _tokenSmartDailerService = tokenSmartDailerService;
            this._configuration = configuration;
            _dictionaryService = dictionaryService;
        }
        public bool LoginCareSoft(string access_token, int UserId, string IPPhone, ref string tokenUser)
        {
            try
            {
                var token = tokenUser = GetToken(access_token, UserId, IPPhone);
                if (string.IsNullOrEmpty(token))
                    return false;
                return CallLoginCaresoft(token);
            }
            catch (Exception ex)
            {
            }
            return false;
        }
        private string GetToken(string access_token, int UserId, string IPPhone)
        {
            var token = string.Empty;
            try
            {

                //lấy token cũ
                var tokenItem = _tokenSmartDailerService.GetLastToken(access_token, UserId);
                if (tokenItem == null || tokenItem.DateExpiration <= DateTime.Now)
                {
                    var DateExpiration = DateTime.Now.AddDays(30);
                    var reToken = GenerateTokenCareSoft(ConstantCareSoft.TokenSystem, IPPhone, 30);
                    if (!string.IsNullOrEmpty(reToken))
                    {
                        token = reToken;
                        //insert to db
                        var objToken = new TokenSmartDailerDTO()
                        {
                            Token = reToken,
                            DateExpiration = DateExpiration,
                            UserId = UserId,
                            CreatedAt = DateTime.Now
                        };
                        _tokenSmartDailerService.AddToken(access_token, objToken);
                        //ThreadPool.QueueUserWorkItem(item => _tokenSmartDailerService.AddToken(access_token, objToken));
                    }
                }
                else
                {
                    token = tokenItem.Token;
                }

            }
            catch
            {
            }
            return token;
        }

        public string GenerateTokenCareSoft(string Secret, string ipPhone, int expireDay = 30)
        {
            var expired = DateTime.Now.AddDays(expireDay).ToString("yyyy-MM-dd HH:mm:ss");
            var claims = new[] { new Claim("ipphone", ipPhone), new Claim("expired", expired) };
            var token = new JwtSecurityToken
            (
                claims: claims,
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret)),
                        SecurityAlgorithms.HmacSha256)
            );
            return new JwtSecurityTokenHandler().WriteToken(token);

        }
        public string GenarateLinkClick2Call(string ipphone, string numercall)
        {
            try
            {
                if (string.IsNullOrEmpty(numercall))
                    return "javascript:;";
                var domain = _configuration["AppSettings:DomainAgent"];
                var Secret = _configuration["AppSettings:SecretCaresoft"];
                string strPhone = Regex.Replace(numercall, @"[^\d]", String.Empty);
                if (strPhone.Length > 2)
                {
                    numercall = strPhone.StartsWith("84") ? "0" + strPhone.Substring(2, strPhone.Length - 2) : strPhone;
                    numercall = "0" + Convert.ToInt64(numercall);
                }
                else
                    numercall = string.Empty;
                var token = GenarateJWT(Secret, ipphone, string.Empty, numercall);
                return domain + "/c2call?token=" + token;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public List<RecordItem> GetListRecord(string? phone, DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                var url = ConstantCareSoft.BaseUrl + ConstantCareSoft.get_record;
                url = string.Format(url, 1, phone, fromDate.HasValue ? fromDate.Value.ToString("yyyy-MM-ddTHH:mm:ssZ") : "", toDate.HasValue ? toDate.Value.ToString("yyyy-MM-ddTHH:mm:ssZ") : "", 50);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", "bearer " + ConstantCareSoft.TokenSystem);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                RecordItemRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<RecordItemRes>(jsonResponse);
                if (result.calls != null && result.calls.Count > 0)
                    return result.calls;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        private string GenarateJWT(string Secret, string ipphone, string callout_id, string number)
        {
            var claims = new[] { new Claim("ipphone", ipphone), new Claim("number", System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(number))) };
            if (!string.IsNullOrWhiteSpace(callout_id))
            {
                claims = new[] { new Claim("ipphone", ipphone), new Claim("number", System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(number))), new Claim("callout_id", callout_id) };
            }
            var token = new JwtSecurityToken
                (
                    claims: claims,
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret)),
                            SecurityAlgorithms.HmacSha256)
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private bool CallLoginCaresoft(string token)
        {
            try
            {
                var url = ConstantCareSoft.BaseUrl + ConstantCareSoft.LoginCareSoft;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                var inputData = new LoginCaresoft
                {
                    token = token
                };
                var jsonInput = Newtonsoft.Json.JsonConvert.SerializeObject(inputData);
                request.AddParameter("application/json", jsonInput, ParameterType.RequestBody);
                var response = client.Execute(request);
                var jsonResponse = response.Content;
                OutputLoginCaresoft result = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputLoginCaresoft>(jsonResponse);
                return (result != null && result.code.ToUpper() == "OK" && result.user != null && result.user.accountId > 0);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<RecordItemV2> GetListRecordByList(List<string> phones, DateTime? fromDate, DateTime? toDate)
        {
            //try
            //{
            //    var resultRecord = new List<RecordItemV2>();
            //    if (phones != null && phones.Count > 0)
            //    {
            //        var task = Task.Run(() =>
            //         Parallel.ForEach(phones, item =>
            //         {
            //             var records = GetListRecordV2(item, fromDate, toDate);
            //             if (records != null && records.Count > 0)
            //                 resultRecord.AddRange(records);
            //         })
            //        );
            //        Task.WaitAll(task);

            //    }
            //    return resultRecord;
            //}
            //catch (Exception ex)
            //{

            //}
            return null;
        }

        public List<RecordItemV2> GetListRecordV2(List<string> phone, DateTime? fromDate, DateTime? toDate, int loanbriefId = 0, string access_token ="")
        {
            try
            {
                var phones = string.Join(",", phone);
                var url = _configuration["AppSettings:ERP"] + ConstantCareSoft.get_recordV2;
                url = string.Format(url, phones, fromDate.Value.ToString("yyyy-MM-ddTHH:mm:ssZ"), toDate.Value.ToString("yyyy-MM-ddTHH:mm:ssZ"), 1, 50);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Timeout = 5000;
                request.AddHeader("cache-control", "no-cache");
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.GetRecordingCaresoftERP.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    CreatedAt = DateTime.Now,
                    Input = ""
                };
                log.Id = _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                RecordItemResV2 result = Newtonsoft.Json.JsonConvert.DeserializeObject<RecordItemResV2>(json);
                if (result.Data != null && result.Data.Count > 0)
                    return result.Data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        private List<RecordItemV2> GetListApiCaresoft(string phone, DateTime? fromDate, DateTime? toDate, int loanbriefId = 0, string access_token = "")
        {
            try
            {
                var url = ConstantCareSoft.BaseUrl + string.Format(ConstantCareSoft.get_record, 1, phone, fromDate.Value.ToString("yyyy-MM-ddTHH:mm:ssZ"), toDate.Value.ToString("yyyy-MM-ddTHH:mm:ssZ"), 50);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", "Bearer fIsfcZ3Ze_X9710IZH");
                request.Timeout = 10000;
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.GetRecordingCaresoftAPI.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    CreatedAt = DateTime.Now,
                    Input = ""
                };
                log.Id = _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);              
                RecordApiCaresoftRes result = Newtonsoft.Json.JsonConvert.DeserializeObject<RecordApiCaresoftRes>(json);
                if (result != null && result.Data != null && result.Data.Count > 0)
                    return result.Data;
            }
            catch (Exception ex)
            {
                Log.Information($"{phone}-{fromDate}-{toDate}-{loanbriefId}");
                Log.Error(ex, "CaresoftService/GetListApiCaresoft Exception");
            }
            return null;
        }

        public List<RecordItemV2> GetListApiCaresoft(List<string> phones, DateTime? fromDate, DateTime? toDate, int loanbriefId = 0, string access_token = "")
        {
            try
            {
                var resultRecord = new List<RecordItemV2>();
                if (phones != null && phones.Count > 0)
                {
                    var listTask = new List<Task>();
                    foreach (var phone in phones)
                    {

                        listTask.Add(Task.Run(() =>
                                           GetListApiCaresoft(phone, fromDate, toDate, loanbriefId, access_token)
                                            ));
                    }

                    Task.WhenAll(listTask.ToArray());
                    foreach (var task in listTask)
                    {
                        var result = ((Task<List<RecordItemV2>>)task).Result;
                        if (result != null && result.Count > 0)
                            resultRecord.AddRange(result);
                    }
                }
                return resultRecord;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
    }
}
