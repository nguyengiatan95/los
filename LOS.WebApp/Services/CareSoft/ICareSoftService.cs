﻿using LOS.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface ICareSoftService
    {
        bool LoginCareSoft(string access_token, int UserId, string IPPhone, ref string tokenUser);
        string GenarateLinkClick2Call(string ipphone, string numercall);
        List<RecordItem> GetListRecord(string? phone = "", DateTime? fromDate = null, DateTime? toDate = null);
        List<RecordItemV2> GetListRecordByList(List<string> phones, DateTime? fromDate = null, DateTime? toDate = null);
        List<RecordItemV2> GetListRecordV2(List<string> phone, DateTime? fromDate = null, DateTime? toDate = null,int loanbriefId = 0, string access_token = "");
        List<RecordItemV2> GetListApiCaresoft(List<string> phones, DateTime? fromDate, DateTime? toDate, int loanbriefId = 0, string access_token = "");
    }
}
