﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using static LOS.WebApp.Models.Cisco.Cisco;

namespace LOS.WebApp.Services.CiscoService
{
    public interface ICiscoService
    {
        List<AuthorizeOutput> Authorize(string access_token);
        List<GetList> GetList(string access_token, string phone, string beginTime, string CSRFTOKEN, string JSESSIONID);
        List<GetList> GetListByTime(string access_token, string phone, string fromeDate, string toDate, string CSRFTOKEN, string JSESSIONID);
        Task<ExportDetail> ExportingRecordings(string access_token, int Id, string CSRFTOKEN, string JSESSIONID);
        Task<ExportDetail> ExportingDetails(string access_token, int Id, long ExportId, string CSRFTOKEN, string JSESSIONID);
        HttpResponseMessage ExportedFile(string access_token, string url, string CSRFTOKEN, string JSESSIONID);
        List<lstCisco> GetListByTimeV2(string access_token, string phone, string fromeDate, string toDate);
        List<lstCisco> GetListByTimeV2(string access_token, List<string> phone, string fromDate, string toDate);
        Task<PushToCiscoOutput> ImportContactCampaign(string access_token, PushToCiscoInput entity);

    }
    public class CiscoService : ICiscoService
    {
        private readonly IConfiguration _baseConfig;
        private readonly IDictionaryService _dictionaryService;
        private readonly ICiscoService _ciscoService;
        public CiscoService(IConfiguration configuration, IDictionaryService dictionaryService)
        {
            _baseConfig = configuration;
            _dictionaryService = dictionaryService;
        }
        public List<AuthorizeOutput> Authorize(string access_token)
        {
            try
            {
                var input = new AuthorizeInput
                {
                    Id = Constants.CISCO_ID,
                    UserId = Constants.CISCO_USERID,
                    Password = Constants.CISCO_PASSWORD,
                    Data = new Data
                    {
                        Domain = Constants.CISCO_DOMAIN
                    }
                };
                var url = _baseConfig["AppSettings:CiscoUrl"] + Constants.CISCO_AUTHORIZE;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(input);
                var request = new RestRequest(Method.POST);
                request.Timeout = 3000;
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", "[" + body + "]", ParameterType.RequestBody);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.CiscoAuthorize.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = access_token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    CreatedAt = DateTime.Now,
                    Input = body
                };
                log.Id = _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    List<AuthorizeOutput> lst = JsonConvert.DeserializeObject<List<AuthorizeOutput>>(json);
                    lst[0].CSRFTOKEN = response.Cookies[0].Value;
                    lst[0].JSESSIONID = response.Cookies[1].Value;

                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public HttpResponseMessage ExportedFile(string access_token, string url, string CSRFTOKEN, string JSESSIONID)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Cookie", "CSRFTOKEN=" + CSRFTOKEN + ";JSESSIONID=" + JSESSIONID);
            IRestResponse res = client.Execute(request);
            HttpResponseMessage response = null;
            if (res.StatusCode == HttpStatusCode.OK)
            {
                var json = res.Content;
                //byte[] bytes = Convert.FromBase64String(json);
                response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new ByteArrayContent(res.RawBytes);
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = "file.wav";
            }
            return response;
        }

        public async Task<ExportDetail> ExportingDetails(string access_token, int Id, long ExportId, string CSRFTOKEN, string JSESSIONID)
        {
            var url = _baseConfig["AppSettings:CiscoUrl"] + string.Format(Constants.CISCO_EXPORTINGDETAILS, Id, ExportId);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Cookie", "CSRFTOKEN=" + CSRFTOKEN + ";JSESSIONID=" + JSESSIONID);
            //Add log
            //var log = new LogCallApi
            //{
            //    ActionCallApi = ActionCallApi.CiscoExportingDetails.GetHashCode(),
            //    LinkCallApi = url,
            //    TokenCallApi = access_token,
            //    Status = (int)StatusCallApi.CallApi,
            //    LoanBriefId = 0,
            //    Input = ""
            //};
            //log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            //log.Status = (int)StatusCallApi.Success;
            //log.Output = json;
            //_dictionaryService.UpdateLogCallApi(access_token, log);
            IRestResponse response = await client.ExecuteTaskAsync(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var json = response.Content;
                ExportDetail lst = JsonConvert.DeserializeObject<ExportDetail>(json);
                return lst;
            }

            return null;
        }

        public async Task<ExportDetail> ExportingRecordings(string access_token, int Id, string CSRFTOKEN, string JSESSIONID)
        {
            var obj = new ExportInput
            {
                MediaFormat = "WAV"
            };
            var url = _baseConfig["AppSettings:CiscoUrl"] + string.Format(Constants.CISCO_EXPORTINGRECORDINGS, Id);
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(obj);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Cookie", "CSRFTOKEN=" + CSRFTOKEN + ";JSESSIONID=" + JSESSIONID);
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            //Add log
            //var log = new LogCallApi
            //{
            //    ActionCallApi = ActionCallApi.CiscoExportingRecordings.GetHashCode(),
            //    LinkCallApi = url,
            //    TokenCallApi = access_token,
            //    Status = (int)StatusCallApi.CallApi,
            //    LoanBriefId = 0,
            //    Input = body
            //};
            //log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            //log.Status = (int)StatusCallApi.Success;
            //log.Output = json;
            //_dictionaryService.UpdateLogCallApi(access_token, log);
            IRestResponse response = await client.ExecuteTaskAsync(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var json = response.Content;
                ExportDetail lst = JsonConvert.DeserializeObject<ExportDetail>(json);
                return lst;
            }
            return null;


        }

        public List<GetList> GetList(string access_token, string phone, string beginTime, string CSRFTOKEN, string JSESSIONID)
        {
            var url = _baseConfig["AppSettings:CiscoUrl"] + string.Format(Constants.CISCO_GETLIST, beginTime);
            if (!string.IsNullOrEmpty(phone))
                url = _baseConfig["AppSettings:CiscoUrl"] + string.Format(Constants.CISCO_GETLIST_PHONE, phone, beginTime);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Cookie", "CSRFTOKEN=" + CSRFTOKEN + ";JSESSIONID=" + JSESSIONID);

            //Add log
            var log = new LogCallApi
            {
                ActionCallApi = ActionCallApi.CiscoGetList.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = access_token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = 0,
                Input = ""
            };
            log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            log.Status = (int)StatusCallApi.Success;
            log.Output = json;
            _dictionaryService.UpdateLogCallApi(access_token, log);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                var author = Authorize(access_token);
                if (author[0].Status == 200)
                    GetList(access_token, phone, beginTime, author[0].CSRFTOKEN, author[0].JSESSIONID);

            }
            json = json.Replace("$", "");
            List<GetList> lst = JsonConvert.DeserializeObject<List<GetList>>(json);
            return lst;
        }
        public List<GetList> GetListByTime(string access_token, string phone, string fromeDate, string toDate, string CSRFTOKEN, string JSESSIONID)
        {
            var url = "";
            if (!String.IsNullOrEmpty(phone))
                url = _baseConfig["AppSettings:CiscoUrl"] + string.Format(Constants.CISCO_GETLIST_PHONE, phone, fromeDate, toDate);
            else
                url = _baseConfig["AppSettings:CiscoUrl"] + string.Format(Constants.CISCO_GETLIST_TIME, fromeDate, toDate);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.Timeout = 5000;
            request.AddHeader("Cookie", "CSRFTOKEN=" + CSRFTOKEN + ";JSESSIONID=" + JSESSIONID);

            request.AddHeader("Range", "items=0-10000");
            //Add log
            var log = new LogCallApi
            {
                ActionCallApi = ActionCallApi.CiscoGetList.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = access_token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = 0,
                CreatedAt = DateTime.Now,
                Input = ""
            };
            log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            log.Status = (int)StatusCallApi.Success;
            log.Output = json;
            _dictionaryService.UpdateLogCallApi(access_token, log);
            //if (response.StatusCode == HttpStatusCode.Unauthorized)
            //{
            //    var author = Authorize(access_token);
            //    if (author[0].Status == 200)
            //        GetListByTime(access_token, phone, fromeDate, toDate, author[0].CSRFTOKEN, author[0].JSESSIONID);

            //}
            json = json.Replace("$", "");
            List<GetList> lst = JsonConvert.DeserializeObject<List<GetList>>(json);
            return lst;
        }

        public List<lstCisco> GetListByTimeV2(string access_token, string phone, string fromDate, string toDate)
        {
            try
            {
                var url = _baseConfig["AppSettings:DomainAPILOS"] + string.Format(Constants.CISCO_GETLIST_BY_TIME_V2, phone, fromDate, toDate);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.CiscoGetList.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = access_token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    CreatedAt = DateTime.Now,
                    Input = phone
                };
                log.Id = _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);

                json = json.Replace("$", "");
                var data = new List<lstCisco>();
                GetDataCisco cisco = JsonConvert.DeserializeObject<GetDataCisco>(json);
                if (cisco.Meta.ErrorCode == 200 && cisco.Data != null)
                    data = cisco.Data;
                return data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public List<lstCisco> GetListByTimeV2(string access_token, List<string> phone, string fromDate, string toDate)
        {
            try
            {
                var url = _baseConfig["AppSettings:DomainAPILOS"] + string.Format(Constants.CISCO_GETLIST_BY_LIST_PHONE_TIME_V2, fromDate, toDate);
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                var body = JsonConvert.SerializeObject(phone);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.CiscoGetList.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = access_token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    CreatedAt = DateTime.Now,
                    Input = body
                };
                log.Id = _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);

                json = json.Replace("$", "");
                var data = new List<lstCisco>();
                GetDataCisco cisco = JsonConvert.DeserializeObject<GetDataCisco>(json);
                if (cisco.Meta.ErrorCode == 200 && cisco.Data != null)
                    data = cisco.Data;
                return data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public async Task<PushToCiscoOutput> ImportContactCampaign(string access_token, PushToCiscoInput entity)
        {
            try
            {
                var url = _baseConfig["AppSettings:PushToCiscoUrl"] + Constants.PUSH_TO_CISCO;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                var body = JsonConvert.SerializeObject(entity);
                request.AddHeader("Authorization", "LOSAPIBAIzaSyA6KpV0cxO05cv0NR9UmvYtjOHZXAfVFiH");
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.PushToCisco.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = body,
                    CreatedAt = DateTime.Now,
                };
                _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    PushToCiscoOutput lst = JsonConvert.DeserializeObject<PushToCiscoOutput>(json);
                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
