﻿using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using LOS.DAL.EntityFramework;
using Serilog;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.ContactCustomer
{
    public class ContactCustomerService : BaseServices, IContactCustomerService
    {
        protected IConfiguration _baseConfig;
        public ContactCustomerService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
        }
        public List<ContactCustomerDetails> Search(string access_token, string param, ref int recordsTotal)
        {
            List<ContactCustomerDetails> data = new List<ContactCustomerDetails>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<ContactCustomerDetails>>>(Constants.ContactCustomer_ENDPOINT + param, access_token);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordsTotal = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public int AddContactCustomer(string access_token, LOS.DAL.EntityFramework.ContactCustomer entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ContactCustomer_ADD_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "ContactCustomer/AddContactCustomer Exception");
            }
            return 0;
        }
        public async Task<LOS.DAL.EntityFramework.ContactCustomer> GetContactCustomerById(string access_token, int id)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LOS.DAL.EntityFramework.ContactCustomer>>(string.Format(Constants.ContactCustomer_GET_ENDPOINT, id), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public async Task<LOS.DAL.EntityFramework.ContactCustomer> GetContactCustomerByPhone(string access_token, string phone)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LOS.DAL.EntityFramework.ContactCustomer>>(string.Format(Constants.ContactCustomerByPhone_GET_ENDPOINT, phone), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public bool Update(string access_token, int id, LOS.DAL.EntityFramework.ContactCustomer entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PutAsync<DefaultResponse<object>>(string.Format(Constants.ContactCustomer_UPDATE_ENDPOINT, id), access_token, stringContent);
                return response.Result.meta.errorCode == 200;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
