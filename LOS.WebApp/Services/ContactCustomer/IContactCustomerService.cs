﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.ContactCustomer
{
    public interface IContactCustomerService
    {
        List<ContactCustomerDetails> Search(string access_token, string param, ref int recordTotals);
        int AddContactCustomer(string access_token, LOS.DAL.EntityFramework.ContactCustomer entity);
        Task<LOS.DAL.EntityFramework.ContactCustomer> GetContactCustomerById(string access_token, int id);
        Task<LOS.DAL.EntityFramework.ContactCustomer> GetContactCustomerByPhone(string access_token, string phone);
        bool Update(string access_token, int id, LOS.DAL.EntityFramework.ContactCustomer entity);

    }
}
