﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace LOS.WebApp.Services
{
    public class DictionaryService : BaseServices, IDictionaryService
    {
        public DictionaryService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public List<DistrictDetail> GetDistrict(string access_token, int id)
        {
            List<DistrictDetail> data = new List<DistrictDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<DistrictDetail>>>(Constants.GET_DISTRICTS_ENPOINT + "?province_id=" + id, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<ProvinceDetail> GetProvince(string access_token)
        {
            List<ProvinceDetail> data = new List<ProvinceDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<ProvinceDetail>>>(Constants.GET_PROVINCES_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<WardDetail> GetWard(string access_token, int wardId)
        {
            List<WardDetail> data = new List<WardDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<WardDetail>>>(string.Format(Constants.GET_WARDS_ENPOINT, wardId), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<JobDetail> GetJob(string access_token)
        {
            List<JobDetail> data = new List<JobDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<JobDetail>>>(Constants.GET_JOBS_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public List<JobDetail> GetJobV2(string access_token)
        {
            List<JobDetail> data = new List<JobDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<JobDetail>>>(Constants.GET_JOBS_V2_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<LoanStatus> GetLoanStatus(string access_token)
        {
            List<LoanStatus> data = new List<LoanStatus>();
            try
            {
                var response = GetAsync<DefaultResponse<List<LoanStatus>>>(Constants.GET_LOAN_STATUS_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<PipelineState> GetPipelineState(string access_token)
        {
            List<PipelineState> data = new List<PipelineState>();
            try
            {
                var response = GetAsync<DefaultResponse<List<PipelineState>>>(Constants.GET_PIPELINE_STATE_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<LoanProduct> GetLoanProduct(string access_token)
        {
            List<LoanProduct> data = new List<LoanProduct>();
            try
            {
                var response = GetAsync<DefaultResponse<List<LoanProduct>>>(Constants.GET_LOAN_PRODUCT_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<UtmSource> GetUtmSource(string access_token)
        {
            List<UtmSource> data = new List<UtmSource>();
            try
            {
                var response = GetAsync<DefaultResponse<List<UtmSource>>>(Constants.GET_UTMSOURCE_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public List<ProductDetail> GetProduct(string access_token, int brandId)
        {
            List<ProductDetail> data = new List<ProductDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<ProductDetail>>>(string.Format(Constants.GET_PRODUCT_ENPOINT, brandId), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<BrandProductDetail> GetBrandProduct(string access_token)
        {
            List<BrandProductDetail> data = new List<BrandProductDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<BrandProductDetail>>>(Constants.GET_BRAND_PRODUCT_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<BankDetail> GetBank(string access_token)
        {
            List<BankDetail> data = new List<BankDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<BankDetail>>>(Constants.GET_BANK_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public List<PlatformTypeDetail> GetPlatformType(string access_token)
        {
            var data = new List<PlatformTypeDetail>();
            try
            {
                var res = GetAsync<DefaultResponse<List<PlatformTypeDetail>>>(Constants.GET_PLATFORM_ENPOINT, access_token, Constants.WEB_API_URL);
                if (res.Result.meta.errorCode == 200)
                    if (res.Result.data.Count > 0)
                        data = res.Result.data;
            }
            catch (Exception ex)
            {
            }
            return data;
        }

        public List<RelativeFamilyDetail> GetRelativeFamily(string access_token)
        {
            List<RelativeFamilyDetail> data = new List<RelativeFamilyDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<RelativeFamilyDetail>>>(Constants.GET_RELATIVE_FAMILY_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public LoanBriefScriptDetail GetScript(string access_token, int ProductId)
        {
            var data = new LoanBriefScriptDetail();
            try
            {
                var response = GetAsync<DefaultResponse<LoanBriefScriptDetail>>(string.Format(Constants.GET_SCRIPT_ENPOINT, ProductId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data != null)
                        data = response.Result.data;
                }
            }
            catch (Exception ex)
            {
            }
            return data;
        }
        public List<UserDetail> GetUserByGroupId(string access_token, int groupId)
        {
            List<UserDetail> data = new List<UserDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<UserDetail>>>(string.Format(Constants.GET_USERBYGROUP_ENPOINT, groupId), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<ReasonCancel> GetReasonCancel(string access_token)
        {
            List<ReasonCancel> data = new List<ReasonCancel>();
            try
            {
                var response = GetAsync<DefaultResponse<List<ReasonCancel>>>(Constants.GET_REASON_CANCEL_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<ShopDetail> GetShop(string access_token)
        {
            List<ShopDetail> data = new List<ShopDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<ShopDetail>>>(Constants.GET_SHOP_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<ShopDetail> GetShopByCityId(string access_token, int cityId)
        {
            List<ShopDetail> data = new List<ShopDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<ShopDetail>>>(string.Format(Constants.GET_SHOP_BY_CITYID_ENPOINT, cityId), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public List<ShopDetail> GetLender(string access_token, string param, ref int recordTotals)
        {
            List<ShopDetail> data = new List<ShopDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<ShopDetail>>>(Constants.GET_LENDER_ENPOINT + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public List<DocumentTypeDetail> GetDocumentType(string access_token, int ProductId)
        {
            var data = new List<DocumentTypeDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<DocumentTypeDetail>>>(string.Format(Constants.GET_DOCUMENT_TYPE_ENPOINT, ProductId), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                        data = response.Result.data;
                }
            }
            catch (Exception ex)
            {
            }
            return data;
        }

        public int AddLogCallApi(string access_token, LogCallApi entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_LOG_CALL_API, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/AddLog Exception");
            }
            return 0;
        }

        public int UpdateLogCallApi(string access_token, LogCallApi entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.UPDATE_LOG_CALL_API, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/UpdateLogCallApi Exception");
            }
            return 0;
        }

        public List<HubType> GetHubType(string access_token)
        {
            List<HubType> data = new List<HubType>();
            try
            {
                var response = GetAsync<DefaultResponse<List<HubType>>>(Constants.GET_HUBTYPE_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public int AddLogClickToCall(string access_token, LogClickToCall entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_LOG_CLICK_TO_CALL, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/AddLogClickToCall Exception");
            }
            return 0;
        }

        public List<TypeOwnerShip> GetTypeOwnerShip(string access_token)
        {
            List<TypeOwnerShip> data = new List<TypeOwnerShip>();
            try
            {
                var response = GetAsync<DefaultResponse<List<TypeOwnerShip>>>(Constants.GET_TYPE_OWNERSHIP_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public int AddLogLoanInfoAi(string access_token, LogLoanInfoAi entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_LOG_CALL_AI, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/AddLogAi Exception");
            }
            return 0;
        }

        public int UpdateLogLoanInfoAi(string access_token, LogLoanInfoAi entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.UPDATE_LOG_CALL_AI, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/UpdateLogCallAi Exception");
            }
            return 0;
        }

        public LogLoanInfoAi CheckLogAiByLoanBriefId(string access_token, int loanBriefId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LogLoanInfoAi>>(string.Format(Constants.CHECK_LOG_CALL_AI_BY_ID_ENDPOINT, loanBriefId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public List<EventConfig> GetEventConfig(string access_token)
        {
            List<EventConfig> data = new List<EventConfig>();
            try
            {
                var response = GetAsync<DefaultResponse<List<EventConfig>>>(Constants.GET_EVENT_CONFIG_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public LoanbriefLender GetLoanBriefLender(string access_token, int loanBriefId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LoanbriefLender>>(string.Format(Constants.LOANBRIEF_LENDER_BY_ID_ENDPOINT, loanBriefId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public District GetDistrictById(string access_token, int districtId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, District>>(string.Format(Constants.GET_DISTRICT_BY_ID, districtId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public LogSendSms CheckLogSendSMS(string access_token, int loanBriefId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LogSendSms>>(string.Format(Constants.CHECK_LOG_SEND_SMS, loanBriefId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public int AddLogSendSms(string access_token, LogSendSms entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_LOG_SEND_SMS, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        public int UpdateLogSendSms(string access_token, LogSendSms entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.UPDATE_LOG_SEND_SMS, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public List<TypeRemarketing> GetTypeRemarketing(string access_token)
        {
            List<TypeRemarketing> data = new List<TypeRemarketing>();
            try
            {
                var response = GetAsync<DefaultResponse<List<TypeRemarketing>>>(Constants.GET_TYPE_REMARKETING_ENPOINT, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public ProvinceDetail GetProvinceIdByName(string access_token, string name)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, ProvinceDetail>>(string.Format(Constants.GET_PROVINCEID_BY_NAME, name), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public DistrictDetail GetDistrictIdByName(string access_token, string name)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, DistrictDetail>>(string.Format(Constants.GET_DISTRICTID_BY_NAME, name), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public LoanProduct GetProductIdByName(string access_token, string name)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LoanProduct>>(string.Format(Constants.GET_PRODUCTID_BY_NAME, name), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public LogResultAutoCall GetLogResultAutoCall(string access_token, int loanBriefId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LogResultAutoCall>>(string.Format(Constants.GET_LOG_RESULT_AUTO_CALL, loanBriefId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public int AddCheckLoanInformation(string access_token, CheckImageReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_CHECKLOAN_INFORMATION, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/AddCheckImage Exception");
            }
            return 0;
        }

        public List<CheckLoanInformation> GetCheckImageByLoanBriefId(string access_token, int loanBriefId)
        {
            var data = new List<CheckLoanInformation>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<CheckLoanInformation>>>(string.Format(Constants.GET_CHECK_IMAGE_BY_LOANBRIEFID, loanBriefId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public int AddDocumentTypeImage(string access_token, DocumentTypeReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_DOCUMENTTYPE_IMAGE, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/AddDocumentType Exception");
            }
            return 0;
        }

        public int AddReasonCoordinator(string access_token, ReturnLoanReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_REASON_COORDINATOR, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/AddReasonCoordinator Exception");
            }
            return 0;
        }

        public List<JobDetail> GetJobByParentId(string access_token, int ParentId)
        {
            List<JobDetail> data = new List<JobDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<JobDetail>>>(string.Format(Constants.GET_JOBS_BY_PARENTID_ENPOINT, ParentId), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public JobDetail GetJobById(string access_token, int id)
        {
            JobDetail data = new JobDetail();
            try
            {
                var response = GetAsync<DefaultResponse<JobDetail>>(string.Format(Constants.GET_JOBS_BY_ID_ENPOINT, id), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    data = response.Result.data;
                }
            }
            catch (Exception ex)
            {
            }
            return data;
        }

        public List<ProductReviewResultDetail> GetProductReviewDetail(string access_token, int id)
        {
            List<ProductReviewResultDetail> data = new List<ProductReviewResultDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<ProductReviewResultDetail>>>(string.Format(Constants.GET_PRODUCT_REVIEWDETAIL_ENPOINT, id), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public Province GetProvinceById(string access_token, int provinceId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, Province>>(string.Format(Constants.GET_PROVINCE_BY_ID, provinceId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public List<DocumentTypeDetail> GetDocumentAll(string access_token)
        {
            var data = new List<DocumentTypeDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<DocumentTypeDetail>>>(string.Format(Constants.GET_DOCUMENT_TYPE_ALL_ENPOINT), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                        data = response.Result.data;
                }
            }
            catch (Exception ex)
            {
            }
            return data;
        }

        public List<ConfigDocument> GetConfigDocument(string access_token, int productId, int typeOwnerShip)
        {
            List<ConfigDocument> data = new List<ConfigDocument>();
            try
            {
                var response = GetAsync<DefaultResponse<List<ConfigDocument>>>(string.Format(Constants.GET_CONFIG_DOCUMENT_ENPOINT, productId, typeOwnerShip), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public int AddConfigDocument(string access_token, ConfigDocument entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_CONFIG_DOCUMENT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/AddConfigDocument Exception");
            }
            return 0;
        }

        public List<DocumentMapInfo> GetDocumentTypeV2(string access_token, int productId, int typeOwnerShip)
        {
            List<DocumentMapInfo> data = new List<DocumentMapInfo>();
            try
            {
                var response = GetAsync<DefaultResponse<List<DocumentMapInfo>>>(string.Format(Constants.GET_DOCUMENT_TYPE_V2_ENPOINT, productId, typeOwnerShip), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public LoanConfigStep GetLoanConfigStep(string access_token, int id)
        {
            var data = new LoanConfigStep();
            try
            {
                var response = GetAsync<DefaultResponse<LoanConfigStep>>(Constants.GET_LOAN_CONFIG_STEP + "?id=" + id, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    data = response.Result.data;
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<LoanConfigStep> GetAllLoanConfigStep(string access_token)
        {
            List<LoanConfigStep> data = new List<LoanConfigStep>();
            try
            {
                var response = GetAsync<DefaultResponse<List<LoanConfigStep>>>(Constants.GET_ALL_LOAN_CONFIG_STEP, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<InfomationProductDetailDTO> GetAllInfomationProductDetail(string access_token)
        {
            List<InfomationProductDetailDTO> data = new List<InfomationProductDetailDTO>();
            try
            {
                var response = GetAsync<DefaultResponse<List<InfomationProductDetailDTO>>>(Constants.GET_ALL_INFOMATION_PRODUCT_DETAIL, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public InfomationProductDetail GetInfomationProductDetailById(string access_token, int id)
        {
            InfomationProductDetail data = new InfomationProductDetail();
            try
            {
                var response = GetAsync<DefaultResponse<InfomationProductDetail>>(string.Format(Constants.GET_INFOMATION_PRODUCT_DETAIL_BY_ID, id), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    data = response.Result.data;
                }
            }
            catch (Exception ex)
            {
            }
            return data;
        }

        public List<InfomationProductDetail> GetInfomationProductDetailByTypeOwnership(string access_token, int typeOwnership)
        {
            List<InfomationProductDetail> data = new List<InfomationProductDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<InfomationProductDetail>>>(string.Format(Constants.GET_INFOMATION_PRODUCT_DETAIL_BY_TYPE_OWNERSHIP, typeOwnership), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<ProductDetailModel> GetInfomationProductDetailByTypeOwnershipAndProduct(string access_token, int typeOwnership, int productId, int typeLoanBrief, int groupId)
        {
            List<ProductDetailModel> data = new List<ProductDetailModel>();
            try
            {
                var response = GetAsync<DefaultResponse<List<ProductDetailModel>>>(string.Format(Constants.GET_INFOMATION_PRODUCT_DETAIL_BY_TYPE_OWNERSHIP_AND_PRODUCT, typeOwnership, productId, typeLoanBrief, groupId), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public ProductDetailModel GetInfomationProductDetailByProductDetailIdAndProduct(string access_token, int productdetailId, int productId)
        {
            ProductDetailModel data = new ProductDetailModel();
            try
            {
                var response = GetAsync<DefaultResponse<ProductDetailModel>>(string.Format(Constants.GET_INFOMATION_PRODUCT_DETAIL_BY_PRODUCT_DETAIL_AND_PRODUCT, productdetailId, productId), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    data = response.Result.data;
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<ConfigDepartmentStatus> GetConfigDepartmentStatus(string access_token, int departmentId)
        {
            var data = new List<ConfigDepartmentStatus>();
            try
            {
                var response = GetAsync<DefaultResponse<List<ConfigDepartmentStatus>>>(string.Format(Constants.GET_CONFIG_DEPARTMENT_ENPOINT, departmentId), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public int AddDistrict(string access_token, District entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_DISTRICT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/AddDistrict Exception");
            }
            return 0;
        }
        public int AddWard(string access_token, Ward entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_WARD, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/AddWard Exception");
            }
            return 0;
        }
     
        public LoanRelationshipDTO GetRelationshipInfo(string access_token, int loanBriefId, int relationshipId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LoanRelationshipDTO>>(string.Format(Constants.GET_RELATIONSHIP_INFO_ENPOINT, loanBriefId, relationshipId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public async Task<int> SaveLogCompare(string access_token, CompareDocument entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.SAVE_COMPARE_DOCUMENT_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/SaveLogCompare Exception");
            }
            return 0;
        }

        public CompareDocument GetLastCompareDocument(string access_token, int loanBriefId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<CompareDocument>>(string.Format(Constants.GET_LAST_COMPARE_DOCUMENT, loanBriefId), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public async Task<bool> CheckCompareFinish(string access_token, int loanBriefId, int groupId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<dynamic>>(string.Format(Constants.CHECK_COMPARE_FINISH_ENDPOINT,loanBriefId, groupId), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/CheckCompareFinish Exception");
            }
            return false;
        }

        public List<DocumentException> GetDocumentExceptions(string access_token)
        {
            List<DocumentException> data = new List<DocumentException>();
            try
            {
                var response = GetAsync<DefaultResponse<List<DocumentException>>>(Constants.GET_DOCUMENT_EXCEPTIONS, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public int AddLogInfoCheckMomo(string access_token, LogLoanInfoPartner entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_LOG_MOMO_INFO, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {

            }
            return 0;
        }
public async Task<int> AddLogChangePhone(string access_token, LogChangePhone entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.SAVE_LOG_CHANGE_PHONE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Dictionnary/AddLogChangePhone Exception");
            }
            return 0;
        }

        public int AddLogSecuredTransaction(string access_token, LogSecuredTransaction entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_LOG_TRANSACTION_SECURED, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {

            }
            return 0;
        }

        public LogSecuredTransaction GetLastLogSecuredTransaction(string access_token, int loanBriefId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<LogSecuredTransaction>>(string.Format(Constants.GET_LAST_LOGSECUREDTrANSACTION, loanBriefId), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
    }
}
