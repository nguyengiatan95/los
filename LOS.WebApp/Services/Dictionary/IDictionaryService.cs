﻿using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IDictionaryService
    {
        List<JobDetail> GetJob(string access_token);
        List<JobDetail> GetJobV2(string access_token);
        List<ProvinceDetail> GetProvince(string access_token);
        List<DistrictDetail> GetDistrict(string access_token, int id);
        List<WardDetail> GetWard(string access_token, int id);
        List<LoanStatus> GetLoanStatus(string access_token);
        List<PipelineState> GetPipelineState(string access_token);
        List<LoanProduct> GetLoanProduct(string access_token);
        List<RelativeFamilyDetail> GetRelativeFamily(string access_token);
        List<UtmSource> GetUtmSource(string access_token);
        List<ProductDetail> GetProduct(string access_token, int brandId);
        List<BankDetail> GetBank(string access_token);
        List<PlatformTypeDetail> GetPlatformType(string access_token);
        List<BrandProductDetail> GetBrandProduct(string access_token);
        List<UserDetail> GetUserByGroupId(string access_token, int groupId);
        LoanBriefScriptDetail GetScript(string access_token, int ProductId);

        List<ReasonCancel> GetReasonCancel(string access_token);
        List<ShopDetail> GetShop(string access_token);
        List<ShopDetail> GetShopByCityId(string access_token, int cityId);
        List<ShopDetail> GetLender(string access_token, string param, ref int recordTotals);
        List<DocumentTypeDetail> GetDocumentType(string access_token, int ProductId);

        int AddLogCallApi(string access_token, LogCallApi entity);
        int UpdateLogCallApi(string access_token, LogCallApi entity);
        List<HubType> GetHubType(string access_token);

        int AddLogClickToCall(string access_token, LogClickToCall entity);

        List<TypeOwnerShip> GetTypeOwnerShip(string access_token);
        int AddLogLoanInfoAi(string access_token, LogLoanInfoAi entity);
        int UpdateLogLoanInfoAi(string access_token, LogLoanInfoAi entity);
        LogLoanInfoAi CheckLogAiByLoanBriefId(string access_token, int loanBriefId);
        List<EventConfig> GetEventConfig(string access_token);
        LoanbriefLender GetLoanBriefLender(string access_token, int loanBriefId);
        District GetDistrictById(string access_token, int districtId);
        LogSendSms CheckLogSendSMS(string access_token, int loanBriefId);
        int AddLogSendSms(string access_token, LogSendSms entity);
        int UpdateLogSendSms(string access_token, LogSendSms entity);
        List<TypeRemarketing> GetTypeRemarketing(string access_token);
        ProvinceDetail GetProvinceIdByName(string access_token, string name);
        DistrictDetail GetDistrictIdByName(string access_token, string name);
        LoanProduct GetProductIdByName(string access_token, string name);
        LogResultAutoCall GetLogResultAutoCall(string access_token, int loanBriefId);
        int AddCheckLoanInformation(string access_token, CheckImageReq entity);
        List<CheckLoanInformation> GetCheckImageByLoanBriefId(string access_token, int loanBriefId);
        int AddDocumentTypeImage(string access_token, DocumentTypeReq entity);
        int AddReasonCoordinator(string access_token, ReturnLoanReq entity);
        List<JobDetail> GetJobByParentId(string access_token, int ParentId);
        JobDetail GetJobById(string access_token, int id);
        List<ProductReviewResultDetail> GetProductReviewDetail(string access_token, int id);
        Province GetProvinceById(string access_token, int provinceId);
        List<ConfigDocument> GetConfigDocument(string access_token, int productId, int typeOwnerShip);
        List<DocumentTypeDetail> GetDocumentAll(string access_token);
        int AddConfigDocument(string access_token, ConfigDocument entity);
        List<DocumentMapInfo> GetDocumentTypeV2(string access_token, int productId, int typeOwnerShip);
        LoanConfigStep GetLoanConfigStep(string access_token, int id);
        List<LoanConfigStep> GetAllLoanConfigStep(string access_token);
        List<InfomationProductDetailDTO> GetAllInfomationProductDetail(string access_token);
        InfomationProductDetail GetInfomationProductDetailById(string access_token, int Id);
        List<InfomationProductDetail> GetInfomationProductDetailByTypeOwnership(string access_token, int typeOwnership);
        List<ProductDetailModel> GetInfomationProductDetailByTypeOwnershipAndProduct(string access_token, int typeOwnership, int productId, int typeLoanBrief = 0, int groupId = 0);
        ProductDetailModel GetInfomationProductDetailByProductDetailIdAndProduct(string access_token, int productdetailId, int productId);
        List<ConfigDepartmentStatus> GetConfigDepartmentStatus(string access_token, int departmentId);
        int AddDistrict(string access_token, District entity);
        int AddWard(string access_token, Ward entity);      

        LoanRelationshipDTO GetRelationshipInfo(string access_token, int loanBriefId, int relationshipId);
        Task<int> SaveLogCompare(string access_token, CompareDocument entity);
        CompareDocument GetLastCompareDocument(string access_token, int loanBriefId);
        Task<bool> CheckCompareFinish(string access_token, int loanBriefId,int groupId);
        List<DocumentException> GetDocumentExceptions(string access_token);
        int AddLogInfoCheckMomo(string access_token, LogLoanInfoPartner entity);
 Task<int> AddLogChangePhone(string access_token, LogChangePhone entity);
        int AddLogSecuredTransaction(string access_token, LogSecuredTransaction entity);
        LogSecuredTransaction GetLastLogSecuredTransaction(string access_token, int loanBriefId);
    }
}
