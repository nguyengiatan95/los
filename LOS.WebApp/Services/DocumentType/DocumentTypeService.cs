﻿using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IDocumentType
    {
        List<DocumentTypeDetail> GetAll(string access_token, string param, ref int recordTotals);
        List<DocumentTypeDetail> GetDocumentTypeParent(string access_token, int ProductId);
        DocumentTypeDetail GetById(string access_token, int Id);
        bool Update(string access_token, DocumentType entity, int Id);
        bool Create(string access_token, DocumentType entity);
        List<DocumentTypeDetail> GetDocumentTypeEnable(string access_token);
        List<DocumentMapInfo> GetDocument(string access_token, int productId, int typeOwnerShip);
    }
    public class DocumentTypeService : BaseServices, IDocumentType
    {
        public DocumentTypeService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public List<DocumentTypeDetail> GetAll(string access_token, string param, ref int recordTotals)
        {
            List<DocumentTypeDetail> data = new List<DocumentTypeDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<DocumentTypeDetail>>>(Constants.DOCUMENTTYPE_GET_ENPOINT + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<DocumentTypeDetail> GetDocumentTypeParent(string access_token, int ProductId)
        {
            List<DocumentTypeDetail> data = new List<DocumentTypeDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<DocumentTypeDetail>>>(string.Format(Constants.DOCUMENTTYPE_GET_PARENT, ProductId), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public DocumentTypeDetail GetById(string access_token, int id)
        {
            var result = new DocumentTypeDetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, DocumentTypeDetail>>(string.Format(Constants.DOCUMENTTYPE_GET_BY_ID, id), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public bool Update(string access_token, DocumentType entity, int id)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PutAsync<DefaultResponse<object>>(string.Format(Constants.DOCUMENTTYPE_UPDATE_ENDPOINT, id), access_token, stringContent);
                return response.Result.meta.errorCode == 200;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Create(string access_token, DocumentType entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.DOCUMENTTYPE_CREATE_ENDPOINT, access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<DocumentTypeDetail> GetDocumentTypeEnable(string access_token)
        {
            List<DocumentTypeDetail> data = new List<DocumentTypeDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<DocumentTypeDetail>>>(Constants.DOCUMENTTYPE_GET_ENABLE, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<DocumentMapInfo> GetDocument(string access_token, int productId, int typeOwnerShip)
        {
            List<DocumentMapInfo> data = new List<DocumentMapInfo>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<DocumentMapInfo>>>(string.Format(Constants.DOCUMENTTYPE_GET_BY_PRODUCT_ENDPOINT, productId, typeOwnerShip), access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

    }
}
