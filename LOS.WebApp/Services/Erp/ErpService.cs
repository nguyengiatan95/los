﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Erp;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IErp
    {
        bool UpdateDeviceStatus(string access_token, PushDeviceReq req);
        CheckEmployeeTima CheckEmployeeTima(string access_token, string NumberCard, string Phone, int LoanBriefId = 0);       
        ResponseCheckStatusDevice CheckStatusDevice(string access_token, string deviceId);
        List<int> GetStaffHub(string access_token, int HubId, int loanbriefId);
        List<CheckCavet> CheckCavet(string access_token, int CodeId);
    }
    public class ErpService : IErp
    {
        private readonly IConfiguration _configuration;
        private readonly IDictionaryService _dictionaryService;
        public ErpService(IConfiguration configuration, IDictionaryService dictionaryService)
        {
            _configuration = configuration;
            _dictionaryService = dictionaryService;
        }
		
        public bool UpdateDeviceStatus(string access_token, PushDeviceReq req)
        {
            try
            {
                var url = _configuration["AppSettings:ERP"] + Constants.UpdateStatusDeviceToERP;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(req);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                request.Timeout = 5000;
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.ApiUpdateDeveiceToERP.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = req.loanId,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                log.Id = _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                log.ModifyAt = DateTime.Now;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                PushDeviceRes result = JsonConvert.DeserializeObject<PushDeviceRes>(json);
                return result != null && result.success;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public CheckEmployeeTima CheckEmployeeTima(string access_token, string NumberCard, string Phone, int LoanBriefId = 0)
        {
            try
            {
                var url = _configuration["AppSettings:ERP"] + Constants.CheckEmployeeTima + "?nationalId=" + NumberCard + "&tel=" + Phone;
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.ApiCheckEployeeTima.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = LoanBriefId,
                    Input = Phone,
                    CreatedAt = DateTime.Now,
                };
                _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                if(response.StatusCode == HttpStatusCode.OK)
                {
                    CheckEmployeeTima lst = JsonConvert.DeserializeObject<CheckEmployeeTima>(json);
                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ResponseCheckStatusDevice CheckStatusDevice(string access_token, string deviceId)
        {
            try
            {
                var url = _configuration["AppSettings:ERP"] + Constants.CheckDeviceErp + "/" + deviceId;
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.CheckDeviceErp.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = ""
                };
                _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ResponseCheckStatusDevice lst = JsonConvert.DeserializeObject<ResponseCheckStatusDevice>(json);
                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<int> GetStaffHub(string access_token, int HubId, int loanbriefId)
        {
            try
            {
                var url = _configuration["AppSettings:ERP"] + string.Format(Constants.get_employee_hub_checkin, HubId);
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Timeout = 5000;
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.GetHubEmployeeErp,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    Input = string.Format("{0} - {1}", HubId, loanbriefId),
                    CreatedAt = DateTime.Now
                };

                _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                var response = client.Execute(request);
                var content = response.Content;
                objLogCallApi.Status = (int)StatusCallApi.Success;
                objLogCallApi.Output = content;
                _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                return JsonConvert.DeserializeObject<List<int>>(content);
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public List<CheckCavet> CheckCavet(string access_token, int CodeId)
        {
            var url = _configuration["AppSettings:ERP"] + String.Format(Constants.CheckCavet,CodeId);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ3YXJlaG91c2UiOnsibmFtZSI6Ikjhu5lpIHPhu58gSE4iLCJwcm92aW5jZSI6IjVmYzcxMWZhMmE2MTUzMzIyMGVkODhlMyIsImlkIjoiNWYwNTc3MzgzNGExNjgwOWNmOTU1OTU3In0sIndhcmVob3VzZXMiOltdLCJyb2xlIjoiQWRtaW4iLCJmdWxsTmFtZSI6Ik5ndXnhu4VuIFbEg24gxJDhu6ljIiwiaWQiOiI1ZjRjYTExYTM4MTY1MzAwNzUwMjEzZTkiLCJpYXQiOjE2Mzg0OTc3MjYsImV4cCI6MTY0MTA4OTcyNn0.WkgyFD2bzc1xJulfwDgphwXqaNctAv_0e_nJJ6E1MJ0");
            request.AddHeader("Content-Type", "application/json");
            //Add log
            var log = new LogCallApi
            {
                ActionCallApi = ActionCallApi.CheckDeviceErp.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = "",
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = 0,
                Input = ""
            };
            _dictionaryService.AddLogCallApi(access_token, log);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            log.Status = (int)StatusCallApi.Success;
            log.Output = json;
            _dictionaryService.UpdateLogCallApi(access_token, log);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var lst =  JsonConvert.DeserializeObject<List<CheckCavet>>(json);
                return lst;
            }
            return null;
        }
    }
}
