﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.Response;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace LOS.WebApp.Services
{
    public class GroupServices : BaseServices, IGroupServices
    {
        protected IConfiguration _baseConfig;
        public GroupServices(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
        }
        public bool Create(string access_token, GroupDTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.GROUP_ENDPOINT, access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Delete(string access_token, int id)
        {
            try
            {
                var response = DeleteAsync<DefaultResponse<object>>(string.Format(Constants.GROUP_DELETE_ENDPOINT, id), access_token);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public GroupDetail GetById(string access_token, int id)
        {
            var result = new GroupDetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, GroupDetail>>(string.Format(Constants.GROUP_GET_ENDPOINT, id), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public int CheckDelete(string access_token, int id)
        {
            var result = 0;
            try
            {
                var response = GetAsync<DefaultResponse<Meta, dynamic>>(string.Format(Constants.GROUP_CHECK_DELETE_ENDPOINT, id), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }


        public List<GroupDetail> Search(string access_token, string param, ref int recordTotals)
        {
            List<GroupDetail> data = new List<GroupDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<GroupDetail>>>(Constants.GROUP_ENDPOINT + param, access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<Group> GetAll(string access_token)
        {
           var data = new  List<Group>();
            try
            {
                var response =  GetAsync<DefaultResponse<SummaryMeta, List<Group>>>(Constants.GROUP_GET_ALL, access_token);
                if (response.Result.meta.errorCode == 200)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {
               
            }
            return data;
        }

        public bool Update(string access_token, int id, GroupDTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PutAsync<DefaultResponse<object>>(string.Format(Constants.GROUP_UPDATE_ENDPOINT, id), access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public GroupDetail GetPermission(string access_token, int groupId)
        {
            var result = new GroupDetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, GroupDetail>>(string.Format(Constants.GROUP_GET_PERMISSION_ENDPOINT, groupId), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public bool UpdatePermission(string access_token, PermissionGroupDTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.GROUP_UPDATE_PERMISSION_ENDPOINT, access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Group> GetAllNotAdministrator(string access_token)
        {
            var data = new List<Group>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<Group>>>(Constants.GROUP_GET_ALL_NOT_ADMINISTRATOR, access_token);
                if (response.Result.meta.errorCode == 200)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
    }
}
