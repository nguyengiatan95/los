﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IGroupServices
    {
        List<GroupDetail> Search(string access_token, string param, ref int recordTotals);
        List<Group> GetAll(string access_token);
        GroupDetail GetById(string access_token, int id);
        bool Update(string access_token, int id, GroupDTO entity);
        bool Create(string access_token, GroupDTO entity);
        bool Delete(string access_token, int id);
        int CheckDelete(string access_token, int id);
        GroupDetail GetPermission(string access_token, int groupId);
        bool UpdatePermission(string access_token, PermissionGroupDTO entity);
        List<Group> GetAllNotAdministrator(string access_token);
    }
}
