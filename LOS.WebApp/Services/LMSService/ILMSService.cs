﻿using LOS.WebApp.Models;
using LOS.WebApp.Models.LMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static LOS.WebApp.Models.ListLoanDebt;

namespace LOS.WebApp.Services
{
    public interface ILMSService
    {
        public InterestParam.Output InterestAndInsurence(string access_token, InterestParam.Input param);
        public LenderParam.Output SearchLender(string access_token, LenderParam.Input param);
        public DeferredPayment.Output CheckCustomerDeferredPayment(string access_token, int loanBriefId, DeferredPayment.Input param);
        public SendSMSParam.OutPut SendSMS(string Phone, string Content);
        public CICParam.OutPut CICCardNumber(string CardNumber);
        public CheckReLoan.OutPut CheckReLoan(string access_token, CheckReLoan.Input param);
        public CheckReLoan.OutPut CheckReLoanV2(string access_token, CheckReLoan.Input param);
        public CheckReLoan.OutPut CheckReLoanV3(string access_token, CheckReLoan.Input param);
        CalculatorMoneyInsurance.OutPut CalculatorMoneyInsurance(string access_token, CalculatorMoneyInsurance.InPut param);
        CheckBlackList.Output CheckBlackList(string access_token, string FullName, string NumberCard, string Phone, int loanbriefId);
        public PaymentLoanById.OutPut GetPaymentLoanById(int loanId);
        public InterestParam.OutputTopup InterestTopup(string access_token, InterestParam.InputTopup param);
        public InterestParam.ViewDataTopUp GenCalendarPayment(string access_token, InterestParam.InputTopup param);
        public PayAmountForCustomer GetPayAmountForCustomer(string access_token, RequestPayAmountForCustomer entity);
        ResponeReportDebt GetReportDebt(string access_token, RequestReportDebt entity);
        PrematureInterest GetPayAmountForCustomerByLoanID(string access_token, RequestPrematureInterest entity, int loanbriefId);
        ListLoanDebtOutput GetListLoanDebt(string access_token, RequestListLoanDebt param);
        ResponeAddBlacklist AddBlackList(string access_token, RequestAddBlacklist param);
        public CheckReLoan.TopupOutPut CheckTopupOto(string access_token,int loanBriefId, CheckReLoan.Input param);
        DebtRestructuring.Output DebtRestructuring(string access_token, DebtRestructuring.Input param);
        ReponseDefaultLMS CloseLoan(ResquestCloseLoan entity, int loanBriefId);
    }
}
