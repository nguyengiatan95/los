﻿using JWT;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using LOS.DAL.EntityFramework;
using LOS.Common.Extensions;
using LOS.WebApp.Models.LMS;
using System.Net;
using static LOS.WebApp.Models.ListLoanDebt;
using LOS.Services.Services;

namespace LOS.WebApp.Services
{
    public class LMSService : ILMSService
    {
        private readonly IConfiguration _configuration;
        private readonly IDictionaryService _dictionaryService;
        private readonly ILogCloseLoanService _logCloseLoanService;
        public LMSService(IConfiguration configuration, IDictionaryService dictionaryService, ILogCloseLoanService logCloseLoanService)
        {
            _configuration = configuration;
            _dictionaryService = dictionaryService;
            _logCloseLoanService = logCloseLoanService;
        }

        public DeferredPayment.Output CheckCustomerDeferredPayment(string access_token, int loanBriefId, DeferredPayment.Input param)
        {
            var url = _configuration["AppSettings:LMS"] + Constants.DeferredPayment;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(param);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            //Add log
            var log = new LogCallApi
            {
                ActionCallApi = ActionCallApi.DeferredPayment.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = access_token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = loanBriefId,
                Input = body
            };
            log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            log.Status = (int)StatusCallApi.Success;
            log.Output = json;
            _dictionaryService.UpdateLogCallApi(access_token, log);
            DeferredPayment.Output lst = JsonConvert.DeserializeObject<DeferredPayment.Output>(json);
            return lst;
        }

        public CICParam.OutPut CICCardNumber(string CardNumber)
        {
            var url = _configuration["AppSettings:Proxy"];
            var client = new RestClient(url + "/api/Kapala/GetCredit?cardNumber=" + CardNumber);
            //client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "24cce015b4945d991db380e0b7cb4eef");
            request.AddHeader("account", "tima");
            request.AddHeader("password", "2c9b7a1e2cbe02dff7c8c15197d6d042");
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            CICParam.OutPut lst = JsonConvert.DeserializeObject<CICParam.OutPut>(json);

            return lst;
        }

        public InterestParam.Output InterestAndInsurence(string access_token, InterestParam.Input param)
        {
            var url = _configuration["AppSettings:LMS"] + Constants.InterestAndInsurence;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(param);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            request.Timeout = 3000;
            //Add log
            var log = new LogCallApi
            {
                ActionCallApi = ActionCallApi.ListLender.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = access_token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = param.LoanBriefId,
                Input = body,
                CreatedAt = DateTime.Now
            };
            log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            log.Status = (int)StatusCallApi.Success;
            log.Output = json;
            _dictionaryService.UpdateLogCallApi(access_token, log);
            InterestParam.Output lst = JsonConvert.DeserializeObject<InterestParam.Output>(json);

            return lst;
        }

        public LenderParam.Output SearchLender(string access_token, LenderParam.Input param)
        {
            var url = _configuration["AppSettings:LMS"] + Constants.ListLender;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(param);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            //Add log
            var log = new LogCallApi
            {
                ActionCallApi = ActionCallApi.ListLender.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = access_token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = 0,
                Input = body
            };
            log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            log.Status = (int)StatusCallApi.Success;
            log.Output = json;
            _dictionaryService.UpdateLogCallApi(access_token, log);
            LenderParam.Output lst = JsonConvert.DeserializeObject<LenderParam.Output>(json);

            return lst;
        }

        public SendSMSParam.OutPut SendSMS(string Phone, string Content)
        {
            string url = _configuration["AppSettings:Proxy"] + Constants.SendSMS;
            SendSMSParam.Input param = new SendSMSParam.Input();
            param.Domain = "vay1h.vn";
            param.Department = "Phòng IT";
            param.Phone = Phone;
            param.MessageContent = Content;
            var body = JsonConvert.SerializeObject(param);
            var client = new RestClient(url);
            //client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "8JTLS9eqmSKXdQKdNWrtXCMV5DAhC3k7");
            request.AddHeader("account", "LOS");
            request.AddHeader("password", "3cmELn3UsqFkvERbuJdzHtUL7HU6uFuH");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            SendSMSParam.OutPut lst = JsonConvert.DeserializeObject<SendSMSParam.OutPut>(json);

            return lst;
        }

        public CheckReLoan.OutPut CheckReLoan(string access_token, CheckReLoan.Input param)
        {
            var obj = new DeferredPayment.Input
            {
                NumberCard = param.NumberCard,
                CustomerName = param.CustomerName
            };
            var url = _configuration["AppSettings:LMS"] + Constants.DeferredPayment;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(obj);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            //Add log
            var log = new LogCallApi
            {
                ActionCallApi = ActionCallApi.DeferredPayment.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = access_token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = 0,
                Input = body
            };
            log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            log.Status = (int)StatusCallApi.Success;
            log.Output = json;
            _dictionaryService.UpdateLogCallApi(access_token, log);
            DeferredPayment.Output lst = JsonConvert.DeserializeObject<DeferredPayment.Output>(json);
            CheckReLoan.OutPut lstReLoan = new CheckReLoan.OutPut();

            if (lst.Result == 1)
            {
                lstReLoan.Result = 1;
                if (lst.Data != null)
                {
                    if (lst.Data.LstLoanCustomer[0].Status == EnumLoanStatus.FINISH.GetHashCode())
                    {
                        int MonthReLoan = Convert.ToInt32(_configuration["AppSettings:MonthReLoan"]);
                        int DayReLoan = Convert.ToInt32(_configuration["AppSettings:DayReLoan"]);
                        var days = (DateTime.Now - Convert.ToDateTime(lst.Data.LstLoanCustomer[0].FinishDate)).Days;
                        if (days <= (MonthReLoan * 30))
                        {
                            if (lst.Data.LstLoanCustomer[0].LstPaymentCustomer.Count() >= 3)
                            {
                                var CountDate = 0;
                                foreach (var item in lst.Data.LstLoanCustomer[0].LstPaymentCustomer)
                                {
                                    if (item.CountDay > DayReLoan)
                                    {
                                        CountDate = Convert.ToInt32(item.CountDay);
                                        break;
                                    }
                                }
                                if (CountDate <= DayReLoan)
                                {
                                    lstReLoan.IsAccept = 1;
                                    if (lst.Data.LstLoanCustomer[0].LoanBriefID > 0)
                                        lstReLoan.LoanBriefID = lst.Data.LstLoanCustomer[0].LoanBriefID;
                                    else
                                        lstReLoan.LoanBriefID = lst.Data.LstLoanCustomer[0].LoanId;
                                    lstReLoan.Message = "Thành công!";
                                }
                                else
                                {
                                    lstReLoan.Message = "Khách hàng có kỳ trả chậm " + CountDate + "!";
                                }
                            }
                            else
                            {
                                lstReLoan.Message = "Khách hàng có ít hơn 3 kỳ thanh toán!";
                            }
                        }
                        else
                        {
                            lstReLoan.Message = "Thời gian tất toán khoản vay cũ > " + MonthReLoan + " tháng!";
                        }
                    }
                    else
                    {
                        lstReLoan.Message = "Khách hàng chưa đủ điều kiện tái vay do đơn vay đang ở trạng thái " + lst.Data.LstLoanCustomer[0].StatusName;
                    }
                }
                else
                {
                    lstReLoan.Message = lst.Message;
                }

            }



            return lstReLoan;
        }
        public CheckReLoan.OutPut CheckReLoanV2(string access_token, CheckReLoan.Input param)
        {
            var obj = new DeferredPayment.Input
            {
                NumberCard = param.NumberCard,
                CustomerName = param.CustomerName
            };
            var url = _configuration["AppSettings:LMS"] + Constants.DeferredPayment;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(obj);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            //Add log
            var log = new LogCallApi
            {
                ActionCallApi = ActionCallApi.DeferredPayment.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = access_token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = 0,
                Input = body
            };
            log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            log.Status = (int)StatusCallApi.Success;
            log.Output = json;
            _dictionaryService.UpdateLogCallApi(access_token, log);
            DeferredPayment.Output lst = JsonConvert.DeserializeObject<DeferredPayment.Output>(json);
            CheckReLoan.OutPut lstReLoan = new CheckReLoan.OutPut();

            if (lst.Result == 1)
            {
                lstReLoan.Result = 1;
                if (lst.Data != null)
                {

                    if (lst.Data.LstLoanCustomer[0].Status == EnumLoanStatus.FINISH.GetHashCode())
                    {
                        //Kiểm tra tổng số kỳ thanh toán của tất cả HĐ tối thiểu 3 kỳ
                        var CountPayment = 0;
                        foreach (var item in lst.Data.LstLoanCustomer)
                        {
                            if (item.LstPaymentCustomer.Count() >= 3)
                            {
                                CountPayment = item.LstPaymentCustomer.Count();
                                break;
                            }
                            else
                            {
                                CountPayment = CountPayment + item.LstPaymentCustomer.Count();
                                if (CountPayment >= 3)
                                    break;
                            }


                        }
                        if (CountPayment < 3)
                            lstReLoan.Message = "Không đủ điều kiện do tổng kỳ thanh toán chưa đạt điều kiện";

                        int DayReLoan = Convert.ToInt32(_configuration["AppSettings:DayReLoan"]);

                        var CountDate = 0;
                        foreach (var item in lst.Data.LstLoanCustomer[0].LstPaymentCustomer)
                        {
                            if (item.CountDay > DayReLoan)
                            {
                                CountDate = Convert.ToInt32(item.CountDay);
                                break;
                            }
                        }
                        if (CountDate <= DayReLoan)
                        {
                            lstReLoan.IsAccept = 1;
                            if (lst.Data.LstLoanCustomer[0].LoanBriefID > 0)
                                lstReLoan.LoanBriefID = lst.Data.LstLoanCustomer[0].LoanBriefID;
                            else
                                lstReLoan.LoanBriefID = lst.Data.LstLoanCustomer[0].LoanId;
                            lstReLoan.Message = "Thành công!";
                        }
                        else
                        {
                            lstReLoan.Message = "Khách hàng có kỳ trả chậm " + CountDate + "!";
                        }
                    }
                    else
                    {
                        lstReLoan.Message = "Khách hàng chưa đủ điều kiện tái vay do đơn vay đang ở trạng thái " + lst.Data.LstLoanCustomer[0].StatusName;
                    }
                }
                else
                {
                    lstReLoan.Message = lst.Message;
                }

            }



            return lstReLoan;
        }
        public CheckReLoan.OutPut CheckReLoanV3(string access_token, CheckReLoan.Input param)
        {
            var obj = new DeferredPayment.Input
            {
                NumberCard = param.NumberCard,
                CustomerName = param.CustomerName
            };
            var url = _configuration["AppSettings:LMS"] + Constants.DeferredPayment;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(obj);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            //Add log
            var log = new LogCallApi
            {
                ActionCallApi = ActionCallApi.DeferredPayment.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = access_token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = 0,
                Input = body
            };
            log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            log.Status = (int)StatusCallApi.Success;
            log.Output = json;
            _dictionaryService.UpdateLogCallApi(access_token, log);
            DeferredPayment.Output lst = JsonConvert.DeserializeObject<DeferredPayment.Output>(json);
            CheckReLoan.OutPut lstReLoan = new CheckReLoan.OutPut();

            if (lst.Result == 1)
            {
                lstReLoan.Result = 1;
                if (lst.Data != null)
                {

                    if (lst.Data.LstLoanCustomer[0].Status == EnumLoanStatus.FINISH.GetHashCode())
                    {
                        //Kiểm tra tổng số kỳ thanh toán của tất cả HĐ tối thiểu 3 kỳ
                        //Phát sinh 1ky thanh toán trước tất toán ? 2 kỳ : 1 kỳ
                        var CountPayment = 0;
                        foreach (var item in lst.Data.LstLoanCustomer)
                        {
                            //if (item.LstPaymentCustomer.Count() >= 3)
                            //{
                            //    CountPayment = item.LstPaymentCustomer.Count();
                            //    break;
                            //}
                            //else
                            //{
                            //    //kiểm tra có 1 kỳ thanh toán trước tất toán tính là 2 kỳ
                            //    if (item.LstPaymentCustomer.Count() == 0)
                            //        CountPayment = CountPayment + 1;
                            //    else if (item.LstPaymentCustomer.Count() == 1)
                            //        CountPayment = CountPayment + 2;
                            //    else
                            //        CountPayment = CountPayment + item.LstPaymentCustomer.Count();
                            //    if (CountPayment >= 3)
                            //        break;
                            //}
                            CountPayment = CountPayment + item.LstPaymentCustomer.Count() + 1;
                            if (CountPayment >= 3)
                                break;


                        }
                        if (CountPayment < 3)
                        {
                            lstReLoan.Message = "Không đủ điều kiện do tổng kỳ thanh toán chưa đạt điều kiện";
                            return lstReLoan;
                        }


                        //Khoản vay gần nhất của KH không có kỳ nào quá hạn trên 30 ngày
                        int DayReLoan = Convert.ToInt32(_configuration["AppSettings:DayReLoan"]);

                        var CountDate = 0;
                        foreach (var item in lst.Data.LstLoanCustomer[0].LstPaymentCustomer)
                        {
                            if (item.CountDay > DayReLoan)
                            {
                                CountDate = Convert.ToInt32(item.CountDay);
                                break;
                            }
                        }
                        if (CountDate <= DayReLoan)
                        {
                            lstReLoan.IsAccept = 1;
                            if (lst.Data.LstLoanCustomer[0].LoanBriefID > 0)
                                lstReLoan.LoanBriefID = lst.Data.LstLoanCustomer[0].LoanBriefID;
                            else
                                lstReLoan.LoanBriefID = lst.Data.LstLoanCustomer[0].LoanId;
                            lstReLoan.Message = "Thành công!";
                        }
                        else
                        {
                            lstReLoan.Message = "Khách hàng có kỳ trả chậm " + CountDate + "!";
                        }
                    }
                    else
                    {
                        lstReLoan.Message = "Khách hàng chưa đủ điều kiện tái vay do đơn vay đang ở trạng thái " + lst.Data.LstLoanCustomer[0].StatusName;
                    }
                }
                else
                {
                    lstReLoan.Message = lst.Message;
                }

            }



            return lstReLoan;
        }

        public CalculatorMoneyInsurance.OutPut CalculatorMoneyInsurance(string access_token, CalculatorMoneyInsurance.InPut param)
        {
            try
            {
                var url = _configuration["AppSettings:LMS"] + Constants.CalculatorMoneyInsurance;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(param);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.CalculatorMoneyInsurance.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = body
                };
                _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                CalculatorMoneyInsurance.OutPut lst = JsonConvert.DeserializeObject<CalculatorMoneyInsurance.OutPut>(json);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public CheckBlackList.Output CheckBlackList(string access_token, string FullName, string NumberCard, string Phone, int loanbriefId)
        {
            var entity = new CheckBlackList.Input
            {
                FullName = FullName,
                Phone = Phone,
                NumberCard = NumberCard
            };
            var url = _configuration["AppSettings:LMS"] + Constants.CheckBlackList;
            var body = JsonConvert.SerializeObject(entity);
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            //Add log
            var log = new LogCallApi
            {
                ActionCallApi = ActionCallApi.BlackList.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = access_token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = loanbriefId,
                Input = body
            };
            log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            log.Status = (int)StatusCallApi.Success;
            log.Output = json;
            _dictionaryService.UpdateLogCallApi(access_token, log);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                CheckBlackList.Output lst = JsonConvert.DeserializeObject<CheckBlackList.Output>(json);
                return lst;
            }
            return null;
        }
        public PaymentLoanById.OutPut GetPaymentLoanById(int loanId)
        {
            var url = _configuration["AppSettings:LMS"] + String.Format(Constants.GetPaymentById, loanId);
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);
            var json = response.Content;

            PaymentLoanById.OutPut lst = JsonConvert.DeserializeObject<PaymentLoanById.OutPut>(json);
            return lst;
        }
        public InterestParam.OutputTopup InterestTopup(string access_token, InterestParam.InputTopup param)
        {
            var url = _configuration["AppSettings:LMS"] + Constants.InterestTopup;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(param);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            //Add log
            var log = new LogCallApi
            {
                ActionCallApi = ActionCallApi.IntersestTopUp.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = access_token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = param.LoanId,
                Input = body
            };
            log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            log.Status = (int)StatusCallApi.Success;
            log.Output = json;
            _dictionaryService.UpdateLogCallApi(access_token, log);
            InterestParam.OutputTopup lst = JsonConvert.DeserializeObject<InterestParam.OutputTopup>(json);

            return lst;
        }
        public InterestParam.ViewDataTopUp GenCalendarPayment(string access_token, InterestParam.InputTopup param)
        {
            var dicPaymentSchedule = new Dictionary<string, List<InterestParam.PaymentSchedule>>();
            var result = InterestTopup(access_token, param);
            var lst = new InterestParam.ViewDataTopUp();
            //var dataToup = new List<InterestParam.PaymentSchedule>();

            if (result != null && result.Data != null && result.Data.ListLoanPaymentSchedule != null && result.Data.ListLoanPaymentSchedule.Count > 0)
            {
                lst.MoneyFeeInsuranceOfCustomer = result.Data.MoneyFeeInsuranceOfCustomer;
                lst.MoneyInsuranceOfMaterialCovered = result.Data.MoneyInsuranceOfMaterialCovered;

                //foreach (var item in result.Data.ListLoanPaymentSchedule)
                //{
                //    if (item.PaymentSchedule != null && item.PaymentSchedule.Count > 0)
                //    {
                //        foreach (var item2 in item.PaymentSchedule)
                //        {
                //            var keyDic = item2.FromDate.ToString("dd/MM/yyyy") + "-" + item2.ToDate.ToString("dd/MM/yyyy");
                //            if (!dicPaymentSchedule.ContainsKey(keyDic))
                //                dicPaymentSchedule[keyDic] = new List<InterestParam.PaymentSchedule>();
                //            dicPaymentSchedule[keyDic].Add(item2);

                //        }
                //    }
                //}

                foreach (var item in result.Data.ListLoanPaymentSchedule)
                {
                    if (item.PaymentSchedule != null && item.PaymentSchedule.Count > 0)
                    {
                        //add dữ liệu vào dic
                        foreach (var item2 in item.PaymentSchedule)
                        {
                            var keyDic = item2.DaysPayable.ToString("dd/MM/yyyy");
                            if (!dicPaymentSchedule.ContainsKey(keyDic))
                                dicPaymentSchedule[keyDic] = new List<InterestParam.PaymentSchedule>();
                            dicPaymentSchedule[keyDic].Add(item2);

                        }
                    }
                }
                lst.DataTopUp = dicPaymentSchedule;
            }
            //if (dicPaymentSchedule.Keys.Count > 0)
            //{
            //foreach (var itemKey in dicPaymentSchedule.Keys)
            //{
            //    var data = dicPaymentSchedule[itemKey];
            //    var objPayment = new InterestParam.PaymentSchedule();
            //    objPayment.FromDate = data.First().FromDate;
            //    objPayment.ToDate = data.First().ToDate;
            //    objPayment.PayDate = data.First().PayDate;
            //    objPayment.Done = data.First().Done;
            //    objPayment.DaysPayable = data.First().DaysPayable;
            //    objPayment.MoneyOriginal = data.Sum(x => x.MoneyOriginal);
            //    objPayment.MoneyInterest = data.Sum(x => x.MoneyInterest);
            //    objPayment.MoneyService = data.Sum(x => x.MoneyService);
            //    objPayment.MoneyConsultant = data.Sum(x => x.MoneyConsultant);
            //    objPayment.OtherMoney = data.Sum(x => x.OtherMoney);
            //    dataToup.Add(objPayment);
            //}
            //}
            //lst.DataTopUp = dataToup;
            return lst;
        }

        public PayAmountForCustomer GetPayAmountForCustomer(string access_token, RequestPayAmountForCustomer entity)
        {
            var url = _configuration["AppSettings:LMS"] + Constants.GetPayAmountForCustomer;
            var body = JsonConvert.SerializeObject(entity);
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                var response = client.Execute<ResponseCheckBillWaterAI>(request);
                //Lưu lại log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.PayAmountForCustomer.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = access_token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = body,
                    CreatedAt = DateTime.Now,
                };
                log.Id = _dictionaryService.AddLogCallApi(access_token, log);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    log.Status = (int)StatusCallApi.Success;
                    log.Output = response.Content;
                    log.ModifyAt = DateTime.Now;
                    _dictionaryService.UpdateLogCallApi(access_token, log);
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<PayAmountForCustomer>(response.Content);
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public ResponeReportDebt GetReportDebt(string access_token, RequestReportDebt entity)
        {
            var url = _configuration["AppSettings:LMS"] + Constants.GetReportDebt;
            var body = JsonConvert.SerializeObject(entity);
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                var response = client.Execute<ResponeReportDebt>(request);
                //Lưu lại log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.GetReportDebt.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = access_token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = body,
                    CreatedAt = DateTime.Now,
                };
                log.Id = _dictionaryService.AddLogCallApi(access_token, log);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    log.Status = (int)StatusCallApi.Success;
                    log.Output = response.Content;
                    log.ModifyAt = DateTime.Now;
                    _dictionaryService.UpdateLogCallApi(access_token, log);
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponeReportDebt>(response.Content);
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public PrematureInterest GetPayAmountForCustomerByLoanID(string access_token, RequestPrematureInterest entity, int loanbriefId)
        {
            var url = _configuration["AppSettings:LMS"] + Constants.GetPayAmountForCustomerByLoanID;
            var body = JsonConvert.SerializeObject(entity);
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                //Lưu lại log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.GetPayAmountForCustomerByLoanID.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = access_token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanbriefId,
                    Input = body,
                    CreatedAt = DateTime.Now
                };
                var response = client.Execute<PrematureInterest>(request);
                log.Id = _dictionaryService.AddLogCallApi(access_token, log);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    log.Status = (int)StatusCallApi.Success;
                    log.Output = response.Content;
                    log.ModifyAt = DateTime.Now;
                    _dictionaryService.UpdateLogCallApi(access_token, log);
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<PrematureInterest>(response.Content);
                    return objData;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public ListLoanDebtOutput GetListLoanDebt(string access_token, RequestListLoanDebt param)
        {
            var url = _configuration["AppSettings:LMS"] + Constants.GetListLoanHubDebt;
            var body = JsonConvert.SerializeObject(param);
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                //Lưu lại log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.GetListLoanHubDebt.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = access_token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = body,
                    CreatedAt = DateTime.Now,
                };
                log.Id = _dictionaryService.AddLogCallApi(access_token, log);
                var response = client.Execute<ListLoanDebtOutput>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    log.Status = (int)StatusCallApi.Success;
                    log.Output = response.Content;
                    log.ModifyAt = DateTime.Now;
                    _dictionaryService.UpdateLogCallApi(access_token, log);
                    var lst = JsonConvert.DeserializeObject<ListLoanDebtOutput>(response.Content);
                    return lst;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public ResponeAddBlacklist AddBlackList(string access_token, RequestAddBlacklist param)
        {
            {
                var url = _configuration["AppSettings:LMS"] + Constants.AddBlacklist;
                var body = JsonConvert.SerializeObject(param);
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                try
                {
                    //Lưu lại log
                    var log = new LogCallApi
                    {
                        ActionCallApi = ActionCallApi.AddBlacklist.GetHashCode(),
                        LinkCallApi = url,
                        TokenCallApi = access_token,
                        Status = (int)StatusCallApi.CallApi,
                        LoanBriefId = param.LoanCreditId,
                        Input = body,
                        CreatedAt = DateTime.Now,
                    };
                    log.Id = _dictionaryService.AddLogCallApi(access_token, log);
                    var response = client.Execute<ResponeAddBlacklist>(request);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        log.Status = (int)StatusCallApi.Success;
                        log.Output = response.Content;
                        log.ModifyAt = DateTime.Now;
                        _dictionaryService.UpdateLogCallApi(access_token, log);
                        var lst = JsonConvert.DeserializeObject<ResponeAddBlacklist>(response.Content);
                        return lst;
                    }
                }
                catch (Exception ex)
                {
                }
                return null;
            }
        }

        public CheckReLoan.TopupOutPut CheckTopupOto(string access_token, int loanBriefId, CheckReLoan.Input param)
        {
            var obj = new DeferredPayment.Input
            {
                NumberCard = param.NumberCard,
                CustomerName = param.CustomerName
            };
            if (!obj.CustomerName.IsNormalized())
                obj.CustomerName = obj.CustomerName.Normalize();
            var url = _configuration["AppSettings:LMS"] + Constants.DeferredPayment;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(obj);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            //Add log
            var log = new LogCallApi
            {
                ActionCallApi = ActionCallApi.DeferredPayment.GetHashCode(),
                LinkCallApi = url,
                TokenCallApi = access_token,
                Status = (int)StatusCallApi.CallApi,
                LoanBriefId = loanBriefId,
                Input = body,
                CreatedAt = DateTime.Now
            };
            log.Id = _dictionaryService.AddLogCallApi(access_token, log);
            IRestResponse response = client.Execute(request);
            var json = response.Content;
            log.Status = (int)StatusCallApi.Success;
            log.Output = json;
            _dictionaryService.UpdateLogCallApi(access_token, log);
            DeferredPayment.Output lst = JsonConvert.DeserializeObject<DeferredPayment.Output>(json);
            CheckReLoan.TopupOutPut lstTopup = new CheckReLoan.TopupOutPut();

            if (lst != null && lst.Result == 1)
            {
                lstTopup.IsAccept = 0;
                if (lst.Data != null && lst.Data.LstLoanCustomer != null)
                {

                    //Lấy ra những đơn đang vay và là gói oto và có đơn đang vay
                    //Kiểm tra điều kiện topup
                    // 1- khoản vay oto đang còn hiệu lực
                    var lstData = lst.Data.LstLoanCustomer.Where(x => (x.Status == (int)EnumStatusLMS.Disbursed || x.Status == (int)EnumStatusLMS.InterestFeeDebt || x.Status == (int)EnumStatusLMS.PaymentNow)
                    && (x.ProductID == (int)EnumProductCredit.OtoCreditType_CC || x.ProductID == (int)EnumProductCredit.OtoCreditType_KCC)).ToList();

                    if (lstData != null && lstData.Count > 0)
                    {
                        var isKyThanhToan = true;
                        var isNoQuaHan = true;
                        int noQuaHan = Convert.ToInt32(_configuration["AppSettings:NoQuaHan"]);
                        var totalMoneyCurrent = 0L;
                        foreach (var item in lstData)
                        {
                            // 2- Kiểm tra các đơn đang vay đã thanh toán tối thiểu 1 kỳ
                            if (item.LstPaymentCustomer.Count() == 0)
                            {
                                isKyThanhToan = false;
                                break;
                            }
                            // 3- Lịch sử thanh toán trên các khoản đang vay hiện tại không nợ quá hạn trên 30 ngày                           
                            foreach (var itemPayment in item.LstPaymentCustomer)
                            {
                                if (itemPayment.CountDay > noQuaHan)
                                {
                                    isNoQuaHan = false;
                                    break;
                                }
                            }
                            if (!isNoQuaHan)
                                break;

                            //TotalMoneyCurrent 
                            totalMoneyCurrent = totalMoneyCurrent + item.TotalMoneyCurrent;
                        }
                        if (!isKyThanhToan)
                            lstTopup.Message = "Không đủ điều kiện do khách hàng có khoản vay chưa thanh toán kỳ nào";
                        if (!isNoQuaHan)
                            lstTopup.Message = "Không đủ điều kiện do khách hàng có kỳ trả chậm > " + noQuaHan + " ngày";
                        if (isKyThanhToan && isNoQuaHan)
                        {
                            lstTopup.IsAccept = 1;
                            if (lstData[0].LoanBriefID > 0)
                                lstTopup.LoanBriefId = lstData[0].LoanBriefID;
                            else
                                lstTopup.LoanBriefId = lstData[0].LoanId;

                            lstTopup.TotalMoneyCurrent = totalMoneyCurrent;
                        }

                    }
                    else
                    {
                        lstTopup.Message = "Khách hàng không có đơn vay ô tô nào ở trạng thái đang vay";
                    }
                }
                else
                {
                    lstTopup.Message = lst.Message;
                }

            }



            return lstTopup;
        }

        public DebtRestructuring.Output DebtRestructuring(string access_token, DebtRestructuring.Input param)
        {
            try
            {
                var url = _configuration["AppSettings:LMS"] + Constants.DebtRestructuring;
                var body = JsonConvert.SerializeObject(param);
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "8228459735");
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);

                var response = client.Execute<ResponeReportDebt>(request);
                //Lưu lại log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.DebtRestructuring.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = access_token,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = body,
                    CreatedAt = DateTime.Now,
                };
                log.Id = _dictionaryService.AddLogCallApi(access_token, log);
                //if (response.StatusCode == HttpStatusCode.OK)
                //{
                log.Status = (int)StatusCallApi.Success;
                log.Output = response.Content;
                log.ModifyAt = DateTime.Now;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<DebtRestructuring.Output>(response.Content);
                return result;
                //}
            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }

        public ReponseDefaultLMS CloseLoan(ResquestCloseLoan entity, int loanBriefId)
        {
            try
            {
                var url = _configuration["AppSettings:LMS_V2"] + Constants.CloseLoan;
                var body = JsonConvert.SerializeObject(entity);
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", Constants.TokenLms);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Lưu lại log
                var log = new LogCloseLoan
                {
                    Link = url,
                    Token = Constants.TokenLms,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = loanBriefId,
                    Input = body,
                    CreateAt = DateTime.Now,
                };
                log.Id = _logCloseLoanService.Add(log);
                var response = client.Execute<ReponseDefaultLMS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    log.Status = (int)StatusCallApi.Success;
                    log.Output = response.Content;
                    log.ModifyAt = DateTime.Now;
                    _logCloseLoanService.Update(log);
                    var lst = JsonConvert.DeserializeObject<ReponseDefaultLMS>(response.Content);
                    return lst;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}