﻿using LOS.Common.Extensions;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface ILosService
    {
        List<RecordingItem> GetRecordingByUser(string access_token, string fromDate, string toDate, string line);
        F2LTelesales GetF2LTelesales(string access_token, int userId);
    }
    public class LosService : ILosService
    {
        private readonly IConfiguration _configuration;
        private readonly IDictionaryService _dictionaryService;
        public LosService(IConfiguration configuration, IDictionaryService dictionaryService)
        {
            _configuration = configuration;
            _dictionaryService = dictionaryService;
        }

        public List<RecordingItem> GetRecordingByUser(string access_token, string fromDate, string toDate, string line)
        {
            try
            {
                var url = _configuration["AppSettings:DomainAPILOS"] + Constants.GetRecordingByUser + "?fromDate=" + fromDate + "&toDate=" + toDate + "&line=" + line;
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Timeout = 5000;
                request.AddHeader("Content-Type", "application/json");
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.GetRecordingByUser.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = ""
                };
                _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ResponceRecordingModel data = JsonConvert.DeserializeObject<ResponceRecordingModel>(json);
                    return data.data;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public F2LTelesales GetF2LTelesales(string access_token, int userId)
        {
            try
            {
                var url = _configuration["AppSettings:DomainAPILOS"] + Constants.GetF2LTelesales + "?telesaleId=" + userId;
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Timeout = 5000;
                request.AddHeader("Content-Type", "application/json");
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.F2LTelesales.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = ""
                };
                _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ResponseF2LTelesales data = JsonConvert.DeserializeObject<ResponseF2LTelesales>(json);
                    return data.data;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
