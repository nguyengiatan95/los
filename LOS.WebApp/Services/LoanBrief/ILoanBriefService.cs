﻿using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.DTOs.Loanbrief;
using LOS.DAL.EntityFramework;
using LOS.DAL.Object;
using LOS.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static LOS.Common.Models.Request.LoanBriefReq;
using static LOS.DAL.Models.Request.ChangePhone.ChangePhone;
using static LOS.DAL.Models.Request.LoanBrief.LoanBrief;

namespace LOS.WebApp.Services
{
    public interface ILoanBriefService
    {
        List<LoanBriefForBrower> Search(string access_token, string param, ref int recordTotals);
        List<LoanBriefDetail> RemarketingSearch(string access_token, string param, ref int recordTotals);
        List<LoanBriefDetail> GetLoanBriefByPhone(string access_token, string phone);
        LoanBriefDetail GetLoanBriefProcessingByPhone(string access_token, string phone);
        LoanBriefDetail GetLoanBriefProcessing(string access_token, string phone, string nationalCard, int loanBriefId);
        Task<LoanBriefInitDetail> GetLoanBriefById(string access_token, int id);
        List<LoanBriefCancell> LoanCancelSearch(string access_token, string param, ref int recordTotals);
        Task<int> InitLoanBrief(string access_token, LoanBriefInit entity);
        Task<int> AddLoanTopup(string access_token, LoanBriefInit entity);
        int CloneLoanBrief(string access_token, LoanBriefInit entity);
        int UpdateLoanBrief(string access_token, LoanBriefInit entity, int id);
        DefaultResponse<object> UpdateBoundTelesale(string access_token, BoundTelesaleReq entity);
        DefaultResponse<object> UpdateBorrowCavet(string access_token, int loanBriefId);
        DefaultResponse<object> UpdateApprove(string access_token, BoundTelesaleReq entity);
        DefaultResponse<object> UpdateLender(string access_token, LenderReq entity);
        DefaultResponse<object> UpdateIsUpload(string access_token, IsUploadReq entity);
        DefaultResponse<object> UpdateInsurence(string access_token, LenderReq entity);
        List<LoanBriefDetail> GetLoanTelesaleId(string access_token, int TelesaleId);
        List<LoanBriefNoteDetail> GetNoteByLoanID(string access_token, int loanBriefId);
        int CreateLoanNote(string access_token, LoanBriefNoteDTO entity);
        int ScriptUpdateLoanBrief(string access_token, LoanBriefScriptItem entity, int id);
        int UpdateInfoForRequestAI(string access_token, LoanBriefScriptItem entity, int id);
        int AddLoanBriefNote(string access_token, LoanBriefNote entity);
        int AddLoanBriefLog(string access_token, LoanBriefLog entity);
        int UpdateLoanBriefStauts(string access_token, LoanBriefInitDetail entity);
        int PushLoanBrief(string access_token, LoanBriefInitDetail entity);
        int ReturnLoanBrief(string access_token, LoanBriefInitDetail entity);
        int CancelLoanBrief(string access_token, LoanBriefInitDetail entity);
        List<LoanBriefDetail> GetLoanBriefSchedule(string access_token, int telesale, DateTime dateTime);
        List<LoanBriefDetail> GetLoanBriefHandle(string access_token, int telesale, DateTime dateTime);
        #region for hub
        List<LoanBriefDetail> GetDataForHub(string access_token, string param, ref int recordTotals);
        int UpdateHub(string access_token, LoanBriefInit entity);
        int AddLoanBriefFile(string access_token, LoanBriefFiles entity);
        List<LoanBriefFileDetail> GetLoanBriefFile(string access_token, int LoanBriefId, int documentType);
        List<LoanBriefFileDetail> GetLoanBriefFileByLoanId(string access_token, int LoanBriefId);
        Task<List<int>> GetLoanBriefFileByLoanId(string access_token, List<int> entity);
        //int DeleteLoanBriefFile(string access_token, int LoanBriefFileId);

        int DistributingStaffHub(string access_token, LoanBriefInitDetail entity);
        #endregion
        int UpdateDevice(string access_token, LoanBriefInitDetail entity);
        List<LoanBriefSearchDetail> SearchLoanAll(string access_token, string param, ref int recordTotals);
        int ReGetLocation(string access_token, LoanBriefInitDetail entity);
        DefaultResponse<object> AddScheduleTimeCall(string access_token, ScheduleTimeReq entity);
        int AddReLoan(string access_token, LoanBriefScriptItem entity);
        int UpdateRefPhoneIsNullRequestAI(string access_token, UpdateRefPhoneLoanBriefRelationship entity);
        bool UpdateCountCall(string access_token, int loanBriefId);
        bool UpdateCountCallV2(string access_token, UpdateCountCallReq entity);
        CountLoanTLSDetail CountLoanTLS(string access_token, int telesaleId, string teamTelesales);
        int UpdateFirstHubFeedback(string access_token, int loanbriefId);
        int UpdateFirstTelesaleFeedback(string access_token, int loanbriefId);
        int UpdateLastTelesaleFeedback(string access_token, int loanbriefId);
        LoanBriefDetailNew GetLoanBriefByIdNew(string access_token, int id);
        int UpdateStatusDevice(string access_token, LoanBriefInitDetail entity);
        bool RemoveFile(string access_token, RemoveFileItem entity);
        bool ReActiveDevice(string access_token, int loanbriefId);
        int ScriptUpdateLoanBriefV2(string access_token, LoanBriefScriptItemV2 entity, int id);
        bool SaveStatusTelesales(string access_token, DAL.Object.RequestSaveStatusTelesales entity);
        LoanBrief GetLoanBriefByCodeId(string access_token, int CodeId);
        Task<int> ReInitLoanBrief(string access_token, LoanBrief entity);
        Task<LoanBriefPropertyDTO> GetLoanbriefProperty(string access_token, int loanbriefId);
        Task<int> AddLoanBriefHistory(string access_token, LoanBriefHistory entity);
        Task<int> InitLoanBriefV2(string access_token, LoanBrief entity);
        Task<int> UpdateLoanBriefV2(string access_token, LoanBrief entity, int id);
        Task<int> UpdateScriptTelesale(string access_token, LoanBrief entity, int id);
        Task<int> CreateLoanbriefScript(string access_token, LoanBrief entity);
        Task<int> CountLoanByApproveId(string access_token, int ApproveId);
        Task<int> HubEmployeeChange(string access_token, LoanBriefInitDetail entity);
        Task<List<LoanBrief>> GetListLoanFromIdAndStatus(string access_token, List<int> loanbriefIds, int Status);
        Task<bool> ChangeHub(string access_token, ChangeHubSupportItem entity);
        Task<bool> ChangeHubAndEmployee(string access_token, ChangeHubSupportItem entity);
        int UpdateFirstStaffHubFeedback(string access_token, int loanbriefId);
        bool UpdateLabelReBorrow(string access_token, UpdateReBorrow entity);
        bool AddLogChangeBoundTelesale(string access_token, TelesaleLoanbrief entity);
        DefaultResponse<object> AddAndEditSchedule(string access_token, ScheduleTimeHubReq entity);
        Task<List<ScheduleTime>> SearchSchedule(string access_token, GetListScheduleTimeHubReq entity);
        Task<List<ScheduleTime>> GetListScheduleByLoan(string access_token, int loanBriefId);
        Task<int> RequestAIV3(string access_token, LoanBrief entity, int id);
        Task<int> RequestRefphoneV3(string access_token, LoanBrief entity, int id);
        Task<int> UpdateScriptTelesaleV2(string access_token, LoanBrief entity, int id);
        Task<int> CreateLoanbriefScriptV2(string access_token, LoanBrief entity);
        bool UpdateJsonInfoFamilyKalapa(string access_token, int loanbriefId, InfomationFamilyKalapa entity);
        List<LoanBriefDetail> SearchRemaketingCar(string access_token, string param, ref int recordTotals);
        DefaultResponse<object> UpdateHubId(string access_token, BoundTelesaleReq entity);
        string GetLatLng(string access_token, int typeGetLatLng, int loanbriefId);
        int UpdateHubEmployeeCallFirst(string access_token, int loanbriefId);
        int UpdateFirstProcessingTime(string access_token, int loanbriefId);
        Task<List<TimeProcessingLoanBrief>> CalculateTimeProcessingLoanBrief(string access_token, List<int> entity, int groupId);
        bool SaveNextStep(string access_token, DAL.Object.LoanStatusDetail.NextStepLoanHub entity);
        bool SaveChangeStatusApprover(string access_token, DAL.Object.LoanStatusDetail.NextStepLoanHub entity);
        List<LoanBriefDetailDebt> ListLoanBriefByCodeId(string access_token, GetLoanByCode lstId);
        Task<List<LoanBriefRelationship>> GetLoanbriefRelationship(string access_token, int loanbriefId);
        List<LoanBriefDetail> GetLoanDepartment(string access_token, string param, ref int recordTotals);
        Task<bool> ToolChangePipeline(string access_token, int loanBriefId, int actionPipeline);
        List<LoanbriefSearch> SearchLoanBrief(string access_token, string param, ref int recordTotals);
        int UpdateReMarketingLoanbriefId(string access_token, int loanbriefId);
        Task<int> CreateLoanBriefChild(string access_token, CreateLoanBriefChild entity);
        int ProposeExceptions(string access_token, DAL.Object.Loanbrief.ProposeExceptions entity);
        bool UpdateFeedback(string access_token, DAL.Object.LoanBriefFeedback entity);
        Task<LoanBriefPrematureInterest> GetLoanBriefByPhoneOrNationalCard(string access_token, string textSearch);
        Task<LoanBriefBasicInfo> GetBasicInfo(string access_token, int loanbriefId);
        Task<LoanBriefForDetail> GetLoanBriefForDetail(string access_token, int id);
        LoanBriefForAppraiser GetLoanBriefForAppraiser(string access_token, int id);
        Task<int> TotalLoanBriefDisbursementInMonth(string access_token, int coordinatorUserId);
        Task<bool> AddLogDistributionUser(string access_token, LogDistributionUser entity);
        bool CheckLoanTopup(string access_token, string nationalCard, string phone, int loanbriefId);
        Task<LoanBriefDetail> GetLoanBriefBorrowing(string access_token, string phone, string nationalCard);
        Task<List<LoanBriefDetail>> GetListLoanTopupCar(string access_token, int customerId);
        List<LoanBriefForBrower> SearchClickTopTls(string access_token, string param, ref int recordTotals);
        Task<LoanBriefDetail> GetLoanBriefByNationalCard(string access_token, string nationalCard, int loanbriefId);
        Task<LoanBriefContractFinancialAgreement> GetContractFinancialAgreement(string access_token, int loanbriefId);
        Task<List<LogDistributionUser>> GetLogDistributionUser(string access_token, GetLogDistributionUserModel entity);
        Task<LoanBriefCommittedPaper> GetCommittedPaper(string access_token, int loanbriefId);
        Task<LoanInfoCompare> GetLoanInfoCompare(string access_token, int loanbriefId);
        Task<List<LoanBriefFileDetail>> GetLoanBriefFileByDocumentMulti(string access_token, int LoanBriefId, List<int> documentType);
        Task<LoanBriefDetail> GetLoanBriefDisbursed(string access_token, string phone, string nationalCard);
        Task<int> InitDebtRevolvingLoan(string access_token, LoanBrief entity);
        Task<bool> CheckDebtRevolvingLoan(string access_token, int loanBriefId);
        Task<ResponseService> ReEsignCustomer(string access_token, DAL.Object.Tool.ToolReq.ReEsignReq req);
        List<int> ChangeSupportAllClickTopTls(string access_token, string param);
        List<int> ChangeSupportAllSearch(string access_token, string param);
        Task<bool> AddRangeLoanBriefNote(string access_token, List<LoanBriefNote> entitys);
        Task<bool> AddRangeLogDistributionUser(string access_token, List<LogDistributionUser> entitys);
        DefaultResponse<object> UpdateRangeBoundTelesale(string access_token, RangeBoundTelesaleReq entity);

        Task<List<DocumentException>> GetDocumentException(string access_token, int parentId);
        Task<ProposeExceptions> GetProposeExceptions(string access_token, int loanBriefId);
        int AddProposeExceptions(string access_token, ProposeExceptionReq entity);
        List<LoanCancelMomo> GetLoanCancelByMomo(string access_token, LoanBriefForDetail entity);
        Task<bool> CheckLoanProcessing(string access_token, string phone);

        Task<List<LoanByPhoneChange>> GetLoanByPhoneChange(string access_token, string phone);
        Task<LoanTransactionsSecured> GetLoanTransactionsSecured(string access_token, int loanBriefId);
        Task<double> GetTotalLoanAmountTranSactionSecured(string access_token, int customerId);

        bool UpdateTransactionState(string access_token, UpdateTransactionStateReq entity);

        Task<List<LoanBriefCheckCavetDetail>> GetLoanToolCheckCavet(string access_token, string search);

        Task<LoanDisplayDocHub> CheckLoanDisplayDocHubEmp(string access_token, string phone,string nationalCard);
        Task<bool> CheckLoanAssign(string access_token, int loanBriefId, string phone, int userId);

        List<LogPushToCisco> PushToCiscoSearch(string access_token, string param, ref int recordTotals);
        Task<int> AddLogPushToCisco(string access_token, LogPushToCisco entity);
    }
}
