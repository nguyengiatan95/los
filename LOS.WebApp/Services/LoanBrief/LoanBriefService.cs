﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.DTOs.Loanbrief;
using LOS.DAL.EntityFramework;
using LOS.DAL.Models.Request.ChangePhone;
using LOS.DAL.Object;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;
using static LOS.Common.Models.Request.LoanBriefReq;
using static LOS.DAL.Models.Request.ChangePhone.ChangePhone;
using static LOS.DAL.Models.Request.LoanBrief.LoanBrief;

namespace LOS.WebApp.Services
{
    public class LoanBriefService : BaseServices, ILoanBriefService
    {
        public LoanBriefService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public List<LoanBriefDetail> GetLoanBriefByPhone(string access_token, string phone)
        {
            var data = new List<LoanBriefDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LoanBriefDetail>>>(string.Format(Constants.LOANBRIEF_GET_BYPHONE_ENDPOINT, phone), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200 && response.Result.data.Count > 0)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public LoanBriefDetail GetLoanBriefProcessingByPhone(string access_token, string phone)
        {
            var data = new LoanBriefDetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LoanBriefDetail>>(string.Format(Constants.LOANBRIEF_GET_PROCESSING_BYPHONE_ENDPOINT, phone), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public LoanBriefDetail GetLoanBriefProcessing(string access_token, string phone, string nationalCard, int loanBriefId)
        {
            var data = new LoanBriefDetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LoanBriefDetail>>(string.Format(Constants.LOANBRIEF_GET_PROCESSING_ENDPOINT, phone, nationalCard, loanBriefId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<LoanBriefForBrower> Search(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefForBrower> data = new List<LoanBriefForBrower>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefForBrower>>>(Constants.SEARCH_LOANBRIEF_ENDPOINT + param, access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/Search Exception");
                return null;
            }
            return data;
        }

        public List<LoanBriefDetail> RemarketingSearch(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefDetail> data = new List<LoanBriefDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>>(Constants.REMARKETING_SEARCH_ENDPOINT + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/RemarketingSearch Exception");
                return null;
            }
            return data;
        }

        public async Task<int> InitLoanBrief(string access_token, LoanBriefInit entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_INIT_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/InitLoanBrief Exception");
            }
            return 0;
        }
        public async Task<LoanBriefInitDetail> GetLoanBriefById(string access_token, int id)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanBriefInitDetail>>(string.Format(Constants.LOANBRIEF_GET_ENDPOINT, id), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public List<LoanBriefCancell> LoanCancelSearch(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefCancell> data = new List<LoanBriefCancell>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefCancell>>>(Constants.LOANCANCELL_SEARCH_ENDPOINT + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/LoanCancellSearch Exception");
                return null;
            }
            return data;
        }

        public int UpdateLoanBrief(string access_token, LoanBriefInit entity, int id)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_ENDPOINT, id), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public int ScriptUpdateLoanBrief(string access_token, LoanBriefScriptItem entity, int id)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_SCRIPT_ENDPOINT, id), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        public int UpdateInfoForRequestAI(string access_token, LoanBriefScriptItem entity, int id)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_FOR_REQUEST_AI_ENDPOINT, id), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public int AddLoanBriefNote(string access_token, LoanBriefNote entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_ADD_NOTE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddLoanBriefNote Exception");
            }
            return 0;
        }

        public int CloneLoanBrief(string access_token, LoanBriefInit entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_CLONE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ACloneLoanBrief Exception");
            }
            return 0;
        }

        public int AddLoanBriefLog(string access_token, LoanBriefLog entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_ADD_LOG_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddLoanBriefLog Exception");
            }
            return 0;
        }

        public int UpdateLoanBriefStauts(string access_token, LoanBriefInitDetail entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_UPDATE_STATUS_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;

        }

        public DefaultResponse<object> UpdateBoundTelesale(string access_token, BoundTelesaleReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.BOUND_TELESALE_ID_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL).Result;
                return response;
            }
            catch (Exception ex)
            {
                DefaultResponse<object> def = new DefaultResponse<object>();
                def.meta = new Meta(500, "internal server error");
                return def;
            }
        }

        public DefaultResponse<object> UpdateApprove(string access_token, BoundTelesaleReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.APPROVE_ID_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL).Result;
                return response;
            }
            catch (Exception ex)
            {
                DefaultResponse<object> def = new DefaultResponse<object>();
                def.meta = new Meta(500, "internal server error");
                return def;
            }
        }

        public DefaultResponse<object> UpdateLender(string access_token, LenderReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.LENDER_ID_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL).Result;
                return response;
            }
            catch (Exception ex)
            {
                DefaultResponse<object> def = new DefaultResponse<object>();
                def.meta = new Meta(500, "internal server error");
                return def;
            }
        }

        public DefaultResponse<object> UpdateIsUpload(string access_token, IsUploadReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.ISUPLOADSTATE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL).Result;
                return response;
            }
            catch (Exception ex)
            {
                DefaultResponse<object> def = new DefaultResponse<object>();
                def.meta = new Meta(500, "internal server error");
                return def;
            }
        }

        public DefaultResponse<object> UpdateInsurence(string access_token, LenderReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.INSURENCE_ID_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL).Result;
                return response;
            }
            catch (Exception ex)
            {
                DefaultResponse<object> def = new DefaultResponse<object>();
                def.meta = new Meta(500, "internal server error");
                return def;
            }
        }

        public List<LoanBriefDetail> GetLoanTelesaleId(string access_token, int TelesaleId)
        {
            var data = new List<LoanBriefDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LoanBriefDetail>>>(string.Format(Constants.GET_LOANBRIEF_BYTELESALE_ENDPOINT, TelesaleId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200 && response.Result.data.Count > 0)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<LoanBriefNoteDetail> GetNoteByLoanID(string access_token, int loanBriefId)
        {
            var data = new List<LoanBriefNoteDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LoanBriefNoteDetail>>>(string.Format(Constants.GET_LOANBRIEFNOTE_BYLOANID_ENDPOINT, loanBriefId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200 && response.Result.data.Count > 0)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public int CreateLoanNote(string access_token, LoanBriefNoteDTO entity)
        {
            int Result = 0;
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.CREATE_LOANNOTE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    Result = (int)response.Result.data;
            }
            catch (Exception ex)
            {
                return Result;
            }
            return Result;
        }

        public int PushLoanBrief(string access_token, LoanBriefInitDetail entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_PUSH_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;

        }

        public int ReturnLoanBrief(string access_token, LoanBriefInitDetail entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_RETURN_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;

        }

        public int CancelLoanBrief(string access_token, LoanBriefInitDetail entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_CANCEL_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;

        }

        public List<LoanBriefDetail> GetLoanBriefSchedule(string access_token, int telesale, DateTime dateTime)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LoanBriefDetail>>>(string.Format(Constants.LOANBRIEF_SCHEDULE_ENDPOINT, telesale, dateTime.ToString("yyyy/MM/dd")), access_token, Constants.WEB_API_URL);
                if (response.Result != null && response.Result.meta.errorCode == 200)
                    return response.Result.data;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LoanBriefDetail> GetLoanBriefHandle(string access_token, int telesale, DateTime dateTime)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LoanBriefDetail>>>(string.Format(Constants.LOANBRIEF_HANDLE_ENDPOINT, telesale, dateTime.ToString("yyyy/MM/dd")), access_token, Constants.WEB_API_URL);
                if (response.Result != null && response.Result.meta.errorCode == 200)
                    return response.Result.data;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #region for hub
        public List<LoanBriefDetail> GetDataForHub(string access_token, string param, ref int recordTotals)
        {
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>>(Constants.LOANBRIEF_DATA_FOR_HUB_ENDPOINT + param, access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        return response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/Search Exception");
            }
            return null;
        }
        public int UpdateHub(string access_token, LoanBriefInit entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_UPDATE_HUB_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL).Result;
                if (response.meta.errorCode == 200)
                    return (int)response.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public int AddLoanBriefFile(string access_token, LoanBriefFiles entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_ADD_FILE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddLoanBriefFile Exception");
            }
            return 0;
        }
        public List<LoanBriefFileDetail> GetLoanBriefFile(string access_token, int LoanBriefId, int documentType)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LoanBriefFileDetail>>>(string.Format(Constants.LOANBRIEF_GET_FILE_ENDPOINT, LoanBriefId, documentType), access_token, Constants.WEB_API_URL);
                if (response.Result != null && response.Result.meta.errorCode == 200)
                    return response.Result.data;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<LoanBriefFileDetail> GetLoanBriefFileByLoanId(string access_token, int LoanBriefId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LoanBriefFileDetail>>>(string.Format(Constants.LOANBRIEF_GET_FILE_BY_LOANID_ENDPOINT, LoanBriefId), access_token, Constants.WEB_API_URL);
                if (response.Result != null && response.Result.meta.errorCode == 200)
                    return response.Result.data;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //public int DeleteLoanBriefFile(string access_token, int LoanBriefFileId)
        //{
        //    try
        //    {
        //        var response = DeleteAsync<DefaultResponse<object>>(string.Format(Constants.LOANBRIEF_DELETE_FILE_ENDPOINT, LoanBriefFileId), access_token);
        //        if (response.Result.meta.errorCode == 200)
        //            return (int)response.Result.data;
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex, "LoanBrief/DeleteLoanBriefFile Exception");
        //    }
        //    return 0;
        //}

        public int DistributingStaffHub(string access_token, LoanBriefInitDetail entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_DISTRIBUTING_STAFF_HUB_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;

        }
        #endregion

        public int UpdateDevice(string access_token, LoanBriefInitDetail entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_UPDATE_DEVICE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;

        }

        public List<LoanBriefSearchDetail> SearchLoanAll(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefSearchDetail> data = new List<LoanBriefSearchDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefSearchDetail>>>(Constants.SEARCH_LOANBRIEF_ALL + param, access_token, Constants.WEB_API_URL);
                if (response.Result != null && response.Result.meta != null && response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data != null && response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/SearchLoanAll Exception");
                return null;
            }
            return data;
        }

        public int ReGetLocation(string access_token, LoanBriefInitDetail entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_RE_GET_LOCATION_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;

        }

        public DefaultResponse<object> AddScheduleTimeCall(string access_token, ScheduleTimeReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.ADD_SCHEDULE_TIME, access_token, stringContent, Constants.WEB_API_URL).Result;
                return response;
            }
            catch (Exception ex)
            {
                DefaultResponse<object> def = new DefaultResponse<object>();
                def.meta = new Meta(500, "internal server error");
                return def;
            }
        }

        public int AddReLoan(string access_token, LoanBriefScriptItem entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.ADD_RELOAN), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public int UpdateRefPhoneIsNullRequestAI(string access_token, UpdateRefPhoneLoanBriefRelationship entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_FOR_REFPHONE_ISNULL_AI_ENDPOINT), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public bool UpdateCountCall(string access_token, int loanBriefId)
        {
            try
            {

                var response = GetAsync<DefaultResponse<object>>(string.Format(Constants.LOANBRIEF_UPDATE_COUNTCALL, loanBriefId), access_token, Constants.WEB_API_URL);
                if (response != null && response.Result != null && response.Result.meta != null)
                    return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Format("LoanBrief/UpdateCountCall Service {0} Exception", loanBriefId));
            }
            return false;
        }

        public bool UpdateCountCallV2(string access_token, UpdateCountCallReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEFV2_UPDATE_COUNTCALL), access_token, stringContent, Constants.WEB_API_URL);
                return (response.Result.meta.errorCode == 200);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Format("LoanBrief/UpdateCountCallV2 Service {0} Exception", entity.loanbriefId));
            }
            return false;
        }
        public int UpdateFirstHubFeedback(string access_token, int loanbriefId)
        {
            try
            {
                var obj = new
                {
                    LoanbriefId = loanbriefId
                };
                var json = JsonConvert.SerializeObject(obj);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_FIRST_HUB_FEEDBACK_ENDPOINT), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public int UpdateFirstTelesaleFeedback(string access_token, int loanbriefId)
        {
            try
            {
                var obj = new
                {
                    LoanbriefId = loanbriefId
                };
                var json = JsonConvert.SerializeObject(obj);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_FIRST_TELESALE_FEEDBACK_ENDPOINT), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public int UpdateLastTelesaleFeedback(string access_token, int loanbriefId)
        {
            try
            {
                var obj = new
                {
                    LoanbriefId = loanbriefId
                };
                var json = JsonConvert.SerializeObject(obj);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_LAST_TELESALE_FEEDBACK_ENDPOINT), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public CountLoanTLSDetail CountLoanTLS(string access_token, int TelesaleId, string teamTelesales)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, CountLoanTLSDetail>>(string.Format(Constants.LOANBRIEF_COUNTLOAN_TLS_ENDPOINT, TelesaleId, teamTelesales), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public LoanBriefDetailNew GetLoanBriefByIdNew(string access_token, int id)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LoanBriefDetailNew>>(string.Format(Constants.LOANBRIEF_DETAIL_ENDPOINT, id), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public int UpdateStatusDevice(string access_token, LoanBriefInitDetail entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_UPDATE_STATUS_DEVICE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;

        }

        public bool RemoveFile(string access_token, RemoveFileItem entity)
        {
            try
            {

                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_REMOVE_FILE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/RemoveFile Exception");
            }
            return false;
        }

        public bool ReActiveDevice(string access_token, int loanbriefId)
        {
            try
            {
                var obj = new
                {
                    LoanbriefId = loanbriefId
                };
                var json = JsonConvert.SerializeObject(obj);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_RE_ACTIVE_DEVICE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/RemoveFile Exception");
            }
            return false;
        }

        public int ScriptUpdateLoanBriefV2(string access_token, LoanBriefScriptItemV2 entity, int id)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_SCRIPTV2_ENDPOINT, id), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public bool SaveStatusTelesales(string access_token, DAL.Object.RequestSaveStatusTelesales entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_SAVE_STATUS_TELESALES, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/RemoveFile Exception");
            }
            return false;
        }
        public DefaultResponse<object> UpdateBorrowCavet(string access_token, int loanbriefId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<object>>(string.Format(Constants.LOANBRIEF_UPDATE_CAVET, loanbriefId), access_token, Constants.WEB_API_URL).Result;
                return response;
            }
            catch (Exception ex)
            {
                DefaultResponse<object> def = new DefaultResponse<object>();
                def.meta = new Meta(500, "internal server error");
                return def;
            }
        }

        public LoanBrief GetLoanBriefByCodeId(string access_token, int codeId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LoanBrief>>(string.Format(Constants.LOANBRIEF_GET_BY_CODEID, codeId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public async Task<int> ReInitLoanBrief(string access_token, LoanBrief entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_RE_INIT_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/InitLoanBrief Exception");
            }
            return 0;
        }

        public async Task<int> AddLoanTopup(string access_token, LoanBriefInit entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_ADD_TOPUP, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddLoanTopup Exception");
            }
            return 0;
        }


        public async Task<List<int>> GetLoanBriefFileByLoanId(string access_token, List<int> param)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, List<int>>>(string.Format(Constants.LOANBRIEF_GET_FILE_BY_LIST_LOANID_ENDPOINT, string.Join(",", param)), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<LoanBriefPropertyDTO> GetLoanbriefProperty(string access_token, int loanbriefId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanBriefPropertyDTO>>(string.Format(Constants.LOANBRIEF_PROPERTY_GET_ENDPOINT, loanbriefId), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public async Task<int> AddLoanBriefHistory(string access_token, LoanBriefHistory entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_ADD_HISTORY_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddLoanBriefHistory Exception");
            }
            return 0;
        }

        public async Task<int> InitLoanBriefV2(string access_token, LoanBrief entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_INIT_V2_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/InitLoanBriefV2 Exception");
            }
            return 0;
        }

        public async Task<int> UpdateLoanBriefV2(string access_token, LoanBrief entity, int id)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_V2_ENDPOINT, id), access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public async Task<int> UpdateScriptTelesale(string access_token, LoanBrief entity, int id)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_SCRIPT_TELESALE_V2_ENDPOINT, id), access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public async Task<int> CreateLoanbriefScript(string access_token, LoanBrief entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_CREATE_SCRIPT_TELESALE_V2_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public async Task<int> CountLoanByApproveId(string access_token, int ApproveId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<object>>(string.Format(Constants.LOANBRIEF_COUNT_APPROVEID_ENDPOINT, ApproveId), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return Convert.ToInt32(response.data);
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public bool UpdateLabelReBorrow(string access_token, UpdateReBorrow entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.LOANBRIEF_UPDATE_LABEL_REBORROW, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/RemoveLabelReBorrow Exception");
            }
            return false;
        }
        public async Task<int> HubEmployeeChange(string access_token, LoanBriefInitDetail entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_CHANGE_EMPLOYEE_IN_HUB_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;
            }
            catch (Exception ex)
            {
            }
            return 0;

        }


        public async Task<List<LoanBrief>> GetListLoanFromIdAndStatus(string access_token, List<int> loanbriefIds, int Status)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<List<LoanBrief>>>(string.Format(Constants.GET_LOAN_FROM_ID_STATUS_ENDPOINT, string.Join(',', loanbriefIds), Status), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {
            }
            return null;

        }

        public async Task<bool> ChangeHub(string access_token, ChangeHubSupportItem entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_CHANGE_HUB_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                return response.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
            }
            return false;

        }

        public async Task<bool> ChangeHubAndEmployee(string access_token, ChangeHubSupportItem entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_CHANGE_HUB_EMPLOYEE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                return response.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
            }
            return false;

        }

        public int UpdateFirstStaffHubFeedback(string access_token, int loanbriefId)
        {
            try
            {
                var obj = new
                {
                    LoanbriefId = loanbriefId
                };
                var json = JsonConvert.SerializeObject(obj);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_FIRST_STAFF_HUB_FEEDBACK_ENDPOINT), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public bool AddLogChangeBoundTelesale(string access_token, TelesaleLoanbrief entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.LOANBRIEF_ADD_LOG_CHANGE_BOUNDTELESALE_V2_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddLogChangeBoundTelesale Exception");
            }
            return false;
        }
        public DefaultResponse<object> AddAndEditSchedule(string access_token, ScheduleTimeHubReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.ADD__AND_EDIT_SCHEDULE, access_token, stringContent, Constants.WEB_API_URL).Result;
                return response;
            }
            catch (Exception ex)
            {
                DefaultResponse<object> def = new DefaultResponse<object>();
                def.meta = new Meta(500, "internal server error");
                return def;
            }
        }

        public async Task<List<ScheduleTime>> SearchSchedule(string access_token, GetListScheduleTimeHubReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<List<ScheduleTime>>>(Constants.SEARCH_SCHEDULE, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public async Task<List<ScheduleTime>> GetListScheduleByLoan(string access_token, int loanBriefId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<List<ScheduleTime>>>(string.Format(Constants.GET_LIST_SCHEDULETIME_BY_LOAN, loanBriefId), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public async Task<int> RequestAIV3(string access_token, LoanBrief entity, int id)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_REQUEST_AI_V3, id), access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public async Task<int> RequestRefphoneV3(string access_token, LoanBrief entity, int id)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_REQUEST_REFPHONE_V3, id), access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public async Task<int> UpdateScriptTelesaleV2(string access_token, LoanBrief entity, int id)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_SCRIPT_TELESALE_V3_ENDPOINT, id), access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public async Task<int> CreateLoanbriefScriptV2(string access_token, LoanBrief entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_CREATE_SCRIPT_TELESALE_V3_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public bool UpdateJsonInfoFamilyKalapa(string access_token, int loanbriefId, InfomationFamilyKalapa entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_HOUSEHOLD_UPDATE_JSONINFOFAMILYKALAPA, loanbriefId), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public List<LoanBriefDetail> SearchRemaketingCar(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefDetail> data = new List<LoanBriefDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>>(Constants.SEARCH_REMARKETINGCAR_ENDPOINT + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/SearchRemaketingCar Exception");
                return null;
            }
            return data;
        }

        public DefaultResponse<object> UpdateHubId(string access_token, BoundTelesaleReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.UPDATE_HUB_ID_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL).Result;
                return response;
            }
            catch (Exception ex)
            {
                DefaultResponse<object> def = new DefaultResponse<object>();
                def.meta = new Meta(500, "internal server error");
                return def;
            }
        }

        public string GetLatLng(string access_token, int typeGetLatLng, int loanbriefId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, string>>(string.Format(Constants.LOANBRIEF_GET_LAT_LNG, loanbriefId, typeGetLatLng), access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    return response.Result.data;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public int UpdateHubEmployeeCallFirst(string access_token, int loanbriefId)
        {
            try
            {
                var obj = new
                {
                    LoanbriefId = loanbriefId
                };
                var json = JsonConvert.SerializeObject(obj);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_HUB_EMPLOYEE_CALL_FIRST_ENDPOINT), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        public async Task<List<TimeProcessingLoanBrief>> CalculateTimeProcessingLoanBrief(string access_token, List<int> param, int groupId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, List<TimeProcessingLoanBrief>>>(string.Format(Constants.LOANBRIEF_CALCULATE_TIME_PROCESSING_LOANBRIEF, string.Join(",", param), groupId), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int UpdateFirstProcessingTime(string access_token, int loanbriefId)
        {
            try
            {
                var obj = new
                {
                    LoanbriefId = loanbriefId
                };
                var json = JsonConvert.SerializeObject(obj);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_TELESALES_FIRST_PROCESSING_TIME_ENDPOINT), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public bool SaveNextStep(string access_token, DAL.Object.LoanStatusDetail.NextStepLoanHub entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_SAVE_NEXT_STEP, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/SaveNextStep Exception");
            }
            return false;
        }

        public List<LoanBriefDetailDebt> ListLoanBriefByCodeId(string access_token, GetLoanByCode lstId)
        {
            try
            {
                var json = JsonConvert.SerializeObject(lstId);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<Meta, List<LoanBriefDetailDebt>>>(string.Format(Constants.GET_LOAN_BY_CODEID_ENDPOINT), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result != null && response.Result.meta.errorCode == 200)
                    return response.Result.data;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public async Task<List<LoanBriefRelationship>> GetLoanbriefRelationship(string access_token, int loanbriefId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, List<LoanBriefRelationship>>>(string.Format(Constants.LOANBRIEF_RELATIONSHIP_GET_ENDPOINT, loanbriefId), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public bool SaveChangeStatusApprover(string access_token, DAL.Object.LoanStatusDetail.NextStepLoanHub entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_SAVE_CHANGE_STATUS_APPROVER, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/SaveChangeStatusApprover Exception");
            }
            return false;
        }

        public List<LoanBriefDetail> GetLoanDepartment(string access_token, string param, ref int recordTotals)
        {
            var data = new List<LoanBriefDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>>(Constants.LOAN_DEPARTMENT_ENDPOINT + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetLoanDepartment Exception");
                return null;
            }
            return data;
        }

        public Task<bool> ToolChangePipeline(string access_token, int loanBriefId, int actionPipeline)
        {
            try
            {
                var json = JsonConvert.SerializeObject(new
                {
                    LoanbriefId = loanBriefId,
                    ActionPipeline = actionPipeline
                });
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_TOOL_CHANGE_PIPELINE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return Task.FromResult(true);
                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ToolChangePipeline Exception");
            }
            return Task.FromResult(true);
        }

        public List<LoanbriefSearch> SearchLoanBrief(string access_token, string param, ref int recordTotals)
        {
            List<LoanbriefSearch> data = new List<LoanbriefSearch>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanbriefSearch>>>(Constants.LOANBRIEF_SEARCH_ENDPOINT + param, access_token, Constants.WEB_API_URL);
                if (response.Result != null && response.Result.meta != null && response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data != null && response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/SearchLoanBrief Exception");
                return null;
            }
            return data;
        }
        public int UpdateReMarketingLoanbriefId(string access_token, int loanbriefId)
        {
            try
            {
                var obj = new
                {
                    LoanbriefId = loanbriefId
                };
                var json = JsonConvert.SerializeObject(obj);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.LOANBRIEF_UPDATE_REMARKETING_ENDPOINT), access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        public async Task<int> CreateLoanBriefChild(string access_token, CreateLoanBriefChild entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_CREATE_CHILD_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public int ProposeExceptions(string access_token, DAL.Object.Loanbrief.ProposeExceptions entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_UPDATE_EXCEPTIONS, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public bool UpdateFeedback(string access_token, DAL.Object.LoanBriefFeedback entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_FEEDBACK_LOANBRIEF_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/UpdateLoanBriefClickToCall Exception");
            }
            return false;
        }

        public async Task<LoanBriefPrematureInterest> GetLoanBriefByPhoneOrNationalCard(string access_token, string textSearch)
        {
            var data = new LoanBriefPrematureInterest();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanBriefPrematureInterest>>(string.Format(Constants.LOANBRIEF_GET_BY_PHONE_OR_NATIONALCARD_ENDPOINT, textSearch), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public async Task<LoanBriefBasicInfo> GetBasicInfo(string access_token, int loanbriefId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanBriefBasicInfo>>(string.Format(Constants.LOANBRIEF_GET_BASIC_INFO_ENDPOINT, loanbriefId), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public async Task<LoanBriefForDetail> GetLoanBriefForDetail(string access_token, int id)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanBriefForDetail>>(string.Format(Constants.LOANBRIEF_FOR_DETAIL_ENDPOINT, id), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public async Task<int> TotalLoanBriefDisbursementInMonth(string access_token, int coordinatorUserId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, dynamic>>(string.Format(Constants.LOANBRIEF_TOTAL_LOAN_DISBURSEMENT_IN_MONTH, coordinatorUserId), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return Convert.ToInt32(response.data);
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public async Task<bool> AddLogDistributionUser(string access_token, LogDistributionUser entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<object>>(Constants.LOANBRIEF_ADD_LOG_DISTRIBUTION_USER_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddLogDistributionUser Exception");
            }
            return false;
        }
        public LoanBriefForAppraiser GetLoanBriefForAppraiser(string access_token, int id)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LoanBriefForAppraiser>>(string.Format(Constants.LOANBRIEF_FOR_APPRAISER_ENDPOINT, id), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public bool CheckLoanTopup(string access_token, string nationalCard, string phone, int loanbriefId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, dynamic>>(string.Format(Constants.LOANBRIEF_CHECK_TOPUP, nationalCard, phone, loanbriefId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public async Task<LoanBriefDetail> GetLoanBriefBorrowing(string access_token, string phone, string nationalCard)
        {
            var data = new LoanBriefDetail();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanBriefDetail>>(string.Format(Constants.LOANBRIEF_GET_BORROWING, nationalCard, phone), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public async Task<List<LoanBriefDetail>> GetListLoanTopupCar(string access_token, int customerId)
        {
            var data = new List<LoanBriefDetail>();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, List<LoanBriefDetail>>>(string.Format(Constants.LOANBRIEF_GET_LIST_TOPUP_CAR, customerId), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public List<LoanBriefForBrower> SearchClickTopTls(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefForBrower> data = new List<LoanBriefForBrower>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefForBrower>>>(Constants.LOANBRIEF_GET_CLICK_TOP_TLS + param, access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/SearchClickTopTls Exception");
                return null;
            }
            return data;
        }

        public async Task<LoanBriefDetail> GetLoanBriefByNationalCard(string access_token, string nationalCard, int loanbriefId)
        {
            var data = new LoanBriefDetail();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanBriefDetail>>(string.Format(Constants.LOANBRIEF_GET_BY_NATIONAL_CARD, nationalCard, loanbriefId), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public async Task<List<LogDistributionUser>> GetLogDistributionUser(string access_token, GetLogDistributionUserModel entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<Meta, List<LogDistributionUser>>>(Constants.GET_LOG_DISTRIBUTION_USER, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<LoanBriefContractFinancialAgreement> GetContractFinancialAgreement(string access_token, int loanbriefId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanBriefContractFinancialAgreement>>(string.Format(Constants.LOANBRIEF_GET_CONTRACT_FINANCIAL_AGREEMENT, loanbriefId), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public async Task<LoanBriefCommittedPaper> GetCommittedPaper(string access_token, int loanbriefId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanBriefCommittedPaper>>(string.Format(Constants.LOANBRIEF_GET_COMMITTED_PAPER, loanbriefId), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public async Task<List<LoanBriefFileDetail>> GetLoanBriefFileByDocumentMulti(string access_token, int LoanBriefId, List<int> documentType)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, List<LoanBriefFileDetail>>>(string.Format(Constants.LOANBRIEF_GET_FILE_BY_DOCUMENT, LoanBriefId, string.Join(",", documentType)), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public async Task<LoanInfoCompare> GetLoanInfoCompare(string access_token, int loanbriefId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanInfoCompare>>(string.Format(Constants.LOANBRIEF_GET_INFO_COMPARE_ENDPOINT, loanbriefId), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }

        public async Task<LoanBriefDetail> GetLoanBriefDisbursed(string access_token, string phone, string nationalCard)
        {
            var data = new LoanBriefDetail();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanBriefDetail>>(string.Format(Constants.LOANBRIEF_DISBURSED, nationalCard, phone), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public async Task<int> InitDebtRevolvingLoan(string access_token, LoanBrief entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_INIT_DEBTREVOLVINGLOAN_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/InitDebtRevolvingLoan Exception");
            }
            return 0;
        }

        public async Task<bool> CheckDebtRevolvingLoan(string access_token, int loanBriefId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<dynamic>>(string.Format(Constants.CHECK_DEBTREVOlVING_LOAN, loanBriefId), access_token, Constants.WEB_API_URL);
                return response.data;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public async Task<ResponseService> ReEsignCustomer(string access_token, DAL.Object.Tool.ToolReq.ReEsignReq req)
        {
            var res = new ResponseService();
            try
            {
                res.Success = false;
                var json = JsonConvert.SerializeObject(req);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.RE_ESIGN_CUSTOMER_ENPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    res.Success = true;
                else
                    res.Message = response.meta.errorMessage;
                return res;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanbriefService/ReEsignCustomer Exception");
            }
            return res;
        }

        public List<int> ChangeSupportAllClickTopTls(string access_token, string param)
        {
            List<int> data = new List<int>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<int>>>(Constants.LOANBRIEF_CHANGE_SUPPORT_ALL_CLICK_TOP_TLS + param, access_token, Constants.WEB_API_URL);
                if (response != null && response.Result != null && response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ChangeSupportAllClickTopTls Exception");
                return null;
            }
            return data;
        }

        public List<int> ChangeSupportAllSearch(string access_token, string param)
        {
            List<int> data = new List<int>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<int>>>(Constants.LOANBRIEF_CHANGE_SUPPORT_ALL + param, access_token, Constants.WEB_API_URL);
                if (response != null && response.Result != null && response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/ChangeSupportAllSearch Exception");
                return null;
            }
            return data;
        }

        public async Task<bool> AddRangeLoanBriefNote(string access_token, List<LoanBriefNote> listData)
        {
            try
            {
                var json = JsonConvert.SerializeObject(listData);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.LOANBRIEF_ADD_RANGE_NOTE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddRangeLoanBriefNote Exception");
            }
            return false;
        }

        public async Task<bool> AddRangeLogDistributionUser(string access_token, List<LogDistributionUser> listData)
        {
            try
            {
                var json = JsonConvert.SerializeObject(listData);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<object>>(Constants.LOANBRIEF_ADD_RANGE_LOG_DISTRIBUTION_USER_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddRangeLogDistributionUser Exception");
            }
            return false;
        }

        public DefaultResponse<object> UpdateRangeBoundTelesale(string access_token, RangeBoundTelesaleReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.RANGE_BOUND_TELESALE_ID_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL).Result;
                return response;
            }
            catch (Exception ex)
            {
                DefaultResponse<object> def = new DefaultResponse<object>();
                def.meta = new Meta(500, "internal server error");
                return def;
            }
        }

        public async Task<List<DocumentException>> GetDocumentException(string access_token, int parentId)
        {
            var data = new List<DocumentException>();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, List<DocumentException>>>(string.Format(Constants.LOANBRIEF_GET_LIST_EXCEPTIONS, parentId), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public async Task<ProposeExceptions> GetProposeExceptions(string access_token, int loanBriefId)
        {
            var data = new ProposeExceptions();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, ProposeExceptions>>(string.Format(Constants.LOANBRIEF_GET_PROPOSE_EXCEPTION, loanBriefId), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public int AddProposeExceptions(string access_token, ProposeExceptionReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_PROPOSE_EXCEPTIONS, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public List<LoanCancelMomo> GetLoanCancelByMomo(string access_token, LoanBriefForDetail entity)
        {
            var data = new List<LoanCancelMomo>();
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<Meta, List<LoanCancelMomo>>>(Constants.LOANBRIEF_GET_LOAN_CANCEL_MOMO_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200 && response.Result.data.Count > 0)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public async Task<bool> CheckLoanProcessing(string access_token, string phone)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<dynamic>>(string.Format(Constants.CHECK_LOAN_PROCESSING, phone), access_token, Constants.WEB_API_URL);
                return response.data;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public async Task<List<LoanByPhoneChange>> GetLoanByPhoneChange(string access_token, string phone)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<List<LoanByPhoneChange>>>(string.Format(Constants.GET_LOAN_BY_PHONE_CHANGE_ENPOINT, phone), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {
            }
            return null;
        }


        public async Task<LoanTransactionsSecured> GetLoanTransactionsSecured(string access_token, int loanBriefId)
        {
            var data = new LoanTransactionsSecured();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanTransactionsSecured>>(string.Format(Constants.LOANBRIEF_GET_LOAN_TRANSACTION_SECURED_ENDPOINT, loanBriefId), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public async Task<double> GetTotalLoanAmountTranSactionSecured(string access_token, int customerId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<dynamic>>(string.Format(Constants.GET_TOTAL_LOAN_AMOUNT_SECURED, customerId), access_token, Constants.WEB_API_URL);
                return response.data;
            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public bool UpdateTransactionState(string access_token, UpdateTransactionStateReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.LOANBRIEF_UPDATE_TRANSACTION_STATE, access_token, stringContent, Constants.WEB_API_URL);
                if (response != null && response.Result != null && response.Result.meta != null)
                    return true;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public async Task<List<LoanBriefCheckCavetDetail>> GetLoanToolCheckCavet(string access_token, string search)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<List<LoanBriefCheckCavetDetail>>>(string.Format(Constants.GET_LOAN_TOOL_CHECK_CAVET_ENPOINT, search), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public async Task<LoanDisplayDocHub> CheckLoanDisplayDocHubEmp(string access_token, string phone,string nationalCard)
        {
            var data = new LoanDisplayDocHub();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, LoanDisplayDocHub>>(string.Format(Constants.CHECK_LOAN_DISPLAY_DOC_HUB_EMP, phone, nationalCard), access_token, Constants.WEB_API_URL);
                if (response.meta != null && response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {
            }
            return data;
        }

        public async Task<bool> CheckLoanAssign(string access_token, int loanBriefId, string phone, int userId)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<dynamic>>(string.Format(Constants.CHECK_LOAN_ASSIGN_ENDPONT, loanBriefId, phone, userId), access_token, Constants.WEB_API_URL);
                return response.data;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public List<LogPushToCisco> PushToCiscoSearch(string access_token, string param, ref int recordTotals)
        {
            List<LogPushToCisco> data = new List<LogPushToCisco>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LogPushToCisco>>>(Constants.SEARCH_PUSH_TO_CISCO_ENDPOINT + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Approved/Search Exception");
                return null;
            }
            return data;
        }

        public async Task<int> AddLogPushToCisco(string access_token, LogPushToCisco entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.ADD_LOG_PUSH_TO_CISCO_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/AddLogPushToCisco Exception");
            }
            return 0;
        }
    }
}
