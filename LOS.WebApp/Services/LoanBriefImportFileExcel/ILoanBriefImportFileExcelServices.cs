﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;

namespace LOS.WebApp.Services
{
    public interface ILoanBriefImportFileExcelServices
    {
        int AddLoanBriefImportFileExcel(string access_token, List<LoanBriefImportFileExcel> entity);
        List<LoanBriefImportFileExcelDetail> GetListTableImportFileExcel(string access_token, string param);
        List<LoanBriefImportFileExcelDetail> GetDetailLoanBriefImportFileExcell(string access_token, string param);
    }
}
