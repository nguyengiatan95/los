﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace LOS.WebApp.Services
{
    public class LoanBriefImportFileExcelServices : BaseServices, ILoanBriefImportFileExcelServices
    {
        public LoanBriefImportFileExcelServices(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public int AddLoanBriefImportFileExcel(string access_token, List<LoanBriefImportFileExcel> entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_LOANBRIEF_IMPORT_FILE_EXCEL, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public List<LoanBriefImportFileExcelDetail> GetListTableImportFileExcel(string access_token, string param)
        {
            List<LoanBriefImportFileExcelDetail> data = new List<LoanBriefImportFileExcelDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LoanBriefImportFileExcelDetail>>>(Constants.GET_LOANBRIEF_IMPORT_FILE_EXCEL + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetListTableImportFileExcel Exception");
                return null;
            }
            return data;
        }

        public List<LoanBriefImportFileExcelDetail> GetDetailLoanBriefImportFileExcell(string access_token, string param)
        {
            List<LoanBriefImportFileExcelDetail> data = new List<LoanBriefImportFileExcelDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LoanBriefImportFileExcelDetail>>>(Constants.GET_DETAIL_LOANBRIEF_IMPORT_FILE_EXCEL + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "LoanBrief/GetDetailLoanBriefImportFileExcell Exception");
                return null;
            }
            return data;
        }
    }
}
