﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.DAL.EntityFramework;

namespace LOS.WebApp.Services
{
    public interface ILoanStatusDetailService
    {
        List<LoanStatusDetail> GetLoanStatusDetail(string access_token, int typeId);
        List<LoanStatusDetail> GetLoanStatusDetailChild(string access_token, int parentId);
        List<LoanStatusDetail> GetStatusTelesalesDetailAll(string access_token);
    }
}
