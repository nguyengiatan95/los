﻿using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public class LoanStatusDetailService : BaseServices, ILoanStatusDetailService
    {
        public LoanStatusDetailService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }
        public List<LoanStatusDetail> GetLoanStatusDetail(string access_token, int typeId)
        {
            var data = new List<LoanStatusDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LoanStatusDetail>>>(string.Format(Constants.GET_LIST_LOAN_STATUS_DETAIL_ENDPOINT, typeId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200 && response.Result.data.Count > 0)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<LoanStatusDetail> GetLoanStatusDetailChild(string access_token, int parentId)
        {
            var data = new List<LoanStatusDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LoanStatusDetail>>>(string.Format(Constants.GET_LIST_LOAN_STATUS_CHILD_DETAIL_ENDPOINT, parentId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200 && response.Result.data.Count > 0)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<LoanStatusDetail> GetStatusTelesalesDetailAll(string access_token)
        {
            var data = new List<LoanStatusDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LoanStatusDetail>>>(string.Format(Constants.GET_LIST_STATUS_TELESALES_DETAIL_ALL_ENDPOINT), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200 && response.Result.data.Count > 0)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
    }
}
