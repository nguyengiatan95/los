﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.Location
{
    public interface ILocationService
    {
        string GetNameAddress(decimal lat, decimal lng);
        double DistanceTwoPoint(double lat, double lon, double latTo, double lonTo);
        double DistanceTwoPoint(string fromAddress, string toAddress);
    }
    public class LocationService : ILocationService
    {
        private IConfiguration _configuration;
        public LocationService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        private GeocodingResult GetAddress(decimal lat, decimal lng)
        {
            var APIKEY = _configuration["AppSettings:KEY_GOOGLE_MAP"];
            var client = new RestClient("https://maps.googleapis.com");
            var request = new RestRequest("maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=" + APIKEY, Method.GET);
            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var strJson = response.Content;
                    var objResultGet =
                        JsonConvert.DeserializeObject<GeocodingResult>(strJson);
                    return objResultGet;
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string GetNameAddress(decimal lat, decimal lng)
        {
            try
            {
                var result = GetAddress(lat, lng);
                if (result != null && result.results != null && result.results.Count > 0)
                    return string.IsNullOrEmpty(result.results[0].formatted_address) ? null : result.results[0].formatted_address;
            }
            catch (Exception e)
            {
                return null;
            }

            return "Không xác định";
        }

        public double DistanceTwoPoint(string fromAddress, string toAddress)
        {

            try
            {
                double lat = 0l, latTo = 0l, lng = 0l, lngTo = 0l;
                ConvertToPoint(fromAddress, ref lat, ref lng);
                ConvertToPoint(toAddress, ref latTo, ref lngTo);
                var R = 6371; //Km
                var dLat = Radians(latTo) - Radians(lat);
                var dLon = Radians(lngTo) - Radians(lng);
                var a = Math.Pow(Math.Sin(dLat / 2), 2) + (Math.Pow(Math.Sin(dLon / 2), 2) * Math.Cos(Radians(lat)) * Math.Cos(Radians(latTo)));
                var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
                return Math.Round(R * c * 1000, 0);
            }
            catch (Exception e)
            {
                return 0;
            }

        }
        public double DistanceTwoPoint(double lat, double lon, double latTo, double lonTo)
        {

            try
            {
                var R = 6371; //Km
                var dLat = Radians(latTo) - Radians(lat);
                var dLon = Radians(lonTo) - Radians(lon);
                var a = Math.Pow(Math.Sin(dLat / 2), 2) + (Math.Pow(Math.Sin(dLon / 2), 2) * Math.Cos(Radians(lat)) * Math.Cos(Radians(latTo)));
                var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
                return Math.Round(R * c * 1000, 0);
            }
            catch (Exception e)
            {
                return 0;
            }

        }


        private double Radians(double x)
        {
            return x * Math.PI / 180;
        }

        private void ConvertToPoint(string sLatLng, ref double lat, ref double lng)
        {
            try
            {
                if (sLatLng.Contains(","))
                {
                    var arr = sLatLng.Split(",");
                    if (arr.Length == 2)
                    {
                        lat = Convert.ToDouble(arr[0]);
                        lng = Convert.ToDouble(arr[1]);
                    }
                }
            }
            catch
            {

            }
        }
        public class GeocodingResult
        {
            public PlusCode plus_code { get; set; }
            public List<result> results { get; set; }
        }
        public class result
        {
            public List<address_component> address_components { get; set; }
            public string formatted_address { get; set; }
            public string place_id { get; set; }
        }
        public class PlusCode
        {
            public string compound_code { get; set; }
            public string global_code { get; set; }
        }
        public class address_component
        {
            public string long_name { get; set; }
            public string short_name { get; set; }
            public List<string> types { get; set; }
        }
    }
}
