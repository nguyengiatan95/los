﻿using System;
using System.Collections.Generic;
using LOS.WebApp.Helpers;
using LOS.Common.Models.Response;
using Microsoft.Extensions.Configuration;
using LOS.DAL.EntityFramework;
using System.Net.Http;

namespace LOS.WebApp.Services
{
    public interface ILogCallApi
    {
        List<LogCallApi> GetRequest(string access_token, int loanbriefId, int actionCallApi);
        LogCallApi GetById(string access_token, int id);
        LogCallApi GetLast(string access_token, int loanbriefId, int actionCallApi);
    }
    public class LogCallApiService : BaseServices, ILogCallApi
    {
        public LogCallApiService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public List<LogCallApi> GetRequest(string access_token, int loanbriefId, int actionCallApi)
        {
            List<LogCallApi> data = new List<LogCallApi>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LogCallApi>>>(string.Format(Constants.LOG_CALL_API_SEARCH_ANDPOINT, loanbriefId, actionCallApi), access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public LogCallApi GetById(string access_token, int id)
        {
            var data = new LogCallApi();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LogCallApi>>(string.Format(Constants.LOG_CALL_API_GET_ENDPOINT, id), access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data != null)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public LogCallApi GetLast(string access_token, int loanbriefId, int actionCallApi)
        {
            LogCallApi data = new LogCallApi();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LogCallApi>>(string.Format(Constants.LOG_CALL_API_GET_LAST_ANDPOINT, loanbriefId, actionCallApi), access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data != null)
                    {
                        return response.Result.data;

                    }
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
