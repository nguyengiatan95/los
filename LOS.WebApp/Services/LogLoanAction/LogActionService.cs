﻿using LOS.Common.Extensions;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Erp;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface ILogActionService
    {
        Task<int> AddLogAction(string access_token, LogLoanAction entity);
        Task<int> AddLogActionRestruct(string access_token, LogActionRestruct entity);
    }
    public class LogActionService : BaseServices, ILogActionService
    {
        private readonly IConfiguration _configuration;
        public LogActionService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public async Task<int> AddLogAction(string access_token, LogLoanAction entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.log_loan_action_add, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogLoanAction/AddLogAction Exception");
            }
            return 0;
        }

        public async Task<int> AddLogActionRestruct(string access_token, LogActionRestruct entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.log_action_restruct_add, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return (int)response.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "LogLoanActionRestruct/AddLogAction Exception");
            }
            return 0;
        }
    }
}
