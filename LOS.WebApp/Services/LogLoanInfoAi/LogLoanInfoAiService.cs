﻿using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface ILogLoanInfoAi
    {
        List<LogLoanInfoAi> GetRequest(string access_token, int loanbriefId, int serviceType);
        LogLoanInfoAi GetById(string access_token, int id);
        LogLoanInfoAi GetByServiceType(string access_token, int loanbriefId, int serviceType);
        List<LogLoanInfoAi> GetByListServiceType(string access_token, int loanbriefId, List<int> lstServiceType);
    }
    public class LogLoanInfoAiService : BaseServices, ILogLoanInfoAi
    {
        public LogLoanInfoAiService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public List<LogLoanInfoAi> GetRequest(string access_token, int loanbriefId, int serviceType)
        {
            List<LogLoanInfoAi> data = new List<LogLoanInfoAi>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LogLoanInfoAi>>>(string.Format(Constants.LOG_LOAN_AI_SEARCH_ENDPOINT, loanbriefId, serviceType), access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public LogLoanInfoAi GetById(string access_token, int id)
        {
            var data = new LogLoanInfoAi();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LogLoanInfoAi>>(string.Format(Constants.LOG_LOAN_AI_GET_ENDPOINT, id), access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data != null)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public LogLoanInfoAi GetByServiceType(string access_token, int loanbriefId, int serviceType)
        {
            LogLoanInfoAi data = new LogLoanInfoAi();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, LogLoanInfoAi>>(string.Format(Constants.LOG_LOAN_AI_BY_SERVICETYPE_ENDPOINT, loanbriefId, serviceType), access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    data = response.Result.data;
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<LogLoanInfoAi> GetByListServiceType(string access_token, int loanbriefId, List<int> lstServiceType)
        {
            List<LogLoanInfoAi> data = new List<LogLoanInfoAi>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LogLoanInfoAi>>>(string.Format(Constants.LOG_LOAN_AI_BY_LST_SERVICETYPE_ENDPOINT, loanbriefId, string.Join(",", lstServiceType)), access_token, Constants.WEB_API_URL);
                if (response.Result != null && response.Result.meta.errorCode == 200)
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
            }
            catch (Exception ex)
            {
            }
            return data;
        }
    }
}
