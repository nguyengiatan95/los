﻿using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface ILogReqestAiService
    {
        bool Create(string access_token, LogLoanInfoAi entity);
        bool Create(string access_token, List<LogLoanInfoAi> entities);
        List<LogLoanInfoAi> GetRequest(string access_token, int loanbriefId);
        int CreateV2(string access_token, LogLoanInfoAi entity);
        bool Update(string access_token, LogLoanInfoAi entity, int id);
    }
    public class LogReqestAiService : BaseServices, ILogReqestAiService
    {
        public LogReqestAiService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }
        public bool Create(string access_token, LogLoanInfoAi entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.LOG_REQUEST_AI_ENDPOINT, access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Create(string access_token, List<LogLoanInfoAi> entities)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entities);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.LOG_REQUEST_MULT_AI_ENDPOINT, access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<LogLoanInfoAi> GetRequest(string access_token, int loanbriefId)
        {
            List<LogLoanInfoAi> data = new List<LogLoanInfoAi>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LogLoanInfoAi>>>(string.Format(Constants.LOG_GET_LOANBRIEF_ENDPOINT, loanbriefId), access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public bool Update(string access_token, LogLoanInfoAi entity, int id)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(string.Format(Constants.UPDATE_LOG_REQUEST_AI_ENDPOINT, id), access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int CreateV2(string access_token, LogLoanInfoAi entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOG_REQUEST_V2_AI_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
            }
            return 0;
        }
    }
}
