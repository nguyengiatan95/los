﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
   public interface ILogWorkingService
    {
        int Add(string access_token, LogWorkingDTO entity);
    }
}
