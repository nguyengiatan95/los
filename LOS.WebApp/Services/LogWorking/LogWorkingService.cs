﻿using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public class LogWorkingService: BaseServices, ILogWorkingService
    {
        public LogWorkingService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public int Add(string access_token, LogWorkingDTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.LOG_WORKING_ADD_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
            }
            return 0;
        }
    }
}
