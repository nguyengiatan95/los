﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.ManagerAreaHub
{
    public interface IManagerAreaHubService
    {
        List<LOS.DAL.EntityFramework.ManagerAreaHub> GetManagerAreaHub(string access_token,int cityid,int hubid,int productid);
        bool SaveSpicesAgency(string access_token, ManagerAreaHubSaveItem entity);
    }
}
