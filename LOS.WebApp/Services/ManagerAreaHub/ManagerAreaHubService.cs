﻿using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.ManagerAreaHub;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.ManagerAreaHub
{
    public class ManagerAreaHubService : BaseServices, IManagerAreaHubService
    {
        protected IConfiguration _baseConfig;
        public ManagerAreaHubService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
        }
        public List<LOS.DAL.EntityFramework.ManagerAreaHub> GetManagerAreaHub(string access_token, int cityid, int hubid, int productid)
        {
            List<LOS.DAL.EntityFramework.ManagerAreaHub> data = new List<LOS.DAL.EntityFramework.ManagerAreaHub>();
            var url = string.Format(Constants.GET_LIST_MANAGER_AREA_HUB_ENDPOINT, cityid, hubid, productid);
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LOS.DAL.EntityFramework.ManagerAreaHub>>>(url, access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public bool SaveSpicesAgency(string access_token, ManagerAreaHubSaveItem entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.MANAGER_AREA_HUB_ENDPOINT, access_token, stringContent);
                return response.Result.meta.errorCode == 200;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
