﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IModuleService
    {
        List<ModuleDetail> Search(string access_token, string param, ref int recordTotals);
        List<Module> GetAll(string access_token);
        ModuleDetail GetById(string access_token, int id);
        ModuleDetail Get(string access_token, string controller, string action);
        bool Update(string access_token, int id, ModuleDTO entity);
        bool Create(string access_token, ModuleDTO entity);
        bool Delete(string access_token, int id);
        int CheckDelete(string access_token, int id);
        List<ModuleDetail> GetByGroupId(string access_token, int groupId, int applicationId);
        List<Module> GetAllMenu(string access_token, int applicationId);
        List<Module> GetAllAction(string access_token, int applicationId);
        bool SaveActionPermissonApiUser(string access_token, int userId, List<UserModule> module);
        bool SaveActionPermissonApiGroup(string access_token, int groupId, List<GroupModule> module);
    }

}
