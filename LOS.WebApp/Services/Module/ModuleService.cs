﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.Response;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public class ModuleService : BaseServices, IModuleService
    {
        protected IConfiguration _baseConfig;
        public ModuleService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
        }
        public bool Create(string access_token, ModuleDTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.MODULE_ENDPOINT, access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Delete(string access_token, int id)
        {
            try
            {
                var response = DeleteAsync<DefaultResponse<object>>(string.Format(Constants.MODULE_DELETE_ENDPOINT, id), access_token);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public ModuleDetail GetById(string access_token, int id)
        {
            var result = new ModuleDetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, ModuleDetail>>(string.Format(Constants.MODULE_GET_ENDPOINT, id), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public ModuleDetail Get(string access_token, string controller, string action)
        {
            var result = new ModuleDetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, ModuleDetail>>(string.Format(Constants.MODULE_GET, controller, action), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public List<Module> GetAll(string access_token)
        {
            var data = new List<Module>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<Module>>>(Constants.MODULE_GET_ALL, access_token);
                if (response.Result.meta.errorCode == 200)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }


        public List<ModuleDetail> Search(string access_token, string param, ref int recordTotals)
        {
            List<ModuleDetail> data = new List<ModuleDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<ModuleDetail>>>(Constants.MODULE_ENDPOINT + param, access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }


        public bool Update(string access_token, int id, ModuleDTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PutAsync<DefaultResponse<object>>(string.Format(Constants.MODULE_UPDATE_ENDPOINT, id), access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public int CheckDelete(string access_token, int id)
        {
            var result = 0;
            try
            {
                var response = GetAsync<DefaultResponse<dynamic>>(string.Format(Constants.MODULE_CHECK_DELETE_ENDPOINT, id), access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    var s = response.Result.data;
                    result = (int)s;
                }

            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        public int Count_Child(string access_token, int id)
        {
            var result = 0;
            try
            {
                var response = GetAsync<DefaultResponse<dynamic>>(string.Format("", id), access_token);
                if (response.Result.meta.errorCode == 200)
                {
                    var s = response.Result.data;
                    result = (int)s;
                }

            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        public List<ModuleDetail> GetByGroupId(string access_token, int groupId, int applicationId)
        {
            var result = new List<ModuleDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<ModuleDetail>>>(string.Format(Constants.MODULE_GET_GROUP_ENDPOINT, groupId, applicationId), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public List<Module> GetAllMenu(string access_token, int applicationId)
        {
            var data = new List<Module>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<Module>>>(string.Format(Constants.MODULE_BY_TYPE, true, applicationId), access_token);
                if (response.Result.meta.errorCode == 200)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public List<Module> GetAllAction(string access_token, int applicationId)
        {
            var data = new List<Module>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<Module>>>(string.Format(Constants.MODULE_BY_TYPE, false, applicationId), access_token);
                if (response.Result.meta.errorCode == 200)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public bool SaveActionPermissonApiUser(string access_token, int userId, List<UserModule> module)
        {
            try
            {
                var json = JsonConvert.SerializeObject(module);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.MODULE_SAVE_PERMISSION_USER_API, userId), access_token, stringContent, Constants.WEB_API_URL);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SaveActionPermissonApiGroup(string access_token, int groupId, List<GroupModule> module)
        {
            try
            {
                var json = JsonConvert.SerializeObject(module);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(string.Format(Constants.MODULE_SAVE_PERMISSION_GROUP_API, groupId), access_token, stringContent, Constants.WEB_API_URL);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
