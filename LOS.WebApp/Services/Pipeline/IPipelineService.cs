﻿using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Request.Pipeline;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IPipelineService
	{       
		List<PipelineDTO> GetAll(string access_token);
		PipelineDTO GetById(string access_token,int id);
		DefaultResponse<object> UpdatePipeline(string access_token, PipelineDTO dto);
		DefaultResponse<object> CreatePipeline(string access_token, PipelineDTO dto);
		DefaultResponse<object> UpdateSection(string access_token, SectionDTO dto);
		DefaultResponse<object> CreateSection(string access_token, SectionDTO dto);
		DefaultResponse<object> DeletePipeline(string access_token, int id);
		DefaultResponse<object> ChangeStatusPipeline(string access_token, int id);
		DefaultResponse<object> DeleteSection(string access_token, int id);
		DefaultResponse<object> DeleteSectionDetail(string access_token, int sectionId, int sectionDetailId);
		List<SectionDTO> GetAllSections(string access_token);
		SectionDTO GetSectionById(string access_token,int id);		
		List<PropertyDTO> GetAllProperty(string access_token);
		PropertyDTO GetPropertyById(string access_token, int id);
		DefaultResponse<object> CreateSectionDetail(string access_token, AddSectionDetailReq detail);
		DefaultResponse<object> UpdateSectionDetail(string access_token, AddSectionDetailReq detail);
		SectionDetailDTO GetDetailById(string access_token, int id);
		DefaultResponse<object> UpdateSectionOrder(string access_token, UpdateSectionOrderReq req);
		bool ChangePipeline(string access_token, ChangePipelineReq req);
	}
}
