﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Request.Pipeline;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace LOS.WebApp.Services
{
	public class PipelineService : BaseServices, IPipelineService
	{
		public PipelineService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
		{
			
		}

		public DefaultResponse<object> DeleteSection(string access_token, int id)
		{
			try
			{
				var response = DeleteAsync<DefaultResponse<object>>(String.Format(Constants.DELETE_PIPELINE_SECTION_ENDPOINT, id), access_token, Constants.WEB_API_URL);
				return response.Result;
			}
			catch (Exception ex)
			{
				Log.Error("Delete Pipeline Section Excetion:" + ex.Message + "\n" + ex.StackTrace);
				return null;
			}
		}

		public DefaultResponse<object> DeletePipeline(string access_token, int id)
		{			
			try
			{
				var response = DeleteAsync<DefaultResponse<object>>(String.Format(Constants.DELETE_PIPELINE_ENDPOINT,id), access_token, Constants.WEB_API_URL);
				return response.Result;
			}
			catch (Exception ex)
			{
				Log.Error("Delete Pipeline Excetion:" + ex.Message + "\n" + ex.StackTrace);
				return null;
			}			
		}

		public DefaultResponse<object> ChangeStatusPipeline(string access_token, int id)
		{
			try
			{
				var response = PostAsync<DefaultResponse<object>>(String.Format(Constants.CHANGE_STATUS_PIPELINE_ENDPOINT, id), access_token, null, Constants.WEB_API_URL);
				return response.Result;
			}
			catch (Exception ex)
			{
				Log.Error("Change Status Pipeline Excetion:" + ex.Message + "\n" + ex.StackTrace);
				return null;
			}
		}

		public DefaultResponse<object> DeleteSectionDetail(string access_token, int sectionId, int sectionDetailId)
		{
			try
			{
				var response = DeleteAsync<DefaultResponse<object>>(String.Format(Constants.DELETE_SECTION_DETAIL_ENDPOINT, sectionId, sectionDetailId), access_token, Constants.WEB_API_URL);
				return response.Result;
			}
			catch (Exception ex)
			{
				Log.Error("Delete Section Detail Excetion:" + ex.Message + "\n" + ex.StackTrace);
				return null;
			}
		}

		public DefaultResponse<object> CreatePipeline(string access_token, PipelineDTO dto)
		{
			try
			{
				var json = JsonConvert.SerializeObject(dto);
				var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
				var response = PostAsync<DefaultResponse<object>>(Constants.CREATE_PIPELINE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
				return response.Result;
			}
			catch (Exception ex)
			{
				Log.Error("Delete Pipeline Excetion:" + ex.Message + "\n" + ex.StackTrace);
				return null;
			}
		}

		public DefaultResponse<object> UpdatePipeline(string access_token, PipelineDTO dto)
		{
			try
			{
				var json = JsonConvert.SerializeObject(dto);
				var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
				var response = PutAsync<DefaultResponse<object>>(String.Format(Constants.UPDATE_PIPELINE_ENDPOINT,dto.PipelineId), access_token, stringContent, Constants.WEB_API_URL);
				return response.Result;
			}
			catch (Exception ex)
			{
				Log.Error("Delete Pipeline Excetion:" + ex.Message + "\n" + ex.StackTrace);
				return null;
			}
		}

		public DefaultResponse<object> CreateSection(string access_token, SectionDTO dto)
		{
			try
			{
				var json = JsonConvert.SerializeObject(dto);
				var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
				var response = PostAsync<DefaultResponse<object>>(Constants.CREATE_SECTION_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
				return response.Result;
			}
			catch (Exception ex)
			{
				Log.Error("Delete Pipeline Excetion:" + ex.Message + "\n" + ex.StackTrace);
				return null;
			}
		}

		public DefaultResponse<object> UpdateSection(string access_token, SectionDTO dto)
		{
			try
			{
				var json = JsonConvert.SerializeObject(dto);
				var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
				var response = PutAsync<DefaultResponse<object>>(String.Format(Constants.UPDATE_SECTION_ENDPOINT, dto.SectionId), access_token, stringContent, Constants.WEB_API_URL);
				return response.Result;
			}
			catch (Exception ex)
			{
				Log.Error("Delete Pipeline Excetion:" + ex.Message + "\n" + ex.StackTrace);
				return null;
			}
		}

		public List<PipelineDTO> GetAll(string access_token)
		{
			List<PipelineDTO> data = new List<PipelineDTO>();
			try
			{
				var response = GetAsync<DefaultResponse<Meta, List<PipelineDTO>>>(Constants.GET_ALL_PIPELINE_ENDPOINT, access_token, Constants.WEB_API_URL);

				if (response.Result.meta.errorCode == 200)
				{
					if (response.Result.data.Count > 0)
					{						
						data = response.Result.data;
					}
				}
			}
			catch (Exception ex)
			{
				Log.Error("Get All Pipeline Excetion:" + ex.Message + "\n" + ex.StackTrace);
			}
			return data;
		}

		public PipelineDTO GetById(string access_token, int id)
		{
			PipelineDTO data = new PipelineDTO();
			try
			{
				var response = GetAsync<DefaultResponse<Meta, PipelineDTO>>(String.Format(Constants.GET_PIPELINE_BY_ID_ENDPOINT, id), access_token, Constants.WEB_API_URL);

				if (response.Result.meta.errorCode == 200)
				{					
					data = response.Result.data;				
				}
			}
			catch (Exception ex)
			{
				Log.Error("Get Pipeline By Id Excetion:" + ex.Message + "\n" + ex.StackTrace);
			}
			return data;
		}

		public List<SectionDTO> GetAllSections(string access_token)
		{
			List<SectionDTO> data = new List<SectionDTO>();
			try
			{
				var response = GetAsync<DefaultResponse<Meta, List<SectionDTO>>>(Constants.GET_ALL_SECTION_ENDPOINT, access_token, Constants.WEB_API_URL);

				if (response.Result.meta.errorCode == 200)
				{
					if (response.Result.data.Count > 0)
					{
						data = response.Result.data;
					}
				}
			}
			catch (Exception ex)
			{
				Log.Error("Get All Section Excetion:" + ex.Message + "\n" + ex.StackTrace);
			}
			return data;
		}

		public SectionDTO GetSectionById(string access_token, int id)
		{
			SectionDTO data = new SectionDTO();
			try
			{
				var response = GetAsync<DefaultResponse<Meta, SectionDTO>>(String.Format(Constants.GET_SECTION_BY_ID_ENDPOINT,id), access_token, Constants.WEB_API_URL);

				if (response.Result.meta.errorCode == 200)
				{
					data = response.Result.data;
				}
			}
			catch (Exception ex)
			{
				Log.Error("Get Section By Id Excetion:" + ex.Message + "\n" + ex.StackTrace);
			}
			return data;
		}

		public List<PropertyDTO> GetAllProperty(string access_token)
		{
			List<PropertyDTO> data = new List<PropertyDTO>();
			try
			{
				var response = GetAsync<DefaultResponse<Meta, List<PropertyDTO>>>(Constants.GET_ALL_PROPERTY_ENDPOINT, access_token, Constants.WEB_API_URL);

				if (response.Result.meta.errorCode == 200)
				{
					if (response.Result.data.Count > 0)
					{
						data = response.Result.data;
					}
				}
			}
			catch (Exception ex)
			{
				Log.Error("Get All Property Excetion:" + ex.Message + "\n" + ex.StackTrace);
			}
			return data;
		}

		public PropertyDTO GetPropertyById(string access_token, int id)
		{
			PropertyDTO data = new PropertyDTO();
			try
			{
				var response = GetAsync<DefaultResponse<Meta, PropertyDTO>>(String.Format(Constants.GET_PROPERTY_BY_ID_ENDPOINT, id), access_token, Constants.WEB_API_URL);

				if (response.Result.meta.errorCode == 200)
				{
					data = response.Result.data;
				}
			}
			catch (Exception ex)
			{
				Log.Error("Get Property By Id Excetion:" + ex.Message + "\n" + ex.StackTrace);
			}
			return data;
		}

		public DefaultResponse<object> CreateSectionDetail(string access_token, AddSectionDetailReq dto)
		{
			try
			{
				var json = JsonConvert.SerializeObject(dto);
				var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
				var response = PostAsync<DefaultResponse<object>>(String.Format(Constants.CREATE_SECTION_DETAIL_ENDPOINT, dto.SectionId), access_token, stringContent, Constants.WEB_API_URL);
				return response.Result;
			}
			catch (Exception ex)
			{
				Log.Error("Create Section Detail Excetion:" + ex.Message + "\n" + ex.StackTrace);
				return null;
			}
		}

		public DefaultResponse<object> UpdateSectionDetail(string access_token, AddSectionDetailReq dto)
		{
			try
			{
				var json = JsonConvert.SerializeObject(dto);
				var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
				var response = PutAsync<DefaultResponse<object>>(String.Format(Constants.UPDATE_SECTION_DETAIL_ENDPOINT, dto.SectionId,dto.SectionDetailId), access_token, stringContent, Constants.WEB_API_URL);
				return response.Result;
			}
			catch (Exception ex)
			{
				Log.Error("Create Section Detail Excetion:" + ex.Message + "\n" + ex.StackTrace);
				return null;
			}
		}

		public SectionDetailDTO GetDetailById(string access_token, int id)
		{
			SectionDetailDTO data = new SectionDetailDTO();
			try
			{
				var response = GetAsync<DefaultResponse<Meta, SectionDetailDTO>>(String.Format(Constants.GET_SECTION_DETAIL_ENDPOINT, id), access_token, Constants.WEB_API_URL);

				if (response.Result.meta.errorCode == 200)
				{
					data = response.Result.data;
				}
			}
			catch (Exception ex)
			{
				Log.Error("Get Section Detail By Id Excetion:" + ex.Message + "\n" + ex.StackTrace);
			}
			return data;
		}

		public DefaultResponse<object> UpdateSectionOrder(string access_token, UpdateSectionOrderReq req)
		{
			try
			{
				var json = JsonConvert.SerializeObject(req);
				var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
				var response = PostAsync<DefaultResponse<object>>(Constants.UPDATE_SECTION_ORDER_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
				return response.Result;
			}
			catch (Exception ex)
			{
				Log.Error("Delete Pipeline Excetion:" + ex.Message + "\n" + ex.StackTrace);
				return null;
			}
		}
		public bool ChangePipeline(string access_token, ChangePipelineReq req)
		{
			try
			{
				var json = JsonConvert.SerializeObject(req);
				var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
				var response = PostAsync<DefaultResponse<object>>(Constants.CHANGE_PIPELINE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
				return response.Result.meta.errorCode == 200;
			}
			catch (Exception ex)
			{
				Log.Error("Change Pipeline Excetion:" + ex.Message + "\n" + ex.StackTrace);
				return false;
			}
		}
	}
}
