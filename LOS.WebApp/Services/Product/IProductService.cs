﻿using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Models;
using LOS.WebApp.Models.AISuggestionsInformation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LOS.DAL.EntityFramework;

namespace LOS.WebApp.Services
{
    public interface IProductService
    {
        ProductDetail GetCurrentPrice(string token, int ProductId);
        List<ProductDetail> GetProductByBrand(string token, int brandId);
        ProductDetail GetProduct(string token, int ProductId);
        BrandProductDetail GetBrand(string token, int brandId);
        List<LOS.WebApp.Models.ProductReview> GetProductReview(string token, int loanbriefId, int productId, int IdType = 0, decimal price = 0);
        bool InsertProductReviewResult(string access_token, ProductReviewResultObject productReviewResults);
        List<ProductDetail> Search(string access_token, string param, ref int recordTotals);
        bool Update(string access_token, int id, Product entity);
        bool Create(string access_token, Product entity);
        PriceMotorLogReq PrepareProductAiLogModel(string access_token, int ProductId);
        PriceMotorReq PrepareProductAiModel(string access_token, int ProductId, int LoanBriefId);
        decimal GetPriceAI(string access_token, int ProductId, int LoanBriefId);
        ProductDetail CheckFullNameProduct(string token, string fullName);
        List<LOS.WebApp.Models.ProductReview> GetProductReviewV2(string token, int loanbriefId, int productId, int productTypeId = 0, decimal price = 0);
    }
}
