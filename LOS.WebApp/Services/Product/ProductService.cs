﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.AISuggestionsInformation;
using LOS.WebApp.Services.AIService;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace LOS.WebApp.Services
{
    public class ProductService : BaseServices, IProductService
    {
        protected IConfiguration _baseConfig;
        private IPriceMotor _priceMotorService;
        public ProductService(IConfiguration configuration, IPriceMotor priceMotorService, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _baseConfig = configuration ?? throw new ArgumentNullException(nameof(configuration));
            this._priceMotorService = priceMotorService;
        }
        public ProductDetail GetCurrentPrice(string token, int ProductId)
        {
            var data = new ProductDetail();
            try
            {
                var result = GetAsync<DefaultResponse<ProductDetail>>(string.Format(Constants.PRODUCT_CURRENT_PRICE_ENDPOINT, ProductId), token, Constants.WEB_API_URL);
                if (result.Result.meta.errorCode == 200)
                    data = result.Result.data;
            }
            catch(Exception ex)
            {
            }
            return data;
        } 
        public List<ProductDetail> GetProductByBrand(string token, int brandId)
        {
            List<ProductDetail> data = new List<ProductDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<ProductDetail>>>(string.Format(Constants.PRODUCT_GET_PRODUCT_BY_BRAND, brandId), token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    { 
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Product/GetProductByBrand Exception");
                return null;
            }
            return data;
        }
        public ProductDetail GetProduct(string token, int ProductId)
        {
            var data = new ProductDetail();
            try
            {
                var result = GetAsync<DefaultResponse<ProductDetail>>(string.Format(Constants.PRODUCT_GET_PRODUCT, ProductId), token, Constants.WEB_API_URL);
                if (result.Result.meta.errorCode == 200)
                    data = result.Result.data;
            }
            catch (Exception ex)
            {
            }
            return data;
        }
        public BrandProductDetail GetBrand(string token, int brandId)
        {
            var data = new BrandProductDetail();
            try
            {
                var result = GetAsync<DefaultResponse<BrandProductDetail>>(string.Format(Constants.PRODUCT_GET_BRAND, brandId), token, Constants.WEB_API_URL);
                if (result.Result.meta.errorCode == 200)
                    data = result.Result.data;
            }
            catch (Exception ex)
            {
            }
            return data;
        }
        public List<LOS.WebApp.Models.ProductReview> GetProductReview(string token, int loanbriefId, int productId, int IdType = 0, decimal price = 0)
        {
            var data = new List<LOS.WebApp.Models.ProductReview>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LOS.WebApp.Models.ProductReview>>>(string.Format(Constants.PRODUCT_GET_PRODUCT_REVIEW, loanbriefId, productId, IdType, price), token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Appraiser/GetProductReview Exception");
                return null;
            }
            return data;
        }
        #region DuyNV 
        public bool InsertProductReviewResult(string access_token, ProductReviewResultObject productReviewResults)
        {
            try
            {
                var json = JsonConvert.SerializeObject(productReviewResults);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.INSERT_PRODUCT_REVIEW_RESULT, access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        public List<ProductDetail> Search(string access_token, string param, ref int recordsTotal)
        {
            List<ProductDetail> data = new List<ProductDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<ProductDetail>>>(Constants.GET_PRODUCT + param, access_token);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordsTotal = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }
        public bool Update(string access_token, int id, Product entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PutAsync<DefaultResponse<object>>(string.Format(Constants.PRODUCT_UPDATE, id), access_token, stringContent);
                return response.Result.meta.errorCode == 200;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Create(string access_token, Product entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(string.Format(Constants.PRODUCT_CREATE), access_token, stringContent);
                return response.Result.meta.errorCode == 200;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #region Factories model
        public PriceMotorLogReq PrepareProductAiLogModel(string access_token, int ProductId)
        {
            try
            {
                var phanh = "";
                var vanh = "";
                var product = GetProduct(access_token, ProductId) ?? new ProductDetail();
                var productBrand = GetBrand(access_token, product.BrandProductId) ?? new BrandProductDetail();
                var brand = productBrand.Name;
                var models = product.ShortName;
                if (product.BrakeType != null && product.BrakeType > 0)
                    phanh = LOS.Common.Helpers.ExtentionHelper.GetName((BrakeType)product.BrakeType);
                if (product.RimType != null && product.RimType > 0)
                    vanh = LOS.Common.Helpers.ExtentionHelper.GetName((RimType)product.RimType);
                var regdate = product.Year ?? 0;
                var requestLogPriceAI = new PriceMotorLogReq
                {
                    brand = brand,
                    model = models,
                    phanh = phanh,
                    vanh = vanh,
                    regdate = regdate,
                    data_raw = product.ShortName
                };
                return requestLogPriceAI;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public PriceMotorReq PrepareProductAiModel(string access_token, int ProductId, int LoanBriefId)
        {
            try
            {
                var phanh = "";
                var vanh = "";
                var product = GetProduct(access_token, ProductId) ?? new ProductDetail();
                var productBrand = GetBrand(access_token, product.BrandProductId) ?? new BrandProductDetail();
                var brand = productBrand.Name;
                var models = product.ShortName;
                if (product.BrakeType != null && product.BrakeType > 0)
                    phanh = LOS.Common.Helpers.ExtentionHelper.GetName((BrakeType)product.BrakeType);
                if (product.RimType != null && product.RimType > 0)
                    vanh = LOS.Common.Helpers.ExtentionHelper.GetName((RimType)product.RimType);

                var regdate = product.Year ?? 0;
                return new PriceMotorReq
                {
                    access_token = access_token,
                    brand = brand,
                    model = models,
                    phanh = phanh,
                    vanh = vanh,
                    regdate = regdate.ToString(),
                    LoanBriefId = LoanBriefId
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        public decimal GetPriceAI(string access_token, int ProductId, int LoanBriefId)
        {
            try
            {
                var productAiModel = PrepareProductAiModel(access_token, ProductId, LoanBriefId);
                if(productAiModel != null)
                {
                    var data = _priceMotorService.GetMotoPrice(productAiModel);
                    if (data != null && data.predicted_price > 0)
                        return  data.predicted_price * 1000000;
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public ProductDetail CheckFullNameProduct(string token, string fullName)
        {
            var data = new ProductDetail();
            try
            {
                var result = GetAsync<DefaultResponse<ProductDetail>>(string.Format(Constants.PRODUCT_CHECK_FULLNAME, fullName), token, Constants.WEB_API_URL);
                if (result.Result.meta.errorCode == 200)
                    data = result.Result.data;
            }
            catch (Exception ex)
            {
            }
            return data;
        }

        public List<LOS.WebApp.Models.ProductReview> GetProductReviewV2(string token, int loanbriefId, int productId, int productTypeId = 0, decimal price = 0)
        {
            var data = new List<LOS.WebApp.Models.ProductReview>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<LOS.WebApp.Models.ProductReview>>>(string.Format(Constants.PRODUCT_GET_PRODUCT_REVIEW_V2, loanbriefId, productId, productTypeId, price), token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Appraiser/GetProductReviewV2 Exception");
                return null;
            }
            return data;
        }

    }
}
