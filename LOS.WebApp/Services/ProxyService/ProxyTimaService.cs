﻿using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.Proxy;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IProxyTimaService
    {
        InfomationFamilyKalapa GetFamily(string access_token, string nationalCard, int loanbriefId);
        InsuranceInfo.Output GetInsuranceInfo(string access_token, string nationalId1, string nationalId2, string fullName, string dob, int loanbriefId);
    }
    public class ProxyTimaService : BaseServices, IProxyTimaService
    {
        protected IDictionaryService _dictionaryService;
        public ProxyTimaService(IConfiguration configuration, IHttpClientFactory clientFactory, IDictionaryService dictionaryService) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
            _dictionaryService = dictionaryService;

        }
        public InfomationFamilyKalapa GetFamily(string access_token, string nationalCard, int loanbriefId)
        {
            try
            {
                string url = _baseConfig["AppSettings:Proxy"] + string.Format(Constants.Kalapa_GetFamily, nationalCard);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "8JTLS9eqmSKXdQKdNWrtXCMV5DAhC3k7");
                request.AddHeader("account", "LOS");
                request.AddHeader("password", "3cmELn3UsqFkvERbuJdzHtUL7HU6uFuH");
                request.AddHeader("Content-Type", "application/json");
                var client = new RestClient(url);
                client.Timeout = 5000;
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.KalapaFamily,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = "",
                    LoanBriefId = loanbriefId,
                    Input = string.Format("{0} - {1}", nationalCard, loanbriefId),
                    CreatedAt = DateTime.Now
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = response.Content;
                    objLogCallApi.Status = (int)StatusCallApi.Success;
                    objLogCallApi.Output = json;
                    _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                    InfoFamilyRes data = JsonConvert.DeserializeObject<InfoFamilyRes>(json);
                    if (data.Result == 1 && !string.IsNullOrEmpty(data.Data))
                    {
                        data.Data = data.Data.Replace("\\", "");
                        return JsonConvert.DeserializeObject<InfomationFamilyKalapa>(data.Data);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public InsuranceInfo.Output GetInsuranceInfo(string access_token, string nationalId1, string nationalId2, string fullName, string dob, int loanbriefId)
        {
            try
            {
                string url = _baseConfig["AppSettings:Proxy"] + string.Format(Constants.ProxyGetInsuranceInfo, nationalId1, nationalId2, fullName, dob);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "8JTLS9eqmSKXdQKdNWrtXCMV5DAhC3k7");
                request.AddHeader("account", "LOS");
                request.AddHeader("password", "3cmELn3UsqFkvERbuJdzHtUL7HU6uFuH");
                request.AddHeader("Content-Type", "application/json");
                var client = new RestClient(url);
                client.Timeout = 5000;
                var objLogCallApi = new LogCallApi
                {
                    ActionCallApi = (int)ActionCallApi.InsuranceInfo,
                    LinkCallApi = url,
                    Status = (int)StatusCallApi.CallApi,
                    TokenCallApi = "",
                    LoanBriefId = loanbriefId,
                    CreatedAt = DateTime.Now
                };
                objLogCallApi.Id = _dictionaryService.AddLogCallApi(access_token, objLogCallApi);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = response.Content;
                    objLogCallApi.Status = (int)StatusCallApi.Success;
                    objLogCallApi.Output = json;
                    objLogCallApi.ModifyAt = DateTime.Now;

                    _dictionaryService.UpdateLogCallApi(access_token, objLogCallApi);
                    InsuranceInfo.Output data = JsonConvert.DeserializeObject<InsuranceInfo.Output>(json);

                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
