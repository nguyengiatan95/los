﻿using LOS.Common.Extensions;
using LOS.Common.Utils;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net.Http;

namespace LOS.WebApp.Services.ProxyService
{
    public interface ISendSmsBrandName
    {
        SendSMSParam.OutPut SendSMS(string access_token, string Phone, int loanbriefId);
    }
    public class SendSmsBrandName : BaseServices, ISendSmsBrandName
    {
        protected IAuthenticationAI _authenService;
        protected IDictionaryService _dictionaryService;
        public SendSmsBrandName(IConfiguration configuration, IHttpClientFactory clientFactory, IDictionaryService dictionaryService) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
            _dictionaryService = dictionaryService;

        }
        public SendSMSParam.OutPut SendSMS(string access_token, string Phone, int loanbriefId)
        {
            try
            {
                var content = _baseConfig["AppSettings:ContentSmsQualified"].ToString();
                if (!string.IsNullOrEmpty(content))
                {
                    var urlTraCuu = string.Format(LOS.Common.Helpers.Const.url_tra_cuu, EncryptUtils.Encrypt(loanbriefId.ToString(), false));
                    content = string.Format(content, urlTraCuu);
                }

                string url = _baseConfig["AppSettings:Proxy"] + Constants.SendSMS;
                SendSMSParam.Input param = new SendSMSParam.Input();
                param.Domain = "los.tima.vn";
                param.Department = "Hệ thống tự động";
                param.Phone = Phone;
                param.MessageContent = content;
                var body = JsonConvert.SerializeObject(param);
                var logSendSms = new LogSendSms
                {
                    LoanbriefId = loanbriefId,
                    TypeSendSms = (int)EnumTypeSendSMS.QualifiedAutoCall,
                    Url = url,
                    Smscontent = content,
                    Request = body,
                    Status = (int)StatusCallApi.Success,
                    CreatedAt = DateTime.Now
                };
                _dictionaryService.AddLogSendSms(access_token, logSendSms);
                var client = new RestClient(url);
                //client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "8JTLS9eqmSKXdQKdNWrtXCMV5DAhC3k7");
                request.AddHeader("account", "LOS");
                request.AddHeader("password", "3cmELn3UsqFkvERbuJdzHtUL7HU6uFuH");
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var json = response.Content;

                logSendSms.UpdatedAt = DateTime.Now;
                logSendSms.Response = json;
                _dictionaryService.UpdateLogSendSms(access_token, logSendSms);

                SendSMSParam.OutPut lst = JsonConvert.DeserializeObject<SendSMSParam.OutPut>(json);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }
}
