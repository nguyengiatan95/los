﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IReasonService
    {
        List<ReasonCancelDetail> GetReasonGroup(string access_token, int typeReason);
        List<ReasonCancelDetail> GetReasonDetail(string access_token, int parentId);
        Task<ReasonCancelDetail> GetDayInBlacklist(string access_token, int id);
    }
}
