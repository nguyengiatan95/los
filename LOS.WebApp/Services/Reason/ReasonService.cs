﻿using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public class ReasonService : BaseServices, IReasonService
    {
        public ReasonService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public List<ReasonCancelDetail> GetReasonGroup(string access_token, int typeReason)
        {
            var data = new List<ReasonCancelDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<ReasonCancelDetail>>>(string.Format(Constants.GET_LIST_REASON_GROUP_ENDPOINT, typeReason), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200 && response.Result.data.Count > 0)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<ReasonCancelDetail> GetReasonDetail(string access_token, int parentId)
        {
            var data = new List<ReasonCancelDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<ReasonCancelDetail>>>(string.Format(Constants.GET_LIST_REASON_DETAIL_ENDPOINT, parentId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200 && response.Result.data.Count > 0)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public async Task<ReasonCancelDetail> GetDayInBlacklist(string access_token, int id)
        {
            var data = new ReasonCancelDetail();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, ReasonCancelDetail>>(string.Format(Constants.GET_DAY_IN_BLACK_LIST, id), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    data = response.data; ;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
    }
}
