﻿using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.Object;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IRemarketingService
    {
        List<ReLoanbriefDetail> GetData(string access_token, string param, ref int recordTotals);
        Task<bool> ReCreateLoanbrief(string access_token, RemarketingLoanModel entity);
        List<int> GetDataAll(string access_token, string param);
    }
    public class RemarketingService : BaseServices, IRemarketingService
    {
        public RemarketingService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;

        }
        public List<ReLoanbriefDetail> GetData(string access_token, string param, ref int recordTotals)
        {
            List<ReLoanbriefDetail> data = new List<ReLoanbriefDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<ReLoanbriefDetail>>>(Constants.REMARKETING_GET_DATA_ENDPOINT + param, access_token, Constants.WEB_API_URL);

                if (response != null && response.Result != null && response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "RemarketingService/GetData Exception");
                return null;
            }
            return data;
        }

        public async Task<bool> ReCreateLoanbrief(string access_token, RemarketingLoanModel entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<bool>>(Constants.REMARKETING_RE_LOANBRIEF_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                return response.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<int> GetDataAll(string access_token, string param)
        {
            List<int> data = new List<int>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<int>>>(Constants.REMARKETING_GET_DATA_ALL_ENDPOINT + param, access_token, Constants.WEB_API_URL);
                if (response != null && response.Result != null && response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "RemarketingService/GetDataAll Exception");
                return null;
            }
            return data;
        }

    }
}
