﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static LOS.DAL.Object.Report.Report;

namespace LOS.WebApp.Services.Report
{
    public interface IReportService
    {
        List<LoanBriefDetail> ReporLoanRegisterOfUserMmo(string access_token, string param, ref int recordTotals);
        List<LoanBriefSuccessFullySoldDetail> ReporLoanSuccessFullySoldMMO(string access_token, string param, ref int recordTotals);
        Task<List<ReportApproveEmployee>> ReporApproveEmployee(string access_token, int month);
        Task<List<ReportEsignCustomer>> ReportEsignCustomer(string access_token, DateTime startDate, DateTime endDate);
    }
}
