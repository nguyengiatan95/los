﻿using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.Response;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.Report
{
    public class ReportService : BaseServices, IReportService
    {
        protected IConfiguration _baseConfig;
        public ReportService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
        }

        public async Task<List<DAL.Object.Report.Report.ReportApproveEmployee>> ReporApproveEmployee(string access_token, int month)
        {
            var data = new List<DAL.Object.Report.Report.ReportApproveEmployee>();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, List<DAL.Object.Report.Report.ReportApproveEmployee>>>(string.Format(Constants.REPORT_APPROVE_EMPLOYEE_ENDPOINT, month), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200 && response.data.Count > 0)
                    data = response.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<LoanBriefDetail> ReporLoanRegisterOfUserMmo(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefDetail> data = new List<LoanBriefDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefDetail>>>(Constants.REPORT_LOANBRIEF_REGISTER_BY_USERMMO_ENDPOINT + param, access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<LoanBriefSuccessFullySoldDetail> ReporLoanSuccessFullySoldMMO(string access_token, string param, ref int recordTotals)
        {
            List<LoanBriefSuccessFullySoldDetail> data = new List<LoanBriefSuccessFullySoldDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefSuccessFullySoldDetail>>>(Constants.REPORT_LOANBRIEF_SUCCESSFULLSOLD_BY_USERMMO_ENDPOINT + param, access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordTotals = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public async Task<List<DAL.Object.Report.Report.ReportEsignCustomer>> ReportEsignCustomer(string access_token, DateTime fromDate, DateTime toDate)
        {
            var data = new List<DAL.Object.Report.Report.ReportEsignCustomer>();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, List<DAL.Object.Report.Report.ReportEsignCustomer>>>(string.Format(Constants.REPORT_ESIGN_CUSTOMER_ENDPOINT, fromDate, toDate), access_token, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200 && response.data.Count > 0)
                    data = response.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
    }
}
