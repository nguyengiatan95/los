﻿using LOS.Common.Models.Response;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.ResultEkyc
{
    public interface IResultEkycService
    {
        DAL.EntityFramework.ResultEkyc GetResultEkycById(string access_token, int id);
        DAL.EntityFramework.ResultEkyc GetEkycByLoanBrief(string access_token, int loanBriefId);

        int Add(string access_token, DAL.EntityFramework.ResultEkyc entity);
    }
    public class ResultEkycService : BaseServices, IResultEkycService
    {
        public ResultEkycService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }
        public DAL.EntityFramework.ResultEkyc GetResultEkycById(string access_token, int id)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, DAL.EntityFramework.ResultEkyc>>(string.Format(Constants.RESULTEKYC_GET_BY_ID_ENDPOINT, id), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public DAL.EntityFramework.ResultEkyc GetEkycByLoanBrief(string access_token, int loanBriefId)
        {
            var data = new DAL.EntityFramework.ResultEkyc();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, DAL.EntityFramework.ResultEkyc>>(string.Format(Constants.RESULTEKYC_GET_BY_LOANBRIEFID_ENDPOINT, loanBriefId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public int Add(string access_token, DAL.EntityFramework.ResultEkyc entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.ADD_RESULTEKYC, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "ResultEkyc/Add Exception");
            }
            return 0;
        }
    }
}
