﻿using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Models;
using LOS.WebApp.Models.ReqSSO;
using LOS.WebApp.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static LOS.WebApp.Models.SSO;

namespace LOS.WebApp.Services
{
    public interface ISSOService
    {
        public Login Login(LoginReq req);
        ResponseUserSSO CreateUserSSO(string access_token, ResquestUserSSO entity);
        ResponseUserSSO UpdatePassword(string access_token, RequestUpdateUserSSO entity);
        ResponeGenNewOtp GenNewOtp(string access_token, string text);
    }
}
