﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LOS.Common.Extensions;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using LOS.WebApp.Models.ReqSSO;
using LOS.WebApp.Models.Request;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;

namespace LOS.WebApp.Services
{
    public class SSOService :ISSOService
    {
        private readonly IConfiguration _configuration;
        private readonly IDictionaryService _dictionaryService;
        public SSOService(IConfiguration configuration, IDictionaryService dictionaryService)
        {
            _configuration = configuration;
            _dictionaryService = dictionaryService;
        }

        public SSO.Login Login(LoginReq req)
        {
            var url = _configuration["AppSettings:SSO"] + Constants.LoginSSO;
            var client = new RestClient(url);
            var body = JsonConvert.SerializeObject(req);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            
            IRestResponse response = client.Execute(request);
            var json = response.Content;

            SSO.Login lst = JsonConvert.DeserializeObject<SSO.Login>(json);
            return lst;
        }
        public ResponseUserSSO CreateUserSSO(string access_token, ResquestUserSSO entity)
        {
            try
            {
                var url = _configuration["AppSettings:SSO"] + Constants.CreateUserSSO;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(entity);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", Constants.TokenSSO);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.CreateUserSSO.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = "",
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = ""
                };
                _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ResponseUserSSO lst = JsonConvert.DeserializeObject<ResponseUserSSO>(json);
                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ResponseUserSSO UpdatePassword(string access_token, RequestUpdateUserSSO entity)
        {
            try
            {
                var url = _configuration["AppSettings:SSO"] + Constants.UpdatePasswordUserSSO;
                var client = new RestClient(url);
                var body = JsonConvert.SerializeObject(entity);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", Constants.TokenSSO);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.UpdatePassSSO.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = Constants.TokenSSO,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = JsonConvert.SerializeObject(entity)
                };
                _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ResponseUserSSO lst = JsonConvert.DeserializeObject<ResponseUserSSO>(json);
                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ResponeGenNewOtp GenNewOtp(string access_token, string text)
        {
            try
            {
                var url = _configuration["AppSettings:SSO"] + string.Format(Constants.GenNewOtpSSO, text);
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", Constants.TokenSSO);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", ParameterType.RequestBody);
                //Add log
                var log = new LogCallApi
                {
                    ActionCallApi = ActionCallApi.GenNewOtp.GetHashCode(),
                    LinkCallApi = url,
                    TokenCallApi = Constants.TokenSSO,
                    Status = (int)StatusCallApi.CallApi,
                    LoanBriefId = 0,
                    Input = text
                };
                _dictionaryService.AddLogCallApi(access_token, log);
                IRestResponse response = client.Execute(request);
                var json = response.Content;
                log.Status = (int)StatusCallApi.Success;
                log.Output = json;
                _dictionaryService.UpdateLogCallApi(access_token, log);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ResponeGenNewOtp lst = JsonConvert.DeserializeObject<ResponeGenNewOtp>(json);
                    return lst;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
