﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IShopServices
    {
        List<ShopDetail> GetAllHub(string access_token);
        ShopDetail GetById(string access_token, int Id);
    }
}
