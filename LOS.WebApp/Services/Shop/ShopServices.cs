﻿using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public class ShopServices : BaseServices, IShopServices
    {
        public ShopServices(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public List<ShopDetail> GetAllHub(string access_token)
        {
            List<ShopDetail> data = new List<ShopDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<List<ShopDetail>>>(Constants.SHOP_GET_ALL_HUB_ENPOINT, access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                        data = response.Result.data;
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public ShopDetail GetById(string access_token, int Id)
        {
            var data = new ShopDetail();
            try
            {
                var response = GetAsync<DefaultResponse<ShopDetail>>(string.Format(Constants.SHOP_GET_BY_ID_ENPOINT, Id), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
    }
}
