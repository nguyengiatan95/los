﻿using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface ITelesalesServices
    {        
        DefaultResponse<object> UpdateCallStatus(string access_token, UpdateCallStatusReq entity);
		DefaultResponse<object> UpdateScheduleCall(string access_token, UpdateScheduleCallReq entity);
		List<LoanBriefTelesale> GetMissedCalls(string access_token, string param, ref int recordTotals);
		List<LoanBriefTelesale> GetScheduleCalls(string access_token, string param, ref int recordTotals);
		List<LoanBriefTelesale> GetCalled(string access_token, string param, ref int recordTotals);
		DefaultResponse<object> BoundTelesale(string access_token, BoundTelesaleReq entity);
		List<UserDetail> Search(string access_token, string param, ref int recordTotals);
		List<TelesalesShift> GetListTelesalesShift(string access_token);
		List<TeamTelesales> GetListTeamTelesales(string access_token);
		List<UserDetail> GetTelesalesByTeam(string access_token, string teamTelesales);
        TeamTelesales GetTelesalesTeam(string access_token, int teamTelesaleId);
    }
}
