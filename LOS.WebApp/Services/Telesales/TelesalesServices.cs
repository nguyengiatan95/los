﻿using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public class TelesalesServices : BaseServices, ITelesalesServices
    {
        protected IConfiguration _baseConfig;
        public TelesalesServices(IConfiguration configuration, IHttpClientFactory clientFactory) :base(configuration, clientFactory)
        {
            _baseConfig = configuration;
        }

		public List<LoanBriefTelesale> GetMissedCalls(string access_token, string param, ref int recordTotals)
		{
			List<LoanBriefTelesale> data = new List<LoanBriefTelesale>();
			try
			{
				var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefTelesale>>>(Constants.GET_MISSED_CALLS_ENDPOINT + param, access_token, Constants.WEB_API_URL);

				if (response.Result.meta.errorCode == 200)
				{
					if (response.Result.data.Count > 0)
					{
						recordTotals = response.Result.meta.totalRecords;
						data = response.Result.data;
					}
				}
			}
			catch (Exception ex)
			{

			}
			return data;
		}

		public List<LoanBriefTelesale> GetScheduleCalls(string access_token, string param, ref int recordTotals)
		{
			List<LoanBriefTelesale> data = new List<LoanBriefTelesale>();
			try
			{
				var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefTelesale>>>(Constants.GET_SCHEDULE_CALLS_ENDPOINT + param, access_token, Constants.WEB_API_URL);

				if (response.Result.meta.errorCode == 200)
				{
					if (response.Result.data.Count > 0)
					{
						recordTotals = response.Result.meta.totalRecords;
						data = response.Result.data;
					}
				}
			}
			catch (Exception ex)
			{

			}
			return data;
		}

		public List<LoanBriefTelesale> GetCalled(string access_token, string param, ref int recordTotals)
		{
			List<LoanBriefTelesale> data = new List<LoanBriefTelesale>();
			try
			{
				var response = GetAsync<DefaultResponse<SummaryMeta, List<LoanBriefTelesale>>>(Constants.GET_CALLED_ENDPOINT + param, access_token, Constants.WEB_API_URL);

				if (response.Result.meta.errorCode == 200)
				{
					if (response.Result.data.Count > 0)
					{
						recordTotals = response.Result.meta.totalRecords;
						data = response.Result.data;
					}
				}
			}
			catch (Exception ex)
			{

			}
			return data;
		}

		public DefaultResponse<object> UpdateCallStatus(string access_token, UpdateCallStatusReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
				var response = PostAsync<DefaultResponse<object>>(Constants.UPDATE_CALL_STATUS_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL).Result;
				return response;
            }
            catch(Exception ex)
            {
				DefaultResponse<object> def = new DefaultResponse<object>();
				def.meta = new Meta(500, "internal server error");
				return def;
            }
        }

		public DefaultResponse<object> UpdateScheduleCall(string access_token, UpdateScheduleCallReq entity)
		{
			try
			{
				var json = JsonConvert.SerializeObject(entity);
				var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
				var response = PostAsync<DefaultResponse<object>>(Constants.UPDATE_SCHEDULE_CALL_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL).Result;
				return response;
			}
			catch (Exception ex)
			{
				DefaultResponse<object> def = new DefaultResponse<object>();
				def.meta = new Meta(500, "internal server error");
				return def;
			}
		}

		public DefaultResponse<object> BoundTelesale(string access_token, BoundTelesaleReq entity)
		{
			try
			{
				var json = JsonConvert.SerializeObject(entity);
				var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
				var response = PostAsync<DefaultResponse<object>>(Constants.BOUND_TELESALE_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL).Result;
				return response;
			}
			catch (Exception ex)
			{
				DefaultResponse<object> def = new DefaultResponse<object>();
				def.meta = new Meta(500, "internal server error");
				return def;
			}
		}

		public List<UserDetail> Search(string access_token, string param, ref int recordsTotal)
		{
			List<UserDetail> data = new List<UserDetail>();
			try
			{
				var response = GetAsync<DefaultResponse<SummaryMeta, List<UserDetail>>>(Constants.GET_TELESALE_STAFF + param, access_token);

				if (response.Result.meta.errorCode == 200)
				{
					if (response.Result.data.Count > 0)
					{
						recordsTotal = response.Result.meta.totalRecords;
						data = response.Result.data;
					}
				}
			}
			catch (Exception ex)
			{

			}
			return data;
		}

		public List<TelesalesShift> GetListTelesalesShift(string access_token)
		{
			List<TelesalesShift> data = new List<TelesalesShift>();
			try
			{
				var response = GetAsync<DefaultResponse<List<TelesalesShift>>>(Constants.GET_LIST_TELESALES_SHIFT, access_token, Constants.WEB_API_URL);
				if (response.Result.meta.errorCode == 200)
				{
					if (response.Result.data.Count > 0)
						data = response.Result.data;
				}
			}
			catch (Exception ex)
			{

			}
			return data;
		}
		public List<TeamTelesales> GetListTeamTelesales(string access_token)
		{
			List<TeamTelesales> data = new List<TeamTelesales>();
			try
			{
				var response = GetAsync<DefaultResponse<List<TeamTelesales>>>(Constants.GET_LIST_TEAM_TELESALES, access_token, Constants.WEB_API_URL);
				if (response.Result.meta.errorCode == 200)
				{
					if (response.Result.data.Count > 0)
						data = response.Result.data;
				}
			}
			catch (Exception ex)
			{

			}
			return data;
		}

		public List<UserDetail> GetTelesalesByTeam(string access_token, string teamTelesales)
		{
			try
			{
				var response = GetAsync<DefaultResponse<Meta, List<UserDetail>>>(string.Format(Constants.GET_TELESALES_BY_TEAM, teamTelesales), access_token, Constants.WEB_API_URL);
				if (response.Result.meta.errorCode == 200)
					return response.Result.data;
			}
			catch (Exception ex)
			{

			}
			return null;
		}
        public TeamTelesales GetTelesalesTeam(string access_token, int teamTelesaleId)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, TeamTelesales>>(string.Format(Constants.GET_TELESALES_TEAM_ENDPOINT, teamTelesaleId), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
    }
}
