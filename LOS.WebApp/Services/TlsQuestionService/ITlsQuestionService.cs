﻿using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Models.TlsQuestionDto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.TlsQuestionService
{
    public interface ITlsQuestionService
    {
        Task<DefaultResponse<SummaryMeta, IEnumerable<TlsQuestionViewModel>>> GetAllAsync(string access_token, ApiPagingPostModel filterInput);
        Task<DefaultResponse<TlsQuestion>> GetByIdAsync(string access_token, int id);
        Task<DefaultResponse<TlsQuestion>> CreateOrUpdateAsync(string access_token, TlsQuestion input);
        Task<DefaultResponse<bool>> DeleteByIdAsync(string access_token, int id);
    }
}
