﻿using LOS.Common.Helpers;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.Common.Net;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.TlsQuestionDto;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.TlsQuestionService
{
    public class TlsQuestionService : BaseServices, ITlsQuestionService
    {
        protected IConfiguration _baseConfig;
        public TlsQuestionService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
        }

        public async Task<DefaultResponse<TlsQuestion>> CreateOrUpdateAsync(string access_token, TlsQuestion input)
        {
            var isCreate = input.Id <= 0;
            var output = new DefaultResponse<TlsQuestion> { meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE) };
            var host = Constants.WEB_API_URL + (isCreate ? Constants.INSERT_TLSQUESTIION : Constants.UPDATE_TLSQUESTIION);
            var client = new RestClient(host);
            var request = new RestRequest((isCreate ? Method.POST : Method.PATCH));
            request.AddHeader("Authorization", "bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(input);

            request.Timeout = 100000;

            var result = await client.ExecuteAsync(request);
            if (result.StatusCode != HttpStatusCode.OK || string.IsNullOrEmpty(result.Content))
            {
                Log.Error("Cannot connect to api service, host:" + host);
                Log.Error($"StatusCode: {result.StatusCode}, ErrorMessage: {result.ErrorMessage},  Content: {result.Content}, host: {host}");

                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return output;
            }
            try
            {
                if (isCreate)
                {
                    output = JsonConvert.DeserializeObject<DefaultResponse<TlsQuestion>>(result.Content);
                }
                else {
                    var outUpdate = JsonConvert.DeserializeObject<DefaultResponse<bool>>(result.Content);
                    output.meta = outUpdate.meta;
                }
                
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"TlsQuestionService/CreateOrUpdateAsync");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output.data = null;
            }

            return output;
        }

        public async Task<DefaultResponse<bool>> DeleteByIdAsync(string access_token, int id)
        {
            var output = new DefaultResponse<bool> { meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE) };
            var host = Constants.WEB_API_URL + Constants.MAIN_TLSQUESTIION + $"/{id}";
            var client = new RestClient(host);
            var request = new RestRequest(Method.DELETE);
            request.AddHeader("Authorization", "bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.Timeout = 100000;

            var result = await client.ExecuteAsync(request);
            if (result.StatusCode != HttpStatusCode.OK || string.IsNullOrEmpty(result.Content))
            {
                Log.Error("Cannot connect to api service, host:" + host);
                Log.Error($"StatusCode: {result.StatusCode}, ErrorMessage: {result.ErrorMessage},  Content: {result.Content}, host: {host}");

                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return output;
            }
            try
            {
                output = JsonConvert.DeserializeObject<DefaultResponse<bool>>(result.Content);
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"TlsQuestionService/DeleteByIdAsync");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output.data = false;
            }

            return output;
        }

        public async Task<DefaultResponse<SummaryMeta, IEnumerable<TlsQuestionViewModel>>> GetAllAsync(string access_token, ApiPagingPostModel filterInput)
        {
            DefaultResponse<SummaryMeta, IEnumerable<TlsQuestionViewModel>> output = new DefaultResponse<SummaryMeta, IEnumerable<TlsQuestionViewModel>>();

            var host = Constants.WEB_API_URL + Constants.GETALL_TLSQUESTIION + filterInput.ToQueryObject();
            var client = new RestClient(host);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(filterInput);
            request.Timeout = 100000;

            var result = await client.ExecuteAsync(request);
            if (result.StatusCode != HttpStatusCode.OK || string.IsNullOrEmpty(result.Content))
            {
                Log.Error("Cannot connect to api service");
                Log.Error($"StatusCode: {result.StatusCode}, ErrorMessage: {result.ErrorMessage},  Content: {result.Content}, host: {host}");

                output.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
                return output;
            }
            try
            {
                output = JsonConvert.DeserializeObject<DefaultResponse<SummaryMeta, IEnumerable<TlsQuestionViewModel>>>(result.Content);
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"TlsQuestion/GetAllAsync");
                output.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
                output.data = null;
            }


            return output;
            //dynamic captchaRequest = JObject.Parse(result.Content);
            //
        }

        public async Task<DefaultResponse<TlsQuestion>> GetByIdAsync(string access_token, int id)
        {
            var output = new DefaultResponse<TlsQuestion>();
            var host = Constants.WEB_API_URL + Constants.MAIN_TLSQUESTIION + $"/{id}";
            var client = new RestClient(host);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.Timeout = 100000;

            var result = await client.ExecuteAsync(request);
            if (result.StatusCode != HttpStatusCode.OK || string.IsNullOrEmpty(result.Content))
            {
                Log.Error("Cannot connect to api service");
                Log.Error($"StatusCode: {result.StatusCode}, ErrorMessage: {result.ErrorMessage},  Content: {result.Content}, host: {host}");

                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return output;
            }
            try
            {
                output = JsonConvert.DeserializeObject<DefaultResponse<TlsQuestion>>(result.Content);
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"TlsQuestion/GetAllAsync");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output.data = null;
            }

            return output;
        }
    }
}
