﻿using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Models.TlsScenarioDto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.TlsScenarioService
{
    public interface ITlsScenarioService
    {
        Task<DefaultResponse<SummaryMeta, IEnumerable<TlsScenario>>> GetAllAsync(string access_token, ApiPagingPostModel filterInput);
        Task<DefaultResponse<TlsScenario>> GetByIdAsync(string access_token, int id);
        Task<DefaultResponse<TlsScenario>> CreateOrUpdateAsync(string access_token, CreateTlsScenarioPostModel input);
        Task<DefaultResponse<bool>> DeleteByIdAsync(string access_token, int id);
        Task<DefaultResponse<IEnumerable<int>>> GetScenarioQuestionIds(string access_token, int scenarioId);
    }
}
