﻿using LOS.Common.Helpers;
using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.Common.Net;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.TlsScenarioDto;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.TlsScenarioService
{
    public class TlsScenarioService : BaseServices, ITlsScenarioService
    {
        protected IConfiguration _baseConfig;
        public TlsScenarioService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
        }

        public async Task<DefaultResponse<TlsScenario>> CreateOrUpdateAsync(string access_token, CreateTlsScenarioPostModel input)
        {
            #region Create/Update Scenario
            var isCreate = input.Id <= 0;
            var output = new DefaultResponse<TlsScenario> { meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE) };
            var host = Constants.WEB_API_URL + (isCreate ? Constants.INSERT_TLSSCENARIO : Constants.UPDATE_TLSSCENARIO);
            var client = new RestClient(host);
            var request = new RestRequest((isCreate ? Method.POST : Method.PATCH));
            request.AddHeader("Authorization", "bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(input);

            request.Timeout = 100000;

            var result = await client.ExecuteAsync(request);
            if (result.StatusCode != HttpStatusCode.OK || string.IsNullOrEmpty(result.Content))
            {
                Log.Error("Cannot connect to api service, host:" + host);
                Log.Error($"StatusCode: {result.StatusCode}, ErrorMessage: {result.ErrorMessage},  Content: {result.Content}, host: {host}");

                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return output;
            }
            try
            {
                if (isCreate)
                {
                    output = JsonConvert.DeserializeObject<DefaultResponse<TlsScenario>>(result.Content);
                }
                else
                {
                    var outUpdate = JsonConvert.DeserializeObject<DefaultResponse<bool>>(result.Content);
                    output.meta = outUpdate.meta;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, $"TlsQuestionService/CreateOrUpdateAsync");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output.data = null;
            }
            #endregion
            if (output.meta.errorCode != ResponseHelper.SUCCESS_CODE)
            {
                return output;
            }

            #region MappingToScenarioQuestion
            if (output.data!= null)
            {
                input.Id = output.data.Id;
            }

            host = Constants.WEB_API_URL + Constants.MAPPING_TLSSCENARIOQUESTION;
            client = new RestClient(host);
            request = new RestRequest(Method.PATCH);
            request.AddHeader("Authorization", "bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(new { ScenarioId = input.Id, QuestionIds = input.QuestionIds });

            request.Timeout = 100000;

            result = await client.ExecuteAsync(request);

            if (result.StatusCode != HttpStatusCode.OK || string.IsNullOrEmpty(result.Content))
            {
                Log.Error("Cannot connect to api service, host:" + host);
                Log.Error($"StatusCode: {result.StatusCode}, ErrorMessage: {result.ErrorMessage},  Content: {result.Content}, host: {host}");

                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return output;
            }
            try
            {
                var outUpdate = JsonConvert.DeserializeObject<DefaultResponse<bool>>(result.Content);
                output.meta = outUpdate.meta;
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"TlsQuestionService/CreateOrUpdateAsync");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output.data = null;
            }
            #endregion

            return output;
        }

        public async Task<DefaultResponse<bool>> DeleteByIdAsync(string access_token, int id)
        {
            var output = new DefaultResponse<bool> {  meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE) };
            var host = Constants.WEB_API_URL + Constants.MAIN_TLSSCENARIO + $"/{id}";
            var client = new RestClient(host);
            var request = new RestRequest(Method.DELETE);
            request.AddHeader("Authorization", "bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.Timeout = 100000;

            var result = await client.ExecuteAsync(request);
            if (result.StatusCode != HttpStatusCode.OK || string.IsNullOrEmpty(result.Content))
            {
                Log.Error("Cannot connect to api service");
                Log.Error($"StatusCode: {result.StatusCode}, ErrorMessage: {result.ErrorMessage},  Content: {result.Content}");

                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return output;
            }
            try
            {
                output = JsonConvert.DeserializeObject<DefaultResponse<bool>>(result.Content);
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"TlsScenario/DeleteByIdAsync");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output.data = false;
            }
            
            return output;
        }

        public async Task<DefaultResponse<SummaryMeta, IEnumerable<TlsScenario>>> GetAllAsync(string access_token, ApiPagingPostModel filterInput)
        {
            DefaultResponse<SummaryMeta, IEnumerable<TlsScenario>> output = new DefaultResponse<SummaryMeta, IEnumerable<TlsScenario>>();

            var host = Constants.WEB_API_URL + Constants.GETALL_TLSSCENARIO + filterInput.ToQueryObject();
            var client = new RestClient(host);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(filterInput);
            request.Timeout = 100000;

            var result = await client.ExecuteAsync(request);
            if (result.StatusCode != HttpStatusCode.OK || string.IsNullOrEmpty(result.Content))
            {
                Log.Error("Cannot connect to api service");
                Log.Error($"StatusCode: {result.StatusCode}, ErrorMessage: {result.ErrorMessage},  Content: {result.Content}, host: {host}");

                output.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE, 0, 0, 0);
                return output;
            }
            try
            {
                output = JsonConvert.DeserializeObject<DefaultResponse<SummaryMeta, IEnumerable<TlsScenario>>>(result.Content);
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"TlsScenario/GetAllAsync");
                output.meta = new SummaryMeta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE,0,0,0);
                output.data = null;
            }


            return output;
            //dynamic captchaRequest = JObject.Parse(result.Content);
            //
        }

        public async Task<DefaultResponse<TlsScenario>> GetByIdAsync(string access_token, int id)
        {
            var output = new DefaultResponse<TlsScenario>();
            var host = Constants.WEB_API_URL + Constants.MAIN_TLSSCENARIO + $"/{id}";
            var client = new RestClient(host);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.Timeout = 100000;

            var result = await client.ExecuteAsync(request);
            if (result.StatusCode != HttpStatusCode.OK || string.IsNullOrEmpty(result.Content))
            {
                Log.Error("Cannot connect to api service");
                Log.Error($"StatusCode: {result.StatusCode}, ErrorMessage: {result.ErrorMessage},  Content: {result.Content}, host: {host}");

                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return output;
            }
            try
            {
                output = JsonConvert.DeserializeObject<DefaultResponse<TlsScenario>>(result.Content);
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"TlsScenario/GetAllAsync");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output.data = null;
            }
            
            return output;
        }

        public async Task<DefaultResponse<IEnumerable<int>>> GetScenarioQuestionIds(string access_token, int scenarioId)
        {
            DefaultResponse<IEnumerable<int>> output = new DefaultResponse<IEnumerable<int>>();
            var host = Constants.WEB_API_URL + string.Format(Constants.GETQUESTIONIDS_TLSSCENARIOQUESTION, scenarioId);
            var client = new RestClient(host);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            //request.AddJsonBody(filterInput);
            request.Timeout = 100000;

            var result = await client.ExecuteAsync(request);
            if (result.StatusCode != HttpStatusCode.OK || string.IsNullOrEmpty(result.Content))
            {
                Log.Error("Cannot connect to api service");
                Log.Error($"StatusCode: {result.StatusCode}, ErrorMessage: {result.ErrorMessage},  Content: {result.Content}, host: {host}");

                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                return output;
            }
            try
            {
                output = JsonConvert.DeserializeObject<DefaultResponse<IEnumerable<int>>>(result.Content);
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"TlsScenario/GetScenarioQuestionIds");
                output.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
                output.data = null;
            }
            return output;
        }
    }
}
