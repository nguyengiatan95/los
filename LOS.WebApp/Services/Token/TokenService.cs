﻿using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface ITokenService
    {
        TokenDetail GetLastToken(string access_token, string app_id);
        int AddToken(string access_token, TokenDetail entity);
    }
    public class TokenService : BaseServices, ITokenService
    {
        public TokenService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public int AddToken(string access_token, TokenDetail entity)
        {
            int Result = 0;
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.TOKEN_ADD_ENPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    Result = (int)response.Result.data;
            }
            catch (Exception ex)
            {
                return Result;
            }
            return Result;
        }

        public TokenDetail GetLastToken(string access_token, string app_id)
        {
            var data = new TokenDetail();
            try
            {
                var response = GetAsync<DefaultResponse<TokenDetail>>(string.Format(Constants.TOKEN_GET_LAST_ENPOINT, app_id), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    data = response.Result.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
    }
}
