﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface ITokenSmartDailerService
    {
        TokenSmartDailerDetail GetLastToken(string access_token, int userId);
        int AddToken(string access_token, TokenSmartDailerDTO entity);
    }
}
