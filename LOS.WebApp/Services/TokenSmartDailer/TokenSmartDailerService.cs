﻿using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public class TokenSmartDailerService: BaseServices, ITokenSmartDailerService
    {
        public TokenSmartDailerService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }

        public int AddToken(string access_token, TokenSmartDailerDTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.SMARTDAILER_ADD_TOKEN_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    return (int)response.Result.data;

            }
            catch (Exception ex)
            {
            }
            return 0;
        }

        public TokenSmartDailerDetail GetLastToken(string access_token, int userId)
        {
           
            try
            {
                var result = GetAsync<DefaultResponse<Meta, TokenSmartDailerDetail>>(string.Format(Constants.SMARTDAILER_LAST_TOKEN_ENDPOINT, userId), access_token, Constants.WEB_API_URL);
                if (result.Result != null && result.Result.meta.errorCode == 200)
                    return result.Result.data;
            }
            catch
            {
              
            }
            return null;

        }
    }
}
