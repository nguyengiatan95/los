﻿using LOS.Common.Extensions;
using LOS.Common.Models.Response;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using static LOS.DAL.Object.Tool.ToolReq;

namespace LOS.WebApp.Services.ToolService
{
    public interface IToolService
    {
        Task<bool> ChangePhone(string access_token, ChangePhoneReq req);
        Task<DefaultResponse<object>> GetLogSendOTP(string access_token, string search, int type);
        Task<bool> ReEsignCustomer(string access_token, ReEsignReq req);
    }
    public class ToolService : BaseServices, IToolService
    {
        public ToolService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {

        }


        public async Task<bool> ChangePhone(string access_token, ChangePhoneReq req)
        {
            try
            {
                var json = JsonConvert.SerializeObject(req);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.CHANGE_PHONE_ENPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Tool/ChangePhone Exception");
            }
            return false;
        }

        public async Task<DefaultResponse<object>> GetLogSendOTP(string access_token, string search, int type)
        {
            try
            {
                var response = await GetAsync<DefaultResponse<object>>(string.Format(Constants.TOOL_GET_LOG_SEND_OTP_ENDPOINT, search, type), access_token, Constants.WEB_API_URL);
                return response;
            }
            catch (Exception ex)
            {
                DefaultResponse<object> def = new DefaultResponse<object>();
                def.meta = new Meta(500, "internal server error");
                return def;
            }
        }

        public async Task<bool> ReEsignCustomer(string access_token, ReEsignReq req)
        {
            try
            {
                var json = JsonConvert.SerializeObject(req);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.RE_ESIGN_CUSTOMER_ENPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.meta.errorCode == 200)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Tool/ReEsignCustomer Exception");
            }
            return false;
        }
    }
}
