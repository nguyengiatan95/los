﻿using LOS.DAL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.UserMMO
{
    public interface IUserMMOService
    {
        List<UserMMODetail> Search(string access_token, string param, ref int recordTotals);
        UserMMODetail GetById(string access_token, int id);
        UserMMODetail GetByUser(string access_token, string email);
        UserMMODetail GetByPhone(string access_token, string phone);
        bool Update(string access_token, int id, UserMMODTO entity);
        int Create(string access_token, UserMMODTO entity);
        bool Delete(string access_token, int id);
    }
}
