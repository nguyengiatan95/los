﻿
using LOS.DAL.DTOs;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOS.WebApp.Services.UserMMO
{
    public class UserMMOService : BaseServices, IUserMMOService
    {
        protected IConfiguration _baseConfig;
        public UserMMOService(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
        }

        public UserMMODetail GetById(string access_token, int id)
        {
            var result = new UserMMODetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, UserMMODetail>>(string.Format(Constants.USERSMMO_GET_ENDPOINT, id), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public UserMMODetail GetByUser(string access_token, string email)
        {
            var result = new UserMMODetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, UserMMODetail>>(string.Format(Constants.USERSMMO_GET_USERNAME_ENDPOINT, email), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public UserMMODetail GetByPhone(string access_token, string phone)
        {
            var result = new UserMMODetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, UserMMODetail>>(string.Format(Constants.USERSMMO_GET_PHONE_ENDPOINT, phone), access_token, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public List<UserMMODetail> Search(string access_token, string param, ref int recordsTotal)
        {
            List<UserMMODetail> data = new List<UserMMODetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<UserMMODetail>>>(Constants.USERSMMO_ENDPOINT + param, access_token, Constants.WEB_API_URL);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordsTotal = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public bool Update(string access_token, int id, UserMMODTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PutAsync<DefaultResponse<object>>(string.Format(Constants.USERSMMO_UPDATE_ENDPOINT, id), access_token, stringContent, Constants.WEB_API_URL);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int Create(string access_token, UserMMODTO entity)
        {
            int Result = 0;
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<dynamic>>(Constants.USERSMMO_ENDPOINT, access_token, stringContent, Constants.WEB_API_URL);
                if (response.Result.meta.errorCode == 200)
                    Result = (int)response.Result.data;
            }
            catch (Exception ex)
            {
                return Result;
            }
            return Result;
        }

        public bool Delete(string access_token, int id)
        {
            try
            {
                var response = DeleteAsync<DefaultResponse<object>>(string.Format(Constants.USERSMMO_DELETE_ENDPOINT, id), access_token, Constants.WEB_API_URL);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
