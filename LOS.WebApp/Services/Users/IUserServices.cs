﻿using LOS.Common.Models.Request;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Services
{
    public interface IUserServices
    {
        List<UserDetail> Search(string access_token, string param, ref int recordTotals);
        UserDetail GetById(string access_token, int id);
        UserDetail GetByUser(string access_token, string username);
        bool Update(string access_token, int id, UserDTO entity);
        bool Create(string access_token, UserDTO entity);
        bool Delete(string access_token, int id);
        List<UserDetail> GetStaff(string access_token, int HubId);
        List<UserDetail> GetManager(string access_token, int HubId);
        UserDetail GetPermission(string access_token, int userId);
        bool UpdatePermission(string access_token, PermissionUserDTO entity);
        UserPassword GetPasswordById(string access_token, int id);
        List<UserDetail> GetListAccountOfTima(string access_token, string param, ref int recordTotals);
        List<UserDetail> GetListUserByIpPhone(string access_token, GetUserReq req);
        bool UpdatePassword(string access_token, int id, UserDTO entity);
        UserDetail UserLoanAsc(string access_token, int HubId);
        UserDetail UserLoanAsc(string access_token, HubEmployeeReq entity);
        bool ChangeInformation(string access_token, User entity);
        List<UserDetail> GetFieldHo(string access_token, int provinceId);
        List<UserDetail> GetMultiGroup(string access_token, int group1, int group2);
        List<UserGroup> GetUser(string access_token, List<int> param);
    }
}
