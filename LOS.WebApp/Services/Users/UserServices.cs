﻿using LOS.Common.Models.Request;
using LOS.Common.Models.Response;
using LOS.DAL.DTOs;
using LOS.DAL.EntityFramework;
using LOS.WebApp.Helpers;
using LOS.WebApp.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace LOS.WebApp.Services
{
    public class UserServices : BaseServices,IUserServices
    {
        protected IConfiguration _baseConfig;
        public UserServices(IConfiguration configuration, IHttpClientFactory clientFactory) :base(configuration, clientFactory)
        {
            _baseConfig = configuration;
        }

        public UserDetail GetById(string access_token, int id)
        {
            var result = new UserDetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, UserDetail>>(string.Format(Constants.USERS_GET_ENDPOINT, id), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch(Exception ex)
            {
            }
            return result;
        }

        public UserDetail GetByUser(string access_token, string username)
        {
            var result = new UserDetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, UserDetail>>(string.Format(Constants.USERS_GET_USERNAME_ENDPOINT, username), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch(Exception ex)
            {
            }
            return result;
        }

        public List<UserDetail> Search(string access_token, string param, ref int recordsTotal)
        {
            List<UserDetail> data = new List<UserDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<UserDetail>>>(Constants.USERS_ENDPOINT + param, access_token);
               
                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordsTotal = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch(Exception ex)
            {
                
            }
            return data;
        }

        public bool Update(string access_token, int id, UserDTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PutAsync<DefaultResponse<object>>(string.Format(Constants.USERS_UPDATE_ENDPOINT, id), access_token, stringContent);
                return response.Result.meta.errorCode == 200;              

            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool Create(string access_token, UserDTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.USERS_ENDPOINT, access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Delete(string access_token, int id)
        {
            try
            {
                var response = DeleteAsync<DefaultResponse<object>>(string.Format(Constants.USERS_DELETE_ENDPOINT, id), access_token);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<UserDetail> GetStaff(string access_token, int HubId)
        {
            var result = new List<UserDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<UserDetail>>>(string.Format(Constants.USERS_GET_STAFF_HUB, HubId), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public List<UserDetail> GetManager(string access_token, int HubId)
        {
            var result = new List<UserDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<UserDetail>>>(string.Format(Constants.USERS_GET_MANAGER_HUB, HubId), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public UserDetail GetPermission(string access_token, int userId)
        {
            var result = new UserDetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, UserDetail>>(string.Format(Constants.USERS_GET_PERMISSION_ENDPOINT, userId), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public bool UpdatePermission(string access_token, PermissionUserDTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.USERS_UPDATE_PERMISSION_ENDPOINT, access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public UserPassword GetPasswordById(string access_token, int id)
        {
            var result = new UserPassword();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, UserPassword>>(string.Format(Constants.USERS_GET_PASSWORD, id), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        public List<UserDetail> GetListAccountOfTima(string access_token, string param, ref int recordsTotal)
        {
            List<UserDetail> data = new List<UserDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<SummaryMeta, List<UserDetail>>>(Constants.USERS_GET_LIST_ACCOUNT_OF_TIMA + param, access_token);

                if (response.Result.meta.errorCode == 200)
                {
                    if (response.Result.data.Count > 0)
                    {
                        recordsTotal = response.Result.meta.totalRecords;
                        data = response.Result.data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<UserDetail> GetListUserByIpPhone(string access_token, GetUserReq req)
        {
            var result = new List<UserDetail>();
            try
            {
                var json = JsonConvert.SerializeObject(req);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<Meta, List<UserDetail>>>(Constants.USERS_GET_USER_BY_IPPHONE, access_token, stringContent);
                if (response.Result.meta.errorCode == 200)
                {
                    result = response.Result.data;
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public bool UpdatePassword(string access_token, int id, UserDTO entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.USERS_UPDATE_PASSWORD_ENDPOINT, access_token, stringContent);
                return response.Result.meta.errorCode == 200;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public UserDetail UserLoanAsc(string access_token, int HubId)
        {
            var result = new UserDetail();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, UserDetail>>(string.Format(Constants.USERS_GET_USER_LOAN_ASC_ENDPOINT, HubId), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public UserDetail UserLoanAsc(string access_token, HubEmployeeReq entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<UserDetail>>(Constants.USERS_HUB_EMPLOYEE_LOAN_ASC_ENDPOINT, access_token, stringContent);
                if (response.Result.meta.errorCode == 200)
                    return response.Result.data;

            }
            catch (Exception ex)
            {
                
            }
            return null;
        }


        public bool ChangeInformation(string access_token, User entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<DefaultResponse<object>>(Constants.USERS_CHANGE_INFORMATION, access_token, stringContent);
                return response.Result.meta.errorCode == 200;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<UserDetail> GetFieldHo(string access_token, int provinceId)
        {
            var result = new List<UserDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<UserDetail>>>(string.Format(Constants.USERS_FIELD_HO, provinceId), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public List<UserDetail> GetMultiGroup(string access_token, int group1, int group2)
        {
            var result = new List<UserDetail>();
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<UserDetail>>>(string.Format(Constants.USERS_GET_MULTI_USER, group1, group2), access_token);
                if (response.Result.meta.errorCode == 200)
                    result = response.Result.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        public List<UserGroup> GetUser(string access_token, List<int> param)
        {
            try
            {
                var response = GetAsync<DefaultResponse<Meta, List<UserGroup>>>(string.Format(Constants.GET_USER_BY_LIST_USERID, string.Join(",", param)), access_token, Constants.WEB_API_URL);
                if (response.Result != null && response.Result.meta.errorCode == 200)
                    return response.Result.data;
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
