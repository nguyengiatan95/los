﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using AutoMapper;
using LOS.WebApp.Cache;
using LOS.WebApp.Helpers;
using LOS.WebApp.Services;
using LOS.WebApp.Services.AIService;
using LOS.WebApp.Services.CiscoService;
using LOS.WebApp.Services.ManagerAreaHub;
using LOS.WebApp.Services.Report;
using LOS.WebApp.Services.ResultEkyc;
using LOS.WebApp.Services.TlsQuestionService;
using LOS.WebApp.Services.TlsScenarioService;
using LOS.WebApp.Services.UserMMO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.DataProtection;
using StackExchange.Redis;
using LOS.WebApp.Services.ProxyService;
using LOS.WebApp.Services.ContactCustomer;
using LOS.WebApp.Services.Location;
using LOS.WebApp.Services.ToolService;
using LOS.DAL.Object.Appsettings;
using LOS.Services.Services.Trandata;
using LOS.DAL.EntityFramework;
using Microsoft.EntityFrameworkCore;
using LOS.DAL.UnitOfWork;
using LOS.Services.Services.Province;
using LOS.DAL.Respository;
using LOS.Services.Services.Loanbrief;
using LOS.Services.Services.DrawImage;
using LOS.Services.Services.District;
using LOS.Services.Services.Ward;
using LOS.Services.Services;
using LOS.WebApp.Factories;
using LOS.Services.Services.LogLoanInfoAI;
using LOS.Services.Services.LoanBriefNote;
using LOS.Services.Services.LogCallApi;
using LOS.Services.Services.LMS;
using LOS.Services.Services.Approve;
using LOS.Services.Services.Telesales;
using LOS.Services.Services.ConfigProductDetail;

namespace LOS.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            var redis = ConnectionMultiplexer.Connect(Environment.GetEnvironmentVariable("REDIS"));

            //map IDatabase redis to db
            //services.AddScoped(s => redis.GetDatabase(Constants.REDIS_DB_CACHE));

            services
                .AddDataProtection()
                .PersistKeysToStackExchangeRedis(redis, "DataProtectionKeys");

            services.AddStackExchangeRedisCache(option =>
            {
                option.Configuration = Environment.GetEnvironmentVariable("REDIS");
                option.InstanceName = "RedisInstance";
            });

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(12);
                options.Cookie.IsEssential = true;
                options.Cookie.Name = "TimaLOS-WebApp";
            });
            ////anti forgerytoken 
            //services.AddAntiforgery(options =>
            //{
            //    options.Cookie.Name = "X-CSRF-TOKEN-MOONGLADE";
            //    options.FormFieldName = "CSRF-TOKEN-MOONGLADE-FORM";
            //});
            //services.AddSession(options =>
            //{
            //    // Set a short timeout for easy testing.
            //    options.IdleTimeout = TimeSpan.FromHours(2);
            //    options.Cookie.HttpOnly = true;
            //    // Make the session cookie essential
            //    options.Cookie.IsEssential = true;

            //    // You might want to only set the application cookies over a secure connection:
            //    //options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
            //    //options.Cookie.SameSite = SameSiteMode.Strict;

            //});

            //services.AddHealthChecks();

            // Automapper
            services.AddAutoMapper();

            services.AddMvc(option => option.EnableEndpointRouting = false).AddControllersAsServices().AddRazorRuntimeCompilation();

            //services.Configure<RequestLocalizationOptions>(
            //            opts =>
            //            {
            //                var supportedCultures = new[]
            //                {
            //                    new CultureInfo("vi-VN"),
            //                };

            //                opts.DefaultRequestCulture = new RequestCulture("vi-VN");
            //                // Formatting numbers, dates, etc.
            //                opts.SupportedCultures = supportedCultures;
            //                // UI strings that we have localized.
            //                opts.SupportedUICultures = supportedCultures;
            //            });



            //DI


            services.AddDbContext<LOSContext>(options =>
              options.UseSqlServer(Configuration.GetConnectionString("LOSDatabase")), ServiceLifetime.Transient);

            services.AddTransient<IUnitOfWork, UnitOfWork>();

            #region Province
            services.AddTransient<IProvinceRepository, ProvinceRepository>();
            services.AddTransient<IProvinceService, ProvinceService>();
            #endregion

            #region Loanbrief
            services.AddTransient<ILoanbriefRepository, LoanbriefRepository>();
            services.AddTransient<ILoanbriefV2Service, LoanbriefV2Service>();
            #endregion

            #region LoanbriefRelationship
            services.AddTransient<ILoanbriefRelationshipRepository, LoanbriefRelationshipRepository>();
            services.AddTransient<IRelationshipService, RelationshipService>();
            #endregion

            #region Approve
            services.AddTransient<IApproveRepository, ApproveRepository>();
            services.AddTransient<IApproveV2Service, ApproveV2Service>();
            #endregion

            #region District
            services.AddTransient<IDistrictRepository, DistrictRepository>();
            services.AddTransient<IDistrictService, DistrictService>();
            #endregion

            #region Ward
            services.AddTransient<IWardRepository, WardRepository>();
            services.AddTransient<IWardService, WardService>();
            #endregion

            #region LogDistributionUser
            services.AddTransient<ILogDistributionUserRepository, LogDistributionUserRepository>();
            services.AddTransient<ILogDistributionUserService, LogDistributionUserService>();
            #endregion

            #region User
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUserV2Service, UserV2Service>();
            #endregion

            #region Loanbrief Factories
            services.AddTransient<ILoanbriefModelFactory, LoanbriefModelFactory>();
            #endregion

            #region LogLoanInfoAI
            services.AddTransient<ILogLoanInfoAIRespository, LogLoanInfoAIRespository>();
            services.AddTransient<ILogLoanInfoAIV2Service, LogLoanInfoAIV2Service>();
            #endregion

            #region LoanBriefNote
            services.AddTransient<ILoanBriefNoteRespository, LoanBriefNoteRespository>();
            services.AddTransient<ILoanBriefNoteV2Service, LoanBriefNoteV2Service>();
            #endregion

            #region LogCallApi
            services.AddTransient<ILogCallApiRespository, LogCallApiRespository>();
            services.AddTransient<ILogCallApiV2Service, LogCallApiV2Service>();
            #endregion

            #region LMS
            services.AddTransient<ILMSV2Service, LMSV2Service>();
            #endregion

            #region Telesales
            services.AddTransient<ITelesalesRepository, TelesalesRepository>();
            services.AddTransient<ITelesalesV2Service, TelesalesV2Service>();
            #endregion

            #region LogLoanAction
            services.AddTransient<ILogLoanActionRespository, LogLoanActionRespository>();
            services.AddTransient<ILogLoanActionV2Service, LogLoanActionV2Service>();
            #endregion

            #region RuleCheckLoan
            services.AddTransient<IRuleCheckLoanRepository, RuleCheckLoanRepository>();
            services.AddTransient<IRuleCheckLoanService, RuleCheckLoanService>();
            #endregion

            #region LogReLoanbrief
            services.AddTransient<ILogReLoanBriefRepository, LogReLoanBriefRepository>();
            services.AddTransient<ILogReLoanBriefService, LogReLoanBriefService>();
            #endregion

            #region LogCloseLoan
            services.AddTransient<ILogCloseLoanRespository, LogCloseLoanRespository>();
            services.AddTransient<ILogCloseLoanService, LogCloseLoanService>();
            #endregion

            #region PushLoanToPartner
            services.AddTransient<IPushLoanToPartnerRespository, PushLoanToPartnerRespository>();
            services.AddTransient<IPushLoanToPartnerService, PushLoanToPartnerService>();
            #endregion

            #region ConfigProductDetail
            services.AddTransient<IConfigProductDetailRepository, ConfigProductDetailRepository>();
            services.AddTransient<IConfigProductDetailService, ConfigProductDetailService>();
            #endregion

            #region LogCallApiCisco
            services.AddTransient<ILogCallApiCiscoRepository, LogCallApiCiscoRepository>();
            services.AddTransient<ILogCallApiCiscoService, LogCallApiCiscoService>();
            #endregion

            #region Shop
            services.AddTransient<IShopV2Service, ShopV2Service>();
            services.AddTransient<IShopRepository, ShopRepository>();
            #endregion



            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IUserServices, UserServices>();
            services.AddTransient<IGroupServices, GroupServices>();
            services.AddTransient<IModuleService, ModuleService>();
            services.AddTransient<ITelesalesServices, TelesalesServices>();
            services.AddTransient<IUserMMOService, UserMMOService>();
            services.AddTransient<IReportService, ReportService>();
            services.AddTransient<ILoanBriefService, LoanBriefService>();
            services.AddTransient<IApproveService, ApproveService>();
            services.AddTransient<IDictionaryService, DictionaryService>();
            services.AddTransient<ITlsScenarioService, TlsScenarioService>();
            services.AddTransient<ITlsQuestionService, TlsQuestionService>();
            services.AddTransient<IPipelineService, PipelineService>();
            //services.AddTransient<IPipelineService, RemarketingService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IReasonService, ReasonService>();
            services.AddTransient<ITokenSmartDailerService, TokenSmartDailerService>();
            services.AddTransient<ICareSoftService, CareSoftService>();
            services.AddTransient<ILogWorkingService, LogWorkingService>();
            services.AddTransient<IShopServices, ShopServices>();
            services.AddTransient<ITokenService, TokenService>();
            services.AddTransient<IAuthenticationAI, AuthenticationService>();
            services.AddTransient<ITrackDeviceService, TrackDeviceService>();
            services.AddTransient<IDetectFace, DetectFaceService>();
            services.AddTransient<IManagerAreaHubService, ManagerAreaHubService>();
            services.AddTransient<ILMSService, LMSService>();
            services.AddTransient<IPriceMotor, PriceMotorService>();
            services.AddTransient<IBODService, BODService>();
            services.AddTransient<ISSOService, SSOService>();
            services.AddTransient<ILogReqestAiService, LogReqestAiService>();
            services.AddTransient<INetworkService, NetworkService>();
            services.AddTransient<IEkycService, EkycService>();
            services.AddTransient<IResultEkycService, ResultEkycService>();
            services.AddTransient<IErp, ErpService>();
            services.AddTransient<ILogLoanInfoAi, LogLoanInfoAiService>();
            services.AddTransient<ICiscoService, CiscoService>();
            services.AddTransient<ILogCallApi, LogCallApiService>();
            services.AddTransient<ICheckBankService, CheckBankService>();
            services.AddTransient<ILoanStatusDetailService, LoanStatusDetailService>();
            services.AddTransient<ISendSmsBrandName, SendSmsBrandName>();
            services.AddTransient<ILoanBriefImportFileExcelServices, LoanBriefImportFileExcelServices>();
            services.AddTransient<IContactCustomerService, ContactCustomerService>();
            services.AddTransient<IDocumentType, DocumentTypeService>();
            services.AddTransient<ICheckElectricityAndWater, CheckElectricityAndWater>();
            services.AddTransient<ILocationService, LocationService>();
            services.AddTransient<IProxyTimaService, ProxyTimaService>();
            services.AddTransient<IRemarketingService, RemarketingService>();
            services.AddTransient<IToolService, ToolService>();
            services.AddTransient<ILosService, LosService>();
            services.AddTransient<ILogActionService, LogActionService>();
            //services.AddTransient<ITrandataService, TrandataService>();
            services.AddTransient<IDrawImageService, DrawImageService>();
            services.AddTransient<IMomoService, MomoService>();
            services.AddTransient<ISecuredTransaction, SecuredTransactionService>();

            //Thêm cache redis
            //services.AddScoped<ICacheService, CacheService>();
            //var provider = services.BuildServiceProvider();
            //var rep = provider.GetService<ICacheService>();
            //rep.InitCacheRedis();

            // signal R
            services.AddSignalR().AddStackExchangeRedis(Environment.GetEnvironmentVariable("REDIS"), options =>
            {
                options.Configuration.ChannelPrefix = "LOSApp";

            });
            //map appsettings
            //var trandataSettings =
            //    Configuration.GetSection("TrandataSettings");
            //services.Configure<TrandataSettings>(trandataSettings);

            // Hosted Service
            services.AddHostedService<ConsumerRabbitMQHostedService>();
            // services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddHttpContextAccessor();

            services.AddHttpClient();

            // setup web api url 
            Constants.WEB_API_URL = Configuration["Configuration:ApiUrl"].ToString();
            Constants.KEY_GOOGLE_MAP = Configuration["AppSettings:KEY_GOOGLE_MAP"].ToString();
            Constants.init();
            CacheData.InitData();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //app.UseRouting();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapHealthChecks("/health");
            //});

            //app.UseAntiXssMiddleware();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseStaticFiles();

            app.UseSignalR(routes =>
            {
                routes.MapHub<HubManagerHub>("/hubManagerHub");
            });

            app.UseRewriter(new RewriteOptions()
                .AddIISUrlRewrite(env.ContentRootFileProvider, "web.config")
                //.AddApacheModRewrite(env.ContentRootFileProvider, "web.config")
                );

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "uploads")),
                RequestPath = "/uploads"
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Login}/{action=Index}/{id?}");
            });

            app.UseMiddleware(typeof(ExceptionHandlingMiddleware));
        }

    }
}
