﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.WebApp.Views.LoanBrief.Detail
{
    public class Description
    {
        public static string GetDescription(Enum value)
        {
            try
            {
                var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return da.Length > 0 ? da[0].Description : value.ToString();
            }
            catch
            {
                return null;
            }
        }
        public enum EnumLivingTime
        {
            [Description("Dưới 1 năm")]
            Duoimotnam = 1,
            [Description("1 -3 năm")]
            Modenbanam = 2,
            [Description("3 - 10 năm")]
            Badenmuoinam = 3,
            [Description("Hơn 10 năm")]
            Honmuoinam = 4,

            [Description("Dưới 6 tháng")]
            Duoi6thang = 5,
            [Description("6 tháng")]
            Trong6thang = 6,
            [Description("Từ 7-12 tháng")]
            Tu7den12thang = 7,
            [Description("Từ 12-36 tháng")]
            Tu12den36thang = 8,
            [Description("Từ 36-72 tháng")]
            Tu36den72thang = 9,
            [Description("Trên 72 tháng")]
            Tren72thang = 10
        }
        public enum TypeLoanBrief
        {
            [Description("Cá nhân")]
            Personal = 1,
            [Description("Doanh nghiệp")]
            Enterprise = 2
        }

        public enum Gender
        {
            [Description("Nam")]
            Male = 0,
            [Description("Nữ")]
            Female = 1
        }

        public enum Merried
        {
            [Description("Chưa có")]
            Notyet = 0,
            [Description("Đã có")]
            Had = 1
        }
        public enum NumberBaby
        {
            [Description("Chưa có")]
            Notyet = 0,
            [Description("1 cháu")]
            One = 1,
            [Description("2 cháu")]
            Two = 2,
            [Description(">= 3cháu")]
            Three = 3
        }

        public enum LivingWith
        {
            [Description("Chung với bố mẹ")]
            Bome = 0,
            [Description("Riêng với vợ con")]
            Vocon = 1,
            [Description("Riêng một mình")]
            Motminh = 2
        }        
        public enum EnumTypeReceivingMoney
        {
            [Description("Tiền mặt")]
            MoneyCash = 1, // tiền mặt
            [Description("Chuyển khoản Số tài khoản")]
            NumberAccount = 3, // chuyển khoản Số tài khoản
            [Description("Số thẻ")]
            NumberCard = 4, // Số thẻ
        }

    }
}
