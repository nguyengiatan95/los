var PortletDraggable = function () {

    return {
        //main function to initiate the module
        // fixed
        init: function () {
            $(".pipeline-inner-container").sortable({
				connectWith: ".m-portlet__head_sortable",
                items: ".m-portlet", 
                opacity: 0.8,
				handle: '.m-portlet__head_sortable',
                coneHelperSize: true,
                placeholder: 'm-portlet--sortable-placeholder',
                forcePlaceholderSize: true,
                tolerance: "pointer",
                helper: "clone",
                tolerance: "pointer",
                forcePlaceholderSize: !0,
                helper: "clone",
                cancel: ".m-portlet--sortable-empty", // cancel dragging if portlet is in fullscreen mode
                revert: 250, // animation in milliseconds
                update: function(b, c) {
                    if (c.item.prev().hasClass("m-portlet--sortable-empty")) {
                        c.item.prev().before(c.item);
                    }                    
                }
            });
        }
    };
}();

jQuery(document).ready(function() {
    PortletDraggable.init();
});