﻿const LOANBRIEF_CANCEL = 99;
const HUB_CHT_LOAN_DISTRIBUTING = 35;
const APPRAISER_REVIEW = 40;
const BRIEF_APPRAISER_REVIEW = 55;
const LIST_WAIT_PIPELINE_RUN = [21, 11, 10, 1];
const LIST_ENUM_CAR = [6, 8, 11];
const LIST_PERMISSION_PUSH_TELE = [10, 20, 16];
const GROUP_MANAGER_TELES = 54;
const GROUP_TELES = 2;
const GROUP_MANAGER_HUB = 44;
const GROUP_HUB = 45;
const GROUP_BOD = 59;
const ENUM_SALEADMIN_LOAN_DISTRIBUTING = 10;
const ENUM_TELESALE_ADVICE = 20;
const ENUM_CANCELED = 99;
const ENUM_INIT = 1;
const WAIT_CUSTOMER_PREPARE_LOAN = 16;
const DK_OTO = 8;
const CAM_OTO = 11;
const PLATFORM_PRESENTER = 112;
const LOANTIME_TOPUP = 12;
const GROUP_KSNB = 55;
const GROUP_TP_KSNB = 56;
const GROUP_APPROVE_LEADER = 46;
const GROUP_APPROVE_EMP = 47;
const TatToanCuoiKy = 14;
const DuNoGiamDan = 13;
const HUB_CHT_APPROVE = 45;
const LOANBRIEF_FINISH = 110;
var LoanFastProductDetailId = ["11", "12", "15", "16", "40", "34"];
var Vina = ['091', '094', '081', '082', '083', '084', '085', '088'];
var Mobi = ['089', '090', '093', '070', '079', '077', '076', '078'];
var Viettel = ['086', '096', '097', '098', '032', '033', '034', '035', '036', '037', '038', '039'];

var App = new function () {

    var access_token = "";

    var SetToken = function (token) {
        this.access_token = token;
    };

    var ArrLoanStatus = {
        "1": { "title": "Đơn khởi tạo", "class": "m-badge--info" },
        "99": { "title": "Đơn đã hủy", "class": "m-badge--danger" },
        "10": { "title": "Chờ Saleadmin chia đơn", "class": "m-badge--info" },
        "15": { "title": "Chờ chia đơn tự động cho telesales", "class": "m-badge--info" },
        "16": { "title": "Chờ chuẩn bị hồ sơ", "class": "m-badge--warning" },
        "20": { "title": "Chờ Telesae tư vấn", "class": "m-badge--info" },
        "30": { "title": "Chờ chia đơn cho Hub", "class": "m-badge--info" },
        "32": { "title": "Chờ CVKD chuẩn bị hồ sơ", "class": "m-badge--info" },
        "35": { "title": "Chờ CHT chia đơn", "class": "m-badge--info" },
        "36": { "title": "Chờ thẩm định HO", "class": "m-badge--info" },
        "40": { "title": "Chờ thẩm định", "class": "m-badge--info" },
        "45": { "title": "Chờ CHT duyệt", "class": "m-badge--info" },
        "50": { "title": "Chờ chia đơn cho TĐHS", "class": "m-badge--info" },
        "55": { "title": "Chờ TĐHS xử lý", "class": "m-badge--info" },
        "56": { "title": "Chốt hợp đồng với khách hàng", "class": "m-badge--info" },
        "60": { "title": "TĐHS đề xuất duyệt", "class": "m-badge--info" },
        "61": { "title": "Leader phê duyệt đề xuất hủy", "class": "m-badge--info" },
        "65": { "title": "TĐHS đề xuất hủy", "class": "m-badge--warning" },
        "90": { "title": "Chờ chọn Lender", "class": "m-badge--info" },
        "70": { "title": "Chờ chia đơn cho BOD", "class": "m-badge--info" },
        "72": { "title": "Chờ BOD duyệt", "class": "m-badge--info" },
        "75": { "title": "Leader phê duyệt hủy", "class": "m-badge--info" },
        "77": { "title": "Chờ giám đốc kinh doanh duyệt", "class": "m-badge--info" },
        "80": { "title": "COO Phê duyệt hủy", "class": "m-badge--info" },
        "100": { "title": "Đã giải ngân", "class": "m-badge--success" },
        "110": { "title": "Đóng hợp đồng", "class": "m-badge--primary" },
        "101": { "title": "Chờ giải ngân", "class": "m-badge--info" }
    };

    var ArrPipelineState = {
        "0": { "title": "Đợi pipeline" },
        "1": { "title": "Đang xử lý pipeline" },
        "9": { "title": "Pipeline hoàn thành" },
        "10": { "title": "Chuyển đến Section tiếp theo" },
        "11": { "title": "Đang xử lý" },
        "20": { "title": "Đang đợi xử lý bằng tay" },
        "21": { "title": "Xử lý bằng tay thành công" },
        "98": { "title": "Pipeline không xác định" },
        "99": { "title": "Gặp lỗi " },
        "100": { "title": "Đã xử lý" }
    };

    var ArrTransactionState = {
        "0": { "title": "Đang chờ duyệt" },
        "1": { "title": "Đã duyệt" },
        "2": { "title": "Hủy" }
    };

    var ArrStatusOfDevice = {
        "0": { "title": "Chưa kích hoạt", "class": "m-badge--info" },
        "1": { "title": "Gắn thiết bị", "class": "m-badge--info" },
        "2": { "title": "Đã kích hoạt", "class": "m-badge--info" },
        "3": { "title": "Gỡ thiết bị(chưa lắp)", "class": "m-badge--info" },
        "-1": { "title": "Kích hoạt lỗi", "class": "m-badge--danger" }
    };

    var ArrTypeHotLead = {
        "1": { "title": "Hotlead chưa phản hồi", "class": "m-badge--danger" },
        "2": { "title": "Hotlead đã phản hồi", "class": "m-badge--info" }
    };

    var ArrDetailStatusTelesales = {
        "4": { "title": "Không nghe máy lần 1", "class": "detail-status-telesales" },
        "5": { "title": "Không nghe máy lần 2", "class": "detail-status-telesales" },
        "6": { "title": "Không nghe máy lần 3", "class": "detail-status-telesales" },
        "7": { "title": "Không nghe máy lần 4", "class": "detail-status-telesales" },
        "8": { "title": "Không nghe máy lần 5", "class": "detail-status-telesales" },
        "9": { "title": "Không nghe máy lần 6", "class": "detail-status-telesales" },
        "10": { "title": "Tắt máy/Thuê bao lần 1", "class": "detail-status-telesales" },
        "11": { "title": "Tắt máy/Thuê bao lần 2", "class": "detail-status-telesales" },
        "12": { "title": "Tắt máy/Thuê bao lần 3", "class": "detail-status-telesales" },
        "13": { "title": "Sai số/Số ĐT không có thực", "class": "detail-status-telesales" },
        "14": { "title": "Khác", "class": "detail-status-telesales" },
        "15": { "title": "Cân nhắc về bảo hiểm", "class": "detail-status-telesales" },
        "16": { "title": "Cân nhắc về phí/lãi suất", "class": "detail-status-telesales" },
        "17": { "title": "Cân nhắc về thời gian vay", "class": "detail-status-telesales" },
        "18": { "title": "Cân nhắc việc giữ ĐKX", "class": "detail-status-telesales" },
        "19": { "title": "Cân nhắc việc TĐ nhà/công ty", "class": "detail-status-telesales" },
        "20": { "title": "Đang bận, hẹn gọi lại sau", "class": "detail-status-telesales" },
        "21": { "title": "Đang follow,đã add zalo", "class": "detail-status-telesales" },
        "22": { "title": "Cân nhắc khác", "class": "detail-status-telesales" },
        "23": { "title": "Chờ CMT/CCCD/HC/ĐKX", "class": "detail-status-telesales" },
        "24": { "title": "Chờ giấy tờ chứng minh NSH", "class": "detail-status-telesales" },
        "25": { "title": "Chờ cung cấp ảnh chụp", "class": "detail-status-telesales" },
        "26": { "title": "Chờ chứng minh thu nhập", "class": "detail-status-telesales" },
        "27": { "title": "Chờ tham khảo người thân", "class": "detail-status-telesales" },
        "28": { "title": "Chờ thông tin tham chiếu", "class": "detail-status-telesales" },
        "29": { "title": "Chờ khách đi công tác/về quê", "class": "detail-status-telesales" },
        "30": { "title": "Chờ vì lý do khác", "class": "detail-status-telesales" },
        "31": { "title": "Gọi lại nhưng khách hàng không nghe máy", "class": "detail-status-telesales" },
        "32": { "title": "Gọi lại nhưng khách hàng không nghe máy", "class": "detail-status-telesales" },
        "34": { "title": "Chờ HS nhưng gọi lại không vay nữa", "class": "detail-status-telesales" },
        "35": { "title": "Cân nhắc nhưng gọi lại không vay nữa", "class": "detail-status-telesales" },
        "36": { "title": "Khách hàng cúp máy ngang", "class": "detail-status-telesales" },
        "37": { "title": "Lý do khác", "class": "detail-status-telesales" },
        "38": { "title": "Chờ ảnh CMT/CCCD/HC", "class": "detail-status-telesales" },
        "39": { "title": "Chờ ảnh ĐKX", "class": "detail-status-telesales" },
        "71": { "title": "KH chưa có nhu cầu ngay, TLS follow thêm" },
    };
    var ArrLoanStatusDetail = {
        "40": { "title": "Lead chưa liên hệ", "class": "m-badge--info" },
        "41": { "title": "CVKD đã liên hệ KH", "class": "m-badge--info" },
        "42": { "title": "Chưa chốt được lịch hẹn với KH", "class": "m-badge--info" },
        "43": { "title": "Chốt ký HS tại nhà/gần nhà", "class": "m-badge--info" },
        "44": { "title": "Chốt ký HS tại nơi làm việc/gần nơi làm việc", "class": "m-badge--warning" },
        "45": { "title": "Chốt ký HS tại điểm bất kỳ", "class": "m-badge--info" },
        "46": { "title": "Chốt ký HS tại cửa hàng", "class": "m-badge--info" },
        "47": { "title": "KH không nghe máy lần 1", "class": "m-badge--info" },
        "48": { "title": "KH không nghe máy lần 2", "class": "m-badge--info" },
        "49": { "title": "KH không nghe máy lần 3", "class": "m-badge--info" },
        "50": { "title": "Không nghe máy trên 3 lần", "class": "m-badge--info" },
        "51": { "title": "Thuê bao/sai số", "class": "m-badge--info" },
        "52": { "title": "KH cân nhắc thêm", "class": "m-badge--info" },
        "53": { "title": "Tình huống khác(comment rõ)", "class": "m-badge--info" },
        "54": { "title": "CVKD gặp được KH", "class": "m-badge--info" },
        "55": { "title": "Đã ký HĐ thành công và hoàn thiện HS", "class": "m-badge--info" },
        "56": { "title": "Đợi KH gửi Ảnh/video Nhà", "class": "m-badge--info" },
        "57": { "title": "Đợi KH gửi Ảnh/video nơi làm việc", "class": "m-badge--info" },
        "58": { "title": "Đợi KH cung cấp chứng từ(comment chi tiết)", "class": "m-badge--info" },
        "59": { "title": "CVKD đi nhưng không gặp được KH", "class": "m-badge--info" },
        "60": { "title": "KH hẹn gửi nhưng liên hệ lại không được", "class": "m-badge--info" },
        "61": { "title": "Tình huống khác(comment rõ)", "class": "m-badge--info" },
        "62": { "title": "CVKD đi thẩm định thực địa", "class": "m-badge--info" },
        "63": { "title": "Hoàn tất TĐ nhà và nơi làm việc", "class": "m-badge--info" },
        "64": { "title": "Hoàn tất TĐ nhà", "class": "m-badge--info" },
        "65": { "title": "Hoàn tất TĐ nơi làm việc", "class": "m-badge--info" },
        "66": { "title": "Đang thẩm định nhà/nơi làm việc", "class": "m-badge--info" },
        "67": { "title": "CVKD TĐTĐ xong, kết quả không thỏa mãn ĐKSP", "class": "m-badge--info" },
        "68": { "title": "CVKD thực địa xong, KH  phải bổ sung thêm chứng từ", "class": "m-badge--info" },
        "69": { "title": "Tình huống khác(comment rõ)" }
    };

    var ArrApproveStatusDetail = {
        "72": { "title": "Chưa xử lý", "class": "m-badge--success" },
        "73": { "title": "Đang xử lý hồ sơ", "class": "m-badge--warning" },
        "74": { "title": "Hồ sơ đẩy lên ngoài giờ làm việc", "class": "m-badge--info" },
        "75": { "title": "Bổ sung giấy tờ theo yêu cầu TĐHS", "class": "m-badge--warning" }
    };

    var ArrTypeProvince = {
        "1": { "title": "Thành phố" },
        "2": { "title": "Tỉnh" }
    };

    var ArrTypeDistrict = {
        "1": { "title": "Quận" },
        "2": { "title": "Huyện" },
        "5": { "title": "Thị xã" },
        "4": { "title": "Thành Phố" },
        "6": { "title": "Unknown" },
    }

    var ArrTypeWard = {
        "1": { "title": "Xã" },
        "2": { "title": "Phường" },
        "3": { "title": "Thị trấn" }
    };

    var ArrPushToCiscoStatus = {
        "0": { "title": "Thành công", "class": "m-badge--info" },
        "4": { "title": "Không thành công", "class": "m-badge--danger" },
        "5": { "title": "Tổng đài còn tồn chưa call hết", "class": "m-badge--warning" }       
    };

    var ctrlDown = false;
    var Init = function () {

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        var placeholderElement = $('#modal-placeholder');

        $(document).on('click', 'button[data-toggle="ajax-modal"]', function (event) {
            var url = $(this).data('url');
            placeholderElement.remove();
            $.get(url).done(function (data) {
                placeholderElement.html(data);
                placeholderElement.find('.modal').modal('show');
            });
        });

        $(document).on('click', 'a[data-toggle="ajax-modal"]', function (event) {
            var url = $(this).data('url');
            placeholderElement.remove();
            $.get(url).done(function (data) {
                placeholderElement.html(data);
                placeholderElement.find('.modal').modal('show');
            });
        });

        $('#filtercreateTime').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: moment().subtract(7, 'days'),
            endDate: moment(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY',
                firstDay: 1,
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7",
                ],
                "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ],
            },
        });
        $('#filtercreateTimeCisco').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',

            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY',
                firstDay: 1,
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7",
                ],
                "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ],
            },
        });
        $('.cus-datepicker').datepicker({
            todayBtn: "linked",
            clearBtn: false,
            todayHighlight: true,
            orientation: "bottom left",
            format: 'dd/mm/yyyy',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });

        //$.ajaxSetup({
        //    beforeSend: function (xhr) {
        //        xhr.setRequestHeader('RequestVerificationToken', $('input:hidden[name="__RequestVerificationToken"]').val());
        //    },
        //});
    }

    var Datetimepicker = function () {
        $('.ScheduleTime').datetimepicker({
            todayHighlight: true,
            autoclose: true,
            pickerPosition: 'bottom-left',
            format: 'dd/mm/yyyy hh:ii'
        });
    }
    var ShowInfo = function (message) {
        toastr.info(message);
    }

    var LibraryMoney = function () {
        $(".inputAmountVND").each(function () {
            formatNumberVND($(this));
        });
        $(".inputAmountVND").keydown(function (e) {
            handleKeyDown(e);
        }).keyup(function (e) {
            handleKeyUp(e);
            if (!ignoreEvent(e)) formatNumberVND($(this));
        });
        $('.numberOnly').keyup(function (e) {
            if (/\D/g.test(this.value)) {
                this.value = this.value.replace(/\D/g, '');
            }
        });
    }

    var ValidCustom = function () {
        jQuery.validator.addMethod('validPhone', function (value, element) {
            if (value == null || value == "")
                return true;
            var txtPhone = value.replace(/\s/g, "");
            var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
            return vnf_regex.test(txtPhone);

        }, "Your Phone Invalid");
        jQuery.validator.addMethod("validDate", function (value, element) {
            if (value == null || value == "")
                return true;
            var year = value.split('/');
            if (value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/) && Number(year[0]) <= 31 && Number(year[1]) <= 12)
                return true;
            else
                return false;
        }, "Please enter a valid date in the format DD/MM/YYYY");
        jQuery.validator.addMethod("validCardnumber", function (value, element) {
            if (value == null || value == "")
                return true;
            if (!isNaN(value)) {
                if (value.length == NationalCardLength.CMND_QD || value.length == NationalCardLength.CMND || value.length == NationalCardLength.CCCD) {//8: cmnd quân đội
                    return true;
                }
            }
            return false;
        }, "Please enter a valid cardnumber");
        jQuery.validator.addMethod('validEmail', function (value, element) {
            if (value == null || value == "")
                return true;
            var txtEmail = value.replace(/\s/g, "");
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(txtEmail);

        }, "Your Email Invalid");
        jQuery.validator.addMethod("validLoanAmount", function (value, element) {
            if (value == null || value == "" || value == "0")
                return false;
            var loanAmount = value.replace(/,/g, '');
            if (Number(loanAmount) > 0)
                return true;
            else
                return false;
        }, "Please enter a valid date in the format DD/MM/YYYY");
    }

    var ShowSuccess = function (message) {
        setTimeout(function () {
            location.reload()
        }, 1000);
        toastr.success(message);
    }
    var ShowSuccessNotLoad = function (message) {
        toastr.success(message);
    }
    var ShowSuccessRedirect = function (message, url) {
        setTimeout(function () {
            window.location.href = url;
        }, 1000);
        toastr.success(message);
    }

    var ShowWarning = function (message) {
        toastr.warning(message);
    }

    var ShowError = function (message) {
        setTimeout(function () {
            location.reload()
        }, 2000);
        toastr.error(message);
    }

    var ShowErrorNotLoad = function (message) {
        toastr.error(message);
    }

    var ReloadData = function () {
        var pathname = window.location.pathname;
        if (pathname == "/loanbrief/manager-hub.html") {
            LoanBriefHubModuleJS.LoadData();
        } else if (pathname == "/loanbrief/index.html") {
            LoanBriefModule.LoadData();
            LoanBriefModule.LoadFilterTop();
        } else if (pathname == "/loanbrief/staff-hub.html") {
            LoanBriefStaffHubModuleJS.LoadData();
        } else if (pathname == "/approve/disbursementafter.html") {
            DisbursementAfterModule.LoadData();
        } else if (pathname == "/loancancel/index.html") {
            LoanCancelModule.LoadData();
        } else if (pathname == "/rmkt/index.html") {
            RemarketingTlsModule.LoadData();
        } else {
            window.location.reload();
        }
    }

    var Ajax = function (method, endpoint, data) {
        return $.ajax({
            type: method,
            url: endpoint,
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: {
                "Authorization": "bearer " + this.access_token
            },
        });
    }

    var AjaxDone = function (method, endpoint, data, done) {
        return $.ajax({
            type: method,
            url: endpoint,
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(done).fail(function () { App.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!"); });
    }

    var AjaxSuccess = function (method, endpoint, data, success, error) {
        return $.ajax({
            type: method,
            url: endpoint,
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            success: success,
            error: error
        });
    }

    var FormatCurrency = function FormatCurrency(Num) { //function to add commas to textboxes
        Num += '';
        Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
        Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
        x = Num.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1))
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        return x1 + x2;
    }

    var HandleDefaultResponse = function (data, successMessage, errorMessage, callbackWhenSuccess, callbackWhenError) {
        if (data && data.meta && data.meta.errorCode == 200) {
            App.ShowSuccessNotLoad((successMessage == 'undifined' || !successMessage || successMessage == '') ? data.meta.errorMessage : successMessage);
            if (callbackWhenSuccess && callbackWhenSuccess != 'undifined') {
                callbackWhenSuccess();
            }
        }
        else if (data && data.meta && data.meta.errorCode != 200) {
            App.ShowError(data.meta.errorMessage);
            if (callbackWhenError && callbackWhenError != 'undifined') {
                callbackWhenError();
            }
        }
        else {
            App.ShowError((errorMessage == 'undifined' || !errorMessage || errorMessage == '') ? data.meta.errorMessage : errorMessage);
            if (callbackWhenError && callbackWhenError != 'undifined') {
                callbackWhenError();
            }
        }
    }
    // Format input number
    function formatNumberVND(e) {
        e.parseNumber({ format: "#,##0", locale: "us" });
        e.formatNumber({ format: "#,##0", locale: "us" });
    }

    var ctrlDown = false;
    function handleKeyDown(e) {
        if (e.which == 17) ctrlDown = true;
    }
    function handleKeyUp(e) {
        if (e.which == 17) ctrlDown = false;
    }
    function ignoreEvent(e) {
        if (e.which >= 16 && e.which <= 18) return true;
        if (e.which >= 33 && e.which <= 40) return true;
        if (ctrlDown && (e.which == 65 || e.which == 67)) return true;
        return false;
    }

    function DecodeEntities(encodedString) {
        var translate_re = /&(nbsp|amp|quot|lt|gt);/g;
        var translate = {
            "nbsp": " ",
            "amp": "&",
            "quot": "\"",
            "lt": "<",
            "gt": ">"
        };
        return encodedString.replace(translate_re, function (match, entity) {
            return translate[entity];
        }).replace(/&#(\d+);/gi, function (match, numStr) {
            var num = parseInt(numStr, 10);
            return String.fromCharCode(num);
        });
    }
    return {
        access_token: access_token,
        ShowInfo: ShowInfo,
        ShowSuccess: ShowSuccess,
        ShowSuccessRedirect: ShowSuccessRedirect,
        ShowSuccessNotLoad: ShowSuccessNotLoad,
        ShowWarning: ShowWarning,
        ShowError: ShowError,
        ShowErrorNotLoad: ShowErrorNotLoad,
        Ajax: Ajax,
        AjaxDone: AjaxDone,
        AjaxSuccess: AjaxSuccess,
        SetToken: SetToken,
        FormatCurrency: FormatCurrency,
        HandleDefaultResponse: HandleDefaultResponse,
        ValidCustom: ValidCustom,
        LibraryMoney: LibraryMoney,
        ArrLoanStatus: ArrLoanStatus,
        ArrPipelineState: ArrPipelineState,
        ReloadData: ReloadData,
        ArrStatusOfDevice: ArrStatusOfDevice,
        Datetimepicker: Datetimepicker,
        ArrTypeHotLead: ArrTypeHotLead,
        DecodeEntities: DecodeEntities,
        ArrDetailStatusTelesales: ArrDetailStatusTelesales,
        ArrLoanStatusDetail: ArrLoanStatusDetail,
        ArrApproveStatusDetail: ArrApproveStatusDetail,
        ArrTypeProvince: ArrTypeProvince,
        ArrTypeDistrict: ArrTypeDistrict,
        ArrTypeWard: ArrTypeWard,
        ArrTransactionState: ArrTransactionState,
        ArrPushToCiscoStatus: ArrPushToCiscoStatus,
        Init: function () {
            Init();
        }
    }
}

$(document).ready(function () {
    App.Init();
});