﻿var ApproveEmpModule = new function () {
    var _groupGlobal;
    var _typeServiceCall;

    var InitData = function (group, typeServiceCall) {
        _groupGlobal = group;
        _typeServiceCall = typeServiceCall;
    }
    var Init = function () {

        $("#filterProvince").select2({
            placeholder: "Tỉnh / Thành"
        });

        $("#filterStatus").select2({
            placeholder: "Trạng thái"
        });

        $("#filterProduct").select2({
            placeholder: "Sản Phẩm"
        });

        $("#filterApprove").select2({
            placeholder: "Chọn Thẩm định hồ sơ"
        });

        $('#filtercreateTime').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY',
                firstDay: 1,
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7",
                ],
                "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ],
            },
        });

        LoadData();
        //GetStatus();
        GetProduct();

        $("#btnSearch").click(function () {
            LoadData();
            return;
        });

        //Mã HĐ //Phone + CMND + Name
        $("#filterLoanId, #filterSearch").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return;
            }
        });

        //Status
        $('#filterStatus, #filterProvince, #filtercreateTime, #filterProduct').on('change', function () {
            LoadData();
            return;
        });

    };

    var LoadData = function () {
        var objQuery = {
            loanBriefId: $("#filterLoanId").val().replace('HĐ-', '').trim(),
            search: $('#filterSearch').val().trim().toLowerCase(),
            provinceId: $('#filterProvince').val(),
            filtercreateTime: $('#filtercreateTime').val(),
            status: $('#filterStatus').val(),
            productId: $('#filterProduct').val()
        }

        InitDataTable(objQuery);
    }

    var InitDataTable = function (objQuery) {
        var datatable = $('#dtApprovedEmp').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Approve/SearchEmp',
                        params: {
                            query: objQuery
                        }
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    title: '',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" value="' + row.loanBriefId + '" name="checkbox"><span></span></label></span>';
                        return html;
                    }
                },
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var html = '';
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        if (row.utmSource != null && row.utmSource != "") {
                            return index + record + "<br /><p style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);'>" + row.utmSource + "</p>";
                        } else {
                            return index + record;
                        }
                    }
                },
                {
                    field: 'call',
                    title: 'Gọi',
                    width: 80,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        //gọi qua caresoft
                        var html = '';
                        if (_typeServiceCall == 1) {
                            html = '<a href="javascript:;" onclick="CareSoftModule.CallCareSoft(' + row.loanBriefId + ')" class="box-call">\
                                    <i class="fa fa-phone item-icon-phone" ></i></a>';
                        } else if (_typeServiceCall == 2) {// gọi qua metech
                            html = `<a href="javascript:;" onclick="finesseC2CModule.ClickToCall('${row.phone}',${row.loanBriefId})" class="box-call">
                                    <i class="fa fa-phone item-icon-phone" ></i></a>`;
                        }
                        if (row.homeNetwork > 0) {
                            if (row.homeNetwork == 3)
                                html += '<span class="item-desciption">Vina</span>'
                            else if (row.homeNetwork == 2)
                                html += '<span class="item-desciption">Mobi</span>'
                            else if (row.homeNetwork == 1)
                                html += '<span class="item-desciption">Viettel</span>'
                            else
                                html += '<span class="item-desciption">Không xác định</span>'
                        } else {
                            var dauso = row.phone.substring(0, 3);
                            if (Vina.includes(dauso))
                                html += '<span class="item-desciption">Vina</span>'
                            else if (Mobi.includes(dauso))
                                html += '<span class="item-desciption">Mobi</span>'
                            else if (Viettel.includes(dauso))
                                html += '<span class="item-desciption">Viettel</span>'
                            else
                                html += '<span class="item-desciption">Không xác định</span>'
                        }



                        return html;
                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    textAlign: 'center',
                    width: 100,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += 'HĐ-' + row.loanBriefId;
                        if (row.hub != null) {
                            html += `<span class="item-desciption">(${row.hub.name})</span>`;
                        }
                        if (row.platformType == PlatformType.LendingOnline || row.platformType == PlatformType.LendingOnlineAPP) {
                            html += ' <br /><span class="item-desciption">(LendingOnline)</span>';
                        }
                        if (row.isReborrow) {
                            html += `<span class="item-desciption">(Tái vay)</span>`;
                        }
                        //đơn topup
                        if (row.typeRemarketing == 2) {
                            html += '<br /><span class="item-desciption">(Topup)</span>';
                        }
                        //Check thời xử lý đơn vay
                        //Đơn có thời gian hub đẩy và chưa có tác động từ TDHS
                        if (row.approverTimeProcessing > 0) {
                            //có phản hồi nhưng trên 15 phút
                            html += '<br /><span style="color: #ff0000; font-size: 12px;" id="_timeProcessingA_' + row.loanBriefId + '"></span>';
                            var minutes = parseInt(30 - row.approverTimeProcessing);
                            var _spanId = "#_timeProcessingA_" + row.loanBriefId;
                            CommonModalJS.CountDown(minutes * 60, _spanId);
                            //}
                        }
                        if (row.productId == ProductCredit.OtoCreditType_CC && row.reMarketingLoanBriefId == row.loanBriefId) {
                            html += '<br /><span style="color: #e88f0b; font-size: 12px;">Có đơn con</span>';
                        }
                        if (row.productId == ProductCredit.OtoCreditType_CC && row.platformType == PlatformType.LoanCarChild) {
                            html += '<br /><span style="color: #e88f0b; font-size: 12px;">Là đơn con của HĐ-' + row.reMarketingLoanBriefId + '</span>';
                        }

                        if (row.typeRemarketing == TypeRemarketing.DebtRevolvingLoan) {
                            html += `<span class="item-desciption">(Tái cấu trúc nợ)</span>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    sortable: false,
                    template: function (row) {
                        var html = '<a id="linkName" style="color:#6C7293;" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>';
                        if (row.buyInsurenceCustomer == true)
                            html += '<span class="item-desciption">Có mua bảo hiểm</span>'
                        else
                            html += '<span class="item-desciption" style="color:red !important;">Không mua bảo hiểm</span>'

                        if (row.creditScoring > 0) {
                            if (row.creditScoring > 957 ) {
                                html += `<span class="item-desciption">${row.creditScoring} điểm</span>`
                            }
                            else
                                html += `<span class="item-desciption" style="color:#e88f0b !important;">${row.creditScoring} điểm</span>`
                        }
                        if (row.isExceptions == 1)
                            html += `<span class="item-desciption">Đơn trình ngoại lệ</span>`;
                        if (row.postRegistration == true)
                            html += `<span style="color:brown !important;" class="item-desciption">Thu đăng ký xe sau</span>`;
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Quận/ Huyện',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.district != null)
                            text += "<p class='district'>" + row.district.name + "</p>";
                        if (row.province != null) {
                            text += "<p class='province'>" + row.province.name + "</p>";
                        }
                        if (row.currentPipelineId != null && row.currentPipelineId == Pipeline['Pipeline_Test'])
                            text += '<span style="color:red !important;"  class="item-desciption">(Luồng thử nghiệm)</span>'
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    textAlign: 'center',
                    width: 100,
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        var html = date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                        if (row.transactionState != null)
                            html += `<span class="item-desciption">GDĐB - ${App.ArrTransactionState[row.transactionState].title}</span>`;

                        return html;
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = '';

                        var money = App.FormatCurrency(row.loanAmount);
                        if (row.loanProduct != null) {
                            html += money + "<br />" + "<p class='district'>" + row.loanProduct.name + "</p>";
                        } else {
                            html += money;
                        }
                        if (row.productId == ProductCredit.MotorCreditType_KCC || row.productId == ProductCredit.MotorCreditType_KGT) {
                            if (row.buyInsuranceProperty == true) {
                                html += "<br />" + "<p class='item-desciption'>Có mua bảo hiểm tài sản</p>";
                            }
                            else {
                                html += "<br />" + "<p class='item-desciption' style='color: red!important;'>Không mua bảo hiểm tài sản</p>";
                            }
                        }
                        return html;
                    }
                },
                {
                    field: 'hubPushAt',
                    title: 'Thời gian Hub đẩy',
                    textAlign: 'center',
                    width: 100,
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (row.hubPushAt != null) {
                            var date = moment(row.hubPushAt);
                            html += date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                        }
                        return html;
                    }
                },
                {
                    field: 'loanTime',
                    title: 'Thời gian vay',
                    //width: 200,
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = '';
                        if (row.loanTime != null)
                            html += row.loanTime + " Tháng";
                        //if (row.isLocate) {
                        //    html += `<span class="m-badge m-badge--success m-badge--wide font-size-11">Xe lắp định vị</span>`;
                        //    if (App.ArrStatusOfDevice[row.deviceStatus] != null) {
                        //        html += '<span class="item-desciption">(' + App.ArrStatusOfDevice[row.deviceStatus].title + ')</span>';
                        //    }
                        //}
                        if (row.isTrackingLocation) {
                            html += `<span class="item-desciption">(KH chia sẻ vị trí)</span>`;
                        }

                        if (row.rateTypeId == DuNoGiamDan)
                            html += `<span class="item-desciption">Dư nợ giảm dần</span>`;
                        else if (row.rateTypeId == TatToanCuoiKy)
                            html += `<span class="item-desciption">Tất toán cuối kỳ</span>`;
                        else
                            html += `<span class="item-desciption">Không xác định</span>`;
                        return html;
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        if (App.ArrLoanStatus[row.status] != null) {
                            var html = '<span class="m-badge ' + App.ArrLoanStatus[row.status].class + ' m-badge--wide">' + App.ArrLoanStatus[row.status].title + '</span>';
                            if (row.status == BRIEF_APPRAISER_REVIEW) {
                                if (row.approverStatusDetail != null)
                                    html += '<span style="color: red;font-style: italic;" class="m-list-timeline__text">' + App.ArrApproveStatusDetail[row.approverStatusDetail].title + '</span>';
                                else {

                                    html += '<span style="color: red;font-style: italic;" class="m-list-timeline__text">' + App.ArrApproveStatusDetail[72].title + '</span>';
                                }

                            }

                            if (App.ArrPipelineState[row.pipelineState] != null && LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1) {
                                html += `<span class="item-desciption-11">${App.ArrPipelineState[row.pipelineState].title}</span>`;
                            }
                            return html;
                        } else {
                            return 'Không xác định';
                        }
                    }
                },

                {
                    field: 'tdhs',
                    title: 'Thẩm định hồ sơ',
                    sortable: false,
                    template: function (row) {
                        if (row.userHub != null) {
                            return row.userHub.fullName + "</br> SDT: " + row.userHub.phone;
                        } else {
                            return "";
                        }

                    }
                },

                {
                    field: 'action',
                    title: 'Hành động',
                    width: 160,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1)
                            return html;
                        if (row.isEdit) {
                            html += `<a href="javascript:;" title="Cập nhật đơn vay" onclick="InitLoanBrief.Create(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="la la-edit"></i></a>`;
                        }
                        if (row.isPush) {
                            html += `<a href="javascript:;" title="Đẩy đơn vay" onclick="CommonModalJS.ConfirmLoanBrief(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-check"></i></a>`;
                        }
                        html += `<a data-toggle="modal" title="Chuyển trạng thái" onclick="CommonModalJS.InitChangeStatusApprover(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-share-square-o"></i></a>`;
                        html += `<a href="javascript:;" title="Nhật ký khoản vay" onclick="CommonModalJS.InitDiaryModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							             <i class="fa fa-wpforms"></i></a>`;
                        if (row.isUpload) {
                            html += `<a href="javascript:;" title="Upload chứng từ" onclick="CommonModalJS.InitUploadModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-upload"></i></a>`;
                        }

                        //                 html += '<a data-toggle="modal" href="#Modal" title="Check CIC" onclick="CICModule.CICCardNumber(' + row.loanBriefId + ',\'' + row.fullName + '\')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
                        //<i class="fa fa-check-square-o"></i></a>';

                        if (row.isConfirmAndPush) {
                            html += '<a data-toggle="modal" href="#Modal" title="Chốt hợp đồng" onclick="ConfirmLoanMoneyModule.Confirm(' + row.loanBriefId + ')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
							<i class="fa fa-external-link"></i></a>';
                            if (row.isTransactionsSecured) {
                                html += `<a data-toggle="modal" title="Tạo giao dịch đảm bảo" onclick="CommonModalJS.TransactionsSecured(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-suitcase"></i></a>`;
                            }
                        }


                        //                 if (row.loanAmount != null)
                        //                     html += '<a data-toggle="modal" href="#Modal" title="Tính lãi & Bảo hiểm" onclick="CommonModalJS.InterestModal(' + row.loanBriefId + ')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
                        //<i class="fa fa-money"></i></a>';

                        if (row.buyInsurenceCustomer != true) {
                            html += `<a href="javascript:;" title="Xác nhận mua bảo hiểm" onclick="CommonModalJS.ConfirmInsurence(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-shield"></i></a>`;
                        }

                        if (row.isBack) {
                            html += '<a href="javascript:;" title="Trả lại TĐTĐ" onclick="ReturnLoanBriefModule.ReturnLoanBrief(' + 2 + ',' + row.loanBriefId + ',\'' + row.fullName + '\')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
							<i class="fa fa-undo"></i></a>';
                        }


                        if (row.comfirmCancel) {
                            html += `<a href="javascript:;" title="Hủy đơn vay" onclick="CommonModalJS.ConfirmCancelLoanBrief(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-trash"></i></a>`;
                        } else {
                            if (row.isCancel) {
                                html += `<a href="javascript:;" title="Hủy đơn vay" onclick="CommonModalJS.InitCancelModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-trash"></i></a>`;
                            }
                        }

                        if (_groupGlobal == GROUP_KSNB || _groupGlobal == GROUP_TP_KSNB) {
                            html += `<a data-toggle="modal" title="File ghi âm" onclick="CommonModalJS.ViewRecordModel('#modalRecording',${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-headphones"></i></a>`;
                        }

                        html += `<a data-toggle="modal" title="File ghi âm" onclick="CommonModalJS.ViewRecordModel('#modalRecording',${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-headphones"></i></a>`;

                        html += `<a data-toggle="modal" title="Compare hồ sơ" onclick="CommonModalJS.CompareModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-retweet"></i></a>`;

                        return html;
                    }
                }
            ]
        });
    }
    var GetUserByGroupId = function (id) {
        App.Ajax("GET", "/Dictionary/GetUserByGroupId?groupId=" + id, undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                console.log(data.data);
                //var html = "<option value=\"0\" disabled selected>Chọn Thẩm định hồ sơ</option>";
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option id='" + data.data[i].userId + "' value='" + data.data[i].userId + "'>" + data.data[i].username + " (" + data.data[i].countLoan + ")" + "</option>";
                }
                $("#filterApprove").append(html);
                $("#filterApprove").select2({
                    placeholder: "Chọn Thẩm định hồ sơ"
                });
            }
        });
    }
    var GetProduct = function () {
        App.Ajax("GET", "/Dictionary/GetLoanProduct", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "<option value=\"-1\">Tất cả</option>";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].loanProductId + "'>" + data.data[i].name + "</option>";
                }
                $("#filterProduct").append(html);
                $("#filterProduct").select2({
                    placeholder: "Sản phẩm"
                });
            }
        });
    }
    var GetStatus = function () {
        App.Ajax("GET", "/Dictionary/GetStatus", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].status + "'>" + data.data[i].description + "</option>";
                }
                $("#filterStatus").append(html);
                $("#filterStatus").select2({
                    placeholder: "Trạng thái"
                });
            }
        });
    }
    var ChangeSupport = function () {
        var approveid = $("#filterApprove").val();
        var aporoveName = $("#filterApprove option:selected").text();
        var favorite = [];
        $.each($("input[name='checkbox']:checked"), function () {
            favorite.push($(this).val());
        });
        if (approveid == '') {
            App.ShowErrorNotLoad('Bạn phải chọn support');
            return;
        } if (favorite == '') {
            App.ShowErrorNotLoad('Bạn phải chọn đơn vay muốn chuyển');
            return;
        }

        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn chuyển giỏ của các đơn đã chọn",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Approve/ChangeSupport",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                        });
                    },
                    data: { ArrId: favorite, ApproveId: approveid, ApproveName: aporoveName },
                    success: function (data) {
                        $.unblockUI();
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            ApproveEmpModule.LoadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                            return;
                        }
                    }
                });
            }
        });
    };

    return {
        Init: Init,
        ChangeSupport: ChangeSupport,
        GetUserByGroupId: GetUserByGroupId,
        InitData: InitData,
        LoadData: LoadData
    };
}

