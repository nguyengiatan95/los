﻿var ApproveModule = new function () {
    var Init = function () {
        $("#filterHub").select2({
            placeholder: "Cửa hàng"
        });

        $("#filterStatus").select2({
            placeholder: "Trạng thái"
        });

        $("#filterApprove").select2({
            placeholder: "Chọn Thẩm định hồ sơ"
        });

        LoadData();

        GetShop();

        $("#btnSearch").click(function () {
            LoadData();
            return;
        });

        //Mã HĐ  //Phone + CMND + Name
        $("#filterLoanId, #filterSearch").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });
        //Status
        $('#filterStatus, #filterHub').on('change', function () {
            LoadData();
            return;
        });
    };

    var LoadData = function () {
        var objQuery = {
            loanBriefId: $("#filterLoanId").val().replace('HĐ-', '').trim(),
            search: $('#filterSearch').val().trim().toLowerCase(),
            hubId: $('#filterHub').val(),
            status: $('#filterStatus').val(),
        }
        InitDataTable(objQuery);
    }

    var InitDataTable = function (objQuery) {
        var datatable = $('#dtApproved').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Approve/Search',
                        params: {
                            query: objQuery
                        }
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    title: '',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" value="' + row.loanBriefId + '" name="checkbox"><span></span></label></span>';
                        return html;
                    }
                },
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        if (row.utmSource != null && row.utmSource != "") {
                            return index + record + "<br /><p style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);'>" + row.utmSource + "</p>";
                        } else {
                            return index + record;
                        }
                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += 'HĐ-' + row.loanBriefId;
                        if (row.isReborrow) {
                            html += `<span class="item-desciption">(Tái vay)</span>`;
                        }
                        if (row.platformType == PlatformType.LendingOnline || row.platformType == PlatformType.LendingOnlineAPP) {
                            html += ' <br /><span class="item-desciption">(LendingOnline)</span>';
                        }
                        return html;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    textAlign: 'center',
                    //width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = '<a id="linkName" style="color:#6C7293;" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>';
                        if (row.creditScoring > 0) {
                            if (row.creditScoring > 957) {
                                html += `<span class="item-desciption">${row.creditScoring} điểm</span>`
                            }
                            else
                                html += `<span class="item-desciption" style="color:#e88f0b !important;">${row.creditScoring} điểm</span>`
                        }
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Quận/ Huyện',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.district != null)
                            text += "<p class='district'>" + row.district.name + "</p>";
                        if (row.province != null) {
                            text += "<p class='province'>" + row.province.name + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                            var money = App.FormatCurrency(row.loanAmount);
                            if (row.loanProduct != null) {
                                html += money + "<br />" + "<p class='district'>" + row.loanProduct.name + "</p>";
                            } else {
                                html += money;
                            }
                        
                        return html;
                    }
                },
                {
                    field: 'hubPushAt',
                    title: 'Thời gian Hub đẩy',
                    textAlign: 'center',
                    width: 150,
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.hubPushAt);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanTime',
                    title: 'Thời gian vay',
                    //width: 200,
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = '';
                        if (row.loanTime != null)
                            html += row.loanTime + " Tháng";
                        if (row.isLocate) {
                            html += `<span class="m-badge m-badge--success m-badge--wide font-size-11">Xe lắp định vị</span>`;
                            if (App.ArrStatusOfDevice[row.deviceStatus] != null) {
                                html += '<span class="item-desciption">(' + App.ArrStatusOfDevice[row.deviceStatus].title + ')</span>';
                            }
                        }
                        if (row.isTrackingLocation) {
                            html += `<span class="item-desciption">(KH chia sẻ vị trí)</span>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        if (App.ArrLoanStatus[row.status] != null) {
                            var html = '<span class="m-badge ' + App.ArrLoanStatus[row.status].class + ' m-badge--wide">' + App.ArrLoanStatus[row.status].title + '</span>';
                            if (App.ArrPipelineState[row.pipelineState] != null && LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1) {
                                html += `<span class="item-desciption-11">${App.ArrPipelineState[row.pipelineState].title}</span>`;
                            }
                            return html;
                        } else {
                            return 'Không xác định';
                        }
                    }
                },
                {
                    field: 'tdhs',
                    title: 'Thẩm định hồ sơ',
                    sortable: false,
                    template: function (row) {
                        if (row.userHub != null) {
                            return row.userHub.fullName + "</br> SDT: " + row.userHub.phone;
                        } else {
                            return "";
                        }
                    }
                },
                {
                    field: 'action',
                    title: 'Hành động',
                    width: 160,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1)
                            return html;
                        if (row.isEdit) {
                            html += `<a href="javascript:;" title="Cập nhật đơn vay" onclick="InitLoanBrief.Create(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="la la-edit"></i></a>`;
                        }
                        if (row.isPush) {
                            html += `<a href="javascript:;" title="Đẩy đơn vay" onclick="CommonModalJS.ConfirmLoanBrief(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-check"></i></a>`;
                        }
                        html += `<a href="javascript:;" title="Nhật ký khoản vay" onclick="CommonModalJS.InitDiaryModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							             <i class="fa fa-wpforms"></i></a>`;
                        if (row.isUpload) {
                            html += `<a href="javascript:;" title="Upload chứng từ" onclick="CommonModalJS.InitUploadModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-upload"></i></a>`;
                        }
                        if (row.isConfirmCancel) {
                            html += `<a href="javascript:;" title="Hủy đơn vay" onclick="CommonModalJS.ConfirmCancelLoanBrief(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-trash"></i></a>`;
                        } else {
                            if (row.isCancel) {
                                html += `<a href="javascript:;" title="Hủy đơn vay" onclick="CommonModalJS.InitCancelModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-trash"></i></a>`;
                            }
                        }
                        if (row.isBack) {
                            html += '<a href="javascript:;" title="Trả lại đơn vay" onclick="ReturnLoanBriefModule.ReturnLoanBrief(' + 1 + ',' + row.loanBriefId + ',\'' + row.fullName + '\')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
							<i class="fa fa-undo"></i></a>';
                        }

                            html += `<a data-toggle="modal" title="File ghi âm" onclick="CommonModalJS.ViewRecordModel('#modalRecording',${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-headphones"></i></a>`;

                        return html;
                    }
                }
            ]
        });
    }

    var GetShop = function () {
        App.Ajax("GET", "/Dictionary/GetShop", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].shopId + "'>" + data.data[i].name + "</option>";
                }
                $("#filterHub").append(html);
                $("#filterHub").select2({
                    placeholder: "Cửa hàng"
                });
            }
        });
    }

    var GetUserByGroupId = function (id) {
        App.Ajax("GET", "/Dictionary/GetUserByGroupId?groupId=" + id, undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option id='" + data.data[i].userId + "' value='" + data.data[i].userId + "'>" + data.data[i].username + " (" + data.data[i].countLoan + ")" + "</option>";
                }
                $("#filterApprove").append(html);
                $("#filterApprove").select2({
                    placeholder: "Chọn Thẩm định hồ sơ"
                });
            }
        });
    }

    var ChangeSupport = function () {
        var approveid = $("#filterApprove").val();
        var favorite = [];
        $.each($("input[name='checkbox']:checked"), function () {
            favorite.push($(this).val());
        });
        if (approveid == '') {
            App.ShowErrorNotLoad('Bạn phải chọn support');
            return;
        } if (favorite == '') {
            App.ShowErrorNotLoad('Bạn phải chọn đơn vay muốn chuyển');
            return;
        }
        $.ajax({
            type: "POST",
            url: "/Approve/ChangeSupport",
            data: { ArrId: favorite, ApproveId: approveid },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                } else {
                    App.ShowErrorNotLoad(data.message);
                    return;
                }
            }
        });

    };

    return {
        Init: Init,
        ChangeSupport: ChangeSupport,
        GetUserByGroupId: GetUserByGroupId
    };
}

