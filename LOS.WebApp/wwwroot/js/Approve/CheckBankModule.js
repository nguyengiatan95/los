﻿var CheckBankModule = new function () {
    var Check = function (loanBriefId) {
        debugger;
        $("#Loading").attr("style", "display:block")
        var bankAccountNumber = $("#BankAccountNumber").val().trim();
        var bankCode = $('#sl_BankReceiveMoney option:selected').attr('code');
        if (bankAccountNumber == '' || bankCode == '') {
            App.ShowErrorNotLoad('Bạn phải nhập đủ thông tin ngân hàng!');
            return;
        }
        $.ajax({      
            type: "POST",
            url: "/LoanBrief/CheckBank",
            data: { BankAccountNumber: bankAccountNumber,BankCode: bankCode,LoanBriefId: loanBriefId },
            success: function (res) {
                $("#Loading").attr("style", "display:none")
                if (res.status == 1) {
                    $('#BankMess').text(res.accountName);
                } else {
                    $('#BankMess').text(res.message);
                }
            }
        });
    }

    return {
        Check: Check
    };
};