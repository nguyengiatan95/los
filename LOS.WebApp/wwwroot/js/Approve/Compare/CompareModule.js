﻿var CompareModule = new function () {
    var _isAddLog = 0;
    var Init = function (isAddLog) {
        _isAddLog = isAddLog;
    }
    var stepCurren = 1;
    var lstFileCMND = new Array();
    var lstFileDKX = new Array();
    var lstFileBCT = new Array();
    var arrTabs = new Array();


    var LoadDisplayStep = function (loanBriefId, tab) {
        $.ajax({
            url: "/Approve/LoadDisplayStep",
            type: "GET",
            data: { loanBriefId: loanBriefId, tab: tab },
            success: function (data) {
                if (data.isSuccess) {
                    $('#step_load_html').html('');
                    $('#step_load_html').html(data.html);
                }
            }
        });

    };
    var LoadFileRoot = function (loanBriefId, documentId) {
        $.ajax({
            type: "GET",
            url: "/Approve/LoadFileRoot",
            data: { id: loanBriefId, documentId: documentId },
            success: function (data) {
                for (var i = 0; i < data.data.length; i++) {
                    if (documentId == 311)
                        lstFileCMND.push(data.data[i]);
                    else if (documentId == 314)
                        lstFileDKX.push(data.data[i]);
                    else if (documentId == 325)
                        lstFileBCT.push(data.data[i]);
                }
            },
            traditional: true
        });

    };
    var LoadDataCMND = function (loanBriefId, step, documentId) {
        if (step == 1)
            LoadDisplayStep(loanBriefId, 1);
        if (step == 0)
            step = stepCurren + 1;
        else if (step == -1)
            step = stepCurren - 1;
        if (lstFileCMND.length == 0)
            LoadFileRoot(loanBriefId, documentId);

        var formData = {
            LoanBriefId: parseInt(loanBriefId),
            Step: parseInt(step),
            LstFiles: lstFileCMND
        };
        $.ajax({
            url: "/Approve/LoadDataCMND",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    baseZ: 2000
                });
            },
            type: "POST",
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(formData),
            success: function (data) {
                if (data.isSuccess) {
                    stepCurren = step;
                    $('#result_cmnd').html('');
                    $('#result_cmnd').html(data.html);

                    $.unblockUI();
                }
            },
            traditional: true
        });
    };
    var LoadDataDKX = function (loanBriefId, step, documentId, isNext) {
        if (!CheckTab(2, isNext, loanBriefId))
            return;

        if (step == 1)
            LoadDisplayStep(loanBriefId, 2);
        if (step == 0)
            step = stepCurren + 1;
        if (step == -1)
            step = stepCurren - 1;
        if (lstFileDKX.length == 0)
            LoadFileRoot(loanBriefId, documentId);
        var formData = {
            LoanBriefId: parseInt(loanBriefId),
            Step: parseInt(step),
            LstFiles: lstFileDKX
        };
        $.ajax({
            url: "/Approve/LoadDataDKX",
            type: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    baseZ: 2000
                });
            },
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(formData),
            success: function (data) {
                if (data.isSuccess) {
                    stepCurren = step;

                    $('#result_dkx').html('');
                    $('#result_dkx').html(data.html);
                    $.unblockUI();
                }
            }
        });

    };
    var LoadDataBCT = function (loanBriefId, step, documentId, isNext) {
        if (!CheckTab(3, isNext, loanBriefId))
            return;
        if (step == 1)
            LoadDisplayStep(loanBriefId, 3);
        if (lstFileBCT.length == 0)
            LoadFileRoot(loanBriefId, documentId);
        var formData = {
            LoanBriefId: parseInt(loanBriefId),
            Step: parseInt(step),
            LstFiles: lstFileBCT
        };
        $.ajax({
            url: "/Approve/LoadDataBCT",
            type: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    baseZ: 2000
                });
            },
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(formData),
            success: function (data) {
                if (data.isSuccess) {
                    stepCurren = step;

                    $('#result_bct').html('');
                    $('#result_bct').html(data.html);
                    $.unblockUI();
                }
            }
        });
    };
    var LoadDataZaloFace = function (loanBriefId, step, isNext) {
        if (!CheckTab(4, isNext, loanBriefId))
            return;
        if (step == 1)
            LoadDisplayStep(loanBriefId, 4);

        var formData = {
            LoanBriefId: parseInt(loanBriefId),
            Step: parseInt(step)
        };
        $.ajax({
            url: "/Approve/LoadDataZaloFace",
            type: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    baseZ: 2000
                });
            },
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(formData),
            success: function (data) {
                if (data.isSuccess) {
                    stepCurren = step;

                    $('#result_zaloface').html('');
                    $('#result_zaloface').html(data.html);
                    $.unblockUI();
                }

            }
        });

    };
    var LoadDataApprove = function (loanBriefId, step, isNext) {
        if (!CheckTab(5, isNext, loanBriefId))
            return;
        if (step == 1)
            LoadDisplayStep(loanBriefId, 5);

        var formData = {
            LoanBriefId: parseInt(loanBriefId),
            Step: parseInt(step)
        };
        $.ajax({
            url: "/Approve/LoadDataApprove",
            type: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    baseZ: 2000
                });
            },
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(formData),
            success: function (data) {
                if (data.isSuccess) {
                    stepCurren = step;

                    $('#result_approve').html('');
                    $('#result_approve').html(data.html);
                    $.unblockUI();
                }

            }
        });

    };
    var Finish = function (loanBriefId, step, isNext) {
        $('.icon-tab5').css("display", "");

        $('#modelInitLoanBrief').modal('hide');
    };

    var CheckTab = function (tab, isNext, loanBriefId, step, isFinish) {
        var check = true;
        if (isNext == 0) {
            LoadMenuActive(tab);
            arrTabs[tab] = tab;
        } else {

            //Kiểm tra isNext = true => được click hết step
            //isNext = false => được cick thẳng từ tab
            if (isNext) {
                arrTabs[tab] = tab;
                LoadMenuActive(tab);
                //if (_isAddLog == 1)
                SaveLogCompare(loanBriefId, (tab - 1), isFinish);
            } else {
                if (arrTabs[tab] == undefined) {
                    App.ShowErrorNotLoad('Bạn phải hoàn thành hết các bước trước đó');
                    check = false;
                }
            }


        }
        return check;

    };
    var LoadMenuActive = function (tab) {
        debugger;
        $('.nav-pills li a.active').removeClass('active');
        $('#tab' + tab).addClass('active');
        $('.icon-tab' + (tab - 1)).css("display", "");
    };
    var SaveLogCompare = function (loanBriefId, tab, isFinish) {
        $.ajax({
            url: "/Approve/SaveLogCompare",
            type: "GET",
            dataType: 'json',
            data: { loanBriefId: loanBriefId, tab: tab, isFinish: isFinish },
            success: function (data) {

            }
        });

    };
    return {
        Init: Init,
        LoadDisplayStep: LoadDisplayStep,
        LoadDataCMND: LoadDataCMND,
        LoadDataDKX: LoadDataDKX,
        LoadDataBCT: LoadDataBCT,
        LoadDataZaloFace: LoadDataZaloFace,
        LoadDataApprove: LoadDataApprove,
        Finish: Finish
    }
};