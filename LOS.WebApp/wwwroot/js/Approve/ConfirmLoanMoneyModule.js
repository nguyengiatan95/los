﻿var ConfirmLoanMoneyModule = new function () {
    var Confirm = function (loanBriefId) {
        $('#divResult').html('');
        $.ajax({
            type: "POST",
            url: "/Approve/ConfirmLoanMoney",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { LoanBriefId: loanBriefId},
            success: function (data) {
                $.unblockUI();
                if (data.isSuccess) {
                    $('#divResult').html(data.html);
                    $('#Modal').modal('show');
                } else {
                    App.ShowErrorNotLoad(data.message);
                    $('#Modal').modal('hide');
                }
               
            },
            traditional: true
        });
    };
    
    var ConfirmPost = function (loanbriefId, type, name, cmt) {
       
        $.ajax({
            type: "POST",
            url: "/Approve/ConfirmLoanMoneyPost",
            data: { LoanBriefId: loanbriefId, Type: type, Name: name, CardNumber: cmt },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccess(data.Message);
                    $("#Modal").css("display", "none");
                }
                else if (data.status == 0) {
                    App.ShowError(data.Message);
                    return;
                }

            },
            traditional: true
        });
    }
    return {
        Confirm: Confirm,
        ConfirmPost: ConfirmPost
    };
};