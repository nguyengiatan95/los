﻿var DebtRestructuringModule = new function () {
    var Init = function () {
        $('#sl_Type').select2({
            placeholder: "Chọn loại hình cơ cấu",
            width: '100%'
        });
        $('#sl_Number').select2({
            placeholder: "Chọn số kỳ cần khoanh",
            width: '100%'
        });
        $('#sl_RateType').select2({
            placeholder: "Hình thức tính lãi",
            width: '100%'
        });

        $('#AppointmentDate').datepicker({
            todayBtn: "linked",
            clearBtn: false,
            todayHighlight: true,
            orientation: "bottom left",
            format: 'dd/mm/yyyy',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
    };

    $("#sl_Type").change(function () {
        var val = $('#sl_Type').val();
        if (val > 0) {

            if (val == 1 || val == 2 || val == 3 || val == 6 || val == 7) {
                $("#dis-Number").css("display", "");
                $("#dis-Date").css("display", "none");
                $("#dis-Percent").css("display", "none");
                $("#dis-Money").css("display", "none");
                $("#dis-RateType").css("display", "none");
            }

            if (val == 4) {
                $("#dis-Number").css("display", "");
                $("#dis-Date").css("display", "");
                $("#dis-Percent").css("display", "none");
                $("#dis-Money").css("display", "none");
                $("#dis-RateType").css("display", "none");
            }
            if (val == 5) {
                $("#dis-Number").css("display", "");
                $("#dis-Date").css("display", "none");
                $("#dis-Percent").css("display", "");
                $("#dis-Money").css("display", "none");
                $("#dis-RateType").css("display", "none");
            }
            if (val == 10) {
                $("#dis-Number").css("display", "none");
                $("#dis-Date").css("display", "none");
                $("#dis-Percent").css("display", "none");
                $("#dis-Money").css("display", "");
                $("#dis-RateType").css("display", "");
            }

        }
    });

    var SaveDebt = function () {
        debugger;
        var loanBriefId = $("#loanBriefId").val();
        var loanId = $("#loanId").val();
        var typeDebtRestructuring = $("#sl_Type").val();
        var numberOfRepaymentPeriod = $("#sl_Number").val();
        var appointmentDate = $("#AppointmentDate").val();
        var percentDiscount = $("#PercentDiscount").val();
        var money = $('#Money').val().replace(/,/g, '');
        var rateTypeId = $("#sl_RateType").val();
        if (typeDebtRestructuring == 0) {
            App.ShowErrorNotLoad('Bạn phải chọn loại cơ cấu nợ');
            return;
        }
        if (typeDebtRestructuring == 10) {
            if (money == 0) {
                App.ShowErrorNotLoad('Bạn phải nhập số tiền');
                return;
            }
            $('#btnSave').attr('disabled', true);
            $.ajax({
                url: '/Approve/InitDebtRevolvingLoan',
                method: "POST",
                data: { loanBriefId: loanBriefId, money: money, rateTypeId: rateTypeId},
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    });
                },
                success: function (data) {
                    $.unblockUI();
                    //$('#btnSave').attr('disabled', false);
                    if (data.status == 1) {
                        $('#Modal').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        DisbursementAfterModule.LoadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
            });
        } else {
            if (typeDebtRestructuring != 3) {
                if (numberOfRepaymentPeriod == 0) {
                    App.ShowErrorNotLoad('Bạn phải chọn số kỳ cần khoanh');
                    return;
                }
            }
            if (typeDebtRestructuring == 4) {
                if (appointmentDate == "") {
                    App.ShowErrorNotLoad('Bạn phải chọn ngày trả mới');
                    return;
                }

            }
            var formData = {
                LoanBriefId: parseInt(loanBriefId),
                LoanId: parseInt(loanId),
                TypeDebtRestructuring: parseInt(typeDebtRestructuring),
                NumberOfRepaymentPeriod: parseInt(numberOfRepaymentPeriod),
                AppointmentDate: appointmentDate,
                PercentDiscount: parseInt(percentDiscount)
            };
            $('#btnSave').attr('disabled', true);
            $.ajax({
                url: '/Approve/DebtRestructuring',
                method: "POST",
                data: formData,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    });
                },
                success: function (data) {
                    $.unblockUI();
                    $('#btnSave').attr('disabled', false);
                    if (data.status == 1) {
                        $('#Modal').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        DisbursementAfterModule.LoadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
            });
        }
            
        }
        
        
    return {
        Init: Init,
        SaveDebt: SaveDebt
    }
}
