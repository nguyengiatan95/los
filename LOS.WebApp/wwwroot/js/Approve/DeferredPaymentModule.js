﻿var DeferredPaymentModule = new function () {
    var Init = function () {
        var customerName = $('#CustomerName').val();
        var numberCard = $('#NumberCard').val();
        var loanBriefId = $('#LoanBriefId').val();
        var nationCardPlace = $('#NationCardPlace').val();
        $.ajax({
            url: "/LoanBrief/DeferredPayment",
            method: "POST",
            data: { CustomerName: customerName, NumberCard: numberCard, LoanBriefId: loanBriefId, NationCardPlace: nationCardPlace },
            success: function (data) {
                $('#dtListLoanPayment').html(data);
                //console.log(data.lstLoan);
                //if (data.status > 0) {
                //    //console.log(data.lstLoan[0].lstPaymentCustomer); return;
                //    var cla = "";
                //    if (data.lstLoan != null) {
                //        for (var i = 0; i < data.lstLoan.count; i++) {
                //            var cla = "";
                //            if (i % 2)
                //                cla = "cla";
                //            //var fromeDate = moment(lst[i]['CreateDate']);
                //            $('#dtListLoanPayment').append('<tr data-row="' + i + '" class="m-datatable__row ' + cla + '" style="left: 0px;">'
                //                + '<td class="m-datatable__cell"><span style="width: 50px;">' + (i + 1) + '</span></td>'
                //                + '<td class="m-datatable__cell"><span style="width: 150px;">' + data.lstLoan[i]['customerName'] + '</span></td>'
                //                + '<td class="m-datatable__cell"><span style="width: 100px;">' + App.FormatCurrency(data.lstLoan[i]['totalMoney']) + '</span></td>'
                //                + '<td class="m-datatable__cell"><span style="width: 100px;">' + data.lstLoan[i]['loanTime'] + ' tháng' + '</span></td>'
                //                + '<td class="m-datatable__cell"><span style="width: 100px;"></span></td>'
                //                + '<td class="m-datatable__cell"><span style="width: 100px;"></span></td>'
                //                + '<td class="m-datatable__cell"><span style="width: 100px;"></span></td>'
                //                + '<td class="m-datatable__cell"><span style="width: 100px;"><a style="cursor: pointer;" onClick="DeferredPaymentModule.toggle(' + data.lstLoan[i].lstPaymentCustomer.length + ',' + data.lstLoan.length + ');">' + data.lstLoan[i]['statusName'] + '   <i id="down-' + i + '" class="fa fa-chevron-down chevron"></i><i style="display:none;" id="up-' + i + '" class="fa fa-chevron-up chevron"></i></a></span></td></tr>');
                //            //debugger;
                //            if (data.lstLoan[i].lstPaymentCustomer != null) {
                //                var lst = data.lstLoan[i].lstPaymentCustomer;
                //                for (var i = 0; i < lst.length; i++) {
                //                    var cla = "cla";
                //                    if (i % 2)
                //                        cla = "";
                //                    //var fromeDate = moment(lst[i]['CreateDate']);
                //                    $('#dtListLoanPayment').append('<tr id="show-' + i + '" style="display:none" data-row="" class="m-datatable__row loantr ' + cla + '" style="left: 0px;">'
                //                        + '<td class="m-datatable__cell"><span style="width: 50px;"></span></td>'
                //                        + '<td class="m-datatable__cell"><span style="width: 150px;"></span></td>'
                //                        + '<td class="m-datatable__cell"><span style="width: 100px;"></span></td>'
                //                        + '<td class="m-datatable__cell"><span style="width: 100px;"></span></td>'
                //                        + '<td class="m-datatable__cell"><span style="width: 100px;">' + lst[i]['strFromDate'] + '</span></td>'
                //                        + '<td class="m-datatable__cell"><span style="width: 100px;">' + lst[i]['strToDate'] + '</span></td>'
                //                        + '<td class="m-datatable__cell"><span style="width: 100px;">' + lst[i]['countDay'] + ' ngày</span></td>'
                //                        + '<td class="m-datatable__cell"><span style="width: 100px;"></span></td></tr>');
                //                }

                //            }
                //        }
                //    }

                //}
                ////console.log(data);

            }

        });
    };

    function toggle(count, count2) {
        //debugger;
        //for (var i = 0; i < count2; i++) {
        //    if (document.getElementById("up-" + i).style.display == 'block') {
        //        document.getElementById("down-" + i).style.display = 'none';
        //        document.getElementById("up-" + i).style.display = '';
        //    } else {
        //        document.getElementById("up-" + i).style.display = 'none';
        //        document.getElementById("down-" + i).style.display = '';
        //    }
        //};
        if (count > 0) {
            for (var i = 0; i < count; i++) {
                var id = '';
                id = "show-" + count2 + "-" + i;
                if (document.getElementById(id).style.display == 'none') {
                    document.getElementById(id).style.display = '';
                } else {
                    document.getElementById(id).style.display = 'none';
                }
            }
        }


    }

    return {
        Init: Init,
        toggle: toggle
    };
}

