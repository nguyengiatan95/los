﻿var DisbursementAfterModule = new function () {
    var _groupGlobal;
    var _typeServiceCall;

    var InitData = function (group, typeServiceCall) {
        _groupGlobal = group;
        _typeServiceCall = typeServiceCall;
    }
    var Init = function () {
        $("#filterProvince").select2({
            placeholder: "Tỉnh / Thành"
        });

        $("#filterStatus").select2({
            placeholder: "Trạng thái"
        });

        $("#filterProduct").select2({
            placeholder: "Sản Phẩm"
        });

        $("#filterLocate").select2({
            placeholder: "Định vị"
        });
        $("#filterHub").select2({
            placeholder: "Hub"
        });
        $("#filterApprove").select2({
            placeholder: "Chọn Thẩm định hồ sơ"
        });
        $("#filterCavet").select2({
            placeholder: "Tìm kiếm"
        });
        $("#filterLoanStatus").select2({
            placeholder: "Tìm kiếm"
        });
        $("#filterTopup").select2({
            placeholder: "Tìm kiếm"
        });

        LoadData();
        GetProduct();
        GetShop(1);

        $("#btnSearch").click(function () {
            LoadData();
            return;
        });

        //Mã HĐ, Phone + CMND + Name
        $("#filterLoanId,#filterSearch").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return;
            }
        });

        //City
        $('#filterProvince').on('change', function () {
            GetShop($(this).val());
            LoadData();
            return;
        });
        //Status
        $('#filterStatus,#filterProduct,#filterLocate,#filterHub,#filtercreateTime,#filterApprove,#filterCavet,#filterLoanStatus,#filterTopup,#filterEmpHub').on('change', function () {
            LoadData();
            return;
        });

    };

    var LoadData = function () {
        var objQuery = {
            loanBriefId: $("#filterLoanId").val().replace('HĐ-', '').trim(),
            search: $('#filterSearch').val().trim().toLowerCase(),
            provinceId: $('#filterProvince').val(),
            filtercreateTime: $('#filtercreateTime').val(),
            status: $('#filterStatus').val(),
            productId: $('#filterProduct').val(),
            locate: $('#filterLocate').val(),
            hubId: $('#filterHub').val(),
            coordinatorUserId: $('#filterApprove').val(),
            borrowCavet: $('#filterCavet').val(),
            typeSearch: $('#filterLoanStatus').val(),
            topup: $('#filterTopup').val(),
            empHubId: $('#filterEmpHub').val()
        }

        InitDataTable(objQuery);
    }
    var InitDataTable = function (objQuery) {
        var datatable = $('#dtDisbursementAfter').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Approve/SearchDisbursementAfter',
                        params: {
                            query: objQuery
                        },
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.refData !== 'undefined' && raw.refData !== null) {
                                $('#totalMoney').html('');
                                $('#totalMoney').html(App.FormatCurrency(raw.refData));
                            }
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    }
                },
                pageSize: 20,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    title: '',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    //visible: true,
                    template: function (row) {
                        var html = '';
                        if (_groupGlobal == GROUP_MANAGER_HUB || _groupGlobal == GROUP_HUB) {
                            html += '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" value="' + row.loanBriefId + '" name="checkbox"><span></span></label></span>';
                        }
                        return html;
                    }
                },
                {
                    field: 'stt',
                    title: 'STT',
                    width: 70,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var html = '';
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        if (row.utmSource != null && row.utmSource != "") {
                            return index + record + "<br /><p style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);'>" + row.utmSource + "</p>";
                        } else {
                            return index + record;
                        }
                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += 'HĐ-' + row.loanBriefId;
                        return html;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    sortable: false,
                    template: function (row) {
                        var html = '<a class="fullname" id="linkName" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>';
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Quận/ Huyện',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.district != null)
                            text += "<p class='district'>" + row.district.name + "</p>";
                        if (row.province != null) {
                            text += "<p class='province'>" + row.province.name + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'disbursementAt',
                    title: 'Ngày giải ngân',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.disbursementAt);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay <br /> VNĐ',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = `<span class="money">${App.FormatCurrency(row.loanAmount)}</span>`
                        if (row.loanProduct != null)
                            html += `<span class="item-desciption">${row.loanProduct.name}</span>`;
                        return html;
                    }
                },

                {
                    field: 'loanTime',
                    title: 'Thời gian vay',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var html = "";
                        if (row.loanTime != null)
                            html += "<br />" + row.loanTime + " Tháng";
                        return html;
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        if (row.isUploadState) {
                            return '<span class="m-badge m-badge--success m-badge--wide"> Đã hoàn thành </span>';
                        } else {
                            return '<span class="m-badge m-badge--metal m-badge--wide"> Chưa hoàn thành </span>';
                        }
                    }
                },
                {
                    field: 'call',
                    title: 'Gọi',
                    width: 80,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        //gọi qua caresoft
                        var html = '';
                        if (_typeServiceCall == 1) {
                            html = '<a href="javascript:;" onclick="CareSoftModule.CallCareSoft(' + row.loanBriefId + ')" class="box-call">\
                                    <i class="fa fa-phone item-icon-phone" ></i></a>';
                        } else if (_typeServiceCall == 2) {// gọi qua metech
                            html = `<a href="javascript:;" onclick="finesseC2CModule.ClickToCall('${row.phone}',${row.loanBriefId})" class="box-call">
                                    <i class="fa fa-phone item-icon-phone" ></i></a>`;
                        }
                        var dauso = row.phone.substring(0, 3);
                        if (Vina.includes(dauso))
                            html += '<span class="network-home">Vina</span>'
                        else if (Mobi.includes(dauso))
                            html += '<span class="network-home">Mobi</span>'
                        else if (Viettel.includes(dauso))
                            html += '<span class="network-home">Viettel</span>'
                        else
                            html += '<span class="network-home">Không xác định</span>'
                        if (_groupGlobal == GROUP_MANAGER_HUB && row.userHubEmployee != null)
                            html += '<br /><span style="font-size: 11px;"><b>CVKD: ' + row.userHubEmployee.fullName + '</b></span>'
                        return html;
                    }
                },
                {
                    field: 'action',
                    title: 'Hành động',
                    width: 160,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (row.isLocate) {
                            html += `<a href="javascript:;" title="Thiết bị định vị" onclick="CommonModalJS.InitTrackDevice(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-location-arrow"></i></a>`;
                            html += `<a href="javascript:;" title="Bản đồ định vị" onclick="CommonModalJS.InitDeviceLocation(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-map-marker"></i></a>`;
                        }
                        html += `<a href="javascript:;" title="Nhật ký khoản vay" onclick="CommonModalJS.InitDiaryModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							             <i class="fa fa-wpforms"></i></a>`;
                        if (row.isUploadState) {
                            html += '<a href="javascript:;" title="Trả lại chưa hoàn thành" onclick="DisbursementAfterModule.ChangeIsUpload(' + row.loanBriefId + ', ' + false + ')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
							<i class="fa fa-mail-reply"></i></a>';
                        } else {

                            html += '<a href="javascript:;" title="Hoàn thành upload chứng từ" onclick="DisbursementAfterModule.ChangeIsUpload(' + row.loanBriefId + ', ' + true + ')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
							<i class="fa fa-share"></i></a>';
                        }

                        if (!row.isUploadState) {
                            html += `<a href="javascript:;" title="Upload chứng từ" onclick="CommonModalJS.InitUploadModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-upload"></i></a>`;
                        }
                        if (row.typeLoanSupport == TypeLoanSupport.IsCanTopup && (_groupGlobal == GROUP_HUB || _groupGlobal == GROUP_MANAGER_HUB)) {
                            html += `<a href="javascript:;" title="Tạo đơn Topup" onclick="LoanBriefTopup.CreateLoanTopup(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-plus"></i></a>`;
                        }

                        if (_groupGlobal == GROUP_KSNB || _groupGlobal == GROUP_TP_KSNB) {
                            html += `<a data-toggle="modal" title="File ghi âm" onclick="CommonModalJS.ViewRecordModel('#modalRecording',${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-headphones"></i></a>`;
                        }
                        html += `<a data-toggle="modal" title="Tái cơ cấu lịch trả nợ" onclick="DisbursementAfterModule.DebtRestructuring(${row.loanBriefId},${row.lmsLoanId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-table"></i></a>`;

                        html += `<a href="javascript:;" title="Gửi yêu cầu đóng HĐ" onclick="CommonModalJS.GetCloseLoan(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-check-square-o"></i></a>`;

                        return html;
                    }
                }
            ]
        });
    }
    var GetProduct = function () {
        App.Ajax("GET", "/Dictionary/GetLoanProduct", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "<option value=\"-1\">Tất cả</option>";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].loanProductId + "'>" + data.data[i].name + "</option>";
                }
                $("#filterProduct").append(html);
                $("#filterProduct").select2({
                    placeholder: "Sản phẩm"
                });
            }
        });
    }
    var GetShop = function (cityId) {
        App.Ajax("GET", "/Dictionary/GetShopByCityId?cityId=" + cityId, undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "<option value=\"-1\">Tất cả</option>";
                html += "<option></option>";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].shopId + "'>" + data.data[i].name + "</option>";
                }
                $("#filterHub").html(html);
                $("#filterHub").select2({
                    placeholder: "Hub"
                });
            }
        });
    }
    var ChangeIsUpload = function (loanid, status) {

        if (loanid == '') {
            App.ShowError('Error');
            return;
        }
        $.ajax({
            type: "POST",
            url: "/Approve/ChangeIsUpload",
            data: { LoanBriefId: loanid, Status: status },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                } else {
                    App.ShowError(data.message);
                    return;
                }
            }
        });

    };
    var GetUserByGroupId = function (id) {
        App.Ajax("GET", "/Dictionary/GetUserByGroupId?groupId=" + id, undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                //var html = "<option value=\"0\" disabled selected>Chọn Thẩm định hồ sơ</option>";
                var html = "<option value=\"-1\">Tất cả</option>";;
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option id='" + data.data[i].userId + "' value='" + data.data[i].userId + "'>" + data.data[i].username + "</option>";
                }
                $("#filterApprove").append(html);
                $("#filterApprove").select2({
                    placeholder: "Chọn Thẩm định hồ sơ"
                });
            }
        });
    }
    var GetUserHub = function (id) {
        App.Ajax("GET", "/Dictionary/GetUserByGroupId?groupId=" + id, undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                //var html = "<option value=\"0\" disabled selected>Chọn Thẩm định hồ sơ</option>";
                var html = "<option value=\"-1\">Tất cả</option>";;
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option id='" + data.data[i].userId + "' value='" + data.data[i].userId + "'>" + data.data[i].username + "</option>";
                }
                $("#filterEmpHub").append(html);
                $("#filterEmpHub").select2({
                    placeholder: "Nhân viên hub"
                });
            }
        });
    }
    var BorrowCavet = function () {
        var favorite = [];
        $.each($("input[name='checkbox']:checked"), function () {
            favorite.push($(this).val());
        });
        if (favorite == '') {
            App.ShowErrorNotLoad('Bạn phải chọn đơn vay!');
            return;
        }
        $.ajax({
            type: "POST",
            url: "/Approve/BorrowCavet",
            data: { ArrId: favorite },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    DisbursementAfterModule.LoadData();
                } else {
                    App.ShowErrorNotLoad(data.message);
                    return;
                }
            }
        });

    };
    var DebtRestructuring = function (loanBriefId, loanId) {
        $('#divResult').html('');
        $.ajax({
            type: "GET",
            url: "/Approve/DebtRestructuring",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { loanBriefId: loanBriefId, loanId: loanId },
            success: function (data) {
                $.unblockUI();
                $('#divResult').html(data);
                $('#Modal').modal('show');
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    };

    return {
        Init: Init,
        ChangeIsUpload: ChangeIsUpload,
        GetUserByGroupId: GetUserByGroupId,
        InitData: InitData,
        BorrowCavet: BorrowCavet,
        LoadData: LoadData,
        GetUserHub: GetUserHub,
        DebtRestructuring: DebtRestructuring
    };
}

