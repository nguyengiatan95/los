﻿var LenderModule = new function () {
    var Init = function () {
        var form = $('#formLender');
        form.ajaxSubmit({
            url: "/Approve/SearchLender",
            method: "POST",
            success: function (data) {
                if (data.status > 0) {
                    //console.log(App.FormatCurrency(data.data.feeInsurrance)); return;
                    $("#dtLenderBody").html('');
                    $("#Total").text('');
                    $("#TotalDisplay").css("display", "none");
                    if (data.totalRow > 10) {
                        $("#Total").text('Displaying 1 - ' + $("#filterPageSize").val() + ' of ' + data.totalRow + ' records');
                        $("#TotalDisplay").css("display", "block");
                    }
                    
                    var lst = data.lst;

                    for (var i = 0; i < lst.length; i++) {
                        var fromeDate = moment(lst[i]['CreateDate']);
                        $('#dtLenderBody').append('<tr data-row="' + i + '" class="m-datatable__row" style="left: 0px;">'
                            + '<td class="m-datatable__cell--center m-datatable__cell"><span style="width: 60px;"><span><label class="m-radio"><input type="radio" value="' + lst[i]['lenderId'] + '" name="radio"><span></span></label></span></span></td>'
                            + '<td class="m-datatable__cell"><span style="width: 100px;">' + lst[i]['lenderCode'] + '</span></td>'
                            + '<td class="m-datatable__cell"><span style="width: 150px;">' + App.FormatCurrency(lst[i]['totalMoney']) + '</span></td>'
                            + '<td class="m-datatable__cell"><span style="width: 150px;">' + fromeDate.format("DD/MM/YYYY") + '</span></td>'
                            + '<td class="m-datatable__cell"><span style="width: 300px;">' + lst[i]['address'] + '</span></td></tr>')
                    }

                }
                //console.log(data);

            }

        });
    };

    //Mã Nhà đầu tư
    $("#lenderId").on('keydown', function (e) {
        if (e.which == 13) {
            LenderModule.Init();
            return false;
        }
    });

    //Filter theo mã DT || NA
    $('#filterLenderCode').on('change', function () {
        LenderModule.Init();
    });

    //Filter theo nhà đầu tư có tiền || All
    $('#filterLenderMoney').on('change', function () {
        LenderModule.Init();
    });

    $('#filterPageSize').on('change', function () {
        LenderModule.Init();
    });


    var PushLoanToLender = function () {
        var lenderId = $("input[name='radio']:checked").val();
        var loanBriefId = $("#LoanBriefId").val();
        if (lenderId == '') {
            App.ShowError('Bạn phải chọn nhà đầu tư');
        } 
        $.ajax({
            type: "POST",
            url: "/Approve/PushLoanToLender",
            data: { LoanBriefId: loanBriefId, LenderId: lenderId },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                } else {
                    App.ShowError(data.message);
                    return;
                }
            }
        });

    };


    return {
        Init: Init,
        PushLoanToLender: PushLoanToLender
    };
}

