﻿var LenderModule = new function () {
    var Init = function () {
        var datatable = $('#dtLender').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Approve/SearchLender'
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    width: 60,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += '<span><label class="m-radio"><input type="radio" value="' + row.lenderId + '" name="radio"><span></span></label></span>';
                        return html;
                    }
                },

                {
                    field: 'lenderCode',
                    width: 100,
                    sortable: false
                },

                {
                    field: 'totalMoney',
                    width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html = App.FormatCurrency(row.totalMoney);                                              
                        return html + ' (đ)';
                    }
                },
                {
                    field: 'createDate',
                    title: 'Thời gian tạo',
                    width: 150,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY");
                    }
                },
                {
                    field: 'address',
                    width: 300,
                    sortable: false
                }
            ]
        });


        //Mã Nhà đầu tư
        $("#lenderId").on('keydown', function (e) {
            if (e.which == 13) {
                datatable.API.params.pagination.page = 1;
                datatable.search($(this).val(), 'lenderId');
                return false;
            }
        });

        //Filter theo mã DT || NA
        $('#filterLenderCode').on('change', function () {
            datatable.API.params.pagination.page = 1
            datatable.search($(this).val(), 'lenderCode');
        });

        //Filter theo nhà đầu tư có tiền || All
        $('#filterLenderMoney').on('change', function () {
            datatable.API.params.pagination.page = 1
            datatable.search($(this).val(), 'totalMoney');
        });

    };


    var PushLoanToLender = function () {
        var lenderId = $("input[name='radio']:checked").val();
        var loanBriefId = $("#LoanBriefId").val();
        if (lenderId == '') {
            App.ShowError('Bạn phải chọn nhà đầu tư');
        } 
        $.ajax({
            type: "POST",
            url: "/Approve/PushLoanToLender",
            data: { LoanBriefId: loanBriefId, LenderId: lenderId },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                } else {
                    App.ShowError(data.message);
                    return;
                }
            }
        });

    };

   


    var Search = function () {

    }

    return {
        Init: Init,
        Search: Search,
        PushLoanToLender: PushLoanToLender
    };
}

