﻿
NoteDetailModule = new function () {
    var Init = function (loanBriefId) {
        var datatable = $('#dtLoanNoteDetail').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Approve/GetNoteDetail?loanBriefId=' + loanBriefId
                    },
                },
                pageSize: 100,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }

                        return index + record;
                    }
                },
                
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    width: 70,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                //{
                //    field: 'actionComment',
                //    title: 'Tên shop',
                //    width: 100,
                //    sortable: false,
                //    template: function (row) {
                //        var html = 'Credit';
                        
                //        return html;
                //    }
                //},
                {
                    field: 'fullName',
                    title: 'Người thao tác',
                    width: 120,
                    //responsive: { visible: 'lg' },
                    sortable: false,
                    template: function (row) {
                        var html = row.fullName;
                        html += `<span class="item-desciption">(${row.shopName})</span>`;
                        return html;
                    }
                },
                {
                    field: 'note',
                    title: 'Nội dung',
                    width: 550,
                    textAlign: 'left',
                    //responsive: { visible: 'lg' },
                    sortable: false,
                    template: function (row) {
                        if (row.note != null) {
                            return App.DecodeEntities(row.note);
                        }                        
                    }
                },
            ]
        });
    };

  
    var Search = function () {

    }

    return {
        Init: Init,
        Search: Search
    };
}

