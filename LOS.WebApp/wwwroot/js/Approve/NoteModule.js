﻿NoteModule = new function () {
    var Init = function (loanBriefId) {
        var datatable = $('#dtLoanNote').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Approve/GetNoteByLoanID?loanBriefId=' + loanBriefId
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }

                        return index + record;
                    }
                },
                
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    sortable: false,
                    width: 80,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'fullName',
                    title: 'Người thao tác',
                    sortable: false,
                    width: 100,
                    template: function (row) {
                        var html = row.fullName;
                        html += `<span class="item-desciption">(${row.shopName})</span>`;
                        return html;
                    }
                },
                {
                    field: 'note',
                    title: 'Nội dung',
                    sortable: false,
                    width: 300,
                    template: function (row) {
                        if (row.note != null) {
                            return App.DecodeEntities(row.note);
                        }   
                    }
                },
            ]
        });
    };

  
    var Search = function () {

    }

    return {
        Init: Init,
        Search: Search
    };
}

