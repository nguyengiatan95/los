﻿var BusinessManagerModule = new function () {
    var InitDataTable = function (objQuery) {
        var datatable = $('#dtBusinessSeach').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/BOD/BusinessManagerSearch',
                        params: {
                            query: objQuery
                        },
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    title: '',
                    width: 45,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" value="' + row.loanBriefId + '" name="checkbox"><span></span></label></span>';
                        return html;
                    }
                },
                {
                    field: 'stt',
                    title: 'STT',
                    width: 70,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var html = '';
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;

                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += 'HĐ-' + row.loanBriefId;
                        if (row.utmSource != null && row.utmSource != "") {
                            return html += "<br /><p style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);'>" + row.utmSource + "</p>";
                        } else {
                            return html;
                        }
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    sortable: false,
                    template: function (row) {
                        var html = '<a id="linkName" style="color:#6C7293;" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>' + "<br />";
                        html += "SĐT:" + ConvertPhone(row.phone);
                        if (row.isExceptions == 1)
                            html += `<span class="item-desciption">Đơn trình ngoại lệ</span>`;
                        return html;
                    }

                },
                {
                    field: 'address',
                    title: 'Quận/ Huyện',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.district != null)
                            text += "<p class='district'>" + row.district.name + "</p>";
                        if (row.province != null) {
                            text += "<p class='province'>" + row.province.name + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = '';
                        var money = App.FormatCurrency(row.loanAmount);
                        if (row.loanProduct != null) {
                            html += money + "<br />" + "<p class='district'>" + row.loanProduct.name + "</p>";
                        } else {
                            html += money;
                        }

                        return html;
                    }
                },
                {
                    field: 'loanTime',
                    title: 'Thời gian vay',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = '';
                        if (row.loanTime != null)
                            html += row.loanTime + " Tháng";
                        return html;
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    sortable: false,
                    template: function (row) {
                        if (App.ArrLoanStatus[row.status] != null) {
                            var html = '<span class="m-badge ' + App.ArrLoanStatus[row.status].class + ' m-badge--wide">' + App.ArrLoanStatus[row.status].title + '</span>';
                            if (App.ArrPipelineState[row.pipelineState] != null && LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1) {
                                html += `<span class="item-desciption-11">${App.ArrPipelineState[row.pipelineState].title}</span>`;
                            }
                            return html;
                        } else {
                            return 'Không xác định';
                        }
                    }
                },

                {
                    field: 'tdhs',
                    title: 'Thẩm định hồ sơ',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (row.coordinatorUser != null) {
                            html = row.coordinatorUser.fullName + "</br>" + "SĐT:" + row.coordinatorUser.phone;
                        }
                        return html;

                    }
                },
                {
                    field: 'action',
                    title: 'Hành động',
                    sortable: false,
                    template: function (row) {
                        var html = '';

                        html += `<a href="javascript:;" title="Đẩy đơn vay" onclick="BODModule.FormConfirm(${row.loanBriefId}, ${row.loanAmount})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-check"></i></a>`;

                        html += `<a href="javascript:;" title="Nhật ký khoản vay" onclick="CommonModalJS.InitDiaryModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							             <i class="fa fa-wpforms"></i></a>`;

                        html += '<a href="javascript:;" title="BOD trả lại đơn" onclick="ReturnLoanBriefModule.ReturnLoanBrief(' + 1 + ',' + row.loanBriefId + ',\'' + row.fullName + '\')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
							<i class="fa fa-undo"></i></a>';

                        return html;
                    }
                }
            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            loanBriefId: $("#filterLoanId").val(),
            search: $("#filterSearch").val().toLowerCase(),
            typeLoan: $('#filterTypeLoan').val()
        }
        InitDataTable(objQuery);
    }

    var Init = function () {
        LoadData();
        $("#filterTypeLoan").select2({});
        $('#filterTypeLoan').on('change', function () {
            LoadData();
            return;
        });

        $("#filterLoanId, #filterSearch").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
            }
        });
        $("#btnSearch").click(function () {
            LoadData();
        });
    };

    return {
        Init: Init
    };
}

