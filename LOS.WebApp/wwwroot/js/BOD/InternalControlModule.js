﻿var InternalControlModule = new function () {
    var Init = function () {
        $("#filterHub").select2({
            placeholder: "Hub",
            width: '100%'
        });

        $("#filterLocate").select2({
            placeholder: "Định vị",
            width: '100%'
        });

        LoadData();

        $("#filterLoanbriefId, #filterSearch").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });

        $('#filterHub,#filterLocate,#filtercreateTime').on('change', function () {
            LoadData();
            return false;
        });

        $("#btnSearch").on('click', function (e) {
            LoadData();
            return false;
        });
    };
    var LoadData = function () {
        var objQuery = {
            loanbriefId: $('#filterLoanbriefId').val().replace('HĐ-', '').trim(),
            search: $('#filterSearch').val().trim().toLowerCase(),
            hubId: $('#filterHub').val(),
            filtercreateTime: $('#filtercreateTime').val(),
            locate: $('#filterLocate').val()
        }

        InitDataTable(objQuery);
    }
    var InitDataTable = function (objQuery) {
        var datatable = $('#dtInternalControl').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/BOD/InternalControlSearch',
                        params: {
                            query: objQuery
                        }
                    },
                },
                pageSize: 100,
                serverPaging: true,
                serverFiltering: false,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },

                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += '<span class="loanBriefId">HĐ-' + row.loanBriefId + '</span>';
                        if (row.isReborrow) {
                            html += `<br /><span class="item-desciption">(Tái vay)</span>`;
                        }
                        if (row.isLocate && row.deviceId != null && row.deviceId != "") {
                            html += ' <br /><span class="item-desciption">(' + row.deviceId + ')</span>';
                        }
                        //đơn topup
                        if (row.typeRemarketing == 2) {
                            html += '<br /><span class="item-desciption">(Topup)</span>';
                        }
                        //Đơn remarketing autocall
                        if (row.typeRemarketing == 4) {
                            html += '<br /><span class="item-desciption">(Remarketing Autocall)</span>';
                        }
                        
                        return html;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    textAlign: 'center',
                    //width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = '<a class="fullname" id="linkName" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>';
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Quận/ Huyện',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.district != null)
                            text += "<p class='district'>" + row.district.name + "</p>";
                        if (row.province != null) {
                            text += "<p class='province'>" + row.province.name + "</p>";
                        }
                       
                        return text;
                    }
                },
                {
                    field: 'disbursementAt',
                    title: 'Thời gian GN',
                    textAlign: 'center',
                    //width: 150,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.disbursementAt);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay <br /> VNĐ',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = `<span class="money">${App.FormatCurrency(row.loanAmount)}</span>`
                        if (row.loanProduct != null)
                            html += `<span class="item-desciption">${row.loanProduct.name}</span>`;
                        //if (row.isLocate) {
                        //    html += `<span class="m-badge m-badge--success m-badge--wide font-size-11">Xe lắp định vị</span>`;
                        //    if (App.ArrStatusOfDevice[row.deviceStatus] != null) {
                        //        html += '<span class="item-desciption">(' + App.ArrStatusOfDevice[row.deviceStatus].title+')</span>';
                        //    }
                        //}
                        //if (row.isTrackingLocation) {
                        //    html += `<span class="item-desciption">(KH chia sẻ vị trí)</span>`;
                        //}
                        return html;
                    }
                },
                {
                    field: 'loanTime',
                    title: 'Thời gian vay',
                    textAlign: 'center',
                    width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = "";
                        if (row.loanTime != null && row.loanTime > 0)
                            html += `<span>${row.loanTime} tháng</span></br>`;
                        if (row.isLocate) {
                            html += `<span class="m-badge m-badge--success m-badge--wide font-size-11">Xe lắp định vị</span>`;
                            if (App.ArrStatusOfDevice[row.deviceStatus] != null) {
                                html += '<span class="item-desciption">(' + App.ArrStatusOfDevice[row.deviceStatus].title + ')</span>';
                            }
                        }
                        if (row.isTrackingLocation) {
                            html += `<span class="item-desciption">(KH chia sẻ vị trí)</span>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = "";
                        if (row.status == APPRAISER_REVIEW) {
                            if (row.loanStatusDetail != null) {
                                html += '<span class="m-badge m-badge--success m-badge--wide">' + App.ArrLoanStatusDetail[row.loanStatusDetail].title + '</span>';
                                if (App.ArrLoanStatusDetail[row.loanStatusDetailChild] != null) {
                                    html += '<span class="detail-status-telesales">' + App.ArrLoanStatusDetail[row.loanStatusDetailChild].title + '</span>';

                                }
                                if (App.ArrPipelineState[row.pipelineState] != null && LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1) {
                                    html += `<span class="item-desciption-11">${App.ArrPipelineState[row.pipelineState].title}</span>`;
                                }
                            } else {
                                html += '<span class="m-badge ' + App.ArrLoanStatusDetail[40].class + ' m-badge--wide">' + App.ArrLoanStatusDetail[40].title + '</span>';
                            }
                        } else {
                            if (App.ArrLoanStatus[row.status] != null) {
                                var html = '<span class="m-badge ' + App.ArrLoanStatus[row.status].class + ' m-badge--wide">' + App.ArrLoanStatus[row.status].title + '</span>';
                                if (App.ArrPipelineState[row.pipelineState] != null && LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1) {
                                    html += `<span class="item-desciption-11">${App.ArrPipelineState[row.pipelineState].title}</span>`;
                                }
                                if (row.status == BRIEF_APPRAISER_REVIEW) {
                                    if (row.approverStatusDetail != null)
                                        html += '<span style="color: red;font-style: italic;" class="m-list-timeline__text">' + App.ArrApproveStatusDetail[row.approverStatusDetail].title + '</span>';
                                    else
                                        html += '<span style="color: red;font-style: italic;" class="m-list-timeline__text">' + App.ArrApproveStatusDetail[72].title + '</span>';
                                }
                                return html;
                            } else {
                                return 'Không xác định';
                            }
                        }

                        return html;

                    }
                },
               
                {
                    field: 'action',
                    title: 'Hành động',
                    width: 160,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        
                        html += `<a href="javascript:;" title="Nhật ký khoản vay" onclick="CommonModalJS.InitDiaryModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							             <i class="fa fa-wpforms"></i></a>`;                       
                        return html;
                    }
                }
            ]
        });
    }

    return {
        Init: Init,
        LoadData: LoadData
    };
}