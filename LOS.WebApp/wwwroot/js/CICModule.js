﻿var CICModule = new function () {
    var CICCardNumber = function (loanBriefId, fullName) {
        $('#divResult').html('');
        $.ajax({
            type: "POST",
            url: "/Approve/CICCardNumber",
            data: { LoanBriefId: loanBriefId, FullName: fullName },
            success: function (data) {
                $('#divResult').html(data);
                $('#Modal').modal('show');
            },
            traditional: true
        });
    };
    var CICCardNumberPost = function (loanbriefId, type, name, cmt) {

        $.ajax({
            type: "POST",
            url: "/Approve/CICCardNumberPost",
            data: { LoanBriefId: loanbriefId, Type: type, Name: name, CardNumber: cmt },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccess(data.Message);
                    $("#Modal").css("display", "none");
                }
                else if (data.status == 0) {
                    App.ShowError(data.Message);
                    return;
                }

            },
            traditional: true
        });
    }

    return {
        CICCardNumber: CICCardNumber,
        CICCardNumberPost: CICCardNumberPost
    };
};