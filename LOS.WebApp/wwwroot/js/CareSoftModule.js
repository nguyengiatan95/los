﻿var CareSoftModule = new function () {
    var CallCareSoft = function (loanBriefId, relationshipId, typePhone) {
        $.ajax({
            type: "POST",
            url: "/LoanBrief/CallCareSoft",
            data: { LoanBriefId: loanBriefId, RelationshipId: relationshipId, TypePhone: typePhone },
            success: function (data) {
                if (data.status == 1) {
                    //alert(data.link); return;
                    window.open(data.url);
                } else {
                    App.ShowError(data.message);
                    return;
                }
            },
            traditional: true
        });
    };

    var CallCareSoftPhone = function (phone) {
        $.ajax({
            type: "POST",
            url: "/LoanBrief/CallCareSoftByPhone",
            data: { phone: phone },
            success: function (data) {
                if (data.status == 1) {
                    //alert(data.link); return;
                    window.open(data.url);
                } else {
                    App.ShowError(data.message);
                    return;
                }
            },
            traditional: true
        });
    };

    return {
        CallCareSoft: CallCareSoft,
        CallCareSoftPhone: CallCareSoftPhone
    };
};