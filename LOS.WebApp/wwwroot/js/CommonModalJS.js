﻿var CommonModalJS = new function () {
    var InitCancelModal = function (loanId, questionQlf, modalClose) {
        $('#CancelLoanBriefBody').html('');
        if (modalClose != null && modalClose != "")
            $(modalClose).modal('hide');
        $.ajax({
            url: '/LoanBrief/CancelLoanBrief?loanbriefId=' + loanId + '&questionQlf=' + questionQlf,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#CancelLoanBriefBody').html(res.html);
                    $('#modelCancelLoanBrief').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    };

    var InitUploadModal = function (loanId) {
        $('#UploadLoanBriefFiles').html('');
        $("#btnCreate").attr('disabled', true);
        $.ajax({
            url: '/LoanBrief/UploadLoanBrief?loanbriefId=' + loanId,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#UploadLoanBriefFiles').html(res.html);
                    $('#modelUploadLoanBriefFiles').modal('show');
                    $('#hdd_LoanBriefId').val(loanId);
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
                $("#btnCreate").attr('disabled', false);
            },
            error: function () {
                $("#btnCreate").attr('disabled', false);
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    };

    var ConfirmLoanBrief = function (loanBriefId, func) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn đẩy <b>HĐ-" + loanBriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/LoanBrief/ConfirmLoanBrief",
                    data: { LoanBriefId: loanBriefId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var InitDiaryModal = function (loanBriefId) {
        $('#resultShowNote').html('');
        $.ajax({
            type: "POST",
            url: "/Approve/ShowNote",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { loanBriefId: loanBriefId },
            success: function (data) {
                $.unblockUI();
                $('#resultShowNote').html(data);
                $('#modalShowNote').modal('show');
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }

    var ConfirmCancelLoanBrief = function (loanBriefId, func) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn phê duyệt hủy <b>HĐ-" + loanBriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/LoanBrief/ConfirmCancelLoanBrief",
                    data: { LoanBriefId: loanBriefId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var ConfirmInsurence = function (loanBriefId) {

        swal({
            title: '<b>Cảnh báo</b>',
            html: "Xác nhận mua bảo hiểm <b>HĐ-" + loanBriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Approve/ConfirmInsurence",
                    data: { LoanBriefId: loanBriefId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccess(data.message);
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    },
                    traditional: true
                });

            }
        });
    }

    var InterestModal = function (loanBriefId) {
        $('#divResult').html('');
        $.ajax({
            type: "POST",
            url: "/Approve/InterestTool",
            data: { loanBriefId: loanBriefId },
            success: function (data) {
                $('#divResult').html(data);
                $('#Modal').modal('show');
            }
        });
    }

    var ManagerHubBackInit = function (loanBriefId) {
        $('#divResult').html('');
        $.ajax({
            type: "GET",
            url: "/LoanBrief/BackToStaffHubInit",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { LoanBriefId: loanBriefId },
            success: function (data) {
                $.unblockUI();
                $('#divResult').html(data);
                $('#Modal').modal('show');
            },
            traditional: true
        });
    };

    var ManagerHubBack = function (formData) {
        $("#btnReturnHub").attr('disabled', true);
        $.ajax({
            type: "POST",
            url: "/LoanBrief/BackToStaffHub",
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(formData),
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    App.ReloadData();
                    $('#Modal').modal('hide');
                }
                else if (data.status == 0) {
                    App.ShowError(data.message);
                    return;
                }
                $("#btnReturnHub").attr('disabled', false);
            },
            traditional: true
        });
    }

    var InitTrackDevice = function (loanBriefId) {
        $('#bodyModalGlobal').html('');

        $.ajax({
            url: '/LoanBrief/AddDevice?loanbriefId=' + loanBriefId,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#bodyModalGlobal').css("max-width", "40%");
                    $('#bodyModalGlobal').html(res.html);
                    $('#modalGlobal').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }

    var DetailModal = function (LoanBriefId) {
        $('#InitLoanBriefBody').html('');
        $.ajax({
            url: '/LoanBriefV3/Detail?id=' + LoanBriefId,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess != undefined) {
                    if (res.isSuccess == 1) {
                        $('#InitLoanBriefBody').html(res.html);
                        $('#modelInitLoanBrief').modal('show');
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                }
                else {
                    $('#InitLoanBriefBody').html(res);
                    $('#modelInitLoanBrief').modal('show');
                }
            },
            error: function () {
                $.unblockUI();
            }

        });
    }

    var InsurenceModal = function (loanBriefId) {
        $('#divResult').html('');

        $.ajax({
            type: "POST",
            url: "/Approve/InsurenceTool",
            data: { loanBriefId: loanBriefId },
            success: function (data) {
                $('#divResult').html(data);
                $('#Modal').modal('show');
            }
        });
    }

    var ReGetLocationSim = function (loanBriefId, func) {

        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn lấy lại thông tin Location Sim <b>HĐ-" + loanBriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    //url: "/LoanBrief/ReGetLocationSim",
                    url: "/LoanBriefV3/ReGetLocationSimV2",
                    data: { LoanBriefId: loanBriefId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var InitDeviceLocation = function (loanBriefId) {
        $('#InitDeviceLocationBody').html('');

        $.ajax({
            url: '/Approve/DeviceLocation?loanbriefId=' + loanBriefId,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                if (res.isSuccess == 1) {
                    var html = '<embed type="text/html" src="' + res.html + '" style="width:100%; height:650px">';
                    $('#titleMap').html("");
                    $('#titleMap').html("Bản đồ định vị HD-" + loanBriefId);
                    $('#InitDeviceLocationBody').html(html);
                    $('#modelDeviceLocation').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }

    var ConfirmRemoveFile = function (document, loanBriefId, fileId, func) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn xóa chứng từ trên",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/LoanBrief/RemoveImage",
                    data: { loanBriefId: loanBriefId, fileId: fileId },
                    success: function (data) {
                        debugger
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            //App.ReloadData();
                            if (document == 0)
                                UploadLoanBriefFileJS.ViewUploadMulti(loanBriefId);
                            else
                                LoadDocument(document, loanBriefId);
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var LoadDocument = function (documentId, loanBriefId) {
        $.ajax({
            type: "GET",
            url: "/LoanBrief/GetDocumetView",
            data: {
                'loanbriefId': loanBriefId,
                'documentId': documentId
            },
            dataType: 'json',
            success: function (data) {
                if (data.isSuccess == 1) {
                    $("#box_galley").html('');
                    $("#box_galley").html(data.html);
                    viewer.update();
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            timeout: 60000
        });
    }

    var CheckEkyc = function (loanBriefId, func) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có muốn check Ekyc <b>HĐ-" + loanBriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                        });
                    },
                    url: "/LoanBrief/CheckEkyc",
                    data: { LoanBriefId: loanBriefId },
                    success: function (data) {
                        $.unblockUI();
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var RequestLocation = function (loanBriefId, func) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có muốn gửi yêu cầu lấy thông tin location <b>HĐ-" + loanBriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                        });
                    },
                    url: "/LoanBrief/RequestLocation",
                    data: { LoanBriefId: loanBriefId },
                    success: function (data) {
                        $.unblockUI();
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var InitStatusTelesalesModal = function (modalStatus, loanBriefId, questionQlf) {
        $('#modelScriptTelesales').modal('hide');
        $('#bodyModalStatusTelesales').html('');
        if (modalStatus != null && modalStatus != "")
            $(modalStatus).modal('hide');
        $.ajax({
            url: '/LoanBrief/StatusTelesales',
            type: 'GET',
            data: { LoanBriefId: loanBriefId, QuestionQlf: questionQlf },
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#bodyModalStatusTelesales').html(res.html);
                    $(modalStatus).modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    };

    var InitCheckListModal = function (loanBriefId) {
        $('#modalCheckList').modal('show');
        $.ajax({
            url: '/LoanBrief/CheckListModal',
            type: 'GET',
            data: { loanBriefid: loanBriefId },
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#bodyModalCheckList').html(res.html);
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    };

    var ViewRecordModel = function (modalRecording, loanBriefId) {
        $('#Modal').modal('hide');
        $(modalRecording).modal('show');
        $('#bodyRecording').html('');
        $.ajax({
            type: "GET",
            url: "/Loanbrief/GetRecordingV2",
            data: { loanBriefId: loanBriefId },
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#bodyRecording').html(res.html);
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            }
        });
    }

    var CheckKalapa = function (loanBriefId, nationalCard) {
        if (nationalCard == null) {
            App.ShowErrorNotLoad('Không có CMT, không kiểm tra được Thông tin sổ hổ khẩu');
            return false;
        }
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có muốn kiểm tra Thông tin sổ hổ khẩu <b>HĐ-" + loanBriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                        });
                    },
                    url: "/LoanBrief/CheckKalapa",
                    data: { LoanBriefId: loanBriefId, NationalCard: nationalCard },
                    success: function (data) {
                        $.unblockUI();
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var InitPrint = function (domPrint) {
        var divToPrint = document.getElementById(domPrint);
        newWin = window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
    };

    var InitScheduleTime = function (loanBriefId) {
        $('#divResult').html('');
        $.ajax({
            type: "GET",
            url: "/ScheduleTime/Init",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { LoanBriefId: loanBriefId },
            success: function (data) {
                $.unblockUI();
                $('#divResult').html(data);
                $('#Modal').modal('show');
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    };

    var CountDown = function (duration, spanId) {
        if ($(spanId) != null && $(spanId) != undefined) {
            var timer = duration, minutes, seconds;
            clearInterval(myInterval);
            var myInterval = setInterval(function () {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);
                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;
                $(spanId).text(minutes + ":" + seconds);
                if (--timer <= 0) {
                    $(spanId).attr('style', 'color: red; font-size: 11px;');
                    $(spanId).text("Chưa phản hồi");
                }
            }, 1000);
        }
    }

    var InitStatusDetailModal = function (loanBriefId) {
        $('#divResult').html('');
        $.ajax({
            type: "GET",
            url: "/LoanBrief/NextStepLoan",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { loanBriefId: loanBriefId },
            success: function (data) {
                $.unblockUI();
                $('#divResult').html(data);
                $('#Modal').modal('show');
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }

    var InitChangeStatusApprover = function (loanBriefId) {
        $('#divResult').html('');
        $.ajax({
            type: "GET",
            url: "/Approve/ChangeStatus",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { loanBriefId: loanBriefId },
            success: function (data) {
                $.unblockUI();
                $('#divResult').html(data);
                $('#Modal').modal('show');
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }

    var CreateLoanCarChild = function (loanBriefId) {
        $.ajax({
            url: '/LoanBriefV3/CreateLoanCarChild?loanbriefId=' + loanBriefId,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#CreateLoanCarChildBody').html(res.html);
                    $('#modalCreateLoanCarChild').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }

    var InitCancelNotContactModal = function (loanId, modalClose) {
        $('#CancelLoanBriefBody').html('');
        if (modalClose != null && modalClose != "")
            $(modalClose).modal('hide');
        $.ajax({
            url: '/LoanBriefV3/CancelNotContactModal?loanbriefId=' + loanId,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#CancelLoanBriefBody').html(res.html);
                    $('#modelCancelLoanBrief').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    };

    var CompareModal = function (loanBriefId) {
        $('#InitLoanBriefBody').html('');
        $.ajax({
            url: '/Approve/CompareLoan?id=' + loanBriefId,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess != undefined) {
                    if (res.isSuccess == 1) {
                        $('#InitLoanBriefBody').html(res.html);
                        $('#modelInitLoanBrief').modal('show');
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                }
                else {
                    $('#InitLoanBriefBody').html(res);
                    $('#modelInitLoanBrief').modal('show');
                }
            },
            error: function () {
                $.unblockUI();
            }

        });
    }

    var ReEsign = function (loanBriefId, func) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn mở lại <b>HĐ-" + loanBriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/LoanBrief/ReEsign",
                    data: { LoanBriefId: loanBriefId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var GetInsuranceInfo = function (loanBriefId, func) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Lấy chứng từ thu nhập <b>HĐ-" + loanBriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "GET",
                    url: "/LoanBrief/GetInsuranceInfo",
                    data: { LoanBriefId: loanBriefId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var CancelExceptions = function (loanBriefId) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn bỏ nhãn trình ngoại lệ đơn <b>HĐ-" + loanBriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/LoanBriefV3/CancelExceptions",
                    data: { loanbriefId: loanBriefId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }




                    },
                    traditional: true
                });

            }
        });
    }

    var CancelCheckMomo = function (loanBriefId) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn bỏ kiểm tra Momo cho <b>HĐ-" + loanBriefId + "</b> không?</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/LoanBriefV3/CancelCheckMomo",
                    data: { loanBriefId: loanBriefId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });
            }
        });
    }

    var CheckMomo = function (loanBriefId) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn check Momo cho <b>HĐ-" + loanBriefId + "</b> không?</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/LoanBriefV3/CheckMomo",
                    data: { loanBriefId: loanBriefId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });
            }
        });
    }

    var ChangePhoneModal = function (loanBriefId) {
        $('#divResult').html('');
        $.ajax({
            url: '/LoanBriefV3/ChangePhone?loanBriefId=' + loanBriefId,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            success: function (data) {
                console.log(data)
                $.unblockUI();
                $('#divResult').html(data);
                $('#Modal').modal('show');
            },
            error: function () {
                $.unblockUI();
            }
        });
    }

    var TransactionsSecured = function (loanBriefId) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn tạo giao dịch đảm bảo cho <b>HĐ-" + loanBriefId + "</b> không?</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                        });
                    },
                    url: "/Approve/TransactionsSecured",
                    data: { loanBriefId: loanBriefId },
                    success: function (data) {
                        $.unblockUI();
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });
            }
        });
    }

    var GetCloseLoan = function (loanBriefId) {
        $('#bodyModalCloseLoan').html('');
        $.ajax({
            url: '/LoanBriefV3/GetCloseLoan',
            type: 'GET',
            data: { loanBriefId: loanBriefId, },
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#bodyModalCloseLoan').html(res.html);
                    $('#modalCloseLoan').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }

    var CloseLoan = function () {
        var loanbriefId = $('#hdd_LoanBriefId_CloseLoan').val();
        var content = $('#content').val();
        if (content == null || content == '' || content == undefined) {
            App.ShowErrorNotLoad('Vui lòng nhập nội dung.');
            return false;
        }
        $.ajax({
            type: "POST",
            url: "/LoanBriefV3/CloseLoan",
            data: { loanbriefId: loanbriefId, content: content },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    App.ReloadData();
                } else {
                    App.ShowErrorNotLoad(data.message);
                }

            },
            traditional: true
        });

    }


    return {
        InitCancelModal: InitCancelModal,
        InitUploadModal: InitUploadModal,
        ConfirmLoanBrief: ConfirmLoanBrief,
        InitDiaryModal: InitDiaryModal,
        ConfirmInsurence: ConfirmInsurence,
        ConfirmCancelLoanBrief: ConfirmCancelLoanBrief,
        InterestModal: InterestModal,
        ManagerHubBack: ManagerHubBack,
        InitTrackDevice: InitTrackDevice,
        DetailModal: DetailModal,
        InsurenceModal: InsurenceModal,
        ReGetLocationSim: ReGetLocationSim,
        InitDeviceLocation: InitDeviceLocation,
        ConfirmRemoveFile: ConfirmRemoveFile,
        CheckEkyc: CheckEkyc,
        InitStatusTelesalesModal: InitStatusTelesalesModal,
        InitCheckListModal: InitCheckListModal,
        InitPrint: InitPrint,
        ViewRecordModel: ViewRecordModel,
        CheckKalapa: CheckKalapa,
        InitScheduleTime: InitScheduleTime,
        CountDown: CountDown,
        InitStatusDetailModal: InitStatusDetailModal,
        ManagerHubBackInit: ManagerHubBackInit,
        InitChangeStatusApprover: InitChangeStatusApprover,
        InitCancelNotContactModal: InitCancelNotContactModal,
        CreateLoanCarChild: CreateLoanCarChild,
        CompareModal: CompareModal,
        ReEsign: ReEsign,
        GetInsuranceInfo: GetInsuranceInfo,
        CancelExceptions: CancelExceptions,
        CancelCheckMomo: CancelCheckMomo,
        CheckMomo: CheckMomo,
        ChangePhoneModal: ChangePhoneModal,
        TransactionsSecured: TransactionsSecured,
        GetCloseLoan: GetCloseLoan,
        CloseLoan: CloseLoan
    };
};