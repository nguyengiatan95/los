﻿var ConfigDocument = new function () {

    var LoadData = function () {
        $('#boxContentForm').html('');
        var productId = $('#sl_Product').val();
        var typeOwnerShip = $('#sl_TypeOwnerShip').val();
        $.ajax({
            type: "POST",
            url: "/ConfigDocument/GetData",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { ProductId: productId, TypeOwnerShip: typeOwnerShip },
            success: function (data) {
                $('#boxContentForm').html(data);
                $.unblockUI();
            },
            traditional: true
        });
    }

    var ShowConfid = function (id) {
        $('#boxConfig').html('');
        $.ajax({
            type: "POST",
            url: "/ConfigDocument/ShowConfig",
            data: { DocumentId: id },
            success: function (data) {
                $('#boxConfig').html(data);
            },
            traditional: true
        });
    }
    var AddConfig = function (btn, e) {

        e.preventDefault();
        var form = $('#formConfig');
        form.ajaxSubmit({
            url: '/ConfigDocument/AddConfig',
            method: 'POST',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            //dataType:'json',
            //data: JSON.stringify(form.serialize()),
            success: function (data, status, xhr, $form) {
                $.unblockUI();
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccessNotLoad(data.message);
                        ConfigDocument.LoadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };

    return {
        LoadData: LoadData,
        ShowConfid: ShowConfid,
        AddConfig: AddConfig
    };
}

