﻿var ConfigMenuJS = new function () {
    var ArrMenu = {
        "/": ".home",
        "/home.html": ".home",
        "/group-manager.html": ".Group-Index",
        "/user-manager.html": ".Users-Index",
        "/module-manager.html": ".module-index",
        "/usermmo-manager.html": ".UserMmo-Index",
        "/telesales/dashboard.html": ".Telesales-Index",
        "/pipeline/index.html": ".Pipeline-Index",
        "/loanbrief/index.html": ".LoanBrief-Index",
        "/loanbrief/manager-hub.html": ".LoanBrief-ManagerHub",
        "/loanbrief/staff-hub.html": ".LoanBrief-StaffHub",
        "/approve/employee.html": ".Approve-Employee",
        "/approve/leader.html": ".Approve-Leader",
        "/approve/disbursementafter.html": ".Approve-DisbursementAfter",
        "/search/index.html": ".LoanBrief-IndexSearch",
        "/module-managerareahub.html": ".ManagerAreaHub-index",
        "/telesales/managestaff.html": ".Telesale-ManageStaffTelesale",
        "/pricingtool/index.html": ".LoanBrief-ToolCalculatormoney",
        "/interesttool/index.html": ".LoanBrief-ToolInterest",
        "/product/manage_price_car": ".Products-ManagePriceCar",
        "/sound-recording/index.html": ".Tool-Recording",
        "/log-reqest-ai/index.html": ".Tool-LogRequestAi",
        "/log-call-api/index.html": ".Tool-LogCallApi",
        "/loancancel/index.html": ".Remarketing-IndexLoanCancel",
        "/user/manageruser.html": ".Users-ManageAccountHCNS",
        "/loanbrief/import_loan_file_excel.html": ".LoanBrief-ImportFileExcel",
        "/documenttype/document-manager.html": ".DocumentType-Index",
        "/contact/index.html": ".ContactCustomer-Index",
        "/calendar/index.html": ".Hub-Calendar",
        "/premature_interest_tool/index.html": ".Tool-PrematureInterestTool",
        "/hub/listloandebt.html": ".Hub-ListLoanDebt",
        "/rmkt/index.html": ".Remarketing-RemarketingTls",
        "/tool-pipeline/index.html": ".Tool-ToolPipeline",
        "/loanbrief-search/index.html": ".LoanBriefV3-LoanbriefSearch",
        "/tool/add-district-and-ward.html": ".Tool-AddDistrictAndWard",
        "/businessmanager/index.html": ".LoanBrief-BusinessManagerIndex",
        "/loanbrief/department.html": ".Loanbrief-LoanDepartment",
        "/internalcontrol/index.html": ".BOD-InternalControlIndex",
        "/tool/gen_new_otp.html": ".Tool-GenNewOtp",
        "/report/approve_employee.html": ".Report-ReportApproveEmployee",
        "/dictionary/manage-province.html": ".Dictionary-ManageProvince",
        "/dictionary/manage-district.html": ".Dictionary-ManageDistrict",
        "/dictionary/manage-ward.html": ".Dictionary-ManageWard",
        "/loanbrief/disbursement_contract.htm": ".LoanBriefV3-DisbursementContract",
        "/tool/support.html": ".Tool-ToolSupport",
        "/tool/manage-telesale-support.html": ".Tool-ToolManageTelesaleSupport",
        "/tool/update_status_push_loan_to_lender.html": ".Tool-UpdateStatusPushLoanToLender",
    };
    var Init = function () {
        $('.m-menu__item').removeClass('m-menu__item--active').removeClass('m-menu__item--open');
        var pathname = window.location.pathname;
        if (ArrMenu[pathname] != null) {
            $(ArrMenu[pathname]).parents('ul').parents('div').parents().closest('li').addClass('m-menu__item--open');
            $(ArrMenu[pathname]).parents('ul').parents().closest('.m-menu__submenu ').css('display', 'block');
            $(ArrMenu[pathname]).addClass('m-menu__item--active');
        }
    };
    return {
        Init: Init
    };
};

jQuery(document).ready(function () {
    ConfigMenuJS.Init();
});