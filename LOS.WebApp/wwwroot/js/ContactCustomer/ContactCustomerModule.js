﻿var ContactCustomerModule = new function () {
    var _groupGlobal;
    var _typeServiceCall;

    var InitData = function (group, typeServiceCall) {
        _groupGlobal = group;
        _typeServiceCall = typeServiceCall;
    }
    var InitDataTable = function (objQuery) {
        var datatable = $('#dtContactCustomer').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/ContactCustomer/GetDataContactCustomer',
                        params: {
                            query: objQuery
                        },
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Họ tên',
                    sortable: false,
                    width: 120,
                    template: function (row) {
                        var html = '<span class="fullname"><b>' + row.fullName + '</b></span>';
                        return html;
                    }
                },
                {
                    field: 'phone',
                    title: 'Số điện thoại',
                    width: 80,
                    sortable: false
                }
                , {
                    field: 'call',
                    title: 'Gọi',
                    width: 80,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        //gọi qua caresoft
                        var html = '';
                        if (_typeServiceCall == 1) {
                            html = '<a href="javascript:;" onclick="CareSoftModule.CallCareSoftPhone(' + row.phone + ')" class="box-call">\
                                    <i class="fa fa-phone item-icon-phone" ></i></a>';
                        } else if (_typeServiceCall == 2) {// gọi qua metech
                            html = `<a href="javascript:;" onclick="finesseC2CModule.ClickToCallNoLog('${row.phone}')" class="box-call">
                                    <i class="fa fa-phone item-icon-phone" ></i></a>`;
                        }
                        var dauso = row.phone.substring(0, 3);
                        if (Vina.includes(dauso))
                            html += '<span class="network-home">Vina</span>'
                        else if (Mobi.includes(dauso))
                            html += '<span class="network-home">Mobi</span>'
                        else if (Viettel.includes(dauso))
                            html += '<span class="network-home">Viettel</span>'
                        else
                            html += '<span class="network-home">Không xác định</span>'

                        return html;
                    }
                }
                , {
                    field: 'province.name',
                    title: 'Tỉnh thành',
                    width: 50,
                    sortable: false
                },
                {
                    field: 'district.name',
                    title: 'Quận huyện/Phường xã',
                    sortable: false,
                    width: 100,
                    template: function (row) {
                        var text = '<div">';
                        if (row.district != null) {
                            text += '<span class="district">' + row.district.name + '</span> <br />';
                        }
                        if (row.ward != null) {
                            text += '<span class="province">' + row.ward.name + '</span>';
                        }
                        text += '</div>';
                        return text;
                    }
                },
                {
                    field: 'job.name',
                    title: 'Ngành nghề',
                    width: 100,
                    sortable: false
                },
                {
                    field: 'income',
                    title: 'Thu nhập',
                    sortable: false,
                    template: function (row) {
                        var html = `<span class="money">${App.FormatCurrency(row.income)}</span>`
                        return html;
                    }
                },
                {
                    field: 'brandProduct.name',
                    title: 'Hãng xe',
                    sortable: false
                },
                {
                    field: 'product.name',
                    title: 'Tên xe',
                    sortable: false
                },
                {
                    field: 'createdate',
                    title: 'Ngày tạo',
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdate);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm")
                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    sortable: false,
                    template: function (row) {
                        var text = '';
                        if (row.loanBriefId != null && row.loanBriefId != 0) {
                            text = '<span class="m-badge m-badge--success m-badge--wide font-size-11"> HĐ-' + row.loanBriefId + '</span>';
                        }
                        return text;
                    }
                }
                , {
                    field: 'Action',
                    title: 'Hành động',
                    sortable: false,
                    width: 160,
                    template: function (row, index, datatable) {
                        var html = '';
                        if (row.isCheckCreateLoanbrief) {
                            html += '\
                 <a href="javascript:;" title="Khởi tạo đơn vay" onclick="ContactCustomerModule.ConfirmCreateLoanBrief('+ row.contactId + ',\'' + row.fullName + '\',\'' + row.phone + '\')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
                         <i class="fa fa-plus"></i></a>'
                        }
                        if (row.loanBriefId <= 0 || row.loanBriefId == null) {
                            html += '\
                <a href="javascript:;" title="cập nhật thông tin liên hệ" onclick="ContactCustomerModule.InitModal('+ row.contactId + ')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
                                         <i class="la la-edit"></i></a>';
                            html += '\
                <a href="javascript:;" title="Xóa thông tin liên hệ" onclick="ContactCustomerModule.ConfirmDeleteContact('+ row.contactId + ',\'' + row.fullName + '\',\'' + row.phone + '\')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
                                         <i class="fa fa-trash"></i></a>';
                        }
                      
                        return html;
                    }
                }
            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            filterPhone: $('#filterPhone').val().trim(),
            filterName: $('#filterName').val().trim(),
            filterProvince: $('#filterProvince').val().trim()
        }
        InitDataTable(objQuery);
    }

    var Init = function () {
        LoadData();
        $("#filterName").on('blur', function (e) {
            LoadData();
            return false;
        });
        $("#filterPhone").on('blur', function (e) {
            LoadData();
            return false;
        });
        $("#filterProvince").select2({
            placeholder: "Tỉnh / Thành"
        });
        $("#filterProvince").on('change', function (e) {
            LoadData();
            return false;
        });
        $('#filterName').bind("enterKey", function (e) {
            LoadData();
        });
        $('#filterName').keyup(function (e) {
            if (e.keyCode == 13) {
                $(this).trigger("enterKey");
            }
        });
        $('#filterPhone').bind("enterKey", function (e) {
            LoadData();
        });
        $('#filterPhone').keyup(function (e) {
            if (e.keyCode == 13) {
                $(this).trigger("enterKey");
            }
        });
    };

    var InitModal = function (contactid) {
        $('#InitContactCustomerBody').html('');
        $.ajax({
            url: '/ContactCustomer/Init?id=' + contactid,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#InitLoanBriefBody').html(res.html);
                    $('#modelInitLoanBrief').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
            }

        });
    }

    var SubmitForm = function (e) {
        e.preventDefault();
        var form = $('#formInitContactCustomer');
        form.validate({
            ignore: [],
            rules: {
                'FullName': {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                'Phone': {
                    required: true,
                    validPhone: true
                }
            },
            messages: {
                'FullName': {
                    required: "Bạn vui lòng nhập họ tên",
                },
                'Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                }
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            return;
        } else {
            $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
            $('#btnSubmit').attr('disabled', true);
            form.ajaxSubmit({
                url: '/ContactCustomer/InitContactCustomer',
                method: 'POST',
                //beforeSend: function (xhr) {
                //    debugger
                //    xhr.setRequestHeader('RequestVerificationToken', $('input:hidden[name="__RequestVerificationToken"]').val());
                //},
                success: function (data, status, xhr, $form) {
                    if (data != undefined) {
                        if (data.status == 1) {
                            $('#modelInitContactCustomer').modal('hide');
                            App.ReloadData();
                            App.ShowSuccessNotLoad(data.message);
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                    $('#btnSubmit').attr('disabled', false);
                },
                error: function () {
                }
            });
        }
    }

    var ConfirmCreateLoanBrief = function (contactId, Name, Phone) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn khởi tạo đơn vay khách hàng <b>" + Name + "</b> .SĐT : " + Phone + "",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/ContactCustomer/CreateLoanBrief",
                    data: { ContactId: contactId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ReloadData();
                            App.ShowSuccessNotLoad(data.message);
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    },
                    traditional: true
                });

            }
        });
    }

    var ConfirmDeleteContact = function (contactId, Name, Phone) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn xóa thông tin khách hàng <b>" + Name + "</b> .SĐT : " + Phone + "",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/ContactCustomer/DeleteContactCustomer",
                    data: { ContactId: contactId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ReloadData();
                            App.ShowSuccessNotLoad(data.message);
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    },
                    traditional: true
                });

            }
        });
    }

    return {
        InitData: InitData,
        Init: Init,
        LoadData: LoadData,
        Create: InitModal,
        InitModal: InitModal,
        Save: SubmitForm,
        ConfirmCreateLoanBrief: ConfirmCreateLoanBrief,
        ConfirmDeleteContact: ConfirmDeleteContact
    };
}

