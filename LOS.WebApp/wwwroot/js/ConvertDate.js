﻿function formattedDateHourMinutes(date) {
    if (date == "0001-01-01T00:00:00") {
        return '';
    } else {
        var d = new Date(date);
        let month = String(d.getMonth() + 1);
        let day = String(d.getDate());
        const year = String(d.getFullYear());
        let hour = "" + d.getHours(); if (hour.length == 1) { hour = "0" + hour; }
        let minute = "" + d.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
        //let second = "" + d.getSeconds(); if (second.length == 1) { second = "0" + second; }
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return `${day}/${month}/${year} ${hour}:${minute} `;
    }

}

function formattedDate(date) {
    if (date == "0001-01-01T00:00:00") {
        return '';
    } else {
        var d = new Date(date);
        let month = String(d.getMonth() + 1);
        let day = String(d.getDate());
        const year = String(d.getFullYear());
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return `${day}/${month}/${year} `;
    }

}


function ConvertPhone(Phone) {
    var str = "xxxxxxx";
    var length = Phone.length;
    var center;
    var end;
    center = length - 6;
    end = length - 3;
    var newPhone;
    newPhone = Phone.substring(0, 3) + str.substring(0, center) + Phone.substring(end, length);
    return newPhone;
}