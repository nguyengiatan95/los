﻿var ManageDistrictModule = new function () {

    var InitDataTable = function (objQuery) {
        var datatable = $('#tbManageDistrict').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/Dictionary/LoadDataDistrict',
                        params: {
                            query: objQuery
                        },
                    }
                },
                pageSize: 50,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'name',
                    title: 'Tên',
                    textAlign: 'center',
                    sortable: false
                },
                {
                    field: 'type',
                    title: 'Loại',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        if (row.type != null)
                            return '<span>' + App.ArrTypeDistrict[row.type].title + '</span>'
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    sortable: false,
                    template: function (row) {
                        if (row.isApply == null)
                            row.isApply = 0;
                        var status = {
                            0: { 'title': 'Không hỗ trợ', 'class': 'm-badge--metal' },
                            1: { 'title': 'Hỗ trợ', 'class': 'm-badge--success' }
                        };
                        return '<span class="m-badge ' + status[row.isApply].class + ' m-badge--wide">' + status[row.isApply].title + '</span>';
                    }
                },
                {
                    field: 'Action',
                    title: 'Action',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = '';
                        html += '<a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" title="Sửa Quận/Huyện"  onclick="ManageDistrictModule.LoadInfoDistrict(\'' + row.districtId + '\',\'' + row.name + '\',\'' + row.provinceId + '\',\'' + row.type + '\',\'' + row.isApply + '\',\'' + row.latLong + '\')" >\
                                        <i class="la la-edit"></i></a>';
                        if (row.isApply == 1) {
                            html += '<a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" title="Hủy hỗ trợ" onclick="ManageDistrictModule.UpdateIsApply(\'' + row.districtId + '\',\'' + row.name + '\', 0)" >\
                                        <i class="fa fa-lock"></i></a>';
                        }
                        else {
                            html += '<a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" title="Kích hoạt hỗ trợ" onclick="ManageDistrictModule.UpdateIsApply(\'' + row.districtId + '\',\'' + row.name + '\', 1)" >\
                                        <i class="fa fa-unlock"></i></a>';
                        }
                        return html;
                    }
                }
            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            status: $('#filterStatus').val(),
            filterName: $('#filterSearchName').val().trim(),
            provinceId: $('#filterProvince').val(),
        }

        InitDataTable(objQuery);
    }

    var Init = function () {
        $("#filterStatus, #filterProvince, #sl_create_province, #sl_create_type").select2({
            placeholder: "Vui lòng chọn"
        });

        $('#filterStatus, #filterProvince').on('change', function () {
            LoadData();
        });

        $("#filterSearchName").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });

        LoadData();
    }

    var UpdateIsApply = function (districtId, name, isApply) {
        var html = '';
        if (isApply == 1) { //hỗ trợ
            html = "Bạn có chắc chắn kích hoạt <b>" + name + "</b> thành hỗ trợ không?"
        } else { // không hỗ trợ
            html = "Bạn có chắc chắn hủy hỗ trợ <b>" + name + "</b> không?"
        }
        swal({
            title: '<b>Cảnh báo</b>',
            html: html,
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Dictionary/UpdateIsApplyDistrict",
                    data: { districtId: districtId, isApply: isApply },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            LoadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    },
                    traditional: true
                });
            }
        });
    }

    var LoadInfoDistrict = function (districtId, name = '', provinceId = 0, type = 0, isApply = 0, latLong = '') {
        if (districtId == 0) {
            $('#header_create_district').html('Tạo mới Quận/Huyện');
            $('#sl_create_province').val(0).change();
            $('#txt_create_name').val('');
            $('#txt_create_latlong').val('');
            $('#sl_create_type').val(0).change();
            document.getElementById("cb_IsApply").checked = false;
        }
        else {
            $('#header_create_district').html('Cập nhập Quận/Huyện');
            $('#hdd_DistrictId').val(districtId);
            $('#sl_create_province').val(provinceId).change();
            $('#txt_create_name').val(name);

            if (latLong != 'null')
                $('#txt_create_latlong').val(latLong);
            else
                $('#txt_create_latlong').val('');

            $('#sl_create_type').val(type).change();
            if (isApply == '1') {
                document.getElementById("cb_IsApply").checked = true;
            }
        }

        $('#modal_create_district').modal('show');
    }

    var AddOrUpdate = function (btn) {
        $(btn).attr('disabled', 'disabled');
        $(btn).attr('style', 'cursor: not-allowed;');
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            rules: {
                ProvinceId: {
                    required: true
                },
                Name: {
                    required: true
                },
                Type: {
                    required: true

                }
            },
            messages: {
                ProvinceId: {
                    required: "Vui lòng chọn Thành phố"
                },
                Name: {
                    required: "Vui lòng nhập tên Quận/Huyện"
                },
                Type: {
                    required: "Vui lòng chọn loại"
                }
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            $(btn).removeAttr('disabled', 'disabled');
            $(btn).removeAttr('style', 'cursor: not-allowed;');
            return false;
        }
        form.ajaxSubmit({
            url: '/Dictionary/AddOrUpdateDistrict',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $(btn).removeAttr('disabled', 'disabled');
                $(btn).removeAttr('style', 'cursor: not-allowed;');
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    $('#modal_create_district').modal('hide');
                    LoadData();
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    return {
        Init: Init,
        UpdateIsApply: UpdateIsApply,
        LoadInfoDistrict: LoadInfoDistrict,
        AddOrUpdate: AddOrUpdate
    }
}