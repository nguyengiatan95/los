﻿var ManageProvinceModule = new function () {

    var InitDataTable = function (objQuery) {
        var datatable = $('#tbManageProvince').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/Dictionary/LoadDataProvince',
                        params: {
                            query: objQuery
                        },
                    }
                },
                pageSize: 50,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'name',
                    title: 'Tên',
                    textAlign: 'center',
                    sortable: false
                },
                {
                    field: 'type',
                    title: 'Loại',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        return '<span>' + App.ArrTypeProvince[row.type].title + '</span>'
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    sortable: false,
                    template: function (row) {
                        if (row.isApply == null)
                            row.isApply = 0;
                        var status = {
                            0: { 'title': 'Không hỗ trợ', 'class': 'm-badge--metal' },
                            1: { 'title': 'Hỗ trợ', 'class': 'm-badge--success' }
                        };
                        return '<span class="m-badge ' + status[row.isApply].class + ' m-badge--wide">' + status[row.isApply].title + '</span>';
                    }
                },
                {
                    field: 'Action',
                    title: 'Action',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = '';
                        if (row.isApply == 1) {
                            html += '<a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" title="Hủy hỗ trợ" onclick="ManageProvinceModule.UpdateIsApply(\'' + row.provinceId + '\',\'' + row.name + '\', 0)" >\
                                        <i class="fa fa-lock"></i></a>';
                        }
                        else {
                            html += '<a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" title="Kích hoạt hỗ trợ" onclick="ManageProvinceModule.UpdateIsApply(\'' + row.provinceId + '\',\'' + row.name + '\', 1)" >\
                                        <i class="fa fa-unlock"></i></a>';
                        }
                        return html;
                    }
                }
            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            status: $('#filterStatus').val(),
            filterName: $('#filterSearchName').val().trim(),
        }

        InitDataTable(objQuery);
    }

    var Init = function () {
        $("#filterStatus").select2({
            placeholder: "Vui lòng chọn"
        });

        $('#filterStatus').on('change', function () {
            LoadData();
        });

        $("#filterSearchName").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });

        LoadData();
    }

    var UpdateIsApply = function (provinceId, name, isApply) {
        var html = '';
        if (isApply == 1) { //hỗ trợ
            html = "Bạn có chắc chắn kích hoạt thành phố <b>" + name + "</b> thành hỗ trợ không?"
        } else { // không hỗ trợ
            html = "Bạn có chắc chắn hủy hỗ trợ thành phố <b>" + name + "</b> không?"
        }
        swal({
            title: '<b>Cảnh báo</b>',
            html: html,
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Dictionary/UpdateIsApplyProvince",
                    data: { provinceId: provinceId, isApply: isApply },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            LoadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    },
                    traditional: true
                });
            }
        });
    }

    return {
        Init: Init,
        UpdateIsApply: UpdateIsApply,
    }
}