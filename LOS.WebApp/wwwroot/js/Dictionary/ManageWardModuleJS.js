﻿var ManageWardModule = new function () {

    var InitDataTable = function (objQuery) {
        var datatable = $('#tbManageWard').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/Dictionary/LoadDataWard',
                        params: {
                            query: objQuery
                        },
                    }
                },
                pageSize: 50,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'name',
                    title: 'Tên',
                    textAlign: 'center',
                    sortable: false
                },
                {
                    field: 'type',
                    title: 'Loại',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        if (row.type != null)
                            return '<span>' + App.ArrTypeWard[row.type].title + '</span>'
                    }
                },
                {
                    field: 'Action',
                    title: 'Action',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var html = '';
                        html += '<a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" title="Sửa Phường/Xã"  onclick="ManageWardModule.LoadInfoWard(\'' + row.wardId + '\',\'' + row.name + '\',\'' + row.districtId + '\',\'' + row.type + '\')" >\
                                        <i class="la la-edit"></i></a>';
                        return html;
                    }
                }
            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            filterDistrict: $('#filterDistrict').val(),
            filterName: $('#filterSearchName').val().trim(),
        }

        InitDataTable(objQuery);
    }

    var Init = function () {
        $("#filterDistrict, #filterProvince, #sl_create_province, #sl_create_district, #sl_create_type").select2({
            placeholder: "Vui lòng chọn"
        });

        $('#filterDistrict').on('change', function () {
            LoadData();
        });

        $("#filterSearchName").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });

        ChangeDistrict(ProvinceSupport.HaNoi, '#filterDistrict');

        setTimeout(function () {
            LoadData();
        }, 1000);
    }

    var ChangeDistrict = function (provinceId, element) {
        App.Ajax("GET", "/Dictionary/GetDistrictByProvinceId?provinceId=" + provinceId).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    if (data.data[i].districtId == 1)
                        html += "<option value='" + data.data[i].districtId + "' selected>" + data.data[i].name + "</option>";
                    else
                        html += "<option value='" + data.data[i].districtId + "'>" + data.data[i].name + "</option>";
                }

                $(element).append(html);
                $(element).select2({
                    placeholder: "Vui lòng chọn"
                });
            }
        });
    }

    var LoadInfoWard = function (wardId, name = '', districtId = 0, type = 0) {
        if (wardId == 0) {
            $('#header_create_ward').html('Tạo mới Phường/Xã');
            $('#sl_create_province').val(0).change();
            $('#sl_create_district').val(0).change();
            $('#txt_create_name').val('');
            $('#sl_create_type').val(0).change();
        }
        else {
            $('#header_create_ward').html('Cập nhập Phường/Xã');
            $('#hdd_WardId').val(wardId);
            if (districtId > 0) {
                App.Ajax("GET", "/Dictionary/GetProvinceIdByDistrictId?districtId=" + districtId).done(function (data) {
                    if (data.data != undefined && data.data > 0) {
                        $('#sl_create_province').val(data.data).change();
                        $('#sl_create_province').append(html);
                        $('#sl_create_province').select2({
                            placeholder: "Vui lòng chọn"
                        });

                        App.Ajax("GET", "/Dictionary/GetDistrictByProvinceId?provinceId=" + data.data).done(function (data) {
                            if (data.data != undefined && data.data.length > 0) {
                                var html = "";
                                for (var i = 0; i < data.data.length; i++) {
                                    if (data.data[i].districtId == districtId)
                                        html += "<option value='" + data.data[i].districtId + "' selected>" + data.data[i].name + "</option>";
                                    else
                                        html += "<option value='" + data.data[i].districtId + "'>" + data.data[i].name + "</option>";
                                }
                                $('#sl_create_district').append(html);
                                $('#sl_create_district').select2({
                                    placeholder: "Vui lòng chọn"
                                });
                            }
                        });
                    }
                });

                
            }
            $('#txt_create_name').val(name);
            $('#sl_create_type').val(type).change();
        }

        $('#modal_create_ward').modal('show');
    }

    var AddOrUpdate = function (btn) {
        $(btn).attr('disabled', 'disabled');
        $(btn).attr('style', 'cursor: not-allowed;');
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            rules: {
                ProvinceId: {
                    required: true
                },
                DistrictId: {
                    required: true
                },
                Name: {
                    required: true
                },
                Type: {
                    required: true

                }
            },
            messages: {
                ProvinceId: {
                    required: "Vui lòng chọn Thành phố"
                },
                DistrictId: {
                    required: "Vui lòng chọn Quận/Huyện"
                },
                Name: {
                    required: "Vui lòng nhập tên Phường/Xã"
                },
                Type: {
                    required: "Vui lòng chọn loại"
                }
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            $(btn).removeAttr('disabled', 'disabled');
            $(btn).removeAttr('style', 'cursor: not-allowed;');
            return false;
        }
        form.ajaxSubmit({
            url: '/Dictionary/AddOrUpdateWard',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $(btn).removeAttr('disabled', 'disabled');
                $(btn).removeAttr('style', 'cursor: not-allowed;');
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    $('#modal_create_ward').modal('hide');
                    LoadData();
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    return {
        Init: Init,
        ChangeDistrict: ChangeDistrict,
        LoadInfoWard: LoadInfoWard,
        AddOrUpdate: AddOrUpdate
    }
}