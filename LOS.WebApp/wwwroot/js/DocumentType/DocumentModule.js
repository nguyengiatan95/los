﻿var DocumentModule = new function () {

    var InitDataTable = function (objQuery) {
        var datatable = $('#tableDocument').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/DocumentType/LoadData',
                        params: {
                            query: objQuery
                        }
                    },
                },
                pageSize: 50,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'name',
                    title: 'Loại chứng từ',
                    sortable: false
                },
                //{
                //    field: 'loanproduct',
                //    title: 'Gói sản phẩm',
                //    sortable: false,
                //},
                //{
                //    field: 'numberImage',
                //    title: 'Tổng file',
                //    sortable: false,
                //    template: function (row) {
                //        if (row.numberImage == null)
                //            return ""
                //        else
                //            return row.numberImage;
                //    }
                //},
                {
                    field: 'isEnable',
                    title: 'Trạng thái',
                    sortable: false,
                    template: function (row) {
                        if (row.status == null)
                            row.status = 0;
                        var status = {
                            0: { 'title': 'Chưa kích hoạt', 'class': 'm-badge--metal' },
                            1: { 'title': 'Kích hoạt', 'class': 'm-badge--success' }
                        };
                        return '<span class="m-badge ' + status[row.isEnable].class + ' m-badge--wide">' + status[row.isEnable].title + '</span>';
                    }
                }
                ,
                {
                    field: 'isMenu',
                    title: 'Mô tả',
                    sortable: false,
                    template: function (row) {
                        return row.guide;
                    }
                }
                , {
                    field: 'Action',
                    title: 'Chức năng',
                    sortable: false,
                    width: 100,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return '<a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" onclick="DocumentModule.LoadInfoDocumentType(\'' + row.id + '\',\'' + row.name + '\')"><i class="la la-edit"></i></a>';
                    }
                }
            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            productId: $('#productId').val(),
            isEnable: $('#isEnable').val(),
            filterName: $("#filterName").val().trim()
        }
        InitDataTable(objQuery);
    }

    var Init = function () {
        LoadData();

        $("#filterName").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });

        $('#productId,#isEnable').on('change', function () {
            LoadData();
            return false;
        });

        $('#productId, #isEnable, #sl_create_productId, #sl_create_parentId,#sl_typeOwnerShip,#sl_workRuleType').select2();
        CKEDITOR.replace("txt_create_document_type_guide");
    };

    var LoadInfoDocumentType = function (documentTypeId, docmentTypeName) {
        var form = $('#form_create_document_type').closest('form');
        var validator = form.validate();
        validator.destroy();
        $('#hdd_create_userID').val(documentTypeId);
        $('#sl_create_parentId').val(0).change();
        $('#sl_create_productId').val(0).change();
        $('#sl_typeOwnerShip').val(0).change();
        //$('#sl_workRuleType').val(0).change();
        $('#txt_create_document_type_name').val('');
        //$('#txt_create_document_type_number_image').val();
        CKEDITOR.instances['txt_create_document_type_guide'].setData('');
        $('#div_create_parentId').attr('style', 'display:none');
        if (documentTypeId == 0) {
            $('#header_create_document_type').html('Tạo mới danh mục chứng từ');
            //Load d/s chứng từ
            LoadChangeParent(0, 0);

        }
        else {
            $('#header_create_document_type').html('Cập nhập danh mục chứng từ <b>' + docmentTypeName + '</b>');
            $.ajax({
                type: "Get",
                url: "/DocumentType/GetById",
                data: { Id: documentTypeId },
                success: function (data) {
                    if (data.status == 1) {
                        //thành công
                        $('#sl_create_productId').val(data.data.productId).change();
                        $('#sl_typeOwnerShip').val(data.data.typeOwnerShip).change();
                        //if (data.data.objectRuleType != null) {
                        //    var ruleArr = [];
                        //    $.each(data.data.objectRuleType.split(","), function (i, e) {
                        //        ruleArr.push(e);
                        //    });
                        //    $('#sl_workRuleType').val(ruleArr).trigger('change');
                        //}

                        //$('#Version').val(data.data.version).change();
                        $('#CreatedTime').val(data.data.createdTime).change();
                        //$('#sl_workRuleType').val(data.data.workRuleType).change();
                        $('#txt_create_document_type_name').val(data.data.name);
                        $('#txt_create_document_type_number_image').val(data.data.numberImage);
                        $("input[name=IsEnable][value=" + data.data.isEnable + "]").prop('checked', true).change();
                        //$("input[name=MobileUpload][value=" + data.data.mobileUpload + "]").attr('checked', 'checked');
                        CKEDITOR.instances['txt_create_document_type_guide'].setData(data.data.guide);
                        if (data.data.parentId != null && data.data.parentId > 0) {
                            $("input[name=Category][value=1]").prop('checked', true).change();
                            $('#div_create_parentId').removeAttr('style', 'display:none');
                            LoadChangeParent(data.data.productId, data.data.parentId);
                        }
                    }
                    else {
                        //lỗi
                        App.ShowError(data.message);
                    }
                },
                traditional: true
            });
        }
        $('#modal_create_document_type').modal('show');
    }

    var ChangeCategory = function (category) {
        if (category == 1)
            $('#div_create_parentId').removeAttr('style', 'display:none');
        else
            $('#div_create_parentId').attr('style', 'display:none');

    }

    var AddOrUpdate = function (btn, e) {
        $(btn).attr('disabled', true);
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            rules: {
                //ProductId: {
                //    min: 1
                //},     
                Name: {
                    required: true,
                }
            },
            messages: {
                //ProductId: {
                //    min: "Bạn chưa chọn sản phẩm", 
                //},          
                Name: {
                    required: "Bạn chưa nhập loại chứng từ"
                }
            }
        });
        if (!form.valid()) {
            $(btn).attr('disabled', false);
            return;
        }

        //var ruleType = '';
        //$("#sl_workRuleType option:selected").each(function () {
        //    var $this = $(this);
        //    var selVal = $this.val();
        //    ruleType += ',' + selVal;
        //});
        //ruleType = ruleType.substring(1);
        //document.getElementById("ObjectRuleType").value = ruleType;

        $('#hdd_Guide').val(CKEDITOR.instances['txt_create_document_type_guide'].getData());
        form.ajaxSubmit({
            url: '/DocumentType/AddOrUpdate',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $(btn).attr('disabled', false);
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccessNotLoad(data.message);
                        $('#modal_create_document_type').modal('hide');
                        LoadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    }

    var ShowChangeParent = function (productId) {
        if (productId > 0) {
            $.ajax({
                url: '/DocumentType/GetDocumentTypeParent?ProductId=' + productId,
                type: 'GET',
                dataType: 'json',
                success: function (res) {
                    $('#sl_create_parentId').html('');
                    $('#sl_create_parentId').append('<option value="' + 0 + '">' + 'Chọn danh mục' + '</option>');
                    if (res.status == 1) {
                        for (var i = 0; i < res.data.length; i++) {
                            $('#sl_create_parentId').append('<option value="' + res.data[i].id + '">' + res.data[i].name + '</option>');
                        }
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
                }
            });
        }
    }

    var LoadChangeParent = function (productId, parentId) {
        //if (productId > 0 && parentId > 0) {
        $.ajax({
            url: '/DocumentType/GetDocumentTypeParent?ProductId=' + productId,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                $('#sl_create_parentId').html('');
                $('#sl_create_parentId').append('<option value="' + 0 + '">' + 'Chọn danh mục' + '</option>');
                if (res.status == 1) {
                    for (var i = 0; i < res.data.length; i++) {
                        if (res.data[i].id == parentId)
                            $('#sl_create_parentId').append('<option selected="selected" value="' + res.data[i].id + '">' + res.data[i].name + '</option>');
                        else
                            $('#sl_create_parentId').append('<option value="' + res.data[i].id + '">' + res.data[i].name + '</option>');
                    }
                }
            },
            error: function () {
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
        //}
    }

    return {
        Init: Init,
        LoadData: LoadData,
        LoadInfoDocumentType: LoadInfoDocumentType,
        ChangeCategory: ChangeCategory,
        AddOrUpdate: AddOrUpdate,
        ShowChangeParent: ShowChangeParent,
        LoadChangeParent: LoadChangeParent
    };
}
