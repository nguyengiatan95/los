﻿const ProductDetail = Object.freeze(
    {
        "VayNhanh4tr": 1,
        "KT1_10": 2,
        "KT1_20": 3,
        "KT1_30": 4,
        "KT3_10": 5,
        "KT3_15": 6,
        "KT3_20": 7,
        "KT1_KGT_10": 8,
        "KT1_KGT_15": 9,
        "KT1_KGT_20": 10,
        "KT1_KSHK_4tr": 11,
        "KT1_KGT_4tr": 12,
        "KT3_KGT_10": 13,
        "KT3_KGT_15": 14,
        "KT3_KTN_4tr": 15,
        "KT3_KGT_4tr": 16,
        "KT1_KSHK_10": 17,
        "LO_KT1_KSHK_5tr": 33,
        "KT1_CC_5": 34,
        "KT1_CC_12": 36,
        "KT1_CC_22": 37,
        "KT1_CC_32": 38,
        "KT1_CC_42": 39,
        "KT3_CC_5": 40,
        "KT3_CC_12": 41,
        "KT3_CC_22": 42,
        "KT3_CC_32": 43,
        "LO_KT3_KTN_5tr": 44,
        "LO_KT1_12tr": 45,
        "LO_KT1_KSHK_12tr": 46,
        "KT1_KSHK_12tr": 47,
    });

const TypeOfOwnerShip = Object.freeze(
    {
        "Sohuucanhan": 1,
        "Dongsohuu": 2,
        "Thuocsohuucuahohang": 3,
        "Thue": 4,
        "NhaSoHuu": 6,
        "DungTenBoMe": 7,
        "DungTenVoChong": 8,
        "NhaSoHuuTrungSHK": 9,
        "NhaSoHuuKhongTrungSHK": 10,
        "ThueKT1": 11,
        "ThueKT2": 12,
        "NhaThueCungTinh": 13,
        "KT1": 14,
        "KT3": 15
    })
const ProductCredit = Object.freeze(
    {
        "MotorCreditType_CC": 2,
        "MotorCreditType_KCC": 5,
        "OtoCreditType_CC": 8,
        "MotorCreditType_KGT": 28,
    });
const RateType = Object.freeze(
    {
        "DuNoGiamDan": 13,
        "TatToanCuoiKy": 14
    });
const PlatformType = Object.freeze(
    {
        "LoanCarChild": 120,
        "LendingOnline": 121,
        "LendingOnlineAPP": 122,
    });

const Pipeline = Object.freeze(
    {
        "Pipeline_Test": 31,
    });
const LoanStatus = Object.freeze(
    {
        "INIT": 1,
        "CANCELED": 99,
        "SALEADMIN_LOAN_DISTRIBUTING": 10,
        "TELESALE_ADVICE": 20,
        "HUB_LOAN_DISTRIBUTING": 30,
        "HUB_CHT_LOAN_DISTRIBUTING": 35,
        "APPRAISER_REVIEW": 40,
        "HUB_CHT_APPROVE": 45,
        "BRIEF_APPRAISER_LOAN_DISTRIBUTING": 50,
        "BRIEF_APPRAISER_REVIEW": 55,
        "BRIEF_APPRAISER_APPROVE_PROPOSION": 60,
        "BRIEF_APPRAISER_APPROVE_CANCEL": 65,
        "LENDER_LOAN_DISTRIBUTING": 90,
        "BOD_LOAN_DISTRIBUTING": 70,
        "BOD_APPROVE_CANCEL": 75,
        "COO_APPROVE_CANCEL": 80,
        "DISBURSED": 100,
        "FINISH": 110,
        "WAIT_DISBURSE": 101,
        "AREA_MANAGER_REVIEW": 48,
        "BOD_REVIEW": 72,
        "BRIEF_APPRAISER_APPROVE": 58,
        "WAIT_GET_RULE_INFO_AI": 31,
        "WAIT_APPRAISER_CONFIRM_CUSTOMER": 56,
        "APPROVAL_LEADER_APPROVE_CANCEL": 61,
        "SYSTEM_TELESALE_DISTRIBUTING": 15,
        "WAIT_CUSTOMER_PREPARE_LOAN": 16,
        "WAIT_CCO_APPROVE": 77,
        "WAIT_HUB_EMPLOYEE_PREPARE_LOAN": 32,
        "WAIT_HO_APPRAISER_REVIEW": 36,
    });

const User = Object.freeze(
    {
        "spt_System": 26695,
        "ctv_autocall": 55684,
        "autocall_coldlead": 55694,
        "follow": 55732,
        "linhtq": 41687,
        "spt_thoapt": 55680,
        "spt_huyennt": 51814,
        "spt_linhntn": 55673,
        "spt_phuonglm1": 30853,
        "tvv_test": 26464,
        "qc_phuongnt": 55764,
        "recare": 55797,
        "recare_team": 55842,
        "system_team": 55840,
        "follow_team": 55841,
    });

const NationalCardLength = Object.freeze(
    {
        "CMND_QD": 8,
        "CMND": 9,
        "CCCD": 12
    });

const LimitMoneyProductDetail = Object.freeze(
    {
        "1": 4000000,
        "2": 10000000,
        "3": 20000000,
        "4": 30000000,
        "5": 10000000,
        "6": 15000000,
        "7": 20000000,
        "8": 10000000,
        "9": 15000000,
        "10": 20000000,
        "11": 4000000,
        "12": 4000000,
        "13": 10000000,
        "14": 15000000,
        "15": 4000000,
        "16": 4000000,
        "17": 10000000,
        "33": 5000000,
        "34": 5000000,
        "36": 12000000,
        "37": 22000000,
        "38": 32000000,
        "39": 42000000,
        "40": 5000000,
        "41": 12000000,
        "42": 22000000,
        "43": 32000000,
        "44": 5000000,
        "45": 12000000,
        "46": 12000000,
        "47": 12000000
    });

const StatusTelesales = Object.freeze(
    {
        "NoContact": 1,
        "Consider": 2,
        "WaitPrepareFile": 3,
        "SeeLater": 33
    });

const TypeRemarketing = Object.freeze(
    {
        "IsRemarketing": 1,
        "IsTopUp": 2,
        "IsCanTopup": 3,
        "IsCreateLoanAutocall": 4,
        "IsAff": 5,
        "DebtRevolvingLoan": 6
    });

const EsignState = Object.freeze(
    {
        "READY_TO_SIGN": 0,
        "BORROWER_SIGNED": 1,
        "LENDER_SIGNED": 2
    });

const GroupUser = Object.freeze(
    {
        "Telesale": 2
    });

const TypeReason = Object.freeze(
    {
        "HubCancel": 100
    });

const TypeLoanSupport = Object.freeze(
    {
        "IsCanTopup": 1
    });


const ProvinceSupport = Object.freeze(
    {
        "HaNoi": 1,
        "HCM": 79,
        "BacNinh": 27,
        "HaiDuong": 30,
        "VinhPhuc": 26,
        "BinhDuong": 74,
    });

const ExceptionRoot = Object.freeze(
    {
        "DK_khac_quy_dinh": 1,
        "CT_khac_quy_dinh": 2,
        "Thieu_chung_tu": 3,
        "Vuot_ti_le": 4,
    });

const StatusBase = Object.freeze(
    {
        "Active": 1,
        "Locked": 0,
    });
const FilterTopupDisbursementContract = Object.freeze(
    {
        "Topup": 1,
        "TlsOff": 2,
        "DPD": 3,
        "CloseLoan": 4,
    });

const TeamTelesales = Object.freeze(
    {
        "HN1": 1,
        "HN2": 2,
        "HCM": 3,
        "Other": 4,
        "SupportTeam": 5,
        "TopupXsell": 6,
        "HN3": 7,
        "Xsell1": 8,
        "Xsell2": 9,
        "Xsell3": 10,
    });

const TypeRemarketingLtv = Object.freeze(
    {
        "TopUp": 1,
        "Reborrow": 2
    });

const ActionToolManageTelesaleSupport = Object.freeze(
    {
        "RestoreLoanTopup": 1,
        "CheckAndUpdateLoanTopup": 2
    });

const FormatCallCisco = Object.freeze(
    {
        "Format_1": 711 
    });


