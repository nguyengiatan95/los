﻿var Env = new function () {
	var WEB_API_URL = "http://10.0.75.1:32771/api/v1.0/";
	var LOGIN_ENDPOINT = WEB_API_URL + "security/login";

	return {
		WEB_API_URL: WEB_API_URL,
		LOGIN_ENDPOINT: LOGIN_ENDPOINT
	}
}