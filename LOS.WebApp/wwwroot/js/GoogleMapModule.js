﻿var GoogleMapModule = new function () {
    //var map = null;
    //var geocoder = null;
    var googleApiKey = "";
    var Init = function (key) {
        googleApiKey = key;
    }
    //var InitMap = function (elementGoogleMap, elementAddress, elementLatLng) {
    //    var myLatlng = { lat: 20.993131, lng: 105.807500 };
    //    console.log("InitMap ok");
    //    var element = elementGoogleMap.replace("#", "");
    //    map = new google.maps.Map(document.getElementById(element), {
    //        center: myLatlng,
    //        zoom: 18
    //    });
    //    var activeInfoWindow; 
    //    map.addListener('click', function (mapsMouseEvent) {
    //        var infoWindow = new google.maps.InfoWindow(
    //            { content: 'Click the map to get Lat/Lng!', position: myLatlng });
    //        // Close the current InfoWindow.
    //        infoWindow.close();
    //        debugger
    //        if (activeInfoWindow) { activeInfoWindow.close(); }
    //        // Create a new InfoWindow.
    //        infoWindow = new google.maps.InfoWindow({ position: mapsMouseEvent.latLng });
    //        infoWindow.setContent(mapsMouseEvent.latLng.toString());
    //        infoWindow.open(map);
    //        var latLng = mapsMouseEvent.latLng.toString().replace("(", "").replace(")", "")
    //        $("#latLngId").html(latLng);
    //        $(elementLatLng).val(latLng);
    //        $(elementAddress).val(latLng);
    //        activeInfoWindow = infoWindow;
    //    });
    //}

    var InitMap = function (elementGoogleMap, elementAddress, elementLatLng) {
        var myLatlng = { lat: 20.993131, lng: 105.807500 };
        console.log("InitMap ok");
        var element = elementGoogleMap.replace("#", "");
        var map = new google.maps.Map(document.getElementById(element), {
            center: myLatlng,
            zoom: 18
        });
        geocoder = new google.maps.Geocoder();
        var activeInfoWindow;
        google.maps.event.addListener(map, 'click', function (event) {
            geocoder.geocode({
                'latLng': event.latLng
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var infoWindow = new google.maps.InfoWindow();
                        // Close the current InfoWindow.
                        infoWindow.close();
                        if (activeInfoWindow) { activeInfoWindow.close(); }
                        // Create a new InfoWindow.
                        infoWindow = new google.maps.InfoWindow({ position: event.latLng });
                        infoWindow.setContent(
                            "<div><strong>" +
                            results[0].formatted_address +
                            "</strong><br>" +
                            "Lat/Lng: " + event.latLng.toString() +
                            "</div>"
                        );
                        infoWindow.open(map);
                        var latLng = event.latLng.toString().replace("(", "").replace(")", "")
                        $(elementLatLng).val(latLng);
                        $(elementAddress).val(results[0].formatted_address);
                        activeInfoWindow = infoWindow;
                    }
                }
            });
        });
        return map;
    }

    var SearchLocation = function (elementGoogleMap, elementAddress, elementLatLng) {
        var address = $(elementAddress).val();
        if (address != null && address.trim() != "") {
           var map = InitMap(elementGoogleMap, elementAddress, elementLatLng);
            $(elementGoogleMap).show();
            FindLocation(address, elementLatLng, map);
        } else {
            App.ShowErrorNotLoad('Vui lòng nhập địa chỉ để tìm kiếm trên bản đồ');
        }
    }

    var FindLocation = function (adress, elementLatLng, map) {
        if (typeof adress != "undefined" && adress != null && adress != "") {
            var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + adress + "&key=" + googleApiKey;
            $.ajax({
                dataType: "json",
                url: url,
                success: function (response) {
                    GoogleMapModule.FindLocationResponce(response, elementLatLng, map);
                }
            });
        }
    }
    var FindLocationResponce = function (data, elementLatLng, map) {
        var isFalse = true;
        if (typeof data != "undefined" && data != null && typeof data.results != "undefined" && data.results != null && data.results.length > 0) {
            var result = data.results[0];
            if (typeof result != "undefined" && result != null) {
                var geometry = result.geometry;
                if (typeof geometry != "undefined" && geometry != null && typeof geometry.location != "undefined" && geometry.location != null) {
                    var location = geometry.location;
                    if (typeof location != "undefined" && location != null && typeof location.lat != "undefined" && location.lat != null && typeof location.lng != "undefined" && location.lng != null) {
                        var lat = location.lat;
                        var lng = location.lng;
                        if (lat > 0 && lng > 0) {
                            $(elementLatLng).val(lat + ", " + lng);
                            isFalse = false;
                            GoogleMapModule.SetcenterMap(map, lat, lng);
                        }
                    }
                }
            }
        }
        if (isFalse) {
            $("#latLngId").html("không có dữ liệu!");
        }
    }
    var SetcenterMap = function (map, lat, lng) {
        if (typeof lat != "undefined" && lat != null && lat != "" && typeof lng != "undefined" && lng != null && lng != "") {
            map.setCenter({ lat: lat, lng: lng });
            map.setZoom(18);
            new google.maps.Marker({ position: { lat: lat, lng: lng }, map: map });
        }
    }

    var SearchLocationInit = function (elementGoogleMap, elementAddress, elementLatLng) {
        //debugger;
        if ($(elementAddress).val() == null || $(elementAddress).val() == "")
            $(elementAddress).val($('#txtHomeAddress').val());
        var address = $(elementAddress).val();
         
        if (address != null && address.trim() != "") {
            var map = InitMap(elementGoogleMap, elementAddress, elementLatLng);
            $(elementGoogleMap).show();
            FindLocation(address, elementLatLng, map);
        } else {
            App.ShowErrorNotLoad('Vui lòng nhập địa chỉ để tìm kiếm trên bản đồ');
        }
    }
    var SearchLocationWorkAddress = function (elementGoogleMap, elementAddress, elementLatLng) {
        //debugger;
        if ($(elementAddress).val() == null || $(elementAddress).val() == "")
            $(elementAddress).val($('#txtWorkAddress').val());
        var address = $(elementAddress).val();

        if (address != null && address.trim() != "") {
            var map = InitMap(elementGoogleMap, elementAddress, elementLatLng);
            $(elementGoogleMap).show();
            FindLocation(address, elementLatLng, map);
        } else {
            App.ShowErrorNotLoad('Vui lòng nhập địa chỉ để tìm kiếm trên bản đồ');
        }
    }
    var HideGoogleMapAddressNew = function (elementHide) {
        $(elementHide).html('');
        $(elementHide).css("display", "none");
    }
    return {
        Init: Init,
        InitMap: InitMap,
        SetcenterMap: SetcenterMap,
        FindLocation: FindLocation,
        FindLocationResponce: FindLocationResponce,
        SearchLocation: SearchLocation,
        SearchLocationInit: SearchLocationInit,
        HideGoogleMapAddressNew: HideGoogleMapAddressNew,
        SearchLocationWorkAddress: SearchLocationWorkAddress
    };
}