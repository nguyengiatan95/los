﻿var GroupModule = new function () {

    var InitDataTable = function (objQuery) {
        var datatable = $('#tbManageGroup').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Group/LoadData',
                        params: {
                            query: objQuery
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'groupName',
                    title: 'Nhóm người dùng',
                    sortable: false
                },
                {
                    field: 'description',
                    title: 'Mô tả',
                    sortable: false
                },
                {
                    field: 'createdAt',
                    title: 'Thời gian tạo',
                    sortable: false,
                    template: function (row, index, datatable) {
                        return formattedDateHourMinutes(row.createdAt)
                    }
                },
                {
                    field: 'defaultPath',
                    title: 'Đường dẫn',
                    sortable: false
                }
                , {
                    field: 'Action',
                    title: 'Action',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return '\
							    <a href="/group?id='+ row.groupId + '" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" >\
                                    <i class="la la-edit"></i></a>\
                                <a href="javascript:;" title="Phân quyền dữ liệu" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only"  onclick="GroupModule.ShowPermissionData('+ row.groupId + ')" >\
                                    <i class="fa fa-gears"></i></a>\
                                <a href="javascript:;" title="Phân quyền API" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only"  onclick="GroupModule.ShowPermissionApi(\''+ row.groupId + '\',\'' + row.groupName + '\')" >\
                                    <i class="fa fa-desktop"></i></a>\
	                            <a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" onclick="GroupModule.DeleteGroup('+ row.groupId + ',\'' + row.groupName + '\')" >\
                                    <i class="la la-times"></i></a>';
                    }
                }
            ]
        });
    }

    var LoadTable = function () {
        var objQuery = {
            filterName: $('#filterName').val().trim(),
        }
        InitDataTable(objQuery);
    }

    var Init = function () {
        $("#filterName").on('keydown', function (e) {
            if (e.which == 13) {
                LoadTable();
            }
        });
        LoadTable();
    };

    var AddGroup = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            rules: {
                GroupName: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                Description: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                }
            },
            messages: {
                GroupName: {
                    required: "Bạn chưa nhập nhóm tài khoản",
                },
                Description: {
                    required: "Bạn chưa nhập mô tả",
                }
            }
        });

        if (!form.valid()) {
            return;
        }
        form.ajaxSubmit({
            url: '/Group/CreateGroup',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccessRedirect(data.message, '/group-manager.html');
                    }
                    else {
                        App.ShowError(data.message);
                    }
                }
                else {
                    App.ShowError(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };

    var ShowModel = function (id) {
        $('#modelGroup').html('');
        $.ajax({
            type: "POST",
            url: "/Group/CreateGroupModalPartial",
            data: { Id: id },
            success: function (data) {
                $('#modelGroup').html(data);
                $('#m_modal_Group').modal('show');
            },
            traditional: true
        });
    }

    var DeleteGroup = function (id, username) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn  xóa tài khoản <b>" + username + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Group/DeleteGroup",
                    data: { Id: id },
                    success: function (data) {
                        if (data.status == 1) {
                            location.reload();
                            swal(
                                'Xóa nhóm tài khoản thành công!'
                            )

                        } else {
                            App.ShowError(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var GetData = function (id) {
        $.ajax({
            async: true,
            type: "POST",
            url: '/Group/GetMenuPermisson',
            data: { Id: id },
            beforeSend: function () {
                mApp.block('#m_tree_3', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'success',
                    message: 'Please wait...'
                });
            },
            dataType: "json",
            success: function (data) {
                loadData(data.data);
                mApp.unblock('#m_tree_3');
            }
        });
    }

    var GetActionPermisson = function (id) {
        $.ajax({
            async: true,
            type: "POST",
            url: '/Group/GetActionPermisson',
            data: { Id: id },
            beforeSend: function () {
                mApp.block('#boxActionPermisson', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'success',
                    message: 'Please wait...'
                });
            },
            dataType: "text",
            success: function (data) {
                mApp.unblock('#boxActionPermisson');
                $('#boxActionPermisson').html(data);

            }
        });
    }

    var loadData = function (jsondata) {
        $("#m_tree_3").jstree('destroy');
        $('#m_tree_3').on('changed.jstree', function (e, data) {

            var arr = [];
            var nodes = $('#m_tree_3').jstree('get_selected', true);
            $.each(nodes, function (index, item) {
                arr.push(item.id);
                arr.push(item.parent);
            });
            var removeItem = "#";
            arr = jQuery.grep(arr, function (value) {
                return value != removeItem;
            });
            var unique = arr.filter(function (itm, i, arr) {
                return i == arr.indexOf(itm);
            });
            $('#lstMenu').val(unique);
            console.log($('#lstMenu').val())

        }).jstree({
            'plugins': ["wholerow", "checkbox", "types"],
            'core': {
                "check_callback": false,
                "themes": {
                    "responsive": false
                },
                'data': jsondata
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder m--font-success"
                },
                "file": {
                    "icon": "fa fa-file  m--font-warning"
                }
            },
        });
    }

    var ShowPermissionData = function (id) {
        $('#bodyPermissionData').html('');
        $.ajax({
            type: "GET",
            url: "/Group/GetPermissionData",
            data: { Id: id },
            success: function (data) {
                $('#bodyPermissionData').html(data);
                $('#modalPermissionData').modal('show');
                App.LibraryMoney();
            },
            traditional: true
        });
    }

    var ShowPermissionApi = function (groupId, groupName) {
        $('#bodyPermissionApiGroup').html('');
        $.ajax({
            type: "GET",
            url: "/Group/GetPermissionApi",
            data: { groupId: groupId },
            success: function (data) {
                $('#bodyPermissionApiGroup').html(data);
                $('#hdd_Permission_Api_GroupId').val(groupId);
                $('#_groupName').text(groupName);
                $('#modalPermissionApiGroup').modal('show');
                App.LibraryMoney();
            },
            traditional: true
        });

    }

    var SaveActionPermissonApi = function (btn) {
        var form = $(btn).closest('form');
        $(btn).addClass('m-loader m-loader--right m-loader--light');
        form.ajaxSubmit({
            url: '/Group/SaveActionPermissonApi',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $(btn).removeClass('m-loader m-loader--right m-loader--light');
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    return {
        Init: Init,
        AddGroup: AddGroup,
        ShowModel: ShowModel,
        DeleteGroup: DeleteGroup,
        GetData: GetData,
        ShowPermissionData: ShowPermissionData,
        GetActionPermisson: GetActionPermisson,
        LoadTable: LoadTable,
        ShowPermissionApi: ShowPermissionApi,
        SaveActionPermissonApi: SaveActionPermissonApi
    };
}
