﻿var AddDeviceModuleJS = new function () {
    var ReGetDeviceStatus = function (loanbriefId) {
        if (loanbriefId > 0) {
            $.ajax({
                type: "POST",
                url: "/LoanBrief/ReGetDeviceStatus",
                beforeSend: function () {
                   
                },
                data: { loanbriefId: loanbriefId },
                success: function (data) {
                    if (data.isSuccess == true) {
                        //chưa kích hoạt
                        if (data.data == 1) {
                            $('#txtLabelStatus').attr('class', 'm-badge m-badge--warning m-badge--wide');
                        }
                        //kích hoạt thành công
                        else if (data.data == 2) {
                            $('#txtLabelStatus').attr('class', 'm-badge m-badge--info m-badge--wide');
                            $('#btnRegetDevice').remove();
                        }
                            //kích hoạt lỗi
                        else {
                            $('#txtLabelStatus').attr('class', 'm-badge m-badge--danger m-badge--wide');
                            //if (data.data == -1) {
                            //    $('#btnRegetDevice').remove();
                            //}
                        }
                        $('#txtLabelStatus').text(data.message);
                    } else {
                        App.ShowErrorNotLoad(data.message);
                        return;
                    }
                }
            });
        }
    };

    var ReActiveDevice = function (loanbriefId) {
        if (loanbriefId > 0) {
            $.ajax({
                type: "POST",
                url: "/LoanBrief/ReActiveDevice",
                beforeSend: function () {

                },
                data: { loanbriefId: loanbriefId },
                success: function (data) {
                    if (data.isSuccess == true) {
                        //chưa kích hoạt
                        $('#btnReActiveDevice').remove();
                        if (data.data == 1) {
                            $('#txtLabelStatus').attr('class', 'm-badge m-badge--warning m-badge--wide');
                        }
                        //kích hoạt lỗi
                        else {
                            $('#txtLabelStatus').attr('class', 'm-badge m-badge--danger m-badge--wide');
                        }
                        //var html = `<a href="javascript:;" id="btnRegetDevice" style="border-radius: 15px;" title="Trạng thái kích hoạt" onclick="AddDeviceModuleJS.ReGetDeviceStatus(${loanbriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only"><i class="fa fa-refresh"></i></a>`
                        //$('#txtLabelStatus').parents('label').append(html)
                        $('#txtLabelStatus').text(data.message);
                    } else {
                        App.ShowErrorNotLoad(data.message);
                        return;
                    }
                }
            });
        }
    };
    

    var SubmitForm = function (e) {
        e.preventDefault();
        var form = $('#formAddDevice');
        form.validate({
            ignore: [],
            rules: {
                'DeviceId': {
                    required: true
                }
            },
            messages: {
                'DeviceId': {
                    required: "Vui lòng nhập imei thiết bị",
                    class: "has-danger"
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        $('#btnSubmit').attr('disabled', true);
        form.ajaxSubmit({
            url: '/LoanBrief/AddCodeDevice',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.isSuccess == 1) {
                        $('#modalGlobal').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        App.ReloadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowError(data.message);
                }
                $('#btnSubmit').attr('disabled', false);
            }
        });
    }

    return {
        SubmitForm: SubmitForm,
        ReGetDeviceStatus: ReGetDeviceStatus,
        ReActiveDevice: ReActiveDevice
    }
    
}