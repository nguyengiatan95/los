﻿var DistributingLoanModuleJS = new function () {
    var Init = function () {
        $("#sl_StaffHub").select2({
            placeholder: "Chọn nhân viên đi thẩm định",
            width: '100%'
        });
        $("#sl_PlaceOfAppraisal").select2({
            placeholder: "Chọn địa điểm thẩm định",
            width: '100%'
        });
    }

    var SubmitForm = function (e) {
        e.preventDefault();
        var form = $('#formDistributingLoanBrief');
        form.validate({
            ignore: [],
            rules: {
                'StaffHub': {
                    required: true
                },
                'PlaceOfAppraisal': {
                    required: true
                }
            },
            messages: {
                'StaffHub': {
                    required: "Vui lòng chọn nhân viên thẩm định",
                    class: "has-danger"
                },
                'PlaceOfAppraisal': {
                    required: "Vui lòng chọn nơi thẩm định",
                    class: "has-danger"
                }
            },
            errorPlacement: function (error, element) {
                if (element.hasClass('m-select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                    error.insertAfter(element.parent().parent());
                }
                else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.appendTo(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        $('#btnSubmit').attr('disabled', true);
        form.ajaxSubmit({
            url: '/LoanBrief/DistributingLoanOfHub',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.isSuccess == 1) {
                        $('#modelDistributingLoanOfHub').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        App.ReloadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowError(data.message);
                }
                $('#btnSubmit').attr('disabled', false);
            }
        });
    }

    return {
        Init: Init,
        SubmitForm: SubmitForm
    }
    
}