﻿var ListLoanDebtModule = new function () {
    var InitDataTable = function (objQuery) {
        var datatable = $('#tbLoanDebt').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/Hub/GetListLoanDebt',
                        params: {
                            query: objQuery
                        },
                    }
                },
                pageSize: 20,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'losData.loanBriefId',
                    title: 'Mã HD',
                    sortable: false
                },
                {
                    field: 'lmsData.maTc',
                    title: 'Mã TC',
                    sortable: false
                },
                {
                    field: 'losData.fullName',
                    title: 'Tên KH',
                    sortable: false
                },
                //{
                //    field: 'address',
                //    title: 'Đỉa chỉ nhà',
                //    sortable: false
                //},
                {
                    field: 'address',
                    title: 'Quận/ Huyện',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.losData.district != null)
                            text += "<p class='district'>" + row.losData.district.name + "</p>";
                        if (row.losData.province != null) {
                            text += "<p class='province'>" + row.losData.province.name + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'lmsData.fromDate',
                    title: 'Ngày giải ngân',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.lmsData.fromDate);
                        return date.format("DD/MM/YYYY");
                    }
                },
                {
                    field: 'lmsData.toDate',
                    title: 'Ngày kết thúc',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.lmsData.toDate);
                        return date.format("DD/MM/YYYY");
                    }
                },
                {
                    field: 'losData.loanProduct.name',
                    title: 'Sản phẩm vay',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = '';
                        if (row.losData.loanProduct != null)
                            html = `${row.losData.loanProduct.name}`;
                        return html;
                    }
                },
                {
                    field: 'losData.loanAmount',
                    title: 'Số tiền vay',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {                      
                        return App.FormatCurrency(row.losData.loanAmount);
                    }
                },
                {
                    field: 'lmsData.nextDate',
                    title: 'Ngày thanh toán',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.lmsData.nextDate);
                        return date.format("DD/MM/YYYY");
                    }
                },
                {
                    field: 'lmsData.totalPayInterestFees ',
                    title: 'Tiền phải thanh toán',
                    sortable: false,
                    template: function (row) {
                        return App.FormatCurrency(row.lmsData.totalPayInterestFees)
                    }
                },
                {
                    field: 'lmsData.numberDayOverdue',
                    title: 'Số ngày quá hạn',
                    sortable: false
                },
                {
                    field: 'lmsData.totalMoneyCurrent',
                    title: 'Dư nợ gốc còn lại',
                    sortable: false,
                    template: function (row) {
                        return App.FormatCurrency(row.lmsData.totalMoneyCurrent)
                    }
                },
                {
                    field: 'losData.hub.name',
                    title: 'Tên HUB',
                    sortable: false
                },
                {
                    field: 'losData.userHub.fullName',
                    title: 'Tên CVKD',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = "";
                        if (row.losData.userHub != null) {
                            html += `<span>${row.losData.userHub.fullName}</span>`;;
                        }
                        return html;
                    }
                },
            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            hubId: $('#sl_Hub').val()
        }
        InitDataTable(objQuery);
    }

    var Init = function () {
        $("#sl_Hub").select2({
            //dropdownParent: $("#Modal"),
            placeholder: "Vui lòng chọn Hub",
            width: '100%'
        });
        $("#btnSearch").click(function () {
            LoadData();
        });
    }


    return {
        Init: Init,
        LoadData: LoadData
    }
}();