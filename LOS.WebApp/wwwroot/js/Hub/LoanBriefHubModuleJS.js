﻿var LoanBriefHubModuleJS = new function () {
    var _groupGlobal;
    var _typeServiceCall;
    var count = 0;
    var InitData = function (group, typeServiceCall) {
        _typeServiceCall = typeServiceCall;
        _groupGlobal = group;
    }
    var InitDataTable = function (objQuery) {
        var datatable = $('#dtLoanBrief').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/LoanBrief/GetDataHub',
                        params: {
                            query: objQuery
                        }
                    }

                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    title: '',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1)
                            return html;
                        //chỉ chuyển đơn ở TH chờ CHT chia đơn
                        if (row.status == HUB_CHT_LOAN_DISTRIBUTING || row.status == APPRAISER_REVIEW) {
                            html += '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" value="' + row.loanBriefId + '" name="checkbox"><span></span></label></span>';
                        }
                        return html;
                    }
                },
                {
                    field: 'stt',
                    title: 'STT',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        var html = index + record;
                        if (row.utmSource != null && row.utmSource != "") {
                            html += "<br /><span style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);'>" + row.utmSource + "</span>";
                        }
                        if (row.utmCampaign != null && row.utmCampaign != "") {
                            html += "<br /><span style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);'>" + row.utmCampaign + "</span>";
                        }
                        return html;
                    }
                },

                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    width: 100,
                    textAlign: 'center',
                    //sortable: true,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += '<span class="loanBriefId">HĐ-' + row.loanBriefId + '</span>';
                        if (row.platformType == PlatformType.LendingOnline || row.platformType == PlatformType.LendingOnlineAPP) {
                            html += ' <br /><span class="item-desciption">(LendingOnline)</span>';
                        }
                        if (row.isReborrow) {
                            html += `<br /><span class="item-desciption">(Tái vay)</span>`;
                        }
                        if (row.isLocate && row.deviceId != null && row.deviceId != "") {
                            html += '<br /><span class="item-desciption">(' + row.deviceId + ')</span>';
                        }
                        //đơn topup
                        if (row.typeRemarketing == TypeRemarketing.IsTopUp) {
                            html += '<br /><span class="item-desciption">(Topup)</span>';
                        }
                        //Đơn remarketing autocall
                        if (row.typeRemarketing == TypeRemarketing.IsCreateLoanAutocall) {
                            html += '<br /><span class="item-desciption">(Remarketing Autocall)</span>';
                        }
                        //Đơn Tái cấu trúc nợ
                        if (row.typeRemarketing == TypeRemarketing.DebtRevolvingLoan) {
                            html += '<br /><span class="item-desciption">(Tái cấu trúc nợ)</span>';
                        }

                        //Check thời xử lý đơn vay
                        if (row.timeProcessingLoan != null) {
                            //có phản hồi nhưng trên 15 phút
                            if (row.timeProcessingLoan.isFeedBack == true && row.timeProcessingLoan.timeProcessing > 15)
                                html += '<br /><span style="color: red; font-size: 11px;">Phản hồi muộn ' + row.timeProcessingLoan.timeProcessing + ' phút</span>';
                            else if (row.timeProcessingLoan.isFeedBack == false && row.timeProcessingLoan.timeProcessing > 15)
                                html += '<br /><span style="color: red; font-size: 11px;">Chưa phản hồi</span>';
                            else if (row.timeProcessingLoan.isFeedBack == false && row.timeProcessingLoan.timeProcessing <= 15) {
                                html += '<br /><span style="color: #fdc502; font-size: 11px;" id="_timeProcessing_' + row.loanBriefId + '"></span>';
                                var minutes = parseInt(15 - row.timeProcessingLoan.timeProcessing);
                                if (minutes > 0) {
                                    var _spanId = "#_timeProcessing_" + row.loanBriefId;
                                    CommonModalJS.CountDown(minutes * 60, _spanId);
                                }
                            }
                        }
                        return html;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    textAlign: 'center',
                    //width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = '<a class="fullname" id="linkName" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>';
                        if (row.isExceptions == 1) {
                            html += `<br /> <a href="javascript:;" title="Bỏ nhãn trình ngoại lệ" onclick="CommonModalJS.CancelExceptions(${row.loanBriefId})">
                                        <span class="item-desciption">Đơn trình ngoại lệ</span></a>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Quận/ Huyện',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.district != null)
                            text += "<p class='district'>" + row.district.name + "</p>";
                        if (row.province != null) {
                            text += "<p class='province'>" + row.province.name + "</p>";
                        }
                        if (row.currentPipelineId != null && row.currentPipelineId == Pipeline['Pipeline_Test'])
                            text += '<span class="item-desciption">Luồng thử nghiệm</span>'
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    width: 100,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay <br /> VNĐ',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = `<span class="money">${App.FormatCurrency(row.loanAmount)}</span>`
                        if (row.loanProduct != null)
                            html += `<span class="item-desciption">${row.loanProduct.name}</span>`;
                        if (row.isLocate) {
                            html += `<span class="m-badge m-badge--success m-badge--wide font-size-11">Xe lắp định vị</span>`;
                            if (App.ArrStatusOfDevice[row.deviceStatus] != null) {
                                html += '<span class="item-desciption">(' + App.ArrStatusOfDevice[row.deviceStatus].title + ')</span>';
                            }
                        }
                        if (row.isTrackingLocation) {
                            html += `<span class="item-desciption">(KH chia sẻ vị trí)</span>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'loanTime',
                    title: 'Thời gian vay',
                    textAlign: 'center',
                    width: 100,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (row.loanTime != null && row.loanTime > 0)
                            html += `${row.loanTime} tháng`;
                        if (row.hubPushAt != null) {
                            var date = moment(row.hubPushAt);
                            html += "</br>" + date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                        }
                        return html;
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    //width: 200,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = "";
                        if (row.status == APPRAISER_REVIEW) {
                            if (row.loanStatusDetail != null) {
                                html += '<span class="m-badge m-badge--success m-badge--wide">' + App.ArrLoanStatusDetail[row.loanStatusDetail].title + '</span>';
                                if (App.ArrLoanStatusDetail[row.loanStatusDetailChild] != null) {
                                    html += '<span class="detail-status-telesales">' + App.ArrLoanStatusDetail[row.loanStatusDetailChild].title + '</span>';

                                }
                                if (App.ArrPipelineState[row.pipelineState] != null && LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1) {
                                    html += `<span class="item-desciption-11">${App.ArrPipelineState[row.pipelineState].title}</span>`;
                                }
                            } else {
                                html += '<span class="m-badge ' + App.ArrLoanStatusDetail[40].class + ' m-badge--wide">' + App.ArrLoanStatusDetail[40].title + '</span>';
                            }
                        } else {
                            if (App.ArrLoanStatus[row.status] != null) {
                                html = '<span class="m-badge ' + App.ArrLoanStatus[row.status].class + ' m-badge--wide">' + App.ArrLoanStatus[row.status].title + '</span>';
                                if (App.ArrPipelineState[row.pipelineState] != null && LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1) {
                                    html += `<span class="item-desciption-11">${App.ArrPipelineState[row.pipelineState].title}</span>`;
                                }
                                if (row.status == BRIEF_APPRAISER_REVIEW) {
                                    if (row.approverStatusDetail != null)
                                        html += '<span style="color: red;font-style: italic;" class="m-list-timeline__text">' + App.ArrApproveStatusDetail[row.approverStatusDetail].title + '</span>';
                                    else
                                        html += '<span style="color: red;font-style: italic;" class="m-list-timeline__text">' + App.ArrApproveStatusDetail[72].title + '</span>';
                                }
                            } else {
                                html = 'Không xác định';
                            }
                        }

                        if (row.platformType == PlatformType.LendingOnline || row.platformType == PlatformType.LendingOnlineAPP) {
                            if (row.step > 0) {
                                html += `<span class="item-desciption-11">(step: ${row.step}/7)</span>`;
                            }
                        }
                        return html;
                    }
                },
                {
                    field: 'call',
                    title: 'Gọi',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (_typeServiceCall == 1) {
                            html = '<a href="javascript:;" onclick="CareSoftModule.CallCareSoft(' + row.loanBriefId + ')" class="box-call">\
                                    <i class="fa fa-phone item-icon-phone" ></i></a>';
                        } else if (_typeServiceCall == 2) {// gọi qua metech
                            html = `<a href="javascript:;" onclick="finesseC2CModule.ClickToCall('${FormatCallCisco.Format_1 + row.phone}',${row.loanBriefId})" class="box-call">
                                    <i class="fa fa-phone item-icon-phone" ></i></a>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'userHub.FullName',
                    title: 'Người phụ trách',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = "";
                        if (row.userHub != null) {
                            html += `<span>${row.userHub.fullName}</span>`;;
                        }
                        if (row.userFieldHo != null) {
                            html += `<br /><span>${row.userFieldHo.fullName}(HO)</span>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'action',
                    title: 'Hành động',
                    width: 160,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1)
                            return html;
                        if (row.isEdit) {
                            html += `<a href="javascript:;" title="Cập nhật đơn vay" onclick="InitLoanBrief.Create(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="la la-edit"></i></a>`;
                        }
                        if (row.isScript) {
                            html += `<a href="javascript:;" title="Kịch bản đơn vay" onclick="InitLoanBrief.ScriptTelesale(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-book"></i></a>`;
                        }
                        if (row.isLocate) {
                            html += `<a href="javascript:;" title="Thiết bị định vị" onclick="CommonModalJS.InitTrackDevice(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-location-arrow"></i></a>`;
                        }
                        if (row.isPush) {
                            if (row.status == HUB_CHT_LOAN_DISTRIBUTING) {
                                html += `<a href="javascript:;" title="Phân phối đơn vay cho nhân viên" onclick="LoanBriefHubModuleJS.DistributingLoanOfHub(${row.loanBriefId}, LoanBriefHubModuleJS.LoadData)" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-check"></i></a>`;
                            } else {
                                html += `<a href="javascript:;" title="Đẩy đơn vay" onclick="CommonModalJS.ConfirmLoanBrief(${row.loanBriefId}, LoanBriefHubModuleJS.LoadData)" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-check"></i></a>`;
                            }
                        }
                        //chia đơn cho nhân viên hoặc chuyển hình thức thẩm định ở trạng thái chờ thẩm định
                        if (row.status == APPRAISER_REVIEW) {
                            html += `<a data-toggle="modal" title="Chuyển nhân viên thẩm định" onclick="LoanBriefHubModuleJS.HubEmployeeChange(${row.loanBriefId}, LoanBriefHubModuleJS.LoadData)" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-exchange"></i></a>`;
                        }
                        if (row.status == LoanStatus.HUB_CHT_APPROVE) {
                            html += `<a data-toggle="modal" title="Đề xuất ngoại lệ" onclick="LoanBriefHubModuleJS.ProposeExceptions(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-warning"></i></a>`;
                        }
                        if (row.isBack) {
                            html += `<a href="javascript:;" title="Trả lại đơn vay" onclick="CommonModalJS.ManagerHubBackInit(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-undo"></i></a>`;
                        }
                        html += `<a href="javascript:;" title="Nhật ký khoản vay" onclick="CommonModalJS.InitDiaryModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							             <i class="fa fa-wpforms"></i></a>`;
                        if (row.isUpload) {
                            html += `<a href="javascript:;" title="Upload chứng từ" onclick="CommonModalJS.InitUploadModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-upload"></i></a>`;
                        }
                        if (row.isCancel) {
                            html += `<a href="javascript:;" title="Hủy đơn vay" onclick="CommonModalJS.InitCancelModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-trash"></i></a>`;
                        }
                        if (_groupGlobal == GROUP_MANAGER_HUB || _groupGlobal == GROUP_BOD) {
                            html += `<a data-toggle="modal" title="File ghi âm" onclick="CommonModalJS.ViewRecordModel('#modalRecording',${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-headphones"></i></a>`;
                        }

                        if (row.isTrackingLocation && row.pickTLS) {
                            html += `<a href="javascript:;" title="Lấy thông tin location Sim" onclick="CommonModalJS.ReGetLocationSim(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-globe"></i></a>`;
                        }
                        if (row.productId == ProductCredit.OtoCreditType_CC
                            && (row.status == APPRAISER_REVIEW || row.status == HUB_CHT_LOAN_DISTRIBUTING || row.status == HUB_CHT_APPROVE)
                            && (row.reMarketingLoanBriefId == null || row.reMarketingLoanBriefId == row.loanBriefId || row.typeRemarketing == TypeRemarketing.IsTopUp)) {
                            html += `<a href="javascript:;" title="Tạo đơn con" onclick="CommonModalJS.CreateLoanCarChild(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-plus-circle"></i></a>`;
                        }

                        //if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                        //    html += `<div class="dropdown dropup" style="position: absolute !important;">
                        //            <a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" data-toggle="dropdown">
                        //                <i class="fa fa-print"></i>                    
                        //            </a>						  	
                        //            <div class="dropdown-menu dropdown-menu-right">	
                        //                <a class="dropdown-item" href="#" onclick="PrintModel.ContractFinancialAgreement(${row.loanBriefId})">
                        //                     In thỏa thuận tài chính 3 bên
                        //                </a>					
                        //                <a class="dropdown-item" href="#" onclick="PrintModel.CommittedPaper(${row.loanBriefId})">
                        //                     In giấy cam kết XMKCC
                        //                </a>						    	
                        //                <a class="dropdown-item" href="#" onclick="PrintModel.AddendumAgreementPledgeOfProperty(${row.loanBriefId})">
                        //                    In phụ lục thỏa thuận cầm cố tài sản
                        //                </a>						  	
                        //            </div>						
                        //        </div>`;
                        //}
                        if (row.esignState == EsignState.BORROWER_SIGNED) {
                            html += `<a href="javascript:;" title="Ký lại" onclick="CommonModalJS.ReEsign(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
                                        <i class="fa fa-unlock-alt"></i></a>`;
                        }
                        if (row.status == LoanStatus.HUB_CHT_APPROVE) {
                            html += `<a data-toggle="modal" title="Kiểm tra hồ sơ" onclick="CommonModalJS.CompareModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-retweet"></i></a>`;
                        }
                        if (row.isChangePhone) {
                            html += `<a data-toggle="modal" title="Thay đổi số điện thoại" onclick="CommonModalJS.ChangePhoneModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-expand"></i></a>`;
                        }
                        return html;
                    }
                }
            ]
        });
    }
    var LoadData = function () {
        var objQuery = {
            LoanbriefId: $('#filterLoanbriefId').val().replace('HĐ-', '').trim(),
            SearchName: $('#filterSearch').val().trim().toLowerCase(),
            HubEmployeeId: $('#filterHubEmployee').val(),
            LoanStatusDetail: $('#filterStatus option:selected').attr('attr-detail-status'),
            LoanStatus: $('#filterStatus').val()
        }
        if ($('#filterHubEmployee').val() != undefined)
            objQuery.hubEmployee = $('#filterHubEmployee').val();
        InitDataTable(objQuery);
    }
    var Init = function () {
        $("#filterStatus").select2({
            placeholder: "Trạng thái",
            width: '100%'
        });

        $("#filterHubEmployee").select2({
            placeholder: "Nhân viên",
            width: '100%'
        });

        $("#sl_ListHub").select2({
            placeholder: "Chọn hub",
            width: '100%'
        });

        LoadData();

        $("#filterLoanbriefId, #filterSearch").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });

        $('#filterStatus, #filterHubEmployee').on('change', function () {
            LoadData();
            return false;
        });

        $("#btnSearch").on('click', function (e) {
            LoadData();
            return false;
        });
    }
    var TranferLoanbrief = function () {
        var hubId = $("#sl_ListHub").val();
        if (hubId > 0) {
            var loanBriefs = [];
            $.each($("input[name='checkbox']:checked"), function () {
                loanBriefs.push($(this).val());
            });
            if (loanBriefs != null && loanBriefs.length > 0) {

                var hubName = $('#sl_ListHub option:selected').text();
                swal({
                    title: '<b>Cảnh báo</b>',
                    html: "Bạn có chắc chắn muốn chuyển các đơn đã chọn cho Hub: <b>" + hubName + "</b>",
                    showCancelButton: true,
                    confirmButtonText: 'Xác nhận',
                    cancelButtonText: 'Hủy',
                    onOpen: function () {
                        $('.swal2-cancel').width($('.swal2-confirm').width());
                    }
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: "/LoanBrief/TranferLoanForHub",
                            data: {
                                hubId: hubId,
                                loanBriefs: loanBriefs
                            },
                            dataType: 'json',
                            success: function (data) {
                                if (data.isSuccess == 1) {
                                    App.ShowSuccessNotLoad(data.message);
                                    App.ReloadData();
                                } else {
                                    App.ShowErrorNotLoad(data.message);
                                }

                            },
                            traditional: true
                        });

                    }
                });
            } else {
                App.ShowErrorNotLoad('Vui lòng chọn đơn vay cần chuyển hỗ trợ');
                return;
            }
        } else {
            App.ShowErrorNotLoad('Vui lòng chọn Hub cần chuyển hỗ trợ.');
            return;
        }
    }

    var InitSignalr = function (userid) {
        var connection = new signalR.HubConnectionBuilder()
            .withUrl("/hubManagerHub")
            .build();

        connection.start().then(function () {
            console.log("SignalR started..")
            connection.invoke("joinGroup", userid);
        }).catch(function (err) {
            return console.error(err.toString());
        });

        connection.on("Send", function (message) {
            console.log(message);
        });

        connection.on("Notification", function (data) {
            // change value
            var source = "/Files/new-loancredit.mp3";
            var audio = new Audio();
            // no event listener needed here
            audio.src = source;
            audio.autoplay = true;
            audio.loop = true;
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "30000",
                "timeOut": "0",
                "extendedTimeOut": "0",
                "showEasing": "swing",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.info(data.message, "HĐ-" + data.loanbriefId);
            console.log(data);
            count++;
            var newTitle = '(' + count + ') ' + " đơn mới về";
            document.title = newTitle;
            audio.play();
            setTimeout(function () {
                audio.pause();
            }, 14000); //tắt sau 15s

        });
    }

    var DistributingLoanOfHub = function (loanId, func) {
        $('#DistributingLoanOfHub').html('');

        $.ajax({
            url: '/LoanBrief/DistributingLoanOfHub?loanbriefId=' + loanId,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#DistributingLoanOfHub').html(res.html);
                    $('#modelDistributingLoanOfHub').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }

    var HubEmployeeChange = function (loanId, func) {
        $('#DistributingLoanOfHub').html('');

        $.ajax({
            url: '/LoanBriefV2/HubEmployeeChange?loanbriefId=' + loanId,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#DistributingLoanOfHub').html(res.html);
                    $('#modelDistributingLoanOfHub').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }

    var ProposeExceptions = function (loanBriefId) {
        $('#divResult').html('');
        $.ajax({
            type: "GET",
            url: "/Hub/ProposeExceptions",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { loanBriefId: loanBriefId },
            success: function (data) {
                $.unblockUI();
                $('#divResult').html(data);
                $('#Modal').modal('show');
            },
            traditional: true
        });
    };
    var ProposeExceptionsPost = function (formData) {
        $("#btnProp").attr('disabled', true);
        //console.log(formData); return;
        //alert(reasonId); return;
        //console.log(formData); return;
        $.ajax({
            type: "POST",
            url: "/Hub/ProposeExceptionsPost",
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(formData),
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                    $("#Modal").modal('hide');
                }
                else if (data.status == 0) {
                    App.ShowError(data.message);
                    return;
                }
                $("#btnProp").attr('disabled', false);
            },
            traditional: true
        });
    }
    return {
        Init: Init,
        InitSignalr: InitSignalr,
        InitData: InitData,
        TranferLoanbrief: TranferLoanbrief,
        DistributingLoanOfHub: DistributingLoanOfHub,
        HubEmployeeChange: HubEmployeeChange,
        LoadData: LoadData,
        ProposeExceptions: ProposeExceptions,
        ProposeExceptionsPost: ProposeExceptionsPost
    }
}