﻿var LoanBriefStaffHubModuleJS = new function () {
    var _typeServiceCall;
    var InitData = function (typeServiceCall) {
        _typeServiceCall = typeServiceCall;
    }
    var Init = function () {
        $("#filterStatus").select2({
            placeholder: "Trạng thái",
            width: '100%'
        });

        LoadData();

        $("#filterLoanbriefId, #filterSearch").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });

        $('#filterStatus,#filterStatusChild').on('change', function () {
            LoadData();
            return false;
        });

        $("#btnSearch").on('click', function (e) {
            LoadData();
            return false;
        });
    };
    var LoadData = function () {
        var objQuery = {
            LoanbriefId: $('#filterLoanbriefId').val().replace('HĐ-', '').trim(),
            SearchName: $('#filterSearch').val().trim().toLowerCase(),
            LoanStatus: $('#filterStatus').val(),
            LoanStatusDetail: $('#filterStatus option:selected').attr('attr-detail-status'),
            HubLoanStatusChild: $('#filterStatusChild').val()
        }

        InitDataTable(objQuery);
    }
    var InitDataTable = function (objQuery) {
        var datatable = $('#dtLoanBrief').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/LoanBrief/GetDataStaffHub',
                        params: {
                            query: objQuery
                        }
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: false,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        var html = index + record;
                        if (row.utmSource != null && row.utmSource != "") {
                            html += "<br /><span style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);'>" + row.utmSource + "</span>";
                        }
                        if (row.utmCampaign != null && row.utmCampaign != "") {
                            html += "<br /><span style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);'>" + row.utmCampaign + "</span>";
                        }
                        return html;
                    }
                },

                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += '<span class="loanBriefId">HĐ-' + row.loanBriefId + '</span>';
                        if (row.platformType == PlatformType.LendingOnline || row.platformType == PlatformType.LendingOnlineAPP) {
                            html += ' <br /><span class="item-desciption">(LendingOnline)</span>';
                        }
                        if (row.isReborrow) {
                            html += `<br /><span class="item-desciption">(Tái vay)</span>`;
                        }
                        if (row.isLocate && row.deviceId != null && row.deviceId != "") {
                            html += ' <br /><span class="item-desciption">(' + row.deviceId + ')</span>';
                        }
                        //đơn topup
                        if (row.typeRemarketing == TypeRemarketing.IsTopUp) {
                            html += '<br /><span class="item-desciption">(Topup)</span>';
                        }
                        //Đơn remarketing autocall
                        if (row.typeRemarketing == TypeRemarketing.IsCreateLoanAutocall) {
                            html += '<br /><span class="item-desciption">(Remarketing Autocall)</span>';
                        }
                        //Đơn Tái cấu trúc nợ
                        if (row.typeRemarketing == TypeRemarketing.DebtRevolvingLoan) {
                            html += '<br /><span class="item-desciption">(Tái cấu trúc nợ)</span>';
                        }
                        //Check thời xử lý đơn vay
                        if (row.timeProcessingLoan != null) {
                            //có phản hồi nhưng trên 15 phút
                            if (row.timeProcessingLoan.isFeedBack == true && row.timeProcessingLoan.timeProcessing > 15)
                                html += '<br /><span style="color: red; font-size: 11px;">Phản hồi muộn ' + row.timeProcessingLoan.timeProcessing + ' phút</span>';
                            else if (row.timeProcessingLoan.isFeedBack == false && row.timeProcessingLoan.timeProcessing > 15)
                                html += '<br /><span style="color: red; font-size: 11px;">Chưa phản hồi</span>';
                            else if (row.timeProcessingLoan.isFeedBack == false && row.timeProcessingLoan.timeProcessing <= 15) {
                                html += '<br /><span style="color: #fdc502; font-size: 11px;" id="_timeProcessing_' + row.loanBriefId + '"></span>';
                                var minutes = parseInt(15 - row.timeProcessingLoan.timeProcessing);
                                if (minutes > 0) {
                                    var _spanId = "#_timeProcessing_" + row.loanBriefId;
                                    CommonModalJS.CountDown(minutes * 60, _spanId);
                                }
                            }
                        }
                        return html;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    textAlign: 'center',
                    //width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = '<a class="fullname" id="linkName" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>';
                        if (row.postRegistration == true)
                            html += `<span style="color:brown !important;" class="item-desciption">Thu đăng ký xe sau</span>`;
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Quận/ Huyện',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.district != null)
                            text += "<p class='district'>" + row.district.name + "</p>";
                        if (row.province != null) {
                            text += "<p class='province'>" + row.province.name + "</p>";
                        }
                        if (row.currentPipelineId != null && row.currentPipelineId == Pipeline['Pipeline_Test'])
                            text += '<span class="item-desciption">Luồng thử nghiệm</span>'
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    width: 150,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay <br /> VNĐ',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = `<span class="money">${App.FormatCurrency(row.loanAmount)}</span>`
                        if (row.loanProduct != null)
                            html += `<span class="item-desciption">${row.loanProduct.name}</span>`;
                        //if (row.isLocate) {
                        //    html += `<span class="m-badge m-badge--success m-badge--wide font-size-11">Xe lắp định vị</span>`;
                        //    if (App.ArrStatusOfDevice[row.deviceStatus] != null) {
                        //        html += '<span class="item-desciption">(' + App.ArrStatusOfDevice[row.deviceStatus].title+')</span>';
                        //    }
                        //}
                        //if (row.isTrackingLocation) {
                        //    html += `<span class="item-desciption">(KH chia sẻ vị trí)</span>`;
                        //}
                        return html;
                    }
                },
                {
                    field: 'loanTime',
                    title: 'Thời gian vay',
                    textAlign: 'center',
                    width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = "";
                        if (row.loanTime != null && row.loanTime > 0)
                            html += `<span>${row.loanTime} tháng</span></br>`;
                        if (row.isLocate) {
                            html += `<span class="m-badge m-badge--success m-badge--wide font-size-11">Xe lắp định vị</span>`;
                            if (App.ArrStatusOfDevice[row.deviceStatus] != null) {
                                html += '<span class="item-desciption">(' + App.ArrStatusOfDevice[row.deviceStatus].title + ')</span>';
                            }
                        }
                        if (row.isTrackingLocation) {
                            html += `<span class="item-desciption">(KH chia sẻ vị trí)</span>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var html = "";
                        if (row.status == APPRAISER_REVIEW) {
                            if (row.loanStatusDetail != null) {
                                html += '<span class="m-badge m-badge--success m-badge--wide">' + App.ArrLoanStatusDetail[row.loanStatusDetail].title + '</span>';
                                if (App.ArrLoanStatusDetail[row.loanStatusDetailChild] != null) {
                                    html += '<span class="detail-status-telesales">' + App.ArrLoanStatusDetail[row.loanStatusDetailChild].title + '</span>';

                                }
                                if (App.ArrPipelineState[row.pipelineState] != null && LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1) {
                                    html += `<span class="item-desciption-11">${App.ArrPipelineState[row.pipelineState].title}</span>`;
                                }
                            } else {
                                html += '<span class="m-badge ' + App.ArrLoanStatusDetail[40].class + ' m-badge--wide">' + App.ArrLoanStatusDetail[40].title + '</span>';
                            }
                        } else {
                            if (App.ArrLoanStatus[row.status] != null) {
                                html = '<span class="m-badge ' + App.ArrLoanStatus[row.status].class + ' m-badge--wide">' + App.ArrLoanStatus[row.status].title + '</span>';
                                if (App.ArrPipelineState[row.pipelineState] != null && LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1) {
                                    html += `<span class="item-desciption-11">${App.ArrPipelineState[row.pipelineState].title}</span>`;
                                }
                                if (row.status == BRIEF_APPRAISER_REVIEW) {
                                    if (row.approverStatusDetail != null)
                                        html += '<span style="color: red;font-style: italic;" class="m-list-timeline__text">' + App.ArrApproveStatusDetail[row.approverStatusDetail].title + '</span>';
                                    else
                                        html += '<span style="color: red;font-style: italic;" class="m-list-timeline__text">' + App.ArrApproveStatusDetail[72].title + '</span>';
                                }
                            } else {
                                html = 'Không xác định';
                            }
                        }
                        if (row.platformType == PlatformType.LendingOnline || row.platformType == PlatformType.LendingOnlineAPP) {
                            if (row.step > 0) {
                                html += `<span class="item-desciption-11">(step: ${row.step}/7)</span>`;
                            }
                        }

                        return html;

                    }
                },
                {
                    field: 'call',
                    title: 'Gọi',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (_typeServiceCall == 1) {
                            html = '<a href="javascript:;" onclick="CareSoftModule.CallCareSoft(' + row.loanBriefId + ')" class="box-call">\
                                    <i class="fa fa-phone item-icon-phone" ></i></a>';
                        } else if (_typeServiceCall == 2) {// gọi qua metech
                            html = `<a href="javascript:;" onclick="finesseC2CModule.ClickToCall('${FormatCallCisco.Format_1 + row.phone}',${row.loanBriefId})" class="box-call">
                                    <i class="fa fa-phone item-icon-phone" ></i></a>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'scheduleTime',
                    title: 'Lịch hẹn',
                    textAlign: 'center',
                    width: 80,
                    template: function (row) {
                        if (row.scheduleTime != null && row.scheduleTime != undefined) {
                            var date = moment(row.scheduleTime);
                            return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                        }

                    }
                },
                {
                    field: 'action',
                    title: 'Hành động',
                    width: 160,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1)
                            return html;
                        if (row.isEdit) {
                            html += `<a href="javascript:;" title="Cập nhật đơn vay" onclick="InitLoanBrief.Create(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="la la-edit"></i></a>`;
                        }
                        if (row.isLocate) {
                            html += `<a href="javascript:;" title="Thiết bị định vị" onclick="CommonModalJS.InitTrackDevice(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-location-arrow"></i></a>`;
                        }
                        if (row.isScript) {
                            html += `<a href="javascript:;" title="Kịch bản đơn vay" onclick="InitLoanBrief.ScriptTelesale(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-book"></i></a>`;
                        }
                        html += `<a data-toggle="modal" title="Chuyển trạng thái" onclick="CommonModalJS.InitStatusDetailModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-share-square-o"></i></a>`;
                        if (row.isPush) {
                            html += `<a href="javascript:;" title="Đẩy đơn vay" onclick="CommonModalJS.ConfirmLoanBrief(${row.loanBriefId}, LoanBriefStaffHubModuleJS.LoadData)" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-check"></i></a>`;
                        }
                        //chia đơn cho nhân viên hoặc chuyển hình thức thẩm định ở trạng thái chờ thẩm định
                        if (row.status == APPRAISER_REVIEW) {
                            html += `<a data-toggle="modal" title="Chuyển nhân viên thẩm định" onclick="LoanBriefStaffHubModuleJS.HubEmployeeChange(${row.loanBriefId}, LoanBriefStaffHubModuleJS.LoadData)" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-exchange"></i></a>`;
                        }

                        if (row.status == LoanStatus.APPRAISER_REVIEW) {
                            html += `<a data-toggle="modal" title="Đề xuất ngoại lệ" onclick="LoanBriefHubModuleJS.ProposeExceptions(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							<i class="fa fa-warning"></i></a>`;
                        }
                        //đơn topup hoặc Đơn rmk autocall của hub hoặc đơn oto hoặc đơn tái vay
                        //             if (row.isTrackingLocation && (row.typeRemarketing == 2 || row.productId == 8 || (row.isReborrow && (row.boundTelesaleId == 0 || row.boundTelesaleId == null)))) {
                        //                 html += `<a href="javascript:;" title="Lấy thông tin location Sim" onclick="CommonModalJS.ReGetLocationSim(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
                        //<i class="fa fa-map-marker"></i></a>`;
                        //             }

                        html += `<a href="javascript:;" title="Nhật ký khoản vay" onclick="CommonModalJS.InitDiaryModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							             <i class="fa fa-wpforms"></i></a>`;
                        if (row.isUpload) {
                            html += `<a href="javascript:;" title="Upload chứng từ" onclick="CommonModalJS.InitUploadModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-upload"></i></a>`;
                        }
                        if (row.isCancel) {
                            html += `<a href="javascript:;" title="Hủy đơn vay" onclick="CommonModalJS.InitCancelModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-trash"></i></a>`;
                        }
                        html += `<a href="javascript:;" title="Check Ekyc" onclick="CommonModalJS.CheckEkyc(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-address-card-o"></i></a>`;
                        if (row.isTrackingLocation && row.pickTLS) {
                            html += `<a href="javascript:;" title="Lấy thông tin location Sim" onclick="CommonModalJS.ReGetLocationSim(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
                                        <i class="fa fa-globe"></i></a>`;
                        }

                        html += `<a href="javascript:;" title="Check List" onclick="CommonModalJS.InitCheckListModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="la la-list-alt"></i></a>`;
                        html += `<a href="javascript:;" title="Đặt lịch hẹn" onclick="CommonModalJS.InitScheduleTime(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-clock-o"></i></a>`;
                        if (row.productId == ProductCredit.OtoCreditType_CC
                            && (row.status == APPRAISER_REVIEW || row.status == HUB_CHT_LOAN_DISTRIBUTING || row.status == HUB_CHT_APPROVE)
                            && (row.reMarketingLoanBriefId == null || row.reMarketingLoanBriefId == row.loanBriefId || row.typeRemarketing == TypeRemarketing.IsTopUp)) {
                            html += `<a href="javascript:;" title="Tạo đơn con" onclick="CommonModalJS.CreateLoanCarChild(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-plus-circle"></i></a>`;
                        }
                        //if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                        //    html += `<div class="dropdown dropup" style="position: absolute !important;">
                        //            <a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" data-toggle="dropdown">
                        //                <i class="fa fa-print"></i>                    
                        //            </a>						  	
                        //            <div class="dropdown-menu dropdown-menu-right">	
                        //                <a class="dropdown-item" href="#" onclick="PrintModel.ContractFinancialAgreement(${row.loanBriefId})">
                        //                     In thỏa thuận tài chính 3 bên
                        //                </a>					
                        //                <a class="dropdown-item" href="#" onclick="PrintModel.CommittedPaper(${row.loanBriefId})">
                        //                     In giấy cam kết XMKCC
                        //                </a>						    	
                        //                <a class="dropdown-item" href="#" onclick="PrintModel.AddendumAgreementPledgeOfProperty(${row.loanBriefId})">
                        //                    In phụ lục thỏa thuận cầm cố tài sản
                        //                </a>						  	
                        //            </div>						
                        //        </div>`;
                        //}

                        if (row.esignState == EsignState.BORROWER_SIGNED) {
                            html += `<a href="javascript:;" title="Ký lại" onclick="CommonModalJS.ReEsign(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
                                        <i class="fa fa-unlock-alt"></i></a>`;
                        }

                        html += `<a href="javascript:;" title="Lấy chứng từ chứng minh thu nhập" onclick="CommonModalJS.GetInsuranceInfo(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
                                        <i class="fa fa-image"></i></a>`;

                        if (row.isCheckMomo) {
                            html += `<a href="javascript:;" title="Check Momo" onclick="CommonModalJS.CheckMomo(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
                                        <i class="flaticon-user-ok"></i></a>`;
                        }
                        return html;
                    }
                }
            ]
        });
    }
    var HubEmployeeChange = function (loanId, func) {
        $('#DistributingLoanOfHub').html('');

        $.ajax({
            url: '/LoanBriefV2/HubEmployeeChange?loanbriefId=' + loanId,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#DistributingLoanOfHub').html(res.html);
                    $('#modelDistributingLoanOfHub').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }

    var GetStatusDetailChild = function (tthis) {
        var LoanStatus = $('option:selected', tthis).attr('attr-detail-status');
        if (LoanStatus > 0 && LoanStatus != 40) {
            $('#filterChild').css("display", "block");
            App.Ajax("GET", "/Dictionary/GetLoanStatusDetailChild?id=" + LoanStatus, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += '<option value="0">Chọn trạng thái chi tiết</option>';
                    for (var i = 0; i < data.data.length; i++) {
                        html += `<option value="${data.data[i].id}" >${data.data[i].name}</option>`;
                    }
                    $("#filterStatusChild").html(html);
                    $("#filterStatusChild").select2();
                }
            });
        } else {
            $('#filterChild').css("display", "none");
        }
    }

    return {
        Init: Init,
        InitData: InitData,
        LoadData: LoadData,
        HubEmployeeChange: HubEmployeeChange,
        GetStatusDetailChild: GetStatusDetailChild
    };
}