﻿var ReportDebtModel = new function () {
    var InitDataTable = function (objQuery) {
        var datatable = $('#tbReportDebt').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/Hub/GetReportDebt',
                        params: {
                            query: objQuery
                        },
                    }
                },
                pageSize: 50,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'hubName',
                    title: 'Tên HUB',
                    sortable: false
                },
                {
                    field: 'TotalMoneyCurrentBegin',
                    title: 'Dư nợ đầu kỳ',
                    sortable: false,
                    template: function (row) {
                        return App.FormatCurrency(row.totalMoneyCurrentBegin)
                    }
                },
                {
                    field: 'TotalMoneyCurrentBegin_10PDP',
                    title: 'Dư nợ đầu kỳ DPD < 10',
                    sortable: false,
                    template: function (row) {
                        return App.FormatCurrency(row.totalMoneyCurrentBegin_10PDP)
                    }
                },
                {
                    field: 'TotalMoneyDisbursementNew',
                    title: 'Dư nợ GN mới',
                    sortable: false,
                    template: function (row) {
                        return App.FormatCurrency(row.totalMoneyDisbursementNew)
                    }
                },
                {
                    field: 'TotalMoneyCurrent',
                    title: 'Dư nợ hiện tại',
                    sortable: false,
                    template: function (row) {
                        return App.FormatCurrency(row.totalMoneyCurrent)
                    }
                },
                {
                    field: 'TotalMoneyCurrent_10DPD',
                    title: 'Dư nợ hiện tại DPD < 10',
                    sortable: false,
                    template: function (row) {
                        return App.FormatCurrency(row.totalMoneyCurrent_10DPD)
                    }
                },
                {
                    field: 'NetAugment',
                    title: 'Tốt Tăng Net',
                    sortable: false,
                    template: function (row) {
                        return App.FormatCurrency(row.netAugment)
                    }
                },
                {
                    field: 'CompletionRateT_3',
                    title: 'Tỷ lệ hoàn thành nợ QH 10+ của tháng T-3',
                    sortable: false,
                    template: function (row) {
                        return App.FormatCurrency(row.completionRateT_3)
                    }
                },
                {
                    field: 'CompletionRateT_2',
                    title: 'Tỷ lệ hoàn thành nợ QH 10+ của tháng T-2',
                    sortable: false,
                    template: function (row) {
                        return App.FormatCurrency(row.completionRateT_2)
                    }
                },
                {
                    field: 'CompletionRateT_1',
                    title: 'Tỷ lệ hoàn thành nợ QH 10+ của tháng T-1',
                    sortable: false,
                    template: function (row) {
                        return App.FormatCurrency(row.completionRateT_1)
                    }
                },
                {
                    field: 'CompletionRate3Month',
                    title: 'Tỷ lệ hoàn thành nợ QH 10+ của 3 tháng giải ngân gần nhất',
                    sortable: false,
                    template: function (row) {
                        return App.FormatCurrency(row.completionRate3Month)
                    }
                },
            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            hubId: $('#filterHubId').val(),
            date: $('#filterDate').val().trim(),
        }
        InitDataTable(objQuery);
    }

    var Init = function () {
        $("#filterHubId").select2({
            placeholder: "Vui lòng chọn",
            width: '100%'
        });
        $("#btnSearch").click(function () {
            LoadData();
        });
    }

    var ExportExcel = function () {
        var hubId = $('#filterHubId').val();
        var date = $('#filterDate').val().trim();
        window.location.href = "/Export/ExportExcelReportDebt?hubid=" + hubId + "&date=" + date;
    }

    return {
        Init: Init,
        LoadData: LoadData,
        ExportExcel: ExportExcel
    }
}();