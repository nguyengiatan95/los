﻿var InvoiceModule = new function () {

    var CreateInvoice = function (type) {
        $('#divResult').html('');
        $.ajax({
            type: "POST",
            url: "/Hub/CreateFromInvoice",
            data: { TypeInvoice: type },
            success: function (data) {
                $('#divResult').html(data);
                $('#Modal').modal('show');
            }
        });
    }


    return {
        CreateInvoice: CreateInvoice
    };
};