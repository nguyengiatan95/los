﻿var AddressHomeModuleJS = new function () {
    var Init = function () {
        $('#sl_province_addresshome').select2({
            placeholder: "Vui lòng chọn Tỉnh/ Thành phố",
            width: '100%'
        }).on("change", function (e) {
            $(this).valid()
        });
        $("#sl_district_addresshome").select2({
            placeholder: "Vui lòng chọn Quận/ Huyện",
            width: '100%'
        }).on("change", function (e) {
            $(this).valid()
        });
        $("#sl_ward_addresshome").select2({
            placeholder: "Vui lòng chọn Phường/ Xã",
            width: '100%'
        }).on("change", function (e) {
            $(this).valid()
        });

        $("#sl_TypeOwnerShip").select2({
            placeholder: "Chọn hình thức cư trú",
            width: '100%'
        }).on("change", function (e) {
            $(this).valid()
        });

        $("#sl_LivingTime").select2({
            placeholder: "Chọn thời gian sống",
            width: '100%'
        });

        $("#sl_water_supplier").select2({
            placeholder: "Vui lòng chọn",
            width: '100%'
        });

        $("#sl_LoanPurpose").select2({
            placeholder: "Chọn mục đích vay",
            width: '100%'
        });
        
    };
    var GetDistrict = function (provinceId, districtId) {
        if (provinceId > 0) {
            App.Ajax("GET", "/Dictionary/GetDistrict?id=" + provinceId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (districtId > 0 && districtId == data.data[i].districtId) {
                            html += `<option value="${data.data[i].districtId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].districtId}">${data.data[i].name}</option>`;
                        }
                    }
                    $("#sl_district_addresshome").html(html);
                    $("#sl_district_addresshome").select2({
                        placeholder: "Vui lòng chọn Quận/ Huyện"
                    });
                }
            });
            //load lại phường xã
            $("#sl_ward_addresshome").html("<option></option>");
            $("#sl_ward_addresshome").select2({
                placeholder: "Vui lòng chọn Phường/ Xã"
            });
        } else {
            $('#sl_district_addresshome').select2({
                placeholder: "Vui lòng chọn Quận/ Huyện",
                width: '100%'
            });
        }
    }
    var GetWard = function (districtId, wardId) {
        if (districtId > 0) {
            App.Ajax("GET", "/Dictionary/GetWard?districtId=" + districtId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (wardId > 0 && wardId == data.data[i].wardId) {
                            html += `<option value="${data.data[i].wardId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].wardId}">${data.data[i].name}</option>`;
                        }
                    }
                    $("#sl_ward_addresshome").html(html);
                    $("#sl_ward_addresshome").select2({
                        placeholder: "Vui lòng chọn Phường/ Xã"
                    });
                }
            });
        } else {
            $('#sl_ward_addresshome').select2({
                placeholder: "Vui lòng chọn Phường/ Xã",
                width: '100%'
            });
        }
    }
    
    return {
        Init: Init,
        GetDistrict: GetDistrict,
        GetWard: GetWard
    }
}