﻿var CalculatorInsurranceModuleJS = new function () {
    var Init = function (loanAmount, loanTime, rateType, frequency, rate, loanbriefId) {
        var notSchedule = 0;
        if (loanAmount != "" && loanTime != "") {
            loanAmount = loanAmount.split(",").join("");
            $.ajax({
                type: "POST",
                url: "/Approve/InterestSubmit",
                data: {
                    TotalMoneyDisbursement: parseInt(loanAmount),
                    LoanTime: parseInt(loanTime),
                    RateType: parseInt(rateType),
                    Frequency: parseInt(frequency),
                    Rate: rate,
                    LoanBriefId: parseInt(loanbriefId),
                    NotSchedule: notSchedule
                },
                success: function (data) {
                    //console.log(data);
                    if (data.status == 1) {
                        //console.log(data);
                        $("#FeeInsurrance").text(App.FormatCurrency(data.data.feeInsurrance) + ' VNĐ');
                        $("#FeeInsurranceProperty").text(App.FormatCurrency(data.data.feeInsuranceMaterialCovered) + ' VNĐ');
                    }
                },
                traditional: true
            });
        }
        
    };


    return {
        Init: Init
    }
}