﻿var CalculatorRateMoneyModuleJS = new function () {
    var Init = function (productId, loanAmount, loanTime) {
        var rate = $("#RateMoney").val();
        if (productId != "" && loanAmount != "" && loanTime != "") {
            var car = $("#ProductCarEnum").val();
            loanAmount = loanAmount.split(",").join("");
            //Phí bảo hiểm
            var insu = (loanAmount / 100) * 5;
            $("#InsurenceMoney").text(App.FormatCurrency(insu) + ' VNĐ');
            //Gói vay oto 01
            if (productId == car && (parseInt(loanAmount) >= 20000000 && parseInt(loanAmount) <= 100000000) && (parseInt(loanTime) == 3 || parseInt(loanTime) == 6)) {
                if (parseInt(loanTime) == 3)
                    rate = 2500;
                if (parseInt(loanTime) == 6)
                    rate = 2000;
            }
            //Gói vay oto 02
            if (productId == car && (parseInt(loanAmount) > 100000000 && parseInt(loanAmount) <= 200000000) && (parseInt(loanTime) == 3 || parseInt(loanTime) == 6 || parseInt(loanTime) == 9 || parseInt(loanTime) == 12)) {
                if (parseInt(loanTime) == 3)
                    rate = 2500;
                if (parseInt(loanTime) == 6)
                    rate = 2000;
                if (parseInt(loanTime) == 9)
                    rate = 1833;
                if (parseInt(loanTime) == 12)
                    rate = 1660;
            } 
            
        } 
        $("#RateMoney").val(App.FormatCurrency(rate));
        Calculator(rate);
    };
    var Calculator = function (rate) {
        rate = rate.replace(/,/g, '');
        if (parseInt(rate) > 1000) {
            $("#Percent").text('');
            var cal = (rate * 30 / 1000000) * 100;
            var cal2 = (rate * 365 / 1000000) * 100;
            $("#Percent").text(cal.toFixed(1) + ' % / tháng');
            $("#RatePercent").val(cal2.toFixed(2));
        }
        
    };
    var OnchangeMoneyCal = function (val) {
        if (val.length >= 8) {
            val = val.split(",").join("");
            var insu = (val / 100) * 5;
            $("#InsurenceMoney").text(App.FormatCurrency(insu) + ' VNĐ');
            
            var productId = $("#sl_loan_product").val();
            var loanTime = $("#sl_LoanTime").val();
            if (loanTime != "" && productId != "") {
                Init(productId, val, loanTime);
            }
            if (val != "" && loanTime != "")
                CalculatorInsurranceModuleJS.Init(val, loanTime);
        }
        

    };

    return {
        Init: Init,
        Calculator: Calculator,
        OnchangeMoneyCal: OnchangeMoneyCal
    }
}