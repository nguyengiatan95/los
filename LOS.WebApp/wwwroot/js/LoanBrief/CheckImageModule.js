﻿
var CheckImageModule = new function () {

    var SubmitForm = function (e) {
        e.preventDefault();
        var form = $('#formAddLoanBrief');
        form.validate({
            ignore: [],
            rules: {
                'PlatformType': {
                    required: true
                },
                'Customer.FullName': {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                'Customer.Phone': {
                    required: true,
                    validPhone: true
                },
                'Customer.NationalCard': {
                    required: true,
                    validCardnumber: true
                },
                'Customer.NationCardPlace': {
                    validCardnumber: true
                },
                'sBirthDay': {
                    validDate: true
                },
                'ProductId': {
                    required: true
                },
                'LoanBriefResident.ProvinceId': {
                    required: true
                },
                'LoanBriefResident.DistrictId': {
                    required: true
                },
                'LoanBriefResident.WardId': {
                    required: true
                },
                'LoanBriefResident.ResidentType': {
                    required: true
                },
                LoanAmount: {
                    validLoanAmount: true
                },
                LoanTime: {
                    required: true
                }
            },
            messages: {
                'PlatformType': {
                    required: "Bạn vui lòng chọn nguồn đơn",
                    class: "has-danger"
                },
                'Customer.FullName': {
                    required: "Bạn vui lòng nhập họ tên",
                },
                'Customer.Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                'Customer.NationalCard': {
                    required: "Vui lòng nhập CMND/CCCD",
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'Customer.NationCardPlace': {
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'sBirthDay': {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                },
                'ProductId': {
                    required: "Vui lòng chọn gói vay"
                },
                'LoanBriefResident.ProvinceId': {
                    required: "Vui lòng chọn thành phố đang sống"
                },
                'LoanBriefResident.DistrictId': {
                    required: "Vui lòng chọn quận/huyện đang sống"
                },
                'LoanBriefResident.WardId': {
                    required: "Vui lòng chọn phường/xã đang sống"
                },
                'LoanBriefResident.ResidentType': {
                    required: "Vui lòng chọn hình thức cư trú"
                },
                LoanAmount: {
                    validLoanAmount: "Vui lòng nhập số tiền khách hàng cần vay"
                },
                LoanTime: {
                    required: "Vui lòng chọn thời gian vay"
                }

            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            return;
        } else {
            $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
            $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
            $('#RateMoney').val($('#RateMoney').val().replace(/,/g, ''));
            $('#btnSubmit').attr('disabled', true);
            form.ajaxSubmit({
                url: '/LoanBrief/InitLoanBrief',
                method: 'POST',
                success: function (data, status, xhr, $form) {
                    if (data != undefined) {
                        if (data.status == 1) {
                            $('#modelInitLoanBrief').modal('hide');
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                    $('#btnSubmit').attr('disabled', false);
                },
                error: function () {
                }
            });
        }
    }

    return {
        Create: InitModal,
        ScriptTelesale: ScriptTelesale,
        Save: SubmitForm
    };
}