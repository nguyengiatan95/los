﻿var ListFileModule = new function () {
    var ListFile = function (loanBriefId) {
        $.ajax({
            type: "GET",
            url: "/LoanBrief/DetailListFile?id=" + loanBriefId,
            success: function (data) {
                $('#m_portlet_tab_1_3').html(data);
            },
            traditional: true
        });

    };
    var ListFileAll = function (loanBriefId) {
        $.ajax({
            type: "GET",
            url: "/LoanBrief/DetailListFileAll?id=" + loanBriefId,
            success: function (data) {
                $('#m_portlet_tab_1_7').html(data);
               
            },
            traditional: true
        });

    };

    return {
        ListFile: ListFile,
        ListFileAll: ListFileAll
    }
}