﻿var LoanBriefDetailModule = new function () {

    var CalculateInterest = function (loanbriefId, loanAmount, loanTime, rateTypeId, ratePercent) {
        if (ratePercent == null || ratePercent == '' || ratePercent == undefined)
            ratePercent = '98.55';
        $.ajax({
            url: '/LoanBrief/GetApiCalculateInterest',
            type: 'POST',
            data: { loanbriefId: loanbriefId, loanAmount: loanAmount, loanTime: loanTime, rateTypeId: rateTypeId, ratePercent: ratePercent },
            success: function (res) {
                if (res.status == 1) {
                    $('#_totalMoneyNeedPayment').html(App.FormatCurrency(res.data) + " VNĐ")
                }
            },
        });
    }

    var ResultEkyc = function (loanbriefId) {
        $.ajax({
            url: '/LoanBrief/ResultEkyc',
            type: 'GET',
            data: { loanbriefId: loanbriefId },
            success: function (res) {
                $('#_result_ekyc').html('');
                $('#_result_ekyc').html(res.html);
                if (res.data != null) {
                    if (res.data.fullNameCheck != null && res.data.fullNameCheck == false)
                        $('#_result_ekyc_fullname').html(res.data.fullNameValue);
                    if (res.data.idCheck != null && res.data.idCheck == false)
                        $('#_result_ekyc_nationalcard').html(res.data.idValue);
                    if (res.data.birthdayCheck != null && res.data.birthdayCheck == false)
                        $('#_result_ekyc_birday').html(res.data.birthdayValue);
                }
            },
        });
    }

    return {
        CalculateInterest: CalculateInterest,
        ResultEkyc: ResultEkyc,
    }
}();