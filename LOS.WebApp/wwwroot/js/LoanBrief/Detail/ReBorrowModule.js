﻿var ReBorrowModuleJS = new function () {


    var RemoveLabelReBorrow = function (loanbriefId) {
        $('#modelInitLoanBrief').modal('hide');
        $('#_title_RemoveLabelReBorrow').html('');
        $('#_title_RemoveLabelReBorrow').html('<span>Gỡ nhãn tái vay <b style="color: red;">HĐ-' + loanbriefId + '</b></span>');
        $('#hdd_loanbriefId').val(loanbriefId);
        $('#modalRemoveLabelReBorrow').modal('show');
    }

    var SubmitForm = function () {
        var form = $('#formRemoveLabelReBorrow');
        $('#btnSaveRemoveLabelReBorrow').attr('disabled', true);
        form.validate({
            ignore: [],
            rules: {
                CommentRemoveLabelReBorrow: {
                    required: true
                },
            },
            messages: {
                CommentRemoveLabelReBorrow: {
                    required: "Vui lòng nhập ghi chú",
                },
            } 
        });
        if (!form.valid()) {
            $('#btnSaveRemoveLabelReBorrow').attr('disabled', false);
            return;
        }
        form.ajaxSubmit({
            url: '/LoanBriefV2/RemoveLabelReBorrow',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $('#btnSaveRemoveLabelReBorrow').attr('disabled', false);
                if (data != undefined) {
                    if (data.status == 1) {
                        App.ShowSuccess(data.message);
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    return {
        RemoveLabelReBorrow: RemoveLabelReBorrow,
        SubmitForm: SubmitForm
    }
}