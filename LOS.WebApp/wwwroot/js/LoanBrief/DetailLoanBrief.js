﻿var DetailLoanBrief = new function () {

    var DetailModal = function (LoanBriefId) {
        $('#InitLoanBriefBody').html('');
        $.ajax({
            url: '/LoanBrief/Detail?id=' + LoanBriefId,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                if (res.isSuccess == 1) {
                    $('#InitLoanBriefBody').html(res.html);
                    $('#modelInitLoanBrief').modal('show');
                } else {
                    App.ShowError(res.message);
                }
            },
            error: function () {

            }

        });
    }


    var ScriptTelesale = function (LoanBriefId) {
        $('#ScriptTelesaleBody').html('');
        $.ajax({
            url: '/LoanBrief/ScriptTelesalse?loanBriefid=' + LoanBriefId,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                if (res.isSuccess == 1) {
                    $('#ScriptTelesaleBody').html(res.html);
                    $('#modelScriptTelesales').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {

            }

        });
    }


    var SubmitForm = function (e) {
        e.preventDefault();

        var form = $('#formDetailLoanBrief');
        form.validate({
            ignore: [],
            rules: {
                'PlatformType': {
                    required: true
                },
                'Customer.FullName': {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                'Customer.Phone': {
                    required: true,
                    validPhone: true
                },
                'Customer.NationalCard': {
                    validCardnumber: true
                },
                'Customer.NationCardPlace': {
                    validCardnumber: true
                },
                'sBirthDay': {
                    validDate: true
                }
            },
            messages: {
                'PlatformType': {
                    required: "Bạn vui lòng chọn nguồn đơn",
                    class: "has-danger"
                },
                'Customer.FullName': {
                    required: "Bạn vui lòng nhập họ tên",
                },
                'Customer.Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                'Customer.NationalCard': {
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'Customer.NationCardPlace': {
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'sBirthDay': {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                }
                //, 
                //LoanAmount: {
                //    required: "Vui lòng nhập số tiền khách hàng cần vay"
                //}

            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || (element.hasClass('m-select2') && element.next('.select2-container').length)) {
                    error.appendTo(element.parents('.box-group'));
                }
                else { // This is the default behavior
                    error.insertAfter(element);
                }
            }
            //,
            //errorPlacement: function (error, element) {
            //    debugger
            //    if (element.hasClass('m-select2') && element.next('.select2-container').length) {
            //        error.insertAfter(element.next('.select2-container'));
            //    } else if (element.parent('.input-group').length) {
            //        error.insertAfter(element.parent());
            //    }
            //    else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
            //        error.insertAfter(element.parent().parent());
            //    }
            //    else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
            //        error.appendTo(element.parent().parent());
            //    }
            //    else {
            //        error.insertAfter(element);
            //    }
            //}
            //,
            //invalidHandler: function (form, validator) {
            //    var errors = validator.numberOfInvalids();
            //    if (errors) {
            //        validator.errorList[0].element.focus();
            //    }
            //}
        });
        if (!form.valid()) {
            return;
        }
        $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
        $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
        $('#btnSubmit').attr('disabled', true);
        form.ajaxSubmit({
            url: '/LoanBrief/DetailLoanBrief',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#modelInitLoanBrief').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        App.ReloadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
                $('#btnSubmit').attr('disabled', false);
            }
        });
    }
    return {
        Detail: DetailModal,
        ScriptTelesale: ScriptTelesale,
        Save: SubmitForm
    };
}