﻿var DisbursementInfomationModeleJS = new function () {
    var Init = function () {
        $('#sl_ReceivingMoneyType').select2({
            placeholder: "Chọn hình thức nhận tiền",
            width: '100%'
        });
        $('#sl_BankReceiveMoney').select2({
            placeholder: "Chọn ngân hàng ",
            width: '100%'
        });
        
    };

    var ChangeTypeReceiveMoney = function (val) {
        if (val == 1) {
            $('#boxTranferBank').css('display', 'none');
        } else {
            if (val == 3) {
                $('#boxCardAccountNumber').css('display', 'none');
                $('#boxAccountNumber').css('display', '');
            } else {
                $('#boxCardAccountNumber').css('display', '');
                $('#boxAccountNumber').css('display', 'none');
            }
            $('#boxTranferBank').css('display', 'block');
        }
    }
    return {
        Init: Init,
        ChangeTypeReceiveMoney: ChangeTypeReceiveMoney
    }
}