﻿var ImportFileExcelModule = new function () {
    var GetListTableImportFileExcel = function () {
        var form = $('#frm-tbImportLoanFileExcel');
        form.ajaxSubmit({
            url: "/LoanBrief/GetListTableImportFileExcel",
            method: "GET",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            success: function (data) {
                $.unblockUI();
                $("#div-table").html('');
                $('#div-table').html(data);
            }
        });
    }

    var Init = function () {
        GetListTableImportFileExcel();
        $('#filtercreateTime').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY',
                firstDay: 1,
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7",
                ],
                "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ],
            },
        });

        $("#filtercreateTime").on('change', function (e) {
            GetListTableImportFileExcel();
            return false;
        });
    };
    var ImportFile = function () {
        $('#btnImportLoanExcel').attr('disabled', true);
        $('#btnImportLoanExcel').attr('style', 'cursor: no-drop;');
        $('#btnImportLoanExcel').addClass('m-loader m-loader--success m-loader--right');
        var formdata = new FormData();
        var totalFiles = document.getElementById("file-importloanexcel").files.length;
        if (totalFiles == 0) {
            $('#btnImportLoanExcel').attr('disabled', false);
            $('#btnImportLoanExcel').attr('style', 'cursor:pointer;');
            $('#btnImportLoanExcel').removeClass('m-loader m-loader--success m-loader--right');
            App.ShowErrorNotLoad('Bạn phải tải file lên !!!');
            return false;
        }
        for (var i = 0; i < totalFiles; i++) {
            var file = document.getElementById("file-importloanexcel").files[i];
            formdata.append("file", file);
        }
        $.ajax({
            type: 'POST',
            url: '/LoanBrief/ImportFileExcel',
            data: formdata,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $('#btnImportLoanExcel').attr('disabled', false);
                $('#btnImportLoanExcel').attr('style', 'cursor:pointer;');
                $('#btnImportLoanExcel').removeClass('m-loader m-loader--success m-loader--right');
                if (data.status == 1) {
                    GetListTableImportFileExcel();
                    App.ShowSuccessNotLoad(data.message);
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    };
    var DownloadFileExsample = function () {
        var link = document.createElement("a");
        link.download = 'file_import_loanbrief_excel';
        link.href = '/Files/file_import_loanbrief_excel.xlsx';
        link.click();
    };
    var DetailLoanBriefImportFileExcel = function (importFileId, status) {
        $.ajax({
            url: "/LoanBrief/GetDetailLoanBriefImportFileExcell",
            method: "POST",
            data: { ImportFileId: importFileId, Status: status },
            success: function (data) {
                $('#modalDetailLoanBriefImportFileExcel').modal('show');
                $('#bodyDetailLoanBriefImportFileExcel').html();
                $('#bodyDetailLoanBriefImportFileExcel').html(data);
            }
        });
    }

    return {
        Init: Init,
        ImportFile: ImportFile,
        DownloadFileExsample: DownloadFileExsample,
        GetListTableImportFileExcel: GetListTableImportFileExcel,
        DetailLoanBriefImportFileExcel: DetailLoanBriefImportFileExcel
    };
}