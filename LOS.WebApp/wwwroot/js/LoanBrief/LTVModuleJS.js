﻿var LTVModuleJS = new function () {
    var Init = function () {
        $("#sl_Ltv").select2();
        var productId = $('#sl_loan_product').val();
        if (productId == ProductCredit.MotorCreditType_CC || productId == ProductCredit.MotorCreditType_KCC || productId == ProductCredit.MotorCreditType_KGT) {
            LoadOption();
            $('#boxLtv').show();
        } else {
            $('#sl_Ltv').html('');
            $('#boxLtv').hide();
        }
    };
    var LoadOption = function () {
        $('#sl_Ltv').html('');
        var ltv = $('#LTV').val();
        var productId = $('#sl_loan_product').val();
        var isReborrow = $('#LTV_IsReborrow').val();
        var typeRemarketing = $('#LTV_TypeRemarketing').val();

        $.ajax({
            url: '/LoanbriefV2/GetOptionLTV?ltv=' + ltv + '&productId=' + productId + '&isReborrow=' + isReborrow + '&typeRemarketing=' + typeRemarketing,
            type: 'GET',
            success: function (data) {
                console.log('html',data);
                $('#sl_Ltv').html(data);
            }
        });
    }

    return {
        Init: Init,
        LoadOption: LoadOption
    }
}