﻿
var LoanBriefTopup = new function () {

    var CreateLoanTopup = function (loanBriefId) {
        $('#InitLoanBriefBody').html('');
        $.ajax({
            type: "GET",
            url: "/LoanBrief/TopupInfo?loanBriefId=" + loanBriefId,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#InitLoanBriefBody').html(res.html);
                    $('#modelInitLoanBrief').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }



    var SubmitForm = function (e) {
        e.preventDefault();
        var form = $('#formAddLoanBriefTopup');

        $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
        $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
        $('#RateMoney').val($('#RateMoney').val().replace(/,/g, ''));
        $('#btnSubmitTopup').attr('disabled', true);
        form.ajaxSubmit({
            url: '/LoanBrief/AddLoanTopup',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#modelInitLoanBrief').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        App.ReloadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
                $('#btnSubmitTopup').attr('disabled', false);
            },
            error: function () {
            }
        });
    }

    var ListPaymentTopup = function (codeId) {
        var x = document.getElementById("ShowHide");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        $("#Loading").attr("style", "display:block")
        var loanAmount = $('#txtLoanAmount').val().replace(/,/g, '');
        var loanTime = $('#sl_LoanTimeTopup').val();
        if (loanTime == '')
            loanTime = 12;
        var loanTimeDay = (Number(loanTime) * 30);
        $.ajax({
            url: "/Approve/ViewIntersestSubmit",
            method: "POST",
            data: { CodeId: codeId, TotalMoneyDisbursementTopup: loanAmount, LoanTimeTopup: loanTimeDay},
            success: function (data) {
                $("#Loading").attr("style", "display:none")
                $('#lstPaymentTopup').html('');
                $('#lstPaymentTopup').append(data.html);
                
            }

        });
    };


    return {
        CreateLoanTopup: CreateLoanTopup,
        Save: SubmitForm,
        ListPaymentTopup: ListPaymentTopup
    };
}