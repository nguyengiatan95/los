﻿var LoanCancelModule = new function () {
    var _groupGlobal;
    var _typeServiceCall;

    var InitData = function (group, typeServiceCall) {
        _groupGlobal = group;
        _typeServiceCall = typeServiceCall;
    }
    var InitDataTable = function (objQuery) {
        var datatable = $('#dtLoanCancel').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Remarketing/SearchLoanCancel',
                        params: {
                            query: objQuery
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                
                {
                    field: 'stt',
                    title: 'STT',
                    width: 40,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }

                        return index + record;
                    }
                },
                {
                    field: 'call',
                    title: 'Gọi',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        //gọi qua caresoft
                        var html = '';
                        if (_typeServiceCall == 1) {
                            html = '<a href="javascript:;" onclick="CareSoftModule.CallCareSoft(' + row.loanBriefId + ')" class="box-call">\
                                    <i class="fa fa-phone item-icon-phone" ></i></a>';
                        } else if (_typeServiceCall == 2) {// gọi qua metech
                           html = `<a href="javascript:;" onclick="finesseC2CModule.ClickToCall('${row.phone}',${row.loanBriefId})" class="box-call">
                                    <i class="fa fa-phone item-icon-phone" ></i></a>`;
                        }
                        //html += '<span>' + row.phone + '</span>'
                        var dauso = row.phone.substring(0, 3);
                        if (Vina.includes(dauso))
                            html += '<span class="item-desciption">Vina</span>'
                        else if (Mobi.includes(dauso))
                            html += '<span class="item-desciption">Mobi</span>'
                        else if (Viettel.includes(dauso))
                            html += '<span class="item-desciption">Viettel</span>'
                        else
                            html += '<span class="item-desciption">Không xác định</span>'
                        

                        return html;
                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += 'HĐ-' + row.loanBriefId;
                        if (row.hub != null) {
                            html += `<span class="item-desciption">(${row.hub.name})</span>`;
                        }
                        if (row.utmSource != null && row.utmSource != "") {
                            html += "<br /><p style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);'>" + row.utmSource + "</p>";
                        }
                        if (row.isReborrow) {
                            html += `<span class="item-desciption">(Tái vay)</span>`;
                        }
                        return html;
                    }
                },
                
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    textAlign: 'center',
                    //width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = '<a id="linkName" style="color:#6C7293;" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>';
                        //<span class="item-desciption">(Tái vay)</span>
                        if (row.countCall > 0)
                            html += `<span class="item-desciption">(Đã gọi ${row.countCall} lần)</span>`
                        else
                            html += `<span class="item-desciption">(Chưa gọi)</span>`
                        return html;
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        //return App.FormatCurrency(row.loanAmount);
                        var html = `${App.FormatCurrency(row.loanAmount)}`
                        if (row.loanProduct != null)
                            html += `<span class="item-desciption">${row.loanProduct.name}</span>`;
                        //if (row.loanProduct != null)
                        //    html += `${row.loanProduct.name}`;
                        if (row.isLocate) {
                            html += `<span class="m-badge m-badge--success m-badge--wide font-size-11">Xe lắp định vị</span>`;
                        }
                        if (row.isTrackingLocation) {
                            html += `<span class="item-desciption">(KH chia sẻ vị trí)</span>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Tỉnh/ Thành',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.district != null)
                            text += "<p class='district'>" + row.district.name + "</p>";
                        if (row.province != null) {
                            text += "<p class='province'>" + row.province.name + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    width: 80,
                    textAlign: 'center',
                    sortable: true,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanBriefCancelAt',
                    title: 'Thời gian hủy',
                    width: 80,
                    textAlign: 'center',
                    sortable: true,
                    template: function (row) {
                        var date = moment(row.loanBriefCancelAt);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanBriefCancelBy',
                    title: 'Người thao tác',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.userCancel != null)
                            text += "<p style=\"color:#6C7293;\">" + row.userCancel.fullName + "</p>";
                        //if (row.userCancel != null) {
                        //    text += "<p class='province'>" + row.userCancel.groupId + "</p>";
                        //}
                        return text;
                    }
                },
                {
                    field: 'loanBriefComment',
                    title: 'Nội dung hủy',
                    width: 300,
                    textAlign: 'left',
                    sortable: false
                },
                {
                    field: 'action',
                    title: 'Hành động',
                    width: 160,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += `<a href="javascript:;" title="Khởi tạo lại đơn vay" onclick="LoanCancelModule.ReInitLoanbrief(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-undo"></i></a>`;


                        return html;
                    }
                },

            ]
        });
    }
    var Init = function () {        
        $("#filterAction").select2({
            placeholder: "Lý do hủy"
        });

        $("#filterProduct").select2({
            placeholder: "Sản phẩm"
        });        

        $("#filterProvince").select2({
            placeholder: "Tỉnh / Thành"
        });
        $("#filterDistrict").select2({
            placeholder: "Quận / Huyện"
        });

        //$('#filterTime').daterangepicker({
        //    buttonClasses: 'm-btn btn',
        //    applyClass: 'btn-primary',
        //    cancelClass: 'btn-secondary',
        //    startDate: moment().subtract(0, 'days'),
        //    endDate: moment(),
        //    locale: {
        //        cancelLabel: 'Clear',
        //        format: 'DD/MM/YYYY',
        //        firstDay: 1,
        //        daysOfWeek: [
        //            "CN",
        //            "T2",
        //            "T3",
        //            "T4",
        //            "T5",
        //            "T6",
        //            "T7",
        //        ],
        //        "monthNames": [
        //            "Tháng 1",
        //            "Tháng 2",
        //            "Tháng 3",
        //            "Tháng 4",
        //            "Tháng 5",
        //            "Tháng 6",
        //            "Tháng 7",
        //            "Tháng 8",
        //            "Tháng 9",
        //            "Tháng 10",
        //            "Tháng 11",
        //            "Tháng 12"
        //        ],
        //    },
        //});
        LoadData();
        //GetAction();
        GetProduct();       

        $("#btnSearch").click(function () {
            LoadData();
            return;          
        });
        
        $('#filterProduct,#filterAction,#filterProvince,#filterDistrict,#filterTime').on('change', function () {
            LoadData();
            return;
        });

    };

    var ReInitLoanbrief = function (loanBriefId) {

        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn khởi tạo lại đơn <b>HĐ-" + loanBriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/ReMarketing/ReInitLoanbriefId",
                    data: { LoanBriefId: loanBriefId },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                        });
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }
                        $.unblockUI();
                    },
                    traditional: true
                });

            }
        });
    }

    var LoadData = function () {
        var objQuery = {
            actionId: $("#filterAction").val(),
            productId: $('#filterProduct').val(),
            provinceId: $('#filterProvince').val(),
            //filtercreateTime: $('#filterTime').val(),
            districtId: $('#filterDistrict').val()
        }
        InitDataTable(objQuery);
    }
    var GetProduct = function () {
        App.Ajax("GET", "/Dictionary/GetLoanProduct", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    if (data.data[i].loanProductId == 2) {
                        html += "<option selected=\"selected\" value='" + data.data[i].loanProductId + "'>" + data.data[i].name + "</option>";
                    } else {
                        html += "<option value='" + data.data[i].loanProductId + "'>" + data.data[i].name + "</option>";
                    }
                   
                }
                $("#filterProduct").append(html);
                $("#filterProduct").select2({
                    placeholder: "Sản phẩm"
                });
            }
        });
    }
    var GetAction = function () {
        App.Ajax("GET", "/Dictionary/GetReasonCancel", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].id + "'>" + data.data[i].reason + "</option>";
                }
                $("#filterAction").append(html);
                $("#filterAction").select2({
                    placeholder: "Chọn lý do"
                });
            }
        });
    }
    var GetDistrict = function (provinceId, districtId) {
        if (provinceId > 0) {
            App.Ajax("GET", "/Dictionary/GetDistrict?id=" + provinceId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (districtId > 0 && districtId == data.data[i].districtId) {
                            html += `<option value="${data.data[i].districtId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].districtId}">${data.data[i].name}</option>`;
                        }
                    }
                    $("#filterDistrict").html(html);
                    $("#filterDistrict").select2({
                        placeholder: "Chọn Quận/ Huyện"
                    });
                }
            });
        } else {
            $("#filterDistrict").html("<option></option>");
            $('#filterDistrict').select2({
                placeholder: "Chọn Quận/ Huyện",
                width: '100%'
            });
        }
    }

    return {
        Init: Init,
        GetDistrict: GetDistrict,
        InitData: InitData,
        ReInitLoanbrief: ReInitLoanbrief,
        LoadData: LoadData
    };
}

