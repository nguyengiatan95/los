﻿var LoancreditFormModuleJS = new function () {
    var Init = function () {
        $('#sl_loan_product').select2({
            placeholder: "Chọn gói vay",
            width: '100%'
        }).on("change", function (e) {
            $(this).valid()
        });
        $('#sl_RateType').select2({
            placeholder: "Chọn hình thức tính lãi",
            width: '100%'
        });

        $('#sl_InfomationProductDetail').select2({
            placeholder: "Vui lòng chọn",
            width: '100%'
        });

        $('#sl_job_document').select2({
            placeholder: "Vui lòng chọn giấy tờ chứng minh công việc",
            width: '100%'
        });

        $('#sl_edit_loan_job_document').select2({
            placeholder: "Vui lòng chọn giấy tờ chứng minh công việc"
        });

        InitAutoCheckAppraiserAndJobDocument();
    };

    var ShowProductDetail = function () {
        $('#div-radio-edit-loan-appraiser').html('');
        $('#sl_edit_loan_job_document').html('');
        $('#div-edit-loan-job-document').attr('style', 'display: none;');
        $('#div-edit-loan-appraiser').attr('style', 'display: none;');
        var productId = $('#sl_loan_product').val();
        var typeOwnerShip = $('#sl_TypeOwnerShip').val();
        var typeLoanBrief = $('#typeCustomer').val();
        var productDetailId = $('#hdd_DetailProduct').val();
        if (productId == null || productId == '' || productId == undefined) {
            return false;
        }
        if (typeOwnerShip == null || typeOwnerShip == '0' || typeOwnerShip == undefined) {
            return false;
        }
        $.ajax({
            type: "GET",
            url: "/Dictionary/GetInfomationProductDetailByTypeOwnershipAndProduct",
            data: { TypeOwnership: typeOwnerShip, ProductId: productId, TypeLoanBrief: typeLoanBrief },
            success: function (data) {
                $('#sl_InfomationProductDetail').html('<option></option>');
                if (data.data != null && data.data.length > 0) {
                    if (productDetailId == ProductDetail.LO_KT1_KSHK_5tr || productDetailId == ProductDetail.LO_KT3_KTN_5tr || productDetailId == ProductDetail.LO_KT1_12tr || productDetailId == ProductDetail.LO_KT1_KSHK_12tr) {
                        for (var i = 0; i < data.data.length; i++) {
                            if (productDetailId == ProductDetail.LO_KT1_KSHK_5tr || productDetailId == ProductDetail.LO_KT3_KTN_5tr || productDetailId == ProductDetail.LO_KT1_12tr || productDetailId == ProductDetail.LO_KT1_KSHK_12tr) {
                                $('#sl_InfomationProductDetail').append('<option value="' + data.data[i].id + '" selected="selected">' + data.data[i].name + '</option>');
                            }
                            else {
                                $('#sl_InfomationProductDetail').append('<option value="' + data.data[i].id + '" selected="selected">' + data.data[i].name + '</option>');
                            }
                        }
                    }
                    else {
                        for (var i = 0; i < data.data.length; i++) {
                            if (data.data[i].id != ProductDetail.LO_KT1_KSHK_5tr && data.data[i].id != ProductDetail.LO_KT3_KTN_5tr && data.data[i].id != ProductDetail.LO_KT1_12tr && data.data[i].id != ProductDetail.LO_KT1_KSHK_12tr) {
                                $('#sl_InfomationProductDetail').append('<option value="' + data.data[i].id + '" selected="selected">' + data.data[i].name + '</option>');
                            }
                        }
                    }
                }
            },
            traditional: true
        });
    }

    var ShowInfomationProductDetail = function (productdetailId) {
        var productId = $('#sl_loan_product').val();
        $.ajax({
            type: "GET",
            url: "/Dictionary/GetInfomationProductDetailByProductDetailIdAndProduct",
            data: { ProductdetailId: productdetailId, ProductId: productId },
            success: function (res) {
                if (res.data != null) {
                    $('#div-radio-edit-loan-appraiser').html('');
                    $('#sl_edit_loan_job_document').html('');
                    if (res.data.appraiser != null) {
                        $('#div-radio-edit-loan-appraiser').html(res.data.appraiser);
                        $('#div-edit-loan-appraiser').removeAttr('style', 'display: none;');
                    }
                    else {
                        $('#div-edit-loan-appraiser').attr('style', 'display: none;');
                    }
                    if (res.data.jobDocument != null) {
                        $('#div-edit-loan-job-document').removeAttr('style', 'display: none;');
                        $('#sl_edit_loan_job_document').html(res.data.jobDocument);
                    }
                    else {
                        $('#div-edit-loan-job-document').attr('style', 'display: none;');
                    }

                    $('#hdd_Edit_Loan_MaxPrice').val(res.data.maxMoney);
                }
            },
            traditional: true
        });
        var typeRemarketing = $('#TypeRemarketing').val();
        //gói vay siêu nhanh
        var htmlRateType = '<option></option>';
        var htmlLoanTime = '<option></option>';
        if (LoanFastProductDetailId.includes(productdetailId) == true || productId == ProductCredit.OtoCreditType_CC) {
            htmlLoanTime += '<option value="6">6 tháng</option>\
                             <option value="12" selected="selected">12 tháng</option>';
            htmlRateType += '<option value="13" selected="selected">Dư nợ giảm dần</option>\
                             <option value="14" >Tất toán cuối kỳ</option>';
        } else if (typeRemarketing == TypeRemarketing.IsTopUp) {
            htmlLoanTime += '<option value="6">6 tháng</option>\
                             <option value="12" selected="selected">12 tháng</option>';
            htmlRateType += '<option value="13" selected="selected">Dư nợ giảm dần</option>';
        }
        else {
            htmlLoanTime += '<option value = "12" selected="selected">12 tháng</option>';
            htmlRateType += '<option value="13" selected="selected">Dư nợ giảm dần</option>';
        }
        $('#sl_RateType').html(htmlRateType);
        $('#sl_RateType').select2({
            placeholder: "Chọn hình thức tính lãi",
            width: '100%'
        });
        $('#sl_LoanTime').html(htmlLoanTime);
        $('#sl_LoanTime').select2({
            placeholder: "Chọn thời gian vay",
            width: '100%'
        });
    }

    var InitAutoCheckAppraiserAndJobDocument = function () {
        var appraiser = $('#hdd_Edit_Loan_Appraiser').val();
        var jobDocument = $('#hdd_Edit_Loan_DocumentBusiness').val();
        setTimeout(function () {
            if (appraiser > 0)
                $('input:radio[name="LoanBriefQuestionScriptModel.Appraiser"]').filter('[value=' + appraiser + ']').attr('checked', true);
            if (jobDocument > 0)
                $('#sl_edit_loan_job_document').val(jobDocument).change();
        }, 1000);
    }

    return {
        Init: Init,
        ShowProductDetail: ShowProductDetail,
        ShowInfomationProductDetail: ShowInfomationProductDetail,
        InitAutoCheckAppraiserAndJobDocument: InitAutoCheckAppraiserAndJobDocument
    }
}