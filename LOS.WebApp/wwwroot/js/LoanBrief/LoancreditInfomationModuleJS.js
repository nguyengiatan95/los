﻿var LoancreditInfomationModuleJS = new function () {
    var Init = function () {
        $('#sl_LoanTime').select2({
            placeholder: "Chọn thời gian vay",
            width: '100%'
        }).on("change", function (e) {
            $(this).valid()
        });
        $('#sl_Frequency').select2({
            placeholder: "Chọn chu kỳ đóng lãi",
            width: '100%'
        });
    };

    var ChangeLoanTime = function (val) {
        if (val > 0) {
            $('#txtTotalLoanTime').text(val);
            var finishLoan = new Date();
            finishLoan.setMonth(finishLoan.getMonth() + Number(val));
            $('#txtfinishLoan').text(formattedDate(finishLoan));

            //Calculator Rate
            var productId = $("#sl_loan_product").val();
            var loanAmount = $("#txtLoanAmount").val();

            if (productId != "" && loanAmount != "")
                CalculatorRateMoneyModuleJS.Init(productId, loanAmount, val);
            if (loanAmount != "" && val != "")
                CalculatorInsurranceModuleJS.Init(loanAmount, val);

            var productDetailId = $('#sl_InfomationProductDetail').val();
            //gói vay siêu nhanh: 1
            //Dư nợ giảm dần : 13
            if (productDetailId != undefined && productDetailId != null && productDetailId != '' && productDetailId == 1 && val == 12)
                $('#sl_RateType').val(13).change();
        }
    }

    var InfomationPayment = function (loanStatus, numberCard) {
        //Đơn GN thì xử lý. Không thì thôi
        if (loanStatus == 100) {
            $.ajax({
                type: "GET",
                url: '/LoanBrief/InfomationPaymentLMS',
                data: { NumberCard: numberCard },
                success: function (data) {
                    $('#div-infomation-payment').html('');
                    $('#div-infomation-payment').html(data);
                },
                traditional: true
            });
        }
    }

    return {
        Init: Init,
        ChangeLoanTime: ChangeLoanTime,
        InfomationPayment: InfomationPayment
    }
}