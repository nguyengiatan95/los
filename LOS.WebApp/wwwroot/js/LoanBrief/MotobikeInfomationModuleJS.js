﻿var MotobikeInfomationModuleJS = new function () {
    var Init = function () {
        $('#sl_brand_product').select2({
            placeholder: "Chọn hãng xe",
            width: '100%'
        });
        $('#sl_product_name').select2({
            placeholder: "Chọn tên xe",
            width: '100%'
        });
        
    };

    var GetProduct = function (brandId, productId) {
        if (brandId > 0) {
            App.Ajax("GET", "/Dictionary/GetProduct?brandId=" + brandId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (productId > 0 && productId == data.data[i].id) {
                            html += `<option value="${data.data[i].id}" selected="selected">${data.data[i].fullName}</option>`;
                        } else {
                            html += `<option value="${data.data[i].id}">${data.data[i].fullName}</option>`;
                        }
                    }
                    $("#sl_product_name").html(html);
                    $("#sl_product_name").select2({
                        placeholder: "Chọn tên xe"
                    });
                }
            });
        } else {
            $('#sl_product_name').select2({
                placeholder: "Chọn tên xe",
                width: '100%'
            });
        }
    }

    return {
        Init: Init,
        GetProduct: GetProduct
    }
}