﻿var PersonalInformationModuleJS = new function () {
    var Init = function () {
        $('#sl_PlatformType').select2({
            placeholder: "Chọn nguồn đơn",
            width: '100%'
        }).on("change", function (e) {
            $(this).valid()
        });
        $('#typeCustomer').select2({
            placeholder: "Chọn loại đơn",
            width: '100%'
        });

        $('#sl_BoundTelesaleId').select2({
            placeholder: "Chọn Telesales",
            width: '100%'
        });
        
        $('#birthDayCus').datepicker({
            todayBtn: "linked",
            clearBtn: false,
            todayHighlight: true,
            orientation: "bottom left",
            format: 'dd/mm/yyyy',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
        if ($('#sl_PlatformType').val() == PLATFORM_PRESENTER) {
            $('#PresenterCode').css('display', 'block');
        }
        if ($('#hdd_GroupId').val() == GROUP_MANAGER_TELES) {
            GetUserTelesales();
        }
    };

    var ChangeTypeCustomer = function (val) {
        if (val == 1) {
            $('#boxCompanyInfo').css('display', 'none');
        } else {
            $('#boxCompanyInfo').css('display', 'block');
        }
        LoancreditFormModuleJS.ShowProductDetail();
    };

    var ChangePlatformType = function (type) {
        if (type == PLATFORM_PRESENTER) {
            $('#PresenterCode').css('display', 'block');
        } else {
            $('#PresenterCode').css('display', 'none');
        }
    }

    var GetUserTelesales = function () {
        App.Ajax("GET", "/Dictionary/GetUserByGroupId?groupId=" + 2, undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                html += '<option value="0">Chọn Telesales</option>'
                for (var i = 0; i < data.data.length; i++) {
                    if (data.data[i].status == 1)
                        html += "<option value='" + data.data[i].userId + "'>" + data.data[i].username + "</option>";
                }
                $("#sl_BoundTelesaleId").append(html);
                $("#sl_BoundTelesaleId").select2({
                    placeholder: "Chọn Telesales"
                });
            }
        });
    };


    return {
        Init: Init,
        ChangeTypeCustomer: ChangeTypeCustomer,
        ChangePlatformType: ChangePlatformType,
        GetUserTelesales: GetUserTelesales
    }
}