﻿var ReLoanModule = new function () {
    var CheckReLoan = function () {
        var name = $("#_CheckName").val();
        var numberCard = $("#_CheckNumberCard").val();
        if (name == "" || numberCard == "") {
            App.ShowErrorNotLoad('Bạn phải nhập đủ thông tin');
            return;
        }
        //Call api LMS
        $.ajax({
            type: "POST",           
            url: "/LoanBrief/CheckReLoan",
            data: { FullName: name, NumberCard: numberCard },
            success: function (data) {
                if (data.status == 1) {
                    //ReLoanInfo(data.loanBriefId);
                    InitLoanBrief.Create(data.loanBriefId,1);
                } else {
                    App.ShowErrorNotLoad(data.message);
                    return;
                }
            }
        });

    };

    var ReLoanInfo = function (loanBriefId) {
        $('#ReLoanBody').html('');
        $('#modelInitLoanBrief').css('display:none');
        $.ajax({
            type: 'GET',
            url: '/LoanBrief/ReLoanInfo?LoanBriefId=' + loanBriefId,
            dataType: 'json',
            success: function (data) {
                if (data.isSuccess == 1) {
                    
                    $('#ReLoanBody').html(data.html);
                    $('#modelReLoan').modal('show');
                } else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });

    }


    return {
        CheckReLoan: CheckReLoan,
        ReLoanInfo: ReLoanInfo
    };
}
