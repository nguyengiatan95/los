﻿var ReasonCancelModuleJS = new function () {
    var Init = function () {
        $('#sl_ReasonGroup').select2({
            placeholder: "Vui lòng chọn nhóm lý do",
            width: '100%'
        }).on("change", function (e) {
            $(this).valid()
        });
        $("#sl_reason_detail").select2({
            placeholder: "Vui lòng chọn lý do",
            width: '100%'
        }).on("change", function (e) {
            $(this).valid()
        });
    }
    var GetReasonDetail = function (groupId, reasonId) {
        if (groupId > 0) {
            App.Ajax("GET", "/LoanBrief/GetReasonDetail?groupId=" + groupId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (reasonId > 0 && reasonId == data.data[i].id) {
                            html += `<option value="${data.data[i].id}" selected="selected">${data.data[i].reason}</option>`;
                        } else {
                            html += `<option value="${data.data[i].id}">${data.data[i].reason}</option>`;
                        }
                    }
                    $("#sl_reason_detail").html(html);
                    $("#sl_reason_detail").select2({
                        placeholder: "Vui lòng chọn lý do"
                    });
                }
            });
        } else {
            $('#sl_ReasonGroup').select2({
                placeholder: "Vui lòng chọn nhóm lý do",
                width: '100%'
            });
        }
    }

    var SubmitForm = function (e) {
        e.preventDefault();
        var form = $('#formCancelLoanBrief');
        form.validate({
            ignore: [],
            rules: {
                'ReasonGroup': {
                    required: true
                },
                'ReasonDetail': {
                    required: true
                }
            },
            messages: {
                'ReasonGroup': {
                    required: "Vui lòng chọn nhóm lý do",
                    class: "has-danger"
                },
                'ReasonDetail': {
                    required: "Vui lòng chọn lý do",
                    class: "has-danger"
                }
            },
            errorPlacement: function (error, element) {
                if (element.hasClass('m-select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                    error.insertAfter(element.parent().parent());
                }
                else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.appendTo(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        var textComment = "";
        var groupCancel = $('#sl_ReasonGroup option:selected').text();
        if (groupCancel != undefined && groupCancel != null && groupCancel != "") {
            textComment += "<b>Nhóm lý do</b> : " + groupCancel;
        }
        var detailGroupCancel = $('#sl_reason_detail option:selected').text();
        if (detailGroupCancel != undefined && detailGroupCancel != null && detailGroupCancel != "") {
            textComment += "<br /><b>Lý do chi tiết</b> : " + detailGroupCancel;
        }
        var comment = $('#txtComment').val();
        if (comment != undefined && comment != null && comment != "") {
            textComment += "<br /><b>Comment</b> : " + comment;
        }
        $('#txtCommentCancel').val(textComment);
        $('#btnSubmit').attr('disabled', true);
        form.ajaxSubmit({
            url: '/LoanBrief/CancelLoanBrief',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#modelCancelLoanBrief').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        App.ReloadData();
                    }
                    else {
                        App.ShowError(data.message);
                    }
                }
                else {
                    App.ShowError(data.message);
                }
                $('#btnSubmit').attr('disabled', false);
            }
        });
    }
    return {
        Init: Init,
        GetReasonDetail: GetReasonDetail,
        SubmitForm: SubmitForm
        //InitFunc: InitFunc
    }
}