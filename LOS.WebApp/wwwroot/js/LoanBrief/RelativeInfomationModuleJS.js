﻿var RelativeInfomationModuleJS = new function () {
    var Init = function () {
        $('#sl_relative_family').select2({
            placeholder: "Chọn mối quan hệ",
            width: '100%'
        });
        $('#sl_relative_family2').select2({
            placeholder: "Chọn mối quan hệ",
            width: '100%'
        });
        $('#sl_relative_family3').select2({
            placeholder: "Chọn mối quan hệ",
            width: '100%'
        });
        $('#sl_relative_family4').select2({
            placeholder: "Chọn mối quan hệ",
            width: '100%'
        });
        $('#sl_relative_house_owner').select2({
            placeholder: "Chọn mối quan hệ",
            width: '100%'
        });

        InitInfomationHouseOwner();

    };
    var ChangeInfomationHouseOwner = function (houseOwner) {
        if (houseOwner == 'false') {
            $('.infomationHouseOwner').removeAttr('style', 'display: none;');
        }
        else {
            $('.infomationHouseOwner').attr('style', 'display: none;');
        }
    }
    var InitInfomationHouseOwner = function () {
        var houseOwner = $("input[name='LoanBriefQuestionScriptModel.QuestionHouseOwner']:checked").val();
        if (houseOwner == 'false') {
            $('.infomationHouseOwner').removeAttr('style', 'display: none;');
        }
        else {
            $('.infomationHouseOwner').attr('style', 'display: none;');
        }
    }

    return {
        Init: Init,
        ChangeInfomationHouseOwner: ChangeInfomationHouseOwner,
        InitInfomationHouseOwner: InitInfomationHouseOwner
    }
}