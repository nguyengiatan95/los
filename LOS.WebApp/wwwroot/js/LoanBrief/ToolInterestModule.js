﻿var ToolInterestModule = new function () {
    var Init = function () {
        ChangeCalculateOften();
        $("#sl_LoanTimeTopup").select2({

        });
    };

    var CalculateOften = function () {
        var form = $('#formInterest');
        $('#LoanAmount').val($('#LoanAmount').val().replace(/,/g, ''));
        $('#RateMoney').val($('#RateMoney').val().replace(/,/g, ''));
        var rate = ($('#RateMoney').val() * 365 / 1000000) * 100;
        $('#Rate').val(rate.toFixed(2));
        form.ajaxSubmit({
            url: "/Approve/ViewIntersestSubmit",
            method: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            success: function (data) {
                $.unblockUI();
                $('#LoanAmount').val(App.FormatCurrency($('#LoanAmount').val()));
                $('#RateMoney').val(App.FormatCurrency($('#RateMoney').val())); 
                $("#div-datatable").html('');
                $('#div-datatable').append(data.html);
            }
        });
    };

    var CalculateTopup = function () {
        var form = $('#formInterestLoanTopup');
        $('#txt_TotalMoneyDisbursementTopup').val($('#txt_TotalMoneyDisbursementTopup').val().replace(/,/g, ''));
        $('#txt_CodeId').val($('#txt_CodeId').val().toLowerCase().replace('tc-', ''));
        if ($('#txt_TotalMoneyDisbursementTopup').val() != '0') {
            form.ajaxSubmit({
                url: "/Approve/ViewIntersestSubmit",
                method: "POST",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    });
                },
                success: function (data) {
                    $.unblockUI();
                    if (data.isSuccess == false) {
                        App.ShowErrorNotLoad(data.message);
                    }
                    else {
                        $('#LoanAmount').val(App.FormatCurrency($('#LoanAmount').val()));
                        $('#RateMoney').val(App.FormatCurrency($('#RateMoney').val()));
                        $("#div-datatable").html('');
                        $('#div-datatable').append(data.html);
                    }
                }
            });
        }
        else {
            App.ShowErrorNotLoad('Vui lòng nhập số tiền vay');
            return;
        }
        
    }

    var ChangeCalculationInterest = function (calculationInterest) {
        if (calculationInterest == 1) {
            $('#div-InterestLoanOften').removeAttr('style', 'display: none;');
            $('#div-InterestLoanTopup').attr('style', 'display: none;');
            $('#hdd_CalculationInterestOften').val('true');
            $('#hdd_CalculationInterestTopup').val('false');
            $('#div-datatable').html('');
        }
        else if (calculationInterest == 2) {
            $('#div-InterestLoanOften').attr('style', 'display: none;');
            $('#div-InterestLoanTopup').removeAttr('style', 'display: none;');
            $('#hdd_CalculationInterestOften').val('false');
            $('#hdd_CalculationInterestTopup').val('true');
            $('#div-datatable').html('');
        }
    };

    var ChangeCalculateOften = function () {
        $('#div-InterestLoanOften').removeAttr('style', 'display: none;');
        $('#div-InterestLoanTopup').attr('style', 'display: none;');
        $('#hdd_CalculationInterestOften').val(true);
        $('#hdd_CalculationInterestTopup').val('false');
        $('#div-datatable').html('');
    };

    return {
        CalculateOften: CalculateOften,
        ChangeCalculationInterest: ChangeCalculationInterest,
        ChangeCalculateOften: ChangeCalculateOften,
        Init: Init,
        CalculateTopup: CalculateTopup
    }
}