﻿var WorkInformationModuleJS = new function () {
    var Init = function () {
        $('#sl_jobId').select2({
            placeholder: "Chọn nghề nghiệp",
            width: '100%'
        });

        $('#sl_JobDescriptionId').select2({
            placeholder: "Chọn ngành nghề",
            width: '100%'
        });

        $('#sl_province_work_company').select2({
            placeholder: "Vui lòng chọn Tỉnh/ Thành phố",
            width: '100%'
        });

        $('#sl_district_work_company').select2({
            placeholder: "Vui lòng chọn Quận/ Huyện",
            width: '100%'
        });

        $('#sl_work_ReceivingMoney').select2({
            placeholder: "Vui lòng chọn Hình thức",
            width: '100%'
        });

        $("#sl_ward_work_company").select2({
            placeholder: "Vui lòng chọn Phường/ Xã",
            width: '100%'
        });

        InitTypeJob();
    }

    var GetDistrict = function (provinceId, districtId) {
        if (provinceId > 0) {
            App.Ajax("GET", "/Dictionary/GetDistrict?id=" + provinceId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (districtId > 0 && districtId == data.data[i].districtId) {
                            html += `<option value="${data.data[i].districtId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].districtId}">${data.data[i].name}</option>`;
                        }
                    }
                    $("#sl_district_work_company").html(html);
                    $("#sl_district_work_company").select2({
                        placeholder: "Vui lòng chọn Quận/ Huyện"
                    });
                }
            });
        } else {
            $('#sl_district_work_company').select2({
                placeholder: "Vui lòng chọn Quận/ Huyện",
                width: '100%'
            });
        }
    }

    var GetWard = function (districtId, wardId) {
        if (districtId > 0) {
            App.Ajax("GET", "/Dictionary/GetWard?districtId=" + districtId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (wardId > 0 && wardId == data.data[i].wardId) {
                            html += `<option value="${data.data[i].wardId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].wardId}">${data.data[i].name}</option>`;
                        }
                    }
                    $("#sl_ward_work_company").html(html);
                    $("#sl_ward_work_company").select2({
                        placeholder: "Vui lòng chọn Phường/ Xã"
                    });
                }
            });
        } else {
            $('#sl_ward_work_company').select2({
                placeholder: "Vui lòng chọn Phường/ Xã",
                width: '100%'
            });
        }
    }

    var ChangeTypeJob = function (jobId) {
        if (jobId == 123 || jobId == 126) {
            var jobDescriptionId = $('#hdd_JobDescriptionId').val();
            $.ajax({
                type: "GET",
                url: "/LoanBriefV3/GetJobByParentId",
                data: { ParentId: jobId },
                success: function (data) {
                    if (data.data.length > 0) {
                        $('#sl_JobDescriptionId').html('');
                        $('#sl_JobDescriptionId').append('<option value="' + 0 + '">' + 'Chọn ngành nghề' + '</option>');
                        for (var i = 0; i < data.data.length; i++) {
                            if (data.data[i].jobId == jobDescriptionId)
                                $('#sl_JobDescriptionId').append('<option selected="selected" value="' + data.data[i].jobId + '">' + data.data[i].name + '</option>');
                            else
                                $('#sl_JobDescriptionId').append('<option value="' + data.data[i].jobId + '">' + data.data[i].name + '</option>');
                        }
                    }
                },
                traditional: true
            });
        }
        //Đi làm hưởng lương
        if (jobId == 123) {
            $('#lbl_ImcomeType').removeAttr('style', 'display: none;');
            $('#div_ImcomeType').removeAttr('style', 'display: none;');
            $('#lbl_CompanyInsurance').removeAttr('style', 'display: none;');
            $('#div_CompanyInsurance').removeAttr('style', 'display: none;');
            $('#lbl_WorkLocation').attr('style', 'display: none;');
            $('#div_WorkLocation').attr('style', 'display: none;');
            $('#lbl_BusinessPapers').attr('style', 'display: none;');
            $('#div_BusinessPapers').attr('style', 'display: none;');
            $('#div-nganhnghe').removeAttr('style', 'display: none;');
        }
        //Tự kinh doanh
        else if (jobId == 124) {
            $('#lbl_BusinessPapers').removeAttr('style', 'display: none;');
            $('#div_BusinessPapers').removeAttr('style', 'display: none;');
            $('#lbl_WorkLocation').attr('style', 'display: none;');
            $('#div_WorkLocation').attr('style', 'display: none;');
            $('#lbl_ImcomeType').attr('style', 'display: none;');
            $('#div_ImcomeType').attr('style', 'display: none;');
            $('#lbl_CompanyInsurance').attr('style', 'display: none;');
            $('#div_CompanyInsurance').attr('style', 'display: none;');
            $('#div-nganhnghe').attr('style', 'display: none;');
        }
        //Làm tự do
        else if (jobId == 126) {
            $('#lbl_WorkLocation').removeAttr('style', 'display: none;');
            $('#div_WorkLocation').removeAttr('style', 'display: none;');
            $('#lbl_BusinessPapers').attr('style', 'display: none;');
            $('#div_BusinessPapers').attr('style', 'display: none;');
            $('#lbl_ImcomeType').attr('style', 'display: none;');
            $('#div_ImcomeType').attr('style', 'display: none;');
            $('#lbl_BusinessPapers').attr('style', 'display: none;');
            $('#div_BusinessPapers').attr('style', 'display: none;');
            $('#div-nganhnghe').removeAttr('style', 'display: none;');
        }

        else {
            $('#lbl_WorkLocation').attr('style', 'display: none;');
            $('#div_WorkLocation').attr('style', 'display: none;');
            $('#lbl_BusinessPapers').attr('style', 'display: none;');
            $('#div_BusinessPapers').attr('style', 'display: none;');
            $('#lbl_CompanyInsurance').attr('style', 'display: none;');
            $('#div_CompanyInsurance').attr('style', 'display: none;');
            $('#lbl_ImcomeType').attr('style', 'display: none;');
            $('#div_ImcomeType').attr('style', 'display: none;');
            $('#div-nganhnghe').attr('style', 'display: none;');
        }
    }

    var InitNganhNghe = function (jobId) {
        if (jobId == 123 || jobId == 126) {
            $('#div-nganhnghe').removeAttr('style', 'display: none;');
        }
        else {
            $('#div-nganhnghe').attr('style', 'display: none;');
        }
    }

    var InitTypeJob = function () {
        var jobId = $('#sl_jobId').val();
        if (jobId == 123 || jobId == 126) {
            var jobDescriptionId = $('#hdd_JobDescriptionId').val();
            $.ajax({
                type: "GET",
                url: "/LoanBriefV3/GetJobByParentId",
                data: { ParentId: jobId },
                success: function (data) {
                    if (data.data.length > 0) {
                        $('#sl_JobDescriptionId').html('');
                        $('#sl_JobDescriptionId').append('<option value="' + 0 + '">' + 'Chọn ngành nghề' + '</option>');
                        for (var i = 0; i < data.data.length; i++) {
                            if (data.data[i].jobId == jobDescriptionId)
                                $('#sl_JobDescriptionId').append('<option selected="selected" value="' + data.data[i].jobId + '">' + data.data[i].name + '</option>');
                            else
                                $('#sl_JobDescriptionId').append('<option value="' + data.data[i].jobId + '">' + data.data[i].name + '</option>');
                        }
                    }
                },
                traditional: true
            });
        }
    }

    return {
        Init: Init,
        GetDistrict: GetDistrict,
        GetWard: GetWard,
        ChangeTypeJob: ChangeTypeJob,
        InitNganhNghe: InitNganhNghe,
    }
}