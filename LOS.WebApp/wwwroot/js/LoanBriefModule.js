﻿var LoanBriefModule = new function () {
    var _groupGlobal;
    var _typeServiceCall;
    var datatable;

    var InitData = function (group, typeServiceCall) {
        _groupGlobal = group;
        _typeServiceCall = typeServiceCall;
    }

    var InitDataTable = function (objQuery) {
        $('#dtLoanBrief').html('');
        datatable = $('#dtLoanBrief').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/LoanBrief/Search',
                        params: {
                            query: objQuery
                        },
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        }
                    },
                },
                pageSize: 50,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false,
                header: true
            },
            // column sorting
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    title: '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input onclick="CheckAll();" id="checkAll" type="checkbox"><span></span></label></span>',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    //visible: false,
                    template: function (row) {
                        var html = '';
                        if (LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1)
                            return html;
                        if ((_groupGlobal == GROUP_MANAGER_TELES || _groupGlobal == GROUP_TELES) && LIST_PERMISSION_PUSH_TELE.indexOf(row.status) > -1) {
                            html += '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" class="checkbox-item"  value="' + row.loanBriefId + '" name="checkbox"><span></span></label></span>';
                        }
                        //Nếu là cskh và là đơn hub tạo

                        else if (row.hub != null && row.hub.shopId > 0 && LIST_PERMISSION_PUSH_TELE.indexOf(row.status) > -1) {
                            html += '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" class="checkbox-item" value="' + row.loanBriefId + '" name="checkbox"><span></span></label></span>';
                        }

                        return html;
                    }
                },
                {
                    field: 'stt',
                    title: 'STT',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },

                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        //hotlead chưa phản hồi
                        if (row.typeHotLead == 1) {
                            html += `<a style="color:red !important" class="loanBriefId" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(${row.loanBriefId})">HĐ-${row.loanBriefId}</a>`;// '<span style="color:red !important" class="loanBriefId">HĐ-' + row.loanBriefId + '</span>';
                        } else {
                            html += `<a class="loanBriefId" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(${row.loanBriefId})">HĐ-${row.loanBriefId}</a>`;
                        }

                        if (row.hubName != null) {
                            html += `<br /><span class="item-desciption">(${row.hubName})</span>`;
                        }
                        if (row.utmSource != null && row.utmSource != "") {
                            html += "<br /><p style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);margin-bottom: 0px;'>" + row.utmSource + "</p>";
                        }
                        if (row.isReborrow) {
                            html += `<br /><span class="item-desciption">(Tái vay)</span>`;
                        } else {
                            if (row.utmSource == "rmkt_loan_finish") {
                                html += `<br /><span class="item-desciption">(Vay lại)</span>`;
                            }
                        }
                        //đơn topup
                        if (row.typeRemarketing == TypeRemarketing.IsTopUp) {
                            html += '<br /><span class="item-desciption">(Topup)</span>';
                        }
                        //Check thời xử lý đơn vay
                        if ((row.status == LoanStatus.WAIT_CUSTOMER_PREPARE_LOAN || row.status == LoanStatus.TELESALE_ADVICE) && row.timeProcessingLoan != null) {
                            //có phản hồi nhưng trên 30 phút
                            if (row.timeProcessingLoan.isFeedBack == true && row.timeProcessingLoan.timeProcessing > 30)
                                html += '<br /><span style="color: red; font-size: 11px;">Phản hồi muộn ' + row.timeProcessingLoan.timeProcessing + ' phút</span>';
                            else if (row.timeProcessingLoan.isFeedBack == false && row.timeProcessingLoan.timeProcessing > 30)
                                html += '<br /><span style="color: red; font-size: 11px;">Chưa phản hồi</span>';
                            else if (row.timeProcessingLoan.isFeedBack == false && row.timeProcessingLoan.timeProcessing <= 30) {
                                html += '<br /><span style="color: #fdc502; font-size: 11px;" id="_timeProcessing_' + row.loanBriefId + '"></span>';
                                var minutes = parseInt(30 - row.timeProcessingLoan.timeProcessing);
                                if (minutes > 0) {
                                    var _spanId = "#_timeProcessing_" + row.loanBriefId;
                                    CommonModalJS.CountDown(minutes * 60, _spanId);
                                }
                            }
                        }
                        return html;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    textAlign: 'center',
                    //width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = '<a class="fullname" id="linkName" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a><br />';
                        //<span class="item-desciption">(Tái vay)</span>
                        if (row.countCall > 0)
                            html += `<span class="item-desciption countcall">(Đã gọi ${row.countCall} lần)</span><br />`
                        else
                            html += `<span class="item-desciption countcall">(Chưa gọi)</span><br />`

                        if (row.uploadImage == true) {
                            html += '<span title="Có chứng từ"><i class="fa fa-file-picture-o" style="color: rebeccapurple;"></i></span>'
                        }
                        if (row.tId != null && row.tId != '' && row.tId != undefined) {
                            html += `<span class="item-desciption">(AFF, MMO)</span><br />`
                        }
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Địa chỉ',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.districtName != null)
                            text += "<p class='district'>" + row.districtName + "</p>";
                        if (row.provinceName != null) {
                            text += "<p class='province'>" + row.provinceName + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    textAlign: 'center',
                    width: 80,
                    sortable: true,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return '<span class="datetime">' + date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm") + '</span>';
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = `<span class="money">${App.FormatCurrency(row.loanAmount)}</span>`
                        if (row.productName != null)
                            html += `<span class="item-desciption">${row.productName}</span>`;
                        if (row.isLocate) {
                            html += `<span class="m-badge m-badge--success m-badge--wide font-size-11">Xe lắp định vị</span>`;
                        }
                        if (row.isTrackingLocation) {
                            html += `<span class="item-desciption">(KH chia sẻ vị trí)</span>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    width: 150,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = "";
                        if (row.typeHotLead == 1) {
                            html += "<span class='m-badge m-badge--warning m-badge--wide'>Chưa liên hệ</span>";
                            html += `<span style="color:red !important"  class="item-desciption-11">(Hotlead)</span>`;
                            //hiển thị tên teles phụ trách
                            if (_groupGlobal == GROUP_MANAGER_TELES) {
                                if (row.userTelesales != null) {
                                    html += `<span class="item-desciption-11">tls: ${row.userTelesales}</span>`;
                                }
                            }
                        }
                        else if (row.status == LoanStatus.TELESALE_ADVICE && row.statusTelesales == null) {
                            html += "<span class='m-badge m-badge--warning m-badge--wide'>Chưa liên hệ</span>";
                            //hiển thị tên teles phụ trách
                            if (_groupGlobal == GROUP_MANAGER_TELES) {
                                if (row.userTelesales != null) {
                                    html += `<span class="item-desciption-11">tls: ${row.userTelesales}</span>`;
                                }
                            }
                        }
                        else if (row.status == LoanStatus.WAIT_CUSTOMER_PREPARE_LOAN && row.statusTelesales == null) {
                            html += "<span class='m-badge m-badge--info m-badge--wide'>Chờ chuẩn bị hồ sơ</span>";
                            //hiển thị tên teles phụ trách
                            if (_groupGlobal == GROUP_MANAGER_TELES) {
                                if (row.userTelesales != null) {
                                    html += `<span class="item-desciption-11">tls: ${row.userTelesales}</span>`;
                                }
                            }
                        }
                        else if (row.status == LoanStatus.CANCELED) {
                            html += "<span class='m-badge m-badge--danger m-badge--wide'>Đơn đã hủy</span><br />";
                        }
                        //đơn đã GN , đã đóng HĐ
                        else if ((row.status == LoanStatus.DISBURSED || row.status == LoanStatus.FINISH) && App.ArrLoanStatus[row.status] != null) {
                            return '<span class="m-badge ' + App.ArrLoanStatus[row.status].class + ' m-badge--wide">' + App.ArrLoanStatus[row.status].title + '</span>'
                        }
                        else if ((row.status == LoanStatus.TELESALE_ADVICE || row.status == LoanStatus.WAIT_CUSTOMER_PREPARE_LOAN) && row.statusTelesales != null) {
                            //Không liên hệ được
                            if (row.statusTelesales == StatusTelesales.NoContact) {
                                html += "<span class='m-badge m-badge--info m-badge--wide'>Không liên hệ được</span><br />";
                            }
                            //Cân nhắc
                            else if (row.statusTelesales == StatusTelesales.Consider) {
                                html += "<span class='m-badge m-badge--info m-badge--wide'>Cân nhắc</span><br />";
                            }
                            //Chờ chuẩn bị hồ sơ
                            else if (row.statusTelesales == StatusTelesales.WaitPrepareFile) {
                                html += "<span class='m-badge m-badge--info m-badge--wide'>Chờ chuẩn bị hồ sơ</span><br />";
                            }
                            //Hẹn gọi lại
                            else if (row.statusTelesales == StatusTelesales.SeeLater) {
                                html += "<span class='m-badge m-badge--info m-badge--wide'>Hẹn gọi lại</span><br />";
                            }
                            //Hiển thị trạng thái chi tiết
                            if (App.ArrDetailStatusTelesales[row.detailStatusTelesales] != null) {
                                html += '<span class="' + App.ArrDetailStatusTelesales[row.detailStatusTelesales].class + '">' + App.ArrDetailStatusTelesales[row.detailStatusTelesales].title + '</span>';
                                if (App.ArrPipelineState[row.pipelineState] != null && LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1) {
                                    html += `<span class="item-desciption-11">${App.ArrPipelineState[row.pipelineState].title}</span>`;
                                }
                                //hiển thị tên teles phụ trách
                                if (_groupGlobal == GROUP_MANAGER_TELES) {
                                    if (row.userTelesales != null) {
                                        html += `<span class="item-desciption-11">tls: ${row.userTelesales}</span>`;
                                    }
                                }
                            }
                        }
                        else if (row.status != null && App.ArrLoanStatus[row.status] != null) {
                            html += '<span class="m-badge ' + App.ArrLoanStatus[row.status].class + ' m-badge--wide">' + App.ArrLoanStatus[row.status].title + '</span>'
                            if (App.ArrPipelineState[row.pipelineState] != null && LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1) {
                                html += `<span class="item-desciption-11">${App.ArrPipelineState[row.pipelineState].title}</span>`;
                            }
                        }
                        else {
                            return 'Không xác định';
                        }

                        //Thên nhãn hub trả lại đơn
                        if (row.telesalesPushAt != null && (row.status == LoanStatus.TELESALE_ADVICE || row.status == LoanStatus.WAIT_CUSTOMER_PREPARE_LOAN)) {
                            html += `<span class="item-desciption-11" style="color:red">(Hub trả lại)</span>`;
                        }
                        if (row.step > 0) {
                            html += `<span class="item-desciption-11">(step: ${row.step})</span>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'scheduleTime',
                    title: 'Lịch hẹn',
                    textAlign: 'center',
                    width: 80,
                    visible: true,
                    template: function (row) {
                        if (row.scheduleTime != null && row.scheduleTime != undefined) {
                            var date = moment(row.scheduleTime);
                            return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                        }
                    }
                },
                {
                    field: 'LastChangeStatusTelesale',
                    title: 'Thời gian tác động',
                    textAlign: 'center',
                    width: 80,
                    visible: true,
                    template: function (row) {
                        if (row.lastChangeStatusTelesale != null && row.lastChangeStatusTelesale != undefined) {
                            var date = moment(row.lastChangeStatusTelesale);
                            return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                        }
                    }
                },
                {
                    field: 'call',
                    title: 'Gọi',
                    width: 80,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        //gọi qua caresoft
                        var html = '';
                        if (_typeServiceCall == 1) {
                            html = '<a href="javascript:;" onclick="CareSoftModule.CallCareSoft(' + row.loanBriefId + ')" class="box-call">\
                                    <i class="fa fa-phone item-icon-phone" ></i></a>';
                        } else if (_typeServiceCall == 2) {// gọi qua metech
                            html = `<a href="javascript:;" onclick="finesseC2CModule.ClickToCall('${row.phone}',${row.loanBriefId})" class="box-call">
                                    <i class="fa fa-phone item-icon-phone" ></i></a>`;
                        }
                        if (row.homeNetwork == null || row.homeNetwork == 0) {
                            var dauso = row.phone.substring(0, 3);
                            if (Vina.includes(dauso))
                                html += '<span class="network-home">Vina</span>'
                            else if (Mobi.includes(dauso))
                                html += '<span class="network-home">Mobi</span>'
                            else if (Viettel.includes(dauso))
                                html += '<span class="network-home">Viettel</span>'
                            else
                                html += '<span class="network-home">Không xác định</span>'
                        }
                        else {
                            if (row.homeNetwork == 3)
                                html += '<span class="network-home">Vina</span>'
                            else if (row.homeNetwork == 2)
                                html += '<span class="network-home">Mobi</span>'
                            else if (row.homeNetwork == 1)
                                html += '<span class="network-home">Viettel</span>'
                            else
                                html += '<span class="network-home">Không xác định</span>'
                        }


                        return html;
                    }
                },
                {
                    field: 'resultRuleCheck',
                    title: 'Rule Check',
                    sortable: false,
                    width: 100,
                    textAlign: 'center',
                    template: function (row) {
                        var text = "";
                        if (row.resultRuleCheck != null && row.resultRuleCheck != "")
                            text += `<span class="m-badge m-badge--success m-badge--wide font-size-11">${row.resultRuleCheck}</span>`;
                        else {
                            text += `<span class="m-badge m-badge--success m-badge--danger font-size-11">Chưa có KQ</span>`
                        }
                        return text;
                    }
                },
                {
                    field: 'action',
                    title: 'Hành động',
                    width: 160,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (row.boundTelesaleId != User.recare && row.boundTelesaleId != User.recare_team) {
                            //trạng thái trên hub
                            if (row.status != LoanStatus.APPRAISER_REVIEW && row.status != LoanStatus.HUB_CHT_LOAN_DISTRIBUTING && row.status != LoanStatus.HUB_CHT_APPROVE && row.status != LoanStatus.BRIEF_APPRAISER_LOAN_DISTRIBUTING
                                && row.status != LoanStatus.BRIEF_APPRAISER_REVIEW && row.status != LoanStatus.BRIEF_APPRAISER_APPROVE_PROPOSION && row.status != LoanStatus.BRIEF_APPRAISER_APPROVE_CANCEL) {
                                if (LIST_WAIT_PIPELINE_RUN.indexOf(row.pipelineState) > -1)
                                    return html;
                                if (row.isEdit) {
                                    html += `<a href="javascript:;" title="Cập nhật đơn vay" onclick="InitLoanBrief.Create(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							                <i class="la la-edit"></i></a>`;
                                }
                                if (row.isScript) {
                                    html += `<a href="javascript:;" title="Kịch bản đơn vay"  onclick="InitLoanBrief.ScriptTelesale(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							                <i class="fa fa-book"></i></a>`;
                                }
                                html += `<a data-toggle="modal" title="Chuyển trạng thái" onclick="CommonModalJS.InitStatusTelesalesModal('#modalStatusTelesales',${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							               <i class="fa fa-share-square-o"></i></a>`;

                                //         if (row.isTrackingLocation) {
                                //             html += `<a href="javascript:;" title="Lấy thông tin location Sim" onclick="CommonModalJS.ReGetLocationSim(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
                                //<i class="fa fa-globe"></i></a>`;
                                //         }

                                if (row.isPush) {
                                    if (row.status == WAIT_CUSTOMER_PREPARE_LOAN && (row.productId == DK_OTO || row.productId == CAM_OTO)) {
                                        html += `<a href="javascript:;" title="Đẩy đơn vay" onclick="LoanBriefModule.PushLoanAndChooseHub(${row.loanBriefId},${row.provinceId} )" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							                    <i class="fa fa-check"></i></a>`;
                                    } else {
                                        html += `<a href="javascript:;" title="Đẩy đơn vay" onclick="CommonModalJS.ConfirmLoanBrief(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							                    <i class="fa fa-check"></i></a>`;
                                    }
                                }
                                if (row.isUpload) {
                                    html += `<a href="javascript:;" title="Upload chứng từ" onclick="CommonModalJS.InitUploadModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							                 <i class="fa fa-upload"></i></a>`;
                                }
                                html += `<a href="javascript:;" title="Nhật ký khoản vay" onclick="CommonModalJS.InitDiaryModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							             <i class="fa fa-wpforms"></i></a>`;
                                if (row.productId == ProductCredit.OtoCreditType_CC) {
                                    if (row.isCancel) {
                                        html += `<a href="javascript:;" title="Hủy đơn vay" onclick="CommonModalJS.InitCancelModal(${row.loanBriefId}, 0)" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							                     <i class="fa fa-trash"></i></a>`;
                                    }
                                }
                                html += '<a data-toggle="modal" href="#Modal" title="Nhắn tin cho khách" onclick="SMSModule.ShowModalSendSMS(' + row.loanBriefId + ',' + row.phone + ')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
							             <i class="fa fa-comment"></i></a>';

                                if (_groupGlobal == GROUP_MANAGER_TELES || _groupGlobal == GROUP_BOD) {
                                    html += `<a data-toggle="modal" title="File ghi âm" onclick="CommonModalJS.ViewRecordModel('#modalRecording',${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							                 <i class="fa fa-headphones"></i></a>`;

                                    html += `<a data-toggle="modal" title="Hủy check Momo" onclick="CommonModalJS.CancelCheckMomo(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							                 <i class="fa fa-exclamation-circle"></i></a>`;
                                }
                                html += `<a href="javascript:;" title="Đặt lịch hẹn" onclick="CommonModalJS.InitScheduleTime(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							             <i class="fa fa-clock-o"></i></a>`;
                                if (row.isCheckMomo) {
                                    html += `<a href="javascript:;" title="Check Momo" onclick="CommonModalJS.CheckMomo(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
                                        <i class="flaticon-user-ok"></i></a>`;
                                }
                                if (_groupGlobal == GROUP_MANAGER_TELES) {
                                    html += `<a data-toggle="modal" title="Thay đổi số điện thoại" onclick="CommonModalJS.ChangePhoneModal(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							                <i class="fa fa-expand"></i></a>`;
                                }
                            }
                        }

                        return html;
                    }
                },

            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            loanBriefId: $('#filterLoanId').val().replace('HĐ-', '').trim(),
            search: $("#filterSearch").val().toLowerCase(),
            productId: $("#filterProduct").val(),
            status: $("#filterStatus").val(),
            filterSource: $("#filterSource").val(),
            provinceId: $("#filterProvince").val(),
            filtercreateTime: $('#filtercreateTime').val().toLowerCase(),
            countCall: $("#filterCountCall").val(),
            filterClickTop: $("#filterClickTop").val(),
            detailStatusTelesales: $("#filterDetailStatusTelesales").val(),
            statusTelesales: $("#filterStatusTelesales").val(),
            filterStaffTelesales: $("#filterSupport2").val(),
            team: $("#filterTeam").val(),
            dateRangerLastChangeStatus: $('#filterLastChangeStatus').val(),
            utmSource: $('#filterUtmSource').val(),
            filterRecare: $('#filterRecare').val(),
        }

        InitDataTable(objQuery);
        if (objQuery.status == ENUM_CANCELED || objQuery.status == ENUM_INIT) {
            datatable.hideColumn('check');
        }

    }

    var Init = function () {
        $("#filterProduct").select2({
            placeholder: "Chọn sản phẩm"
        });

        $("#filterStatus").select2({
            placeholder: "Chọn trạng thái"
        });

        $("#filterSource").select2({
            placeholder: "Chọn nguồn"
        });

        $("#filterProvince").select2({
            placeholder: "Chọn Tỉnh / Thành"
        });

        $("#filterSupport").select2({
            placeholder: "Chọn Support"
        });

        $("#filterHub").select2({
            placeholder: "Chọn Hub"
        });

        $("#filterCountCall").select2({
            placeholder: "Số lần gọi"
        });

        $("#filterDetailStatusTelesales").select2({
            placeholder: "Trạng thái cuộc gọi"
        });

        $("#filterStatusTelesales").select2({
            placeholder: "Chọn trạng thái"
        });

        $("#filterSupport2").select2({
            placeholder: "Chọn Telesales"
        });

        $("#filterTeam").select2({
            placeholder: "Chọn Team"
        });

        $("#filterRecare").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#ChangeTeam").select2({
            placeholder: "Chọn team cần chuyển"
        });

        //GetPlatformType();

        $("#btnSearch").click(function () {
            $("#filterClickTop").val('-1');
            LoadData();
        });

        $("#filterLoanId, #filterSearch").on('keydown', function (e) {
            if (e.which == 13) {
                $("#filterClickTop").val('-1');
                LoadData();
                return false;
            }
        });


        $("#filterProduct, #filterStatus, #filterSource, #filterProvince, #filtercreateTime,#filterCountCall,#filterDetailStatusTelesales,#filterSupport2,#filterTeam,#filterRecare").on('change', function (e) {
            $("#filterClickTop").val('-1');
            LoadData();
            return false;
        });

        $('#filtercreateTime').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: moment().subtract(90, 'days'),
            endDate: moment(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY',
                firstDay: 1,
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7",
                ],
                "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ],
            },
        });

        $('#filterLastChangeStatus').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY',
                firstDay: 1,
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7",
                ],
                "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ],
            },
        });

        $('#filterLastChangeStatus').val('');

        GetTelesalesByTeam();

        $(document).on('change', '.checkbox-item', function () {
            var totalSelected = $(".checkbox-item:checked").length;
            $(".sTotalSelected").text(totalSelected);
            //var showbox = true;
            //if (totalSelected > 0 && _groupGlobal == GroupUser.Telesale) {
            //    if ($('#filterRecare').val() == User.recare_team)
            //        showbox = false;
            //}
            //if (showbox == true && totalSelected > 0) {
            //    $('.box-show-selected').css('display', 'block');
            //} else {
            //    $('.box-show-selected').css('display', 'none');
            //}

        });
        $(document).on('change', '#checkAll', function () {
            var totalSelected = $(".checkbox-item:checked").length;
            $(".sTotalSelected").text(totalSelected);
            ////nếu là giỏ recare team và là tls thì bỏ qua
            //var showbox = true;
            //if (totalSelected > 0 && _groupGlobal == GroupUser.Telesale) {
            //    if ($('#filterRecare').val() == User.recare_team)
            //        showbox = false;
            //}
            //if (showbox == true && totalSelected > 0) {
            //    $('.box-show-selected').css('display', 'block');
            //} else {
            //    $('.box-show-selected').css('display', 'none');
            //}            
        });


        $("#filterSupport").on('change', function (e) {
            var value = $(this).val();
            if (value == User.recare_team || value == User.system_team || value == User.follow_team) {
                $('#boxChangeTeam').css('display', 'block');
            } else {
                $('#boxChangeTeam').css('display', 'none');
            }
        });
    };

    var GetStatus = function () {
        App.Ajax("GET", "/Dictionary/GetStatus", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].status + "'>" + data.data[i].description + "</option>";
                }
                $("#filterStatus").append(html);
                $("#filterStatus").select2({
                    placeholder: "Trạng thái"
                });
            }
        });
    }

    var GetProvince = function () {
        App.Ajax("GET", "/Dictionary/GetProvince", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].provinceId + "'>" + data.data[i].name + "</option>";
                }
                $("#filterProvince").append(html);
                $("#filterProvince").select2({
                    placeholder: "Tỉnh / Thành"
                });

            }
        });
    }

    var GetDetailStatusTelesales = function (parentId) {
        $("#filterDetailStatusTelesales").html('');
        App.Ajax("GET", "/LoanBrief/DetailStatusTelesales?parentId=" + parentId, undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {

                var html = "";
                html += "<option value='-1'>Chọn trạng thái chi tiết</option>"
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].id + "'>" + data.data[i].name + "</option>";
                }
                $("#filterDetailStatusTelesales").append(html);
                $("#filterDetailStatusTelesales").select2({
                    placeholder: "Trạng thái cuộc gọi"
                });
            }
            LoadData();
        });
    }

    var GetUserByGroupId = function (id, append) {
        App.Ajax("GET", "/Dictionary/GetUserByGroupId?groupId=" + id, undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                if (append)
                    html += '<option value="-1">Chọn Telesales</option>';
                for (var i = 0; i < data.data.length; i++) {
                    if (data.data[i].teamTelesalesId > 0) {
                        html += "<option value='" + data.data[i].userId + "'>" + data.data[i].username + "</option>";
                    }
                }
                if (append) {
                    $("#filterSupport2").append(html);
                    $("#filterSupport2").select2({
                        placeholder: "Chọn Telesales"
                    });
                }
                $("#filterSupport").append(html);
                $("#filterSupport").select2({
                    placeholder: "Chọn Telesales"
                });
            }
        });
    };

    var GetTelesalesByTeam = function () {
        App.Ajax("GET", "/Telesales/GetTelesalesByTeam").done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    if (data.data[i].teamTelesalesId > 0) {
                        html += "<option value='" + data.data[i].userId + "'>" + data.data[i].username + "</option>";
                    }
                }
                $("#filterSupport2").append(html);
                $("#filterSupport2").select2({
                    placeholder: "Chọn Telesales"
                });
                $("#filterSupport").append(html);
                $("#filterSupport").select2({
                    placeholder: "Chọn Telesales"
                });
            }
        });
    }

    var ChangeSupport = function () {
        var teleid = $("#filterSupport").val();
        var favorite = [];
        $.each($("input[name='checkbox']:checked"), function () {
            favorite.push($(this).val());
        });
        if (teleid == '') {
            App.ShowErrorNotLoad('Bạn phải chọn support');
            return;
        } if (favorite == '') {
            App.ShowErrorNotLoad('Bạn phải chọn đơn vay muốn chuyển');
            return;
        }
        var team = $('#ChangeTeam').val();
        //Kiểm tra nếu là chuyển sang giỏ của team
        //Nếu user quản lý nhiều hơn 1 team telesale
        if (teleid == User.recare_team || teleid == User.system_team || teleid == User.follow_team) {
            if ($('#totalTeamManager').val() > 1) {
                if (team == '' || team == null || team == undefined) {
                    App.ShowErrorNotLoad('Bạn phải chọn Team để chuyển đơn vay');
                    return;
                }
            }
        }
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn chuyển giỏ của các đơn đã chọn",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/LoanBrief/ChangeSupport",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                        });
                    },
                    data: { ArrId: favorite, TelesaleId: teleid, teamTelesaleId: team },
                    success: function (data) {
                        $.unblockUI();
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad("Chuyển giỏ thành công");
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                            return;
                        }
                    }
                });
            }
        });
    };

    var FilterClickTop = function (status, element) {
        $("#filterClickTop").val(status);
        $('.filter').each(function (index, element) {
            $(this).removeClass('filter-border-button')
        })
        $(element).find('.filter').addClass('filter-border-button');
        LoadData();

    };

    var PushLoanAndChooseHub = function (loanBriefId, provinceId) {
        $('#divResult').html('');
        $.ajax({
            type: "POST",
            url: "/LoanBrief/PushLoanAndChooseHub",
            data: { LoanBriefId: loanBriefId, ProvinceId: provinceId },
            success: function (data) {
                $('#divResult').html(data);
                $('#Modal').modal('show');
            },
            traditional: true
        });
    };

    var PushLoanAndChooseHubPOST = function (loanbriefId, hubId) {

        $.ajax({
            type: "POST",
            url: "/LoanBrief/ConfirmLoanBrief",
            data: { LoanBriefId: loanbriefId, HubId: hubId },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    $('#Modal').modal('hide');
                    App.ReloadData();
                } else {
                    App.ShowErrorNotLoad(data.message);
                }

            },
            traditional: true
        });
    }

    var LoadFilterTop = function () {
        $.ajax({
            type: "GET",
            url: "/LoanBrief/FilterLoanTop",
            success: function (data) {
                $('#FilterLoanTop').html(data);

            },
            traditional: true
        });

    }

    var FilterStaffTLS = function () {
        $.ajax({
            type: "GET",
            url: "/LoanBrief/FilterStaffTLS",
            success: function (data) {
                $('#filterStaffTLS').html(data);

            },
            traditional: true
        });

    }

    var ChangeMineAndRecare = function (userId) {
        var favorite = [];
        $.each($("input[name='checkbox']:checked"), function () {
            favorite.push($(this).val());
        });
        if (favorite == '') {
            App.ShowErrorNotLoad('Bạn phải chọn đơn vay muốn chuyển');
            return;
        }
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn chuyển giỏ của các đơn đã chọn",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/LoanBrief/ChangeSupport",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                        });
                    },
                    data: { ArrId: favorite, TelesaleId: userId },
                    success: function (data) {
                        $.unblockUI();
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad("Chuyển giỏ thành công");
                            $(".sTotalSelected").text(0);
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                            return;
                        }
                    }
                });
            }
        });
    };

    var ChangeRecare = function (recare) {
        if (recare != -1) {
            $('.div_mine').removeAttr('style', 'display: none;');
            $('.div_mine').attr('style', 'padding: 10px;');
            $('.div_recare').attr('style', 'display: none;');
            $('#filterStaffTLS').addClass('col-md-11');
            $('#filterStaffTLS').removeClass('col-md-9');
        }
        else {
            $('.div_recare').removeAttr('style', 'display: none;');
            $('.div_recare').attr('style', 'padding: 10px;');
            $('.div_mine').attr('style', 'display: none;');
            $('#filterStaffTLS').addClass('col-md-9');
            $('#filterStaffTLS').removeClass('col-md-11');
        }
    }

    var ChangeSupportAll = function () {
        var teleId = $("#filterSupport").val();
        if (teleId == '') {
            App.ShowErrorNotLoad('Bạn phải chọn support');
            return;
        }

        var teamChange = $('#ChangeTeam').val();
        //Kiểm tra nếu là chuyển sang giỏ của team
        //Nếu user quản lý nhiều hơn 1 team telesale
        if (teleId == User.recare_team || teleId == User.system_team || teleId == User.follow_team) {
            if ($('#totalTeamManager').val() > 1) {
                if (teamChange == '' || teamChange == null || teamChange == undefined) {
                    App.ShowErrorNotLoad('Bạn phải chọn Team để chuyển đơn vay');
                    return;
                }
            }
        }

        var loanBriefId = $('#filterLoanId').val().replace('HĐ-', '').trim();
        var search = $("#filterSearch").val().toLowerCase();
        var productId = $("#filterProduct").val();
        var status = $("#filterStatus").val();
        var filterSource = $("#filterSource").val();
        var provinceId = $("#filterProvince").val();
        var filtercreateTime = $('#filtercreateTime').val().toLowerCase();
        var countCall = $("#filterCountCall").val();
        var filterClickTop = $("#filterClickTop").val();
        var detailStatusTelesales = $("#filterDetailStatusTelesales").val();
        var statusTelesales = $("#filterStatusTelesales").val();
        var filterStaffTelesales = $("#filterSupport2").val();
        var team = $("#filterTeam").val();
        var dateRangerLastChangeStatus = $('#filterLastChangeStatus').val();
        var utmSource = $('#filterUtmSource').val();
        var filterRecare = $('#filterRecare').val();
        $.ajax({
            type: "GET",
            url: "/LoanBrief/ChangeSupportAll",
            data: {
                loanBriefId: loanBriefId, search: search, productId: productId, status: status, filterSource: filterSource,
                provinceId: provinceId, filtercreateTime: filtercreateTime, countCall: countCall, filterClickTop: filterClickTop,
                detailStatusTelesales: detailStatusTelesales, statusTelesales: statusTelesales, filterStaffTelesales: filterStaffTelesales,
                team: team, dateRangerLastChangeStatus: dateRangerLastChangeStatus, utmSource: utmSource, filterRecare: filterRecare
            },
            success: function (res) {
                if (res.status == 1) {
                    var html = "";
                    var lstLoanBriefId = [];
                    if (res.data.length <= 1000) {
                        html = "Bạn có chắc chắn muốn chuyển " + res.data.length + " HĐ.";
                        lstLoanBriefId = res.data;
                    }
                    else {
                        html = "Bạn có chắc chắn muốn chuyển 1000 / " + res.data.length + " HĐ.";
                        lstLoanBriefId = res.data.slice(0, 1000);
                    }
                    swal({
                        title: '<b>Tổng đơn: ' + res.data.length + '</b>',
                        html: html,
                        showCancelButton: true,
                        confirmButtonText: 'Xác nhận',
                        cancelButtonText: 'Hủy',
                        onOpen: function () {
                            $('.swal2-cancel').width($('.swal2-confirm').width());
                        }
                    }).then(function (result) {
                        if (result.value) {
                            $.ajax({
                                type: "POST",
                                url: "/LoanBrief/ChangeSupport",
                                beforeSend: function () {
                                    $.blockUI({
                                        message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                                    });
                                },
                                data: { ArrId: lstLoanBriefId, TelesaleId: teleId, teamTelesaleId: teamChange },
                                success: function (data) {
                                    $.unblockUI();
                                    if (data.status == 1) {
                                        App.ShowSuccessNotLoad("Chuyển giỏ thành công");
                                        App.ReloadData();
                                    } else {
                                        App.ShowErrorNotLoad(data.message);
                                        return;
                                    }
                                }
                            });
                        }
                    });
                }
                else {
                    App.ShowErrorNotLoad(res.message);
                }


            }
        });

    };

    return {
        Init: Init,
        ChangeSupport: ChangeSupport,
        GetUserByGroupId: GetUserByGroupId,
        InitData: InitData,
        LoadData: LoadData,
        FilterClickTop: FilterClickTop,
        PushLoanAndChooseHub: PushLoanAndChooseHub,
        PushLoanAndChooseHubPOST: PushLoanAndChooseHubPOST,
        LoadFilterTop: LoadFilterTop,
        GetDetailStatusTelesales: GetDetailStatusTelesales,
        FilterStaffTLS: FilterStaffTLS,
        ChangeMineAndRecare: ChangeMineAndRecare,
        ChangeRecare: ChangeRecare,
        GetTelesalesByTeam: GetTelesalesByTeam,
        ChangeSupportAll: ChangeSupportAll,
    };
}

