﻿var LoanSearchAllModule = new function () {
    var _group;
    var _typeServiceCall;
    var InitData = function (group, typeServiceCall) {
        _group = group;
        _typeServiceCall = typeServiceCall;
    }
    var Init = function (group) {
        _group = group;
        //LoadData();
        $("#btnSearch").click(function () {
            LoadData();
            return;
        });
        //Mã HĐ
        $("#filterLoanId,#filterSearch,#filterPhone,#filterNationalCard").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return;
            }
        });
    };

    var LoadData = function () {
        var objQuery = {
            loanBriefId: $("#filterLoanId").val().replace('HĐ-', '').trim(),
            search: $('#filterSearch').val().trim().toLowerCase(),
            phone: $('#filterPhone').val().trim().toLowerCase(), 
            nationalCard: $('#filterNationalCard').val().trim().toLowerCase(),
        }
        if ((objQuery.loanBriefId != null && objQuery.loanBriefId != "")
            || (objQuery.search != null && objQuery.search != "")
            || (objQuery.phone != null && objQuery.phone != "")
            || (objQuery.nationalCard != null && objQuery.nationalCard != "")) {
            InitDataTable(objQuery);
        }        
    }
    var InitDataTable = function (objQuery) {
        $('#dtLoanSeach').html('');
        var datatable = $('#dtLoanSeach').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/LoanBrief/SearchLoanAll',
                        params: {
                            query: objQuery
                        }
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 70,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var html = '';
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;

                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += 'HĐ-' + row.loanBriefId;
                        if (row.hub != null) {
                            html += `<span class="item-desciption">(${row.hub.name})</span>`;
                        }
                        if (row.isReborrow) {
                            html += `<span class="item-desciption">(Tái vay)</span>`;
                        }
                        if (row.utmSource != null && row.utmSource != "") {
                            return html += "<br /><p style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);margin-bottom: 0px;'>" + row.utmSource + "</p>";
                        } else {
                            return html;
                        }
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '<a id="linkName" style="color:#6C7293;" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>' + "<br />";
                        if (row.phone != null) {
                            html += ConvertPhone(row.phone);
                        }
                        return html;
                    }

                },
                {
                    field: 'address',
                    title: 'Quận/ Huyện',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.district != null)
                            text += "<p class='district'>" + row.district.name + "</p>";
                        if (row.province != null) {
                            text += "<p class='province'>" + row.province.name + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    textAlign: 'center',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = '';
                        var money = App.FormatCurrency(row.loanAmount);
                        if (row.loanProduct != null) {
                            html += money + "<br />" + "<p class='district'>" + row.loanProduct.name + "</p>";
                        } else {
                            html += money;
                        }
                        if (row.isLocate) {
                            html += `<span class="m-badge m-badge--success m-badge--wide font-size-11">Xe lắp định vị</span>`;
                        }
                        if (row.isTrackingLocation) {
                            html += `<span class="item-desciption">(KH chia sẻ vị trí)</span>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'loanTime',
                    title: 'Thời gian vay',
                    //width: 200,
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = '';
                        if (row.loanTime != null)
                            html += row.loanTime + " Tháng";
                        return html;
                    }
                },
                {
                    field: 'scheduleTime',
                    title: 'Lịch hẹn',
                    textAlign: 'center',
                    width: 80,
                    visible: true,
                    template: function (row) {
                        if (row.scheduleTime != null && row.scheduleTime != undefined) {
                            var date = moment(row.scheduleTime);
                            return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                        }
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        if ((row.status == 20 || row.status == 16 ) && (_group == GROUP_TELES || _group == GROUP_MANAGER_TELES)) {
                            var html = "";
                            if (row.status == 20 && row.statusTelesales == null) {
                                html += "<span class='m-badge m-badge--warning m-badge--wide'>Chưa liên hệ</span>";
                                //hiển thị tên teles phụ trách
                                if (_group == GROUP_MANAGER_TELES) {
                                    if (row.userTelesale != null) {
                                        html += `<span class="item-desciption-11">tls: ${row.userTelesale.username}</span>`;
                                    }
                                }
                                return html;
                            }
                            else if (row.status == 16 && row.statusTelesales == null) {
                                html += "<span class='m-badge m-badge--info m-badge--wide'>Chờ chuẩn bị hồ sơ</span>";
                                //hiển thị tên teles phụ trách
                                if (_group == GROUP_MANAGER_TELES) {
                                    if (row.userTelesale != null) {
                                        html += `<span class="item-desciption-11">tls: ${row.userTelesale.username}</span>`;
                                    }
                                }
                                return html;
                            }
                            else if (row.status == 99) {
                                html += "<span class='m-badge m-badge--danger m-badge--wide'>Đơn đã hủy</span>";
                                return html;
                            }
                            else if (row.statusTelesales != null) {
                                //Không liên hệ được
                                if (row.statusTelesales == 1) {
                                    html += "<span class='m-badge m-badge--info m-badge--wide'>Không liên hệ được</span>";
                                }
                                //Cân nhắc/Hẹn gọi lại
                                else if (row.statusTelesales == 2) {
                                    html += "<span class='m-badge m-badge--info m-badge--wide'>Cân nhắc/Hẹn gọi lại</span>";
                                }
                                //Chờ chuẩn bị hồ sơ
                                else if (row.statusTelesales == 3) {
                                    html += "<span class='m-badge m-badge--info m-badge--wide'>Chờ chuẩn bị hồ sơ</span>";
                                }
                                //Hiển thị trạng thái chi tiết
                                if (App.ArrDetailStatusTelesales[row.detailStatusTelesales] != null) {
                                    html += '<span class="' + App.ArrDetailStatusTelesales[row.detailStatusTelesales].class + '">' + App.ArrDetailStatusTelesales[row.detailStatusTelesales].title + '</span>';
                                    //hiển thị tên teles phụ trách
                                    if (_group == GROUP_MANAGER_TELES) {
                                        if (row.userTelesale != null) {
                                            html += `<span class="item-desciption-11">tls: ${row.userTelesale.username}</span>`;
                                        }
                                    }
                                }
                                return html;
                            }
                        } else {
                            if (App.ArrLoanStatus[row.status] != null) {
                                var html = '<span class="m-badge ' + App.ArrLoanStatus[row.status].class + ' m-badge--wide">' + App.ArrLoanStatus[row.status].title + '</span>';
                                if (row.inProcess == 1) {
                                    html += `<span class="item-desciption-11">Đang xử lý</span>`;
                                }
                                return html;
                            } else {
                                return 'Không xác định';
                            }
                        }
                    }
                },
                {
                    field: 'tdhs',
                    title: 'Nhân viên thụ lý',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var telesale = '';
                        var hub = '';
                        var approve = '';
                        var html = '';
                        if (row.boundTelesale != null) {
                            telesale = row.boundTelesale.fullName + `(${row.boundTelesale.username})`;
                        }
                        if (row.userHub != null) {
                            hub = row.userHub.fullName + `(${row.userHub.username})`;
                        }
                        if (row.coordinatorUser != null) {
                            approve = row.coordinatorUser.fullName + `(${row.coordinatorUser.username})`;
                        }
                        html = "TLS: " + "<b>" + telesale + "</b>" + "<br />" + "CVKD: " + "<b>" + hub + "</b>" + "<br />" + "TĐHS: " + "<b>" + approve + "</b>";
                        return html;

                    }
                }
            ]
        });
    }
    return {
        Init: Init,
        InitData: InitData
    };
}

