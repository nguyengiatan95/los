﻿var ApproverStatusDetailModule = new function () {
    var Init = function () {
        $("#sl_LoanStatusDetail").select2({
            placeholder: "Chọn trạng thái"
        });
    };


    var SubmitForm = function () {
        var form = $('#form_LoanStatusDetail');
        $('#btnSaveLoanStatus').attr('disabled', true);
        form.validate({
            ignore: [],
            rules: {
                StatusDetail: {
                    min: 1
                }
            },
            messages: {
                StatusDetail: {
                    min: "Vui lòng chọn trạng thái",
                }
                
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            $('#btnSaveLoanStatus').attr('disabled', false);
            return;
        }
        var txtLoanStatus = $('#sl_LoanStatusDetail option:selected').text();
        var note = $('#txt_Comment').val();
        var noteHub = "";
        if (note != '')
            noteHub = '<b>Comment: </b>' + note;
        var comment = '<b>Trạng thái: </b>' + txtLoanStatus + '<br />' + '<br />' + note;
        $('#txtNote').val(comment);
        form.ajaxSubmit({
            url: '/Approve/SaveChangeStatus',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#Modal').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        ApproveEmpModule.LoadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }


    return {
        Init: Init,
        SubmitForm: SubmitForm,
    };
}

