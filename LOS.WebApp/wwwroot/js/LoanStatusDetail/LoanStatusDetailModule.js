﻿var LoanStatusDetailModule = new function () {
    var Init = function () {
        $("#sl_LoanStatusDetail").select2({
            placeholder: "Chọn trạng thái"
        });
        $("#sl_StatusDetailChild").select2({
            placeholder: "Chọn trạng thái chi tiết"
        });
    };

    var GetLoanStatusChild = function (statusId) {
        if (statusId > 0) {
            App.Ajax("GET", "/Dictionary/GetLoanStatusDetailChild?id=" + statusId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += '<option value="0">Chọn trạng thái chi tiết</option>';
                    for (var i = 0; i < data.data.length; i++) {
                        html += `<option value="${data.data[i].id}" >${data.data[i].name}</option>`;
                    }
                    $("#sl_StatusDetailChild").html(html);
                    $("#sl_StatusDetailChild").select2();
                }
            });
        } else {
            $('#sl_StatusDetailChild').select2({
                placeholder: "Vui lòng chọn trạng thái",
                width: '100%'
            });
        }
    }

    var SubmitForm = function () {
        var form = $('#form_LoanStatusDetail');
        $('#btnSaveLoanStatus').attr('disabled', true);
        form.validate({
            ignore: [],
            rules: {
                StatusDetail: {
                    min: 1
                },
                StatusDetailChild: {
                    min: 1
                },
            },
            messages: {
                StatusDetail: {
                    min: "Vui lòng chọn trạng thái",
                },
                StatusDetailChild: {
                    min: "Vui lòng chọn trạng thái chi tiết",
                },
                
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            $('#btnSaveLoanStatus').attr('disabled', false);
            return;
        }
        var txtLoanStatus = $('#sl_LoanStatusDetail option:selected').text();
        var txtLoanStatusChild = $('#sl_StatusDetailChild option:selected').text();
        var note = $('#txt_Comment').val();
        var noteHub = "";
        if (note != '')
            noteHub = '<b>Comment: </b>' + note;
        var comment = '<b>Trạng thái: </b>' + txtLoanStatus + '<br />' + '<b>Trạng thái chi tiết: </b>' + txtLoanStatusChild + '<br />' + note;
        $('#txtNote').val(comment);
        form.ajaxSubmit({
            url: '/LoanBrief/SaveNextStep',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#Modal').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        App.ReloadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    return {
        Init: Init,
        GetLoanStatusChild: GetLoanStatusChild,
        SubmitForm: SubmitForm
    };
}

