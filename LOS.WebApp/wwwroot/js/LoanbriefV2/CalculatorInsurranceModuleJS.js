﻿var CalculatorInsurranceModuleJS = new function () {
    var Init = function (loanAmount, loanTime) {
        var notSchedule = 1;
        if (loanAmount != "" && loanTime != "") {
            loanAmount = loanAmount.split(",").join("");
            $.ajax({
                type: "POST",
                url: "/Approve/InterestSubmit",
                data: { TotalMoneyDisbursement: parseInt(loanAmount), LoanTime: parseInt(loanTime), NotSchedule: notSchedule  },
                success: function (data) {
                    console.log(data);
                    if (data.status == 1) {
                        $("#FeeInsurrance").text(App.FormatCurrency(data.data.feeInsurrance) + ' VNĐ');   
                        $("#FeeInsurranceProperty").text(App.FormatCurrency(data.data.feeInsuranceMaterialCovered) + ' VNĐ');
                    }
                },
                traditional: true
            });
        }
        
    };


    return {
        Init: Init
    }
}