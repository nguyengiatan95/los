﻿var CalculatorRateMoneyModuleJS = new function () {
    var Init = function (productId, loanAmount, loanTime) {
        var rate = $("#RateMoney").val();
        if (productId != "" && loanAmount != "" && loanTime != "") {
            var rateType = $("#sl_RateType").val(); // = 6 -> tất toán cuối kỳ, 10 -> dư nợ giảm dần
            loanAmount = loanAmount.split(",").join("");           
            //Check nếu là gói cầm đăng ký xe ô tô 
            if (productId == ProductCredit.OtoCreditType_CC) {
                //if (parseInt(loanTime) == 1) rate = 2500;
                if (parseInt(loanTime) == 3) rate = 2300;
                if (parseInt(loanTime) == 6) rate = 2000;
                if (parseInt(loanTime) == 9 || parseInt(loanTime) == 12) rate = 1500;
            }
            else {
                rate = 2700;
            }
        }
        $("#RateMoney").val(App.FormatCurrency(rate));
        Calculator(rate);
    };
    var Calculator = function (rate) {
        if (!Number.isInteger(rate))
            rate = rate.replace(/,/g, '');
        if (parseInt(rate) > 700) {
            $("#Percent").text('');
            var cal = (rate * 30 / 1000000) * 100;
            var cal2 = (rate * 365 / 1000000) * 100;
            $("#Percent").text(cal.toFixed(1) + ' % / tháng');
            $("#RatePercent").val(cal2.toFixed(2));
        }

    };
    var OnchangeMoneyCal = function (val, id) {
        if (val.length >= 8) {
            val = val.split(",").join("");
            var insu = (val / 100) * 5;
            $("#InsurenceMoney").text(App.FormatCurrency(insu) + ' VNĐ');

            var productId = $("#sl_loan_product").val();
            var loanTime = $("#sl_LoanTime").val();
            if (loanTime != "" && productId != "") {
                Init(productId, val, loanTime);
            }
            if (val != "" && loanTime != "" && document.getElementById(id).checked)
                CalculatorInsurranceModuleJS.Init(val, loanTime);
        }
    };

    var ChangeRateType = function () {
        var productId = $("#sl_loan_product").val();
        var loanTime = $("#sl_LoanTime").val();
        var loanAmount = $("#txtLoanAmount").val();
        //Nếu là gói oto + 
        if (productId == ProductCredit.OtoCreditType_CC) {
            var rateType = $('#sl_RateType').val();
            var htmlLoanTime = '<option></option>';
            if (parseInt(rateType) == RateType.DuNoGiamDan) {
                htmlLoanTime += `<option value="3">3 tháng</option>`
                htmlLoanTime += `<option value="6">6 tháng</option>`
                htmlLoanTime += `<option value="9">9 tháng</option>`
                htmlLoanTime += `<option value="12" selected="selected">12 tháng</option>`
            } else {
                //htmlLoanTime += `<option value="1">1 tháng</option>`
                htmlLoanTime += `<option value="3">3 tháng</option>`
                htmlLoanTime += `<option value="6">6 tháng</option>`
                htmlLoanTime += `<option value="9">9 tháng</option>`
                htmlLoanTime += `<option value="12" selected="selected">12 tháng</option>`
            }
            $('#sl_LoanTime').html(htmlLoanTime);
            $('#sl_LoanTime').select2({
                placeholder: "Chọn thời gian vay",
                width: '100%'
            });
        }
        Init(productId, loanAmount, loanTime);
    };


    return {
        Init: Init,
        Calculator: Calculator,
        OnchangeMoneyCal: OnchangeMoneyCal,
        ChangeRateType: ChangeRateType
    }
}