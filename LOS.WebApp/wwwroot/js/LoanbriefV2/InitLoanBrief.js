﻿
var InitLoanBrief = new function () {

    var InitModal = function (LoanBriefId, TypeInit) {
        $('#ScriptTelesaleBody').html('');
        $('#InitLoanBriefBody').html('');
        $.ajax({
            url: '/LoanBriefV2/Init?id=' + LoanBriefId + '&type=' + TypeInit,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#InitLoanBriefBody').html(res.html);
                    $('#modelInitLoanBrief').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
            }

        });
    }


    var ScriptTelesale = function (LoanBriefId) {
        $('#ScriptTelesaleBody').html('');
        $.ajax({
            url: '/LoanBrief/ScriptTelesalse?loanBriefid=' + LoanBriefId,
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'json',
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#ScriptTelesaleBody').html(res.html);
                    $('#modelScriptTelesales').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
            }
        });
    }


    var SubmitForm = function (e) {
        e.preventDefault();
        var form = $('#formAddLoanBrief');
        form.validate({
            ignore: [],
            rules: {
                'PlatformType': {
                    required: true
                },
                'FullName': {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                'Phone': {
                    required: true,
                    validPhone: true
                },
                'NationalCard': {
                    required: true,
                    validCardnumber: true
                },
                'NationCardPlace': {
                    validCardnumber: true
                },
                'sBirthDay': {
                    validDate: true
                },
                'ProductId': {
                    required: true
                },
                'ProvinceId': {
                    required: true
                },
                'DistrictId': {
                    required: true
                },
                'WardId': {
                    required: true
                },
                'LoanBriefResidentModel.ResidentType': {
                    required: true
                },
                LoanAmount: {
                    validLoanAmount: true
                },
                LoanTime: {
                    required: true
                },
                ProductDetailId: {
                    required: true
                },
            },
            messages: {
                'PlatformType': {
                    required: "Bạn vui lòng chọn nguồn đơn",
                    class: "has-danger"
                },
                'FullName': {
                    required: "Bạn vui lòng nhập họ tên",
                },
                'Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                'NationalCard': {
                    required: "Vui lòng nhập CMND/CCCD",
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'NationCardPlace': {
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'sBirthDay': {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                },
                'ProductId': {
                    required: "Vui lòng chọn gói vay"
                },
                'ProvinceId': {
                    required: "Vui lòng chọn thành phố đang sống"
                },
                'DistrictId': {
                    required: "Vui lòng chọn quận/huyện đang sống"
                },
                'WardId': {
                    required: "Vui lòng chọn phường/xã đang sống"
                },
                'LoanBriefResidentModel.ResidentType': {
                    required: "Vui lòng chọn hình thức cư trú"
                },
                LoanAmount: {
                    validLoanAmount: "Vui lòng nhập số tiền khách hàng cần vay"
                },
                LoanTime: {
                    required: "Vui lòng chọn thời gian vay"
                },
                ProductDetailId: {
                    required: "Vui lòng chọn gói vay chi tiết"
                },
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            return;
        } else {
            $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
            $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
            $('#txt_LoanAmountExpertiseLast').val($('#txt_LoanAmountExpertiseLast').val().replace(/,/g, ''));
            $('#txt_LoanAmountExpertiseLast').val($('#txt_LoanAmountExpertiseLast').val().replace(/,/g, ''));
            $('#RateMoney').val($('#RateMoney').val().replace(/,/g, ''));

            //gán số khung, số máy ô tô
            var productId = $('#sl_loan_product').val();
            if (productId == ProductCredit.OtoCreditType_CC) {
                if ($('#txt_car_chassis').val() != "" && $('#txt_car_chassis').val() != null && $('#txt_car_chassis').val() != undefined) {
                    $('#txt_chassis').val($('#txt_car_chassis').val());
                }
                if ($('#txt_car_engine').val() != "" && $('#txt_car_engine').val() != null && $('#txt_car_engine').val() != undefined) {
                    $('#txt_engine').val($('#txt_car_engine').val());
                }
            }           

            //kiểm tra xem  Ngày đăng ký xe có đúng định dạng không
            if ($('#txt_motobike_certificate_date').val() != "" && $('#txt_motobike_certificate_date').val() != null && $('#txt_motobike_certificate_date').val() != undefined) {
                if ($('#txt_motobike_certificate_date').val().length != 10) {
                    App.ShowErrorNotLoad('Ngày đăng ký xe không đúng định dạng');
                    return false;
                }
            }

            $('#btnSubmit').attr('disabled', true);
            form.ajaxSubmit({
                url: '/LoanBriefV3/InitLoanBrief',
                method: 'POST',
                //beforeSend: function (xhr) {
                //    debugger
                //    xhr.setRequestHeader('RequestVerificationToken', $('input:hidden[name="__RequestVerificationToken"]').val());
                //},
                success: function (data, status, xhr, $form) {
                    if (data != undefined) {
                        if (data.status == 1) {
                            $('#modelInitLoanBrief').modal('hide');
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                    $('#btnSubmit').attr('disabled', false);
                },
                error: function () {
                }
            });
        }
    }

    return {
        Create: InitModal,
        ScriptTelesale: ScriptTelesale,
        Save: SubmitForm
    };
}