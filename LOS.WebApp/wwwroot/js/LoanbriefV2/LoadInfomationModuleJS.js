﻿var LoadInfomationModuleJS = new function () {
    var Load = function (productId) {
        var checkInsuranceProperty = $('#checkInsuranceProperty').val();
        var loanBriefId = $('#LoanBriefId').val();
        if (productId != "") {
            if (LIST_ENUM_CAR.indexOf(Number(productId)) > -1) {
                document.getElementById("CarDisplay").style.display = '';
                document.getElementById("MotoDisplay").style.display = 'none';
            } else {

                document.getElementById("CarDisplay").style.display = 'none';
                document.getElementById("MotoDisplay").style.display = '';
            }
            //Check nếu là KCC & KGT => không hiển thị
            if ((checkInsuranceProperty == 1 && (productId == ProductCredit['MotorCreditType_KCC'] || productId == ProductCredit['MotorCreditType_KGT'] || productId == ProductCredit['MotorCreditType_CC']))
                || (loanBriefId == 0 && (productId == ProductCredit['MotorCreditType_KCC'] || productId == ProductCredit['MotorCreditType_KGT'] || productId == ProductCredit['MotorCreditType_CC']))) {
                if (document.getElementById("disInsuranceProperty") != null) {
                    document.getElementById("disInsuranceProperty").style.display = 'block';
                }
                if (document.getElementById("disInsurancePropertyMoney") != null) {
                    document.getElementById("disInsurancePropertyMoney").style.display = 'block';
                }
            }
            else {
                if (document.getElementById("disInsuranceProperty") != null) {
                    document.getElementById("disInsuranceProperty").style.display = 'none';
                }
                if (document.getElementById("disInsurancePropertyMoney") != null) {
                    document.getElementById("disInsurancePropertyMoney").style.display = 'none';
                }
            }
        } else {
            if (document.getElementById("CarDisplay") != null) {
                document.getElementById("CarDisplay").style.display = 'none';
            }
            if (document.getElementById("MotoDisplay") != null) {
                document.getElementById("MotoDisplay").style.display = 'none';
            } 
        }
    };

    var ChangeProduct = function (val) {
        //ẩn hiện hình thức tính lãi
        var typeProduct = $('#sl_loan_product :selected').attr("attr-type-product");
        //sản phẩm nhỏ
        var htmlRateType = '<option></option>';
        var htmlLoanTime = '<option></option>';
        if (typeProduct == 1) {
            htmlRateType += `<option value="13" selected="selected">Dư nợ giảm dần</option>`
            htmlLoanTime += `<option value="12" selected="selected">12 tháng</option>`
            LoancreditInfomationModuleJS.ChangeLoanTime(12);
            //bỏ disable mua bảo hiểm
            $('#BuyInsurenceCustomer').prop('checked', true);
            $('#BuyInsurenceCustomer').attr('disabled', 'disabled');
        } else if (typeProduct == 2) {
            //Hình thức tính lãi
            htmlRateType += `<option value="14" >Tất toán cuối kỳ</option>`
            htmlRateType += `<option value="13" selected="selected">Dư nợ giảm dần</option>`
            //Thời gian vay
            //htmlLoanTime += `<option value="1">1 tháng</option>`
            htmlLoanTime += `<option value="3">3 tháng</option>`
            htmlLoanTime += `<option value="6">6 tháng</option>`
            htmlLoanTime += `<option value="9">9 tháng</option>`
            htmlLoanTime += `<option value="12"  selected="selected">12 tháng</option>`

            //Không mua bảo hiểm và disable
            $('#BuyInsurenceCustomer').prop('checked', false);
            $('#BuyInsurenceCustomer').attr('disabled', 'disabled');
        }
        $('#sl_RateType').html(htmlRateType);
        $('#sl_RateType').select2({
            placeholder: "Chọn hình thức tính lãi",
            width: '100%'
        });
        $('#sl_LoanTime').html(htmlLoanTime);
        $('#sl_LoanTime').select2({
            placeholder: "Chọn thời gian vay",
            width: '100%'
        });
        var productId = $('#sl_loan_product').val();
        if (productId == ProductCredit.MotorCreditType_CC || productId == ProductCredit.MotorCreditType_KCC || productId == ProductCredit.MotorCreditType_KGT) {
            LTVModuleJS.LoadOption();
            $('#boxLtv').show();
        } else {
            $('#sl_Ltv').html('');
            $('#boxLtv').hide();
        }       
        LoancreditFormModuleJS.ShowProductDetail();
        Load(val);
    };



    return {
        Load: Load,
        ChangeProduct: ChangeProduct
    }
}