﻿var AppraiserPriceMoto = new function () {
    var arrProductReview = [];
    var countField = 0;
    var InitData = function (data) {
        var objData = JSON.parse(data);
        if (objData != null && objData.length > 0) {
            for (var i = 0; i < objData.length; i++) {
                var item = objData[i];
                if (item.Id > 0) {
                    arrProductReview[item.Id] = item;
                    countField++;
                }
            }
        }
    }

    var ChangeAppraiserMotoPrice = function (loanBriefId) {
        $.ajax({
            type: "GET",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            url: "/LoanBriefV3/AppraiserMotoPrice?id=" + loanBriefId,
            success: function (data) {
                $.unblockUI();
                if (data.isSuccess) {
                    $('#m_tabs_tham_dinh_xe').html(data.html);

                } else {
                    App.ShowErrorNotLoad(data.message);
                }

            },
            traditional: true
        });
    };

    var LoadPriceAppraiser = function (productReviewId, checked) {
        //Nếu không thỏa mãn tiêu chí bị trừ tiền 
        //Kiểm tra xem tiêu chí có bị hủy k cho vay k
        if (productReviewId > 0 && arrProductReview[productReviewId] != null) {
            if (arrProductReview[productReviewId].IsCancel == true && arrProductReview[productReviewId].State != checked) {
                $("#TotalBorrowMoney").html("0 &nbsp;VNĐ");
                return false;
            }
        }
        //định giá xe trên thị trường 
        var productPriceCurrent = $('#hdf_CurrentPrice').val();
        //Tổng số tiền của các tiêu chí
        var moneyDiscount = MoneyDiscount();
        if (moneyDiscount.IsCancel) {
            $("#TotalBorrowMoney").html("0 &nbsp;VNĐ");
            return false;
        }
        // console.log('productPriceCurrent: ' + productPriceCurrent)
        var totalDiscount = moneyDiscount.TotalDiscount;
        //console.log('totalDiscount: ' + totalDiscount)
        // Số tiền KH nhận được ( đã trừ tiêu chí)
        var totalMoney = parseInt(productPriceCurrent) - parseInt(totalDiscount);
        //console.log('totalMoney: ' + totalMoney)
        if (totalMoney < 0) {
            $("#TotalBorrowMoney").html("0 &nbsp;VNĐ");
        }
        else {
            $("#TotalBorrowMoney").html(FormatMoneyTDX(RoundingMoney(totalMoney), '.', ',') + "&nbsp;VNĐ");
        }
    }

    var FormatMoneyTDX = function (nStr, decSeperate, groupSeperate) {
        nStr += '';
        x = nStr.split(decSeperate);
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
        }
        return x1 + x2;
    }

    var MoneyDiscount = function () {
        //lấy ra d/s các radio đã được check
        //Tổng rố tiền discount
        var listCheck = $("input:radio:checked");
        var totalDiscount = 0;
        var isCancel = false;
        if (listCheck != null && listCheck.length > 0) {
            listCheck.each(function (item, domElement) {
                var productReviewId = $(domElement).attr('attr-id');
                var valueChecked = domElement.value == "true" || domElement.value == "True";
                if (productReviewId > 0 && arrProductReview[productReviewId] != null) {

                    //Nếu không thỏa mãn tiêu chí bị trừ tiền 
                    //Kiểm tra xem tiêu chí có bị hủy k cho vay k
                    if (arrProductReview[productReviewId].IsCancel == true && valueChecked != arrProductReview[productReviewId].State) {
                        isCancel = true;
                        console.log('Cancel : ' + productReviewId + ' name: ' + arrProductReview[productReviewId].Name)
                    }

                    //Nếu chọn đúng tiêu chí + khác State => trừ tiền theo discount
                    if (valueChecked != arrProductReview[productReviewId].State && arrProductReview[productReviewId].MoneyDiscount > 0) {
                        totalDiscount += Number(arrProductReview[productReviewId].MoneyDiscount);
                        console.log('Id: ' + productReviewId + ' name: ' + arrProductReview[productReviewId].Name + ' discount: ' + arrProductReview[productReviewId].MoneyDiscount)
                    }
                }
            })
        } else {
            isCancel = true;
        }
        return {
            TotalDiscount: totalDiscount,
            IsCancel: isCancel
        };
    }

    function RoundingMoney(totalmoney) {
        // nếu là xe chính chủ / xe không chính chủ
        var productCreditId = $("#hddProductCreditId").val();
        var typeOfOwnerShip = $("#hddTypeOwnerShipId").val();
        var loanProductDetailId = $('#hddLoanProductDetailId').val();
        var typeRemarketingLtv = $('#hddTypeRemarketingLtv').val();
        //tinh số tiền dư nợ hiện tại\
        var sTotalMoneyDebtCurrent = $('#hdf_TotalMoneyDebtCurrent').val();
        var totalMoneyDebtCurrent = 0;
        if (sTotalMoneyDebtCurrent != null && Number(sTotalMoneyDebtCurrent) > 0) {
            totalMoneyDebtCurrent = Number(sTotalMoneyDebtCurrent);
        }
        var getPrice = GetMaxPrice(productCreditId, totalmoney, typeOfOwnerShip, loanProductDetailId, totalMoneyDebtCurrent, Number(typeRemarketingLtv));
        return getPrice;
    }
    var GetMaxPrice = function (productId, originalCarPrice, typeOfOwnerShip, productDetailId, totalMoneyDebtCurrent, typeRemarketingLtv) {
        var maxPriceProduct = 0;
        var percentLtv = 0;
        var ltv = $('#hddLtv').val();
        if (ltv != undefined && Number(ltv) > 0) {
            percentLtv = Number(ltv);
        } else {
            percentLtv = GetPercentLtv(productId, typeRemarketingLtv);
        }
        if (typeRemarketingLtv == TypeRemarketingLtv.TopUp) {
            maxPriceProduct = RoundMoney((originalCarPrice * percentLtv / 100) - totalMoneyDebtCurrent);
        } else {
            maxPriceProduct = RoundMoney(originalCarPrice * percentLtv / 100) - totalMoneyDebtCurrent;
        }
        var maxPrice = MaxPriceV2(typeOfOwnerShip, productDetailId);
        if (typeRemarketingLtv == TypeRemarketingLtv.TopUp) {
            var limitedLoanAmount = LimitedLoanAmount(productId, typeOfOwnerShip)
            if (maxPrice > limitedLoanAmount)
                maxPrice = limitedLoanAmount;
        }   
        if (maxPriceProduct > maxPrice)
            maxPriceProduct = maxPrice;
        return maxPriceProduct;
    }


    var LimitedLoanAmount = function (productId, typeOfOwnerShip) {
        if (typeOfOwnerShip == TypeOfOwnerShip.KT1)//KT1
        {
            if (productId == ProductCredit.MotorCreditType_CC) //xe cc
            {
                return 42000000;
            }
            else if (productId == ProductCredit.MotorCreditType_KCC)
            {
                return 30000000;
            } else 
            {
                return 20000000;
            }
        }
        else//KT3
        {
            if (productId == ProductCredit.MotorCreditType_CC) //xe cc
            {
                return 32000000;
            }
            else if (productId == ProductCredit.MotorCreditType_KCC)
            {
                return 20000000;
            } else {
                return 15000000;
            }
        }
    }
    var GetPercentLtv = function (productId, typeRemarketingLtv) {
        var percentLtv = 60;
        if (productId == ProductCredit.MotorCreditType_CC) //xe cc
        {
            percentLtv = 70;
            if (typeRemarketingLtv == TypeRemarketingLtv.TopUp) {
                percentLtv = 90;
            } else if (typeRemarketingLtv == TypeRemarketingLtv.Reborrow) {
                percentLtv = 90;
            }
        }
        else if (productId == ProductCredit.MotorCreditType_KCC
            || productId == ProductCredit.MotorCreditType_KGT) {
            percentLtv = 60;
            if (typeRemarketingLtv == TypeRemarketingLtv.TopUp) {
                percentLtv = 80;
            } else if (typeRemarketingLtv == TypeRemarketingLtv.Reborrow) {
                percentLtv = 80;
            }
        }
        return percentLtv;
    }

    var MaxPriceV2 = function (typeOfOwnerShip, productDetailId) {
        if (productDetailId > 0) {
            if (LimitMoneyProductDetail[productDetailId] > 0) {
                return LimitMoneyProductDetail[productDetailId];
            }
        } else {
            //Nếu chưa chọn chi tiết gói vay 
            if (ConvertTypeOfOwnerShip(typeOfOwnerShip) == TypeOfOwnerShip.KT1)//KT1
                return 30000000;
            else if (ConvertTypeOfOwnerShip(typeOfOwnerShip) == TypeOfOwnerShip.KT3)//KT3
                return 20000000;
        }
        return 10000000;
    }

    var UnitMillion = function (totalmoney) {
        // làm tròn số
        var total1 = parseInt(totalmoney) % parseInt(1000000);
        return totalmoney - total1;
    }
    var RoundMoney = function (totalmoney) {
        // làm tròn số
        var total1 = parseInt(totalmoney) % parseInt(1000000);
        if (parseInt(total1) >= 500000) {
            var total2 = totalmoney - total1;
            totalmoney = parseInt(total2) + 1000000;
        }
        else {
            var total2 = totalmoney - total1;
            totalmoney = total2;
        }
        return totalmoney;
    }

    var SetThamDinhXe = function (productId, loanCreditId) {
        var dataProductArr = [];
        var elementPropertyChecked = $("input:radio:checked");
        //Kiểm tra xem tất cả các điều kiện đã được check chưa
        if (elementPropertyChecked.length != Object.keys(arrProductReview).length) {
            App.ShowErrorNotLoad("Bạn hãy chọn đầy đủ các tiêu chí thẩm định");
            return;
        }
        if (elementPropertyChecked != null && elementPropertyChecked.length > 0) {
            elementPropertyChecked.each(function (item, domElement) {
                var productReviewId = $(domElement).attr('attr-id');
                var valueChecked = domElement.value == "true" || domElement.value == "True";
                var newObject = { "LoanBriefId": loanCreditId, "ProductId": productId, "ProductReviewId": productReviewId, "IsCheck": valueChecked };
                dataProductArr.push(newObject);
            })
        }
        // Add them parentId
        $(".custom-box-parrent input").each(function () {
            var productReviewId = $(this).val();
            var newObject = { "LoanBriefId": loanCreditId, "ProductId": productId, "ProductReviewId": productReviewId, "IsCheck": true };
            dataProductArr.push(newObject);
        });

        //Tính lại tiền sau khi thẩm định
        var totalMoney = 0; //Số tiền tối đa cho vay
        //định giá xe trên thị trường 
        var productPriceCurrent = $('#hdf_CurrentPrice').val();
        //Tổng số tiền của các tiêu chí
        var moneyDiscount = MoneyDiscount();
        if (moneyDiscount.IsCancel) {
            App.ShowErrorNotLoad("Số tiền sau khi thẩm định phải lớn hơn 0");
            return;
        }
        var totalDiscount = moneyDiscount.TotalDiscount;
        // Số tiền KH nhận được ( đã trừ tiêu chí)
        var totalMoneyExpertise = parseInt(productPriceCurrent) - parseInt(totalDiscount);
        if (totalMoneyExpertise < 0) {
            App.ShowErrorNotLoad("Số tiền sau khi thẩm định phải lớn hơn 0");
            return;
        }
        else {
            totalMoney = RoundingMoney(totalMoneyExpertise);
            if (totalMoney > 0) {
                $.ajax({
                    type: "POST",
                    url: "/Products/UpdateProductResultReview/",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                        });
                    },
                    data: { productId: productId, totalMoney: totalMoney, loanBriefId: loanCreditId, productReviewResults: dataProductArr, totalMoneyExpertise: totalMoneyExpertise }
                }).done(function (result) {
                    if (result.isSuccess) {
                        App.ShowSuccessNotLoad(result.message);
                        //$("#txtAgencyMoney").val(formatMoneyTDX(totalprice, '.', ','));
                        $.unblockUI();
                        return;
                    }
                    else {
                        App.ShowErrorNotLoad(result.message);
                        return;
                    }
                });
            } else {
                App.ShowErrorNotLoad("Số tiền sau khi thẩm định phải lớn hơn 0");
                return;
            }
        }
    }

    return {
        InitData: InitData,
        ChangeAppraiserMotoPrice: ChangeAppraiserMotoPrice,
        LoadPriceAppraiser: LoadPriceAppraiser,
        FormatMoneyTDX: FormatMoneyTDX,
        MoneyDiscount: MoneyDiscount,
        RoundingMoney: RoundingMoney,
        GetMaxPrice: GetMaxPrice,
        UnitMillion: UnitMillion,
        MaxPriceV2: MaxPriceV2,
        SetThamDinhXe: SetThamDinhXe
    };
}