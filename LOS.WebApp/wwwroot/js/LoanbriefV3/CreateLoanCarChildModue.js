﻿var CreateLoanCarChildModel = new function () {

    var CreateLoanCarChild = function (btn, e) {
        $(btn).attr('disabled', true);
        $(btn).attr('style', 'cursor: no-drop;');
        $(btn).addClass('m-loader m-loader--success m-loader--right');
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            rules: {
                LoanAmount: {
                    required: true,
                    minlength: 2,
                },
            },
            messages: {
                LoanAmount: {
                    required: "Vui lòng số tiền",
                    minlength: "Vui lòng số tiền",
                },
               
            }
        });
        if (!form.valid()) {
            $(btn).removeAttr('disabled', true);
            $(btn).removeAttr('style', 'cursor: no-drop;');
            $(btn).removeClass('m-loader m-loader--success m-loader--right');
            return;
        }
        $('#LoanAmount').val($('#LoanAmount').val().replace(/,/g, ''));
        form.ajaxSubmit({
            url: '/LoanBriefV3/CreateLoanCarChild',
            method: 'POST',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui"><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            success: function (data, status, xhr, $form) {
                $.unblockUI();
                $(btn).removeAttr('disabled', true);
                $(btn).removeAttr('style', 'cursor: no-drop;');
                $(btn).removeClass('m-loader m-loader--success m-loader--right');
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    setTimeout(function () {
                        location.reload()
                    }, 1000);
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    return {
        CreateLoanCarChild, CreateLoanCarChild,
    }
}();