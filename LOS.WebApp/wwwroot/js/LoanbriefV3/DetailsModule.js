﻿var DetailModel = new function () {

    var ChangeLoanBriefNote = function (loanBriefId) {
        $.ajax({
            url: '/LoanBriefV3/GetNoteDetail',
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    baseZ: 2000
                });
            },
            data: { loanBriefId: loanBriefId },
            success: function (res) {
                $.unblockUI();
                $('#m_tabs_lich_su').html('');
                $('#m_tabs_lich_su').html(res);
            }
        });
    }

    var ChangeDetailDocumentFile = function (loanBriefId) {
        $.ajax({
            type: "GET",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    baseZ: 2000
                });
            },
            url: "/LoanBriefV3/ListDetailFile?id=" + loanBriefId,
            success: function (data) {
                $('#m_tabs_chi_tiet_chung_tu').html('');
                $('#m_tabs_chi_tiet_chung_tu').html(data);
                $.unblockUI();
            },
            traditional: true
        });

    };

    var ChangeDocumentFileAll = function (loanBriefId) {
        $.ajax({
            type: "GET",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    baseZ: 2000
                });
            },
            url: "/LoanBriefV3/ListFileAll?id=" + loanBriefId,
            success: function (data) {
                $('#m_tabs_chung_tu').html('');
                $('#m_tabs_chung_tu').html(data);
                $.unblockUI();
            },
            traditional: true
        });

    };

    var ChangeFollowDevice = function (url) {
        $.ajax({
            type: "GET",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    baseZ: 2000
                });
            },
            url: "/LoanBriefV3/FollowDevice",
            data: { url: url.toString() },
            success: function (data) {
                $('#m_tabs_theo_doi_thiet_bi').html('');
                $('#m_tabs_theo_doi_thiet_bi').html(data);
                $.unblockUI();
            },
            traditional: true
        });
        //var embed = '<embed type="text/html" src="' + url + '" style="width:100%; height:800px">'
        //$('#m_portlet_tab_1_4').html(embed);
    };

    var CalculateInterest = function (loanbriefId, loanAmount, loanTime, rateTypeId, ratePercent) {
        if (ratePercent == null || ratePercent == '' || ratePercent == undefined)
            ratePercent = '98.55';
        $.ajax({
            url: '/LoanBrief/GetApiCalculateInterest',
            type: 'POST',
            data: { loanbriefId: loanbriefId, loanAmount: loanAmount, loanTime: loanTime, rateTypeId: rateTypeId, ratePercent: ratePercent },
            success: function (res) {
                if (res.status == 1) {
                    $('#_totalMoneyNeedPayment').html(App.FormatCurrency(res.data) + " VNĐ")
                }
            },
        });
    }

    var ResultEkyc = function (loanbriefId) {
        $.ajax({
            url: '/LoanBriefV3/ResultEkyc',
            type: 'GET',
            data: { loanbriefId: loanbriefId },
            success: function (res) {
                $('#_result_ekyc').html('');
                $('#_result_ekyc').html(res.html);
                if (res.data != null) {
                    if (res.data.fullNameCheck != null && res.data.fullNameCheck == false)
                        $('#_result_ekyc_fullname').html(res.data.fullNameValue);
                    if (res.data.idCheck != null && res.data.idCheck == false)
                        $('#_result_ekyc_nationalcard').html(res.data.idValue);
                    if (res.data.birthdayCheck != null && res.data.birthdayCheck == false)
                        $('#_result_ekyc_birday').html(res.data.birthdayValue);
                }
            },
        });
    }

    var CheckPhoneDetail = function (id, typePhone) {
        $('#resultCheckPhone').html('');
        $.ajax({
            type: "POST",
            url: "/LoanBriefV3/CheckPhoneDetail",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    baseZ: 2000
                });
            },
            data: { id: id, typePhoneCheck: typePhone },
            success: function (res) {
                $.unblockUI();
                if (res.isSuccess == 1) {
                    $('#resultCheckPhone').html(res.html);
                    $('#modalCheckPhone').modal('show');
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }

    var CheckStatusTransactionSecured = function (loanBriefId) {
        $.ajax({
            url: '/Approve/CheckStatusTransactionSecured',
            type: 'GET',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                    baseZ: 2000
                });
            },
            data: { loanBriefId: loanBriefId },
            success: function (res) {
                $.unblockUI();
                if (res.status == 1)
                    $("#txtRerultTrans").text(res.message)
            }
        });
    }

    return {
        ChangeLoanBriefNote: ChangeLoanBriefNote,
        ChangeDetailDocumentFile: ChangeDetailDocumentFile,
        ChangeDocumentFileAll: ChangeDocumentFileAll,
        ChangeFollowDevice: ChangeFollowDevice,
        CalculateInterest: CalculateInterest,
        ResultEkyc: ResultEkyc,
        CheckPhoneDetail: CheckPhoneDetail,
        CheckStatusTransactionSecured: CheckStatusTransactionSecured
    }
}();