﻿var DisbursementContractModule = new function () {
    var _typeServiceCall;
    var InitData = function (typeServiceCall) {
        _typeServiceCall = typeServiceCall;
    }

    var LoadData = function () {
        var objQuery = {
            loanBriefId: $("#filterLoanId").val().replace('HĐ-', '').trim(),
            search: $('#filterSearch').val().trim().toLowerCase(),
            provinceId: $('#filterProvince').val(),
            filtercreateTime: $('#filtercreateTime').val(),
            productId: $('#filterProduct').val(),
            topup: $('#filterTopup').val(),
        }

        InitDataTable(objQuery);
    }

    var InitDataTable = function (objQuery) {
        var datatable = $('#dtDisbursementContract').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/LoanBriefV3/GetDisbursementContract',
                        params: {
                            query: objQuery
                        },
                    }
                },
                pageSize: 20,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    title: '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input onclick="DisbursementContractModule.CheckAll()" id="checkAll" type="checkbox"><span></span></label></span>',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    //visible: true,
                    template: function (row) {
                        return '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" value="' + row.loanBriefId + '" name="checkbox"><span></span></label></span>';
                    }
                },
                {
                    field: 'stt',
                    title: 'STT',
                    width: 70,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var html = '';
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        if (row.utmSource != null && row.utmSource != "") {
                            return index + record + "<br /><p style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);'>" + row.utmSource + "</p>";
                        } else {
                            return index + record;
                        }
                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += 'HĐ-' + row.loanBriefId;
                        return html;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    sortable: false,
                    template: function (row) {
                        var html = '<a class="fullname" id="linkName" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>';
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Quận/ Huyện',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.districtName != null)
                            text += "<p class='district'>" + row.districtName + "</p>";
                        if (row.provinceName != null) {
                            text += "<p class='province'>" + row.provinceName + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'disbursementAt',
                    title: 'Ngày giải ngân',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.disbursementAt);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay <br /> VNĐ',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = `<span class="money">${App.FormatCurrency(row.loanAmount)}</span>`
                        if (row.productName != null)
                            html += `<span class="item-desciption">${row.productName}</span>`;
                        return html;
                    }
                },

                {
                    field: 'loanTime',
                    title: 'Thời gian vay',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var html = "";
                        if (row.loanTime != null)
                            html += "<br />" + row.loanTime + " Tháng";
                        return html;
                    }
                },
                {
                    field: 'call',
                    title: 'Gọi',
                    width: 80,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        //gọi qua caresoft
                        var html = '';
                        if (_typeServiceCall == 1) {
                            html = '<a href="javascript:;" onclick="CareSoftModule.CallCareSoft(' + row.loanBriefId + ')" class="box-call">\
                                    <i class="fa fa-phone item-icon-phone" ></i></a>';
                        } else if (_typeServiceCall == 2) {// gọi qua metech
                            html = `<a href="javascript:;" onclick="finesseC2CModule.ClickToCall('${row.phone}',${row.loanBriefId})" class="box-call">
                                    <i class="fa fa-phone item-icon-phone" ></i></a>`;
                        }
                        var dauso = row.phone.substring(0, 3);
                        if (Vina.includes(dauso))
                            html += '<span class="network-home">Vina</span>'
                        else if (Mobi.includes(dauso))
                            html += '<span class="network-home">Mobi</span>'
                        else if (Viettel.includes(dauso))
                            html += '<span class="network-home">Viettel</span>'
                        else
                            html += '<span class="network-home">Không xác định</span>'
                        return html;
                    }
                },
                {
                    field: 'action',
                    title: 'Hành động',
                    width: 160,
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = '';
                        if (row.typeLoanSupport == TypeLoanSupport.IsCanTopup) {
                            html += `<a href="javascript:;" title="Tạo đơn Topup" onclick="LoanBriefTopup.CreateLoanTopup(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-plus"></i></a>`;
                        }
                        else if (row.statusUser != StatusBase.Active && $('#filterTopup').val() == FilterTopupDisbursementContract.TlsOff) {
                            html += `<a href="javascript:;" title="Chuyển Telesales" onclick="DisbursementContractModule.ChangeBoundTelesale(${row.loanBriefId}, ${row.boundTelesaleId}, '${row.boundTelesaleName}')" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-exchange"></i></a>`;
                        }
                        else if ($('#filterTopup').val() == FilterTopupDisbursementContract.CloseLoan) {
                            html += `<a href="javascript:;" title="Gửi yêu cầu đóng HĐ" onclick="CommonModalJS.GetCloseLoan(${row.loanBriefId})" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">
							    <i class="fa fa-check-square-o"></i></a>`;
                        }

                        return html;
                    }
                }
            ]
        });
    }

    var Init = function () {

        $("#filterProvince, #filterProduct, #filterTopup, #sl_TeamTelesales, #sl_TelesaleId").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#btnSearch").click(function () {
            LoadData();
            return;
        });

        //Mã HĐ, Phone + CMND + Name
        $("#filterLoanId, #filterSearch").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return;
            }
        });

        //City
        $('#filterProvince').on('change', function () {
            LoadData();
            return;
        });
        //Status
        $('#filterProduct, #filtercreateTime, #filterTopup').on('change', function () {
            LoadData();
            return;
        });

        LoadData();
        GetProduct();
    }

    var GetProduct = function () {
        App.Ajax("GET", "/Dictionary/GetLoanProduct", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "<option value=\"-1\" selected=\"selected\">Tất cả sản phẩm</option>";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].loanProductId + "'>" + data.data[i].name + "</option>";
                }
                $("#filterProduct").append(html);
                $("#filterProduct").select2({
                    placeholder: "Sản phẩm"
                });
            }
        });
    }

    var ChangeBoundTelesale = function (loanbriefId, boundTelesaleId, boundTelesaleName) {
        $('#header_change_boundtelesale').html('Chuyển <b>HĐ-' + loanbriefId + ' </b> cho Telesales khác');
        $('#hdd_LoanBriefId').val(loanbriefId);
        $('#hdd_BoundTelesaleIdOld').val(boundTelesaleId);
        $('#hdd_BoundTelesaleNameOld').val(boundTelesaleName);
        //lấy danh sách team tls
        App.Ajax("GET", "/Telesales/GetTeamEnable", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                html += "<option></option>";
                for (var i = 0; i < data.data.length; i++) {
                    html += `<option value="${data.data[i].teamTelesalesId}">${data.data[i].name}</option>`;
                }
                $('#sl_TeamTelesales').html(html);
                $('#sl_TeamTelesales').select2({
                    placeholder: "Vui lòng chọn"
                });
            }
        });

        $('#modal_change_boundtelesale').modal('show');
    }

    var GetTelesalesByTeam = function (team) {
        App.Ajax("GET", "/Telesales/GetTelesalesByTeamV2?team=" + team, undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                html += "<option></option>";
                for (var i = 0; i < data.data.length; i++) {
                    html += `<option value="${data.data[i].userId}">${data.data[i].username}</option>`;
                }
                $('#sl_TelesaleId').html(html);
                $('#sl_TelesaleId').select2({
                    placeholder: "Vui lòng chọn"
                });
            }
        });
    }

    var SaveChangeTelesales = function (btn) {
        $(btn).attr('disabled', true);
        $(btn).addClass('m-loader m-loader--success m-loader--right');
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            rules: {
                TeamTelesales: {
                    required: true
                },
                TelesaleId: {
                    required: true
                }
            },
            messages: {
                TeamTelesales: {
                    required: "Vui lòng chọn Team Telesales",
                    class: "has-danger"
                },
                TelesaleId: {
                    required: "Vui lòng chon Telesales",
                    class: "has-danger"
                }
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            $(btn).attr('disabled', false);
            $(btn).removeClass('m-loader m-loader--success m-loader--right');
            return false;
        }
        $('#hdd_TelesaleName').val($("#sl_TelesaleId option:selected").text());
        form.ajaxSubmit({
            url: '/LoanBriefV3/SaveChangeBoundTelesaleByTelesalesOff',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $(btn).attr('disabled', false);
                $(btn).removeClass('m-loader m-loader--success m-loader--right');
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    $('#modal_change_boundtelesale').modal('hide');
                    LoadData();
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            },
            error: function () {
            }
        });
    }

    var CheckAll = function () {
        if ($("#checkAll").is(':checked')) {
            $("#dtDisbursementContract input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $("#dtDisbursementContract input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
    };

    var CreateLoanTopup = function (btn) {
        $(btn).attr('disabled', true);
        $(btn).addClass('m-loader m-loader--success m-loader--right');
        var lstLoanBriefId = [];
        var typeLoan = $('#filterTopup').val();
        $.each($("input[name='checkbox']:checked"), function () {
            lstLoanBriefId.push($(this).val());
        });
        if (lstLoanBriefId == '') {
            App.ShowErrorNotLoad('Bạn phải chọn đơn vay muốn tạo Topup');
            $(btn).removeAttr('disabled', true);
            $(btn).removeClass('m-loader m-loader--success m-loader--right');
            return;
        }

        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn tạo đơn Topup các HĐ đã chọn",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/LoanBriefV3/CreateLoanToup",
                    data: { listLoanbriefId: lstLoanBriefId, typeLoan: typeLoan },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                        });
                    },
                    success: function (data) {
                        $.unblockUI();
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                            return;
                        }
                    },
                    traditional: true
                });
            }
        });
    }

    return {
        Init: Init,
        InitData: InitData,
        ChangeBoundTelesale: ChangeBoundTelesale,
        GetTelesalesByTeam: GetTelesalesByTeam,
        SaveChangeTelesales: SaveChangeTelesales,
        CheckAll: CheckAll,
        CreateLoanTopup: CreateLoanTopup,
    }
}