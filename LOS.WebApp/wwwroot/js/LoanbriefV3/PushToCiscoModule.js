﻿var PushToCiscoModule = new function () {

    var Init = function () {

        $("#filterStatus").select2({
            placeholder: "Trạng thái"
        });

        LoadData();

        $("#btnSearch").click(function () {
            LoadData();
            return;
        });

        $("#filterSearch").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return;
            }
        });

        $('#filterStatus').on('change', function () {
            LoadData();
            return;
        });

    };

    var LoadData = function () {
        var objQuery = {
            search: $('#filterSearch').val().trim().toLowerCase(),
            status: $('#filterStatus').val()
        }
        InitDataTable(objQuery);
    }

    var InitDataTable = function (objQuery) {
        var datatable = $('#dtPushToCisco').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/LoanBriefV3/PushToCiscoSearch',
                        params: {
                            query: objQuery
                        }
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [

                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var html = '';
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }

                        return index + record;
                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    textAlign: 'center',
                    width: 100,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (row.loanBriefId != null)
                            html = 'HĐ-' + row.loanBriefId;

                        return html;
                    }
                },
                {
                    field: 'phone',
                    title: 'SĐT',
                    sortable: false,
                    width: 100
                },

                {
                    field: 'createdAt',
                    title: 'Thời gian tạo',
                    textAlign: 'center',
                    width: 100,
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        var html = date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");

                        return html;
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        if (App.ArrPushToCiscoStatus[row.status] != null) {
                            var html = '<span class="m-badge ' + App.ArrPushToCiscoStatus[row.status].class + ' m-badge--wide">' + App.ArrPushToCiscoStatus[row.status].title + '</span>';
                            
                            return html;
                        } else {
                            return 'Không xác định';
                        }
                    }
                }
            ]
        });
    }

    var InitModal = function (id) {
        $('#divResult').html('');
        $.ajax({
            type: "POST",
            url: "/LoanBriefV3/PushCiscoModal",
            data: { id: id },
            success: function (data) {
                $('#divResult').html(data);
                $('#Modal').modal('show');
            }
        });
    }

    var PushToCiscoPost = function (e) {
        e.preventDefault();
        var form = $('#formPushToCisco');
        form.validate({
            ignore: [],
            rules: {
                'Phone': {
                    required: true,
                    validPhone: true
                }
            },
            messages: {                
                'Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                }
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            return;
        } else {          

            $('#btnSubmit').attr('disabled', true);
            form.ajaxSubmit({
                url: '/LoanBriefV3/PushToCiscoPost',
                method: 'POST',
                success: function (data, status, xhr, $form) {
                    if (data != undefined) {
                        if (data.status == 0) {
                            App.ShowSuccessNotLoad(data.message);
                            $('#Modal').modal('hide');
                            LoadData();
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                    $('#btnSubmit').attr('disabled', false);
                },
                error: function () {
                }
            });
        }
    }
    return {
        Init: Init,        
        LoadData: LoadData,
        InitModal: InitModal,
        PushToCiscoPost: PushToCiscoPost
    };
}

