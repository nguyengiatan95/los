﻿var LoginModule = new function () {
	var Login = function (btn,e) {
		var username = $("#ipUsername").val();
		var password = $("#ipPassword").val();
		//password = md5(password);

		e.preventDefault();

		var form = $(btn).closest('form');

		form.validate({
			rules: {
                Username: {
					required: true
				},				
                Password: {
					required: true
				}				
			}
		});

		if (!form.valid()) {
			return;
		}

		$(btn).addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);				
		//form.submit();
        var data = {
            Username: username.trim(),
            Password: password
        };
        form.ajaxSubmit({
            url: '/Login/PostLogin',
            method: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('RequestVerificationToken', $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: { obj: data},          
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        var remember = document.getElementById("checkRemember").checked;
                        if (remember == true) {
                            createCookie("cmd_username", username, 30);
                            createCookie("cmd_password", password, 30);
                        }
                        else {
                            createCookie("cmd_username", username, -1);
                            createCookie("cmd_password", password, -1);
                        }
                        window.location.href = data.message;
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                        $('#m_login_signin_submit').removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);				
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                    $('#m_login_signin_submit').removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);				
                }
            }
        });
       
    };
    var createCookie = function (name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    };

    var readCookie = function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return '';
    };
    var checkRemmember = function () {
        var username = readCookie("cmd_username");
        if (username != '') {
            var password = readCookie("cmd_password");
            $("#ipUsername").val(username);
            $("#ipPassword").val(password);
            $(".m-login__form").find("#checkRemember").prop('checked', true);
            $(".m-login__form").find("#checkRemember").closest("span").addClass("checked");
        }
        else {
            $("#ipUsername").val('');
            $("#ipPassword").val('');
        }
    };

	return {		
        Init: function () {
          
		},
        Login: Login,
        checkRemmember: checkRemmember
	};
}
$(document).ready(function () {
    LoginModule.checkRemmember();
})