﻿var ManagerAreaHub = new function () {

    var LoadData = function () {
        var objQuery = {
            groupId: $('#filterGroup').val(),
            status: $('#filterStatus').val(),
            filterName: $('#filterName').val().trim()
        }
        InitDataTable(objQuery);
    }

    var Init = function () {
        $("#filterCity").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#filterHub").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#filterProduct").select2({
            placeholder: "Vui lòng chọn"
        });

        $('#filterCity, #filterHub,#filterProduct').on('change', function () {
            GetDataDistrict(0);
        });
    };

    var GetDataDistrict = function (checkall) {
        var cityname = $('#filterCity option:selected').html();
        $('#txtCityName').html(cityname)
        var cityid = $('#filterCity').val();
        var hubid = $('#filterHub').val();
        var productid = $('#filterProduct').val();
        if (cityid == "-1") {
            App.ShowWarning("Chọn thành phố");
            return false;
        } if (hubid == "-1") {
            App.ShowWarning("Chọn đại lý");
            return false;
        }
        $.ajax({
            type: "POST",
            url: '/ManagerAreaHub/GetDataDistrictManagerAreaHub',
            data: { ctiyId: cityid, hubid: hubid, productid: productid, checkall: checkall },
            success: function (data) {
                $('#div-Result').html(data);
            },
            traditional: true
        });
    }

    function GetdatByDistrict(district) {
        $('#Check-all').prop('checked', false);
        if ($(".txtdistrictId-" + district).is(':checked')) {
            //$('.div-district-' + districtid).remove;

            var cityid = $('#filterCity').val();
            var hubid = $('#filterHub').val();
            var productid = $('#filterProduct').val();

            if (cityid == "-1") {
                App.ShowWarning("Chọn thành phố");
                return false;
            } if (hubid == "-1") {
                App.ShowWarning("Chọn đại lý");
                return false;
            }
            $.ajax({
                type: "POST",
                url: '/ManagerAreaHub/GetWardByDistrict',
                data: { ctiyId: cityid, hubid: hubid, productid: productid, districtid: district },
                success: function (data) {
                    $('#div_ward').prepend(data);
                },
                traditional: true
            });
        }
        else {
            $('.div-district-' + district).remove();
        }
    }

    function checkboxdistrict(districtid) {
        if ($(".txtDistrict-" + districtid).is(':checked')) {
            $('.wardId-' + districtid).prop('checked', true);
        }
        else {
            $('.wardId-' + districtid).prop('checked', false);
        }
    }

    function checkboxward(districtid) {
        if ($('.wardId-' + districtid).length == $('[data-district="' + districtid + '"]:checked').length) {
            $('.txtDistrict-' + districtid).prop('checked', true);
        } else {
            $('.txtDistrict-' + districtid).prop('checked', false);
        }
    }

    function CheckAllDistrict() {
        if ($("#Check-all").is(':checked')) {
            $('.checkAll-District').prop('checked', true);
            GetDataDistrict(1);
        }
        else {
            $('.checkAll-District').prop('checked', false);
            $("#div_ward").html("");
        }
    }

    function SaveSpicesAgency(btn) {
        $(btn).addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
        var ListWardId = [];
        var lst = [];
        var cityid = $('#filterCity').val();
        var hubid = $('#filterHub').val();
        var productid = $('#filterProduct').val();
        $('.txtWardId').each(function () {
            if ($(this).is(":checked")) {
                var wardid = $(this).val();
                var district = $(this).attr("data-district");
                data = {
                    DistrictID: district,
                    WardID: wardid
                }
                ListWardId.push(data);
            }
        });
        var lst = JSON.stringify(ListWardId);
        if (cityid == "-1") {
            App.ShowErrorNotLoad("Bạn phải chọn thành phố cụ thể!", "error");
            return false;
        }
        if (hubid == "-1") {
            App.ShowErrorNotLoad("Bạn phải chọn tên đại lý!", "error");
            return false;
        }

        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn thay đổi cấu hình khẩu vị không?",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: '/ManagerAreaHub/SaveSpicesAgency',
                    dataType: "json",
                    data: { CityId: cityid, Shopid: hubid, ProductId: productid, ListWardId: ListWardId },
                    success: function (response) {
                        if (response.status == 1) {
                            App.ShowSuccessNotLoad("Thành công");
                            App.ReloadData();
                            $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        } else {
                            App.ShowError("Thất bại");
                            $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        }
                    },
                    failure: function (response) {
                        typeMes = "error";
                        message = "Lỗi hệ thống,thử lại sau ít phút!";
                        App.ShowError(message);
                        $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                    },
                    error: function (response) {
                        typeMes = "error";
                        message = "Lỗi xảy ra trong quá trình xử lý dữ liệu!";
                        App.ShowError(message);
                        $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    }
                });
            }
        });


    }

    return {
        Init: Init,
        GetDataDistrict: GetDataDistrict,
        GetdatByDistrict: GetdatByDistrict,
        checkboxdistrict: checkboxdistrict,
        checkboxward: checkboxward,
        SaveSpicesAgency: SaveSpicesAgency,
        CheckAllDistrict: CheckAllDistrict,
        LoadData: LoadData
    };
}

