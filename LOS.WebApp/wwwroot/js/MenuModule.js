﻿var MenuModule = new function () {

    var InitDataTable = function (objQuery) {
        var datatable = $('#tableMenu').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Module/LoadData',
                        params: {
                            query: objQuery
                        }
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'name',
                    title: 'Tên menu',
                    sortable: false
                },
                {
                    field: 'path',
                    title: 'Đường dẫn',
                    sortable: false
                },
                {
                    field: 'controller',
                    title: 'Controller',
                    sortable: false
                },
                {
                    field: 'action',
                    title: 'Action',
                    sortable: false
                },
                {
                    field: 'description',
                    title: 'Mô tả',
                    sortable: false
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    sortable: false,
                    template: function (row) {
                        if (row.status == null)
                            row.status = 0;
                        var status = {
                            0: { 'title': 'Chưa kích hoạt', 'class': 'm-badge--metal' },
                            1: { 'title': 'Kích hoạt', 'class': 'm-badge--success' }
                        };
                        return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
                    }
                }
                ,
                {
                    field: 'isMenu',
                    title: 'Loại menu',
                    sortable: false,
                    template: function (row) {
                        if (row.isMenu == true) {
                            return '<span class="m--font-bold m--font-accent"> Menu</span>';
                        } else {
                            return '<span class="m--font-bold m--font-primary">Action phân quyền</span>';
                        }
                    }
                }
                , {
                    field: 'Action',
                    title: 'Chức năng',
                    sortable: false,
                    width: 100,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return '\
							<a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" onclick="MenuModule.ShowModelMenu('+ row.moduleId + ')">\
                                <i class="la la-edit"></i></a>\
	                        <a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only"  onclick="MenuModule.DeleteMenu('+ row.moduleId + ',\'' + row.name + '\')" >\
                                <i class="la la-times"></i></a>';
                    }
                }
            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            status: $('#filterStatus').val(),
            menu: $('#filterIsMenu').val(),
            filterName: $("#filterName").val().trim(),
            filterName: $("#filterName").val().trim(),
            ApplicationId: $("#filterApplication").val().trim()
        }
        InitDataTable(objQuery);
    }

    var Init = function () {
        LoadData();

        $("#filterIsMenu").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#filterStatus").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#filterApplication").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#filterName").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });

        $('#filterStatus,#filterIsMenu,#filterApplication').on('change', function () {
            LoadData();
            return false;
        });
    };

    var AddMenu = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            rules: {
                //Code: {
                //    required: true,
                //    normalizer: function (value) {
                //        return $.trim(value);
                //    }
                //},
                Name: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                Controller: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                Action: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                Path: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                }
            },
            messages: {
                //Code: {
                //    required: "Bạn chưa nhập code",
                //},
                Name: {
                    required: "Bạn chưa nhập tên menu",
                },
                Controller: {
                    required: "Bạn chưa nhập tên controller",
                },
                Action: {
                    required: "Bạn chưa nhập tên action",
                },
                Path: {
                    required: "Bạn chưa nhập đường dẫn"
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        form.ajaxSubmit({
            url: '/Module/CreateModule',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccess(data.message);
                        $('#create-user').modal('toggle');
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };

    var ShowModelMenu = function (id) {
        $('#modelMenu').html('');
        $.ajax({
            type: "POST",
            url: "/Module/CreateModuleModal",
            data: { Id: id },
            success: function (data) {
                $('#modelMenu').html(data);
                $('#m_modal_4').modal('show');
            },
            traditional: true
        });
    }

    var DeleteMenu = function (id, menu) {
        swal({
            title: 'Cảnh báo',
            text: "Bạn có chắc chắn muốn  menu " + menu + "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xoá'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Module/DeleteModule",
                    data: { Id: id },
                    success: function (data) {
                        if (data.status == 1) {
                            location.reload();
                            swal(
                                'Xóa menu thành công!'
                            )

                        } else {
                            App.ShowError(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var ChangeTypeMenu = function (typeMenu) {
        if (typeMenu == 0) {
            $('.div_menu_parent').attr('style', 'display:none');
        }
        else {
            $('.div_menu_parent').removeAttr('style', 'display:none');
        }
    }

    return {
        Init: Init,
        AddMenu: AddMenu,
        ShowModelMenu: ShowModelMenu,
        DeleteMenu: DeleteMenu,
        ChangeTypeMenu: ChangeTypeMenu
    };
}
