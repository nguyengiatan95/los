﻿var PipelineModule = new function () {

    var properties;
    var sections;

    var Init = function () {
        var portlet = $('.m-portlet').mPortlet();

        $(".m-portlet").on("sortupdate", function (event, ui) {
            // save sort changed
            var sectionContainers = ui.item.closest('.pipeline-container').find(".section-portlet");
            if (sectionContainers != undefined && sectionContainers.length > 0) {
                var sections = [];
                for (var i = 0; i < sectionContainers.length; i++) {
                    var order = $(sectionContainers[i]).find('.section-order');
                    order.html(i + 1);
                    // update order				
                    var section = {
                        id: $(sectionContainers[i]).data('id'),
                        order: (i + 1)
                    };
                    sections.push(section);
                }
                UpdateSectionOrder(sections);
            }
        });
    };


    var UpdateSectionOrder = function (sections) {
        App.Ajax("POST", "/Pipeline/UpdateSectionOrder", { data: sections }, function (data) {
        });
    }

    var AssignValues = function (_properties, _sections) {
        properties = _properties;
        sections = _sections;
    }

    var CreateUpdatePipeline = function (btn, e) {
        e.preventDefault();
        var id = parseInt($("#PipelineId").val());
        var name = $("#txtName").val();
        var note = $("#txtNote").val();
        var data = {
            pipelineId: id,
            name: name,
            note: note
        };
        App.AjaxDone("POST", "/Pipeline/CreateUpdatePipeline", data, function (data) {
            if (data != undefined && data.meta.errorCode == 200) {
                $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                App.ShowSuccessRedirect("Thành công", '/pipeline/index.html');
                $('#modalUpdate').modal('toggle');
            }
            else {
                App.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại!");
                $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
            }
        });
    };

    var CreateUpdateSection = function (btn, e) {
        e.preventDefault();
        var sectionId = parseInt($("#SectionId").val());
        var pipelineId = parseInt($("#PipelineId").val());
        var name = $("#txtName").val();
        var data = {
            sectionId: sectionId,
            pipelineId: pipelineId,
            name: name
        };
        App.AjaxDone("POST", "/Pipeline/CreateUpdateSection", data, function (data) {
            if (data != undefined && data.meta.errorCode == 200) {
                $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                App.ShowSuccessRedirect("Thành công", '/pipeline/index.html');
                $('#modalUpdate').modal('toggle');
            }
            else {
                App.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại!");
                $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
            }
        });
    };

    var DeletePipeline = function (id, name) {
        swal({
            title: 'Cảnh báo',
            text: "Bạn có chắc chắn muốn xóa Pipeline " + name + " không ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xoá'
        }).then(function (result) {
            if (result.value) {
                App.AjaxDone("POST", "/Pipeline/DeletePipeline", { id: id }, function (data) {
                    if (data.meta.errorCode == 200) {
                        location.reload();
                        swal(
                            'Xóa Pipeline thành công!'
                        )

                    } else {
                        App.ShowErrorNotLoad("Xóa Pipeline không thành công!");
                    }
                });
            }
        });
    }

    var ChangeStatus = function (id, status) {
        if (status == 0) {
            App.AjaxDone("POST", "/Pipeline/ChangeStatus", { id: id }, function (data) {
                if (data.meta.errorCode == 200) {
                    location.reload();
                } else {
                    App.ShowErrorNotLoad("Kích hoạt Pipeline không thành công!");
                }
            });
        }
        else {
            swal({
                title: 'Cảnh báo',
                text: "Vô hiệu hóa luồng đơn có thể ảnh hưởng đến đơn vay mới, bạn có muốn tiếp tục ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Đồng ý'
            }).then(function (result) {
                if (result.value) {
                    App.AjaxDone("POST", "/Pipeline/ChangeStatus", { id: id }, function (data) {
                        if (data.meta.errorCode == 200) {
                            location.reload();
                        } else {
                            App.ShowErrorNotLoad("Vô hiệu hóa Pipeline không thành công!");
                        }
                    });
                }
            });
        }
    }

    var DeleteSection = function (id, name) {
        swal({
            title: 'Cảnh báo',
            text: "Bạn có chắc chắn muốn xóa Section " + name + " không ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xoá'
        }).then(function (result) {
            if (result.value) {
                App.AjaxDone("POST", "/Pipeline/DeleteSection", { id: id }, function (data) {
                    if (data.meta.errorCode == 200) {
                        location.reload();
                        swal(
                            'Xóa Section thành công!'
                        )

                    } else {
                        App.ShowErrorNotLoad("Xóa Section không thành công!");
                    }
                });
            }
        });
    }

    var DeleteSectionDetail = function (sectionId, detailId, name) {
        swal({
            title: 'Cảnh báo',
            text: "Bạn có chắc chắn muốn xóa hành động " + name + " không ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xoá'
        }).then(function (result) {
            if (result.value) {
                App.AjaxDone("POST", "/Pipeline/DeleteSectionDetail", { sectionId: sectionId, detailId: detailId }, function (data) {
                    if (data.meta.errorCode == 200) {
                        location.reload();
                        swal(
                            'Xóa hành động thành công!'
                        )

                    } else {
                        App.ShowErrorNotLoad("Xóa hành động không thành công!");
                    }
                });
            }
        });
    }

    var ShowCreateUpdatePipelineModal = function (id) {
        $('#modalContent').html('');
        App.AjaxSuccess("POST", "/Pipeline/CreateUpdatePipelinePartial", { id: id }, function (data) {
            $('#modalContent').html(data);
            $('#modalUpdate').modal('show');
        }, function (response) {
            console.log(response);
        });
    }

    var ShowCreateUpdateSectionModal = function (pipelineId, sectionId) {
        $('#modalContent').html('');
        App.AjaxSuccess("POST", "/Pipeline/CreateUpdateSectionPartial", { pipelineId: pipelineId, sectionId: sectionId }, function (data) {
            $('#modalContent').html(data);
            $('#modalUpdate').modal('show');
        }, function (response) {
            console.log(response);
        });
    }

    var ShowSectionDetailModal = function (pipelineId, sectionId, detailId) {
        $('#modalContent').html('');
        if (detailId == 0) {
            // create new
            App.AjaxSuccess("POST", "/Pipeline/SectionDetailPartial", { sectionId: sectionId, pipelineId: pipelineId }, function (data) {
                $('#modalContent').html(data);
                $('#hdPipelineId').val(pipelineId);
                $('.m_selectpicker').selectpicker();
                $('#modalUpdate').modal('show');
            }, function (response) {
                console.log(response);
            });
        }
        else {
            // edit
        }
    }

    var ShowSectionDetailModal = function (pipelineId, sectionId, detailId) {
        $('#modalContent').html('');
        if (detailId == 0) {
            // create new
            App.AjaxSuccess("POST", "/Pipeline/SectionDetailPartial", { sectionId: sectionId, pipelineId: pipelineId }, function (data) {
                $('#modalContent').html(data);
                $('#hdPipelineId').val(pipelineId);
                $('.m_selectpicker').selectpicker();
                $('#modalUpdate').modal('show');
            }, function (response) {
                console.log(response);
            });
        }
        else {
            App.AjaxSuccess("POST", "/Pipeline/EditSectionDetailPartial", { id: detailId }, function (data) {
                $('#modalContent').html(data);
                $('.m_selectpicker').selectpicker();
                $('#modalUpdate').modal('show');
            }, function (response) {
                console.log(response);
            });
        }
    }

    var AddSection = function () {
        var sectionId = parseInt($("#slSection").val());
        var pipelineId = parseInt($('#hdPipelineId').val());
        if (sectionId > 0) {
            App.Ajax("POST", "/Pipeline/AddSection", { sectionId: sectionId, pipelineId: pipelineId }, function (data) {
                if (data.meta.errorCode == 200) {
                    App.ShowSuccessRedirect("Thành công", '/pipeline/index.html');

                } else {
                    App.ShowErrorNotLoad("Thêm section không thành công!");
                }
            });
        }
        else {
            // Create section then add section
        }
    }

    var OnApproveSectionChanged = function (index) {
        console.log($('.approveContainer[data-index="' + index + '"]').find(".slApproveSection.m_selectpicker"));
        var sectionId = parseInt($('.approveContainer[data-index="' + index + '"]').find(".slApproveSection.m_selectpicker ").val());
        console.log(sectionId);
        if (sectionId > 0) {
            for (var i = 0; i < sections.length; i++) {
                if (sections[i].sectionId == sectionId) {
                    var html = '';
                    for (var j = 0; j < sections[i].details.length; j++) {
                        if (sections[i].details.length == 1) {
                            html += '<option value="' + sections[i].details[j].sectionDetailId + '" selected>' + sections[i].details[j].name + '</option>';
                        }
                        else {
                            html += '<option value="' + sections[i].details[j].sectionDetailId + '">' + sections[i].details[j].name + '</option>';
                        }
                    }
                    $('.approveContainer[data-index="' + index + '"]').find(".slApproveSectionDetail.m_selectpicker").html(html);
                    $('.approveContainer[data-index="' + index + '"]').find(".slApproveSectionDetail.m_selectpicker").selectpicker('refresh');
                    break;
                }
            }
        }
    }


    var OnDisapproveSectionChanged = function (index) {
        console.log($('.disapproveContainer[data-index="' + index + '"]').find(".slDisapproveSection.m_selectpicker"));
        var sectionId = parseInt($('.disapproveContainer[data-index="' + index + '"]').find(".slDisapproveSection.m_selectpicker ").val());
        console.log(sectionId);
        if (sectionId > 0) {
            for (var i = 0; i < sections.length; i++) {
                if (sections[i].sectionId == sectionId) {
                    var html = '';
                    for (var j = 0; j < sections[i].details.length; j++) {
                        if (sections[i].details.length == 1) {
                            html += '<option value="' + sections[i].details[j].sectionDetailId + '" selected>' + sections[i].details[j].name + '</option>';
                        }
                        else {
                            html += '<option value="' + sections[i].details[j].sectionDetailId + '">' + sections[i].details[j].name + '</option>';
                        }
                    }
                    $('.disapproveContainer[data-index="' + index + '"]').find(".slDisapproveSectionDetail.m_selectpicker").html(html);
                    $('.disapproveContainer[data-index="' + index + '"]').find(".slDisapproveSectionDetail.m_selectpicker").selectpicker('refresh');
                    break;
                }
            }
        }
    }

    //var OnDisapproveSectionChanged = function () {
    //    var sectionId = parseInt($("#slDisapproveSection").val());
    //    if (sectionId > 0) {
    //        for (var i = 0; i < sections.length; i++) {
    //            if (sections[i].sectionId == sectionId) {
    //                var html = '';
    //                console.log(sections[i].details.length);
    //                for (var j = 0; j < sections[i].details.length; j++) {
    //                    if (sections[i].details.length == 1) {
    //                        html += '<option value="' + sections[i].details[j].sectionDetailId + '" selected>' + sections[i].details[j].name + '</option>';
    //                    }
    //                    else {
    //                        html += '<option value="' + sections[i].details[j].sectionDetailId + '">' + sections[i].details[j].name + '</option>';
    //                    }
    //                }
    //                $(".slDisapproveDetail.m_selectpicker").html(html);
    //                $(".slDisapproveDetail.m_selectpicker").selectpicker('refresh');
    //                break;
    //            }
    //        }
    //    }
    //}

    var OnReturnSectionChanged = function () {
        var sectionId = parseInt($("#slReturnSection").val());
        if (sectionId > 0) {
            for (var i = 0; i < sections.length; i++) {
                if (sections[i].sectionId == sectionId) {
                    var html = '';
                    for (var j = 0; j < sections[i].details.length; j++) {
                        if (sections[i].details.length == 1) {
                            html += '<option value="' + sections[i].details[j].sectionDetailId + '" selected>' + sections[i].details[j].name + '</option>';
                        }
                        else {
                            html += '<option value="' + sections[i].details[j].sectionDetailId + '">' + sections[i].details[j].name + '</option>';
                        }
                    }
                    $(".slReturnDetail.m_selectpicker").html(html);
                    $(".slReturnDetail.m_selectpicker").selectpicker('refresh');
                    break;
                }
            }
        }
    }

    var OnInitialPropertyChanged = function (index, type) {
        //get container		
        var container;
        if (type == 1)
            container = $("#initialValuesContainer").find('.initial-value-container[data-index="' + index + '"]');
        else if (type == 10)
            container = $("#eventValuesContainer").find('.initial-value-container[data-index="' + index + '"]');
        else if (type == 11)
            container = $("#disapproveEventValuesContainer").find('.initial-value-container[data-index="' + index + '"]');
        else if (type == 12)
            container = $("#returnEventValuesContainer").find('.initial-value-container[data-index="' + index + '"]');
        else
            container = $("#returnValuesContainer").find('.initial-value-container[data-index="' + index + '"]');
        var propertyId = parseInt(container.find("select").val());
        if (propertyId > 0) {
            for (var i = 0; i < properties.length; i++) {
                if (properties[i].propertyId == propertyId) {
                    // show input text							
                    App.AjaxSuccess("POST", "/Pipeline/GetPropertyPartial", { id: propertyId }, function (data) {
                        container.find(".valueContainer").html("");
                        container.find(".valueContainer").html(data);
                        container.find(".m_selectpicker").selectpicker('render');
                    }, function () { });
                    break;
                }
            }
        }
    }

    var OnConditionPropertyChanged = function (parentIndex, index, type) {              
        var container = $('.approveContainer[data-index="' + parentIndex + '"]').find('.condition-value-container[data-index="' + index + '"]');               
        var propertyId = parseInt(container.find("select").val());
        if (propertyId > 0) {
            for (var i = 0; i < properties.length; i++) {
                if (properties[i].propertyId == propertyId) {
                    App.AjaxSuccess("POST", "/Pipeline/GetPropertyPartial", { id: propertyId }, function (data) {
                        container.find(".valueContainer").html("");
                        container.find(".valueContainer").html(data);
                        container.find(".m_selectpicker").selectpicker('render');
                    }, function () { });
                    break;
                }
            }
        }
    }

    var OnDisapproveConditionPropertyChanged = function (parentIndex, index) {
        var container = $('.disapproveContainer[data-index="' + parentIndex + '"]').find('.condition-value-container[data-index="' + index + '"]');
        var propertyId = parseInt(container.find("select").val());
        if (propertyId > 0) {
            for (var i = 0; i < properties.length; i++) {
                if (properties[i].propertyId == propertyId) {
                    App.AjaxSuccess("POST", "/Pipeline/GetPropertyPartial", { id: propertyId }, function (data) {
                        container.find(".valueContainer").html("");
                        container.find(".valueContainer").html(data);
                        container.find(".m_selectpicker").selectpicker('render');
                    }, function () { });
                    break;
                }
            }
        }
    }

    var OnAddInitialPropertyClick = function () {
        var length = $("#initialValuesContainer").find('.initial-value-container').length;
        App.AjaxSuccess("POST", "/Pipeline/InitialValuePropertyPartial", { index: length, type: 1 }, function (data) {
            var container = $("#initialValuesContainer");
            container.append(data);
            container.find(".m_selectpicker").selectpicker('render');
        }, function () { });
    }

    var OnAddReturnPropertyClick = function () {
        var length = $("#returnValuesContainer").find('.initial-value-container').length;
        App.AjaxSuccess("POST", "/Pipeline/InitialValuePropertyPartial", { index: length, type: 2 }, function (data) {
            var container = $("#returnValuesContainer");
            container.append(data);
            container.find(".m_selectpicker").selectpicker('render');
        }, function () { });
    }

    var OnAddConditionPropertyClick = function (index) {
        var length = $('.approveContainer[data-index="' + index + '"]').find('.conditionValuesContainer').find(".condition-value-container").length;
        App.AjaxSuccess("POST", "/Pipeline/ConditionValuePropertyPartial", { id: index, index: length }, function (data) {
            var container = $('.approveContainer[data-index="' + index + '"]').find('.conditionValuesContainer');
            container.append(data);
            container.find(".m_selectpicker").selectpicker('render');
        }, function () { });
    }

    var OnDisapproveAddConditionPropertyClick = function (index) {
        var length = $('.disapproveContainer[data-index="' + index + '"]').find('.conditionValuesContainer').find(".condition-value-container").length;
        App.AjaxSuccess("POST", "/Pipeline/DisapproveConditionValuePropertyPartial", { id: index, index: length }, function (data) {
            var container = $('.disapproveContainer[data-index="' + index + '"]').find('.conditionValuesContainer');
            container.append(data);
            container.find(".m_selectpicker").selectpicker('render');
        }, function () { });
    }

    var OnAddApproveSectionClick = function (pipelineId) {
        var length = $('.approveContainer').length;
        App.AjaxSuccess("POST", "/Pipeline/ApproveSectionPartial", { index: length, id: pipelineId }, function (data) {
            var container = $("#approveSectionContainer");
            container.append(data);
            container.find(".m_selectpicker").selectpicker('render');
        }, function () { });
    }

    var OnAddDisapproveSectionClick = function (pipelineId) {
        var length = $('.disapproveContainer').length;
        App.AjaxSuccess("POST", "/Pipeline/DisapproveSectionPartial", { index: length, id: pipelineId }, function (data) {
            var container = $("#disapproveSectionContainer");
            container.append(data);
            container.find(".m_selectpicker").selectpicker('render');
        }, function () { });
    }

    var OnCreateSectionDetail = function () {
        var sectionId = parseInt($("#slSection").val());
        if (sectionId <= 0) {
            swal(
                'Vui lòng chọn bộ phận!'
            );
            return;
        }
        var name = $("#sectionName").val();
        var mode = $("#slMode").val();
        // section actions
        var actionContainers = $('.initial-value-container');
        var actions = [];
        if (actionContainers != undefined && actionContainers.length > 0) {
            for (var i = 0; i < actionContainers.length; i++) {
                var action = {};
                var container = $(actionContainers[i]);
                var propertyId = container.find("select.property").val();
                var property;
                for (var j = 0; j < properties.length; j++) {
                    if (properties[j].propertyId == propertyId) {
                        property = properties[j];
                        break;
                    }
                }
                if (property != undefined) {
                    action.propertyId = parseInt(propertyId);
                    action.type = parseInt(container.find('.hdType').val());
                    if (container.find("select.propertyValue").length) {
                        if (Array.isArray(container.find("select.propertyValue").val())) {
                            var values = container.find("select.propertyValue").val().join(",");
                            action.value = values;
                        }
                        else {
                            action.value = container.find("select.propertyValue").val();
                        }
                    }
                    else
                        action.value = container.find(".propertyValue").val();
                    actions.push(action);
                }
            }
        }
        // approve
        var approveContainer = $(".approveContainer");
        var approves = [];
        if (approveContainer != undefined && approveContainer.length > 0) {
            for (var i = 0; i < approveContainer.length; i++) {
                var approve = {};
                var container = $('.approveContainer[data-index="' + i + '"]');
                var approveId = parseInt(container.find(".slApproveSectionDetail.m_selectpicker").val());
                if (approveId > 0) {
                    approve.approveId = approveId;
                    var conditions = [];
                    var conditionContainer = container.find('.condition-value-container');
                    if (conditionContainer != undefined && conditionContainer.length > 0) {
                        for (var j = 0; j < conditionContainer.length; j++) {
                            var condition = {};
                            var containerCond = $(conditionContainer[j]);
                            var propertyId = containerCond.find("select.property").val();
                            var property;
                            for (var k = 0; k < properties.length; k++) {
                                if (properties[k].propertyId == propertyId) {
                                    property = properties[k];
                                    break;
                                }
                            }
                            if (property != undefined) {
                                condition.propertyId = parseInt(propertyId);
                                condition.operator = containerCond.find("select.conditionOperator").val();
                                if (containerCond.find("select.propertyValue").length) {
                                    if (Array.isArray(containerCond.find("select.propertyValue").val())) {
                                        var values = containerCond.find("select.propertyValue").val().join(",");
                                        condition.value = values;
                                    }
                                    else {
                                        condition.value = containerCond.find("select.propertyValue").val();
                                    }
                                }
                                else
                                    condition.value = containerCond.find(".propertyValue").val();
                                conditions.push(condition);
                            }
                        }
                        approve.conditions = conditions;
                        approves.push(approve);
                    }
                }
            }
        }
        // disapprove
        var disapproveContainer = $(".disapproveContainer");
        var disapproves = [];
        if (disapproveContainer != undefined && disapproveContainer.length > 0) {
            for (var i = 0; i < disapproveContainer.length; i++) {
                var approve = {};
                var container = $('.disapproveContainer[data-index="' + i + '"]');
                var approveId = parseInt(container.find(".slDisapproveSectionDetail.m_selectpicker").val());
                if (approveId > 0) {
                    approve.approveId = approveId;
                    var conditions = [];
                    var conditionContainer = container.find('.condition-value-container');
                    if (conditionContainer != undefined && conditionContainer.length > 0) {
                        for (var j = 0; j < conditionContainer.length; j++) {
                            var condition = {};
                            var containerCond = $(conditionContainer[j]);
                            var propertyId = containerCond.find("select.property").val();
                            var property;
                            for (var k = 0; k < properties.length; k++) {
                                if (properties[k].propertyId == propertyId) {
                                    property = properties[k];
                                    break;
                                }
                            }
                            if (property != undefined) {
                                condition.propertyId = parseInt(propertyId);
                                condition.operator = containerCond.find("select.conditionOperator").val();
                                if (containerCond.find("select.propertyValue").length) {
                                    if (Array.isArray(containerCond.find("select.propertyValue").val())) {
                                        var values = containerCond.find("select.propertyValue").val().join(",");
                                        condition.value = values;
                                    }
                                    else {
                                        condition.value = containerCond.find("select.propertyValue").val();
                                    }
                                }
                                else
                                    condition.value = containerCond.find(".propertyValue").val();
                                conditions.push(condition);
                            }
                        }
                        approve.conditions = conditions;
                        disapproves.push(approve);
                    }
                }
            }
        }
        // return
        var returnId = parseInt($(".slReturnDetail.m_selectpicker").val());
        // disapprove
        // var disapproveId = parseInt($(".slDisapproveDetail.m_selectpicker").val());
        var data = {
            sectionDetail: 0,
            name: name,
            mode: mode,
            sectionId: sectionId,
            actions: actions,
            approves: approves,
            returnId: returnId,
            disapproves: disapproves          
        };        
        App.AjaxDone("POST", "/Pipeline/CreateSectionDetail", data, function (data) {
            if (data.meta.errorCode == 200) {
                App.ShowSuccessRedirect("Tạo hành động thành công", '/pipeline/index.html');

            } else {
                App.ShowErrorNotLoad("Tạo hành động không thành công!");
            }
        });
    }

    var OnUpdateSectionDetail = function () {
        var sectionId = parseInt($("#slSection").val());
        if (sectionId <= 0) {
            swal(
                'Vui lòng chọn bộ phận!'
            );
            return;
        }
        var name = $("#sectionName").val();
        var mode = $("#slMode").val();
        // section actions
        var actionContainers = $('.initial-value-container');
        var actions = [];
        if (actionContainers != undefined && actionContainers.length > 0) {
            for (var i = 0; i < actionContainers.length; i++) {
                var action = {};
                var container = $(actionContainers[i]);
                var propertyId = container.find("select.property").val();
                var property;
                for (var j = 0; j < properties.length; j++) {
                    if (properties[j].propertyId == propertyId) {
                        property = properties[j];
                        break;
                    }
                }
                if (property != undefined) {
                    action.propertyId = parseInt(propertyId);
                    action.type = parseInt(container.find('.hdType').val());
                    if (container.find("select.propertyValue").length) {
                        //action.value = container.find("select.propertyValue").val();
                        if (Array.isArray(container.find("select.propertyValue").val())) {
                            var values = container.find("select.propertyValue").val().join(",");
                            action.value = values;//containerCond.find("select.propertyValue").val();
                        }
                        else {
                            action.value = container.find("select.propertyValue").val();
                        }
                    }
                    else
                        action.value = container.find(".propertyValue").val();
                    actions.push(action);
                }
            }
        }
        // approve
        var approveContainer = $(".approveContainer");
        var approves = [];
        if (approveContainer != undefined && approveContainer.length > 0) {
            for (var i = 0; i < approveContainer.length; i++) {
                var approve = {};
                var container = $('.approveContainer[data-index="' + i + '"]');
                var approveId = parseInt(container.find(".slApproveSectionDetail.m_selectpicker").val());
                console.log(approveId);
                if (approveId > 0) {
                    approve.approveId = approveId;
                    var conditions = [];
                    var conditionContainer = container.find('.condition-value-container');
                    if (conditionContainer != undefined && conditionContainer.length > 0) {
                        for (var j = 0; j < conditionContainer.length; j++) {
                            var condition = {};
                            var containerCond = $(conditionContainer[j]);
                            var propertyId = containerCond.find("select.property").val();
                            var property;
                            for (var k = 0; k < properties.length; k++) {
                                if (properties[k].propertyId == propertyId) {
                                    property = properties[k];
                                    break;
                                }
                            }
                            if (property != undefined) {
                                condition.propertyId = parseInt(propertyId);
                                condition.operator = containerCond.find("select.conditionOperator").val();
                                if (containerCond.find("select.propertyValue").length) {
                                    if (Array.isArray(containerCond.find("select.propertyValue").val())) {
                                        var values = containerCond.find("select.propertyValue").val().join(",");
                                        condition.value = values;
                                    }
                                    else {
                                        condition.value = containerCond.find("select.propertyValue").val();
                                    }
                                }
                                else
                                    condition.value = containerCond.find(".propertyValue").val();
                                conditions.push(condition);
                            }
                        }
                        approve.conditions = conditions;
                        approves.push(approve);
                    }
                    else {
                        approves.push(approve);
                    }
                }
            }
        }
        // approve
        var disapproveContainer = $(".disapproveContainer");
        var disapproves = [];
        if (disapproveContainer != undefined && disapproveContainer.length > 0) {
            for (var i = 0; i < disapproveContainer.length; i++) {
                var approve = {};
                var container = $('.disapproveContainer[data-index="' + i + '"]');
                var approveId = parseInt(container.find(".slDisapproveSectionDetail.m_selectpicker").val());
                console.log(approveId);
                if (approveId > 0) {
                    approve.approveId = approveId;
                    var conditions = [];
                    var conditionContainer = container.find('.condition-value-container');
                    if (conditionContainer != undefined && conditionContainer.length > 0) {
                        for (var j = 0; j < conditionContainer.length; j++) {
                            var condition = {};
                            var containerCond = $(conditionContainer[j]);
                            var propertyId = containerCond.find("select.property").val();
                            var property;
                            for (var k = 0; k < properties.length; k++) {
                                if (properties[k].propertyId == propertyId) {
                                    property = properties[k];
                                    break;
                                }
                            }
                            if (property != undefined) {
                                condition.propertyId = parseInt(propertyId);
                                condition.operator = containerCond.find("select.conditionOperator").val();
                                if (containerCond.find("select.propertyValue").length) {
                                    if (Array.isArray(containerCond.find("select.propertyValue").val())) {
                                        var values = containerCond.find("select.propertyValue").val().join(",");
                                        condition.value = values;
                                    }
                                    else {
                                        condition.value = containerCond.find("select.propertyValue").val();
                                    }
                                }
                                else
                                    condition.value = containerCond.find(".propertyValue").val();
                                conditions.push(condition);
                            }
                        }
                        approve.conditions = conditions;
                        disapproves.push(approve);
                    }
                    else {
                        disapproves.push(approve);
                    }
                }
            }
        }
        // return
        var returnId = parseInt($(".slReturnDetail.m_selectpicker").val());
        // disapprove        
        var sectionDetailId = parseInt($("#SectionDetailId").val());
        var data = {
            sectionDetailId: sectionDetailId,
            name: name,
            mode: mode,
            sectionId: sectionId,
            actions: actions,
            approves: approves,
            returnId: returnId,
            disapproves: disapproves,
        };
        App.AjaxDone("POST", "/Pipeline/UpdateSectionDetail", data, function (data) {
            if (data.meta.errorCode == 200) {
                App.ShowSuccessRedirect("Cập nhật hành động thành công", '/pipeline/index.html');
            } else {
                App.ShowErrorNotLoad("Cập nhật hành động không thành công!");
            }
        });
    }

    return {
        Init: Init,
        AssignValues: AssignValues,
        DeletePipeline: DeletePipeline,
        ChangeStatus: ChangeStatus,
        DeleteSection: DeleteSection,
        DeleteSectionDetail: DeleteSectionDetail,
        CreateUpdatePipeline: CreateUpdatePipeline,
        CreateUpdateSection: CreateUpdateSection,
        ShowCreateUpdatePipelineModal: ShowCreateUpdatePipelineModal,
        ShowCreateUpdateSectionModal: ShowCreateUpdateSectionModal,
        ShowSectionDetailModal: ShowSectionDetailModal,
        AddSection: AddSection,
        OnApproveSectionChanged: OnApproveSectionChanged,
        OnDisapproveSectionChanged: OnDisapproveSectionChanged,
        OnReturnSectionChanged: OnReturnSectionChanged,
        OnConditionPropertyChanged: OnConditionPropertyChanged,
        OnInitialPropertyChanged: OnInitialPropertyChanged,
        OnAddInitialPropertyClick: OnAddInitialPropertyClick,
        OnAddConditionPropertyClick: OnAddConditionPropertyClick,
        OnAddApproveSectionClick: OnAddApproveSectionClick,
        OnCreateSectionDetail: OnCreateSectionDetail,
        OnUpdateSectionDetail: OnUpdateSectionDetail,
        OnAddReturnPropertyClick: OnAddReturnPropertyClick,
        OnDisapproveAddConditionPropertyClick: OnDisapproveAddConditionPropertyClick,
        OnDisapproveConditionPropertyChanged: OnDisapproveConditionPropertyChanged,
        OnAddDisapproveSectionClick: OnAddDisapproveSectionClick
    };
}

$(document).ready(function () {
    PipelineModule.Init();
});