﻿var PrintModel = new function () {
    var ContractFinancialAgreement = function (loanbriefId) {
        if (loanbriefId > 0) {
            $.ajax({
                type: "POST",
                url: "/Print/ContractFinancialAgreement",
                data: { loanbriefId: loanbriefId }
            }).done(function (data) {
                var mywindow = window.open('', '', 'height=' + $(window).height() + ',width=' + $(window).width());
                mywindow.document.write('<html><head><title></title>');
                mywindow.document.write('</head><body>');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');
                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10
                mywindow.print();
                mywindow.close();

                return true;
            });
        }
        else {
            App.ShowErrorNotLoad("Không tìm thấy hợp đồng");
        }
    }

    var CommittedPaper = function (loanbriefId) {
        if (loanbriefId > 0) {
            $.ajax({
                type: "POST",
                url: "/Print/CommittedPaper",
                data: { loanbriefId: loanbriefId }
            }).done(function (data) {
                var mywindow = window.open('', '', 'height=' + $(window).height() + ',width=' + $(window).width());
                mywindow.document.write('<html><head><title></title>');
                mywindow.document.write('</head><body>');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');
                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10
                mywindow.print();
                mywindow.close();

                return true;
            });
        }
        else {
            App.ShowErrorNotLoad("Không tìm thấy hợp đồng");
        }
    }

    var AddendumAgreementPledgeOfProperty = function (loanbriefId) {
        if (loanbriefId > 0) {
            $.ajax({
                type: "POST",
                url: "/Print/AddendumAgreementPledgeOfProperty",
                data: { loanbriefId: loanbriefId }
            }).done(function (data) {
                var mywindow = window.open('', '', 'height=' + $(window).height() + ',width=' + $(window).width());
                mywindow.document.write('<html><head><title></title>');
                mywindow.document.write('</head><body>');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');
                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10
                mywindow.print();
                mywindow.close();

                return true;
            });
        }
        else {
            App.ShowErrorNotLoad("Không tìm thấy hợp đồng");
        }
    }

    return {
        ContractFinancialAgreement: ContractFinancialAgreement,
        CommittedPaper: CommittedPaper,
        AddendumAgreementPledgeOfProperty: AddendumAgreementPledgeOfProperty
    }
}