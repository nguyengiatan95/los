﻿
var ProductModule = new function () {
    var Init = function () {
        $("#sl_brand_product").select2({
            placeholder: "Chọn thương hiệu xe",
            width: '100%'
        });
        $("#sl_product_name").select2({
            placeholder: "Chọn xe",
            width: '100%'
        });
        $("#sl_OwnerProduct").select2({
            placeholder: "Hình thức sở hữu xe",
            width: '100%'
        });
        $("#sl_TypeOwnerShip").select2({
            placeholder: "Hình thức sở hữu nhà",
            width: '100%'
        });
        $('#sl_BrandProduct').on('change', function () {
            // ProductModule.GetProductByBrand();
        });
        $('#sl_productId').on('change', function () {
            ProductModule.GetPriceByProduct();
        });
        //$("#sl_TypeJob").select2({
        //    placeholder: "Loại công việc",
        //    width: '100%'
        //});
        //$("#sl_TypeJobDescription").select2({
        //    placeholder: "Phân loại công việc",
        //    width: '100%'
        //});
        $('#sl_LoanProductDetail').select2({
            placeholder: "Phân loại công việc",
            width: '100%'
        });
    };
    var GeturlEmbedMapTracking = function (url) {
        var embed = '<embed type="text/html" src="' + url + '" style="width:100%; height:800px">'
        $('#m_portlet_tab_1_4').html(embed);
    };
    //var GetMotoPrice = function (loanBriefId) {
    //    $.ajax({
    //        type: "GET",
    //        url: "/LoanBrief/MotoPrice?id=" + loanBriefId,
    //        success: function (data) {
    //            $('#m_portlet_tab_1_6').html(data);
    //            ProductModule.LoadMoney(); 
    //        },
    //        traditional: true
    //    }); 
    //};
    var GetMotoPrice = function (loanBriefId) {
        $.ajax({
            type: "GET",
            url: "/LoanBriefV2/AppraiserMotoPrice?id=" + loanBriefId,
            success: function (data) {
                if (data.isSuccess) {
                    $('#m_portlet_tab_1_6').html(data.html);
                    ProductModule.LoadMoney();
                } else {
                    App.ShowErrorNotLoad(data.message);
                }

            },
            traditional: true
        });
    };
    var MotoPriceCheckList = function (loanBriefId, price) {
        $.ajax({
            type: "GET",
            url: "/LoanBrief/MotoPriceCheckList?id=" + loanBriefId + "&price=" + price,
            success: function (data) {
                $('#_MotoPriceCheckList').html(data);
                ProductModule.LoadMoney();
            },
            traditional: true
        });
    };
    var GetProductByBrand = function () {
        $('#productCombobox').html('');
        var brandId = $('#sl_BrandProduct').val();
        if (brandId > 0) {
            $.ajax({
                type: "GET",
                url: "/Products/GetProductByBrand?brandId=" + brandId,
                success: function (data) {
                    $('#productCombobox').html(data.html);
                },
                traditional: true
            });
        }
    };
    var GetPriceByProduct = function () {
        $('#priceId').val('');
        var productId = $('#sl_BrandProduct').val();
        if (productId > 0) {
            $.ajax({
                type: "GET",
                url: "/Products/GetPriceByProduct?productId=" + productId,
                success: function (data) {
                    $('#priceId').val(data.data.predicted_price_string);
                },
                traditional: true
            });
        }
    };
    var GetProduct = (brandId, productId) =>
        new Promise((resolve, reject) => {
            if (brandId > 0) {
                App.Ajax("GET", "/Dictionary/GetProduct?brandId=" + brandId, undefined).done(function (data) {
                    if (data.data != undefined && data.data.length > 0) {
                        var html = "";
                        html += '<option value="0" >Chọn tên xe</option>';
                        for (var i = 0; i < data.data.length; i++) {
                            if (productId > 0 && productId == data.data[i].id) {
                                html += `<option value="${data.data[i].id}" selected="selected">${data.data[i].fullName}</option>`;
                            } else {
                                html += `<option value="${data.data[i].id}">${data.data[i].fullName}</option>`;
                            }
                        }
                        //$("#sl_product_name").html(html); 
                        resolve(1);
                    }
                });
            } else {
                reject(0);
            }
        });

    var GetPriceFromAiAndLos = function () {
        var ProductCredit = $("#sl_OwnerProduct").val();
        var TypeOfOwnerShip = $("#sl_TypeOwnerShip").val();
        var ProductId = $('#sl_product_name').val();
        var LoanbriefId = $('#hddLoanbriefId').val();
        var TypeJob = $('#sl_TypeJob').val();
        var TypeJobDescription = $('#sl_TypeJobDescription').val();
        var LoanProductDetailId = $('#sl_LoanProductDetail').val();
        if (ProductId > 0 && TypeOfOwnerShip > 0 && ProductCredit > 0 && LoanbriefId > 0 && LoanProductDetailId > 0) {
            //App.CheckPermisson("LoanBrief", "ProductCurrentPrice");
            $.ajax({
                url: '/LoanBriefV2/GetPriceCurrent',
                type: 'GET',
                data: {
                    'loanbriefId': LoanbriefId,
                    'productId': ProductId,
                    'typeOfOwnerShip': TypeOfOwnerShip,
                    'productCredit': ProductCredit,
                    'loanProductDetailId': LoanProductDetailId
                },
                dataType: 'json',
                success: function (res) {
                    if (res.status == 1) {
                        $('#CurrentPrice').text(App.FormatCurrency(res.currentPrice));
                        $('#MaxPrice').text(App.FormatCurrency(res.maxPrice));
                        $('#hddtotalpricediscount').val(res.currentPrice);
                        ProductModule.MotoPriceCheckList($("#LoanBriefId").val(), res.currentPrice);
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
                }

            });
        } else {
            App.ShowErrorNotLoad('Bạn phải chọn đủ thông tin xe!');
        }
    }
    var LoadInforPriceDisCount = function (ProductReviewId, state, isCancel, isCheck, moneyDiscount, divpricediscount) {
        // Không thỏa mãn tiêu chí thì bị trừ tiền
        if (state != isCheck) {
            // Đánh dấu trạng thái bị từ chối
            if (isCancel == 'True')
                $("#hddIsCancel" + ProductReviewId).val(1);

            if (moneyDiscount > 0) {
                var strmoneyDiscount = formatMoneyTDX(moneyDiscount, '.', ',');
                // Hiển thị số tiền giảm trừ theo tiêu chí tương ứng
                // $("#"+divpricediscount).html("-"+strmoneyDiscount+"&nbsp;(đ)");
                // $("#"+divpricediscount).css("display","block");
                if ($("#hddtotalmoneytheotieuchi" + ProductReviewId).val() == 0) {
                    $("#hddtotalmoneytheotieuchi" + ProductReviewId).val(parseInt(moneyDiscount))
                }
                // Hiển thị số tiền KH nhận được
                LoadMoney();
            }
            else {
                $("#" + divpricediscount).css("display", "none");
                $("#" + divpricediscount).html("");
                LoadMoney();
            }
        }
        else {
            // Bỏ đánh dấu trạng thái bị từ chối
            $("#hddIsCancel" + ProductReviewId).val(0);
            // Ẩn số tiền giảm trừ theo tiêu chí tương ứng
            // $("#"+divpricediscount).css("display","none");
            // Gán lại số tiền theo tiêu chí
            if ($("#hddtotalmoneytheotieuchi" + ProductReviewId).val() > 0) {
                $("#hddtotalmoneytheotieuchi" + ProductReviewId).val(parseInt($("#hddtotalmoneytheotieuchi" + ProductReviewId).val()) - parseInt(moneyDiscount));
            }
            // Hiển thị số tiền KH nhận được
            LoadMoney();
        }
    }

    var LoadMoney = function () {
        // Số tiền lúc đầu đã giảm trừ phần trăm theo năm
        var totalpricediscount = $("#hddtotalpricediscount").val();
        $("#totalmoneyPrevious").html(formatMoneyTDX(totalpricediscount, '.', ',') + " VNĐ");
        // Hiển thị số tiền KH nhận được tối đa ( chưa trừ tiêu chí)
        $("#divtotalpricediscount").html(formatMoneyTDX(RoundingMoney(totalpricediscount), '.', ',') + " VNĐ");
        // Số tiền cộng dồn theo tiêu chí
        var totalpriceReview = 0;
        $('.widget-content input[class=hddtotaltieuchi]').each(function () {
            totalpriceReview = parseInt(totalpriceReview) + parseInt($(this).val());
        });
        // Số tiền KH nhận được ( đã trừ tiêu chí)
        var totalpriceforcustommer = parseInt(totalpricediscount) - parseInt(totalpriceReview);

        // Hiển thị số tiền KH nhận được
        var isCancel = 0;
        $('.widget-content input[class=hddIsCancel]').each(function () {

            if ($(this).val() == 1) {
                isCancel = 1;
                return false;
            }
        });

        if (totalpriceforcustommer < 0 || (isCancel == 1)) {
            $("#divtotalpriceforcustommer").html("0 &nbsp;VNĐ");
        }
        else {
            $("#divtotalpriceforcustommer").html(formatMoneyTDX(RoundingMoney(totalpriceforcustommer), '.', ',') + "&nbsp;VNĐ");
        }
    }
    var formatMoneyTDX = function (nStr, decSeperate, groupSeperate) {
        nStr += '';
        x = nStr.split(decSeperate);
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
        }
        return x1 + x2;
    }
    function RoundingMoney(totalmoney) {
        // nếu là xe chính chủ / xe không chính chủ
        var ProductId = $("#hddLoanBriefProductId").val();
        var TypeOfOwnerShip = $("#sl_TypeOwnerShip").val();
        var TypeJob = $('#sl_TypeJob').val();
        var TypeJobDescription = $('#sl_TypeJobDescription').val();
        var LoanProductDetailId = $('#sl_LoanProductDetail').val();
        var getPrice = GetMaxPriceV3(ProductId, totalmoney, TypeOfOwnerShip, LoanProductDetailId);
        return getPrice;
    }
    var RoundMoney = function (totalmoney) {
        // làm tròn số
        var total1 = parseInt(totalmoney) % parseInt(1000000);
        if (parseInt(total1) >= 500000) {
            var total2 = totalmoney - total1;
            totalmoney = parseInt(total2) + 1000000;
        }
        else {
            var total2 = totalmoney - total1;
            totalmoney = total2;
        }
        return totalmoney;
    }
    var UnitMillion = function (totalmoney) {
        // làm tròn số
        var total1 = parseInt(totalmoney) % parseInt(1000000);
        return totalmoney - total1;
    }
    var RoundingTo = function (myNum, roundTo) {
        if (roundTo <= 0) return myNum;
        return (myNum + roundTo / 2) / roundTo * roundTo;
    }
    var MaxPrice = function (TypeOfOwnerShip, TypeJob, TypeJobDescription) {
        //Nhà sở hữu
        if (TypeOfOwnerShip == 9
            || TypeOfOwnerShip == 10) {
            if (TypeJob == 123) {//làm hưởng lương
                return 30000000;//30tr
            }
            else if (TypeJob == 124) {// Tự kinh doanh
                return 15000000;//15tr
            }
            else if (TypeJob == 126) {//Làm tự do
                return 15000000;//15tr
            }
            if (TypeJob == 125) {//Làm tài xế công nghệ
                return 15000000;//15tr
            }
        }
        else {
            if (TypeJob == 123) {//làm hưởng lương
                if (TypeJobDescription == 1) {//làm hưởng lương chuyển khoản
                    return 15000000;//10tr
                }
            }
            else if (TypeJob == 124) {// Tự kinh doanh
                return 10000000;//10tr
            }
            else if (TypeJob == 126) {//Làm tự do
                return 10000000;//15tr
            }
            if (TypeJob == 125) {//Làm tài xế công nghệ
                return 10000000;//15tr
            }

        }
        return 10000000;
    }

    var SetThamDinhXe = function () {

        var productId = $("#sl_product_name").val();
        var loanCreditId = $("#hdd_LoanCreditId").val();
        var totalprice = 0;
        var dataProductArr = [];
        var numberchecked = 0;
        $('.ptb5 input[type=radio]').each(function () {
            if ($(this).is(":checked")) {
                numberchecked += 1;
                // Add vao object 
                var ProductReviewId = $(this).attr("tabindex").split("|")[1];
                var IsCheck = ($(this).attr("tabindex").split("|")[0] == 0) ? false : true;
                var newObject = { "LoanBriefId": loanCreditId, "ProductId": productId, "ProductReviewId": ProductReviewId, "IsCheck": IsCheck };
                dataProductArr.push(newObject);
            }
        });
        if (numberchecked != $("#hddNumberProductPreview").val()) {
            App.ShowErrorNotLoad("Bạn hãy chọn đầy đủ thông tin!");
            return;
        }

        // Add them parentId

        $(".TDXParentID a").each(function () {
            var ProductReviewId = $(this).attr("title");
            var newObject = { "LoanBriefId": loanCreditId, "ProductId": productId, "ProductReviewId": ProductReviewId, "IsCheck": true };
            dataProductArr.push(newObject);
        });

        //////Tính tổng số tiền KH được nhận//////
        // Số tiền lúc đầu đã giảm trừ phần trăm theo năm
        var totalpricediscount = $("#hddtotalpricediscount").val();
        // Số tiền trước khi thẩm định
        var totalMoneyExpertise = totalpricediscount;

        // Số tiền cộng dồn theo tiêu chí
        var totalpriceReview = 0;
        $('.widget-content input[class=hddtotaltieuchi]').each(function () {
            totalpriceReview = parseInt(totalpriceReview) + parseInt($(this).val());
        });

        // Số tiền KH nhận được sau khi giảm trừ
        totalprice = RoundingMoney((parseInt(totalpricediscount) - parseInt(totalpriceReview)));

        var isCancelStr = 0;
        $('.widget-content input[class=hddIsCancel]').each(function () {

            if ($(this).val() == 1) {
                isCancelStr = 1;
                return false;
            }
        });

        if ((totalprice <= 0) || (isCancelStr == 1))
            totalprice = 0;

        var strjson = JSON.stringify(dataProductArr);
        $.ajax({
            type: "POST",
            url: "/Products/UpdateProductResultReview/",
            data: { productId: productId, totalMoney: totalprice, loanBriefId: loanCreditId, productReviewResults: dataProductArr, totalMoneyExpertise: totalMoneyExpertise }
        }).done(function (result) {
            if (result.isSuccess) {
                App.ShowSuccessNotLoad(result.message);
                $("#txtAgencyMoney").val(formatMoneyTDX(totalprice, '.', ','));
                $("#formLoanListCredit").submit();
                return;
            }
            else {
                App.ShowErrorNotLoad(result.message);
                return;
            }
        });
    }

    var GetMaxPriceV3 = function (ProductId, originalCarPrice, typeOfOwnerShip, productDetailId) {
        var maxPriceProduct = 0;
        if (ProductId == ProductCredit.MotorCreditType_CC) //xe cc
        {
            //70% giá trị định giá xe hiện tại
            var percent70 = originalCarPrice * 70 / 100;
            maxPriceProduct = RoundMoney(percent70);
            var max = MaxPriceV2(typeOfOwnerShip, productDetailId);
            if (maxPriceProduct > max)
                maxPriceProduct = max;
        }
        else if (ProductId == ProductCredit.MotorCreditType_KCC || ProductId == ProductCredit.MotorCreditType_KGT)//(int)EnumProductCredit.MotorCreditType_KCC
        {
            var percent50 = originalCarPrice * 60 / 100;
            maxPriceProduct = RoundMoney(percent50);
            var max = MaxPriceV2(typeOfOwnerShip, productDetailId);
            if (maxPriceProduct > max)
                maxPriceProduct = max;
        }
        return maxPriceProduct;
    }

    var MaxPriceV2 = function (typeOfOwnerShip, productDetailId) {
        //Nhà sở hữu
        if (productDetailId > 0) {
            if (productDetailId == ProductDetail.VayNhanh4tr || productDetailId == ProductDetail.KT1_KSHK_4tr
                || productDetailId == ProductDetail.KT1_KGT_4tr || productDetailId == ProductDetail.KT3_KTN_4tr
                || productDetailId == ProductDetail.KT3_KGT_4tr)
                return 4000000;
            else if (productDetailId == ProductDetail.KT1_10 || productDetailId == ProductDetail.KT1_KGT_10
                || productDetailId == ProductDetail.KT3_KGT_10)
                return 10000000;
            else if (productDetailId == ProductDetail.KT1_20 || productDetailId == ProductDetail.KT1_KGT_20)
                return 20000000;
            else if (productDetailId == ProductDetail.KT1_30)
                return 30000000;
            else if (productDetailId == ProductDetail.KT3_10)
                return 10000000;
            else if (productDetailId == ProductDetail.KT3_15 || productDetailId == ProductDetail.KT1_KGT_15
                || productDetailId == ProductDetail.KT3_KGT_15)
                return 15000000;
            else if (productDetailId == ProductDetail.KT3_20)
                return 20000000;
        } else {
            //Nếu chưa chọn chi tiết gói vay 
            if (ConvertTypeOfOwnerShip(typeOfOwnerShip) == TypeOfOwnerShip.KT1)//KT1
                return 30000000;
            else if (ConvertTypeOfOwnerShip(typeOfOwnerShip) == TypeOfOwnerShip.KT3)//KT3
                return 20000000;
        }
        return 10000000;
    }

    var ConvertTypeOfOwnerShip = function (typeOfOwnerShip) {
        if (TypeOfOwnerShip == TypeOfOwnerShip.Sohuucanhan || //EnumTypeofownership.Sohuucanhan.GetHashCode()
            TypeOfOwnerShip == TypeOfOwnerShip.Dongsohuu || //EnumTypeofownership.Dongsohuu.GetHashCode()
            TypeOfOwnerShip == TypeOfOwnerShip.NhaSoHuu ||  //EnumTypeofownership.NhaSoHuu.GetHashCode()
            TypeOfOwnerShip == TypeOfOwnerShip.DungTenBoMe || //EnumTypeofownership.DungTenBoMe.GetHashCode()
            TypeOfOwnerShip == TypeOfOwnerShip.DungTenVoChong || //EnumTypeofownership.DungTenVoChong.GetHashCode()
            TypeOfOwnerShip == TypeOfOwnerShip.NhaSoHuuTrungSHK || //EnumTypeofownership.NhaSoHuuTrungSHK.GetHashCode()
            TypeOfOwnerShip == TypeOfOwnerShip.NhaSoHuuKhongTrungSHK || //EnumTypeofownership.NhaSoHuuKhongTrungSHK.GetHashCode()
            TypeOfOwnerShip == TypeOfOwnerShip.NhaThueCungTinh || //EnumTypeofownership.NhaThueCungTinh.GetHashCode()
            typeOfOwnerShip == TypeOfOwnerShip.KT1) //EnumTypeofownership.KT1.GetHashCode()
        {
            return TypeOfOwnerShip.KT1;//KT1
        } else {
            return TypeOfOwnerShip.KT3; //KT3
        }
    }
    return {
        Init: Init,
        GetProductByBrand: GetProductByBrand,
        GetPriceByProduct: GetPriceByProduct,
        GetProduct: GetProduct,
        GetPriceFromAiAndLos: GetPriceFromAiAndLos,
        LoadInforPriceDisCount: LoadInforPriceDisCount,
        LoadMoney: LoadMoney,
        formatMoneyTDX: formatMoneyTDX,
        RoundingMoney: RoundingMoney,
        SetThamDinhXe: SetThamDinhXe,
        GetMotoPrice: GetMotoPrice,
        MotoPriceCheckList: MotoPriceCheckList,
        GeturlEmbedMapTracking: GeturlEmbedMapTracking,
        RoundingTo: RoundingTo
    };
}
