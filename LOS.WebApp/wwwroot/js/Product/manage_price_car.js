﻿var ProductManageModule = new function () {

    var InitDataTable = function (objQuery) {
        var datatable = $('#tbManagePriceCar').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/Products/LoadDataManagePriceCar',
                        params: {
                            query: objQuery
                        },
                    }
                },
                pageSize: 25,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'BrandProduct',
                    title: 'Hãng xe',
                    width: 120,
                    sortable: false,
                    textAlign: 'left',
                    template: function (row, index, datatable) {
                        return row.brandProducts.name;
                    }
                },
                {
                    field: 'name',
                    title: 'Tên xe',
                    width: 150,
                    sortable: false
                },
                {
                    field: 'fullName',
                    title: 'Tên đầy đủ',
                    width: 230,
                    textAlign: 'left',
                    sortable: false
                },
                {
                    field: 'price',
                    title: 'Giá (VNĐ)',
                    textAlign: 'right',
                    width: 150,
                    sortable: false,
                    template: function (row, index, datatable) {
                        if (row.price != null)
                            return '<span style="color: #1c901d;">' + App.FormatCurrency(row.price) + '</span>';
                    }
                },
                {
                    field: 'year',
                    width: 120,
                    title: 'Năm sản xuất',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        return '<span style="color: #0c88c9;">' + row.year + '</span>';
                    }
                },
                {
                    field: 'Action',
                    title: 'Chức năng',
                    sortable: false,
                    width: 100,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return '<a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" title="Cập nhập xe"  onclick="ProductManageModule.loadInforCar(\'' + row.id + '\',\'' + row.fullName + '\')" >\
                                        <i class="la la-edit"></i></a>';
                    }
                }
            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            filterName: $('#filterSearch').val().trim(),
        }
        InitDataTable(objQuery);
    }

    var Init = function () {
        $("#txt_create_brand_product_id").select2({
            placeholder: "Vui lòng chọn hãng sản xuất",
            fontsize: "20px",
            fontcolor: "#9CA0B6"
        });
        $("#txt_create_type_product_id").select2({
            placeholder: "Vui lòng chọn loại xe",
            fontsize: "20px",
            fontcolor: "#9CA0B6"
        });
        $("#txt_create_brake_type").select2({
            placeholder: "Vui lòng chọn loại phanh",
            fontsize: "20px",
            fontcolor: "#9CA0B6"
        });
        $("#txt_create_rim_type").select2({
            placeholder: "Vui lòng chọn loại vành",
            fontsize: "20px",
            fontcolor: "#9CA0B6"
        });
        LoadData();

        $("#filterSearch").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });

    };

    var loadInforCar = function (productId, fullName) {
        var form = $('#form_create_car').closest('form');
        var validator = form.validate();
        validator.destroy();
        $('#hdd_create_productId').val(productId);
        $('#txt_create_brand_product_id').val(0).change();
        $('#txt_create_type_product_id').val(0).change();
        $('#txt_create_name').val('');
        $('#txt_create_fullname').val('');
        $('#txt_create_year').val('');
        $('#txt_create_brake_type').val(0).change();
        $('#txt_create_rim_type').val(0).change();
        $('#txt_create_shortname').val('');
        $('#div-note').show();

        if (productId == 0) {
            $('#header_create_car').html('Thêm mới xe');
            $('#btn_save_car').html('<i class="la la-save"></i>Thêm mới');
        } else {
            $('#header_create_car').html('Cập nhật xe <b>' + fullName + '</b>');
            $('#btn_save_car').html('<i class="la la-save"></i>Cập nhật');
            $.ajax({
                type: "GET",
                url: "/Products/GetProductById",
                data: { Id: productId },
                success: function (data) {
                    if (data.status == 1) {
                        //thành công
                        $('#txt_create_brand_product_id').val(data.data.brandProductId).change();
                        $('#txt_create_type_product_id').val(data.data.productTypeId).change();
                        $('#txt_create_name').val(data.data.name);
                        $('#txt_create_fullname').val(data.data.fullName);
                        $('#txt_create_year').val(data.data.year);
                        $('#txt_create_brake_type').val(data.data.brakeType).change();
                        $('#txt_create_rim_type').val(data.data.rimType).change();
                        $('#txt_create_shortname').val(data.data.shortName);
                        $('#div-note').hide();
                    }
                    else {
                        //lỗi
                        App.ShowError(data.message);
                    }
                },
                traditional: true
            });
        }
        $('#modal_create_car').modal('show');
    };

    var AddOrUpdateProduct = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            rules: {
                Name: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                FullName: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                ShortName: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                ShortName: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },

            },
            messages: {
                Name: {
                    required: "Bạn chưa nhập tên sản phẩm",
                    class: "has-danger"
                },
                FullName: {
                    required: "Bạn chưa nhập tên đầy đủ",
                    class: "has-danger"
                },
                ShortName: {
                    required: "Bạn chưa nhập tên ngắn",
                    class: "has-danger"
                },
                Year: {
                    required: "Bạn chưa nhập năm sản xuất",
                    class: "has-danger"
                },
                ShortName: {
                    required: "Bạn chưa nhập tên ngắn",
                    class: "has-danger"
                },
            }
        });

        if (!form.valid()) {
            return;
        }

        var year = $('#txt_create_year').val();
        var current_year = new Date().getFullYear();
        if (year.length != 4) {
            App.ShowErrorNotLoad("Năm sản xuất không đúng định dạng");
            return;
        }
        else if (year > current_year) {
            App.ShowErrorNotLoad("Năm sản xuất không được để hơn năm hiện tại");
            return;
        }

        form.ajaxSubmit({
            url: '/Products/AddOrUpdateProduct',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccessNotLoad(data.message);
                        $('#modal_create_car').modal('hide');
                        LoadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };


    return {
        Init: Init,
        LoadData: LoadData,
        loadInforCar: loadInforCar,
        AddOrUpdateProduct: AddOrUpdateProduct
    };
}
