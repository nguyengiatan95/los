﻿var PushLoanLenderModule = new function () {
    var PushLoanLender = function (loanBriefId, loanAmountFinal) {
        $('#divResult').html('');
        $.ajax({
            type: "POST",
            url: "/Approve/PushLoanLender",
            data: { LoanBriefId: loanBriefId, LoanAmountFinal: loanAmountFinal },
            success: function (data) {
                $('#divResult').html(data);
                $('#Modal').modal('show');
            },
            traditional: true
        });
    }

    return {
        PushLoanLender: PushLoanLender
    };
};