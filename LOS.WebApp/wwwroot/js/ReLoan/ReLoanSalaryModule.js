﻿var ReLoanSalaryModule = new function () {
    var Init = function () {
        $("#StatusCall").select2({
            placeholder: "Trạng thái"
        });
        $("#LivingTime").select2({
            placeholder: "Vui lòng chọn"
        });

        $('#province_addresshome,#province_addresshousehold,#province_work_company').select2({
            placeholder: "Tỉnh/ Thành phố",
            width: '100%'
        });
        $("#district_addresshome,#districtaddresshousehold,#district_work_company").select2({
            placeholder: "Quận/ Huyện",
            width: '100%'
        });
        $("#ward_addresshome,#ward_addresshousehold,#ward_work_company").select2({
            placeholder: "Phường/ Xã",
            width: '100%'
        });

        $("#TypeOwnerShip").select2({
            placeholder: "Chọn hình thức nhà ở",
            width: '100%'
        });
        $("#jobId").select2({
            placeholder: "Chọn nghề nghiệp",
            width: '100%'
        });

        $('#brand_product').select2({
            placeholder: "Chọn hãng xe",
            width: '100%'
        });
        $('#product_name').select2({
            placeholder: "Chọn tên xe",
            width: '100%'
        });
        $('#ContractType').select2({
            placeholder: "Chọn loại hợp đồng",
            width: '100%'
        });
        $('#LoanPurpose').select2({
            placeholder: "Mục đích vay vốn",
            width: '100%'
        });

        $('#LoanTime').select2({
            placeholder: "Thời gian vay",
            width: '100%'
        });

        $('#RateType').select2({
            placeholder: "Hình thức thanh toán",
            width: '100%'
        });

        $('#ContractPlace').select2({
            placeholder: "Nơi ký hợp đồng",
            width: '100%'
        });
    }

    var GetDistrict = function (provinceId, districtId, element_district, element_ward) {
        if (provinceId > 0) {
            App.Ajax("GET", "/Dictionary/GetDistrict?id=" + provinceId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (districtId > 0 && districtId == data.data[i].districtId) {
                            html += `<option value="${data.data[i].districtId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].districtId}">${data.data[i].name}</option>`;
                        }
                    }
                    $(element_district).html(html);
                    $(element_district).select2({
                        placeholder: "Quận/ Huyện"
                    });
                }
            });
            //load lại phường xã
            $(element_ward).html("<option></option>");
            $(element_ward).select2({
                placeholder: "Phường/ Xã"
            });
        } else {
            $(element_district).select2({
                placeholder: "Quận/ Huyện",
                width: '100%'
            });
        }
    }
    var GetWard = function (districtId, wardId, element) {
        if (districtId > 0) {
            App.Ajax("GET", "/Dictionary/GetWard?districtId=" + districtId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (wardId > 0 && wardId == data.data[i].wardId) {
                            html += `<option value="${data.data[i].wardId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].wardId}">${data.data[i].name}</option>`;
                        }
                    }
                    $(element).html(html);
                    $(element).select2({
                        placeholder: "Phường/ Xã"
                    });
                }
            });
        } else {
            $(element).select2({
                placeholder: "Phường/ Xã",
                width: '100%'
            });
        }
    }
    var GetProduct = function (brandId, productId) {
        if (brandId > 0) {
            App.Ajax("GET", "/Dictionary/GetProduct?brandId=" + brandId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (productId > 0 && productId == data.data[i].id) {
                            html += `<option value="${data.data[i].id}" selected="selected">${data.data[i].fullName}</option>`;
                        } else {
                            html += `<option value="${data.data[i].id}">${data.data[i].fullName}</option>`;
                        }
                    }
                    $("#product_name").html(html);
                    $("#product_name").select2({
                        placeholder: "Chọn tên xe"
                    });
                }
            });
        } else {
            $('#product_name').select2({
                placeholder: "Chọn tên xe",
                width: '100%'
            });
        }
    }
    var GetLoanBrief = function (phone) {
        if (phone != null && phone.trim() != "") {
                        $.ajax({
                url: '/LoanBrief/ListLoanBriefSearch?phone=' + phone,
                type: 'GET',
                dataType: 'json',
                success: function (res) {
                    if (res.isSuccess == 1) {
                        $('#box-loanbrief-search').html(res.html);
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
                }

            });
        } else {
            App.ShowErrorNotLoad('Chưa nhập số điện thoại tra cứu');
        }

    }

    var ChangeOptionPhone = function (option) {
        $('#boxLoanBriefInfo').hide();
        $('#boxPhoneFail').hide();
        $('#boxRefuseAcceptCall').hide();
        $('#btnAppointment').hide();

        $('#btnSave').attr('disabled', true);
        $('#btnSubmit').attr('disabled', true);

        $("#ddl_StatusCall").val(null).trigger("change"); 

        if (option == 1) {
            $('#boxConfirmAcceptTeles').show();
        } else {
            $('#boxConfirmAcceptTeles').hide();
            $('#boxPhoneFail').show();
        }
    }

    var ChangeStautsAccept = function (option) {
        $('#boxRefuseAcceptCall').hide();
        $('#btnAppointment').hide();
        $('#box-group-action').show();

        $('#btnSave').attr('disabled', true);
        $('#btnSubmit').attr('disabled', true);
        //đồng ý
        if (option == 1) {
            $('#boxLoanBriefInfo').show();
            $('#btnSave').attr('disabled', false);
            $('#btnSubmit').attr('disabled', false);
        }
        //từ chối nhận cuộc gọi 
        else if (option == 2) {
            $('#boxLoanBriefInfo').hide();
            $('#boxRefuseAcceptCall').show();
           
        }
    }

    var GetMaxPrice = function () {
        var ProductCredit = 0;
        if ($("input[name='OwnerProduct']:checked").val() == 1) {
            ProductCredit = 2;//xe máy chính chủ
        } else {
            ProductCredit = 21;// xe máy kcc
        }
        var LoanBriefId = $('#hdd_LoanBriefId').val();
        var ProductId = $('#product_name').val();
        var TypeOfOwnerShip = $('#TypeOwnerShip').val();
        if (ProductId > 0 && TypeOfOwnerShip > 0 && ProductCredit > 0) {
            $.ajax({
                url: '/LoanBrief/ProductCurrentPrice',
                type: 'GET',
                data: {
                    'productId': ProductId,
                    'typeOfOwnerShip': TypeOfOwnerShip,
                    'productCredit': ProductCredit,
                    'loanBriefId': LoanBriefId
                },
                dataType: 'json',
                success: function (res) {
                    if (res.isSuccess == 1) {
                        $('#txtMaxPrice').html(App.FormatCurrency(res.data));
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
                }

            });
        }
    }

    var SubmitFormSalary = function (action) {

        var form = $('#formReLoanSalary');
        form.validate({
            ignore: [],
            rules: {
                PlatformType: {
                    required: true
                },
                'Customer.FullName': {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                'Customer.Phone': {
                    required: true,
                    validPhone: true
                },
                'Customer.NationalCard': {
                    validCardnumber: true
                },
                'Customer.NationCardPlace': {
                    validCardnumber: true
                },
                sBirthDay: {
                    validDate: true
                }
            },
            messages: {
                PlatformType: {
                    required: "Bạn vui lòng chọn nguồn đơn",
                    class: "has-danger"
                },
                'Customer.FullName': {
                    required: "Bạn vui lòng nhập họ tên",
                },
                'Customer.Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                'Customer.NationalCard': {
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'Customer.NationCardPlace': {
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                sBirthDay: {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                }
                //, 
                //LoanAmount: {
                //    required: "Vui lòng nhập số tiền khách hàng cần vay"
                //}

            },
            errorPlacement: function (error, element) {
                if (element.hasClass('m-select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                    error.insertAfter(element.parent().parent());
                }
                else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.appendTo(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        
        $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
        $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
        $('#txtActionTelesales').val(action);
        if (action == 1) {
            $('#btnSave').attr('disabled', true);
        } else {
            $('#btnSubmit').attr('disabled', true);
        }
        form.ajaxSubmit({
            url: '/LoanBrief/AddReLoan',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#modelReLoan').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        App.ReloadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
                $('#btnSubmit').attr('disabled', false);
                $('#btnSave').attr('disabled', false);
            }
        });
    }

    return {
        Init: Init,
        GetDistrict: GetDistrict,
        GetWard: GetWard,
        GetProduct: GetProduct,
        GetLoanBrief: GetLoanBrief,
        SubmitFormSalary: SubmitFormSalary,
        ChangeOptionPhone: ChangeOptionPhone,
        ChangeStautsAccept: ChangeStautsAccept,
        GetMaxPrice: GetMaxPrice
    };
}