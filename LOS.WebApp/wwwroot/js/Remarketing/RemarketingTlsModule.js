﻿var RemarketingTlsModule = new function () {

    var Init = function () {

        $("#filterAction").select2({
        });

        $("#filterStatus").select2({
        });

        $("#filterDetailReson, #filterSource").select2({
        });

        $("#filterProvince").select2({
        });


        $('#filterTime').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY',
                firstDay: 1,
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7",
                ],
                "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ],
            },
        });

        $('#filterCancelTime').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY',
                firstDay: 1,
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7",
                ],
                "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ],
            },
        });

        $('#filterCancelTime').val('');

        LoadData();

        $("#btnSearch").click(function () {
            LoadData();
            return;
        });

        $('#filterAction,#filterTime,#filterCancelTime,#filterDetailReson,#filterStatus,#filterSource,#filterProvince').on('change', function () {
            LoadData();
            return;
        });

        InitChangeStatus();
    }

    var LoadData = function () {
        var status = $('#filterStatus').val();
        if (status == LOANBRIEF_CANCEL) {
            var objQuery = {
                reasonCancel: $("#filterAction").val(),
                filterCreateTime: $('#filterTime').val(),
                filterCancelTime: $('#filterCancelTime').val(),
                detailReason: $('#filterDetailReson').val(),
                status: $('#filterStatus').val(),
                filterTextSearch: $('#filterTextSearch').val(),
                filterSource: $('#filterSource').val(),
                filterUtmSource: $('#filterUtmSource').val(),
                filterProvince: $('#filterProvince').val(),
            }
            InitDataTableCancel(objQuery);
        }
        else if (status == LOANBRIEF_FINISH) {
            var objQuery = {
                filterCreateTime: $('#filterTime').val(),
                status: $('#filterStatus').val(),
                filterTextSearch: $('#filterTextSearch').val(),
                filterSource: $('#filterSource').val(),
                filterUtmSource: $('#filterUtmSource').val(),
            }
            InitDataTableFinish(objQuery);
        }
    }

    var InitDataTableFinish = function (objQuery) {
        var datatable = $('#dtListLoan').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Remarketing/GetDataRmktTls',
                        params: {
                            query: objQuery
                        },
                    },
                },
                pageSize: 50,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    title: '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input onclick="RemarketingTlsModule.CheckAll();" id="checkAll" type="checkbox"><span></span></label></span>',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    //visible: true,
                    template: function (row) {
                        return '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" value="' + row.loanBriefId + '" name="checkbox"><span></span></label></span>';
                    }
                },
                {
                    field: 'stt',
                    title: 'STT',
                    width: 40,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }

                        return index + record;
                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += 'HĐ-' + row.loanBriefId;
                        return html;
                    }
                },

                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    textAlign: 'center',
                    //width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = '<a id="linkName" style="color:#6C7293;" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>';
                        return html;
                    }
                },
                {
                    field: 'utmSource',
                    title: 'Nguồn đơn',
                    textAlign: 'center',
                    sortable: false
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        //return App.FormatCurrency(row.loanAmount);
                        var html = `${App.FormatCurrency(row.loanAmount)}`
                        if (row.productCreditName != null)
                            html += `<span class="item-desciption">${row.productCreditName}</span>`;
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Tỉnh/ Thành',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.districtName != null)
                            text += "<p class='district'>" + row.districtName + "</p>";
                        if (row.provinceName != null) {
                            text += "<p class='province'>" + row.provinceName + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'finishAt',
                    title: 'Thời gian tất toán',
                    width: 80,
                    textAlign: 'center',
                    sortable: true,
                    template: function (row) {
                        var date = moment(row.finishAt);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                }]
        });
    }

    var InitDataTableCancel = function (objQuery) {
        var datatable = $('#dtListLoan').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Remarketing/GetDataRmktTls',
                        params: {
                            query: objQuery
                        },
                    },
                },
                pageSize: 50,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    title: '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input onclick="RemarketingTlsModule.CheckAll();" id="checkAll" type="checkbox"><span></span></label></span>',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    //visible: true,
                    template: function (row) {
                        return '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" value="' + row.loanBriefId + '" name="checkbox"><span></span></label></span>';
                    }
                },
                {
                    field: 'stt',
                    title: 'STT',
                    width: 40,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }

                        return index + record;
                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += 'HĐ-' + row.loanBriefId;
                        return html;
                    }
                },

                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    textAlign: 'center',
                    //width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = '<a id="linkName" style="color:#6C7293;" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>';
                        return html;
                    }
                },
                {
                    field: 'utmSource',
                    title: 'Nguồn đơn',
                    textAlign: 'center',
                    sortable: false
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay',
                    width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        //return App.FormatCurrency(row.loanAmount);
                        var html = `${App.FormatCurrency(row.loanAmount)}`
                        if (row.productCreditName != null)
                            html += `<span class="item-desciption">${row.productCreditName}</span>`;
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Tỉnh/ Thành',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.districtName != null)
                            text += "<p class='district'>" + row.districtName + "</p>";
                        if (row.provinceName != null) {
                            text += "<p class='province'>" + row.provinceName + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    width: 80,
                    textAlign: 'center',
                    sortable: true,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanBriefCancelAt',
                    title: 'Thời gian hủy',
                    width: 80,
                    textAlign: 'center',
                    sortable: true,
                    template: function (row) {
                        var date = moment(row.loanBriefCancelAt);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanBriefCancelBy',
                    title: 'Người thao tác',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.telesaleName != null)
                            text += "<p style=\"color:#6C7293;\">" + row.telesaleName + "</p>";
                        //if (row.userCancel != null) {
                        //    text += "<p class='province'>" + row.userCancel.groupId + "</p>";
                        //}
                        return text;
                    }
                },
                {
                    field: 'loanBriefComment',
                    title: 'Nội dung hủy',
                    width: 300,
                    textAlign: 'left',
                    sortable: false
                }
            ]
        });
    }

    var CheckAll = function () {
        if ($("#checkAll").is(':checked')) {
            $(".tableCheckAll input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $(".tableCheckAll input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
    }

    var ReCreateLoanbrief = function () {
        var listLoanbriefId = [];
        var status = $('#filterStatus').val();
        $.each($("input[name='checkbox']:checked"), function () {
            listLoanbriefId.push($(this).val());
        });
        if (listLoanbriefId == null || listLoanbriefId.length == 0) {
            App.ShowErrorNotLoad('Vui lòng chọn danh sách đơn vay cần khởi tạo lại');
            return;
        }
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn khởi tạo lại đơn các HĐ đã chọn",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Remarketing/ReCreateLoanbrief",
                    data: { listLoanbriefId: listLoanbriefId, status: status },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                        });
                    },
                    success: function (data) {
                        $.unblockUI();
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                            App.ReloadData();
                        } else {
                            App.ShowErrorNotLoad(data.message);
                            return;
                        }
                    },
                    traditional: true
                });
            }
        });
    };

    var ChangeDetailReason = function (reasonId) {
        $("#filterDetailReson").html("<option value='-1'>Tất cả</option>");
        if (reasonId == -2) {
            //lấy ra lý do hủy chi tiết của hub
            App.Ajax("GET", "/LoanBrief/GetReasonGroup?groupId=" + TypeReason.HubCancel, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option value='-1'>Tất cả</option>";
                    for (var i = 0; i < data.data.length; i++) {
                        html += `<option value="${data.data[i].id}">${data.data[i].reason}</option>`;
                    }
                    $("#filterDetailReson").html(html);
                    $("#filterDetailReson").select2({
                        placeholder: "Vui lòng chọn lý do"
                    });
                }
            });
        }
        else {
            App.Ajax("GET", "/LoanBrief/GetReasonDetail?groupId=" + reasonId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option value='-1'>Tất cả</option>";
                    for (var i = 0; i < data.data.length; i++) {
                        html += `<option value="${data.data[i].id}">${data.data[i].reason}</option>`;
                    }
                    $("#filterDetailReson").html(html);
                    $("#filterDetailReson").select2({
                        placeholder: "Vui lòng chọn lý do"
                    });
                }
            });
        }
    }

    var ChangeStatus = function (status) {
        if (status == LOANBRIEF_CANCEL) {
            $('.loan_finish').attr('style', 'display: none;');
            $('.loan_cancel').removeAttr('style', 'display: none;');
            $('#lbl_Date').html('Ngày tạo đơn');
        }
        else if (status == LOANBRIEF_FINISH) {
            $('.loan_cancel').attr('style', 'display: none;');
            $('.loan_finish').removeAttr('style', 'display: none;');
            $('#lbl_Date').html('Ngày tất toán');
        }
    }

    var InitChangeStatus = function () {
        var status = $('#filterStatus').val();
        if (status == LOANBRIEF_CANCEL) {
            $('.loan_finish').attr('style', 'display: none;');
            $('.loan_cancel').removeAttr('style', 'display: none;');
            $('#lbl_Date').html('Ngày tạo đơn');
        }
        else if (status == LOANBRIEF_FINISH) {
            $('.loan_cancel').attr('style', 'display: none;');
            $('.loan_finish').removeAttr('style', 'display: none;');
            $('#lbl_Date').html('Ngày tất toán');
        }
    }

    var ReCreateLoanbriefAll = function () {
        var status = $('#filterStatus').val();
        var reasonCancel = $("#filterAction").val();
        var detailReason = $('#filterDetailReson').val();
        var filterCreateTime = $('#filterTime').val();
        var filterCancelTime = $('#filterCancelTime').val();
        var filterTextSearch = $('#filterTextSearch').val();
        var filterSource = $('#filterSource').val();
        var filterUtmSource = $('#filterUtmSource').val();
        $.ajax({
            type: "Get",
            url: "/Remarketing/GetDataRmktTlsAll",
            data: {
                Status: status, ReasonCancel: reasonCancel, DetailReason: detailReason, strCreateDateRanger: filterCreateTime,
                strCancelTimeRanger: filterCancelTime, TextSearch: filterTextSearch, Source: filterSource, UtmSource: filterUtmSource
            },
            success: function (res) {
                if (res.status == 1) {
                    var html = "";
                    var listLoanbriefId = [];
                    if (res.data.length <= 1000) {
                        html = "Bạn có chắc chắn muốn khởi tạo lại " + res.data.length + " HĐ.";
                        listLoanbriefId = res.data;
                    }
                    else {
                        html = "Bạn có chắc chắn muốn khởi tạo lại 1000 / " + res.data.length + " HĐ.";
                        listLoanbriefId = res.data.slice(0, 1000);
                    }

                    swal({
                        title: '<b>Tổng đơn: ' + res.data.length + '</b>',
                        html: html,
                        showCancelButton: true,
                        confirmButtonText: 'Xác nhận',
                        cancelButtonText: 'Hủy',
                        onOpen: function () {
                            $('.swal2-cancel').width($('.swal2-confirm').width());
                        }
                    }).then(function (result) {
                        if (result.value) {
                            $.ajax({
                                type: "POST",
                                url: "/Remarketing/ReCreateLoanbrief",
                                data: { listLoanbriefId: listLoanbriefId, status: status },
                                beforeSend: function () {
                                    $.blockUI({
                                        message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                                    });
                                },
                                success: function (data) {
                                    $.unblockUI();
                                    if (data.status == 1) {
                                        App.ShowSuccessNotLoad(data.message);
                                        App.ReloadData();
                                    } else {
                                        App.ShowErrorNotLoad(data.message);
                                        return;
                                    }
                                },
                                traditional: true
                            });
                        }
                    });
                }
                else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            traditional: true
        });
    }

    return {
        Init: Init,
        LoadData: LoadData,
        CheckAll: CheckAll,
        ReCreateLoanbrief: ReCreateLoanbrief,
        ChangeDetailReason: ChangeDetailReason,
        ChangeStatus: ChangeStatus,
        InitChangeStatus: InitChangeStatus,
        ReCreateLoanbriefAll: ReCreateLoanbriefAll,
    }
}