﻿var RemarketingCarModule = new function () {
    var Init = function () {
        var datatable = $('#dtRemarketingCar').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Remarketing/SearchRemarketingCar'
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    //title: '<span style="width: 50px;"><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" name="checkbox"><span></span></label></span>',
                    title: '',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" value="' + row.loanBriefId + '" name="checkbox"><span></span></label></span>';
                        return html;
                    }
                },
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }

                        return index + record;
                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    width: 100,
                    textAlign: 'center',
                    //sortable: true,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += '<span class="loanBriefId">HĐ-' + row.loanBriefId + '</span>';                       
                        return html;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = '<a class="fullname" id="linkName" href="javascript:;" title="Thông tin chi tiết đơn vay" onclick="CommonModalJS.DetailModal(' + row.loanBriefId + ')">' + row.fullName + '</a>';
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Quận/ Huyện',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.district != null)
                            text += "<p class='district'>" + row.district.name + "</p>";
                        if (row.province != null) {
                            text += "<p class='province'>" + row.province.name + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    width: 150,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay <br /> VNĐ',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row) {
                        var html = `<span class="money">${App.FormatCurrency(row.loanAmount)}</span>`
                        if (row.loanProduct != null)
                            html += `<span class="item-desciption">${row.loanProduct.name}</span>`;
                        if (row.isLocate) {
                            html += `<span class="m-badge m-badge--success m-badge--wide font-size-11">Xe lắp định vị</span>`;
                            if (App.ArrStatusOfDevice[row.deviceStatus] != null) {
                                html += '<span class="item-desciption">(' + App.ArrStatusOfDevice[row.deviceStatus].title + ')</span>';
                            }
                        }
                        if (row.isTrackingLocation) {
                            html += `<span class="item-desciption">(KH chia sẻ vị trí)</span>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'loanTime',
                    title: 'Thời gian vay',
                    textAlign: 'center',
                    width: 100,
                    sortable: false,
                    template: function (row) {
                        if (row.loanTime != null && row.loanTime > 0)
                            return `${row.loanTime} tháng`;
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    //width: 200,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        if (App.ArrLoanStatus[row.status] != null) {
                            var html = '<span class="m-badge--info m-badge--wide">' + 'Chờ GĐKD chia đơn cho Hub' + '</span>';
                            
                            return html;
                        } else {
                            return 'Không xác định';
                        }
                    }
                },

            ]
        });

        $("#sl_ListHub").select2({
            placeholder: "Chọn Hub"
        });

    };

    var ChangeSupport = function () {
        var hubId = $("#sl_ListHub").val();
        if (hubId > 0) {
            var favorite = [];
            $.each($("input[name='checkbox']:checked"), function () {
                favorite.push($(this).val());
            });

            if (favorite == '') {
                App.ShowErrorNotLoad('Bạn phải chọn đơn vay muốn chuyển');
                return;
            }
            var hubName = $('#sl_ListHub option:selected').text();
            swal({
                title: '<b>Cảnh báo</b>',
                html: "Bạn có chắc chắn muốn chuyển các đơn đã chọn cho Hub: <b>" + hubName + "</b>",
                showCancelButton: true,
                confirmButtonText: 'Xác nhận',
                cancelButtonText: 'Hủy',
                onOpen: function () {
                    $('.swal2-cancel').width($('.swal2-confirm').width());
                }
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: "/Remarketing/TranferLoanForHub",
                        data: {
                            HubId: hubId,
                            ArrId: favorite,
                            HubName: hubName
                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data.status == 1) {
                                App.ShowSuccessNotLoad(data.message);
                                App.ReloadData();
                            } else {
                                App.ShowErrorNotLoad('Có lỗi xảy ra, vui lòng liên hệ kỹ thuật!');
                            }

                        },
                        traditional: true
                    });

                }
            });
        }
        else {
            App.ShowErrorNotLoad('Vui lòng chọn Hub cần chuyển hỗ trợ.');
            return;
        }

    }

    var Search = function () {

    }

    return {
        Init: Init,
        Search: Search,
        ChangeSupport: ChangeSupport
    };
}

