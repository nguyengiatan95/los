﻿var RemarketingModule = new function () {
    var Init = function () {
        var datatable = $('.m-datatable__table').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Remarketing/Search'
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    //title: '<span style="width: 50px;"><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" name="checkbox"><span></span></label></span>',
                    title: '',
                    width: 45,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" value="' + row.loanBriefId + '" name="checkbox"><span></span></label></span>';
                        return html;
                    }
                },
                {
                    field: 'stt',
                    title: 'STT',
                    width: 40,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }

                        return index + record;
                    }
                },

                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    width: 150,
                    sortable: false
                },
                {
                    field: 'address',
                    title: 'Tỉnh/ Thành',
                    sortable: false,
                    width: 100,
                    template: function (row) {
                        var text = "";
                        if (row.district != null)
                            text += "<p class='district'>" + row.district.name + "</p>";
                        if (row.province != null) {
                            text += "<p class='province'>" + row.province.name + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    width: 150,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanProduct.name',
                    title: 'Gói vay',
                    //width: 200,
                    sortable: false
                },


                {
                    field: 'loanBriefComment',
                    title: 'Nội dung hủy',
                    width: 500,
                    sortable: false
                },

            ]
        });

        $("#filterAction").select2({
            placeholder: "Hành động"
        });

        $("#filterProduct").select2({
            placeholder: "Sản phẩm"
        });        

        $("#filterProvince").select2({
            placeholder: "Tỉnh / Thành"
        });

        GetAction();
        //GetProvince();
        GetProduct();
        

        $("#btnSearch").click(function () {

            var txtSearch = $('#filtercreateTime').val();
            datatable.search(txtSearch.toLowerCase(), 'filtercreateTime');
            
        });
        
        //SP
        $('#filterProduct').on('change', function () {
            datatable.API.params.pagination.page = 1
            datatable.search($(this).val(), 'productId');
        });
        //Action
        $('#filterAction').on('change', function () {
            datatable.API.params.pagination.page = 1
            datatable.search($(this).val(), 'actionId');
        });

        //City
        $('#filterProvince').on('change', function () {
            datatable.API.params.pagination.page = 1
            datatable.search($(this).val(), 'provinceId');
        });

        $('#filtercreateTime').on('change', function () {
            datatable.API.params.pagination.page = 1;
            var txtSearch = $('#filtercreateTime').val();
            datatable.search(txtSearch.toLowerCase(), 'filtercreateTime');
        });
    };

    //Diep NH GetProduct
    var GetProduct = function () {
        App.Ajax("GET", "/Dictionary/GetLoanProduct", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].loanProductId + "'>" + data.data[i].name + "</option>";
                }
                $("#filterProduct").append(html);
                $("#filterProduct").select2({
                    placeholder: "Sản phẩm"
                });
            }
        });
    }
    // Diep NH GetLoanStatus
    var GetAction = function () {
        App.Ajax("GET", "/Dictionary/GetReasonCancel", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].id + "'>" + data.data[i].reason + "</option>";
                }
                $("#filterAction").append(html);
                $("#filterAction").select2({
                    placeholder: "Hành động"
                });
            }
        });
    }

    var GetProvince = function () {
        App.Ajax("GET", "/Dictionary/GetProvince", undefined).done(function (data) {
            if (data.data != undefined && data.data.length > 0) {
                var html = "";
                for (var i = 0; i < data.data.length; i++) {
                    html += "<option value='" + data.data[i].provinceId + "'>" + data.data[i].name + "</option>";
                }
                $("#filterProvince").append(html);
                $("#filterProvince").select2({
                    placeholder: "Tỉnh / Thành"
                });

            }
        });
    }

    var CloneLoan = function () {
        var favorite = [];
        $.each($("input[name='checkbox']:checked"), function () {
            favorite.push($(this).val());
        });
        
        if (favorite == '') {
            App.ShowErrorNotLoad('Bạn phải chọn đơn vay muốn khởi tạo');
            return;
        }
        $.ajax({
            type: "POST",
            url: "/Remarketing/CloneLoanBrief",
            data: { arrId: favorite },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                } else {
                    App.ShowErrorNotLoad(data.message);
                    return;
                }
            }
        });

    }

    var Search = function () {

    }

    return {
        Init: Init,
        Search: Search,
        CloneLoan: CloneLoan
    };
}

