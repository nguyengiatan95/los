﻿var ReportApproveModule = new function () {
    var Init = function () {
        $("#sl_Month").select2({
            placeholder: "Chọn tháng"
        });

        LoadData();
        $('#sl_Month').on('change', function () {
            LoadData();
            return;
        });
    };
    var LoadData = function () {
        var month = $('#sl_Month').val()

        InitDataTable(month);
    }
    var InitDataTable = function (month) {
        var datatable = $('#dtReportApprove').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'GET',
                        url: '/Report/GetReportApproveEmployee?month=' + month,
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.totalPush !== 'undefined') {
                                $('#totalLoanPush').html('');
                                $('#totalLoanPush').html(raw.totalPush);
                            }
                            if (typeof raw.totalDis !== 'undefined') {
                                $('#totalLoanDis').html('');
                                $('#totalLoanDis').html(raw.totalDis);
                            }
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Tên Nhân Viên',
                    sortable: false,
                    template: function (row) {
                        var html = '<p style="color:#6C7293;padding-top: 10px;">' + row.fullName + '</p>';
                        return html;
                    }
                },
                {
                    field: 'totalPush',
                    title: 'Số hồ sơ đẩy',
                    sortable: false,
                    template: function (row) {
                        var html = '<p style="color:#000;">' + row.totalPush + '</p>';
                        return html;
                    }
                },
                {
                    field: 'disbursement',
                    title: 'Số hồ sơ được giải ngân',
                    sortable: false,
                    template: function (row) {
                        var html = '<p style="color:#000;">' + row.disbursement + '</p>';
                        return html;
                    }
                },
                {
                    field: 'percent',
                    title: 'Tỉ lệ giải ngân/đẩy',
                    sortable: false,
                    template: function (row, index, datatable) {

                        return '<p style="color:red;">' + ((row.disbursement / row.totalPush) * 100).toFixed(2) + " %" + '</p>';
                    }
                },

            ]
        });
    }
   
    return {
        Init: Init
    };
}

