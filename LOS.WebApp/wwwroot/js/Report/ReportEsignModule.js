﻿var ReportEsignModule = new function () {
    var Init = function () {

        LoadData();
        $('#filtercreateTime').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY',
                firstDay: 1,
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7",
                ],
                "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ],
            },
        });

        $("#btnSearch").click(function () {
            LoadData();
            return;
        });

        $('#filtercreateTime').on('change', function () {
            LoadData();
            return;
        });
    };
    var LoadData = function () {

        filtercreateTime = $('#filtercreateTime').val();
        console.log(filtercreateTime);
        InitDataTable(filtercreateTime);
    }
    var InitDataTable = function (filtercreateTime) {
        var datatable = $('#dtReportEsign').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'GET',
                        url: '/Report/GetReportEsignCustomer?dateRanger=' + filtercreateTime,
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.total !== 'undefined') {
                                $('#total').html('');
                                $('#total').html(raw.total);
                            }
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,
            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    textAlign: 'center',
                    width: 100,
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += 'HĐ-' + row.loanBriefId;

                        return html;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    textAlign: 'center',
                    width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = '<a id="linkName" style="color:#6C7293;" href="javascript:;">' + row.fullName + '</a>';

                        return html;
                    }
                },
                {
                    field: 'phone',
                    title: 'SĐT',
                    textAlign: 'center',
                    width: 150,
                    sortable: false
                },
                {
                    field: 'agreementId',
                    title: 'AgreementId',
                    textAlign: 'center',
                    width: 300,
                    sortable: false
                },
                {
                    field: 'createdAt',
                    title: 'Ngày ký',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdAt);
                        return date.format("DD/MM/YYYY")
                        return html;
                    }
                }

            ]
        });
    }

    return {
        Init: Init
    };
}

