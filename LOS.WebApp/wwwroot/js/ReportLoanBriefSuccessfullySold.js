﻿var ReportLoanBriefSuccessfullySold = new function () {
    var Init = function () {
        var datatable = $('.m-datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/Report/GetReportLoanBriefSuccessfullySold'
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã đơn',
                    sortable: false
                },
                {
                    field: 'loanBriefDTO.fullName',
                    title: 'Tên người vay',
                    sortable: false
                },
                {
                    field: 'loanBriefDTO.phone',
                    title: 'Số điện thoại',
                    sortable: false,
                    template: function (row, index, datatable) {
                        return ConvertPhone(row.loanBriefDTO.phone)
                    }
                },
                {
                    field: 'money',
                    title: 'Số tiền mua đơn',
                    sortable: false,
                    template: function (row, index, datatable) {
                        if (row.money != null && row.money != "")
                            return App.FormatCurrency(row.money) + "đ";
                        return "";
                    }
                },
                {
                    field: 'money',
                    title: 'Số tiền hoa hồng',
                    sortable: false,
                    template: function (row, index, datatable) {
                        return row.money + "đ";
                    }
                }
                ,
                {
                    field: 'createdDate',
                    title: 'Ngày Bán',
                    sortable: false,
                    template: function (row, index, datatable) {
                        return formattedDateHourMinutes(row.createdDate)
                    }
                }
                ,
                {
                    field: 'loanBriefDTO.affStatus',
                    title: 'Trạng thái',
                    sortable: false,
                    template: function (row) {
                        if (row.loanBriefDTO.affStatus == 1) {
                            return '<span class="m-badge m-badge--success m-badge--wide"> Chưa bán </span>';
                        } else {
                            return '<span class="m-badge m-badge--metal m-badge--wide"> Đã bán </span>';
                        }

                    }
                }
            ]
        });
        $("#filterName").on('keydown', function (e) {
            if (e.which == 13) {
                datatable.API.params.pagination.page = 1;
                datatable.search($(this).val().toLowerCase(), 'filterName');
                return false;
            }
        });

        $('#filtercreateTime').on('change', function () {
            datatable.API.params.pagination.page = 1;
            datatable.search($('#filtercreateTime').val().toLowerCase(), 'filtercreateTime');
        });
    };
   
    return {
        Init: Init
    };
}
$(document).ready(function () {
    ReportLoanBriefSuccessfullySold.Init();
});

