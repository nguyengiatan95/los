﻿var ReturnLoanBriefModule = new function () {
    var ReturnLoanBrief = function (type, loanBriefId, fullName) {
        $('#resultReturnLoanBrief').html('');
        $.ajax({
            type: "POST",
            url: "/Approve/ReturnLoanBrief", 
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { Type: type, LoanBriefId: loanBriefId, FullName: fullName },
            success: function (data) {
                $.unblockUI();
                $('#resultReturnLoanBrief').html(data);
                $('#modalReturnLoanBrief').modal('show');
            },
            traditional: true
        });
    };

    var ReturnLoanPost = function (formData) {
        $("#btnReturn").attr('disabled', true);
        //alert(reasonId); return;
        //console.log(formData); return;
        $.ajax({
            type: "POST",
            url: "/Approve/ReturnLoanBriefPost",
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(formData),
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                    $("#Modal").css("display", "none");
                }
                else if (data.status == 0) {
                    App.ShowError(data.message);
                    return;
                }
                $("#btnReturn").attr('disabled', false);
            },
            traditional: true
        });
    }

    return {
        ReturnLoanBrief: ReturnLoanBrief,
        ReturnLoanPost: ReturnLoanPost
    };
};