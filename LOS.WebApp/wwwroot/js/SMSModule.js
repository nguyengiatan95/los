﻿var SMSModule = new function () {
    var ShowModalSendSMS = function (loanBriefId, phone) {
        //var phone = phone;
        $('#divResult').html('');
        $.ajax({
            type: "POST",
            url: "/Utils/SendSMS",
            data: { loanBriefId: loanBriefId, phone: phone },
            success: function (data) {
                $('#divResult').html(data);
                $('#Modal').modal('show');
            },
            traditional: true
        });
    }

    return {
        ShowModalSendSMS: ShowModalSendSMS
    };
};