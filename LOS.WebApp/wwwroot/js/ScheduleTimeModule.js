﻿var ScheduleTimeModule = new function () {

    var LoadData = function (loanBriefId) {
        var datatable = $('#dtScheduleTime').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'GET',
                        url: '/ScheduleTime/GetScheduleByLoan?loanBriefId=' + loanBriefId
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }

                        return index + record;
                    }
                },

                {
                    field: 'startTime',
                    title: 'Thời gian hẹn',
                    width: 80,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.startTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'endTime',
                    title: 'Thời gian kết thúc',
                    width: 80,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.endTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'userFullName',
                    title: 'Người thao tác',
                    width: 80,
                    sortable: false,
                    //responsive: { visible: 'lg' },
                    template: function (row) {
                        var html = row.userFullName;
                        return html;
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    width: 70,
                    sortable: false,
                    //responsive: { visible: 'lg' },
                    template: function (row) {

                        var html = '';
                        if (row.status == 1)
                            html += '<span class="m-badge m-badge--warning m-badge--wide">' + "Đã hẹn" + '</span>';
                        if (row.status == 2)
                            html += '<span class="m-badge m-badge--info m-badge--wide">' + "Đã gặp" + '</span>';
                        if (row.status == 0)
                            html += '<span class="m-badge m-badge--success m-badge--danger">' + "Hủy" + '</span>';
                        return html;
                    }
                },
                {
                    field: 'note',
                    title: 'Nội dung',
                    width: 300,
                    //responsive: { visible: 'lg' },
                    sortable: false,
                    template: function (row) {
                        if (row.note != null) {
                            return App.DecodeEntities(row.note);
                        }
                    }
                },
            ]
        });
    };
    var ScheduleTime = function (loanbriefId) {
        var scheduleTime = $("#ScheduleTime").val();
        if (scheduleTime == "") {
            scheduleTime = $("#ScheduleTimeDetail").val();
        }

        var comment = $("#Comment").val();
        if (scheduleTime == '') {
            App.ShowErrorNotLoad('Bạn phải chọn thời gian gọi lại!');
            return;
        }
        $.ajax({
            type: "POST",
            url: "/LoanBrief/AddScheduleTime",
            data: { LoanBriefId: loanbriefId, DateTimeCall: scheduleTime, Comment: comment },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    $('#ScheduleTime').val('');
                    $('#Modal').modal('hide');
                    $('#modelScriptTelesales').modal('hide');

                }
                else if (data.status == 0) {
                    App.ShowErrorNotLoad(data.message);
                    return;
                }
            },
            traditional: true
        });
    }

    var AddAndUpdate = function (loanbriefId, type) {
        var scheduleId = $("#scheduleId").val();
        var startTime = $("#StartTime").val();
        var endTime = $("#EndTime").val();
        var comment = CKEDITOR.instances['txtNoteHub'].getData();
        if (startTime == '' || endTime == '') {
            App.ShowErrorNotLoad('Bạn phải chọn thời gian bắt đầu và kết thúc cuộc hẹn');
            return;
        }
        if (comment == '') {
            App.ShowErrorNotLoad('Bạn phải nhập nội dung cuộc hẹn');
            return;
        }
        if (endTime < startTime) {
            App.ShowErrorNotLoad('Thời gian kết thúc không được nhỏ hơn thời gian bắt đầu');
            return;
        }
        $('#btnSubmitHub').attr('disabled', true);
        $.ajax({
            type: "POST",
            url: "/ScheduleTime/AddEndUpdate",
            data: { LoanBriefId: loanbriefId, StartTime: startTime, EndTime: endTime, Comment: comment, scheduleId: scheduleId, typeSubmit: type },

            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);

                    $('#btnSubmitAdd').attr('disabled', true);
                    $('#btnSubmitUpdate').attr('disabled', true);
                    $('#btnSubmitFinish').attr('disabled', true);
                    if (type == 2) {
                        $('#btnSubmitFinish').attr('disabled', false);
                        $('#btnSubmitUpdate').attr('disabled', false);
                    }else {
                        $('#StartTime').val('');
                        $('#EndTime').val('');
                        CKEDITOR.instances['txtNoteHub'].setData('');
                    }


                    ScheduleTimeModule.LoadData(loanbriefId);
                }
                else if (data.status == 0) {
                    App.ShowErrorNotLoad(data.message);
                    return;
                    $('#btnSubmitHub').attr('disabled', false);
                }
            },
            traditional: true
        });
    }
    var GetListScheduleHub = function () {
        //debugger;
        var hubEmp = $("#filterHubEmployee").val();
        var filterGroupId = $("#filterGroupId").val();
        var startTime = $("#StartTime").val();
        var endTime = $("#EndTime").val();
        if (startTime == '' || endTime == '') {
            App.ShowErrorNotLoad('Bạn phải chọn thời gian bắt đầu và kết thúc cuộc hẹn');
            return;
        }
        //alert(startTime); return;
        $.ajax({
            url: '/ScheduleTime/GetListScheduleHub',
            type: 'POST',
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { HubEmpId: hubEmp, FilterGroupId: filterGroupId, StartTime: startTime, EndTime: endTime },
            success: function (res) {
                $.unblockUI();
                if (res.status == 1) {
                    var calendarEl = document.getElementById('calendar');
                    var calendar = new FullCalendar.Calendar(calendarEl, {
                        initialView: 'resourceTimeGridTwoDay',
                        initialDate: res.initialDate,
                        editable: true,
                        allDaySlot: false,
                        selectable: true,
                        selectHelper: true,
                        eventLimit: true,
                        /*dayMaxEvents: true,*/ // allow "more" link when too many events
                        dayMinWidth: 200,
                        headerToolbar: {
                            left: '',
                            center: '',
                            right: ''
                        },
                        eventClick: function (event, jsEvent, view) {
                            //set the values and open the modal
                            $("#eventInfo").html(event.description);
                            $("#eventLink").attr('href', event.url);
                        },
                        //headerToolbar: {
                        //    left: 'prev,next today',
                        //    center: 'title',
                        //    right: 'resourceTimeGridDay,resourceTimeGridTwoDay,resourceTimeGridWeek,dayGridMonth'
                        //},
                        views: {
                            resourceTimeGridTwoDay: {
                                type: 'resourceTimeGrid',
                                duration: { days: res.day + 1 }
                                //buttonText: '2 days',
                            }
                        },

                        // uncomment this line to hide the all-day slot
                        //allDaySlot: false,

                        resources: [
                            //{ id: 'b', title: 'Room B', eventColor: 'green' },
                            //{ id: 'c', title: 'Room C', eventColor: 'orange' },
                            { id: '1', title: 'Lịch hẹn từ: ' + startTime + ' đến: ' + endTime, color: 'red' }
                        ],
                        events: res.data,
                        eventClick: function (event) {
                            //console.log(event);
                            $('#divResult').html('');
                            var loanBriefId = event.event._def.extendedProps.loanBriefId;
                            if (loanBriefId < 0) {
                                App.ShowErrorNotLoad('Có lỗi không mong muốn xảy ra!');
                                return;
                            }
                            //alert(event.event._def.extendedProps.loanBriefId);
                            $.ajax({
                                type: "GET",
                                url: "/ScheduleTime/DetailSchedule",
                                data: {
                                    'LoanBriefId': loanBriefId
                                },
                                beforeSend: function () {
                                    $.blockUI({
                                        message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                                    });
                                },
                                //dataType: 'json',
                                success: function (res) {
                                    $.unblockUI();
                                    $('#divResult').html(res);
                                    $('#Modal').modal('show');

                                    //console.log(res);
                                },
                                error: function () {
                                    $.unblockUI();
                                    App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
                                }
                            });
                            // Display the modal and set event values.
                            //console.log(calEvent.event._def.title);
                            //$("#myModal").modal("show");

                            //$("#myModal")
                            //    .find("#detailSchedule")
                            //    .text(calEvent.event._def.title);
                        }
                        //    dateClick: function (arg) {
                        //        console.log(
                        //            'dateClick',
                        //            arg.date,
                        //            arg.resource ? arg.resource.id : '(no resource)'
                        //        );
                        //    }
                    });
                    calendar.render();
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });

    };
    return {
        LoadData: LoadData,
        ScheduleTime: ScheduleTime,
        AddAndUpdate: AddAndUpdate,
        GetListScheduleHub: GetListScheduleHub
    };
};