﻿var ScriptCarModuleJS = new function () {

    var Init = function () {
        $('.cus-datepicker').datepicker({
            todayBtn: "linked",
            clearBtn: false,
            clearBtn: false,
            todayHighlight: true,
            orientation: "bottom left",
            format: 'dd/mm/yyyy',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });

        $('#box-script #sl_province_addresshome,#box-script #sl_province_addresshousehold,#box-script #sl_province_work_company').select2({
            placeholder: "Tỉnh/ Thành phố",
            width: '100%'
        });

        $("#box-script #sl_district_addresshome,#box-script #sl_district_addresshousehold,#box-script #sl_district_work_company").select2({
            placeholder: "Quận/ Huyện",
            width: '100%'
        });

        $("#box-script #sl_ward_addresshome,#box-script #sl_ward_addresshousehold,#box-script #sl_ward_work_company").select2({
            placeholder: "Phường/ Xã",
            width: '100%'
        });

        $('#box-script #sl_brand_product').select2({
            placeholder: "Chọn hãng xe",
            width: '100%'
        });

        $('#box-script #sl_product_name').select2({
            placeholder: "Chọn tên xe",
            width: '100%'
        });

        $('#box-script #sl_LoanPurpose').select2({
            placeholder: "Mục đích vay vốn",
            width: '100%'
        });

        $('#box-script #sl_RateType').select2({
            placeholder: "Hình thức thanh toán",
            width: '100%'
        });

        $("#box-script #sl_JobDescriptionId").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_InfomationProductDetail").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_JobId").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_job_document").select2({
            placeholder: "Vui lòng chọn giấy tờ chứng minh công việc"
        });

        $('#box-script #sl_LoanTime').select2({
            placeholder: "Chọn thời gian vay",
            width: '100%'
        });

        InitAutoCheckAppraiserAndJobDocument();
    }

    var GetDistrict = function (provinceId, districtId, element_district, element_ward) {
        if (provinceId > 0) {
            App.Ajax("GET", "/Dictionary/GetDistrict?id=" + provinceId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (districtId > 0 && districtId == data.data[i].districtId) {
                            html += `<option value="${data.data[i].districtId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].districtId}">${data.data[i].name}</option>`;
                        }
                    }
                    $(element_district).html(html);
                    $(element_district).select2({
                        placeholder: "Quận/ Huyện"
                    });
                }
            });
            //load lại phường xã
            $(element_ward).html("<option></option>");
            $(element_ward).select2({
                placeholder: "Phường/ Xã"
            });
        } else {
            $(element_district).select2({
                placeholder: "Quận/ Huyện",
                width: '100%'
            });
        }
    }

    var GetWard = function (districtId, wardId, element) {
        if (districtId > 0) {
            App.Ajax("GET", "/Dictionary/GetWard?districtId=" + districtId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (wardId > 0 && wardId == data.data[i].wardId) {
                            html += `<option value="${data.data[i].wardId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].wardId}">${data.data[i].name}</option>`;
                        }
                    }
                    $(element).html(html);
                    $(element).select2({
                        placeholder: "Phường/ Xã"
                    });
                }
            });
        } else {
            $(element).select2({
                placeholder: "Phường/ Xã",
                width: '100%'
            });
        }
    }

    var GetProduct = function (brandId, productId) {
        if (brandId > 0) {
            App.Ajax("GET", "/Dictionary/GetProduct?brandId=" + brandId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (productId > 0 && productId == data.data[i].id) {
                            html += `<option value="${data.data[i].id}" selected="selected">${data.data[i].fullName}</option>`;
                        } else {
                            html += `<option value="${data.data[i].id}">${data.data[i].fullName}</option>`;
                        }
                    }
                    $("#box-script #sl_product_name").html(html);
                    $("#box-script #sl_product_name").select2({
                        placeholder: "Chọn tên xe"
                    });
                }
                else {
                    var html = "";
                    html += "<option></option>";
                    $("#box-script #sl_product_name").html(html);
                    $("#box-script #sl_product_name").select2({
                        placeholder: "Chọn tên xe"
                    });
                }
            });
        } else {
            $('#box-script #sl_product_name').select2({
                placeholder: "Chọn tên xe",
                width: '100%'
            });
        }
    }

    var GetLoanBrief = function (phone) {
        if (phone != null && phone.trim() != "") {
            $('#div-loanbrief-check').removeAttr('style', 'display: none;');
            $.ajax({
                url: '/LoanBrief/ListLoanBriefSearch?phone=' + phone,
                type: 'GET',
                dataType: 'json',
                success: function (res) {
                    if (res.isSuccess == 1) {
                        $('#box-loanbrief-search').html(res.html);
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
                }

            });
        } else {
            App.ShowErrorNotLoad('Chưa nhập số điện thoại tra cứu');
        }

    }

    var InitDisplayCancel = function (checkValue) {
        if (checkValue == 'false') {
            var checkQuestionSupportArea = $("input[name='LoanBriefQuestionScriptModel.QuestionSupportArea']:checked").val();
            var checkQuestionMotobike = $("input[name='LoanBriefQuestionScriptModel.QuestionUseMotobikeGo']:checked").val();
            var checkQuestionBorrow = $("input[name='LoanBriefQuestionScriptModel.QuestionBorrow']:checked").val();
            if (checkQuestionSupportArea == 'true' && checkQuestionMotobike == 'true' && checkQuestionBorrow == 'true') {
                return false;
            }
            else {
                if (checkQuestionMotobike == undefined) {
                    App.ShowErrorNotLoad("Bạn phải tích chọn có ô tô không");
                    return false;
                }
                else if (checkQuestionSupportArea == undefined) {
                    App.ShowErrorNotLoad("Bạn phải tích chọn có thuộc khu vực không");
                    return false;
                }
                else if (checkQuestionBorrow == undefined) {
                    App.ShowErrorNotLoad("Bạn phải tích chọn có nhu cầu vay không không");
                    return false;
                }
                else {
                    var form = $('#formScriptCar');
                    var loanBriefId = $('#hdd_LoanBriefId').val();
                    var questionQlf = 0;
                    CommonModalJS.InitCancelModal(loanBriefId, questionQlf, '#modelScriptTelesales');
                    form.ajaxSubmit({
                        url: '/LoanBriefV3/ScriptSaveInformationLoanBriefV3',
                        method: 'POST',
                        success: function (data, status, xhr, $form) {
                        }
                    });
                }
            }
        }
    }

    var CheckQLFTelesales = function () {
        //kiểm tra xem telesales tích đủ qualify chưa
        var questionQlf = 0;
        //có nhu cầu vay
        var questionBorrow = $("input[name='LoanBriefQuestionScriptModel.QuestionBorrow']:checked").val();
        //có xe máy
        var questionUseMotobikeGo = $("input[name='LoanBriefQuestionScriptModel.QuestionUseMotobikeGo']:checked").val();
        //có đăng ký bản gốc
        var questionMotobikeCertificate = $("input[name='LoanBriefQuestionScriptModel.QuestionMotobikeCertificate']:checked").val();

        if (questionBorrow == undefined)
            return questionQlf = -10;
        else if (questionBorrow == 'true')
            questionQlf += 1;
        //trong độ tuổi hỗ trợ
        var birtday = $('#txtBirthDay').val();
        var yearNow = new Date().getFullYear();
        if (birtday != '') {
            var yearBirday = parseInt(birtday.substr(6, 4));
            age = yearNow - yearBirday;
            if (age >= 18 && age <= 60)
                questionQlf += 1;
        }
        else {
            return questionQlf = -11;
        }
        //có xe máy
        if (questionUseMotobikeGo == undefined)
            return questionQlf = -12;
        else if (questionUseMotobikeGo == 'true')
            questionQlf += 1;
        //có đăng ký xe bản gốc
        if (questionMotobikeCertificate == undefined)
            return questionQlf = -13;
        if (questionMotobikeCertificate == 'true')
            questionQlf += 1;
        return questionQlf;
    }

    var CancelLoanBrief = function (loanBriefId) {
        $('#btnRefuse').attr('disabled', false);
        $('#btnRefuse').attr('style', 'cursor: no-drop; width: 100%;');
        $('#btnRefuse').addClass('m-loader m-loader--success m-loader--right');
        var form = $('#formScriptCar');
        var questionNotCall = $("input[name='QuestionNotCall']:checked").val();
        if (questionNotCall != 'false') {
            //kiểm tra xem telesales tích đủ lý do qualify chưa
            var checkQLFTelesales = CheckQLFTelesales();
            if (checkQLFTelesales == -10) {
                App.ShowErrorNotLoad('Bạn chưa chọn tích qlf (Có nhu cầu vay) !!!');
                $('#btnRefuse').removeAttr('disabled', '');
                $('#btnRefuse').attr('style', 'width: 100%;');
                $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
                return false;
            }
            else if (checkQLFTelesales == -11) {
                App.ShowErrorNotLoad('Bạn chưa chọn tích qlf (Chọn ngày sinh) !!!');
                $('#btnRefuse').removeAttr('disabled', '');
                $('#btnRefuse').attr('style', 'width: 100%;');
                $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
                return false;
            }
            else if (checkQLFTelesales == -12) {
                App.ShowErrorNotLoad('Bạn chưa chọn tích qlf (Có ô tô không) !!!');
                $('#btnRefuse').removeAttr('disabled', '');
                $('#btnRefuse').attr('style', 'width: 100%;');
                $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
                return false;
            }
            else if (checkQLFTelesales == -13) {
                App.ShowErrorNotLoad('Bạn chưa chọn tích qlf (Có đăng ký xe bản gốc không) !!!');
                $('#btnRefuse').removeAttr('disabled', '');
                $('#btnRefuse').attr('style', 'width: 100%;');
                $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
                return false;
            }
        }
        form.ajaxSubmit({
            url: '/LoanBriefV3/ScriptSaveInformationLoanBriefV3',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $('#btnRefuse').removeAttr('disabled', '');
                    $('#btnRefuse').attr('style', 'width: 100%;');
                    $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
                    if (data.status == 1) {
                        CommonModalJS.InitCancelModal(loanBriefId, checkQLFTelesales, '#modelScriptTelesales');
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    var SubmitForm = function () {
        $('#btnScriptSave').attr('disabled', '');
        $('#btnScriptSave').attr('style', 'cursor: no-drop; width: 100%;');
        $('#btnScriptSave').addClass('m-loader m-loader--success m-loader--right');
        //Check QLF telesales tích
        var questionQlf = CheckQLFTelesales();
        var form = $('#formScriptCar');
        form.validate({
            ignore: [],
            rules: {
                'FullName': {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                'Phone': {
                    required: true,
                    validPhone: true
                },
                'NationalCard': {
                    required: true,
                    validCardnumber: true
                },
                'NationCardPlace': {
                    validCardnumber: true
                },
                sBirthDay: {
                    validDate: true
                },
                'ProvinceId': {
                    required: true
                },
                'DistrictId': {
                    required: true
                },
                LoanAmount: {
                    validLoanAmount: true
                },
            },
            messages: {
                'FullName': {
                    required: "Bạn vui lòng nhập họ tên",
                },
                'Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                'NationalCard': {
                    required: "Vui lòng nhập CMND/CCCD",
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'NationCardPlace': {
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                sBirthDay: {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                },
                'ProvinceId': {
                    required: "Vui lòng chọn thành phố đang sống"
                },
                'DistrictId': {
                    required: "Vui lòng chọn quận/huyện đang sống"
                },
                LoanAmount: {
                    validLoanAmount: "Vui lòng nhập số tiền khách hàng cần vay"
                },
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            $('#btnScriptSave').removeAttr('disabled', '');
            $('#btnScriptSave').attr('style', 'width: 100%;');
            $('#btnScriptSave').removeClass('m-loader m-loader--success m-loader--right');
            return;
        }

        $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
        $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
        form.ajaxSubmit({
            url: '/LoanBriefV3/ScriptUpdateLoanBriefCar',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#modelscripttelesales').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        var loanBriefId = $('#hdd_LoanBriefId').val();
                        CommonModalJS.InitStatusTelesalesModal('#modalStatusTelesales', loanBriefId, questionQlf);
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
                $('#btnScriptSave').removeAttr('disabled', '');
                $('#btnScriptSave').attr('style', 'width: 100%;');
                $('#btnScriptSave').removeClass('m-loader m-loader--success m-loader--right');
            }
        });
    }

    var InitChangeBirday = function (birtday) {
        var form = $('#formScriptCar');
        var loanBriefId = $('#hdd_LoanBriefId').val();
        var questionQlf = 0;
        var yearNow = new Date().getFullYear();
        if (birtday != '') {
            var yearBirday = parseInt(birtday.substr(6, 4));
            age = yearNow - yearBirday;
            if (age < 18 || age > 60) {
                var checkQuestionAnalysis = CheckQuestionAnalysis();
                var checkQuestionMotobike = $("input[name='LoanBriefQuestionScriptModel.QuestionUseMotobikeGo']:checked").val();
                if (checkQuestionAnalysis == false) {
                    App.ShowErrorNotLoad("Bạn phải tích đủ những câu hỏi phân tích dữ liệu");
                    return false;
                }
                if (checkQuestionMotobike == undefined) {
                    App.ShowErrorNotLoad("Bạn phải tích chọn có ô tô không");
                    return false;
                }
                else {
                    CommonModalJS.InitCancelModal(loanBriefId, questionQlf, '#modelScriptTelesales');
                    form.ajaxSubmit({
                        url: '/LoanBriefV3/ScriptSaveInformationLoanBriefV3',
                        method: 'POST',
                        success: function (data, status, xhr, $form) {
                        }
                    });
                }
            }
        }
    }

    var ChangeProductDetail = function (productdetailId) {
        var productId = $('#hdd_ProductId').val();
        $.ajax({
            type: "GET",
            url: "/Dictionary/GetInfomationProductDetailByProductDetailIdAndProduct",
            data: { ProductdetailId: productdetailId, ProductId: productId },
            success: function (res) {
                if (res.data != null) {
                    $('#div-is-appraiser').html('');
                    $('#sl_job_document').html('');
                    $('#div-description').html('');
                    if (res.data.appraiser != null) {
                        $('#div-is-appraiser').html(res.data.appraiser);
                    }
                    if (res.data.jobDocument != null) {
                        $('#div-is-job-document').removeAttr('style', 'display: none;');
                        $('#sl_job_document').html(res.data.jobDocument);
                    }
                    else {
                        $('#div-is-job-document').attr('style', 'display: none;');
                    }
                    if (res.data.description != null) {
                        $('#div-description').html(res.data.description);
                    }

                    var htmlRateType = '<option></option>';
                    var htmlLoanTime = '<option></option>';
                    //thuộc gói vay nhanh
                    if (LoanFastProductDetailId.includes(productdetailId) == true) {
                        htmlLoanTime += '<option value="6">6 tháng</option>\
                             <option value="12" selected="selected">12 tháng</option>';
                        htmlRateType += '<option value="13" selected="selected">Dư nợ giảm dần</option>\
                             <option value="14" >Tất toán cuối kỳ</option>';
                    }
                    else {
                        htmlLoanTime += '<option value = "12" selected="selected">12 tháng</option>';
                        htmlRateType += '<option value="13" selected="selected">Dư nợ giảm dần</option>';
                    }
                    $('#sl_RateType').html(htmlRateType);
                    $('#sl_RateType').select2({
                        placeholder: "Chọn hình thức tính lãi",
                        width: '100%'
                    });
                    $('#sl_LoanTime').html(htmlLoanTime);
                    $('#sl_LoanTime').select2({
                        placeholder: "Chọn thời gian vay",
                        width: '100%'
                    });
                }
            },
            traditional: true
        });


        setTimeout(function () {
            GetMaxPrice();
        }, 500);

    }

    var BindToTextbox = function (tthis, elementBind) {
        if (elementBind == '#AddressGoogleMap' && $('#boxCoordinateAddress').val().trim() == '')
            $(elementBind).val($(tthis).val());
    }

    var ChangeAddressCoincideAreaSupport = function (tthis) {
        $('#lbl_type_ownership').html('');
        if (tthis == 'true') {
            $('#lbl_type_ownership').html('KT1');
            $('#hdd_TypeOwnerShip').val('14');
        }
        else if (tthis == 'false') {
            $('#lbl_type_ownership').html('KT3');
            $('#hdd_TypeOwnerShip').val('15');
        }
        else
            $('#hdd_TypeOwnerShip').val('');

        ShowProductDetail();
    }

    var InitAutoCheckAppraiserAndJobDocument = function () {
        var appraiser = $('#hdd_Appraiser').val();
        var jobDocument = $('#hdd_DocumentBusiness').val();
        setTimeout(function () {
            if (appraiser > 0)
                $('input:radio[name="LoanBriefQuestionScriptModel.Appraiser"]').filter('[value=' + appraiser + ']').attr('checked', true);
            if (jobDocument > 0)
                $('#sl_job_document').val(jobDocument).change();
        }, 1000);
    }

    var InitOwnerShip = function (ownerShip) {
        if (ownerShip == 14) {
            $('#lbl_type_ownership').html('KT1');
        }
        else if (ownerShip == 15) {
            $('#lbl_type_ownership').html('KT3');
        }
        else
            $('#hdd_TypeOwnerShip').val('');
    }

    var ShowProductDetail = function () {
        $('#div-is-appraiser').html('');
        $('#sl_job_document').html('');
        $('#div-description').html('');
        $('#div-is-job-document').attr('style', 'display: none;');
        var productId = $('#hdd_ProductId').val();
        var typeOwnerShip = $('#hdd_TypeOwnerShip').val();
        if (productId == null || productId == '' || productId == undefined) {
            return false;
        }
        if (typeOwnerShip == null || typeOwnerShip == '0' || typeOwnerShip == undefined) {
            return false;
        }
        $.ajax({
            type: "GET",
            url: "/Dictionary/GetInfomationProductDetailByTypeOwnershipAndProduct",
            data: { TypeOwnership: typeOwnerShip, ProductId: productId },
            success: function (data) {
                $('#sl_InfomationProductDetail').html('<option></option>');
                if (data.data != null && data.data.length > 0) {
                    for (var i = 0; i < data.data.length; i++) {
                        $('#sl_InfomationProductDetail').append('<option value="' + data.data[i].id + '" selected="selected">' + data.data[i].name + '</option>');
                    }
                }
            },
            traditional: true
        });
    }

    var ChangeOwnerCar = function (ownerCar) {
        //xe chính chủ
        if (ownerCar == "1") {
            $('.div-hdmb').attr('style', 'display: none;');
        }
        //xe không chính chủ
        else if (ownerCar == "2") {
            $('.div-hdmb').removeAttr('style', 'display: none;');
        }
    }

    return {
        Init: Init,
        GetDistrict: GetDistrict,
        GetWard: GetWard,
        GetProduct: GetProduct,
        GetLoanBrief: GetLoanBrief,
        InitDisplayCancel: InitDisplayCancel,
        CheckQLFTelesales: CheckQLFTelesales,
        CancelLoanBrief: CancelLoanBrief,
        SubmitForm: SubmitForm,
        InitChangeBirday: InitChangeBirday,
        ChangeProductDetail: ChangeProductDetail,
        BindToTextbox: BindToTextbox,
        ChangeAddressCoincideAreaSupport: ChangeAddressCoincideAreaSupport,
        InitAutoCheckAppraiserAndJobDocument: InitAutoCheckAppraiserAndJobDocument,
        InitOwnerShip: InitOwnerShip,
        ChangeOwnerCar: ChangeOwnerCar,
    }
}