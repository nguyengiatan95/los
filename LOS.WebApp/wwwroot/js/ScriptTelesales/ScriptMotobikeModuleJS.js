﻿var ScriptMotobikeModuleJS = new function () {
    var Init = function () {
        $("#box-script #ddl_StatusCall").select2({
            placeholder: "Trạng thái"
        });
        $("#box-script #sl_LivingTime").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_relative_family").select2({
            placeholder: "Vui lòng chọn"
        });
        $("#box-script #sl_relative_family2").select2({
            placeholder: "Vui lòng chọn"
        });

        $('#box-script #sl_province_addresshome,#box-script #sl_province_addresshousehold,#box-script #sl_province_work_company').select2({
            placeholder: "Tỉnh/ Thành phố",
            width: '100%'
        });
        $("#box-script #sl_district_addresshome,#box-script #sl_district_addresshousehold,#box-script #sl_district_work_company").select2({
            placeholder: "Quận/ Huyện",
            width: '100%'
        });
        $("#box-script #sl_ward_addresshome,#box-script #sl_ward_addresshousehold,#box-script #sl_ward_work_company").select2({
            placeholder: "Phường/ Xã",
            width: '100%'
        });

        $("#box-script #sl_TypeOwnerShip").select2({
            placeholder: "Chọn hình thức nhà ở",
            width: '100%'
        });
        $("#box-script #sl_jobId").select2({
            placeholder: "Chọn nghề nghiệp",
            width: '100%'
        });

        $('#box-script #sl_brand_product').select2({
            placeholder: "Chọn hãng xe",
            width: '100%'
        });
        $('#box-script #sl_product_name').select2({
            placeholder: "Chọn tên xe",
            width: '100%'
        });
        $('#box-script #sl_ContractType').select2({
            placeholder: "Chọn loại hợp đồng",
            width: '100%'
        });
        $('#box-script #sl_LoanPurpose').select2({
            placeholder: "Mục đích vay vốn",
            width: '100%'
        });

        $('#box-script #sl_LoanTime').select2({
            placeholder: "Thời gian vay",
            width: '100%'
        });

        $('#box-script #sl_RateType').select2({
            placeholder: "Hình thức thanh toán",
            width: '100%'
        });

        $('#box-script #sl_formrepurchase_xmkcc').select2({
            placeholder: "Chọn hình thức mua lại",
            width: '100%'
        });

        if ($("input[name='OwnerProduct']:checked").val() == 2) {
            $('#div-FormRepurchase').show();
        }
        else {
            $('#div-FormRepurchase').hide();
        }
    }
    var GetDistrict = function (provinceId, districtId, element_district, element_ward) {
        if (provinceId > 0) {
            App.Ajax("GET", "/Dictionary/GetDistrict?id=" + provinceId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (districtId > 0 && districtId == data.data[i].districtId) {
                            html += `<option value="${data.data[i].districtId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].districtId}">${data.data[i].name}</option>`;
                        }
                    }
                    $(element_district).html(html);
                    $(element_district).select2({
                        placeholder: "Quận/ Huyện"
                    });
                }
            });
            //load lại phường xã
            $(element_ward).html("<option></option>");
            $(element_ward).select2({
                placeholder: "Phường/ Xã"
            });
        } else {
            $(element_district).select2({
                placeholder: "Quận/ Huyện",
                width: '100%'
            });
        }
    }
    var GetWard = function (districtId, wardId, element) {
        if (districtId > 0) {
            App.Ajax("GET", "/Dictionary/GetWard?districtId=" + districtId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (wardId > 0 && wardId == data.data[i].wardId) {
                            html += `<option value="${data.data[i].wardId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].wardId}">${data.data[i].name}</option>`;
                        }
                    }
                    $(element).html(html);
                    $(element).select2({
                        placeholder: "Phường/ Xã"
                    });
                }
            });
        } else {
            $(element).select2({
                placeholder: "Phường/ Xã",
                width: '100%'
            });
        }
    }
    var GetProduct = function (brandId, productId) {
        if (brandId > 0) {
            App.Ajax("GET", "/Dictionary/GetProduct?brandId=" + brandId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (productId > 0 && productId == data.data[i].id) {
                            html += `<option value="${data.data[i].id}" selected="selected">${data.data[i].fullName}</option>`;
                        } else {
                            html += `<option value="${data.data[i].id}">${data.data[i].fullName}</option>`;
                        }
                    }
                    $("#box-script #sl_product_name").html(html);
                    $("#box-script #sl_product_name").select2({
                        placeholder: "Chọn tên xe"
                    });
                }
                else {
                    var html = "";
                    html += "<option></option>";
                    $("#box-script #sl_product_name").html(html);
                    $("#box-script #sl_product_name").select2({
                        placeholder: "Chọn tên xe"
                    });
                }
            });
        } else {
            $('#box-script #sl_product_name').select2({
                placeholder: "Chọn tên xe",
                width: '100%'
            });
        }
    }
    var GetLoanBrief = function (phone) {
        if (phone != null && phone.trim() != "") {
            $.ajax({
                url: '/LoanBrief/ListLoanBriefSearch?phone=' + phone,
                type: 'GET',
                dataType: 'json',
                success: function (res) {
                    if (res.isSuccess == 1) {
                        $('#box-loanbrief-search').html(res.html);
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
                }

            });
        } else {
            App.ShowErrorNotLoad('Chưa nhập số điện thoại tra cứu');
        }

    }
    var ChangeOptionPhone = function (option) {
        $('#boxLoanBriefInfo').hide();
        $('#boxPhoneFail').hide();
        $('#boxRefuseAcceptCall').hide();
        $('#btnAppointment').hide();

        $('#btnScriptSave').attr('disabled', true);
        $('#btnScriptSubmit').attr('disabled', true);

        $("#box-script #ddl_StatusCall").val(null).trigger("change");

        if (option == 1) {
            $('#boxConfirmAcceptTeles').show();
        } else {
            $('#boxConfirmAcceptTeles').hide();
            $('#boxPhoneFail').show();
        }
    }
    var ChangeStautsAccept = function (option) {
        $('#boxRefuseAcceptCall').hide();
        $('#btnAppointment').hide();
        $('#box-group-action').show();
        $('#boxLoanBriefInfo').hide();
        $('#btnScriptSave').attr('disabled', true);
        $('#btnScriptSubmit').attr('disabled', true);
        //đồng ý
        if (option == 1) {
            $('#boxConfirmLocate').show();
            //$('#boxLoanBriefInfo').show();
            //nếu đac chọn location => hiển thị chi tiết
            if ($('input[name="IsLocate"]').is(":checked")) {
                $('#boxLoanBriefInfo').show();
                $('#btnScriptSave').attr('disabled', false);
                $('#btnScriptSubmit').attr('disabled', false);
            }
        }
        //từ chối nhận cuộc gọi 
        else if (option == 2) {
            $('#boxConfirmLocate').hide();
            $('#boxRefuseAcceptCall').show();
        }
        //hẹn gọi lại
        else if (option == 3) {
            $('#boxConfirmLocate').hide();
            $('#btnAppointment').show();
            $('#box-group-action').hide();

        } else {
            $('#boxConfirmLocate').hide();
        }
    }
    var ChangeOptionLocate = function (tthis) {
        $('#boxLoanBriefInfo').hide();
        $('#boxPhoneFail').hide();
        $('#boxRefuseAcceptCall').hide();
        $('#btnAppointment').hide();

        $('#btnScriptSave').attr('disabled', true);
        $('#btnScriptSubmit').attr('disabled', true);
        if (tthis.checked) {
            $('#boxLoanBriefInfo').show();
            $('#btnScriptSave').attr('disabled', false);
            $('#btnScriptSubmit').attr('disabled', false);
        } else {
            $('#boxLoanBriefInfo').hide();
            //$('#boxPhoneFail').show();
        }
    }
    var GetMaxPrice = function () {
        var ProductCredit = 0;
        if ($("input[name='OwnerProduct']:checked").val() == 1) {
            ProductCredit = 2;//xe máy chính chủ
            $('#div-FormRepurchase').hide();
        } else {
            ProductCredit = 5;// xe máy kcc
            $('#div-FormRepurchase').show();
        }
        var LoanBriefId = $('#box-script #hdd_LoanBriefId').val();
        var ProductId = $('#box-script #sl_product_name').val();
        var TypeOfOwnerShip = $('#box-script #sl_TypeOwnerShip').val();
        if (ProductId > 0 && TypeOfOwnerShip > 0 && ProductCredit > 0) {
            $.ajax({
                url: '/LoanBrief/ProductCurrentPrice',
                type: 'GET',
                data: {
                    'productId': ProductId,
                    'typeOfOwnerShip': TypeOfOwnerShip,
                    'productCredit': ProductCredit,
                    'loanBriefId': LoanBriefId
                },
                dataType: 'json',
                success: function (res) {
                    if (res.isSuccess == 1) {
                        $('#txtMaxPrice').html(App.FormatCurrency(res.data));
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
                }

            });
        }
    }

    var LoadPriceMoto = function (LoanBriefId, ProductId, TypeOfOwnerShip, ProductCredit) {
        if (ProductId > 0 && TypeOfOwnerShip > 0 && ProductCredit > 0) {
            $.ajax({
                url: '/LoanBrief/ProductCurrentPrice',
                type: 'GET',
                data: {
                    'productId': ProductId,
                    'typeOfOwnerShip': TypeOfOwnerShip,
                    'productCredit': ProductCredit,
                    'loanBriefId': LoanBriefId
                },
                dataType: 'json',
                success: function (res) {
                    if (res.isSuccess == 1) {
                        $('#txtMaxPrice').html(App.FormatCurrency(res.data));
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
                }

            });
        }
    }
    var SubmitForm = function (action) {
        if ($('#hdd_ValueCheckQualify').val() == 0) {
            App.ShowErrorNotLoad("Bạn vui lòng xác nhận lead qualify");
            return false;
        }
        var form = $('#formScriptTelesales');
        form.validate({
            ignore: [],
            rules: {
                PlatformType: {
                    required: true
                },
                'Customer.FullName': {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                'Customer.Phone': {
                    required: true,
                    validPhone: true
                },
                'Customer.NationalCard': {
                    required: true,
                    validCardnumber: true
                },
                'Customer.NationCardPlace': {
                    validCardnumber: true
                },
                sBirthDay: {
                    validDate: true
                },
                'LoanBriefResident.ProvinceId': {
                    required: true
                },
                'LoanBriefResident.DistrictId': {
                    required: true
                },
                'LoanBriefResident.WardId': {
                    required: true
                },
                'LoanBriefResident.ResidentType': {
                    required: true
                },
                LoanAmount: {
                    validLoanAmount: true
                },
                'LoanBriefRelationship[0].Phone': {
                    validPhone: true
                },
                'LoanBriefRelationship[1].Phone': {
                    validPhone: true
                },
            },
            messages: {
                PlatformType: {
                    required: "Bạn vui lòng chọn nguồn đơn",
                    class: "has-danger"
                },
                'Customer.FullName': {
                    required: "Bạn vui lòng nhập họ tên",
                },
                'Customer.Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                'Customer.NationalCard': {
                    required: "Vui lòng nhập CMND/CCCD",
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'Customer.NationCardPlace': {
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                sBirthDay: {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                },
               'LoanBriefResident.ProvinceId': {
                    required: "Vui lòng chọn thành phố đang sống"
                },
                'LoanBriefResident.DistrictId': {
                    required: "Vui lòng chọn quận/huyện đang sống"
                },
                'LoanBriefResident.WardId': {
                    required: "Vui lòng chọn phường/xã đang sống"
                },
                'LoanBriefResident.ResidentType': {
                    required: "Vui lòng chọn hình thức cư trú"
                },
                LoanAmount: {
                    validLoanAmount: "Vui lòng nhập số tiền khách hàng cần vay"
                },
                'LoanBriefRelationship[0].Phone': {
                    validPhone: "Số điện thoại người thân 1 không hợp lệ"
                },
                'LoanBriefRelationship[1].Phone': {
                    validPhone: "Số điện thoại người thân 2 không hợp lệ"
                },
            },
            //errorPlacement: function (error, element) {
            //    if (element.hasClass('m-select2') && element.next('.select2-container').length) {
            //        error.insertAfter(element.next('.select2-container'));
            //    } else if (element.parent('.input-group').length) {
            //        error.insertAfter(element.parent());
            //    }
            //    else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
            //        error.insertAfter(element.parent().parent());
            //    }
            //    else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
            //        error.appendTo(element.parent().parent());
            //    }
            //    else {
            //        error.insertAfter(element);
            //    }
            //},
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
        $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
        $('#txtActionTelesales').val(action);
        if (action == 1) {
            $('#btnScriptSave').attr('disabled', true);
        } else {
            $('#btnScriptSubmit').attr('disabled', true);
        }
        form.ajaxSubmit({
            url: '/LoanBrief/ScriptUpdateLoanBrief',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#modelScriptTelesales').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        App.ReloadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
                $('#btnScriptSubmit').attr('disabled', false);
                $('#btnScriptSave').attr('disabled', false);
            }
        });
    }
    var CallAI = function () {
        var form = $('#formScriptTelesales');
        form.validate({
            ignore: [],
            rules: {
                PlatformType: {
                    required: true
                },
                'Customer.FullName': {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                'Customer.Phone': {
                    required: true,
                    validPhone: true
                },
                'Customer.NationalCard': {
                    required: true,
                    validCardnumber: true
                },
                'LoanBriefResident.Address': {
                    required: true
                },
                'LoanBriefJob.CompanyAddress': {
                    required: true
                },
                sBirthDay: {
                    validDate: true
                }
            },
            messages: {
                PlatformType: {
                    required: "Bạn vui lòng chọn nguồn đơn",
                    class: "has-danger"
                },
                'Customer.FullName': {
                    required: "Bạn vui lòng nhập họ tên",
                },
                'Customer.Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                'Customer.NationalCard': {
                    required: "Vui lòng nhập thông tin CMND",
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'LoanBriefResident.Address': {
                    required: "Vui lòng nhập địa chỉ nơi ở"
                },
                'LoanBriefJob.CompanyAddress': {
                    required: "Vui lòng nhập địa chỉ công ty"
                },
                sBirthDay: {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                }
                //, 
                //LoanAmount: {
                //    required: "Vui lòng nhập số tiền khách hàng cần vay"
                //}

            },
            errorPlacement: function (error, element) {
                if (element.hasClass('m-select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                    error.insertAfter(element.parent().parent());
                }
                else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.appendTo(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
        $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
        $('#btn-request-ai').hide();
        form.ajaxSubmit({
            url: '/LoanBrief/RequestAI',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#btn-request-ai').remove();
                        $('#btn-response-ai').show();
                        $('#boxResultAI').show();
                        App.ShowSuccessNotLoad(data.message);
                    }
                    else {
                        $('#btn-request-ai').show();
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    $('#btn-request-ai').show();
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    var BindToTextbox = function (tthis, elementBind) {
        //$(elementBind).val($(tthis).val());
    }

    var ViewResponseAI = function (loanbriefId) {
        $.ajax({
            url: '/LoanBrief/GetResultAI?loanbriefId=' + loanbriefId,
            type: 'GET',
            dataType: 'text',
            success: function (res) {
                $('#boxResultAI').html(res);
                $('#boxResultAI').show();
            },
            error: function (xrx, xhr) {
                App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
            }

        });
    }

    var SendCheckPhoneRelationshipAI = function () {
        $('#btn_CheckPhoneRelationshipAI').attr('disabled', 'disabled');
        var form = $('#formScriptTelesales');
        form.validate({
            ignore: [],
            rules: {
                'LoanBriefRelationship[0].Phone': {
                    required: true,
                    validPhone: true
                },
                'LoanBriefRelationship[1].Phone': {
                    required: true,
                    validPhone: true
                }
            },
            messages: {

                'LoanBriefRelationship[0].Phone': {
                    required: "Vui lòng nhập số điện thoại người thân 1",
                    validPhone: "Số điện thoại người thân 1 không hợp lệ "
                },
                'LoanBriefRelationship[1].Phone': {
                    required: "Vui lòng nhập số điện thoại người thân 2",
                    validPhone: "Số điện thoại người thân 2 không hợp lệ "
                }
            },
            errorPlacement: function (error, element) {
                if (element.hasClass('m-select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                    error.insertAfter(element.parent().parent());
                }
                else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.appendTo(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });
        if (!form.valid()) {
            $('#btn_CheckPhoneRelationshipAI').removeAttr('disabled', 'disabled');
            return;
        }
        form.ajaxSubmit({
            url: '/LoanBrief/RequestRefphone',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $('#btn_CheckPhoneRelationshipAI').removeAttr('disabled', 'disabled');
                    if (data.status == 1) {
                        $('#div-sendResultCheckPhoneRelationshipAI1').show();
                        $('#div-sendResultCheckPhoneRelationshipAI').show();
                        $('#div-AIresult-CheckPhoneRelationship').hide();
                        $('#div-AIresult-CheckPhoneRelationship1').hide();
                        App.ShowSuccessNotLoad(data.message);
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    var ChangeOptionQualify = function (tthis, checkQlf) {
        if (checkQlf == 2) {
            $("#cb_NeedLoan").prop('checked', false);
            $("#cb_InAge").prop('checked', false);
            $("#cb_InAreaSupport").prop('checked', false);
            $("#cb_HaveMotobike").prop('checked', false);
            $("#cb_HaveOriginalVehicleRegistration").prop('checked', false);
            $('#hdd_ValueCheckQualify').val('2');
            return;
        }
        else {
            $("#cb_NoQualify").prop('checked', false); 
        }
        var totalValueQlf = parseInt($('#hdd_ValueCheckQualify').val());
        if (totalValueQlf == 2)
            totalValueQlf = 0;
        if (tthis.checked)
            totalValueQlf += checkQlf;
        else
            totalValueQlf -= checkQlf;

        $('#hdd_ValueCheckQualify').val(totalValueQlf);

    }

    var GetDataLoanOld = function (loanBriefId, func) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc muốn sử dụng thông tin đơn vay <b>HĐ-" + loanBriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            $("#loading").css("display", "");
            
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: '/LoanBrief/GetLoanBriefOld?loanBriefid=' + loanBriefId,
                    dataType: 'json',
                    
                    success: function (data) {
                        $("#loading").css("display", "none");
                        console.log(data);
                        if (data.status == 1) {
                            debugger;
                            document.getElementsByName("Customer.FullName")[0].value = data.data.fullName;
                            document.getElementsByName("sBirthDay")[0].value = moment(data.data.dob).format("DD/MM/YYYY");
                            document.getElementsByName("Customer.Gender")[0].value = data.data.gender;
                            document.getElementsByName("Customer.NationalCard")[0].value = data.data.nationalCard;
                            document.getElementsByName("LoanBriefResident.ProvinceId")[0].value = data.data.loanBriefResident.provinceId;
                            document.getElementsByName("LoanBriefResident.Address")[0].value = data.data.loanBriefResident.address; 
                            document.getElementsByName("LoanBriefResident.AddressGoogleMap")[0].value = data.data.loanBriefResident.addressGoogleMap;
                            
                            if (data.data.loanBriefResident.provinceId > 0) {
                                $("#sl_province_addresshome").val(data.data.loanBriefResident.provinceId).trigger('change');
                                if (data.data.loanBriefResident.districtId > 0) {
                                    ScriptMotobikeModuleJS.GetDistrict(data.data.loanBriefResident.provinceId, data.data.loanBriefResident.districtId, '#sl_district_addresshome', '#sl_ward_addresshome')
                                    if (data.data.loanBriefResident.wardId > 0)
                                        ScriptMotobikeModuleJS.GetWard(data.data.loanBriefResident.districtId, data.data.loanBriefResident.wardId, '#sl_ward_addresshome')
                                    else
                                        ScriptMotobikeModuleJS.GetWard(data.data.loanBriefResident.districtId, 0, '#sl_ward_addresshome')
                                } else {
                                    ScriptMotobikeModuleJS.GetDistrict(data.data.loanBriefResident.provinceId, 0, '#sl_district_addresshome', '#sl_ward_addresshome')
                                }                                   
                                
                            }
                            if (data.data.loanBriefHousehold.provinceId > 0) {
                                $("#sl_province_addresshousehold").val(data.data.loanBriefHousehold.provinceId).trigger('change');
                                if (data.data.loanBriefHousehold.districtId > 0) {
                                    ScriptMotobikeModuleJS.GetDistrict(data.data.loanBriefHousehold.provinceId, data.data.loanBriefHousehold.districtId, '#sl_district_addresshousehold', '#sl_ward_addresshousehold')
                                    if (data.data.loanBriefHousehold.wardId > 0)
                                        ScriptMotobikeModuleJS.GetWard(data.data.loanBriefHousehold.districtId, data.data.loanBriefHousehold.wardId, '#sl_ward_addresshousehold')
                                    else
                                        ScriptMotobikeModuleJS.GetWard(data.data.loanBriefHousehold.districtId, 0, '#sl_ward_addresshousehold')
                                } else {
                                    ScriptMotobikeModuleJS.GetDistrict(data.data.loanBriefHousehold.provinceId, 0, '#sl_district_addresshousehold', '#sl_ward_addresshousehold')
                                }

                            }
                            document.getElementsByName("LoanBriefHousehold.Address")[0].value = data.data.loanBriefHousehold.address;

                            //document.getElementsByName("LoanBriefJob.JobId")[0].value = data.data.loanBriefJob.jobId;
                            $("#sl_jobId").val(data.data.loanBriefJob.jobId).trigger('change');
                            //$('#sl_jobId option').removeAttr('selected').filter('[value=10]').attr('selected', true);
                            document.getElementsByName("LoanBriefJob.Description")[0].value = data.data.loanBriefJob.description;
                            document.getElementsByName("LoanBriefJob.CompanyName")[0].value = data.data.loanBriefJob.companyName;
                            if (data.data.loanBriefJob.companyProvinceId > 0) {     
                                $("#sl_province_work_company").val(data.data.loanBriefJob.companyProvinceId).trigger('change');
                                if (data.data.loanBriefJob.companyDistrictId > 0) {
                                    ScriptMotobikeModuleJS.GetDistrict(data.data.loanBriefJob.companyProvinceId, data.data.loanBriefJob.companyDistrictId, '#sl_district_work_company', '#sl_ward_work_company')
                                    if (data.data.loanBriefJob.companyWardId > 0)
                                        ScriptMotobikeModuleJS.GetWard(data.data.loanBriefJob.companyDistrictId, data.data.loanBriefJob.companyWardId, '#sl_ward_work_company')
                                    else
                                        ScriptMotobikeModuleJS.GetWard(data.data.loanBriefJob.companyDistrictId, 0, '#sl_ward_work_company')
                                } else {
                                    ScriptMotobikeModuleJS.GetDistrict(data.data.loanBriefJob.companyProvinceId, 0, '#sl_district_work_company', '#sl_ward_work_company')
                                }

                            }
                            document.getElementsByName("LoanBriefJob.CompanyAddress")[0].value = data.data.loanBriefJob.companyAddress;
                            document.getElementsByName("LoanBriefJob.TotalIncome")[0].value = App.FormatCurrency(data.data.loanBriefJob.totalIncome);
                            document.getElementsByName("LoanBriefJob.CompanyAddressGoogleMap")[0].value = data.data.loanBriefJob.companyAddressGoogleMap;
                            
                            
                        } else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var CancelLoanBrief = function (loanBriefId) {
        var form = $('#formScriptTelesales');
        $('#btnRefuse').attr('disabled', false);
        form.ajaxSubmit({
            url: '/LoanBrief/ScriptSaveInformationLoanBrief',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        CommonModalJS.InitCancelModal(loanBriefId, '#modelScriptTelesales');
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    return {
        Init: Init,
        GetDistrict: GetDistrict,
        GetWard: GetWard,
        GetProduct: GetProduct,
        GetLoanBrief: GetLoanBrief,
        SubmitForm: SubmitForm,
        ChangeOptionPhone: ChangeOptionPhone,
        ChangeStautsAccept: ChangeStautsAccept,
        GetMaxPrice: GetMaxPrice,
        ChangeOptionLocate: ChangeOptionLocate,
        BindToTextbox: BindToTextbox,
        CallAI: CallAI,
        ViewResponseAI: ViewResponseAI,
        SendCheckPhoneRelationshipAI: SendCheckPhoneRelationshipAI,
        ChangeOptionQualify: ChangeOptionQualify,
        GetDataLoanOld: GetDataLoanOld,
        CancelLoanBrief: CancelLoanBrief,
        LoadPriceMoto: LoadPriceMoto
    };
}