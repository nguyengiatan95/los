﻿var ScriptMotobikeModuleJS_v2 = new function () {
    var Init = function () {
        $("#box-script #ddl_StatusCall").select2({
            placeholder: "Trạng thái"
        });
        $("#box-script #sl_LivingTime").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_relative_family").select2({
            placeholder: "Vui lòng chọn"
        });
        $("#box-script #sl_relative_family2").select2({
            placeholder: "Vui lòng chọn"
        });

        $('#box-script #sl_province_addresshome,#box-script #sl_province_addresshousehold,#box-script #sl_province_work_company').select2({
            placeholder: "Tỉnh/ Thành phố",
            width: '100%'
        });
        $("#box-script #sl_district_addresshome,#box-script #sl_district_addresshousehold,#box-script #sl_district_work_company").select2({
            placeholder: "Quận/ Huyện",
            width: '100%'
        });
        $("#box-script #sl_ward_addresshome,#box-script #sl_ward_addresshousehold,#box-script #sl_ward_work_company").select2({
            placeholder: "Phường/ Xã",
            width: '100%'
        });

        $("#box-script #sl_TypeOwnerShip").select2({
            placeholder: "Chọn hình thức nhà ở",
            width: '100%'
        });
        $("#box-script #sl_jobId").select2({
            placeholder: "Chọn nghề nghiệp",
            width: '100%'
        });

        $('#box-script #sl_brand_product').select2({
            placeholder: "Chọn hãng xe",
            width: '100%'
        });
        $('#box-script #sl_product_name').select2({
            placeholder: "Chọn tên xe",
            width: '100%'
        });
        $('#box-script #sl_ContractType').select2({
            placeholder: "Chọn loại hợp đồng",
            width: '100%'
        });
        $('#box-script #sl_LoanPurpose').select2({
            placeholder: "Mục đích vay vốn",
            width: '100%'
        });

        $('#box-script #sl_LoanTime').select2({
            placeholder: "Thời gian vay",
            width: '100%'
        });

        $('#box-script #sl_RateType').select2({
            placeholder: "Hình thức thanh toán",
            width: '100%'
        });

        $('#box-script #sl_formrepurchase_xmkcc').select2({
            placeholder: "Chọn hình thức mua lại",
            width: '100%'
        });

        $("#box-script #sl_TypeJob").select2({
            placeholder: "Chọn loại hình công việc</option>",
            width: '100%'
        });

        $("#box-script #sl_WorkingTime").select2({
            placeholder: "Chọn thời gian làm việc</option>",
            width: '100%'
        });

        $("#box-script #sl_Career").select2({
            placeholder: "Chọn ngành nghề</option>",
            width: '100%'
        });

        $("#box-script #sl_DriverOfCompany").select2({
            placeholder: "Chọn công ty</option>",
            width: '100%'
        });

        $("#box-script #sl_TimeDriver").select2({
            placeholder: "Chọn thời gian</option>",
            width: '100%'
        });

        if ($("input[name='OwnerProduct']:checked").val() == 2) {
            $('#div-FormRepurchase').show();
        }
        else {
            $('#div-FormRepurchase').hide();
        }

        $('#div-sendResultCheckPhoneRelationshipAI1').hide();
        $('#div-sendResultCheckPhoneRelationshipAI').hide();

        InitChangTypeJob();
        InitShowTypeCareer();
        InitShowQuestionXMKCC();
    }
    var GetDistrict = function (provinceId, districtId, element_district, element_ward) {
        if (provinceId > 0) {
            App.Ajax("GET", "/Dictionary/GetDistrict?id=" + provinceId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (districtId > 0 && districtId == data.data[i].districtId) {
                            html += `<option value="${data.data[i].districtId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].districtId}">${data.data[i].name}</option>`;
                        }
                    }
                    $(element_district).html(html);
                    $(element_district).select2({
                        placeholder: "Quận/ Huyện"
                    });
                }
            });
            //load lại phường xã
            $(element_ward).html("<option></option>");
            $(element_ward).select2({
                placeholder: "Phường/ Xã"
            });
        } else {
            $(element_district).select2({
                placeholder: "Quận/ Huyện",
                width: '100%'
            });
        }
    }
    var GetWard = function (districtId, wardId, element) {
        if (districtId > 0) {
            App.Ajax("GET", "/Dictionary/GetWard?districtId=" + districtId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (wardId > 0 && wardId == data.data[i].wardId) {
                            html += `<option value="${data.data[i].wardId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].wardId}">${data.data[i].name}</option>`;
                        }
                    }
                    $(element).html(html);
                    $(element).select2({
                        placeholder: "Phường/ Xã"
                    });
                }
            });
        } else {
            $(element).select2({
                placeholder: "Phường/ Xã",
                width: '100%'
            });
        }
    }
    var GetProduct = function (brandId, productId) {
        if (brandId > 0) {
            App.Ajax("GET", "/Dictionary/GetProduct?brandId=" + brandId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (productId > 0 && productId == data.data[i].id) {
                            html += `<option value="${data.data[i].id}" selected="selected">${data.data[i].fullName}</option>`;
                        } else {
                            html += `<option value="${data.data[i].id}">${data.data[i].fullName}</option>`;
                        }
                    }
                    $("#box-script #sl_product_name").html(html);
                    $("#box-script #sl_product_name").select2({
                        placeholder: "Chọn tên xe"
                    });
                }
                else {
                    var html = "";
                    html += "<option></option>";
                    $("#box-script #sl_product_name").html(html);
                    $("#box-script #sl_product_name").select2({
                        placeholder: "Chọn tên xe"
                    });
                }
            });
        } else {
            $('#box-script #sl_product_name').select2({
                placeholder: "Chọn tên xe",
                width: '100%'
            });
        }
    }
    var GetLoanBrief = function (phone) {
        if (phone != null && phone.trim() != "") {
            $.ajax({
                url: '/LoanBrief/ListLoanBriefSearch?phone=' + phone,
                type: 'GET',
                dataType: 'json',
                success: function (res) {
                    if (res.isSuccess == 1) {
                        $('#box-loanbrief-search').html(res.html);
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
                }

            });
        } else {
            App.ShowErrorNotLoad('Chưa nhập số điện thoại tra cứu');
        }

    }
    var GetMaxPrice = function () {
        var ProductCredit = 0;
        if ($("input[name='OwnerProduct']:checked").val() == 1) {
            ProductCredit = 2;//xe máy chính chủ
            $('#div-FormRepurchase').hide();
            $('#div-Question').attr('style', 'display: none;');
        } else {
            ProductCredit = 5;// xe máy kcc
            $('#div-FormRepurchase').show();
            $('#div-Question').removeAttr('style', 'display: none;');
        }
        var LoanBriefId = $('#box-script #hdd_LoanBriefId').val();
        var ProductId = $('#box-script #sl_product_name').val();
        var TypeOfOwnerShip = $('#box-script #sl_TypeOwnerShip').val();
        if (ProductId > 0 && TypeOfOwnerShip > 0 && ProductCredit > 0) {
            $.ajax({
                url: '/LoanBrief/ProductCurrentPrice',
                type: 'GET',
                data: {
                    'productId': ProductId,
                    'typeOfOwnerShip': TypeOfOwnerShip,
                    'productCredit': ProductCredit,
                    'loanBriefId': LoanBriefId
                },
                dataType: 'json',
                success: function (res) {
                    if (res.isSuccess == 1) {
                        $('#txtMaxPrice').html(App.FormatCurrency(res.data));
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
                }

            });
        }
    }
    var LoadPriceMoto = function (LoanBriefId, ProductId, TypeOfOwnerShip, ProductCredit) {
        if (ProductId > 0 && TypeOfOwnerShip > 0 && ProductCredit > 0) {
            $.ajax({
                url: '/LoanBrief/ProductCurrentPrice',
                type: 'GET',
                data: {
                    'productId': ProductId,
                    'typeOfOwnerShip': TypeOfOwnerShip,
                    'productCredit': ProductCredit,
                    'loanBriefId': LoanBriefId
                },
                dataType: 'json',
                success: function (res) {
                    if (res.isSuccess == 1) {
                        $('#txtMaxPrice').html(App.FormatCurrency(res.data));
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
                }

            });
        }
    }
    var SubmitForm = function () {
        $('#btnScriptSave').attr('disabled', '');
        $('#btnScriptSave').attr('style', 'cursor: no-drop; width: 100%;');
        $('#btnScriptSave').addClass('m-loader m-loader--success m-loader--right');
        //kiểm tra xem telesales tích đủ 2 lý do qualify chưa
        var questionQlf = 0;
        //có nhu cầu vay
        var questionBorrow = $("input[name='LoanBriefQuestionScriptModel.QuestionBorrow']:checked").val();
        var questionUseMotobikeGo = $("input[name='LoanBriefQuestionScriptModel.QuestionUseMotobikeGo']:checked").val();
        var questionMotobikeCertificate = $("input[name='LoanBriefQuestionScriptModel.QuestionMotobikeCertificate']:checked").val();

        if (questionBorrow == 'true')
            questionQlf += 1;
        else
            questionQlf -= 1;

        //trong độ tuổi hỗ trợ
        var birtday = $('#txtBirthDay').val();
        var yearNow = new Date().getFullYear();
        if (birtday != '') {
            var yearBirday = parseInt(birtday.substr(6, 4));
            age = yearNow - yearBirday;
            if (age >= 18 && age <= 60)
                questionQlf += 1;
            else
                questionQlf -= 1;
        }


        //trong khu vực hỗ trợ
        var cityId = parseInt($('#sl_province_addresshome').val());
        if (cityId == 1 || cityId == 79 || cityId == 27 || cityId == 26 || cityId == 30)
            questionQlf += 1;
        else
            questionQlf -= 1;

        //có xe máy
        if (questionUseMotobikeGo == 'true')
            questionQlf += 1;
        else
            questionQlf -= 1;

        //có đăng ký xe bản gốc
        if (questionMotobikeCertificate == 'true')
            questionQlf += 1;
        else
            questionQlf -= 1;

        var form = $('#formScriptTelesalesv2');
        form.validate({
            ignore: [],
            rules: {
                PlatformType: {
                    required: true
                },
                'FullName': {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                'Phone': {
                    required: true,
                    validPhone: true
                },
                'NationalCard': {
                    required: true,
                    validCardnumber: true
                },
                'NationCardPlace': {
                    validCardnumber: true
                },
                sBirthDay: {
                    validDate: true
                },
                'ProvinceId': {
                    required: true
                },
                'DistrictId': {
                    required: true
                },
                //'LoanBriefResident.WardId': {
                //    required: true
                //},
                //'LoanBriefResident.ResidentType': {
                //    required: true
                //},
                LoanAmount: {
                    validLoanAmount: true
                },
                'LoanBriefRelationshipModels[0].Phone': {
                    validPhone: true
                },
                'LoanBriefRelationshipModels[1].Phone': {
                    validPhone: true
                },
            },
            messages: {
                PlatformType: {
                    required: "Bạn vui lòng chọn nguồn đơn",
                    class: "has-danger"
                },
                'FullName': {
                    required: "Bạn vui lòng nhập họ tên",
                },
                'Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                'NationalCard': {
                    required: "Vui lòng nhập CMND/CCCD",
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'NationCardPlace': {
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                sBirthDay: {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                },
                'ProvinceId': {
                    required: "Vui lòng chọn thành phố đang sống"
                },
                'DistrictId': {
                    required: "Vui lòng chọn quận/huyện đang sống"
                },
                //'LoanBriefResident.WardId': {
                //    required: "Vui lòng chọn phường/xã đang sống"
                //},
                //'LoanBriefResident.ResidentType': {
                //    required: "Vui lòng chọn hình thức cư trú"
                //},
                LoanAmount: {
                    validLoanAmount: "Vui lòng nhập số tiền khách hàng cần vay"
                },
                'LoanBriefRelationshipModels[0].Phone': {
                    validPhone: "Số điện thoại người thân 1 không hợp lệ"
                },
                'LoanBriefRelationshipModels[1].Phone': {
                    validPhone: "Số điện thoại người thân 2 không hợp lệ"
                },
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            $('#btnScriptSave').removeAttr('disabled', '');
            $('#btnScriptSave').attr('style', 'width: 100%;');
            $('#btnScriptSave').removeClass('m-loader m-loader--success m-loader--right');
            return;
        }
        $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
        $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
        $('#hdd_JobId').val($('#sl_TypeJob').val());
        if ($('#sl_TypeJob').val() == 123) {
            $('#txt_CompanyName').val($('#txt_CompanyName').val());
            $('#hdd_WorkingTime').val($('#txt_WorkCompany_WokingTime').val());
        }
        else if ($('#sl_TypeJob').val() == 124) {
            if ($('#sl_Career').val() == 1) {
                $('#txt_CompanyName').val($('#txt_NameCompanyTD').val());
            }
        }
        else if ($('#sl_TypeJob').val() == 125)
            $('#txt_CompanyName').val($('#txt_DriverOfCompany').val());
        else if ($('#sl_TypeJob').val() == 126) {
            $('#txt_CompanyName').val('');
            $('#txt_Description').val($('#txt_DetailJob').val());
            $('#hdd_WorkingTime').val($('#txt_Freelance_WorkingTime').val());
        }
        else {
            $('#txt_CompanyName').val('');
            $('#txt_Description').val('');
        }
        form.ajaxSubmit({
            //url: '/LoanBrief/ScriptUpdateLoanBriefV2',
            url: '/LoanBriefV2/ScriptUpdateLoanBriefV2',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#modelscripttelesales').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        var loanBriefId = $('#hdd_LoanBriefId').val();
                        CommonModalJS.InitStatusTelesalesModal('#modalStatusTelesales', loanBriefId, questionQlf);
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
                $('#btnScriptSave').removeAttr('disabled', '');
                $('#btnScriptSave').attr('style', 'width: 100%;');
                $('#btnScriptSave').removeClass('m-loader m-loader--success m-loader--right');
            }
        });
    }
    var SubmitCreateLoanbrief = function () {
        $('#btnScriptSave').attr('disabled', '');
        $('#btnScriptSave').attr('style', 'cursor: no-drop; width: 100%;');
        $('#btnScriptSave').addClass('m-loader m-loader--success m-loader--right');
        //kiểm tra xem telesales tích đủ 2 lý do qualify chưa
        var questionQlf = 0;
        //có nhu cầu vay
        var questionBorrow = $("input[name='LoanBriefQuestionScriptModel.QuestionBorrow']:checked").val();
        var questionUseMotobikeGo = $("input[name='LoanBriefQuestionScriptModel.QuestionUseMotobikeGo']:checked").val();
        var questionMotobikeCertificate = $("input[name='LoanBriefQuestionScriptModel.QuestionMotobikeCertificate']:checked").val();

        if (questionBorrow == 'true')
            questionQlf += 1;
        else
            questionQlf -= 1;

        //trong độ tuổi hỗ trợ
        var birtday = $('#txtBirthDay').val();
        var yearNow = new Date().getFullYear();
        if (birtday != '') {
            var yearBirday = parseInt(birtday.substr(6, 4));
            age = yearNow - yearBirday;
            if (age >= 18 && age <= 60)
                questionQlf += 1;
            else
                questionQlf -= 1;
        }


        //trong khu vực hỗ trợ
        var cityId = parseInt($('#sl_province_addresshome').val());
        if (cityId == 1 || cityId == 79 || cityId == 27 || cityId == 26 || cityId == 30)
            questionQlf += 1;
        else
            questionQlf -= 1;

        //có xe máy
        if (questionUseMotobikeGo == 'true')
            questionQlf += 1;
        else
            questionQlf -= 1;

        //có đăng ký xe bản gốc
        if (questionMotobikeCertificate == 'true')
            questionQlf += 1;
        else
            questionQlf -= 1;

        var form = $('#formScriptTelesalesv2');
        form.validate({
            ignore: [],
            rules: {
                PlatformType: {
                    required: true
                },
                'FullName': {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                'Phone': {
                    required: true,
                    validPhone: true
                },
                'NationalCard': {
                    required: true,
                    validCardnumber: true
                },
                'NationCardPlace': {
                    validCardnumber: true
                },
                sBirthDay: {
                    validDate: true
                },
                'ProvinceId': {
                    required: true
                },
                'DistrictId': {
                    required: true
                },
                //'LoanBriefResident.WardId': {
                //    required: true
                //},
                //'LoanBriefResident.ResidentType': {
                //    required: true
                //},
                LoanAmount: {
                    validLoanAmount: true
                },
                'LoanBriefRelationshipModels[0].Phone': {
                    validPhone: true
                },
                'LoanBriefRelationshipModels[1].Phone': {
                    validPhone: true
                },
            },
            messages: {
                PlatformType: {
                    required: "Bạn vui lòng chọn nguồn đơn",
                    class: "has-danger"
                },
                'FullName': {
                    required: "Bạn vui lòng nhập họ tên",
                },
                'Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                'NationalCard': {
                    required: "Vui lòng nhập CMND/CCCD",
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'NationCardPlace': {
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                sBirthDay: {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                },
                'ProvinceId': {
                    required: "Vui lòng chọn thành phố đang sống"
                },
                'DistrictId': {
                    required: "Vui lòng chọn quận/huyện đang sống"
                },
                //'LoanBriefResident.WardId': {
                //    required: "Vui lòng chọn phường/xã đang sống"
                //},
                //'LoanBriefResident.ResidentType': {
                //    required: "Vui lòng chọn hình thức cư trú"
                //},
                LoanAmount: {
                    validLoanAmount: "Vui lòng nhập số tiền khách hàng cần vay"
                },
                'LoanBriefRelationshipModels[0].Phone': {
                    validPhone: "Số điện thoại người thân 1 không hợp lệ"
                },
                'LoanBriefRelationshipModels[1].Phone': {
                    validPhone: "Số điện thoại người thân 2 không hợp lệ"
                },
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            $('#btnScriptSave').removeAttr('disabled', '');
            $('#btnScriptSave').attr('style', 'width: 100%;');
            $('#btnScriptSave').removeClass('m-loader m-loader--success m-loader--right');
            return;
        }
        $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
        $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
        $('#hdd_JobId').val($('#sl_TypeJob').val());
        if ($('#sl_TypeJob').val() == 123) {
            $('#txt_CompanyName').val($('#txt_CompanyName').val());
            $('#hdd_WorkingTime').val($('#txt_WorkCompany_WokingTime').val());
        }
        else if ($('#sl_TypeJob').val() == 124) {
            if ($('#sl_Career').val() == 1) {
                $('#txt_CompanyName').val($('#txt_NameCompanyTD').val());
            }
        }
        else if ($('#sl_TypeJob').val() == 125)
            $('#txt_CompanyName').val($('#txt_DriverOfCompany').val());
        else if ($('#sl_TypeJob').val() == 126) {
            $('#txt_CompanyName').val('');
            $('#txt_Description').val($('#txt_DetailJob').val());
            $('#hdd_WorkingTime').val($('#txt_Freelance_WorkingTime').val());
        }
        else {
            $('#txt_CompanyName').val('');
            $('#txt_Description').val('');
        }
        form.ajaxSubmit({
            url: '/LoanBriefV2/CreateLoanBriefScriptV2',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#modelscripttelesales').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        var loanBriefId = data.data;
                        CommonModalJS.InitStatusTelesalesModal('#modalStatusTelesales', loanBriefId, questionQlf);
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
                $('#btnScriptSave').removeAttr('disabled', '');
                $('#btnScriptSave').attr('style', 'width: 100%;');
                $('#btnScriptSave').removeClass('m-loader m-loader--success m-loader--right');
            }
        });
    }
    var CallAI = function () {
        var form = $('#formScriptTelesalesv2');
        form.validate({
            ignore: [],
            rules: {
                PlatformType: {
                    required: true
                },
                FullName: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                Phone: {
                    required: true,
                    validPhone: true
                },
                NationalCard: {
                    required: true,
                    validCardnumber: true
                },
                'LoanBriefResident.Address': {
                    required: true
                }, 
                sBirthDay: {
                    validDate: true
                }
            },
            messages: {
                PlatformType: {
                    required: "Bạn vui lòng chọn nguồn đơn",
                    class: "has-danger"
                },
                FullName: {
                    required: "Bạn vui lòng nhập họ tên",
                },
                Phone: {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                NationalCard: {
                    required: "Vui lòng nhập thông tin CMND",
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'LoanBriefResident.Address': {
                    required: "Vui lòng nhập địa chỉ nơi ở"
                },
               
                sBirthDay: {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                }
               

            },
            errorPlacement: function (error, element) {
                if (element.hasClass('m-select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                    error.insertAfter(element.parent().parent());
                }
                else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.appendTo(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
        $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
        $('#btn-request-ai').hide();
        form.ajaxSubmit({
            //url: '/LoanBrief/RequestAI',
            url: '/LoanBriefV2/RequestAIV2',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#btn-request-ai').remove();
                        $('#btn-response-ai').show();
                        $('#boxResultAI').show();
                        App.ShowSuccessNotLoad(data.message);
                    }
                    else {
                        $('#btn-request-ai').show();
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    $('#btn-request-ai').show();
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }
    var ViewResponseAI = function (loanbriefId) {
        $.ajax({
            url: '/LoanBrief/GetResultAI?loanbriefId=' + loanbriefId,
            type: 'GET',
            dataType: 'text',
            success: function (res) {
                $('#boxResultAI').html(res);
                $('#boxResultAI').show();
            },
            error: function (xrx, xhr) {
                App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
            }

        });
    }
    var SendCheckPhoneRelationshipAI = function () {
        $('#btn_CheckPhoneRelationshipAI').attr('disabled', 'disabled');
        var form = $('#formScriptTelesalesv2');
        form.validate({
            ignore: [],
            rules: {
                'LoanBriefRelationshipModels[0].Phone': {
                    required: true,
                    validPhone: true
                },
                'LoanBriefRelationshipModels[1].Phone': {
                    required: true,
                    validPhone: true
                }
            },
            messages: {

                'LoanBriefRelationshipModels[0].Phone': {
                    required: "Vui lòng nhập số điện thoại người thân 1",
                    validPhone: "Số điện thoại người thân 1 không hợp lệ "
                },
                'LoanBriefRelationshipModels[1].Phone': {
                    required: "Vui lòng nhập số điện thoại người thân 2",
                    validPhone: "Số điện thoại người thân 2 không hợp lệ "
                }
            },
            errorPlacement: function (error, element) {
                if (element.hasClass('m-select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                    error.insertAfter(element.parent().parent());
                }
                else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.appendTo(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });
        if (!form.valid()) {
            $('#btn_CheckPhoneRelationshipAI').removeAttr('disabled', 'disabled');
            return;
        }
        form.ajaxSubmit({
            //url: '/LoanBrief/RequestRefphone',
            url: '/LoanBriefV2/RequestRefphoneV2',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $('#btn_CheckPhoneRelationshipAI').removeAttr('disabled', 'disabled');
                    if (data.status == 1) {
                        $('#div-sendResultCheckPhoneRelationshipAI1').show();
                        $('#div-sendResultCheckPhoneRelationshipAI').show();
                        $('#div-AIresult-CheckPhoneRelationship').hide();
                        $('#div-AIresult-CheckPhoneRelationship1').hide();
                        App.ShowSuccessNotLoad(data.message);
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }
    var CancelLoanBrief = function (loanBriefId) {
        $('#btnRefuse').attr('disabled', '');
        $('#btnRefuse').attr('style', 'cursor: no-drop; width: 100%;');
        $('#btnRefuse').addClass('m-loader m-loader--success m-loader--right');
        var form = $('#formScriptTelesalesv2');
        //kiểm tra xem telesales tích đủ 2 lý do qualify chưa
        var questionQlf = 0;
        //có nhu cầu vay
        var questionBorrow = $("input[name='LoanBriefQuestionScriptModel.QuestionBorrow']:checked").val();
        var questionUseMotobikeGo = $("input[name='LoanBriefQuestionScriptModel.QuestionUseMotobikeGo']:checked").val();
        var questionMotobikeCertificate = $("input[name='LoanBriefQuestionScriptModel.QuestionMotobikeCertificate']:checked").val();

        if (questionBorrow == undefined) {
            App.ShowErrorNotLoad('Bạn chưa chọn tích qlf (Có nhu cầu vay) !!!');
            $('#btnRefuse').removeAttr('disabled', '');
            $('#btnRefuse').attr('style', 'width: 100%;');
            $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
            return false;
        }
        else if (questionBorrow == 'true')
            questionQlf += 1;
        else
            questionQlf -= 1;

        //trong độ tuổi hỗ trợ
        var birtday = $('#txtBirthDay').val();
        var yearNow = new Date().getFullYear();
        if (birtday != '') {
            var yearBirday = parseInt(birtday.substr(6, 4));
            age = yearNow - yearBirday;
            if (age >= 18 && age <= 60)
                questionQlf += 1;
            else
                questionQlf -= 1;
        }
        else {
            App.ShowErrorNotLoad('Bạn chưa chọn tích qlf (Chọn ngày sinh) !!!');
            $('#btnRefuse').removeAttr('disabled', '');
            $('#btnRefuse').attr('style', 'width: 100%;');
            $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
            return false;
        }

        //trong khu vực hỗ trợ
        var cityId = parseInt($('#sl_province_addresshome').val());
        if (cityId == 1 || cityId == 79 || cityId == 27 || cityId == 26 || cityId == 30)
            questionQlf += 1;
        else
            questionQlf -= 1;
        //có xe máy
        if (questionUseMotobikeGo == undefined) {
            App.ShowErrorNotLoad('Bạn chưa chọn tích qlf (Có xe máy không) !!!');
            $('#btnRefuse').removeAttr('disabled', '');
            $('#btnRefuse').attr('style', 'width: 100%;');
            $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
            return false;
        }
        else if (questionUseMotobikeGo == 'true')
            questionQlf += 1;
        else
            questionQlf -= 1;

        //có đăng ký xe bản gốc
        if (questionMotobikeCertificate == undefined) {
            App.ShowErrorNotLoad('Bạn chưa chọn tích qlf (Có đăng ký xe bản gốc không) !!!');
            $('#btnRefuse').removeAttr('disabled', '');
            $('#btnRefuse').attr('style', 'width: 100%;');
            $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
            return false;
        }
        if (questionMotobikeCertificate == 'true')
            questionQlf += 1;
        else
            questionQlf -= 1;

        $('#btnRefuse').attr('disabled', false);
        form.ajaxSubmit({
            url: '/LoanBriefV2/ScriptSaveInformationLoanBriefV2',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $('#btnRefuse').removeAttr('disabled', '');
                    $('#btnRefuse').attr('style', 'width: 100%;');
                    $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
                    if (data.status == 1) {
                        CommonModalJS.InitCancelModal(loanBriefId, questionQlf, '#modelScriptTelesales');
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }
    var HideGoogleMapAddress = function () {
        $('#boxGoogleMapAddress').html('');
    }
    var HideGoogleMapAddressNew = function () {
        $('#boxGoogleMapAddress').html('');
        $('#boxGoogleMapAddress').css("display", "none");
    }
    var HideGoogleMapCompany = function () {
        $('#boxGoogleMapCompanyAddress').html('');
    }
    var ChangeTypeJob = function (tthis) {
        if (tthis.value == "123") {
            $("#div-WorkCompany").removeAttr("style", "display: none;");
            $("#div-Self-employed").attr("style", "display: none;");
            $("#div-Driver-Shipper").attr("style", "display: none;");
            $("#div-General-WorkCompanyAndSelfEmployed").removeAttr("style", "display: none;");
            $('#div-General-TypeJob').removeAttr("style", "display: none;");
            $('#div-One-WorkCompany').removeAttr("style", "display: none;");
            $('#div-One-WorkCompany').attr("style", "padding-top: 25px;");
            $('#lbl_Income').html('Lương bao nhiêu 1 tháng và nhận tiền mặt hay chuyển khoản ạ?');

        }
        else if (tthis.value == "124") {
            $("#div-Self-employed").removeAttr("style", "display: none;");
            $("#div-WorkCompany").attr("style", "display: none;");
            $("#div-Driver-Shipper").attr("style", "display: none;");
            $("#div-Freelance").attr("style", "display: none;");
            $("#div-General-WorkCompanyAndSelfEmployed").removeAttr("style", "display: none;");
            $('#div-General-TypeJob').removeAttr("style", "display: none;");
            $('#div-One-WorkCompany').attr("style", "display: none;");
            $('#lbl_Income').html('Thu nhập mỗi tháng bao nhiêu ạ?');
        }
        else if (tthis.value == "125") {
            $("#div-Driver-Shipper").removeAttr("style", "display: none;");
            $("#div-WorkCompany").attr("style", "display: none;");
            $("#div-Self-employed").attr("style", "display: none;");
            $("#div-Freelance").attr("style", "display: none;");
            $("#div-General-WorkCompanyAndSelfEmployed").attr("style", "display: none;");
            $('#div-General-TypeJob').removeAttr("style", "display: none;");
            $('#div-One-WorkCompany').attr("style", "display: none;");
            $('#lbl_Income').html('Thu nhập mỗi tháng bao nhiêu? App có thể hiện/lưu lại thu nhập của anh trong ít nhất 4 tuần gần đây không ạ?');
        }
        else if (tthis.value == "126") {
            $("#div-Freelance").removeAttr("style", "display: none;");
            $("#div-Driver-Shipper").attr("style", "display: none;");
            $("#div-WorkCompany").attr("style", "display: none;");
            $("#div-Self-employed").attr("style", "display: none;");
            $("#div-General-WorkCompanyAndSelfEmployed").attr("style", "display: none;");
            $('#div-General-TypeJob').removeAttr("style", "display: none;");
            $('#div-One-WorkCompany').attr("style", "display: none;");
            $('#lbl_Income').html('Thu nhập mỗi tháng bao nhiêu ạ?');
        }
        else {
            $("#div-WorkCompany").attr("style", "display: none;");
            $("#div-Self-employed").attr("style", "display: none;");
            $("#div-Driver-Shipper").attr("style", "display: none;");
            $("#div-Freelance").attr("style", "display: none;");
            $("#div-General-WorkCompanyAndSelfEmployed").attr("style", "display: none;");
            $('#div-General-TypeJob').attr("style", "display: none;");
            $('#div-One-WorkCompany').attr("style", "display: none;");
            $('#lbl_Income').html('');
        }
    }
    var BindToTextbox = function (tthis, elementBind) {
        if (elementBind == '#AddressGoogleMap' && $('#boxCoordinateAddress').val().trim() == '')
            $(elementBind).val($(tthis).val());
        else if (elementBind == '#AddressComapanyGoogleMap' && $('#boxCoordinateCompanyAddress').val().trim() == '')
            $(elementBind).val($(tthis).val());
    }
    var ShowImcomeTypeImage = function (imcomeType) {
        if (imcomeType == 1) {
            $('#div-WageTransfer').removeAttr('style', 'display: none;');
            $('#div-CashSalary').attr('style', 'display: none;');
        }
        else {
            $('#div-WageTransfer').attr('style', 'display: none;');
            $('#div-CashSalary').removeAttr('style', 'display: none;');
        }
    }
    var ShowTypeCareer = function (tthis) {
        if (tthis.value == "1") {
            $('#div-sellonline').attr("style", "display: none;");
            $('#div-sellcompany').removeAttr("style", "display: none;");
        }
        else if (tthis.value == "2") {
            $('#div-sellcompany').attr("style", "display: none;");
            $('#div-sellonline').removeAttr("style", "display: none;");
        }
        else {
            $('#div-sellonline').attr("style", "display: none;");
            $('#div-sellcompany').attr("style", "display: none;");
        }
    }
    var InitChangTypeJob = function () {
        var typeJob = $('#sl_TypeJob').val();
        if (typeJob == "123") {
            $("#div-WorkCompany").removeAttr("style", "display: none;");
            $("#div-Self-employed").attr("style", "display: none;");
            $("#div-Driver-Shipper").attr("style", "display: none;");
            $("#div-Freelance").attr("style", "display: none;");
            $("#div-General-WorkCompanyAndSelfEmployed").removeAttr("style", "display: none;");
            $('#div-General-TypeJob').removeAttr("style", "display: none;");
            $('#div-One-WorkCompany').removeAttr("style", "display: none;");
            $('#div-One-WorkCompany').attr("style", "padding-top: 25px;");
            $('#lbl_Income').html('Lương bao nhiêu 1 tháng và nhận tiền mặt hay chuyển khoản ạ?');
        }
        else if (typeJob == "124") {
            $("#div-Self-employed").removeAttr("style", "display: none;");
            $("#div-WorkCompany").attr("style", "display: none;");
            $("#div-Driver-Shipper").attr("style", "display: none;");
            $("#div-Freelance").attr("style", "display: none;");
            $("#div-General-WorkCompanyAndSelfEmployed").removeAttr("style", "display: none;");
            $('#div-General-TypeJob').removeAttr("style", "display: none;");
            $('#div-One-WorkCompany').attr("style", "display: none;");
            $('#lbl_Income').html('Thu nhập mỗi tháng bao nhiêu ạ?');
        }
        else if (typeJob == "125") {
            $("#div-Driver-Shipper").removeAttr("style", "display: none;");
            $("#div-WorkCompany").attr("style", "display: none;");
            $("#div-Self-employed").attr("style", "display: none;");
            $("#div-Freelance").attr("style", "display: none;");
            $("#div-General-WorkCompanyAndSelfEmployed").attr("style", "display: none;");
            $('#div-General-TypeJob').removeAttr("style", "display: none;");
            $('#div-One-WorkCompany').attr("style", "display: none;");
            $('#lbl_Income').html('Thu nhập mỗi tháng bao nhiêu? App có thể hiện/lưu lại thu nhập của anh trong ít nhất 4 tuần gần đây không ạ?');
        }
        else if (typeJob == "126") {
            $("#div-Freelance").removeAttr("style", "display: none;");
            $("#div-Driver-Shipper").attr("style", "display: none;");
            $("#div-WorkCompany").attr("style", "display: none;");
            $("#div-Self-employed").attr("style", "display: none;");
            $("#div-General-WorkCompanyAndSelfEmployed").attr("style", "display: none;");
            $('#div-General-TypeJob').removeAttr("style", "display: none;");
            $('#div-One-WorkCompany').attr("style", "display: none;");
            $('#lbl_Income').html('Thu nhập mỗi tháng bao nhiêu ạ?');
        }
        else {
            $("#div-WorkCompany").attr("style", "display: none;");
            $("#div-Self-employed").attr("style", "display: none;");
            $("#div-Driver-Shipper").attr("style", "display: none;");
            $("#div-Freelance").attr("style", "display: none;");
            $("#div-General-WorkCompanyAndSelfEmployed").attr("style", "display: none;");
            $('#div-General-TypeJob').attr("style", "display: none;");
            $('#div-One-WorkCompany').attr("style", "display: none;");
            $('#lbl_Income').html('');
        }
    }
    var InitShowTypeCareer = function () {
        var chooseCareerId = $('#sl_Career').val();
        if (chooseCareerId == "1") {
            $('#div-sellonline').attr("style", "display: none;");
            $('#div-sellcompany').removeAttr("style", "display: none;");
        }
        else if (chooseCareerId == "2") {
            $('#div-sellcompany').attr("style", "display: none;");
            $('#div-sellonline').removeAttr("style", "display: none;");
        }
        else {
            $('#div-sellonline').attr("style", "display: none;");
            $('#div-sellcompany').attr("style", "display: none;");
        }
    }
    var InitShowQuestionXMKCC = function () {
        if ($("input[name='OwnerProduct']:checked").val() == 1) {
            $('#div-Question').attr('style', 'display: none;');
        } else {
            $('#div-Question').removeAttr('style', 'display: none;');
        }
    }
    var InitDisplayCancel = function (valueCheck) {
        var form = $('#formScriptTelesalesv2');
        var loanBriefId = $('#hdd_LoanBriefId').val();
        var questionQlf = 0;
        if (valueCheck == 'false') {
            CommonModalJS.InitCancelModal(loanBriefId, questionQlf, '#modelScriptTelesales');
            form.ajaxSubmit({
                url: '/LoanBriefV2/ScriptSaveInformationLoanBriefV2',
                method: 'POST',
                success: function (data, status, xhr, $form) {
                }
            });
        }
    }
    var InitChangeBirday = function (birtday) {
        var form = $('#formScriptTelesalesv2');
        var loanBriefId = $('#hdd_LoanBriefId').val();
        var questionQlf = 0;
        var yearNow = new Date().getFullYear();
        if (birtday != '') {
            var yearBirday = parseInt(birtday.substr(6, 4));
            age = yearNow - yearBirday;
            if (age < 18 || age > 60) {
                CommonModalJS.InitCancelModal(loanBriefId, questionQlf, '#modelScriptTelesales');
                form.ajaxSubmit({
                    url: '/LoanBriefV2/ScriptSaveInformationLoanBriefV2',
                    method: 'POST',
                    success: function (data, status, xhr, $form) {
                    }
                });
            }
        }
    }


    return {
        Init: Init,
        GetDistrict: GetDistrict,
        GetWard: GetWard,
        GetProduct: GetProduct,
        GetLoanBrief: GetLoanBrief,
        SubmitForm: SubmitForm,
        GetMaxPrice: GetMaxPrice,
        CallAI: CallAI,
        ViewResponseAI: ViewResponseAI,
        SendCheckPhoneRelationshipAI: SendCheckPhoneRelationshipAI,
        CancelLoanBrief: CancelLoanBrief,
        LoadPriceMoto: LoadPriceMoto,
        HideGoogleMapAddress: HideGoogleMapAddress,
        HideGoogleMapCompany: HideGoogleMapCompany,
        ChangeTypeJob: ChangeTypeJob,
        BindToTextbox: BindToTextbox,
        ShowImcomeTypeImage: ShowImcomeTypeImage,
        InitChangTypeJob: InitChangTypeJob,
        ShowTypeCareer: ShowTypeCareer,
        InitShowTypeCareer: InitShowTypeCareer,
        InitShowQuestionXMKCC: InitShowQuestionXMKCC,
        HideGoogleMapAddressNew: HideGoogleMapAddressNew,
        InitDisplayCancel: InitDisplayCancel,
        InitChangeBirday: InitChangeBirday,
        SubmitCreateLoanbrief: SubmitCreateLoanbrief
    };
}