﻿var ScriptMotobikeModuleJS_v5 = new function () {

    var Init = function () {
        $('#box-script #sl_province_addresshome,#box-script #sl_province_addresshousehold,#box-script #sl_province_work_company').select2({
            placeholder: "Tỉnh/ Thành phố",
            width: '100%'
        });
        $("#box-script #sl_district_addresshome,#box-script #sl_district_addresshousehold,#box-script #sl_district_work_company").select2({
            placeholder: "Quận/ Huyện",
            width: '100%'
        });
        $("#box-script #sl_ward_addresshome,#box-script #sl_ward_addresshousehold,#box-script #sl_ward_work_company").select2({
            placeholder: "Phường/ Xã",
            width: '100%'
        });

        $("#box-script #sl_DocmentRelationship").select2({
            placeholder: "Vui lòng chọn",
            width: '100%'
        });

        $('#box-script #sl_brand_product').select2({
            placeholder: "Chọn hãng xe",
            width: '100%'
        });

        $('#box-script #sl_product_name').select2({
            placeholder: "Chọn tên xe",
            width: '100%'
        });

        $('#box-script #sl_TypeHouseHold').select2({
            placeholder: "Chọn địa chỉ SHK",
            width: '100%'
        });

        $("#box-script #sl_TypeJob").select2({
            placeholder: "Chọn chứng từ công việc",
            width: '100%'
        });

        $("#box-script #sl_relative_family").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_relative_family2").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_DocumentTrungSHK").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_DocumentKhongTrungSHK").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_DocumentRentHouse").select2({
            placeholder: "Vui lòng chọn"
        });

        $('#box-script #sl_LoanPurpose').select2({
            placeholder: "Mục đích vay vốn",
            width: '100%'
        });

        $('#box-script #sl_RateType').select2({
            placeholder: "Hình thức thanh toán",
            width: '100%'
        });

        $("#box-script #sl_DocumentLuongCK").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_DocumentLuongTM").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_JobDescriptionId").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_water_supplier").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_relative_house_owner").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_DocumentFormsOfResidence").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#box-script #sl_DocumentBusiness").select2({
            placeholder: "Vui lòng chọn"
        });

        $('#div-sendResultCheckPhoneRelationshipAI1').hide();
        $('#div-sendResultCheckPhoneRelationshipAI').hide();
        InitImcomeTypeImage();
        InitShowImageAddress();
        InitDocmentRelationship();
        InitInfomationHouseOwner();
    }

    var GetDistrict = function (provinceId, districtId, element_district, element_ward) {
        if (provinceId > 0) {
            App.Ajax("GET", "/Dictionary/GetDistrict?id=" + provinceId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (districtId > 0 && districtId == data.data[i].districtId) {
                            html += `<option value="${data.data[i].districtId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].districtId}">${data.data[i].name}</option>`;
                        }
                    }
                    $(element_district).html(html);
                    $(element_district).select2({
                        placeholder: "Quận/ Huyện"
                    });
                }
            });
            //load lại phường xã
            $(element_ward).html("<option></option>");
            $(element_ward).select2({
                placeholder: "Phường/ Xã"
            });
        } else {
            $(element_district).select2({
                placeholder: "Quận/ Huyện",
                width: '100%'
            });
        }
    }

    var GetWard = function (districtId, wardId, element) {
        if (districtId > 0) {
            App.Ajax("GET", "/Dictionary/GetWard?districtId=" + districtId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (wardId > 0 && wardId == data.data[i].wardId) {
                            html += `<option value="${data.data[i].wardId}" selected="selected">${data.data[i].name}</option>`;
                        } else {
                            html += `<option value="${data.data[i].wardId}">${data.data[i].name}</option>`;
                        }
                    }
                    $(element).html(html);
                    $(element).select2({
                        placeholder: "Phường/ Xã"
                    });
                }
            });
        } else {
            $(element).select2({
                placeholder: "Phường/ Xã",
                width: '100%'
            });
        }
    }

    var GetProduct = function (brandId, productId) {
        if (brandId > 0) {
            App.Ajax("GET", "/Dictionary/GetProduct?brandId=" + brandId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html += "<option></option>";
                    for (var i = 0; i < data.data.length; i++) {
                        if (productId > 0 && productId == data.data[i].id) {
                            html += `<option value="${data.data[i].id}" selected="selected">${data.data[i].fullName}</option>`;
                        } else {
                            html += `<option value="${data.data[i].id}">${data.data[i].fullName}</option>`;
                        }
                    }
                    $("#box-script #sl_product_name").html(html);
                    $("#box-script #sl_product_name").select2({
                        placeholder: "Chọn tên xe"
                    });
                }
                else {
                    var html = "";
                    html += "<option></option>";
                    $("#box-script #sl_product_name").html(html);
                    $("#box-script #sl_product_name").select2({
                        placeholder: "Chọn tên xe"
                    });
                }
            });
        } else {
            $('#box-script #sl_product_name').select2({
                placeholder: "Chọn tên xe",
                width: '100%'
            });
        }
    }

    var GetLoanBrief = function (phone) {
        if (phone != null && phone.trim() != "") {
            $.ajax({
                url: '/LoanBrief/ListLoanBriefSearch?phone=' + phone,
                type: 'GET',
                dataType: 'json',
                success: function (res) {
                    if (res.isSuccess == 1) {
                        $('#box-loanbrief-search').html(res.html);
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
                }

            });
        } else {
            App.ShowErrorNotLoad('Chưa nhập số điện thoại tra cứu');
        }

    }

    var InitPriceMoto = function (ProductId, typeOfOwnerShipScript, jobId, imcomeType, companyInsurance, questionLicenseDkkd, ProductCredit, LoanBriefId) {
        $.ajax({
            url: '/LoanBriefV3/GetMaxPriceProductScript',
            type: 'GET',
            data: {
                'productId': ProductId,
                'typeOfOwnerShipScript': typeOfOwnerShipScript,
                'jobId': jobId,
                'imcomeType': imcomeType,
                'companyInsurance': companyInsurance,
                'questionLicenseDkkd': questionLicenseDkkd,
                'productCredit': ProductCredit,
                'loanBriefId': LoanBriefId
            },
            dataType: 'json',
            success: function (res) {
                if (res.isSuccess == 1) {
                    $('#txtMaxPrice').html(App.FormatCurrency(res.data));
                    $('#txtMaxPrice2').html(App.FormatCurrency(res.data));
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            error: function () {
                App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
            }

        });
    }

    var GetMaxPrice = function () {
        var ProductCredit = 0;
        if ($("input[name='OwnerProduct']:checked").val() == 1)
            ProductCredit = 2;//xe máy chính chủ
        else
            ProductCredit = 5;// xe máy kcc
        var LoanBriefId = $('#box-script #hdd_LoanBriefId').val();
        var ProductId = $('#box-script #sl_product_name').val();
        var typeOfOwnerShipScript = $('#box-script #hdd_TypeOwnerShip').val();
        var jobId = $('#box-script #hdd_JobId').val();
        var imcomeType = $("#box-script #hdd_ImcomeType").val();
        var companyInsurance = $("input[name='LoanBriefJobModel.CompanyInsurance']:checked").val();
        var questionLicenseDkkd = $("#hdd_BusinessPapers").val();

        if (ProductId > 0 && typeOfOwnerShipScript > 0 && ProductCredit > 0) {
            $.ajax({
                url: '/LoanBriefV3/GetMaxPriceProductScript',
                type: 'GET',
                data: {
                    'productId': ProductId,
                    'typeOfOwnerShipScript': typeOfOwnerShipScript,
                    'jobId': jobId,
                    'imcomeType': imcomeType,
                    'companyInsurance': companyInsurance,
                    'questionLicenseDkkd': questionLicenseDkkd,
                    'productCredit': ProductCredit,
                    'loanBriefId': LoanBriefId
                },
                dataType: 'json',
                success: function (res) {
                    if (res.isSuccess == 1) {
                        $('#txtMaxPrice').html(App.FormatCurrency(res.data));
                        $('#txtMaxPrice2').html(App.FormatCurrency(res.data));
                    } else {
                        App.ShowErrorNotLoad(res.message);
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi không mong muốn. Vui lòng thử lại');
                }

            });
        }
    }

    var CallAI = function () {
        $('#btn-request-ai').attr('disabled', 'disabled');
        var form = $('#formScriptTelesalesv4');
        form.validate({
            ignore: [],
            rules: {
                FullName: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                Phone: {
                    required: true,
                    validPhone: true
                },
                NationalCard: {
                    required: true,
                    validCardnumber: true
                },
                'LoanBriefResident.Address': {
                    required: true
                },
                sBirthDay: {
                    validDate: true
                }
            },
            messages: {
                FullName: {
                    required: "Bạn vui lòng nhập họ tên",
                },
                Phone: {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                NationalCard: {
                    required: "Vui lòng nhập thông tin CMND",
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'LoanBriefResident.Address': {
                    required: "Vui lòng nhập địa chỉ nơi ở"
                },

                sBirthDay: {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                }


            },
            errorPlacement: function (error, element) {
                if (element.hasClass('m-select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                    error.insertAfter(element.parent().parent());
                }
                else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.appendTo(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });
        if (!form.valid()) {
            $('#btn-request-ai').removeAttr('disabled', 'disabled');
            return;
        }
        $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
        $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
        $('#btn-request-ai').hide();
        form.ajaxSubmit({
            url: '/LoanBriefV3/RequestAIV3',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $('#btn-request-ai').removeAttr('disabled', 'disabled');
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#btn-request-ai').remove();
                        $('#btn-response-ai').show();
                        App.ShowSuccessNotLoad(data.message);
                    }
                    else {
                        $('#btn-request-ai').show();
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    $('#btn-request-ai').show();
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    var SendPhoneRelationshipAI = function () {
        $('#btn_CheckPhoneRelationshipAI').attr('disabled', 'disabled');
        var form = $('#formScriptTelesalesv4');
        form.validate({
            ignore: [],
            rules: {
                'LoanBriefRelationshipModels[0].Phone': {
                    required: true,
                    validPhone: true
                },
                'LoanBriefRelationshipModels[1].Phone': {
                    required: true,
                    validPhone: true
                }
            },
            messages: {

                'LoanBriefRelationshipModels[0].Phone': {
                    required: "Vui lòng nhập số điện thoại người thân 1",
                    validPhone: "Số điện thoại người thân 1 không hợp lệ "
                },
                'LoanBriefRelationshipModels[1].Phone': {
                    required: "Vui lòng nhập số điện thoại người thân 2",
                    validPhone: "Số điện thoại người thân 2 không hợp lệ "
                }
            },
            errorPlacement: function (error, element) {
                if (element.hasClass('m-select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                    error.insertAfter(element.parent().parent());
                }
                else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.appendTo(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });
        if (!form.valid()) {
            $('#btn_CheckPhoneRelationshipAI').removeAttr('disabled', 'disabled');
            return;
        }
        form.ajaxSubmit({
            url: '/LoanBriefV3/RequestRefphoneV3',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $('#btn_CheckPhoneRelationshipAI').removeAttr('disabled', 'disabled');
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#div-sendResultCheckPhoneRelationshipAI1').show();
                        $('#div-sendResultCheckPhoneRelationshipAI').show();
                        $('#div-AIresult-CheckPhoneRelationship').hide();
                        $('#div-AIresult-CheckPhoneRelationship1').hide();
                        App.ShowSuccessNotLoad(data.message);
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    var BindToTextbox = function (tthis, elementBind) {
        if (elementBind == '#AddressGoogleMap' && $('#boxCoordinateAddress').val().trim() == '')
            $(elementBind).val($(tthis).val());
        else if (elementBind == '#AddressComapanyGoogleMap' && $('#boxCoordinateCompanyAddress').val().trim() == '')
            $(elementBind).val($(tthis).val());
    }

    var InitDisplayCancel = function (valueCheck) {
        var form = $('#formScriptTelesalesv4');
        var loanBriefId = $('#hdd_LoanBriefId').val();
        var questionQlf = 0;
        if (valueCheck == 'false') {
            CommonModalJS.InitCancelModal(loanBriefId, questionQlf, '#modelScriptTelesales');
            form.ajaxSubmit({
                url: '/LoanBriefV3/ScriptSaveInformationLoanBriefV3',
                method: 'POST',
                success: function (data, status, xhr, $form) {
                }
            });
        }
    }

    var ShowInterestTool = function () {
        var totalMoneyDisbursement = $('#txtMaxPrice').text().replace(/,/g, '');
        if (totalMoneyDisbursement == null || totalMoneyDisbursement == undefined || totalMoneyDisbursement == '' || totalMoneyDisbursement == '0') {
            App.ShowErrorNotLoad("Chưa có số tiền định giá xe không thể tính lãi phí");
            return false;
        }
        var loanTime = 12; //default
        var rateType = $('#sl_RateType').val();
        var frequency = 30; //default
        var strRate = (2700 * 365 / 1000000) * 100;
        var rate = strRate.toFixed(2);
        var loanbriefId = $('#hdd_LoanBriefId').val();
        var calculationInterestOften = true;
        $.ajax({
            type: "POST",
            url: "/Approve/ViewIntersestSubmit",
            data: {
                CalculationInterestOften: calculationInterestOften, LoanTime: loanTime, TotalMoneyDisbursement: totalMoneyDisbursement,
                RateType: rateType, Frequency: frequency, Rate: rate, LoanbriefId: loanbriefId
            },
            success: function (data) {
                $("#box-interesttool").removeAttr('style', 'display: none;');
                $("#box-interesttool").html('');
                $('#box-interesttool').append(data.html);
            },
            traditional: true
        });
    }

    var CheckQLFTelesales = function () {
        //kiểm tra xem telesales tích đủ qualify chưa
        var questionQlf = 0;
        //có nhu cầu vay
        var questionBorrow = $("input[name='LoanBriefQuestionScriptModel.QuestionBorrow']:checked").val();
        //có xe máy
        var questionUseMotobikeGo = $("input[name='LoanBriefQuestionScriptModel.QuestionUseMotobikeGo']:checked").val();
        //có đăng ký bản gốc
        var questionMotobikeCertificate = $("input[name='LoanBriefQuestionScriptModel.QuestionMotobikeCertificate']:checked").val();

        if (questionBorrow == undefined)
            return questionQlf = -10;
        else if (questionBorrow == 'true')
            questionQlf += 1;
        //trong độ tuổi hỗ trợ
        var birtday = $('#txtBirthDay').val();
        var yearNow = new Date().getFullYear();
        if (birtday != '') {
            var yearBirday = parseInt(birtday.substr(6, 4));
            age = yearNow - yearBirday;
            if (age >= 18 && age <= 60)
                questionQlf += 1;
        }
        else {
            return questionQlf = -11;
        }
        //có xe máy
        if (questionUseMotobikeGo == undefined)
            return questionQlf = -12;
        else if (questionUseMotobikeGo == 'true')
            questionQlf += 1;
        //có đăng ký xe bản gốc
        if (questionMotobikeCertificate == undefined)
            return questionQlf = -13;
        if (questionMotobikeCertificate == 'true')
            questionQlf += 1;
        return questionQlf;
    }

    var CancelLoanBrief = function (loanBriefId) {
        $('#btnRefuse').attr('disabled', false);
        $('#btnRefuse').attr('style', 'cursor: no-drop; width: 100%;');
        $('#btnRefuse').addClass('m-loader m-loader--success m-loader--right');
        var form = $('#formScriptTelesalesv4');
        //kiểm tra xem telesales tích đủ lý do qualify chưa
        var checkQLFTelesales = CheckQLFTelesales();
        if (checkQLFTelesales == -10) {
            App.ShowErrorNotLoad('Bạn chưa chọn tích qlf (Có nhu cầu vay) !!!');
            $('#btnRefuse').removeAttr('disabled', '');
            $('#btnRefuse').attr('style', 'width: 100%;');
            $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
            return false;
        }
        else if (checkQLFTelesales == -11) {
            App.ShowErrorNotLoad('Bạn chưa chọn tích qlf (Chọn ngày sinh) !!!');
            $('#btnRefuse').removeAttr('disabled', '');
            $('#btnRefuse').attr('style', 'width: 100%;');
            $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
            return false;
        }
        else if (checkQLFTelesales == -12) {
            App.ShowErrorNotLoad('Bạn chưa chọn tích qlf (Có xe máy không) !!!');
            $('#btnRefuse').removeAttr('disabled', '');
            $('#btnRefuse').attr('style', 'width: 100%;');
            $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
            return false;
        }
        else if (checkQLFTelesales == -13) {
            App.ShowErrorNotLoad('Bạn chưa chọn tích qlf (Có đăng ký xe bản gốc không) !!!');
            $('#btnRefuse').removeAttr('disabled', '');
            $('#btnRefuse').attr('style', 'width: 100%;');
            $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
            return false;
        }

        form.ajaxSubmit({
            url: '/LoanBriefV3/ScriptSaveInformationLoanBriefV3',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $('#btnRefuse').removeAttr('disabled', '');
                    $('#btnRefuse').attr('style', 'width: 100%;');
                    $('#btnRefuse').removeClass('m-loader m-loader--success m-loader--right');
                    if (data.status == 1) {
                        CommonModalJS.InitCancelModal(loanBriefId, checkQLFTelesales, '#modelScriptTelesales');
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    var SubmitForm = function () {
        $('#btnScriptSave').attr('disabled', '');
        $('#btnScriptSave').attr('style', 'cursor: no-drop; width: 100%;');
        $('#btnScriptSave').addClass('m-loader m-loader--success m-loader--right');
        //Check QLF telesales tích
        var questionQlf = CheckQLFTelesales();
        var form = $('#formScriptTelesalesv4');
        form.validate({
            ignore: [],
            rules: {
                'FullName': {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                'Phone': {
                    required: true,
                    validPhone: true
                },
                'NationalCard': {
                    required: true,
                    validCardnumber: true
                },
                'NationCardPlace': {
                    validCardnumber: true
                },
                sBirthDay: {
                    validDate: true
                },
                'ProvinceId': {
                    required: true
                },
                'DistrictId': {
                    required: true
                },
                LoanAmount: {
                    validLoanAmount: true
                },
                'LoanBriefRelationshipModels[0].Phone': {
                    validPhone: true
                },
                'LoanBriefRelationshipModels[1].Phone': {
                    validPhone: true
                },
            },
            messages: {
                'FullName': {
                    required: "Bạn vui lòng nhập họ tên",
                },
                'Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                'NationalCard': {
                    required: "Vui lòng nhập CMND/CCCD",
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'NationCardPlace': {
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                sBirthDay: {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                },
                'ProvinceId': {
                    required: "Vui lòng chọn thành phố đang sống"
                },
                'DistrictId': {
                    required: "Vui lòng chọn quận/huyện đang sống"
                },
                LoanAmount: {
                    validLoanAmount: "Vui lòng nhập số tiền khách hàng cần vay"
                },
                'LoanBriefRelationshipModels[0].Phone': {
                    validPhone: "Số điện thoại người thân 1 không hợp lệ"
                },
                'LoanBriefRelationshipModels[1].Phone': {
                    validPhone: "Số điện thoại người thân 2 không hợp lệ"
                },
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            $('#btnScriptSave').removeAttr('disabled', '');
            $('#btnScriptSave').attr('style', 'width: 100%;');
            $('#btnScriptSave').removeClass('m-loader m-loader--success m-loader--right');
            return;
        }
        //Check xem đã tích giấy tờ chứng nhận chưa
        var jobId = $('#sl_TypeJob').val();
        //đi làm hường lương
        if (jobId == 123) {
            if ($("input[name='LoanBriefJobModel.CompanyInsurance']:checked").val() == undefined) {
                App.ShowErrorNotLoad('Vui lòng chọn có tham gia BHXH không');
                $('#btnScriptSave').removeAttr('disabled', '');
                $('#btnScriptSave').attr('style', 'width: 100%;');
                $('#btnScriptSave').removeClass('m-loader m-loader--success m-loader--right');
                return;
            }
        }
        //Tự kinh doanh
        if (jobId == 124) {
            if ($("input[name='LoanBriefJobModel.BusinessPapers']:checked").val() == undefined) {
                App.ShowErrorNotLoad('Vui lòng chọn có Giấy ĐKKD/Vệ sinh ATTP/Chứng nhận địa điểm KD không');
                $('#btnScriptSave').removeAttr('disabled', '');
                $('#btnScriptSave').attr('style', 'width: 100%;');
                $('#btnScriptSave').removeClass('m-loader m-loader--success m-loader--right');
                return;
            }
        }

        $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
        $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
        form.ajaxSubmit({
            url: '/LoanBriefV3/ScriptUpdateLoanBriefV3',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#modelscripttelesales').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        var loanBriefId = $('#hdd_LoanBriefId').val();
                        CommonModalJS.InitStatusTelesalesModal('#modalStatusTelesales', loanBriefId, questionQlf);
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
                $('#btnScriptSave').removeAttr('disabled', '');
                $('#btnScriptSave').attr('style', 'width: 100%;');
                $('#btnScriptSave').removeClass('m-loader m-loader--success m-loader--right');
            }
        });
    }

    var SubmitCreateLoanBrief = function () {
        $('#btnScriptSave').attr('disabled', '');
        $('#btnScriptSave').attr('style', 'cursor: no-drop; width: 100%;');
        $('#btnScriptSave').addClass('m-loader m-loader--success m-loader--right');
        //Check QLF telesales tích
        var questionQlf = CheckQLFTelesales();
        var form = $('#formScriptTelesalesv4');
        form.validate({
            ignore: [],
            rules: {
                'FullName': {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                'Phone': {
                    required: true,
                    validPhone: true
                },
                'NationalCard': {
                    required: true,
                    validCardnumber: true
                },
                'NationCardPlace': {
                    validCardnumber: true
                },
                sBirthDay: {
                    validDate: true
                },
                'ProvinceId': {
                    required: true
                },
                'DistrictId': {
                    required: true
                },
                LoanAmount: {
                    validLoanAmount: true
                },
                'LoanBriefRelationshipModels[0].Phone': {
                    validPhone: true
                },
                'LoanBriefRelationshipModels[1].Phone': {
                    validPhone: true
                },
            },
            messages: {
                'FullName': {
                    required: "Bạn vui lòng nhập họ tên",
                },
                'Phone': {
                    required: "Vui lòng nhập số điện thoại",
                    validPhone: "Số điện thoại không hợp lệ "
                },
                'NationalCard': {
                    required: "Vui lòng nhập CMND/CCCD",
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                'NationCardPlace': {
                    validCardnumber: "Vui lòng nhập đúng định dạng CMND"
                },
                sBirthDay: {
                    validDate: "Bạn vui lòng nhập đúng định dạng dd/MM/yyyy"
                },
                'ProvinceId': {
                    required: "Vui lòng chọn thành phố đang sống"
                },
                'DistrictId': {
                    required: "Vui lòng chọn quận/huyện đang sống"
                },
                LoanAmount: {
                    validLoanAmount: "Vui lòng nhập số tiền khách hàng cần vay"
                },
                'LoanBriefRelationshipModels[0].Phone': {
                    validPhone: "Số điện thoại người thân 1 không hợp lệ"
                },
                'LoanBriefRelationshipModels[1].Phone': {
                    validPhone: "Số điện thoại người thân 2 không hợp lệ"
                },
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            $('#btnScriptSave').removeAttr('disabled', '');
            $('#btnScriptSave').attr('style', 'width: 100%;');
            $('#btnScriptSave').removeClass('m-loader m-loader--success m-loader--right');
            return;
        }
        $('#txtTotalIncome').val($('#txtTotalIncome').val().replace(/,/g, ''));
        $('#txtLoanAmount').val($('#txtLoanAmount').val().replace(/,/g, ''));
        form.ajaxSubmit({
            url: '/LoanBriefV3/CreateLoanBriefScriptV3',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#modelscripttelesales').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        var loanBriefId = data.data;
                        CommonModalJS.InitStatusTelesalesModal('#modalStatusTelesales', loanBriefId, questionQlf);
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
                $('#btnScriptSave').removeAttr('disabled', '');
                $('#btnScriptSave').attr('style', 'width: 100%;');
                $('#btnScriptSave').removeClass('m-loader m-loader--success m-loader--right');
            }
        });
    }

    var InitImcomeTypeImage = function () {
        var imcomeType = $("input[name='LoanBriefJobModel.ImcomeType']:checked").val();
        if (imcomeType == 5) {
            $('#div-LuongCK').removeAttr('style', 'display: none;');
            $('#div-LuongTM').attr('style', 'display: none;');
        }
        if (imcomeType == 1) {
            $('#div-LuongTM').removeAttr('style', 'display: none;');
            $('#div-LuongCK').attr('style', 'display: none;');
        }
    }

    var InitShowImageAddress = function () {
        var questionAddress = $("input[name='LoanBriefQuestionScriptModel.QuestionAddress']:checked").val();
        if (questionAddress == "true") {
            $('#lbl_type_ownership').html('');
            $('#lbl_type_ownership').html('Sống cùng Tỉnh/TP với địa chi trên SHK');
            $('#div-TrungSHK').removeAttr('style', 'display: none;');
            $('#div-KhongTrungSHK').attr('style', 'display: none;');
        }
        else if (questionAddress == "false") {
            $('#lbl_type_ownership').html('');
            $('#lbl_type_ownership').html('Sống khác Tỉnh/TP với địa chi trên SHK');
            $('#div-KhongTrungSHK').removeAttr('style', 'display: none;');
            $('#div-TrungSHK').attr('style', 'display: none;');
        }
    }

    var ChangeDocumentRelationship = function (value) {
        //check hiển thị ra khai khác giấy tờ mối quan hệ
        if (value == 2 || value == 4 || value == 6) {
            $('#div-DocmentRelationship').removeAttr('style', 'display: none;');
        }
        else {
            $('#div-DocmentRelationship').attr('style', 'display: none;');
        }
        //Hiển thị lấy thông tin mã hóa đơn điện và nước
        if (value == 3 || value == 4) {
            $('.div-BillElectrictyAndWaterId').removeAttr('style', 'display: none;');
        }
        else {
            $('.div-BillElectrictyAndWaterId').attr('style', 'display: none;');
        }
        //gán giá trị nhà sở hữu THS với KTHS vào biến ẩn
        var questionAddress = $("input[name='LoanBriefQuestionScriptModel.QuestionAddress']:checked").val();
        if (questionAddress == "true") {
            if (value == 1 || value == 2 || value == 3 || value == 4)
                $('#hdd_TypeOwnerShip').val('9');
            else if (value == 5 || value == 6)
                $('#hdd_TypeOwnerShip').val('10');
            else
                $('#hdd_TypeOwnerShip').val('11');
        }
        else if (questionAddress == "false") {
            if (value == 11 || value == 12 || value == 13)
                $('#hdd_TypeOwnerShip').val('13');
            else
                $('#hdd_TypeOwnerShip').val('11');
        }
        GetMaxPrice();
    }

    var InitDocmentRelationship = function () {
        var value = 0;
        var checkDataTHS = $('#sl_DocumentTrungSHK').val();
        var checkDataKTHS = $('#sl_DocumentKhongTrungSHK').val();
        if (checkDataTHS != "")
            value = parseInt(checkDataTHS);
        else if (checkDataKTHS != "")
            value = parseInt(checkDataKTHS);
        //check hiển thị ra khai khác giấy tờ mối quan hệ
        if (value == 2 || value == 4 || value == 6) {
            $('#div-DocmentRelationship').removeAttr('style', 'display: none;');
        }
        else {
            $('#div-DocmentRelationship').attr('style', 'display: none;');
        }
        //Hiển thị lấy thông tin mã hóa đơn điện và nước
        if (value == 3 || value == 4) {
            $('.div-BillElectrictyAndWaterId').removeAttr('style', 'display: none;');
        }
        else {
            $('.div-BillElectrictyAndWaterId').attr('style', 'display: none;');
        }
    }

    var InitChangeBirday = function (birtday) {
        var form = $('#formScriptTelesalesv4');
        var loanBriefId = $('#hdd_LoanBriefId').val();
        var questionQlf = 0;
        var yearNow = new Date().getFullYear();
        if (birtday != '') {
            var yearBirday = parseInt(birtday.substr(6, 4));
            age = yearNow - yearBirday;
            if (age < 18 || age > 60) {
                CommonModalJS.InitCancelModal(loanBriefId, questionQlf, '#modelScriptTelesales');
                form.ajaxSubmit({
                    url: '/LoanBriefV3/ScriptSaveInformationLoanBriefV3',
                    method: 'POST',
                    success: function (data, status, xhr, $form) {
                    }
                });
            }
        }
    }

    var CheckElectricity = function () {
        $('#btnCheckElectricity').attr('disabled', false);
        $('#btnCheckElectricity').attr('style', 'cursor: no-drop; width: 100%;');
        var electricityId = $('#txt-BillElectricityId').val();
        var loanbriefId = $('#hdd_LoanBriefId').val();
        if (electricityId == undefined || electricityId == null || electricityId == '') {
            App.ShowErrorNotLoad('Bạn phải nhập mã hóa đơn điện');
            $('#btnCheckElectricity').removeAttr('disabled', false);
            $('#btnCheckElectricity').removeAttr('style', 'cursor: no-drop; width: 100%;');
            return false;
        }
        else {
            $.ajax({
                type: "GET",
                url: "/LoanBriefV3/CallAICheckElectricity",
                data: { LoanbriefId: loanbriefId, ElectricityId: electricityId },
                success: function (data) {
                    $('#btnCheckElectricity').removeAttr('disabled', false);
                    $('#btnCheckElectricity').removeAttr('style', 'cursor: no-drop; width: 100%;');
                    $('#div-response-checkbillelectricty').html('');
                    $('#div-response-checkbillelectricty').append(data);
                },
                traditional: true
            });
        }
    }

    var CheckWater = function () {
        $('#btnCheckWater').attr('disabled', false);
        $('#btnCheckWater').attr('style', 'cursor: no-drop; width: 100%;');
        var waterId = $('#txt-BillWaterId').val();
        var waterSupplier = $('#sl_water_supplier').val();
        var loanbriefId = $('#hdd_LoanBriefId').val();
        if (waterId == undefined || waterId == null || waterId == '') {
            App.ShowErrorNotLoad('Bạn phải nhập mã hóa đơn nước');
            $('#btnCheckWater').removeAttr('disabled', false);
            $('#btnCheckWater').removeAttr('style', 'cursor: no-drop; width: 100%;');
            return false;
        }
        else if (waterSupplier == undefined || waterSupplier == null || waterSupplier == '') {
            App.ShowErrorNotLoad('Bạn phải chọn nhà cung cấp nước');
            $('#btnCheckWater').removeAttr('disabled', false);
            $('#btnCheckWater').removeAttr('style', 'cursor: no-drop; width: 100%;');
            return false;
        }
        else {
            $.ajax({
                type: "GET",
                url: "/LoanBriefV3/CallAICheckWater",
                data: { LoanbriefId: loanbriefId, WaterId: waterId, WaterSupplier: waterSupplier },
                success: function (data) {
                    $('#btnCheckWater').removeAttr('disabled', false);
                    $('#btnCheckWater').removeAttr('style', 'cursor: no-drop; width: 100%;');
                    $('#div-response-checkbillwater').html('');
                    $('#div-response-checkbillwater').append(data);
                },
                traditional: true
            });
        }
    }

    var ShowAndHideElectricity = function () {
        var check = document.getElementById("div-Electricity").style.display == "none";
        if (check == false)
            $('#div-Electricity').attr('style', 'display: none;');
        else
            $('#div-Electricity').removeAttr('style', 'display: none;');
    }

    var ShowAndHideWater = function () {
        var check = document.getElementById("div-Water").style.display == "none";
        if (check == false)
            $('#div-Water').attr('style', 'display: none;');
        else
            $('#div-Water').removeAttr('style', 'display: none;');
    }

    var HideInterestTool = function () {
        $("#box-interesttool").attr('style', 'display: none;');
    }

    var ShowImageAddress = function (questionAddress) {
        $('#hdd_TypeOwnerShip').val('');
        if (questionAddress == "true") {
            $('#lbl_type_ownership').html('Sống cùng Tỉnh/TP với địa chi trên SHK');
            $('#div-TrungSHK').removeAttr('style', 'display: none;');
            $('#div-KhongTrungSHK').attr('style', 'display: none;');
            $('#sl_DocumentTrungSHK').val('').change();
            $('#sl_DocumentKhongTrungSHK').val('').change();
            $('#hdd_TypeOwnerShip').val('9');
        }
        else if (questionAddress == "false") {
            $('#lbl_type_ownership').html('Sống khác Tỉnh/TP với địa chi trên SHK');
            $('#div-KhongTrungSHK').removeAttr('style', 'display: none;');
            $('#div-TrungSHK').attr('style', 'display: none;');
            $('#sl_DocumentTrungSHK').val('').change();
            $('#sl_DocumentKhongTrungSHK').val('').change();
            $('#hdd_TypeOwnerShip').val('11');
        }
        GetMaxPrice();
    }

    var ChangImcomeTypeImage = function (imcomeType) {
        if (imcomeType == 5) {
            $('#div-LuongCK').removeAttr('style', 'display: none;');
            $('#div-LuongTM').attr('style', 'display: none;');
        }
        else if (imcomeType == 1) {
            $('#div-LuongTM').removeAttr('style', 'display: none;');
            $('#div-LuongCK').attr('style', 'display: none;');
        }
    }

    var ChangeInfomationHouseOwner = function (houseOwner) {
        if (houseOwner == 'false') {
            $('.infomationHouseOwner').removeAttr('style', 'display: none;');
            $('.lbl_infomationHouseOwner').removeAttr('style', 'display: none;');
            $('.lbl_infomationHouseOwner').attr('style', 'margin-top: 10px;');
        }
        else {
            $('.infomationHouseOwner').attr('style', 'display: none;');
            $('.lbl_infomationHouseOwner').attr('style', 'display: none;');
        }
    }

    var InitInfomationHouseOwner = function () {
        var houseOwner = $("input[name='LoanBriefQuestionScriptModel.QuestionHouseOwner']:checked").val();
        if (houseOwner == 'false') {
            $('.infomationHouseOwner').removeAttr('style', 'display: none;');
            $('.lbl_infomationHouseOwner').removeAttr('style', 'display: none;');
            $('.lbl_infomationHouseOwner').attr('style', 'margin-top: 10px;');
        }
        else {
            $('.infomationHouseOwner').attr('style', 'display: none;');
            $('.lbl_infomationHouseOwner').attr('style', 'display: none;');
        }
    }

    var ChangeTypeJob = function (typeJob) {
        //Lương chuyển khoản
        if (typeJob == 1) {
            $('#div-LuongCK').removeAttr('style', 'display: none;');
            $('#div-LuongTM').attr('style', 'display: none;');
            $('.div-TuKinhDoanh').attr('style', 'display: none;');
            $('.div-TaiXeCongNghe').attr('style', 'display: none;');
            $('#hdd_ImcomeType').val('5'); // lương chuyển khoản
            $('#hdd_JobId').val('123');//đi làm hưởng lương
            $('#hdd_BusinessPapers').val('');
            $('.div-CommentDetailJob').attr('style', 'display: none;');
        }
        //Lương tiền mặt
        else if (typeJob == 2) {
            $('#div-LuongCK').attr('style', 'display: none;');
            $('#div-LuongTM').removeAttr('style', 'display: none;');
            $('.div-TuKinhDoanh').attr('style', 'display: none;');
            $('.div-TaiXeCongNghe').attr('style', 'display: none;');
            $('#hdd_ImcomeType').val('1'); // lương tiền mặt
            $('#hdd_JobId').val('123');//đi làm hưởng lương
            $('#hdd_BusinessPapers').val('');
            $('.div-CommentDetailJob').attr('style', 'display: none;');
        }
        //Kinh doanh có ĐK
        else if (typeJob == 3) {
            $('#div-LuongCK').attr('style', 'display: none;');
            $('#div-LuongTM').attr('style', 'display: none;');
            $('.div-TuKinhDoanh').removeAttr('style', 'display: none;');
            $('.div-TaiXeCongNghe').attr('style', 'display: none;');
            $('#hdd_BusinessPapers').val('true');
            $('#hdd_JobId').val('124');//tự kinh doanh
            $('#hdd_ImcomeType').val('');
            $('.div-CommentDetailJob').removeAttr('style', 'display: none;');
        }
        //Kinh doanh không có ĐK và làm tự do
        else if (typeJob == 4) {
            $('#div-LuongCK').attr('style', 'display: none;');
            $('#div-LuongTM').attr('style', 'display: none;');
            $('.div-TuKinhDoanh').attr('style', 'display: none;');
            $('.div-TaiXeCongNghe').attr('style', 'display: none;');
            $('#hdd_BusinessPapers').val('false');
            $('#hdd_JobId').val('126');//làm tự do
            $('#hdd_ImcomeType').val('');
            $('.div-CommentDetailJob').removeAttr('style', 'display: none;');

        }

        //Shipper/Tài xế công nghệ
        else if (typeJob == 5) {
            $('#div-LuongCK').attr('style', 'display: none;');
            $('#div-LuongTM').attr('style', 'display: none;');
            $('.div-TuKinhDoanh').attr('style', 'display: none;');
            $('.div-TaiXeCongNghe').removeAttr('style', 'display: none;');
            $('#hdd_JobId').val('125');//làm tự do
            $('#hdd_ImcomeType').val('');
            $('#hdd_BusinessPapers').val('');
            $('.div-CommentDetailJob').attr('style', 'display: none;');
        }
        GetMaxPrice();
    }

    var InitTypeJob = function (typeJob) {
        //Lương chuyển khoản
        if (typeJob == 1) {
            $('#div-LuongCK').removeAttr('style', 'display: none;');
            $('#div-LuongTM').attr('style', 'display: none;');
            $('.div-TuKinhDoanh').attr('style', 'display: none;');
            $('.div-TaiXeCongNghe').attr('style', 'display: none;');
            $('#hdd_ImcomeType').val('5'); // lương chuyển khoản
            $('#hdd_JobId').val('123');//đi làm hưởng lương
            $('#hdd_BusinessPapers').val('');
            $('.div-CommentDetailJob').attr('style', 'display: none;');
        }
        //Lương tiền mặt
        else if (typeJob == 2) {
            $('#div-LuongCK').attr('style', 'display: none;');
            $('#div-LuongTM').removeAttr('style', 'display: none;');
            $('.div-TuKinhDoanh').attr('style', 'display: none;');
            $('.div-TaiXeCongNghe').attr('style', 'display: none;');
            $('#hdd_ImcomeType').val('1'); // lương tiền mặt
            $('#hdd_JobId').val('123');//đi làm hưởng lương
            $('#hdd_BusinessPapers').val('');
            $('.div-CommentDetailJob').attr('style', 'display: none;');
        }
        //Kinh doanh có ĐK
        else if (typeJob == 3) {
            $('#div-LuongCK').attr('style', 'display: none;');
            $('#div-LuongTM').attr('style', 'display: none;');
            $('.div-TuKinhDoanh').removeAttr('style', 'display: none;');
            $('.div-TaiXeCongNghe').attr('style', 'display: none;');
            $('#hdd_BusinessPapers').val('true');
            $('#hdd_JobId').val('124');//tự kinh doanh
            $('#hdd_ImcomeType').val('');
            $('.div-CommentDetailJob').removeAttr('style', 'display: none;');
        }
        //Kinh doanh không có ĐK và làm tự do
        else if (typeJob == 4) {
            $('#div-LuongCK').attr('style', 'display: none;');
            $('#div-LuongTM').attr('style', 'display: none;');
            $('.div-TuKinhDoanh').attr('style', 'display: none;');
            $('.div-TaiXeCongNghe').attr('style', 'display: none;');
            $('#hdd_BusinessPapers').val('false');
            $('#hdd_JobId').val('126');//làm tự do
            $('#hdd_ImcomeType').val('');
            $('.div-CommentDetailJob').removeAttr('style', 'display: none;');

        }

        //Shipper/Tài xế công nghệ
        else if (typeJob == 5) {
            $('#div-LuongCK').attr('style', 'display: none;');
            $('#div-LuongTM').attr('style', 'display: none;');
            $('.div-TuKinhDoanh').attr('style', 'display: none;');
            $('.div-TaiXeCongNghe').removeAttr('style', 'display: none;');
            $('#hdd_JobId').val('125');//làm tự do
            $('#hdd_ImcomeType').val('');
            $('#hdd_BusinessPapers').val('');
            $('.div-CommentDetailJob').attr('style', 'display: none;');
        }
    }

    return {
        Init: Init,
        GetDistrict: GetDistrict,
        GetWard: GetWard,
        GetProduct: GetProduct,
        GetLoanBrief: GetLoanBrief,
        InitPriceMoto: InitPriceMoto,
        GetMaxPrice: GetMaxPrice,
        CallAI: CallAI,
        SendPhoneRelationshipAI: SendPhoneRelationshipAI,
        BindToTextbox: BindToTextbox,
        InitDisplayCancel: InitDisplayCancel,
        ShowInterestTool: ShowInterestTool,
        CheckQLFTelesales: CheckQLFTelesales,
        CancelLoanBrief: CancelLoanBrief,
        SubmitForm: SubmitForm,
        SubmitCreateLoanBrief: SubmitCreateLoanBrief,
        InitTypeJob: InitTypeJob,
        InitImcomeTypeImage: InitImcomeTypeImage,
        InitShowImageAddress: InitShowImageAddress,
        ChangeDocumentRelationship: ChangeDocumentRelationship,
        InitDocmentRelationship: InitDocmentRelationship,
        InitChangeBirday: InitChangeBirday,
        CheckElectricity: CheckElectricity,
        CheckWater: CheckWater,
        ShowAndHideElectricity: ShowAndHideElectricity,
        ShowAndHideWater: ShowAndHideWater,
        HideInterestTool: HideInterestTool,
        ShowImageAddress: ShowImageAddress,
        ChangeTypeJob: ChangeTypeJob,
        ChangImcomeTypeImage: ChangImcomeTypeImage,
        ChangeInfomationHouseOwner: ChangeInfomationHouseOwner,
        InitInfomationHouseOwner: InitInfomationHouseOwner,
    };
}
