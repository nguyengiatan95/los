﻿var StatusTelesalesModule = new function () {
    var Init = function () {
        $("#sl_StatusTelesales").select2({
            placeholder: "Chọn trạng thái"
        });
        $("#sl_StatusTelesales_Detail").select2({
            placeholder: "Chọn trạng thái chi tiết"
        });
    };

    var GetDetailStatusTelesales = function (tthis) {
        if (tthis.value > 0) {
            $.ajax({
                url: '/LoanBrief/DetailStatusTelesales?parentId=' + tthis.value,
                type: 'GET',
                dataType: 'json',
                success: function (res) {
                    $('#sl_StatusTelesales_Detail').html('');
                    $('#sl_StatusTelesales_Detail').append('<option value="' + 0 + '">' + 'Chọn trạng thái chi tiết' + '</option>');
                    if (res.status == 1) {
                        for (var i = 0; i < res.data.length; i++) {
                            $('#sl_StatusTelesales_Detail').append('<option value="' + res.data[i].id + '">' + res.data[i].name + '</option>');
                        }
                    }
                },
                error: function () {
                    App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
                }
            });
        }
    }

    var SubmitForm = function () {
        var form = $('#formStatusTelesales');
        $('#btnSaveTelesalesStatus').attr('disabled', true);
        form.validate({
            ignore: [],
            rules: {
                StatusTelesales: {
                    min: 1
                },
                StatusTelesalesDetail: {
                    min: 1
                },
            },
            messages: {
                StatusTelesales: {
                    min: "Vui lòng chọn trạng thái",
                },
                StatusTelesalesDetail: {
                    min: "Vui lòng chọn chi tiết trạng thái",
                },
                
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            $('#btnSaveTelesalesStatus').attr('disabled', false);
            return;
        }
        var statusTelesales = $('#sl_StatusTelesales option:selected').text();
        var detailStatusTelesales = $('#sl_StatusTelesales_Detail option:selected').text();
        var noteStatusTelesales = $('#txt_CommentStatusTelesales').val();
        var note = "";
        if (noteStatusTelesales != '')
            note = '<b>Comment: </b>' + noteStatusTelesales;
        var commentTelesales = '<b>Trạng thái liên hệ: </b>' + statusTelesales + '<br />' + '<b>Tình trạng cuộc gọi: </b>' + detailStatusTelesales + '<br />' + note;
        $('#hdd_TelesaleComment').val(commentTelesales);
        form.ajaxSubmit({
            url: '/LoanBrief/SaveStatusTelesales',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    if (data.status == 1) {
                        $('#modalStatusTelesales').modal('hide');
                        App.ShowSuccessNotLoad(data.message);
                        App.ReloadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    return {
        Init: Init,
        GetDetailStatusTelesales: GetDetailStatusTelesales,
        SubmitForm: SubmitForm
    };
}

