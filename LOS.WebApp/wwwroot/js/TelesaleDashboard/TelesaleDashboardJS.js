﻿var TelesaleDashboardJS = new function () {

    var sec = 0
    var min = 0
    var hour = 0
    function startClock() {
        var strTimeCall = String("0" + hour).slice(-2) + ":" + String("0" + min).slice(-2) + ":" + String("0" + sec).slice(-2);
        $('#callTimer').text(strTimeCall);
        go = setTimeout("TelesaleDashboardJS.StartClock()", 1000)
        sec++
        if (sec == 60) {
            sec = 0
            min++
        }
        if (min == 60) {
            min = 0
            hour++
        }
    }

    var Init = function () {
        // init bootstrap switch
        $('#swWorking').bootstrapSwitch();
    };

    var ListScheduleToday = function () {
        var datatable = $('#dtSchedule').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/Telesales/ListScheduleToDay'
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: false,

            toolbar: {
                items: {
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    }
                }
            },
            search: {
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 35,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        if (row.utmSource != null && row.utmSource != "") {
                            return index + record + "<br /><p style='font-family: Poppins, sans-serif;font-weight: 400;font-style: normal;font-size: 11px;color: rgb(167, 171, 195);'>" + row.utmSource + "</p>";
                        } else {
                            return index + record;
                        }

                    }
                },
                {
                    field: 'loanBriefId',
                    title: 'Mã HĐ',
                    //width: 100,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = `HĐ-${row.loanBriefId}`;
                        return html;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Khách hàng',
                    //width: 150,
                    sortable: false,
                    template: function (row) {
                        var html = `${row.fullName}`;
                        html += `<span class="item-desciption">SĐT: ${ConvertPhone(row.phone)}</span>`;
                        return html;
                    }
                },
                {
                    field: 'address',
                    title: 'Quận/ Huyện',
                    sortable: false,
                    template: function (row) {
                        var text = "";
                        if (row.district != null)
                            text += "<p class='district'>" + row.district.name + "</p>";
                        if (row.province != null) {
                            text += "<p class='province'>" + row.province.name + "</p>";
                        }
                        return text;
                    }
                },
                {
                    field: 'createdTime',
                    title: 'Thời gian tạo',
                    //width: 200,
                    sortable: false,
                    template: function (row) {
                        var date = moment(row.createdTime);
                        return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
                    }
                },
                {
                    field: 'loanAmount',
                    title: 'Tiền vay',
                    sortable: false,
                    template: function (row) {
                        var html = `${App.FormatCurrency(row.loanAmount)}`;
                        if (row.loanProduct != null)
                            html += `<span class="item-desciption">${row.loanProduct.name}</span>`;
                        return html;
                    }
                },
                {
                    field: 'scheduleTime',
                    title: 'Lịch hẹn',
                    //width: 150,
                    template: function (row) {
                        var date = moment(row.scheduleTime);
                        return date.format("HH:mm") + "<br />" + date.format("DD/MM/YYYY");
                    }
                }
            ]
        });
    };

    var LoadChartToDay = function () {
        $.ajax({
            url: '/Telesales/GetDataHandelToday',
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                if (res.data != null && (res.data.totalPush > 0 || res.data.totalCancel > 0)) {
                    ChartHandle(res.data.totalPush, res.data.totalCancel);
                } else {
                    $('#loanbriefHandleChart').html("<span>Không có dữ liệu</span>");
                }
            },
            error: function () {
            }
        });
       
    };

    var ChartHandle = function (loanpush, loancancel) {
        Highcharts.chart('loanbriefHandleChart', {
            chart: {
                type: 'column',
                style: {
                    fontFamily: 'sans-serif'
                }
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            credits: {
                enabled: false
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: ''
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y} đơn</b><br/>'
            },
            exporting: {
                buttons: {
                    contextButton: {
                        enabled: false
                    }
                }
            },
            series: [
                {
                    name: "",
                    colorByPoint: true,
                    data: [
                        {
                            name: "Đơn đã đẩy",
                            y: loanpush,
                            color: '#5f7afc'
                        },
                        {
                            name: "Đơn hủy",
                            y: loancancel,
                            color: '#fb3795'
                        }
                    ]
                }
            ]
        });
    }

    return {
        Init: Init,
        ListScheduleToday: ListScheduleToday,
        LoadChartToDay: LoadChartToDay,
        StartClock: startClock

    };
};