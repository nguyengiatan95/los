﻿$(window).bind('beforeunload', function (e) {

});

$(window).bind('unload', function () {
    csUnregister();
    if (csVoice.enableVoice) {
        reConfigDeviceType();
    }
});

// kết thúc cuộc gọi ra/vào
function csEndCall() {
    console.log("Call is ended");
    $('#txtAudioCallRing')[0].pause();
    $('#btnCalling').hide();
    $('#btnEndCall').hide();
    $('#txtPhoneAccept').val('');
    $('#boxAutoLoanInfo').html('<p style="font-size: 30px;color: white;text-align: center; padding-top: 30px;">Chờ nhận cuộc gọi</p>');
    $('#callTimer').html('00:00');
    $('#callTimer').hide();
    $('#box-icon-call').hide();
      //hủy đồng hồ đếm giờ
    if (go != undefined && go != null)
        clearTimeout(go);
}

// đổ chuông trình duyệt của agent khi gọi ra
function csCallRingingAgent() {
    console.log("Has a new call to agent");
}

// đổ chuông trình duyệt của agent khi gọi vào
// đổ chuông tới khách hàng khi gọi ra
function csCallRinging(phone) {
    console.log("Has a new call to customer: " + phone);
    //console.log("Has a new call to customer new: " + phoneNumber);
    $('#txtAudioCallRing')[0].play();
    $('#btnCalling').show();
    $('#btnEndCall').hide();
    $('#txtPhoneAccept').val(phone);
    $('#callTimer').show();
    LoadLoanInfo(phone);
}

// cuộc gọi vào được agent trả lời
function csAcceptCall() {
    $('#txtAudioCallRing')[0].pause();
    console.log("Call is Accepted");
    $('#btnCalling').hide();
    $('#btnEndCall').show();
   
    var phone = csVoice.callInfo.caller;
    //var phone = $('#txtPhoneAccept').val();
    if (phone != "")
        LoadScript(phone);
    TelesaleDashboardJS.StartClock();
}

// cuộc gọi ra được khách hàng trả lời
function csCustomerAccept() {
    console.log("csCustomerAccept");
    // document.getElementById('phoneNo').innerHTML = "Đang trả lời";
}

function csMuteCall() {
    console.log("Call is muted");
}

function csUnMuteCall() {
    console.log("Call is unmuted");
}

function csHoldCall() {
    console.log("Call is holded");
}

function csUnHoldCall() {
    console.log("Call is unholded");
}

function showCalloutInfo(number) {
    console.log("callout to " + number);
}

function showCalloutError(errorCode, sipCode) {
    console.log("callout error " + errorCode + " - " + sipCode);
}

function csShowEnableVoice(enableVoice) {
    //if (enableVoice) {
    //    $('#enable').attr('disabled', 'disabled');
    //} else {
    //    $('#enable').removeAttr('disabled');
    //}
}

function csShowDeviceType(type) {
    console.log(type);
    console.log("csShowDeviceType");
}

function csShowCallStatus(status) {
    console.log(status);
    console.log("csShowCallStatus");
}

function csInitComplete() {
    if (!csVoice.enableVoice) {
        csEnableCall();
    }
    console.log("csInitComplete");
    if (csVoice.callStatus != 'Online') {
        //hiển thị nút offline
        $('#swWorking').bootstrapSwitch('state', false);
    } else {
        //hiển thị online
        $('#swWorking').bootstrapSwitch('state', true);
    }

    $('#swWorking').on('switchChange.bootstrapSwitch', function (event, state) {
        var State = state == true ? "1" : "0";
        changeCallStatus();
        $.ajax({
            url: '/Telesales/LogWorkingUser',
            type: 'POST',
            data: {
                'state': State
            },
            dataType: 'json',
            success: function (res) {
            }
        });
    });
}

function csCurrentCallId(callId) {
    console.log("csCurrentCallId: " + callId);
}

function csInitError(error) {
    console.log("csInitError: " + error);
}

function csListTransferAgent(listTransferAgent) {
    console.log(listTransferAgent);
}
function csTransferCallError(error, tranferedAgentInfo) {
    console.log('Transfer call failed,' + error);
}
function csTransferCallSuccess(tranferedAgentInfo) {
    console.log('transfer call success');
}
function csNewCallTransferRequest(transferCall) {
    console.log('new call transfer');
    console.log(transferCall);
  
}

function csTransferCallResponse(status) {
  
    console.log(status);
}

function LoadScript(phone) {
    $('#ScriptTelesaleBody').html('');
    $.ajax({
        url: '/LoanBrief/AcceptCall',
        type: 'GET',
        data: { 'phone': phone },
        dataType: 'json',
        success: function (res) {
            if (res.isSuccess == 1) {
                $('#ScriptTelesaleBody').html(res.html);
                $('#modelScriptTelesales').modal('show');
            } else {
                App.ShowErrorNotLoad(res.message);
            }
        },
        error: function () {

        }
    });
}

function LoadLoanInfo(phone) {
    $.ajax({
        url: '/Telesales/LoanBriefInfo',
        type: 'GET',
        data: { 'phone': phone },
        dataType: 'text',
        success: function (response) {
            $('#boxAutoLoanInfo').html(response);
            if (csVoice.callInfo != null) {
                $('#box-icon-call').show();
                if (csVoice.callInfo.callDirection == 1) {
                    $('#box-icon-call').html('<i class="fa fa-angle-double-right icon-call"></i>');
                } else {
                    $('#box-icon-call').html('<i class="fa fa-angle-double-left icon-call"></i>');
                }
            }           
        },
        error: function () {
        }
    });
}

function EditInfomation() {
    var phone = $('#txtPhoneAccept').val();
    if (phone != "")
        LoadScript(phone);
}
