﻿
var TelesaleManageModule = new function () {

    var InitDataTable = function (objQuery) {
        var datatable = $('#tbManageTelesale').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/Telesales/LoadDataStaffTelesale',
                        params: {
                            query: objQuery
                        },
                    }
                },
                pageSize: 50,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'check',
                    title: '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input onclick="TelesaleManageModule.CheckAll()" id="checkAll" type="checkbox"><span></span></label></span>',
                    width: 25,
                    textAlign: 'center',
                    sortable: false,
                    //visible: true,
                    template: function (row) {
                        var html = '<span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" value="' + row.userId + '" name="checkbox"><span></span></label></span>';
                        return html;
                    }
                },
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'username',
                    title: 'Tài khoản',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var userName = '<span>' + row.username + '</span><br />';
                        if (row.telesaleRecievedLoan == true) {
                            userName += '<span style="font-size: 10px;" class="m-badge m-badge--primary m-badge--wide">Nhận đơn</span><br />'
                        }
                        else if (row.telesaleRecievedLoan == false) {
                            userName += '<span style="font-size: 10px;" class="m-badge m-badge--danger m-badge--wide">Không nhận đơn</span><br />'
                        }
                        if (row.typeRecievedProduct == 1) {
                            userName += '<span style="font-size: 11px;color: #ea4e0c;">Sản phẩm nhỏ</span>'
                        }
                        else if (row.typeRecievedProduct == 2) {
                            userName += '<span style="font-size: 11px;color: #ea4e0c;">Sản phẩm lớn</span>'
                        }
                        else if (row.typeRecievedProduct == 3) {
                            userName += '<span style="font-size: 11px;color: #ea4e0c;">Tất cả</span>'
                        }
                        return userName;
                    }
                },
                {
                    field: 'team',
                    title: 'Team',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var teamName = '';
                        if (row.teamTelesalesId == TeamTelesales.HN1)
                            teamName = 'Hà nội 1';
                        else if (row.teamTelesalesId == TeamTelesales.HN2)
                            teamName = 'Hà nội 2';
                        else if (row.teamTelesalesId == TeamTelesales.HCM)
                            teamName = 'HCM';
                        else if (row.teamTelesalesId == TeamTelesales.Other)
                            teamName = 'Other';
                        else if (row.teamTelesalesId == TeamTelesales.SupportTeam)
                            teamName = 'Support Team';
                        else if (row.teamTelesalesId == TeamTelesales.TopupXsell)
                            teamName = 'Topup/Xsell';
                        else if (row.teamTelesalesId == TeamTelesales.HN3)
                            teamName = 'Hà nội 3';
                        else if (row.teamTelesalesId == TeamTelesales.Xsell1)
                            teamName = 'Xsell 1';
                        else if (row.teamTelesalesId == TeamTelesales.Xsell2)
                            teamName = 'Xsell 2';
                        else if (row.teamTelesalesId == TeamTelesales.Xsell3)
                            teamName = 'Xsell 3';
                        var team = '<span class="m-badge m-badge--success m-badge--wide">' + teamName + '</span> <br />';
                        if (row.valueTelesalesShift != undefined || row.valueTelesalesShift != null) {
                            if (row.listTelesalesShift != null && row.listTelesalesShift.length > 0) {
                                for (var i = 0; i < row.listTelesalesShift.length; i++) {
                                    var checkValueTelesalesShift = (row.valueTelesalesShift & row.listTelesalesShift[i].telesalesShiftId)
                                    if (checkValueTelesalesShift == row.listTelesalesShift[i].telesalesShiftId) {
                                        team += '<span style="font-size: 12px;">' + row.listTelesalesShift[i].shift + '(' + row.listTelesalesShift[i].time + ')' + '</span><br />'
                                    }
                                }
                            }
                        }
                        return team;
                    }
                },
                {
                    field: 'fullName',
                    title: 'Họ tên',
                    sortable: false
                },
                {
                    field: 'phone',
                    title: 'Số điện thoại',
                    textAlign: 'left',
                    sortable: false
                },
                {
                    field: 'email',
                    title: 'Email',
                    sortable: false
                },
                {
                    field: 'cityName',
                    title: 'Khu vực',
                    sortable: false,
                    template: function (row) {
                        if (row.city != null) {
                            return '<span>' + row.city.name != null ? row.city.name : row.city.name != "" ? row.city.name : "" + '</span>'
                        }
                        else {
                            return '';
                        }
                    }
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    sortable: false,
                    template: function (row) {
                        if (row.status == null)
                            row.status = 0;
                        var status = {
                            0: { 'title': 'Khóa', 'class': 'm-badge--metal' },
                            1: { 'title': 'Đang hoạt động', 'class': 'm-badge--success' }
                        };
                        return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
                    }
                },
                {
                    field: 'Action',
                    title: 'Action',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var meta = '<a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" title="Sửa nhân viên"  onclick="TelesaleManageModule.loadInforStaffTeleSale(\'' + row.userId + '\',\'' + row.username + '\')" >\
                                        <i class="la la-edit"></i></a>\
	                                <a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" title="Reset mật khẩu"  onclick="TelesaleManageModule.resetPassword(\''+ row.userId + '\',\'' + row.username + '\')" >\
                                        <i class="la la-refresh"></i></a>';
                        if (row.telesaleRecievedLoan == true) {
                            meta += '<a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" title="Hủy nhận đơn" onclick="TelesaleManageModule.updateTelesalerecievedloan(\'' + row.userId + '\',\'' + row.username + '\', 0)" >\
                                        <i class="fa fa-lock"></i></a>';
                        }
                        else {
                            meta += '<a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" title="Kích hoạt nhận đơn" onclick="TelesaleManageModule.updateTelesalerecievedloan(\'' + row.userId + '\',\'' + row.username + '\', 1)" >\
                                        <i class="fa fa-unlock"></i></a>';
                        }

                        return meta;
                    }
                }
            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            status: $('#filterStatus').val(),
            filterName: $('#filterSearchName').val().trim(),
            team: $('#filterTeam').val(),
            typeRecievedProduct: $('#filterTypeRecievedProduct').val(),
            telesaleRecievedLoan: $('#filterTelesaleRecievedLoan').val()
        }
        InitDataTable(objQuery);
    }

    var Init = function () {
        $("#filterStatus").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#filterTypeRecievedProduct").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#filterTeam").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#ddl_TeamTelesales").select2({
            placeholder: "Vui lòng chọn Team"
        });

        $("#ddl_create_staff_typeCallService").select2({
            placeholder: "Vui lòng chọn tổng đài"
        });

        $("#ddl_create_staff_cityId").select2({
            placeholder: "Vui lòng chọn khu vực"
        });

        $("#ddl_create_staff_typeRecievedProduct").select2({
            placeholder: "Vui lòng chọn gói sản phẩm"
        });


        LoadData();
        $('#filterStatus').on('change', function () {
            LoadData();
        });
        $('#filterCityId').on('change', function () {
            LoadData();
        });
        $('#filterTypeRecievedProduct').on('change', function () {
            LoadData();
        });
        $('#filterTelesaleRecievedLoan').on('change', function () {
            LoadData();
        });
        $('#filterTeam').on('change', function () {
            LoadData();
        });
        $("#filterSearchName").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });
    };

    var loadInforStaffTeleSale = function (userId, userName) {
        var form = $('#form_create_staff_telesale').closest('form');
        var validator = form.validate();
        validator.destroy();
        $('#hdd_create_userID').val(userId);
        $('#txt_create_staff_username').val('');
        $('#txt_create_staff_fullname').val('');
        $('#txt_create_staff_phone').val('');
        $('#txt_create_staff_email').val('');
        $('#txt_create_staff_ipphone').val('');
        $('#ddl_create_staff_typeCallService').val(0).change();
        $('#txt_create_staff_cisco_username').val('');
        $('#txt_create_staff_cisco_password').val('');
        $('#txt_create_staff_cisco_extension').val('');
        $('#ddl_create_staff_cityId').val(0).change();
        $('#ddl_create_staff_typeRecievedProduct').val(0).change();
        $('#ddl_TeamTelesales').val(0).change();

        if (userId == 0) {
            $('#header_create_staff_telesale').html('Thêm mới nhân viên');
            $('#btn_save_staff_telesalse').html('<i class="la la-save"></i>Thêm mới');
            $('.div-password-account').show();
            $('#ddl_ValueTelesalesShift option:selected').removeAttr('selected', 'selected')
            $('#ddl_ValueTelesalesShift').select2();
        } else {
            $('#header_create_staff_telesale').html('Cập nhật tài khoản <b>' + userName + '</b>');
            $('#btn_save_staff_telesalse').html('<i class="la la-save"></i>Cập nhật');
            $('.div-password-account').attr('style', 'display: none;');
            $.ajax({
                type: "Get",
                url: "/Users/GetUserById",
                data: { Id: userId },
                success: function (data) {
                    if (data.status == 1) {
                        //thành công
                        $('#txt_create_staff_username').val(data.data.username);
                        $('#txt_create_staff_fullname').val(data.data.fullName);
                        $('#txt_create_staff_phone').val(data.data.phone);
                        $('#txt_create_staff_email').val(data.data.email);
                        $('#txt_create_staff_ipphone').val(data.data.ipphone);
                        $('#ddl_create_staff_typeCallService').val(data.data.typeCallService).change();
                        $('#txt_create_staff_cisco_username').val(data.data.ciscoUsername);
                        $('#txt_create_staff_cisco_password').val(data.data.ciscoPassword);
                        $('#txt_create_staff_cisco_extension').val(data.data.ciscoExtension);
                        $("input[name=Status][value=" + data.data.status + "]").prop('checked', true).change();
                        $('#ddl_create_staff_cityId').val(data.data.cityId).change();
                        $('#ddl_TeamTelesales').val(data.data.teamTelesalesId).change();
                        $('#ddl_create_staff_typeRecievedProduct').val(data.data.typeRecievedProduct).change();

                        if (data.data.telesaleRecievedLoan == true) {
                            document.getElementById("cb_TelesaleRecievedLoan").checked = true;
                        }
                        if (data.data.valueTelesalesShift != null && data.data.valueTelesalesShift > 0) {
                            var appenSlTelesalesShift = '';
                            if (data.data.listTelesalesShift != null && data.data.listTelesalesShift.length > 0) {
                                for (var i = 0; i < data.data.listTelesalesShift.length; i++) {
                                    var checkValueTelesalesShift = (data.data.valueTelesalesShift & data.data.listTelesalesShift[i].telesalesShiftId)
                                    if (checkValueTelesalesShift == data.data.listTelesalesShift[i].telesalesShiftId) {
                                        appenSlTelesalesShift += '<option selected="selected" value="' + data.data.listTelesalesShift[i].telesalesShiftId + '">' + data.data.listTelesalesShift[i].shift + '(' + data.data.listTelesalesShift[i].time + ')</option>';
                                    }
                                    else {
                                        appenSlTelesalesShift += '<option value="' + data.data.listTelesalesShift[i].telesalesShiftId + '">' + data.data.listTelesalesShift[i].shift + '(' + data.data.listTelesalesShift[i].time + ')</option>';
                                    }
                                }
                                $('#ddl_ValueTelesalesShift').html('');
                                $('#ddl_ValueTelesalesShift').html(appenSlTelesalesShift).select2();
                            }
                        }
                        else {
                            $('#ddl_ValueTelesalesShift option:selected').removeAttr('selected', 'selected')
                            $('#ddl_ValueTelesalesShift').select2();
                        }
                    }
                    else {
                        //lỗi
                        App.ShowError(data.message);
                    }
                },
                traditional: true
            });
        }
        $('#modal_create_staff_telesale').modal('show');
    };

    var AddOrUpdateUser = function (btn, e) {
        var userId = $('#hdd_create_userID').val();
        if ($("input[name='cb_TelesaleRecievedLoan']").is(":checked") == true)
            $('#hdd_TelesaleRecievedLoan').val(true);

        e.preventDefault();
        var form = $(btn).closest('form');
        if (userId > 0) {
            form.validate({
                ignore: [],
                rules: {
                    Username: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                    Phone: {
                        validPhone: true
                    },
                    Email: {
                        required: true,
                        validEmail: true
                    },
                    sl_ValueTelesalesShift: {
                        required: true,
                    },
                    TypeRecievedProduct: {
                        required: true,
                    }
                },
                messages: {
                    Username: {
                        required: "Bạn chưa nhập tài khoản",
                        class: "has-danger"
                    },
                    Phone: {
                        validPhone: "Số điện thoại không hợp lệ",
                    },
                    Email: {
                        required: "Bạn chưa nhập email",
                        validEmail: "Email không hợp lệ",
                    },
                    sl_ValueTelesalesShift: {
                        required: "Bạn phải chọn ca làm việc"
                    },
                    TypeRecievedProduct: {
                        required: "Bạn phải chọn loại sản phẩm",
                    }
                }
            });
        }
        else {
            form.validate({
                ignore: [],
                rules: {
                    Username: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                    Password: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        },
                        minlength: 6
                    },
                    Phone: {
                        validPhone: true
                    },
                    Email: {
                        required: true,
                        validEmail: true
                    },
                    sl_ValueTelesalesShift: {
                        required: true,
                    },
                    TypeRecievedProduct: {
                        required: true,
                    }
                },
                messages: {
                    Username: {
                        required: "Bạn chưa nhập tài khoản",
                        class: "has-danger"
                    },
                    Password: {
                        required: "Bạn chưa nhập mật khẩu",
                        minlength: "Mật khẩu phải chứa ít nhất 6 ký tự"
                    },
                    Phone: {
                        validPhone: "Số điện thoại không hợp lệ",
                    },
                    Email: {
                        required: "Bạn chưa nhập email",
                        validEmail: "Email không hợp lệ",
                    },
                    sl_ValueTelesalesShift: {
                        required: "Bạn chưa chọn ca làm việc"
                    },
                    TypeRecievedProduct: {
                        required: "Bạn phải chọn loại sản phẩm",
                    }
                },
                invalidHandler: function (e, validation) {
                    if (validation.errorList != null && validation.errorList.length > 0) {
                        App.ShowErrorNotLoad(validation.errorList[0].message)
                    }
                }
            });
        }
        if (!form.valid()) {
            return;
        }
        if (document.getElementById("cb_TelesaleRecievedLoan").checked == true) {
            var typeRecievedProduct = $('#ddl_create_staff_typeRecievedProduct').val();
            if (typeRecievedProduct == 0) {
                App.ShowErrorNotLoad('Bạn phải chọn sản phẩm !!!');
                return false;
            }
        }


        var arrValueTelesalesShift = new Array();
        arrValueTelesalesShift = $('#ddl_ValueTelesalesShift').val();
        if (arrValueTelesalesShift.length > 0) {
            var shift1 = arrValueTelesalesShift[0] == undefined ? 0 : parseInt(arrValueTelesalesShift[0]);
            var shift2 = arrValueTelesalesShift[1] == undefined ? 0 : parseInt(arrValueTelesalesShift[1]);
            var shift3 = arrValueTelesalesShift[2] == undefined ? 0 : parseInt(arrValueTelesalesShift[2]);
            var countShift = shift1 + shift2 + shift3;
            $('#hdd_ValueTelesalesShift').val(countShift);
        }
        form.ajaxSubmit({
            url: '/Users/AddOrUpdateUser',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccessNotLoad(data.message);
                        $('#modal_create_staff_telesale').modal('hide');
                        LoadData();
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };

    var resetPassword = function (userId, userName) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn  đặt lại mật khẩu : <b>" + userName + "</b> về mặc định <br /> <span style='font-size: 12px; color: #909090;'>(Mật khẩu mặc định: Tima@2021)</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Users/ResetPassword",
                    data: { Id: userId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                        } else {
                            App.ShowError(data.message);
                        }
                    },
                    traditional: true
                });
            }
        });
    }

    var ChangeLook = function (telesaleRecievedLoan) {
        var favorite = [];
        $.each($("input[name='checkbox']:checked"), function () {
            favorite.push($(this).val());
        });
        if (favorite == '') {
            App.ShowErrorNotLoad('Bạn phải chọn nhân viên muốn chuyển trạng thái cuộ gọi');
            return;
        }
        $.ajax({
            type: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            url: "/Telesales/ChangeLook",
            data: { ArrId: favorite, TelesaleRecievedLoan: telesaleRecievedLoan },
            success: function (data) {
                $.unblockUI();
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    LoadData();
                } else {
                    App.ShowErrorNotLoad(data.message);
                    return;
                }
            }
        });

    };

    var updateTelesalerecievedloan = function (userId, userName, telesalerecievedloan) {
        $.ajax({
            type: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            url: "/Telesales/UpdateTelesaleRecievedLoan",
            data: { Id: userId, TelesaleRecievedLoan: telesalerecievedloan },
            success: function (data) {
                $.unblockUI();
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    LoadData();
                } else {
                    App.ShowError(data.message);
                }
            },
            traditional: true
        });
    }

    var CheckAll = function () {
        if ($("#checkAll").is(':checked')) {
            $("#tbManageTelesale input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $("#tbManageTelesale input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
    };

    return {
        Init: Init,
        LoadData: LoadData,
        loadInforStaffTeleSale: loadInforStaffTeleSale,
        AddOrUpdateUser: AddOrUpdateUser,
        resetPassword: resetPassword,
        updateTelesalerecievedloan: updateTelesalerecievedloan,
        ChangeLook: ChangeLook,
        CheckAll: CheckAll
    };
}