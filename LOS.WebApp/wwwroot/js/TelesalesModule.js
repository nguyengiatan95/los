﻿var TelesalesModule = new function () {

	var missCallOptions;
	var scheduleCallOptions;
	var calledOptions;

    var Init = function () {

		// init table

		missCallOptions = {
			// datasource definition
			data: {
				type: 'remote',
				source: {
					read: {						
						method: 'POST',
						url: '/Telesales/GetMissedCalls'
					},
				},
				pageSize: 5,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			},			
			// layout definition
			layout: {
				scroll: false,
				footer: false
			},
			// column sorting
			sortable: true,
			pagination: true,

			toolbar: {
				// toolbar items
				items: {
					// pagination
					pagination: {
						// page size select
						pageSizeSelect: [5, 10, 20, 30, 50, 100],
					},
				},
			},
			search: {
				//input: $('#filterName'),
			},
			columns: [
				{
					field: 'stt',
					title: 'STT',
					width: 50,
					textAlign: 'center',
					sortable: false,
					template: function (row, index, datatable) {
						var pageIndex = datatable.API.params.pagination.page;
						var record = 1;
						if (pageIndex > 1) {
							var pageSize = datatable.API.params.pagination.perpage;
							record = (pageIndex - 1) * pageSize + 1;
						}
						return index + record;
					}
				},
				{
					field: 'loanBriefId',
					title: 'Mã đơn vay',
					sortable: false,
					template: function (row) {
						return 'ĐV-' + row.loanBriefId						
					}
				},
				{
					field: 'fullName',
					title: 'Khách hàng',
					sortable: false,
					template: function (row) {	
						return row.fullName + "<br />" + row.phone;						
					}
				},
				{
					field: 'districtId',
					title: 'Quận/Huyện',
					sortable: false,
					template: function (row) {
						var text = "";
						if (row.district != null)
							text = row.district.name;						
						if (text == "") {
							if (row.province != null)
								text = text + " - " + row.province.name;
						}
						else {
							if (row.province != null)
								text = row.province.name;
						}
						return text;
					}
				},
				{
					field: 'createdTime',
					title: 'Thời gian tạo',
					sortable: false,
					template: function (row) {
						var date = moment(row.createdTime);		
						return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
					}
				},				
				{
					field: 'loanAmount',
					title: 'Tiền vay',
					sortable: false,
					template: function (row) {
						return App.FormatCurrency(row.loanAmount);
					}
				},	
				{
					field: 'loanTime',
					title: 'Thời gian vay',
					sortable: false,
					template: function (row) {
						return row.loanTime + " tháng";
					}
				},
				{
					field: 'startTime',
					title: 'Thời gian chuyển đơn',
					sortable: false,
					template: function (row) {
						var date = moment(row.startTime);
						return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
					}
				},
				{
					field: 'Action',
					title: 'Hành động',
					sortable: false,
					width: 250,
					template: function (row) {
						var html = '';
						html = '<div class="row align-items-center">'
						html += '<div class="col-lg-6">';
						html += '	<select id="slReason_' + row.loanActionId + '">';
						html += '		<option value="0">Chọn lý do</option>';								
						html += '		<option value="7">Bận việc khác</option>';								
						html += '		<option value="12">Đi ra ngoài</option>';		
						html += '		<option value="13">Lý do khác</option>';				
						html += '	</select>';						
						html += '</div>';
						html += '<div class="col-lg-6">';
						html += '	<button type="button" class="btn btn-primary btn-sm m-btn m-btn--custom" onclick="TelesalesModule.ConfirmReason(' + row.loanActionId + ')">';
						html += '		Xác nhận';
						html += '	</button>'
						html += '</div>';
						html += '</div>';
						return $(html);
					}
				}
			]
		};

		$('#dtMissCall').mDatatable(missCallOptions);

		scheduleCallOptions = {
			// datasource definition
			data: {
				type: 'remote',
				source: {
					read: {
						method: 'POST',
						url: '/Telesales/GetScheduledCalls'
					},
				},
				pageSize: 5,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			},
			// layout definition
			layout: {
				scroll: false,
				footer: false
			},
			// column sorting
			sortable: true,
			pagination: true,

			toolbar: {
				// toolbar items
				items: {
					// pagination
					pagination: {
						// page size select
						pageSizeSelect: [5, 10, 20, 30, 50, 100],
					},
				},
			},
			search: {
				//input: $('#filterName'),
			},
			columns: [
				{
					field: 'stt',
					title: 'STT',
					width: 50,
					textAlign: 'center',
					sortable: false,
					template: function (row, index, datatable) {
						var pageIndex = datatable.API.params.pagination.page;
						var record = 1;
						if (pageIndex > 1) {
							var pageSize = datatable.API.params.pagination.perpage;
							record = (pageIndex - 1) * pageSize + 1;
						}
						return index + record;
					}
				},
				{
					field: 'loanBriefId',
					title: 'Mã đơn vay',
					sortable: false,
					template: function (row) {
						return 'ĐV-' + row.loanBriefId
					}
				},
				{
					field: 'fullName',
					title: 'Khách hàng',
					sortable: false,
					template: function (row) {
						return row.fullName + "<br />" + row.phone;
					}
				},
				{
					field: 'districtId',
					title: 'Quận/Huyện',
					sortable: false,
					template: function (row) {
						var text = "";
						if (row.district != null)
							text = row.district.name;
						if (text == "") {
							if (row.province != null)
								text = text + " - " + row.province.name;
						}
						else {
							if (row.province != null)
								text = row.province.name;
						}
						return text;
					}
				},
				{
					field: 'createdTime',
					title: 'Thời gian tạo',
					sortable: false,
					template: function (row) {
						var date = moment(row.createdTime);
						return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
					}
				},
				{
					field: 'loanAmount',
					title: 'Tiền vay',
					sortable: false,
					template: function (row) {
						return App.FormatCurrency(row.loanAmount);
					}
				},
				{
					field: 'loanTime',
					title: 'Thời gian vay',
					sortable: false,
					template: function (row) {
						return row.loanTime + " tháng";
					}
				},
				{
					field: 'scheduleTime',
					title: 'Thời gian hẹn',
					sortable: false,
					template: function (row) {
						var date = moment(row.scheduleTime);
						return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
					}
				},
				{
					field: 'scheduleStatus',
					title: 'Trạng thái',
					sortable: false,
					template: function (row) {
						if (row.scheduleStatus == 0) {
							return "Đã lên lịch"
						}
						else {
							return "Đã chuyển cuộc gọi"
						}
					}
				}
			]
		};

		$('#dtSchedule').mDatatable(scheduleCallOptions);		

		calledOptions = {
			// datasource definition
			data: {
				type: 'remote',
				source: {
					read: {
						method: 'POST',
						url: '/Telesales/GetCalled'
					},
				},
				pageSize: 5,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			},
			// layout definition
			layout: {
				scroll: false,
				footer: false
			},
			// column sorting
			sortable: true,
			pagination: true,

			toolbar: {
				// toolbar items
				items: {
					// pagination
					pagination: {
						// page size select
						pageSizeSelect: [5, 10, 20, 30, 50, 100],
					},
				},
			},
			search: {
				//input: $('#filterName'),
			},
			columns: [
				{
					field: 'stt',
					title: 'STT',
					width: 50,
					textAlign: 'center',
					sortable: false,
					template: function (row, index, datatable) {
						var pageIndex = datatable.API.params.pagination.page;
						var record = 1;
						if (pageIndex > 1) {
							var pageSize = datatable.API.params.pagination.perpage;
							record = (pageIndex - 1) * pageSize + 1;
						}
						return index + record;
					}
				},
				{
					field: 'loanBriefId',
					title: 'Mã đơn vay',
					sortable: false,
					template: function (row) {
						return 'ĐV-' + row.loanBriefId
					}
				},
				{
					field: 'fullName',
					title: 'Khách hàng',
					sortable: false,
					template: function (row) {
						return row.fullName + "<br />" + row.phone;
					}
				},
				{
					field: 'districtId',
					title: 'Quận/Huyện',
					sortable: false,
					template: function (row) {
						var text = "";
						if (row.district != null)
							text = row.district.name;
						if (text == "") {
							if (row.province != null)
								text = text + " - " + row.province.name;
						}
						else {
							if (row.province != null)
								text = row.province.name;
						}
						return text;
					}
				},
				{
					field: 'createdTime',
					title: 'Thời gian tạo',
					sortable: false,
					template: function (row) {
						var date = moment(row.createdTime);
						return date.format("DD/MM/YYYY") + "<br />" + date.format("HH:mm");
					}
				},
				{
					field: 'loanAmount',
					title: 'Tiền vay',
					sortable: false,
					template: function (row) {
						return App.FormatCurrency(row.loanAmount);
					}
				},
				{
					field: 'loanTime',
					title: 'Thời gian vay',
					sortable: false,
					template: function (row) {
						return row.loanTime + " tháng";
					}
				},
				{
					field: 'loanTime',
					title: 'Thời gian vay',
					sortable: false,
					template: function (row) {
						return row.loanTime + " tháng";
					}
				},
				{
					field: 'loanAction.actionStatusText',
					title: 'Lịch sử cuộc gọi',
					sortable: false					
				},
				{
					field: 'Action',
					title: 'Action',
					sortable: false,
					width: 100,
					textAlign: 'center',
					template: function (row) {
						var html = '';
						if (row.status == 16) {
							html = '<a href="javascript:;" class="btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" onclick="TelesalesModule.Bound(' + row.loanBriefId + ')" >\
                                <i class="la la-star bound" id="bound_loanBrief_'+ row.loanBriefId + '"></i></a>';
						}
						else {
							html = '<a href="javascript:;" class="btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" onclick="TelesalesModule.Bound(' + row.loanBriefId + ')" >\
                                <i class="la la-star" id="bound_loanBrief_'+ row.loanBriefId + '"></i></a>';
						}
						html += '<a href="javascript:;" class="btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" onclick="TelesalesModule.EditInfo(' + row.loanBriefId +')" >\
                                <i class="la la-pencil-square"></i></a>';
						return html;
					}
				}
			]
		};

		$('#dtCalled').mDatatable(calledOptions);

		// enable clear button 
		$('#m_datepicker_1, #m_datepicker_1_validate').datepicker({
			todayBtn: "linked",
			clearBtn: true,
			todayHighlight: true,
			orientation: "bottom left",			
			format: 'dd/mm/yyyy',
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>'
			}
		});

		// enable clear button for modal demo
		$('#m_datepicker_1_modal').datepicker({
			todayBtn: "linked",
			clearBtn: true,
			todayHighlight: true,
			orientation: "bottom left",			
			format: 'dd/mm/yyyy',
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>'
			}
		});


		// enable clear button 
		$('#m_datepicker_2, #m_datepicker_2_validate').datepicker({
			todayBtn: "linked",
			clearBtn: true,
			todayHighlight: true,
			orientation: "bottom left",		
			format: 'dd/mm/yyyy',
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>'
			}
		});

		// enable clear button for modal demo
		$('#m_datepicker_2_modal').datepicker({
			todayBtn: "linked",
			clearBtn: true,
			todayHighlight: true,
			orientation: "bottom left",		
			format: 'dd/mm/yyyy',
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>'
			}
		});

		// init chart
		InitCalledChart();
		InitProductChart();
		InitProcessedChart();

		// init bootstrap switch		
		$('[data-switch=true]').bootstrapSwitch();

		// signalR init
		var connection = new signalR.HubConnectionBuilder()
			.withUrl("/telesaleHub")			
			.build();
		
		connection.start().then(function () {
			console.log("SignalR started..")
			connection.invoke("joinGroup", groupName);			
		}).catch(function (err) {
			return console.error(err.toString());
		});

		connection.on("Send", function (message) {	
			console.log(message);
		});		

		connection.on("LoanBriefReceived", function (loan) {	
			// change value
			var objLoan = JSON.parse(loan);
			console.log(objLoan);
			$("#loanName").html(objLoan.fullName);
			$("#loanPhone").html(objLoan.phone);
			if (objLoan.district != null && objLoan.province != null)
				$("#loanAddress").html(objLoan.district.name + " - " + objLoan.province.name)
			$("#loanId").html("HĐ-" + objLoan.loanBriefId);
			$("#loanShop").html("Credit");
			$("#loanAmount").html(App.FormatCurrency(objLoan.loanAmount));
			if (objLoan.product != null)
				$("#loanProduct").html(objLoan.product.name);	
			if (objLoan.loanAction != null)
				$("#lastActionStatusText").html(objLoan.loanAction.actionStatusText);
			else
				$("#lastActionStatusText").html("Chưa có lịch sử gọi");
			$("#loanBriefId").val(objLoan.loanBriefId);
			$("#loanActionId").val(objLoan.loanActionId);
			$("#LoanBriefContainer").show();
			$("#NoLoanBriefContainer").hide();
			// show call button
			$("#btnCall").show();
		});

		connection.on("LoanBriefRetrieved", function (loan) {
			// change value
			var objLoan = JSON.parse(loan);
			console.log(objLoan);
			// retrive loan
			$("#btnCalling").hide();
			$("#btnCall").hide();
			$("#btnEndCall").hide();
			$("#btnEnd").hide();
			$("#LoanBriefContainer").hide();
			$("#NoLoanBriefContainer").show();
			$("#loanName").html("");
			$("#loanPhone").html("");			
			$("#loanAddress").html("")
			$("#loanId").html("");
			$("#loanShop").html("Credit");
			$("#loanAmount").html("");			
			$("#loanProduct").html("");
			$("#lastActionStatusText").html("");
			$("#loanBriefId").val("");
			$("#loanActionId").val("");
			// reload datatables
			// $(".m-datatable").mDatatable('reload');				
			$('#dtMissCall').mDatatable(missCallOptions);
		});

		

		$('.m_selectpicker').selectpicker();
	};

	var InitCalledChart = function () {
		var data = [];
		var series = Math.floor(Math.random() * 10) + 1;
		series = series < 5 ? 5 : series;

		for (var i = 0; i < series; i++) {
			data[i] = {
				label: "Series" + (i + 1),
				data: Math.floor(Math.random() * 100) + 1
			};
		}

		$.plot($("#fcCalled"), data, {
			series: {
				pie: {
					show: true
				}
			}
		});
	}

	var InitProductChart = function () {
		var data = [];
		var series = Math.floor(Math.random() * 10) + 1;
		series = series < 5 ? 5 : series;

		for (var i = 0; i < series; i++) {
			data[i] = {
				label: "Series" + (i + 1),
				data: Math.floor(Math.random() * 100) + 1
			};
		}

		$.plot($("#fcProductTypeChart"), data, {
			series: {
				pie: {
					show: true
				}
			}
		});
	}

	var InitProcessedChart = function () {
		var data = [];
		var series = Math.floor(Math.random() * 10) + 1;
		series = series < 5 ? 5 : series;

		for (var i = 0; i < series; i++) {
			data[i] = {
				label: "Series" + (i + 1),
				data: Math.floor(Math.random() * 100) + 1
			};
		}

		$.plot($("#fcProcessedChart"), data, {
			series: {
				bar: {
					show: true
				}
			}
		});
	}

	var TriggerOnline = function () {		
		var state = $('#swWorking').bootstrapSwitch('state');
		App.Ajax("GET", "/Telesales/TriggerOnline?state=" + state, null).done(function (data) {						
			$("#onlineTime").html(Math.ceil(data.onlineTime / 60) + " phút");
			$("#restTime").html(Math.ceil(data.restTime / 60) + " phút");
			$("#workingTime").html(Math.ceil(data.workingTime / 60) + " phút");
		});
	}	
	var seconds = 0;
	var StartCall = function () {
		// show calling buttno
		$("#btnCalling").show();
		$("#btnCall").hide();
		$("#btnEndCall").hide();
		$("#btnEnd").hide();
		// update call status
		UpdateCallStatus(3); // 3 >> Calling
		// Trigger when call success - just test
		seconds = 0;
		setTimeout(function () {
			Calling();
		}, 5 * 1000);
	}

	var callingInterval;

	var Calling = function () {
		// set timer	
		callingInterval =  setInterval(callingTimer, 1000);
	}

	var callingTimer = function () {		
		timerTick();	
	}

	var EndCall = function () {
		clearInterval(callingInterval);
		callingInterval = null;
		$("#btnEndCall").hide();
		$("#btnEnd").show();
		// update call status
		UpdateCallStatus(4); // 4 >> End call		
	}

	var FinishCall = function () {		
		var result = UpdateCallStatus(5); // 5 >> End call
		if (result) {
			$("#btnCalling").hide();
			$("#btnCall").hide();
			$("#btnEndCall").hide();
			$("#btnEnd").hide();
			$("#LoanBriefContainer").hide();
			$("#NoLoanBriefContainer").show();
			$("#loanName").val("");
			$("#loanPhone").val("");
			$("#loanAddress").val("")
			$("#loanId").val("");
			$("#loanShop").val("Credit");
			$("#loanAmount").val("");
			$("#loanProduct").val("");
			$("#loanBriefId").val("");
			$("#loanActionId").val("");
		}
	}

	var timerTick = function () {		
		seconds++;
		// change timers
		var minute = Math.floor(seconds / 60)
		var remainSeconds = seconds - (minute * 60);
		var sMinute = "00";
		var sSecond = "00";
		if (minute < 10) {
			sMinute = "0" + minute;
		}
		else {
			sMinute = minute + "";
		}
		if (remainSeconds < 10) {
			sSecond = "0" + remainSeconds;
		}
		else {
			sSecond = remainSeconds + "";
		}
		var sTimer = sMinute + ":" + sSecond;
		$("#callTimer").html(sTimer);
		// Testing show end call button
		if (seconds > 5) {
			$("#btnCalling").hide();
			$("#btnEndCall").show();
		}
	}

	var UpdateCallStatus = function (status) {
		var loanActionId = $("#loanActionId").val();
		var data = { loanActionId: loanActionId, actionStatusId: status };
		var result = false;
		App.Ajax("POST", "/Telesales/UpdateCallStatus", data).done(function (response) {
			if (response.meta.errorCode == 200) {
				result = true;
			}			
		});		
		return result;
	}

	var ConfirmReason = function (loanActionId) {
		var reasonId = $("#slReason_" + loanActionId).val();
		if (reasonId == 0) {
			alert("Vui lòng chọn lí do");
			return;
		}
		var data = { loanActionId: loanActionId, actionStatusId: reasonId };
		App.Ajax("POST", "/Telesales/UpdateCallStatus", data).done(function (data) {
			$('#dtMissCall').mDatatable(missCallOptions);
		});
	}

	var ShowUpdateModal = function () {		
		$('#modalContent').html('');
		$.ajax({
			type: "POST",
			url: "/Telesales/UpdateLoanBriefModalPartialAsync",		
			success: function (data) {
				$('#modalContent').html(data);
				$('.m_selectpicker').selectpicker();
				$('#modalUpdate').modal('show');
				// datepicker
				// enable clear button 
				$('#dpCustomerDOB, #dpCustomerDOB_validate').datepicker({
					todayBtn: "linked",
					clearBtn: true,
					todayHighlight: true,
					orientation: "bottom left",
					format: 'dd/mm/yyyy',
					templates: {
						leftArrow: '<i class="la la-angle-left"></i>',
						rightArrow: '<i class="la la-angle-right"></i>'
					}
				});

				// enable clear button for modal demo
				$('#dpCustomerDOB_modal').datepicker({
					todayBtn: "linked",
					clearBtn: true,
					todayHighlight: true,
					orientation: "bottom left",
					format: 'dd/mm/yyyy',
					templates: {
						leftArrow: '<i class="la la-angle-left"></i>',
						rightArrow: '<i class="la la-angle-right"></i>'
					}
				});
			},
			traditional: true
		});	
	}

	var OnCallStatusChanged = function () {
		var status = $("#slCallStatus").val();
		if (status >= 15) {
			$(".script-form").hide();			
			if (status == 15) {
				$(".schedule-form").show();
				// input group demo
				$('#dpScheduleCall, #dpScheduleCall_validate').datetimepicker({
					todayHighlight: true,
					autoclose: true,
					pickerPosition: 'bottom-left',
					format: 'dd/mm/yyyy hh:ii'
				});
				$('#dpScheduleCall_modal').datetimepicker({
					todayHighlight: true,
					autoclose: true,
					pickerPosition: 'bottom-left',
					format: 'dd/mm/yyyy hh:ii'
				});
			}
			// change button text
			$("#btnModalNext").html("Kết thúc");
			$("#btnModalPrevious").prop("disabled", true);
		}
		else {
			$(".script-form").show();
			$(".schedule-form").hide();
			$("btnModalNext").html("Tiếp theo");
			$("#btnModalPrevious").prop("disabled", false);
		}
	}

	var OnNextClick = function () {
		// check status
		var status = $("#slCallStatus").val();
		mApp.block('#modalUpdate', {
			overlayColor: '#000000',
			type: 'loader',
			state: 'success',
			message: 'Please wait...'
		});
		if (status == 14) {
			var loanActionId = $("#loanActionId").val();
			var data = { loanActionId: loanActionId, actionStatusId: status };			
			App.Ajax("POST", "/Telesales/UpdateCallStatus", data).done(function (result) {
				if (result.meta.errorCode == 200) {
					App.ShowSuccess(result.meta.errorMessage);
					$('#modalUpdate').modal('hide');					
				}
				else {
					App.ShowError(result.meta.errorMessage);
					$('#modalUpdate').modal('hide');
				}
			});
		}
		else if (status == 15) {
			var loanActionId = $("#loanActionId").val();
			var scheduleTime = $("#dpScheduleCall").val();
			var data = { loanActionId: loanActionId, actionStatusId: status, scheduleTime: scheduleTime };
			App.Ajax("POST", "/Telesales/UpdateScheduleCall", data).done(function (result) {
				if (result.meta.errorCode == 200) {
					App.ShowSuccess(result.meta.errorMessage);
					$('#modalUpdate').modal('hide');
					$('#dtSchedule').mDatatable(scheduleCallOptions);
				}
				else {
					App.ShowError(result.meta.errorMessage);
					$('#modalUpdate').modal('hide');
				}
			});
		}
		else {
			var loanActionId = $("#loanActionId").val();
			var data = { loanActionId: loanActionId, actionStatusId: status };
			App.Ajax("POST", "/Telesales/UpdateCallStatus", data).done(function (result) {
				if (result.meta.errorCode == 200) {
					App.ShowSuccess(result.meta.errorMessage);
					$('#modalUpdate').modal('hide');					
				}
				else {
					App.ShowError(result.meta.errorMessage);
					$('#modalUpdate').modal('hide');
				}
			});
		}
	}

	var OnPreviousClick = function () {

	}

	var Bound = function (loanBriefId) {		
		var data = { loanBriefId: loanBriefId };
		App.Ajax("POST", "/Telesales/BoundTelesale", data).done(function (result) {
			if (result.meta.errorCode == 200) {
				$('#dtCalled').mDatatable(calledOptions);
			}
		});	
	}

	var EditInfo = function () {
		$('#modalContent').html('');
		$.ajax({
			type: "POST",
			url: "/Telesales/UpdateLoanBriefBoundModalPartialAsync",
			success: function (data) {
				$('#modalContent').html(data);				
				$('#modalUpdate').modal('show');				
			},
			traditional: true
		});	
	}
	
    return {
		Init: Init,
		TriggerOnline: TriggerOnline,
		StartCall: StartCall,
		EndCall: EndCall,
		FinishCall: FinishCall,
		ConfirmReason: ConfirmReason,
		ShowUpdateModal: ShowUpdateModal,
		OnCallStatusChanged: OnCallStatusChanged,
		OnNextClick: OnNextClick,
		OnPreviousClick: OnPreviousClick,
		EditInfo: EditInfo,
		Bound: Bound
    };
}

$(document).ready(function () {
	TelesalesModule.Init();
});