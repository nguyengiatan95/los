﻿'use strict';
var TlsQuestion_CreateOrUpdate = new function () {
    var createNewAnswer = $('#btnAddNewAnswer');
    var btnModalSave = $('#btnCreateNewQuestionSave');
    var frmCreateOrUpdateQuestion = $('#frmCreateOrUpdateQuestion');


    createNewAnswer.click(function () {
        generateAnswerInput('', '');
    });

    function generateAnswerInput(answer, value) {
        $('#answer-zone-id').append(
            '<div>'
            +'<div class="form-group nopadding row col-12">'
            + '     <div class="col-sm-12 col-md-6 m-form__group nopadding">'
            + '     	    <input type="text" class="form-control m-input" name="JSonOptionsText" value="' + answer + '"  placeholder="Câu trả lời" required>'
            + '      </div>'
            + '     <div class="col-sm-10 col-md-5 m-form__group nopadding">'
            + '     	    <input type="text" class="form-control m-input" name="JSonOptionsValue" value="' + value + '"  placeholder="Giá trị cần lưu"  required>'
            + '     </div>'
            + '     <div class="col-sm-10 col-md-1 m-form__group nopadding">'
            + '     	    <a href="#!" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only form-control m-input btnDeleteAnswer" style="height:100%;width:100%;" onclick="deleteRow(this);" > <i class="la la-times"></i> </a>'
            + '     </div>'
            + '</div>'
            + '<span class="clearfix"></span>'
            + '</div>'
        );
    }

    btnModalSave.on('click', function (e) {
        if (!frmCreateOrUpdateQuestion.valid()) {
            e.preventDefault();
            return false;
        }
        e.preventDefault();
        var data = frmCreateOrUpdateQuestion.serialize();
        $.post(frmCreateOrUpdateQuestion[0].action, data).done(function (data) {
            var functionSucc = function () {
                $('#m_modal_Group').modal('hide');
                isNeedReloadDataTable = true;
            };
            App.HandleDefaultResponse(data, 'Tạo/cập nhật câu hỏi thành công', 'Tạo/cập nhật câu hỏi không thành công', functionSucc, null);
        });
    });
    var generateAnswer = function myfunction() {
        var jSonOptions = $('#JSonOptions').val();
        if (jSonOptions && jSonOptions != '') {
            var jsonResult = JSON.parse(jSonOptions);
            jsonResult.forEach(x => generateAnswerInput(x.Text, x.Value));
        }

    }
    generateAnswer();
}
var deleteRow = function (ctl) {
    $(ctl).parent().parent().parent().remove();
}
