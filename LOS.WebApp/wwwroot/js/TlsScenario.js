﻿'use strict';
var isNeedReloadDataTable = false;
var TlsScenario = new function () {
    var _$filterTextControl = $('#FilterTextControl');
    var _$addNewButton = $('#AddNewButton');

    var datatable = $('.m-datatable').mDatatable({
        data: {
            type: 'remote',
            source: {
                read: {
                    // sample GET method
                    method: 'POST',
                    url: '/TLSScenario/LoadData'
                },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
        },
        // layout definition
        layout: {
            theme: 'default',
            class: '',
            scroll: false,
            footer: false
        },
        // column sorting
        sortable: true,
        pagination: true,

        toolbar: {
            // toolbar items
            items: {
                // pagination
                pagination: {
                    // page size select
                    pageSizeSelect: [10, 20, 30, 50, 100],
                },
            },
        },
        search: {
            //input: $('#filterName'),
        },
        columns: [
            {
                field: 'stt',
                title: 'STT',
                width: 50,
                textAlign: 'center',
                sortable: false,
                template: function (row, index, datatable) {
                    var pageIndex = datatable.API.params.pagination.page;
                    var record = 1;
                    if (pageIndex > 1) {
                        var pageSize = datatable.API.params.pagination.perpage;
                        record = (pageIndex - 1) * pageSize + 1;
                    }
                    return index + record;
                }
            },
            {
                field: 'configName',
                title: 'Tên kịch bản',
                sortable: true
            },
            {
                field: 'description',
                title: 'Ghi chú',
                sortable: true
            },
            {
                field: 'creationTime',
                title: 'Ngày giờ tạo',
                sortable: true
            }
            , {
                field: 'Action',
                title: 'Action',
                sortable: false,
                width: 100,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    return '\
							<a href="#!" class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only" onclick="TlsScenario.ShowModel('+ row.id + ')">\
                                <i class="la la-edit"></i></a>\
	                        <a href="#!" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only"  onclick="TlsScenario.Delete('+ row.id + ',\'' + row.configName + '\')" >\
                                <i class="la la-times"></i></a>';
                }
            }
        ]
    });


    function GetNewData() {
        datatable.API.params.pagination.page = 1;
        var searchValue = $('#FilterTextControl').val();
        datatable.search(searchValue, 'keyword');
        return false;
    }

    _$filterTextControl.on('keyup', function (e) {
        if (e.keyCode == 13) {
            GetNewData();
        }
    })

    _$filterTextControl.focus();
    var ShowModel = function (id) {
        $('#modal-TlsScenario').html('');
        $.ajax({
            type: "GET",
            url: "/TLSScenario/CreateOrUpdate",
            data: { Id: id },
            success: function (data) {
                $('#modal-TlsScenario').html(data);
                $('#m_modal_Group').modal('show');
            },
            traditional: true
        });
    }
    var Delete = function (id, name) {
        swal({
            title: 'Xóa kịch bản?',
            text: 'Bạn muốn xóa kịch bản "' + name + '"',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "DELETE",
                    url: "/TLSScenario/Delete",
                    data: { Id: id },
                    success: function (data) {
                        if (data && data.meta && data.meta.errorCode == 200) {

                            App.ShowSuccessNotLoad('Kịch bản của bạn đã bị xóa');
                            datatable.reload();
                        }
                        else if (data && data.meta && data.meta.errorCode != 200) {
                            App.ShowError(data.meta.errorMessage);
                        }
                        else {
                            App.ShowError('Có lỗi xảy ra, vui lòng liên hệ kỹ thuật!');
                        }
                    },
                    error: function (err, xhr, msg) {
                        App.ShowError('Không xóa được kịch bản "' + name + '"');
                    }
                });

            }
        });
    }
    $('#m_modal_Group').on('hidden.bs.modal', function () {
        if (isNeedReloadDataTable) {
            datatable.reload();
            isNeedReloadDataTable = false;
        }
    });

    return {
        ShowModel: ShowModel,
        Delete: Delete
    };
}