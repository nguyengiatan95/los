﻿'use strict';
var TlsScenario_CreateOrUpdate = new function () {
    var chkSelectAllQuestionsConfig = $('#chkSelectAllQuestionsConfig');
    var chkSelectAllQuestionsExist = $('#chkSelectAllQuestionsExist');
    var takeQuestionId = $('#TakeQuestionId');
    var releaseQuestionId = $('#ReleaseQuestionId');

    var btnModalSave = $('#btnCreateNewScenarioSave');
    var frmCreateOrUpdateScenario = $('#frmCreateOrUpdateScenario')

    chkSelectAllQuestionsConfig.click(function () {
        $('#questions_config input').prop("checked", this.checked);
    });

    chkSelectAllQuestionsExist.click(function () {
        $('#questions_exist input').prop("checked", this.checked);
    });
    takeQuestionId.click(function () {
        $('#questions_exist input:checked').map(function () {
            var control = $(this);
            var questionId = control.data('questionid');
            var questionText = control.parent().text();
            AddQuestionToDiv(true, questionId, questionText);
            control.parent().parent().remove();
        });
        chkSelectAllQuestionsConfig.prop("checked", false);
        chkSelectAllQuestionsExist.prop("checked", false);

    });
    releaseQuestionId.click(function () {
        $('#questions_config input:checked').map(function () {
            var control = $(this);
            var questionId = control.data('questionid');
            var questionText = control.parent().text();
            AddQuestionToDiv(false, questionId, questionText);
            control.parent().parent().remove();
        });
        chkSelectAllQuestionsConfig.prop("checked", false);
        chkSelectAllQuestionsExist.prop("checked", false);
    });

    function AddQuestionToDiv(isAddToQuestionConfig, questionId, questionText) {
        if (isAddToQuestionConfig) {
            $('#questions_config').append('<li class="ui-state-default">'
                + '<label class= "m-checkbox  m-checkbox--bold m-checkbox--state-success labelCheckbox">'
                + '<input type="hidden" name="QuestionIds" value="'
                + questionId
                + '"><input type="checkbox" data-questionId="'
                + questionId
                + '">'
                + questionText
                + '<span></span></label></li >'
            );
        }
        else {
            $('#questions_exist').append('<li class="ui-state-default">'
                + '<label class= "m-checkbox  m-checkbox--bold m-checkbox--state-success labelCheckbox" >'
                + '<input type="checkbox" data-questionId="'
                + questionId
                + '">'
                + questionText
                + '<span></span></label></li >'
            );
        }
    }

    btnModalSave.on('click', function (e) {
        if (!frmCreateOrUpdateScenario.valid()) {
            e.preventDefault();
            return false;
        }
        e.preventDefault();
        var data = frmCreateOrUpdateScenario.serialize();
        $.post(frmCreateOrUpdateScenario[0].action, data).done(function (data) {
            var functionSucc = function () {
                $('#m_modal_Group').modal('hide');
                isNeedReloadDataTable = true;
            };
            App.HandleDefaultResponse(data, 'Tạo/cập nhật kịch bản thành công', 'Tạo/cập nhật kịch bản không thành công', functionSucc, null);
        });
    });


    $('label.m-checkbox input[type="checkbox"]').change(function () {
        $(this).val(this.checked);
    });


}
$(document).ready(function () {
    $(".sortable").sortable();
});