﻿var AddDistrictAndWardModel = new function () {

    var Init = function () {
        $('#create_province_id').select2({
            placeholder: "Chọn Tỉnh/ Thành phố",
            width: '100%'
        });

        $('#create_province_id_v2').select2({
            placeholder: "Chọn Tỉnh/ Thành phố",
            width: '100%'
        });

        $('#create_district_id').select2({
            placeholder: "Chọn Quận/ Huyện",
            width: '100%'
        });

        $('#create_type_ward, #create_type_district').select2({
            placeholder: "Vui lòng chọn",
            width: '100%'
        });
    }

    var GetDistrict = function (provinceId) {
        if (provinceId > 0) {
            App.Ajax("GET", "/Dictionary/GetDistrict?id=" + provinceId, undefined).done(function (data) {
                if (data.data != undefined && data.data.length > 0) {
                    var html = "";
                    html = '<option></option>';
                    for (var i = 0; i < data.data.length; i++) {
                        html += `<option value="${data.data[i].districtId}">${data.data[i].name}</option>`;
                    }
                    $('#create_district_id').html(html);
                    $('#create_district_id').select2({
                        placeholder: "Chọn Quận/ Huyện"
                    });
                }
            });
        } else {
            $(element_district).select2({
                placeholder: "Quận/ Huyện",
                width: '100%'
            });
        }
    }

    var SaveDistrict = function (btn, e) {
        $(btn).attr('disabled', true);
        $(btn).addClass('m-loader m-loader--success m-loader--right');
        var form = $('#formAddDistrict');
        form.validate({
            ignore: [],
            rules: {
                ProvinceId: {
                    min: 1,
                    required: true,
                },
                DistrictName: {
                    required: true,
                },
                TypeDistrict: {
                    required: true,
                },
            },
            messages: {
                ProvinceId: {
                    min: "Vui lòng chọn Tỉnh/ Thành phố",
                    required: "Vui lòng chọn Tỉnh/ Thành phố",
                },
                DistrictName: {
                    required: "Vui lòng nhập tên Quận/Huyện",
                },
                TypeDistrict: {
                    required: "Vui lòng chọn Type",
                },
            }
        });
        if (!form.valid()) {
            $(btn).removeAttr('disabled', true);
            $(btn).removeClass('m-loader m-loader--success m-loader--right');
            return;
        }
        form.ajaxSubmit({
            url: '/Tool/AddDistrict',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $(btn).removeAttr('disabled', true);
                $(btn).removeClass('m-loader m-loader--success m-loader--right');
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    var SaveWard = function (btn, e) {
        $(btn).attr('disabled', true);
        $(btn).addClass('m-loader m-loader--success m-loader--right');
        var form = $('#formAddWard');
        form.validate({
            ignore: [],
            rules: {
                DistrictId: {
                    min: 1,
                    required: true,
                },
                WardName: {
                    required: true,
                },
                TypeWard: {
                    required: true,
                }
            },
            messages: {
                DistrictId: {
                    min: "Vui lòng chọn Quận/Huyện",
                    required: "Vui lòng chọn Quận/Huyện",
                },
                WardName: {
                    required: "Vui lòng nhập tên Phường/Xã",
                },
                TypeWard: {
                    required: "Vui lòng chọn Type",
                },
            }
        });
        if (!form.valid()) {
            $(btn).removeAttr('disabled', true);
            $(btn).removeClass('m-loader m-loader--success m-loader--right');
            return;
        }
        form.ajaxSubmit({
            url: '/Tool/AddWard',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $(btn).removeAttr('disabled', true);
                $(btn).removeClass('m-loader m-loader--success m-loader--right');
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    return {
        Init: Init,
        GetDistrict: GetDistrict,
        SaveDistrict: SaveDistrict,
        SaveWard: SaveWard
    }
}();