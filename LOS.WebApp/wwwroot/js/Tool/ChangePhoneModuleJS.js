﻿var ChangePhoneModule = new function () {

    var ChangePhone = function (btn, e) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn đổi SĐT không?</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $(btn).attr('disabled', true);
                $(btn).addClass('m-loader m-loader--success m-loader--right');
                e.preventDefault();
                var form = $('#formChangePhone');
                form.validate({
                    ignore: [],
                    rules: {
                        'LoanBriefId': {
                            required: true
                        },
                        'Phone': {
                            required: true,
                            validPhone: true
                        }
                    },
                    messages: {
                        'LoanBriefId': {
                            required: "Bạn vui lòng nhập mã đơn",
                            class: "has-danger"
                        },
                        'Phone': {
                            required: "Vui lòng nhập số điện thoại",
                            validPhone: "Số điện thoại không hợp lệ "
                        }

                    },
                    invalidHandler: function (e, validation) {
                        if (validation.errorList != null && validation.errorList.length > 0) {
                            App.ShowErrorNotLoad(validation.errorList[0].message)
                        }
                    }
                });
                if (!form.valid()) {
                    $(btn).attr('disabled', false);
                    $(btn).removeClass('m-loader m-loader--success m-loader--right');
                    return false;
                }

                form.ajaxSubmit({
                    url: '/Tool/ChangePhonePost',
                    method: 'POST',
                    success: function (data, status, xhr, $form) {
                        $(btn).attr('disabled', false);
                        $(btn).removeClass('m-loader m-loader--success m-loader--right');
                        if (data != undefined) {
                            if (data.status == 1) {
                                App.ShowSuccessNotLoad(data.message);
                                $('#formChangePhone').trigger("reset");
                            }
                            else {
                                App.ShowErrorNotLoad(data.message);
                            }
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    },
                    error: function () {
                    }
                });
            }
        });
    }

    var ReEsignCustomer = function (btn, e) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn ký lại hợp đồng điện tử cho khách hàng không?</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $(btn).attr('disabled', true);
                $(btn).addClass('m-loader m-loader--success m-loader--right');
                e.preventDefault();
                var form = $('#formReEsign');
                form.validate({
                    ignore: [],
                    rules: {
                        'LoanBriefId': {
                            required: true
                        }
                    },
                    messages: {
                        'LoanBriefId': {
                            required: "Bạn vui lòng nhập mã đơn",
                            class: "has-danger"
                        }
                    },
                    invalidHandler: function (e, validation) {
                        if (validation.errorList != null && validation.errorList.length > 0) {
                            App.ShowErrorNotLoad(validation.errorList[0].message)
                        }
                    }
                });
                if (!form.valid()) {
                    $(btn).attr('disabled', false);
                    $(btn).removeClass('m-loader m-loader--success m-loader--right');
                    return false;
                }
                form.ajaxSubmit({
                    url: '/Tool/ReEsignCustomer',
                    method: 'POST',
                    success: function (data, status, xhr, $form) {
                        $(btn).attr('disabled', false);
                        $(btn).removeClass('m-loader m-loader--success m-loader--right');
                        if (data != undefined) {
                            if (data.status == 1) {
                                App.ShowSuccessNotLoad(data.message);
                                $('#formReEsign').trigger("reset");
                            }
                            else {
                                App.ShowErrorNotLoad(data.message);
                            }
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    },
                    error: function () {
                    }
                });
            }
        });

    }

    var CancelCheckMomo = function (btn, e) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn bỏ kiểm tra Momo cho hợp đồng không?</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $(btn).attr('disabled', true);
                $(btn).addClass('m-loader m-loader--success m-loader--right');
                e.preventDefault();
                var form = $('#formCancelCheckMomo');
                form.validate({
                    ignore: [],
                    rules: {
                        'LoanBriefId': {
                            required: true
                        }
                    },
                    messages: {
                        'LoanBriefId': {
                            required: "Bạn vui lòng nhập mã đơn",
                            class: "has-danger"
                        }
                    },
                    invalidHandler: function (e, validation) {
                        if (validation.errorList != null && validation.errorList.length > 0) {
                            App.ShowErrorNotLoad(validation.errorList[0].message)
                        }
                    }
                });
                if (!form.valid()) {
                    $(btn).attr('disabled', false);
                    $(btn).removeClass('m-loader m-loader--success m-loader--right');
                    return false;
                }
                form.ajaxSubmit({
                    url: '/Tool/CancelCheckMomo',
                    method: 'POST',
                    success: function (data, status, xhr, $form) {
                        $(btn).attr('disabled', false);
                        $(btn).removeClass('m-loader m-loader--success m-loader--right');
                        if (data != undefined) {
                            if (data.status == 1) {
                                App.ShowSuccessNotLoad(data.message);
                                $('#formCancelCheckMomo').trigger("reset");
                            }
                            else {
                                App.ShowErrorNotLoad(data.message);
                            }
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    },
                    error: function () {
                    }
                });
            }
        });
    }

    var CancelEsign = function (btn, e) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn hủy ký hợp đồng điện tử không?</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $(btn).attr('disabled', true);
                $(btn).addClass('m-loader m-loader--success m-loader--right');
                e.preventDefault();
                var form = $('#formCancelEsign');
                form.validate({
                    ignore: [],
                    rules: {
                        'LoanBriefId': {
                            required: true
                        }
                    },
                    messages: {
                        'LoanBriefId': {
                            required: "Bạn vui lòng nhập mã đơn",
                            class: "has-danger"
                        }
                    },
                    invalidHandler: function (e, validation) {
                        if (validation.errorList != null && validation.errorList.length > 0) {
                            App.ShowErrorNotLoad(validation.errorList[0].message)
                        }
                    }
                });
                if (!form.valid()) {
                    $(btn).attr('disabled', false);
                    $(btn).removeClass('m-loader m-loader--success m-loader--right');
                    return false;
                }
                form.ajaxSubmit({
                    url: '/Tool/CancelEsign',
                    method: 'POST',
                    success: function (data, status, xhr, $form) {
                        $(btn).attr('disabled', false);
                        $(btn).removeClass('m-loader m-loader--success m-loader--right');
                        if (data != undefined) {
                            if (data.status == 1) {
                                App.ShowSuccessNotLoad(data.message);
                                $('#formCancelEsign').trigger("reset");
                            }
                            else {
                                App.ShowErrorNotLoad(data.message);
                            }
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    },
                    error: function () {
                    }
                });
            }
        });
    }

    var CancelCheckPayment = function (btn, e) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn bỏ kiểm tra Kỳ thanh toán cho hợp đồng không?</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $(btn).attr('disabled', true);
                $(btn).addClass('m-loader m-loader--success m-loader--right');
                e.preventDefault();
                var form = $('#formCancelCheckPayment');
                form.validate({
                    ignore: [],
                    rules: {
                        'LoanBriefId': {
                            required: true
                        }
                    },
                    messages: {
                        'LoanBriefId': {
                            required: "Bạn vui lòng nhập mã đơn",
                            class: "has-danger"
                        }
                    },
                    invalidHandler: function (e, validation) {
                        if (validation.errorList != null && validation.errorList.length > 0) {
                            App.ShowErrorNotLoad(validation.errorList[0].message)
                        }
                    }
                });
                if (!form.valid()) {
                    $(btn).attr('disabled', false);
                    $(btn).removeClass('m-loader m-loader--success m-loader--right');
                    return false;
                }
                form.ajaxSubmit({
                    url: '/Tool/CancelCheckPayment',
                    method: 'POST',
                    success: function (data, status, xhr, $form) {
                        $(btn).attr('disabled', false);
                        $(btn).removeClass('m-loader m-loader--success m-loader--right');
                        if (data != undefined) {
                            if (data.status == 1) {
                                App.ShowSuccessNotLoad(data.message);
                                $('#formCancelCheckPayment').trigger("reset");
                            }
                            else {
                                App.ShowErrorNotLoad(data.message);
                            }
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    },
                    error: function () {
                    }
                });
            }
        });
    }

    var CloseLoan = function (btn, e) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn đóng hợp đồng không?</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $(btn).attr('disabled', true);
                $(btn).addClass('m-loader m-loader--success m-loader--right');
                e.preventDefault();
                var form = $('#formCloseLoan');
                form.validate({
                    ignore: [],
                    rules: {
                        'LoanBriefId': {
                            required: true
                        }
                    },
                    messages: {
                        'LoanBriefId': {
                            required: "Bạn vui lòng nhập mã đơn",
                            class: "has-danger"
                        }
                    },
                    invalidHandler: function (e, validation) {
                        if (validation.errorList != null && validation.errorList.length > 0) {
                            App.ShowErrorNotLoad(validation.errorList[0].message)
                        }
                    }
                });
                if (!form.valid()) {
                    $(btn).attr('disabled', false);
                    $(btn).removeClass('m-loader m-loader--success m-loader--right');
                    return false;
                }
                form.ajaxSubmit({
                    url: '/Tool/CloseLoan',
                    method: 'POST',
                    success: function (data, status, xhr, $form) {
                        $(btn).attr('disabled', false);
                        $(btn).removeClass('m-loader m-loader--success m-loader--right');
                        if (data != undefined) {
                            if (data.status == 1) {
                                App.ShowSuccessNotLoad(data.message);
                                $('#formCloseLoan').trigger("reset");
                            }
                            else {
                                App.ShowErrorNotLoad(data.message);
                            }
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    },
                    error: function () {
                    }
                });
            }
        });
    }

    var CheckAndUpdateLoanTopup = function (btn, e) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn kiểm tra và cập nhập HĐ Topup?</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $(btn).attr('disabled', true);
                $(btn).addClass('m-loader m-loader--success m-loader--right');
                e.preventDefault();
                var form = $('#formTopup');
                form.validate({
                    ignore: [],
                    rules: {
                        'LoanBriefId': {
                            required: true
                        }
                    },
                    messages: {
                        'LoanBriefId': {
                            required: "Bạn vui lòng nhập mã đơn",
                            class: "has-danger"
                        }
                    },
                    invalidHandler: function (e, validation) {
                        if (validation.errorList != null && validation.errorList.length > 0) {
                            App.ShowErrorNotLoad(validation.errorList[0].message)
                        }
                    }
                });
                if (!form.valid()) {
                    $(btn).attr('disabled', false);
                    $(btn).removeClass('m-loader m-loader--success m-loader--right');
                    return false;
                }
                form.ajaxSubmit({
                    url: '/Tool/CheckAndUpdateLoanTopup',
                    method: 'POST',
                    success: function (data, status, xhr, $form) {
                        $(btn).attr('disabled', false);
                        $(btn).removeClass('m-loader m-loader--success m-loader--right');
                        if (data != undefined) {
                            if (data.status == 1) {
                                App.ShowSuccessNotLoad(data.message);
                                $('#formTopup').trigger("reset");
                            }
                            else {
                                App.ShowErrorNotLoad(data.message);
                            }
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }
                    },
                    error: function () {
                    }
                });
            }
        });
    }

    return {
        ChangePhone: ChangePhone,
        ReEsignCustomer: ReEsignCustomer,
        CancelCheckMomo: CancelCheckMomo,
        CancelEsign: CancelEsign,
        CancelCheckPayment: CancelCheckPayment,
        CloseLoan: CloseLoan,
        CheckAndUpdateLoanTopup: CheckAndUpdateLoanTopup,
    }
}