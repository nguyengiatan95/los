﻿var GenNewOtpModel = new function () {

    var GenNewOtp = function (btn) {
        $(btn).attr('disabled', '');
        $(btn).addClass('m-loader m-loader--success m-loader--right');
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            rules: {
                TextSearch: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
            },
            messages: {
                TextSearch: {
                    required: "Vui lòng nhập tên tài khoản hoặc Email",
                    class: "has-danger"
                },
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            $(btn).removeAttr('disabled', '');
            $(btn).removeClass('m-loader m-loader--success m-loader--right');
            return;
        }
        form.ajaxSubmit({
            url: "/Tool/GenNewOtp",
            method: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            success: function (data) {
                $.unblockUI();
                $(btn).removeAttr('disabled', '');
                $(btn).removeClass('m-loader m-loader--success m-loader--right');
                $('#_responce').html('');
                if (data.data != null) {
                    if (data.data.meta.errorCode == 200 && data.data.data != null) {
                        $('#_responce').html('Mã OTP của tài khoản: <b>' + data.data.data.userName + '</b> là: <b>' + data.data.data.otp + '</b>');
                    }
                    else {
                        $('#_responce').html(data.data.meta.errorMessage);
                    }
                }
            }
        });
    }

    return {
        GenNewOtp: GenNewOtp
    }
}
