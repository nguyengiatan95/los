﻿var PrematureInterestToolModel = new function () {
    var CalculatePrematureInterest = function (btn, e) {
        $(btn).attr('disabled', '');
        $(btn).addClass('m-loader m-loader--success m-loader--right');
        var form = $(btn).closest('form');
        var textSearch = $('#txt_TextSearch').val().trim();
        var loanbriefId = $('#txt_LoanBriefId').val().trim();
        if (textSearch == "" && loanbriefId == "") {
            App.ShowErrorNotLoad("Vui lòng nhập SĐT (CMND) hoặc Mã hợp đồng để tìm kiếm");
            $(btn).removeAttr('disabled', '');
            $(btn).removeClass('m-loader m-loader--success m-loader--right');
            return;
        }
        if (textSearch != "") {
            if (textSearch.length != 9 && textSearch.length != 10 && textSearch.length != 12) {
                App.ShowErrorNotLoad("SĐT (CMND) không đúng định dạng");
                $(btn).removeAttr('disabled', '');
                $(btn).removeClass('m-loader m-loader--success m-loader--right');
                return;
            }
        }
        form.ajaxSubmit({
            url: "/Tool/CalculatePrematureInterest",
            method: "GET",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            success: function (data) {
                $.unblockUI();
                $(btn).removeAttr('disabled', '');
                $(btn).removeClass('m-loader m-loader--success m-loader--right');
                if (data.isSuccess == false) {
                    App.ShowErrorNotLoad(data.message);
                }
                else {
                    $("#div-datatable").html('');
                    $('#div-datatable').html(data.html);
                }
            }
        });
    }

    return {
        CalculatePrematureInterest: CalculatePrematureInterest
    }
}();

