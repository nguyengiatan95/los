﻿var ToolManageTelesaleSupportModule = new function () {
    var Init = function () {
        $('#sl_TypeAction').select2({
            placeholder: "Vui lòng chọn",
        });
    }

    var UpdateLoanTopup = function (btn, e) {
        $(btn).attr('disabled', true);
        $(btn).addClass('m-loader m-loader--success m-loader--right');
        var loanBriefId = $('#txt_LoanBriefIdLoanTopup').val();
        var typeAction = $('#sl_TypeAction').val();
        if (loanBriefId == null || loanBriefId == '' || loanBriefId == undefined) {
            App.ShowErrorNotLoad('Vui lòng nhập mã hợp đồng.');
            $(btn).removeAttr('disabled', true);
            $(btn).removeClass('m-loader m-loader--success m-loader--right');
            return false;
        }
        if (typeAction == null || typeAction == '' || typeAction == undefined) {
            App.ShowErrorNotLoad('Vui lòng chọn chức năng.');
            $(btn).removeAttr('disabled', true);
            $(btn).removeClass('m-loader m-loader--success m-loader--right');
            return false;
        }
        //Khôi phục lại đơn Topup
        if (typeAction == ActionToolManageTelesaleSupport.RestoreLoanTopup) {
            swal({
                title: '<b>Cảnh báo</b>',
                html: "Bạn có chắc chắn muốn khôi phục lại HĐ Topup không?</span>",
                showCancelButton: true,
                confirmButtonText: 'Xác nhận',
                cancelButtonText: 'Hủy',
                onOpen: function () {
                    $('.swal2-cancel').width($('.swal2-confirm').width());
                }
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: '/Tool/RestoreTopup',
                        method: 'POST',
                        data: { loanBriefId: loanBriefId },
                        success: function (data, status, xhr, $form) {
                            $(btn).attr('disabled', false);
                            $(btn).removeClass('m-loader m-loader--success m-loader--right');
                            if (data != undefined) {
                                if (data.status == 1) {
                                    App.ShowSuccessNotLoad(data.message);
                                }
                                else {
                                    App.ShowErrorNotLoad(data.message);
                                }
                            }
                            else {
                                App.ShowErrorNotLoad(data.message);
                            }
                        },
                        error: function () {
                        }
                    });
                }
            });
        }
        //Kiểm tra và cập nhật đơn có thể Topup
        else if (typeAction == ActionToolManageTelesaleSupport.CheckAndUpdateLoanTopup) {
            swal({
                title: '<b>Cảnh báo</b>',
                html: "Bạn có chắc chắn muốn kiểm tra và cập nhập HĐ Topup?</span>",
                showCancelButton: true,
                confirmButtonText: 'Xác nhận',
                cancelButtonText: 'Hủy',
                onOpen: function () {
                    $('.swal2-cancel').width($('.swal2-confirm').width());
                }
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: '/Tool/CheckAndUpdateLoanTopup',
                        method: 'POST',
                        data: { LoanBriefId: loanBriefId },
                        success: function (data, status, xhr, $form) {
                            $(btn).attr('disabled', false);
                            $(btn).removeClass('m-loader m-loader--success m-loader--right');
                            if (data != undefined) {
                                if (data.status == 1) {
                                    App.ShowSuccessNotLoad(data.message);
                                }
                                else {
                                    App.ShowErrorNotLoad(data.message);
                                }
                            }
                            else {
                                App.ShowErrorNotLoad(data.message);
                            }
                        },
                        error: function () {
                        }
                    });
                }
            });
        }
    }

    return {
        Init: Init,
        UpdateLoanTopup: UpdateLoanTopup,
    }
}