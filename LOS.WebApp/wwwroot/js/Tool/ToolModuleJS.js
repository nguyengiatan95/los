﻿var ToolModuleJS = new function () {

    var SearchLogRequestAI = function () {
        var loanbriefId = $('#loanbriefId').val();
        if (loanbriefId == null || loanbriefId == "" || loanbriefId == 0) {
            App.ShowErrorNotLoad("Vui lòng chọn đơn vay cần tra cứu");
            return;
        }

        var form = $('#formLogRequestAi');
        form.ajaxSubmit({
            url: "/Tool/SearchLogRequestAI",
            method: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'text',
            success: function (data) {
                $('#dtLogRequestAi').html(data);
                $.unblockUI();
            }
        });
    }

    var SearchRecord = function () {
        //var phone = $('#txtPhone').val();
        //if (phone == null || phone == "") {
        //    App.ShowErrorNotLoad("Vui lòng nhập số điện thoại tìm kiếm");
        //    return;
        //}
        var time = $('#filtercreateTime').val();
        if (time == null || time == "") {
            App.ShowErrorNotLoad("Vui lòng chọn thời gian tìm kiếm");
            return;
        }

        var form = $('#formRecord');
        form.ajaxSubmit({
            url: "/Tool/SearchRecording",
            method: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'text',
            success: function (data) {
                $('#dtRecording').html(data);
                $.unblockUI();
            }
        });
    }

    var DetailLogRequestAI = function (id) {
        $('#modal_detail_logrequestai').modal('show');
        $('#tbl_detail_logrequestai').html('');
        $.ajax({
            url: "/Tool/GetDetailLogRequestAi",
            method: "GET",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { Id: id },
            success: function (data) {
                $('#tbl_detail_logrequestai').html(data);
                $.unblockUI();
            }
        });
    }

    var SearchLogCallApi = function () {
        var loanbriefId = $('#loanbriefId').val();
        if (loanbriefId == null || loanbriefId == "" || loanbriefId == 0) {
            App.ShowErrorNotLoad("Vui lòng nhập đơn vay cần tra cứu");
            return;
        }
        var form = $('#formLogCallApi');
        form.ajaxSubmit({
            url: "/Tool/SearchLogCallApi",
            method: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'text',
            success: function (data) {
                $('#dtLogCallApi').html(data);
                $.unblockUI();
            }
        });
    }

    var DetailLogCallApi = function (id) {
        $('#modal_detail_logcallapi').modal('show');
        $('#tbl_detail_logcallapi').html('');
        $.ajax({
            url: "/Tool/GetDetailLogCallApi",
            method: "GET",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { Id: id },
            success: function (data) {
                $('#tbl_detail_logcallapi').html(data);
                $.unblockUI();
            }
        });
    }

    var DownloadFile = function (id) {
        debugger;
        $.ajax({
            url: "/Tool/DownloadFile",
            method: "GET",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { Id: id },
            success: function (data) {
                $.unblockUI();
                if (data.status == 1)
                    App.ShowSuccessNotLoad(data.message);
            }
        });
    }

    var SearchRecordCisco = function () {
        var form = $('#formRecordCisco');
        form.ajaxSubmit({
            url: "/Tool/SearchRecordingCisco",
            method: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            dataType: 'text',
            success: function (data) {
                $('#dtRecordCisco').html(data);
                $.unblockUI();
            }
        });
    }

    var CheckPhone = function () {

        var phone = $('#txtCheckPhone').val();
        if (phone == null || phone == "") {
            App.ShowErrorNotLoad("Vui lòng nhập SĐT tìm kiếm");
            return;
        }

        $.ajax({
            url: "/Tool/CheckPhone",
            method: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { phone: phone },
            success: function (data) {
                $('#dtCheckPhone').html(data);
                $.unblockUI();
            }
        });
    }

    var CheckCavet = function () {

        var search = $('#txtCavetSearch').val();
        if (search == null || search == "") {
            App.ShowErrorNotLoad("Vui lòng nhập SĐT hoặc CMND để tìm kiếm");
            return;
        }

        $.ajax({
            url: "/Tool/CheckCavet",
            method: "POST",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { search: search },
            success: function (data) {
                $('#dtCheckCavet').html(data);
                $.unblockUI();
            }
        });
    }
    return {
        SearchRecord: SearchRecord,
        SearchLogRequestAI: SearchLogRequestAI,
        DetailLogRequestAI: DetailLogRequestAI,
        SearchLogCallApi: SearchLogCallApi,
        DetailLogCallApi: DetailLogCallApi,
        DownloadFile: DownloadFile,
        SearchRecordCisco: SearchRecordCisco,
        CheckPhone: CheckPhone,
        CheckCavet: CheckCavet
    };
}