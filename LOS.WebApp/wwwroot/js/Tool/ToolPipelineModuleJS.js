﻿var ToolPipelineModuleJS = new function () {

    var OnChangePipeline = function (action) {
        var loanbriefId = $('#loanbriefId').val();
        if (loanbriefId == null || loanbriefId == "" || loanbriefId == 0) {
            App.ShowErrorNotLoad("Vui lòng nhập mã đơn vay");
            return;
        }

        var actionPipeline = $('#ddl_ActionPipeline').val();
        if (actionPipeline == null || actionPipeline == "" || actionPipeline == 0 || actionPipeline == -1) {
            App.ShowErrorNotLoad("Vui lòng chọn action");
            return;
        }
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn chuyển trạng thái <b>HĐ-" + loanbriefId + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Tool/ChangePipeline",
                    data: {
                        loanbriefId: loanbriefId,
                        actionPipeline: actionPipeline
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccess(data.message);
                        }
                        else {
                            App.ShowErrorNotLoad(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }
    return {
        OnChangePipeline: OnChangePipeline
    };
}