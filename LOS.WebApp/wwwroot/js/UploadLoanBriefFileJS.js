﻿var UploadLoanBriefFileJS = new function () {
    var isClick = false;

    var Init = function () {

        $('#sl_document').select2({
            placeholder: "Vui lòng chọn"
        });

        $('#sl_document_multiple').select2({
            placeholder: "Vui lòng chọn"
        });

        var loanBriefId = $('#hdd_LoanBriefId').val();
        $('[data-toggle="m-tooltip"]').tooltip();
        UploadLoanBriefFileJS.ViewUploadMulti(loanBriefId);
        UploadLoanBriefFileJS.viewerLoading();
        Dropzone.autoDiscover = false;
        Dropzone.options.mDropzoneUploads = {
            paramName: "files",
            maxFiles: 10,
            maxFilesize: 10,
            addRemoveLinks: true,
            acceptedFiles: "image/*",
            autoProcessQueue: false,
            accept: function (file, done) {
                done();
            },
            init: function () {
                var wrapperThis = this;
                document.getElementById("btnBrowserImageMultiple").addEventListener("click", function (e) {
                    // Make sure that the form isn't actually being sent.
                    e.preventDefault();
                    e.stopPropagation();
                    var files = wrapperThis.files;
                    console.log(files);
                    UploadDropFile(files);
                });
            }
        }
        Dropzone.discover();
    }

    var OpenChooseFile = function () {
        $("#fileUploadFile").click();
    }

    var ViewDocument = function (documentId, documentName, allowFromUpload, totalFile) {
        $("#loading").css("display", "");
        //check xem có file không
        //không có thì ẩn phần chuyển danh mục đi
        $('#btnBrowserImageMultiple').attr('style', 'display: none;');
        $('#btnBrowserImage').removeAttr('style', 'display: none;');
        $('#btnBrowserImage').addClass('btn-upload-image');

        if (totalFile == 0) {
            $('.moved-document').attr('style', 'display: none;');
        }
        else {
            $('.moved-document').removeAttr('style', 'display: none;');
            $('#btnMovedDocument').addClass('btn-moved-document');
        }

        if (documentId > 0) {
            $('._a_ducument').removeClass("active");
            if (allowFromUpload != null && allowFromUpload != '' && allowFromUpload != undefined) {
                var arrayAllowFromUpload = JSON.parse("[" + allowFromUpload + "]");
                if (arrayAllowFromUpload.indexOf(1) < 0)
                    $('#btnBrowserImage').attr('style', 'display: none;');
                else {
                    $('#btnBrowserImage').removeAttr('style', 'display: none;');
                    $('#btnBrowserImage').addClass('btn-upload-image');
                }
            }
            else {
                $('#btnBrowserImage').removeAttr('style', 'display: none;');
                $('#btnBrowserImage').addClass('btn-upload-image');
            }
            $('#hdd_DocumentType').val(documentId);
            var loanBriefId = $('#hdd_LoanBriefId').val();
            $('#hdd_DocumentName').val(documentName);
            LoadDocument(documentId, loanBriefId);
        }
    }

    var viewerLoading = function () {
        var galley = document.getElementById('box_galley');
        viewer = new Viewer(galley, {
            url: 'data-original',
            title: function (image) {
                return image.alt + ' (' + (this.index + 1) + '/' + this.length + ')';
            },
        });
    }

    var LoadDocument = function (documentId, loanBriefId) {
        $("#dropImage").css("display", "none");
        $.ajax({
            type: "GET",
            url: "/LoanBrief/GetDocumetView",
            data: {
                'loanbriefId': loanBriefId,
                'documentId': documentId
            },
            dataType: 'json',
            success: function (data) {
                $("#loading").css("display", "none");
                if (data.isSuccess == 1) {
                    $("#box_galley").html('');
                    $("#box_galley").html(data.html);
                    viewer.update();
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            timeout: 60000
        });
    }

    var ViewUploadMulti = function (loanBriefId, totalFile) {
        $('#btnBrowserImage').attr('style', 'display: none;');
        $('#btnBrowserImageMultiple').removeAttr('style', 'display: none;');
        $('#btnBrowserImageMultiple').addClass('btn-upload-image');
        //check xem có file không
        //không có thì ẩn phần chuyển danh mục đi
        if (totalFile == 0) {
            $('.moved-document').attr('style', 'display: none;');
        }
        else {
            $('.moved-document').removeAttr('style', 'display: none;');
            $('#btnMovedDocument').addClass('btn-moved-document');
        }
        $("#loading").css("display", "");
        $("#dropImage").css("display", "block");
        $.ajax({
            type: "GET",
            url: "/LoanBrief/GetUploadMulti",
            data: {
                'loanbriefId': loanBriefId
            },
            dataType: 'json',
            success: function (data) {
                $("#loading").css("display", "none");
                if (data.isSuccess == 1) {
                    $("#box_galley").html('');
                    $("#box_galley").html(data.html);
                    viewer.update();
                } else {
                    App.ShowErrorNotLoad(data.message);
                }
            },
            timeout: 60000
        });
    }

    var UploadFile = function () {
        var formData = new FormData();
        var documentId = $('#hdd_DocumentType').val();
        var loanBriefId = $('#hdd_LoanBriefId').val();
        var documentName = $('#hdd_DocumentName').val();
        var file = $('#fileUploadFile')[0].files;
        $("#btnBrowserImage").addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
        for (var i = 0; i < file.length; i++) {
            if (file[i].name.includes('.mp4') || file[i].name.includes('.mov')) {
                if (file[i].size > 51200000 || file[i].fileSize > 51200000) {
                    App.ShowErrorNotLoad("Dung lượng file không được quá 50MB");
                    $('#fileUploadFile').val('');
                } else {
                    formData.append('files', file[i]);
                    documentName = 'video';
                }
            } else {
                if (file[i].size > 10240000 || file[i].fileSize > 10240000) {
                    App.ShowErrorNotLoad("Dung lượng file không được quá 10MB");
                    $('#fileUploadFile').val('');
                } else {
                    formData.append('files', file[i]);
                }
            }
        }
        formData.append('documentId', documentId);
        formData.append('loanBriefId', loanBriefId);
        formData.append('documentName', documentName);
        $.ajax({
            type: "POST",
            url: "/LoanBrief/UploadFile",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                $("#btnBrowserVideo").removeClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                $("#btnBrowserImage").removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                $('#fileUploadFile').val('');
                if (data.status == 1) {
                    LoadDocument(documentId, loanBriefId);
                } else {
                    App.ShowErrorNotLoad(data.message);
                }
            },
            timeout: 60000
        });
    }

    var GetDocunmentType = function (loanBriefId) {
        $.ajax({
            type: "GET",
            url: "/LoanBrief/GetDocunmentType",
            data: {
                'loanbriefId': loanBriefId
            },
            dataType: 'json',
            success: function (data) {
                if (data.isSuccess == 1) {
                    $("#box-Document").html('');
                    $("#box-Document").html(data.html);
                    viewer.update();
                } else {
                    App.ShowErrorNotLoad(res.message);
                }
            },
            timeout: 60000
        });
    }

    var AddResultCheck = function (loanBriefId, typeCheck) {
        $("#btnSubmit").attr('disabled', true);
        $("#btn-Submit").attr('disabled', true);
        var selected = [];
        if (typeCheck == 1) {
            $('#checkboxes input:checked').each(function () {
                //selected.push($(this).attr('attr-doctype'));
                var object = {};
                object.resultCheck = JSON.parse($(this).attr('value'));
                object.documentType = parseInt($(this).attr('attr-doctype'));
                object.description = $(this).attr('attr-description');
                selected.push(object);
            });
        } else if (typeCheck == 2) {
            $('#m_portlet_tab_1_1 input:checked').each(function () {
                //selected.push($(this).attr('attr-doctype'));
                var object = {};
                object.resultCheck = JSON.parse($(this).attr('value'));
                object.documentType = parseInt($(this).attr('attr-doctype'));
                object.description = $(this).attr('attr-description');
                selected.push(object);
            });
        }

        if (selected.length <= 0) {
            App.ShowErrorNotLoad('Bạn phải tick chọn trước khi lưu!');
            return;
        }

        //console.log(selected);
        //return;
        var formData = {
            LoanBriefId: parseInt(loanBriefId),
            TypeCheck: parseInt(typeCheck),
            LstCheckImage: selected
        };
        //console.log(formData);
        $.ajax({
            url: '/LoanBrief/AddResultCheckImage',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(formData),
            success: function (data) {
                if (data.status == 1) {
                    $("#btnSubmit").attr('disabled', false);
                    $("#btn-Submit").attr('disabled', false);
                    App.ShowSuccessNotLoad(data.message);
                } else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    var UploadDropFile = function (files) {
        if (isClick)
            return;
        var documentId = 0;
        $("input[name='documentType']:checked").each(function () {
            documentId = $(this).val();
        });

        var formData = new FormData();
        if (files.length < 1) {
            App.ShowErrorNotLoad("Bạn phải chọn ảnh upload");
            return false;
        }
        for (var i = 0; i < files.length; i++) {
            if (files[i].size > 10240000 || files[i].fileSize > 10240000) {
                App.ShowErrorNotLoad("Dung lượng file không được quá 10MB");
                return;
            } else {
                formData.append('files', files[i]);
            }
        }
        $("#btnBrowserImageMultiple").addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
        var loanBriefId = $('#hdd_LoanBriefId').val();
        if (loanBriefId > 0) {
            isClick = true;
            formData.append('documentId', documentId);
            formData.append('loanBriefId', loanBriefId);
            $.ajax({
                type: "POST",
                url: "/LoanBrief/DropUpload",
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    isClick = false;
                    if (data.status == 1) {
                        $('#sl_document').val(0).change();
                        $('#sl_document_multiple').val(0).change();
                        Dropzone.forElement("#m-dropzone-uploads").removeAllFiles(true);
                        UploadLoanBriefFileJS.ViewUploadMulti(loanBriefId);
                        UploadLoanBriefFileJS.GetDocunmentType(loanBriefId);
                        $("#btnBrowserImageMultiple").removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    } else {
                        App.ShowErrorNotLoad(data.message);
                    }
                },
                error: function () {
                    isClick = false;
                },
                timeout: 60000
            });
        }
        return false;
    }

    var AddDocumentTypeImage = function () {
        var documentId = 0;
        var document = $('#sl_document').val();
        var documentMultiple = $('#sl_document_multiple').val();
        if (document != null && document != '' && document != undefined && document != 0)
            documentId = document;
        else if (documentMultiple != null && documentMultiple != '' && documentMultiple != undefined && documentMultiple != 0)
            documentId = documentMultiple;

        if (documentId == 0) {
            App.ShowErrorNotLoad('Bạn chưa chọn chuyên mục để chuyển!');
            return;
        }
        var selected = [];
        $('#box_galley input:checked').each(function () {
            var object = {};
            object.fileId = parseInt($(this).attr('value'));
            selected.push(object);
        });
        if (selected.length <= 0) {
            App.ShowErrorNotLoad('Bạn phải chọn ảnh trước khi chuyển!');
            return;
        }
        $("#btn-add").addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
        var loanBriefId = $('#hdd_LoanBriefId').val();
        var formData = {
            LoanBriefId: parseInt(loanBriefId),
            DocumentId: parseInt(documentId),
            LstFileId: selected
        };
        $.ajax({
            url: '/LoanBrief/AddDocumentTypeImage',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(formData),
            success: function (data) {
                if (data.status == 1) {
                    $("#btn-add").removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    $('#sl_document').val(0).change();
                    $('#sl_document_multiple').val(0).change();
                    UploadLoanBriefFileJS.ViewUploadMulti(loanBriefId);
                    UploadLoanBriefFileJS.GetDocunmentType(loanBriefId);

                    App.ShowSuccessNotLoad(data.message);
                } else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    return {
        OpenChooseFile: OpenChooseFile,
        UploadFile: UploadFile,
        ViewDocument: ViewDocument,
        viewerLoading: viewerLoading,
        ViewUploadMulti: ViewUploadMulti,
        GetDocunmentType: GetDocunmentType,
        AddResultCheck: AddResultCheck,
        Init: Init,
        UploadDropFile: UploadDropFile,
        AddDocumentTypeImage: AddDocumentTypeImage,
    }
}