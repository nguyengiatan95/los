﻿var UserModule = new function () {

    var InitDataTable = function (objQuery) {
        var datatable = $('#tbUser').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/Users/LoadData',
                        params: {
                            query: objQuery
                        },
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'username',
                    title: 'Tài khoản',
                    sortable: false
                },
                {
                    field: 'fullName',
                    title: 'Họ tên',
                    sortable: false
                },
                {
                    field: 'phone',
                    title: 'Số điện thoại',
                    sortable: false
                },
                {
                    field: 'email',
                    title: 'Email',
                    sortable: false
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    sortable: false,
                    template: function (row) {
                        if (row.status == null)
                            row.status = 0;
                        var status = {
                            0: { 'title': 'Khóa', 'class': 'm-badge--metal' },
                            1: { 'title': 'Kích hoạt', 'class': 'm-badge--success' }
                        };
                        return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
                    }
                },
                {
                    field: 'group.groupName',
                    title: 'Nhóm tài khoản',
                    sortable: false
                }
                , {
                    field: 'Action',
                    title: 'Chức năng',
                    sortable: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return '\
							<a href="/user?q='+ row.urlEdit + '" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only">\
                                <i class="la la-edit"></i></a>\
                             <a href="javascript:;" title="Phân quyền dữ liệu" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only"  onclick="UserModule.ShowPermissionData(\''+ row.urlEdit + '\')" >\
                                <i class="fa fa-gears"></i></a>\
	                        <a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only"  onclick="UserModule.DeleteUser(\''+ row.urlEdit + '\',\'' + row.username + '\')" >\
                                <i class="la la-times"></i></a>\
                            <a href="javascript:;" title="Phân quyền API" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only"  onclick="UserModule.ShowPermissionApi(\''+ row.userId + '\',\'' + row.username + '\')" >\
                                <i class="fa fa-desktop"></i></a>\
                             <a href="javascript:;" title="Cấu hình Cisco cho mobile" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only"  onclick="UserModule.ConfigCiscoMobile(\''+ row.userId + '\')" >\
                                <i class="fa fa-mobile-phone"></i></a>\
                            <a href="#" class="btn-action btn btn-brand m-btn m-btn--icon btn-sm m-btn--icon-only" title="Reset mật khẩu"  onclick="UserModule.ResetPassword(\''+ row.userId + '\',\'' + row.username + '\')" >\
                                <i class="la la-refresh"></i></a>';
                    }
                }
            ]
        });
    }

    var LoadData = function () {
        var objQuery = {
            groupId: $('#filterGroup').val(),
            status: $('#filterStatus').val(),
            filterName: $('#filterName').val().trim()
        }
        InitDataTable(objQuery);
    }

    var Init = function () {
        $("#filterGroup").select2({
            placeholder: "Vui lòng chọn"
        });

        $("#filterStatus").select2({
            placeholder: "Vui lòng chọn"
        });

        LoadData();

        $('#filterGroup, #filterStatus').on('change', function () {
            LoadData();
        });

        $("#filterName").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });

    };

    var ResetPassword = function (userId, userName) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn  đặt lại mật khẩu : <b>" + userName + "</b> về mặc định <br /> <span style='font-size: 12px; color: #909090;'>(Mật khẩu mặc định: Tima@2021)</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Users/ResetPassword",
                    data: { Id: userId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                        } else {
                            App.ShowError(data.message);
                        }
                    },
                    traditional: true
                });
            }
        });
    }

    var AddUser = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            rules: {
                Username: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                //FullName: {
                //    required: true,
                //    normalizer: function (value) {
                //        return $.trim(value);
                //    }
                //},
                //Email: {
                //    required: true,
                //    normalizer: function (value) {
                //        return $.trim(value);
                //    },
                //    email: true
                //},
                //Phone: {
                //    required: true,
                //    //matches: "^(\\d|\\s)+$",
                //    number: true,
                //    minlength: 10,
                //    maxlength: 11,
                //},
                Password: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    },
                    minlength: 6
                },
                GroupId:
                {
                    required: true
                }
            },
            messages: {
                Username: {
                    required: "Bạn chưa nhập tài khoản",
                    class: "has-danger"
                },
                //FullName: {
                //    required: "Bạn chưa nhập họ tên",
                //},
                //Email: {
                //    required: "Bạn chưa nhập email",
                //    email: "Định dạng email không đúng"
                //},
                //Phone: {
                //   // matches: "^(\\d|\\s)+$",
                //    number: "Số điện thoại không hợp lệ",
                //    minlength: "Số điện thoại không hợp lệ ",
                //    maxlength: "Số điện thoại không hợp lệ",
                //},

                Password: {
                    required: "Bạn chưa nhập mật khẩu",
                    minlength: "Mật khẩu phải chứa ít nhất 6 ký tự"
                },
                GroupId: {
                    required: "Bạn chưa chọn nhóm tài khoản",
                }
            },
            invalidHandler: function (e, validation) {
                if (validation.errorList != null && validation.errorList.length > 0) {
                    App.ShowErrorNotLoad(validation.errorList[0].message)
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        form.ajaxSubmit({
            url: '/Users/CreateUser',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccessRedirect(data.message, '/user-manager.html');
                        $('#create-user').modal('toggle');
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };

    var ShowModelUser = function (id) {
        $('#modelUser').html('');
        $.ajax({
            type: "POST",
            url: "/Users/CreateUserModalPartialAsync",
            data: { Id: id },
            success: function (data) {
                $('#modelUser').html(data);
                $('#m_modal_4').modal('show');
            },
            traditional: true
        });
    }

    var DeleteUser = function (id, username) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn  xóa tài khoản: <b>" + username + "</b>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Users/DeleteUser?q=" + id,
                    success: function (data) {
                        if (data.status == 1) {
                            location.reload();
                            swal(
                                'Xóa tài khoản thành công!'
                            )

                        } else {
                            App.ShowError(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }

    var GetDataByGroup = function (id) {
        $.ajax({
            async: true,
            type: "POST",
            url: '/Module/GetDataModule',
            data: { Id: id },
            beforeSend: function () {
                mApp.block('#m_tree_3', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'success',
                    message: 'Please wait...'
                });
            },
            dataType: "json",
            success: function (data) {
                loadData(data.data);
                mApp.unblock('#m_tree_3');
            }
        });
    }

    var GetMenuByGroup = function (id) {
        $.ajax({
            async: true,
            type: "POST",
            url: '/Users/GetMenuPermissonByGroup',
            data: { Id: id },
            beforeSend: function () {
                mApp.block('#m_tree_3', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'success',
                    message: 'Please wait...'
                });
            },
            dataType: "json",
            success: function (data) {
                loadData(data.data);
                mApp.unblock('#m_tree_3');
            }
        });
    }

    var GetMenuByUser = function (id) {
        $.ajax({
            async: true,
            type: "POST",
            url: '/Users/GetMenuPermisson',
            data: { Id: id },
            beforeSend: function () {
                mApp.block('#m_tree_3', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'success',
                    message: 'Please wait...'
                });
            },
            dataType: "json",
            success: function (data) {
                loadData(data.data);
                mApp.unblock('#m_tree_3');
            }
        });
    }

    var GetActionPermisson = function (id, groupId) {
        $.ajax({
            async: true,
            type: "POST",
            url: '/Users/GetActionPermisson',
            data: { Id: id, GroupId: groupId },
            beforeSend: function () {
                mApp.block('#boxActionPermisson', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'success',
                    message: 'Please wait...'
                });
            },
            dataType: "text",
            success: function (data) {
                mApp.unblock('#boxActionPermisson');
                $('#boxActionPermisson').html(data);

            }
        });
    }

    var GetActionPermissonGroup = function (groupId) {
        $.ajax({
            async: true,
            type: "POST",
            url: '/Users/GetActionPermissonGroup',
            data: { GroupId: groupId },
            beforeSend: function () {
                mApp.block('#boxActionPermisson', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'success',
                    message: 'Please wait...'
                });
            },
            dataType: "text",
            success: function (data) {
                mApp.unblock('#boxActionPermisson');
                $('#boxActionPermisson').html(data);

            }
        });
    }

    var loadData = function (jsondata) {
        $("#m_tree_3").jstree('destroy');
        $('#m_tree_3').on('changed.jstree', function (e, data) {

            var arr = [];
            var nodes = $('#m_tree_3').jstree('get_selected', true);
            $.each(nodes, function (index, item) {
                arr.push(item.id);
                arr.push(item.parent);
            });
            var removeItem = "#";
            arr = jQuery.grep(arr, function (value) {
                return value != removeItem;
            });
            var unique = arr.filter(function (itm, i, arr) {
                return i == arr.indexOf(itm);
            });
            $('#lstMenu').val(unique);
            console.log($('#lstMenu').val())

        }).jstree({
            'plugins': ["wholerow", "checkbox", "types"],
            'core': {
                "check_callback": false,
                "themes": {
                    "responsive": false
                },
                'data': jsondata
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder m--font-success"
                },
                "file": {
                    "icon": "fa fa-file  m--font-warning"
                }
            },
        });
    }

    var ShowPermissionData = function (id) {
        $('#bodyPermissionData').html('');
        $.ajax({
            type: "GET",
            url: "/Users/GetPermissionData?q=" + id,
            //data: { Id: id },
            success: function (data) {
                $('#bodyPermissionData').html(data);
                $('#modalPermissionData').modal('show');
                App.LibraryMoney();
            },
            traditional: true
        });
    }

    var ChangePassword = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            rules: {
                txt_change_pasword_old: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                txt_change_pasword_new: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    },
                    minlength: 6
                },
                txt_change_pasword_new_retype: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    },
                    minlength: 6
                }
            },
            messages: {
                txt_change_pasword_old: {
                    required: "Bạn chưa nhập mật khẩu cũ",
                    class: "has-danger",
                },

                txt_change_pasword_new: {
                    required: "Bạn chưa nhập mật khẩu mới",
                    minlength: "Mật khẩu ít nhất 6 ký tự"
                },
                txt_change_pasword_new_retype: {
                    required: "Bạn chưa nhập lại mật khẩu mới",
                    minlength: "Mật khẩu ít nhất 6 ký tự"
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        var passwordold = $('#txt_change_pasword_old').val();
        var passwordnew = $('#txt_change_pasword_new').val();
        var passwordnewretype = $('#txt_change_pasword_new_retype').val();
        if (passwordnew != passwordnewretype) {
            App.ShowErrorNotLoad("Mật khẩu mới không giống nhau");
            return;
        }
        if (passwordold == passwordnew) {
            App.ShowErrorNotLoad("Mật khẩu cũ và mật khẩu mới giống nhau");
            return;
        }
        var userId = 0;
        var type = 1;
        $.ajax({
            type: "POST",
            url: "/Users/ResetPassword",
            data: { Id: userId, type: type, PasswordOld: passwordold, PasswordNew: passwordnew },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    setTimeout(function () {
                        location.href = '../logout.html';
                    }, 2000);
                } else {
                    App.ShowErrorNotLoad(data.message);
                }
            },
            traditional: true
        });
    };

    var ChangeShopGlobal = function (shopId, shopName) {
        $.ajax({
            type: "POST",
            url: "/Users/ChangeShopGlobal",
            data: { ShopId: shopId, ShopName: shopName },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                } else {
                    App.ShowErrorNotLoad(data.message);
                }
            },
            traditional: true
        });
    };

    var ChangeInformation = function (userId, fullName, phone, email, typeCall, ipPhone, ciscoUserName, ciscoPassword, ciscoExtension) {
        $('#ddl_change_typeCallService').selectpicker();
        if (typeCall == 1) {
            $('#div-ipphone').removeAttr('style', 'display: none;');
            $('#div-cisco-usename').attr('style', 'display: none;');
            $('#div-cisco-password').attr('style', 'display: none;');
            $('#div-cisco-extension').attr('style', 'display: none;');
        }
        else if (typeCall == 2) {
            $('#div-ipphone').attr('style', 'display: none;');
            $('#div-cisco-usename').removeAttr('style', 'display: none;');
            $('#div-cisco-password').removeAttr('style', 'display: none;');
            $('#div-cisco-extension').removeAttr('style', 'display: none;');
        }
        else {
            $('#div-ipphone').attr('style', 'display: none;');
            $('#div-cisco-usename').attr('style', 'display: none;');
            $('#div-cisco-password').attr('style', 'display: none;');
            $('#div-cisco-extension').attr('style', 'display: none;');
        }
        $('#hdd_UserId').val(userId);
        $('#txt_change_fullname').val(fullName);
        $('#txt_change_phone').val(phone);
        $('#txt_change_email').val(email);
        $('#ddl_change_typeCallService').val(typeCall).change();
        $('#txt_change_ipphone').val(ipPhone);
        $('#txt_change_cisco_username').val(ciscoUserName);
        $('#txt_change_cisco_password').val(ciscoPassword);
        $('#txt_change_cisco_extension').val(ciscoExtension);
        $('#modal_change_information_user').modal('show');
    }

    var ChangeTypeCallService = function (typeCall) {
        if (typeCall == 1) {
            $('#div-ipphone').removeAttr('style', 'display: none;');
            $('#div-cisco-usename').attr('style', 'display: none;');
            $('#div-cisco-password').attr('style', 'display: none;');
            $('#div-cisco-extension').attr('style', 'display: none;');
        }
        else if (typeCall == 2) {
            $('#div-ipphone').attr('style', 'display: none;');
            $('#div-cisco-usename').removeAttr('style', 'display: none;');
            $('#div-cisco-password').removeAttr('style', 'display: none;');
            $('#div-cisco-extension').removeAttr('style', 'display: none;');
        }
        else {
            $('#div-ipphone').attr('style', 'display: none;');
            $('#div-cisco-usename').attr('style', 'display: none;');
            $('#div-cisco-password').attr('style', 'display: none;');
            $('#div-cisco-extension').attr('style', 'display: none;');
        }
    }

    var SaveChangeInformation = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        $(btn).attr('disabled', true);
        form.validate({
            ignore: [],
            rules: {
                Password: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                }
            },
            messages: {
                Password: {
                    required: "Bạn chưa nhập mật khẩu",
                    class: "has-danger",
                }
            }
        });
        if (!form.valid()) {
            $(btn).removeAttr('disabled', true);
            return;
        }
        form.ajaxSubmit({
            url: '/Users/SaveChangeInformation',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $(btn).removeAttr('disabled', true);
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    setTimeout(function () {
                        $('#modal_change_information_user').modal('hide');
                    }, 1000);
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    var ShowPermissionApi = function (userId, userName) {
        $('#bodyPermissionApiUser').html('');
        $.ajax({
            type: "GET",
            url: "/Users/GetPermissionApi",
            data: { userId: userId },
            success: function (data) {
                $('#bodyPermissionApiUser').html(data);
                $('#hdd_Permission_Api_UserId').val(userId);
                $('#_userName').text(userName);
                $('#modalPermissionApiUser').modal('show');
                App.LibraryMoney();
            },
            traditional: true
        });

    }

    var SaveActionPermissonApi = function (btn) {
        var form = $(btn).closest('form');
        $(btn).addClass('m-loader m-loader--right m-loader--light');
        form.ajaxSubmit({
            url: '/Users/SaveActionPermissonApi',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $(btn).removeClass('m-loader m-loader--right m-loader--light');
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }

    var ConfigCiscoMobile = function (userId) {
        $('#bodyConfigCiscoMobile').html('');
        $.ajax({
            type: "GET",
            url: "/Users/ConfigCiscoMobile",
            data: { userId: userId },
            success: function (data) {
                $('#bodyConfigCiscoMobile').html(data);
                $('#modalConfigCiscoMobile').modal('show');
                App.LibraryMoney();
            },
            traditional: true
        });
    }

    var SaveConfigCiscoMobile = function (btn) {
        var form = $(btn).closest('form');
        $(btn).addClass('m-loader m-loader--right m-loader--light');
        form.ajaxSubmit({
            url: '/Users/SaveConfigCiscoMobile',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                $(btn).removeClass('m-loader m-loader--right m-loader--light');
                if (data.status == 1) {
                    App.ShowSuccess(data.message);
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    }
    var Logout = function () {
        window.localStorage.removeItem("user_finesse");
    }

    return {
        Init: Init,
        LoadData: LoadData,
        AddUser: AddUser,
        ShowModelUser: ShowModelUser,
        DeleteUser: DeleteUser,
        GetMenuByUser: GetMenuByUser,
        GetDataByGroup: GetDataByGroup,
        ShowPermissionData: ShowPermissionData,
        GetActionPermisson: GetActionPermisson,
        GetMenuByGroup: GetMenuByGroup,
        GetActionPermissonGroup: GetActionPermissonGroup,
        ChangePassword: ChangePassword,
        ChangeShopGlobal: ChangeShopGlobal,
        ChangeInformation: ChangeInformation,
        SaveChangeInformation: SaveChangeInformation,
        ChangeTypeCallService: ChangeTypeCallService,
        ResetPassword: ResetPassword,
        ShowPermissionApi: ShowPermissionApi,
        SaveActionPermissonApi: SaveActionPermissonApi,
        ConfigCiscoMobile: ConfigCiscoMobile,
        SaveConfigCiscoMobile: SaveConfigCiscoMobile,
        Logout: Logout
    };
}
