﻿var UserModuleManager = new function () {
    var InitDataTable = function (objQuery) {
        var datatable = $('#tbUserManager').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: '/Users/GetListAccountOfTima',
                        params: {
                            query: objQuery
                        },
                    }
                },
                pageSize: 50,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'username',
                    title: 'Tài khoản',
                    sortable: false
                },
                {
                    field: 'fullName',
                    title: 'Họ tên',
                    sortable: false
                },
                {
                    field: 'phone',
                    title: 'Số điện thoại',
                    sortable: false
                },
                {
                    field: 'email',
                    title: 'Email',
                    sortable: false
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    sortable: false,
                    template: function (row) {
                        if (row.status == null)
                            row.status = 0;
                        var status = {
                            0: { 'title': 'Khóa', 'class': 'm-badge--metal' },
                            1: { 'title': 'Kích hoạt', 'class': 'm-badge--success' }
                        };
                        return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
                    }
                },
                {
                    field: 'group.groupName',
                    title: 'Nhóm tài khoản',
                    sortable: false
                }
                , {
                    field: 'Action',
                    title: 'Action',
                    sortable: false,
                    width: 100,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        var meta = '<a href="#"  class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only" onclick="UserModuleManager.LoadInforAccountTima(\'' + row.userId + '\',\'' + row.username + '\')">\
                                        <i class="la la-edit"></i></a>\
	                                <a href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only" title="Reset mật khẩu"  onclick="UserModuleManager.ResetPassword(\''+ row.userId + '\',\'' + row.username + '\')" >\
                                        <i class="la la-refresh"></i></a>';
                        if (row.status == 1) {
                            meta += '  <a href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only" title="Khóa" onclick="UserModuleManager.ChangeStatus(\'' + row.userId + '\',\'' + row.username + '\', 0)" >\
                                    <i class="fa fa-lock"></i></a>';
                        }
                        else if (row.status == 0) {
                            meta += '  <a href="#" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only" title="Kích hoạt" onclick="UserModuleManager.ChangeStatus(\'' + row.userId + '\',\'' + row.username + '\', 1)" >\
                                    <i class="fa fa-unlock"></i></a>';
                        }

                        return meta;
                    }
                }
            ]
        });
    }
    var LoadData = function () {
        var objQuery = {
            groupId: $('#filterGroup').val(),
            status: $('#filterStatus').val(),
            filterName: $('#filterName').val().trim()
        }
        InitDataTable(objQuery);
    }
    var Init = function () {
        $('#filterGroup,#filterStatus,#ddl_TypeCallService').selectpicker();
        LoadData();
        $('#filterGroup, #filterStatus').on('change', function () {
            LoadData();
        });
        $("#filterName").on('keydown', function (e) {
            if (e.which == 13) {
                LoadData();
                return false;
            }
        });
        $('#ddl_GroupID').select2();
        $(document).on('change', '#ddl_GroupID', function () {
            var groupId = $(this).val();
            $("#ddl_GroupID-error").hide();
            GetMenuByGroup(groupId);
            $('#errorGroupId').hide();
            GetActionPermissonGroup(groupId);
        });

    };
    var AddUser = function (btn, e) {
        var userId = $('#hdd_create_userID').val();
        e.preventDefault();
        var form = $(btn).closest('form');
        if (userId > 0) {
            form.validate({
                ignore: [],
                rules: {
                    Username: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                    Phone: {
                        validPhone: true
                    },
                    Email: {
                        required: true,
                        validEmail: true
                    },
                    GroupId:
                    {
                        required: true
                    }
                },
                messages: {
                    Username: {
                        required: "Bạn chưa nhập tài khoản",
                        class: "has-danger"
                    },
                    Phone: {
                        validPhone: "Số điện thoại không hợp lệ",
                    },
                    Email: {
                        required: "Bạn chưa nhập email",
                        validEmail: "Email không hợp lệ",
                    },
                    GroupId: {
                        required: "Bạn chưa chọn nhóm tài khoản",
                    }
                }
            });
        }
        else {
            form.validate({
                ignore: [],
                rules: {
                    Username: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                    Password: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        },
                        minlength: 6
                    },
                    Phone: {
                        validPhone: true
                    },
                    Email: {
                        required: true,
                        validEmail: true
                    },
                    GroupId:
                    {
                        required: true
                    }
                },
                messages: {
                    Username: {
                        required: "Bạn chưa nhập tài khoản",
                        class: "has-danger"
                    },
                    Password: {
                        required: "Bạn chưa nhập mật khẩu",
                        minlength: "Mật khẩu phải chứa ít nhất 6 ký tự"
                    },
                    Phone: {
                        validPhone: "Số điện thoại không hợp lệ",
                    },
                    Email: {
                        required: "Bạn chưa nhập email",
                        validEmail: "Email không hợp lệ",
                    },
                    GroupId: {
                        required: "Bạn chưa chọn nhóm tài khoản",
                    }
                },
                invalidHandler: function (e, validation) {
                    if (validation.errorList != null && validation.errorList.length > 0) {
                        App.ShowErrorNotLoad(validation.errorList[0].message)
                    }
                }
            });
        }
        if (!form.valid()) {
            return;
        }
        form.ajaxSubmit({
            url: '/Users/CreateUser',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccess(data.message);
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };
    var LoadInforAccountTima = function (userId, userName) {
        var form = $('#form_create_account_tima').closest('form');
        var validator = form.validate();
        validator.destroy();
        $('#hdd_create_userID').val(userId);
        $('#txt_create_staff_username').val('');
        $('#txt_create_staff_fullname').val('');
        $('#txt_create_staff_phone').val('');
        $('#txt_create_staff_email').val('');
        $('#ddl_GroupID').val('').change();
        if (userId == 0) {
            $('#header_create_account_tima').html('Thêm mới nhân viên');
            $('#btn_save_account_tima').html('<i class="la la-save"></i>Thêm mới');
            $('#div-password-account').removeAttr('style', 'display: none;');
            $('#ddl_HubId option:selected').removeAttr('selected', 'selected')
            $('#ddl_HubId').select2();
        } else {
            $('#header_create_account_tima').html('Cập nhật tài khoản <b>' + userName + '</b>');
            $('#btn_save_account_tima').html('<i class="la la-save"></i>Cập nhật');
            $('#div-password-account').attr('style', 'display: none;');
            $.ajax({
                type: "Get",
                url: "/Users/GetUserById",
                data: { Id: userId },
                success: function (data) {
                    if (data.status == 1) {
                        $('#txt_create_staff_username').val(data.data.username);
                        $('#txt_create_staff_fullname').val(data.data.fullName);
                        $('#txt_create_staff_phone').val(data.data.phone);
                        $('#txt_create_staff_email').val(data.data.email);
                        $('#ddl_GroupID').val(data.data.groupId).change();
                        var appenOption = '';
                        if (data.data.listHub != null && data.data.listHub.length > 0) {
                            var arrListhub = new Array();
                            for (var j = 0; j < data.data.listHub.length; j++) {
                                arrListhub.push(data.data.listHub[j].shopId)
                            }
                            for (var i = 0; i < data.data.listAllHub.length; i++) {
                                if (arrListhub.includes(data.data.listAllHub[i].shopId)) {
                                    appenOption += '<option selected="selected" value="' + data.data.listAllHub[i].shopId + '">' + data.data.listAllHub[i].name + '</option>'
                                }
                                else {
                                    appenOption += '<option value="' + data.data.listAllHub[i].shopId + '">' + data.data.listAllHub[i].name + '</option>';
                                }
                            }
                            $('#ddl_HubId').html('');
                            $('#ddl_HubId').html(appenOption).select2();
                        }
                        else {
                            for (var i = 0; i < data.data.listAllHub.length; i++) {
                                appenOption += '<option value="' + data.data.listAllHub[i].shopId + '">' + data.data.listAllHub[i].name + '</option>';
                            }
                            $('#ddl_HubId').html('');
                            $('#ddl_HubId').html(appenOption).select2();
                        }
                    }
                    else {
                        App.ShowError(data.message);
                    }
                },
                traditional: true
            });
        }
        $('#modal_create_account_tima').modal('show');
    }
    var ResetPassword = function (userId, userName) {
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn  đặt lại mật khẩu : <b>" + userName + "</b> về mặc định <br /> <span style='font-size: 12px; color: #909090;'>(Mật khẩu mặc định: Tima@2021)</span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/Users/ResetPassword",
                    data: { Id: userId },
                    success: function (data) {
                        if (data.status == 1) {
                            App.ShowSuccessNotLoad(data.message);
                        } else {
                            App.ShowError(data.message);
                        }
                    },
                    traditional: true
                });
            }
        });
    }
    var ChangeStatus = function (userId, userName, status) {
        var mes = "";
        if (status == 1)
            mes = "kích hoạt";
        else
            mes = "khóa"
        swal({
            title: '<b>Cảnh báo</b>',
            html: "Bạn có chắc chắn muốn <b>" + mes + "</b> tài khoản : <b>" + userName + "</b></span>",
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            cancelButtonText: 'Hủy',
            onOpen: function () {
                $('.swal2-cancel').width($('.swal2-confirm').width());
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                        });
                    },
                    url: "/Users/ChangeStatus",
                    data: { Id: userId, Status: status },
                    success: function (data) {
                        $.unblockUI();
                        if (data.status == 1) {
                            App.ShowSuccess(data.message);
                        } else {
                            App.ShowError(data.message);
                        }
                    },
                    traditional: true
                });
            }
        });
    }
    var GetMenuByGroup = function (id) {
        $.ajax({
            async: true,
            type: "POST",
            url: '/Users/GetMenuPermissonByGroup',
            data: { Id: id },
            beforeSend: function () {
                mApp.block('#m_tree_3', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'success',
                    message: 'Please wait...'
                });
            },
            dataType: "json",
            success: function (data) {
                loadData(data.data);
                mApp.unblock('#m_tree_3');
            }
        });
    }
    var GetActionPermissonGroup = function (groupId) {
        $.ajax({
            async: true,
            type: "POST",
            url: '/Users/GetActionPermissonGroup',
            data: { GroupId: groupId },
            beforeSend: function () {
                mApp.block('#boxActionPermisson', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'success',
                    message: 'Please wait...'
                });
            },
            dataType: "text",
            success: function (data) {
                mApp.unblock('#boxActionPermisson');
                $('#boxActionPermisson').html(data);

            }
        });
    }
    var loadData = function (jsondata) {
        $("#m_tree_3").jstree('destroy');
        $('#m_tree_3').on('changed.jstree', function (e, data) {

            var arr = [];
            var nodes = $('#m_tree_3').jstree('get_selected', true);
            $.each(nodes, function (index, item) {
                arr.push(item.id);
                arr.push(item.parent);
            });
            var removeItem = "#";
            arr = jQuery.grep(arr, function (value) {
                return value != removeItem;
            });
            var unique = arr.filter(function (itm, i, arr) {
                return i == arr.indexOf(itm);
            });
            $('#lstMenu').val(unique);
            console.log($('#lstMenu').val())

        }).jstree({
            'plugins': ["wholerow", "checkbox", "types"],
            'core': {
                "check_callback": false,
                "themes": {
                    "responsive": false
                },
                'data': jsondata
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder m--font-success"
                },
                "file": {
                    "icon": "fa fa-file  m--font-warning"
                }
            },
        });
    }
    var ShowAndHideChangeHub = function (groupId) {
        if (groupId == 44 || groupId == 45 || groupId == 48) {
            $('#div-changhub').removeAttr('style', 'display: none;')
        }
        else {
            $('#div-changhub').attr('style', 'display: none;')
        }
    }
    return {
        Init: Init,
        LoadData: LoadData,
        AddUser: AddUser,
        LoadInforAccountTima: LoadInforAccountTima,
        ResetPassword: ResetPassword,
        ChangeStatus: ChangeStatus,
        GetMenuByGroup: GetMenuByGroup,
        GetActionPermissonGroup: GetActionPermissonGroup,
        ShowAndHideChangeHub: ShowAndHideChangeHub
    };
}
