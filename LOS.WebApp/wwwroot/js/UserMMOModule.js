﻿var UserMmoModule = new function () {
    var Init = function () {

        var datatable = $('.m-datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: '/UserMmo/LoadData'
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 50,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                }
                //,
                //{
                //    field: 'userName',
                //    title: 'Tài khoản',
                //    sortable: false
                //}
                ,
                {
                    field: 'fullName',
                    title: 'Họ tên',
                    sortable: false
                },
                {
                    field: 'address',
                    title: 'Địa chỉ',
                    sortable: false
                },
                {
                    field: 'phone',
                    title: 'Số điện thoại',
                    sortable: false
                },
                {
                    field: 'email',
                    title: 'Email',
                    sortable: false
                },
                {
                    field: 'Affiliate Link',
                    title: 'Affiliate Link',
                    sortable: false,
                    width: 250,
                    template: function (row) {
                        var url = "http://tima.vn/Borrower/?aff=" + row.userId;
                        return '<a href="' + url + '">' + url + ' </a>';
                    }
                },
                {
                    field: 'isActive',
                    title: 'Trạng thái',
                    sortable: false,
                    template: function (row) {
                        if (row.isActive == true) {
                            return '<span class="m-badge m-badge--success m-badge--wide"> Hoạt động </span>';
                        } else {
                            return '<span class="m-badge m-badge--metal m-badge--wide"> Khóa </span>';
                        }

                    }
                }
                , {
                    field: 'Action',
                    title: 'Action',
                    sortable: false,
                    width: 100,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return '\
							<a href="#" class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only" onclick="UserMmoModule.ShowModelUserMmo('+ row.userId + ')">\
                                <i class="la la-edit"></i></a>\
	                        <a href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only"  onclick="UserMmoModule.DeleteUserMmo('+ row.userId + ',\'' + row.userName + '\')" >\
                                <i class="la la-times"></i></a>';
                    }
                }
            ]
        });

        $('#filterIsActive').on('change', function () {
            datatable.API.params.pagination.page = 1;
            datatable.search($(this).val().toLowerCase(), 'isActive');
        });

        $("#filterName").on('keydown', function (e) {
            if (e.which == 13) {
                datatable.API.params.pagination.page = 1;
                datatable.search($(this).val().toLowerCase(), 'filterName');
                return false;
            }
        });

        $('#filterIsActive, #filterStatus').selectpicker();

    };
    var AddUserMmo = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            focusInvalid: false,
            rules: {
                //UserName: {
                //    required: true,
                //    normalizer: function (value) {
                //        return $.trim(value);
                //    }
                //},
                FullName: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                Email: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    },
                    email: true
                },
                Phone: {
                    required: true,
                    //matches: "^(\\d|\\s)+$",
                    number: true,
                    minlength: 10,
                    maxlength: 11,
                },
                Password: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                Cmt: {
                    required: true,
                }
            },
            messages: {
                //UserName: {
                //    required: "Bạn chưa nhập tài khoản",
                //    class: "has-danger"
                //},
                FullName: {
                    required: "Bạn chưa nhập họ tên",

                },
                Email: {
                    required: "Bạn chưa nhập email",
                    email: "Định dạng email không đúng"
                },
                Phone: {
                    // matches: "^(\\d|\\s)+$",
                    number: "Số điện thoại không hợp lệ",
                    minlength: "Số điện thoại không hợp lệ ",
                    maxlength: "Số điện thoại không hợp lệ",
                }, Password: {
                    required: "Bạn chưa nhập mật khẩu",
                }, Cmt: {
                    required: "Bạn chưa nhập số CMND/Thẻ căn cước",
                }
            },
            
        });
        if (!form.valid()) {
            return;
        }
        form.ajaxSubmit({
            url: '/UserMmo/CreateUser',
            method: 'POST',
            success: function (data, status, xhr, $form) {
                if (data != undefined) {
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (data.status == 1) {
                        App.ShowSuccess(data.message);
                        $('#create-user').modal('toggle');
                    }
                    else {
                        App.ShowError(data.message);
                    }
                }
                else {
                    App.ShowError(data.message);
                    $(btn).removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            }
        });
    };

    var ShowModelUserMmo = function (id) {
        $('#modelUserMmo').html('');
        $.ajax({
            type: "POST",
            url: "/UserMmo/CreateUserMmoModal",
            data: { Id: id },
            success: function (data) {
                $('#modelUserMmo').html(data);
                $('#m_modal_4').modal('show');
            },
            traditional: true
        });
    }
    var DeleteUserMmo = function (id, username) {
        swal({
            title: 'Cảnh báo',
            text: "Bạn có chắc chắn muốn  xóa tài khoản " + username + "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xoá'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/UserMmo/DeleteUser",
                    data: { Id: id },
                    success: function (data) {
                        if (data.status == 1) {
                            location.reload();
                            swal(
                                'Xóa tài khoản thành công!'
                            )

                        } else {
                            App.ShowError(data.message);
                        }

                    },
                    traditional: true
                });

            }
        });
    }
    return {
        Init: Init,
        AddUserMmo: AddUserMmo,
        ShowModelUserMmo: ShowModelUserMmo,
        DeleteUserMmo: DeleteUserMmo
    };
}

