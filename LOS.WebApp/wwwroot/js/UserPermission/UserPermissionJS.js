﻿var UserPermissionJS = new function () {
    var properties;
    var Init = function () {

    };
    var AssignValues = function (_properties) {
        properties = _properties;
    };

    var OnAddInitialProperty = function () {
        var length = $('.initial-value-container').length;
        App.AjaxSuccess("POST", "/Users/ActionPropertyPartial", { index: length }, function (data) {
            var container = $("#initialValuesContainer");
            container.append(data);
            $('.m_selectpicker').selectpicker();
        }, function () { });
    }

    var OnAddConditionProperty = function () {
        var length = $('.condition-value-container').length;
        App.AjaxSuccess("POST", "/Users/ConditionValuePropertyPartial", { index: length }, function (data) {
            var container = $("#conditionValuesContainer");
            container.append(data);
            $('.m_selectpicker').selectpicker();

        }, function () { });
    }

    var OnConditionPropertyChanged = function (tthis) {
        var container = $(tthis).parents().parents().closest('.condition-value-container');
        var propertyId = parseInt(container.find("select").val());
        if (propertyId > 0) {
            for (var i = 0; i < properties.length; i++) {
                if (properties[i].propertyId == propertyId) {
                    App.AjaxSuccess("POST", "/Users/GetPropertyPartial", { id: propertyId }, function (data) {
                        container.find(".valueContainer").html("");
                        container.find(".valueContainer").html(data);
                        $('.m_selectpicker').selectpicker();
                        App.LibraryMoney();
                    }, function () { });
                    break;
                }
            }
        }
    };

    var RemoveConditionProperty = function (tthis) {
        $(tthis).parents('div').parents('div').closest('.condition-value-container').remove();
    };

    var RemoveActionPermission = function (tthis) {
        $(tthis).parents('div').parents('div').closest('.action-value-container').remove();
    };

    var OnSavePermissionUser = function () {
        var userId = $('#hdf_UserId').val();
        var conditionContainers = $('.condition-value-container');
        var conditions = new Array();
        if (conditionContainers != undefined && conditionContainers.length > 0) {
            conditionContainers.each(function (index, element) {
                var condition = {};
                var container = $(element);
                var propertyId = container.find("select.property").val();
                var property;
                for (var j = 0; j < properties.length; j++) {
                    if (properties[j].propertyId == propertyId) {
                        property = properties[j];
                        break;
                    }
                }
                if (property != undefined) {
                    condition.PropertyId = parseInt(propertyId);
                    condition.Operator = container.find("select.conditionOperator").val();
                    if (container.find("select.propertyValue").length)
                        condition.Value = container.find("select.propertyValue").val();
                    else
                        condition.Value = container.find(".propertyValue").val();
                    if (condition.Value != undefined && condition.Value.length > 0 && Array.isArray(condition.Value))
                        condition.Value = condition.Value.join(',');
                    else
                        condition.Value = condition.Value.replace(/,/g, '');
                    if (condition.PropertyId > 0 && condition.Value != undefined)
                        conditions.push(condition);
                }
            });
        }
        if (conditions == undefined || conditions.length == 0) {
            App.ShowErrorNotLoad("Vui lòng phân quyền đữ liệu");
            return;
        }
        var actionContainers = $('.action-value-container');
        var actions = new Array();
        if (actionContainers != undefined && actionContainers.length > 0) {
            actionContainers.each(function (index, element) {
                var action = {};
                var container = $(element);
                var loanBriefStatus = container.find("select.property").val();
                action.PropertyId = parseInt(loanBriefStatus);
                var totalAction = 0;
                var actionElement = container.find("input:checkbox:checked");
                if (actionElement != undefined && actionElement.length > 0) {
                    $(actionElement).each(function (index, element) {
                        if ($(this).val() > 0)
                            totalAction += parseInt($(this).val());
                    })
                    if (totalAction > 0) {
                        action.Value = totalAction;
                    }
                }
                if (action.PropertyId > 0 && action.Value > 0)
                    actions.push(action);
            });
        }

        var formData = {
            UserId: parseInt(userId),
            ActionUsers: actions,
            ConditionUsers: conditions
        };
        $.ajax({
            url: '/Users/UpdatePermissionUser',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(formData),
            success: function (data) {
                if (data.isSuccess == 1) {
                    App.ShowSuccess(data.message);
                } else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });
    };

    return {
        Init: Init,
        AssignValues: AssignValues,
        OnAddInitialProperty: OnAddInitialProperty,
        OnAddConditionProperty: OnAddConditionProperty,
        OnConditionPropertyChanged: OnConditionPropertyChanged,
        RemoveConditionProperty: RemoveConditionProperty,
        RemoveActionPermission: RemoveActionPermission,
        OnSavePermissionUser: OnSavePermissionUser
    };
};