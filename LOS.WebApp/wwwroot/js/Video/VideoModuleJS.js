﻿var VideoModuleJS = new function () {

    var InitModal = function (url) {
        $('#bodyModalVideo').html('');
        //$('#modelInitLoanBrief').modal('hide');
        $.ajax({
            type: "GET",
            url: "/LoanBrief/VideoInit",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="m-blockui "><span style="font-size: 15px;">Please wait...</span><span><div class="m-loader  m-loader--success m-loader--lg"></div></span></div>',
                });
            },
            data: { Url: url },
            success: function (data) {
                $.unblockUI();
                $('#bodyModalVideo').html(data);
                $('#modalVideo').modal('show');
            },
            error: function () {
                $.unblockUI();
                App.ShowErrorNotLoad('Xảy ra lỗi. Vui lòng thử lại');
            }
        });
    }
    var rotate = 0;
    var Rotate = function (type) {
        var v = document.getElementsByTagName('video')[0];
        if (type == 1)
            rotate = rotate - 90;
        else if (type == 2)
            rotate = rotate + 90;
        else
            rotate = 0;
        $('#video').css("transform", "rotate(" + rotate + "deg)");

    };

    return {
        InitModal: InitModal,
        Rotate: Rotate
    };
};