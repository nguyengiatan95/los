﻿var finesseC2CModule = new function () {
    var
        //Private finesse object.
        _finesse,
        //Store agent information
        _username, _password, _extension, _domain, _login, _state, _callIdMake, _phoneCall,
        //Private reference to JabberWerx eventing object.
        _jwClient;

    var _signInHandler = function (data, statusText, xhr) {
        //Ensure success.
        SaveLogLoginFinesse(_username, _password, _extension, xhr.status);
        if (xhr.status === 202) {
            //Hide signin forms and show actions.
            console.log("signin success");
            //window.sessionStorage.clear();
            //window.sessionStorage.setItem("user_finesse", _username);
            window.localStorage.removeItem('user_finesse')
            window.localStorage.setItem('user_finesse', _username)
            console.log("save session storage");
        } else {
            console.log("signin error");
            alert("Xảy ra lỗi khi đăng nhập finesse để click to call");
        }
        
    };

    var _makeCallHandler = function (data, statusText, xhr) {
        //Validate success.
        if (statusText === "success") {
            console.log("make call success");
        }
    };

    var _handler = function (data, statusText, xhr) {
        if (xhr) {
            console.log("RESPONSE " + xhr.status);
        } else {
            console.log("RESPONSE " + data);
        }
    };

    var LoginFinesse = function (username, password, extension, domain) {
        _username = username;
        _password = password;
        _extension = extension;
        _domain = domain;
        //Check non-empty fields
        if (!_username || !_password || !_extension || !_domain) {
            console.log("Please enter valid domain and credentials.");
        } else {
            _finesse = new Finesse(_username, _password);
            //var userSession = window.localStorage.getItem('user_finesse');
            //if (userSession == null) {
            //    _finesse.signIn(_username, _extension, true, _signInHandler, _signInHandler);
            //} 
            getState();
        }
    };

    var SaveLogClickToCall = function (loanbriefId, phone) {
        $.ajax({
            type: "POST",
            url: "/Loanbrief/LogClickToCall",
            data: { loanbriefId: loanbriefId, phone: phone }
        }).done(function (data) {
        });
    };

    var SaveLogLoginFinesse = function (username, password, extension, statusResponse) {
        $.ajax({
            type: "POST",
            url: "/Loanbrief/LogLoginFinesse",
            data: { UserName: username, Password: password, Extension: extension, StatusResponse: statusResponse}
        }).done(function (data) {
        });
    };

    var ClickToCall = function (phoneNumber, loanbriefId) {
        _finesse.makeCall(phoneNumber, _extension, _makeCallHandler, _handler);
        SaveLogClickToCall(loanbriefId,  phoneNumber);
    };

    var ClickToCallNoLog = function (phoneNumber) {
        _finesse.makeCall(phoneNumber, _extension, _makeCallHandler, _handler);
    };

    function _getStateHandler(data) {
        var state = $(data.xml).find("state");
        if (state.text() !== "") {
            _state = state.text();
            console.log("State: ", _state);
            if (_state == "LOGOUT") {
                _finesse.signIn(_username, _extension, true, _signInHandler, _signInHandler);
            }
        }
    }
    function getState() {
        _finesse.getState(_username, _getStateHandler, _handler);
    }
    return {
        LoginFinesse: LoginFinesse,
        ClickToCall: ClickToCall,
        ClickToCallNoLog: ClickToCallNoLog
    };
}