﻿var
    //Private finesse object.
    _finesse,
    //Store agent information
    _username, _password, _extension, _domain, _login, _state, _callIdMake, _phoneCall, _loanbriefId,
    //Private reference to JabberWerx eventing object.
    _jwClient;
//var audio = new Audio('zalo_ringtone.mp3');

function LoadScript(phone, loanbriefId) {
    $('#ScriptTelesaleBody').html('');
    $.ajax({
        url: '/LoanBrief/AcceptCall',
        type: 'GET',
        data: { 'phone': phone, 'loanbriefId': Number(loanbriefId)},
        dataType: 'json',
        success: function (res) {
            if (res.isSuccess == 1) {
                $('#ScriptTelesaleBody').html(res.html);
                $('#modelScriptTelesales').modal('show');
            } else {
                App.ShowErrorNotLoad(res.message);
            }
        },
        error: function () {

        }
    });
}

function LoadLoanInfo(phone, loanbriefId) {
    $.ajax({
        url: '/Telesales/LoanBriefInfo',
        type: 'GET',
        data: { 'phone': phone, 'loanbriefId': Number(loanbriefId) },
        dataType: 'text',
        success: function (response) {
            $('#boxAutoLoanInfo').html(response);
            //if (csVoice.callInfo != null) {              
            $('#box-icon-call').html('<i class="fa fa-angle-double-right icon-call"></i>');
            $('#box-icon-call').show();
                //if (csVoice.callInfo.callDirection == 1) {
                //    $('#box-icon-call').html('<i class="fa fa-angle-double-right icon-call"></i>');
                //} else {
                //    $('#box-icon-call').html('<i class="fa fa-angle-double-left icon-call"></i>');
                //}
            //}
        },
        error: function () {
        }
    });
}

function EditInfomation() {
    var phone = $('#txtPhoneAccept').val();
  var loanbriefId =  $('#txtLoanbriefId').val();
    if (phone != "" || loanbriefId != "")
        LoadScript(phone, loanbriefId);
}


function replaceAll(str, stringToReplace, replaceWith) {
    var result = str, index = 1;
    while (index > 0) {
        result = result.replace(stringToReplace, replaceWith);
        index = result.indexOf(stringToReplace);
    }
    return result;
}

/**
 * Print text to console output.
 */
function print2Console(type, data) {
    //var date = new Date(),
    //    xml = null,
    //    consoleArea = $("#console-area");

    //if (type === "EVENT") {
    //    xml = data.data._DOM.xml;
    //    xml = replaceAll(xml, "&lt;", "<");
    //    xml = replaceAll(xml, "&gt;", ">");

    //    consoleArea.val(consoleArea.val() + "[" + date.getTime() + "] [" +
    //        type + "] " + xml + "\n\n");
    //} else {
    //    //Convert data object to string and print to console.
    //    consoleArea.val(consoleArea.val() + "[" + date.getTime() + "] [" +
    //        type + "] " + data + "\n\n");
    //}

    ////Scroll to bottom to see latest.
    //consoleArea.scrollTop(consoleArea[0].scrollHeight);
    //consoleArea = null;
}

function onClientError(rsp) {
    //print2Console("ERROR " + rsp);
}

/**
 * Event handler that prints events to console.
 */
function _eventHandler(data) {
    //print2Console("EVENT", data);
     data = data.selected.firstChild.data;
    console.log(data);
    //print2Console("MYEVENT", data);
    var sEvent = $(data).find("event");
    //xử lý lấy sdt để hiển thị kịch bản tương ứng
    var phone = $(data).find("dialedNumber");
    var callid = $(data).find("id");
    var state = $(data).find("state");
   
    if (state != null && state.text() == "NOT_READY") {
        $("#swWorking").bootstrapSwitch('state', false);
       // _state = "READY";
        _state = "NOT_READY";
        console.log("State: " + _state);
    }
    var calltype = $(data).find("callType");
    //Cuộc gọi foword từ autocall
    if (calltype.text() == "ACD_IN" && sEvent.text() == "POST") {
        phone = $(data).find("fromAddress").text();
        var toAddress = $(data).find("toAddress").text();
        if (phone != "") {//&& (toAddress == "1805" || toAddress == "1800")
            var loanbriefId = $($(data).find('CallVariable')[5]).find('value').text();
           // if (loanbriefId != "" && Number(loanbriefId) > 0) {
                var textCallId = callid.text();
                if (phone !== "" && textCallId !== "") {
                    _phoneCall = phone;
                    console.log("KH FOROWORD PHONE: " + _phoneCall);
                    if (_phoneCall.length == 13)
                        _phoneCall = _phoneCall.substring(3, 13);
                    console.log("KH FOROWORD PHONE substring: " + _phoneCall);
                    $('#field-phone-customer').text(_phoneCall);
                    console.log("KH ANSWER");
                    //nạp callId
                    _callIdMake = Number(textCallId);
                    _loanbriefId = Number(loanbriefId);
                    //nap sdt  
                    $("#field-call-control-callid").val(textCallId);
                    $('#txtPhoneAccept').val(_phoneCall);
                    $('#txtLoanbriefId').val(_loanbriefId);
                    LoadLoanInfo(_phoneCall, _loanbriefId);
               
                    //alert('KH ACCEPT')
                }
            //}            
        }
        
    } else {
        var action = $(data).find("action").first();
        //Xử lý lấy callId để accept cuộc gọi
        if (phone.text() !== "" && callid.text() !== "" && (action.text() == "ANSWER" || calltype.text() == "OUTBOUND")) {
            _phoneCall = phone.text();
            var loanbriefId = 0;
            if ($(data).find('CallVariable').length >= 12) {
                loanbriefId = $($(data).find('CallVariable')[12]).find('value').text();
                _loanbriefId = Number(loanbriefId);
            } 
            console.log("KH PHONE: " + _phoneCall);
            if (_phoneCall.length == 13)
                _phoneCall = _phoneCall.substring(3, 13);
            console.log("KH PHONE substring: " + _phoneCall);
            $('#field-phone-customer').text(_phoneCall);
            console.log("KH ANSWER");
            //nạp callId
            _callIdMake = Number(callid.text());
            //nap sdt     

            $("#field-call-control-callid").val(callid.text());

            $('#btnCalling').show();
            $('#btnEndCall').hide();
            $('#txtPhoneAccept').val(_phoneCall);
            $('#txtLoanbriefId').val(_loanbriefId);
            LoadLoanInfo(_phoneCall, _loanbriefId);

            //alert('KH ACCEPT')
        }
    }
    
    //EVENT KH CANCEL
    if (phone.text() !== "" && callid.text() !== "" && sEvent.text() == "DELETE") {
        CustomerCancel();
    }
}


function CustomerCancel() {
    console.log("KH CANCEL");
    $('#btnCalling').hide();
    $('#btnEndCall').hide();
    $('#txtPhoneAccept').val('');
    $('#txtLoanbriefId').val('');
    $('#boxAutoLoanInfo').html('<p style="font-size: 30px;color: white;text-align: center; padding-top: 30px;">Chờ nhận cuộc gọi</p>');
    $('#callTimer').html('00:00');
    $('#callTimer').hide();
    $('#box-icon-call').hide();
    //hủy đồng hồ đếm giờ
    if (go != undefined && go != null)
        clearTimeout(go);
    //hủy sdt
    _phoneCall = null;
    _loanbriefId = null;
    //hủy callIdMake
    _callIdMake = null;
}
//telesale nhận cuộc gọi tới KH
function telesaleAnswer() {
    //kết nối nhận cuộc gọi
    _finesse.answerCall(_callIdMake, _extension, _handler, _handler);
    //hủy bỏ luôn callIdMake
    //_callIdMake = null;
    //hiển thị kịch bản
    console.log("Call is Accepted");
    $('#btnCalling').hide();
    $('#btnEndCall').show();
    var phone = _phoneCall;
    //var phone = $('#txtPhoneAccept').val();
    if (phone != "")
        LoadScript(phone, _loanbriefId);
    TelesaleDashboardJS.StartClock();

}

function telesaleCancel() {
    //hủy kết nối tới cuộc gọi
    _finesse.dropCall(_callIdMake, _extension, _handler, _handler);
    //hủy sdt
    _phoneCall = null;
    _loanbriefId = null;
    //hủy callIdMake
    _callIdMake = null;
    console.log("Call is Cancel");

    $('#btnCalling').hide();
    $('#btnEndCall').hide();
    $('#txtPhoneAccept').val('');
    $('#txtLoanbriefId').val('');
    $('#boxAutoLoanInfo').html('<p style="font-size: 30px;color: white;text-align: center; padding-top: 30px;">Chờ nhận cuộc gọi</p>');
    $('#callTimer').html('00:00');
    $('#callTimer').hide();
    $('#box-icon-call').hide();
    //hủy đồng hồ đếm giờ
    if (go != undefined && go != null)
        clearTimeout(go);
}

/**
 * Connects to the BOSH connection. Any XMPP library or implementation can be
 * used to connect, as long as it conforms to BOSH over XMPP specifications. In
 * this case, we are using Cisco's Ajax XMPP library (aka JabberWerx). In order
 * to make a cross-domain request to the XMPP server, a proxy should be
 * configured to forward requests to the correct server.
 */
function _eventConnect() {
    if (window.jabberwerx) {
        var
            //Construct JID with username and domain.
            jid = _username + "@" + _domain,

            //Create JabbwerWerx object.
            _jwClient = new jabberwerx.Client("cisco");

        //Arguments to feed into the JabberWerx client on creation.
        jwArgs = {
            //Defines the BOSH path. Should match the path pattern in the proxy
            //so that it knows where to forward the BOSH request to.
            //httpBindingURL: "https://proxy.tima.local:8083/http-bind/", // local test
            //httpBindingURL: "https://ciscoproxy.tima.vn/http-bind/", // server test
            httpBindingURL: "https://ciscoagproxy.tima.vn/http-bind/", 
            //Calls this function callback on successful BOSH connection by the
            //JabberWerx library.
            errorCallback: onClientError,
            successCallback: function () {
                //Get the server generated resource ID to be used for subscriptions.
                _finesse.setResource(_jwClient.resourceName);
            }
        };


        jabberwerx._config.unsecureAllowed = true;
        //Bind invoker function to any events that is received. Only invoke
        //handler if XMPP message is in the specified structure.
        _jwClient.event("messageReceived").bindWhen(
            "event[xmlns='http://jabber.org/protocol/pubsub#event'] items item notification",
            _eventHandler);
        _jwClient.event("clientStatusChanged").bind(function (evt) {

            if (evt.data.next == jabberwerx.Client.status_connected) {
                // attempt to login the agent
                _finesse.signIn(_username, _extension, true, _signInHandler, _signInHandler);
            } else if (evt.data.next == jabberwerx.Client.status_disconnected) {
                _finesse.signOut(_username, _extension, null, _signOutHandler, _signOutHandler);
            }
        });

        //Connect to BOSH connection.
        _jwClient.connect(jid, _password, jwArgs);
    } else {
        console.log("CAXL library not found. Please download from http://developer.cisco.com/web/xmpp/resources")
    }
}

/**
 * Disconnects from the BOSH connection.
 */
function _eventDisconnect() {
    if (_jwClient) {
        _jwClient.disconnect();
        _jwClient = null;
    }
}

/**
 * Generic handler that prints response to console.
 */
function _handler(data, statusText, xhr) {
    if (xhr) {
        print2Console("RESPONSE", xhr.status);
    } else {
        print2Console("RESPONSE", data);
    }
}
//xử lý khi thay đổi state
/**
 * GetState handler that prints response to console.
 */
function _getStateHandler(data) {
    print2Console("RESPONSE", data.xml);
    var state = $(data.xml).find("state");
    if (state.text() !== "") {
        _state = state.text();
        showStateAgent();
    }
}

/**
 * Handler for the make call that will validate the response and automatically
 * store the call id retrieve from the response data.
 */
function _makeCallHandler(data, statusText, xhr) {
    print2Console("Make call RESPONSE", statusText);

    //Validate success.
    if (statusText === "success") {
        $("#field-call-control-callid").val("");
    }
}

/**
 * Sign in handler. If successful, hide sign in forms, display actions, and
 * connect to BOSH channel to receive events.
 */
function _signInHandler(data, statusText, xhr) {
    print2Console("Sign in RESPONSE", xhr.status);
    //Ensure success.
    if (xhr.status === 202) {
        //Hide signin forms and show actions.
        console.log("signin success");
    }
}

function _signOutHandler(data, statusText, xhr) {
    print2Console("Sign out RESPONSE", xhr.status);

    //Ensure success.
    if (xhr.status === 202) {
        // Disconnect from getting events
        _eventDisconnect();

        // Clean up the values of objects
        _username = null;
        _password = null;
        _extension = null;
        _domain = null;

        // Clean up the Finesse object
        _finesse = null;

        // Reload the page after successful sign out.
        // go to logout.jsp
        window.location.reload();
    }
}

function showStateAgent() {
    if (_state != null) {
        if (_state == "NOT_READY") {          
            $('#btnActiveUser').find('div').attr('class', 'switch-off switch-animate');
            $('#callStatusUser').val('0');
            UpdateStatusWorkingUser(0);
        } else {
            //hiển thị online
            $('#btnActiveUser').find('div').attr('class', 'switch-on switch-animate');
            $('#callStatusUser').val('1');
            UpdateStatusWorkingUser(1);
        }
    }

}

function changeState(state) {
    if (state != null && state != "")
        _state = state;
    if (_state == "NOT_READY") {
        _finesse.changeState(_username, "READY", null, _handler, _handler);
        _state = "READY";
        console.log("State: " + _state);
    } else {
        _finesse.changeState(_username, "NOT_READY", null, _handler, _handler);
        _state = "NOT_READY";
        console.log("State: " + _state);
    }
    showStateAgent();
}

function signIn(username, password, extension, domain) {
    _username = username;
    _password = password;
    _extension = extension;
    _domain = domain;
    //Check non-empty fields
    if (!_username || !_password || !_extension || !_domain) {
        console.log("Please enter valid domain and credentials.");
    } else {
        login = true;
        //Create Finesse object and sign in user. On successful sign in, a
        //handler will be invoked to present more API options in UI.
        _finesse = new Finesse(_username, _password);
        _eventConnect();
        //hiển thị nút offline
        $('#btnActiveUser').find('div').attr('class', 'switch-off switch-animate');
        $('#callStatusUser').val('0');
    }

}

function capturekey(e) {
    e = e || window.event;
    if (e.code == 'F5') {
        if (confirm('Bạn có chắc chắn muốn tải lại trang không??')) {
            //allow to refresh
        }
        else {
            //avoid from refresh
            e.preventDefault();
            e.stopPropagation();
        }
    }
}

function getState() {
    _finesse.getState(_username, _getStateHandler, _handler);
}
