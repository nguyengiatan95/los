﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SSO.EntityFramework;
using SSO.Helpers;
using SSO.Models;
using SSO.Service;
using SSO.Models.Extensions;

namespace SSO.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly TimaSSOContext _dbContext;
        protected IConfiguration _configuration;
        private readonly IEmailService _emailService;
        public HomeController(ILogger<HomeController> logger, IEmailService emailService)
        {
            _logger = logger;
            _emailService = emailService;
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<TimaSSOContext>();
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("SSODatabase"));
            DbContextOptions<TimaSSOContext> options = optionsBuilder.Options;
            _dbContext = new TimaSSOContext(options);
        }
        [Route("login.html")]
        [Route("/")]
        public IActionResult Index()
        {
            return View();
        }

        //[Authorize]
        [Route("/index.html")]
        public IActionResult SSOIndex()
        {
            var user = HttpContext.Session.GetObjectFromJson<User>("USER_DATA").Result;
            if (user == null)
                return RedirectToAction("Index", "Home");

            ViewBag.CheckOtp = null;
            var logOtp = _dbContext.LogOtps.Where(x => x.UserId == user.Id).OrderByDescending(x => x.Id).FirstOrDefault();
            if (logOtp != null && logOtp.Id > 0 && DateTime.Now < logOtp.ExpiredDate.Value && logOtp.IsVerifyOtp == true)
                ViewBag.CheckOtp = true;

            ViewBag.LOS = _configuration["AppSettings:LOS"];
            ViewBag.AG = "https://ag.tima.vn";
            ViewBag.ERP = "https://erp.tima.vn";
            return View(user);
        }

        private string GenerateToken(int userId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                     new Claim(ClaimTypes.NameIdentifier, userId.ToString()),
                                                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                                    new Claim("UserId", userId.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(Convert.ToInt32(_configuration["AppSettings:TokenTime"])),
                Issuer = _configuration["JWT:Issuer"],
                Audience = _configuration["JWT:Audience"],
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["JWT:SigningKey"])), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginPost(LoginReq req)
        {
            try
            {
                req.Username = req.Username.Trim().ToLower();
                req.Password = Utils.Security.GetMD5Hash(req.Password.Trim());
                var data = _dbContext.Users.Where(x => x.Username == req.Username && x.Password == req.Password).FirstOrDefault();
                if (data != null)
                {
                    if (data.Status == 0)
                        return Json(new { status = 0, message = "Tài khoản đang bị khóa" });
                    if (data.IsDeleted == true)
                        return Json(new { status = 0, message = "Tài khoản đã bị xóa" });
                    var Token = GenerateToken(data.Id);

                    data.Token = Token;
                    // save access token					
                    HttpContext.Session.SetObjectAsJson("USER_DATA", data);
                    await HttpContext.Session.CommitAsync();

                    //check xem có tồn tại và hết hạn thì bắt đổi mật khẩu
                    if (data.ExpiredDate.HasValue && Convert.ToInt32((DateTime.Now - data.ExpiredDate.Value).TotalDays) >= 0)
                    {
                        return Json(new { status = -1 });
                    }

                    //Addlog Login
                    var logs = new Log()
                    {
                        CreatedTime = DateTime.Now,
                        Type = (int)TypeLog.Login,
                        Status = 1,
                        Input = JsonConvert.SerializeObject(req),
                        OutPut = Token,
                        UserId = data.Id
                    };
                    _dbContext.Add(logs);
                    _dbContext.SaveChanges();

                    return Json(new { status = 1 });
                    //check xem tài khoản có email không
                    //nếu có thì gửi otp
                    //không thì cho đăng nhập luôn
                    //if (!string.IsNullOrEmpty(data.Email))
                    //{

                    //    //random otp 6 số
                    //    Random random = new Random();
                    //    var objOtp = new UserOtp()
                    //    {
                    //        UserId = data.Id,
                    //        Otp = random.Next(100001, 999999),
                    //    };
                    //    //gửi mail cho user
                    //    string emailTemplate = @"<div style='font-family:Arial'>
                    //                 <div style='margin:0px;padding:10px;background:#cccccc;font-family:Arial;font-size:12px'>
                    //                  <div style='border-radius:6px;width:598px;margin:0px auto;padding:15px 0px;background:#ffffff;color:#4d4d4d'>
                    //                   <div style='width:546px;margin:0px auto;border:4px solid #007cc2'>
                    //                    <div style='padding:10px 20px'>
                    //                     <div style='margin-bottom:0px'>
                    //                      <p style='font-family:Arial'>Mã OTP của bạn là: {0}</p>
                    //                     </div>
                    //                    </div>
                    //                   </div>
                    //                  </div>
                    //                 </div>
                    //                </div>";
                    //    var bodyContent = string.Format(emailTemplate, objOtp.Otp);
                    //    _emailService.SendMail(data.Email, "[sso.tima.vn] OTP", bodyContent);
                    //    HttpContext.Session.SetString("SessionOtp", JsonConvert.SerializeObject(objOtp));

                    //    // Addlog OTP
                    //    var logOtp = new LogOtp()
                    //    {
                    //        CreateDate = DateTime.Now,
                    //        Otp = objOtp.Otp,
                    //        UserId = data.Id,
                    //    };
                    //    _dbContext.Add(logOtp);
                    //    _dbContext.SaveChanges();
                    //    return Json(new { status = 2 });
                    //}
                    //else
                    //{
                    //    var logs = new Log()
                    //    {
                    //        CreatedTime = DateTime.Now,
                    //        Type = (int)TypeLog.Login,
                    //        Status = 1,
                    //        Input = JsonConvert.SerializeObject(req),
                    //        OutPut = Token,
                    //        UserId = data.Id
                    //    };
                    //    _dbContext.Add(logs);
                    //    _dbContext.SaveChanges();

                    //}
                }
                else
                {
                    var logs = new Log()
                    {
                        CreatedTime = DateTime.Now,
                        Type = (int)TypeLog.Login,
                        Status = 1,
                        Input = JsonConvert.SerializeObject(req),
                        OutPut = "Thông tin đăng nhập không đúng",
                        UserId = 0
                    };
                    _dbContext.Add(logs);
                    _dbContext.SaveChanges();
                    return Json(new { status = 0, message = "Thông tin tài khoản không đúng" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Xảy ra lỗi khi đăng nhập. Vui lòng thử lại." });
            }

        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePass(ChangePasss req)
        {
            try
            {
                //var passOld = req.PasswordOld;
                //var passNew = req.PasswordNew;
                var user = HttpContext.Session.GetObjectFromJson<User>("USER_DATA").Result;
                req.PasswordOld = Utils.Security.GetMD5Hash(req.PasswordOld.Trim());
                req.PasswordNew = Utils.Security.GetMD5Hash(req.PasswordNew.Trim());
                var data = _dbContext.Users.Where(x => x.Id == user.Id && x.Password == req.PasswordOld).FirstOrDefault();
                if (data != null)
                {
                    if (data.Status == 0)
                        return Json(new { status = 0, message = "Tài khoản đang bị khóa" });
                    if (data.IsDeleted == true)
                        return Json(new { status = 0, message = "Tài khoản đã bị xóa" });

                    data.Password = req.PasswordNew;
                    data.UpdateTime = DateTime.Now;
                    _dbContext.Users.Update(data);
                    _dbContext.Entry(data).State = EntityState.Modified;

                    //Addlog Login
                    //req.PasswordOld = passOld;
                    //req.PasswordNew = passNew;
                    var logs = new Log()
                    {
                        CreatedTime = DateTime.Now,
                        Type = (int)TypeLog.ChangePass,
                        Status = 1,
                        Input = JsonConvert.SerializeObject(req),
                        OutPut = "success",
                        UserId = user.Id
                    };
                    _dbContext.Add(logs);
                    _dbContext.SaveChanges();

                    return Json(new { status = 1, message = "Mật khẩu đã được thay đổi" });
                }
                else
                {
                    //Addlog Login
                    //req.PasswordOld = passOld;
                    //req.PasswordNew = passNew;
                    var logs = new Log()
                    {
                        CreatedTime = DateTime.Now,
                        Type = (int)TypeLog.ChangePass,
                        Status = 1,
                        Input = JsonConvert.SerializeObject(req),
                        OutPut = "Mật khẩu bạn nhập không đúng",
                        UserId = user.Id
                    };
                    _dbContext.Add(logs);
                    _dbContext.SaveChanges();
                    return Json(new { status = 0, message = "Mật khẩu bạn nhập không đúng" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi xảy ra. Vui lòng thử lại." });
            }

        }

        [Route("logout.html")]
        public IActionResult Logout()
        {
            HttpContext.Session.SetObjectAsJson("USER_DATA", null);
            return Redirect("/");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        [Route("/doi-mat-khau.html")]
        public IActionResult ChangPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePasswordV2(ChangePasss req)
        {
            try
            {
                var user = HttpContext.Session.GetObjectFromJson<User>("USER_DATA").Result;
                req.PasswordOld = Utils.Security.GetMD5Hash(req.PasswordOld.Trim());
                req.PasswordNew = Utils.Security.GetMD5Hash(req.PasswordNew.Trim());
                var data = _dbContext.Users.Where(x => x.Id == user.Id && x.Password == req.PasswordOld).FirstOrDefault();
                if (data != null)
                {
                    if (data.Status == 0)
                        return Json(new { status = 0, message = "Tài khoản đang bị khóa" });
                    if (data.IsDeleted == true)
                        return Json(new { status = 0, message = "Tài khoản đã bị xóa" });

                    data.Password = req.PasswordNew;
                    data.UpdateTime = DateTime.Now;
                    if (data.ExpiredDate.HasValue && Convert.ToInt32((DateTime.Now - data.ExpiredDate.Value).TotalDays) >= 0)
                        data.ExpiredDate = data.ExpiredDate.Value.AddMonths(6);

                    _dbContext.Users.Update(data);
                    _dbContext.Entry(data).State = EntityState.Modified;

                    var logs = new Log()
                    {
                        CreatedTime = DateTime.Now,
                        Type = (int)TypeLog.ChangePass,
                        Status = 1,
                        Input = JsonConvert.SerializeObject(req),
                        OutPut = "success",
                        UserId = user.Id
                    };
                    _dbContext.Add(logs);
                    _dbContext.SaveChanges();

                    return Json(new { status = 1, message = "Mật khẩu đã được thay đổi" });
                }
                else
                {
                    var logs = new Log()
                    {
                        CreatedTime = DateTime.Now,
                        Type = (int)TypeLog.ChangePass,
                        Status = 1,
                        Input = JsonConvert.SerializeObject(req),
                        OutPut = "Mật khẩu bạn nhập không đúng",
                        UserId = user.Id
                    };
                    _dbContext.Add(logs);
                    _dbContext.SaveChanges();
                    return Json(new { status = 0, message = "Mật khẩu bạn nhập không đúng" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Có lỗi xảy ra. Vui lòng thử lại." });
            }

        }

        [HttpPost]
        public IActionResult CheckOTP(int userId, int otp)
        {
            //lấy ra bản ghi cuối cùng trong log
            try
            {
                var logOtp = _dbContext.LogOtps.Where(x => x.UserId == userId && x.Action.GetValueOrDefault(1) == (int)ActionLogOtp.OTPLogin).OrderByDescending(x => x.Id).FirstOrDefault();

                if (logOtp != null && logOtp.Id > 0)
                {
                    if (otp == logOtp.Otp)
                    {
                        if (DateTime.Now > logOtp.ExpiredDate.Value)
                        {
                            return Json(new { status = -1, message = "Mã OTP đã hết hạn" });
                        }
                        else
                        {
                            var user = HttpContext.Session.GetObjectFromJson<User>("USER_DATA").Result;
                            logOtp.IsVerifyOtp = true;
                            _dbContext.LogOtps.Update(logOtp);
                            _dbContext.SaveChanges();
                            return Json(new { status = 1, token = user.Token });
                        }
                    }
                    else
                        return Json(new { status = 0, message = "Mã OTP không đúng. Vui lòng kiểm tra lại" });
                }
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi gửi OTP. Vui lòng liên hệ phòng kĩ thuật." });
            }
            catch (Exception ex)
            {
                Serilog.Log.Error("SSO CheckOTP: " + ex.ToString());
                return Json(new { status = 0, message = "Xảy ra lỗi gửi OTP. Vui lòng liên hệ phòng kĩ thuật." });
            }

        }

        [HttpPost]
        public IActionResult CheckSendOtp(int userId, string email)
        {
            //check xem email đã đc gửi email chưa (chưa thì gửi otp vào email)
            //otp đã hết hạn chưa : 10 phút (Hết hạn thì gửi lại otp)
            //xem đã nhập otp chưa

            //lấy ra bản ghi cuối cùng trong log
            try
            {
                var logOtp = _dbContext.LogOtps.Where(x => x.UserId == userId && x.Action.GetValueOrDefault(1) == (int)ActionLogOtp.OTPLogin).OrderByDescending(x => x.Id).FirstOrDefault();
                if (logOtp != null && logOtp.Id > 0)
                {
                    //nếu chưa hết hạn và đã verify rồi thì redirect luôn
                    if (DateTime.Now < logOtp.ExpiredDate.Value)
                    {
                        if (logOtp.IsVerifyOtp == true)
                            return Json(new { status = 2 });
                        else
                            SendOtp(userId, email, logOtp.Otp, (int)ActionLogOtp.OTPLogin);
                    }
                    else
                        SendOtp(userId, email, 0, (int)ActionLogOtp.OTPLogin);
                }
                else
                    SendOtp(userId, email, 0, (int)ActionLogOtp.OTPLogin);

            }
            catch (Exception ex)
            {
                Serilog.Log.Error("SSO CheckSendOtp: " + ex.StackTrace);
            }

            return Json(new { status = 1 });
        }

        private void SendOtp(int userId, string email, int? otpOld = 0, int action = 0)
        {
            try
            {
                var otp = otpOld > 0 ? otpOld : 0;
                if (otp == 0)
                {
                    //random otp 6 số
                    Random random = new Random();
                    otp = random.Next(100001, 999999);
                }

                //gửi mail cho user
                string emailTemplate = @"<div style='font-family:Arial'>
                             <div style='margin:0px;padding:10px;background:#cccccc;font-family:Arial;font-size:12px'>
                              <div style='border-radius:6px;width:598px;margin:0px auto;padding:15px 0px;background:#ffffff;color:#4d4d4d'>
                               <div style='width:546px;margin:0px auto;border:4px solid #007cc2'>
                                <div style='padding:10px 20px'>
                                 <div style='margin-bottom:0px'>
                                  <p style='font-family:Arial'>Mã OTP của bạn là: {0}</p>
                                 </div>
                                </div>
                               </div>
                              </div>
                             </div>
                            </div>";
                var bodyContent = string.Format(emailTemplate, otp);
                var username = _configuration["AppSettings:MailServerUserName"];
                var password = _configuration["AppSettings:MailServerPassword"];
                _emailService.SendMail(username, password, email, "[sso.tima.vn] OTP", bodyContent);
                // Addlog OTP
                var logOtp = new LogOtp()
                {
                    CreateDate = DateTime.Now,
                    Otp = otp,
                    UserId = userId,
                    Email = email,
                    ExpiredDate = DateTime.Now.AddMinutes(10),
                    Action = action
                };
                _dbContext.Add(logOtp);
                _dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Serilog.Log.Error("SSO SendOtp: " + ex.StackTrace);
            }

        }

        [Route("/lay-lai-mat-khau.html")]
        public IActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SendRequireResetPass(string userName)
        {
            var user = _dbContext.Users.Where(x => x.Username == userName).FirstOrDefault();
            if (user != null && user.Id > 0)
            {
                if (!string.IsNullOrEmpty(user.Email))
                {
                    //lấy ra bản ghi cuối cùng trong log
                    var logOtp = _dbContext.LogOtps.Where(x => x.UserId == user.Id && x.Action.GetValueOrDefault(1) == (int)ActionLogOtp.OTPResetPass).OrderByDescending(x => x.Id).FirstOrDefault();
                    if (logOtp != null && logOtp.Id > 0)
                    {
                        //nếu chưa hết hạn thì lấy lại OTP
                        if (DateTime.Now < logOtp.ExpiredDate.Value)
                        {
                            SendOtp(user.Id, user.Email, logOtp.Otp, (int)ActionLogOtp.OTPResetPass);
                        }
                        else
                            SendOtp(user.Id, user.Email, 0, (int)ActionLogOtp.OTPResetPass);
                    }
                    else
                        SendOtp(user.Id, user.Email, 0, (int)ActionLogOtp.OTPResetPass);

                    return Json(new { data = user, status = 1, message = string.Format("Mã OTP đã được gửi đến Email: {0}", user.Email) });
                }
                else
                    return Json(new { status = 0, message = "Tài khoản không có Email. Vui lòng báo với phòng kỹ thuật" });
            }
            else
                return Json(new { status = 0, message = "Tài khoản không tồn tại. Vui lòng kiểm tra lại" });
        }

        [HttpPost]
        public IActionResult ConfirmOtpResetPass(int userId, int otp)
        {
            //lấy ra bản ghi cuối cùng trong log
            try
            {
                var user = _dbContext.Users.Where(x => x.Id == userId).FirstOrDefault();
                if (user == null)
                    return Json(new { status = 0, message = "Xảy ra lỗi. Vui lòng liên hệ phòng kĩ thuật." });
                if (user.Status == 0)
                    return Json(new { status = 0, message = "Tài khoản đang bị khóa" });
                if (user.IsDeleted == true)
                    return Json(new { status = 0, message = "Tài khoản đã bị xóa" });

                var logOtp = _dbContext.LogOtps.Where(x => x.UserId == userId && x.Action.GetValueOrDefault(1) == (int)ActionLogOtp.OTPResetPass).OrderByDescending(x => x.Id).FirstOrDefault();
                if (logOtp != null && logOtp.Id > 0)
                {
                    if (otp == logOtp.Otp)
                    {
                        if (DateTime.Now > logOtp.ExpiredDate.Value)
                        {
                            return Json(new { status = -1, message = "Mã OTP đã hết hạn" });
                        }
                        else
                        {
                            PasswordGenerator randomPass = new PasswordGenerator();
                            string password = randomPass.GeneratePassword(true, true, true, true, 12);
                            user.Password = Utils.Security.GetMD5Hash(password);
                            _dbContext.Users.Update(user);
                            _dbContext.Entry(user).State = EntityState.Modified;
                            logOtp.IsVerifyOtp = true;
                            _dbContext.LogOtps.Update(logOtp);
                            _dbContext.SaveChanges();
                            SendNewPassword(password, user.Email, user.Username);
                            return Json(new { status = 1, message = string.Format("Mật khẩu mới đã được gửi đến Email: {0}", user.Email) });
                        }
                    }
                    else
                        return Json(new { status = 0, message = "Mã OTP không đúng. Vui lòng kiểm tra lại" });
                }
                else
                    return Json(new { status = 0, message = "Xảy ra lỗi gửi OTP. Vui lòng liên hệ phòng kĩ thuật." });
            }
            catch (Exception ex)
            {
                Serilog.Log.Error("SSO ConfirmOtpResetPass: " + ex.StackTrace);
                return Json(new { status = 0, message = "Xảy ra lỗi gửi OTP. Vui lòng liên hệ phòng kĩ thuật." });
            }
        }

        private void SendNewPassword(string passWord, string email, string userName)
        {
            string emailTemplate = @"<div style='font-family:Arial'>
	                                    <div style='margin:0px;padding:10px;background:#cccccc;font-family:Arial;font-size:12px'>
		                                    <div style='border-radius:6px;width:598px;margin:0px auto;padding:15px 0px;background:#ffffff;color:#4d4d4d'>
			                                    <div style='width:546px;margin:0px auto;border:4px solid #007cc2'>
				                                    <div style='padding:10px 20px'>
                                                        <p style='padding:4px 0px 10px 0px;margin:0px;font-family:Arial'>Tài khoản <b>{0},</b></p>
					                                    <div style='margin-bottom:0px'>
						                                    <p style='font-family:Arial'>SSO đã reset lại mật khẩu của bạn trên website <a href='https://sso.tima.vn/' target='_blank'>sso.tima.vn</a></p>
						                                    <p style='font-family:Arial'>Mật khẩu mới của bạn là: <b>{1}</b></p>
						                                    <p style='font-family:Arial'>Xin vui lòng đăng nhập vào hệ thống bằng mật khẩu này và cập nhật lại mật khẩu mới của bạn!</p>
					                                    </div>
				                                    </div>
			                                    </div>
		                                    </div>
	                                    </div>
                                    </div>";
            var bodyContent = string.Format(emailTemplate, userName, passWord);
            var username = _configuration["AppSettings:MailServerUserName"];
            var password = _configuration["AppSettings:MailServerPassword"];
            _emailService.SendMail(username, password, email, "[sso.tima.vn] Reset mật khẩu", bodyContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}