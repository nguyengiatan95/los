﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using LOS.SSO.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SSO.EntityFramework;
using SSO.Models;
using static SSO.Models.UserReq;
using SSO.Models.Extensions;

namespace SSO.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly TimaSSOContext _dbContext;
        protected IConfiguration _configuration;

        public UserController()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<TimaSSOContext>();
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("SSODatabase"));
            DbContextOptions<TimaSSOContext> options = optionsBuilder.Options;
            _dbContext = new TimaSSOContext(options);
        }

        [HttpPost]
        [Route("Create")]
        [AuthorizeToken("API_SSO")]
        public ActionResult Create([FromBody] CreateUserModel entity)
        {
            var def = new DefaultResponse<DataUserOutPut>();
            try
            {
                if (string.IsNullOrEmpty(entity.Username) || string.IsNullOrEmpty(entity.Password))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                entity.Username = entity.Username.Trim().ToLower();
                if (!string.IsNullOrEmpty(entity.Email))
                    entity.Email = entity.Email.Trim().ToLower();
                //Check xem user đã tồn tại trên hệ thống hay chưa
                var info = _dbContext.Users.Where(x => x.Username == entity.Username).FirstOrDefault();
                var obj = new DataUserOutPut();
                if (info != null)
                {
                    obj.Username = info.Username;
                    obj.Email = info.Email;
                    obj.Id = info.Id;
                    def.meta = new Meta(ResponseHelper.EXIST_CODE, ResponseHelper.EXIST_MESSAGE);
                    def.data = obj;
                }
                else
                {
                    var userModel = new User()
                    {
                        Username = entity.Username,
                        Password = Utils.Security.GetMD5Hash(entity.Password.Trim()),
                        CreatedTime = DateTime.Now,
                        Status = 1,
                        Application = entity.Application,
                        IsDeleted = false,
                        ExpiredDate = DateTime.Now.AddDays(30),
                        IsValidOtp = entity.IsValidOtp,
                        Email = entity.Email
                    };
                    _dbContext.Users.Add(userModel);
                    _dbContext.SaveChanges();
                    obj.Username = userModel.Username;
                    obj.Email = userModel.Email;
                    obj.Id = userModel.Id;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = obj;
                }
            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }


            return Ok(def);
        }

        [HttpPost]
        [Route("Info")]
        [AuthorizeToken("API_SSO")]
        public ActionResult Info([FromBody] User entity)
        {
            var def = new DefaultResponse<DataUserOutPut>();
            try
            {
                if (string.IsNullOrEmpty(entity.Username) || string.IsNullOrEmpty(entity.Password))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }

                var info = _dbContext.Users.Where(x => x.Username == entity.Username.Trim().ToLower() && x.Password == Utils.Security.GetMD5Hash(entity.Password.Trim())).FirstOrDefault();
                var obj = new DataUserOutPut();
                if (info != null)
                {
                    obj.Username = info.Username;
                    obj.Email = info.Email;
                    obj.Id = info.Id;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = obj;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Tài khoản không tồn tại trong hệ thống!");
                    def.data = null;
                }
            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }


            return Ok(def);
        }

        [HttpPost]
        [Route("Login")]
        public ActionResult Login([FromBody] LoginReq req)
        {
            var def = new DefaultResponse<DataUserOutPut>();
            try
            {
                var pass = req.Password;
                req.Username = req.Username.Trim().ToLower();
                req.Password = Utils.Security.GetMD5Hash(req.Password.Trim());
                var data = _dbContext.Users.Where(x => x.Username == req.Username && x.Password == req.Password).FirstOrDefault();
                if (data != null)
                {
                    if (data.Status == 0)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Tài khoản đang bị khóa. Vui lòng liên hệ với Admin để mở khóa.");
                        return Ok(def);
                    }

                    if (data.IsDeleted == true)
                    {
                        def.meta = new Meta(ResponseHelper.FAIL_CODE, "Tài khoản đã bị xóa khỏi hệ thống");
                        return Ok(def);
                    }

                    //Addlog Login
                    req.Password = pass;
                    var logs = new Log()
                    {
                        CreatedTime = DateTime.Now,
                        Type = (int)TypeLog.LoginApp,
                        Status = 1,
                        Input = JsonConvert.SerializeObject(req),
                        OutPut = "success",
                        UserId = data.Id
                    };
                    _dbContext.Add(logs);
                    _dbContext.SaveChanges();

                    var obj = new DataUserOutPut();
                    if (data != null)
                    {
                        obj.Username = data.Username;
                        obj.Email = data.Email;
                        obj.Id = data.Id;
                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = obj;
                    }
                }
                else
                {
                    req.Password = pass;
                    var logs = new Log()
                    {
                        CreatedTime = DateTime.Now,
                        Type = (int)TypeLog.LoginApp,
                        Status = 1,
                        Input = JsonConvert.SerializeObject(req),
                        OutPut = "Thông tin đăng nhập không đúng",
                        UserId = 0
                    };
                    _dbContext.Add(logs);
                    _dbContext.SaveChanges();
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Thông tin đăng nhập không đúng. Vui lòng kiểm tra lại");
                    def.data = null;
                }
            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }


            return Ok(def);
        }


        [HttpPost]
        [Route("asyn_user")]
        [AuthorizeToken("API_SSO")]
        public ActionResult AsynUser([FromBody] CreateUserModel entity)
        {
            var def = new DefaultResponse<DataUserOutPut>();
            try
            {
                if (string.IsNullOrEmpty(entity.Username) || string.IsNullOrEmpty(entity.Password))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                entity.Username = entity.Username.Trim().ToLower();
                if (!string.IsNullOrEmpty(entity.Email))
                    entity.Email = entity.Email.Trim().ToLower();
                //Check xem user đã tồn tại trên hệ thống hay chưa
                var info = _dbContext.Users.Where(x => x.Username == entity.Username
                || (!string.IsNullOrEmpty(entity.Email) && x.Email == entity.Email)).FirstOrDefault();
                var obj = new DataUserOutPut();
                if (info != null)
                {
                    obj.Username = info.Username;
                    obj.Email = info.Email;
                    obj.Id = info.Id;
                    if (Utils.Security.GetMD5Hash(entity.Password.Trim()) != info.Password)
                    {
                        def.meta = new Meta(203, "Trùng tài khoản nhưng khác pass");
                    }
                    else
                    {
                        def.meta = new Meta(ResponseHelper.EXIST_CODE, ResponseHelper.EXIST_MESSAGE);
                    }
                    def.data = obj;
                }
                else
                {
                    var userModel = new User()
                    {
                        Username = entity.Username,
                        Password = entity.Password.Trim(),
                        CreatedTime = DateTime.Now,
                        Status = 1,
                        Application = entity.Application,
                        IsDeleted = false,
                        ExpiredDate = DateTime.Now.AddDays(30)
                    };
                    _dbContext.Users.Add(userModel);
                    _dbContext.SaveChanges();
                    obj.Username = userModel.Username;
                    obj.Email = userModel.Email;
                    obj.Id = userModel.Id;
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = obj;
                }
            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("enctypt")]
        public ActionResult GetEncode(string password)
        {
            var def = new DefaultResponse<string>();
            try
            {
                var pass = Utils.Security.GetMD5Hash(password);
                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = pass;
            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("API_SSO")]
        [Route("reset_pass")]
        public ActionResult ResetPass([FromBody] User entity)
        {
            var def = new DefaultResponse<DataUserOutPut>();
            try
            {
                if (entity.Id <= 0 || string.IsNullOrEmpty(entity.Password))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //Check xem user đã tồn tại trên hệ thống hay chưa
                var info = _dbContext.Users.Where(x => x.Id == entity.Id).FirstOrDefault();
                var obj = new DataUserOutPut();
                if (info != null)
                {
                    obj.Username = info.Username;
                    obj.Email = info.Email;
                    obj.Id = info.Id;
                    info.Password = Utils.Security.GetMD5Hash(entity.Password.Trim());
                    _dbContext.Users.Update(info);
                    _dbContext.Entry(info).State = EntityState.Modified;
                    _dbContext.SaveChanges();
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, "Mật khẩu đã được reset thành công!");
                    def.data = obj;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Tài khoản không tồn tại trong hệ thống!");
                    def.data = null;
                }
            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        [HttpPost]
        [AuthorizeToken("API_SSO")]
        [Route("gen_new_otp")]
        public ActionResult GenNewOtp(string text)
        {
            var def = new DefaultResponse<ResponeGenNewOtp>();
            try
            {
                if (string.IsNullOrEmpty(text))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                //kiểm tra xem có user nào trùng với username hoặc email truyền vào không
                var user = _dbContext.Users.Where(x => x.Username == text || x.Email == text).FirstOrDefault();
                if (user != null && user.Id > 0)
                {
                    var otp = 0;
                    var logOtp = _dbContext.LogOtps.Where(x => x.UserId == user.Id && x.Action.GetValueOrDefault(1) == (int)ActionLogOtp.OTPLogin).OrderByDescending(x => x.Id).FirstOrDefault();
                    if (logOtp != null && logOtp.Id > 0 && DateTime.Now < logOtp.ExpiredDate.Value && logOtp.IsVerifyOtp == null)
                    {
                        otp = logOtp.Otp.Value;
                    }
                    else
                    {
                        Random random = new Random();
                        otp = random.Next(100001, 999999);
                        // Addlog OTP
                        var objLogOtp = new LogOtp()
                        {
                            CreateDate = DateTime.Now,
                            Otp = otp,
                            UserId = user.Id,
                            Email = user.Email,
                            ExpiredDate = DateTime.Now.AddMinutes(10),
                            Action = (int)ActionLogOtp.OTPLogin,
                        };
                        _dbContext.Add(objLogOtp);
                        _dbContext.SaveChanges();
                    }
                    var objGenNewOtp = new ResponeGenNewOtp()
                    {
                        UserName = user.Username,
                        Otp = otp
                    };
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    def.data = objGenNewOtp;
                }
                else
                {
                    def.meta = new Meta(ResponseHelper.NOT_FOUND_CODE, "Tài khoản không tồn tại trong hệ thống!");
                    def.data = null;
                }
            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

