﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSO
{
    public class DefaultResponse<T> where T : class
    {
        public Meta meta { get; set; }
        public T data { get; set; }
    }

    public class DefaultResponse<T1, T2> where T1 : class where T2 : class
    {
        public T1 meta { get; set; }
        public T2 data { get; set; }
    }

    public class Meta
    {
        public int errorCode { get; set; }
        public string errorMessage { get; set; }

        public Meta(int errorCode, string errorMessage)
        {
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
        }
    }

    public class SummaryMeta
    {
        public int errorCode { get; set; }
        public string errorMessage { get; set; }
        public int page { get; set; }
        public int pageSize { get; set; }
        public int totalRecords { get; set; }

        public SummaryMeta(int errorCode, string errorMessage, int page, int pageSize, int totalRecords)
        {
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
            this.page = page;
            this.pageSize = pageSize;
            this.totalRecords = totalRecords;
        }
    }
}
