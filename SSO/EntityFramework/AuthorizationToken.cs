﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SSO.EntityFramework
{
    public partial class AuthorizationToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ServiceName { get; set; }
        public bool? Enlabled { get; set; }
    }
}
