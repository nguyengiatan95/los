﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SSO.EntityFramework
{
    public partial class Log
    {
        public int Id { get; set; }
        public int? Type { get; set; }
        public string Input { get; set; }
        public string OutPut { get; set; }
        public DateTime? CreatedTime { get; set; }
        public int? Status { get; set; }
        public int? UserId { get; set; }
    }
}
