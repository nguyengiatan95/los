﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SSO.EntityFramework
{
    public partial class LogLogin
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public string Ip { get; set; }
        public int? TypeLogin { get; set; }
        public int? UserId { get; set; }
        public string Email { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}
