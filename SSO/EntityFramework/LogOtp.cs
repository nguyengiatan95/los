﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SSO.EntityFramework
{
    public partial class LogOtp
    {
        public int Id { get; set; }
        public int? Otp { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Email { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public bool? IsVerifyOtp { get; set; }
        public int? Action { get; set; }
    }
}
