﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SSO
{
    public class Description
    {
        public static string GetDescription(Enum value)
        {
            try
            {
                var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return da.Length > 0 ? da[0].Description : value.ToString();
            }
            catch
            {
                return null;
            }
        }
       
    }

    public enum TypeLog
    {
        [Description("Login")]
        Login = 1,
        [Description("ChangePage")]
        ChangePass = 2,
        [Description("LoginApp")]
        LoginApp = 3
    }
}
