﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SSO.Models.Extensions
{
    public class Description
    {
        public static string GetDescription(Enum value)
        {
            try
            {
                var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return da.Length > 0 ? da[0].Description : value.ToString();
            }
            catch
            {
                return null;
            }
        }
    }

    public enum ActionLogOtp
    {
        [Description("OTP đăng nhập")]
        OTPLogin = 1,
        [Description("OTP lấy lại mật khẩu")]
        OTPResetPass = 2
    }
}
