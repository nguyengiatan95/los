﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSO.Models
{
    public class LoginReq
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }

    public class ChangePasss
    {
        public string PasswordOld { get; set; }
        public string PasswordNew { get; set; }
    }
}
