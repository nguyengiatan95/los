﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSO.Models
{
    public class ResponeGenNewOtp
    {
        public string UserName { get; set; }
        public int Otp { get; set; }
    }
}
