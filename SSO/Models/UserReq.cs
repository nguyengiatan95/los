﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSO.Models
{
    public class UserReq
    {
        public partial class Update
        {
            public int Id { get; set; }
            public int? UserId { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public string Email { get; set; }
           
        }

        public partial class OutPutCreate
        {
            public int Check { get; set; }
            public DataUserOutPut Data { get; set; }
        }
        public partial class DataUserOutPut
        {
            public int Id { get; set; }
            public string Username { get; set; }
            public string Email { get; set; }

        }

        public partial class GetListDTO
        {
            public int Id { get; set; }
            public string Username { get; set; }
            public string Email { get; set; }
            public int? Status { get; set; }
            public DateTime? CreatedTime { get; set; }
        }

        public partial class ChangePass
        {
            public int Id { get; set; }
            public string OldPass { get; set; }
            public string NewPass { get; set; }
        }
    }
}
