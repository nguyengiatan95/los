﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SSO.EntityFramework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LOS.SSO.Service.AuthorizeToken
{
    public interface IAuthorizeTokenService
    {
        bool CheckAuthorizeTokenRequest(string servername, String token);
    }
    public class AuthorizeTokenService : IAuthorizeTokenService
    {
        private readonly TimaSSOContext _dbContext;
        protected IConfiguration _configuration;

        public AuthorizeTokenService()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<TimaSSOContext>();
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("SSODatabase"));
            DbContextOptions<TimaSSOContext> options = optionsBuilder.Options;
            _dbContext = new TimaSSOContext(options);
        }
        public bool CheckAuthorizeTokenRequest(string servername, string token)
        {
            var authorize = false;
            try
            {
                var result = _dbContext.AuthorizationTokens.Where(x => x.ServiceName == servername).FirstOrDefault();
                if (result != null && result.Enlabled == true)
                {
                    if (result.Token == token)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex, "API_APP AuthorizeTokenService/AuthorizeTokenRequest exception");
            }
            return authorize;
        }
    }
}
