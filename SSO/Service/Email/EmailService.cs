﻿using Microsoft.Extensions.Configuration;
using SSO.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SSO.Service
{
    public interface IEmailService
    {
        public bool SendMail(string fromEmail, string fromPass, string sendTo, string title, string body);
        public Task<bool> SendMail(MailAddress mailFrom, MailAddress mailTo, string subject, string body, SmtpInfo smtp);
    }

    public class EmailService : IEmailService
    {
        protected IConfiguration _configuration;
        public EmailService()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
             .AddJsonFile("appsettings.json", true)
             .AddJsonFile($"appsettings.{environmentName}.json", true);
            _configuration = builder.Build();
        }
        public bool SendMail(string fromEmail, string fromPass, string sendTo, string title, string body)
        {
            try
            {
                //var host = _configuration["AppSettings:SmtpMailServer"];
                //var username = _configuration["AppSettings:MailServerUserName"];
                //var password = _configuration["AppSettings:MailServerPassword"];
                //var port = Convert.ToInt32(_configuration["AppSettings:MailServerPort"]);
                if (string.IsNullOrEmpty(fromEmail))
                    fromEmail = "report@tima.vn";
                if (string.IsNullOrEmpty(fromPass))
                    fromPass = "Tima@report";
                var mailFrom = new MailAddress(fromEmail, "sso.tima.vn");
                var mailTo = new MailAddress(sendTo);
                var smtpInfo = new SmtpInfo()
                {
                    AuthenticationPassword = fromPass,
                    AuthenticationUserName = fromEmail,
                    HasAuthentication = true,
                    SmtpHost = "smtp.gmail.com",
                    SmtpPort = 587
                };
                return SendMail(mailFrom, mailTo, title, body, smtpInfo).Result;
            }
            catch(Exception ex)
            {
                Serilog.Log.Error("SSO Service SendMail: " + ex.ToString());
                return false;
            }          
        }

        public Task<bool> SendMail(MailAddress mailFrom, MailAddress mailTo, string subject, string body, SmtpInfo smtp)
        {
            try
            {
                //Send mail.
                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Host = smtp.SmtpHost;
                    smtpClient.Port = smtp.SmtpPort;
                    smtpClient.UseDefaultCredentials = false;
                    if (smtp.HasAuthentication)
                    {
                        smtpClient.Credentials = new NetworkCredential(smtp.AuthenticationUserName,
                                                                       smtp.AuthenticationPassword);
                    }

                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.EnableSsl = true;
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    using (var mailMessage = new MailMessage())
                    {
                        mailMessage.From = mailFrom;
                        mailMessage.To.Add(mailTo);
                        mailMessage.Subject = subject;
                        mailMessage.Body = body;
                        mailMessage.IsBodyHtml = true;
                        smtpClient.Send(mailMessage);
                        return Task.FromResult(true);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Information(Newtonsoft.Json.JsonConvert.SerializeObject(mailFrom));
                Serilog.Log.Information(Newtonsoft.Json.JsonConvert.SerializeObject(mailTo));
                Serilog.Log.Error("SSO SendMail: " + ex.StackTrace);
                return Task.FromResult(false);
            }

        }
    }
}
