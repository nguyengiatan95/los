﻿var App = new function () {
    var Init = function () {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    }

    var ShowInfo = function (message) {
        toastr.info(message);
    }

    var ShowSuccess = function (message) {
        setTimeout(function () {
            location.reload()
        }, 1000);
        toastr.success(message);
    }

    var ShowSuccessNotLoad = function (message) {
        toastr.success(message);
    }

    var ShowSuccessRedirect = function (message, url) {
        setTimeout(function () {
            window.location.href = url;
        }, 1000);
        toastr.success(message);
    }

    var ShowWarning = function (message) {
        toastr.warning(message);
    }

    var ShowError = function (message) {
        setTimeout(function () {
            location.reload()
        }, 2000);
        toastr.error(message);
    }

    var ShowErrorNotLoad = function (message) {
        toastr.error(message);
    }

    return {
        ShowInfo: ShowInfo,
        ShowSuccess: ShowSuccess,
        ShowSuccessRedirect: ShowSuccessRedirect,
        ShowSuccessNotLoad: ShowSuccessNotLoad,
        ShowWarning: ShowWarning,
        ShowError: ShowError,
        ShowErrorNotLoad: ShowErrorNotLoad,
        Init: function () {
            Init();
        }
    }
}

$(document).ready(function () {
    App.Init();
});