﻿var LoginModule = new function () {
    var Login = function (btn, e) {
        var username = $("#Username").val();
        var password = $("#Password").val();
        e.preventDefault();
        var form = $(btn).closest('form');
        $('#_btn_otp_loading').addClass("fa fa-spinner fa-spin");
        $(btn).attr('disabled', true);
        form.validate({
            rules: {
                Username: {
                    required: true
                },
                Password: {
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                Username: {
                    required: "Username không được để trống",
                    class: "has-danger",
                },

                Password: {
                    required: "Mật khẩu không được để trống",
                    minlength: "Mật khẩu ít nhất 6 ký tự"
                }
            }
        });

        if (!form.valid()) {
            $(btn).removeAttr('disabled', true);
            $('#_btn_otp_loading').removeClass("fa fa-spinner fa-spin");
            return;
        }

        var data = {
            Username: username.trim(),
            Password: password
        };
        form.ajaxSubmit({
            url: '/Home/LoginPost',
            method: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('RequestVerificationToken', $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: { obj: data },
            success: function (data, status, xhr, $form) {
                $(btn).removeAttr('disabled', true);
                $('#_btn_otp_loading').removeClass("fa fa-spinner fa-spin");
                if (data != undefined) {
                    if (data.status == 1) {
                        window.location.href = "../index.html";
                    }
                    //else if (data.status == 2) {
                    //    App.ShowSuccessNotLoad("Vui lòng nhập mã OTP được gửi về Emai của bạn.");
                    //    $('.div-otp').removeAttr('style', 'display: none;');
                    //    $('#Username').attr('readonly', true);
                    //    $('#Password').attr('readonly', true);
                    //    $('#login_submit').attr('style', 'display: none;');
                    //}
                    else if (data.status == -1) {
                        window.location.href = "../doi-mat-khau.html";
                    }
                    else {
                        App.ShowErrorNotLoad(data.message);
                    }
                }
                else {
                    App.ShowErrorNotLoad(data.message);
                }
            }
        });

    };

    var ChangePassword = function (btn, e) {
        e.preventDefault();
        var form = $(btn).closest('form');
        form.validate({
            ignore: [],
            rules: {
                txt_change_pasword_old: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                txt_change_pasword_new: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    },
                    minlength: 6
                },
                txt_change_pasword_new_retype: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    },
                    minlength: 6
                }
            },
            messages: {
                txt_change_pasword_old: {
                    required: "Bạn chưa nhập mật khẩu cũ",
                    class: "has-danger",
                },

                txt_change_pasword_new: {
                    required: "Bạn chưa nhập mật khẩu mới",
                    minlength: "Mật khẩu ít nhất 6 ký tự"
                },
                txt_change_pasword_new_retype: {
                    required: "Bạn chưa nhập lại mật khẩu mới",
                    minlength: "Mật khẩu ít nhất 6 ký tự"
                }
            }
        });
        if (!form.valid()) {
            return;
        }
        var passwordold = $('#txt_change_pasword_old').val();
        var passwordnew = $('#txt_change_pasword_new').val();
        var passwordnewretype = $('#txt_change_pasword_new_retype').val();
        if (passwordnew != passwordnewretype) {
            App.ShowErrorNotLoad("Mật khẩu mới không giống nhau");
            return;
        }

        if (passwordold == passwordnew) {
            App.ShowErrorNotLoad("Bạn đã nhập mật khẩu cũ");
            return;
        }

        $.ajax({
            type: "POST",
            url: "/Home/ChangePass",
            data: { PasswordOld: passwordold, PasswordNew: passwordnew },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    setTimeout(function () {
                        location.href = '../logout.html';
                    }, 2000);
                } else {
                    App.ShowErrorNotLoad(data.message);
                }
            },
            traditional: true
        });
    };

    var ChangPasswordV2 = function (btn, e) {
        $(btn).attr('disabled', true);
        var passwordold = $('#txt_password_old').val();
        var passwordnew = $('#txt_password_new').val();
        var passwordnewretype = $('#txt_password_new_retype').val();

        if (passwordold == null || passwordold == '' || passwordold == undefined) {
            App.ShowErrorNotLoad("Mật khẩu cũ không được để trống");
            $(btn).removeAttr('disabled', true);
            return false;
        }
        else if (passwordnew == null || passwordnew == '' || passwordnew == undefined) {
            App.ShowErrorNotLoad("Mật khẩu mới không được để trống");
            $(btn).removeAttr('disabled', true);
            return false;
        }
        else if (passwordnewretype == null || passwordnewretype == '' || passwordnewretype == undefined) {
            App.ShowErrorNotLoad("Nhập lại mật khẩu không được để trống");
            $(btn).removeAttr('disabled', true);
            return false;
        }

        if (passwordnew != passwordnewretype) {
            App.ShowErrorNotLoad("Hai mật khẩu không giống nhau");
            $(btn).removeAttr('disabled', true);
            return false;
        }

        if (passwordnew.length < 8) {
            App.ShowErrorNotLoad("Độ dài tối thiểu là 8 ký tự");
            $(btn).removeAttr('disabled', true);
            $('#_check_8_ki_tu').attr('style', 'color: red;');
            return false;
        }
        else {
            $('#_check_8_ki_tu').attr('style', 'color: #34bfa3;');
        }

        if (passwordold == passwordnew) {
            App.ShowErrorNotLoad("Mật khẩu mới không được trùng với mật khẩu cũ");
            $(btn).removeAttr('disabled', true);
            $('#_check_khac_mat_khau_cu').attr('style', 'color: red;');
            return false;
        }
        else {
            $('#_check_khac_mat_khau_cu').attr('style', 'color: #34bfa3;');
        }

        var checkIsNumber = RegExp(/\d/);
        if (!checkIsNumber.test(passwordnew)) {
            App.ShowErrorNotLoad("Mật khẩu mới phải chứa ít nhất 1 số");
            $(btn).removeAttr('disabled', true);
            $('#_check_chu_va_so').attr('style', 'color: red;');
            return false;
        }
        else {
            $('#_check_chu_va_so').attr('style', 'color: #34bfa3;');
        }

        var checkSpecialCharacters = RegExp(/^[a-zA-Z0-9- ]*$/);
        if (checkSpecialCharacters.test(passwordnew)) {
            App.ShowErrorNotLoad("Mật khẩu mới phải chứa ít nhất 1 ký tự đặc biệt");
            $(btn).removeAttr('disabled', true);
            $('#_check_ki_tu_dac_biet').attr('style', 'color: red;');
            return false;
        }
        else {
            $('#_check_ki_tu_dac_biet').attr('style', 'color: #34bfa3;');
        }

        $.ajax({
            type: "POST",
            url: "/Home/ChangePasswordV2",
            data: { PasswordOld: passwordold, PasswordNew: passwordnew },
            success: function (data) {
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    setTimeout(function () {
                        location.href = '../logout.html';
                    }, 2000);
                } else {
                    $(btn).removeAttr('disabled', true);
                    App.ShowErrorNotLoad(data.message);
                }
            },
            traditional: true
        });
    }

    var CheckOTP = function (btn) {
        $('#_btn_confirm_loading').addClass("fa fa-spinner fa-spin");
        $(btn).attr('disabled', true);
        var otp = $('#Otp').val();
        if (otp == null || otp == '' || otp == undefined) {
            App.ShowErrorNotLoad("Mã OTP không được để trống");
            $(btn).removeAttr('disabled', true);
            $('#_btn_confirm_loading').removeClass("fa fa-spinner fa-spin");
            return false;
        }
        $.ajax({
            type: "POST",
            url: "/Home/CheckOTP",
            data: { otp: otp },
            success: function (data) {
                $(btn).removeAttr('disabled', true);
                $('#_btn_confirm_loading').removeClass("fa fa-spinner fa-spin");
                if (data.status == 1) {
                    window.location.href = "../index.html";
                } else {
                    App.ShowErrorNotLoad(data.message);
                }
            },
            traditional: true
        });
    }

    var SendRequireResetPass = function (btn) {
        $('#_btn_send_require_loading').addClass("fa fa-spinner fa-spin");
        $(btn).attr('disabled', true);
        var userName = $('#txt_username').val();
        $.ajax({
            type: "POST",
            url: "/Home/SendRequireResetPass",
            data: { userName: userName },
            success: function (data) {
                $(btn).removeAttr('disabled', true);
                $('#_btn_send_require_loading').removeClass("fa fa-spinner fa-spin");
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    $('#hdd_UserId').val(data.data.id);
                    $('#txt_username').attr('disabled', 'disabled');
                    $('.div_otp').removeAttr('style', 'display: none;');
                    $('#btn_submit').removeAttr('style', 'display: none;');
                    $('#btn_send_require').attr('style', 'display: none;');
                } else {
                    App.ShowErrorNotLoad(data.message);
                }
            },
            traditional: true
        });
    }

    var ConfirmOtpResetPass = function (btn) {
        $('#_btn_confirm_otp_loading').addClass("fa fa-spinner fa-spin");
        $(btn).attr('disabled', true);
        var userId = $('#hdd_UserId').val();
        var otp = $('#txt_otp').val();
        $.ajax({
            type: "POST",
            url: "/Home/ConfirmOtpResetPass",
            data: { userId: userId, otp: otp },
            success: function (data) {
                $(btn).removeAttr('disabled', true);
                $('#_btn_confirm_otp_loading').removeClass("fa fa-spinner fa-spin");
                if (data.status == 1) {
                    App.ShowSuccessNotLoad(data.message);
                    setTimeout(function () {
                        location.href = '../logout.html';
                    }, 2000);
                } else {
                    App.ShowErrorNotLoad(data.message);
                }
            },
            traditional: true
        });
    }

    return {
        Login: Login,
        ChangePassword: ChangePassword,
        ChangPasswordV2: ChangPasswordV2,
        CheckOTP: CheckOTP,
        SendRequireResetPass: SendRequireResetPass,
        ConfirmOtpResetPass: ConfirmOtpResetPass
    };
}
