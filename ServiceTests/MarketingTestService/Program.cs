#region snippet2
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Net.Client;
using MarketingTestService;
using Google.Protobuf.WellKnownTypes;
using Newtonsoft.Json;

namespace GrpcGreeterClient
{
    class Program
    {
        #region snippet
        static async Task Main(string[] args)
        {
            // The port number(5001) must match the port of the gRPC server.
            var channel = GrpcChannel.ForAddress("https://localhost:50019");
            var client = new Marketing.MarketingClient(channel);
            //            var reply =  await client.reportAsync(
            //                              new ReportRequest { Page = 1, PageSize = 20, Skip = -1, Take = 10, Name = "", FromDate = Timestamp.FromDateTimeOffset( DateTimeOffset.FromUnixTimeSeconds(1568835057)), ToDate = Timestamp.FromDateTimeOffset(DateTimeOffset.FromUnixTimeSeconds(1574105457)), SortBy = "JobTitleId", SortOrder = "DESC"});

            //var reply = await client.leadAsync(
            //                 new LeadRequest {
            //                    LoanTime = 1542612291,
            //                    CityId = 1
            //                 });
            //var reply = await client.getDictionaryProvincesAsync(new DictionaryProvinceRequest());
            //var reply = await client.getDictionaryDistrictsAsync(new DictionaryDistrictRequest() { 
            //    ProvinceId = 1
            //});
            //var reply = await client.getDictionaryWardsAsync(new DictionaryWardRequest() { 
            //    DistrictId = 1
            //});;
            //var reply = await client.getDictionaryJobsAsync(new DictionaryJobRequest()); ;
            //var reply = await client.getAllJobsAsync(new AllJobRequest()); ;
            var reply = await client.getJobByIdAsync(new JobByIdRequest() { 
                JobId = 1
            }); ;
            Console.WriteLine(JsonConvert.SerializeObject(reply));
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
        #endregion
    }
}
#endregion