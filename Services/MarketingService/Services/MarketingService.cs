using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using LOS.Common.Helpers;
using LOS.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Serilog;

namespace MarketingService
{
    public class MarketingService: Marketing.MarketingBase
    {
        private readonly ILogger<MarketingService> _logger;

        private IUnitOfWork unitOfWork;
        private IConfiguration configuration;

        public MarketingService(IUnitOfWork unitOfWork, ILogger<MarketingService> logger, IConfiguration configuration)
        {
            this._logger = logger;
            this.unitOfWork = unitOfWork;
            this.configuration = configuration;
        }

        public override Task<GetReportReply> marketingReport(ReportRequest request, ServerCallContext context)
        {
            var def = new GetReportReply();

            try
            {
                var query = unitOfWork.LoanBriefRepository.Query(x => (x.UtmSource.Contains(request.Name) || request.Name == null), null, false).GroupBy(x => x.UtmSource).Select(x => new MarketingReportUtmDTO
                {
                    UtmSource = x.First().UtmSource,
                    Status = x.First().Status.Value,
                    CountLoanBrief = x.Count(),
                    CountWaitTelesale = x.Count(m => m.Status == 10),
                    CountStatusDisbured = x.Count(m => m.Status == 50),
                    CountStatusCanceled = x.Count(m => m.Status == 99),
                    RateDisbured = 100 * x.Count(m => m.Status == 50) / x.Count(),
                    RateCanceled = 100 * x.Count(m => m.Status == 99) / x.Count(),
                });

                var query1 = unitOfWork.LoanBriefRepository.Query(x => (x.UtmSource.Contains(request.Name) || request.Name == null), null, false).Where(x => (x.CreatedTime >=  DateTime.Parse(request.FromDate) || DateTime.Parse(request.FromDate) == null) && (x.CreatedTime <= DateTime.Parse(request.ToDate) || DateTime.Parse(request.ToDate) == null)).GroupBy(x => x.UtmSource).Select(x => new MarketingReportUtmDTO
                {
                    UtmSource = x.First().UtmSource,
                    Status = x.First().Status.Value,
                    CountLoanBrief = x.Count(),
                    CountWaitTelesale = x.Count(m => m.Status == 10),
                    CountStatusDisbured = x.Count(m => m.Status == 50),
                    CountStatusCanceled = x.Count(m => m.Status == 99),
                    RateDisbured = 100 * x.Count(m => m.Status == 50) / x.Count(),
                    RateCanceled = 100 * x.Count(m => m.Status == 99) / x.Count(),
                });

                var totalRecords = query1.Count();

                List<MarketingReportUtmDTO> list;
                list = query1.ToList();

                def.Meta = new DefaultReplyMetaSummary()
                {
                    Code = ResponseHelper.SUCCESS_CODE,
                    Message = ResponseHelper.SUCCESS_MESSAGE,
                    Page = request.Page,
                    PageSize = request.PageSize,
                    TotalRecords = 0
                };
                Console.WriteLine(JsonConvert.SerializeObject(list));
                def.Data.AddRange(list);
                Console.WriteLine(JsonConvert.SerializeObject(def));
                return Task.FromResult(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Jobs/GetAllJobs Exception");
                def.Meta = new DefaultReplyMetaSummary()
                {
                    Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
                    Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE,
                    Page = request.Page,
                    PageSize = request.PageSize,
                    TotalRecords = 0
                };
            }
            return Task.FromResult(def);
        }

        [Authorize]
        public override Task<LeadReply> lead(LeadRequest req, ServerCallContext context)
        {
            var def = new LeadReply();
            try
            {

                // validate input
                if (String.IsNullOrEmpty(req.Name) || String.IsNullOrEmpty(req.Phone) || req.LoanAmount <= 0 || req.LoanTime <= 0 || req.CityId <= 0 || req.DistrictId <= 0 || req.LoanProduct <= 0)
                {
                    def.Meta = new DefaultReplyMeta()
                    {
                        Code = ResponseHelper.BAD_INPUT_CODE,
                        Message = ResponseHelper.BAD_INPUT_MESSAGE
                    };
                    Console.WriteLine(JsonConvert.SerializeObject(def));
                    return Task.FromResult(def);
                }

                // check exists
                var exist = unitOfWork.LoanBriefRepository.Any(x => x.Phone == req.Phone && x.Status < 100);
                if (exist)
                {
                    def.Meta = new DefaultReplyMeta()
                    {
                        Code = ResponseHelper.BAD_INPUT_CODE,
                        Message = ResponseHelper.BAD_INPUT_MESSAGE
                    };
                    Console.WriteLine(JsonConvert.SerializeObject(def));
                    return Task.FromResult(def);
                }

                // create loan credit
                var loanCredit = new LOS.DAL.EntityFramework.LoanBrief();

                loanCredit.FullName = req.Name;
                loanCredit.UtmSource = req.UtmSource;
                loanCredit.UtmCampaign = req.UtmCampaign;
                loanCredit.UtmContent = req.UtmContent;
                loanCredit.UtmTerm = req.UtmTerm;
                loanCredit.UtmMedium = req.UtmMedium;
                loanCredit.Phone = req.Phone;
                loanCredit.ProductId = req.LoanProduct;
                loanCredit.Status = 0;
                if (req.LoanTime > 0)
                {
                    loanCredit.LoanTime = req.LoanTime;
                    loanCredit.FromDate = DateTime.Now;
                    loanCredit.ToDate = DateTime.Now.AddMonths(req.LoanTime);
                }
                loanCredit.LoanAmount = req.LoanAmount;
                unitOfWork.LoanBriefRepository.Insert(loanCredit);
                unitOfWork.Save();
                def.Meta = new DefaultReplyMeta()
                {
                    Code = ResponseHelper.SUCCESS_CODE, 
                    Message = ResponseHelper.SUCCESS_MESSAGE
                };
                Log.Information(JsonConvert.SerializeObject(def));
                Console.WriteLine(JsonConvert.SerializeObject(def));
                return Task.FromResult(def);
            }catch(Exception ex)
            {
                Log.Error(ex, "Lead Exception");
                Console.WriteLine(JsonConvert.SerializeObject(ex));
                def.Meta = new DefaultReplyMeta()
                {
                    Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
                    Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE
                };
                return Task.FromResult(def);
            }
        }

        public override Task<DictionaryProvinceReply> getDictionaryProvinces(DictionaryProvinceRequest request, ServerCallContext context)
        {
            var provinces = unitOfWork.ProvinceRepository.All().Select(x => new ProvinceDetail() {
                Latitude = x.Latitude ?? default(double),
                Longitude = x.Longitude ?? default(double),
                Name = x.Name,
                Priority = x.Priority ?? default(int),
                ProvinceId = x.ProvinceId,
                Type = x.Type ?? default(int)
            }).ToList();
            var def = new DictionaryProvinceReply();
            def.Meta = new DefaultReplyMeta(){
                Code= 200,
                Message = "success"
            };
            def.Data.AddRange(provinces);
            return Task.FromResult(def);
        }

        public override Task<DictionaryDistrictReply> getDictionaryDistricts(DictionaryDistrictRequest request, ServerCallContext context)
        {
            var districts = unitOfWork.DistrictRepository.Query(x => x.ProvinceId == request.ProvinceId, null, false).Select(x=> new DistrictDetail{
                 DistrictId = x.DistrictId,
                 ProvinceId = x.ProvinceId ?? default(int),
                 Name = x.Name ?? default(string),
                 Type = x.Type ?? default(int),
                 Latitude = x.Latitude ?? default(double),
                 Longitude = x.Longitude ?? default(double),
                 Priority = x.Priority ?? default(int)
            }).ToList();
            var def = new DictionaryDistrictReply();
            def.Meta = new DefaultReplyMeta()
            {
                Code = 200,
                Message = "success"
            };
            def.Data.AddRange(districts);
            return Task.FromResult(def);
        }

        public override Task<DictionaryWardReply> getDictionaryWards(DictionaryWardRequest request, ServerCallContext context)
        {
            var wards = unitOfWork.WardRepository.Query(x => x.DistrictId == request.DistrictId, null, false).Select(x => new WardDetail
            {
              DistrictId = x.DistrictId ?? default(int),
              WardId = x.WardId,
              Latitude = x.Latitude ?? default(double),
              Longitude = x.Longitude ?? default(double),
              Name = x.Name ?? default(string),
              Priority = x.Priority ?? default(int),
              Type = x.Type ?? default(int)
            }).ToList();
            var def = new DictionaryWardReply();
            def.Meta = new DefaultReplyMeta()
            {
                Code = 200,
                Message = "success"
            };
            def.Data.AddRange(wards);
            return Task.FromResult(def);
        }

        public override Task<DictionaryJobReply> getDictionaryJobs(DictionaryJobRequest request, ServerCallContext context)
        {
            var jobs = unitOfWork.JobRepository.All().Select(x => new JobDetail
            {
                JobId = x.JobId,
                Name = x.Name,
                Priority = x.Priority ?? default(int)
            }).ToList();
            var def = new DictionaryJobReply();
            def.Meta = new DefaultReplyMeta()
            {
                Code = 200,
                Message = "success"
            };
            def.Data.AddRange(jobs);
            return Task.FromResult(def);
        }

        public override Task<AllJobReply> getAllJobs(AllJobRequest request, ServerCallContext context)
        {
            var def = new AllJobReply();

            try
            {
                var query = unitOfWork.JobRepository.Query(x => (x.Name.Contains(request.Name) || (x.Priority.ToString().Contains(request.Name)) || request.Name == null), null, false).Select(x => new JobDetail()
                {
                    JobId = x.JobId,
                    Name = x.Name,
                    Priority = x.Priority ?? default(int)
                });

                var totalRecords = query.Count();
                List<JobDetail> data;
                if (request.Skip >= 0)
                {
                    //data = query.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    data = query.ToList();
                }
                else
                {
                    data = query.Skip(request.Skip).Take(request.Take).ToList();
                }

                def.Meta = new DefaultReplyMetaSummary()
                {
                    Code = 200,
                    Message = "success",
                    Page = request.Page,
                    PageSize = request.PageSize,
                    TotalRecords = totalRecords
                };
                def.Data.AddRange(data);
                return Task.FromResult(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Jobs/GetAllJobs Exception");
                def.Meta = new DefaultReplyMetaSummary()
                {
                    Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
                    Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE,
                    Page = 0,
                    PageSize = 0,
                    TotalRecords = 0
                };
            }
            return Task.FromResult(def);
        }


        public override Task<JobByIdReply> getJobById(JobByIdRequest request, ServerCallContext context)
        {
            var def = new JobByIdReply();
            try
            {
                var job = unitOfWork.JobRepository.GetById(request.JobId);
                def.Data = new JobDetail()
                {
                    JobId = job.JobId,
                    Name = job.Name,
                    Priority = job.Priority ?? default(int)
                };
                def.Meta = new DefaultReplyMeta()
                {
                    Code = ResponseHelper.SUCCESS_CODE,
                    Message = ResponseHelper.SUCCESS_MESSAGE
                };
                return Task.FromResult(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Jobs/GetAllJobs Exception");
                def.Meta = new DefaultReplyMeta()
                {
                    Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
                    Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE
                };
            }
            return Task.FromResult(def);
        }

        public override Task<PostJobReply> postJob(PostJobRequest request, ServerCallContext context)
        {
            var def = new PostJobReply();

            try
            {
                var result = unitOfWork.JobRepository.Insert(new LOS.DAL.EntityFramework.Job() { 
                    JobId = request.JobId,
                    Name = request.Name,
                    Priority = request.Priority
                });
                unitOfWork.Save();

                if (result.JobId > 0)
                {
                    def.Data = result.JobId;
                    def.Meta = new DefaultReplyMeta()
                    {
                        Code = ResponseHelper.SUCCESS_CODE,
                        Message = ResponseHelper.SUCCESS_MESSAGE
                    };
                }
                else
                {
                    def.Data = 0;
                    def.Meta = new DefaultReplyMeta()
                    {
                        Code = ResponseHelper.FAIL_CODE,
                        Message = ResponseHelper.FAIL_MESSAGE
                    };
                }
                return Task.FromResult(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Job/CreateJob Exception");
                def.Meta = new DefaultReplyMeta()
                {
                    Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
                    Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE
                };
            }

            return Task.FromResult(def);
        }
    }

}
