﻿using Grpc.Core;
using LOS.Common.Helpers;
using LOS.DAL.EntityFramework;
using LOS.DAL.UnitOfWork;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TelesaleService.Protos;


namespace TelesaleService.Services
{
    public class TelesaleService : Telesales.TelesalesBase
	{
		private IUnitOfWork unitOfWork;
		private IConfiguration configuration;
		public TelesaleService(IUnitOfWork unitOfWork, IConfiguration configuration)
		{			
			this.unitOfWork = unitOfWork;
			this.configuration = configuration;
		}

		public override Task<DefaultReply> UpdateCallStatus(UpdateCallStatusRequest req, ServerCallContext context)
		{
			var def = new DefaultReply();
			try
			{
				if (req.LoanActionId <= 0 || req.ActionStatusId <= 0)
				{
					def.Meta = new DefaultReplyMeta() { 
						Code = ResponseHelper.BAD_INPUT_CODE,
						Message = ResponseHelper.BAD_INPUT_MESSAGE
					};
					return Task.FromResult(def);
				}
				// check if loan action exists
				var loanAction = unitOfWork.LoanActionRepository.GetById(req.LoanActionId);
				if (loanAction != null)
				{
					// update to new status
					unitOfWork.LoanActionRepository.Update(x => x.LoanActionId == req.LoanActionId, x => new LoanAction()
					{
						ActionStatusId = req.ActionStatusId,
						LastActionStatusId = loanAction.ActionStatusId.Value
					});
					if (req.ActionStatusId >= 14)
					{
						var loanBrief = unitOfWork.LoanBriefRepository.GetById(loanAction.LoanBriefId);
						// kiểm tra nếu về trạng thái finish
						if (loanBrief != null)
						{
							if (loanBrief.Status == 11)
							{
								loanBrief.Status = 10;
								loanBrief.UpdatedTime = DateTimeOffset.Now;
							}
							else if (loanBrief.Status == 14) // Schedule call
							{
								// nếu khách hàng nghe máy ?
								if (req.ActionStatusId == 14)
								{
									loanBrief.Status = 15;
									loanBrief.UpdatedTime = DateTimeOffset.Now;
									// update schedule status
									unitOfWork.ScheduleRepository.Update(x => x.LoanBriefId == loanBrief.LoanBriefId, x => new Schedule()
									{
										Status = 10
									});
								}
								else
								{
									loanBrief.Status = 14;
									loanBrief.UpdatedTime = DateTimeOffset.Now;
									// update schedule status
									unitOfWork.ScheduleRepository.Update(x => x.LoanBriefId == loanBrief.LoanBriefId, x => new Schedule()
									{
										Status = 0
									});
								}
							}
							else
							{
								loanBrief.Status = 15;
								loanBrief.UpdatedTime = DateTimeOffset.Now;
							}
							unitOfWork.LoanBriefRepository.Update(loanBrief);
							unitOfWork.Save();
						}
						else
						{
							def.Meta = new DefaultReplyMeta()
							{
								Code = ResponseHelper.NOT_FOUND_CODE,
								Message = ResponseHelper.NOT_FOUND_MESSAGE
							};
							return Task.FromResult(def);
						}
					}
					unitOfWork.Save();
					def.Meta = new DefaultReplyMeta()
					{
						Code = ResponseHelper.SUCCESS_CODE,
						Message = ResponseHelper.SUCCESS_MESSAGE
					};
					return Task.FromResult(def);
				}
				else
				{
					def.Meta = new DefaultReplyMeta()
					{
						Code = ResponseHelper.NOT_FOUND_CODE,
						Message = ResponseHelper.NOT_FOUND_MESSAGE
					};
					return Task.FromResult(def);
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Telesales/Update Call Status Exception");
				def.Meta = new DefaultReplyMeta()
				{
					Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
					Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE
				};
				return Task.FromResult(def);
			}			
		}

		public override Task<DefaultReply> UpdateScheduleCall(UpdateScheduleCallRequest req, ServerCallContext context)
		{
			var def = new DefaultReply();
			try
			{
				if (req.LoanActionId <= 0 || req.ActionStatusId <= 0)
				{
					def.Meta = new DefaultReplyMeta()
					{
						Code = ResponseHelper.BAD_INPUT_CODE,
						Message = ResponseHelper.BAD_INPUT_MESSAGE
					};
					return Task.FromResult(def);
				}
				// check if loan action exists
				var loanAction = unitOfWork.LoanActionRepository.GetById(req.LoanActionId);
				if (loanAction != null)
				{
					// update to new status
					unitOfWork.LoanActionRepository.Update(x => x.LoanActionId == req.LoanActionId, x => new LoanAction()
					{
						ActionStatusId = req.ActionStatusId,
						LastActionStatusId = loanAction.ActionStatusId.Value
					});
					// update loan status
					unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanAction.LoanBriefId, x => new LoanBrief()
					{
						Status = 14,
						BoundTelesaleId = loanAction.ActorId
					});
					// set schedule 
					unitOfWork.ScheduleRepository.Insert(new Schedule()
					{
						CreatedAt = DateTimeOffset.Now,
						LoanBriefId = loanAction.LoanBriefId,
						ScheduleTime = DateTimeOffset.ParseExact(req.ScheduleTime, "dd/MM/yyyy HH:mm", null),
						Status = 0,
						UserId = loanAction.ActorId
					});
					unitOfWork.Save();
					def.Meta = new DefaultReplyMeta()
					{
						Code = ResponseHelper.SUCCESS_CODE,
						Message = ResponseHelper.SUCCESS_MESSAGE
					};
					return Task.FromResult(def);
				}
				else
				{
					def.Meta = new DefaultReplyMeta()
					{
						Code = ResponseHelper.NOT_FOUND_CODE,
						Message = ResponseHelper.NOT_FOUND_MESSAGE
					};
					return Task.FromResult(def);
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Telesales/Update Call Status Exception");
				def.Meta = new DefaultReplyMeta()
				{
					Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
					Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE
				};
				return Task.FromResult(def);
			}			
		}

		public override Task<GetMissedCallReply> GetMissedCalls(GetMissedCallRequest request, ServerCallContext context)
		{
			var def = new GetMissedCallReply();
			try
			{
				var query = (from loan in unitOfWork.LoanBriefRepository.All()
							 join district in unitOfWork.DistrictRepository.All() on loan.DistrictId equals district.DistrictId into LoanDistricts
							 from LoanDistrict in LoanDistricts.DefaultIfEmpty()
							 join province in unitOfWork.ProvinceRepository.All() on loan.ProvinceId equals province.ProvinceId into LoanProvinces
							 from LoanProvince in LoanProvinces.DefaultIfEmpty()
							 join product in unitOfWork.LoanProductRepository.All() on loan.ProductId equals product.LoanProductId into LoanProducts
							 from LoanProduct in LoanProducts.DefaultIfEmpty()
							 join loanAction in unitOfWork.LoanActionRepository.All() on loan.LoanBriefId equals loanAction.LoanBriefId
							 where loanAction.ActionStatusId == 6 && loanAction.ActorId == request.UserId // missed call
							 orderby loanAction.StartTime descending
							 select new TelesaleLoanBrief()
							 {
								 CreatedTime = Google.Protobuf.WellKnownTypes.Timestamp.FromDateTimeOffset(loan.CreatedTime.Value),
								 DistrictId = loan.DistrictId.Value,
								 District = LoanDistrict != null ? new DistrictReply()
								 {
									 DistrictId = LoanDistrict.DistrictId,
									 Name = LoanDistrict.Name
								 } : null,
								 FullName = loan.FullName,
								 LoanAmount = (double)loan.LoanAmount,
								 IsRetrieved = true,
								 LoanActionId = loanAction.LoanActionId,
								 LoanBriefId = loan.LoanBriefId,
								 LoanTime = loan.LoanTime.Value,
								 Phone = loan.Phone,
								 Product = LoanProduct != null ? new ProductReply()
								 {
									 ProductId = LoanProduct.LoanProductId,
									 Name = LoanProduct.Name
								 } : null,
								 ProductId = loan.ProductId.Value,
								 Province = LoanProvince != null ? new ProvinceReply()
								 {
									 ProvinceId = LoanProvince.ProvinceId,
									 Name = LoanProvince.Name
								 } : null,
								 ProvinceId = loan.ProvinceId.Value,
								 RateTypeId = loan.RateTypeId.Value,
								 StartTime = Google.Protobuf.WellKnownTypes.Timestamp.FromDateTimeOffset(loanAction.StartTime.Value)
							 });
				var totalRecords = query.Count();
				List<TelesaleLoanBrief> data;
				if (request.Skip == -1)
					data = query.Skip((request.Page - 1) * request.PageSize).Take(request.PageSize).ToList();
				else
					data = query.Skip(request.Skip).Take(request.PageSize).ToList();
				def.Meta = new DefaultReplyMetaSummary()
				{
					Code = ResponseHelper.SUCCESS_CODE,
					Message = ResponseHelper.SUCCESS_MESSAGE,
					Page = request.Page,
					PageSize = request.PageSize,
					TotalRecords = totalRecords
				};
				def.Data.AddRange(data);
				return Task.FromResult(def);
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Telesales/Get Missed Calls Exception");
				def.Meta = new DefaultReplyMetaSummary()
				{
					Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
					Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE,
					Page = request.Page,
					PageSize = request.PageSize,
					TotalRecords = 0
				};
				return Task.FromResult(def);
			}			
		}

		public override Task<GetMissedCallReply> GetScheduleCalls(GetMissedCallRequest request, ServerCallContext context)
		{
			var def = new GetMissedCallReply();
			try
			{
				var query = (from loan in unitOfWork.LoanBriefRepository.All()
							 join district in unitOfWork.DistrictRepository.All() on loan.DistrictId equals district.DistrictId into LoanDistricts
							 from LoanDistrict in LoanDistricts.DefaultIfEmpty()
							 join province in unitOfWork.ProvinceRepository.All() on loan.ProvinceId equals province.ProvinceId into LoanProvinces
							 from LoanProvince in LoanProvinces.DefaultIfEmpty()
							 join product in unitOfWork.LoanProductRepository.All() on loan.ProductId equals product.LoanProductId into LoanProducts
							 from LoanProduct in LoanProducts.DefaultIfEmpty()
							 join schedule in unitOfWork.ScheduleRepository.All() on loan.LoanBriefId equals schedule.LoanBriefId
							 where loan.Status == 14 // scheduled 
							 orderby schedule.ScheduleTime descending
							 select new TelesaleLoanBrief()
							 {
								 CreatedTime = Google.Protobuf.WellKnownTypes.Timestamp.FromDateTimeOffset(loan.CreatedTime.Value),
								 DistrictId = loan.DistrictId.Value,
								 District = LoanDistrict != null ? new DistrictReply()
								 {
									 DistrictId = LoanDistrict.DistrictId,
									 Name = LoanDistrict.Name
								 } : null,
								 FullName = loan.FullName,
								 LoanAmount = (double)loan.LoanAmount,
								 IsRetrieved = true,
								 LoanBriefId = loan.LoanBriefId,
								 LoanTime = loan.LoanTime.Value,
								 Phone = loan.Phone,
								 Product = LoanProduct != null ? new ProductReply()
								 {
									 ProductId = LoanProduct.LoanProductId,
									 Name = LoanProduct.Name
								 } : null,
								 ProductId = loan.ProductId.Value,
								 Province = LoanProvince != null ? new ProvinceReply()
								 {
									 ProvinceId = LoanProvince.ProvinceId,
									 Name = LoanProvince.Name
								 } : null,
								 ProvinceId = loan.ProvinceId.Value,
								 RateTypeId = loan.RateTypeId.Value,
								 ScheduleTime = Google.Protobuf.WellKnownTypes.Timestamp.FromDateTimeOffset(schedule.ScheduleTime.Value),
								 ScheduleStatus = schedule.Status.Value
							 });
				var totalRecords = query.Count();
				List<TelesaleLoanBrief> data;
				if (request.Skip == -1)
					data = query.Skip((request.Page - 1) * request.PageSize).Take(request.PageSize).ToList();
				else
					data = query.Skip(request.Skip).Take(request.PageSize).ToList();
				def.Meta = new DefaultReplyMetaSummary()
				{
					Code = ResponseHelper.SUCCESS_CODE,
					Message = ResponseHelper.SUCCESS_MESSAGE,
					Page = request.Page,
					PageSize = request.PageSize,
					TotalRecords = totalRecords
				};
				def.Data.AddRange(data);
				return Task.FromResult(def);
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Telesales/Get Missed Calls Exception");
				def.Meta = new DefaultReplyMetaSummary()
				{
					Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
					Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE,
					Page = request.Page,
					PageSize = request.PageSize,
					TotalRecords = 0
				};
				return Task.FromResult(def);
			}			
		}

		public override Task<GetMissedCallReply> GetCalled(GetMissedCallRequest request, ServerCallContext context)
		{
			var def = new GetMissedCallReply();
			try
			{
				var calledStatus = unitOfWork.ActionStatusRepository.Query(x => x.ActionId == 3, null, false).Select(x => x.ActionStatusId).ToList();
				var query = (from loan in unitOfWork.LoanBriefRepository.All()
							 join district in unitOfWork.DistrictRepository.All() on loan.DistrictId equals district.DistrictId into LoanDistricts
							 from LoanDistrict in LoanDistricts.DefaultIfEmpty()
							 join province in unitOfWork.ProvinceRepository.All() on loan.ProvinceId equals province.ProvinceId into LoanProvinces
							 from LoanProvince in LoanProvinces.DefaultIfEmpty()
							 join product in unitOfWork.LoanProductRepository.All() on loan.ProductId equals product.LoanProductId into LoanProducts
							 from LoanProduct in LoanProducts.DefaultIfEmpty()
							 join loanAction in unitOfWork.LoanActionRepository.Query(x => calledStatus.Contains(x.ActionStatusId.Value) && x.ActorId == request.UserId, x => x.OrderByDescending(x1 => x1.StartTime), 1, 1, false, x => x.ActionStatus) on loan.LoanBriefId equals loanAction.LoanBriefId
							 orderby loan.UpdatedTime descending
							 select new TelesaleLoanBrief()
							 {
								 CreatedTime = Google.Protobuf.WellKnownTypes.Timestamp.FromDateTimeOffset(loan.CreatedTime.Value),
								 DistrictId = loan.DistrictId.Value,
								 District = LoanDistrict != null ? new DistrictReply()
								 {
									 DistrictId = LoanDistrict.DistrictId,
									 Name = LoanDistrict.Name
								 } : null,
								 FullName = loan.FullName,
								 LoanAmount = (double)loan.LoanAmount,
								 IsRetrieved = true,
								 LoanBriefId = loan.LoanBriefId,
								 LoanTime = loan.LoanTime.Value,
								 Phone = loan.Phone,
								 Product = LoanProduct != null ? new ProductReply()
								 {
									 ProductId = LoanProduct.LoanProductId,
									 Name = LoanProduct.Name
								 } : null,
								 ProductId = loan.ProductId.Value,
								 Province = LoanProvince != null ? new ProvinceReply()
								 {
									 ProvinceId = LoanProvince.ProvinceId,
									 Name = LoanProvince.Name
								 } : null,
								 ProvinceId = loan.ProvinceId.Value,
								 RateTypeId = loan.RateTypeId.Value,
								 Status = loan.Status.Value,
								 LoanAction = new LoanActionDetailReply()
								 {
									 LoanActionId = loanAction.LoanActionId,
									 StartTime = Google.Protobuf.WellKnownTypes.Timestamp.FromDateTimeOffset(loanAction.StartTime.Value),
									 FinishTime = Google.Protobuf.WellKnownTypes.Timestamp.FromDateTimeOffset(loanAction.FinishTime.Value),
									 Note = loanAction.Note,
									 ActionStatusText = loanAction.ActionStatus.Description,
									 ActorId = loanAction.ActorId.Value
								 }
							 });
				var totalRecords = query.Count();
				List<TelesaleLoanBrief> data;
				if (request.Skip == -1)
					data = query.Skip((request.Page - 1) * request.PageSize).Take(request.PageSize).ToList();
				else
					data = query.Skip(request.Skip).Take(request.PageSize).ToList();
				def.Meta = new DefaultReplyMetaSummary()
				{
					Code = ResponseHelper.SUCCESS_CODE,
					Message = ResponseHelper.SUCCESS_MESSAGE,
					Page = request.Page,
					PageSize = request.PageSize,
					TotalRecords = totalRecords
				};
				def.Data.AddRange(data);
				return Task.FromResult(def);
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Telesales/Get Missed Calls Exception");
				def.Meta = new DefaultReplyMetaSummary()
				{
					Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
					Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE,
					Page = request.Page,
					PageSize = request.PageSize,
					TotalRecords = 0
				};
				return Task.FromResult(def);
			}			
		}

		public override Task<DefaultReply> UpdatedMissedCall(UpdateCallStatusRequest req, ServerCallContext context)
		{
			var def = new DefaultReply();
			try
			{
				if (req.LoanActionId <= 0 || req.ActionStatusId <= 0)
				{
					def.Meta = new DefaultReplyMeta()
					{
						Code = ResponseHelper.BAD_INPUT_CODE,
						Message = ResponseHelper.BAD_INPUT_MESSAGE
					};
					return Task.FromResult(def);
				}
				// check if loan action exists
				var loanAction = unitOfWork.LoanActionRepository.GetById(req.LoanActionId);
				if (loanAction != null)
				{
					// update to new status
					unitOfWork.LoanActionRepository.Update(x => x.LoanActionId == req.LoanActionId, x => new LoanAction()
					{
						ActionStatusId = req.ActionStatusId,
						LastActionStatusId = loanAction.ActionStatusId.Value
					});
					// kiểm tra nếu về trạng thái finish
					unitOfWork.LoanBriefRepository.Update(x => x.LoanBriefId == loanAction.LoanBriefId, x => new LoanBrief()
					{
						Status = 15,
						UpdatedTime = DateTimeOffset.Now
					});
					unitOfWork.Save();
					def.Meta = new DefaultReplyMeta()
					{
						Code = ResponseHelper.SUCCESS_CODE,
						Message = ResponseHelper.SUCCESS_MESSAGE
					};
					return Task.FromResult(def);
				}
				else
				{
					def.Meta = new DefaultReplyMeta()
					{
						Code = ResponseHelper.NOT_FOUND_CODE,
						Message = ResponseHelper.NOT_FOUND_MESSAGE
					};
					return Task.FromResult(def);
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Telesales/Update missed call  Exception");
				def.Meta = new DefaultReplyMeta()
				{
					Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
					Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE
				};
				return Task.FromResult(def);
			}			
		}

		public override Task<DefaultReply> BoundTelesale(BoundTelesaleRequest req, ServerCallContext context)
		{
			var def = new DefaultReply();
			try
			{
				if (req.LoanBriefId <= 0 || req.UserId <= 0)
				{
					def.Meta = new DefaultReplyMeta()
					{
						Code = ResponseHelper.BAD_INPUT_CODE,
						Message = ResponseHelper.BAD_INPUT_MESSAGE
					};
					return Task.FromResult(def);
				}
				// check if loan action exists
				var loanBrief = unitOfWork.LoanBriefRepository.GetById(req.LoanBriefId);
				if (loanBrief != null)
				{
					if (loanBrief.BoundTelesaleId != req.UserId && loanBrief.BoundTelesaleId != null)
					{
						def.Meta = new DefaultReplyMeta()
						{
							Code = ResponseHelper.BAD_INPUT_CODE,
							Message = ResponseHelper.BAD_INPUT_MESSAGE
						};
						return Task.FromResult(def);
					}
					if (loanBrief.Status == 15)
					{
						// bound
						loanBrief.BoundTelesaleId = req.UserId;
						loanBrief.Status = 16; // Bound to telesale;
						loanBrief.UpdatedTime = DateTimeOffset.Now;
					}
					else
					{
						// unbound
						loanBrief.BoundTelesaleId = null;
						loanBrief.Status = 15; // Bound to telesale;
						loanBrief.UpdatedTime = DateTimeOffset.Now;
					}
					unitOfWork.LoanBriefRepository.Update(loanBrief);
					unitOfWork.Save();
					def.Meta = new DefaultReplyMeta()
					{
						Code = ResponseHelper.SUCCESS_CODE,
						Message = ResponseHelper.SUCCESS_MESSAGE
					};
					return Task.FromResult(def);
				}
				else
				{
					def.Meta = new DefaultReplyMeta()
					{
						Code = ResponseHelper.NOT_FOUND_CODE,
						Message = ResponseHelper.NOT_FOUND_MESSAGE
					};
					return Task.FromResult(def);
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Telesales/Update missed call  Exception");
				def.Meta = new DefaultReplyMeta()
				{
					Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
					Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE
				};
				return Task.FromResult(def);
			}			
		}

		public override Task<GetMissedCallReasonsReply> GetMissedCallReasons(GetMissedCallReasonsRequest request, ServerCallContext context)
		{
			var def = new GetMissedCallReasonsReply();
			try
			{
				var reasons = (from actionStatus in unitOfWork.ActionStatusRepository.All()
							   join action in unitOfWork.ActionRepository.All() on actionStatus.ActionId equals action.ActionId
							   where action.ActionCode == "MISSED_CALL_REASONS"
							   select new ActionStatusReply()
							   {
								   Action = actionStatus.Action,
								   ActionStatusId = actionStatus.ActionStatusId,
								   Description = actionStatus.Description
							   }).ToList();
				def.Meta = new DefaultReplyMeta()
				{
					Code = ResponseHelper.SUCCESS_CODE,
					Message = ResponseHelper.SUCCESS_MESSAGE
				};
				def.Data.AddRange(reasons);
				return Task.FromResult(def);
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Telesales/Get Missed Calls Exception");
				def.Meta = new DefaultReplyMeta()
				{
					Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
					Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE
				};
				return Task.FromResult(def);
			}			
		}
	}
}
