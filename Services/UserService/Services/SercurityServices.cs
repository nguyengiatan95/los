﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Grpc.Core;
using LOS.Common.Helpers;
using LOS.DAL.DTOs;
using LOS.DAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using GrpcUserServices;
using Serilog;
using Microsoft.Extensions.Logging;

namespace UserService.Services
{
    public class SercurityServices : Sercurity.SercurityBase
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration configuration;
       private IMapper _mapper;

        public SercurityServices(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this.configuration = configuration;
            this._mapper = mapper;
        }
        public override Task<LoginReply> Login(LoginItem req, ServerCallContext context)
        {
            var def = new LoginReply();
            try
            {
                req.Password = req.Password.ToLower();
                var data = new UserDetailResponse();
                var dataDb = _unitOfWork.UserRepository.Query(x => x.Username == req.Username
                                        && x.Password == req.Password, null, false, x => x.Company, x => x.Department, x => x.Position)
                                        .Include(x => x.UserModule).Select(UserDetail.ProjectionDetail).FirstOrDefault();

                data.UserId = dataDb.UserId;
                data.Username = dataDb.Username;
                data.FullName = dataDb.FullName;
                data.Email = dataDb.Email;
                data.Phone = dataDb.Phone;
                data.GroupId = dataDb.GroupId??0;
                data.Status = dataDb.Status??0;
                data.Company = new GrpcUserServices.CompanyDTO(){CompanyId = dataDb.Company.CompanyId, Name = dataDb.Company.Name};
                data.Group = new GrpcUserServices.GroupDetail() { GroupId = dataDb.Group.GroupId, GroupName = dataDb.Group.GroupName, DefaultPath = dataDb.Group.DefaultPath??string.Empty} ;
             
                if(dataDb.Modules != null && dataDb.Modules.Count > 0)
                {
                    for(int i = 0; i < dataDb.Modules.Count; i++)
                    {
                        var objItem = new GrpcUserServices.ModuleDetail
                        {
                            Code = dataDb.Modules[i].Code ?? string.Empty,
                            Description = dataDb.Modules[i].Description ?? string.Empty,
                            IsMenu = dataDb.Modules[i].IsMenu,
                            ModuleId = dataDb.Modules[i].ModuleId,
                            Name = dataDb.Modules[i].Name ?? string.Empty,
                            ParentId = dataDb.Modules[i].ParentId ?? 0,
                            Path = dataDb.Modules[i].Path ?? string.Empty
                        };
                        data.ListModules.Insert(i, objItem);
                    }
                }
                if (data != null)
                {
                    #region insert into log

                    #endregion
                    #region generate token
                    //var claims = new[]{
                    //                                new Claim(ClaimTypes.NameIdentifier, data.UserId.ToString()),
                    //                                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    //                                new Claim("Id", data.UserId.ToString()),
                    //                                new Claim("CompanyId" , data.Company != null ? data.Company?.CompanyId.ToString() : ""),
                    //                                new Claim("DepartmentId" , data.Department != null ? data.Department?.DepartmentId.ToString() : ""),
                    //                                new Claim("PositionId" , data.Position != null ? data.Position?.PositionId.ToString() : "")
                    //                            };

                    //var token = new JwtSecurityToken
                    //(
                    //    issuer: configuration["JWT:Issuer"],
                    //    audience: configuration["JWT:Audience"],
                    //    claims: claims,
                    //    expires: DateTime.UtcNow.AddDays(90),
                    //    notBefore: DateTime.UtcNow,
                    //    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:SigningKey"])),
                    //            SecurityAlgorithms.HmacSha256)
                    //);

                    //data.Token = new JwtSecurityTokenHandler().WriteToken(token);
                    //#endregion
                    //#region get user permission					
                    //var group = _unitOfWork.UserRepository.Query(x => x.UserId == data.UserId, null, false)
                    //    .Include(x => x.Group).ThenInclude(x => x.GroupModule)
                    //    //.ThenInclude(x => x.GroupModulePermission).ThenInclude(x => x.Permission)
                    //    .Select(x => x.Group).Select(GroupDetail.ProjectionDetail).FirstOrDefault();
                    //data.Group = group;


                    #endregion
                    def.Data = data;

                    def.Meta = new DefaultReplyMeta()
                    {
                        Code = ResponseHelper.SUCCESS_CODE,
                        Message = ResponseHelper.SUCCESS_MESSAGE
                    };
                }
                else
                {
                    if (_unitOfWork.UserRepository.Any(x => x.Username == req.Username))
                    {
                        def.Meta = new DefaultReplyMeta()
                        {
                            Code = ResponseHelper.USER_WRONG_PASSWORD_CODE,
                            Message = ResponseHelper.USER_WRONG_PASSWORD_MESSAGE
                        };
                    }
                    else
                    {
                        def.Meta = new DefaultReplyMeta()
                        {
                            Code = ResponseHelper.NOT_FOUND_CODE,
                            Message = ResponseHelper.NOT_FOUND_MESSAGE
                        };
                    }
                }
                return Task.FromResult(def);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Security/Login exception");
                def.Meta = new DefaultReplyMeta()
                {
                    Code = ResponseHelper.INTERNAL_SERVER_ERROR_CODE,
                    Message = ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE
                };
                return Task.FromResult(def);
            }
        }
    }
}
