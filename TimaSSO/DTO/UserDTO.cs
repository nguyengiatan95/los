﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TimaSSO.EntityFramework;

namespace TimaSSO.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        //public DateTime? CreatedTime { get; set; }
        //public DateTime? UpdateTime { get; set; }
    }

    public class UserDetail : UserDTO
    {
        public static Expression<Func<User, UserDetail>> ProjectionDetail
        {
            get
            {
                return x => new UserDetail
                {
                    Id = x.Id,
                    Username = x.Username,
                    Email = x.Email
                };
            }
        }

    }
}
