﻿using System;
using System.Collections.Generic;

namespace TimaSSO.EntityFramework
{
    public partial class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
