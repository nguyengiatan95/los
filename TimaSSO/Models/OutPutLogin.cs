﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TimaSSO.Models
{
    public class OutPutLogin
    {
        public string Token { get; set; }
    }
}
