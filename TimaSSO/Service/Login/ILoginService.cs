﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimaSSO.EntityFramework;


namespace TimaSSO.Service.Login
{
    public interface ILoginService
    {
        int Create(string access_token, LogLogin  entity);
        int Update(string access_token, LogLogin entity);
    }
}
