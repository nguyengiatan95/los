﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimaSSO.EntityFramework;

namespace TimaSSO.Service.Login
{
    public class LoginService : ILoginService
    {
        private readonly TimaSSOContext _dbContext;
        public LoginService(TimaSSOContext dbContext)
        {
            _dbContext = dbContext;
        }
        public int Create(string access_token, LogLogin entity)
        {
            try
            {
                _dbContext.LogLogin.Add(entity);
                _dbContext.SaveChanges();

                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Update(string access_token, LogLogin entity)
        {
            try
            {
                _dbContext.LogLogin.Update(entity);
                _dbContext.Entry(entity).State = EntityState.Modified;
                _dbContext.SaveChanges();
                return entity.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
