﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimaSSO.EntityFramework;
using TimaSSO.Models;
using static TimaSSO.Models.UserReq;

namespace TimaSSO.Service.Users
{
    public interface IUserService
    {
        List<User> GetList(int page, int pageSize);
        OutPutCreate Create(User entity);
        int Update(User entity);
        bool Delete(int id);
        User Login(LoginReq req);
        User GetInfo(int id);
        int ChangePass(ChangePass req);
        
    }
}
