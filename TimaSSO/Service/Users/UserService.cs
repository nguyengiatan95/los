﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimaSSO.EntityFramework;
using TimaSSO.Models;
using TimaSSO.Utils;
using static TimaSSO.Models.UserReq;

namespace TimaSSO.Service.Users
{
    public class UserService : IUserService
    {
        private readonly TimaSSOContext _dbContext;
        public UserService(TimaSSOContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int ChangePass(ChangePass req)
        {
            int result = 0;
            var data = _dbContext.User.Where(x => x.Id == req.Id).FirstOrDefault();
            if(data != null)
            {
                result = data.Id;
            }
            return result;
        }

        public OutPutCreate Create(User entity)
        {
            try
            {
                entity.Email = entity.Email.ToLower();
                //Check xem user đã tồn tại trên hệ thống hay chưa
                var info = _dbContext.User.Where(x => x.Username == entity.Username || x.Email == entity.Email).FirstOrDefault();
                var obj = new OutPutCreate();
                var objData = new DataUserOutPut();
                obj.Check = 0;
                if(info != null)
                {
                    objData.Username = info.Username;
                    objData.Email = info.Email;
                    objData.Id = info.Id;
                    obj.Check = 1;
                    obj.Data = objData;
                }else
                {
                    _dbContext.User.Add(entity);
                    _dbContext.SaveChanges();
                    objData.Username = entity.Username;
                    objData.Email = entity.Email;
                    objData.Id = entity.Id;
                    obj.Check = 2;
                    obj.Data = objData;
                }
                
                
                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var data = _dbContext.User.Find(id);
                data.IsDeleted = true;
                _dbContext.User.Update(data);
                _dbContext.Entry(data).State = EntityState.Modified;
                _dbContext.SaveChanges();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
            
        }


        public User GetInfo(int id)
        {
            var data = _dbContext.User.Where(x => x.Id == id).FirstOrDefault();
            return data;
        }

        public List<User> GetList(int page, int pageSize)
        {
            var data = _dbContext.User.Where(x => x.Status == 1 && x.IsDeleted == false).ToList();
            data = data.OrderByDescending(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return data;
        }

        public User Login(LoginReq req)
        {
            var data = new User();
            if(req.Username != null && req.Password != null)
            {
                req.Password = Security.GetMD5Hash(req.Password.ToLower());
                data = _dbContext.User.Where(x => x.Status == 1 && (x.Username == req.Username || x.Email == req.Username) && x.Password == req.Password).FirstOrDefault();
            }
            if (req.Email != null)
            {
                data = _dbContext.User.Where(x => x.Email == req.Email.ToLower()).FirstOrDefault();
            }
            
            return data;
        }

        public int Update(User entity)
        {
            try
            {
                _dbContext.User.Update(entity);
                _dbContext.Entry(entity).State = EntityState.Modified;
                _dbContext.SaveChanges();
                return entity.Id;
            }catch(Exception ex)
            {
                return 0;
            }
            
        }

        

    }
}
