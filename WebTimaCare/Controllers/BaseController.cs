﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebTimaCare.Entity;
using WebTimaCare.Helpers;

namespace WebTimaCare.Controllers
{
    public class BaseController : Controller
    {
       
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //var user = GetUserGlobal();
            //if (user == null || string.IsNullOrEmpty(user.Token))
            //    filterContext.Result = new RedirectResult("/");
            base.OnActionExecuted(filterContext);
        }
        protected string GetToken()
        {
            return HttpContext.Session.GetObjectFromJson<RequestVerifyOtp>("SessionToken").Result.Token;
        }
        protected RequestVerifyOtp GetUserGlobal()
        {
            return HttpContext.Session.GetObjectFromJson<RequestVerifyOtp>("SessionToken").Result;
        }       
    }
}